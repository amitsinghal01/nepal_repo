package terms.eng.setup.model.view;

import java.math.BigDecimal;

import oracle.jbo.RowSet;
import oracle.jbo.domain.Number;
import oracle.jbo.server.EntityImpl;
import oracle.jbo.server.ViewRowImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Tue Mar 23 15:03:19 IST 2021
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class AltBomVORowImpl extends ViewRowImpl {
    public static final int ENTITY_ALTBOMEO = 0;
    public static final int ENTITY_ITEMSTOCKEO = 1;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        AltNoOff,
        AltPartCd,
        PartCd,
        ProdCode,
        ProdId,
        ProdLineId,
        ItemDesc,
        ItemCd,
        PartTyp,
        TransLovName,
        ObjectVersionNumber,
        TransPageVal,
        AltPartVO1,
        PartMasterVO1,
        ProductsVO1,
        PartCodeSourceVO1;
        static AttributesEnum[] vals = null;
        ;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int ALTNOOFF = AttributesEnum.AltNoOff.index();
    public static final int ALTPARTCD = AttributesEnum.AltPartCd.index();
    public static final int PARTCD = AttributesEnum.PartCd.index();
    public static final int PRODCODE = AttributesEnum.ProdCode.index();
    public static final int PRODID = AttributesEnum.ProdId.index();
    public static final int PRODLINEID = AttributesEnum.ProdLineId.index();
    public static final int ITEMDESC = AttributesEnum.ItemDesc.index();
    public static final int ITEMCD = AttributesEnum.ItemCd.index();
    public static final int PARTTYP = AttributesEnum.PartTyp.index();
    public static final int TRANSLOVNAME = AttributesEnum.TransLovName.index();
    public static final int OBJECTVERSIONNUMBER = AttributesEnum.ObjectVersionNumber.index();
    public static final int TRANSPAGEVAL = AttributesEnum.TransPageVal.index();
    public static final int ALTPARTVO1 = AttributesEnum.AltPartVO1.index();
    public static final int PARTMASTERVO1 = AttributesEnum.PartMasterVO1.index();
    public static final int PRODUCTSVO1 = AttributesEnum.ProductsVO1.index();
    public static final int PARTCODESOURCEVO1 = AttributesEnum.PartCodeSourceVO1.index();

    /**
     * This is the default constructor (do not remove).
     */
    public AltBomVORowImpl() {
    }

    /**
     * Gets AltBomEO entity object.
     * @return the AltBomEO
     */
    public EntityImpl getAltBomEO() {
        return (EntityImpl) getEntity(ENTITY_ALTBOMEO);
    }

    /**
     * Gets ItemStockEO entity object.
     * @return the ItemStockEO
     */
    public EntityImpl getItemStockEO() {
        return (EntityImpl) getEntity(ENTITY_ITEMSTOCKEO);
    }

    /**
     * Gets the attribute value for ALT_NO_OFF using the alias name AltNoOff.
     * @return the ALT_NO_OFF
     */
    public BigDecimal getAltNoOff() {
        return (BigDecimal) getAttributeInternal(ALTNOOFF);
    }

    /**
     * Sets <code>value</code> as attribute value for ALT_NO_OFF using the alias name AltNoOff.
     * @param value value to set the ALT_NO_OFF
     */
    public void setAltNoOff(BigDecimal value) {
        setAttributeInternal(ALTNOOFF, value);
    }

    /**
     * Gets the attribute value for ALT_PART_CD using the alias name AltPartCd.
     * @return the ALT_PART_CD
     */
    public String getAltPartCd() {
        return (String) getAttributeInternal(ALTPARTCD);
    }

    /**
     * Sets <code>value</code> as attribute value for ALT_PART_CD using the alias name AltPartCd.
     * @param value value to set the ALT_PART_CD
     */
    public void setAltPartCd(String value) {
        setAttributeInternal(ALTPARTCD, value);
    }

    /**
     * Gets the attribute value for PART_CD using the alias name PartCd.
     * @return the PART_CD
     */
    public String getPartCd() {
        return (String) getAttributeInternal(PARTCD);
    }

    /**
     * Sets <code>value</code> as attribute value for PART_CD using the alias name PartCd.
     * @param value value to set the PART_CD
     */
    public void setPartCd(String value) {
        setAttributeInternal(PARTCD, value);
    }

    /**
     * Gets the attribute value for PROD_CODE using the alias name ProdCode.
     * @return the PROD_CODE
     */
    public String getProdCode() {
        return (String) getAttributeInternal(PRODCODE);
    }

    /**
     * Sets <code>value</code> as attribute value for PROD_CODE using the alias name ProdCode.
     * @param value value to set the PROD_CODE
     */
    public void setProdCode(String value) {
        setAttributeInternal(PRODCODE, value);
    }

    /**
     * Gets the attribute value for PROD_ID using the alias name ProdId.
     * @return the PROD_ID
     */
    public Long getProdId() {
        return (Long) getAttributeInternal(PRODID);
    }

    /**
     * Sets <code>value</code> as attribute value for PROD_ID using the alias name ProdId.
     * @param value value to set the PROD_ID
     */
    public void setProdId(Long value) {
        setAttributeInternal(PRODID, value);
    }

    /**
     * Gets the attribute value for PROD_LINE_ID using the alias name ProdLineId.
     * @return the PROD_LINE_ID
     */
    public Long getProdLineId() {
        return (Long) getAttributeInternal(PRODLINEID);
    }

    /**
     * Sets <code>value</code> as attribute value for PROD_LINE_ID using the alias name ProdLineId.
     * @param value value to set the PROD_LINE_ID
     */
    public void setProdLineId(Long value) {
        setAttributeInternal(PRODLINEID, value);
    }

    /**
     * Gets the attribute value for ITEM_DESC using the alias name ItemDesc.
     * @return the ITEM_DESC
     */
    public String getItemDesc() {
        return (String) getAttributeInternal(ITEMDESC);
    }

    /**
     * Sets <code>value</code> as attribute value for ITEM_DESC using the alias name ItemDesc.
     * @param value value to set the ITEM_DESC
     */
    public void setItemDesc(String value) {
        setAttributeInternal(ITEMDESC, value);
    }

    /**
     * Gets the attribute value for ITEM_CD using the alias name ItemCd.
     * @return the ITEM_CD
     */
    public String getItemCd() {
        return (String) getAttributeInternal(ITEMCD);
    }

    /**
     * Sets <code>value</code> as attribute value for ITEM_CD using the alias name ItemCd.
     * @param value value to set the ITEM_CD
     */
    public void setItemCd(String value) {
        setAttributeInternal(ITEMCD, value);
    }

    /**
     * Gets the attribute value for PART_TYP using the alias name PartTyp.
     * @return the PART_TYP
     */
    public String getPartTyp() {
//        if(getAttributeInternal(PARTTYP)==null){
//            setTransPageVal("Products");
//        }
//        else{
//            setTransPageVal("Part Master");
//        }
        return (String) getAttributeInternal(PARTTYP);
    }

    /**
     * Sets <code>value</code> as attribute value for PART_TYP using the alias name PartTyp.
     * @param value value to set the PART_TYP
     */
    public void setPartTyp(String value) {
       
        setAttributeInternal(PARTTYP, value);
    }

    /**
     * Gets the attribute value for the calculated attribute TransLovName.
     * @return the TransLovName
     */
    public String getTransLovName() {

        return (String) getAttributeInternal(TRANSLOVNAME);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute TransLovName.
     * @param value value to set the  TransLovName
     */
    public void setTransLovName(String value) {
        setAttributeInternal(TRANSLOVNAME, value);
    }


    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Number getObjectVersionNumber() {
        return (Number) getAttributeInternal(OBJECTVERSIONNUMBER);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber(Number value) {
        setAttributeInternal(OBJECTVERSIONNUMBER, value);
    }

    /**
     * Gets the attribute value for the calculated attribute TransPageVal.
     * @return the TransPageVal
     */
    public String getTransPageVal() {
        System.out.println("Outside getter ()()()(");
        if(getAttributeInternal(TRANSPAGEVAL)==null && getAttributeInternal(PARTTYP)!=null){
            System.out.println("In geetter &*&*");
            setTransPageVal("Part Master");
        }
        else if(getAttributeInternal(TRANSPAGEVAL)==null && getAttributeInternal(PARTTYP)==null){
            setTransPageVal("Products");
        }
        return (String) getAttributeInternal(TRANSPAGEVAL);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute TransPageVal.
     * @param value value to set the  TransPageVal
     */
    public void setTransPageVal(String value) {
        
        System.out.println("Value for Val &*&*" + value);
        setAttributeInternal(TRANSPAGEVAL, value);
    }

    /**
     * Gets the view accessor <code>RowSet</code> AltPartVO1.
     */
    public RowSet getAltPartVO1() {
        return (RowSet) getAttributeInternal(ALTPARTVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> PartMasterVO1.
     */
    public RowSet getPartMasterVO1() {
        return (RowSet) getAttributeInternal(PARTMASTERVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> ProductsVO1.
     */
    public RowSet getProductsVO1() {
        return (RowSet) getAttributeInternal(PRODUCTSVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> PartCodeSourceVO1.
     */
    public RowSet getPartCodeSourceVO1() {
        return (RowSet) getAttributeInternal(PARTCODESOURCEVO1);
    }
}

