package terms.eng.setup.model.view;

import oracle.jbo.server.ViewObjectImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Fri Apr 21 16:29:28 IST 2017
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class ItemMasterVOImpl extends ViewObjectImpl {
    /**
     * This is the default constructor (do not remove).
     */
    public ItemMasterVOImpl() {
    }

    /**
     * Returns the variable value for Bind_ItemId.
     * @return variable value for Bind_ItemId
     */
    public Long getBind_ItemId() {
        return (Long) ensureVariableManager().getVariableValue("Bind_ItemId");
    }

    /**
     * Sets <code>value</code> for variable Bind_ItemId.
     * @param value value to bind as Bind_ItemId
     */
    public void setBind_ItemId(Long value) {
        ensureVariableManager().setVariableValue("Bind_ItemId", value);
    }

    /**
     * Returns the variable value for bindCloseSt.
     * @return variable value for bindCloseSt
     */
    public String getbindCloseSt() {
        return (String) ensureVariableManager().getVariableValue("bindCloseSt");
    }

    /**
     * Sets <code>value</code> for variable bindCloseSt.
     * @param value value to bind as bindCloseSt
     */
    public void setbindCloseSt(String value) {
        ensureVariableManager().setVariableValue("bindCloseSt", value);
    }

    /**
     * Returns the variable value for ItemSRVBind.
     * @return variable value for ItemSRVBind
     */
    public String getItemSRVBind() {
        return (String) ensureVariableManager().getVariableValue("ItemSRVBind");
    }

    /**
     * Sets <code>value</code> for variable ItemSRVBind.
     * @param value value to bind as ItemSRVBind
     */
    public void setItemSRVBind(String value) {
        ensureVariableManager().setVariableValue("ItemSRVBind", value);
    }

    /**
     * Returns the variable value for bindGroupCode.
     * @return variable value for bindGroupCode
     */
    public String getbindGroupCode() {
        return (String) ensureVariableManager().getVariableValue("bindGroupCode");
    }

    /**
     * Sets <code>value</code> for variable bindGroupCode.
     * @param value value to bind as bindGroupCode
     */
    public void setbindGroupCode(String value) {
        ensureVariableManager().setVariableValue("bindGroupCode", value);
    }

    /**
     * Returns the variable value for bindItemCode.
     * @return variable value for bindItemCode
     */
    public String getbindItemCode() {
        return (String) ensureVariableManager().getVariableValue("bindItemCode");
    }

    /**
     * Sets <code>value</code> for variable bindItemCode.
     * @param value value to bind as bindItemCode
     */
    public void setbindItemCode(String value) {
        ensureVariableManager().setVariableValue("bindItemCode", value);
    }

    /**
     * Returns the variable value for bindItemCd.
     * @return variable value for bindItemCd
     */
    public String getbindItemCd() {
        return (String) ensureVariableManager().getVariableValue("bindItemCd");
    }

    /**
     * Sets <code>value</code> for variable bindItemCd.
     * @param value value to bind as bindItemCd
     */
    public void setbindItemCd(String value) {
        ensureVariableManager().setVariableValue("bindItemCd", value);
    }

    /**
     * Returns the variable value for bindSubGroup.
     * @return variable value for bindSubGroup
     */
    public String getbindSubGroup() {
        return (String) ensureVariableManager().getVariableValue("bindSubGroup");
    }

    /**
     * Sets <code>value</code> for variable bindSubGroup.
     * @param value value to bind as bindSubGroup
     */
    public void setbindSubGroup(String value) {
        ensureVariableManager().setVariableValue("bindSubGroup", value);
    }

    /**
     * Returns the variable value for bindSaleTaxType.
     * @return variable value for bindSaleTaxType
     */
    public String getbindSaleTaxType() {
        return (String) ensureVariableManager().getVariableValue("bindSaleTaxType");
    }

    /**
     * Sets <code>value</code> for variable bindSaleTaxType.
     * @param value value to bind as bindSaleTaxType
     */
    public void setbindSaleTaxType(String value) {
        ensureVariableManager().setVariableValue("bindSaleTaxType", value);
    }
}

