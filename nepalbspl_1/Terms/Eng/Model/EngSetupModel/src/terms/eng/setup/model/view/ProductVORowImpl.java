package terms.eng.setup.model.view;

import java.math.BigDecimal;

import oracle.jbo.Row;
import oracle.jbo.RowIterator;
import oracle.jbo.RowSet;
import oracle.jbo.domain.NClobDomain;
import oracle.jbo.domain.Number;
import oracle.jbo.domain.Timestamp;
import oracle.jbo.server.EntityImpl;
import oracle.jbo.server.ViewRowImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Fri Dec 22 18:13:45 IST 2017
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class ProductVORowImpl extends ViewRowImpl {
    public static final int ENTITY_PRODUCTEO = 0;
    public static final int ENTITY_UNITEO = 1;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        ApplFrom,
        ChapterNo,
        Code,
        Code2,
        Contribution,
        ConvRate,
        CreatedBy,
        CreationDate,
        CustPartNo,
        CustPartRevNo,
        Description,
        DesignTime,
        ExciseCd,
        ExciseChapterNumber,
        IsExisting,
        LastUpdateDate,
        LastUpdatedBy,
        MarketType,
        ModelNo,
        ObjectVersionNumber,
        OpenBal,
        ProdGroup,
        ProductId,
        Specification,
        Status,
        UnitCode,
        UnitOfMeasurement,
        Name,
        Code1,
        ObjectVersionNumber1,
        EditTrans,
        Captive,
        CommodityName,
        Er6Flag,
        ApplicableRpt,
        ProductTyp,
        RefferalCode,
        ProdSubGroup,
        TaxFlag,
        TaxPer,
        ControlCode,
        PurUom,
        SafetyStk,
        SaleUom,
        ScrapPerc,
        StdPack,
        LeadTime,
        LotSizePrd,
        MaxLotSize,
        MinLotSize,
        MrpApplicable,
        MrpControl,
        CoverageProfile,
        ProcType,
        ProductsVO,
        ItemUnitStoreLocVO,
        ItemPackingUomVO,
        ProductGroupSecControlVO1,
        ProductStatusSecControlVO1,
        UomVVO1,
        UnitVO1,
        CaptiveSecControlVO,
        ER6FlagSecControlVO,
        ApplicableRptSecControlVO,
        ProductTypSecControlVO,
        ProdSubTypeSecControlVO1,
        SecControlVO1,
        UomVVO2,
        UomVVO3,
        DataSrcAttVVO1,
        MrpControllerVVO1;
        static AttributesEnum[] vals = null;
        ;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int APPLFROM = AttributesEnum.ApplFrom.index();
    public static final int CHAPTERNO = AttributesEnum.ChapterNo.index();
    public static final int CODE = AttributesEnum.Code.index();
    public static final int CODE2 = AttributesEnum.Code2.index();
    public static final int CONTRIBUTION = AttributesEnum.Contribution.index();
    public static final int CONVRATE = AttributesEnum.ConvRate.index();
    public static final int CREATEDBY = AttributesEnum.CreatedBy.index();
    public static final int CREATIONDATE = AttributesEnum.CreationDate.index();
    public static final int CUSTPARTNO = AttributesEnum.CustPartNo.index();
    public static final int CUSTPARTREVNO = AttributesEnum.CustPartRevNo.index();
    public static final int DESCRIPTION = AttributesEnum.Description.index();
    public static final int DESIGNTIME = AttributesEnum.DesignTime.index();
    public static final int EXCISECD = AttributesEnum.ExciseCd.index();
    public static final int EXCISECHAPTERNUMBER = AttributesEnum.ExciseChapterNumber.index();
    public static final int ISEXISTING = AttributesEnum.IsExisting.index();
    public static final int LASTUPDATEDATE = AttributesEnum.LastUpdateDate.index();
    public static final int LASTUPDATEDBY = AttributesEnum.LastUpdatedBy.index();
    public static final int MARKETTYPE = AttributesEnum.MarketType.index();
    public static final int MODELNO = AttributesEnum.ModelNo.index();
    public static final int OBJECTVERSIONNUMBER = AttributesEnum.ObjectVersionNumber.index();
    public static final int OPENBAL = AttributesEnum.OpenBal.index();
    public static final int PRODGROUP = AttributesEnum.ProdGroup.index();
    public static final int PRODUCTID = AttributesEnum.ProductId.index();
    public static final int SPECIFICATION = AttributesEnum.Specification.index();
    public static final int STATUS = AttributesEnum.Status.index();
    public static final int UNITCODE = AttributesEnum.UnitCode.index();
    public static final int UNITOFMEASUREMENT = AttributesEnum.UnitOfMeasurement.index();
    public static final int NAME = AttributesEnum.Name.index();
    public static final int CODE1 = AttributesEnum.Code1.index();
    public static final int OBJECTVERSIONNUMBER1 = AttributesEnum.ObjectVersionNumber1.index();
    public static final int EDITTRANS = AttributesEnum.EditTrans.index();
    public static final int CAPTIVE = AttributesEnum.Captive.index();
    public static final int COMMODITYNAME = AttributesEnum.CommodityName.index();
    public static final int ER6FLAG = AttributesEnum.Er6Flag.index();
    public static final int APPLICABLERPT = AttributesEnum.ApplicableRpt.index();
    public static final int PRODUCTTYP = AttributesEnum.ProductTyp.index();
    public static final int REFFERALCODE = AttributesEnum.RefferalCode.index();
    public static final int PRODSUBGROUP = AttributesEnum.ProdSubGroup.index();
    public static final int TAXFLAG = AttributesEnum.TaxFlag.index();
    public static final int TAXPER = AttributesEnum.TaxPer.index();
    public static final int CONTROLCODE = AttributesEnum.ControlCode.index();
    public static final int PURUOM = AttributesEnum.PurUom.index();
    public static final int SAFETYSTK = AttributesEnum.SafetyStk.index();
    public static final int SALEUOM = AttributesEnum.SaleUom.index();
    public static final int SCRAPPERC = AttributesEnum.ScrapPerc.index();
    public static final int STDPACK = AttributesEnum.StdPack.index();
    public static final int LEADTIME = AttributesEnum.LeadTime.index();
    public static final int LOTSIZEPRD = AttributesEnum.LotSizePrd.index();
    public static final int MAXLOTSIZE = AttributesEnum.MaxLotSize.index();
    public static final int MINLOTSIZE = AttributesEnum.MinLotSize.index();
    public static final int MRPAPPLICABLE = AttributesEnum.MrpApplicable.index();
    public static final int MRPCONTROL = AttributesEnum.MrpControl.index();
    public static final int COVERAGEPROFILE = AttributesEnum.CoverageProfile.index();
    public static final int PROCTYPE = AttributesEnum.ProcType.index();
    public static final int PRODUCTSVO = AttributesEnum.ProductsVO.index();
    public static final int ITEMUNITSTORELOCVO = AttributesEnum.ItemUnitStoreLocVO.index();
    public static final int ITEMPACKINGUOMVO = AttributesEnum.ItemPackingUomVO.index();
    public static final int PRODUCTGROUPSECCONTROLVO1 = AttributesEnum.ProductGroupSecControlVO1.index();
    public static final int PRODUCTSTATUSSECCONTROLVO1 = AttributesEnum.ProductStatusSecControlVO1.index();
    public static final int UOMVVO1 = AttributesEnum.UomVVO1.index();
    public static final int UNITVO1 = AttributesEnum.UnitVO1.index();
    public static final int CAPTIVESECCONTROLVO = AttributesEnum.CaptiveSecControlVO.index();
    public static final int ER6FLAGSECCONTROLVO = AttributesEnum.ER6FlagSecControlVO.index();
    public static final int APPLICABLERPTSECCONTROLVO = AttributesEnum.ApplicableRptSecControlVO.index();
    public static final int PRODUCTTYPSECCONTROLVO = AttributesEnum.ProductTypSecControlVO.index();
    public static final int PRODSUBTYPESECCONTROLVO1 = AttributesEnum.ProdSubTypeSecControlVO1.index();
    public static final int SECCONTROLVO1 = AttributesEnum.SecControlVO1.index();
    public static final int UOMVVO2 = AttributesEnum.UomVVO2.index();
    public static final int UOMVVO3 = AttributesEnum.UomVVO3.index();
    public static final int DATASRCATTVVO1 = AttributesEnum.DataSrcAttVVO1.index();
    public static final int MRPCONTROLLERVVO1 = AttributesEnum.MrpControllerVVO1.index();

    /**
     * This is the default constructor (do not remove).
     */
    public ProductVORowImpl() {
    }

    /**
     * Gets ProductEO entity object.
     * @return the ProductEO
     */
    public EntityImpl getProductEO() {
        return (EntityImpl) getEntity(ENTITY_PRODUCTEO);
    }

    /**
     * Gets UnitEO entity object.
     * @return the UnitEO
     */
    public EntityImpl getUnitEO() {
        return (EntityImpl) getEntity(ENTITY_UNITEO);
    }

    /**
     * Gets the attribute value for APPL_FROM using the alias name ApplFrom.
     * @return the APPL_FROM
     */
    public Timestamp getApplFrom() {
        return (Timestamp) getAttributeInternal(APPLFROM);
    }

    /**
     * Sets <code>value</code> as attribute value for APPL_FROM using the alias name ApplFrom.
     * @param value value to set the APPL_FROM
     */
    public void setApplFrom(Timestamp value) {
        setAttributeInternal(APPLFROM, value);
    }

    /**
     * Gets the attribute value for CHAPTER_NO using the alias name ChapterNo.
     * @return the CHAPTER_NO
     */
    public String getChapterNo() {
        return (String) getAttributeInternal(CHAPTERNO);
    }

    /**
     * Sets <code>value</code> as attribute value for CHAPTER_NO using the alias name ChapterNo.
     * @param value value to set the CHAPTER_NO
     */
    public void setChapterNo(String value) {
        setAttributeInternal(CHAPTERNO, value);
    }

    /**
     * Gets the attribute value for CODE using the alias name Code.
     * @return the CODE
     */
    public String getCode() {
        return (String) getAttributeInternal(CODE);
    }

    /**
     * Sets <code>value</code> as attribute value for CODE using the alias name Code.
     * @param value value to set the CODE
     */
    public void setCode(String value) {
        setAttributeInternal(CODE, value);
    }

    /**
     * Gets the attribute value for CODE_2 using the alias name Code2.
     * @return the CODE_2
     */
    public String getCode2() {
        return (String) getAttributeInternal(CODE2);
    }

    /**
     * Sets <code>value</code> as attribute value for CODE_2 using the alias name Code2.
     * @param value value to set the CODE_2
     */
    public void setCode2(String value) {
        setAttributeInternal(CODE2, value);
    }

    /**
     * Gets the attribute value for CONTRIBUTION using the alias name Contribution.
     * @return the CONTRIBUTION
     */
    public BigDecimal getContribution() {
        return (BigDecimal) getAttributeInternal(CONTRIBUTION);
    }

    /**
     * Sets <code>value</code> as attribute value for CONTRIBUTION using the alias name Contribution.
     * @param value value to set the CONTRIBUTION
     */
    public void setContribution(BigDecimal value) {
        setAttributeInternal(CONTRIBUTION, value);
    }

    /**
     * Gets the attribute value for CONV_RATE using the alias name ConvRate.
     * @return the CONV_RATE
     */
    public Integer getConvRate() {
        return (Integer) getAttributeInternal(CONVRATE);
    }

    /**
     * Sets <code>value</code> as attribute value for CONV_RATE using the alias name ConvRate.
     * @param value value to set the CONV_RATE
     */
    public void setConvRate(Integer value) {
        setAttributeInternal(CONVRATE, value);
    }

    /**
     * Gets the attribute value for CREATED_BY using the alias name CreatedBy.
     * @return the CREATED_BY
     */
    public String getCreatedBy() {
        return (String) getAttributeInternal(CREATEDBY);
    }

    /**
     * Sets <code>value</code> as attribute value for CREATED_BY using the alias name CreatedBy.
     * @param value value to set the CREATED_BY
     */
    public void setCreatedBy(String value) {
        setAttributeInternal(CREATEDBY, value);
    }

    /**
     * Gets the attribute value for CREATION_DATE using the alias name CreationDate.
     * @return the CREATION_DATE
     */
    public Timestamp getCreationDate() {
        return (Timestamp) getAttributeInternal(CREATIONDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for CREATION_DATE using the alias name CreationDate.
     * @param value value to set the CREATION_DATE
     */
    public void setCreationDate(Timestamp value) {
        setAttributeInternal(CREATIONDATE, value);
    }

    /**
     * Gets the attribute value for CUST_PART_NO using the alias name CustPartNo.
     * @return the CUST_PART_NO
     */
    public String getCustPartNo() {
        return (String) getAttributeInternal(CUSTPARTNO);
    }

    /**
     * Sets <code>value</code> as attribute value for CUST_PART_NO using the alias name CustPartNo.
     * @param value value to set the CUST_PART_NO
     */
    public void setCustPartNo(String value) {
        setAttributeInternal(CUSTPARTNO, value);
    }

    /**
     * Gets the attribute value for CUST_PART_REV_NO using the alias name CustPartRevNo.
     * @return the CUST_PART_REV_NO
     */
    public String getCustPartRevNo() {
        return (String) getAttributeInternal(CUSTPARTREVNO);
    }

    /**
     * Sets <code>value</code> as attribute value for CUST_PART_REV_NO using the alias name CustPartRevNo.
     * @param value value to set the CUST_PART_REV_NO
     */
    public void setCustPartRevNo(String value) {
        setAttributeInternal(CUSTPARTREVNO, value);
    }

    /**
     * Gets the attribute value for DESCRIPTION using the alias name Description.
     * @return the DESCRIPTION
     */
    public String getDescription() {
        return (String) getAttributeInternal(DESCRIPTION);
    }

    /**
     * Sets <code>value</code> as attribute value for DESCRIPTION using the alias name Description.
     * @param value value to set the DESCRIPTION
     */
    public void setDescription(String value) {
        setAttributeInternal(DESCRIPTION, value);
    }

    /**
     * Gets the attribute value for DESIGN_TIME using the alias name DesignTime.
     * @return the DESIGN_TIME
     */
    public Integer getDesignTime() {
        return (Integer) getAttributeInternal(DESIGNTIME);
    }

    /**
     * Sets <code>value</code> as attribute value for DESIGN_TIME using the alias name DesignTime.
     * @param value value to set the DESIGN_TIME
     */
    public void setDesignTime(Integer value) {
        setAttributeInternal(DESIGNTIME, value);
    }

    /**
     * Gets the attribute value for EXCISE_CD using the alias name ExciseCd.
     * @return the EXCISE_CD
     */
    public String getExciseCd() {
        return (String) getAttributeInternal(EXCISECD);
    }

    /**
     * Sets <code>value</code> as attribute value for EXCISE_CD using the alias name ExciseCd.
     * @param value value to set the EXCISE_CD
     */
    public void setExciseCd(String value) {
        setAttributeInternal(EXCISECD, value);
    }

    /**
     * Gets the attribute value for EXCISE_CHAPTER_NUMBER using the alias name ExciseChapterNumber.
     * @return the EXCISE_CHAPTER_NUMBER
     */
    public String getExciseChapterNumber() {
        return (String) getAttributeInternal(EXCISECHAPTERNUMBER);
    }

    /**
     * Sets <code>value</code> as attribute value for EXCISE_CHAPTER_NUMBER using the alias name ExciseChapterNumber.
     * @param value value to set the EXCISE_CHAPTER_NUMBER
     */
    public void setExciseChapterNumber(String value) {
        setAttributeInternal(EXCISECHAPTERNUMBER, value);
    }

    /**
     * Gets the attribute value for IS_EXISTING using the alias name IsExisting.
     * @return the IS_EXISTING
     */
    public String getIsExisting() {
        return (String) getAttributeInternal(ISEXISTING);
    }

    /**
     * Sets <code>value</code> as attribute value for IS_EXISTING using the alias name IsExisting.
     * @param value value to set the IS_EXISTING
     */
    public void setIsExisting(String value) {
        setAttributeInternal(ISEXISTING, value);
    }

    /**
     * Gets the attribute value for LAST_UPDATE_DATE using the alias name LastUpdateDate.
     * @return the LAST_UPDATE_DATE
     */
    public oracle.jbo.domain.Timestamp getLastUpdateDate() {
        return (oracle.jbo.domain.Timestamp) getAttributeInternal(LASTUPDATEDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for LAST_UPDATE_DATE using the alias name LastUpdateDate.
     * @param value value to set the LAST_UPDATE_DATE
     */
    public void setLastUpdateDate(oracle.jbo.domain.Timestamp value) {
        setAttributeInternal(LASTUPDATEDATE, value);
    }

    /**
     * Gets the attribute value for LAST_UPDATED_BY using the alias name LastUpdatedBy.
     * @return the LAST_UPDATED_BY
     */
    public String getLastUpdatedBy() {
        return (String) getAttributeInternal(LASTUPDATEDBY);
    }

    /**
     * Sets <code>value</code> as attribute value for LAST_UPDATED_BY using the alias name LastUpdatedBy.
     * @param value value to set the LAST_UPDATED_BY
     */
    public void setLastUpdatedBy(String value) {
        setAttributeInternal(LASTUPDATEDBY, value);
    }

    /**
     * Gets the attribute value for MARKET_TYPE using the alias name MarketType.
     * @return the MARKET_TYPE
     */
    public String getMarketType() {
        return (String) getAttributeInternal(MARKETTYPE);
    }

    /**
     * Sets <code>value</code> as attribute value for MARKET_TYPE using the alias name MarketType.
     * @param value value to set the MARKET_TYPE
     */
    public void setMarketType(String value) {
        setAttributeInternal(MARKETTYPE, value);
    }

    /**
     * Gets the attribute value for MODEL_NO using the alias name ModelNo.
     * @return the MODEL_NO
     */
    public String getModelNo() {
        return (String) getAttributeInternal(MODELNO);
    }

    /**
     * Sets <code>value</code> as attribute value for MODEL_NO using the alias name ModelNo.
     * @param value value to set the MODEL_NO
     */
    public void setModelNo(String value) {
        setAttributeInternal(MODELNO, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER, value);
    }

    /**
     * Gets the attribute value for OPEN_BAL using the alias name OpenBal.
     * @return the OPEN_BAL
     */
    public BigDecimal getOpenBal() {
        return (BigDecimal) getAttributeInternal(OPENBAL);
    }

    /**
     * Sets <code>value</code> as attribute value for OPEN_BAL using the alias name OpenBal.
     * @param value value to set the OPEN_BAL
     */
    public void setOpenBal(BigDecimal value) {
        setAttributeInternal(OPENBAL, value);
    }

    /**
     * Gets the attribute value for PROD_GROUP using the alias name ProdGroup.
     * @return the PROD_GROUP
     */
    public String getProdGroup() {
        return (String) getAttributeInternal(PRODGROUP);
    }

    /**
     * Sets <code>value</code> as attribute value for PROD_GROUP using the alias name ProdGroup.
     * @param value value to set the PROD_GROUP
     */
    public void setProdGroup(String value) {
        setAttributeInternal(PRODGROUP, value);
    }

    /**
     * Gets the attribute value for PRODUCT_ID using the alias name ProductId.
     * @return the PRODUCT_ID
     */
    public Long getProductId() {
        return (Long) getAttributeInternal(PRODUCTID);
    }

    /**
     * Sets <code>value</code> as attribute value for PRODUCT_ID using the alias name ProductId.
     * @param value value to set the PRODUCT_ID
     */
    public void setProductId(Long value) {
        setAttributeInternal(PRODUCTID, value);
    }

    /**
     * Gets the attribute value for SPECIFICATION using the alias name Specification.
     * @return the SPECIFICATION
     */
    public String getSpecification() {
        return (String) getAttributeInternal(SPECIFICATION);
    }

    /**
     * Sets <code>value</code> as attribute value for SPECIFICATION using the alias name Specification.
     * @param value value to set the SPECIFICATION
     */
    public void setSpecification(String value) {
        setAttributeInternal(SPECIFICATION, value);
    }

    /**
     * Gets the attribute value for STATUS using the alias name Status.
     * @return the STATUS
     */
    public String getStatus() {
        return (String) getAttributeInternal(STATUS);
    }

    /**
     * Sets <code>value</code> as attribute value for STATUS using the alias name Status.
     * @param value value to set the STATUS
     */
    public void setStatus(String value) {
        setAttributeInternal(STATUS, value);
    }

    /**
     * Gets the attribute value for UNIT_CODE using the alias name UnitCode.
     * @return the UNIT_CODE
     */
    public String getUnitCode() {
        return (String) getAttributeInternal(UNITCODE);
    }

    /**
     * Sets <code>value</code> as attribute value for UNIT_CODE using the alias name UnitCode.
     * @param value value to set the UNIT_CODE
     */
    public void setUnitCode(String value) {
        setAttributeInternal(UNITCODE, value);
    }

    /**
     * Gets the attribute value for UNIT_OF_MEASUREMENT using the alias name UnitOfMeasurement.
     * @return the UNIT_OF_MEASUREMENT
     */
    public String getUnitOfMeasurement() {
        return (String) getAttributeInternal(UNITOFMEASUREMENT);
    }

    /**
     * Sets <code>value</code> as attribute value for UNIT_OF_MEASUREMENT using the alias name UnitOfMeasurement.
     * @param value value to set the UNIT_OF_MEASUREMENT
     */
    public void setUnitOfMeasurement(String value) {
        setAttributeInternal(UNITOFMEASUREMENT, value);
    }

    /**
     * Gets the attribute value for NAME using the alias name Name.
     * @return the NAME
     */
    public String getName() {
        return (String) getAttributeInternal(NAME);
    }

    /**
     * Sets <code>value</code> as attribute value for NAME using the alias name Name.
     * @param value value to set the NAME
     */
    public void setName(String value) {
        setAttributeInternal(NAME, value);
    }

    /**
     * Gets the attribute value for CODE using the alias name Code1.
     * @return the CODE
     */
    public String getCode1() {
        return (String) getAttributeInternal(CODE1);
    }

    /**
     * Sets <code>value</code> as attribute value for CODE using the alias name Code1.
     * @param value value to set the CODE
     */
    public void setCode1(String value) {
        setAttributeInternal(CODE1, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber1.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber1() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER1);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber1.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber1(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER1, value);
    }

    /**
     * Gets the attribute value for the calculated attribute EditTrans.
     * @return the EditTrans
     */
    public Integer getEditTrans()
    {
        byte entityState = this.getEntity(0).getEntityState();
        return new Integer(entityState);
//        return (Integer) getAttributeInternal(EDITTRANS);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute EditTrans.
     * @param value value to set the  EditTrans
     */
    public void setEditTrans(Integer value) {
        setAttributeInternal(EDITTRANS, value);
    }

    /**
     * Gets the attribute value for CAPTIVE using the alias name Captive.
     * @return the CAPTIVE
     */
    public String getCaptive() {
        return (String) getAttributeInternal(CAPTIVE);
    }

    /**
     * Sets <code>value</code> as attribute value for CAPTIVE using the alias name Captive.
     * @param value value to set the CAPTIVE
     */
    public void setCaptive(String value) {
        setAttributeInternal(CAPTIVE, value);
    }

    /**
     * Gets the attribute value for COMMODITY_NAME using the alias name CommodityName.
     * @return the COMMODITY_NAME
     */
    public String getCommodityName() {
        return (String) getAttributeInternal(COMMODITYNAME);
    }

    /**
     * Sets <code>value</code> as attribute value for COMMODITY_NAME using the alias name CommodityName.
     * @param value value to set the COMMODITY_NAME
     */
    public void setCommodityName(String value) {
        setAttributeInternal(COMMODITYNAME, value);
    }

    /**
     * Gets the attribute value for ER6_FLAG using the alias name Er6Flag.
     * @return the ER6_FLAG
     */
    public String getEr6Flag() {
        return (String) getAttributeInternal(ER6FLAG);
    }

    /**
     * Sets <code>value</code> as attribute value for ER6_FLAG using the alias name Er6Flag.
     * @param value value to set the ER6_FLAG
     */
    public void setEr6Flag(String value) {
        setAttributeInternal(ER6FLAG, value);
    }

    /**
     * Gets the attribute value for APPLICABLE_RPT using the alias name ApplicableRpt.
     * @return the APPLICABLE_RPT
     */
    public String getApplicableRpt() {
        return (String) getAttributeInternal(APPLICABLERPT);
    }

    /**
     * Sets <code>value</code> as attribute value for APPLICABLE_RPT using the alias name ApplicableRpt.
     * @param value value to set the APPLICABLE_RPT
     */
    public void setApplicableRpt(String value) {
        setAttributeInternal(APPLICABLERPT, value);
    }

    /**
     * Gets the attribute value for PRODUCT_TYP using the alias name ProductTyp.
     * @return the PRODUCT_TYP
     */
    public String getProductTyp() {
        return (String) getAttributeInternal(PRODUCTTYP);
    }

    /**
     * Sets <code>value</code> as attribute value for PRODUCT_TYP using the alias name ProductTyp.
     * @param value value to set the PRODUCT_TYP
     */
    public void setProductTyp(String value) {
        setAttributeInternal(PRODUCTTYP, value);
    }

    /**
     * Gets the attribute value for REFFERAL_CODE using the alias name RefferalCode.
     * @return the REFFERAL_CODE
     */
    public String getRefferalCode() {
        return (String) getAttributeInternal(REFFERALCODE);
    }

    /**
     * Sets <code>value</code> as attribute value for REFFERAL_CODE using the alias name RefferalCode.
     * @param value value to set the REFFERAL_CODE
     */
    public void setRefferalCode(String value) {
        setAttributeInternal(REFFERALCODE, value);
    }

    /**
     * Gets the attribute value for PROD_SUB_GROUP using the alias name ProdSubGroup.
     * @return the PROD_SUB_GROUP
     */
    public String getProdSubGroup() {
        return (String) getAttributeInternal(PRODSUBGROUP);
    }

    /**
     * Sets <code>value</code> as attribute value for PROD_SUB_GROUP using the alias name ProdSubGroup.
     * @param value value to set the PROD_SUB_GROUP
     */
    public void setProdSubGroup(String value) {
        setAttributeInternal(PRODSUBGROUP, value);
    }

    /**
     * Gets the attribute value for TAX_FLAG using the alias name TaxFlag.
     * @return the TAX_FLAG
     */
    public String getTaxFlag() {
        return (String) getAttributeInternal(TAXFLAG);
    }

    /**
     * Sets <code>value</code> as attribute value for TAX_FLAG using the alias name TaxFlag.
     * @param value value to set the TAX_FLAG
     */
    public void setTaxFlag(String value) {
        setAttributeInternal(TAXFLAG, value);
    }

    /**
     * Gets the attribute value for TAX_PER using the alias name TaxPer.
     * @return the TAX_PER
     */
    public Number getTaxPer() {
        return (Number) getAttributeInternal(TAXPER);
    }

    /**
     * Sets <code>value</code> as attribute value for TAX_PER using the alias name TaxPer.
     * @param value value to set the TAX_PER
     */
    public void setTaxPer(Number value) {
        setAttributeInternal(TAXPER, value);
    }

    /**
     * Gets the attribute value for the calculated attribute ControlCode.
     * @return the ControlCode
     */
    public String getControlCode() {
        
        //return (String) getAttributeInternal(CONTROLCODE);
        return getProdGroup()!=null ? getProdGroup() :"N";
    }

    /**
     * Gets the attribute value for PUR_UOM using the alias name PurUom.
     * @return the PUR_UOM
     */
    public String getPurUom() {
        return (String) getAttributeInternal(PURUOM);
    }

    /**
     * Sets <code>value</code> as attribute value for PUR_UOM using the alias name PurUom.
     * @param value value to set the PUR_UOM
     */
    public void setPurUom(String value) {
        setAttributeInternal(PURUOM, value);
    }

    /**
     * Gets the attribute value for SAFETY_STK using the alias name SafetyStk.
     * @return the SAFETY_STK
     */
    public Number getSafetyStk() {
        return (Number) getAttributeInternal(SAFETYSTK);
    }

    /**
     * Sets <code>value</code> as attribute value for SAFETY_STK using the alias name SafetyStk.
     * @param value value to set the SAFETY_STK
     */
    public void setSafetyStk(Number value) {
        setAttributeInternal(SAFETYSTK, value);
    }

    /**
     * Gets the attribute value for SALE_UOM using the alias name SaleUom.
     * @return the SALE_UOM
     */
    public String getSaleUom() {
        return (String) getAttributeInternal(SALEUOM);
    }

    /**
     * Sets <code>value</code> as attribute value for SALE_UOM using the alias name SaleUom.
     * @param value value to set the SALE_UOM
     */
    public void setSaleUom(String value) {
        setAttributeInternal(SALEUOM, value);
    }

    /**
     * Gets the attribute value for SCRAP_PERC using the alias name ScrapPerc.
     * @return the SCRAP_PERC
     */
    public Number getScrapPerc() {
        return (Number) getAttributeInternal(SCRAPPERC);
    }

    /**
     * Sets <code>value</code> as attribute value for SCRAP_PERC using the alias name ScrapPerc.
     * @param value value to set the SCRAP_PERC
     */
    public void setScrapPerc(Number value) {
        setAttributeInternal(SCRAPPERC, value);
    }

    /**
     * Gets the attribute value for STD_PACK using the alias name StdPack.
     * @return the STD_PACK
     */
    public Integer getStdPack() {
        return (Integer) getAttributeInternal(STDPACK);
    }

    /**
     * Sets <code>value</code> as attribute value for STD_PACK using the alias name StdPack.
     * @param value value to set the STD_PACK
     */
    public void setStdPack(Integer value) {
        setAttributeInternal(STDPACK, value);
    }

    /**
     * Gets the attribute value for LEAD_TIME using the alias name LeadTime.
     * @return the LEAD_TIME
     */
    public Integer getLeadTime() {
        return (Integer) getAttributeInternal(LEADTIME);
    }

    /**
     * Sets <code>value</code> as attribute value for LEAD_TIME using the alias name LeadTime.
     * @param value value to set the LEAD_TIME
     */
    public void setLeadTime(Integer value) {
        setAttributeInternal(LEADTIME, value);
    }

    /**
     * Gets the attribute value for LOT_SIZE_PRD using the alias name LotSizePrd.
     * @return the LOT_SIZE_PRD
     */
    public Integer getLotSizePrd() {
        return (Integer) getAttributeInternal(LOTSIZEPRD);
    }

    /**
     * Sets <code>value</code> as attribute value for LOT_SIZE_PRD using the alias name LotSizePrd.
     * @param value value to set the LOT_SIZE_PRD
     */
    public void setLotSizePrd(Integer value) {
        setAttributeInternal(LOTSIZEPRD, value);
    }

    /**
     * Gets the attribute value for MAX_LOT_SIZE using the alias name MaxLotSize.
     * @return the MAX_LOT_SIZE
     */
    public Integer getMaxLotSize() {
        return (Integer) getAttributeInternal(MAXLOTSIZE);
    }

    /**
     * Sets <code>value</code> as attribute value for MAX_LOT_SIZE using the alias name MaxLotSize.
     * @param value value to set the MAX_LOT_SIZE
     */
    public void setMaxLotSize(Integer value) {
        setAttributeInternal(MAXLOTSIZE, value);
    }

    /**
     * Gets the attribute value for MIN_LOT_SIZE using the alias name MinLotSize.
     * @return the MIN_LOT_SIZE
     */
    public Integer getMinLotSize() {
        return (Integer) getAttributeInternal(MINLOTSIZE);
    }

    /**
     * Sets <code>value</code> as attribute value for MIN_LOT_SIZE using the alias name MinLotSize.
     * @param value value to set the MIN_LOT_SIZE
     */
    public void setMinLotSize(Integer value) {
        setAttributeInternal(MINLOTSIZE, value);
    }

    /**
     * Gets the attribute value for MRP_APPLICABLE using the alias name MrpApplicable.
     * @return the MRP_APPLICABLE
     */
    public String getMrpApplicable() {
        return (String) getAttributeInternal(MRPAPPLICABLE);
    }

    /**
     * Sets <code>value</code> as attribute value for MRP_APPLICABLE using the alias name MrpApplicable.
     * @param value value to set the MRP_APPLICABLE
     */
    public void setMrpApplicable(String value) {
        setAttributeInternal(MRPAPPLICABLE, value);
    }

    /**
     * Gets the attribute value for MRP_CONTROL using the alias name MrpControl.
     * @return the MRP_CONTROL
     */
    public String getMrpControl() {
        return (String) getAttributeInternal(MRPCONTROL);
    }

    /**
     * Sets <code>value</code> as attribute value for MRP_CONTROL using the alias name MrpControl.
     * @param value value to set the MRP_CONTROL
     */
    public void setMrpControl(String value) {
        setAttributeInternal(MRPCONTROL, value);
    }

    /**
     * Gets the attribute value for COVERAGE_PROFILE using the alias name CoverageProfile.
     * @return the COVERAGE_PROFILE
     */
    public Integer getCoverageProfile() {
        return (Integer) getAttributeInternal(COVERAGEPROFILE);
    }

    /**
     * Sets <code>value</code> as attribute value for COVERAGE_PROFILE using the alias name CoverageProfile.
     * @param value value to set the COVERAGE_PROFILE
     */
    public void setCoverageProfile(Integer value) {
        setAttributeInternal(COVERAGEPROFILE, value);
    }

    /**
     * Gets the attribute value for PROC_TYPE using the alias name ProcType.
     * @return the PROC_TYPE
     */
    public Integer getProcType() {
        return (Integer) getAttributeInternal(PROCTYPE);
    }

    /**
     * Sets <code>value</code> as attribute value for PROC_TYPE using the alias name ProcType.
     * @param value value to set the PROC_TYPE
     */
    public void setProcType(Integer value) {
        setAttributeInternal(PROCTYPE, value);
    }

    /**
     * Gets the associated <code>Row</code> using master-detail link ProductsVO.
     */
    public Row getProductsVO() {
        return (Row) getAttributeInternal(PRODUCTSVO);
    }

    /**
     * Sets the master-detail link ProductsVO between this object and <code>value</code>.
     */
    public void setProductsVO(Row value) {
        setAttributeInternal(PRODUCTSVO, value);
    }

    /**
     * Gets the associated <code>RowIterator</code> using master-detail link ItemUnitStoreLocVO.
     */
    public RowIterator getItemUnitStoreLocVO() {
        return (RowIterator) getAttributeInternal(ITEMUNITSTORELOCVO);
    }

    /**
     * Gets the associated <code>RowIterator</code> using master-detail link ItemPackingUomVO.
     */
    public RowIterator getItemPackingUomVO() {
        return (RowIterator) getAttributeInternal(ITEMPACKINGUOMVO);
    }

    /**
     * Gets the view accessor <code>RowSet</code> ProductGroupSecControlVO1.
     */
    public RowSet getProductGroupSecControlVO1() {
        return (RowSet) getAttributeInternal(PRODUCTGROUPSECCONTROLVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> ProductStatusSecControlVO1.
     */
    public RowSet getProductStatusSecControlVO1() {
        return (RowSet) getAttributeInternal(PRODUCTSTATUSSECCONTROLVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> UomVVO1.
     */
    public RowSet getUomVVO1() {
        return (RowSet) getAttributeInternal(UOMVVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> UnitVO1.
     */
    public RowSet getUnitVO1() {
        return (RowSet) getAttributeInternal(UNITVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> CaptiveSecControlVO.
     */
    public RowSet getCaptiveSecControlVO() {
        return (RowSet) getAttributeInternal(CAPTIVESECCONTROLVO);
    }

    /**
     * Gets the view accessor <code>RowSet</code> ER6FlagSecControlVO.
     */
    public RowSet getER6FlagSecControlVO() {
        return (RowSet) getAttributeInternal(ER6FLAGSECCONTROLVO);
    }

    /**
     * Gets the view accessor <code>RowSet</code> ApplicableRptSecControlVO.
     */
    public RowSet getApplicableRptSecControlVO() {
        return (RowSet) getAttributeInternal(APPLICABLERPTSECCONTROLVO);
    }

    /**
     * Gets the view accessor <code>RowSet</code> ProductTypSecControlVO.
     */
    public RowSet getProductTypSecControlVO() {
        return (RowSet) getAttributeInternal(PRODUCTTYPSECCONTROLVO);
    }

    /**
     * Gets the view accessor <code>RowSet</code> ProdSubTypeSecControlVO1.
     */
    public RowSet getProdSubTypeSecControlVO1() {
        return (RowSet) getAttributeInternal(PRODSUBTYPESECCONTROLVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> SecControlVO1.
     */
    public RowSet getSecControlVO1() {
        return (RowSet) getAttributeInternal(SECCONTROLVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> UomVVO2.
     */
    public RowSet getUomVVO2() {
        return (RowSet) getAttributeInternal(UOMVVO2);
    }

    /**
     * Gets the view accessor <code>RowSet</code> UomVVO3.
     */
    public RowSet getUomVVO3() {
        return (RowSet) getAttributeInternal(UOMVVO3);
    }

    /**
     * Gets the view accessor <code>RowSet</code> DataSrcAttVVO1.
     */
    public RowSet getDataSrcAttVVO1() {
        return (RowSet) getAttributeInternal(DATASRCATTVVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> MrpControllerVVO1.
     */
    public RowSet getMrpControllerVVO1() {
        return (RowSet) getAttributeInternal(MRPCONTROLLERVVO1);
    }
}

