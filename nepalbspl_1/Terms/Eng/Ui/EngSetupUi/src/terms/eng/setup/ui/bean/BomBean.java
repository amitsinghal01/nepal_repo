package terms.eng.setup.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;

import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.adf.view.rich.event.PopupFetchEvent;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.domain.Number;

public class BomBean {
   
    private RichInputComboboxListOfValues getProductCode;
    private RichPopup bompopBinding;
    private RichInputComboboxListOfValues inputTextPartCdBind;
    private RichInputComboboxListOfValues bindProdCode;
    private RichTable bomPartEntryTableBinding;
    private RichTable bomConsumableEntryTableBinding;
    private RichTable costingDetailTableBinding;
    private RichInputComboboxListOfValues selectProdCopyBinding;
    private RichInputText prodCopyDescBinding;
    private RichInputText revNoBinding;
    private RichInputText prodCopRevBinding;
    private RichOutputText bindingOutputText;
    private RichButton headerEditBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton partEntrydetailcreateBinding;
    private RichButton consumableEntrydetailcreateBinding;
    private RichInputText partEntrySNoBinding;
    private RichInputText consumableSNoBinding;
    private RichInputText itemDescBinding;
    private RichButton copyButtonBinding;
    private RichInputText alternateBomDetailPartCdBinding;
    private RichInputText alternateBomDetaildescBinding;
    private RichButton saveBinding;
    private RichButton saveAndCloseBinding;
    private RichTable bindAltBomTable;
    private RichShowDetailItem partsEntryButtonBinding;
    private RichShowDetailItem consumableButtonBinding;
    private RichShowDetailItem costingButtonBinding;
    private RichShowDetailItem bomMakerButtonBinding;

    public BomBean() {
    }

    public void getLeafNumberAL(ActionEvent actionEvent)
    {
        ADFUtils.setLastUpdatedByNew("BomHeaderVO1Iterator","LastUpdatedBy");
  //  ADFUtils.findOperation("getLeafNumberSequence").execute();
        ADFUtils.findOperation("getProdCodBbomMaker").execute();
        Integer TransValue=(Integer)bindingOutputText.getValue();
        System.out.println("TRANS VALUE"+TransValue);
//        String param=resolvEl("#{pageFlowScope.mode=='E'}"); 
//        System.out.println("Save Mode is ====> "+param);
        if(TransValue==0)       
        {
           FacesMessage Message = new FacesMessage("Record Save Successfully");   
           Message.setSeverity(FacesMessage.SEVERITY_INFO);   
           FacesContext fc = FacesContext.getCurrentInstance();   
           fc.addMessage(null, Message);      
            
        }
        else 
        {
            FacesMessage Message = new FacesMessage("Record Update Successfully");   
            Message.setSeverity(FacesMessage.SEVERITY_INFO);   
            FacesContext fc = FacesContext.getCurrentInstance();   
            fc.addMessage(null, Message);      
        }
        
        // Add event code here...
    }

    public void copyProductDataAL(ActionEvent actionEvent) {
       ADFUtils.findOperation("PopulateBom").execute();
                ADFUtils.findOperation("populateBom1").execute();       
                    
            }

   

    
    
   

    /*  public void altBomAL(ActionEvent actionEvent) 
    {
    
        // Add event code here...
    } */

    /*  public void populateAltBomAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("populateAltBom").execute();
    } */

    public void saveAndCloseAL(ActionEvent actionEvent) 
        {
        ADFUtils.setLastUpdatedByNew("BomHeaderVO1Iterator","LastUpdatedBy");
        //ADFUtils.findOperation("getLeafNumberSequence").execute();
        ADFUtils.findOperation("getProdCodBbomMaker").execute();
        
        Integer i=0;
        i=(Integer)bindingOutputText.getValue();
        System.out.println("EDIT TRANS VALUE"+i);
        if(i.equals(0))
        {
           FacesMessage Message = new FacesMessage("Record Save Successfully");   
           Message.setSeverity(FacesMessage.SEVERITY_INFO);   
           FacesContext fc = FacesContext.getCurrentInstance();   
           fc.addMessage(null, Message);      
            
        }
        else 
        {
            FacesMessage Message = new FacesMessage("Record Updated Successfully");   
            Message.setSeverity(FacesMessage.SEVERITY_INFO);   
            FacesContext fc = FacesContext.getCurrentInstance();   
            fc.addMessage(null, Message);      
        }
            
        
        
    }

    public void bindCancelAL(ActionEvent actionEvent) {
        Object Val=getBindProdCode().getValue();
              OperationBinding op = ADFUtils.findOperation("goForProductCode");
              op.getParamsMap().put("bindProdCode",Val);
              op.execute();
        
    
    }

    public void setGetProductCode(RichInputComboboxListOfValues getProductCode) {
        this.getProductCode = getProductCode;
    }

    public RichInputComboboxListOfValues getGetProductCode() {
        return getProductCode;
    }

    public void setBompopBinding(RichPopup bompopBinding) {
        this.bompopBinding = bompopBinding;
    }

    public RichPopup getBompopBinding() {
        return bompopBinding;
    }

    public void bomMakerDialogListener(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome()==DialogEvent.Outcome.ok)
        {
            bompopBinding.hide();
        }
    }

    public void onPageLoadAction() {
        ADFUtils.findOperation("CreateInsertBomHeader").execute();
        ADFUtils.findOperation("CreateInsertBomMaker").execute();
    }

    /* public void fetchAltBomFL(PopupFetchEvent popupFetchEvent) {
        ADFUtils.findOperation("populateAltBom").execute();
    } */
    
    /*  public BindingContainer getBindingsCont() {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
    }
    
    public OperationBinding exceuteOperation(String operation ){
        OperationBinding od=getBindingsCont().getOperationBinding(operation);
        
        return od;
    } */
      /*  public void fetchAltBomFL(PopupFetchEvent popupFetchEvent) {
        OperationBinding od=exceuteOperation("populateAltBom");
         od.getParamsMap().put("PartCd",inputTextPartCdBind.getValue());
        od.execute();

    }   */


    public void setBindProdCode(RichInputComboboxListOfValues bindProdCode) {
        this.bindProdCode = bindProdCode;
    }

    public RichInputComboboxListOfValues getBindProdCode() {
        return bindProdCode;
    }


    public void setBomPartEntryTableBinding(RichTable bomPartEntryTableBinding) {
        this.bomPartEntryTableBinding = bomPartEntryTableBinding;
    }

    public RichTable getBomPartEntryTableBinding() {
        return bomPartEntryTableBinding;
    }

    public void bomPartsEntrydeletePopupDialogDL(DialogEvent dialogEvent) 
    {
        if(dialogEvent.getOutcome().name().equals("ok"))
            {
            ADFUtils.findOperation("Delete").execute();
            System.out.println("Record Delete Successfully");
            
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");   
                Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                FacesContext fc = FacesContext.getCurrentInstance();   
                fc.addMessage(null, Message);   
            
            }

        AdfFacesContext.getCurrentInstance().addPartialTarget(bomPartEntryTableBinding);

    }

    public void bomConsumableEntrydeletePopupDialogDL(DialogEvent dialogEvent) 
    {
        if(dialogEvent.getOutcome().name().equals("ok"))
            {
            ADFUtils.findOperation("Delete1").execute();
            //ADFUtils.findOperation("Commit").execute();
            System.out.println("Record Delete Successfully");
            
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");   
                Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                FacesContext fc = FacesContext.getCurrentInstance();   
                fc.addMessage(null, Message);  
            
            }

        AdfFacesContext.getCurrentInstance().addPartialTarget(bomConsumableEntryTableBinding);

    }

    public void setBomConsumableEntryTableBinding(RichTable bomConsumableEntryTableBinding) {
        this.bomConsumableEntryTableBinding = bomConsumableEntryTableBinding;
    }

    public RichTable getBomConsumableEntryTableBinding() {
        return bomConsumableEntryTableBinding;
    }

    public void costingDetailDeletePopupDialogDL(DialogEvent dialogEvent) 
    {
        if(dialogEvent.getOutcome().name().equals("ok"))
            {
            ADFUtils.findOperation("Delete2").execute();
            //ADFUtils.findOperation("Commit").execute();
            System.out.println("Record Delete Successfully");
            
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");   
                Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                FacesContext fc = FacesContext.getCurrentInstance();   
                fc.addMessage(null, Message);  
                
              
            }

        AdfFacesContext.getCurrentInstance().addPartialTarget(costingDetailTableBinding);

    }

    public void setCostingDetailTableBinding(RichTable costingDetailTableBinding) {
        this.costingDetailTableBinding = costingDetailTableBinding;
    }

    public RichTable getCostingDetailTableBinding() {
        return costingDetailTableBinding;
    }
    
    
    public String resolvEl(String data)
    {
                      FacesContext fc = FacesContext.getCurrentInstance();
                      Application app = fc.getApplication();
                      ExpressionFactory elFactory = app.getExpressionFactory();
                      ELContext elContext = fc.getELContext();
                      ValueExpression valueExp = elFactory.createValueExpression(elContext, data, Object.class);
                      String Message=valueExp.getValue(elContext).toString();
                      return Message;
    }
    
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
            try {
                Method method1 =
                    component.getClass().getMethod("setDisabled", boolean.class);
                if (method1 != null) {
                    method1.invoke(component, valueComponent);
                }
            } catch (NoSuchMethodException e) {
                try {
                    Method method =
                        component.getClass().getMethod("setReadOnly", boolean.class);
                    if (method != null) {
                        method.invoke(component, valueComponent);
                    }
                } catch (Exception e1) {
                    // e.printStackTrace();//silently eat this exception.
                }


            } catch (Exception e) {
                // e.printStackTrace();//silently eat this exception.
            }
            List<UIComponent> childComponents = component.getChildren();
            for (UIComponent comp : childComponents) {
                makeComponentHierarchyReadOnly(comp, valueComponent);
            }
        }
    
    private void cevmodecheck(){
           if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
               cevModeDisableComponent("V");
           }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
               cevModeDisableComponent("C");
           }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
               cevModeDisableComponent("E");
           }
       }

    public void setSelectProdCopyBinding(RichInputComboboxListOfValues selectProdCopyBinding) {
        this.selectProdCopyBinding = selectProdCopyBinding;
    }

    public RichInputComboboxListOfValues getSelectProdCopyBinding() {
        return selectProdCopyBinding;
    }

    public void setProdCopyDescBinding(RichInputText prodCopyDescBinding) {
        this.prodCopyDescBinding = prodCopyDescBinding;
    }

    public RichInputText getProdCopyDescBinding() {
        return prodCopyDescBinding;
    }

    public void setRevNoBinding(RichInputText revNoBinding) {
        this.revNoBinding = revNoBinding;
    }

    public RichInputText getRevNoBinding() {
        return revNoBinding;
    }

    public void setProdCopRevBinding(RichInputText prodCopRevBinding) {
        this.prodCopRevBinding = prodCopRevBinding;
    }

    public RichInputText getProdCopRevBinding() {
        return prodCopRevBinding;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void EditButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }
    
  
    
    public void cevModeDisableComponent(String mode) {
    //            FacesContext fctx = FacesContext.getCurrentInstance();
    //            ELContext elctx = fctx.getELContext();
    //            Application jsfApp = fctx.getApplication();
    //            //create a ValueExpression that points to the ADF binding layer
    //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
    //            //
    //            ValueExpression valueExpr = exprFactory.createValueExpression(
    //                                         elctx,
    //                                         "#{pageFlowScope.mode=='E'}",
    //                                          Object.class
    //                                         );
    //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
        if (mode.equals("E")) {
               
                
            getHeaderEditBinding().setDisabled(true);
            getConsumableSNoBinding().setDisabled(true);
            getPartEntrySNoBinding().setDisabled(true);
            getConsumableEntrydetailcreateBinding().setDisabled(false);
            getPartEntrydetailcreateBinding().setDisabled(false);
            getItemDescBinding().setDisabled(true);
            getProdCopyDescBinding().setDisabled(true);
            getRevNoBinding().setDisabled(true);
            getProdCopRevBinding().setDisabled(true);
            getSelectProdCopyBinding().setDisabled(true);
            getPartsEntryButtonBinding().setDisabled(false);
            getConsumableButtonBinding().setDisabled(false);
            getCostingButtonBinding().setDisabled(false);
            getBomMakerButtonBinding().setDisabled(false);
            getCopyButtonBinding().setDisabled(true);
            
            
            
           
            
        } 
        
        else if (mode.equals("C")) 
        {
            getPartEntrySNoBinding().setDisabled(true);
            getConsumableEntrydetailcreateBinding().setDisabled(false);
            getPartEntrydetailcreateBinding().setDisabled(false);
            getItemDescBinding().setDisabled(true);
            getProdCopyDescBinding().setDisabled(true);
            getRevNoBinding().setDisabled(true);
            getProdCopRevBinding().setDisabled(true);
            getPartsEntryButtonBinding().setDisabled(false);
            getConsumableButtonBinding().setDisabled(false);
            getCostingButtonBinding().setDisabled(false);
            getBomMakerButtonBinding().setDisabled(false);
            getCopyButtonBinding().setDisabled(false);
            getConsumableSNoBinding().setDisabled(true);
           
            
            
        } 
        else if (mode.equals("V"))
        {
            getConsumableEntrydetailcreateBinding().setDisabled(true);
            getPartEntrydetailcreateBinding().setDisabled(true);
            getPartsEntryButtonBinding().setDisabled(false);
            getConsumableButtonBinding().setDisabled(false);
            getCostingButtonBinding().setDisabled(false);
            getBomMakerButtonBinding().setDisabled(false);
            getCopyButtonBinding().setDisabled(true);
        }
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setPartEntrydetailcreateBinding(RichButton partEntrydetailcreateBinding) {
        this.partEntrydetailcreateBinding = partEntrydetailcreateBinding;
    }

    public RichButton getPartEntrydetailcreateBinding() {
        return partEntrydetailcreateBinding;
    }
    public void setConsumableEntrydetailcreateBinding(RichButton consumableEntrydetailcreateBinding) {
        this.consumableEntrydetailcreateBinding = consumableEntrydetailcreateBinding;
    }

    public RichButton getConsumableEntrydetailcreateBinding() {
        return consumableEntrydetailcreateBinding;
    }

    public void setPartEntrySNoBinding(RichInputText partEntrySNoBinding) {
        this.partEntrySNoBinding = partEntrySNoBinding;
    }

    public RichInputText getPartEntrySNoBinding() {
        return partEntrySNoBinding;
    }

    public void setConsumableSNoBinding(RichInputText consumableSNoBinding) {
        this.consumableSNoBinding = consumableSNoBinding;
    }

    public RichInputText getConsumableSNoBinding() {
        return consumableSNoBinding;
    }

    public void setItemDescBinding(RichInputText itemDescBinding) {
        this.itemDescBinding = itemDescBinding;
    }

    public RichInputText getItemDescBinding() {
        return itemDescBinding;
    }

    public void setCopyButtonBinding(RichButton copyButtonBinding) {
        this.copyButtonBinding = copyButtonBinding;
    }

    public RichButton getCopyButtonBinding() {
        return copyButtonBinding;
    }

    public void setAlternateBomDetailPartCdBinding(RichInputText alternateBomDetailPartCdBinding) {
        this.alternateBomDetailPartCdBinding = alternateBomDetailPartCdBinding;
    }

    public RichInputText getAlternateBomDetailPartCdBinding() {
        return alternateBomDetailPartCdBinding;
    }

    public void setAlternateBomDetaildescBinding(RichInputText alternateBomDetaildescBinding) {
        this.alternateBomDetaildescBinding = alternateBomDetaildescBinding;
    }

    public RichInputText getAlternateBomDetaildescBinding() {
        return alternateBomDetaildescBinding;
    }

    public void setSaveBinding(RichButton saveBinding) {
        this.saveBinding = saveBinding;
    }

    public RichButton getSaveBinding() {
        return saveBinding;
    }

    public void setSaveAndCloseBinding(RichButton saveAndCloseBinding) {
        this.saveAndCloseBinding = saveAndCloseBinding;
    }

    public RichButton getSaveAndCloseBinding() {
        return saveAndCloseBinding;
    }

    public void partCreateInsert(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert").execute();
        partEntrySNoBinding.setValue((ADFUtils.evaluateEL("#{bindings.BomPartsEntryVO1Iterator.estimatedRowCount}")));
    }

    public void consumableCreateInsert(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert1").execute();
        consumableSNoBinding.setValue((ADFUtils.evaluateEL("#{bindings.BomConsumableEntryVO1Iterator.estimatedRowCount}")));
    }

    public void deleteDialogPopupDL(DialogEvent dialogEvent) {
        {
            if(dialogEvent.getOutcome().name().equals("ok"))
                {
                ADFUtils.findOperation("Delete3").execute();
                System.out.println("Record Delete Successfully");
                
                    FacesMessage Message = new FacesMessage("Record Deleted Successfully");   
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                    FacesContext fc = FacesContext.getCurrentInstance();   
                    fc.addMessage(null, Message);   
                
                }

            AdfFacesContext.getCurrentInstance().addPartialTarget(bindAltBomTable);
            
            System.out.println("jfdhhfd");
            
            
            System.out.println("adbssfushusfiushfs");


        }
    }

    public void setBindAltBomTable(RichTable bindAltBomTable) {
        this.bindAltBomTable = bindAltBomTable;
    }

    public RichTable getBindAltBomTable() {
        return bindAltBomTable;
    }

    public void setPartsEntryButtonBinding(RichShowDetailItem partsEntryButtonBinding) {
        this.partsEntryButtonBinding = partsEntryButtonBinding;
    }

    public RichShowDetailItem getPartsEntryButtonBinding() {
        return partsEntryButtonBinding;
    }

    public void setConsumableButtonBinding(RichShowDetailItem consumableButtonBinding) {
        this.consumableButtonBinding = consumableButtonBinding;
    }

    public RichShowDetailItem getConsumableButtonBinding() {
        return consumableButtonBinding;
    }

    public void setCostingButtonBinding(RichShowDetailItem costingButtonBinding) {
        this.costingButtonBinding = costingButtonBinding;
    }

    public RichShowDetailItem getCostingButtonBinding() {
        return costingButtonBinding;
    }

    public void setBomMakerButtonBinding(RichShowDetailItem bomMakerButtonBinding) {
        this.bomMakerButtonBinding = bomMakerButtonBinding;
    }

    public RichShowDetailItem getBomMakerButtonBinding() {
        return bomMakerButtonBinding;
    }
    
    
}
