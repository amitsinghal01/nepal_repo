package terms.eng.setup.ui.bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichCommandLink;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.util.ResetUtils;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

import org.apache.myfaces.trinidad.model.UploadedFile;

public class ChangeManagementBean {
    private RichOutputText bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichInputText entryNoBinding;
    private RichInputComboboxListOfValues preparedByBinding;
    private RichInputComboboxListOfValues checkedByBinding;
    private RichInputComboboxListOfValues approvedByBinding;
    private RichInputText approvePersonRemarkBinding;
    private RichInputText remarksBinding;
    private RichInputComboboxListOfValues moduleBinding;
    private RichInputDate reportingDateBinding;
    private RichSelectOneChoice statusBinding;
    private RichSelectOneChoice priorityBinding;
    private RichSelectOneChoice problemTypeBinding;
    private RichSelectOneChoice formTypeBinding;
    private RichInputComboboxListOfValues menuOptionBinding;
    private RichInputDate statusDateBinding;
    private RichInputText pathBinding;
    private RichInputText docSeqNoBinding;
    private RichInputText unitCodeDocBinding;
    private RichInputText refrenceDocNoBinding;
    private RichInputText refDocTypeBinding;
    private RichInputText fileNameBinding;
    private RichInputDate refDocDateBinding;
    private RichCommandLink downloadLinkBinding;
    private RichTable docRefTableBinding;
    private RichShowDetailItem cmTabBinding;
    private RichShowDetailItem rdTabBinding;

    public ChangeManagementBean() {
    }

    public void saveAL(ActionEvent actionEvent)
    {
        ADFUtils.setLastUpdatedByNew("ChangeManagementEntryVO1Iterator","LastUpdatedBy");
        OperationBinding op = ADFUtils.findOperation("getChangeManageEntryNo");
        op.execute();
        System.out.println("Result is===> "+op.getResult());
        if(op.getResult() != null && op.getResult().toString().equalsIgnoreCase("E"))
        {
            ADFUtils.findOperation("Commit").execute();
            ADFUtils.showMessage(" Record Updated Successfully", 2);
        }
        else if (op.getResult() != null && op.getResult().toString() != ""  && op.getResult().equals("S"))
        {
            System.out.println("+++++ in the save mode+++++++++++");
            ADFUtils.findOperation("Commit").execute();
            ADFUtils.showMessage("Record Saved Successfully.Change Management Entry No. is "+ADFUtils.evaluateEL("#{bindings.EntryNo.inputValue}"), 2);
        }
        else if(op.getResult() != null && op.getResult().toString().equalsIgnoreCase("N"))
        {
            ADFUtils.showMessage("Change Management Entry No. could not be generated. Try Again !!", 0);
        }
    }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
        
        if(checkedByBinding.getValue()==null)
        {
            approvedByBinding.setDisabled(true);
            approvePersonRemarkBinding.setDisabled(true);
        }
        else if(approvedByBinding.getValue()==null)
        {
            checkedByBinding.setDisabled(true);
            approvePersonRemarkBinding.setDisabled(true);
            
        }
        else if(approvedByBinding.getValue()!=null)
        {
            remarksBinding.setDisabled(true);
            approvedByBinding.setDisabled(true);
            checkedByBinding.setDisabled(true);
            if(approvePersonRemarkBinding.getValue()==null){
                approvePersonRemarkBinding.setDisabled(false);
            }else{
                approvePersonRemarkBinding.setDisabled(true);
            }
        }
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
            try {
                Method method1 =
                    component.getClass().getMethod("setDisabled", boolean.class);
                if (method1 != null) {
                    method1.invoke(component, valueComponent);
                }
            } catch (NoSuchMethodException e) {
                try {
                    Method method =
                        component.getClass().getMethod("setReadOnly", boolean.class);
                    if (method != null) {
                        method.invoke(component, valueComponent);
                    }
                } catch (Exception e1) {
                    // e.printStackTrace();//silently eat this exception.
                }


            } catch (Exception e) {
                // e.printStackTrace();//silently eat this exception.
            }
            List<UIComponent> childComponents = component.getChildren();
            for (UIComponent comp : childComponents) {
                makeComponentHierarchyReadOnly(comp, valueComponent);
            }
        }
    private void cevmodecheck(){
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }
    public void cevModeDisableComponent(String mode) {
    //            FacesContext fctx = FacesContext.getCurrentInstance();
    //            ELContext elctx = fctx.getELContext();
    //            Application jsfApp = fctx.getApplication();
    //            //create a ValueExpression that points to the ADF binding layer
    //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
    //            //
    //            ValueExpression valueExpr = exprFactory.createValueExpression(
    //                                         elctx,
    //                                         "#{pageFlowScope.mode=='E'}",
    //                                          Object.class
    //                                         );
    //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
        if (mode.equals("E")) {
            getStatusDateBinding().setDisabled(true);
            getPreparedByBinding().setDisabled(true);
            getEntryNoBinding().setDisabled(true);
            disabledDocField();
            
        } else if (mode.equals("C")) {
            getStatusDateBinding().setDisabled(true);
            getPreparedByBinding().setDisabled(true);
            getEntryNoBinding().setDisabled(true);
            getStatusBinding().setDisabled(true);
            disabledDocField();
            
        } else if (mode.equals("V")) {
            cmTabBinding.setDisabled(false);
            rdTabBinding.setDisabled(false);
        } 
    }
    
    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setEntryNoBinding(RichInputText entryNoBinding) {
        this.entryNoBinding = entryNoBinding;
    }

    public RichInputText getEntryNoBinding() {
        return entryNoBinding;
    }

    public String saveAndCloseAL() {
        ADFUtils.setLastUpdatedByNew("ChangeManagementEntryVO1Iterator","LastUpdatedBy");
        OperationBinding op = ADFUtils.findOperation("getChangeManageEntryNo");
        op.execute();
        System.out.println("Result is===> "+op.getResult());
        if(op.getResult() != null && op.getResult().toString().equalsIgnoreCase("E"))
        {
            ADFUtils.findOperation("Commit").execute();
            ADFUtils.showMessage(" Record Updated Successfully", 2);
            return "saveAndClose";
        }
        else if (op.getResult() != null && op.getResult().toString() != ""  && op.getResult().equals("S"))
        {
            System.out.println("+++++ in the save mode+++++++++++");
            ADFUtils.findOperation("Commit").execute();
            ADFUtils.showMessage("Record Saved Successfully.Change Management Entry No. is "+ADFUtils.evaluateEL("#{bindings.EntryNo.inputValue}"), 2);
            return "saveAndClose";
        }
        else if(op.getResult() != null && op.getResult().toString().equalsIgnoreCase("N"))
        {
            ADFUtils.showMessage("Change Management Entry No. could not be generated. Try Again !!", 0);
            return null;
        }
        return null;
    }

    public void setPreparedByBinding(RichInputComboboxListOfValues preparedByBinding) {
        this.preparedByBinding = preparedByBinding;
    }

    public RichInputComboboxListOfValues getPreparedByBinding() {
        return preparedByBinding;
    }

    public void setCheckedByBinding(RichInputComboboxListOfValues checkedByBinding) {
        this.checkedByBinding = checkedByBinding;
    }

    public RichInputComboboxListOfValues getCheckedByBinding() {
        return checkedByBinding;
    }

    public void setApprovedByBinding(RichInputComboboxListOfValues approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputComboboxListOfValues getApprovedByBinding() {
        return approvedByBinding;
    }

    public void setApprovePersonRemarkBinding(RichInputText approvePersonRemarkBinding) {
        this.approvePersonRemarkBinding = approvePersonRemarkBinding;
    }

    public RichInputText getApprovePersonRemarkBinding() {
        return approvePersonRemarkBinding;
    }

    public void setRemarksBinding(RichInputText remarksBinding) {
        this.remarksBinding = remarksBinding;
    }

    public RichInputText getRemarksBinding() {
        return remarksBinding;
    }

    public void setModuleBinding(RichInputComboboxListOfValues moduleBinding) {
        this.moduleBinding = moduleBinding;
    }

    public RichInputComboboxListOfValues getModuleBinding() {
        return moduleBinding;
    }

    public void setReportingDateBinding(RichInputDate reportingDateBinding) {
        this.reportingDateBinding = reportingDateBinding;
    }

    public RichInputDate getReportingDateBinding() {
        return reportingDateBinding;
    }

    public void setStatusBinding(RichSelectOneChoice statusBinding) {
        this.statusBinding = statusBinding;
    }

    public RichSelectOneChoice getStatusBinding() {
        return statusBinding;
    }

    public void setPriorityBinding(RichSelectOneChoice priorityBinding) {
        this.priorityBinding = priorityBinding;
    }

    public RichSelectOneChoice getPriorityBinding() {
        return priorityBinding;
    }

    public void setProblemTypeBinding(RichSelectOneChoice problemTypeBinding) {
        this.problemTypeBinding = problemTypeBinding;
    }

    public RichSelectOneChoice getProblemTypeBinding() {
        return problemTypeBinding;
    }

    public void setFormTypeBinding(RichSelectOneChoice formTypeBinding) {
        this.formTypeBinding = formTypeBinding;
    }

    public RichSelectOneChoice getFormTypeBinding() {
        return formTypeBinding;
    }

    public void setMenuOptionBinding(RichInputComboboxListOfValues menuOptionBinding) {
        this.menuOptionBinding = menuOptionBinding;
    }

    public RichInputComboboxListOfValues getMenuOptionBinding() {
        return menuOptionBinding;
    }

    public void ApproveByVCE(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        OperationBinding ob = ADFUtils.findOperation("checkApprovalStatus");
        ob.getParamsMap().put("formNm", "CH_MGM");
        ob.getParamsMap().put("authoLim", "AP");
        ob.getParamsMap().put("empcd", valueChangeEvent.getNewValue().toString());
        ob.execute();
        System.out.println(" EMP CD" + valueChangeEvent.getNewValue().toString());
        if ((ob.getResult() != null && ob.getResult().equals("Y"))) {
            Row row = (Row) ADFUtils.evaluateEL("#{bindings.ChangeManagementEntryVO1Iterator.currentRow}");
            row.setAttribute("ApprovedBy", null);
            ADFUtils.showMessage("You Don't Have Permission To Approve This Record.", 0);
        }

        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
    }

    public void checkedByVCE(ValueChangeEvent valueChangeEvent) 
    {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        OperationBinding ob = ADFUtils.findOperation("checkApprovalStatus");
        ob.getParamsMap().put("formNm", "CH_MGM");
        ob.getParamsMap().put("authoLim", "CH");
        ob.getParamsMap().put("empcd", valueChangeEvent.getNewValue().toString());
        ob.execute();
        System.out.println(" EMP CD" + valueChangeEvent.getNewValue().toString());
        if ((ob.getResult() != null && ob.getResult().equals("Y"))) {
            Row row = (Row) ADFUtils.evaluateEL("#{bindings.ChangeManagementEntryVO1Iterator.currentRow}");
            row.setAttribute("CheckedBy", null);
            ADFUtils.showMessage("You Don't Have Permission To Approve This Record.", 0);
        }
            valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        }

    public void statusVCL(ValueChangeEvent valueChangeEvent) 
    {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(valueChangeEvent!=null)
        {
            System.out.println("------STAtus-------"+statusBinding.getValue());
            if(!statusBinding.getValue().equals("P") && ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E"))
            {
                java.sql.Timestamp datetime = new java.sql.Timestamp(System.currentTimeMillis());
                oracle.jbo.domain.Date daTime = new  oracle.jbo.domain.Date(datetime);
                Row row = (Row) ADFUtils.evaluateEL("#{bindings.ChangeManagementEntryVO1Iterator.currentRow}");
                row.setAttribute("StatusDate", daTime);

            }
        }
    }

    public void setStatusDateBinding(RichInputDate statusDateBinding) {
        this.statusDateBinding = statusDateBinding;
    }

    public RichInputDate getStatusDateBinding() {
        return statusDateBinding;
    }
    
    public void uploadAction(ValueChangeEvent vce) {
        // Add event code here...
        System.out.println("New Value In VCE Of Upload File"+vce.getNewValue());
        if (vce.getNewValue() != null) {
                    //Get File Object from VC Event
                    UploadedFile fileVal = (UploadedFile) vce.getNewValue();
                    //Upload File to path- Return actual server path
                    String path = ADFUtils.uploadFile(fileVal);
                      System.out.println(fileVal.getContentType());
                      System.out.println("Content Type==>>"+fileVal.getContentType()+"Path==>>"+path+"File name==>>"+fileVal.getFilename());
                    //Method to insert data in table to keep track of uploaded files
                    OperationBinding ob = ADFUtils.findOperation("setFileDataChangeManagement");
                    ob.getParamsMap().put("name", fileVal.getFilename());
                    ob.getParamsMap().put("path", path);
                    ob.getParamsMap().put("contTyp", fileVal.getContentType());
                    ob.getParamsMap().put("SrNo", ADFUtils.evaluateEL("#{bindings.DocAttachRefDtlVo4Iterator.estimatedRowCount+1}"));
                    ob.execute();
                    // Reset inputFile component after upload
                    ResetUtils.reset(vce.getComponent());
                }
    }
    
    public void downloadAction(FacesContext facesContext, OutputStream outputStream) throws Exception {
        
        File filed = new File(pathBinding.getValue().toString());
                               FileInputStream fis;
                               byte[] b;
                               try {
                                   fis = new FileInputStream(filed);

                                   int n;
                                   while ((n = fis.available()) > 0) {
                                       b = new byte[n];
                                       int result = fis.read(b);
                                       outputStream.write(b, 0, b.length);
                                       if (result == -1)
                                           break;
                                   }
                               } catch (Exception e) {
                                   e.printStackTrace();
                               }
                               outputStream.flush();
        
    }

    public void setPathBinding(RichInputText pathBinding) {
        this.pathBinding = pathBinding;
    }

    public RichInputText getPathBinding() {
        return pathBinding;
    }

    public void setDocSeqNoBinding(RichInputText docSeqNoBinding) {
        this.docSeqNoBinding = docSeqNoBinding;
    }

    public RichInputText getDocSeqNoBinding() {
        return docSeqNoBinding;
    }

    public void setUnitCodeDocBinding(RichInputText unitCodeDocBinding) {
        this.unitCodeDocBinding = unitCodeDocBinding;
    }

    public RichInputText getUnitCodeDocBinding() {
        return unitCodeDocBinding;
    }

    public void setRefrenceDocNoBinding(RichInputText refrenceDocNoBinding) {
        this.refrenceDocNoBinding = refrenceDocNoBinding;
    }

    public RichInputText getRefrenceDocNoBinding() {
        return refrenceDocNoBinding;
    }

    public void setRefDocTypeBinding(RichInputText refDocTypeBinding) {
        this.refDocTypeBinding = refDocTypeBinding;
    }

    public RichInputText getRefDocTypeBinding() {
        return refDocTypeBinding;
    }

    public void setFileNameBinding(RichInputText fileNameBinding) {
        this.fileNameBinding = fileNameBinding;
    }

    public RichInputText getFileNameBinding() {
        return fileNameBinding;
    }

    public void setRefDocDateBinding(RichInputDate refDocDateBinding) {
        this.refDocDateBinding = refDocDateBinding;
    }

    public RichInputDate getRefDocDateBinding() {
        return refDocDateBinding;
    }

    public void setDownloadLinkBinding(RichCommandLink downloadLinkBinding) {
        this.downloadLinkBinding = downloadLinkBinding;
    }

    public RichCommandLink getDownloadLinkBinding() {
        return downloadLinkBinding;
    }

    public void docDialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                    {
                    ADFUtils.findOperation("Delete").execute();
                    ADFUtils.showMessage("Record Deleted Successfully", 2);
                    }
                   AdfFacesContext.getCurrentInstance().addPartialTarget(docRefTableBinding); 
    }
    
    public void disabledDocField()
    {
     getDocSeqNoBinding().setDisabled(true);
     getUnitCodeDocBinding().setDisabled(true);
     getRefrenceDocNoBinding().setDisabled(true);
     getRefDocTypeBinding().setDisabled(true);
     getFileNameBinding().setDisabled(true);
     getRefDocDateBinding().setDisabled(true);
     
     
    }

    public void setDocRefTableBinding(RichTable docRefTableBinding) {
        this.docRefTableBinding = docRefTableBinding;
    }

    public RichTable getDocRefTableBinding() {
        return docRefTableBinding;
    }

    public void setCmTabBinding(RichShowDetailItem cmTabBinding) {
        this.cmTabBinding = cmTabBinding;
    }

    public RichShowDetailItem getCmTabBinding() {
        return cmTabBinding;
    }

    public void setRdTabBinding(RichShowDetailItem rdTabBinding) {
        this.rdTabBinding = rdTabBinding;
    }

    public RichShowDetailItem getRdTabBinding() {
        return rdTabBinding;
    }
}
