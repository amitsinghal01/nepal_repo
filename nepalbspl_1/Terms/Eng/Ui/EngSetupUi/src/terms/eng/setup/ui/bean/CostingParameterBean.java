package terms.eng.setup.ui.bean;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import terms.eng.setup.model.view.CostingParameterVORowImpl;

public class CostingParameterBean {
    private RichTable tableBinding;
    private String editAction="V";

    public CostingParameterBean() {
    }

    public void deletepopUpDialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
        {
        OperationBinding op = null;
        ADFUtils.findOperation("Delete").execute();
        op = (OperationBinding) ADFUtils.findOperation("Commit");
        if (getEditAction().equals("V")) {
        Object rst = op.execute();
        }
        System.out.println("Record Delete Successfully");
        if(op.getErrors().isEmpty()){
        FacesMessage Message = new FacesMessage("Record Deleted Successfully");
        Message.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        } else if (!op.getErrors().isEmpty())
        {
        OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
        Object rstr = opr.execute();
        FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
        Message.setSeverity(FacesMessage.SEVERITY_ERROR);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(tableBinding);
    }

    public void setTableBinding(RichTable tableBinding) {
        this.tableBinding = tableBinding;
    }

    public RichTable getTableBinding() {
        return tableBinding;
    }

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public void createAL(ActionEvent actionEvent) {
        DCIteratorBinding dci = ADFUtils.findIterator("CostingParameterVO1Iterator");
        System.out.println("Rows : " + dci.getAllRowsInRange());
        CostingParameterVORowImpl row = (CostingParameterVORowImpl) dci.getCurrentRow();
        if (row == null || row.getParameter() != null) {
            ADFUtils.findOperation("CreateInsert").execute();
            ADFUtils.findOperation("getCostCodeCostingParameter").execute();
        } else {
            ADFUtils.showMessage("Please fill Description.", 0);
        }
    }

    public void saveAL(ActionEvent actionEvent) {
        oracle.binding.OperationBinding opr = ADFUtils.findOperation("descFindCostingParameter");
        Object obj = opr.execute();
        System.out.println("Returned After Amimpl: " + obj);
        if (obj != null && !obj.equals("Y")) {

            oracle.binding.OperationBinding op = ADFUtils.findOperation("Commit");

            Object rst = op.execute();

            if (op.getErrors().isEmpty())

            {

            } else

            {

                FacesMessage Message = new FacesMessage("Record can't be modified");

                Message.setSeverity(FacesMessage.SEVERITY_ERROR);

                FacesContext fc = FacesContext.getCurrentInstance();

                fc.addMessage(null, Message);

            }
        } else {

            ADFUtils.setEL("#{pageFlowScope.CostingParameterBean.editAction}", "A");
            ADFUtils.showMessage("Please fill Description.", 0);
        }
    }

}
