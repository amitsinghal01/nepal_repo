package terms.eng.setup.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.application.ViewHandler;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

public class CreateGroupHsnMappingBean {
    private RichButton groupHsnMapping;
    private RichButton groupHsnMappingTableBinding;
    private RichInputComboboxListOfValues bindHsnCode;
    private RichTable hsnProductTableMappingBinding;
    private RichInputText prodGrDescBinding;
    private RichSelectOneChoice typeBinding;
    private RichInputText serviceCodeBinding;
    private RichInputText serviceDescBinding;
    private RichPanelHeader myPageRootComponent;
    private RichOutputText bindingOutputText;
    private RichButton bindPopUp;
    private RichButton headerEditBinding;

    public CreateGroupHsnMappingBean() {
    }

   
    public void setGroupHsnMapping(RichButton groupHsnMapping) {
        this.groupHsnMapping = groupHsnMapping;
    }

    public RichButton getGroupHsnMapping() {
        return groupHsnMapping;
    }

    public void setGroupHsnMappingTableBinding(RichButton groupHsnMappingTableBinding) {
        this.groupHsnMappingTableBinding = groupHsnMappingTableBinding;
    }

    public RichButton getGroupHsnMappingTableBinding() {
        return groupHsnMappingTableBinding;
    }

    public void populateHsnAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("populateHsn").execute();
        FacesContext fctx = FacesContext.getCurrentInstance();
        String page = fctx.getViewRoot().getViewId();
        ViewHandler ViewH = fctx.getApplication().getViewHandler();
        UIViewRoot UIV = ViewH.createView(fctx, page);
        UIV.setViewId(page);
        fctx.setViewRoot(UIV);
        AdfFacesContext.getCurrentInstance().addPartialTarget(hsnProductTableMappingBinding);

    }

    public void saveHsnCodeAL(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedByNew("GroupHsnMappingVO1Iterator","LastUpdatedBy");
        ADFUtils.findOperation("insertPopulate").execute();
        ADFUtils.findOperation("Commit").execute();
        ADFUtils.showMessage("Record Saved Successfully", 2);
      
    
        
    }

    public void setBindHsnCode(RichInputComboboxListOfValues bindHsnCode) {
        this.bindHsnCode = bindHsnCode;
    }

    public RichInputComboboxListOfValues getBindHsnCode() {
        return bindHsnCode;
    }

//    public void saveAndCloseAL(ActionEvent actionEvent) {
//        ADFUtils.findOperation("insertPopulate").execute();
//        ADFUtils.findOperation("Commit").execute();
//    }

    public void hsnCodeVListener(ValueChangeEvent valueChangeEvent) {
    //ADFUtils.findOperation("refreshPage").execute();
    }

    public void setHsnProductTableMappingBinding(RichTable hsnProductTableMappingBinding) {
        this.hsnProductTableMappingBinding = hsnProductTableMappingBinding;
    }

    public RichTable getHsnProductTableMappingBinding() {
        return hsnProductTableMappingBinding;
    }

    public void setProdGrDescBinding(RichInputText prodGrDescBinding) {
        this.prodGrDescBinding = prodGrDescBinding;
    }

    public RichInputText getProdGrDescBinding() {
        return prodGrDescBinding;
    }

    public void setTypeBinding(RichSelectOneChoice typeBinding) {
        this.typeBinding = typeBinding;
    }

    public RichSelectOneChoice getTypeBinding() {
        return typeBinding;
    }

    public void setServiceCodeBinding(RichInputText serviceCodeBinding) {
        this.serviceCodeBinding = serviceCodeBinding;
    }

    public RichInputText getServiceCodeBinding() {
        return serviceCodeBinding;
    }

    public void setServiceDescBinding(RichInputText serviceDescBinding) {
        this.serviceDescBinding = serviceDescBinding;
    }

    public RichInputText getServiceDescBinding() {
        return serviceDescBinding;
    }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setMyPageRootComponent(RichPanelHeader myPageRootComponent) {
        this.myPageRootComponent = myPageRootComponent;
    }

    public RichPanelHeader getMyPageRootComponent() {
        return myPageRootComponent;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }
    
    private void cevmodecheck(){
            if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
                makeComponentHierarchyReadOnly(this.getMyPageRootComponent(), true);
                cevModeDisableComponent("V");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
                makeComponentHierarchyReadOnly(this.getMyPageRootComponent(), false);
                cevModeDisableComponent("C");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
                makeComponentHierarchyReadOnly(this.getMyPageRootComponent(), false);
                cevModeDisableComponent("E");
            }
        }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
               try {
                   Method method1 =
                       component.getClass().getMethod("setDisabled", boolean.class);
                   if (method1 != null) {
                       method1.invoke(component, valueComponent);
                   }
               } catch (NoSuchMethodException e) {
                   try {
                       Method method =
                           component.getClass().getMethod("setReadOnly", boolean.class);
                       if (method != null) {
                           method.invoke(component, valueComponent);
                       }
                   } catch (Exception e1) {
                       // e.printStackTrace();//silently eat this exception.
                   }


               } catch (Exception e) {
                   // e.printStackTrace();//silently eat this exception.
               }
               List<UIComponent> childComponents = component.getChildren();
               for (UIComponent comp : childComponents) {
                   makeComponentHierarchyReadOnly(comp, valueComponent);
               }
           }
    public void setBindPopUp(RichButton bindPopUp) {
        this.bindPopUp = bindPopUp;
    }

    public RichButton getBindPopUp() {
        return bindPopUp;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }
    public void cevModeDisableComponent(String mode) {
            if (mode.equals("E")) {
             getServiceCodeBinding().setDisabled(true); 
             getTypeBinding().setDisabled(true);
             getProdGrDescBinding().setDisabled(true);
             getBindPopUp().setDisabled(true); 
             getHeaderEditBinding().setDisabled(true);
             getBindHsnCode().setDisabled(false);
                
            }
            if (mode.equals("C")) {
                getServiceCodeBinding().setDisabled(true); 
                getTypeBinding().setDisabled(true);
                getProdGrDescBinding().setDisabled(true);  
                getHeaderEditBinding().setDisabled(true);
            }
            if (mode.equals("V")) {
               getBindPopUp().setDisabled(true);    
            }
        
        }

   
}
