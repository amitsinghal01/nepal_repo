package terms.eng.setup.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

public class CreateGstPurchaseLedgerMappingBean {
    private RichTable createGstPurchaseLedgerMappingTableBinding;
    private RichInputText gstCodeBinding;
    private RichInputText gstDescBinding;
//    private RichInputText cgstBinding;
    private RichInputText sgstBinding;
    private RichInputText igstRateBinding;
    private RichInputDate validUptoBinding;
    private RichInputText hsnCodeBinding;
    private RichButton headerEditBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichOutputText bindingOutputText;
    private RichButton detailcreateBinding;
    private RichButton detaildeleteBinding;

    public CreateGstPurchaseLedgerMappingBean() {
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        
        if(dialogEvent.getOutcome().name().equals("ok"))
                    {
                    ADFUtils.findOperation("Delete").execute();
                    //ADFUtils.findOperation("Commit").execute();    //Unable on Search page.
                    System.out.println("Record Delete Successfully");
                   
                        FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                        FacesContext fc = FacesContext.getCurrentInstance();  
                        fc.addMessage(null, Message);  
                   
                    }

                AdfFacesContext.getCurrentInstance().addPartialTarget(createGstPurchaseLedgerMappingTableBinding);
    }

    public void setCreateGstPurchaseLedgerMappingTableBinding(RichTable createGstPurchaseLedgerMappingTableBinding) {
        this.createGstPurchaseLedgerMappingTableBinding = createGstPurchaseLedgerMappingTableBinding;
    }

    public RichTable getCreateGstPurchaseLedgerMappingTableBinding() {
        return createGstPurchaseLedgerMappingTableBinding;
    }

    public void saveButtonAL(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedByNew("GstRateMasterVO3Iterator","LastUpdatedBy");
        FacesMessage Message = new FacesMessage("Record Updated Successfully");   
              Message.setSeverity(FacesMessage.SEVERITY_INFO);   
              FacesContext fc = FacesContext.getCurrentInstance();   
              fc.addMessage(null, Message);
    }

    public void setGstCodeBinding(RichInputText gstCodeBinding) {
        this.gstCodeBinding = gstCodeBinding;
    }

    public RichInputText getGstCodeBinding() {
        return gstCodeBinding;
    }

    public void setGstDescBinding(RichInputText gstDescBinding) {
        this.gstDescBinding = gstDescBinding;
    }

    public RichInputText getGstDescBinding() {
        return gstDescBinding;
    }

//    public void setCgstBinding(RichInputText cgstBinding) {
//        this.cgstBinding = cgstBinding;
//    }
//
//    public RichInputText getCgstBinding() {
//        return cgstBinding;
//    }

    public void setSgstBinding(RichInputText sgstBinding) {
        this.sgstBinding = sgstBinding;
    }

    public RichInputText getSgstBinding() {
        return sgstBinding;
    }

    public void setIgstRateBinding(RichInputText igstRateBinding) {
        this.igstRateBinding = igstRateBinding;
    }

    public RichInputText getIgstRateBinding() {
        return igstRateBinding;
    }

    public void setValidUptoBinding(RichInputDate validUptoBinding) {
        this.validUptoBinding = validUptoBinding;
    }

    public RichInputDate getValidUptoBinding() {
        return validUptoBinding;
    }

    public void setHsnCodeBinding(RichInputText hsnCodeBinding) {
        this.hsnCodeBinding = hsnCodeBinding;
    }

    public RichInputText getHsnCodeBinding() {
        return hsnCodeBinding;
    }

    

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        
        cevmodecheck();
        return bindingOutputText;
    }

    public void setDetailcreateBinding(RichButton detailcreateBinding) {
        this.detailcreateBinding = detailcreateBinding;
    }

    public RichButton getDetailcreateBinding() {
        return detailcreateBinding;
    }

    public void setDetaildeleteBinding(RichButton detaildeleteBinding) {
        this.detaildeleteBinding = detaildeleteBinding;
    }

    public RichButton getDetaildeleteBinding() {
        return detaildeleteBinding;
    }
    
    public void EditButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
            try {
                Method method1 =
                    component.getClass().getMethod("setDisabled", boolean.class);
                if (method1 != null) {
                    method1.invoke(component, valueComponent);
                }
            } catch (NoSuchMethodException e) {
                try {
                    Method method =
                        component.getClass().getMethod("setReadOnly", boolean.class);
                    if (method != null) {
                        method.invoke(component, valueComponent);
                    }
                } catch (Exception e1) {
                    // e.printStackTrace();//silently eat this exception.
                }


            } catch (Exception e) {
                // e.printStackTrace();//silently eat this exception.
            }
            List<UIComponent> childComponents = component.getChildren();
            for (UIComponent comp : childComponents) {
                makeComponentHierarchyReadOnly(comp, valueComponent);
            }
        }
    
    
    private void cevmodecheck(){
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }
    
    public void cevModeDisableComponent(String mode) {
        
//        
//        if(mode.equals("C")) {
//            getHeaderEditBinding().setDisabled(true);
//            
//            
//        }
//        
        if(mode.equals("V")) {
            
            getDetailcreateBinding().setDisabled((true));
        getDetaildeleteBinding().setDisabled(true);
            
        }
        if(mode.equals("E")){
            
            getGstCodeBinding().setDisabled(true);
            getGstDescBinding().setDisabled(true);
           // getCgstBinding().setDisabled(true);
            getSgstBinding().setDisabled(true);
            getIgstRateBinding().setDisabled(true);
            getValidUptoBinding().setDisabled(true);
            getHsnCodeBinding().setDisabled(true);
            getDetailcreateBinding().setDisabled(false);           getDetaildeleteBinding().setDisabled(false);
            getHeaderEditBinding().setDisabled(true);
            
            
        }
    }
}
