package terms.eng.setup.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

public class CreateGstSaleLedgerMappingBean {
    private RichTable createGstSaleLedgerMappingTableBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichOutputText bindingOutputText;
    private RichInputText gstCodeBinding;
//    private RichInputText cgstBinding;
    private RichInputText sgstBinding;
    private RichInputText igstRateBinding;
    private RichInputDate validUptoBinding;
    private RichButton headerEditBinding;
    private RichButton detailCreateBinding;
    private RichInputText gstDescriptionBinding;

    public CreateGstSaleLedgerMappingBean() {
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        // Add event code here...
        

        if(dialogEvent.getOutcome().name().equals("ok"))
                    {
                    ADFUtils.findOperation("Delete").execute();
                    //ADFUtils.findOperation("Commit").execute();    //Unable on Search page.
                    System.out.println("Record Delete Successfully");
                   
                        FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                        FacesContext fc = FacesContext.getCurrentInstance();  
                        fc.addMessage(null, Message);  
                   
                    }

                AdfFacesContext.getCurrentInstance().addPartialTarget(createGstSaleLedgerMappingTableBinding);
        
    }

    public void setCreateGstSaleLedgerMappingTableBinding(RichTable createGstSaleLedgerMappingTableBinding) {
        this.createGstSaleLedgerMappingTableBinding = createGstSaleLedgerMappingTableBinding;
    }

    public RichTable getCreateGstSaleLedgerMappingTableBinding() {
        return createGstSaleLedgerMappingTableBinding;
    }

    public void saveButtonAL(ActionEvent actionEvent)
    {
        ADFUtils.setLastUpdatedByNew("GstRateMasterVO2Iterator","LastUpdatedBy");
        ADFUtils.findOperation("Commit").execute();
        FacesMessage Message = new FacesMessage("Record Updated Successfully.");   
        Message.setSeverity(FacesMessage.SEVERITY_INFO);   
        FacesContext fc = FacesContext.getCurrentInstance();   
        fc.addMessage(null, Message); 
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGstCodeBinding(RichInputText gstCodeBinding) {
        this.gstCodeBinding = gstCodeBinding;
    }

    public RichInputText getGstCodeBinding() {
        return gstCodeBinding;
    }

//    public void setCgstBinding(RichInputText cgstBinding) {
//        this.cgstBinding = cgstBinding;
//    }
//
//    public RichInputText getCgstBinding() {
//        return cgstBinding;
//    }

    public void setSgstBinding(RichInputText sgstBinding) {
        this.sgstBinding = sgstBinding;
    }

    public RichInputText getSgstBinding() {
        return sgstBinding;
    }

    public void setIgstRateBinding(RichInputText igstRateBinding) {
        this.igstRateBinding = igstRateBinding;
    }

    public RichInputText getIgstRateBinding() {
        return igstRateBinding;
    }

    public void setValidUptoBinding(RichInputDate validUptoBinding) {
        this.validUptoBinding = validUptoBinding;
    }

    public RichInputDate getValidUptoBinding() {
        return validUptoBinding;
    }
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
            try {
                Method method1 =
                    component.getClass().getMethod("setDisabled", boolean.class);
                if (method1 != null) {
                    method1.invoke(component, valueComponent);
                }
            } catch (NoSuchMethodException e) {
                try {
                    Method method =
                        component.getClass().getMethod("setReadOnly", boolean.class);
                    if (method != null) {
                        method.invoke(component, valueComponent);
                    }
                } catch (Exception e1) {
                    // e.printStackTrace();//silently eat this exception.
                }


            } catch (Exception e) {
                // e.printStackTrace();//silently eat this exception.
            }
            List<UIComponent> childComponents = component.getChildren();
            for (UIComponent comp : childComponents) {
                makeComponentHierarchyReadOnly(comp, valueComponent);
            }
        }
    private void cevmodecheck(){
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }
    public void cevModeDisableComponent(String mode) {
    //            FacesContext fctx = FacesContext.getCurrentInstance();
    //            ELContext elctx = fctx.getELContext();
    //            Application jsfApp = fctx.getApplication();
    //            //create a ValueExpression that points to the ADF binding layer
    //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
    //            //
    //            ValueExpression valueExpr = exprFactory.createValueExpression(
    //                                         elctx,
    //                                         "#{pageFlowScope.mode=='E'}",
    //                                          Object.class
    //                                         );
    //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
        if (mode.equals("E")) {
            getGstDescriptionBinding().setDisabled(true);
            getValidUptoBinding().setDisabled(true);
            getIgstRateBinding().setDisabled(true);  
            getSgstBinding().setDisabled(true); 
//                getCgstBinding() .setDisabled(true);
                getGstCodeBinding().setDisabled(true);
                getHeaderEditBinding().setDisabled(true);
               getDetailCreateBinding().setDisabled(false);
        } else if (mode.equals("C")) {
            getGstDescriptionBinding().setDisabled(true);
            getValidUptoBinding().setDisabled(true);
            getIgstRateBinding().setDisabled(true);  
            getSgstBinding().setDisabled(true); 
//            getCgstBinding().setDisabled(true); 
            getGstCodeBinding().setDisabled(true);
           getDetailCreateBinding().setDisabled(false);
        } else if (mode.equals("V")) {
            getDetailCreateBinding().setDisabled(true);
        }
    }
    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void EditButtonAL(ActionEvent actionEvent) {
        
        cevmodecheck();
    }

    public void setDetailCreateBinding(RichButton detailCreateBinding) {
        this.detailCreateBinding = detailCreateBinding;
    }

    public RichButton getDetailCreateBinding() {
        return detailCreateBinding;
    }


    public void setGstDescriptionBinding(RichInputText gstDescriptionBinding) {
        this.gstDescriptionBinding = gstDescriptionBinding;
    }

    public RichInputText getGstDescriptionBinding() {
        return gstDescriptionBinding;
    }
}
