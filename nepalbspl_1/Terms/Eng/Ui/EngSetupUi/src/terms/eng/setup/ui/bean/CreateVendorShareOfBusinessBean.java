package terms.eng.setup.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

public class CreateVendorShareOfBusinessBean {
    private RichInputText itemCodeBinding;
    private RichInputText vendorCodeBinding;
    private RichInputText rateBinding;
    private RichInputText vendorNameBinding;
    private RichButton headerEditBinding;
    private RichPanelHeader myPageRootComponent;
    private RichOutputText bindingOutputText;
    private RichInputText bindSob;
    private RichInputText bindMoq;
    private RichInputText totalSobBinding;

    public CreateVendorShareOfBusinessBean() {
    }

    public void saveAL(ActionEvent actionEvent)
    {
        ADFUtils.setLastUpdatedByNew("SearchVendorItemVO1Iterator","LastUpdatedBy");
        BigDecimal TotalSob=new BigDecimal(0);
        Long RowCnt=(Long)ADFUtils.evaluateEL("#{bindings.SearchVendorItemVO1Iterator.estimatedRowCount}");
        if(totalSobBinding.getValue()!=null)
        {
        TotalSob=(BigDecimal)totalSobBinding.getValue();   
        }
        if( (RowCnt >1 && TotalSob.compareTo(new BigDecimal(100))==0) || RowCnt<=1) 
        {
            ADFUtils.findOperation("Commit").execute();
//            System.out.println("Editing Mode " + "pageFlowScope.mode".equals("S"));
            FacesMessage Message = new FacesMessage("Record Update Successfully");
            System.out.println("After Record Update");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);   
            FacesContext fc = FacesContext.getCurrentInstance();   
            fc.addMessage(null, Message);
            System.out.println("After Message Update");
        }
        else
        {
        ADFUtils.showMessage("Total SOB must be 100%",0);    
        }
    }
    
   

    public void setItemCodeBinding(RichInputText itemCodeBinding) {
        this.itemCodeBinding = itemCodeBinding;
    }

    public RichInputText getItemCodeBinding() {
        return itemCodeBinding;
    }

    public void setVendorCodeBinding(RichInputText vendorCodeBinding) {
        this.vendorCodeBinding = vendorCodeBinding;
    }

    public RichInputText getVendorCodeBinding() {
        return vendorCodeBinding;
    }

    public void setRateBinding(RichInputText rateBinding) {
        this.rateBinding = rateBinding;
    }

    public RichInputText getRateBinding() {
        return rateBinding;
    }

    public void setVendorNameBinding(RichInputText vendorNameBinding) {
        this.vendorNameBinding = vendorNameBinding;
    }

    public RichInputText getVendorNameBinding() {
        return vendorNameBinding;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setMyPageRootComponent(RichPanelHeader myPageRootComponent) {
        this.myPageRootComponent = myPageRootComponent;
    }

    public RichPanelHeader getMyPageRootComponent() {
        return myPageRootComponent;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }
    private void cevmodecheck(){
          if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
              makeComponentHierarchyReadOnly(this.getMyPageRootComponent(), true);
              cevModeDisableComponent("V");
          }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
              makeComponentHierarchyReadOnly(this.getMyPageRootComponent(), false);
              cevModeDisableComponent("C");
          }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
              makeComponentHierarchyReadOnly(this.getMyPageRootComponent(), false);
              cevModeDisableComponent("E");
          }
      }
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
               try {
                   Method method1 =
                       component.getClass().getMethod("setDisabled", boolean.class);
                   if (method1 != null) {
                       method1.invoke(component, valueComponent);
                   }
               } catch (NoSuchMethodException e) {
                   try {
                       Method method =
                           component.getClass().getMethod("setReadOnly", boolean.class);
                       if (method != null) {
                           method.invoke(component, valueComponent);
                       }
                   } catch (Exception e1) {
                       // e.printStackTrace();//silently eat this exception.
                   }


               } catch (Exception e) {
                   // e.printStackTrace();//silently eat this exception.
               }
               List<UIComponent> childComponents = component.getChildren();
               for (UIComponent comp : childComponents) {
                   makeComponentHierarchyReadOnly(comp, valueComponent);
               }
           }
    
    public void cevModeDisableComponent(String mode) {
        
        if (mode.equals("E")) {
                     getVendorCodeBinding().setDisabled(true);
                     getVendorNameBinding().setDisabled(true);
                     getItemCodeBinding().setDisabled(true);
                     getHeaderEditBinding().setDisabled(true);
                     getRateBinding().setDisabled(true);
              }  else if (mode.equals("V")) {
                 
              }
    }

    public void setBindSob(RichInputText bindSob) {
        this.bindSob = bindSob;
    }

    public RichInputText getBindSob() {
        return bindSob;
    }

    public void setBindMoq(RichInputText bindMoq) {
        this.bindMoq = bindMoq;
    }

    public RichInputText getBindMoq() {
        return bindMoq;
    }
    
    public void totalSob()
    {
        //#{bindings.SearchVendorItemVO1Iterator.estimatedRowCount}
    //#{bindings.SearchVendorIShareBusinessVO1Iterator.currentRow}
//        Row selectedRow = (Row)ADFUtils.evaluateEL("#{bindings.SearchVendorItemVO1Iterator.estimatedRowCount}"); 
//        System.out.println("selected address is "+selectedRow.getAttribute("TotalSob"));


    }


    public void setTotalSobBinding(RichInputText totalSobBinding) {
        this.totalSobBinding = totalSobBinding;
    }

    public RichInputText getTotalSobBinding() {
        return totalSobBinding;
    }
    
    public String saveAndClose()
    {
        ADFUtils.setLastUpdatedByNew("SearchVendorItemVO1Iterator","LastUpdatedBy");
        BigDecimal TotalSob=new BigDecimal(0);
        Long RowCnt=(Long)ADFUtils.evaluateEL("#{bindings.SearchVendorItemVO1Iterator.estimatedRowCount}");
        if(totalSobBinding.getValue()!=null)
        {
        TotalSob=(BigDecimal)totalSobBinding.getValue();   
        }
        BigDecimal abc=new BigDecimal(100);
        System.out.println("abc=."+abc+"  Total sob"+TotalSob);
        System.out.println(abc.compareTo(TotalSob));
        System.out.println("TotalSob.compareTo(new BigDecimal(100)"+TotalSob.compareTo(abc));
        
        if( (RowCnt >1 && TotalSob.compareTo(new BigDecimal(100))==0) || RowCnt<=1) 
        {
            ADFUtils.findOperation("Commit").execute();
    //            System.out.println("Editing Mode " + "pageFlowScope.mode".equals("S"));
            FacesMessage Message = new FacesMessage("Record Update Successfully");
            System.out.println("After Record Update");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);   
            FacesContext fc = FacesContext.getCurrentInstance();   
            fc.addMessage(null, Message);
            System.out.println("After Message Update");
            return "SaveAndClose";
        }
        else
        {
        ADFUtils.showMessage("Total Sob must be 100%",0);    
        }
        return null;
    }
    
    
}
