package terms.eng.setup.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

public class EditProductwiseHsnCodeMappingBean {
    private RichInputText itemCodeBinding;
    private RichInputText uomBinding;
    private RichInputText specificationBinding;
    private RichInputText gstCodeBinding;
    private RichInputText itemDescBinding;
    private RichButton headerEditBinding;
    private RichOutputText bindingOutputText;
    private RichInputComboboxListOfValues hsnCodeBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton saveBinding;
    private RichButton saveAndCloseBinding;

    public EditProductwiseHsnCodeMappingBean() {
    }

    public void saveAL(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedByNew("ItemStockVO2Iterator","LastUpdatedBy");
        FacesMessage Message = new FacesMessage("Record Updated Successfully.");   
          Message.setSeverity(FacesMessage.SEVERITY_INFO);   
          FacesContext fc = FacesContext.getCurrentInstance();   
          fc.addMessage(null, Message); 
       
    }

    public void setItemCodeBinding(RichInputText itemCodeBinding) {
        this.itemCodeBinding = itemCodeBinding;
    }

    public RichInputText getItemCodeBinding() {
        return itemCodeBinding;
    }

    public void setUomBinding(RichInputText uomBinding) {
        this.uomBinding = uomBinding;
    }

    public RichInputText getUomBinding() {
        return uomBinding;
    }

    public void setSpecificationBinding(RichInputText specificationBinding) {
        this.specificationBinding = specificationBinding;
    }

    public RichInputText getSpecificationBinding() {
        return specificationBinding;
    }

    public void setGstCodeBinding(RichInputText gstCodeBinding) {
        this.gstCodeBinding = gstCodeBinding;
    }

    public RichInputText getGstCodeBinding() {
        return gstCodeBinding;
    }

    public void setItemDescBinding(RichInputText itemDescBinding) {
        this.itemDescBinding = itemDescBinding;
    }

    public RichInputText getItemDescBinding() {
        return itemDescBinding;
    }

    public void EditButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        
        cevmodecheck();
        return bindingOutputText;
    }

    public void setHsnCodeBinding(RichInputComboboxListOfValues hsnCodeBinding) {
        this.hsnCodeBinding = hsnCodeBinding;
    }

    public RichInputComboboxListOfValues getHsnCodeBinding() {
        return hsnCodeBinding;
    }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
            try {
                Method method1 =
                    component.getClass().getMethod("setDisabled", boolean.class);
                if (method1 != null) {
                    method1.invoke(component, valueComponent);
                }
            } catch (NoSuchMethodException e) {
                try {
                    Method method =
                        component.getClass().getMethod("setReadOnly", boolean.class);
                    if (method != null) {
                        method.invoke(component, valueComponent);
                    }
                } catch (Exception e1) {
                    // e.printStackTrace();//silently eat this exception.
                }


            } catch (Exception e) {
                // e.printStackTrace();//silently eat this exception.
            }
            List<UIComponent> childComponents = component.getChildren();
            for (UIComponent comp : childComponents) {
                makeComponentHierarchyReadOnly(comp, valueComponent);
            }
        }
    
    public void cevModeDisableComponent(String mode) 
    {
        if(mode.equals("E")) {
           
           getItemCodeBinding().setDisabled(true);
           getItemDescBinding().setDisabled(true);
           //getHsnCodeBinding().setDisabled(false);
           getSpecificationBinding().setDisabled(true);
           getUomBinding().setDisabled(true);
           getGstCodeBinding().setDisabled(true);
           getHeaderEditBinding().setDisabled(true);
            getSaveAndCloseBinding().setDisabled(false);
            getSaveBinding().setDisabled(false);
           
           
            
        }
        if(mode.equals("V")){
        
        getSaveAndCloseBinding().setDisabled(true);
        getSaveBinding().setDisabled(true);
        
        }
}
    
    private void cevmodecheck(){
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
            
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setSaveBinding(RichButton saveBinding) {
        this.saveBinding = saveBinding;
    }

    public RichButton getSaveBinding() {
        return saveBinding;
    }

    public void setSaveAndCloseBinding(RichButton saveAndCloseBinding) {
        this.saveAndCloseBinding = saveAndCloseBinding;
    }

    public RichButton getSaveAndCloseBinding() {
        return saveAndCloseBinding;
    }
}
