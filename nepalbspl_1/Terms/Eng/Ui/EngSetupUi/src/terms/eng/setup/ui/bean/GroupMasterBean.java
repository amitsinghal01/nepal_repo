package terms.eng.setup.ui.bean;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;
public class GroupMasterBean {
    private RichTable tableBinding;
    private String editAction="V";
    private RichInputText groupCodeBinding;

    public GroupMasterBean() {
    }

    public void popUpDialogDL(DialogEvent dialogEvent)
    {
        if(dialogEvent.getOutcome().name().equals("ok"))
            {
            OperationBinding op = null;
            ADFUtils.findOperation("Delete").execute();
            op = (OperationBinding) ADFUtils.findOperation("Commit");
            if (getEditAction().equals("V")) {
                Object rst = op.execute();
            }
            System.out.println("Record Delete Successfully");
                if(op.getErrors().isEmpty()){
                    FacesMessage Message = new FacesMessage("Record Deleted Successfully");   
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                    FacesContext fc = FacesContext.getCurrentInstance();   
                    fc.addMessage(null, Message);
                } else if (!op.getErrors().isEmpty())
                {
                   OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
                   Object rstr = opr.execute();
                   FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                   Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                   FacesContext fc = FacesContext.getCurrentInstance();
                   fc.addMessage(null, Message);
                 }
            }

        AdfFacesContext.getCurrentInstance().addPartialTarget(tableBinding);
            
    }
    
    

    public void setTableBinding(RichTable tableBinding) {
        this.tableBinding = tableBinding;
    }

    public RichTable getTableBinding() {
        return tableBinding;
    }

    public void isEnableEdit(ActionEvent actionEvent) {
        // Add event code here...
        this.editAction=getEditAction();
    }

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public void groupCodeVCE(ValueChangeEvent vce) 
    {
     vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
     System.out.println("Edit Action is=> "+editAction);
     if(vce !=null && editAction.equals("E"))
     {
    String Oldvalue=(String) vce.getOldValue(); 
    OperationBinding opr =(OperationBinding) ADFUtils.findOperation("checkGroupCode");
    opr.getParamsMap().put("Oldvalue",Oldvalue);
    Object obj=opr.execute();

    if(opr.getResult() !=null && opr.getResult().equals("Y")){
        
        FacesMessage Message = new FacesMessage("Record cannot change. Child record found.");
        Message.setSeverity(FacesMessage.SEVERITY_ERROR);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        groupCodeBinding.setValue(Oldvalue);
    }
             
     }
    
    }

    public void setGroupCodeBinding(RichInputText groupCodeBinding) {
        this.groupCodeBinding = groupCodeBinding;
    }

    public RichInputText getGroupCodeBinding() {
        return groupCodeBinding;
    }
}
