package terms.eng.setup.ui.bean;

import java.math.BigDecimal;

import javax.faces.application.FacesMessage;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.validator.ValidatorException;

import oracle.adf.model.OperationBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

public class GstRateMasterBean {
    private RichTable gstRateTableBinding;
    private String editAction="V";
    
    
    BigDecimal val=new BigDecimal(0);
    private RichInputText gstCodeBinding;
    private RichInputText cgstPerBinding;
    private RichInputText sgstperBinding;
    private RichInputText igstPerBinding;

    public GstRateMasterBean() {
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) 
    {
            if(dialogEvent.getOutcome().name().equals("ok"))
            {
            OperationBinding op = null;
            ADFUtils.findOperation("Delete").execute();
            op = (OperationBinding) ADFUtils.findOperation("Commit");
            if (getEditAction().equals("V")) {
            Object rst = op.execute();
            }
            System.out.println("Record Delete Successfully");
            if(op.getErrors().isEmpty()){
            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
            } else if (!op.getErrors().isEmpty())
            {
            OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
            Object rstr = opr.execute();
            FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
            Message.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
            }
                   
                    }

                AdfFacesContext.getCurrentInstance().addPartialTarget(gstRateTableBinding);
    }

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public void setGstRateTableBinding(RichTable gstRateTableBinding) {
        this.gstRateTableBinding = gstRateTableBinding;
    }

    public RichTable getGstRateTableBinding() {
        return gstRateTableBinding;
    }

//    public void cgstValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
//    
//        if(object!=null){
//        
//        BigDecimal obj = (BigDecimal) object;
//        System.out.println("<---CGST No.------>"+obj);
//        if(obj.compareTo(val) <=0){
//            System.out.println("bvmfdbhf");
//            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "CGST (%) must be greater than zero.", null));
//        }
////        if(!obj.equals(sgstperBinding.getValue()))
////        {
////            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "CGST % must be equal to the SGST %.", null));
////        }
//    }
//
//    }

    public void sgstValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if(object!=null)
        {
        
            BigDecimal obj = (BigDecimal) object;
                System.out.println("<---SGST No.------>"+obj);
            if(obj.compareTo(val) <=0){
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "VAT % must be greater than zero.", null));
            }
//            if(cgstPerBinding.getValue()!=null)
//            {
//                if(!obj.equals(cgstPerBinding.getValue())){
//                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "SGST % must be equal to the CGST %.", null));
//                }
//            }
        }

    }

    public void igstRateValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if(object!=null){
        
        BigDecimal obj = (BigDecimal) object;
        System.out.println("<---IGST No.------>"+obj);
        if(obj.compareTo(val) <=0){
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Excise Rate must be greater than zero.", null));
        }
//            BigDecimal cgst =  new BigDecimal(0);
//            BigDecimal sgst =  new BigDecimal(0);
        
//            cgst = (BigDecimal)cgstPerBinding.getValue();
//            sgst = (BigDecimal)sgstperBinding.getValue();
//            BigDecimal TotalIgst =  new BigDecimal(0);
//            if(cgst!=null && sgst!=null)
//            {
//                TotalIgst=cgst.add(sgst);
//                System.out.println("----- the sum is ----"+TotalIgst);
//                if(TotalIgst.compareTo(obj)==-1 ||TotalIgst.compareTo(obj)==1)
//                {
//                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "IGST Rate must be the sum of SGST and CGST.", null));
//                }
//            }
        }
    }

    public void CreateButtonAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert").execute();
                ADFUtils.findOperation("getGstNo").execute();
    }

    public void setCgstPerBinding(RichInputText cgstPerBinding) {
        this.cgstPerBinding = cgstPerBinding;
    }

    public RichInputText getCgstPerBinding() {
        return cgstPerBinding;
    }

    public void setSgstperBinding(RichInputText sgstperBinding) {
        this.sgstperBinding = sgstperBinding;
    }

    public RichInputText getSgstperBinding() {
        return sgstperBinding;
    }

    public void setIgstPerBinding(RichInputText igstPerBinding) {
        this.igstPerBinding = igstPerBinding;
    }

    public RichInputText getIgstPerBinding() {
        return igstPerBinding;
    }
}
