package terms.eng.setup.ui.bean;

import java.math.BigDecimal;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.jbo.Row;

public class GstServiceItemMasterBean {
    private String editAction="V";
    private RichTable gstServiceItemTableBinding;
    String Code="";
    Integer CodeNo=0;

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public GstServiceItemMasterBean() {
    }
    
    public void deletePopUpDialogDL(DialogEvent dialogEvent) 
    {
            if(dialogEvent.getOutcome().name().equals("ok"))
            {
            OperationBinding op = null;
            ADFUtils.findOperation("Delete").execute();
            op = (OperationBinding) ADFUtils.findOperation("Commit");
            if (getEditAction().equals("V")) {
            Object rst = op.execute();
            }
                System.out.println("Record Delete Successfully");
                if(op.getErrors().isEmpty()){
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            } else if (!op.getErrors().isEmpty())
            {
                OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
                Object rstr = opr.execute();
                FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            }
                   
                    }
            AdfFacesContext.getCurrentInstance().addPartialTarget(getGstServiceItemTableBinding());
    }

    public void setGstServiceItemTableBinding(RichTable gstServiceItemTableBinding) {
        this.gstServiceItemTableBinding = gstServiceItemTableBinding;
    }

    public RichTable getGstServiceItemTableBinding() {
        return gstServiceItemTableBinding;
    }

    public void createAL(ActionEvent actionEvent) {
        // Add event code here...
        System.out.println("Before Create Insert in Bean");
        ADFUtils.findOperation("CreateInsert").execute();
        
       try
       {
         if(Code==null || Code =="")
        {
            oracle.binding.OperationBinding opr=ADFUtils.findOperation("generateGstItemServiceCode");
            String obj=(String)opr.execute();
            Code=obj;
        }
        else
        {
         String NewCode=Code.substring(4);
             System.out.println("AFTER SUBSTRING************"+NewCode);
         if(NewCode !=null)
         {
        CodeNo=Integer.parseInt(NewCode);
        CodeNo=CodeNo+1;
        //PART000
        if(CodeNo>=1 && CodeNo<=9)
        NewCode="PART00"+CodeNo;
        else if(CodeNo>=10 && CodeNo<=99)
        NewCode="PART0"+CodeNo;                
        else if(CodeNo>=100)
        NewCode="PART"+CodeNo;
        Code=NewCode;
        Row rr=(Row) ADFUtils.evaluateEL("#{bindings.GstServiceItemMasterVO1Iterator.currentRow}");
        rr.setAttribute("ItemCd",NewCode);            
         }
        }
    }
    catch (Exception ee)
    {
    ee.printStackTrace();
    ADFUtils.showMessage("Problem in number generation.Please Try Again.",0);
    }
    }

    public void cancelButtonAL(ActionEvent actionEvent) 
    {
    ADFUtils.findOperation("Rollback").execute();
    Code="";
    }
}
