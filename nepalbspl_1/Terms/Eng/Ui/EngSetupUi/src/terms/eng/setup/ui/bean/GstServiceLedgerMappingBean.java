package terms.eng.setup.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

public class GstServiceLedgerMappingBean {
    private RichPanelHeader getMyPageRootComponent;
    private RichButton detailcreateBinding;
    private RichButton detaildeleteBinding;
    private RichInputDate validUptoBinding;
    private RichInputText igstRateBinding;
    private RichInputText sgstBinding;
//    private RichInputText cgstBinding;
    private RichInputText gstDescBinding;
    private RichInputText gstCodeBinding;
    private RichButton headerEditBinding;
    private RichOutputText bindingOutputText;
    private RichInputComboboxListOfValues unitCodeBinding;

    public GstServiceLedgerMappingBean() {
    }

    public void SaveButtonAL(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedByNew("GstRateMasterVO3Iterator","LastUpdatedBy");
        ADFUtils.showMessage("Record Updated Successfully.", 2);
    }
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
            try {
                Method method1 =
                    component.getClass().getMethod("setDisabled", boolean.class);
                if (method1 != null) {
                    method1.invoke(component, valueComponent);
                }
            } catch (NoSuchMethodException e) {
                try {
                    Method method =
                        component.getClass().getMethod("setReadOnly", boolean.class);
                    if (method != null) {
                        method.invoke(component, valueComponent);
                    }
                } catch (Exception e1) {
                    // e.printStackTrace();//silently eat this exception.
                }


            } catch (Exception e) {
                // e.printStackTrace();//silently eat this exception.
            }
            List<UIComponent> childComponents = component.getChildren();
            for (UIComponent comp : childComponents) {
                makeComponentHierarchyReadOnly(comp, valueComponent);
            }
        }
    
    
    private void cevmodecheck(){
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }
    
    public void cevModeDisableComponent(String mode) {
        
        if(mode.equals("V")) {
            
            getDetailcreateBinding().setDisabled((true));
            getDetaildeleteBinding().setDisabled(true);
            
        }
        if(mode.equals("E")){
            
            getGstCodeBinding().setDisabled(true);
            getGstDescBinding().setDisabled(true);
           // getCgstBinding().setDisabled(true);
            getSgstBinding().setDisabled(true);
            getIgstRateBinding().setDisabled(true);
            getValidUptoBinding().setDisabled(true);
            getDetailcreateBinding().setDisabled(false);          
            getDetaildeleteBinding().setDisabled(false);
            getHeaderEditBinding().setDisabled(true);
            //getUnitCodeBinding().setDisabled(true);           
        }
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setDetailcreateBinding(RichButton detailcreateBinding) {
        this.detailcreateBinding = detailcreateBinding;
    }

    public RichButton getDetailcreateBinding() {
        return detailcreateBinding;
    }

    public void setDetaildeleteBinding(RichButton detaildeleteBinding) {
        this.detaildeleteBinding = detaildeleteBinding;
    }

    public RichButton getDetaildeleteBinding() {
        return detaildeleteBinding;
    }

    public void setValidUptoBinding(RichInputDate validUptoBinding) {
        this.validUptoBinding = validUptoBinding;
    }

    public RichInputDate getValidUptoBinding() {
        return validUptoBinding;
    }

    public void setIgstRateBinding(RichInputText igstRateBinding) {
        this.igstRateBinding = igstRateBinding;
    }

    public RichInputText getIgstRateBinding() {
        return igstRateBinding;
    }

    public void setSgstBinding(RichInputText sgstBinding) {
        this.sgstBinding = sgstBinding;
    }

    public RichInputText getSgstBinding() {
        return sgstBinding;
    }

//    public void setCgstBinding(RichInputText cgstBinding) {
//        this.cgstBinding = cgstBinding;
//    }
//
//    public RichInputText getCgstBinding() {
//        return cgstBinding;
//    }

    public void setGstDescBinding(RichInputText gstDescBinding) {
        this.gstDescBinding = gstDescBinding;
    }

    public RichInputText getGstDescBinding() {
        return gstDescBinding;
    }

    public void setGstCodeBinding(RichInputText gstCodeBinding) {
        this.gstCodeBinding = gstCodeBinding;
    }

    public RichInputText getGstCodeBinding() {
        return gstCodeBinding;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void EditButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }
}
