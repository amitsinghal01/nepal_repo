package terms.eng.setup.ui.bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputFile;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectBooleanCheckbox;
import oracle.adf.view.rich.component.rich.input.RichSelectOneRadio;

import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import org.apache.myfaces.trinidad.model.UploadedFile;
import java.lang.reflect.Method;


import java.util.List;




public class GuageMasterBean {
    private RichInputText transCheckBinding;
    private RichInputFile uploadFileBind;
    private RichOutputText fileNamePath;
    private RichInputText bindGaugeCode;
    private RichTable gaugeParaTableBinding;
    private RichTable gaugeCalibTableBinding;
    private RichTable docAttachRefTableBinding;
    private RichInputDate issueDateBinding;
    private RichInputText revNoBinding;
    private RichInputDate lastChangeDateBinding;
    private RichInputDate calibrationDueDateBinding;
    private RichSelectBooleanCheckbox calibratedOutsideBinding;
    private RichInputText leadTimeForCalBinding;
    private RichInputText measurementUnitNameBinding;
    private RichInputText calibrationFreqBinding;
    private RichInputComboboxListOfValues lowerMeasureLimitBinding;
    private RichInputText snoBinding;
    private RichInputText lowerWearLimitBinding;
    private RichInputText upperMeasureLimitBinding;
    private RichInputText docSNoBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichOutputText bindingOutputText;
    private RichButton headerEditBinding;
    private RichButton measureParameterDetailcreateBinding;
    private RichButton calMasterDetailcreateBinding;
    private RichButton calMasterDetaildeleteBinding;
    private RichButton measureParameterDetaildeleteBinding;
    private RichButton saveBinding;
    private RichButton saveAndCloseBinding;
    private RichInputComboboxListOfValues issuedToUnitBinding;
    private RichInputComboboxListOfValues employeeBinding;
    private RichButton measureParameterDetailBinding;
    private RichButton calibrationMasterBinding;
    private RichButton attachmentBinding;

    public GuageMasterBean() {
    }

    public void setTransCheckBinding(RichInputText transCheckBinding) {
        this.transCheckBinding = transCheckBinding;
    }

    public RichInputText getTransCheckBinding() {
        return transCheckBinding;
    }

    public void setUploadFileBind(RichInputFile uploadFileBind) {
        this.uploadFileBind = uploadFileBind;
    }

    public RichInputFile getUploadFileBind() {
        return uploadFileBind;
    }
    
    InputStream inputStream = null;
    InputStream SaveInputStream = null;
    private UploadedFile PhotoFile = null;
    private String AbsoluteFilePath = "";
    String imagePath = "";
    Boolean check = false;

   

    public void uploadFileVCE(ValueChangeEvent vce) {
        if (vce.getNewValue() != null) {
            setPhotoFile((UploadedFile) vce.getNewValue());
            UploadedFile myfile = (UploadedFile) vce.getNewValue();
            String path = null;

            if (myfile == null) {

            } else {
                File dir= new File("/tmp/pht");
                System.out.println("In Desktop pics folder");
                File savedir= new File("/home/beta2/pics");
                if(!dir.exists()){
                    try{
                        dir.mkdir();
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }
                }
                if(!savedir.exists()){
                    try{
                        savedir.mkdir();
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }
                }
                
                // All uploaded files will be stored in below path
                path = "/home/beta2/pics/" + myfile.getFilename();
                //                Imagepath=path;
                File f = new File(path);
                if (!f.exists()) {
                    try {
                        f.createNewFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                inputStream = null;
                try {

                    inputStream = myfile.getInputStream();
                    SaveInputStream = myfile.getInputStream();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                check = true;
        //                System.out.println("check in bean" + check);
                Imagepath = uploadFile(PhotoFile, "/tmp/pht/" + PhotoFile.getFilename());

            }
        }
    }
    
    

    private String uploadFile(UploadedFile file, String path) {
        UploadedFile myfile = file;

        try {

            FileOutputStream out = new FileOutputStream(path);
            byte[] buffer = new byte[8192];
            int bytesRead = 0;
            while ((bytesRead = inputStream.read(buffer, 0, 8192)) != -1) {
                out.write(buffer, 0, bytesRead);
            }
            out.flush();
            out.close();
        } catch (Exception ex) {
            // handle exception
            ex.printStackTrace();
        } finally{
            try {
                inputStream.close();
            } catch (IOException e) {
            }
        }


        //Returns the path where file is stored
        return path;
    }
    
    private String SaveuploadFile(UploadedFile file, String path) {

        UploadedFile myfile = file;

        try {

            FileOutputStream out = new FileOutputStream(path);
            byte[] buffer = new byte[8192];
            int bytesRead = 0;
            while ((bytesRead = SaveInputStream.read(buffer, 0, 8192)) != -1) {
                out.write(buffer, 0, bytesRead);
            }
            out.flush();
            out.close();
        } catch (Exception ex) {
            // handle exception
            ex.printStackTrace();
        } finally{
            try {
                SaveInputStream.close();
            } catch (IOException e) {
            }
        }
        return path;
        }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getImagePath() {
        return imagePath;
    }
    
    String Imagepath = "";


    public void saveGaugemasterAL(ActionEvent actionEvent) {
        
        ADFUtils.setLastUpdatedByNew("GaugeMasterVO1Iterator","LastUpdatedBy");
            ADFUtils.findOperation("generateGaugeCode").execute();
            check = false;
            String path;
            if(getPhotoFile()!= null){
                path = "/home/beta2/pics/" + PhotoFile.getFilename();
                Imagepath = SaveuploadFile(PhotoFile, path);
                //        System.out.println("path " + Imagepath);
                OperationBinding ob = ADFUtils.findOperation("AddImagePathGauge");
                ob.getParamsMap().put("ImagePath", Imagepath);
                ob.execute();    
            
            
            
            File directory = new File("/tmp/pht");
                    //get all the files from a directory
                    File[] fList = directory.listFiles();
                    if(fList !=null){
                    for (File file : fList) {
                        //Delete all previously uploaded files
                       // if (!"NoImage.png".equalsIgnoreCase(file.getName())) {
                            file.delete();
                        //}

                    }
                    }
        
        ADFUtils.findOperation("Commit").execute();
        }
            
            
           
        if(bindGaugeCode.isDisabled())
        {
            System.out.println("Edit Mode"+bindGaugeCode.isDisabled());
            FacesMessage Message = new FacesMessage("Record Update Successfully.");  
            Message.setSeverity(FacesMessage.SEVERITY_INFO);  
            FacesContext fc = FacesContext.getCurrentInstance();  
            fc.addMessage(null, Message); 
            
        }            
        else {
                System.out.println("Create Mode"+bindGaugeCode.isDisabled());
                FacesMessage Message = new FacesMessage("Record Save Successfully.");  
                Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                FacesContext fc = FacesContext.getCurrentInstance();  
                fc.addMessage(null, Message); 
                
            
            }  
            
            
            
            
    }

    public void setCheck(Boolean check) {
        this.check = check;
    }

    public Boolean getCheck() {
        return check;
    }

    public void setPhotoFile(UploadedFile PhotoFile) {
        this.PhotoFile = PhotoFile;
    }

    public UploadedFile getPhotoFile() {
        return PhotoFile;
    }
    
    public void downloadDocment(FacesContext facesContext, OutputStream outputStream) {
        //Read file from particular path, path bind is binding of table field that contains path
              File filed = new File(fileNamePath.getValue().toString());
              FileInputStream fis;
              byte[] b;
              try {
                  fis = new FileInputStream(filed);

                  int n;
                  while ((n = fis.available()) > 0) {
                      b = new byte[n];
                      int result = fis.read(b);
                      outputStream.write(b, 0, b.length);
                      if (result == -1)
                          break;
                  }
              } catch (IOException e) {
                  e.printStackTrace();
              }

    }


    public void setFileNamePath(RichOutputText fileNamePath) {
        this.fileNamePath = fileNamePath;
    }

    public RichOutputText getFileNamePath() {
        return fileNamePath;
    }

    public void createInsertInDetailAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert").execute();


                ADFUtils.findOperation("getSequenceParaIdGauge").execute();
        
    }

    public void docFileCreateAL(ActionEvent actionEvent) {
        // Add event code here...
        ADFUtils.findOperation("CreateInsert2").execute();


                ADFUtils.findOperation("getDocSequenceGaugePara").execute();
    }

    public void igFlagVCL(ValueChangeEvent valueChangeEvent) {
        
        transCheckBinding.setValue(valueChangeEvent);
    }

    public void setBindGaugeCode(RichInputText bindGaugeCode) {
        this.bindGaugeCode = bindGaugeCode;
    }

    public RichInputText getBindGaugeCode() {
        return bindGaugeCode;
    }



    public void gaugeParadeletePopupDialogDL(DialogEvent dialogEvent) 
    {
    
        if(dialogEvent.getOutcome().name().equals("ok"))
            {
                ADFUtils.findOperation("Delete").execute();
                System.out.println("Record Delete Successfully");
            
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");   
                Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                FacesContext fc = FacesContext.getCurrentInstance();   
                fc.addMessage(null, Message);   
            }
         AdfFacesContext.getCurrentInstance().addPartialTarget(gaugeParaTableBinding);
 
    }

    public void setGaugeParaTableBinding(RichTable gaugeParaTableBinding) {
        this.gaugeParaTableBinding = gaugeParaTableBinding;
    }

    public RichTable getGaugeParaTableBinding() {
        return gaugeParaTableBinding;
    }




    public void gaugeCalibDeletePopupDialogDL(DialogEvent dialogEvent) 
    {
        if(dialogEvent.getOutcome().name().equals("ok"))
            {
                ADFUtils.findOperation("Delete1").execute();
                System.out.println("Record Delete Successfully");
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");   
                Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                FacesContext fc = FacesContext.getCurrentInstance();   
                fc.addMessage(null, Message);   
            }
         AdfFacesContext.getCurrentInstance().addPartialTarget(gaugeCalibTableBinding);

    }

    public void setGaugeCalibTableBinding(RichTable gaugeCalibTableBinding) {
        this.gaugeCalibTableBinding = gaugeCalibTableBinding;
    }

    public RichTable getGaugeCalibTableBinding() {
        return gaugeCalibTableBinding;
    }



    public void docAttachRefDeletePopupDialogDL(DialogEvent dialogEvent) 
    {
            if(dialogEvent.getOutcome().name().equals("ok"))
            {
                ADFUtils.findOperation("Delete2").execute();
                System.out.println("Record Delete Successfully");
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");   
                Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                FacesContext fc = FacesContext.getCurrentInstance();   
                fc.addMessage(null, Message);   
            }
         AdfFacesContext.getCurrentInstance().addPartialTarget(docAttachRefTableBinding);
        
    }

    public void setDocAttachRefTableBinding(RichTable docAttachRefTableBinding) {
        this.docAttachRefTableBinding = docAttachRefTableBinding;
    }

    public RichTable getDocAttachRefTableBinding() {
        return docAttachRefTableBinding;
    }

    public void setIssueDateBinding(RichInputDate issueDateBinding) {
        this.issueDateBinding = issueDateBinding;
    }

    public RichInputDate getIssueDateBinding() {
        return issueDateBinding;
    }

    public void setRevNoBinding(RichInputText revNoBinding) {
        this.revNoBinding = revNoBinding;
    }

    public RichInputText getRevNoBinding() {
        return revNoBinding;
    }

    public void setLastChangeDateBinding(RichInputDate lastChangeDateBinding) {
        this.lastChangeDateBinding = lastChangeDateBinding;
    }

    public RichInputDate getLastChangeDateBinding() {
        return lastChangeDateBinding;
    }

    public void setCalibrationDueDateBinding(RichInputDate calibrationDueDateBinding) {
        this.calibrationDueDateBinding = calibrationDueDateBinding;
    }

    public RichInputDate getCalibrationDueDateBinding() {
        return calibrationDueDateBinding;
    }

    public void setCalibratedOutsideBinding(RichSelectBooleanCheckbox calibratedOutsideBinding) {
        this.calibratedOutsideBinding = calibratedOutsideBinding;
    }

    public RichSelectBooleanCheckbox getCalibratedOutsideBinding() {
        return calibratedOutsideBinding;
    }

    public void setLeadTimeForCalBinding(RichInputText leadTimeForCalBinding) {
        this.leadTimeForCalBinding = leadTimeForCalBinding;
    }

    public RichInputText getLeadTimeForCalBinding() {
        return leadTimeForCalBinding;
    }

    public void setMeasurementUnitNameBinding(RichInputText measurementUnitNameBinding) {
        this.measurementUnitNameBinding = measurementUnitNameBinding;
    }

    public RichInputText getMeasurementUnitNameBinding() {
        return measurementUnitNameBinding;
    }
    

    public void setCalibrationFreqBinding(RichInputText calibrationFreqBinding) {
        this.calibrationFreqBinding = calibrationFreqBinding;
    }

    public RichInputText getCalibrationFreqBinding() {
        return calibrationFreqBinding;
    }

    public void setLowerMeasureLimitBinding(RichInputComboboxListOfValues lowerMeasureLimitBinding) {
        this.lowerMeasureLimitBinding = lowerMeasureLimitBinding;
    }

    public RichInputComboboxListOfValues getLowerMeasureLimitBinding() {
        return lowerMeasureLimitBinding;
    }

    public void setSnoBinding(RichInputText snoBinding) {
        this.snoBinding = snoBinding;
    }

    public RichInputText getSnoBinding() {
        return snoBinding;
    }

    public void setLowerWearLimitBinding(RichInputText lowerWearLimitBinding) {
        this.lowerWearLimitBinding = lowerWearLimitBinding;
    }

    public RichInputText getLowerWearLimitBinding() {
        return lowerWearLimitBinding;
    }

    public void setUpperMeasureLimitBinding(RichInputText upperMeasureLimitBinding) {
        this.upperMeasureLimitBinding = upperMeasureLimitBinding;
    }

    public RichInputText getUpperMeasureLimitBinding() {
        return upperMeasureLimitBinding;
    }

    public void setDocSNoBinding(RichInputText docSNoBinding) {
        this.docSNoBinding = docSNoBinding;
    }

    public RichInputText getDocSNoBinding() {
        return docSNoBinding;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
        
        
    }
    
   

   

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setMeasureParameterDetailcreateBinding(RichButton measureParameterDetailcreateBinding) {
        this.measureParameterDetailcreateBinding = measureParameterDetailcreateBinding;
    }

    public RichButton getMeasureParameterDetailcreateBinding() {
        return measureParameterDetailcreateBinding;
    }

    public void setCalMasterDetailcreateBinding(RichButton calMasterDetailcreateBinding) {
        this.calMasterDetailcreateBinding = calMasterDetailcreateBinding;
    }

    public RichButton getCalMasterDetailcreateBinding() {
        return calMasterDetailcreateBinding;
    }

    public void setCalMasterDetaildeleteBinding(RichButton calMasterDetaildeleteBinding) {
        this.calMasterDetaildeleteBinding = calMasterDetaildeleteBinding;
    }

    public RichButton getCalMasterDetaildeleteBinding() {
        return calMasterDetaildeleteBinding;
    }

    public void setMeasureParameterDetaildeleteBinding(RichButton measureParameterDetaildeleteBinding) {
        this.measureParameterDetaildeleteBinding = measureParameterDetaildeleteBinding;
    }

    public RichButton getMeasureParameterDetaildeleteBinding() {
        return measureParameterDetaildeleteBinding;
    }
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                try {
                    Method method1 =
                        component.getClass().getMethod("setDisabled", boolean.class);
                    if (method1 != null) {
                        method1.invoke(component, valueComponent);
                    }
                } catch (NoSuchMethodException e) {
                    try {
                        Method method =
                            component.getClass().getMethod("setReadOnly", boolean.class);
                        if (method != null) {
                            method.invoke(component, valueComponent);
                        }
                    } catch (Exception e1) {
                        // e.printStackTrace();//silently eat this exception.
                    }


                } catch (Exception e) {
                    // e.printStackTrace();//silently eat this exception.
                }
                List<UIComponent> childComponents = component.getChildren();
                for (UIComponent comp : childComponents) {
                    makeComponentHierarchyReadOnly(comp, valueComponent);
                }
            }
    
    public RichOutputText getBindingOutputText() {
        
        cevmodecheck();
        return bindingOutputText;
    }
    
    
    
    public void EditButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }
    
   
    private void cevmodecheck(){
           if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
               cevModeDisableComponent("V");
           }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
               cevModeDisableComponent("C");
           }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
               cevModeDisableComponent("E");
           }
       }
    
    public void cevModeDisableComponent(String mode) {
    //            FacesContext fctx = FacesContext.getCurrentInstance();
    //            ELContext elctx = fctx.getELContext();
    //            Application jsfApp = fctx.getApplication();
    //            //create a ValueExpression that points to the ADF binding layer
    //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
    //            //
    //            ValueExpression valueExpr = exprFactory.createValueExpression(
    //                                         elctx,
    //                                         "#{pageFlowScope.mode=='E'}",
    //                                          Object.class
    //                                         ); 
    //    //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
           if (mode.equals("E")) {
                    getCalibrationDueDateBinding().setDisabled(true);
                    getLeadTimeForCalBinding().setDisabled(true);
                    getCalibrationFreqBinding().setDisabled(true);
                    getCalibratedOutsideBinding().setDisabled(true);
                    getRevNoBinding().setDisabled(true);
                    getBindGaugeCode().setDisabled(true);
                    getHeaderEditBinding().setDisabled(true);
                    getLastChangeDateBinding().setDisabled(true);
                    getIssuedToUnitBinding().setDisabled(false);
                    getEmployeeBinding().setDisabled(false);
                  //  getCalMasterDetailcreateBinding().setDisabled(false);
                  //  getMeasureParameterDetailcreateBinding().setDisabled(false);
                   // getCalMasterDetaildeleteBinding().setDisabled(false);
                  //  getMeasureParameterDetaildeleteBinding().setDisabled(false);
            } 
           
           else if (mode.equals("C")) {
                
              //  getCalMasterDetailcreateBinding().setDisabled(false);
                getIssueDateBinding().setDisabled(true);
                getLastChangeDateBinding().setDisabled(true);
                getHeaderEditBinding().setDisabled(true);
                getRevNoBinding().setDisabled(true);
                getIssuedToUnitBinding().setDisabled(false);
                getEmployeeBinding().setDisabled(false);
                
               // getLowerMeasureLimitBinding().setDisabled(true);
              //  getSnoBinding().setDisabled(true);
               // getLowerWearLimitBinding().setDisabled(true);
              //  getUpperMeasureLimitBinding().setDisabled(true);
              //  getMeasureParameterDetailcreateBinding().setDisabled(false);
                //getCalMasterDetaildeleteBinding().setDisabled(false);
               // getMeasureParameterDetaildeleteBinding().setDisabled(false);
            } 
           
           else if (mode.equals("V")) {
               
         //       getCalMasterDetailcreateBinding().setDisabled(true);
              //  getCalMasterDetaildeleteBinding().setDisabled(true);
                //getMeasureParameterDetailcreateBinding().setDisabled(true);
               // getMeasureParameterDetaildeleteBinding().setDisabled(true);
               getAttachmentBinding().setDisabled(false);
               getCalibrationMasterBinding().setDisabled(false);
               getMeasureParameterDetailBinding().setDisabled(false);
                
            }
        }


    public void setSaveBinding(RichButton saveBinding) {
        this.saveBinding = saveBinding;
    }

    public RichButton getSaveBinding() {
        return saveBinding;
    }

    public void setSaveAndCloseBinding(RichButton saveAndCloseBinding) {
        this.saveAndCloseBinding = saveAndCloseBinding;
    }

    public RichButton getSaveAndCloseBinding() {
        return saveAndCloseBinding;
    }

    public void setIssuedToUnitBinding(RichInputComboboxListOfValues issuedToUnitBinding) {
        this.issuedToUnitBinding = issuedToUnitBinding;
    }

    public RichInputComboboxListOfValues getIssuedToUnitBinding() {
        return issuedToUnitBinding;
    }

    public void setEmployeeBinding(RichInputComboboxListOfValues employeeBinding) {
        this.employeeBinding = employeeBinding;
    }

    public RichInputComboboxListOfValues getEmployeeBinding() {
        return employeeBinding;
    }

    public void setMeasureParameterDetailBinding(RichButton measureParameterDetailBinding) {
        this.measureParameterDetailBinding = measureParameterDetailBinding;
    }

    public RichButton getMeasureParameterDetailBinding() {
        return measureParameterDetailBinding;
    }

    public void setCalibrationMasterBinding(RichButton calibrationMasterBinding) {
        this.calibrationMasterBinding = calibrationMasterBinding;
    }

    public RichButton getCalibrationMasterBinding() {
        return calibrationMasterBinding;
    }

    public void setAttachmentBinding(RichButton attachmentBinding) {
        this.attachmentBinding = attachmentBinding;
    }

    public RichButton getAttachmentBinding() {
        return attachmentBinding;
    }
}
