package terms.eng.setup.ui.bean;

import javax.faces.application.FacesMessage;

import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

public class HsnMasterBean {
    private RichTable inlineHsnMasterTableBinding;
    private String editAction="V";
    public HsnMasterBean() {
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
            {
            if(dialogEvent.getOutcome().name().equals("ok"))
            {
            OperationBinding op = null;
            ADFUtils.findOperation("Delete").execute();
            op = (OperationBinding) ADFUtils.findOperation("Commit");
            if (getEditAction().equals("V")) {
            Object rst = op.execute();
            }
            System.out.println("Record Delete Successfully");
            if(op.getErrors().isEmpty()){
            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
            } else if (!op.getErrors().isEmpty())
            {
            OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
            Object rstr = opr.execute();
            FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
           
            Message.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
            }
            }
                AdfFacesContext.getCurrentInstance().addPartialTarget(inlineHsnMasterTableBinding);
    }
    }

    public void setInlineHsnMasterTableBinding(RichTable inlineHsnMasterTableBinding) {
        this.inlineHsnMasterTableBinding = inlineHsnMasterTableBinding;
    }

    public RichTable getInlineHsnMasterTableBinding() {
        return inlineHsnMasterTableBinding;
    }

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public void saveButtonAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("getHsnNo").execute();
                ADFUtils.findOperation("Commit").execute();
    }
}
