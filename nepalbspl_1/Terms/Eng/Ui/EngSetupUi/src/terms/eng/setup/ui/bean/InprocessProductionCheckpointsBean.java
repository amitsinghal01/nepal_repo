package terms.eng.setup.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.validator.ValidatorException;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class InprocessProductionCheckpointsBean {
    private RichTable inProcessProductionTableBinding;
    private RichInputComboboxListOfValues partCodeBinding;
    private RichInputText processSeqNoBinding;
    private RichInputText seqRevNoBinding;
    private RichInputText processCodeBinding;
    private RichInputComboboxListOfValues partCodeCopyBinding;
    private RichInputText partRevTransBinding;
    private RichInputText procCodeCopyBinding;
    private RichInputText procSeqRevNoTransBinding;
    private RichInputText procSeqRevnoCopyBinding;
    private RichInputText chkptSeqNoBinding;
    private RichOutputText bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichButton detailcreateBinding;
    private RichButton detaildeleteBinding;
    private RichButton saveBinding;
    private RichButton saveAndCloseBinding;
    private RichInputText partDescTransient;
    private RichInputText processDescTransient;
    private RichButton copyButtonBinding;
    private RichButton saveButtonBinding;
    private RichButton limitButtonBinding;
    private RichInputText baseLimitBinding;
    private RichInputText upperLimitBinding;
    private RichInputText lowerLimitBinding;

    public InprocessProductionCheckpointsBean() {
    }

    public void populateCopyValueAL(ActionEvent actionEvent) 
    {
        if(partCodeCopyBinding.getValue() != null)
        {
        OperationBinding op1 = ADFUtils.findOperation("populateDataInprocessCheckpoints");
                           Object Obj = op1.execute();
                           System.out.println("Result is===> "+op1.getResult());
                           if(op1.getResult()!=null && op1.getResult().toString().equalsIgnoreCase("N"))
                           {
                                FacesMessage Message = new FacesMessage("Record not found for this entry.");   
                                Message.setSeverity(FacesMessage.SEVERITY_ERROR);   
                                FacesContext fc = FacesContext.getCurrentInstance();   
                                fc.addMessage(null, Message);    
                               
                           }
                           getPartCodeBinding().setDisabled(true);
                           getProcessCodeBinding().setDisabled(true);
                           getDetailcreateBinding().setDisabled(true);
        }
                           else
                           {
                                ADFUtils.showMessage("Please Select Part Code Copy Value.",0);   
                           }
                           

         
        
        //ADFUtils.findOperation("populateDataInprocessCheckpoints").execute();
    }

    public void saveButtonAL(ActionEvent actionEvent)
    {
        ADFUtils.setLastUpdatedByNew("InprocessProductionCheckpointsHeaderVO1Iterator","LastUpdatedBy");
//    ADFUtils.findOperation("setCheckPointsSequnceNumber").execute();
        Integer i=0;
        i=(Integer)getBindingOutputText().getValue();
        System.out.println("EDIT TRANS VALUE"+i);
//        String param=resolvEl("#{pageFlowScope.mode=='E'}"); 
//        System.out.println("Mode is ====> "+param);
        if(i.equals(0))
        {
           FacesMessage Message = new FacesMessage("Record Save Successfully");   
           Message.setSeverity(FacesMessage.SEVERITY_INFO);   
           FacesContext fc = FacesContext.getCurrentInstance();   
           fc.addMessage(null, Message);      
            
        }
        else 
        {
            FacesMessage Message = new FacesMessage("Record Updated Successfully");   
            Message.setSeverity(FacesMessage.SEVERITY_INFO);   
            FacesContext fc = FacesContext.getCurrentInstance();   
            fc.addMessage(null, Message);      
        }
        


    }

    public void deletePopupDialogDL(DialogEvent dialogEvent)
    {
        if(dialogEvent.getOutcome().name().equals("ok"))
                    {
                    ADFUtils.findOperation("Delete").execute();
                    //ADFUtils.findOperation("Commit").execute();    //Unable on Search page.
                    System.out.println("Record Delete Successfully");
                   
                        FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                        FacesContext fc = FacesContext.getCurrentInstance();  
                        fc.addMessage(null, Message);  
                   
                    }

                AdfFacesContext.getCurrentInstance().addPartialTarget(inProcessProductionTableBinding);
    }

    public void setInProcessProductionTableBinding(RichTable inProcessProductionTableBinding) {
        this.inProcessProductionTableBinding = inProcessProductionTableBinding;
    }

    public RichTable getInProcessProductionTableBinding() {
        return inProcessProductionTableBinding;
    }
    
    public String resolvEl(String data)
    {
                      FacesContext fc = FacesContext.getCurrentInstance();
                      Application app = fc.getApplication();
                      ExpressionFactory elFactory = app.getExpressionFactory();
                      ELContext elContext = fc.getELContext();
                      ValueExpression valueExp = elFactory.createValueExpression(elContext, data, Object.class);
                      String Message=valueExp.getValue(elContext).toString();
                      return Message;
    }

    public void setPartCodeBinding(RichInputComboboxListOfValues partCodeBinding) {
        this.partCodeBinding = partCodeBinding;
    }

    public RichInputComboboxListOfValues getPartCodeBinding() {
        return partCodeBinding;
    }

    public void setProcessSeqNoBinding(RichInputText processSeqNoBinding) {
        this.processSeqNoBinding = processSeqNoBinding;
    }

    public RichInputText getProcessSeqNoBinding() {
        return processSeqNoBinding;
    }

    public void setSeqRevNoBinding(RichInputText seqRevNoBinding) {
        this.seqRevNoBinding = seqRevNoBinding;
    }

    public RichInputText getSeqRevNoBinding() {
        return seqRevNoBinding;
    }

    public void setProcessCodeBinding(RichInputText processCodeBinding) {
        this.processCodeBinding = processCodeBinding;
    }

    public RichInputText getProcessCodeBinding() {
        return processCodeBinding;
    }

    public void setPartCodeCopyBinding(RichInputComboboxListOfValues partCodeCopyBinding) {
        this.partCodeCopyBinding = partCodeCopyBinding;
    }

    public RichInputComboboxListOfValues getPartCodeCopyBinding() {
        return partCodeCopyBinding;
    }

    public void setPartRevTransBinding(RichInputText partRevTransBinding) {
        this.partRevTransBinding = partRevTransBinding;
    }

    public RichInputText getPartRevTransBinding() {
        return partRevTransBinding;
    }

    public void setProcCodeCopyBinding(RichInputText procCodeCopyBinding) {
        this.procCodeCopyBinding = procCodeCopyBinding;
    }

    public RichInputText getProcCodeCopyBinding() {
        return procCodeCopyBinding;
    }

    public void setProcSeqRevNoTransBinding(RichInputText procSeqRevNoTransBinding) {
        this.procSeqRevNoTransBinding = procSeqRevNoTransBinding;
    }

    public RichInputText getProcSeqRevNoTransBinding() {
        return procSeqRevNoTransBinding;
    }

    public void setProcSeqRevnoCopyBinding(RichInputText procSeqRevnoCopyBinding) {
        this.procSeqRevnoCopyBinding = procSeqRevnoCopyBinding;
    }

    public RichInputText getProcSeqRevnoCopyBinding() {
        return procSeqRevnoCopyBinding;
    }

    public void setChkptSeqNoBinding(RichInputText chkptSeqNoBinding) {
        this.chkptSeqNoBinding = chkptSeqNoBinding;
    }

    public RichInputText getChkptSeqNoBinding() {
        return chkptSeqNoBinding;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }
    

    public void EditButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }
    
    private void cevmodecheck(){
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }


    private void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 =
                component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method =
                    component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }

    private void cevModeDisableComponent(String mode) {
        if (mode.equals("E")) {
                  getPartCodeBinding().setDisabled(true);
                  getPartRevTransBinding().setDisabled(true);
                  getProcessSeqNoBinding().setDisabled(true);
                  getSeqRevNoBinding().setDisabled(true);
                  getProcessCodeBinding().setDisabled(true);
                  getPartCodeCopyBinding().setDisabled(true);
                  getProcSeqRevNoTransBinding().setDisabled(true);
                  getProcCodeCopyBinding().setDisabled(true);
                  getChkptSeqNoBinding().setDisabled(true);
                  getPartDescTransient().setDisabled(true);
                  getProcessDescTransient().setDisabled(true);
                  getHeaderEditBinding().setDisabled(true);
                  getDetailcreateBinding().setDisabled(false);
                  getDetaildeleteBinding().setDisabled(false);
                  getProcSeqRevnoCopyBinding().setDisabled(true);
                  getCopyButtonBinding().setDisabled(true);
                  
        } else if (mode.equals("C")) {
              getPartRevTransBinding().setDisabled(true);
              getProcessSeqNoBinding().setDisabled(true);
              getSeqRevNoBinding().setDisabled(true);
              getProcessCodeBinding().setDisabled(true);
              getProcSeqRevNoTransBinding().setDisabled(true);
              getProcCodeCopyBinding().setDisabled(true);
              getChkptSeqNoBinding().setDisabled(true);
              getPartDescTransient().setDisabled(true);
              getProcessDescTransient().setDisabled(true);
              getHeaderEditBinding().setDisabled(true);
              getDetailcreateBinding().setDisabled(false);
            getProcSeqRevnoCopyBinding().setDisabled(true);

              
        } else if (mode.equals("V")) {
            getPartDescTransient().setDisabled(true);
            getLimitButtonBinding().setDisabled(false);
            getCopyButtonBinding().setDisabled(true);
            getPartCodeCopyBinding().setDisabled(true);
            getDetailcreateBinding().setDisabled(true);
            getDetaildeleteBinding().setDisabled(true);
         
        }
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setDetailcreateBinding(RichButton detailcreateBinding) {
        this.detailcreateBinding = detailcreateBinding;
    }

    public RichButton getDetailcreateBinding() {
        return detailcreateBinding;
    }

    public void setDetaildeleteBinding(RichButton detaildeleteBinding) {
        this.detaildeleteBinding = detaildeleteBinding;
    }

    public RichButton getDetaildeleteBinding() {
        return detaildeleteBinding;
    }

    public void setSaveBinding(RichButton saveBinding) {
        this.saveBinding = saveBinding;
    }

    public RichButton getSaveBinding() {
        return saveBinding;
    }

    public void setSaveAndCloseBinding(RichButton saveAndCloseBinding) {
        this.saveAndCloseBinding = saveAndCloseBinding;
    }

    public RichButton getSaveAndCloseBinding() {
        return saveAndCloseBinding;
    }

    public void setPartDescTransient(RichInputText partDescTransient) {
        this.partDescTransient = partDescTransient;
    }

    public RichInputText getPartDescTransient() {
        return partDescTransient;
    }

    public void setProcessDescTransient(RichInputText processDescTransient) {
        this.processDescTransient = processDescTransient;
    }

    public RichInputText getProcessDescTransient() {
        return processDescTransient;
    }

    public void setCopyButtonBinding(RichButton copyButtonBinding) {
        this.copyButtonBinding = copyButtonBinding;
    }

    public RichButton getCopyButtonBinding() {
        return copyButtonBinding;
    }

    public void setSaveButtonBinding(RichButton saveButtonBinding) {
        this.saveButtonBinding = saveButtonBinding;
    }

    public RichButton getSaveButtonBinding() {
        return saveButtonBinding;
    }

    public void createDetailButtonAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert").execute();
        getPartCodeBinding().setDisabled(true);
        getProcessCodeBinding().setDisabled(true);
        chkptSeqNoBinding.setValue(ADFUtils.evaluateEL("#{bindings.InprocessProductionCheckpointsDetailVO1Iterator.estimatedRowCount}"));

    }

    public void lowerLimitValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if(object !=null && baseLimitBinding.getValue() !=null)
        {
            BigDecimal baselimit = (BigDecimal)baseLimitBinding.getValue();
            BigDecimal lowerlimit = (BigDecimal) object;
            
            if(lowerlimit.compareTo(baselimit)!=-1)
            {
                 throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Lower Limit should be less than to Base Limit.", null));
             }
            
        }

    }

    public void setLimitButtonBinding(RichButton limitButtonBinding) {
        this.limitButtonBinding = limitButtonBinding;
    }

    public RichButton getLimitButtonBinding() {
        return limitButtonBinding;
    }

    public void setBaseLimitBinding(RichInputText baseLimitBinding) {
        this.baseLimitBinding = baseLimitBinding;
    }

    public RichInputText getBaseLimitBinding() {
        return baseLimitBinding;
    }

    public void setUpperLimitBinding(RichInputText upperLimitBinding) {
        this.upperLimitBinding = upperLimitBinding;
    }

    public RichInputText getUpperLimitBinding() {
        return upperLimitBinding;
    }

    public void setLowerLimitBinding(RichInputText lowerLimitBinding) {
        this.lowerLimitBinding = lowerLimitBinding;
    }

    public RichInputText getLowerLimitBinding() {
        return lowerLimitBinding;
    }

    public void upperLimitValidatorr(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if(object !=null && baseLimitBinding.getValue() !=null)
        {
            BigDecimal baselimit = (BigDecimal)baseLimitBinding.getValue();
            BigDecimal upperlimit = (BigDecimal) object;
            
            if(upperlimit.compareTo(baselimit)!=1)
            {
                 throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Upper Limit should be greater than to Base Limit.", null));
             }
            
        }

    }
}
