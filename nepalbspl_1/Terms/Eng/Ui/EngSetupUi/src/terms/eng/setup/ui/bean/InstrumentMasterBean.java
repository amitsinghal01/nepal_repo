package terms.eng.setup.ui.bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.sql.SQLException;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectBooleanCheckbox;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.nav.RichCommandLink;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.adf.view.rich.util.ResetUtils;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;

import org.apache.myfaces.trinidad.model.UploadedFile;

import terms.eng.setup.model.view.InstrumentCalibrationParameterVORowImpl;

public class InstrumentMasterBean {
    private RichTable calibrationMasterTableBinding;
    private RichTable calibrationParameterTableBinding;
    private RichTable accessoryTableBinding;
    private RichInputText bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton calibrationMasterButtonBinding;
    private RichButton calibrationParameterButtonBinding;
    private RichButton accessoryButtonBinding;
    private RichButton headerEditBinding;
    private RichInputText instrumentCdBinding;
    private RichButton externalButtonBinding;
    private RichInputComboboxListOfValues agency1Binding;
    private RichInputComboboxListOfValues agency2Binding;
    private RichSelectBooleanCheckbox inOutBinding;
    private RichInputText chkSeqBinding;
    private RichSelectOneChoice freqTypeBinding;
    private RichInputText freqBinding;
    private RichInputComboboxListOfValues issueToUnitBinding;
    private RichShowDetailItem referenceDocumentTabBinding;
    private RichInputText docSlNoBinding;
    private RichInputText unitCdRefDocBinding;
    private RichInputText refDocNoBinding;
    private RichInputText refDocTypeBinding;
    private RichInputText docFileNameBinding;
    private RichInputDate refDocDateBinding;
    private RichInputText pathBinding;
    private RichInputText contentTypeBinding;
    private RichTable referenceDocumentTableBinding;
    private RichShowDetailItem instrumentMasterTabBinding;
    private RichCommandLink downloadLinkBinding;
    private RichSelectOneChoice instrumentTypeBinding;
    private RichInputDate calibDueDateBinding;

    public InstrumentMasterBean() {
    }

    public void deletepopupDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(calibrationMasterTableBinding);
    }

    public void deletepopupDialog1DL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete1").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(calibrationParameterTableBinding);
    }

    public void deletepopupDialog2DL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete2").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(accessoryTableBinding);
    }

    public void setCalibrationMasterTableBinding(RichTable calibrationMasterTableBinding) {
        this.calibrationMasterTableBinding = calibrationMasterTableBinding;
    }

    public RichTable getCalibrationMasterTableBinding() {
        return calibrationMasterTableBinding;
    }

    public void setCalibrationParameterTableBinding(RichTable calibrationParameterTableBinding) {
        this.calibrationParameterTableBinding = calibrationParameterTableBinding;
    }

    public RichTable getCalibrationParameterTableBinding() {
        return calibrationParameterTableBinding;
    }

    public void setAccessoryTableBinding(RichTable accessoryTableBinding) {
        this.accessoryTableBinding = accessoryTableBinding;
    }

    public RichTable getAccessoryTableBinding() {
        return accessoryTableBinding;
    }

    public void setBindingOutputText(RichInputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichInputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }
   
    private void cevmodecheck(){
           if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
               cevModeDisableComponent("V");
           }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
               cevModeDisableComponent("C");
           }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
               cevModeDisableComponent("E");
           }
       }
    
    

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
            try {
                Method method1 =
                    component.getClass().getMethod("setDisabled", boolean.class);
                if (method1 != null) {
                    method1.invoke(component, valueComponent);
                }
            } catch (NoSuchMethodException e) {
                try {
                    Method method =
                        component.getClass().getMethod("setReadOnly", boolean.class);
                    if (method != null) {
                        method.invoke(component, valueComponent);
                    }
                } catch (Exception e1) {
                    // e.printStackTrace();//silently eat this exception.
                }


            } catch (Exception e) {
                // e.printStackTrace();//silently eat this exception.
            }
            List<UIComponent> childComponents = component.getChildren();
            for (UIComponent comp : childComponents) {
                makeComponentHierarchyReadOnly(comp, valueComponent);
            }
        }
    
    
    
    //Set Fields and Button disable.
        public void cevModeDisableComponent(String mode) {
        //            FacesContext fctx = FacesContext.getCurrentInstance();
        //            ELContext elctx = fctx.getELContext();
        //            Application jsfApp = fctx.getApplication();
        //            //create a ValueExpression that points to the ADF binding layer
        //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
        //            //
        //            ValueExpression valueExpr = exprFactory.createValueExpression(
        //                                         elctx,
        //                                         "#{pageFlowScope.mode=='E'}",
        //                                          Object.class
        //                                         );
        //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
            if (mode.equals("E")) {
                getHeaderEditBinding().setDisabled(true);
                instrumentCdBinding.setDisabled(true);
                getChkSeqBinding().setDisabled(true);
                getDocSlNoBinding().setDisabled(true);
                getUnitCdRefDocBinding().setDisabled(true);
                getRefDocNoBinding().setDisabled(true);
                getRefDocTypeBinding().setDisabled(true);
                getDocFileNameBinding().setDisabled(true);
                getRefDocDateBinding().setDisabled(true);
                disableButton();

            } else if (mode.equals("C")) {
                getHeaderEditBinding().setDisabled(true);
                getChkSeqBinding().setDisabled(true);
                getInstrumentCdBinding().setDisabled(true);
                getDocSlNoBinding().setDisabled(true);
                getUnitCdRefDocBinding().setDisabled(true);
                getRefDocNoBinding().setDisabled(true);
                getRefDocTypeBinding().setDisabled(true);
                getDocFileNameBinding().setDisabled(true);
                getRefDocDateBinding().setDisabled(true);
                disableButton();

            } else if (mode.equals("V")) {
                getCalibrationMasterButtonBinding().setDisabled(false);
                getCalibrationParameterButtonBinding().setDisabled(false);
                getAccessoryButtonBinding().setDisabled(false);
                getAgency1Binding().setDisabled(true);
                getAgency2Binding().setDisabled(true);
                getReferenceDocumentTabBinding().setDisabled(false);
                getInstrumentMasterTabBinding().setDisabled(false);
                getDownloadLinkBinding().setDisabled(false);
            }
            
        }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setCalibrationMasterButtonBinding(RichButton calibrationMasterButtonBinding) {
        this.calibrationMasterButtonBinding = calibrationMasterButtonBinding;
    }

    public RichButton getCalibrationMasterButtonBinding() {
        return calibrationMasterButtonBinding;
    }

    public void setCalibrationParameterButtonBinding(RichButton calibrationParameterButtonBinding) {
        this.calibrationParameterButtonBinding = calibrationParameterButtonBinding;
    }

    public RichButton getCalibrationParameterButtonBinding() {
        return calibrationParameterButtonBinding;
    }

    public void setAccessoryButtonBinding(RichButton accessoryButtonBinding) {
        this.accessoryButtonBinding = accessoryButtonBinding;
    }

    public RichButton getAccessoryButtonBinding() {
        return accessoryButtonBinding;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void saveAL(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedByNew("InstrumentMasterVO1Iterator","LastUpdatedBy");
        if(issueToUnitBinding.getValue()!=null){
            OperationBinding op = ADFUtils.findOperation("generateInstrumentNo");
            op.execute();
            System.out.println("Result is===> "+op.getResult());
            if(op.getResult() != null && op.getResult().toString().equalsIgnoreCase("E"))
            {
                ADFUtils.findOperation("Commit").execute();
                ADFUtils.showMessage(" Record Updated Successfully", 2);
            }
            else if (op.getResult() != null && op.getResult().toString() != ""  && op.getResult().equals("S"))
            {
                System.out.println("+++++ in the save mode+++++++++++");
                ADFUtils.findOperation("Commit").execute();
                ADFUtils.showMessage("Record Saved Successfully.Instrument Code is "+ADFUtils.evaluateEL("#{bindings.InstCd.inputValue}"), 2);
                
            }
            else if(op.getResult() != null && op.getResult().toString().equalsIgnoreCase("N"))
            {
                ADFUtils.showMessage("Instrument Code could not be generated. Try Again !!", 0);
            }
        }else{
            FacesMessage message = new FacesMessage("Issue To Unit is required.");
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(issueToUnitBinding.getClientId(), message);
        }
       
        
        
        /*         System.out.println("Chal jaaaa");
        if(instrumentCdBinding.isDisabled())
        {
        ADFUtils.findOperation("Commit").execute();
        FacesMessage Message = new FacesMessage("Record Update Successfully.");  
        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
        FacesContext fc = FacesContext.getCurrentInstance();  
        fc.addMessage(null, Message); 
        System.out.println("Chal gyyaaaa");
        }            
        else {
        ADFUtils.findOperation("Commit").execute();
        FacesMessage Message = new FacesMessage("Record Save Successfully.");  
        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
        FacesContext fc = FacesContext.getCurrentInstance();  
        fc.addMessage(null, Message);  
        System.out.println("Chalra haaiiii");
        } */
    }

    public void setInstrumentCdBinding(RichInputText instrumentCdBinding) {
        this.instrumentCdBinding = instrumentCdBinding;
    }

    public RichInputText getInstrumentCdBinding() {
        return instrumentCdBinding;
    }

    public void setExternalButtonBinding(RichButton externalButtonBinding) {
        this.externalButtonBinding = externalButtonBinding;
    }

    public RichButton getExternalButtonBinding() {
        return externalButtonBinding;
    }

    public void setAgency1Binding(RichInputComboboxListOfValues agency1Binding) {
        this.agency1Binding = agency1Binding;
    }

    public RichInputComboboxListOfValues getAgency1Binding() {
        return agency1Binding;
    }

    public void setAgency2Binding(RichInputComboboxListOfValues agency2Binding) {
        this.agency2Binding = agency2Binding;
    }

    public RichInputComboboxListOfValues getAgency2Binding() {
        return agency2Binding;
    }

    public void inOutVL(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        disableButton();
    }

    public void setInOutBinding(RichSelectBooleanCheckbox inOutBinding) {
        this.inOutBinding = inOutBinding;
    }

    public RichSelectBooleanCheckbox getInOutBinding() {
        return inOutBinding;
    }
    
    public void disableButton(){
        if(inOutBinding.getValue()!=null){
            if(inOutBinding.getValue().toString().equalsIgnoreCase("Y") || inOutBinding.getValue().toString().equalsIgnoreCase("false")) {
                getExternalButtonBinding().setDisabled(true);
                    System.out.println("Inside If");
            }else {
                getExternalButtonBinding().setDisabled(false);
                    System.out.println("Inside If else");
            }
            AdfFacesContext.getCurrentInstance().addPartialTarget(externalButtonBinding);
        }
    }

        private oracle.jbo.domain.Number fetchMaxLineNumber(DCIteratorBinding itr) throws SQLException {
                            System.out.println("####################In fetchmaxline number###########################");
               oracle.jbo.domain.Number max = new oracle.jbo.domain.Number(0);
                            System.out.println("Starting Max value: " + max);
               InstrumentCalibrationParameterVORowImpl currRow = (InstrumentCalibrationParameterVORowImpl) itr.getCurrentRow();
               if (currRow != null && currRow.getChkSeq() != null)
                   max = currRow.getChkSeq();
                            System.out.println("Max Value after setting currentseqno: " + max);
               RowSetIterator rsi = itr.getRowSetIterator();
               if (rsi != null) {
                   Row[] allRowsInRange = rsi.getAllRowsInRange();
                   for (Row rw : allRowsInRange) {
                       if (rw != null && rw.getAttribute("ChkSeq") != null &&
                           max.compareTo(new oracle.jbo.domain.Number(rw.getAttribute("ChkSeq").toString())) < 0)
                           System.out.println("rw.getAttribute(\"ChkSeq\") " + rw.getAttribute("ChkSeq"));
                       max =new  oracle.jbo.domain.Number(rw.getAttribute("ChkSeq").toString());
                   }
               }
               rsi.closeRowSetIterator();
               max = max.add(new oracle.jbo.domain.Number(1));
                            System.out.println("max Value: " + max);
                            System.out.println("####################out fetchmaxline number###########################");
               return max;
           }

        public void createButtonAl(ActionEvent actionEvent) throws SQLException {
                            System.out.println("Create button");
                    DCIteratorBinding dci = ADFUtils.findIterator("InstrumentCalibrationParameterVO1Iterator");
                    if (dci != null) {
                        oracle.jbo.domain.Number CHK_SEQ = fetchMaxLineNumber(dci);
                            System.out.println("Sequence Number after fetchMaxLineNumber: " + CHK_SEQ);
                        RowSetIterator rsi = dci.getRowSetIterator();
                        if (rsi != null) {
                            Row last = rsi.last();
                            int i = rsi.getRangeIndexOf(last);
                            InstrumentCalibrationParameterVORowImpl newRow = (InstrumentCalibrationParameterVORowImpl) rsi.createRow();
                            newRow.setNewRowState(Row.STATUS_INITIALIZED);
                            rsi.insertRowAtRangeIndex(i + 1, newRow);
                            rsi.setCurrentRow(newRow);
                            newRow.setChkSeq(CHK_SEQ);
                        }
                        rsi.closeRowSetIterator();
                    }
            ADFUtils.findOperation("setInstrumentCodeDetails").execute();
        }

    public void setChkSeqBinding(RichInputText chkSeqBinding) {
        this.chkSeqBinding = chkSeqBinding;
    }

    public RichInputText getChkSeqBinding() {
        return chkSeqBinding;
    }

    public void setFreqTypeBinding(RichSelectOneChoice freqTypeBinding) {
        this.freqTypeBinding = freqTypeBinding;
    }

    public RichSelectOneChoice getFreqTypeBinding() {
        return freqTypeBinding;
    }

    public void setFreqBinding(RichInputText freqBinding) {
        this.freqBinding = freqBinding;
    }

    public RichInputText getFreqBinding() {
        return freqBinding;
    }

    public void typeChangeVL(ValueChangeEvent value) {
       value.getComponent().processUpdates(FacesContext.getCurrentInstance());
            if(!value.getNewValue().equals("D")){
                    getFreqBinding().setDisabled(true);
                }
    }

    public void setIssueToUnitBinding(RichInputComboboxListOfValues issueToUnitBinding) {
        this.issueToUnitBinding = issueToUnitBinding;
    }

    public RichInputComboboxListOfValues getIssueToUnitBinding() {
        return issueToUnitBinding;
    }

    public void calibrationCreateInsertAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert").execute();
        ADFUtils.findOperation("setInstrumentCodeDetails").execute();
    }

    public void accessoryCreateAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert1").execute();
        ADFUtils.findOperation("setInstrumentCodeDetails").execute();
    }

    public String saveAndCloseAL() {
        ADFUtils.setLastUpdatedBy("InstrumentMasterVO1Iterator");
        if(issueToUnitBinding.getValue()!=null)
        {
            OperationBinding op = ADFUtils.findOperation("generateInstrumentNo");
            op.execute();
            System.out.println("Result is===> "+op.getResult());
            if(op.getResult() != null && op.getResult().toString().equalsIgnoreCase("E"))
            {
                ADFUtils.findOperation("Commit").execute();
                ADFUtils.showMessage(" Record Updated Successfully", 2);
                return "SaveAndClose";
            }
            else if (op.getResult() != null && op.getResult().toString() != ""  && op.getResult().equals("S"))
            {
                System.out.println("+++++ in the save mode+++++++++++");
                ADFUtils.findOperation("Commit").execute();
                ADFUtils.showMessage("Record Saved Successfully.Instrument Code is "+ADFUtils.evaluateEL("#{bindings.InstCd.inputValue}"), 2);
                return "SaveAndClose";
                
            }
            else if(op.getResult() != null && op.getResult().toString().equalsIgnoreCase("N"))
            {
                ADFUtils.showMessage("Instrument Code could not be generated. Try Again !!", 0);
                return null;
            }
        }else{
            FacesMessage message = new FacesMessage("Issue To Unit is required.");
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(issueToUnitBinding.getClientId(), message);
            return null;
        }
        return null;
    }

    public void setReferenceDocumentTabBinding(RichShowDetailItem referenceDocumentTabBinding) {
        this.referenceDocumentTabBinding = referenceDocumentTabBinding;
    }

    public RichShowDetailItem getReferenceDocumentTabBinding() {
        return referenceDocumentTabBinding;
    }

    public void uploadAction(ValueChangeEvent vce) {
        System.out.println("inside uploadAction #### ");
                    //Get File Object from VC Event
                    UploadedFile fileVal = (UploadedFile) vce.getNewValue();
                    //Upload File to path- Return actual server path
                    String path = ADFUtils.uploadFile(fileVal);
                      System.out.println(fileVal.getContentType());
        //                      System.out.println("Content Type==>>"+fileVal.getContentType()+"Path==>>"+path+"File name==>>"+fileVal.getFilename());
                    //Method to insert data in table to keep track of uploaded files
                    OperationBinding ob = ADFUtils.findOperation("setFileDataForInstrumentMaster");
                    ob.getParamsMap().put("name", fileVal.getFilename());
                    ob.getParamsMap().put("path", path);
                    ob.getParamsMap().put("contTyp", fileVal.getContentType());
                    ob.getParamsMap().put("SrNo", ADFUtils.evaluateEL("#{bindings.DocAttachRefDtlVo3Iterator.estimatedRowCount+1}"));
                    ob.execute();
                    // Reset inputFile component after upload
                    ResetUtils.reset(vce.getComponent());
    }

    public void setDocSlNoBinding(RichInputText docSlNoBinding) {
        this.docSlNoBinding = docSlNoBinding;
    }

    public RichInputText getDocSlNoBinding() {
        return docSlNoBinding;
    }

    public void setUnitCdRefDocBinding(RichInputText unitCdRefDocBinding) {
        this.unitCdRefDocBinding = unitCdRefDocBinding;
    }

    public RichInputText getUnitCdRefDocBinding() {
        return unitCdRefDocBinding;
    }

    public void setRefDocNoBinding(RichInputText refDocNoBinding) {
        this.refDocNoBinding = refDocNoBinding;
    }

    public RichInputText getRefDocNoBinding() {
        return refDocNoBinding;
    }

    public void setRefDocTypeBinding(RichInputText refDocTypeBinding) {
        this.refDocTypeBinding = refDocTypeBinding;
    }

    public RichInputText getRefDocTypeBinding() {
        return refDocTypeBinding;
    }

    public void setDocFileNameBinding(RichInputText docFileNameBinding) {
        this.docFileNameBinding = docFileNameBinding;
    }

    public RichInputText getDocFileNameBinding() {
        return docFileNameBinding;
    }

    public void setRefDocDateBinding(RichInputDate refDocDateBinding) {
        this.refDocDateBinding = refDocDateBinding;
    }

    public RichInputDate getRefDocDateBinding() {
        return refDocDateBinding;
    }

    public void setPathBinding(RichInputText pathBinding) {
        this.pathBinding = pathBinding;
    }

    public RichInputText getPathBinding() {
        return pathBinding;
    }

    public void setContentTypeBinding(RichInputText contentTypeBinding) {
        this.contentTypeBinding = contentTypeBinding;
    }

    public RichInputText getContentTypeBinding() {
        return contentTypeBinding;
    }

    public void downloadAction(FacesContext facesContext, OutputStream outputStream) throws IOException {
        File filed = new File(pathBinding.getValue().toString());
                               FileInputStream fis;
                               byte[] b;
                               try {
                                   fis = new FileInputStream(filed);

                                   int n;
                                   while ((n = fis.available()) > 0) {
                                       b = new byte[n];
                                       int result = fis.read(b);
                                       outputStream.write(b, 0, b.length);
                                       if (result == -1)
                                           break;
                                   }
                               } catch (Exception e) {
                                   e.printStackTrace();
                               }
                               outputStream.flush();

    }

    public void deletePopupDLReferenceDocument(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                    {
                    ADFUtils.findOperation("Delete3").execute();
                    //ADFUtils.findOperation("Commit").execute();    //Unable on Search page.
                    System.out.println("Record Delete Successfully");
                   
                        FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                        FacesContext fc = FacesContext.getCurrentInstance();  
                        fc.addMessage(null, Message);  
                   
                    }

              AdfFacesContext.getCurrentInstance().addPartialTarget(referenceDocumentTableBinding);

    }

    public void setReferenceDocumentTableBinding(RichTable referenceDocumentTableBinding) {
        this.referenceDocumentTableBinding = referenceDocumentTableBinding;
    }

    public RichTable getReferenceDocumentTableBinding() {
        return referenceDocumentTableBinding;
    }

    public void setInstrumentMasterTabBinding(RichShowDetailItem instrumentMasterTabBinding) {
        this.instrumentMasterTabBinding = instrumentMasterTabBinding;
    }

    public RichShowDetailItem getInstrumentMasterTabBinding() {
        return instrumentMasterTabBinding;
    }

    public void setDownloadLinkBinding(RichCommandLink downloadLinkBinding) {
        this.downloadLinkBinding = downloadLinkBinding;
    }

    public RichCommandLink getDownloadLinkBinding() {
        return downloadLinkBinding;
    }

    public void instrumentTypeVCL(ValueChangeEvent vce) {
        System.out.println("INSIDE VCE #### ");
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        
        if(vce != null) {
            System.out.println("INSIDE IF ##### ");
            String val = "U";
            
            if(vce.getNewValue().equals(val)) {
                System.out.println("INSIDE VCE IF #### ");
                calibDueDateBinding.setDisabled(true);
            }
            else {
                System.out.println("INSIDE ELSE #### ");
                calibDueDateBinding.setDisabled(false);
            }
        }
        
    }

    public void setInstrumentTypeBinding(RichSelectOneChoice instrumentTypeBinding) {
        this.instrumentTypeBinding = instrumentTypeBinding;
    }

    public RichSelectOneChoice getInstrumentTypeBinding() {
        return instrumentTypeBinding;
    }

    public void setCalibDueDateBinding(RichInputDate calibDueDateBinding) {
        this.calibDueDateBinding = calibDueDateBinding;
    }

    public RichInputDate getCalibDueDateBinding() {
        return calibDueDateBinding;
    }
}
