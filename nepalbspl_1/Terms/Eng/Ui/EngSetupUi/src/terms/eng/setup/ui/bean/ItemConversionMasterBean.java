package terms.eng.setup.ui.bean;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.validator.ValidatorException;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;


public class ItemConversionMasterBean {
    private RichTable tableBinding;
    private RichInputText storeUnitValidator;
    private RichInputText storeUnitBinding;
    private RichSelectOneChoice uomBinding;
    private RichInputComboboxListOfValues bindUom;
    private String editAction="V";

    public ItemConversionMasterBean() {
    }
    public void setUomBinding(RichSelectOneChoice uomBinding) {
        this.uomBinding = uomBinding;
    }

    public RichSelectOneChoice getUomBinding() {
        return uomBinding;
    }
    public void deletePopupDialogDL(DialogEvent dialogEvent) 
    {
        if(dialogEvent.getOutcome().name().equals("ok"))
            {
            oracle.adf.model.OperationBinding op = null;
            ADFUtils.findOperation("Delete").execute();
            op = (oracle.adf.model.OperationBinding) ADFUtils.findOperation("Commit");
            if (getEditAction().equals("V")) {
                Object rst = op.execute();
            }
            System.out.println("Record Delete Successfully");
                if(op.getErrors().isEmpty()){
                    FacesMessage Message = new FacesMessage("Record Deleted Successfully");   
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                    FacesContext fc = FacesContext.getCurrentInstance();   
                    fc.addMessage(null, Message);
                } else if (!op.getErrors().isEmpty())
                {
                   oracle.adf.model.OperationBinding opr = (oracle.adf.model.OperationBinding) ADFUtils.findOperation("Rollback");
                   Object rstr = opr.execute();
                   FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                   Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                   FacesContext fc = FacesContext.getCurrentInstance();
                   fc.addMessage(null, Message);
                 }
            }

        AdfFacesContext.getCurrentInstance().addPartialTarget(tableBinding);
     }

    public void setTableBinding(RichTable tableBinding) {
        this.tableBinding = tableBinding;
    }

    public RichTable getTableBinding() {
        return tableBinding;
    }

    public void convertUnitValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        
       if(object!=null)
       {
               
         
        System.out.println("Value of uom"+  object.toString());
               Object recuom =  "";
               if(getStoreUnitBinding().getValue()!=null){
                   
                        recuom =  getStoreUnitBinding().getValue().toString();
                   
                   }
          
               System.out.println("Value of Recieved "+ recuom);
           if( object.toString().equals(recuom))
           {
               System.out.println("in the loop");
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "UOM can not be equal to Received UOM.", null));
            }
           
           
           }
    }

    public void setStoreUnitValidator(RichInputText storeUnitValidator) {
        this.storeUnitValidator = storeUnitValidator;
    }

    public RichInputText getStoreUnitValidator() {
        return storeUnitValidator;
    }

    public void setStoreUnitBinding(RichInputText storeUnitBinding) {
        this.storeUnitBinding = storeUnitBinding;
    }

    public RichInputText getStoreUnitBinding() {
        return storeUnitBinding;
    }


    public void duplicateUomVCE(ValueChangeEvent vce) {
        if(vce!=null){
         Object uom =   vce.getNewValue(); 
            
                Object recuom =  "";
                if(getStoreUnitBinding().getValue()!=null){
                    
                         recuom =  getStoreUnitBinding().getValue().toString();
                    
                    }
                
                System.out.println("Value of Recieved "+ recuom);
                if(uom.equals(recuom))
                {
                System.out.println("in the loop");
               // throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "UOM can not be equal to Received UOM.", null));
                    FacesMessage message = new FacesMessage("UOM can not be equal to Received UOM.");
                                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                                FacesContext context = FacesContext.getCurrentInstance();
                                context.addMessage(bindUom.getClientId(), message);
                }
        }
        
    }

    public void setBindUom(RichInputComboboxListOfValues bindUom) {
        this.bindUom = bindUom;
    }

    public RichInputComboboxListOfValues getBindUom() {
        return bindUom;
    }
    public void isEnableEdit(ActionEvent actionEvent) {
    // Add event code here...
    this.editAction=getEditAction();
    }

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }
}
