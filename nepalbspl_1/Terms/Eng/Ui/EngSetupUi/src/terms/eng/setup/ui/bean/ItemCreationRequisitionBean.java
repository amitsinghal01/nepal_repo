package terms.eng.setup.ui.bean;

import java.lang.reflect.Method;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.validator.ValidatorException;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

public class ItemCreationRequisitionBean {
    private RichPanelGroupLayout getMyPageRootComponent;
    private RichOutputText bindingOutputText;
    private RichInputComboboxListOfValues approvedByBinding;
    private RichButton editBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText bindEntNo;
    private RichInputComboboxListOfValues preparedbyBinding;
    private RichInputDate getentdate;
    private RichInputText itemDesc;

    public ItemCreationRequisitionBean() {
    }

//        public void SaveItemAl(ActionEvent actionEvent) {
//            
//           
//                }

//        public void AddItemDtlAl(ActionEvent actionEvent) {
//            //refreshPage();
//            ADFUtils.findOperation("CreateInsert").execute();
//        }
        //
        //    protected void refreshPage()
        //    {
        //    FacesContext fctx = FacesContext.getCurrentInstance();
        //    String refreshpage = fctx.getViewRoot().getViewId();
        //    ViewHandler ViewH = fctx.getApplication().getViewHandler();
        //    UIViewRoot UIV = ViewH.createView(fctx, refreshpage);
        //    UIV.setViewId(refreshpage);
        //    fctx.setViewRoot(UIV);
        //    }

//        public void deletePopupDialogDL(DialogEvent dialogEvent)
//        {
//            if(dialogEvent.getOutcome().name().equals("ok"))
//                {
//                
//                ADFUtils.findOperation("Delete").execute();
//                System.out.println("Record Delete Successfully");
//                
//                    FacesMessage Message = new FacesMessage("Record Deleted Successfully");   
//                    Message.setSeverity(FacesMessage.SEVERITY_INFO);   
//                    FacesContext fc = FacesContext.getCurrentInstance();   
//                    fc.addMessage(null, Message);   
//                
//                }
//
//            AdfFacesContext.getCurrentInstance().addPartialTarget(itemUnitStoreTableBinding);
//            
//        }

    
     

        public void EditButtonAL(ActionEvent actionEvent) {
            cevmodecheck();
        }
        
        private void cevmodecheck(){
            if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
                cevModeDisableComponent("V");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("C");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("E");
            }
        }
        
        private void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
            try {
                Method method1 =
                    component.getClass().getMethod("setDisabled", boolean.class);
                if (method1 != null) {
                    method1.invoke(component, valueComponent);
                }
            } catch (NoSuchMethodException e) {
                try {
                    Method method =
                        component.getClass().getMethod("setReadOnly", boolean.class);
                    if (method != null) {
                        method.invoke(component, valueComponent);
                    }
                } catch (Exception e1) {
                    // e.printStackTrace();//silently eat this exception.
                }


            } catch (Exception e) {
                // e.printStackTrace();//silently eat this exception.
            }
            List<UIComponent> childComponents = component.getChildren();
            for (UIComponent comp : childComponents) {
                makeComponentHierarchyReadOnly(comp, valueComponent);
            }
        }

        private void cevModeDisableComponent(String mode) {
            if (mode.equals("E")) {
               getUnitCodeBinding().setDisabled(true);
                getBindEntNo().setDisabled(true);
            getPreparedbyBinding().setDisabled(true);
                      
                        
        //                    getDetaildeleteBinding().setDisabled(false);
            } else if (mode.equals("C")) {
                getApprovedByBinding().setDisabled(true);
                getUnitCodeBinding().setDisabled(true);
                getBindEntNo().setDisabled(true);
                     
            } else if (mode.equals("V")) {
                      
            }
        }

    public void EditAL(ActionEvent actionEvent) {
//      
//      if(getApprovedByBinding()!=null){
//          getEditBinding().setDisabled(true);
//         
//      }
        cevmodecheck();
    }


    public void setGetMyPageRootComponent(RichPanelGroupLayout getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelGroupLayout getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void SaveItemAl(ActionEvent actionEvent) {
                ADFUtils.setLastUpdatedByNew("ItemCreationRequisitionformVO1Iterator","LastUpdatedBy");
        System.out.println("entry no===>>"+bindEntNo.getValue());
        if(bindEntNo.getValue()==null){
        OperationBinding op = ADFUtils.findOperation("getEntNo");
        Object rst = op.execute();
        
        ADFUtils.findOperation("Commit").execute();
        
        
        System.out.println("--------Commit-------");
            System.out.println("value aftr getting result--->?"+rst);
            
        
        
                    FacesMessage Message = new FacesMessage("Record Saved Successfully. New EntNo is "+rst+".");   
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                    FacesContext fc = FacesContext.getCurrentInstance();   
                    fc.addMessage(null, Message);  
        
                
            }
            
        //            if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ")
           else
            {
               
                ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message = new FacesMessage("Record Update Successfully.");   
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                    FacesContext fc = FacesContext.getCurrentInstance();   
                    fc.addMessage(null, Message);  
            
                

            }
            }
//            ADFUtils.findOperation("getEntNo").execute();
//            ADFUtils.findOperation("Commit").execute();
//            
//        if(approvedByBinding.isDisabled()){
//              
//                FacesMessage Message = new FacesMessage("Record Saved Successfully with Entry No"+ADFUtils.findOperation("getEntNo").execute());
//          Message.setSeverity(FacesMessage.SEVERITY_INFO);   
//            FacesContext fc = FacesContext.getCurrentInstance();   
//            fc.addMessage(null, Message);  
//            
//            }
//        else{
//            ADFUtils.findOperation("Commit").execute();
//            FacesMessage Message = new FacesMessage("Record Update Successfully");   
//            Message.setSeverity(FacesMessage.SEVERITY_INFO);   
//            FacesContext fc = FacesContext.getCurrentInstance();   
//            fc.addMessage(null, Message);  
//            
//        }
//        }
    

    public void setApprovedByBinding(RichInputComboboxListOfValues approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputComboboxListOfValues getApprovedByBinding() {
        return approvedByBinding;
    }

    public void SaveAndCloseAL(ActionEvent actionEvent){
     
                                ADFUtils.setLastUpdatedByNew("ItemCreationRequisitionformVO1Iterator","LastUpdatedBy");
   
                    
        
                            if(getBindEntNo()==null ||getApprovedByBinding().isDisabled() ){
                            OperationBinding op = ADFUtils.findOperation("getEntNo");
                            Object rst = op.execute();
                            
                            ADFUtils.findOperation("Commit").execute();
                            
                            
                            System.out.println("--------Commit-------");
                                System.out.println("value aftr getting result--->?"+rst);
                                
                            
                            
                                        FacesMessage Message = new FacesMessage("Record Saved Successfully. New EntNo is "+rst+".");   
                                        Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                                        FacesContext fc = FacesContext.getCurrentInstance();   
                                        fc.addMessage(null, Message);  
                                
                              
                            }
                            
                                
                                
                            //            if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ")
                              
                            else {
                                
                                ADFUtils.findOperation("Commit").execute();
                                    FacesMessage Message = new FacesMessage("Record Update Sucessfully.");   
                                    Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                                    FacesContext fc = FacesContext.getCurrentInstance();   
                                    fc.addMessage(null, Message);  
                                
                            
                                }
                            }

    

    public void setEditBinding(RichButton editBinding) {
        this.editBinding = editBinding;
    }

    public RichButton getEditBinding() {
        return editBinding;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setBindEntNo(RichInputText bindEntNo) {
        this.bindEntNo = bindEntNo;
    }

    public RichInputText getBindEntNo() {
        return bindEntNo;
    }

    public void approvedbyVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());            
                     OperationBinding ob = ADFUtils.findOperation("checkApprovalStatus");
                    ob.getParamsMap().put("formNm", "IR");
                    ob.getParamsMap().put("authoLim", "AP");
                    ob.getParamsMap().put("empcd", vce.getNewValue().toString());
                    ob.execute();
                    System.out.println(" EMP CD"+vce.getNewValue().toString());
                    if((ob.getResult() !=null && ob.getResult().equals("Y")))
                    {
                        Row row=(Row)ADFUtils.evaluateEL("#{bindings.ItemCreationRequisitionformVO1Iterator.currentRow}");
                        row.setAttribute("ApproveBy", null);
                        ADFUtils.showMessage("You Don't Have Permission To Approve This Record.",0);
                    }

        

        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        }

    public void setPreparedbyBinding(RichInputComboboxListOfValues preparedbyBinding) {
        this.preparedbyBinding = preparedbyBinding;
    }

    public RichInputComboboxListOfValues getPreparedbyBinding() {
        return preparedbyBinding;
    }

    public void setGetentdate(RichInputDate getentdate) {
        this.getentdate = getentdate;
    }

    public RichInputDate getGetentdate() {
        return getentdate;
    }

    public void setItemDesc(RichInputText itemDesc) {
        this.itemDesc = itemDesc;
    }

    public RichInputText getItemDesc() {
        return itemDesc;
    }
}
