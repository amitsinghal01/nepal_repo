package terms.eng.setup.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.application.ViewHandler;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import javax.faces.validator.ValidatorException;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;

public class ItemMasterBean {
    private RichTable itemUnitStoreTableBinding;
    private RichInputText bindItemDescription;
    private RichInputText bindSpecification;
    private RichInputComboboxListOfValues groupCodeBinding;
    private RichInputComboboxListOfValues subGroupCodeBinding;
    private RichInputText itemCodeBinding;
    private RichInputText gstCodeBinding;
    private RichOutputText bindingOutputText;
    private RichPanelGroupLayout getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichButton detailcreateBinding;
    private RichInputComboboxListOfValues weekGroupBinding;
    boolean flag=true;
    //    private RichButton detaildeleteBinding;

    public ItemMasterBean() {
    }
    
    public void forDetailLocation(){
                DCIteratorBinding dci = ADFUtils.findIterator("ItemUnitStoreLocVO1Iterator");
                RowSetIterator rsi=dci.getViewObject().createRowSetIterator(null);
                System.out.println("Flag: "+flag);
                    while(rsi.hasNext()) {
                        Row r = rsi.next();
                        String lCd=(String)r.getAttribute("LocCd");
                        if(lCd==null){
                        flag=false;
                        System.out.println("Flag is: "+flag);
                        }
                    }
                    rsi.closeRowSetIterator();
            }

    public void SaveItemAl(ActionEvent actionEvent) {
        forDetailLocation();
        System.out.println("--------Commit-------"+flag);
        if(flag){
        ADFUtils.setLastUpdatedByNew("ItemMasterVO1Iterator","LastUpdatedBy");
         if (!groupCodeBinding.isDisabled()) {
                OperationBinding op = ADFUtils.findOperation("getitemCode");
                Object rst = op.execute();
                System.out.println("value aftr getting result--->?"+rst);
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message = new FacesMessage("Record Saved Successfully. New Item Code is "+rst+".");   
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                    FacesContext fc = FacesContext.getCurrentInstance();   
                    fc.addMessage(null, Message);  

            }
            else
            {
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message = new FacesMessage("Record Update Successfully.");   
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                    FacesContext fc = FacesContext.getCurrentInstance();   
                    fc.addMessage(null, Message);  
            
                }
        }else{
            FacesMessage mg = new FacesMessage("Please fill Location in detail.");
            mg.setSeverity(FacesMessage.SEVERITY_INFO);   
            FacesContext fc = FacesContext.getCurrentInstance();   
            fc.addMessage(null, mg);
        }
            }

    public void AddItemDtlAl(ActionEvent actionEvent) {
        //refreshPage();
        ADFUtils.findOperation("CreateInsert").execute();
        String itemcd=(String)itemCodeBinding.getValue();
        if(itemcd!=null && !itemcd.isEmpty())
        {
        Row row=(Row)ADFUtils.evaluateEL("#{bindings.ItemUnitStoreLocVO1.currentRow}");
        row.setAttribute("ItemCd",itemcd);
        }
    }
//
//    protected void refreshPage() 
//    {
//    FacesContext fctx = FacesContext.getCurrentInstance();
//    String refreshpage = fctx.getViewRoot().getViewId();
//    ViewHandler ViewH = fctx.getApplication().getViewHandler();
//    UIViewRoot UIV = ViewH.createView(fctx, refreshpage);
//    UIV.setViewId(refreshpage);
//    fctx.setViewRoot(UIV);
//    }

    public void deletePopupDialogDL(DialogEvent dialogEvent)
    {
        if(dialogEvent.getOutcome().name().equals("ok"))
            {
            
            ADFUtils.findOperation("Delete").execute();
            System.out.println("Record Delete Successfully");
            
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");   
                Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                FacesContext fc = FacesContext.getCurrentInstance();   
                fc.addMessage(null, Message);   
            
            }

        AdfFacesContext.getCurrentInstance().addPartialTarget(itemUnitStoreTableBinding);
        
    }

    public void setItemUnitStoreTableBinding(RichTable itemUnitStoreTableBinding) {
        this.itemUnitStoreTableBinding = itemUnitStoreTableBinding;
    }

    public RichTable getItemUnitStoreTableBinding() {
        return itemUnitStoreTableBinding;
    }

    public void setBindItemDescription(RichInputText bindItemDescription) {
        this.bindItemDescription = bindItemDescription;
    }

    public RichInputText getBindItemDescription() {
        return bindItemDescription;
    }

    public void ItemDescriptionSpecVCL(ValueChangeEvent valueChangeEvent) {
        if(bindItemDescription.getValue()!=null&& bindSpecification.getValue()!=null)
        {
            
        String Spec = (String) bindItemDescription.getValue();
        System.out.println("Spec------"+Spec);
        bindSpecification.setValue(Spec);
    
    }
    }

    public void setBindSpecification(RichInputText bindSpecification) {
        this.bindSpecification = bindSpecification;
    }

    public RichInputText getBindSpecification() {
        return bindSpecification;
    }

    public void setGroupCodeBinding(RichInputComboboxListOfValues groupCodeBinding) {
        this.groupCodeBinding = groupCodeBinding;
    }

    public RichInputComboboxListOfValues getGroupCodeBinding() {
        return groupCodeBinding;
    }

    public void setSubGroupCodeBinding(RichInputComboboxListOfValues subGroupCodeBinding) {
        this.subGroupCodeBinding = subGroupCodeBinding;
    }

    public RichInputComboboxListOfValues getSubGroupCodeBinding() {
        return subGroupCodeBinding;
    }

    public void setItemCodeBinding(RichInputText itemCodeBinding) {
        this.itemCodeBinding = itemCodeBinding;
    }

    public RichInputText getItemCodeBinding() {
        return itemCodeBinding;
    }

    public void setGstCodeBinding(RichInputText gstCodeBinding) {
        this.gstCodeBinding = gstCodeBinding;
    }

    public RichInputText getGstCodeBinding() {
        return gstCodeBinding;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelGroupLayout getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelGroupLayout getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void EditButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }
    
    private void cevmodecheck(){
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }
    
    private void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 =
                component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method =
                    component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }

    private void cevModeDisableComponent(String mode) {
        if (mode.equals("E")) {
                    getGroupCodeBinding().setDisabled(true);
                    getSubGroupCodeBinding().setDisabled(true);
                    getItemCodeBinding().setDisabled(true);
                    getGstCodeBinding().setDisabled(true);
                    weekGroupBinding.setDisabled(true);
                    getHeaderEditBinding().setDisabled(true);
                    getDetailcreateBinding().setDisabled(false);
//                    getDetaildeleteBinding().setDisabled(false);
        } else if (mode.equals("C")) {
                   getItemCodeBinding().setDisabled(true);
                   getGstCodeBinding().setDisabled(true);
                   weekGroupBinding.setDisabled(true);
                   getHeaderEditBinding().setDisabled(true);
                   getDetailcreateBinding().setDisabled(false);
//                   getDetaildeleteBinding().setDisabled(false);

        } else if (mode.equals("V")) {
                   getDetailcreateBinding().setDisabled(true);
//                   getDetaildeleteBinding().setDisabled(true);
        }
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setDetailcreateBinding(RichButton detailcreateBinding) {
        this.detailcreateBinding = detailcreateBinding;
    }

    public RichButton getDetailcreateBinding() {
        return detailcreateBinding;
    }

//    public void setDetaildeleteBinding(RichButton detaildeleteBinding) {
//        this.detaildeleteBinding = detaildeleteBinding;
//    }
//
//    public RichButton getDetaildeleteBinding() {
//        return detaildeleteBinding;
//    }

    public void itemDescriptionValidator(FacesContext facesContext, UIComponent uIComponent, Object object)
    {
    if(object !=null && bindingOutputText.getValue().equals(0))
    {
        String Oldvalue=(String) object;
                System.out.println("Oldvalue111"+Oldvalue);
                oracle.adf.model.OperationBinding opr =(oracle.adf.model.OperationBinding) ADFUtils.findOperation("checkItemDuplicateDesc");
                opr.getParamsMap().put("Oldvalue",Oldvalue);
                Object obj=opr.execute();
                if(opr.getResult() !=null && opr.getResult().toString().equalsIgnoreCase("Y"))
                {
                   System.out.println("YYYYY De ra");
                   bindItemDescription.setValue(null);
                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Duplicate Item Description. ", null));
                    
                }
                Pattern pat=Pattern.compile("[ ]{2,}|^[^\\s].+[\\s]$|^\\s");//[ ]{2,}---->for two or more spaces
                Matcher mat=pat.matcher(Oldvalue);//[^\s].+[\s]$ ---->space @end
                boolean found=mat.find();//^\s---->space @initial
                System.out.println("foundddd"+found);
                if(found==true)
                {
                        throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Double space or Space cannot be entered at starting or ending of Item Description.", null));
                         
                    
                    
                    }
    }
    
    

    }

    public void setWeekGroupBinding(RichInputComboboxListOfValues weekGroupBinding) {
        this.weekGroupBinding = weekGroupBinding;
    }

    public RichInputComboboxListOfValues getWeekGroupBinding() {
        return weekGroupBinding;
    }

    public void groupCdVCE(ValueChangeEvent vce) {
        if(vce.getNewValue()!=null)
        {
            String group=(String)vce.getNewValue();
            if(group.equals("RM"))
            {
                weekGroupBinding.setDisabled(false);
            }else
            {
                weekGroupBinding.setDisabled(true);
            }
        }
    }
}
