package terms.eng.setup.ui.bean;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.ViewObject;

import terms.eng.setup.model.view.ItemRequisitionApprovalVORowImpl;

public class ItemRequisitionApprovalBean {
    private RichTable tableBinding;
    private String editAction="V";
    private RichInputText approvedByBinding;
    private RichColumn bindUnit;
    private RichInputText unitBinding;

    public ItemRequisitionApprovalBean() {
    }

    public void populateAL(ActionEvent actionEvent) {
        OperationBinding ob=ADFUtils.findOperation("PopulateItemRequisition");
        ob.execute();
        if(ob.getResult()!=null && ob.getResult().toString().equalsIgnoreCase("V"))
        {
            ADFUtils.showMessage("Data populated Succesfully.", 2);
            }
         AdfFacesContext.getCurrentInstance().addPartialTarget(tableBinding);
    }

    public void setTableBinding(RichTable tableBinding) {
        this.tableBinding = tableBinding;
    }

    public RichTable getTableBinding() {
        return tableBinding;
    }

    public void SaveAL(ActionEvent actionEvent) {
        DCIteratorBinding dciter = (DCIteratorBinding) ADFUtils.findIterator("ItemRequisitionApprovalVO1Iterator");

         ViewObject voh = dciter.getViewObject();
         String Trans =(String) (voh.getCurrentRow().getAttribute("TransCheck") != null ?
                      voh.getCurrentRow().getAttribute("TransCheck") :"N");
         System.out.println("Trans is===="+Trans);
        if(Trans.equalsIgnoreCase("Y")||Trans.equals(true))
          {
            System.out.println("committt");
        ADFUtils.findOperation("Commit1").execute();
        voh.setNamedWhereClauseParam("BindModeType", "C");
     voh.executeQuery();
        }
   
          else
          {
              ADFUtils.showMessage("TransCheck must be check to save the particular row", 2);
              }
            voh.closeRowSetIterator();
        }
    

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }
    public void isEnableEdit(ActionEvent actionEvent) {
    // Add event code here...
    this.editAction=getEditAction();
    }

    public void EditAL(ActionEvent actionEvent) {
        AdfFacesContext.getCurrentInstance().addPartialTarget(tableBinding);
    }

    public void ApprovedByVCE(ValueChangeEvent vce) {
       vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
       
       Row row =  (Row) ADFUtils.evaluateEL("#{bindings.ItemRequisitionApprovalVO1Iterator.currentRow}");
       System.out.println("ROW ENTRY NO=>"+row.getAttribute("EntNo"));
       
       if(vce.getNewValue()!=null)
       {
           vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
           System.out.println("TransCheck==="+vce.getNewValue());
           Boolean check = (Boolean) (vce.getNewValue() != null ? vce.getNewValue() : false);
           if(check.equals(true))
           {
//               approvedByBinding.setValue(ADFContext.getCurrent().getPageFlowScope().get("empCode")!=null?ADFContext.getCurrent().getPageFlowScope().get("empCode"):"SWE161");
                   oracle.binding.OperationBinding op=ADFUtils.findOperation("CheckAuthority");
                     op.getParamsMap().put("unitCd",unitBinding.getValue());
                     op.getParamsMap().put("EmpCd", ADFContext.getCurrent().getPageFlowScope().get("empCode")!=null?ADFContext.getCurrent().getPageFlowScope().get("empCode"):"SWE161");
                     op.execute();
               }
           else
           {
               approvedByBinding.setValue(null);
               }
           
           }
    }

    public void setApprovedByBinding(RichInputText approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputText getApprovedByBinding() {
        return approvedByBinding;
    }

    public void setBindUnit(RichColumn bindUnit) {
        this.bindUnit = bindUnit;
    }

    public RichColumn getBindUnit() {
        return bindUnit;
    }

    public void setUnitBinding(RichInputText unitBinding) {
        this.unitBinding = unitBinding;
    }

    public RichInputText getUnitBinding() {
        return unitBinding;
    }

    public void CheckAllVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce!=null)
        {
                OperationBinding Opr = ADFUtils.findOperation("checkEditSelect");
                 Object Obj=Opr.execute();
        
            DCIteratorBinding Dc=ADFUtils.findIterator("ItemRequisitionApprovalVO1Iterator");
          
            ItemRequisitionApprovalVORowImpl Invoice_Row=(ItemRequisitionApprovalVORowImpl) Dc.getCurrentRow();
            System.out.println("Invoice_Row.getEditSelect()"+Invoice_Row.getCheckAll());
            if(Invoice_Row.getTransCheck()!=null && Invoice_Row.getTransCheck().toString().equalsIgnoreCase("Y"))
            {
        System.out.println("insidecheckYrowimpl");
        //               approvedByBinding.setValue(ADFContext.getCurrent().getPageFlowScope().get("empCode")!=null?ADFContext.getCurrent().getPageFlowScope().get("empCode"):"SWE161");
                oracle.binding.OperationBinding op=ADFUtils.findOperation("CheckAuthority");
                  op.getParamsMap().put("unitCd",unitBinding.getValue());
                  op.getParamsMap().put("EmpCd", ADFContext.getCurrent().getPageFlowScope().get("empCode")!=null?ADFContext.getCurrent().getPageFlowScope().get("empCode"):"SWE161");
                  op.execute();
            }
   else
        {
            approvedByBinding.setValue(null);
            }
    
    }
    
    }
}
