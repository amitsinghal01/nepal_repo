package terms.eng.setup.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class JigsFixtureMasterBean {
    private RichTable jigsFixtureDetailTableBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText jigFixturesCodeBinding;
    private RichOutputText bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichButton detailcreateBinding;
    private RichButton detaildeleteBinding;
    private RichInputText unitNameBind;
    private RichInputText jigFixtureDescriptionBinding;
    private RichInputText manufacturerBinding;

    public JigsFixtureMasterBean() {
    }

    public void jigFixNumber(ActionEvent actionEvent) {
            ADFUtils.setLastUpdatedByNew("JigsFixtureHeaderVO1Iterator","LastUpdatedBy");
        OperationBinding op = ADFUtils.findOperation("getJFCode");
                           Object rst = op.execute();
                          
                           System.out.println("--------Commit-------");
                           System.out.println("value aftr getting result--->?"+rst);
                          
                               if (rst.toString() != null && rst.toString() != "") {
                                   if (op.getErrors().isEmpty()) {
                                       OperationBinding op1= ADFUtils.findOperation("Commit");
                                       Object rs=op1.execute();
                                     
                                     FacesMessage Message = new FacesMessage("Record Saved Successfully. New Jig/Fixture Code is "+rst+".");  
                                       Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                                       FacesContext fc = FacesContext.getCurrentInstance();  
                                       fc.addMessage(null, Message); 
                                   }
                               }

                              
                               if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ") {
                                   if (op.getErrors().isEmpty()) {
                                       ADFUtils.findOperation("Commit").execute();
                                       FacesMessage Message = new FacesMessage("Record Update Successfully.");  
                                       Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                                       FacesContext fc = FacesContext.getCurrentInstance();  
                                       fc.addMessage(null, Message); 
                              
                                   }

                              
                               }
        }

    public void deletePopupDialogDL(DialogEvent dialogEvent) 
    {
        if(dialogEvent.getOutcome().name().equals("ok"))
            {
            ADFUtils.findOperation("Delete").execute();
            System.out.println("Record Delete Successfully");
            
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");   
                Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                FacesContext fc = FacesContext.getCurrentInstance();   
                fc.addMessage(null, Message);   
            
            }
        AdfFacesContext.getCurrentInstance().addPartialTarget(jigsFixtureDetailTableBinding);
    }

    public void setJigsFixtureDetailTableBinding(RichTable jigsFixtureDetailTableBinding) {
        this.jigsFixtureDetailTableBinding = jigsFixtureDetailTableBinding;
    }

    public RichTable getJigsFixtureDetailTableBinding() {
        return jigsFixtureDetailTableBinding;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setJigFixturesCodeBinding(RichInputText jigFixturesCodeBinding) {
        this.jigFixturesCodeBinding = jigFixturesCodeBinding;
    }

    public RichInputText getJigFixturesCodeBinding() {
        return jigFixturesCodeBinding;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void EditButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }
    
    private void cevmodecheck(){
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }
    
    private void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 =
                component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method =
                    component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }
    
    private void cevModeDisableComponent(String mode) {
        if (mode.equals("E")) {
                    getUnitCodeBinding().setDisabled(true);
                    getUnitNameBind().setDisabled(true);
                    getJigFixturesCodeBinding().setDisabled(true);
                    getJigFixtureDescriptionBinding().setDisabled(true);
                    getManufacturerBinding().setDisabled(true);
                    getHeaderEditBinding().setDisabled(true);
                    getDetailcreateBinding().setDisabled(false);
                    getDetaildeleteBinding().setDisabled(false);
        } else if (mode.equals("C")) {
                     getUnitCodeBinding().setDisabled(true);
                     getUnitNameBind().setDisabled(true); 
                     getJigFixturesCodeBinding().setDisabled(true);            
                     getHeaderEditBinding().setDisabled(true);
                     getDetailcreateBinding().setDisabled(false);
                   
        } else if (mode.equals("V")) {
            
                     getDetailcreateBinding().setDisabled(true);
                     getDetaildeleteBinding().setDisabled(true);
        }
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setDetailcreateBinding(RichButton detailcreateBinding) {
        this.detailcreateBinding = detailcreateBinding;
    }

    public RichButton getDetailcreateBinding() {
        return detailcreateBinding;
    }

    public void setDetaildeleteBinding(RichButton detaildeleteBinding) {
        this.detaildeleteBinding = detaildeleteBinding;
    }

    public RichButton getDetaildeleteBinding() {
        return detaildeleteBinding;
    }

    public void setUnitNameBind(RichInputText unitNameBind) {
        this.unitNameBind = unitNameBind;
    }

    public RichInputText getUnitNameBind() {
        return unitNameBind;
    }

    public void setJigFixtureDescriptionBinding(RichInputText jigFixtureDescriptionBinding) {
        this.jigFixtureDescriptionBinding = jigFixtureDescriptionBinding;
    }

    public RichInputText getJigFixtureDescriptionBinding() {
        return jigFixtureDescriptionBinding;
    }

    public void setManufacturerBinding(RichInputText manufacturerBinding) {
        this.manufacturerBinding = manufacturerBinding;
    }

    public RichInputText getManufacturerBinding() {
        return manufacturerBinding;
    }
}
