package terms.eng.setup.ui.bean;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

public class LocationMasterBean {
    private RichTable locationTableBinding;
    private String editAction="V";
    private RichInputText locationCodeBinding;
    private RichInputText longDescBinding;

    public LocationMasterBean() {
    }

    public void deletePopUpDailogDL(DialogEvent dialogEvent) 
    {
            if(dialogEvent.getOutcome().name().equals("ok"))
            {
            OperationBinding op = null;
            ADFUtils.findOperation("Delete").execute();
            op = (OperationBinding) ADFUtils.findOperation("Commit");
            if (getEditAction().equals("V")) {
            Object rst = op.execute();
            }
            System.out.println("Record Delete Successfully");
            if(op.getErrors().isEmpty()){
            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
            } else if (!op.getErrors().isEmpty())
            {
            OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
            Object rstr = opr.execute();
            FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
            Message.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
            }
            
            }

        AdfFacesContext.getCurrentInstance().addPartialTarget(locationTableBinding);
        
    
    
    
    }

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public void setLocationTableBinding(RichTable locationTableBinding) {
        this.locationTableBinding = locationTableBinding;
    }

    public RichTable getLocationTableBinding() {
        return locationTableBinding;
    }

    public void locationCodeChildRecordMethod(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
            System.out.println("Edit Action is=> "+editAction);
            if(vce !=null && editAction.equals("E"))
            {
           String Oldvalue=(String) vce.getOldValue();
           OperationBinding opr =(OperationBinding) ADFUtils.findOperation("getChildRecordLocationForm");
           opr.getParamsMap().put("Oldvalue",Oldvalue);
           Object obj=opr.execute();

           if(opr.getResult() !=null && opr.getResult().equals("Y")){
              
               FacesMessage Message = new FacesMessage("Record cannot change. Child record found.");
               Message.setSeverity(FacesMessage.SEVERITY_ERROR);
               FacesContext fc = FacesContext.getCurrentInstance();
               fc.addMessage(null, Message);
               locationCodeBinding.setValue(Oldvalue);
           }
    }
}

    public void setLocationCodeBinding(RichInputText locationCodeBinding) {
        this.locationCodeBinding = locationCodeBinding;
    }

    public RichInputText getLocationCodeBinding() {
        return locationCodeBinding;
    }

    public void shortDescVCE(ValueChangeEvent vce) 
    {
        
    vce.getComponent().processUpdates(FacesContext.getCurrentInstance());

    if(vce !=null)
    {
    longDescBinding.setValue(vce.getNewValue());    
    }
    }

    public void setLongDescBinding(RichInputText longDescBinding) {
        this.longDescBinding = longDescBinding;
    }

    public RichInputText getLongDescBinding() {
        return longDescBinding;
    }
}
