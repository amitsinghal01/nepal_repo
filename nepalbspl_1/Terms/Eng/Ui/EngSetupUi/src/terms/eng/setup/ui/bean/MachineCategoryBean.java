package terms.eng.setup.ui.bean;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class MachineCategoryBean {
    private RichTable machineCategoryTableBinding;
    private String editAction="V";
    private RichInputText machineCodeBinding;

    public MachineCategoryBean() {
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) 
    {
            if(dialogEvent.getOutcome().name().equals("ok"))
            {
            OperationBinding op = null;
            ADFUtils.findOperation("Delete").execute();
            op = (OperationBinding) ADFUtils.findOperation("Commit");
            if (getEditAction().equals("V")) {
            Object rst = op.execute();
            }
            System.out.println("Record Delete Successfully");
            if(op.getErrors().isEmpty()){
            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
            } else if (!op.getErrors().isEmpty())
            {
            OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
            Object rstr = opr.execute();
            FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
            Message.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
            }
            }
        AdfFacesContext.getCurrentInstance().addPartialTarget(machineCategoryTableBinding);
    }

    public void setMachineCategoryTableBinding(RichTable machineCategoryTableBinding) {
        this.machineCategoryTableBinding = machineCategoryTableBinding;
    }

    public RichTable getMachineCategoryTableBinding() {
        return machineCategoryTableBinding;
    }


    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public void MachCodeDuplicate(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
            System.out.println("Edit Action is=> "+editAction);
            if(vce !=null && editAction.equals("E"))
            {
           String Oldvalue=(String) vce.getOldValue();
           OperationBinding opr =(OperationBinding) ADFUtils.findOperation("checkMachineCode");
           opr.getParamsMap().put("Oldvalue",Oldvalue);
           Object obj=opr.execute();

           if(opr.getResult() !=null && opr.getResult().equals("Y")){
              
               FacesMessage Message = new FacesMessage("Record cannot be changed. Child record found.");
               Message.setSeverity(FacesMessage.SEVERITY_ERROR);
               FacesContext fc = FacesContext.getCurrentInstance();
               fc.addMessage(null, Message);
               getMachineCodeBinding().setValue(Oldvalue);
           }
                   
            }
    }

    public void setMachineCodeBinding(RichInputText machineCodeBinding) {
        this.machineCodeBinding = machineCodeBinding;
    }

    public RichInputText getMachineCodeBinding() {
        return machineCodeBinding;
    }
}
