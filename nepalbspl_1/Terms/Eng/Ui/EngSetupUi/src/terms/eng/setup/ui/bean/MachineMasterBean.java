package terms.eng.setup.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;

import oracle.adf.view.rich.component.rich.input.RichInputText;

import oracle.adf.view.rich.component.rich.input.RichSelectBooleanCheckbox;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;

import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;

import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.binding.OperationBinding;

public class MachineMasterBean {
    private RichInputComboboxListOfValues unitBinding;
    private RichInputText codeBinding;
    private RichSelectOneChoice machineStatusBinding;
    private RichInputText macCategoryNameBinding;
    private RichInputText workCenterBinding;
    private RichInputText macHrRateBinding;
    private RichSelectBooleanCheckbox toolReqBinding;
    private RichInputText setUpAspectsBinding;
    private RichButton headerEditBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichOutputText bindingOutputText;
    private RichInputText supplierNameBinding;
    private RichInputText installedAtDescriptionBinding;
    private RichInputText unitNameBinding;
    private String message="CM";


    public MachineMasterBean() {
    }
    
    public oracle.jbo.domain.Date getCurrentDate()  
    {  
        oracle.jbo.domain.Date cdate=new oracle.jbo.domain.Date(new java.sql.Date(new java.util.Date().getTime()));    
        return cdate;  
    }



    public void setUnitBinding(RichInputComboboxListOfValues unitBinding) {
        this.unitBinding = unitBinding;
    }

    public RichInputComboboxListOfValues getUnitBinding() {
        return unitBinding;
    }

    public void saveButtonAL(ActionEvent actionEvent) 
    {
        ADFUtils.setLastUpdatedByNew("MachineMasterVO1Iterator","LastUpdatedBy");
        ADFUtils.findOperation("generateMachCode").execute();
    if(message.equalsIgnoreCase("EM"))
    {
         System.out.println("Edit Mode"+unitBinding.isDisabled());
         FacesMessage Message = new FacesMessage("Record Updated Successfully.");  
         Message.setSeverity(FacesMessage.SEVERITY_INFO);  
         FacesContext fc = FacesContext.getCurrentInstance();  
         fc.addMessage(null, Message);  
     }
    else if(message.equalsIgnoreCase("CM"))
    {
        FacesMessage Message = new FacesMessage("Record Saved Successfully. New Machine Code "+codeBinding.getValue()+".");  
        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
        FacesContext fc = FacesContext.getCurrentInstance();  
        fc.addMessage(null, Message);    
        message="EM";

            }
    }

    public void setCodeBinding(RichInputText codeBinding) {
        this.codeBinding = codeBinding;
    }

    public RichInputText getCodeBinding() {
        return codeBinding;
    }

    public void setMachineStatusBinding(RichSelectOneChoice machineStatusBinding) {
        this.machineStatusBinding = machineStatusBinding;
    }

    public RichSelectOneChoice getMachineStatusBinding() {
        return machineStatusBinding;
    }

    public void setMacCategoryNameBinding(RichInputText macCategoryNameBinding) {
        this.macCategoryNameBinding = macCategoryNameBinding;
    }

    public RichInputText getMacCategoryNameBinding() {
        return macCategoryNameBinding;
    }

    public void setWorkCenterBinding(RichInputText workCenterBinding) {
        this.workCenterBinding = workCenterBinding;
    }

    public RichInputText getWorkCenterBinding() {
        return workCenterBinding;
    }

    public void setMacHrRateBinding(RichInputText macHrRateBinding) {
        this.macHrRateBinding = macHrRateBinding;
    }

    public RichInputText getMacHrRateBinding() {
        return macHrRateBinding;
    }

    public void setToolReqBinding(RichSelectBooleanCheckbox toolReqBinding) {
        this.toolReqBinding = toolReqBinding;
    }

    public RichSelectBooleanCheckbox getToolReqBinding() {
        return toolReqBinding;
    }

    public void setSetUpAspectsBinding(RichInputText setUpAspectsBinding) {
        this.setUpAspectsBinding = setUpAspectsBinding;
    }

    public RichInputText getSetUpAspectsBinding() {
        return setUpAspectsBinding;
    }

    public void EditButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
            try {
                Method method1 =
                    component.getClass().getMethod("setDisabled", boolean.class);
                if (method1 != null) {
                    method1.invoke(component, valueComponent);
                }
            } catch (NoSuchMethodException e) {
                try {
                    Method method =
                        component.getClass().getMethod("setReadOnly", boolean.class);
                    if (method != null) {
                        method.invoke(component, valueComponent);
                    }
                } catch (Exception e1) {
                    // e.printStackTrace();//silently eat this exception.
                }


            } catch (Exception e) {
                // e.printStackTrace();//silently eat this exception.
            }
            List<UIComponent> childComponents = component.getChildren();
            for (UIComponent comp : childComponents) {
                makeComponentHierarchyReadOnly(comp, valueComponent);
            }
        }
    private void cevmodecheck(){
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }
    public void cevModeDisableComponent(String mode) {
    //            FacesContext fctx = FacesContext.getCurrentInstance();
    //            ELContext elctx = fctx.getELContext();
    //            Application jsfApp = fctx.getApplication();
    //            //create a ValueExpression that points to the ADF binding layer
    //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
    //            //
    //            ValueExpression valueExpr = exprFactory.createValueExpression(
    //                                         elctx,
    //                                         "#{pageFlowScope.mode=='E'}",
    //                                          Object.class
    //                                         );
    //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
        if (mode.equals("E")) {
                   getUnitBinding().setDisabled(true);
                //   getMachineStatusBinding().setDisabled(true);
                   getMacCategoryNameBinding().setDisabled(true);
                  // getMacHrRateBinding().setDisabled(true);
                   getToolReqBinding().setDisabled(true);
                  // getSetUpAspectsBinding().setDisabled(true);
                   getWorkCenterBinding().setDisabled(true);
                   getSupplierNameBinding().setDisabled(true);
                   getInstalledAtDescriptionBinding().setDisabled(true);
                   getUnitNameBinding().setDisabled(true);
                   getHeaderEditBinding().setDisabled(true);
                   getCodeBinding().setDisabled(true);
                   message="EM";
             
               
        } else if (mode.equals("C")) {
            getHeaderEditBinding().setDisabled(true);
            getMacCategoryNameBinding().setDisabled(true);
            getWorkCenterBinding().setDisabled(true);
            getSupplierNameBinding().setDisabled(true);
            getInstalledAtDescriptionBinding().setDisabled(true);
            getUnitNameBinding().setDisabled(true);
//            getUnitBinding().setDisabled(true);
            /*  getVendorcodeBinding().setDisabled(true);
            getDetailcreateBinding().setDisabled(false);
            getDetaildeleteBinding().setDisabled(false); */
        } else if (mode.equals("V")) {
            /*   getDetailcreateBinding().setDisabled(true);
            getDetaildeleteBinding().setDisabled(true); */
        } 
    }
    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() 
    {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setSupplierNameBinding(RichInputText supplierNameBinding) {
        this.supplierNameBinding = supplierNameBinding;
    }

    public RichInputText getSupplierNameBinding() {
        return supplierNameBinding;
    }

    public void setInstalledAtDescriptionBinding(RichInputText installedAtDescriptionBinding) {
        this.installedAtDescriptionBinding = installedAtDescriptionBinding;
    }

    public RichInputText getInstalledAtDescriptionBinding() {
        return installedAtDescriptionBinding;
    }

    public void setUnitNameBinding(RichInputText unitNameBinding) {
        this.unitNameBinding = unitNameBinding;
    }

    public RichInputText getUnitNameBinding() {
        return unitNameBinding;
    }
    
    public void methodAction() {
        System.out.println("Before Create");
        ADFUtils.findOperation("CreateInsert").execute();
        ADFUtils.findOperation("CreateInsert1").execute();
        System.out.println("After Create");
    }
}


