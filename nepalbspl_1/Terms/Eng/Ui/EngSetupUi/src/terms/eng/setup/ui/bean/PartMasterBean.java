package terms.eng.setup.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import javax.el.ELContext;
import javax.el.ExpressionFactory;

import javax.el.ValueExpression;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.application.ViewHandler;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import javax.faces.validator.ValidatorException;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;

import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;


public class PartMasterBean {
    private RichSelectOneChoice bindingRawMtrl;
    private RichInputText bindingIH_SOB;
    private RichInputText bindingBO_SOB;
    private RichInputText bindPartCode;
    private RichTable itemUnitStoreTableBinding;
    private RichInputDate validUptoBinding;
    private RichInputText unitCodeBinding;
    private RichInputText lastModByBinding;
    private RichInputDate modifiedDateBinding;
    private RichInputText revenueNoBinding;
    private RichInputText gstCodeBinding;
    private RichPanelGroupLayout getMyPageRootComponent;
    private RichOutputText bindingOutputText;
    private RichButton headerEditBinding;
    private RichButton detailcreateBinding;
    private RichInputText groupDescBinding;
    private RichInputText subGroupDescBinding;
    private RichInputComboboxListOfValues groupCodeBinding;
    private RichInputComboboxListOfValues subGroupCdBinding;
    private RichSelectOneChoice measurementUnitbinding;
    private RichShowDetailItem purchaseBinding;
    private RichShowDetailItem qualityBinding;
    private RichShowDetailItem mrpBind;
    private RichShowDetailItem saleBind;
    private RichShowDetailItem financeBind;
    private RichShowDetailItem costingBind;
    private RichShowDetailItem locMappingBinding;
    //  private RichButton detaildeleteBinding;

    public PartMasterBean() {
    }

    public void PartDtlAl(ActionEvent actionEvent) {
       // refreshPage();
        ADFUtils.findOperation("CreateInsert").execute();
//        if(bindPartCode.getValue()!=null)
//        {
//           // System.out.println("jkhjkhdsjkfhjkhd"+ADFUtils.resolveExpression("#{pageFlowScope.mode}"));
//            getBindPartCode().setDisabled(true);
//        }
    }

    public void SavePartAL(ActionEvent actionEvent) 
    {
       ADFUtils.setLastUpdatedByNew("PartMasterVO1Iterator","LastUpdatedBy");

        Integer i = 0;
        i=(Integer)bindingOutputText.getValue();
        System.out.println("iiiii is====="+i);
        
        OperationBinding op = ADFUtils.findOperation("generatePartCode");
        op.execute();
        System.out.println("Result is------> "+op.getResult());
        if(op.getResult() != null && op.getResult().toString().equalsIgnoreCase("E"))
        {
             ADFUtils.findOperation("Commit").execute();
             ADFUtils.showMessage(" Record Updated Successfully", 2);
        }
        else if (op.getResult() != null && op.getResult().toString() != ""  && op.getResult().equals("S"))
         {
             System.out.println("+++++ in the save mode+++++++++++");
             ADFUtils.findOperation("Commit").execute();
            ADFUtils.showMessage("Record Saved Successfully.Part Code is "+ADFUtils.evaluateEL("#{bindings.PartCode.inputValue}"), 2);  
        }
        else if(op.getResult() != null && op.getResult().toString().equalsIgnoreCase("N"))
        {
             ADFUtils.showMessage("Part Code could not be generated.Try Again !!", 0);
        }
        else if(op.getResult() != null && op.getResult().toString().equalsIgnoreCase("MNL"))
        {
            System.out.println("and The Manula Entry ------>>"+op.getResult());
            if(i==0)
            {
                getBindPartCode().setDisabled(false);
                if(bindPartCode.getValue()!=null)
                {
                    ADFUtils.findOperation("Commit").execute();
                    ADFUtils.showMessage("Record Saved Successfully.Part Code is "+ADFUtils.evaluateEL("#{bindings.PartCode.inputValue}"), 2); 
                }else{
                        FacesMessage message = new FacesMessage("Part Code must be required.");   
                        message.setSeverity(FacesMessage.SEVERITY_ERROR);   
                        FacesContext fc = FacesContext.getCurrentInstance();   
                        fc.addMessage(bindPartCode.getClientId(), message);
                }
            }
            else{
                   ADFUtils.findOperation("Commit").execute();
                   ADFUtils.showMessage(" Record Updated Successfully", 2); 
            }
        }
    
    }

    
    
    protected void refreshPage() 
    {
    FacesContext fctx = FacesContext.getCurrentInstance();
    String refreshpage = fctx.getViewRoot().getViewId();
    ViewHandler ViewH = fctx.getApplication().getViewHandler();
    UIViewRoot UIV = ViewH.createView(fctx, refreshpage);
    UIV.setViewId(refreshpage);
    fctx.setViewRoot(UIV);
    }

   
 
    public void setBindingRawMtrl(RichSelectOneChoice bindingRawMtrl) {
        this.bindingRawMtrl = bindingRawMtrl;
    }

    public RichSelectOneChoice getBindingRawMtrl() {
        return bindingRawMtrl;
    }

    public void setBindingIH_SOB(RichInputText bindingIH_SOB) {
        this.bindingIH_SOB = bindingIH_SOB;
    }

    public RichInputText getBindingIH_SOB() {
        return bindingIH_SOB;
    }

    public void setBindingBO_SOB(RichInputText bindingBO_SOB) {
        this.bindingBO_SOB = bindingBO_SOB;
    }

    public RichInputText getBindingBO_SOB() {
        return bindingBO_SOB;
    }
    
    // 10-05-2017
    //To set field blank aftr again changing the dependent field value.
    public void ProcTypVCE(ValueChangeEvent vce) {
        if(vce!=null){
            
            String ib = vce.getNewValue().toString();
            String IB ="IB";
            
        System.out.println("value at vce for prc typ--->"+vce.getNewValue());
        if(!ib.equalsIgnoreCase(IB)){
            
            getBindingBO_SOB().setValue(null);
            getBindingIH_SOB().setValue(null);
                AdfFacesContext.getCurrentInstance().addPartialTarget(getBindingBO_SOB());
                AdfFacesContext.getCurrentInstance().addPartialTarget(getBindingIH_SOB());
            
            }
            
            }
    }
    // 10-05-2017
    //To set field blank aftr again changing the dependent field value.
    public void PartTypVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce!=null){
        System.out.println("value at vce for part typ--->"+vce.getNewValue());
            String rm = vce.getNewValue().toString();
            String RM ="RM";
            if(!rm.equalsIgnoreCase("RM")){
                
                getBindingRawMtrl().setValue(null);
                //getBindingRawMtrl().setDisabled(true);
                    AdfFacesContext.getCurrentInstance().addPartialTarget(getBindingRawMtrl());
                }
        }
    }

    //10-05-2017
    //Validation for sum of two value should not exceed than 100.
    public void sumValidtn(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if(object!=null && getBindingIH_SOB().getValue()!=null)
       {
            
            BigDecimal val = new BigDecimal(object.toString());
            BigDecimal ih = new BigDecimal(getBindingIH_SOB().getValue().toString());
                
            
                BigDecimal value = val.add(ih);
                       Integer sum = Integer.parseInt(value.toString());
                       if (sum > 100) {
                           FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error",
                               "Sum of IH_SOB and BO_SOB must be equal to 100 or less than 100!!");
                           throw new ValidatorException(msg);
                       }
                   } 
            
            }


    public void setBindPartCode(RichInputText bindPartCode) {
        this.bindPartCode = bindPartCode;
    }

    public RichInputText getBindPartCode() {
        return bindPartCode;
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent)
    {
        if(dialogEvent.getOutcome().name().equals("ok"))
            {
            ADFUtils.findOperation("Delete").execute();
            System.out.println("Record Delete Successfully");
            
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");   
                Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                FacesContext fc = FacesContext.getCurrentInstance();   
                fc.addMessage(null, Message);   
            }
        AdfFacesContext.getCurrentInstance().addPartialTarget(itemUnitStoreTableBinding);

    }

    public void setItemUnitStoreTableBinding(RichTable itemUnitStoreTableBinding) {
        this.itemUnitStoreTableBinding = itemUnitStoreTableBinding;
    }

    public RichTable getItemUnitStoreTableBinding() {
        return itemUnitStoreTableBinding;
    }

    public void setValidUptoBinding(RichInputDate validUptoBinding) {
        this.validUptoBinding = validUptoBinding;
    }

    public RichInputDate getValidUptoBinding() {
        return validUptoBinding;
    }

    public void setUnitCodeBinding(RichInputText unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputText getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setLastModByBinding(RichInputText lastModByBinding) {
        this.lastModByBinding = lastModByBinding;
    }

    public RichInputText getLastModByBinding() {
        return lastModByBinding;
    }

    public void setModifiedDateBinding(RichInputDate modifiedDateBinding) {
        this.modifiedDateBinding = modifiedDateBinding;
    }

    public RichInputDate getModifiedDateBinding() {
        return modifiedDateBinding;
    }

    public void setRevenueNoBinding(RichInputText revenueNoBinding) {
        this.revenueNoBinding = revenueNoBinding;
    }

    public RichInputText getRevenueNoBinding() {
        return revenueNoBinding;
    }

    public void setGstCodeBinding(RichInputText gstCodeBinding) {
        this.gstCodeBinding = gstCodeBinding;
    }

    public RichInputText getGstCodeBinding() {
        return gstCodeBinding;
    }

    public void setGetMyPageRootComponent(RichPanelGroupLayout getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelGroupLayout getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
            try {
                Method method1 =
                    component.getClass().getMethod("setDisabled", boolean.class);
                if (method1 != null) {
                    method1.invoke(component, valueComponent);
                }
            } catch (NoSuchMethodException e) {
                try {
                    Method method =
                        component.getClass().getMethod("setReadOnly", boolean.class);
                    if (method != null) {
                        method.invoke(component, valueComponent);
                    }
                } catch (Exception e1) {
                    // e.printStackTrace();//silently eat this exception.
                }


            } catch (Exception e) {
                // e.printStackTrace();//silently eat this exception.
            }
            List<UIComponent> childComponents = component.getChildren();
            for (UIComponent comp : childComponents) {
                makeComponentHierarchyReadOnly(comp, valueComponent);
            }
        }
    private void cevmodecheck(){
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }
    public void cevModeDisableComponent(String mode) {
    //            FacesContext fctx = FacesContext.getCurrentInstance();
    //            ELContext elctx = fctx.getELContext();
    //            Application jsfApp = fctx.getApplication();
    //            //create a ValueExpression that points to the ADF binding layer
    //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
    //            //
    //            ValueExpression valueExpr = exprFactory.createValueExpression(
    //                                         elctx,
    //                                         "#{pageFlowScope.mode=='E'}",
    //                                          Object.class
    //                                         );
    //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
        if (mode.equals("E")) {
            getGstCodeBinding().setDisabled(true);
            getLastModByBinding().setDisabled(true);
            getRevenueNoBinding().setDisabled(true);
            getModifiedDateBinding().setDisabled(true);
            getValidUptoBinding().setDisabled(true);
            getMeasurementUnitbinding().setDisabled(true);          
            getHeaderEditBinding().setDisabled(true);
            getDetailcreateBinding().setDisabled(false);
           // getDetaildeleteBinding().setDisabled(false);
            getBindPartCode().setDisabled(true);
            getGroupDescBinding().setDisabled(true);
            getSubGroupDescBinding().setDisabled(true);
            getSubGroupCdBinding().setDisabled(true);
            getGroupCodeBinding().setDisabled(true);
        } else if (mode.equals("C")) {
            getGroupDescBinding().setDisabled(true);
            getSubGroupDescBinding().setDisabled(true);
            getGstCodeBinding().setDisabled(true);
            getLastModByBinding().setDisabled(true);
            getRevenueNoBinding().setDisabled(true);
            getModifiedDateBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            getDetailcreateBinding().setDisabled(false);
            getBindPartCode().setDisabled(true);
          //  getDetaildeleteBinding().setDisabled(false);
        } else if (mode.equals("V")) {
            getDetailcreateBinding().setDisabled(true);
            getPurchaseBinding().setDisabled(false);
            getQualityBinding().setDisabled(false);
            getMrpBind().setDisabled(false);
            getSaleBind().setDisabled(false);
            getFinanceBind().setDisabled(false);
            getCostingBind().setDisabled(false);
            getLocMappingBinding().setDisabled(false);
          //  getDetaildeleteBinding().setDisabled(true);
        } 
    }

    public void setDetailcreateBinding(RichButton detailcreateBinding) {
        this.detailcreateBinding = detailcreateBinding;
    }

    public RichButton getDetailcreateBinding() {
        return detailcreateBinding;
    }

//    public void setDetaildeleteBinding(RichButton detaildeleteBinding) {
//        this.detaildeleteBinding = detaildeleteBinding;
//    }
//
//    public RichButton getDetaildeleteBinding() {
//        return detaildeleteBinding;
//    }


    public void EditButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void partCodeValidator(FacesContext facesContext, UIComponent uIComponent, Object object) 
    {
    if(object != null)
    {
        if(object.toString().contains(" "))
        {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Part Code Not Accepted Space.", null));
        }
        
    }
    

    }

    public void partDescriptionValidator(FacesContext facesContext, UIComponent uIComponent, Object object) 
    {
        if(object != null)
        {
            if(object.toString().startsWith(" ") || object.toString().endsWith(" "))
            {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Description Should Not Be Start And End With Space.", null));
            }
            
        }


    }

    public void setGroupDescBinding(RichInputText groupDescBinding) {
        this.groupDescBinding = groupDescBinding;
    }

    public RichInputText getGroupDescBinding() {
        return groupDescBinding;
    }

    public void setSubGroupDescBinding(RichInputText subGroupDescBinding) {
        this.subGroupDescBinding = subGroupDescBinding;
    }

    public RichInputText getSubGroupDescBinding() {
        return subGroupDescBinding;
    }

    public void setGroupCodeBinding(RichInputComboboxListOfValues groupCodeBinding) {
        this.groupCodeBinding = groupCodeBinding;
    }

    public RichInputComboboxListOfValues getGroupCodeBinding() {
        return groupCodeBinding;
    }

    public void setSubGroupCdBinding(RichInputComboboxListOfValues subGroupCdBinding) {
        this.subGroupCdBinding = subGroupCdBinding;
    }

    public RichInputComboboxListOfValues getSubGroupCdBinding() {
        return subGroupCdBinding;
    }

    public void setMeasurementUnitbinding(RichSelectOneChoice measurementUnitbinding) {
        this.measurementUnitbinding = measurementUnitbinding;
    }

    public RichSelectOneChoice getMeasurementUnitbinding() {
        return measurementUnitbinding;
    }

    public void setPurchaseBinding(RichShowDetailItem purchaseBinding) {
        this.purchaseBinding = purchaseBinding;
    }

    public RichShowDetailItem getPurchaseBinding() {
        return purchaseBinding;
    }

    public void setQualityBinding(RichShowDetailItem qualityBinding) {
        this.qualityBinding = qualityBinding;
    }

    public RichShowDetailItem getQualityBinding() {
        return qualityBinding;
    }

    public void setMrpBind(RichShowDetailItem mrpBind) {
        this.mrpBind = mrpBind;
    }

    public RichShowDetailItem getMrpBind() {
        return mrpBind;
    }

    public void setSaleBind(RichShowDetailItem saleBind) {
        this.saleBind = saleBind;
    }

    public RichShowDetailItem getSaleBind() {
        return saleBind;
    }

    public void setFinanceBind(RichShowDetailItem financeBind) {
        this.financeBind = financeBind;
    }

    public RichShowDetailItem getFinanceBind() {
        return financeBind;
    }

    public void setCostingBind(RichShowDetailItem costingBind) {
        this.costingBind = costingBind;
    }

    public RichShowDetailItem getCostingBind() {
        return costingBind;
    }

    public void setLocMappingBinding(RichShowDetailItem locMappingBinding) {
        this.locMappingBinding = locMappingBinding;
    }

    public RichShowDetailItem getLocMappingBinding() {
        return locMappingBinding;
    }
}
