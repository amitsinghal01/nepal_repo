package terms.eng.setup.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import javax.faces.validator.ValidatorException;

import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class PartMasterConsolidateBean {
    private RichInputText bindPartCode;
    private RichTable unitStoreTableBinding;
    private RichTable vendorItemTableBinding;
    private RichTable purchaseMapTablebinding;
    private RichTable customerMapTableBinding;
    private RichPopup alternatePartPopupBinding;
    private RichTable alternateTableBinding;
    private RichSelectOneChoice materialGradeBinding;
    private RichInputComboboxListOfValues modelCdBinding;
    private RichInputComboboxListOfValues techCodeBinding;
    private RichInputComboboxListOfValues packCodeBinding;
    private RichOutputText bindingOutputText;
    private RichButton headerEditBinding;
    private RichButton detailcreateBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichInputText partrevnoBinding;
    private RichInputDate wefBinding;
    private RichInputText gstCodeBinding;
    private RichInputText modifiedByBinding;
    private RichInputDate prepareDateBinding;
    private RichInputText currentRateBinding;
    private RichInputText cityCodeBinding;
    private RichShowDetailItem infobuttonbinding;
    private RichShowDetailItem sobButtonBinding;
    private RichShowDetailItem purchaseMappingButtonBinding;
    private RichShowDetailItem customerMappingButtonBinding;
    private RichButton alternatePartButtonBinding;
    private RichButton sobCreateButtonBinding;
    private RichButton purchaseMappingCreateBinding;
    private RichButton customerMappingCreateBinding;
    private RichButton alternatePartCreateButtonBinding;
    private RichInputText totalSobBinding;
    private String Msg="CM";
    
    public PartMasterConsolidateBean() {
    }

    public void savePartAL(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedByNew("PartMasterConsolidateVO1Iterator","LastUpdatedBy");
        oracle.jbo.domain.Number TotalSob=new oracle.jbo.domain.Number(0);
        
        Long RowCnt=(Long)ADFUtils.evaluateEL("#{bindings.VendorItemConsolidateVO1Iterator.estimatedRowCount}");
        System.out.println("Tota Sob is===>"+totalSobBinding.getValue());
        if(totalSobBinding.getValue()!=null)
        {
        TotalSob=(oracle.jbo.domain.Number)totalSobBinding.getValue();   
        }
        System.out.println("TOTAL SOB IS=>"+TotalSob);
        System.out.println("Total Sob is"+TotalSob.compareTo(new oracle.jbo.domain.Number(100)));
        System.out.println("Total Sob is"+TotalSob.compareTo(new oracle.jbo.domain.Number(101)));

        if( (RowCnt >1 && TotalSob.compareTo(new oracle.jbo.domain.Number(101))==-1) || RowCnt<=1) 
        {
            if(Msg.equalsIgnoreCase("EM"))
                    {
                        ADFUtils.findOperation("Commit").execute();
                        FacesMessage Message = new FacesMessage("Record Updated Successfully.");
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);
                        FacesContext fc = FacesContext.getCurrentInstance();
                        fc.addMessage(null, Message);
                    } 
                else 
                {
                
                
                            ADFUtils.findOperation("Commit").execute();
                            FacesMessage Message = new FacesMessage("Record Saved Successfully. New Part Code is " + bindPartCode.getValue().toString());
                            Message.setSeverity(FacesMessage.SEVERITY_INFO);
                            FacesContext fc = FacesContext.getCurrentInstance();
                            fc.addMessage(null, Message);
                            Msg="EM";
                        }
        }
        
        else
        {
        ADFUtils.showMessage("Total SOB must be 100%",0);    
        }
        
  
    }



    public void setBindPartCode(RichInputText bindPartCode) {
        this.bindPartCode = bindPartCode;
    }

    public RichInputText getBindPartCode() {
        return bindPartCode;
    }

    public void deleteUnitStorePopupDialogDL(DialogEvent dialogEvent) {

        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }

        AdfFacesContext.getCurrentInstance().addPartialTarget(unitStoreTableBinding);
    }

    public void setUnitStoreTableBinding(RichTable unitStoreTableBinding) {
        this.unitStoreTableBinding = unitStoreTableBinding;
    }

    public RichTable getUnitStoreTableBinding() {
        return unitStoreTableBinding;
    }

    public void deleteVendorItemPopupDialogDL(DialogEvent dialogEvent) {

        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete1").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }

        AdfFacesContext.getCurrentInstance().addPartialTarget(vendorItemTableBinding);
    }

    public void setVendorItemTableBinding(RichTable vendorItemTableBinding) {
        this.vendorItemTableBinding = vendorItemTableBinding;
    }

    public RichTable getVendorItemTableBinding() {
        return vendorItemTableBinding;
    }

    public void deletePurchaseMapPopupDialogDL(DialogEvent dialogEvent) {

        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete2").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(purchaseMapTablebinding);
    }

    public void setPurchaseMapTablebinding(RichTable purchaseMapTablebinding) {
        this.purchaseMapTablebinding = purchaseMapTablebinding;
    }

    public RichTable getPurchaseMapTablebinding() {
        return purchaseMapTablebinding;
    }

    public void deleteCustomerMapPopupDialogDL(DialogEvent dialogEvent) {

        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete3").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(customerMapTableBinding);
    }

    public void setCustomerMapTableBinding(RichTable customerMapTableBinding) {
        this.customerMapTableBinding = customerMapTableBinding;
    }

    public RichTable getCustomerMapTableBinding() {
        return customerMapTableBinding;
    }

    public void setAlternatePartPopupBinding(RichPopup alternatePartPopupBinding) {
        this.alternatePartPopupBinding = alternatePartPopupBinding;
    }

    public RichPopup getAlternatePartPopupBinding() {
        return alternatePartPopupBinding;
    }

    public void AltenatePartDialogListener(DialogEvent de) {
        if (de.getOutcome() == DialogEvent.Outcome.ok) {

            alternatePartPopupBinding.hide();
        }
    }

    public void setAlternateTableBinding(RichTable alternateTableBinding) {
        this.alternateTableBinding = alternateTableBinding;
    }

    public RichTable getAlternateTableBinding() {
        return alternateTableBinding;
    }

    public void deleteAlternatePartPopupDialogDL(DialogEvent dialogEvent) {

        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete4").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(alternateTableBinding);
    }

    public void setMaterialGradeBinding(RichSelectOneChoice materialGradeBinding) {
        this.materialGradeBinding = materialGradeBinding;
    }

    public RichSelectOneChoice getMaterialGradeBinding() {
        return materialGradeBinding;
    }

    public void PartTypVCE(ValueChangeEvent vce) {
        if (vce != null) {
            System.out.println("value at vce for patruert typ--->" + vce.getNewValue());
            String rm = vce.getNewValue().toString();
            String RM = "RM";
            if (!rm.equalsIgnoreCase("RM")) {

                getMaterialGradeBinding().setValue(null);
                AdfFacesContext.getCurrentInstance().addPartialTarget(getMaterialGradeBinding());
            }
       }
    }


    public void currentRateVCL(ValueChangeEvent valueChangeEvent) {
        ADFUtils.findOperation("getPartCurrentRate").execute();
    }

    public void setModelCdBinding(RichInputComboboxListOfValues modelCdBinding) {
        this.modelCdBinding = modelCdBinding;
    }

    public RichInputComboboxListOfValues getModelCdBinding() {
        return modelCdBinding;
    }

    public void setTechCodeBinding(RichInputComboboxListOfValues techCodeBinding) {
        this.techCodeBinding = techCodeBinding;
    }

    public RichInputComboboxListOfValues getTechCodeBinding() {
        return techCodeBinding;
    }

    public void setPackCodeBinding(RichInputComboboxListOfValues packCodeBinding) {
        this.packCodeBinding = packCodeBinding;
    }

    public RichInputComboboxListOfValues getPackCodeBinding() {
        return packCodeBinding;
    }

    public void categoryVCL(ValueChangeEvent vce) {
        if(vce.getNewValue().equals(null)){
            getTechCodeBinding().setDisabled(true);
        }else
        {
            getTechCodeBinding().setDisabled(false);
        }
        
//                        
//                    System.out.println("value at vce for category--->"+vce.getNewValue());
//if(getModelCdBinding().getValue()==null && vce!=null)
//                    {
//                        getTechCodeBinding().setValue(null);
//                    }
//                            AdfFacesContext.getCurrentInstance().addPartialTarget(getTechCodeBinding());
//                        }

  }

    public void itemTypeVCL(ValueChangeEvent vce) {
        //        if(vce==null){
        //        System.out.println("value at vce for Item Type--->"+vce.getNewValue());
        //        if(getTechCodeBinding()==null)
        //        {
        //        getPackCodeBinding().setDisabled(true);
        //        }
        //                    AdfFacesContext.getCurrentInstance().addPartialTarget(getPackCodeBinding());
        //    }
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void EditButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }

    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }

    public void cevModeDisableComponent(String mode) {

        if (mode.equals("E")) {
            getPartrevnoBinding().setDisabled(true);
            getBindPartCode().setDisabled(true);
           // getWefBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            getModifiedByBinding().setDisabled(true);
            getPrepareDateBinding().setDisabled(true);
            getCurrentRateBinding().setDisabled(true);
            getDetailcreateBinding().setDisabled(false);
            getCityCodeBinding().setDisabled(true);
            getSobCreateButtonBinding().setDisabled(false);
            getCustomerMappingCreateBinding().setDisabled(false);
            getPurchaseMappingCreateBinding().setDisabled(false);
           getTechCodeBinding().setDisabled(true);
            getModelCdBinding().setDisabled(true);
            Msg="EM";

            
        } else if (mode.equals("C")) {
            getPartrevnoBinding().setDisabled(true);
            getGstCodeBinding().setDisabled(true);
            getWefBinding().setDisabled(true);
            getModifiedByBinding().setDisabled(true);
            getPrepareDateBinding().setDisabled(true);
            getCurrentRateBinding().setDisabled(true);
            getDetailcreateBinding().setDisabled(false);
            getCityCodeBinding().setDisabled(true);
            getSobCreateButtonBinding().setDisabled(false);
            getCustomerMappingCreateBinding().setDisabled(false);
            getPurchaseMappingCreateBinding().setDisabled(false);
            getTechCodeBinding().setDisabled(true);
          
           
        } else if (mode.equals("V")) {
            getDetailcreateBinding().setDisabled(true);
            getSobButtonBinding().setDisabled(false);
            getCustomerMappingButtonBinding().setDisabled(false);
            getInfobuttonbinding().setDisabled(false);
            getPurchaseMappingButtonBinding().setDisabled(false);
            getAlternatePartButtonBinding().setDisabled(true);
            getSobCreateButtonBinding().setDisabled(true);
            getCustomerMappingCreateBinding().setDisabled(true);
            getPurchaseMappingCreateBinding().setDisabled(true);
            
            
        }
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setDetailcreateBinding(RichButton detailcreateBinding) {
        this.detailcreateBinding = detailcreateBinding;
    }

    public RichButton getDetailcreateBinding() {
        return detailcreateBinding;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setPartrevnoBinding(RichInputText partrevnoBinding) {
        this.partrevnoBinding = partrevnoBinding;
    }

    public RichInputText getPartrevnoBinding() {
        return partrevnoBinding;
    }

    public void setWefBinding(RichInputDate wefBinding) {
        this.wefBinding = wefBinding;
    }

    public RichInputDate getWefBinding() {
        return wefBinding;
    }

    public void setGstCodeBinding(RichInputText gstCodeBinding) {
        this.gstCodeBinding = gstCodeBinding;
    }

    public RichInputText getGstCodeBinding() {
        return gstCodeBinding;
    }

    public void setModifiedByBinding(RichInputText modifiedByBinding) {
        this.modifiedByBinding = modifiedByBinding;
    }

    public RichInputText getModifiedByBinding() {
        return modifiedByBinding;
    }

    public void setPrepareDateBinding(RichInputDate prepareDateBinding) {
        this.prepareDateBinding = prepareDateBinding;
    }

    public RichInputDate getPrepareDateBinding() {
        return prepareDateBinding;
    }

    public void setCurrentRateBinding(RichInputText currentRateBinding) {
        this.currentRateBinding = currentRateBinding;
    }

    public RichInputText getCurrentRateBinding() {
        return currentRateBinding;
    }

    public void setCityCodeBinding(RichInputText cityCodeBinding) {
        this.cityCodeBinding = cityCodeBinding;
    }

    public RichInputText getCityCodeBinding() {
        return cityCodeBinding;
    }

    public void setInfobuttonbinding(RichShowDetailItem infobuttonbinding) {
        this.infobuttonbinding = infobuttonbinding;
    }

    public RichShowDetailItem getInfobuttonbinding() {
        return infobuttonbinding;
    }

    public void setSobButtonBinding(RichShowDetailItem sobButtonBinding) {
        this.sobButtonBinding = sobButtonBinding;
    }

    public RichShowDetailItem getSobButtonBinding() {
        return sobButtonBinding;
    }

    public void setPurchaseMappingButtonBinding(RichShowDetailItem purchaseMappingButtonBinding) {
        this.purchaseMappingButtonBinding = purchaseMappingButtonBinding;
    }

    public RichShowDetailItem getPurchaseMappingButtonBinding() {
        return purchaseMappingButtonBinding;
    }

    public void setCustomerMappingButtonBinding(RichShowDetailItem customerMappingButtonBinding) {
        this.customerMappingButtonBinding = customerMappingButtonBinding;
    }

    public RichShowDetailItem getCustomerMappingButtonBinding() {
        return customerMappingButtonBinding;
    }

    public void setAlternatePartButtonBinding(RichButton alternatePartButtonBinding) {
        this.alternatePartButtonBinding = alternatePartButtonBinding;
    }

    public RichButton getAlternatePartButtonBinding() {
        return alternatePartButtonBinding;
    }

    public void setSobCreateButtonBinding(RichButton sobCreateButtonBinding) {
        this.sobCreateButtonBinding = sobCreateButtonBinding;
    }

    public RichButton getSobCreateButtonBinding() {
        return sobCreateButtonBinding;
    }

    public void setPurchaseMappingCreateBinding(RichButton purchaseMappingCreateBinding) {
        this.purchaseMappingCreateBinding = purchaseMappingCreateBinding;
    }

    public RichButton getPurchaseMappingCreateBinding() {
        return purchaseMappingCreateBinding;
    }

    public void setCustomerMappingCreateBinding(RichButton customerMappingCreateBinding) {
        this.customerMappingCreateBinding = customerMappingCreateBinding;
    }

    public RichButton getCustomerMappingCreateBinding() {
        return customerMappingCreateBinding;
    }

    public void setAlternatePartCreateButtonBinding(RichButton alternatePartCreateButtonBinding) {
        this.alternatePartCreateButtonBinding = alternatePartCreateButtonBinding;
    }

    public RichButton getAlternatePartCreateButtonBinding() {
        return alternatePartCreateButtonBinding;
    }

    public void SobValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        {
            
//            if((Long)ADFUtils.evaluateEL("#{bindings.VendorItemConsolidateVO1Iterator.estimatedRowCount}")>1)
//            {
//                if(totalSobBinding.getValue() !=null)
//                {
//                    oracle.jbo.domain.Number TotalSob=(oracle.jbo.domain.Number)totalSobBinding.getValue();
//                    System.out.println("Result is=>"+TotalSob.compareTo(new oracle.jbo.domain.Number(100)));
//
//                    if(TotalSob.compareTo(new oracle.jbo.domain.Number(100))==1)
//                    {
//                    
//                       throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Sob should be less than or equal to 100.", null));
//                    }
//                }
//            }
            
//            if(object!=null && object.toString().trim().length()>=2 && stateGstBinding.getValue()!=null)
//            {
//            if(!object.toString().substring(0, 2).equals(stateGstBinding.getValue().toString().substring(0, 2)))
//            {
//            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "First Two Characters Of GST Regd. No. Is Equals First Two Characters Of State GST Code.", null));
//            }
//            }

        }

    }

    public void setTotalSobBinding(RichInputText totalSobBinding) {
        this.totalSobBinding = totalSobBinding;
    }

    public RichInputText getTotalSobBinding() {
        return totalSobBinding;
    }
    
    public String saveAndCloseButton() {
        ADFUtils.setLastUpdatedByNew("PartMasterConsolidateVO1Iterator","LastUpdatedBy");
        oracle.jbo.domain.Number TotalSob=new oracle.jbo.domain.Number(0);
        
        Long RowCnt=(Long)ADFUtils.evaluateEL("#{bindings.VendorItemConsolidateVO1Iterator.estimatedRowCount}");
        System.out.println("Tota Sob is===>"+totalSobBinding.getValue());
        if(totalSobBinding.getValue()!=null)
        {
        TotalSob=(oracle.jbo.domain.Number)totalSobBinding.getValue();   
        }
        System.out.println("TOTAL SOB IS=>"+TotalSob);
        System.out.println("Total Sob is"+TotalSob.compareTo(new oracle.jbo.domain.Number(100)));
        System.out.println("Total Sob is"+TotalSob.compareTo(new oracle.jbo.domain.Number(101)));

        if( (RowCnt >1 && TotalSob.compareTo(new oracle.jbo.domain.Number(101))==-1) || RowCnt<=1) 
        {
            if(Msg.equalsIgnoreCase("EM"))
                    {
                        ADFUtils.findOperation("Commit").execute();
                        FacesMessage Message = new FacesMessage("Record Updated Successfully.");
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);
                        FacesContext fc = FacesContext.getCurrentInstance();
                        fc.addMessage(null, Message);
                        return "save";
                    } 
                else 
                {
                
                
                            ADFUtils.findOperation("Commit").execute();
                            FacesMessage Message = new FacesMessage("Record Saved Successfully. New Part Code is " + bindPartCode.getValue().toString());
                            Message.setSeverity(FacesMessage.SEVERITY_INFO);
                            FacesContext fc = FacesContext.getCurrentInstance();
                            fc.addMessage(null, Message);
                            Msg="EM";
                            return "save";

                        }
        }
        
        else
        {
        ADFUtils.showMessage("Total SOB must be 100%",0);  
            return null;

        }
        
       // return null;

    }
    
    
    
}
