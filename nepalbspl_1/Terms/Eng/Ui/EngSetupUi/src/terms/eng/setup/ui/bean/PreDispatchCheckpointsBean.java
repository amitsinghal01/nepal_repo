package terms.eng.setup.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;
import javax.faces.validator.ValidatorException;

import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class PreDispatchCheckpointsBean {
    private RichTable preDispatchDetailTableBinding;
    private RichInputComboboxListOfValues productCodeBinding;
    private RichInputText revNoBinding;
    private RichInputComboboxListOfValues productCdBinding;
    private RichInputText checkptSeqnoBinding;
    private RichButton headerEditBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichOutputText bindingOutputText;
    private RichButton detatilcreateBinding;
 //   private RichButton detaildeleteBinding;
    private RichInputText bindingCopyDescription;
    private RichInputText bindingCopyCustCd;
    private RichInputText bindingCopyCityCd;
    private RichInputText bindingCopyPlantCd;
    private RichButton bindingCopyButton;
    private RichInputComboboxListOfValues checkedByBinding;
    private RichInputComboboxListOfValues approvedByBinding;
    private RichInputComboboxListOfValues drnByBinding;
    private RichInputDate chcDateBinding;
    private RichInputDate appDateBinding;
    private RichInputDate drnDateBinding;
    private RichInputText noteBinding;
    private RichButton editButtonBinding;
    private RichInputText baseLimitBinding;
    private RichInputText upperLimitBinding;
    private RichInputText lowerLimitBinding;
    private RichButton detailDeleteButtonBinding;
    private RichButton limitButtonBinding;
    private RichInputComboboxListOfValues customerCodeBinding;
    private RichInputComboboxListOfValues customerTypeBinding;
    private RichColumn customerCdDtlBinding;
    private RichSelectOneChoice calculativeFlagBinding;
    private RichInputComboboxListOfValues uomOfLimitsBinding;

    public PreDispatchCheckpointsBean() {
    }

    public void copyValueButtonAL(ActionEvent actionEvent) 
    {
            if(productCdBinding.getValue() != null)
            {
            System.out.println("Before ");
            OperationBinding opb=ADFUtils.findOperation("populatePreDispatchCheckpointsData");
            Object obj=opb.execute();
            System.out.println("Result is===> "+opb.getResult());
            if(opb.getResult()!=null && opb.getResult().toString().equalsIgnoreCase("N"))
            {
                FacesMessage Message = new FacesMessage("Record Not Found For This Product.");  
                Message.setSeverity(FacesMessage.SEVERITY_ERROR);  
                FacesContext fc = FacesContext.getCurrentInstance();  
                fc.addMessage(null, Message);  
                
            }
//                getProductCdBinding().setDisabled(true);
                getProductCodeBinding().setDisabled(true);
                getNoteBinding().setDisabled(true);
                getEditButtonBinding().setDisabled(true);
                getCheckedByBinding().setDisabled(true);
                //getBindingCopyButton().setDisabled(true);
                getDetatilcreateBinding().setDisabled(true);
                getCustomerCodeBinding().setDisabled(true);
            }
                else
                {
                System.out.println("Inside Else Block");
                     ADFUtils.showMessage("Please Select Product Code Copy Value.",0); 
                System.out.println("After message Block");
                }
            
//            Long RowId=(Long)ADFUtils.evaluateEL("#{bindings.PreDispatchCheckPointsDetailVO1Iterator.estimatedRowCount}");
//            if(RowId >= 1)
//            {
//                getProductCdBinding().setDisabled(true);
//                getProductCodeBinding().setDisabled(true);
//                getNoteBinding().setDisabled(true);
//                getEditButtonBinding().setDisabled(true);
//                getCheckedByBinding().setDisabled(true);
//                getBindingCopyButton().setDisabled(true);
//                getDetatilcreateBinding().setDisabled(true);
//            }
            
        
    }
    public void saveButtonAL(ActionEvent actionEvent)
    {
        ADFUtils.setLastUpdatedByNew("PreDispatchCheckPointsHeaderVO1Iterator","LastUpdatedBy");
//        ADFUtils.findOperation("setCustomerCodeInPDI").execute();
//    ADFUtils.findOperation("setPreDispatchChkPtsSequnceNumber").execute();
         OperationBinding op = ADFUtils.findOperation("saveGenerateErrorForPreDispatchCheckpoints");
                 Object obj = op.execute();
                 
                 System.out.println("RESULT IS************"+op.getResult());
                 
         if(op.getResult().equals("Y"))
         {
        if((Long)ADFUtils.evaluateEL("#{bindings.PreDispatchCheckPointsDetailVO1Iterator.estimatedRowCount}")>0){
     Integer i=0;
     i=(Integer)bindingOutputText.getValue();
     System.out.println("EDIT TRANS VALUE"+i);
        if(i.equals(0))
        {
           ADFUtils.findOperation("Commit").execute();
           FacesMessage Message = new FacesMessage("Record Save Successfully");   
           Message.setSeverity(FacesMessage.SEVERITY_INFO);   
           FacesContext fc = FacesContext.getCurrentInstance();   
           fc.addMessage(null, Message);      
            
        }
        else 
        {
            ADFUtils.findOperation("Commit").execute();
            FacesMessage Message = new FacesMessage("Record Updated Successfully");   
            Message.setSeverity(FacesMessage.SEVERITY_INFO);   
            FacesContext fc = FacesContext.getCurrentInstance();   
            fc.addMessage(null, Message);      
        }
     }
         }
     else {
         ADFUtils.showMessage("Please Fill Either Upper Limit or Either Lower Limit", 0);
     }
    }
    public void deletePopupDialogDL(DialogEvent dialogEvent)
    {
        if(dialogEvent.getOutcome().name().equals("ok"))
                    {
                    ADFUtils.findOperation("Delete").execute();
                    //ADFUtils.findOperation("Commit").execute();    //Unable on Search page.
                    System.out.println("Record Delete Successfully");
                   
                        FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                        FacesContext fc = FacesContext.getCurrentInstance();  
                        fc.addMessage(null, Message);  
                   
                    }

                AdfFacesContext.getCurrentInstance().addPartialTarget(preDispatchDetailTableBinding);
    }

    public void setPreDispatchDetailTableBinding(RichTable preDispatchDetailTableBinding) {
        this.preDispatchDetailTableBinding = preDispatchDetailTableBinding;
    }

    public RichTable getPreDispatchDetailTableBinding() {
        return preDispatchDetailTableBinding;
    }
    
    
    
    public String resolvEl(String data)
    {
                      FacesContext fc = FacesContext.getCurrentInstance();
                      Application app = fc.getApplication();
                      ExpressionFactory elFactory = app.getExpressionFactory();
                      ELContext elContext = fc.getELContext();
                      ValueExpression valueExp = elFactory.createValueExpression(elContext, data, Object.class);
                      String Message=valueExp.getValue(elContext).toString();
                      return Message;
    }


    public void setProductCodeBinding(RichInputComboboxListOfValues productCodeBinding) {
        this.productCodeBinding = productCodeBinding;
    }

    public RichInputComboboxListOfValues getProductCodeBinding() {
        return productCodeBinding;
    }

    public void setRevNoBinding(RichInputText revNoBinding) {
        this.revNoBinding = revNoBinding;
    }

    public RichInputText getRevNoBinding() {
        return revNoBinding;
    }

    public void setProductCdBinding(RichInputComboboxListOfValues productCdBinding) {
        this.productCdBinding = productCdBinding;
    }

    public RichInputComboboxListOfValues getProductCdBinding() {
        return productCdBinding;
    }

    public void setCheckptSeqnoBinding(RichInputText checkptSeqnoBinding) {
        this.checkptSeqnoBinding = checkptSeqnoBinding;
    }

    public RichInputText getCheckptSeqnoBinding() {
        return checkptSeqnoBinding;
    }

    public void EditButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        
        cevmodecheck();
        return bindingOutputText;
    }
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
            try {
                Method method1 =
                    component.getClass().getMethod("setDisabled", boolean.class);
                if (method1 != null) {
                    method1.invoke(component, valueComponent);
                }
            } catch (NoSuchMethodException e) {
                try {
                    Method method =
                        component.getClass().getMethod("setReadOnly", boolean.class);
                    if (method != null) {
                        method.invoke(component, valueComponent);
                    }
                } catch (Exception e1) {
                    // e.printStackTrace();//silently eat this exception.
                }


            } catch (Exception e) {
                // e.printStackTrace();//silently eat this exception.
            }
            List<UIComponent> childComponents = component.getChildren();
            for (UIComponent comp : childComponents) {
                makeComponentHierarchyReadOnly(comp, valueComponent);
            }
        }
    
    private void cevmodecheck(){
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }
    public void cevModeDisableComponent(String mode) {
    //            FacesContext fctx = FacesContext.getCurrentInstance();
    //            ELContext elctx = fctx.getELContext();
    //            Application jsfApp = fctx.getApplication();
    //            //create a ValueExpression that points to the ADF binding layer
    //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
    //            //
    //            ValueExpression valueExpr = exprFactory.createValueExpression(
    //                                         elctx,
    //                                         "#{pageFlowScope.mode=='E'}",
    //                                          Object.class
    //                                         );
    //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
        if (mode.equals("E")) {
            getCheckedByBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            getProductCodeBinding().setDisabled(true);
            getProductCdBinding().setDisabled(true);
            getCheckptSeqnoBinding().setDisabled(true);
            getDetatilcreateBinding().setDisabled(false);
            //getDetaildeleteBinding().setDisabled(false);
            getRevNoBinding().setDisabled(true);
            getBindingCopyDescription().setDisabled(true);
            getBindingCopyCustCd().setDisabled(true);
            getBindingCopyCityCd().setDisabled(true);
            getBindingCopyPlantCd().setDisabled(true);
            getBindingCopyButton().setDisabled(true);
            getChcDateBinding().setDisabled(true);
            getAppDateBinding().setDisabled(true);
            getDrnDateBinding().setDisabled(true);
            getCheckptSeqnoBinding().setDisabled(true);
            getCustomerTypeBinding().setDisabled(true);
            getCustomerCodeBinding().setDisabled(true);
            
        } else if (mode.equals("C")) {
            getApprovedByBinding().setDisabled(true);
            getDrnByBinding().setDisabled(true);
            getBindingCopyDescription().setDisabled(true);
            getBindingCopyCustCd().setDisabled(true);
            getBindingCopyCityCd().setDisabled(true);
            getBindingCopyPlantCd().setDisabled(true);
            getRevNoBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            getDetatilcreateBinding().setDisabled(false);
            getChcDateBinding().setDisabled(true);
            getAppDateBinding().setDisabled(true);
          getDrnDateBinding().setDisabled(true);
          getCheckptSeqnoBinding().setDisabled(true);
        } else if (mode.equals("V")) {
            getLimitButtonBinding().setDisabled(false);
            getDetatilcreateBinding().setDisabled(true);
            getDetailDeleteButtonBinding().setDisabled(true);
        } 
    }



    public void setDetatilcreateBinding(RichButton detatilcreateBinding) {
        this.detatilcreateBinding = detatilcreateBinding;
    }

    public RichButton getDetatilcreateBinding() {
        return detatilcreateBinding;
    }
//
//    public void setDetaildeleteBinding(RichButton detaildeleteBinding) {
//        this.detaildeleteBinding = detaildeleteBinding;
//    }
//
//    public RichButton getDetaildeleteBinding() {
//        return detaildeleteBinding;
//    }

    public void setBindingCopyDescription(RichInputText bindingCopyDescription) {
        this.bindingCopyDescription = bindingCopyDescription;
    }

    public RichInputText getBindingCopyDescription() {
        return bindingCopyDescription;
    }

    public void setBindingCopyCustCd(RichInputText bindingCopyCustCd) {
        this.bindingCopyCustCd = bindingCopyCustCd;
    }

    public RichInputText getBindingCopyCustCd() {
        return bindingCopyCustCd;
    }

    public void setBindingCopyCityCd(RichInputText bindingCopyCityCd) {
        this.bindingCopyCityCd = bindingCopyCityCd;
    }

    public RichInputText getBindingCopyCityCd() {
        return bindingCopyCityCd;
    }

    public void setBindingCopyPlantCd(RichInputText bindingCopyPlantCd) {
        this.bindingCopyPlantCd = bindingCopyPlantCd;
    }

    public RichInputText getBindingCopyPlantCd() {
        return bindingCopyPlantCd;
    }

    public void setBindingCopyButton(RichButton bindingCopyButton) {
        this.bindingCopyButton = bindingCopyButton;
    }

    public RichButton getBindingCopyButton() {
        return bindingCopyButton;
    }

    public void setCheckedByBinding(RichInputComboboxListOfValues checkedByBinding) {
        this.checkedByBinding = checkedByBinding;
    }

    public RichInputComboboxListOfValues getCheckedByBinding() {
        return checkedByBinding;
    }

    public void setApprovedByBinding(RichInputComboboxListOfValues approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputComboboxListOfValues getApprovedByBinding() {
        return approvedByBinding;
    }

    public void setDrnByBinding(RichInputComboboxListOfValues drnByBinding) {
        this.drnByBinding = drnByBinding;
    }

    public RichInputComboboxListOfValues getDrnByBinding() {
        return drnByBinding;
    }

    public void setChcDateBinding(RichInputDate chcDateBinding) {
        this.chcDateBinding = chcDateBinding;
    }

    public RichInputDate getChcDateBinding() {
        return chcDateBinding;
    }

    public void setAppDateBinding(RichInputDate appDateBinding) {
        this.appDateBinding = appDateBinding;
    }

    public RichInputDate getAppDateBinding() {
        return appDateBinding;
    }

    public void setDrnDateBinding(RichInputDate drnDateBinding) {
        this.drnDateBinding = drnDateBinding;
    }

    public RichInputDate getDrnDateBinding() {
        return drnDateBinding;
    }

    public void createDetailButtonAL(ActionEvent actionEvent) 
    {
    ADFUtils.findOperation("CreateInsert").execute();
    getProductCodeBinding().setDisabled(true);
    getNoteBinding().setDisabled(true);
    getCustomerTypeBinding().setDisabled(true);
    getCustomerCodeBinding().setDisabled(true);
    getEditButtonBinding().setDisabled(true);
    getCheckedByBinding().setDisabled(true);
    checkptSeqnoBinding.setValue(ADFUtils.evaluateEL("#{bindings.PreDispatchCheckPointsDetailVO1Iterator.estimatedRowCount}"));
    }

    public void setNoteBinding(RichInputText noteBinding) {
        this.noteBinding = noteBinding;
    }

    public RichInputText getNoteBinding() {
        return noteBinding;
    }

    public void setEditButtonBinding(RichButton editButtonBinding) {
        this.editButtonBinding = editButtonBinding;
    }

    public RichButton getEditButtonBinding() {
        return editButtonBinding;
    }

    public void upperLimitValidate(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if(object !=null && baseLimitBinding.getValue() !=null)
        {
            BigDecimal baselimit = (BigDecimal)baseLimitBinding.getValue();
            BigDecimal upperlimit = (BigDecimal) object;
            
            if(upperlimit.compareTo(baselimit)!=1)
            {
                 throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Upper Limit should be greater than to Base Limit.", null));
             }
            
        }

    }

    public void lowerLimitValidate(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if(object !=null && baseLimitBinding.getValue() !=null)
        {
            BigDecimal baselimit = (BigDecimal)baseLimitBinding.getValue();
            BigDecimal lowerlimit = (BigDecimal) object;
            
            if(lowerlimit.compareTo(baselimit)!=-1)
            {
                 throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Lower Limit should be less than to Base Limit.", null));
             }
            
        }

    }

    public void setBaseLimitBinding(RichInputText baseLimitBinding) {
        this.baseLimitBinding = baseLimitBinding;
    }

    public RichInputText getBaseLimitBinding() {
        return baseLimitBinding;
    }

    public void setUpperLimitBinding(RichInputText upperLimitBinding) {
        this.upperLimitBinding = upperLimitBinding;
    }

    public RichInputText getUpperLimitBinding() {
        return upperLimitBinding;
    }

    public void setLowerLimitBinding(RichInputText lowerLimitBinding) {
        this.lowerLimitBinding = lowerLimitBinding;
    }

    public RichInputText getLowerLimitBinding() {
        return lowerLimitBinding;
    }

    public void setDetailDeleteButtonBinding(RichButton detailDeleteButtonBinding) {
        this.detailDeleteButtonBinding = detailDeleteButtonBinding;
    }

    public RichButton getDetailDeleteButtonBinding() {
        return detailDeleteButtonBinding;
    }

    public void setLimitButtonBinding(RichButton limitButtonBinding) {
        this.limitButtonBinding = limitButtonBinding;
    }

    public RichButton getLimitButtonBinding() {
        return limitButtonBinding;
    }

    public void setCustomerCodeBinding(RichInputComboboxListOfValues customerCodeBinding) {
        this.customerCodeBinding = customerCodeBinding;
    }

    public RichInputComboboxListOfValues getCustomerCodeBinding() {
        return customerCodeBinding;
    }

    public void customerTypeVCL(ValueChangeEvent vce) {
        System.out.println("INSIDE CUSTOMER TYPE VCL");
        
        
        if(vce != null) {
            vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
            System.out.println("INSIDE VCE ####");
//            ADFUtils.findOperation("customerTypeValuePreDCheckPts").execute();
//            String custType1 = "General";
//            
//            System.out.println("Customer Type Value is " + customerTypeBinding.getValue());
//            String custType = (String)customerTypeBinding.getValue();
//            
//            if(vce.getNewValue().equals("G")) {
//                System.out.println("INSIDE IF BLOCK ####");
//                customerCodeBinding.setDisabled(true);
//                customerCodeBinding.setValue(custType1);
////                customerCdDtlBinding.setVal
//            }
//            else {
//                System.out.println("INSIDE ELSE BLOCK ####");
//                customerCodeBinding.setDisabled(false);
//            }
            
        }
    }

    public void setCustomerTypeBinding(RichInputComboboxListOfValues customerTypeBinding) {
        this.customerTypeBinding = customerTypeBinding;
    }

    public RichInputComboboxListOfValues getCustomerTypeBinding() {
        return customerTypeBinding;
    }

    public void setCustomerCdDtlBinding(RichColumn customerCdDtlBinding) {
        this.customerCdDtlBinding = customerCdDtlBinding;
    }

    public RichColumn getCustomerCdDtlBinding() {
        return customerCdDtlBinding;
    }

//    public void inspectionParameterVCL(ValueChangeEvent vce) {
//        if(vce!=null) {
//            vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
//            String custCdDtl = (String)customerCdDtlBinding.toString();
//            
//            if(custCdDtl == null) {
//                customerCdDtlBinding.setValueBinding(custCdDtl, customerCodeBinding);
//            }
//        }
//    }
    public void calculativeFlagVCL(ValueChangeEvent vce) {
        System.out.println("INSIDE calculativeFlagVCL ##### ");
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce != null) {
            if(vce.getNewValue()!=vce.getOldValue())
            {
               // System.out.println("Item Type VCL:"+.getValue());
                OperationBinding op = ADFUtils.findOperation("changeValuePreDCheckPts");
                op.execute();
            }
//            String calFlag = (String)calculativeFlagBinding.getValue();
//            System.out.println("----------Calculative Flag Value is ----------" + calFlag);
//
//            if(vce.getNewValue().equals("Non-Calculative")) 
//            {
//                System.out.println("-----If Block-----");
//                upperLimitBinding.setDisabled(true);
//                lowerLimitBinding.setDisabled(true);
//                uomOfLimitsBinding.setDisabled(true);
//            }
//            else {
//                System.out.println("-----Inside Else Block-----");
//                upperLimitBinding.setDisabled(false);
//                lowerLimitBinding.setDisabled(false);
//                uomOfLimitsBinding.setDisabled(false);
//            }
        }
    }

    public void setCalculativeFlagBinding(RichSelectOneChoice calculativeFlagBinding) {
        this.calculativeFlagBinding = calculativeFlagBinding;
    }

    public RichSelectOneChoice getCalculativeFlagBinding() {
        return calculativeFlagBinding;
    }

    public void setUomOfLimitsBinding(RichInputComboboxListOfValues uomOfLimitsBinding) {
        this.uomOfLimitsBinding = uomOfLimitsBinding;
    }

    public RichInputComboboxListOfValues getUomOfLimitsBinding() {
        return uomOfLimitsBinding;
    }
}
