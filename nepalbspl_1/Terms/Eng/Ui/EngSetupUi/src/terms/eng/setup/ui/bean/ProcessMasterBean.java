package terms.eng.setup.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectBooleanCheckbox;
import oracle.adf.view.rich.component.rich.input.RichSelectOneRadio;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import terms.eng.setup.model.view.ProcessDetailVORowImpl;
import terms.eng.setup.model.view.VendUnitVORowImpl;


public class ProcessMasterBean {
    private RichTable processDetailTableBinding;
    private RichInputText processCodeBinding;
    private RichOutputText bindingOutputText;
    private RichButton headerEditBinding;
    private RichButton detailcreateBinding;
    private RichButton detaildeleteBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichInputText machineDescriptionBinding;
    private RichSelectOneRadio jobWorkIOBinding;
    private RichInputComboboxListOfValues workCentorBinding;
    private RichInputText shortDescBinding;
    private RichInputText longDescBinding;

    public ProcessMasterBean() {
    }

    public void SaveAL(ActionEvent actionEvent)
    {
        ADFUtils.setLastUpdatedByNew("ProcessVO1Iterator","LastUpdatedBy");
        DCIteratorBinding Dcite=ADFUtils.findIterator("ProcessDetailVO1Iterator");
        ProcessDetailVORowImpl row=(ProcessDetailVORowImpl) Dcite.getCurrentRow(); 
        
        if(row.getWorkCntr()!=null)
        {
            if(shortDescBinding.getValue()!=null && longDescBinding.getValue()!=null)
            {
                OperationBinding op = ADFUtils.findOperation("getProcessCode");
                Object rst = op.execute();
                
                System.out.println("--------Commit-------");
                System.out.println("value aftr getting result--->?"+rst);
                
                if (rst.toString() != null && rst.toString() != "") {
                if (op.getErrors().isEmpty()) {
                    OperationBinding op1= ADFUtils.findOperation("Commit");
                    Object rs=op1.execute();
                    
                    FacesMessage Message = new FacesMessage("Record Saved Successfully. New Process Code is "+rst+".");  
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                    FacesContext fc = FacesContext.getCurrentInstance();  
                    fc.addMessage(null, Message); 
                  }
                }
                              
               if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ") {
                   if (op.getErrors().isEmpty()) {
                       ADFUtils.findOperation("Commit").execute();
                       FacesMessage Message = new FacesMessage("Record Update Successfully.");  
                       Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                       FacesContext fc = FacesContext.getCurrentInstance();  
                       fc.addMessage(null, Message); 
              
                   }
               }
                               
           }
           else{
            ADFUtils.showMessage("Process Descriptions must be required.",0); 
          }
         }else{
            ADFUtils.showMessage("Work Center must be required in Detail table.",0); 
        }
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) 
    {
        if(dialogEvent.getOutcome().name().equals("ok"))
            {
            ADFUtils.findOperation("Delete").execute();
            System.out.println("Record Delete Successfully");
            
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");   
                Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                FacesContext fc = FacesContext.getCurrentInstance();   
                fc.addMessage(null, Message);   
            
            }

        AdfFacesContext.getCurrentInstance().addPartialTarget(processDetailTableBinding);

    }

    public void setProcessDetailTableBinding(RichTable processDetailTableBinding) {
        this.processDetailTableBinding = processDetailTableBinding;
    }

    public RichTable getProcessDetailTableBinding() {
        return processDetailTableBinding;
    }

    public void setProcessCodeBinding(RichInputText processCodeBinding) {
        this.processCodeBinding = processCodeBinding;
    }

    public RichInputText getProcessCodeBinding() {
        return processCodeBinding;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText()
    {
        cevmodecheck();
        return bindingOutputText;
    }


    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setDetailcreateBinding(RichButton detailcreateBinding) {
        this.detailcreateBinding = detailcreateBinding;
    }

    public RichButton getDetailcreateBinding() {
        return detailcreateBinding;
    }

    public void setDetaildeleteBinding(RichButton detaildeleteBinding) {
        this.detaildeleteBinding = detaildeleteBinding;
    }

    public RichButton getDetaildeleteBinding() {
        return detaildeleteBinding;
    }
    
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
            try {
                Method method1 =
                    component.getClass().getMethod("setDisabled", boolean.class);
                if (method1 != null) {
                    method1.invoke(component, valueComponent);
                }
            } catch (NoSuchMethodException e) {
                try {
                    Method method =
                        component.getClass().getMethod("setReadOnly", boolean.class);
                    if (method != null) {
                        method.invoke(component, valueComponent);
                    }
                } catch (Exception e1) {
                    // e.printStackTrace();//silently eat this exception.
                }


            } catch (Exception e) {
                // e.printStackTrace();//silently eat this exception.
            }
            List<UIComponent> childComponents = component.getChildren();
            for (UIComponent comp : childComponents) {
                makeComponentHierarchyReadOnly(comp, valueComponent);
            }
        }
    
    
    
//SET Mode value    
    private void cevmodecheck(){
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }
    
//Disable Field According To Field is Disable.    
    public void cevModeDisableComponent(String mode) {
    //            FacesContext fctx = FacesContext.getCurrentInstance();
    //            ELContext elctx = fctx.getELContext();
    //            Application jsfApp = fctx.getApplication();
    //            //create a ValueExpression that points to the ADF binding layer
    //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
    //            //
    //            ValueExpression valueExpr = exprFactory.createValueExpression(
    //                                         elctx,
    //                                         "#{pageFlowScope.mode=='E'}",
    //                                          Object.class
    //                                         );
    //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
        if (mode.equals("E")) {
                getProcessCodeBinding().setDisabled(true);
                getHeaderEditBinding().setDisabled(true);
                getDetailcreateBinding().setDisabled(false);
               // getMachineDescriptionBinding().setDisabled(true);
        } else if (mode.equals("C")) {
           // getMachineDescriptionBinding().setDisabled(true);
            getProcessCodeBinding().setDisabled(true);
            getDetailcreateBinding().setDisabled(false);
            getHeaderEditBinding().setDisabled(true);
            getJobWorkIOBinding().setDisabled(true);
        } else if (mode.equals("V")) {
            getDetailcreateBinding().setDisabled(true);
          //  getDetaildeleteBinding().setDisabled(true);
        }
        
    }


    public void editButtonAL(ActionEvent actionEvent)
    {
        cevmodecheck();
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setMachineDescriptionBinding(RichInputText machineDescriptionBinding) {
        this.machineDescriptionBinding = machineDescriptionBinding;
    }

    public RichInputText getMachineDescriptionBinding() {
        return machineDescriptionBinding;
    }

    public void setJobWorkIOBinding(RichSelectOneRadio jobWorkIOBinding) {
        this.jobWorkIOBinding = jobWorkIOBinding;
    }

    public RichSelectOneRadio getJobWorkIOBinding() {
        return jobWorkIOBinding;
    }

    public void jobWorkVCL(ValueChangeEvent vce) {
     Boolean b=(Boolean)vce.getOldValue();
     System.out.println("VALUE OF CHECK BOX"+b);
     if(b.equals(false))     
     {
         getJobWorkIOBinding().setDisabled(false);
     }
     else
     {
             getJobWorkIOBinding().setDisabled(true);
     }
    }

    public void setWorkCentorBinding(RichInputComboboxListOfValues workCentorBinding) {
        this.workCentorBinding = workCentorBinding;
    }

    public RichInputComboboxListOfValues getWorkCentorBinding() {
        return workCentorBinding;
    }

    public String saveandCloseAL() {
        ADFUtils.setLastUpdatedByNew("ProcessVO1Iterator","LastUpdatedBy");
        DCIteratorBinding Dcite=ADFUtils.findIterator("ProcessDetailVO1Iterator");
        ProcessDetailVORowImpl row=(ProcessDetailVORowImpl) Dcite.getCurrentRow(); 
        
        if(row.getWorkCntr()!=null)
        {
            if(shortDescBinding.getValue()!=null && longDescBinding.getValue()!=null){
        OperationBinding op = ADFUtils.findOperation("getProcessCode");
                           Object rst = op.execute();
                          
                           System.out.println("--------Commit-------");
                           System.out.println("value aftr getting result--->?"+rst);
                          
                               if (rst.toString() != null && rst.toString() != "") {
                                   if (op.getErrors().isEmpty()) {
                                       OperationBinding op1= ADFUtils.findOperation("Commit");
                                       Object rs=op1.execute();
                                     
                                     FacesMessage Message = new FacesMessage("Record Saved Successfully. New Process Code is "+rst+".");  
                                       Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                                       FacesContext fc = FacesContext.getCurrentInstance();  
                                       fc.addMessage(null, Message); 
                                       return "save and close";
                                   }
                               }

                              
                               if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ") {
                                   if (op.getErrors().isEmpty()) {
                                       ADFUtils.findOperation("Commit").execute();
                                       FacesMessage Message = new FacesMessage("Record Update Successfully.");  
                                       Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                                       FacesContext fc = FacesContext.getCurrentInstance();  
                                       fc.addMessage(null, Message); 
                                       return "save and close";
                              
                                   }

                              
                               }
            }
            else{
             ADFUtils.showMessage("Process Descriptions must be required.",0); 
             return null;
            }
        }else{
            ADFUtils.showMessage("Work Center must be required in Detail table.",0); 
            return null;
        }
        return null;
    }

    public void setShortDescBinding(RichInputText shortDescBinding) {
        this.shortDescBinding = shortDescBinding;
    }

    public RichInputText getShortDescBinding() {
        return shortDescBinding;
    }

    public void setLongDescBinding(RichInputText longDescBinding) {
        this.longDescBinding = longDescBinding;
    }

    public RichInputText getLongDescBinding() {
        return longDescBinding;
    }
}
