 package terms.eng.setup.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import javax.faces.validator.ValidatorException;

import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;

import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

public class ProcessSheetBean {
    private RichInputComboboxListOfValues partCodeBinding;
    private RichTable processTableBinding;
    private RichSelectOneChoice itemTypeBinding;
    private RichButton headerEditBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichOutputText bindingOutputText;
    private RichTable altInputTableBinding;
    private RichTable partProductTableBinding;
    private RichInputText processseqrevNoBinding;
    private RichInputText processSeqRevBinding;
    private RichInputText detailInputserialNoBinding;
    private RichInputText processSeqBinding;
    private RichPopup calcPowPopBinding;
    private RichInputText machineToolSerialNoBinding;
    private RichTable unitTableBinding;
    private RichTable machineToolTableBinding;
    private RichTable costingDetailTableBinding;
    private RichInputText stdTotalper8hrBinding;
    private RichInputText noOffBinding;
    private RichButton rmcButtonBinding;
    private RichPopup inputPopupBinding;
    private RichShowDetailItem cycleTimeTabBinding;
    private RichShowDetailItem unitMappingTabBinding;
    private RichShowDetailItem machToolTabBinding;
    private RichShowDetailItem costingDetailTabBinding;
    private RichButton costingDtlCreateButtonBinding;
    private RichButton machToolCreateButtonBinding;
    private RichButton unitMappingCreateButtonBinding;
    private RichButton machToolDeleteButtonBinding;
    private RichButton costDetailDeleteButtonBinding;
    private RichButton unitMappingDeleteButtonBinding;
    private RichInputComboboxListOfValues processCodeBinding;
    private RichInputText rmTypeBinding;
    private RichSelectOneChoice processSheetForBinding;
    private RichInputComboboxListOfValues tabUnitCodeBinding;
    private RichButton inputButtonBinding;
    private RichButton altButtonBinding;
    private RichInputText componentPerKgBinding;
    private RichInputText netWtBinding;
    private RichInputText runnerWtBinding;
    private RichInputComboboxListOfValues machCodeBinding;


    public ProcessSheetBean() {
    }


    public void onPageLoad() {
        
        OperationBinding op = ADFUtils.findOperation("CreateFilter");
        op.execute();
        
        OperationBinding op1 = ADFUtils.findOperation("CreateInsertProcessSheet");
        op1.execute();   
        
    }

    public void detailCreateAL(ActionEvent actionEvent) {
        
        ADFUtils.findOperation("CreateInsert").execute();
       detailInputserialNoBinding.setValue(ADFUtils.evaluateEL("#{bindings.ProcessSheetDetailVO1Iterator.estimatedRowCount}"));
       // ADFUtils.evaluateEL("#{bindings.ProcessSheetDetailVO1Iterator.estimatedRowCount}");
       
    }
    public void setPartCodeBinding(RichInputComboboxListOfValues partCodeBinding) {
        this.partCodeBinding = partCodeBinding;
    }

    public RichInputComboboxListOfValues getPartCodeBinding() {
        return partCodeBinding;
    }

    public void setProcessTableBinding(RichTable processTableBinding) {
        this.processTableBinding = processTableBinding;
    }

    public RichTable getProcessTableBinding() {
        return processTableBinding;
    }

    public void createProcessAL(ActionEvent actionEvent)
    {
//        ADFUtils.findOperation("addProcessSheetData").execute();
//        processSeqBinding.setValue(ADFUtils.evaluateEL("#{bindings.ProcessSheetHeaderVO1Iterator.estimatedRowCount}"));
     if((Long)ADFUtils.evaluateEL("#{bindings.ProcessSheetPartProcUnitVO1Iterator.estimatedRowCount}")>0) 
        {
        System.out.println("when no row created************"+tabUnitCodeBinding.getValue());
        OperationBinding Opp1=ADFUtils.findOperation("unitMappingProcessSheet");
        Object Obj=Opp1.execute();
        System.out.println("Result is===>"+Opp1.getResult());
        if(Opp1.getResult()!=null && Opp1.getResult().equals("Y"))
        {
            System.out.println("--------THERE IS NO UNIT , UNIT IS NULL*********");
            ADFUtils.findOperation("addProcessSheetData").execute();
            processSeqBinding.setValue(ADFUtils.evaluateEL("#{bindings.ProcessSheetHeaderVO1Iterator.estimatedRowCount}"));
         
        }
        else
        {
        System.out.println("********CREATE BUTTON ACTION******Unit Check Condition**********");
        ADFUtils.showMessage("Please Enter Unit Code in Unit Mapping Tab.",0);    
        }
    }
        else
        {
        System.out.println("***<<<<<CREATE BUTTON ACTION>>>>>*****Row Check Condition**********");
        ADFUtils.showMessage("Please Enter Unit Code in Unit Mapping Tab.",0);    
        }

    }

    public void popRMCDetailAL(ActionEvent actionEvent)
    {
        RichPopup.PopupHints hint=new  RichPopup.PopupHints();
        
        OperationBinding oprBind=ADFUtils.findOperation("itemTypeChangeValue");
        Object obj=oprBind.execute();
        System.out.println("Result is ====> "+oprBind.getResult());
            if(oprBind.getResult().equals("A"))
                  {
                      getCalcPowPopBinding().show(hint);
                      Long Check=(Long)ADFUtils.evaluateEL("#{bindings.RmCalculationsPowderVO1Iterator.estimatedRowCount}");
                      if(Check <1)
                      {
                      ADFUtils.findOperation("RmCalcPowderCreateInsert").execute();
                      }
                      
                 }
                    if(oprBind.getResult().equals("B"))
                         {
////                           if(ADFUtils.evaluateEL("#{pageFlowScope.mode}").equals("E"))
////                          {
//                                System.out.println("------ Disable Block B Block Bean Code----");
//                                getNoOffBinding().setDisabled(false);
//                                getRmcButtonBinding().setDisabled(true);
//                          //  }
//                                
                            System.out.println("------- Powder Block -----------");
                  }
        
    }

    public void setItemTypeBinding(RichSelectOneChoice itemTypeBinding) {
        this.itemTypeBinding = itemTypeBinding;
    }

    public RichSelectOneChoice getItemTypeBinding() {
        return itemTypeBinding;
    }
    public void insertAltProductAL(ActionEvent actionEvent) {
        
        RichPopup.PopupHints hint=new  RichPopup.PopupHints();
    }

    public void saveProcessSheetAL(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedByNew("ProcessSheetHeaderVO1Iterator","LastUpdatedBy");
        ADFUtils.findOperation("setValueProcessSheet").execute();
        
        
        if((Long)ADFUtils.evaluateEL("#{bindings.ProcessSheetPartProcUnitVO1Iterator.estimatedRowCount}")>0) 
        {
        System.out.println("when no row created************"+tabUnitCodeBinding.getValue());
        OperationBinding Opp=ADFUtils.findOperation("unitMappingProcessSheet");
        Object Obj=Opp.execute();
        System.out.println("Result is===>"+Opp.getResult());
        if(Opp.getResult()!=null && Opp.getResult().equals("Y"))
        {
            System.out.println("when no  tab unit not null created************");
        
                if(bindingOutputText.getValue().equals(0))
                {
                    System.out.println("-------In Create Mode--------");
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message = new FacesMessage("Record Save Successfully.");  
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                    FacesContext fc = FacesContext.getCurrentInstance();  
                    fc.addMessage(null, Message);    
                }
                else{
                        System.out.println("------In Edit Mode-------");
                        ADFUtils.findOperation("Commit").execute();
                        FacesMessage Message = new FacesMessage("Record Update Successfully.");  
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                        FacesContext fc = FacesContext.getCurrentInstance();  
                        fc.addMessage(null, Message); 
                    }
        }
        else
        {
        System.out.println("********Unit Check Condition**********");
        ADFUtils.showMessage("Please Enter Unit Code in Unit Mapping Tab.",0);    
        }
        }
        else
        {
        System.out.println("********Row Check Condition**********");
        ADFUtils.showMessage("Please Enter Unit Code in Unit Mapping Tab.",0);    
        }
    }

    public void EditButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
        
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }

        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }

    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }

    public void cevModeDisableComponent(String mode) {

        if (mode.equals("E")) {
            getRmTypeBinding().setDisabled(true);
            getDetailInputserialNoBinding().setDisabled(true);
            getProcessSeqBinding().setDisabled(true);
            getProcessSeqRevBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            getMachineToolSerialNoBinding().setDisabled(true);
            getStdTotalper8hrBinding().setDisabled(true);
            getUnitMappingCreateButtonBinding().setDisabled(false);
            getMachToolCreateButtonBinding().setDisabled(false);
            getCostingDtlCreateButtonBinding().setDisabled(false);
            getPartCodeBinding().setDisabled(true);
            getProcessSheetForBinding().setDisabled(true);
//            getUnitMappingDeleteButtonBinding().setDisabled(false);
//            getMachToolDeleteButtonBinding().setDisabled(false);
//            getCostDetailDeleteButtonBinding().setDisabled(false);
        } else if (mode.equals("C")) {
            getRmTypeBinding().setDisabled(true);
            getDetailInputserialNoBinding().setDisabled(true);
            getProcessSeqRevBinding().setDisabled(true);
            getProcessSeqBinding().setDisabled(true);
            getMachineToolSerialNoBinding().setDisabled(true);
            getStdTotalper8hrBinding().setDisabled(true);
            getUnitMappingCreateButtonBinding().setDisabled(false);
            getMachToolCreateButtonBinding().setDisabled(false);
            getCostingDtlCreateButtonBinding().setDisabled(false);
//            getUnitMappingDeleteButtonBinding().setDisabled(false);
//            getMachToolDeleteButtonBinding().setDisabled(false);
//            getCostDetailDeleteButtonBinding().setDisabled(false);
            
        } else if (mode.equals("V")) {
           
            getRmcButtonBinding().setDisabled(false);
            getAltButtonBinding().setDisabled(false);
            getCycleTimeTabBinding().setDisabled(false);
            getUnitMappingTabBinding().setDisabled(false);
            getMachToolTabBinding().setDisabled(false);
            getCostingDetailTabBinding().setDisabled(false);
            getUnitMappingCreateButtonBinding().setDisabled(true);
            getMachToolCreateButtonBinding().setDisabled(true);
            getCostingDtlCreateButtonBinding().setDisabled(true);
            getInputButtonBinding().setDisabled(false);
//            getUnitMappingDeleteButtonBinding().setDisabled(true);
//            getMachToolDeleteButtonBinding().setDisabled(true);
//            getCostDetailDeleteButtonBinding().setDisabled(true);
        }
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            OperationBinding op = null;
            ADFUtils.findOperation("Delete").execute();
            op = (OperationBinding) ADFUtils.findOperation("Commit");
            Object rst = op.execute();
            System.out.println("Record Delete Successfully");
            if (op.getErrors().isEmpty()) {
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            } else if (!op.getErrors().isEmpty()) {
                OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
                Object rstr = opr.execute();
                FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            }
        }

        AdfFacesContext.getCurrentInstance().addPartialTarget(processTableBinding);

    }

    public void altDeleteDialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                                 {
                                 OperationBinding op = null;
                                 ADFUtils.findOperation("Delete2").execute();
                                 op = (OperationBinding) ADFUtils.findOperation("Commit");
                                 Object rst = op.execute();
                                 System.out.println("Record Delete Successfully");
                                     if(op.getErrors().isEmpty()){
                                         FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                  Message.setSeverity(FacesMessage.SEVERITY_INFO);
                                         FacesContext fc = FacesContext.getCurrentInstance();
                                         fc.addMessage(null, Message);
                                     } else if (!op.getErrors().isEmpty())
                                     {
                                        OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
                                        Object rstr = opr.execute();
                                        FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                  Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                                        FacesContext fc = FacesContext.getCurrentInstance();
                                        fc.addMessage(null, Message);
                                      }
                                 }

              AdfFacesContext.getCurrentInstance().addPartialTarget(altInputTableBinding);

    }

    public void setAltInputTableBinding(RichTable altInputTableBinding) {
        this.altInputTableBinding = altInputTableBinding;
    }

    public RichTable getAltInputTableBinding() {
        return altInputTableBinding;
    }

    public void setPartProductTableBinding(RichTable partProductTableBinding) {
        this.partProductTableBinding = partProductTableBinding;
    }

    public RichTable getPartProductTableBinding() {
        return partProductTableBinding;
    }

    public void partProductDialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                                 {
                                 OperationBinding op = null;
                                 ADFUtils.findOperation("Delete1").execute();
                                 op = (OperationBinding) ADFUtils.findOperation("Commit");
                                 Object rst = op.execute();
                                 System.out.println("Record Delete Successfully");
                                     if(op.getErrors().isEmpty()){
                                         FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                  Message.setSeverity(FacesMessage.SEVERITY_INFO);
                                         FacesContext fc = FacesContext.getCurrentInstance();
                                         fc.addMessage(null, Message);
                                     } else if (!op.getErrors().isEmpty())
                                     {
                                        OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
                                        Object rstr = opr.execute();
                                        FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                  Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                                        FacesContext fc = FacesContext.getCurrentInstance();
                                        fc.addMessage(null, Message);
                                      }
                                 }

              AdfFacesContext.getCurrentInstance().addPartialTarget(partProductTableBinding);
    }

    public void setProcessseqrevNoBinding(RichInputText processseqrevNoBinding) {
        this.processseqrevNoBinding = processseqrevNoBinding;
    }

    public RichInputText getProcessseqrevNoBinding() {
        return processseqrevNoBinding;
    }

    public void setProcessSeqRevBinding(RichInputText processSeqRevBinding) {
        this.processSeqRevBinding = processSeqRevBinding;
    }

    public RichInputText getProcessSeqRevBinding() {
        return processSeqRevBinding;
    }
    public void setDetailInputserialNoBinding(RichInputText detailInputserialNoBinding) {
        this.detailInputserialNoBinding = detailInputserialNoBinding;
    }

    public RichInputText getDetailInputserialNoBinding() {
        return detailInputserialNoBinding;
    }

    public void setProcessSeqBinding(RichInputText processSeqBinding) {
        this.processSeqBinding = processSeqBinding;
    }

    public RichInputText getProcessSeqBinding() {
        return processSeqBinding;
    }

    public void setCalcPowPopBinding(RichPopup calcPowPopBinding) {
        this.calcPowPopBinding = calcPowPopBinding;
    }

    public RichPopup getCalcPowPopBinding() {
        return calcPowPopBinding;
    }

    public void addMachineDetailAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("MachineToolCreateInsert").execute();
        machineToolSerialNoBinding.setValue(ADFUtils.evaluateEL("#{bindings.ProcessSheetMachineToolDetailVO1Iterator.estimatedRowCount}"));
    }

    public void setMachineToolSerialNoBinding(RichInputText machineToolSerialNoBinding) {
        this.machineToolSerialNoBinding = machineToolSerialNoBinding;
    }

    public RichInputText getMachineToolSerialNoBinding() {
        return machineToolSerialNoBinding;
    }

    public void deleteUnitDialogAL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
            {
            
            ADFUtils.findOperation("Delete3").execute();
            System.out.println("Record Delete Successfully");
            
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");   
                Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                FacesContext fc = FacesContext.getCurrentInstance();   
                fc.addMessage(null, Message);   
            
            }

        AdfFacesContext.getCurrentInstance().addPartialTarget(unitTableBinding);
    }

    public void setUnitTableBinding(RichTable unitTableBinding) {
        this.unitTableBinding = unitTableBinding;
    }

    public RichTable getUnitTableBinding() {
        return unitTableBinding;
    }

    public void deleteMachineToolDetailDialogAL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
            {
            
            ADFUtils.findOperation("Delete4").execute();
            System.out.println("Record Delete Successfully");
            
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");   
                Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                FacesContext fc = FacesContext.getCurrentInstance();   
                fc.addMessage(null, Message);   
            
            }

        AdfFacesContext.getCurrentInstance().addPartialTarget(machineToolTableBinding);

    }

    public void setMachineToolTableBinding(RichTable machineToolTableBinding) {
        this.machineToolTableBinding = machineToolTableBinding;
    }

    public RichTable getMachineToolTableBinding() {
        return machineToolTableBinding;
    }

    public void deleteCostingDetailDialogAL(DialogEvent dialogEvent) {
           
     if(dialogEvent.getOutcome().name().equals("ok"))
        {
        ADFUtils.findOperation("Delete5").execute();
        System.out.println("Record Delete Successfully");
        
            FacesMessage Message = new FacesMessage("Record Deleted Successfully");   
            Message.setSeverity(FacesMessage.SEVERITY_INFO);   
            FacesContext fc = FacesContext.getCurrentInstance();   
            fc.addMessage(null, Message);   
        
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(costingDetailTableBinding);
    }

    public void setCostingDetailTableBinding(RichTable costingDetailTableBinding) {
        this.costingDetailTableBinding = costingDetailTableBinding;
    }

    public RichTable getCostingDetailTableBinding() {
        return costingDetailTableBinding;
    }

    public void menInvolveVCL(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        System.out.println("----------Per Piece Value VCL-------");
        ADFUtils.findOperation("calculateTimeSecondInPS").execute();
    }

    public void setStdTotalper8hrBinding(RichInputText stdTotalper8hrBinding) {
        this.stdTotalper8hrBinding = stdTotalper8hrBinding;
    }

    public RichInputText getStdTotalper8hrBinding() {
        return stdTotalper8hrBinding;
    }

    public void setNoOffBinding(RichInputText noOffBinding) {
        this.noOffBinding = noOffBinding;
    }

    public RichInputText getNoOffBinding() {
        return noOffBinding;
    }

    public void setRmcButtonBinding(RichButton rmcButtonBinding) {
        this.rmcButtonBinding = rmcButtonBinding;
    }

    public RichButton getRmcButtonBinding() {
        return rmcButtonBinding;
    }

    public void inputButtonAL(ActionEvent actionEvent) {
        RichPopup.PopupHints hint=new  RichPopup.PopupHints();
        getInputPopupBinding().show(hint);

    }

    public void setInputPopupBinding(RichPopup inputPopupBinding) {
        this.inputPopupBinding = inputPopupBinding;
    }

    public RichPopup getInputPopupBinding() {
        return inputPopupBinding;
    }
    public void itemTypeVCE(ValueChangeEvent vce) {
        
        if(vce.getNewValue()!=vce.getOldValue())
        {
               System.out.println("Item Type VCL:"+itemTypeBinding.getValue());
               OperationBinding op = ADFUtils.findOperation("valueSetNullInProcessSheetDetail");
               op.execute();
        }
//            OperationBinding oprBind=ADFUtils.findOperation("itemTypeChangeValue");
//            Object obj=oprBind.execute();
//            System.out.println("Result is in rmc button caae ====> "+oprBind.getResult());
//            if(oprBind.getResult().equals("B")){
//                
//                getRmcButtonBinding().setDisabled(true);
//                
//                
//            }
//            else {
//                getRmcButtonBinding().setDisabled(false);
//            }
//            
        
    }

    public void processSheetForVCE(ValueChangeEvent vce) {
        if(vce.getNewValue()!=vce.getOldValue())
        {
               System.out.println("Item Type VCL:"+itemTypeBinding.getValue());
               OperationBinding op = ADFUtils.findOperation("valueSetNullInProcessSheetHeader");
               op.execute();
        }
    }

    public void altItemTypeVCE(ValueChangeEvent vce) {
        if(vce.getNewValue()!=vce.getOldValue())
        {
               System.out.println("Item Type VCL:"+itemTypeBinding.getValue());
               OperationBinding op = ADFUtils.findOperation("valueSetNullInAltPartProcessSheet");
               op.execute();
        }
    }

    public void setCycleTimeTabBinding(RichShowDetailItem cycleTimeTabBinding) {
        this.cycleTimeTabBinding = cycleTimeTabBinding;
    }

    public RichShowDetailItem getCycleTimeTabBinding() {
        return cycleTimeTabBinding;
    }

    public void setUnitMappingTabBinding(RichShowDetailItem unitMappingTabBinding) {
        this.unitMappingTabBinding = unitMappingTabBinding;
    }

    public RichShowDetailItem getUnitMappingTabBinding() {
        return unitMappingTabBinding;
    }

    public void setMachToolTabBinding(RichShowDetailItem machToolTabBinding) {
        this.machToolTabBinding = machToolTabBinding;
    }

    public RichShowDetailItem getMachToolTabBinding() {
        return machToolTabBinding;
    }

    public void setCostingDetailTabBinding(RichShowDetailItem costingDetailTabBinding) {
        this.costingDetailTabBinding = costingDetailTabBinding;
    }

    public RichShowDetailItem getCostingDetailTabBinding() {
        return costingDetailTabBinding;
    }

    public void setCostingDtlCreateButtonBinding(RichButton costingDtlCreateButtonBinding) {
        this.costingDtlCreateButtonBinding = costingDtlCreateButtonBinding;
    }

    public RichButton getCostingDtlCreateButtonBinding() {
        return costingDtlCreateButtonBinding;
    }

    public void setMachToolCreateButtonBinding(RichButton machToolCreateButtonBinding) {
        this.machToolCreateButtonBinding = machToolCreateButtonBinding;
    }

    public RichButton getMachToolCreateButtonBinding() {
        return machToolCreateButtonBinding;
    }

    public void setUnitMappingCreateButtonBinding(RichButton unitMappingCreateButtonBinding) {
        this.unitMappingCreateButtonBinding = unitMappingCreateButtonBinding;
    }

    public RichButton getUnitMappingCreateButtonBinding() {
        return unitMappingCreateButtonBinding;
    }

    public void setMachToolDeleteButtonBinding(RichButton machToolDeleteButtonBinding) {
        this.machToolDeleteButtonBinding = machToolDeleteButtonBinding;
    }

    public RichButton getMachToolDeleteButtonBinding() {
        return machToolDeleteButtonBinding;
    }

    public void setCostDetailDeleteButtonBinding(RichButton costDetailDeleteButtonBinding) {
        this.costDetailDeleteButtonBinding = costDetailDeleteButtonBinding;
    }

    public RichButton getCostDetailDeleteButtonBinding() {
        return costDetailDeleteButtonBinding;
    }

    public void setUnitMappingDeleteButtonBinding(RichButton unitMappingDeleteButtonBinding) {
        this.unitMappingDeleteButtonBinding = unitMappingDeleteButtonBinding;
    }

    public RichButton getUnitMappingDeleteButtonBinding() {
        return unitMappingDeleteButtonBinding;
    }

    public void setProcessCodeBinding(RichInputComboboxListOfValues processCodeBinding) {
        this.processCodeBinding = processCodeBinding;
    }

    public RichInputComboboxListOfValues getProcessCodeBinding() {
        return processCodeBinding;
    }

    public void setRmTypeBinding(RichInputText rmTypeBinding) {
        this.rmTypeBinding = rmTypeBinding;
    }

    public RichInputText getRmTypeBinding() {
        return rmTypeBinding;
    }

    public void setProcessSheetForBinding(RichSelectOneChoice processSheetForBinding) {
        this.processSheetForBinding = processSheetForBinding;
    }

    public RichSelectOneChoice getProcessSheetForBinding() {
        return processSheetForBinding;
    }

    public void setTabUnitCodeBinding(RichInputComboboxListOfValues tabUnitCodeBinding) {
        this.tabUnitCodeBinding = tabUnitCodeBinding;
    }

    public RichInputComboboxListOfValues getTabUnitCodeBinding() {
        return tabUnitCodeBinding;
    }
    
    
    public String saveAndCloseButton() {
        ADFUtils.setLastUpdatedByNew("ProcessSheetHeaderVO1Iterator","LastUpdatedBy");
        ADFUtils.findOperation("setValueProcessSheet").execute();
        
        
        if((Long)ADFUtils.evaluateEL("#{bindings.ProcessSheetPartProcUnitVO1Iterator.estimatedRowCount}")>0) 
        {
        System.out.println("when no row created************"+tabUnitCodeBinding.getValue());
        OperationBinding Opp=ADFUtils.findOperation("unitMappingProcessSheet");
        Object Obj=Opp.execute();
        System.out.println("Result is===>"+Opp.getResult());
        if(Opp.getResult()!=null && Opp.getResult().equals("Y"))
        {
            System.out.println("when no  tab unit not null created************");
        
                if(bindingOutputText.getValue().equals(0))
                {
                    System.out.println("-------In Create Mode--------");
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message = new FacesMessage("Record Save Successfully.");  
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                    FacesContext fc = FacesContext.getCurrentInstance();  
                    fc.addMessage(null, Message);  
                    return "saveandclose";
                }
                else{
                        System.out.println("------In Edit Mode-------");
                        ADFUtils.findOperation("Commit").execute();
                        FacesMessage Message = new FacesMessage("Record Update Successfully.");  
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                        FacesContext fc = FacesContext.getCurrentInstance();  
                        fc.addMessage(null, Message); 
                        return "saveandclose";

                    }
        }
        else
        {
        System.out.println("********Unit Check Condition**********");
        ADFUtils.showMessage("Please Enter Unit Code in Unit Mapping Tab.",0);    
        }
        }
        else
        {
        System.out.println("********Row Check Condition**********");
        ADFUtils.showMessage("Please Enter Unit Code in Unit Mapping Tab.",0);    
        }
        return  null;
    }


    public void setInputButtonBinding(RichButton inputButtonBinding) {
        this.inputButtonBinding = inputButtonBinding;
    }

    public RichButton getInputButtonBinding() {
        return inputButtonBinding;
    }

    public void setAltButtonBinding(RichButton altButtonBinding) {
        this.altButtonBinding = altButtonBinding;
    }

    public RichButton getAltButtonBinding() {
        return altButtonBinding;
    }

    public void setComponentPerKgBinding(RichInputText componentPerKgBinding) {
        this.componentPerKgBinding = componentPerKgBinding;
    }

    public RichInputText getComponentPerKgBinding() {
        return componentPerKgBinding;
    }

    public void setNetWtBinding(RichInputText netWtBinding) {
        this.netWtBinding = netWtBinding;
    }

    public RichInputText getNetWtBinding() {
        return netWtBinding;
    }

    public void grocessPerPieceWtVCL(ValueChangeEvent vce) 
    {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        BigDecimal compWt=(BigDecimal)vce.getNewValue();
        BigDecimal NetWt=(BigDecimal)netWtBinding.getValue();
        if(compWt!=null)
        {
            if(NetWt==null)
            {
                runnerWtBinding.setValue(compWt);
//                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error",
//                    "Net Per Piece Wt must be required.!!");
//                FacesContext context = FacesContext.getCurrentInstance();
//                context.addMessage(netWtBinding.getClientId(), msg);
//                throw new ValidatorException(msg);
                
            }else{
                    runnerWtBinding.setValue(compWt.subtract(NetWt));
                    runnerWtBinding.setDisabled(true);
                }
        }
        
    }

    public void setRunnerWtBinding(RichInputText runnerWtBinding) {
        this.runnerWtBinding = runnerWtBinding;
    }

    public RichInputText getRunnerWtBinding() {
        return runnerWtBinding;
    }

    public void netWeightVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        BigDecimal NetWt=(BigDecimal)vce.getNewValue();
        BigDecimal compWt=(BigDecimal)componentPerKgBinding.getValue();
        if(NetWt!=null)
        {
             runnerWtBinding.setValue(compWt.subtract(NetWt));
             runnerWtBinding.setDisabled(true);
         }
    }

    public void netWeightValidator(FacesContext facesContext, UIComponent uIComponent, Object ob) {
        
        BigDecimal compWtt = (BigDecimal)componentPerKgBinding.getValue();
        BigDecimal netWtt = (BigDecimal)ob;
        if(netWtt!=null && compWtt!=null && netWtt.compareTo(compWtt)!=-1)
        {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error",
                "Net Per Piece Wt must be less than Gross Per Piece Wt.!!");
            throw new ValidatorException(msg);
        }
    }

    public void machCodeVCL(ValueChangeEvent vce) {
//        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
//        String machCode = (String)machCodeBinding.getValue();
//        System.out.println("------Mach Code -----*** > : "+machCode);
//        if(machCode!=null)
//        {
//            if(vce.getNewValue()==machCode)
//            {
//                System.out.println("-------Enter Here-------");
//                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error",
//                    "Duplicate Record Found!!");
//                throw new ValidatorException(msg);
//            }
//        }
    }

    public void setMachCodeBinding(RichInputComboboxListOfValues machCodeBinding) {
        this.machCodeBinding = machCodeBinding;
    }

    public RichInputComboboxListOfValues getMachCodeBinding() {
        return machCodeBinding;
    }

    public void runnerWeightValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
//       
//        BigDecimal compWtt = (BigDecimal)componentPerKgBinding.getValue();
//        BigDecimal runnerWtt = (BigDecimal)object;
////        BigDecimal d=new BigDecimal(0);
////        if(runnerWtt.compareTo(d)==-1)
////        {
////            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error",
////                "Per Piece Scrap Wt. should be non negative value.!!");
////            throw new ValidatorException(msg);
////        }else{
//        
//        if(runnerWtt!=null && compWtt!=null && runnerWtt.compareTo(compWtt)!=-1)
//        {
//            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error",
//                "Per Piece Scrap Wt. should be non negative value.!!");
//            throw new ValidatorException(msg);
//        }

    }

    public void machineCategoryVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce!=null)
        {
            System.out.println("-----sop before-----");
            if(vce.getNewValue()!=vce.getOldValue())
            {
                   System.out.println("-----sop inside-----");
                   OperationBinding op = ADFUtils.findOperation("valueSetNullInProcessSheetMachTool");
                   op.execute();
                AdfFacesContext.getCurrentInstance().addPartialTarget(getMachineToolTableBinding());
            }
        }
    }
}
