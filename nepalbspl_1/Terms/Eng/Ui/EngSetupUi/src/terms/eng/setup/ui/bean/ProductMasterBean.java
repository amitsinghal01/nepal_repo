package terms.eng.setup.ui.bean;

import java.awt.image.BufferedImage;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import java.lang.reflect.Method;

import java.text.DateFormat;

import java.text.SimpleDateFormat;

import java.util.Date;

import java.util.List;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.application.ViewHandler;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import javax.faces.validator.ValidatorException;

import javax.imageio.ImageIO;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

import org.apache.myfaces.trinidad.model.UploadedFile;

public class ProductMasterBean {


    private UploadedFile imageFile;
    String imagePath = null;
    private RichTable itemUnitStoreTableBinding;
    private RichInputText productCodeBinding;
    private RichInputText gstCodeBinding;
    private RichOutputText bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichButton detailcreateBinding;
    private RichInputText productDescriptionValdator;
    private RichSelectOneChoice prodSubTypeBinding;
    private RichSelectOneChoice uombinding;
    private RichTable itemPackingUomTableBinding;
    private RichShowDetailItem tab2Binding;
    private RichShowDetailItem tab1Binding;
    private RichButton detailcreate2Binding;
    private RichShowDetailItem purchaseBinding;
    private RichShowDetailItem qualityBinding;
    private RichShowDetailItem mrpBinding;
    private RichShowDetailItem saleBinding;
    private RichShowDetailItem financeBinding;
    private RichShowDetailItem costingBinding;

    public void setImageFile(UploadedFile imageFile) {
        this.imageFile = imageFile;
    }

    public UploadedFile getImageFile() {
        return imageFile;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getImagePath() {
        return imagePath;
    }


    public ProductMasterBean() {
        File dir = new File("/tmp/pht");
        File savedir = new File("/home/beta4/drawingpics");
        if (!dir.exists()) {
            try {
                dir.mkdir();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (!savedir.exists()) {
            try {
                savedir.mkdir();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    public void onPageLoad() {
        ADFUtils.findOperation("ProductCreateInsert").execute();
        ADFUtils.findOperation("ProductsCreateInsert").execute();

    }

    public void addProductMasterAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert").execute();
        //        OperationBinding op=ADFUtils.findOperation("generateProductCode");
        //        op.execute();
        //        if(productCodeBinding.getValue()!=null)
        //        {
        //           // //   System.out.println("jkhjkhdsjkfhjkhd"+ADFUtils.resolveExpression("#{pageFlowScope.mode}"));
        //            getProductCodeBinding().setDisabled(true);
        //        }
    }


    private String SaveuploadFile(UploadedFile file) {
        ADFUtils.setLastUpdatedByNew("ProductVO1Iterator", "LastUpdatedBy");
        UploadedFile myfile = file;

        if (myfile == null) {
        } else {
            if (myfile.getContentType().equalsIgnoreCase("image/jpeg") ||
                myfile.getContentType().equalsIgnoreCase("image/png") ||
                myfile.getContentType().equalsIgnoreCase("image/bmp") ||
                myfile.getContentType().equalsIgnoreCase("image/gif")) {

                //Path of folder on drive
                String path = "/home/beta4/drawingpics/";
                String type = "PNG";
                String TypeVal = ".png";

                if (myfile.getContentType().equalsIgnoreCase("image/jpeg")) {
                    type = "JPEG";
                    TypeVal = ".jpeg";
                } else if (myfile.getContentType().equalsIgnoreCase("image/png")) {
                    type = "PNG";
                    TypeVal = ".png";
                } else if (myfile.getContentType().equalsIgnoreCase("image/bmp")) {
                    type = "PNG";
                    TypeVal = ".png";
                } else if (myfile.getContentType().equalsIgnoreCase("image/gif")) {
                    type = "GIF";
                    TypeVal = ".gif";
                }

                InputStream inputStream = null;
                try {
                    //Generate a unique name for uploaded image with date time
                    DateFormat dateFormat = new SimpleDateFormat("yyMMdd_HHmmss");
                    Date date = new Date();
                    String dtTime = dateFormat.format(date);
                    dtTime = dtTime.replace(" ", "_");

                    String name = "IMG" + "_" + dtTime;
                    //   System.out.println("File name is-" + name);

                    File f = new File(path);
                    if (!f.exists()) {
                        try {
                            f.createNewFile();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    inputStream = myfile.getInputStream();
                    BufferedImage input = ImageIO.read(inputStream);

                    //Writing file to path
                    File outputFile = new File(path + name + TypeVal);
                    ImageIO.write(input, type, outputFile);
                    imagePath = outputFile.getAbsolutePath();
                    //   System.out.println("--------image path::"+imagePath);


                } catch (Exception ex) {
                    // handle exception
                    ex.printStackTrace();
                } finally {
                    try {
                        inputStream.close();
                    } catch (IOException e) {
                    }
                }
            } else {
                imagePath = "NO";
            }
        }
        setImageFile(null);
        return imagePath;

    }

    public void saveAL(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedByNew("ProductVO1Iterator", "LastUpdatedBy");
        if (getImageFile() != null) {
            String flag = SaveuploadFile(imageFile);
            //   System.out.println("in save al image path::"+flag);

            /*             if ("NO".equalsIgnoreCase(flag)) {
            FacesMessage msg =
            new FacesMessage("This is not an Image file, Please upload supported file type (.jpg,.png etc)");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext.getCurrentInstance().addMessage(null, msg);

            } */
            OperationBinding ob = ADFUtils.findOperation("AddDrawingImagePath");
            ob.getParamsMap().put("ImagePath", flag);
            ob.execute();
        }
        Integer i = 0;
        i = (Integer) bindingOutputText.getValue();
        //   System.out.println("iiiii is--------->"+i);

        OperationBinding op = ADFUtils.findOperation("getProductCode");
        op.execute();
        //   System.out.println("Result is------> "+op.getResult());
        if (op.getResult() != null && op.getResult().toString().equalsIgnoreCase("E")) {
            ADFUtils.findOperation("copyProdSpecsDesgnUnit").execute();
            ADFUtils.findOperation("Commit").execute();
            ADFUtils.showMessage(" Record Updated Successfully", 2);
        } else if (op.getResult() != null && op.getResult().toString() != "" && op.getResult().equals("S")) {
            //   System.out.println("+++++ in the save mode+++++++++++");
            ADFUtils.findOperation("copyProdSpecsDesgnUnit").execute();
            ADFUtils.findOperation("Commit").execute();
            ADFUtils.showMessage("Record Saved Successfully.New Product Code is " +
                                 ADFUtils.evaluateEL("#{bindings.Code.inputValue}"), 2);
        } else if (op.getResult() != null && op.getResult().toString().equalsIgnoreCase("N")) {
            ADFUtils.showMessage("Product Code could not be generated.Try Again !!", 0);
        } else if (op.getResult() != null && op.getResult().toString().equalsIgnoreCase("MNL")) {
            //   System.out.println("and The Manula Entry ------>>"+op.getResult());
            if (i == 0) {
                getProductCodeBinding().setDisabled(false);
                if (productCodeBinding.getValue() != null) {
                    ADFUtils.findOperation("copyProdSpecsDesgnUnit").execute();
                    ADFUtils.findOperation("Commit").execute();
                    ADFUtils.showMessage("Record Saved Successfully.New Product Code is " +
                                         ADFUtils.evaluateEL("#{bindings.Code.inputValue}"), 2);
                } else {
                    FacesMessage message = new FacesMessage("Product Code must be required.");
                    message.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(productCodeBinding.getClientId(), message);
                }
            } else {
                ADFUtils.findOperation("copyProdSpecsDesgnUnit").execute();
                ADFUtils.findOperation("Commit").execute();
                ADFUtils.showMessage(" Record Updated Successfully", 2);
            }
        }


    }

    public void drgFileNameVCL(ValueChangeEvent valueChangeEvent) {
        if (valueChangeEvent.getNewValue() != null) {
            setImageFile((UploadedFile) valueChangeEvent.getNewValue());
        }
    }

    public void onBlurNotify(ClientEvent clientEvent) {

        //        OperationBinding op=ADFUtils.findOperation("copyProdCodeDesc");
        //        op.execute();
    }


    public void descriptionVCL(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        //        OperationBinding op=ADFUtils.findOperation("copyProdCodeDesc");
        //        op.execute();
    }

    public void codeVCL(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        //        OperationBinding op=ADFUtils.findOperation("copyProdCodeDesc");
        //        op.execute();
    }


    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete").execute();
            //   System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }

        AdfFacesContext.getCurrentInstance().addPartialTarget(itemUnitStoreTableBinding);
    }

    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }

    public void cevModeDisableComponent(String mode) {
        //            FacesContext fctx = FacesContext.getCurrentInstance();
        //            ELContext elctx = fctx.getELContext();
        //            Application jsfApp = fctx.getApplication();
        //            //create a ValueExpression that points to the ADF binding layer
        //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
        //            //
        //            ValueExpression valueExpr = exprFactory.createValueExpression(
        //                                         elctx,
        //                                         "#{pageFlowScope.mode=='E'}",
        //                                          Object.class
        //                                         );
        //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
        if (mode.equals("E")) {
            getProductCodeBinding().setDisabled(true);
            getGstCodeBinding().setDisabled(true);
            getUombinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            getDetailcreateBinding().setDisabled(false);
            prodSubTypeBinding.setDisabled(true);
            getDetailcreate2Binding().setDisabled(false);
        } else if (mode.equals("C")) {
            getProductCodeBinding().setDisabled(true);
            getGstCodeBinding().setDisabled(true);
            getDetailcreateBinding().setDisabled(false);
            getHeaderEditBinding().setDisabled(true);
            getDetailcreate2Binding().setDisabled(false);

        } else if (mode.equals("V")) {
            getTab1Binding().setDisabled(false);
            getTab2Binding().setDisabled(false);
            getDetailcreateBinding().setDisabled(true);
            getDetailcreate2Binding().setDisabled(true);
            getPurchaseBinding().setDisabled(false);
            getQualityBinding().setDisabled(false);
            getMrpBinding().setDisabled(false);
            getSaleBinding().setDisabled(false);
            getFinanceBinding().setDisabled(false);
            getCostingBinding().setDisabled(false);
        }

    }


    public void setItemUnitStoreTableBinding(RichTable itemUnitStoreTableBinding) {
        this.itemUnitStoreTableBinding = itemUnitStoreTableBinding;
    }

    public RichTable getItemUnitStoreTableBinding() {
        return itemUnitStoreTableBinding;
    }

    public void setProductCodeBinding(RichInputText productCodeBinding) {
        this.productCodeBinding = productCodeBinding;
    }

    public RichInputText getProductCodeBinding() {
        return productCodeBinding;
    }

    public void setGstCodeBinding(RichInputText gstCodeBinding) {
        this.gstCodeBinding = gstCodeBinding;
    }

    public RichInputText getGstCodeBinding() {
        return gstCodeBinding;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setDetailcreateBinding(RichButton detailcreateBinding) {
        this.detailcreateBinding = detailcreateBinding;
    }

    public RichButton getDetailcreateBinding() {
        return detailcreateBinding;
    }


    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void SpaceValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (object != null) {
            if (object.toString().contains(" ")) {
                {
                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                  "Product Code Should not contain space", null));
                }
            }
        }
    }

    //    public void DescriptionValidate(FacesContext facesContext, UIComponent uIComponent, Object object) {
    //            if(object != null)
    //            {
    //                if(object.toString().contains(" "))
    //                {
    //        {
    //               throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Product Description Should not contain space", null));
    //           }
    //        }
    //        }
    //    }


    public void setProductDescriptionValdator(RichInputText productDescriptionValdator) {
        this.productDescriptionValdator = productDescriptionValdator;
    }

    public RichInputText getProductDescriptionValdator() {
        return productDescriptionValdator;
    }

    public void setProdSubTypeBinding(RichSelectOneChoice prodSubTypeBinding) {
        this.prodSubTypeBinding = prodSubTypeBinding;
    }

    public RichSelectOneChoice getProdSubTypeBinding() {
        return prodSubTypeBinding;
    }

    public void setUombinding(RichSelectOneChoice uombinding) {
        this.uombinding = uombinding;
    }

    public RichSelectOneChoice getUombinding() {
        return uombinding;
    }

    public void setItemPackingUomTableBinding(RichTable itemPackingUomTableBinding) {
        this.itemPackingUomTableBinding = itemPackingUomTableBinding;
    }

    public RichTable getItemPackingUomTableBinding() {
        return itemPackingUomTableBinding;
    }

    public void setTab2Binding(RichShowDetailItem tab2Binding) {
        this.tab2Binding = tab2Binding;
    }

    public RichShowDetailItem getTab2Binding() {
        return tab2Binding;
    }

    public void setTab1Binding(RichShowDetailItem tab1Binding) {
        this.tab1Binding = tab1Binding;
    }

    public RichShowDetailItem getTab1Binding() {
        return tab1Binding;
    }

    public void setDetailcreate2Binding(RichButton detailcreate2Binding) {
        this.detailcreate2Binding = detailcreate2Binding;
    }

    public RichButton getDetailcreate2Binding() {
        return detailcreate2Binding;
    }

    public void setPurchaseBinding(RichShowDetailItem purchaseBinding) {
        this.purchaseBinding = purchaseBinding;
    }

    public RichShowDetailItem getPurchaseBinding() {
        return purchaseBinding;
    }

    public void setQualityBinding(RichShowDetailItem qualityBinding) {
        this.qualityBinding = qualityBinding;
    }

    public RichShowDetailItem getQualityBinding() {
        return qualityBinding;
    }

    public void setMrpBinding(RichShowDetailItem mrpBinding) {
        this.mrpBinding = mrpBinding;
    }

    public RichShowDetailItem getMrpBinding() {
        return mrpBinding;
    }

    public void setSaleBinding(RichShowDetailItem saleBinding) {
        this.saleBinding = saleBinding;
    }

    public RichShowDetailItem getSaleBinding() {
        return saleBinding;
    }

    public void setFinanceBinding(RichShowDetailItem financeBinding) {
        this.financeBinding = financeBinding;
    }

    public RichShowDetailItem getFinanceBinding() {
        return financeBinding;
    }

    public void setCostingBinding(RichShowDetailItem costingBinding) {
        this.costingBinding = costingBinding;
    }

    public RichShowDetailItem getCostingBinding() {
        return costingBinding;
    }

    public String saveAndCloseAction() {
        ADFUtils.setLastUpdatedByNew("ProductVO1Iterator", "LastUpdatedBy");
        if (getImageFile() != null) {
            String flag = SaveuploadFile(imageFile);
            OperationBinding ob = ADFUtils.findOperation("AddDrawingImagePath");
            ob.getParamsMap().put("ImagePath", flag);
            ob.execute();
        }
        Integer i = 0;
        i = (Integer) bindingOutputText.getValue();

        OperationBinding op = ADFUtils.findOperation("getProductCode");
        op.execute();

        if (op.getResult() != null && op.getResult().toString().equalsIgnoreCase("MND")) {
            return null;
        }
        if (op.getResult() != null && op.getResult().toString().equalsIgnoreCase("E")) {
            ADFUtils.findOperation("copyProdSpecsDesgnUnit").execute();
            ADFUtils.findOperation("Commit").execute();
            ADFUtils.showMessage(" Record Updated Successfully", 2);
        } else if (op.getResult() != null && op.getResult().toString() != "" && op.getResult().equals("S")) {
            ADFUtils.findOperation("copyProdSpecsDesgnUnit").execute();
            ADFUtils.findOperation("Commit").execute();
            ADFUtils.showMessage("Record Saved Successfully.New Product Code is " +
                                 ADFUtils.evaluateEL("#{bindings.Code.inputValue}"), 2);
        } else if (op.getResult() != null && op.getResult().toString().equalsIgnoreCase("N")) {
            ADFUtils.showMessage("Product Code could not be generated.Try Again !!", 0);
        } else if (op.getResult() != null && op.getResult().toString().equalsIgnoreCase("MNL")) {
            if (i == 0) {
                getProductCodeBinding().setDisabled(false);
                if (productCodeBinding.getValue() != null) {
                    ADFUtils.findOperation("copyProdSpecsDesgnUnit").execute();
                    ADFUtils.findOperation("Commit").execute();
                    ADFUtils.showMessage("Record Saved Successfully.New Product Code is " +
                                         ADFUtils.evaluateEL("#{bindings.Code.inputValue}"), 2);
                } else {
                    FacesMessage message = new FacesMessage("Product Code must be required.");
                    message.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(productCodeBinding.getClientId(), message);
                }
            } else {
                ADFUtils.findOperation("copyProdSpecsDesgnUnit").execute();
                ADFUtils.findOperation("Commit").execute();
                ADFUtils.showMessage(" Record Updated Successfully", 2);
            }
        }
        return "saveAndClose";
    }
}