package terms.eng.setup.ui.bean;

import javax.faces.application.FacesMessage;

import javax.faces.context.FacesContext;

import oracle.adf.model.OperationBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

public class SearchInprocessProdCheckpointsBean {
    private RichTable inProcessProductionTableBinding;

    public SearchInprocessProdCheckpointsBean() {
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) 
    {
            if(dialogEvent.getOutcome().name().equals("ok"))
                          {
                          OperationBinding op = null;
                          ADFUtils.findOperation("Delete").execute();
                          op = (OperationBinding) ADFUtils.findOperation("Commit");
                          Object rst = op.execute();
                          System.out.println("Record Delete Successfully");
                              if(op.getErrors().isEmpty()){
                                  FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
                                  FacesContext fc = FacesContext.getCurrentInstance();
                                  fc.addMessage(null, Message);
                              } else if (!op.getErrors().isEmpty())
                              {
                                 OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
                                 Object rstr = opr.execute();
                                 FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
            Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                                 FacesContext fc = FacesContext.getCurrentInstance();
                                 fc.addMessage(null, Message);
                               }
                          }


                AdfFacesContext.getCurrentInstance().addPartialTarget(inProcessProductionTableBinding);
    }

    public void setInProcessProductionTableBinding(RichTable inProcessProductionTableBinding) {
        this.inProcessProductionTableBinding = inProcessProductionTableBinding;
    }

    public RichTable getInProcessProductionTableBinding() {
        return inProcessProductionTableBinding;
    }
}
