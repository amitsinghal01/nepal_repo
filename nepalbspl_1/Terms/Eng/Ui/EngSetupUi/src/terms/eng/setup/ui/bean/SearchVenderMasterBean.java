package terms.eng.setup.ui.bean;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class SearchVenderMasterBean {
    private RichTable searchVenderTableBinding;

    public SearchVenderMasterBean() {
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent)
        {
            if(dialogEvent.getOutcome().name().equals("ok"))
                {
                OperationBinding op = null;
                ADFUtils.findOperation("Delete").execute();
                op = (OperationBinding) ADFUtils.findOperation("Commit");
                Object rst = op.execute();
                System.out.println("Record Delete Successfully");
                    if(op.getErrors().isEmpty()){
                        FacesMessage Message = new FacesMessage("Record Deleted Successfully");   
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                        FacesContext fc = FacesContext.getCurrentInstance();   
                        fc.addMessage(null, Message);
                    } else if (!op.getErrors().isEmpty())
                    {
                       OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
                       Object rstr = opr.execute();
                       FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                       Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                       FacesContext fc = FacesContext.getCurrentInstance();
                       fc.addMessage(null, Message);
                     }
                }

            AdfFacesContext.getCurrentInstance().addPartialTarget(searchVenderTableBinding);
                
        }

    public void setSearchVenderTableBinding(RichTable searchVenderTableBinding) {
        this.searchVenderTableBinding = searchVenderTableBinding;
    }

    public RichTable getSearchVenderTableBinding() {
        return searchVenderTableBinding;
    }
}
