package terms.eng.setup.ui.bean;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.validator.ValidatorException;

import oracle.adf.model.OperationBinding;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import terms.eng.setup.model.view.SubGroupMasterVORowImpl;

public class SubGroupMasterBean {
    private RichTable subGroupTableBinding;
    private RichInputComboboxListOfValues bindGroupCd;
    private String editAction="V";
    private RichInputText subGroupCodeBinding;

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public SubGroupMasterBean() {
    }

    public void deletePopUpDialogDL(DialogEvent dialogEvent) 
    {
            if(dialogEvent.getOutcome().name().equals("ok"))
            {
            OperationBinding op = null;
            ADFUtils.findOperation("Delete").execute();
            op = (OperationBinding) ADFUtils.findOperation("Commit");
            if (getEditAction().equals("V")) {
            Object rst = op.execute();
            }
            System.out.println("Record Delete Successfully");
            if(op.getErrors().isEmpty()){
            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
            } else if (!op.getErrors().isEmpty())
            {
            OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
            Object rstr = opr.execute();
            FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
            Message.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
            }
            }
            AdfFacesContext.getCurrentInstance().addPartialTarget(subGroupTableBinding);

    }

    public void setSubGroupTableBinding(RichTable subGroupTableBinding) {
        this.subGroupTableBinding = subGroupTableBinding;
    }

    public RichTable getSubGroupTableBinding() {
        return subGroupTableBinding;
    }

    public void subGroupCodeValidator(FacesContext facesContext, UIComponent uIComponent, Object object) 
    {
        try {
            if (object != null && bindGroupCd.getValue() != null) {
                
                System.out.println("INSIDE VALIDATOR");
                try{
                if (!object.toString().substring(0, 2).equals(bindGroupCd.getValue().toString().substring(0, 2))) {
                    System.out.println("INSIDE OF IFFFFFFFFF"+object.toString().substring(0, 2)+
                                       "GROUP CD"+bindGroupCd.getValue().toString().substring(0, 2));
//                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
//                                                                  "First two characters of Sub Group Code must be of Group Code",
//                                                                  null));
//                    ADFUtils.showMessage("First two characters of Sub Group Code must be of Group Code", 0);
                                FacesMessage message = new FacesMessage("First two characters of Sub Group Code must be of Group Code.");
                    message.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext context = FacesContext.getCurrentInstance();
                    context.addMessage(subGroupCodeBinding.getClientId(), message);
                }
                }
                catch(StringIndexOutOfBoundsException se){
                  System.out.println("Exception Caught");  
                }
            }
        } catch (ValidatorException ve) {
            // TODO: Add catch code
           // ve.printStackTrace();
        }
    }

    public void setBindGroupCd(RichInputComboboxListOfValues bindGroupCd) {
        this.bindGroupCd = bindGroupCd;
    }

    public RichInputComboboxListOfValues getBindGroupCd() {
        return bindGroupCd;
    }

    public void setSubGroupCodeBinding(RichInputText subGroupCodeBinding) {
        this.subGroupCodeBinding = subGroupCodeBinding;
    }

    public RichInputText getSubGroupCodeBinding() {
        return subGroupCodeBinding;
    }

    public void ConstrainVCE(ValueChangeEvent valueChangeEvent) 
    {
    valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
    if(valueChangeEvent!=null && editAction.equals("E"))
    {
    String Oldvalue=(String) valueChangeEvent.getOldValue(); 
    System.out.println("old value is=> "+Oldvalue);
    
        OperationBinding  oppr=(OperationBinding)ADFUtils.findOperation("checkTest"); 
        oppr.getParamsMap().put("test", Oldvalue.toString());        
        oppr.execute();        
       
       
        if(oppr.getResult() !=null && oppr.getResult().equals("Y"))
        {
            FacesMessage Message = new FacesMessage("Record cannot be change. Child record found.");
            Message.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);  
            subGroupCodeBinding.setValue(Oldvalue);
            
        }
        
        
    }
    
    
    
    
    }

    public void saveAL(ActionEvent actionEvent) {
        DCIteratorBinding binding=ADFUtils.findIterator("SubGroupMasterVO1Iterator");
        binding.executeQuery();
        SubGroupMasterVORowImpl row = (SubGroupMasterVORowImpl) binding.getCurrentRow();
        
        String sub = (String) row.getAttribute("SubGrpCd");
        String group = (String) row.getAttribute("GroupCd");
        System.out.println("sub group cd is==>>"+sub+"group cd is==>>"+group);
        
        if (sub != null && group != null) {
            
            System.out.println("INSIDE VALIDATOR");
            if (!sub.toString().substring(0, 2).equals(group.substring(0, 2))) {
                System.out.println("INSIDE OF IFFFFFFFFF"+sub.toString().substring(0, 2)+
                                   "GROUP CD"+group.substring(0, 2));
        //                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
        //                                                                  "First two characters of Sub Group Code must be of Group Code",
        //                                                                  null));
        //                    ADFUtils.showMessage("First two characters of Sub Group Code must be of Group Code", 0);
                            FacesMessage message = new FacesMessage("First two characters of Sub Group Code must be of Group Code.");
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(subGroupCodeBinding.getClientId(), message);
            }
            else{
                ADFUtils.findOperation("Commit").execute();
            }
        }
        
    }

    public void groupCdVCL(ValueChangeEvent valueChangeEvent) {
        
        if(valueChangeEvent!=null){
//        DCIteratorBinding binding=ADFUtils.findIterator("SubGroupMasterVO1Iterator");
//        binding.executeQuery();
//        SubGroupMasterVORowImpl row = (SubGroupMasterVORowImpl) binding.getCurrentRow();
        
//        String sub = (String) row.getAttribute("SubGrpCd");
        String group = (String) bindGroupCd.getValue();
        System.out.println("group code is==>>"+group);
        subGroupCodeBinding.setValue(group);
        AdfFacesContext.getCurrentInstance().addPartialTarget(subGroupTableBinding);
        }
    }
}
