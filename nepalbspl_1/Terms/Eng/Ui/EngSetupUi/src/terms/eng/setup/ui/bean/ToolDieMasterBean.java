package terms.eng.setup.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

public class ToolDieMasterBean {
    private RichPanelHeader pageRootBinding;
    private RichOutputText outputTextBinding;

    public ToolDieMasterBean() {
    }

    public void saveAll(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedByNew("ToolDiesMasterVO1Iterator","LastUpdatedBy");
        ADFUtils.findOperation("Commit").execute();
        ADFUtils.showMessage("Record Save Sucessfully", 2);
    }

    public void editAll(ActionEvent actionEvent) {
        modecheck();
    }

    public void setPageRootBinding(RichPanelHeader pageRootBinding) {
        this.pageRootBinding = pageRootBinding;
    }

    public RichPanelHeader getPageRootBinding() {
        return pageRootBinding;
    }

    public void setOutputTextBinding(RichOutputText outputTextBinding) {
        this.outputTextBinding = outputTextBinding;
    }

    public RichOutputText getOutputTextBinding() {
        modecheck();
        return outputTextBinding;
    }
    private void modecheck(){
            if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
                makeComponentHierarchyReadOnly(this.getPageRootBinding(), true);
                modeDisableComponent("V");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
                makeComponentHierarchyReadOnly(this.getPageRootBinding(), false);
                modeDisableComponent("C");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
                makeComponentHierarchyReadOnly(this.getPageRootBinding(), false);
                modeDisableComponent("E");
            }
        }
        
        private void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
            try {
                Method method1 =component.getClass().getMethod("setDisabled", boolean.class);
                if (method1 != null) {
                    method1.invoke(component, valueComponent);
                }
            } catch (NoSuchMethodException e) {
                try {
                    Method method =
                        component.getClass().getMethod("setReadOnly", boolean.class);
                    if (method != null) {
                        method.invoke(component, valueComponent);
                    }
                } catch (Exception e1) {
                    // e.printStackTrace();//silently eat this exception.
                }


            } catch (Exception e) {
                // e.printStackTrace();//silently eat this exception.
            }
            List<UIComponent> childComponents = component.getChildren();
            for (UIComponent comp : childComponents) {
                makeComponentHierarchyReadOnly(comp, valueComponent);
            }
        }

         private void modeDisableComponent(String mode) {
            if (mode.equals("E")) {

            } else if (mode.equals("C")) {

            } else if (mode.equals("V")) {
         //                   getDetaildeleteBinding().setDisabled(true);
             }
         }

}
