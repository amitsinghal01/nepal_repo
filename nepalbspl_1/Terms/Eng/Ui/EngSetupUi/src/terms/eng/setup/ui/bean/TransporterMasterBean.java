package terms.eng.setup.ui.bean;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

public class TransporterMasterBean {
    private RichTable transporterTableBinding;

    public TransporterMasterBean() {
    }
    private String editAction="V";
    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }
    public void SaveAL(ActionEvent actionEvent) {
       OperationBinding op = (OperationBinding) ADFUtils.findOperation("GenerateTransporter");
        op.execute();
        System.out.println("Result Save"+op.getResult());
        if(op.getResult()!=null && op.getResult().toString().equalsIgnoreCase("N"))
        {
           if (op.getErrors().isEmpty()) 
           {
               ADFUtils.findOperation("Commit").execute();
               FacesMessage Message = new FacesMessage("Record Update Successfully.");
               Message.setSeverity(FacesMessage.SEVERITY_INFO);
               FacesContext fc = FacesContext.getCurrentInstance();
               fc.addMessage(null, Message);
           }
        }
        else if (op.getResult() != null && op.getResult().toString() != "") 
        {
          if (op.getErrors().isEmpty()) 
          {
                System.out.println("CREATE MODE");
                ADFUtils.findOperation("Commit").execute();
                FacesMessage Message = new FacesMessage("Record Save Successfully.Transporter Code is "+op.getResult());
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
          }
        }
    }

    public void DeletePopupDialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
        {
        OperationBinding op = null;
        ADFUtils.findOperation("Delete").execute();
        op = (OperationBinding) ADFUtils.findOperation("Commit");
        if (getEditAction().equals("V")) {
        Object rst = op.execute();
        }
        System.out.println("Record Delete Successfully");
        if(op.getErrors().isEmpty()){
        FacesMessage Message = new FacesMessage("Record Deleted Successfully");
        Message.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        } else if (!op.getErrors().isEmpty())
        {
        OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
        Object rstr = opr.execute();
        FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
        Message.setSeverity(FacesMessage.SEVERITY_ERROR);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(transporterTableBinding);

    }

    public void setTransporterTableBinding(RichTable transporterTableBinding) {
        this.transporterTableBinding = transporterTableBinding;
    }

    public RichTable getTransporterTableBinding() {
        return transporterTableBinding;
    }
}
