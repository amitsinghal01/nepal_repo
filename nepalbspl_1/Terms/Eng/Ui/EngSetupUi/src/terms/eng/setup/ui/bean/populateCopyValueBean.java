package terms.eng.setup.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import javax.el.ELContext;
import javax.el.ExpressionFactory;

import javax.el.ValueExpression;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;
import javax.faces.validator.ValidatorException;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;


public class populateCopyValueBean {
    private RichTable createIncomingChkPtsTableBinding;
    private RichInputText processSequenceBinding;
    private RichInputComboboxListOfValues itemCodeBinding;
    private RichInputComboboxListOfValues processCodeBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichOutputText bindingOutputText;
    private RichButton headerEditBinding;
    private RichButton detailCreateBinding;
    private RichButton copyValueBinding;
    private RichInputText itemDescCopyBinding;
    private RichInputText siNoBinding;
    private RichInputComboboxListOfValues itemCodeCopyBinding;
    private RichButton limitButtonBinding;
    private RichButton copyButtonBinding;
   // private RichInputText baseLimitBinding;
    private RichInputText upperLimitBinding;
    private RichInputText lowerLimitBinding;
    private RichSelectOneChoice checkFlagBinding;
    private RichInputComboboxListOfValues uomBinding;
    private RichSelectOneChoice itemTypeBinding;
    //    private RichButton detailDeleteBinding;
  
    public populateCopyValueBean() {
    }

    public void populateCopyValueIncomingChk(ActionEvent actionEvent) 
    {
        if(itemCodeBinding.getValue()!=null)
        {
            
            if(itemCodeCopyBinding.getValue() !=null)
            {
                OperationBinding op=ADFUtils.findOperation("populateDataIncomingCheckpoints");
                Object rst=op.execute();
                System.out.println("--------Item Code Copy vAlue-----:: "+itemCodeCopyBinding.getValue());
                if(op.getResult()!=null && op.getResult().toString().equalsIgnoreCase("N"))
                {
                    ADFUtils.showMessage("Record not found for this entry.", 0);
                }
            }
            else
            {
             ADFUtils.showMessage("Please Select Item Code Copy Value.",0);   
             }
        }
        else{
            FacesMessage message = new FacesMessage("Item Code is required.");
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(itemCodeBinding.getClientId(), message);
        }
        if((Long)ADFUtils.evaluateEL("#{bindings.IncomingCheckPointsDetailVO1Iterator.estimatedRowCount}")>0 )
        {
            getItemTypeBinding().setDisabled(true);
            getItemCodeBinding().setDisabled(true);
            getItemCodeCopyBinding().setDisabled(true);
            getCopyButtonBinding().setDisabled(true);
            getDetailCreateBinding().setDisabled(true);
        }
        else{
            getItemTypeBinding().setDisabled(false);
            getItemCodeCopyBinding().setDisabled(false);
            getItemCodeBinding().setDisabled(false);
            getCopyButtonBinding().setDisabled(false);
            getDetailCreateBinding().setDisabled(false);
        }
       
    }

    public void sequenceNoIncomingChkpts(ActionEvent actionEvent) {
        if(itemCodeBinding.getValue()!=null)
        {
            ADFUtils.findOperation("CreateInsert").execute();
            siNoBinding.setValue(ADFUtils.evaluateEL("#{bindings.IncomingCheckPointsDetailVO1Iterator.estimatedRowCount}"));
            
            getItemTypeBinding().setDisabled(true);
            getItemCodeBinding().setDisabled(true);
            getItemCodeCopyBinding().setDisabled(true);
        }
        else{
            FacesMessage message = new FacesMessage("Item Code is required.");
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(itemCodeBinding.getClientId(), message);
        }
        
    }

    public void createIncomingCheckPoints(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                    {
                    ADFUtils.findOperation("Delete").execute();
                    //ADFUtils.findOperation("Commit").execute();    
                    System.out.println("Record Delete Successfully");
                   
                        FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                        FacesContext fc = FacesContext.getCurrentInstance();  
                        fc.addMessage(null, Message);  
                   
                    }

                AdfFacesContext.getCurrentInstance().addPartialTarget(createIncomingChkPtsTableBinding);
        }

    public void setCreateIncomingChkPtsTableBinding(RichTable createIncomingChkPtsTableBinding) {
        this.createIncomingChkPtsTableBinding = createIncomingChkPtsTableBinding;
    }

    public RichTable getCreateIncomingChkPtsTableBinding() {
        return createIncomingChkPtsTableBinding;
    }

    public void recordSaveIncomingCheckpoints(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedByNew("IncomingCheckPointsHeaderVO1Iterator","LastUpdatedBy");
        OperationBinding op=ADFUtils.findOperation("IncomingChkptValCheck");
        op.execute();
        if(op.getResult()!=null && op.getResult().equals("Y")){
                if((Long)ADFUtils.evaluateEL("#{bindings.IncomingCheckPointsDetailVO1Iterator.estimatedRowCount}")>0){
                 Integer i=0;
                 i=(Integer)bindingOutputText.getValue();
                 System.out.println("EDIT TRANS VALUE"+i);
                    if(i.equals(0))
                        {
                           ADFUtils.findOperation("Commit").execute();
                           FacesMessage Message = new FacesMessage("Record Save Successfully.");  
                           Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                           FacesContext fc = FacesContext.getCurrentInstance();  
                           fc.addMessage(null, Message);     
                        }
                        else
                        {   ADFUtils.findOperation("Commit").execute();
                            FacesMessage Message = new FacesMessage("Record Updated Successfully.");  
                            Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                            FacesContext fc = FacesContext.getCurrentInstance();  
                            fc.addMessage(null, Message);     
                        }
                }else{
                    ADFUtils.showMessage("Please Enter data in detail field.",0);
                }
            }
        else{
                ADFUtils.showMessage("Either Lower Limit or Upper Limit is required in detail table.",0);
            }
        
    }

    public String resolvEl(String data)
        {
              FacesContext fc = FacesContext.getCurrentInstance();
              Application app = fc.getApplication();
              ExpressionFactory elFactory = app.getExpressionFactory();
              ELContext elContext = fc.getELContext();
              ValueExpression valueExp = elFactory.createValueExpression(elContext, data, Object.class);
              String Message=valueExp.getValue(elContext).toString();
              return Message;
        }

    public void setProcessSequenceBinding(RichInputText processSequenceBinding) {
        this.processSequenceBinding = processSequenceBinding;
    }

    public RichInputText getProcessSequenceBinding() {
        return processSequenceBinding;
    }

    public void setItemCodeBinding(RichInputComboboxListOfValues itemCodeBinding) {
        this.itemCodeBinding = itemCodeBinding;
    }

    public RichInputComboboxListOfValues getItemCodeBinding() {
        return itemCodeBinding;
    }

    public void setProcessCodeBinding(RichInputComboboxListOfValues processCodeBinding) {
        this.processCodeBinding = processCodeBinding;
    }

    public RichInputComboboxListOfValues getProcessCodeBinding() {
        return processCodeBinding;
    }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
            try {
                Method method1 =
                    component.getClass().getMethod("setDisabled", boolean.class);
                if (method1 != null) {
                    method1.invoke(component, valueComponent);
                }
            } catch (NoSuchMethodException e) {
                try {
                    Method method =
                        component.getClass().getMethod("setReadOnly", boolean.class);
                    if (method != null) {
                        method.invoke(component, valueComponent);
                    }
                } catch (Exception e1) {
                    // e.printStackTrace();//silently eat this exception.
                }


            } catch (Exception e) {
                // e.printStackTrace();//silently eat this exception.
            }
            List<UIComponent> childComponents = component.getChildren();
            for (UIComponent comp : childComponents) {
                makeComponentHierarchyReadOnly(comp, valueComponent);
            }
        }
    

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        
        cevmodecheck();
        return bindingOutputText;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void EditButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }
    private void cevmodecheck(){
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }
    public void cevModeDisableComponent(String mode) {
    //            FacesContext fctx = FacesContext.getCurrentInstance();
    //            ELContext elctx = fctx.getELContext();
    //            Application jsfApp = fctx.getApplication();
    //            //create a ValueExpression that points to the ADF binding layer
    //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
    //            //
    //            ValueExpression valueExpr = exprFactory.createValueExpression(
    //                                         elctx,
    //                                         "#{pageFlowScope.mode=='E'}",
    //                                          Object.class
    //                                         );
    //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
        if (mode.equals("E")) {
                getCopyButtonBinding().setDisabled(true);
                getItemCodeCopyBinding().setDisabled(true);
                getProcessSequenceBinding().setDisabled(true);
                getHeaderEditBinding().setDisabled(true);
                getDetailCreateBinding().setDisabled(false);
                getItemDescCopyBinding().setDisabled(true);
                getItemCodeBinding().setDisabled(true);
                getProcessCodeBinding().setDisabled(true);
                getSiNoBinding().setDisabled(true);
                getItemTypeBinding().setDisabled(true);

        } else if (mode.equals("C")) {
            getHeaderEditBinding().setDisabled(true);
            getProcessSequenceBinding().setDisabled(true);
            getDetailCreateBinding().setDisabled(false);
            getItemDescCopyBinding().setDisabled(true);
            getSiNoBinding().setDisabled(true);
            
           
        } else if (mode.equals("V")) {
            getCopyButtonBinding().setDisabled(true);
            getLimitButtonBinding().setDisabled(false);
            getItemCodeCopyBinding().setDisabled(true);
            getDetailCreateBinding().setDisabled(true);
          // getDetailDeleteBinding().setDisabled(true);
        }
    }

    public void setDetailCreateBinding(RichButton detailCreateBinding) {
        this.detailCreateBinding = detailCreateBinding;
    }

    public RichButton getDetailCreateBinding() {
        return detailCreateBinding;
    }

//    public void setDetailDeleteBinding(RichButton detailDeleteBinding) {
//        this.detailDeleteBinding = detailDeleteBinding;
//    }
//
//    public RichButton getDetailDeleteBinding() {
//        return detailDeleteBinding;
//    }

    public void setCopyValueBinding(RichButton copyValueBinding) {
        this.copyValueBinding = copyValueBinding;
    }

    public RichButton getCopyValueBinding() {
        return copyValueBinding;
    }

    public void setItemDescCopyBinding(RichInputText itemDescCopyBinding) {
        this.itemDescCopyBinding = itemDescCopyBinding;
    }

    public RichInputText getItemDescCopyBinding() {
        return itemDescCopyBinding;
    }

    public void setSiNoBinding(RichInputText siNoBinding) {
        this.siNoBinding = siNoBinding;
    }

    public RichInputText getSiNoBinding() {
        return siNoBinding;
    }

    public void setItemCodeCopyBinding(RichInputComboboxListOfValues itemCodeCopyBinding) {
        this.itemCodeCopyBinding = itemCodeCopyBinding;
    }

    public RichInputComboboxListOfValues getItemCodeCopyBinding() {
        return itemCodeCopyBinding;
    }

    public void setLimitButtonBinding(RichButton limitButtonBinding) {
        this.limitButtonBinding = limitButtonBinding;
    }

    public RichButton getLimitButtonBinding() {
        return limitButtonBinding;
    }

    public void setCopyButtonBinding(RichButton copyButtonBinding) {
        this.copyButtonBinding = copyButtonBinding;
    }

    public RichButton getCopyButtonBinding() {
        return copyButtonBinding;
    }

//    public void upperLimitValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
//            if(object !=null && baseLimitBinding.getValue() !=null)
//            {
//                BigDecimal baselimit = (BigDecimal)baseLimitBinding.getValue();
//                BigDecimal upperlimit = (BigDecimal) object;
//                
//                if(upperlimit.compareTo(baselimit)!=1)
//                {
//                     throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Upper Limit should be greater than to Base Limit.", null));
//                 }
//                
//            }
//
//
//    }

//    public void setBaseLimitBinding(RichInputText baseLimitBinding) {
//        this.baseLimitBinding = baseLimitBinding;
//    }
//
//    public RichInputText getBaseLimitBinding() {
//        return baseLimitBinding;
//    }

    public void setUpperLimitBinding(RichInputText upperLimitBinding) {
        this.upperLimitBinding = upperLimitBinding;
    }

    public RichInputText getUpperLimitBinding() {
        return upperLimitBinding;
    }

    public void setLowerLimitBinding(RichInputText lowerLimitBinding) {
        this.lowerLimitBinding = lowerLimitBinding;
    }

    public RichInputText getLowerLimitBinding() {
        return lowerLimitBinding;
    }

//    public void lowerLimitValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
//        if(object !=null && baseLimitBinding.getValue() !=null)
//        {
//            BigDecimal baselimit = (BigDecimal)baseLimitBinding.getValue();
//            BigDecimal lowerlimit = (BigDecimal) object;
//            
//            if(lowerlimit.compareTo(baselimit)!=-1)
//            {
//                 throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Lower Limit should be less than to Base Limit.", null));
//             }
//            
//        }
//
//    }

    public void checkFlagVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce!=null){
//            if(checkFlagBinding.getValue().equals(1)){ 
//                getUpperLimitBinding().setDisabled(true);
//                getBaseLimitBinding().setDisabled(true);
//                getLowerLimitBinding().setDisabled(true);
//            }
//            else{
//                getUpperLimitBinding().setDisabled(false);
//                getBaseLimitBinding().setDisabled(false);
//                getLowerLimitBinding().setDisabled(false);
//            }
        }
    }

    public void setCheckFlagBinding(RichSelectOneChoice checkFlagBinding) {
        this.checkFlagBinding = checkFlagBinding;
    }

    public RichSelectOneChoice getCheckFlagBinding() {
        return checkFlagBinding;
    }

    public void inspectionParaVCL(ValueChangeEvent valueChangeEvent) 
    {
            valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
            if(valueChangeEvent!=null)
            {
    
            OperationBinding opr=ADFUtils.findOperation("checkDuplicateICPara");
            opr.getParamsMap().put("inspParameter",valueChangeEvent.getNewValue());
            Object obj=opr.execute();
            if(opr.getResult().equals("N"))
            {
                ADFUtils.showMessage("Inspection Parameter is duplicate.Please Check.",0);
            } 
     }
      AdfFacesContext.getCurrentInstance().addPartialTarget(createIncomingChkPtsTableBinding);
            
  }
    
    public String saveAndCloseAL()
    {        
         ADFUtils.setLastUpdatedByNew("IncomingCheckPointsHeaderVO1Iterator","LastUpdatedBy");
        
        OperationBinding op=ADFUtils.findOperation("IncomingChkptValCheck");
        op.execute();
        if(op.getResult()!=null && op.getResult().equals("Y")){
                if((Long)ADFUtils.evaluateEL("#{bindings.IncomingCheckPointsDetailVO1Iterator.estimatedRowCount}")>0){
                 Integer i=0;
                 i=(Integer)bindingOutputText.getValue();
                 System.out.println("EDIT TRANS VALUE"+i);
                    if(i.equals(0))
                        {
                           ADFUtils.findOperation("Commit").execute();
                           FacesMessage Message = new FacesMessage("Record Save Successfully.");  
                           Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                           FacesContext fc = FacesContext.getCurrentInstance();  
                           fc.addMessage(null, Message);  
                           return "save and close";
                        }
                        else
                        {   ADFUtils.findOperation("Commit").execute();
                            FacesMessage Message = new FacesMessage("Record Updated Successfully.");  
                            Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                            FacesContext fc = FacesContext.getCurrentInstance();  
                            fc.addMessage(null, Message); 
                            return "save and close";
                        }
                }else{
                    ADFUtils.showMessage("Please Enter data in detail field.",0);
                }
            }
        else{
                ADFUtils.showMessage("Either Lower Limit or Upper Limit is required in detail table.",0);
                //return null;
            }
        return null;
    }

    public void calculativeFlagVCL(ValueChangeEvent vce) {
        if(vce.getNewValue()!=vce.getOldValue())
        {
           // System.out.println("Item Type VCL:"+.getValue());
            OperationBinding op = ADFUtils.findOperation("valueSetNullIncomingCheckPts");
            op.execute();
        }
    }

    public void setUomBinding(RichInputComboboxListOfValues uomBinding) {
        this.uomBinding = uomBinding;
    }

    public RichInputComboboxListOfValues getUomBinding() {
        return uomBinding;
    }

    public void setItemTypeBinding(RichSelectOneChoice itemTypeBinding) {
        this.itemTypeBinding = itemTypeBinding;
    }

    public RichSelectOneChoice getItemTypeBinding() {
        return itemTypeBinding;
    }
}
