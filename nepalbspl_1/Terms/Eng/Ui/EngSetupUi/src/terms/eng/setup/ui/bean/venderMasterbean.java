package terms.eng.setup.ui.bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import java.io.OutputStream;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import javax.el.ELContext;
import javax.el.ExpressionFactory;

import javax.el.ValueExpression;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import javax.faces.validator.ValidatorException;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputFile;

import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

import org.apache.myfaces.trinidad.model.UploadedFile;

import terms.eng.setup.model.view.VendUnitVORowImpl;
import terms.eng.setup.model.view.VendorMasterVORowImpl;
import terms.eng.setup.model.view.VendorRegdAddressVORowImpl;

public class venderMasterbean {
    private RichInputFile uploadFileBind;
    private RichOutputText filenNmPath;
    private RichTable vendUnitTableBinding;
    private RichTable vendorConttactTableBinding;
    private RichTable vendorSuppTableBinding;
    private RichTable vendorItemTableBinding;
    private RichTable vendorCertfTableBinding;
    private RichTable docAttachRefTableBinding;
    private RichInputText stateGstBinding;
    private RichInputText gstRegBinding;
    private RichPanelGroupLayout bindingPanelGroupLayout;
    private RichOutputText bindingOutputText;
    private RichSelectOneChoice etdsBinding;
    private RichButton detailcreatebutton;
    private RichButton detailcreateBinding;
    private RichButton detaildeleteBinding;
    private RichPopup deletepopupBinding;
    private RichInputText vendorcodeBinding;
    private RichButton headerEditBinding;
    private RichShowDetailItem commercialTabBinding;
    private RichShowDetailItem generalTabBinding;
    private RichShowDetailItem addressTabBinding;
    private RichShowDetailItem contactTabBinding;
    private RichShowDetailItem referenceTabBinding;
    private RichShowDetailItem itemMappingTabBinding;
    private RichShowDetailItem otherTabBinding;
    private RichShowDetailItem certificateTabBinding;
    private RichInputText vendorNameBinding;
    private RichInputComboboxListOfValues untCodeBinding;
    private RichInputText addressStateBinding;
    private RichInputText countryBinding;
    private RichInputText cityCodeBinding;
    private RichInputText pinCodeBinding;
    private RichInputText stateCodeBinding;
    private RichButton contactCreateBinding;
//    private RichButton contactDeleteBinding;
    private RichButton referenceCreateBinding;
    private RichButton refrenceDeleteBinding;
    private RichButton itemMappingCreateBinding;
    private RichButton itemMappingDeleteBinding;
    private RichButton certificateCreateBinding;
    private RichButton certificateDeleteBinding;
    private RichButton docCreateBinding;
    private RichButton docDeleteBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichSelectOneChoice paytermsBinding;
    private RichInputText addressBinding;
    
    private String Msg="";
    private RichInputText panNumberBinding;
    private RichShowDetailItem certificateChildTabBinding;
    private RichShowDetailItem attachmentChildTabBinding;
    private RichInputText transitPeriodBinding;
    private RichInputText address2Binding;
    private RichInputText address3Binding;
    private RichTable vednorItemDtlTableBinding;
    private RichButton vendorItemDtlButtonBinding;
    private RichInputText itemCdDtlBinding;
    private RichInputComboboxListOfValues manufactureBinding;
    private RichSelectOneChoice regFlagBinding;
    private RichInputComboboxListOfValues cityBinding;
    private RichSelectOneChoice vendorFlagBinding;
    private RichInputText vendorNameTransBinding;

    public venderMasterbean() {
    }

    public void AddRegAddressAl(ActionEvent actionEvent) {
        // Add event code here...kaushal  setvendorCode
       
        ADFUtils.findOperation("CreateInsert").execute();
        ADFUtils.findOperation("setvendorCode").execute();
        
        
    }

    public void addVendUnitAL(ActionEvent actionEvent) {
       
        ADFUtils.findOperation("CreateInsert1").execute();
        ADFUtils.findOperation("setvendorCode").execute();
       
        
    }

    public void addVendorConttactAL(ActionEvent actionEvent) {
        // Add event code here...CreateInsert2
        
        ADFUtils.findOperation("CreateInsert2").execute();
        ADFUtils.findOperation("setvendorCode").execute();
        
    }

    public void addVenderItemAL(ActionEvent actionEvent) {
        // Add event code here...CreateInsert3
       
        ADFUtils.findOperation("CreateInsert3").execute();
        ADFUtils.findOperation("setvendorCode").execute();
        vendorItemDtlButtonBinding.setDisabled(false);
        
    }

    public void addVendorBankAL(ActionEvent actionEvent) {
        // Add event code here...CreateInsert4
       
        ADFUtils.findOperation("CreateInsert4").execute();
        ADFUtils.findOperation("setvendorCode").execute();
        
    }

    public void AddVenderSuplierAL(ActionEvent actionEvent) {
        // Add event code here...CreateInsert5
        
        ADFUtils.findOperation("CreateInsert5").execute();
        ADFUtils.findOperation("setvendorCode").execute();
       
    }

    public void addCertificateAL(ActionEvent actionEvent) {
       
        ADFUtils.findOperation("CreateInsert6").execute();
        ADFUtils.findOperation("setvendorCode").execute();
    }


    
   

 

    public void setUploadFileBind(RichInputFile uploadFileBind) {
        this.uploadFileBind = uploadFileBind;
    }

    public RichInputFile getUploadFileBind() {
        return uploadFileBind;
    }
    
    InputStream inputStream = null;
    InputStream SaveInputStream = null;
    private UploadedFile PhotoFile = null;
    private String AbsoluteFilePath = "";
    String imagePath = "";
    Boolean check = false;
   

    public void uploadFileVCE(ValueChangeEvent vce) {
        if (vce.getNewValue() != null) {
            setPhotoFile((UploadedFile) vce.getNewValue());
            UploadedFile myfile = (UploadedFile) vce.getNewValue();
            String path = null;

            if (myfile == null) {

            } else {
                File dir= new File("/tmp/pht");
                File savedir= new File("/home/beta6/pics");
                if(!dir.exists()){
                    try{
                        dir.mkdir();
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }
                }
                if(!savedir.exists()){
                    try{
                        savedir.mkdir();
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }
                }
                
                // All uploaded files will be stored in below path
                path = "/home/beta6/pics/" + myfile.getFilename();
                //                Imagepath=path;
                File f = new File(path);
                if (!f.exists()) {
                    try {
                        f.createNewFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                inputStream = null;
                try {

                    inputStream = myfile.getInputStream();
                    SaveInputStream = myfile.getInputStream();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                check = true;
        //                System.out.println("check in bean" + check);
                Imagepath = uploadFile(PhotoFile, "/tmp/pht/" + PhotoFile.getFilename());

            }
        }
        }
    
   
    private String uploadFile(UploadedFile file, String path) {

        UploadedFile myfile = file;

        try {

            FileOutputStream out = new FileOutputStream(path);
            byte[] buffer = new byte[8192];
            int bytesRead = 0;
            while ((bytesRead = inputStream.read(buffer, 0, 8192)) != -1) {
                out.write(buffer, 0, bytesRead);
            }
            out.flush();
            out.close();
        } catch (Exception ex) {
            // handle exception
            ex.printStackTrace();
        } finally{
            try {
                inputStream.close();
            } catch (IOException e) {
            }
        }


        //Returns the path where file is stored
        return path;
    }

    
    private String SaveuploadFile(UploadedFile file, String path) {
            ADFUtils.setLastUpdatedByNew("VendorMasterVO1Iterator","LastUpdatedBy");
        UploadedFile myfile = file;

        try {

            FileOutputStream out = new FileOutputStream(path);
            byte[] buffer = new byte[8192];
            int bytesRead = 0;
            while ((bytesRead = SaveInputStream.read(buffer, 0, 8192)) != -1) {
                out.write(buffer, 0, bytesRead);
            }
            out.flush();
            out.close();
        } catch (Exception ex) {
            // handle exception
            ex.printStackTrace();
        } finally{
            try {
                SaveInputStream.close();
            } catch (IOException e) {
            }
        }
        return path;
        }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getImagePath() {
        return imagePath;
    }
    
    String Imagepath = "";
    
    
    /*     public void saveVendormasterAL(ActionEvent actionEvent) 
    {
    System.out.println("Payment terms=>"+paytermsBinding.getValue()+"\n addrss=>"+addressBinding.getValue()+
                                                                          "\n State gst=>"+stateGstBinding.getValue());
           
            if(gstRegBinding.getValue() ==null || gstRegBinding.getValue().equals(""))
            {
                Msg="GST Regd. No";
            }
            if(panNumberBinding.getValue()==null || panNumberBinding.getValue().equals(""))
            {
                if(Msg !=null || Msg !="")
                {
                    Msg=",PAN No";
                }
                else
                {
                    Msg="PAN No";
                }
            }
       
    //        if(paytermsBinding.getValue()==null || paytermsBinding.getValue().equals(""))
    //        {
    //            System.out.println("**********Pay inside");
    //             if(Msg !=null || Msg !="")
    //            {
    //               Msg=",Payment Term";
    //            }
    //            else
    //            {
    //                Msg="Payment Term";
    //            }
    //        }
            if(addressBinding.getValue()==null || addressBinding.getValue().equals(""))
            {
                System.out.println("addressBinding**********Pay inside");
                if(Msg !=null || Msg !="")
                {
                    Msg=Msg+",Address";   
                }
                else
                {
                    Msg=Msg+"Address";   
                }
            }
            if(stateGstBinding.getValue()==null || stateGstBinding.getValue().equals(""))
            {
                System.out.println("stateGstBinding**********Pay inside");
                if(Msg !=null || Msg !="")
                {
                    Msg=Msg+",State GST Code";
                }
                else
                {
                    Msg=Msg+"State GST Code";
                }
            }
           
            Msg =Msg+" is required.";
           
    //        if(addressBinding.getValue()!=null && paytermsBinding.getValue()!=null && addressBinding.getValue()!=null && stateGstBinding.getValue()!=null && gstRegBinding.getValue() !=null && panNumberBinding.getValue()!=null)
            if(addressBinding.getValue()!=null && addressBinding.getValue()!=null && stateGstBinding.getValue()!=null && gstRegBinding.getValue() !=null && panNumberBinding.getValue()!=null)
            {
               
              DCIteratorBinding Dcite=ADFUtils.findIterator("VendUnitVO1Iterator");
              VendUnitVORowImpl row=(VendUnitVORowImpl) Dcite.getCurrentRow();
            
              if(row.getTransitPeriod()!=null)
              {
                 String panNo=(String)panNumberBinding.getValue();
                 int i=panNo.length();
                 if(i==10)
                  {
                    if(gstRegBinding.getValue().toString().substring(0, 2).equals(stateGstBinding.getValue().toString().substring(0, 2)))
                    {
                         System.out.println("========= PAN NUMBER IS  ======"+panNumberBinding.getValue());
                         OperationBinding op=ADFUtils.findOperation("genarateVendorCode");
                         op.execute();
                     
                         if(op.getResult()!=null && op.getResult().toString().equalsIgnoreCase("N"))
                         {
                            if (op.getErrors().isEmpty())
                            {
                                ADFUtils.findOperation("Commit").execute();
                                FacesMessage Message = new FacesMessage("Record Update Successfully.");
                                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                                FacesContext fc = FacesContext.getCurrentInstance();
                                fc.addMessage(null, Message);
                            }
                         }
                         else if (op.getResult() != null && op.getResult().toString() != "")
                         {
                           if (op.getErrors().isEmpty())
                           {
                                 System.out.println("CREATE MODE");
                                 ADFUtils.findOperation("Commit").execute();
                                 FacesMessage Message = new FacesMessage("Record Save Successfully.Vendor Code is "+op.getResult());
                                 Message.setSeverity(FacesMessage.SEVERITY_INFO);
                                 FacesContext fc = FacesContext.getCurrentInstance();
                                 fc.addMessage(null, Message);
                           }
                        } 
                       if(op.getResult()!=null || op.getResult().toString().equalsIgnoreCase("N"))
                       {
                                check = false;
                                String path;
                                if(getPhotoFile()!= null){
                                path = "/home/beta6/pics/" + PhotoFile.getFilename();
                                Imagepath = SaveuploadFile(PhotoFile, path);
                                //        System.out.println("path " + Imagepath);
                                OperationBinding ob = ADFUtils.findOperation("AddImagePath");
                                ob.getParamsMap().put("ImagePath", Imagepath);
                                ob.execute();   
                            }
                            File directory = new File("/tmp/pht");
                            //get all the files from a directory
                            File[] fList = directory.listFiles();
                            if(fList !=null){
                            for (File file : fList) {
                                //Delete all previously uploaded files
                               // if (!"NoImage.png".equalsIgnoreCase(file.getName())) {
                                    file.delete();
                                //}
                            }
                          }
                        }
                  }
                  else{
                      ADFUtils.showMessage("First Two Characters Of GST Regd. No. should be Equals First Two Characters Of State GST Code.",0);
                      }
               }
               else{
                      ADFUtils.showMessage("Pan No should be maximum 10 Digits.",0);
                   }
             }
             else{
                      ADFUtils.showMessage("Transit Period must be required in Vendor Unit table.",0);
                 }
          }
          else{
                if(Msg.startsWith(","))
                Msg=Msg.substring(1);
                System.out.println("Message is==>"+Msg);
                ADFUtils.showMessage(Msg,0);
                 Msg="";
            }
        
   } */
    public void setCheck(Boolean check) {
        this.check = check;
    }

    public Boolean getCheck() {
        return check;
    }

    public void setPhotoFile(UploadedFile PhotoFile) {
        this.PhotoFile = PhotoFile;
    }

    public UploadedFile getPhotoFile() {
        return PhotoFile;
    }


    public void downloadDocment(FacesContext facesContext, OutputStream outputStream) {
        //Read file from particular path, path bind is binding of table field that contains path
        if(filenNmPath.getValue()!=null){
              File filed = new File(filenNmPath.getValue().toString());
              FileInputStream fis;
              byte[] b;
              try {
                  fis = new FileInputStream(filed);

                  int n;
                  while ((n = fis.available()) > 0) {
                      b = new byte[n];
                      int result = fis.read(b);
                      outputStream.write(b, 0, b.length);
                      if (result == -1)
                          break;
                  }
              } catch (IOException e) {
                  e.printStackTrace();
              }
             
        }
    }

    public void setFilenNmPath(RichOutputText filenNmPath) {
        this.filenNmPath = filenNmPath;
    }

    public RichOutputText getFilenNmPath() {
        return filenNmPath;
    }

    public void addVendorCertiAL(ActionEvent actionEvent) {
        
        ADFUtils.findOperation("CreateInsert8").execute();
        ADFUtils.findOperation("setvendorCode").execute();
    }
    
    public void pageCreateInseart()
    {
           
            ADFUtils.findOperation("CreateInsert11").execute();
            ADFUtils.findOperation("CreateInsert12").execute();
            ADFUtils.findOperation("CreateInsert13").execute();
            
        }
    public void vendUnitDeletePopupDialogDL(DialogEvent dialogEvent)
    {
        if(dialogEvent.getOutcome().name().equals("ok"))
            {
            ADFUtils.findOperation("Delete").execute();
            System.out.println("Record Delete Successfully");
            
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");   
                Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                FacesContext fc = FacesContext.getCurrentInstance();   
                fc.addMessage(null, Message);   
            
            }

        AdfFacesContext.getCurrentInstance().addPartialTarget(vendUnitTableBinding);
    }

    public void setVendUnitTableBinding(RichTable vendUnitTableBinding) {
        this.vendUnitTableBinding = vendUnitTableBinding;
    }

    public RichTable getVendUnitTableBinding() {
        return vendUnitTableBinding;
    }



    public void vendorConttactDeletePopupDialogDL(DialogEvent dialogEvent) 
    {
        if(dialogEvent.getOutcome().name().equals("ok"))
            {
            ADFUtils.findOperation("Delete2").execute();
            System.out.println("Record Delete Successfully");
            
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");   
                Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                FacesContext fc = FacesContext.getCurrentInstance();   
                fc.addMessage(null, Message);   
            
            }

        AdfFacesContext.getCurrentInstance().addPartialTarget(vendorConttactTableBinding);
    }

    public void setVendorConttactTableBinding(RichTable vendorConttactTableBinding) {
        this.vendorConttactTableBinding = vendorConttactTableBinding;
    }

    public RichTable getVendorConttactTableBinding() {
        return vendorConttactTableBinding;
    }



    public void vendorSuppDeletePopupDialogDL(DialogEvent dialogEvent) 
    {
        if(dialogEvent.getOutcome().name().equals("ok"))
            {
            ADFUtils.findOperation("Delete6").execute();
            System.out.println("Record Delete Successfully");
            
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");   
                Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                FacesContext fc = FacesContext.getCurrentInstance();   
                fc.addMessage(null, Message);   
            
            }

        AdfFacesContext.getCurrentInstance().addPartialTarget(vendorSuppTableBinding);
    
    }

    public void setVendorSuppTableBinding(RichTable vendorSuppTableBinding) {
        this.vendorSuppTableBinding = vendorSuppTableBinding;
    }

    public RichTable getVendorSuppTableBinding() {
        return vendorSuppTableBinding;
    }
    
    
    
    

    public void vendorItemDeletePopupDialogDL(DialogEvent dialogEvent)
    {
        if(dialogEvent.getOutcome().name().equals("ok"))
            {
            ADFUtils.findOperation("Delete3").execute();
            System.out.println("Record Delete Successfully");
            
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");   
                Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                FacesContext fc = FacesContext.getCurrentInstance();   
                fc.addMessage(null, Message);   
             }
        AdfFacesContext.getCurrentInstance().addPartialTarget(vendorItemTableBinding);
    }

    public void setVendorItemTableBinding(RichTable vendorItemTableBinding) {
        this.vendorItemTableBinding = vendorItemTableBinding;
    }

    public RichTable getVendorItemTableBinding() {
        return vendorItemTableBinding;
    }




    public void vendorCertfDeletePopupDialogDL(DialogEvent dialogEvent) 
    {
        if(dialogEvent.getOutcome().name().equals("ok"))
            {
            ADFUtils.findOperation("Delete4").execute();
            System.out.println("Record Delete Successfully");
            
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");   
                Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                FacesContext fc = FacesContext.getCurrentInstance();   
                fc.addMessage(null, Message);   
             }
        AdfFacesContext.getCurrentInstance().addPartialTarget(vendorCertfTableBinding);
    }

    public void setVendorCertfTableBinding(RichTable vendorCertfTableBinding) {
        this.vendorCertfTableBinding = vendorCertfTableBinding;
    }

    public RichTable getVendorCertfTableBinding() {
        return vendorCertfTableBinding;
    }



    public void docAttachRefDeletePopupDialogDL(DialogEvent dialogEvent) 
    {
        if(dialogEvent.getOutcome().name().equals("ok"))
            {
            ADFUtils.findOperation("Delete7").execute();
            System.out.println("Record Delete Successfully");
            
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");   
                Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                FacesContext fc = FacesContext.getCurrentInstance();   
                fc.addMessage(null, Message);   
             }
        AdfFacesContext.getCurrentInstance().addPartialTarget(docAttachRefTableBinding);

    }

    public void setDocAttachRefTableBinding(RichTable docAttachRefTableBinding) {
        this.docAttachRefTableBinding = docAttachRefTableBinding;
    }

    public RichTable getDocAttachRefTableBinding() {
        return docAttachRefTableBinding;
    }

    public void setStateGstBinding(RichInputText stateGstBinding) {
        this.stateGstBinding = stateGstBinding;
    }

    public RichInputText getStateGstBinding() {
        return stateGstBinding;
    }



    public void gstRegNoValidator(FacesContext facesContext, UIComponent uIComponent, Object object) 
    {
        String venFlag = (String) (regFlagBinding.getValue() != null ? regFlagBinding.getValue() : "R");
        
        if(object!=null && object.toString().trim().length()>=2 && stateGstBinding.getValue()!=null)
        {
            if(venFlag.equalsIgnoreCase("R"))
            {
                System.out.println("----- GST REGD No-----When the Vendor is Registered====>>"+venFlag);
                if(!object.toString().substring(0, 2).equals(stateGstBinding.getValue().toString().substring(0, 2)))
                {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "First Two Characters Of GST Regd. No. Is Equals First Two Characters Of State GST Code.", null));
                }
            }
        }

    }
    
    public void stateGstCodeValidator(FacesContext facesContext, UIComponent uIComponent, Object object) 
    {
        String regFg = (String) (regFlagBinding.getValue() != null ? regFlagBinding.getValue() : "R");
        System.out.println("regFlag-------"+regFg);
        
        if(object!=null && gstRegBinding.getValue()!=null && stateGstBinding.getValue()!=null)
        {
            if(regFg.equalsIgnoreCase("R"))
            {
                System.out.println("----- State-----When the Vendor is Registered====>>"+regFg);
                if(!stateGstBinding.getValue().toString().substring(0, 2).equals(gstRegBinding.getValue().toString().substring(0, 2)) &&
                stateGstBinding.getValue().toString().trim().length()>=2)
                {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "First Two Characters Of GST Regd. No. Is Equals First Two Characters Of State GST Code.", null));
                } 
            }

        }
    }    
    

    public void setGstRegBinding(RichInputText gstRegBinding) {
        this.gstRegBinding = gstRegBinding;
    }

    public RichInputText getGstRegBinding() {
        return gstRegBinding;
    }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
            try {
                Method method1 =
                    component.getClass().getMethod("setDisabled", boolean.class);
                if (method1 != null) {
                    method1.invoke(component, valueComponent);
                }
            } catch (NoSuchMethodException e) {
                try {
                    Method method =
                        component.getClass().getMethod("setReadOnly", boolean.class);
                    if (method != null) {
                        method.invoke(component, valueComponent);
                    }
                } catch (Exception e1) {
                    // e.printStackTrace();//silently eat this exception.
                }


            } catch (Exception e) {
                // e.printStackTrace();//silently eat this exception.
            }
            List<UIComponent> childComponents = component.getChildren();
            for (UIComponent comp : childComponents) {
                makeComponentHierarchyReadOnly(comp, valueComponent);
            }
        }
    
//    private UIComponent getUIComponent(String id){
//        FacesContext facesCtx = FacesContext.getCurrentInstance();
//        return findComponent(facesCtx.getViewRoot(), id);
//    }
    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;  
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }
    public void EditButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }
    
    private void cevmodecheck(){
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }
    public void cevModeDisableComponent(String mode) {
//            FacesContext fctx = FacesContext.getCurrentInstance();
//            ELContext elctx = fctx.getELContext();
//            Application jsfApp = fctx.getApplication();
//            //create a ValueExpression that points to the ADF binding layer
//            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
//            //
//            ValueExpression valueExpr = exprFactory.createValueExpression(
//                                         elctx,
//                                         "#{pageFlowScope.mode=='E'}",
//                                          Object.class
//                                         ); 
//            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
        if (mode.equals("E")) {
            
            getAddressStateBinding().setDisabled(true);
            getCountryBinding().setDisabled(true);
            getUntCodeBinding().setDisabled(true);
            getCityCodeBinding().setDisabled(true);
            getPinCodeBinding().setDisabled(true);
            getStateCodeBinding().setDisabled(true);
            getVendorNameBinding().setDisabled(false);
            getStateGstBinding().setDisabled(true);
            getEtdsBinding().setDisabled(true);
            getVendorcodeBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            getDetailcreateBinding().setDisabled(false);
            //getDetaildeleteBinding().setDisabled(false);
            itemCdDtlBinding.setDisabled(true);   
                getContactCreateBinding().setDisabled(false);
                //getContactDeleteBinding().setDisabled(false);
                getReferenceCreateBinding().setDisabled(false);
                //getRefrenceDeleteBinding().setDisabled(false);
                getItemMappingCreateBinding().setDisabled(false);
                //getItemMappingDeleteBinding().setDisabled(false);
                getCertificateCreateBinding().setDisabled(false);
               // getCertificateDeleteBinding().setDisabled(false);
                getDocCreateBinding().setDisabled(false);
              // getDocDeleteBinding().setDisabled(false);
                
                
                
        } else if (mode.equals("C")) {
            
            getAddressStateBinding().setDisabled(true);
            getCountryBinding().setDisabled(true);
            getCityCodeBinding().setDisabled(true);
            getPinCodeBinding().setDisabled(true);
            getStateCodeBinding().setDisabled(true);
            getStateGstBinding().setDisabled(true);
            getUntCodeBinding().setDisabled(true);
            getVendorcodeBinding().setDisabled(true);
            getDetailcreateBinding().setDisabled(false);
           // getDetaildeleteBinding().setDisabled(false);
           
           vendorItemDtlButtonBinding.setDisabled(true);
            getContactCreateBinding().setDisabled(false);
            //getContactDeleteBinding().setDisabled(false);
            getReferenceCreateBinding().setDisabled(false);
           // getRefrenceDeleteBinding().setDisabled(false);
            getItemMappingCreateBinding().setDisabled(false);
            //getItemMappingDeleteBinding().setDisabled(false);
            getCertificateCreateBinding().setDisabled(false);
            //getCertificateDeleteBinding().setDisabled(false);
            getDocCreateBinding().setDisabled(false);
            itemCdDtlBinding.setDisabled(true);
           // getDocDeleteBinding().setDisabled(false);
            
            
            
            
        } else if (mode.equals("V")) {
            getCommercialTabBinding().setDisabled(false);
            getGeneralTabBinding().setDisabled(false);
            getAddressTabBinding().setDisabled(false);
            getContactTabBinding().setDisabled(false);
            getReferenceTabBinding().setDisabled(false);
            getItemMappingTabBinding().setDisabled(false);
            getOtherTabBinding().setDisabled(false);
            getCertificateTabBinding().setDisabled(false);
            getDetailcreateBinding().setDisabled(true);
            //getDetaildeleteBinding().setDisabled(true);
            
            getContactCreateBinding().setDisabled(true);
//            getContactDeleteBinding().setDisabled(true);
            getReferenceCreateBinding().setDisabled(true);
           // getRefrenceDeleteBinding().setDisabled(true);
            getItemMappingCreateBinding().setDisabled(true);
            //getItemMappingDeleteBinding().setDisabled(true);
            getCertificateCreateBinding().setDisabled(true);
            //getCertificateDeleteBinding().setDisabled(true);
            getDocCreateBinding().setDisabled(true);
            //getDocDeleteBinding().setDisabled(true);
            getCertificateChildTabBinding().setDisabled(false);
            getAttachmentChildTabBinding().setDisabled(false);
            vendorItemDtlButtonBinding.setDisabled(false);
            
            
            
        }
    }

    public void setEtdsBinding(RichSelectOneChoice etdsBinding) {
        this.etdsBinding = etdsBinding;
    }

    public RichSelectOneChoice getEtdsBinding() {
        return etdsBinding;
    }

    public void setDetailcreateBinding(RichButton detailcreateBinding) {
        this.detailcreateBinding = detailcreateBinding;
    }

    public RichButton getDetailcreateBinding() {
        return detailcreateBinding;
    }

    public void setDetaildeleteBinding(RichButton detaildeleteBinding) {
        this.detaildeleteBinding = detaildeleteBinding;
    }

    public RichButton getDetaildeleteBinding() {
        return detaildeleteBinding;
    }

    public void setVendorcodeBinding(RichInputText vendorcodeBinding) {
        this.vendorcodeBinding = vendorcodeBinding;
    }

    public RichInputText getVendorcodeBinding() {
        return vendorcodeBinding;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setCommercialTabBinding(RichShowDetailItem commercialTabBinding) {
        this.commercialTabBinding = commercialTabBinding;
    }

    public RichShowDetailItem getCommercialTabBinding() {
        return commercialTabBinding;
    }

    public void setGeneralTabBinding(RichShowDetailItem generalTabBinding) {
        this.generalTabBinding = generalTabBinding;
    }

    public RichShowDetailItem getGeneralTabBinding() {
        return generalTabBinding;
    }

    public void setAddressTabBinding(RichShowDetailItem addressTabBinding) {
        this.addressTabBinding = addressTabBinding;
    }

    public RichShowDetailItem getAddressTabBinding() {
        return addressTabBinding;
    }

    public void setContactTabBinding(RichShowDetailItem contactTabBinding) {
        this.contactTabBinding = contactTabBinding;
    }

    public RichShowDetailItem getContactTabBinding() {
        return contactTabBinding;
    }

    public void setReferenceTabBinding(RichShowDetailItem referenceTabBinding) {
        this.referenceTabBinding = referenceTabBinding;
    }

    public RichShowDetailItem getReferenceTabBinding() {
        return referenceTabBinding;
    }

    public void setItemMappingTabBinding(RichShowDetailItem itemMappingTabBinding) {
        this.itemMappingTabBinding = itemMappingTabBinding;
    }

    public RichShowDetailItem getItemMappingTabBinding() {
        return itemMappingTabBinding;
    }

    public void setOtherTabBinding(RichShowDetailItem otherTabBinding) {
        this.otherTabBinding = otherTabBinding;
    }

    public RichShowDetailItem getOtherTabBinding() {
        return otherTabBinding;
    }

    public void setCertificateTabBinding(RichShowDetailItem certificateTabBinding) {
        this.certificateTabBinding = certificateTabBinding;
    }

    public RichShowDetailItem getCertificateTabBinding() {
        return certificateTabBinding;
    }

    public void setVendorNameBinding(RichInputText vendorNameBinding) {
        this.vendorNameBinding = vendorNameBinding;
    }

    public RichInputText getVendorNameBinding() {
        return vendorNameBinding;
    }

    public void setUntCodeBinding(RichInputComboboxListOfValues untCodeBinding) {
        this.untCodeBinding = untCodeBinding;
    }

    public RichInputComboboxListOfValues getUntCodeBinding() {
        return untCodeBinding;
    }

    public void setAddressStateBinding(RichInputText addressStateBinding) {
        this.addressStateBinding = addressStateBinding;
    }

    public RichInputText getAddressStateBinding() {
        return addressStateBinding;
    }

    public void setCountryBinding(RichInputText countryBinding) {
        this.countryBinding = countryBinding;
    }

    public RichInputText getCountryBinding() {
        return countryBinding;
    }

    public void setCityCodeBinding(RichInputText cityCodeBinding) {
        this.cityCodeBinding = cityCodeBinding;
    }

    public RichInputText getCityCodeBinding() {
        return cityCodeBinding;
    }

    public void setPinCodeBinding(RichInputText pinCodeBinding) {
        this.pinCodeBinding = pinCodeBinding;
    }

    public RichInputText getPinCodeBinding() {
        return pinCodeBinding;
    }

    public void setStateCodeBinding(RichInputText stateCodeBinding) {
        this.stateCodeBinding = stateCodeBinding;
    }

    public RichInputText getStateCodeBinding() {
        return stateCodeBinding;
    }

    public void setContactCreateBinding(RichButton contactCreateBinding) {
        this.contactCreateBinding = contactCreateBinding;
    }

    public RichButton getContactCreateBinding() {
        return contactCreateBinding;
    }

//    public void setContactDeleteBinding(RichButton contactDeleteBinding) {
//        this.contactDeleteBinding = contactDeleteBinding;
//    }
//
//    public RichButton getContactDeleteBinding() {
//        return contactDeleteBinding;
//    }

    public void setReferenceCreateBinding(RichButton referenceCreateBinding) {
        this.referenceCreateBinding = referenceCreateBinding;
    }

    public RichButton getReferenceCreateBinding() {
        return referenceCreateBinding;
    }

    public void setRefrenceDeleteBinding(RichButton refrenceDeleteBinding) {
        this.refrenceDeleteBinding = refrenceDeleteBinding;
    }

    public RichButton getRefrenceDeleteBinding() {
        return refrenceDeleteBinding;
    }

    public void setItemMappingCreateBinding(RichButton itemMappingCreateBinding) {
        this.itemMappingCreateBinding = itemMappingCreateBinding;
    }

    public RichButton getItemMappingCreateBinding() {
        return itemMappingCreateBinding;
    }

    public void setItemMappingDeleteBinding(RichButton itemMappingDeleteBinding) {
        this.itemMappingDeleteBinding = itemMappingDeleteBinding;
    }

    public RichButton getItemMappingDeleteBinding() {
        return itemMappingDeleteBinding;
    }

    public void setCertificateCreateBinding(RichButton certificateCreateBinding) {
        this.certificateCreateBinding = certificateCreateBinding;
    }

    public RichButton getCertificateCreateBinding() {
        return certificateCreateBinding;
    }

    public void setCertificateDeleteBinding(RichButton certificateDeleteBinding) {
        this.certificateDeleteBinding = certificateDeleteBinding;
    }

    public RichButton getCertificateDeleteBinding() {
        return certificateDeleteBinding;
    }

    public void setDocCreateBinding(RichButton docCreateBinding) {
        this.docCreateBinding = docCreateBinding;
    }

    public RichButton getDocCreateBinding() {
        return docCreateBinding;
    }

    public void setDocDeleteBinding(RichButton docDeleteBinding) {
        this.docDeleteBinding = docDeleteBinding;
    }

    public RichButton getDocDeleteBinding() {
        return docDeleteBinding;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setPaytermsBinding(RichSelectOneChoice paytermsBinding) {
        this.paytermsBinding = paytermsBinding;
    }

    public RichSelectOneChoice getPaytermsBinding() {
        return paytermsBinding;
    }

    public void setAddressBinding(RichInputText addressBinding) {
        this.addressBinding = addressBinding;
    }

    public RichInputText getAddressBinding() {
        return addressBinding;
    }
    
    /*     public String saveAndCloseButton() 
    {
    if(gstRegBinding.getValue() ==null || gstRegBinding.getValue().equals(""))
    {
        Msg="GST Regd. No";
    }
    if(panNumberBinding.getValue()==null || panNumberBinding.getValue().equals(""))
    {
        if(Msg !=null || Msg !="")
        {
        Msg=",PAN No";
        }
        else
        {
            Msg="PAN No";
        }
    }
    //        if(paytermsBinding.getValue()==null || paytermsBinding.getValue().equals(""))
    //        {
    //               Msg="Payment Term";
    //        }
    if(addressBinding.getValue()==null || addressBinding.getValue().equals(""))
    {
        if(Msg !=null)
        {
           Msg=Msg+",Address";   
        }
        else
        {
            Msg=Msg+"Address";   
        }
    }
    if(stateGstBinding.getValue()==null || stateGstBinding.getValue().equals(""))
    {
        if(Msg !=null)
        {
          Msg=Msg+",State GST Code";
        }
        else
        {
            Msg=Msg+"State GST Code";
        }
    }
    Msg =Msg+" is required.";
    
    //    if(addressBinding.getValue()!=null && paytermsBinding.getValue()!=null && addressBinding.getValue()!=null && stateGstBinding.getValue()!=null && gstRegBinding.getValue() !=null && panNumberBinding.getValue()!=null)
    if(addressBinding.getValue()!=null && addressBinding.getValue()!=null && stateGstBinding.getValue()!=null && gstRegBinding.getValue() !=null && panNumberBinding.getValue()!=null)
    {
    DCIteratorBinding Dcite=ADFUtils.findIterator("VendUnitVO1Iterator");
    VendUnitVORowImpl row=(VendUnitVORowImpl) Dcite.getCurrentRow();
    
    if(row.getTransitPeriod()!=null)
    {
      String panNo=(String)panNumberBinding.getValue();
      int i=panNo.length();
      if(i==10)
      {
         if(gstRegBinding.getValue().toString().substring(0, 2).equals(stateGstBinding.getValue().toString().substring(0, 2)))
         {
                System.out.println("========= PAN NUMBER IS  ======"+panNumberBinding.getValue());
            
                OperationBinding op=ADFUtils.findOperation("genarateVendorCode");
                op.execute();
                if(op.getResult()!=null || op.getResult().toString().equalsIgnoreCase("N"))
                {
                        check = false;
                        String path;
                        if(getPhotoFile()!= null){
                        path = "/home/beta6/pics/" + PhotoFile.getFilename();
                        Imagepath = SaveuploadFile(PhotoFile, path);
                        //        System.out.println("path " + Imagepath);
                        OperationBinding ob = ADFUtils.findOperation("AddImagePath");
                        ob.getParamsMap().put("ImagePath", Imagepath);
                        ob.execute();   
                        }
                    File directory = new File("/tmp/pht");
                    //get all the files from a directory
                    File[] fList = directory.listFiles();
                    if(fList !=null){
                    for (File file : fList) {
                    //Delete all previously uploaded files
                    // if (!"NoImage.png".equalsIgnoreCase(file.getName())) {
                    file.delete();
                    //}
                    }
                  }
              }
               
            if(op.getResult()!=null && op.getResult().toString().equalsIgnoreCase("N"))
            {
                    if (op.getErrors().isEmpty())
                    {
                        ADFUtils.findOperation("Commit").execute();
                        FacesMessage Message = new FacesMessage("Record Update Successfully.");
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);
                        FacesContext fc = FacesContext.getCurrentInstance();
                        fc.addMessage(null, Message);
                        return "SaveAndClose";
                    }
            }
            else if (op.getResult() != null && op.getResult().toString() != "")
            {
                     if (op.getErrors().isEmpty())
                     {
                         System.out.println("CREATE MODE");
                        
                         ADFUtils.findOperation("Commit").execute();
                         FacesMessage Message = new FacesMessage("Record Save Successfully.Vendor Code is "+op.getResult());
                         Message.setSeverity(FacesMessage.SEVERITY_INFO);
                         FacesContext fc = FacesContext.getCurrentInstance();
                         fc.addMessage(null, Message);
                         return "SaveAndClose";
                     }
            } 
        }
        else{
              ADFUtils.showMessage("First Two Characters Of GST Regd. No. should be Equals First Two Characters Of State GST Code.",0);
              return null;
            }
    }else{
          ADFUtils.showMessage("Pan No should be maximum 10 Digits.",0);
          return null;
         }
    }else{
          ADFUtils.showMessage("Transit Period must be required in Vendor Unit table.",0);
          return null;
        }
    }
    else{
    if(Msg.startsWith(","))
    Msg=Msg.substring(1);
     System.out.println("Message is==>"+Msg);
    ADFUtils.showMessage(Msg,0);
        Msg="";
    }
    return null;
 } */

    public void setPanNumberBinding(RichInputText panNumberBinding) {
        this.panNumberBinding = panNumberBinding;
    }

    public RichInputText getPanNumberBinding() {
        return panNumberBinding;
    }

    public void setCertificateChildTabBinding(RichShowDetailItem certificateChildTabBinding) {
        this.certificateChildTabBinding = certificateChildTabBinding;
    }

    public RichShowDetailItem getCertificateChildTabBinding() {
        return certificateChildTabBinding;
    }

    public void setAttachmentChildTabBinding(RichShowDetailItem attachmentChildTabBinding) {
        this.attachmentChildTabBinding = attachmentChildTabBinding;
    }

    public RichShowDetailItem getAttachmentChildTabBinding() {
        return attachmentChildTabBinding;
    }

    public void setTransitPeriodBinding(RichInputText transitPeriodBinding) {
        this.transitPeriodBinding = transitPeriodBinding;
    }

    public RichInputText getTransitPeriodBinding() {
        return transitPeriodBinding;
    }

    public void setAddress2Binding(RichInputText address2Binding) {
        this.address2Binding = address2Binding;
    }

    public RichInputText getAddress2Binding() {
        return address2Binding;
    }

    public void setAddress3Binding(RichInputText address3Binding) {
        this.address3Binding = address3Binding;
    }

    public RichInputText getAddress3Binding() {
        return address3Binding;
    }

    public void address2VCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce!=null){
            if(address2Binding.getValue()!=null)
             {
                if(address2Binding.getValue().equals(addressBinding.getValue())){
                    FacesMessage message = new FacesMessage("Address can not be same as the above addresses.");
                    message.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext context = FacesContext.getCurrentInstance();
                    context.addMessage(address2Binding.getClientId(), message);
                    
                }
                if(address2Binding.getValue().equals(address3Binding.getValue()))
                {
                    FacesMessage message = new FacesMessage("Address can not be same as the below addresses.");
                    message.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext context = FacesContext.getCurrentInstance();
                    context.addMessage(address2Binding.getClientId(), message);
                }
             }
            }
        }

    public void address1VCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce!=null){
            if(addressBinding.getValue()!=null)
             {
                if(addressBinding.getValue().equals(address2Binding.getValue())){
                    FacesMessage message = new FacesMessage("Address can not be same as the below addresses.");
                    message.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext context = FacesContext.getCurrentInstance();
                    context.addMessage(addressBinding.getClientId(), message);
                    
                }
                if(addressBinding.getValue().equals(address3Binding.getValue()))
                {
                    FacesMessage message = new FacesMessage("Address can not be same as the below addresses.");
                    message.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext context = FacesContext.getCurrentInstance();
                    context.addMessage(addressBinding.getClientId(), message);
                }
             }
            }
    }

    public void address3VCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce!=null){
            if(address3Binding.getValue()!=null)
             {
                if(address3Binding.getValue().equals(addressBinding.getValue())){
                    FacesMessage message = new FacesMessage("Address can not be same as the above addresses.");
                    message.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext context = FacesContext.getCurrentInstance();
                    context.addMessage(address3Binding.getClientId(), message);
                    
                }
                if(address3Binding.getValue().equals(address2Binding.getValue()))
                {
                    FacesMessage message = new FacesMessage("Address can not be same as the above addresses.");
                    message.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext context = FacesContext.getCurrentInstance();
                    context.addMessage(address3Binding.getClientId(), message);
                }
             }
       }
    }

    public void deleteVendorItemDtlDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
            {
            ADFUtils.findOperation("Delete5").execute();
            System.out.println("Record Delete Successfully");
            
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");   
                Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                FacesContext fc = FacesContext.getCurrentInstance();   
                fc.addMessage(null, Message);   
             }
        AdfFacesContext.getCurrentInstance().addPartialTarget(vednorItemDtlTableBinding);
    }

    public void setVednorItemDtlTableBinding(RichTable vednorItemDtlTableBinding) {
        this.vednorItemDtlTableBinding = vednorItemDtlTableBinding;
    }

    public RichTable getVednorItemDtlTableBinding() {
        return vednorItemDtlTableBinding;
    }

    public void setVendorItemDtlButtonBinding(RichButton vendorItemDtlButtonBinding) {
        this.vendorItemDtlButtonBinding = vendorItemDtlButtonBinding;
    }

    public RichButton getVendorItemDtlButtonBinding() {
        return vendorItemDtlButtonBinding;
    }

    public void setItemCdDtlBinding(RichInputText itemCdDtlBinding) {
        this.itemCdDtlBinding = itemCdDtlBinding;
    }

    public RichInputText getItemCdDtlBinding() {
        return itemCdDtlBinding;
    }

    public void setManufactureBinding(RichInputComboboxListOfValues manufactureBinding) {
        this.manufactureBinding = manufactureBinding;
    }

    public RichInputComboboxListOfValues getManufactureBinding() {
        return manufactureBinding;
    }

    public void createItemDtlAL(ActionEvent actionEvent) {

        ADFUtils.findOperation("CreateInsert9").execute();
        if(vendorcodeBinding.getValue()!=null)
        {
            System.out.println("VEN DOR CODE "+vendorcodeBinding.getValue());
        Row row=(Row)ADFUtils.evaluateEL("#{bindings.VendorItemDtlVO1Iterator.currentRow}");
        row.setAttribute("VenCd", (vendorcodeBinding.getValue()));
        }
    }

    public void setRegFlagBinding(RichSelectOneChoice regFlagBinding) {
        this.regFlagBinding = regFlagBinding;
    }

    public RichSelectOneChoice getRegFlagBinding() {
        return regFlagBinding;
    }

    public void setCityBinding(RichInputComboboxListOfValues cityBinding) {
        this.cityBinding = cityBinding;
    }

    public RichInputComboboxListOfValues getCityBinding() {
        return cityBinding;
    }

    public void cityCodeVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        String venFlag = (String) vendorFlagBinding.getValue();  //&& venFlag.equalsIgnoreCase("I")
        if(vce!=null){
            String regFg = (String) (regFlagBinding.getValue() != null ? regFlagBinding.getValue() : "R");
          //  System.out.println("regFlag-------"+regFg);
            if(gstRegBinding.getValue()!=null && stateGstBinding.getValue()!=null && venFlag.equalsIgnoreCase("I"))
            {
                if(regFg.equalsIgnoreCase("R"))
                {
                   // System.out.println("----- State-----When the Vendor is Registered====>>"+regFg);
                    if(!stateGstBinding.getValue().toString().substring(0, 2).equals(gstRegBinding.getValue().toString().substring(0, 2)) &&
                    stateGstBinding.getValue().toString().trim().length()>=2)
                    {
                        FacesMessage message = new FacesMessage("First Two Characters Of GST Regd. No. Is Equals First Two Characters Of State GST Code.");
                        message.setSeverity(FacesMessage.SEVERITY_ERROR);
                        FacesContext context = FacesContext.getCurrentInstance();
                        context.addMessage(cityBinding.getClientId(), message);
                   // throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "First Two Characters Of GST Regd. No. Is Equals First Two Characters Of State GST Code.", null));
                    } 
                }
            }
        }
    }
 // New Method================
    public String requiredAttributes() 
    {
        DCIteratorBinding dci = ADFUtils.findIterator("VendUnitVO1Iterator");
        
//        if(panNumberBinding.getValue() == null && ADFUtils.resolveExpression("#{bindings.RegFlag.inputValue}").equals("R")){
//        ADFUtils.showMessage("PAN No. is required in General tab.", 0);
//        return "N";
//        }
//        else if(gstRegBinding.getValue() == null && ADFUtils.resolveExpression("#{bindings.RegFlag.inputValue}").equals("R")){
//            ADFUtils.showMessage("Gst Reg No. is required.", 0);
//            return "N";
//        }
        if(paytermsBinding.getValue() == null){
            ADFUtils.showMessage("Payment Term is required.", 0);
            return "N";
        }
        else if(addressBinding.getValue() == null){
            ADFUtils.showMessage("Address is required.", 0);
            return "N";
        }
        else if( dci.getCurrentRow().getAttribute("TransitPeriod") == null){
            ADFUtils.showMessage("Transit Period is required in General tab.", 0);
            return "N";
        }
        else if(cityBinding.getValue() == null){
            ADFUtils.showMessage("City is required in Address tab.", 0);
            return "N";
        }
        if(paytermsBinding.getValue() != null &&
        dci.getCurrentRow().getAttribute("TransitPeriod") != null && addressBinding.getValue() != null &&
        cityBinding.getValue() != null) 
        {
            return "Y";
        }
        return "N";
    }

    public void saveVendorMasterDataAL(ActionEvent actionEvent) 
    {
        ADFUtils.setLastUpdatedByNew("VendorMasterVO1Iterator","LastUpdatedBy");
        if (requiredAttributes().equals("Y")) 
        {
            DCIteratorBinding Dcite = ADFUtils.findIterator("VendorMasterVO1Iterator");
            DCIteratorBinding Dcite1 = ADFUtils.findIterator("VendorRegdAddressVO1Iterator");
            VendorMasterVORowImpl row = (VendorMasterVORowImpl) Dcite.getCurrentRow();
            VendorRegdAddressVORowImpl row1 = (VendorRegdAddressVORowImpl) Dcite1.getCurrentRow();
            String RegFLag = (String) (regFlagBinding.getValue() != null ? regFlagBinding.getValue() : "R");
            String venFlag = (String) vendorFlagBinding.getValue();  //&& venFlag.equalsIgnoreCase("I")

            if(RegFLag.equalsIgnoreCase("R") && venFlag.equalsIgnoreCase("I"))
            {
//                String panNo=(String)panNumberBinding.getValue();
//                int i=panNo.length();
//                if(i==10)
//                 {
//                if (gstRegBinding.getValue() != null && row1.getCity() != null) // && row1.getCity() != null
//                {
//                if(row1.getStateGstCode()!=null)
//                {
//                    if (gstRegBinding.getValue().toString().substring(0, 2).equals(stateGstBinding.getValue().toString().substring(0, 2))) 
//                    {
                        System.out.println("In save method");
                        OperationBinding op=ADFUtils.findOperation("genarateVendorCode");
                        op.execute();
                        
                        System.out.println("Get Result is====> " + op.getResult());
                        if(op.getResult()!=null && op.getResult().toString().equalsIgnoreCase("N"))
                        {
                            if (op.getErrors().isEmpty()) 
                            {
                                ADFUtils.findOperation("Commit").execute();
                                FacesMessage Message = new FacesMessage("Record Update Successfully.");
                                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                                FacesContext fc = FacesContext.getCurrentInstance();
                                fc.addMessage(null, Message);
                            }
                        }
                        else if (op.getResult() != null && op.getResult().toString() != "") 
                        {
                            if (op.getErrors().isEmpty()) 
                            {
                                System.out.println("CREATE MODE");
                                ADFUtils.findOperation("Commit").execute();
                                FacesMessage Message = new FacesMessage("Record Save Successfully.Vendor Code is "+op.getResult());
                                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                                FacesContext fc = FacesContext.getCurrentInstance();
                                fc.addMessage(null, Message);
                            }
                        }  
                        if(op.getResult()!=null || op.getResult().toString().equalsIgnoreCase("N"))
                        {
                            try {
                                check = false;
                                String path;
                                if(getPhotoFile()!= null){
                                path = "/home/beta17/pics/" + PhotoFile.getFilename();
                                Imagepath = SaveuploadFile(PhotoFile, path);
                                //        System.out.println("path " + Imagepath);
                                OperationBinding ob = ADFUtils.findOperation("AddImagePath");
                                ob.getParamsMap().put("ImagePath", Imagepath);
                                ob.execute();    
                                }
                                File directory = new File("/tmp/pht");
                                //get all the files from a directory
                                File[] fList = directory.listFiles();
                                if(fList !=null){
                                for (File file : fList) {
                                //Delete all previously uploaded files
                                // if (!"NoImage.png".equalsIgnoreCase(file.getName())) {
                                file.delete();
                                //}
                                }
                                }
                            } catch (Exception ee) {
                                System.out.println("Error in Path and Directory");
                            }
                        }
//                } else {
//                    ADFUtils.showMessage("First Two Characters Of GST Regd. No. Is Equals First Two Characters Of State GST Code",0);
//                }
//            } 
//            else{
//                 ADFUtils.showMessage("State GST Code not found when city is selected.", 0);
//                }
//        }else {
//                ADFUtils.showMessage("GST Regd.No. in General tab is required.", 0);
//            }
//            }else{
//                ADFUtils.showMessage("Pan No. should be maximum 10 Digits.",0);
//            }
        }
        else 
        {
                System.out.println("=====the vendor is SS");
            OperationBinding op=ADFUtils.findOperation("genarateVendorCode");
            op.execute();
            
            System.out.println("Get Result is====> " + op.getResult());
            if(op.getResult()!=null && op.getResult().toString().equalsIgnoreCase("N"))
            {
                if (op.getErrors().isEmpty()) 
                {
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message = new FacesMessage("Record Update Successfully.");
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);
                }
            }
            else if (op.getResult() != null && op.getResult().toString() != "") 
            {
                if (op.getErrors().isEmpty()) 
                {
                    System.out.println("CREATE MODE");
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message = new FacesMessage("Record Save Successfully.Vendor Code is "+op.getResult());
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);
                }
            }  
            if(op.getResult()!=null || op.getResult().toString().equalsIgnoreCase("N"))
            {
                try {
                    check = false;
                    String path;
                    if(getPhotoFile()!= null){
                    path = "/home/beta17/pics/" + PhotoFile.getFilename();
                    Imagepath = SaveuploadFile(PhotoFile, path);
                    //        System.out.println("path " + Imagepath);
                    OperationBinding ob = ADFUtils.findOperation("AddImagePath");
                    ob.getParamsMap().put("ImagePath", Imagepath);
                    ob.execute();    
                    }
                    File directory = new File("/tmp/pht");
                    //get all the files from a directory
                    File[] fList = directory.listFiles();
                    if(fList !=null){
                    for (File file : fList) {
                    //Delete all previously uploaded files
                    // if (!"NoImage.png".equalsIgnoreCase(file.getName())) {
                    file.delete();
                    //}
                    }
                    }
                } catch (Exception ee) {
                    System.out.println("Error in Path and Directory");
                }
            }
         }          //else close
            
      }             // required Method close
        
    }

    public void gstRegdNoVCE(ValueChangeEvent vce) 
    {
            vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
            if(vce.getNewValue()!=null)
            {
                  String regFg = (String) (regFlagBinding.getValue() != null ? regFlagBinding.getValue() : "R");
                  if(gstRegBinding.getValue()!=null && stateGstBinding.getValue()!=null)
                  {
                    if(regFg.equalsIgnoreCase("R"))
                    {
                       // System.out.println("----- State-----When the Vendor is Registered====>>"+regFg);
                        if(!stateGstBinding.getValue().toString().substring(0, 2).equals(gstRegBinding.getValue().toString().substring(0, 2)) &&
                        gstRegBinding.getValue().toString().trim().length()>=2){
                        FacesMessage message = new FacesMessage("First Two Characters Of GST Regd.No. Is Equals First Two Characters Of State GST Code.");
                        message.setSeverity(FacesMessage.SEVERITY_ERROR);
                        FacesContext context = FacesContext.getCurrentInstance();
                        context.addMessage(gstRegBinding.getClientId(), message);
                        } 
                    }
                  }
                  
                  
                if(vce.getNewValue()!=null && vce.getNewValue().toString().length()>=12)
                {  
                     // System.out.println("----- State-----When the Vendor is Registered====>>"+regFg);
                     String gstno=(String)vce.getNewValue().toString().subSequence(2, 12);
                     System.out.println("gstno----->"+gstno);
                     if(gstno!=null){
                         System.out.println("gstno 2 ----->"+gstno);
                     getPanNumberBinding().setValue(gstno);
                     }
                      } 
                  
                }
              
        }
    
 

    public String saveAndCloseDataAL() {
        ADFUtils.setLastUpdatedByNew("VendorMasterVO1Iterator","LastUpdatedBy");
        if (requiredAttributes().equals("Y")) 
        {
            DCIteratorBinding Dcite = ADFUtils.findIterator("VendorMasterVO1Iterator");
            DCIteratorBinding Dcite1 = ADFUtils.findIterator("VendorRegdAddressVO1Iterator");
            VendorMasterVORowImpl row = (VendorMasterVORowImpl) Dcite.getCurrentRow();
            VendorRegdAddressVORowImpl row1 = (VendorRegdAddressVORowImpl) Dcite1.getCurrentRow();
            String RegFLag = (String) (regFlagBinding.getValue() != null ? regFlagBinding.getValue() : "R");
            String venFlag = (String) vendorFlagBinding.getValue();

            if(RegFLag.equalsIgnoreCase("R") && venFlag.equalsIgnoreCase("I"))
            {
//                String panNo=(String)panNumberBinding.getValue();
//                int i=panNo.length();
//                if(i==10)
//                 {
//                if (gstRegBinding.getValue() != null && row1.getCity() != null) // && row1.getCity() != null
//                {
//                if(row1.getStateGstCode()!=null)
//                {
//                    if (gstRegBinding.getValue().toString().substring(0, 2).equals(stateGstBinding.getValue().toString().substring(0, 2))) 
//                    {
                        System.out.println("In save method");
                        OperationBinding op=ADFUtils.findOperation("genarateVendorCode");
                        op.execute();
                        
                        System.out.println("Get Result is====> " + op.getResult());
                        if(op.getResult()!=null && op.getResult().toString().equalsIgnoreCase("N"))
                        {
                            if (op.getErrors().isEmpty()) 
                            {
                                ADFUtils.findOperation("Commit").execute();
                                FacesMessage Message = new FacesMessage("Record Update Successfully.");
                                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                                FacesContext fc = FacesContext.getCurrentInstance();
                                fc.addMessage(null, Message);
                                return "SaveAndClose";
                            }
                        }
                        else if (op.getResult() != null && op.getResult().toString() != "") 
                        {
                            if (op.getErrors().isEmpty()) 
                            {
                                System.out.println("CREATE MODE");
                                ADFUtils.findOperation("Commit").execute();
                                FacesMessage Message = new FacesMessage("Record Save Successfully.Vendor Code is "+op.getResult());
                                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                                FacesContext fc = FacesContext.getCurrentInstance();
                                fc.addMessage(null, Message);
                                return "SaveAndClose";
                            }
                        }  
                        if(op.getResult()!=null || op.getResult().toString().equalsIgnoreCase("N"))
                        {
                            try {
                                check = false;
                                String path;
                                if(getPhotoFile()!= null){
                                path = "/home/beta17/pics/" + PhotoFile.getFilename();
                                Imagepath = SaveuploadFile(PhotoFile, path);
                                //        System.out.println("path " + Imagepath);
                                OperationBinding ob = ADFUtils.findOperation("AddImagePath");
                                ob.getParamsMap().put("ImagePath", Imagepath);
                                ob.execute();    
                                }
                                File directory = new File("/tmp/pht");
                                //get all the files from a directory
                                File[] fList = directory.listFiles();
                                if(fList !=null){
                                for (File file : fList) {
                                //Delete all previously uploaded files
                                // if (!"NoImage.png".equalsIgnoreCase(file.getName())) {
                                file.delete();
                                //}
                                }
                                }
                            } catch (Exception ee) {
                                System.out.println("Error in Path and Directory");
                            }
                        }
//                } else {
//                    ADFUtils.showMessage("First Two Characters Of GST Regd. No. Is Equals First Two Characters Of State GST Code",0);
//                        return null;
//                }
//            } 
//            else{
//                 ADFUtils.showMessage("State GST Code not found when city is selected.", 0);
//                    return null;
//                }
//        }else {
//                ADFUtils.showMessage("GST Regd.No. in General tab is required.", 0);
//                return null;
//            }
//            }else{
//                ADFUtils.showMessage("Pan No. should be maximum 10 Digits.",0);
//                return null;
//            }
        }
        else 
        {
            OperationBinding op=ADFUtils.findOperation("genarateVendorCode");
            op.execute();
            
            System.out.println("Get Result is====> " + op.getResult());
            if(op.getResult()!=null && op.getResult().toString().equalsIgnoreCase("N"))
            {
                if (op.getErrors().isEmpty()) 
                {
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message = new FacesMessage("Record Update Successfully.");
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);
                    return "SaveAndClose";
                }
            }
            else if (op.getResult() != null && op.getResult().toString() != "") 
            {
                if (op.getErrors().isEmpty()) 
                {
                    System.out.println("CREATE MODE");
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message = new FacesMessage("Record Save Successfully.Vendor Code is "+op.getResult());
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);
                    return "SaveAndClose";
                }
            }  
            if(op.getResult()!=null || op.getResult().toString().equalsIgnoreCase("N"))
            {
                try {
                    check = false;
                    String path;
                    if(getPhotoFile()!= null){
                    path = "/home/beta17/pics/" + PhotoFile.getFilename();
                    Imagepath = SaveuploadFile(PhotoFile, path);
                    //        System.out.println("path " + Imagepath);
                    OperationBinding ob = ADFUtils.findOperation("AddImagePath");
                    ob.getParamsMap().put("ImagePath", Imagepath);
                    ob.execute();    
                    }
                    File directory = new File("/tmp/pht");
                    //get all the files from a directory
                    File[] fList = directory.listFiles();
                    if(fList !=null){
                    for (File file : fList) {
                    //Delete all previously uploaded files
                    // if (!"NoImage.png".equalsIgnoreCase(file.getName())) {
                    file.delete();
                    //}
                    }
                    }
                } catch (Exception ee) {
                    System.out.println("Error in Path and Directory");
                }
            }
         }          //else close
            
        }             // required Method close
        return null;
    }

    public void glCodeVCE(ValueChangeEvent vce) 
    {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        String gl_code=(String)vce.getOldValue();
        System.out.println(" the old Gl Code is----"+gl_code+"---New value is--"+vce.getNewValue());
        if(vce.getOldValue()!=null && ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E"))
        {
            System.out.println("=----- In the Edit Mode------");

            OperationBinding ob = ADFUtils.findOperation("glCodeFilterInVendor");
            ob.getParamsMap().put("glcode", vce.getOldValue());
            ob.execute();
            Row row=(Row)ADFUtils.evaluateEL("#{bindings.VendUnitVO1Iterator.currentRow}");
            System.out.println(" and the gl code is =====>>"+row.getAttribute("GlCd"));
                if((ob.getResult() !=null && ob.getResult().equals("Y")))
                {
                    
                    row.setAttribute("GlCd", null);
                    ADFUtils.showMessage("You can not change already mapped Gl Code.",0);
                    
                    if(row.getAttribute("GlCd")==null){
                        row.setAttribute("GlCd", gl_code);
                    }
                }
                else{
                    //System.out.println("hello------");
                }
                
        }
    }

    public void setVendorFlagBinding(RichSelectOneChoice vendorFlagBinding) {
        this.vendorFlagBinding = vendorFlagBinding;
    }

    public RichSelectOneChoice getVendorFlagBinding() {
        return vendorFlagBinding;
    }

    public void panNumberBinding(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
    }

    public void vendorNameValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        String name = (String) object;
        String trnasName = (String) vendorNameTransBinding.getValue();
        System.out.println("name"+name+" trnasName "+trnasName);
        //        System.out.println("Character "+name.substring(0,1)+" "+trnasName.substring(0, 1));
        if(trnasName!=null){
        if(name.substring(0,1).equalsIgnoreCase(trnasName.substring(0, 1))){
            System.out.println("Equals");
            cevmodecheck();
            }
        else{
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                              "First Charcater should be same.",
                                                              null));
            }
        }
        

    }

    public void setVendorNameTransBinding(RichInputText vendorNameTransBinding) {
        this.vendorNameTransBinding = vendorNameTransBinding;
    }

    public RichInputText getVendorNameTransBinding() {
        return vendorNameTransBinding;
    }
}
