package terms.eng.setup.ui.servlet;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

@WebServlet(name = "VendorMasterUploade", urlPatterns = { "/vendormasteruploade" })
public class VendorMasterUploade extends HttpServlet {
    private static final String CONTENT_TYPE = "text/html; charset=UTF-8";

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }
    PreparedStatement ps = null;
    ResultSet rs = null;
    String Path = "";

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType(CONTENT_TYPE);
        String check = request.getParameter("check");
        String path = "";
        if (check.equals("true")) {
            Path = request.getParameter("path");
            String FileName = Path.substring(Path.lastIndexOf("/") + 1);
            Path = "/tmp/pht/" + FileName;
        } else {
            Path =
                request.getParameter("pathdb") == null ? request.getParameter("path") : request.getParameter("pathdb");
        }

        OutputStream os = response.getOutputStream();
        String impath = Path;
        InputStream inputStream = null;

        try {
            File outputFile = new File(impath);
            inputStream = new FileInputStream(outputFile);
            BufferedInputStream in = new BufferedInputStream(inputStream);
            int b;
            byte[] buffer = new byte[10240];
            while ((b = in.read(buffer, 0, 10240)) != -1) {
                os.write(buffer, 0, b);
            }
        } catch (Exception e) {

            System.out.println(e);
        } finally {
            if (os != null) {
                os.close();
            }
            if (inputStream != null) {
                inputStream.close();
            }

        }
    }
}


