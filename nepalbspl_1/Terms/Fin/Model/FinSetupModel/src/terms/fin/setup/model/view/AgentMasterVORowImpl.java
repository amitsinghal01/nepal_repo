package terms.fin.setup.model.view;

import java.sql.Timestamp;

import oracle.jbo.RowSet;
import oracle.jbo.server.EntityImpl;
import oracle.jbo.server.ViewRowImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Tue Sep 04 12:55:54 IST 2018
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class AgentMasterVORowImpl extends ViewRowImpl {
    public static final int ENTITY_AGENTMASTEREO = 0;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        AgentCode,
        AgentName,
        AgentType,
        Address,
        CityCode,
        ContPerson,
        Phone,
        Mobile,
        Email,
        FreeDays,
        StateCode,
        CountryCode,
        CreatedBy,
        ObjectVersionNumber,
        CreationDate,
        LastUpdateDate,
        LastUpdatedBy,
        EditTrans,
        CityVO1,
        SecControlVO1;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }
    public static final int AGENTCODE = AttributesEnum.AgentCode.index();
    public static final int AGENTNAME = AttributesEnum.AgentName.index();
    public static final int AGENTTYPE = AttributesEnum.AgentType.index();
    public static final int ADDRESS = AttributesEnum.Address.index();
    public static final int CITYCODE = AttributesEnum.CityCode.index();
    public static final int CONTPERSON = AttributesEnum.ContPerson.index();
    public static final int PHONE = AttributesEnum.Phone.index();
    public static final int MOBILE = AttributesEnum.Mobile.index();
    public static final int EMAIL = AttributesEnum.Email.index();
    public static final int FREEDAYS = AttributesEnum.FreeDays.index();
    public static final int STATECODE = AttributesEnum.StateCode.index();
    public static final int COUNTRYCODE = AttributesEnum.CountryCode.index();
    public static final int CREATEDBY = AttributesEnum.CreatedBy.index();
    public static final int OBJECTVERSIONNUMBER = AttributesEnum.ObjectVersionNumber.index();
    public static final int CREATIONDATE = AttributesEnum.CreationDate.index();
    public static final int LASTUPDATEDATE = AttributesEnum.LastUpdateDate.index();
    public static final int LASTUPDATEDBY = AttributesEnum.LastUpdatedBy.index();
    public static final int EDITTRANS = AttributesEnum.EditTrans.index();
    public static final int CITYVO1 = AttributesEnum.CityVO1.index();
    public static final int SECCONTROLVO1 = AttributesEnum.SecControlVO1.index();

    /**
     * This is the default constructor (do not remove).
     */
    public AgentMasterVORowImpl() {
    }

    /**
     * Gets AgentMasterEO entity object.
     * @return the AgentMasterEO
     */
    public EntityImpl getAgentMasterEO() {
        return (EntityImpl) getEntity(ENTITY_AGENTMASTEREO);
    }

    /**
     * Gets the attribute value for AGENT_CODE using the alias name AgentCode.
     * @return the AGENT_CODE
     */
    public String getAgentCode() {
        return (String) getAttributeInternal(AGENTCODE);
    }

    /**
     * Sets <code>value</code> as attribute value for AGENT_CODE using the alias name AgentCode.
     * @param value value to set the AGENT_CODE
     */
    public void setAgentCode(String value) {
        setAttributeInternal(AGENTCODE, value);
    }

    /**
     * Gets the attribute value for AGENT_NAME using the alias name AgentName.
     * @return the AGENT_NAME
     */
    public String getAgentName() {
        return (String) getAttributeInternal(AGENTNAME);
    }

    /**
     * Sets <code>value</code> as attribute value for AGENT_NAME using the alias name AgentName.
     * @param value value to set the AGENT_NAME
     */
    public void setAgentName(String value) {
        setAttributeInternal(AGENTNAME, value);
    }

    /**
     * Gets the attribute value for AGENT_TYPE using the alias name AgentType.
     * @return the AGENT_TYPE
     */
    public String getAgentType() {
        return (String) getAttributeInternal(AGENTTYPE);
    }

    /**
     * Sets <code>value</code> as attribute value for AGENT_TYPE using the alias name AgentType.
     * @param value value to set the AGENT_TYPE
     */
    public void setAgentType(String value) {
        setAttributeInternal(AGENTTYPE, value);
    }

    /**
     * Gets the attribute value for ADDRESS using the alias name Address.
     * @return the ADDRESS
     */
    public String getAddress() {
        return (String) getAttributeInternal(ADDRESS);
    }

    /**
     * Sets <code>value</code> as attribute value for ADDRESS using the alias name Address.
     * @param value value to set the ADDRESS
     */
    public void setAddress(String value) {
        setAttributeInternal(ADDRESS, value);
    }

    /**
     * Gets the attribute value for CITY_CODE using the alias name CityCode.
     * @return the CITY_CODE
     */
    public String getCityCode() {
        return (String) getAttributeInternal(CITYCODE);
    }

    /**
     * Sets <code>value</code> as attribute value for CITY_CODE using the alias name CityCode.
     * @param value value to set the CITY_CODE
     */
    public void setCityCode(String value) {
        setAttributeInternal(CITYCODE, value);
    }

    /**
     * Gets the attribute value for CONT_PERSON using the alias name ContPerson.
     * @return the CONT_PERSON
     */
    public String getContPerson() {
        return (String) getAttributeInternal(CONTPERSON);
    }

    /**
     * Sets <code>value</code> as attribute value for CONT_PERSON using the alias name ContPerson.
     * @param value value to set the CONT_PERSON
     */
    public void setContPerson(String value) {
        setAttributeInternal(CONTPERSON, value);
    }

    /**
     * Gets the attribute value for PHONE using the alias name Phone.
     * @return the PHONE
     */
    public String getPhone() {
        return (String) getAttributeInternal(PHONE);
    }

    /**
     * Sets <code>value</code> as attribute value for PHONE using the alias name Phone.
     * @param value value to set the PHONE
     */
    public void setPhone(String value) {
        setAttributeInternal(PHONE, value);
    }

    /**
     * Gets the attribute value for MOBILE using the alias name Mobile.
     * @return the MOBILE
     */
    public String getMobile() {
        return (String) getAttributeInternal(MOBILE);
    }

    /**
     * Sets <code>value</code> as attribute value for MOBILE using the alias name Mobile.
     * @param value value to set the MOBILE
     */
    public void setMobile(String value) {
        setAttributeInternal(MOBILE, value);
    }

    /**
     * Gets the attribute value for EMAIL using the alias name Email.
     * @return the EMAIL
     */
    public String getEmail() {
        return (String) getAttributeInternal(EMAIL);
    }

    /**
     * Sets <code>value</code> as attribute value for EMAIL using the alias name Email.
     * @param value value to set the EMAIL
     */
    public void setEmail(String value) {
        setAttributeInternal(EMAIL, value);
    }

    /**
     * Gets the attribute value for FREE_DAYS using the alias name FreeDays.
     * @return the FREE_DAYS
     */
    public Integer getFreeDays() {
        return (Integer) getAttributeInternal(FREEDAYS);
    }

    /**
     * Sets <code>value</code> as attribute value for FREE_DAYS using the alias name FreeDays.
     * @param value value to set the FREE_DAYS
     */
    public void setFreeDays(Integer value) {
        setAttributeInternal(FREEDAYS, value);
    }

    /**
     * Gets the attribute value for STATE_CODE using the alias name StateCode.
     * @return the STATE_CODE
     */
    public String getStateCode() {
        return (String) getAttributeInternal(STATECODE);
    }

    /**
     * Sets <code>value</code> as attribute value for STATE_CODE using the alias name StateCode.
     * @param value value to set the STATE_CODE
     */
    public void setStateCode(String value) {
        setAttributeInternal(STATECODE, value);
    }

    /**
     * Gets the attribute value for COUNTRY_CODE using the alias name CountryCode.
     * @return the COUNTRY_CODE
     */
    public String getCountryCode() {
        return (String) getAttributeInternal(COUNTRYCODE);
    }

    /**
     * Sets <code>value</code> as attribute value for COUNTRY_CODE using the alias name CountryCode.
     * @param value value to set the COUNTRY_CODE
     */
    public void setCountryCode(String value) {
        setAttributeInternal(COUNTRYCODE, value);
    }

    /**
     * Gets the attribute value for CREATED_BY using the alias name CreatedBy.
     * @return the CREATED_BY
     */
    public String getCreatedBy() {
        return (String) getAttributeInternal(CREATEDBY);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER, value);
    }

    /**
     * Gets the attribute value for CREATION_DATE using the alias name CreationDate.
     * @return the CREATION_DATE
     */
    public Timestamp getCreationDate() {
        return (Timestamp) getAttributeInternal(CREATIONDATE);
    }

    /**
     * Gets the attribute value for LAST_UPDATE_DATE using the alias name LastUpdateDate.
     * @return the LAST_UPDATE_DATE
     */
    public Timestamp getLastUpdateDate() {
        return (Timestamp) getAttributeInternal(LASTUPDATEDATE);
    }

    /**
     * Gets the attribute value for LAST_UPDATED_BY using the alias name LastUpdatedBy.
     * @return the LAST_UPDATED_BY
     */
    public String getLastUpdatedBy() {
        return (String) getAttributeInternal(LASTUPDATEDBY);
    }

    /**
     * Gets the attribute value for the calculated attribute EditTrans.
     * @return the EditTrans
     */
    public Integer getEditTrans() {
        byte entityState = this.getEntity(0).getEntityState();
        return new Integer(entityState);
        //return (Integer) getAttributeInternal(EDITTRANS);
       // return (Integer) getAttributeInternal(EDITTRANS);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute EditTrans.
     * @param value value to set the  EditTrans
     */
    public void setEditTrans(Integer value) {
        setAttributeInternal(EDITTRANS, value);
    }

    /**
     * Gets the view accessor <code>RowSet</code> CityVO1.
     */
    public RowSet getCityVO1() {
        return (RowSet) getAttributeInternal(CITYVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> SecControlVO1.
     */
    public RowSet getSecControlVO1() {
        return (RowSet) getAttributeInternal(SECCONTROLVO1);
    }
}

