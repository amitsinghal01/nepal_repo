package terms.fin.transaction.model.applicationModule.common;

import java.math.BigDecimal;

import java.util.ArrayList;

import oracle.jbo.ApplicationModule;
import oracle.jbo.domain.Date;
import oracle.jbo.domain.Timestamp;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Wed Feb 07 19:31:43 IST 2018
// ---------------------------------------------------------------------
public interface FinTransactionAM extends ApplicationModule {
    String checkForCurrentDate(Date vou_dt);

    String checkForInvoiceDate(Date ca_dt);

    String checkForMonthAndYearVouSeries(Date vou_dt);

    String checkForMonthlyClosing(Date vou_dt);

    void depidTdsDeposit(Long DepId);

    String finYearTdsDeposit();

    String generateRecoNo();

    String generateTdsDepositNumber();

    void getBankRecoDetails(String RecoNo);


    //    void getContraId(Long contaId);
   
    void getContraId(Long contraId);

    String getCurrencyRate(BigDecimal fc_Amount);

    String getVouNo();

    String getcheckApprovedSatus();

    String getdummyFunction();

    String populateBankReco();

    String populdateTdsDepositDetailData();
   
    void getBankCashVoucherId(Long bankCashVoucherId);

    String checkDummyApprovedVoucherStatusBankCash();

    String dummyBankFunctionBankCash();

    String newVoucherFunctionBankCash();

    void balanceBankCash(String unit, String cntrlCode, oracle.jbo.domain.Date vouDate, String year);

    String checkfcAmountValidation(String currCode, BigDecimal opbal, String hCurrCode);


    void controlAmountValue(BigDecimal val);

    String checkGlBalanceOverdraft(String unit, String cntrlCode, oracle.jbo.domain.Date vouDate, String year,
                                   BigDecimal ovrdrftLmt, String vouType, String vouSeries, BigDecimal debit,
                                   BigDecimal credit);


    void valueSetNullInBankCash();

    void getCollectionAdviceFiletred(Long adviceNo);

    void maxInvoiceDate();

    void allDataFetched();

    String toGenerateAdviceNo(String type);
   
    String filterFinTvouchVO(Long vou_id);

    String restVouchVOCriteria();

    String checkDifferenceAmount();

    String checkForDuplicateAcc(String sub_code);

    String checkVoucherSeries();

    String fetchVouNumber(String vou_mode);


    //    String checkForMonthlyClosing(Date vou_dt);

//    Sring checkForCurrentDate(Date vou_dt);

//    String checkForMonthAndYearVouSeries(Date vou_dt);

    String filterPaymentVoForView(oracle.jbo.domain.Number pay_id);

    String populateAndInsertRecordIntoPayDetails();

    String checkForValidBillAmount();

    oracle.jbo.domain.Number calculateTotalBillAmount();

    String generatePayDocNoAndSave();
   
    String getFilterPuchaseData(Long purchaseId);

    String getFilterSalesData(Long purchaseId);

    String checkForMonthlyClosingForContraVoucher(Date vou_dt);

    String checkForMonthAndYearVouSeriesForContraVoucher(Date vou_dt);

    void setInEditModeContraVoucher();


    String detailRowCount();

    void getBillId(Long billId);

    String populateContractServiceBillData(String joNo);

    void GSTCalculateValue();

    void getGstHsnCodeForItem();

    void getItemCodeDetailForBP(String itemCode);


    void roundOffCalc();

    void billDueDate();
    void getDocId(Long DocId);

    void RefreshAmount(String Unit, String vencd);


    void GstCodefromProdcode(String HsnNo);

    void gethsnno(String ProdCode);

    String generateDnCnNo();


    void TdsCalculation();
   
//    void TdsCalculationForSB();

    String curreRate();
    void checkSameRecord();
    void TdsCalculationForSB(BigDecimal totalAmt);

    void tdsValueSet();

    void claerRoundOffValue();

    void checkSameRecordBankCash();
    void generatedrcrid();

    void controlAmountValueContraVoucher(BigDecimal val);

    String CurrencyRateContraV(String currencyCode);
    void clearHeaderFieldValue();

    String checkApprovedServiceBillStatus();


    void CheckAuthority(String unitCd, String EmpCd, String NoteType);

  
    String checkServiceBillAutho(String ename, String empCd);

    void checkApprovalStatusBankCash(String unitCd, String empCode, BigDecimal amount, String formNm);

    void checkApprovalStatusContraVoucher(String unitCd, String empCode, BigDecimal amount);

    void checkApprovalStatusInCheckAutho(String unitCd, String empCode, BigDecimal amount);
   
    void filterPurchaseBillById(Long BillId);


    void roundOffPurchaseBill();

    String gstCalculationPurchaseBill();

    void findMaxBillDatePurchaseBill();

    String populateSrvDetailDataPurchaseBill();

    void changeGstCalculationValuePurchaseBill(String TypeVal);

    void tdsCalculationPurchaseBill();

    void populateDataInItemDetailPurchaseBill();

    void venLedgerPurchaseBill();

    void multipleSrvCasePurchaseBill();

    String makerCheckerForPurchseBill();

    String checkAuthorityForApprovalPurchaseBill(String EmpCode);

    String dateCheckerInPurchaseBill();


    String checkForMonthAndYearVouSeriesForAllVouchers(Date vou_dt, String FinYear, String Vou_Type, String Vou_Series,
                                                       String Unit_Code);

    String generatePurchaseBillNo(String UserName);

    String generateBillPvServiceBill(String userName);


    String fcToLcAmount(String currencyCode);

    String fetchCurrRate(String currencyCode);

    void populateVoucherDetails();

    void insertDataDetailToHeader();

    String getChequeNoValidBankCashVoucher(String chequeno, String bankCode);
    String insertDataInPurchaseBillJV();

    void setItemDetailDataInBillJVPurchaseBill();

    String getChequeNoValid(String chequeno, String bankCode);

    String getBankCodeForCV(String glCode);
   
    void createBillJvRowServiceBill();
    String srvToItem();


    void SgstGlDrCr();

    String generateDrCrTds();

    String generateDrCrVou();

    void tdsCalculationOnPaymentAmountPurchaseBill();

    void clearDetailRowCollectionAdvice();

    void refreshRoundOffGL();

    String checkDuplicateSrvPurchaseBill(String srvNo);

    String populateDetailFormItem();
   
    String checkDuplicateVendorBillNo(String finyear);

    String checkAuthorityForApprovalVendorPayments(String EmpCode);

    void clearTDSValue();

    String checkChequeRangeVendorPayments();

    String getLicenceNumber();
    void clearHeaderCollectionAdvice();

    String duplicateCheckCollectionAdvice(String val);

    void checkApprovalStatusCollectionAdvice(String unitCd, String empCode, BigDecimal amount);


    String currencyRateCollectionAdvice(String currencyCode);

    String collectionAdviceNoGeneration();

    void tdsAmountForVendorPayment();

    void selectAndUnSelectAllVendorPayment(String mode);

    void getExportInvoiceUpdate(String InvNo);

    void getCommInvoiceId(Long invoiceId);

    String generateCommercialInvNo();

    void checkPrifixVendorPayment();

    void setCommInvoiceNo();

    String finYearCommercialInvoice();

    String generalLedgerTransfer();

    String SubLedgerTransfer();

    void getProductInvoiceCalculation();

    void getProductCodeDetailCI(String product);
   
    void filterSrvIdImportBillPassing(Long srvId);


    void populateDataInDetailAndCalulateImportBillPassing();
   
    void commercialInvoiceGstCalculation();
    void getCostGlCode(String GlCodeCost, Timestamp ToDate, Timestamp FromDate, String UnitCode);

    String getEntryNo();

    void checkEditSelect();
    void partyBillHoldPopulateData();

    void valueSetNullInDetail();

    void checkAuthorityForComInvoice(String unitCd, String empCd);

    void clearSelectedRowOnChangeDocType();
    void setNullInHeaderCommercialInvoice();

    void getAdvanceCollectionFiletred(Long adviceNo);

    void amountCalculateAc();

    String advanceCollectionNoGeneration();

    void igstItemDetailGstGlCodeDataImportBillPassing();

    String insertHeadVoucherDataImportBillPassing();

    String checkApprovalStatus(String emp_code, String form_name, String unit_code, String autho_level,
                               BigDecimal amount);

    void itemGlCodeSetToBillImportBillPassing();

    String generateDocumentNoImportBillPassing(String userName);

    String makerCheckerForImportBillPassing();

    String checkAuthoForImportBillPassing();

    void getShippingBillNo(String docNo);

    String generateShippingBillNo();
   
    void getBillOfEntryFiletred(Long DocId);

    String generateDocNoBOE();
   
    String documentNumberGenerationBRC();

    void docNoFilterBRC(String docNo);


    String documentNumberGenerationAnnexurea();

    String voucherGenerationBillApproval(String UserName, String empCode);


    void unselectAllReversalVoucherApproval();


    void unselectAllVoucherApproval();

    String allVoucherApproval(String empCd);

    String checkApprovalAuthority(String emp_code, String form_name, String unit_code, String autho_level);

    void selectAllVoucherApproval(String emp_code);

    void refreshSearchApproval();

    void ApprovalDateVoucherApproval(Date val);
    void FilterRtgsPayment(String DocNo);

    void PopulateRtgsDetail();

    String generateRtgsNo();

    void checkALLRTGS();
    void filterBillOfLading(String DocNo);

    String generateDocNoForBillOfLading();

    String getServiceBillNumber(String userName);

    void setServiceBillNumber();

    void getTdsVoucherCalculationsSB();

    String checkDuplicateContractSB(String contractNo);

    void itemCodeDuplicateServiceBill();

    String duplicateItemCheckInDetailSB(String itemCode);
    void calculateDebitNotePurchaseBill();

    void checkTransRTGSPayment();

    String getChequeNoValidBankCashVoucherDuplicacy(String chequeno);

    String CheckFromDailyCurrBrc(String brcCurrCd, Date brcDt, String check);
    String IgstGlDrCr();

    String CgstGlDrCr();

    String reversalVoucherApproval(String userName);

    void changeValuesOnPurchaseBill();

    String checkRejectionSumPurchaseBill();

    void diffrenceValueInBillDrPvPurchaseBill();

    void removeDebitNoteRowInPurchaseBill();

    void filterSrvAfterSetDataPurchaseBill();

    void getExporterDetailAnnex(String unitCd);

    String populateAnnexureaDetail(String invoiceNo);

    void docNoFilterInAnnexurea(Long docId);

    String populateAnnexaDtlToItemDtl(String invNo);

    void narrationValuePurchaseBill();

    BigDecimal importAmtCalcBOE(BigDecimal value);

    String fileNameForPrint(String fileId);


    void TrialCallingProcedure(String unitCode, String Type, String SID, Date DateFrom, Date DateTo);

    void BankRecoSummaryProcedureCalling(Date DateFrom, Date DateTo, String BankGL, String unitCode, String SID);

    void SubLedgerCallingProc(String glCd, String unitCd, String sId, Date frDate, Date toDate, String subCodeFr,
                              String subCodeTo);

    void LedgerCallingProc(String glCd, String glCd1, String unitCd, String sId, Date frDate, Date toDate);

    void BankRecoProcedureCalling(Date FRDATE, Date TODATE, String BANK_CODE, String LV_UNIT, String LV_SID,
                                  String LV_TP);

    void SubLedgerTrailProc(String glCd, String unitCd, String sId, Date frDate, Date toDate, String year);

    void controlAmountValueJV(BigDecimal val);

    void refreshMemorendumVoucher();

    String unapprovedVouDeletion();

    void filteMemoHeaderById(Long MemId);

    BigDecimal diffValueMemorendumVoucherDetail();

    String docNoGenerateMemorendumVoucher();

    String generateSeqNo();
    void brcShippingBillNo();

    void filteMeisHeaderByIdMeis(Long MeisId);

    String generateDocNoInMesi();

    void populateDataSrvToImportBillHeader();
void reverseDateVoucherApproval(Date val);
void removeImportDetailImportBillPvRow();

    void ProfitAndLossCallingProcedure(String LV_UNIT, String LV_FIN_YEAR, String LV_SID, Date FRDATE, Date TODATE);

    void BalanceSheetCallingProcedure(String LV_UNIT, String LV_FIN_YEAR, String LV_SID, Date FRDATE, Date TODATE);

    void contractDetailCopyInSB();

    void getPaymentRecomFilter(Long docId);

    String populatePaymentRecommendation();

    String generatePaymentRecomDocNo();

    String checkPaymentRecommendationStatus();

    String locationWiseAllocDetails(String joBillNo);

    void currencyCodeValuesFromInvoiceBRC();

    void CheckAllCheckbox();

    void narrationValueServiceBill();

    void voucherNoInUpdateModeJournalVoucher();

    void getBillDeletionDetails(String billNo);

    String checkBillDeletionAuthority();

    String saveUnApproveBillDeletion();
    void filterFinanceAttachment(String VouId);

    void setFileDataPI(String name, String path, String contTyp, Integer SrNo);

    String uploadFilePath();

    String checkAuthorityBillApproval(String authoLevel, String empCode, String formName, String unitCode,
                                      BigDecimal amount);

    String selectUnselectBillApproval(String check_value, String employeeCode, Date billdate);

    void transBillDateValueSetBillApproval(Date billdate);

    void removeRowBankCashVoucher(String glCode);

    void forwardOCLedgerImportBillPassing(Integer keyNo, Integer keyId);

    void freightOCLedgerImportBillPassing(Integer keyNo, Integer keyId);

    void insuranceOCLedgerImportBillPassing(Integer keyNo, Integer keyId);

    String countInvoice(String invoiceNo);


    String copyBankVoucherFromExisting();

    void refreshPage();

    String copyVoucherFromExisting(String vouDate);

    void uploadJouralVoucher(String inputstream, String vouDate);

    String EndDat();

    String startDate();
}
