package terms.fin.transaction.model.helper;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class FinTransactionModelHelper {
    public FinTransactionModelHelper() {
        super();
    }
    
    public static String convertAdToBs(String adDate2) throws ParseException {
        String adDate1 = adDate2.substring(0, 10);
        String[] getCurrentYear1 = adDate1.split("-");
        String adDate = getCurrentYear1[2] + '-' + getCurrentYear1[1] + '-' + getCurrentYear1[0];
        String[] getCurrentYear = adDate.split("-");
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        java.util.Date current = df.parse(adDate);
        java.util.Date start = null;
        long equBs = Lookup.lookupNepaliYearStart;
        Integer[] monthDay = null;
        for (int i = 0; i < Lookup.lookup.size(); i++) {
            String[] getStartYear = Lookup.lookup.get(i)[0].split("-");
            if (getStartYear[2].equals(getCurrentYear[2])) {
                DateFormat df1 = new SimpleDateFormat("dd-MMM-yyyy");
                start = df1.parse(Lookup.lookup.get(i)[0]);
                monthDay = Lookup.monthDays.get(i);
                equBs += i;
                if (start.getTime() >= current.getTime()) {
                    start = df1.parse(Lookup.lookup.get(i - 1)[0]);
                    equBs -= 1;
                }
            }
        }
        long diff = current.getTime() - start.getTime();
        long difference = diff / (1000 * 60 * 60 * 24);
        int nepYear = (int) equBs;
        int nepMonth = 0;
        int nepDay = 1;
        int daysInMonth;
        while (difference != 0) {
            if (difference >= 0) {
                daysInMonth = monthDay[nepMonth];
                nepDay++;
                if (nepDay > daysInMonth) {
                    nepMonth++;
                    nepDay = 1;
                }
                if (nepMonth >= 12) {
                    nepYear++;
                    nepMonth = 0;
                }
                difference--;
            }
        }
        nepMonth += 1;
        return nepDay + "/" + getNepaliMonthString(nepMonth) + "/" + nepYear;

    }

    public static String getNepaliMonthString(int month) {
        switch (month) {
        case 1:
            return "Baishakh";
        case 2:
            return "Jestha";
        case 3:
            return "Asar";
        case 4:
            return "Shrawan";
        case 5:
            return "Bhadau";
        case 6:
            return "Aswin";
        case 7:
            return "Kartik";
        case 8:
            return "Mansir";
        case 9:
            return "Poush";
        case 10:
            return "Magh";
        case 11:
            return "Falgun";
        case 12:
            return "Chaitra";
        }
        return null;
    }
}
