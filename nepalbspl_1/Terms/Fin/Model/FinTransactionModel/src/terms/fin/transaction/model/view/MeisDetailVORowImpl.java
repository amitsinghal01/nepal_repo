package terms.fin.transaction.model.view;

import java.math.BigDecimal;

import java.sql.Timestamp;

import oracle.jbo.RowSet;
import oracle.jbo.domain.Date;
import oracle.jbo.server.EntityImpl;
import oracle.jbo.server.ViewRowImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Fri Apr 26 12:59:38 IST 2019
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class MeisDetailVORowImpl extends ViewRowImpl {


    public static final int ENTITY_MEISDETAILEO = 0;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        ActualReceive,
        BrcVal,
        CreatedBy,
        CreationDate,
        DocDate,
        DocNo,
        InvDt,
        InvNo,
        LastUpdateDate,
        LastUpdatedBy,
        MeisId,
        MeisLineId,
        ObjectVersionNumber,
        Remarks,
        ShippBillDt,
        ShippBillNo,
        ShippBillVal,
        ValForIntitle,
        SumOfActRecTrans,
        InvoiceNoMeisDetailVVO1;
        static AttributesEnum[] vals = null;
        ;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int ACTUALRECEIVE = AttributesEnum.ActualReceive.index();
    public static final int BRCVAL = AttributesEnum.BrcVal.index();
    public static final int CREATEDBY = AttributesEnum.CreatedBy.index();
    public static final int CREATIONDATE = AttributesEnum.CreationDate.index();
    public static final int DOCDATE = AttributesEnum.DocDate.index();
    public static final int DOCNO = AttributesEnum.DocNo.index();
    public static final int INVDT = AttributesEnum.InvDt.index();
    public static final int INVNO = AttributesEnum.InvNo.index();
    public static final int LASTUPDATEDATE = AttributesEnum.LastUpdateDate.index();
    public static final int LASTUPDATEDBY = AttributesEnum.LastUpdatedBy.index();
    public static final int MEISID = AttributesEnum.MeisId.index();
    public static final int MEISLINEID = AttributesEnum.MeisLineId.index();
    public static final int OBJECTVERSIONNUMBER = AttributesEnum.ObjectVersionNumber.index();
    public static final int REMARKS = AttributesEnum.Remarks.index();
    public static final int SHIPPBILLDT = AttributesEnum.ShippBillDt.index();
    public static final int SHIPPBILLNO = AttributesEnum.ShippBillNo.index();
    public static final int SHIPPBILLVAL = AttributesEnum.ShippBillVal.index();
    public static final int VALFORINTITLE = AttributesEnum.ValForIntitle.index();
    public static final int SUMOFACTRECTRANS = AttributesEnum.SumOfActRecTrans.index();
    public static final int INVOICENOMEISDETAILVVO1 = AttributesEnum.InvoiceNoMeisDetailVVO1.index();

    /**
     * This is the default constructor (do not remove).
     */
    public MeisDetailVORowImpl() {
    }

    /**
     * Gets MeisDetailEO entity object.
     * @return the MeisDetailEO
     */
    public EntityImpl getMeisDetailEO() {
        return (EntityImpl) getEntity(ENTITY_MEISDETAILEO);
    }

    /**
     * Gets the attribute value for ACTUAL_RECEIVE using the alias name ActualReceive.
     * @return the ACTUAL_RECEIVE
     */
    public BigDecimal getActualReceive() {
        return (BigDecimal) getAttributeInternal(ACTUALRECEIVE);
    }

    /**
     * Sets <code>value</code> as attribute value for ACTUAL_RECEIVE using the alias name ActualReceive.
     * @param value value to set the ACTUAL_RECEIVE
     */
    public void setActualReceive(BigDecimal value) {
        setAttributeInternal(ACTUALRECEIVE, value);
    }

    /**
     * Gets the attribute value for BRC_VAL using the alias name BrcVal.
     * @return the BRC_VAL
     */
    public BigDecimal getBrcVal() {
        return (BigDecimal) getAttributeInternal(BRCVAL);
    }

    /**
     * Sets <code>value</code> as attribute value for BRC_VAL using the alias name BrcVal.
     * @param value value to set the BRC_VAL
     */
    public void setBrcVal(BigDecimal value) {
        setAttributeInternal(BRCVAL, value);
    }

    /**
     * Gets the attribute value for CREATED_BY using the alias name CreatedBy.
     * @return the CREATED_BY
     */
    public String getCreatedBy() {
        return (String) getAttributeInternal(CREATEDBY);
    }

    /**
     * Gets the attribute value for CREATION_DATE using the alias name CreationDate.
     * @return the CREATION_DATE
     */
    public Timestamp getCreationDate() {
        return (Timestamp) getAttributeInternal(CREATIONDATE);
    }

    /**
     * Gets the attribute value for DOC_DATE using the alias name DocDate.
     * @return the DOC_DATE
     */
    public Date getDocDate() {
        return (Date) getAttributeInternal(DOCDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for DOC_DATE using the alias name DocDate.
     * @param value value to set the DOC_DATE
     */
    public void setDocDate(Date value) {
        setAttributeInternal(DOCDATE, value);
    }

    /**
     * Gets the attribute value for DOC_NO using the alias name DocNo.
     * @return the DOC_NO
     */
    public String getDocNo() {
        return (String) getAttributeInternal(DOCNO);
    }

    /**
     * Sets <code>value</code> as attribute value for DOC_NO using the alias name DocNo.
     * @param value value to set the DOC_NO
     */
    public void setDocNo(String value) {
        setAttributeInternal(DOCNO, value);
    }

    /**
     * Gets the attribute value for INV_DT using the alias name InvDt.
     * @return the INV_DT
     */
    public Date getInvDt() {
        return (Date) getAttributeInternal(INVDT);
    }

    /**
     * Sets <code>value</code> as attribute value for INV_DT using the alias name InvDt.
     * @param value value to set the INV_DT
     */
    public void setInvDt(Date value) {
        setAttributeInternal(INVDT, value);
    }

    /**
     * Gets the attribute value for INV_NO using the alias name InvNo.
     * @return the INV_NO
     */
    public String getInvNo() {
        return (String) getAttributeInternal(INVNO);
    }

    /**
     * Sets <code>value</code> as attribute value for INV_NO using the alias name InvNo.
     * @param value value to set the INV_NO
     */
    public void setInvNo(String value) {
        setAttributeInternal(INVNO, value);
        System.out.println("Shipp Bill"+getAttributeInternal(SHIPPBILLVAL));
        System.out.println("BRC Bill  "+getAttributeInternal(BRCVAL));
        BigDecimal shippValue= new BigDecimal(0);
        BigDecimal brcValue= new BigDecimal(0);
        if(value!=null)  {
        shippValue = (BigDecimal)(getAttributeInternal(SHIPPBILLVAL)!=null?getAttributeInternal(SHIPPBILLVAL):new BigDecimal(0));
        brcValue = (BigDecimal)(getAttributeInternal(BRCVAL)!=null?getAttributeInternal(BRCVAL):new BigDecimal(0));
        try {
            if (shippValue.compareTo(brcValue) == -1)
                setAttributeInternal(VALFORINTITLE, shippValue);
            else if (brcValue.compareTo(shippValue) == -1)
                setAttributeInternal(VALFORINTITLE, shippValue);
            else if (brcValue.compareTo(shippValue) == 0)
                setAttributeInternal(VALFORINTITLE, shippValue);
        } catch (Exception e) {
            // TODO: Add catch code
            e.printStackTrace();
        }
        
        }
        
    }

    /**
     * Gets the attribute value for LAST_UPDATE_DATE using the alias name LastUpdateDate.
     * @return the LAST_UPDATE_DATE
     */
    public Timestamp getLastUpdateDate() {
        return (Timestamp) getAttributeInternal(LASTUPDATEDATE);
    }

    /**
     * Gets the attribute value for LAST_UPDATED_BY using the alias name LastUpdatedBy.
     * @return the LAST_UPDATED_BY
     */
    public String getLastUpdatedBy() {
        return (String) getAttributeInternal(LASTUPDATEDBY);
    }

    /**
     * Gets the attribute value for MEIS_ID using the alias name MeisId.
     * @return the MEIS_ID
     */
    public Long getMeisId() {
        return (Long) getAttributeInternal(MEISID);
    }

    /**
     * Sets <code>value</code> as attribute value for MEIS_ID using the alias name MeisId.
     * @param value value to set the MEIS_ID
     */
    public void setMeisId(Long value) {
        setAttributeInternal(MEISID, value);
    }

    /**
     * Gets the attribute value for MEIS_LINE_ID using the alias name MeisLineId.
     * @return the MEIS_LINE_ID
     */
    public Long getMeisLineId() {
        return (Long) getAttributeInternal(MEISLINEID);
    }

    /**
     * Sets <code>value</code> as attribute value for MEIS_LINE_ID using the alias name MeisLineId.
     * @param value value to set the MEIS_LINE_ID
     */
    public void setMeisLineId(Long value) {
        setAttributeInternal(MEISLINEID, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER, value);
    }

    /**
     * Gets the attribute value for REMARKS using the alias name Remarks.
     * @return the REMARKS
     */
    public String getRemarks() {
        return (String) getAttributeInternal(REMARKS);
    }

    /**
     * Sets <code>value</code> as attribute value for REMARKS using the alias name Remarks.
     * @param value value to set the REMARKS
     */
    public void setRemarks(String value) {
        setAttributeInternal(REMARKS, value);
    }

    /**
     * Gets the attribute value for SHIPP_BILL_DT using the alias name ShippBillDt.
     * @return the SHIPP_BILL_DT
     */
    public Date getShippBillDt() {
        return (Date) getAttributeInternal(SHIPPBILLDT);
    }

    /**
     * Sets <code>value</code> as attribute value for SHIPP_BILL_DT using the alias name ShippBillDt.
     * @param value value to set the SHIPP_BILL_DT
     */
    public void setShippBillDt(Date value) {
        setAttributeInternal(SHIPPBILLDT, value);
    }

    /**
     * Gets the attribute value for SHIPP_BILL_NO using the alias name ShippBillNo.
     * @return the SHIPP_BILL_NO
     */
    public String getShippBillNo() {
        return (String) getAttributeInternal(SHIPPBILLNO);
    }

    /**
     * Sets <code>value</code> as attribute value for SHIPP_BILL_NO using the alias name ShippBillNo.
     * @param value value to set the SHIPP_BILL_NO
     */
    public void setShippBillNo(String value) {
        setAttributeInternal(SHIPPBILLNO, value);
    }

    /**
     * Gets the attribute value for SHIPP_BILL_VAL using the alias name ShippBillVal.
     * @return the SHIPP_BILL_VAL
     */
    public BigDecimal getShippBillVal() {
        return (BigDecimal) getAttributeInternal(SHIPPBILLVAL);
    }

    /**
     * Sets <code>value</code> as attribute value for SHIPP_BILL_VAL using the alias name ShippBillVal.
     * @param value value to set the SHIPP_BILL_VAL
     */
    public void setShippBillVal(BigDecimal value) {
        setAttributeInternal(SHIPPBILLVAL, value);
    }

    /**
     * Gets the attribute value for VAL_FOR_INTITLE using the alias name ValForIntitle.
     * @return the VAL_FOR_INTITLE
     */
    public BigDecimal getValForIntitle()
    {
        System.out.println("Shipp Bill"+getAttributeInternal(SHIPPBILLVAL));
        System.out.println("BRC Bill  "+getAttributeInternal(BRCVAL));
        
        BigDecimal shippValue = (BigDecimal)(getAttributeInternal(SHIPPBILLVAL)!=null?getAttributeInternal(SHIPPBILLVAL):new BigDecimal(0));
        BigDecimal brcValue = (BigDecimal)(getAttributeInternal(BRCVAL)!=null?getAttributeInternal(BRCVAL):new BigDecimal(0));
        try {
            if (shippValue.compareTo(brcValue) == -1)
                return shippValue;
            else if (brcValue.compareTo(shippValue) == -1)
                return brcValue;
            else if (brcValue.compareTo(shippValue) == 0)
                return shippValue;
        } catch (Exception e) {
            // TODO: Add catch code
            e.printStackTrace();
        }
        
        
        return (BigDecimal) getAttributeInternal(VALFORINTITLE);
    }

    /**
     * Sets <code>value</code> as attribute value for VAL_FOR_INTITLE using the alias name ValForIntitle.
     * @param value value to set the VAL_FOR_INTITLE
     */
    public void setValForIntitle(BigDecimal value) {
        
        System.out.println("SETTER OF INITIAL RECEIVED VALUE");
        setAttributeInternal(VALFORINTITLE, value);
        System.out.println("SETTER OF INITIAL RECEIVED VALUE");
    }

    /**
     * Gets the attribute value for the calculated attribute SumOfActRecTrans.
     * @return the SumOfActRecTrans
     */
    public BigDecimal getSumOfActRecTrans() {
        return (BigDecimal) getAttributeInternal(SUMOFACTRECTRANS);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute SumOfActRecTrans.
     * @param value value to set the  SumOfActRecTrans
     */
    public void setSumOfActRecTrans(BigDecimal value) {
        setAttributeInternal(SUMOFACTRECTRANS, value);
    }

    /**
     * Gets the view accessor <code>RowSet</code> InvoiceNoMeisDetailVVO1.
     */
    public RowSet getInvoiceNoMeisDetailVVO1() {
        return (RowSet) getAttributeInternal(INVOICENOMEISDETAILVVO1);
    }
}

