package terms.fin.transaction.model.view;

import java.math.BigDecimal;

import java.sql.Timestamp;

import oracle.jbo.domain.DBSequence;
import oracle.jbo.domain.Date;
import oracle.jbo.domain.NClobDomain;
import oracle.jbo.domain.Number;
import oracle.jbo.server.EntityImpl;
import oracle.jbo.server.ViewRowImpl;

import terms.fin.transaction.model.applicationModule.FinTransactionAMImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Wed Dec 20 12:48:11 IST 2017
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class PaymtsDetailVORowImpl extends ViewRowImpl {


    public static final int ENTITY_PAYMTSDETAILEO = 0;
    public static final int ENTITY_PAYMTSEO = 1;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        AdjAmt,
        AmountPaid,
        AmountToPay,
        AmtPaid,
        ApprovalDate,
        BankCode,
        BillHold,
        Cancel,
        ChangeDate,
        ChequeNo,
        DueDate,
        EDate,
        ExRaten,
        ExRateo,
        FcAmountPaid,
        FcAmountToPay,
        FcAmtPaid,
        FinMauthActivityCode,
        FinMauthActivityId,
        FinMauthAuthCode,
        FinTpschId,
        FlcAmount,
        LoanType,
        PayId,
        PayLineId,
        PaySchNo,
        PaySchType,
        PaymtsDocDate,
        PaymtsDocNo,
        PeriodFrom,
        PeriodTo,
        SelectedOn,
        SrvNo,
        StartNo,
        UnitCode,
        VenBillDate,
        VenBillNo,
        VenCode,
        YN,
        ExRate,
        PayId1,
        TransTotalFlcAmount,
        TransRoundOff;
        static AttributesEnum[] vals = null;
        ;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int ADJAMT = AttributesEnum.AdjAmt.index();
    public static final int AMOUNTPAID = AttributesEnum.AmountPaid.index();
    public static final int AMOUNTTOPAY = AttributesEnum.AmountToPay.index();
    public static final int AMTPAID = AttributesEnum.AmtPaid.index();
    public static final int APPROVALDATE = AttributesEnum.ApprovalDate.index();
    public static final int BANKCODE = AttributesEnum.BankCode.index();
    public static final int BILLHOLD = AttributesEnum.BillHold.index();
    public static final int CANCEL = AttributesEnum.Cancel.index();
    public static final int CHANGEDATE = AttributesEnum.ChangeDate.index();
    public static final int CHEQUENO = AttributesEnum.ChequeNo.index();
    public static final int DUEDATE = AttributesEnum.DueDate.index();
    public static final int EDATE = AttributesEnum.EDate.index();
    public static final int EXRATEN = AttributesEnum.ExRaten.index();
    public static final int EXRATEO = AttributesEnum.ExRateo.index();
    public static final int FCAMOUNTPAID = AttributesEnum.FcAmountPaid.index();
    public static final int FCAMOUNTTOPAY = AttributesEnum.FcAmountToPay.index();
    public static final int FCAMTPAID = AttributesEnum.FcAmtPaid.index();
    public static final int FINMAUTHACTIVITYCODE = AttributesEnum.FinMauthActivityCode.index();
    public static final int FINMAUTHACTIVITYID = AttributesEnum.FinMauthActivityId.index();
    public static final int FINMAUTHAUTHCODE = AttributesEnum.FinMauthAuthCode.index();
    public static final int FINTPSCHID = AttributesEnum.FinTpschId.index();
    public static final int FLCAMOUNT = AttributesEnum.FlcAmount.index();
    public static final int LOANTYPE = AttributesEnum.LoanType.index();
    public static final int PAYID = AttributesEnum.PayId.index();
    public static final int PAYLINEID = AttributesEnum.PayLineId.index();
    public static final int PAYSCHNO = AttributesEnum.PaySchNo.index();
    public static final int PAYSCHTYPE = AttributesEnum.PaySchType.index();
    public static final int PAYMTSDOCDATE = AttributesEnum.PaymtsDocDate.index();
    public static final int PAYMTSDOCNO = AttributesEnum.PaymtsDocNo.index();
    public static final int PERIODFROM = AttributesEnum.PeriodFrom.index();
    public static final int PERIODTO = AttributesEnum.PeriodTo.index();
    public static final int SELECTEDON = AttributesEnum.SelectedOn.index();
    public static final int SRVNO = AttributesEnum.SrvNo.index();
    public static final int STARTNO = AttributesEnum.StartNo.index();
    public static final int UNITCODE = AttributesEnum.UnitCode.index();
    public static final int VENBILLDATE = AttributesEnum.VenBillDate.index();
    public static final int VENBILLNO = AttributesEnum.VenBillNo.index();
    public static final int VENCODE = AttributesEnum.VenCode.index();
    public static final int YN = AttributesEnum.YN.index();
    public static final int EXRATE = AttributesEnum.ExRate.index();
    public static final int PAYID1 = AttributesEnum.PayId1.index();
    public static final int TRANSTOTALFLCAMOUNT = AttributesEnum.TransTotalFlcAmount.index();
    public static final int TRANSROUNDOFF = AttributesEnum.TransRoundOff.index();

    /**
     * This is the default constructor (do not remove).
     */
    public PaymtsDetailVORowImpl() {
    }

    /**
     * Gets PaymtsDetailEO entity object.
     * @return the PaymtsDetailEO
     */
    public EntityImpl getPaymtsDetailEO() {
        return (EntityImpl) getEntity(ENTITY_PAYMTSDETAILEO);
    }

    /**
     * Gets PaymtsEO entity object.
     * @return the PaymtsEO
     */
    public EntityImpl getPaymtsEO() {
        return (EntityImpl) getEntity(ENTITY_PAYMTSEO);
    }

    /**
     * Gets the attribute value for ADJ_AMT using the alias name AdjAmt.
     * @return the ADJ_AMT
     */
    public oracle.jbo.domain.Number getAdjAmt() {

        return (oracle.jbo.domain.Number) getAttributeInternal(ADJAMT);
    }

    /**
     * Sets <code>value</code> as attribute value for ADJ_AMT using the alias name AdjAmt.
     * @param value value to set the ADJ_AMT
     */
    public void setAdjAmt(oracle.jbo.domain.Number value) {
        
        
        FinTransactionAMImpl am = (FinTransactionAMImpl) this.getApplicationModule();
        String payment_through=(String)am.getPaymtsVO().getCurrentRow().getAttribute("PaymentThrough");
        System.out.println("PaymentThrough"+payment_through);
        
        value = value != null ? value : new Number(0);
        setAttributeInternal(ADJAMT, value);
        setAmountPaid(value);
        if (value != null && value.compareTo(new Number(0)) > 0)
        {
            setYN("Y");
        }
        else if (value != null && value.compareTo(new Number(0)) == 0)
        {
            if(!payment_through.equalsIgnoreCase("A"))
            {
            setYN("N");
            }
            else
            {
                ((FinTransactionAMImpl) this.getApplicationModule()).calculateTotalBillAmount();
            }
        }
        else
        {
            if(!payment_through.equalsIgnoreCase("A"))
            {
            setYN("N");
            }
            else
            {
                ((FinTransactionAMImpl) this.getApplicationModule()).calculateTotalBillAmount();
            }
        }
       
        try
        {
            BigDecimal flc_amount=new BigDecimal(0);
            Number adjust_amount=value;
            String str_adjAmount=adjust_amount.toString();
            BigDecimal amount=new BigDecimal(str_adjAmount);
            BigDecimal curr_rate=(getExRate()!=null?getExRate():new BigDecimal(0));
            BigDecimal ex_rateo=(getExRateo()!=null?getExRateo():new BigDecimal(0));
            flc_amount=(curr_rate.subtract(ex_rateo)).multiply(amount);
            setFlcAmount(flc_amount);
        }
        catch(Exception ee)
        {
        ee.printStackTrace();
        }
        
    }

    /**
     * Gets the attribute value for AMOUNT_PAID using the alias name AmountPaid.
     * @return the AMOUNT_PAID
     */
    public oracle.jbo.domain.Number getAmountPaid() {
        return (oracle.jbo.domain.Number) getAttributeInternal(AMOUNTPAID);
    }

    /**
     * Sets <code>value</code> as attribute value for AMOUNT_PAID using the alias name AmountPaid.
     * @param value value to set the AMOUNT_PAID
     */
    public void setAmountPaid(oracle.jbo.domain.Number value) {
        if (value != null) {
            if (getLoanType() != null && getLoanType().toString().equalsIgnoreCase("D"))
                setAmtPaid(value);
            if (getLoanType() != null && getLoanType().toString().equalsIgnoreCase("C"))
                setAmtPaid(value.multiply(new Number(-1)));
        }
        setAttributeInternal(AMOUNTPAID, value);
    }

    /**
     * Gets the attribute value for AMOUNT_TO_PAY using the alias name AmountToPay.
     * @return the AMOUNT_TO_PAY
     */
    public oracle.jbo.domain.Number getAmountToPay() {
        return (oracle.jbo.domain.Number) getAttributeInternal(AMOUNTTOPAY);
    }

    /**
     * Sets <code>value</code> as attribute value for AMOUNT_TO_PAY using the alias name AmountToPay.
     * @param value value to set the AMOUNT_TO_PAY
     */
    public void setAmountToPay(oracle.jbo.domain.Number value) {
        setAttributeInternal(AMOUNTTOPAY, value);
    }

    /**
     * Gets the attribute value for AMT_PAID using the alias name AmtPaid.
     * @return the AMT_PAID
     */
    public Number getAmtPaid() {
        return (Number) getAttributeInternal(AMTPAID);
    }

    /**
     * Sets <code>value</code> as attribute value for AMT_PAID using the alias name AmtPaid.
     * @param value value to set the AMT_PAID
     */
    public void setAmtPaid(Number value) {
        setAttributeInternal(AMTPAID, value);
    }

    /**
     * Gets the attribute value for APPROVAL_DATE using the alias name ApprovalDate.
     * @return the APPROVAL_DATE
     */
    public Timestamp getApprovalDate() {
        return (Timestamp) getAttributeInternal(APPROVALDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for APPROVAL_DATE using the alias name ApprovalDate.
     * @param value value to set the APPROVAL_DATE
     */
    public void setApprovalDate(Timestamp value) {
        setAttributeInternal(APPROVALDATE, value);
    }

    /**
     * Gets the attribute value for BANK_CODE using the alias name BankCode.
     * @return the BANK_CODE
     */
    public String getBankCode() {
        return (String) getAttributeInternal(BANKCODE);
    }

    /**
     * Sets <code>value</code> as attribute value for BANK_CODE using the alias name BankCode.
     * @param value value to set the BANK_CODE
     */
    public void setBankCode(String value) {
        setAttributeInternal(BANKCODE, value);
    }

    /**
     * Gets the attribute value for BILL_HOLD using the alias name BillHold.
     * @return the BILL_HOLD
     */
    public String getBillHold() {
        return (String) getAttributeInternal(BILLHOLD);
    }

    /**
     * Sets <code>value</code> as attribute value for BILL_HOLD using the alias name BillHold.
     * @param value value to set the BILL_HOLD
     */
    public void setBillHold(String value) {
        setAttributeInternal(BILLHOLD, value);
    }

    /**
     * Gets the attribute value for CANCEL using the alias name Cancel.
     * @return the CANCEL
     */
    public String getCancel() {
        return (String) getAttributeInternal(CANCEL);
    }

    /**
     * Sets <code>value</code> as attribute value for CANCEL using the alias name Cancel.
     * @param value value to set the CANCEL
     */
    public void setCancel(String value) {
        setAttributeInternal(CANCEL, value);
    }

    /**
     * Gets the attribute value for CHANGE_DATE using the alias name ChangeDate.
     * @return the CHANGE_DATE
     */
    public Timestamp getChangeDate() {
        return (Timestamp) getAttributeInternal(CHANGEDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for CHANGE_DATE using the alias name ChangeDate.
     * @param value value to set the CHANGE_DATE
     */
    public void setChangeDate(Timestamp value) {
        setAttributeInternal(CHANGEDATE, value);
    }

    /**
     * Gets the attribute value for CHEQUE_NO using the alias name ChequeNo.
     * @return the CHEQUE_NO
     */
    public Long getChequeNo() {
        return (Long) getAttributeInternal(CHEQUENO);
    }

    /**
     * Sets <code>value</code> as attribute value for CHEQUE_NO using the alias name ChequeNo.
     * @param value value to set the CHEQUE_NO
     */
    public void setChequeNo(Long value) {
        setAttributeInternal(CHEQUENO, value);
    }

    /**
     * Gets the attribute value for DUE_DATE using the alias name DueDate.
     * @return the DUE_DATE
     */
    public Date getDueDate() {
        return (Date) getAttributeInternal(DUEDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for DUE_DATE using the alias name DueDate.
     * @param value value to set the DUE_DATE
     */
    public void setDueDate(Date value) {
        setAttributeInternal(DUEDATE, value);
    }

    /**
     * Gets the attribute value for E_DATE using the alias name EDate.
     * @return the E_DATE
     */
    public Timestamp getEDate() {
        return (Timestamp) getAttributeInternal(EDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for E_DATE using the alias name EDate.
     * @param value value to set the E_DATE
     */
    public void setEDate(Timestamp value) {
        setAttributeInternal(EDATE, value);
    }

    /**
     * Gets the attribute value for EX_RATEN using the alias name ExRaten.
     * @return the EX_RATEN
     */
    public BigDecimal getExRaten() {
        return (BigDecimal) getAttributeInternal(EXRATEN);
    }

    /**
     * Sets <code>value</code> as attribute value for EX_RATEN using the alias name ExRaten.
     * @param value value to set the EX_RATEN
     */
    public void setExRaten(BigDecimal value) {
        setAttributeInternal(EXRATEN, value);
    }

    /**
     * Gets the attribute value for EX_RATEO using the alias name ExRateo.
     * @return the EX_RATEO
     */
    public BigDecimal getExRateo() {
        return (BigDecimal) getAttributeInternal(EXRATEO);
    }

    /**
     * Sets <code>value</code> as attribute value for EX_RATEO using the alias name ExRateo.
     * @param value value to set the EX_RATEO
     */
    public void setExRateo(BigDecimal value) {
        setAttributeInternal(EXRATEO, value);
    }

    /**
     * Gets the attribute value for FC_AMOUNT_PAID using the alias name FcAmountPaid.
     * @return the FC_AMOUNT_PAID
     */
    public BigDecimal getFcAmountPaid() {
        return (BigDecimal) getAttributeInternal(FCAMOUNTPAID);
    }

    /**
     * Sets <code>value</code> as attribute value for FC_AMOUNT_PAID using the alias name FcAmountPaid.
     * @param value value to set the FC_AMOUNT_PAID
     */
    public void setFcAmountPaid(BigDecimal value) {
        setAttributeInternal(FCAMOUNTPAID, value);
    }

    /**
     * Gets the attribute value for FC_AMOUNT_TO_PAY using the alias name FcAmountToPay.
     * @return the FC_AMOUNT_TO_PAY
     */
    public BigDecimal getFcAmountToPay() {
        return (BigDecimal) getAttributeInternal(FCAMOUNTTOPAY);
    }

    /**
     * Sets <code>value</code> as attribute value for FC_AMOUNT_TO_PAY using the alias name FcAmountToPay.
     * @param value value to set the FC_AMOUNT_TO_PAY
     */
    public void setFcAmountToPay(BigDecimal value) {
        setAttributeInternal(FCAMOUNTTOPAY, value);
    }

    /**
     * Gets the attribute value for FC_AMT_PAID using the alias name FcAmtPaid.
     * @return the FC_AMT_PAID
     */
    public Number getFcAmtPaid() {
        return (Number) getAttributeInternal(FCAMTPAID);
    }

    /**
     * Sets <code>value</code> as attribute value for FC_AMT_PAID using the alias name FcAmtPaid.
     * @param value value to set the FC_AMT_PAID
     */
    public void setFcAmtPaid(Number value) {
        
        setAttributeInternal(TRANSROUNDOFF, null);
        FinTransactionAMImpl am = (FinTransactionAMImpl) this.getApplicationModule();
        String payment_through=(String)am.getPaymtsVO().getCurrentRow().getAttribute("PaymentThrough");
        BigDecimal curr_rate=(BigDecimal)am.getPaymtsVO().getCurrentRow().getAttribute("ExRate");
        Number val=value;
        System.out.println("PaymentThrough"+payment_through+" Currency Rate: "+curr_rate+"Currency Rate After Conversion: "+curr_rate.doubleValue());
        
        value = value != null ? value : new Number(0);
        if(curr_rate!=null){
         val=value.multiply(curr_rate.doubleValue());
        }
        System.out.println("Value After RoundOff: "+val.round(2));
        setAttributeInternal(ADJAMT, val.round(2));
        setAmountPaid(value);
        if (value != null && value.compareTo(new Number(0)) > 0)
        {
            setYN("Y");
        }
        else if (value != null && value.compareTo(new Number(0)) == 0)
        {
            if(!payment_through.equalsIgnoreCase("A"))
            {
            setYN("N");
            }
            else
            {
                ((FinTransactionAMImpl) this.getApplicationModule()).calculateTotalBillAmount();
            }
        }
        else
        {
            if(!payment_through.equalsIgnoreCase("A"))
            {
            setYN("N");
            }
            else
            {
                ((FinTransactionAMImpl) this.getApplicationModule()).calculateTotalBillAmount();
            }
        }
        
        try
        {
            BigDecimal flc_amount=new BigDecimal(0);
            Number adjust_amount=value;
            String str_adjAmount=adjust_amount.toString();
            BigDecimal amount=new BigDecimal(str_adjAmount);
          //  BigDecimal curr_rate=(getExRate()!=null?getExRate():new BigDecimal(0));
            BigDecimal ex_rateo=(getExRateo()!=null?getExRateo():new BigDecimal(0));
            flc_amount=(ex_rateo.subtract(curr_rate)).multiply(amount);
            setFlcAmount(flc_amount);
        }
        catch(Exception ee)
        {
        ee.printStackTrace();
        }
        setAttributeInternal(FCAMTPAID, value);
    }

    /**
     * Gets the attribute value for FIN_MAUTH_ACTIVITY_CODE using the alias name FinMauthActivityCode.
     * @return the FIN_MAUTH_ACTIVITY_CODE
     */
    public String getFinMauthActivityCode() {
        return (String) getAttributeInternal(FINMAUTHACTIVITYCODE);
    }

    /**
     * Sets <code>value</code> as attribute value for FIN_MAUTH_ACTIVITY_CODE using the alias name FinMauthActivityCode.
     * @param value value to set the FIN_MAUTH_ACTIVITY_CODE
     */
    public void setFinMauthActivityCode(String value) {
        setAttributeInternal(FINMAUTHACTIVITYCODE, value);
    }

    /**
     * Gets the attribute value for FIN_MAUTH_ACTIVITY_ID using the alias name FinMauthActivityId.
     * @return the FIN_MAUTH_ACTIVITY_ID
     */
    public Integer getFinMauthActivityId() {
        return (Integer) getAttributeInternal(FINMAUTHACTIVITYID);
    }

    /**
     * Sets <code>value</code> as attribute value for FIN_MAUTH_ACTIVITY_ID using the alias name FinMauthActivityId.
     * @param value value to set the FIN_MAUTH_ACTIVITY_ID
     */
    public void setFinMauthActivityId(Integer value) {
        setAttributeInternal(FINMAUTHACTIVITYID, value);
    }

    /**
     * Gets the attribute value for FIN_MAUTH_AUTH_CODE using the alias name FinMauthAuthCode.
     * @return the FIN_MAUTH_AUTH_CODE
     */
    public String getFinMauthAuthCode() {
        return (String) getAttributeInternal(FINMAUTHAUTHCODE);
    }

    /**
     * Sets <code>value</code> as attribute value for FIN_MAUTH_AUTH_CODE using the alias name FinMauthAuthCode.
     * @param value value to set the FIN_MAUTH_AUTH_CODE
     */
    public void setFinMauthAuthCode(String value) {
        setAttributeInternal(FINMAUTHAUTHCODE, value);
    }

    /**
     * Gets the attribute value for FIN_TPSCH_ID using the alias name FinTpschId.
     * @return the FIN_TPSCH_ID
     */
    public oracle.jbo.domain.Number getFinTpschId() {
        return (oracle.jbo.domain.Number) getAttributeInternal(FINTPSCHID);
    }

    /**
     * Sets <code>value</code> as attribute value for FIN_TPSCH_ID using the alias name FinTpschId.
     * @param value value to set the FIN_TPSCH_ID
     */
    public void setFinTpschId(oracle.jbo.domain.Number value) {
        setAttributeInternal(FINTPSCHID, value);
    }

    /**
     * Gets the attribute value for FLC_AMOUNT using the alias name FlcAmount.
     * @return the FLC_AMOUNT
     */
    public BigDecimal getFlcAmount() {
        return (BigDecimal) getAttributeInternal(FLCAMOUNT);
    }

    /**
     * Sets <code>value</code> as attribute value for FLC_AMOUNT using the alias name FlcAmount.
     * @param value value to set the FLC_AMOUNT
     */
    public void setFlcAmount(BigDecimal value) {
        setAttributeInternal(FLCAMOUNT, value);
    }

    /**
     * Gets the attribute value for LOAN_TYPE using the alias name LoanType.
     * @return the LOAN_TYPE
     */
    public String getLoanType() {
        return (String) getAttributeInternal(LOANTYPE);
    }

    /**
     * Sets <code>value</code> as attribute value for LOAN_TYPE using the alias name LoanType.
     * @param value value to set the LOAN_TYPE
     */
    public void setLoanType(String value) {
        setAttributeInternal(LOANTYPE, value);
    }

    /**
     * Gets the attribute value for PAY_ID using the alias name PayId.
     * @return the PAY_ID
     */
    public oracle.jbo.domain.Number getPayId() {
        return (oracle.jbo.domain.Number) getAttributeInternal(PAYID);
    }

    /**
     * Sets <code>value</code> as attribute value for PAY_ID using the alias name PayId.
     * @param value value to set the PAY_ID
     */
    public void setPayId(oracle.jbo.domain.Number value) {
        setAttributeInternal(PAYID, value);
    }

    /**
     * Gets the attribute value for PAY_LINE_ID using the alias name PayLineId.
     * @return the PAY_LINE_ID
     */
    public oracle.jbo.domain.Number getPayLineId() {
        return (oracle.jbo.domain.Number) getAttributeInternal(PAYLINEID);
    }

    /**
     * Sets <code>value</code> as attribute value for PAY_LINE_ID using the alias name PayLineId.
     * @param value value to set the PAY_LINE_ID
     */
    public void setPayLineId(oracle.jbo.domain.Number value) {
        setAttributeInternal(PAYLINEID, value);
    }

    /**
     * Gets the attribute value for PAY_SCH_NO using the alias name PaySchNo.
     * @return the PAY_SCH_NO
     */
    public String getPaySchNo() {
        return (String) getAttributeInternal(PAYSCHNO);
    }

    /**
     * Sets <code>value</code> as attribute value for PAY_SCH_NO using the alias name PaySchNo.
     * @param value value to set the PAY_SCH_NO
     */
    public void setPaySchNo(String value) {
        setAttributeInternal(PAYSCHNO, value);
    }

    /**
     * Gets the attribute value for PAY_SCH_TYPE using the alias name PaySchType.
     * @return the PAY_SCH_TYPE
     */
    public String getPaySchType() {
        return (String) getAttributeInternal(PAYSCHTYPE);
    }

    /**
     * Sets <code>value</code> as attribute value for PAY_SCH_TYPE using the alias name PaySchType.
     * @param value value to set the PAY_SCH_TYPE
     */
    public void setPaySchType(String value) {
        setAttributeInternal(PAYSCHTYPE, value);
    }

    /**
     * Gets the attribute value for PAYMTS_DOC_DATE using the alias name PaymtsDocDate.
     * @return the PAYMTS_DOC_DATE
     */
    public Date getPaymtsDocDate() {
        return (Date) getAttributeInternal(PAYMTSDOCDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for PAYMTS_DOC_DATE using the alias name PaymtsDocDate.
     * @param value value to set the PAYMTS_DOC_DATE
     */
    public void setPaymtsDocDate(Date value) {
        setAttributeInternal(PAYMTSDOCDATE, value);
    }

    /**
     * Gets the attribute value for PAYMTS_DOC_NO using the alias name PaymtsDocNo.
     * @return the PAYMTS_DOC_NO
     */
    public String getPaymtsDocNo() {
        return (String) getAttributeInternal(PAYMTSDOCNO);
    }

    /**
     * Sets <code>value</code> as attribute value for PAYMTS_DOC_NO using the alias name PaymtsDocNo.
     * @param value value to set the PAYMTS_DOC_NO
     */
    public void setPaymtsDocNo(String value) {
        setAttributeInternal(PAYMTSDOCNO, value);
    }

    /**
     * Gets the attribute value for PERIOD_FROM using the alias name PeriodFrom.
     * @return the PERIOD_FROM
     */
    public Timestamp getPeriodFrom() {
        return (Timestamp) getAttributeInternal(PERIODFROM);
    }

    /**
     * Sets <code>value</code> as attribute value for PERIOD_FROM using the alias name PeriodFrom.
     * @param value value to set the PERIOD_FROM
     */
    public void setPeriodFrom(Timestamp value) {
        setAttributeInternal(PERIODFROM, value);
    }

    /**
     * Gets the attribute value for PERIOD_TO using the alias name PeriodTo.
     * @return the PERIOD_TO
     */
    public Timestamp getPeriodTo() {
        return (Timestamp) getAttributeInternal(PERIODTO);
    }

    /**
     * Sets <code>value</code> as attribute value for PERIOD_TO using the alias name PeriodTo.
     * @param value value to set the PERIOD_TO
     */
    public void setPeriodTo(Timestamp value) {
        setAttributeInternal(PERIODTO, value);
    }

    /**
     * Gets the attribute value for SELECTED_ON using the alias name SelectedOn.
     * @return the SELECTED_ON
     */
    public Timestamp getSelectedOn() {
        return (Timestamp) getAttributeInternal(SELECTEDON);
    }

    /**
     * Sets <code>value</code> as attribute value for SELECTED_ON using the alias name SelectedOn.
     * @param value value to set the SELECTED_ON
     */
    public void setSelectedOn(Timestamp value) {
        setAttributeInternal(SELECTEDON, value);
    }

    /**
     * Gets the attribute value for SRV_NO using the alias name SrvNo.
     * @return the SRV_NO
     */
    public String getSrvNo() {
        return (String) getAttributeInternal(SRVNO);
    }

    /**
     * Sets <code>value</code> as attribute value for SRV_NO using the alias name SrvNo.
     * @param value value to set the SRV_NO
     */
    public void setSrvNo(String value) {
        setAttributeInternal(SRVNO, value);
    }

    /**
     * Gets the attribute value for START_NO using the alias name StartNo.
     * @return the START_NO
     */
    public Long getStartNo() {
        return (Long) getAttributeInternal(STARTNO);
    }

    /**
     * Sets <code>value</code> as attribute value for START_NO using the alias name StartNo.
     * @param value value to set the START_NO
     */
    public void setStartNo(Long value) {
        setAttributeInternal(STARTNO, value);
    }

    /**
     * Gets the attribute value for UNIT_CODE using the alias name UnitCode.
     * @return the UNIT_CODE
     */
    public String getUnitCode() {
        return (String) getAttributeInternal(UNITCODE);
    }

    /**
     * Sets <code>value</code> as attribute value for UNIT_CODE using the alias name UnitCode.
     * @param value value to set the UNIT_CODE
     */
    public void setUnitCode(String value) {
        setAttributeInternal(UNITCODE, value);
    }

    /**
     * Gets the attribute value for VEN_BILL_DATE using the alias name VenBillDate.
     * @return the VEN_BILL_DATE
     */
    public Timestamp getVenBillDate() {
        return (Timestamp) getAttributeInternal(VENBILLDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for VEN_BILL_DATE using the alias name VenBillDate.
     * @param value value to set the VEN_BILL_DATE
     */
    public void setVenBillDate(Timestamp value) {
        setAttributeInternal(VENBILLDATE, value);
    }

    /**
     * Gets the attribute value for VEN_BILL_NO using the alias name VenBillNo.
     * @return the VEN_BILL_NO
     */
    public String getVenBillNo() {
        return (String) getAttributeInternal(VENBILLNO);
    }

    /**
     * Sets <code>value</code> as attribute value for VEN_BILL_NO using the alias name VenBillNo.
     * @param value value to set the VEN_BILL_NO
     */
    public void setVenBillNo(String value) {
        setAttributeInternal(VENBILLNO, value);
    }

    /**
     * Gets the attribute value for VEN_CODE using the alias name VenCode.
     * @return the VEN_CODE
     */
    public String getVenCode() {
        return (String) getAttributeInternal(VENCODE);
    }

    /**
     * Sets <code>value</code> as attribute value for VEN_CODE using the alias name VenCode.
     * @param value value to set the VEN_CODE
     */
    public void setVenCode(String value) {
        setAttributeInternal(VENCODE, value);
    }

    /**
     * Gets the attribute value for Y_N using the alias name YN.
     * @return the Y_N
     */
    public String getYN() {
        return (String) getAttributeInternal(YN);
    }

    /**
     * Sets <code>value</code> as attribute value for Y_N using the alias name YN.
     * @param value value to set the Y_N
     */
    public void setYN(String value) {
        setAttributeInternal(YN, value);
        if (value.toString().equalsIgnoreCase("Y") && getAdjAmt().compareTo(new Number(0)) == 0)
        {
            setFcAmtPaid(getAmountToPay());
        }
        if (value.toString().equalsIgnoreCase("N"))
        {
            setAttributeInternal(FCAMTPAID, new Number(0));
            setAttributeInternal(FLCAMOUNT, new BigDecimal(0));
        }
        ((FinTransactionAMImpl) this.getApplicationModule()).calculateTotalBillAmount();
    }

    /**
     * Gets the attribute value for EX_RATE using the alias name ExRate.
     * @return the EX_RATE
     */
    public BigDecimal getExRate() {
        return (BigDecimal) getAttributeInternal(EXRATE);
    }

    /**
     * Sets <code>value</code> as attribute value for EX_RATE using the alias name ExRate.
     * @param value value to set the EX_RATE
     */
    public void setExRate(BigDecimal value) {
        setAttributeInternal(EXRATE, value);
    }

    /**
     * Gets the attribute value for PAY_ID using the alias name PayId1.
     * @return the PAY_ID
     */
    public Number getPayId1() {
        return (Number) getAttributeInternal(PAYID1);
    }

    /**
     * Sets <code>value</code> as attribute value for PAY_ID using the alias name PayId1.
     * @param value value to set the PAY_ID
     */
    public void setPayId1(Number value) {
        setAttributeInternal(PAYID1, value);
    }

    /**
     * Gets the attribute value for the calculated attribute TransTotalFlcAmount.
     * @return the TransTotalFlcAmount
     */
    public BigDecimal getTransTotalFlcAmount() {
        return (BigDecimal) getAttributeInternal(TRANSTOTALFLCAMOUNT);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute TransTotalFlcAmount.
     * @param value value to set the  TransTotalFlcAmount
     */
    public void setTransTotalFlcAmount(BigDecimal value) {
        setAttributeInternal(TRANSTOTALFLCAMOUNT, value);
    }

    /**
     * Gets the attribute value for the calculated attribute TransRoundOff.
     * @return the TransRoundOff
     */
    public String getTransRoundOff() {
        return (String) getAttributeInternal(TRANSROUNDOFF);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute TransRoundOff.
     * @param value value to set the  TransRoundOff
     */
    public void setTransRoundOff(String value) {
        Number val=(Number)getAttributeInternal(ADJAMT);
        if(value!=null && value.equals("Y")){
                System.out.println("Result in Integer: "+val.round(0));
                setAttributeInternal(ADJAMT, val.round(0));
            }
        else{
            System.out.println("In Else loop: "+val);
                FinTransactionAMImpl am = (FinTransactionAMImpl) this.getApplicationModule();
                BigDecimal curr_rate=(BigDecimal)am.getPaymtsVO().getCurrentRow().getAttribute("ExRate");
                Number val1=(Number)getAttributeInternal(FCAMTPAID);
                if(curr_rate!=null){
                 val1=val1.multiply(curr_rate.doubleValue());
                    setAttributeInternal(ADJAMT, val1.round(2));
                }
            }
        ((FinTransactionAMImpl) this.getApplicationModule()).calculateTotalBillAmount();
        setAttributeInternal(TRANSROUNDOFF, value);
    }


}

