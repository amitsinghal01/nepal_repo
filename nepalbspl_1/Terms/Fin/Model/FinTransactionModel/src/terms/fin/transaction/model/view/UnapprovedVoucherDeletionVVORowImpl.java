package terms.fin.transaction.model.view;

import java.math.BigDecimal;

import java.sql.Timestamp;

import oracle.jbo.Row;
import oracle.jbo.RowSet;
import oracle.jbo.domain.Date;
import oracle.jbo.domain.Number;
import oracle.jbo.server.ViewRowImpl;

import terms.fin.transaction.model.applicationModule.FinTransactionAMImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Mon Sep 10 11:50:14 IST 2018
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class UnapprovedVoucherDeletionVVORowImpl extends ViewRowImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        VouNo,
        VouDate,
        VouDate1,
        ControlAmount,
        SubCode,
        UnitCode,
        VouType,
        Year,
        UnitName,
        FromDate,
        ToDate,
        EditTrans,
        Delete_ALL,
        DeleteCheck2,
        UnitVO1,
        SecControlVO1;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int VOUNO = AttributesEnum.VouNo.index();
    public static final int VOUDATE = AttributesEnum.VouDate.index();
    public static final int VOUDATE1 = AttributesEnum.VouDate1.index();
    public static final int CONTROLAMOUNT = AttributesEnum.ControlAmount.index();
    public static final int SUBCODE = AttributesEnum.SubCode.index();
    public static final int UNITCODE = AttributesEnum.UnitCode.index();
    public static final int VOUTYPE = AttributesEnum.VouType.index();
    public static final int YEAR = AttributesEnum.Year.index();
    public static final int UNITNAME = AttributesEnum.UnitName.index();
    public static final int FROMDATE = AttributesEnum.FromDate.index();
    public static final int TODATE = AttributesEnum.ToDate.index();
    public static final int EDITTRANS = AttributesEnum.EditTrans.index();
    public static final int DELETE_ALL = AttributesEnum.Delete_ALL.index();
    public static final int DELETECHECK2 = AttributesEnum.DeleteCheck2.index();
    public static final int UNITVO1 = AttributesEnum.UnitVO1.index();
    public static final int SECCONTROLVO1 = AttributesEnum.SecControlVO1.index();

    /**
     * This is the default constructor (do not remove).
     */
    public UnapprovedVoucherDeletionVVORowImpl() {
    }

    /**
     * Gets the attribute value for the calculated attribute VouNo.
     * @return the VouNo
     */
    public String getVouNo() {
        return (String) getAttributeInternal(VOUNO);
    }

    /**
     * Gets the attribute value for the calculated attribute VouDate.
     * @return the VouDate
     */
    public Date getVouDate() {
        return (Date) getAttributeInternal(VOUDATE);
    }

    /**
     * Gets the attribute value for the calculated attribute VouDate1.
     * @return the VouDate1
     */
    public Timestamp getVouDate1() {
        return (Timestamp) getAttributeInternal(VOUDATE1);
    }

    /**
     * Gets the attribute value for the calculated attribute ControlAmount.
     * @return the ControlAmount
     */
    public BigDecimal getControlAmount() {
        return (BigDecimal) getAttributeInternal(CONTROLAMOUNT);
    }

    /**
     * Gets the attribute value for the calculated attribute SubCode.
     * @return the SubCode
     */
    public String getSubCode() {
        return (String) getAttributeInternal(SUBCODE);
    }

    /**
     * Gets the attribute value for the calculated attribute UnitCode.
     * @return the UnitCode
     */
    public String getUnitCode() {
        return (String) getAttributeInternal(UNITCODE);
    }

    /**
     * Gets the attribute value for the calculated attribute VouType.
     * @return the VouType
     */
    public String getVouType() {
        return (String) getAttributeInternal(VOUTYPE);
    }

    /**
     * Gets the attribute value for the calculated attribute Year.
     * @return the Year
     */
    public String getYear() {
        return (String) getAttributeInternal(YEAR);
    }

    /**
     * Gets the attribute value for the calculated attribute UnitName.
     * @return the UnitName
     */
    public String getUnitName() {
        if(getUnitCode()!=null)
        {
               
         Row[] r=this.getUnitVO1().getFilteredRows("Code", getUnitCode());
      
       if(r.length>0)
       {
          setAttributeInternal(UNITNAME, r[0].getAttribute("Name"));
           }
       
            }
        return (String) getAttributeInternal(UNITNAME);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute UnitName.
     * @param value value to set the  UnitName
     */
    public void setUnitName(String value) {
        setAttributeInternal(UNITNAME, value);
    }

    /**
     * Gets the attribute value for the calculated attribute FromDate.
     * @return the FromDate
     */
    public Date getFromDate() {
        return (Date) getAttributeInternal(FROMDATE);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute FromDate.
     * @param value value to set the  FromDate
     */
    public void setFromDate(Date value) {
        setAttributeInternal(FROMDATE, value);
    }

    /**
     * Gets the attribute value for the calculated attribute ToDate.
     * @return the ToDate
     */
    public Date getToDate() {
        return (Date) getAttributeInternal(TODATE);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute ToDate.
     * @param value value to set the  ToDate
     */
    public void setToDate(Date value) {
        setAttributeInternal(TODATE, value);
    }


    /**
     * Gets the attribute value for the calculated attribute DeleteCheck.
     * @return the DeleteCheck
     */
  


    /**
     * Gets the attribute value for the calculated attribute EditTrans.
     * @return the EditTrans
     */
    public Integer getEditTrans() {
        return (Integer) getAttributeInternal(EDITTRANS);
    }


    /**
     * Gets the attribute value for the calculated attribute Delete_ALL.
     * @return the Delete_ALL
     */
    public String getDelete_ALL() {
//        System.out.println("getAttributeInternal(DELETE_ALL)"+getAttributeInternal(DELETE_ALL));
//        System.out.println("getDeleteCheck()"+getDeleteCheck());
//        if(getAttributeInternal(DELETE_ALL).toString().equalsIgnoreCase("Y")
//        && getDeleteCheck().toString().equalsIgnoreCase("N"))
//        {
//                setAttributeInternal(DELETECHECK, "Y"); 
//            }
        return (String) getAttributeInternal(DELETE_ALL);
    }


    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute Delete_ALL.
     * @param value value to set the  Delete_ALL
     */
    public void setDelete_ALL(String value) {
//        System.out.println("getAttributeInternal(DELETE_ALL)"+value);
//      
//        if(getAttributeInternal(DELETE_ALL)!=null && getAttributeInternal(DELETE_ALL).toString().equalsIgnoreCase("Y"))
//                {
//            System.out.println("inside iffff");
//                      setDeleteCheck2("Y");
//                    }
        setAttributeInternal(DELETE_ALL, value);
    }

    /**
     * Gets the attribute value for the calculated attribute DeleteCheck2.
     * @return the DeleteCheck2
     */
    public String getDeleteCheck2() {
        return (String) getAttributeInternal(DELETECHECK2);
    }


    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute DeleteCheck2.
     * @param value value to set the  DeleteCheck2
     */
    public void setDeleteCheck2(String value) {
        setAttributeInternal(DELETECHECK2, value);
    }

    /**
     * Gets the view accessor <code>RowSet</code> UnitVO1.
     */
    public RowSet getUnitVO1() {
        return (RowSet) getAttributeInternal(UNITVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> SecControlVO1.
     */
    public RowSet getSecControlVO1() {
        return (RowSet) getAttributeInternal(SECCONTROLVO1);
    }
}

