package terms.fin.transaction.model.view.common;

import java.math.BigDecimal;

import java.sql.Timestamp;

import oracle.jbo.Row;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Thu Oct 04 10:48:41 IST 2018
// ---------------------------------------------------------------------
public interface BillOfLadingVORow extends Row {
    Timestamp getBlDt();

    void setBlDt(Timestamp value);

    String getBlNo();

    void setBlNo(String value);

    BigDecimal getCargoPackingWt();

    void setCargoPackingWt(BigDecimal value);

    BigDecimal getContainerTareWt();

    void setContainerTareWt(BigDecimal value);

    String getCreatedBy();

    Timestamp getCreationDate();

    Timestamp getDocDate();

    void setDocDate(Timestamp value);

    String getDocNo();

    void setDocNo(String value);

    Timestamp getInvDt();

    void setInvDt(Timestamp value);

    String getInvNo();

    void setInvNo(String value);

    Timestamp getLastUpdateDate();

    String getLastUpdatedBy();

    Integer getObjectVersionNumber();

    void setObjectVersionNumber(Integer value);

    String getShipingLineName();

    void setShipingLineName(String value);

    BigDecimal getVgm();

    void setVgm(BigDecimal value);

    Timestamp getWeightmentSlipDt();

    void setWeightmentSlipDt(Timestamp value);

    String getWeightmentSlipNo();

    void setWeightmentSlipNo(String value);
}

