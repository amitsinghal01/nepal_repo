package terms.fin.transaction.model.view.common;

import java.math.BigDecimal;

import oracle.jbo.Row;
import oracle.jbo.domain.Date;
import oracle.jbo.domain.Timestamp;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Tue Oct 16 12:35:29 IST 2018
// ---------------------------------------------------------------------
public interface RtgsDetailVORow extends Row {
    String getCreatedBy();

    Timestamp getCreationDate();

    BigDecimal getDocAmt();

    void setDocAmt(BigDecimal param);


    Long getDocId();

    void setDocId(Long param);

    Long getDocLineId();

    void setDocLineId(Long param);

    String getDocNo();

    void setDocNo(String param);

    String getGlCd();

    void setGlCd(String param);

    Timestamp getLastUpdateDate();

    String getLastUpdatedBy();

    Integer getObjectVersionNumber();

    void setObjectVersionNumber(Integer param);

    String getPayType();

    void setPayType(String param);

    Date getPaymentDocDate();

    void setPaymentDocDate(Date param);

    String getPaymentDocNo();

    void setPaymentDocNo(String param);

    String getSubCode();

    void setSubCode(String param);

    String getSubType();

    void setSubType(String param);

    Date getDocDate();

    void setDocDate(Date value);


}

