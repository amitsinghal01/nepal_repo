package terms.fin.transaction.model.view.common;

import java.math.BigDecimal;

import oracle.jbo.Row;
import oracle.jbo.domain.Date;
import oracle.jbo.domain.Timestamp;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Wed Oct 24 11:18:23 IST 2018
// ---------------------------------------------------------------------
public interface SearchRtgsHeaderVORow extends Row {
    Timestamp getApprovalDate();

    void setApprovalDate(Timestamp value);

    String getBankCode();

    void setBankCode(String value);

    Timestamp getChequeDate();

    void setChequeDate(Timestamp value);

    String getChequeNo();

    void setChequeNo(String value);

    String getCheques();

    void setCheques(String value);

    String getChkBook();

    void setChkBook(String value);

    String getCrGlCode();

    void setCrGlCode(String value);

    String getCreatedBy();

    Timestamp getCreationDate();

    BigDecimal getDocAmount();

    void setDocAmount(BigDecimal value);

    Date getDocDate();

    void setDocDate(Date value);

    Long getDocId();

    void setDocId(Long value);

    String getDocNo();

    void setDocNo(String value);

    String getFavourOf();

    void setFavourOf(String value);

    String getFinMauthAuthCode();

    void setFinMauthAuthCode(String value);

    String getFinTvouchVouNo();

    void setFinTvouchVouNo(String value);

    Timestamp getLastUpdateDate();

    String getLastUpdatedBy();

    Integer getObjectVersionNumber();

    void setObjectVersionNumber(Integer value);

    String getPaymentThrough();

    void setPaymentThrough(String value);

    String getPreparedBy();

    void setPreparedBy(String value);

    String getRemarks();

    void setRemarks(String value);

    String getUnitCode();

    void setUnitCode(String value);

    String getBankName();

    void setBankName(String value);

    Long getBankId();

    void setBankId(Long value);

    Integer getObjectVersionNumber1();

    void setObjectVersionNumber1(Integer value);

    String getEmpFirstName();

    void setEmpFirstName(String value);

    String getEmpNumber();

    void setEmpNumber(String value);

    Integer getObjectVersionNumber2();

    void setObjectVersionNumber2(Integer value);

    String getEmpLastName();

    void setEmpLastName(String value);

    String getEmpFirstName1();

    void setEmpFirstName1(String value);

    String getEmpNumber1();

    void setEmpNumber1(String value);

    Integer getObjectVersionNumber3();

    void setObjectVersionNumber3(Integer value);

    String getEmpLastName1();

    void setEmpLastName1(String value);
}

