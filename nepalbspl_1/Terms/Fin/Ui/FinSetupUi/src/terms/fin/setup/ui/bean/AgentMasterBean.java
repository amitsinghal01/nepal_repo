package terms.fin.setup.ui.bean;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.event.DialogEvent;


public class AgentMasterBean {
    private String editAction="V";
    private RichOutputText bindingOutputText;

    public AgentMasterBean() {
    }

//    public void SaveAL(ActionEvent actionEvent) {
//        ADFUtils.findOperation("CreateInsert").execute();
//       ADFUtils.findOperation("getAgentCodeNo").execute();
//    }

    public void CreateAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert").execute();
        ADFUtils.findOperation("getAgentCodeNo").execute();
    }

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        return bindingOutputText;
    }

    public void deleteDialogDL(DialogEvent dialogEvent) {
        Integer i=0;
                i=(Integer)bindingOutputText.getValue();
                System.out.println("EDIT TRANS VALUE"+i);
                   if(i.equals(0))
                       {
                          FacesMessage Message = new FacesMessage("Record Save Successfully."); 
                          Message.setSeverity(FacesMessage.SEVERITY_INFO); 
                          FacesContext fc = FacesContext.getCurrentInstance(); 
                          fc.addMessage(null, Message);    
                         
                       }
                       else
                       {
                           FacesMessage Message = new FacesMessage("Record Updated Successfully."); 
                           Message.setSeverity(FacesMessage.SEVERITY_INFO); 
                           FacesContext fc = FacesContext.getCurrentInstance(); 
                           fc.addMessage(null, Message);    
                       
           
    }
}
}