package terms.fin.setup.ui.bean;

import javax.faces.application.FacesMessage;

import javax.faces.context.FacesContext;

import oracle.adf.model.OperationBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;


public class AssetCategoryBean {
    private RichOutputText bindingOutputText;
    private String editAction="V";
    private RichTable assetCategoryTableBinding;

    public AssetCategoryBean() {
    }

    public void deletePopupDL(DialogEvent dialogEvent) {
        
    if(dialogEvent.getOutcome().name().equals("ok"))
    {
    OperationBinding op = null;
    ADFUtils.findOperation("Delete").execute();
    op = (OperationBinding) ADFUtils.findOperation("Commit");
    if (getEditAction().equals("V")) {
    Object rst = op.execute();
    }
    System.out.println("Record Delete Successfully");
    if(op.getErrors().isEmpty()){
    FacesMessage Message = new FacesMessage("Record Deleted Successfully");
    Message.setSeverity(FacesMessage.SEVERITY_INFO);
    FacesContext fc = FacesContext.getCurrentInstance();
    fc.addMessage(null, Message);
    } else if (!op.getErrors().isEmpty())
    {
    OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
    Object rstr = opr.execute();
    FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
    Message.setSeverity(FacesMessage.SEVERITY_ERROR);
    FacesContext fc = FacesContext.getCurrentInstance();
    fc.addMessage(null, Message);
    }
    }
    AdfFacesContext.getCurrentInstance().addPartialTarget(assetCategoryTableBinding);

    }

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }


//               Integer i=0;
//                       i=(Integer)bindingOutputText.getValue();
//                       System.out.println("EDIT TRANS VALUE"+i);
//                          if(i.equals(0))
//                              {
//                                 FacesMessage Message = new FacesMessage("Record Save Successfully."); 
//                                 Message.setSeverity(FacesMessage.SEVERITY_INFO); 
//                                 FacesContext fc = FacesContext.getCurrentInstance(); 
//                                 fc.addMessage(null, Message);    
//                                
//                              }
//                              else
//                              {
//                                  FacesMessage Message = new FacesMessage("Record Updated Successfully."); 
//                                  Message.setSeverity(FacesMessage.SEVERITY_INFO); 
//                                  FacesContext fc = FacesContext.getCurrentInstance(); 
//                                  fc.addMessage(null, Message);    
//                              
//                  
//               }
//               }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        return bindingOutputText;
    }

//    public void setEditAction(String editAction) {
//        this.editAction = editAction;
//    }
//
//    public String getEditAction() {
//        return editAction;
//    }


    public void setAssetCategoryTableBinding(RichTable assetCategoryTableBinding) {
        this.assetCategoryTableBinding = assetCategoryTableBinding;
    }

    public RichTable getAssetCategoryTableBinding() {
        return assetCategoryTableBinding;
    }
}
