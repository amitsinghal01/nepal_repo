package terms.fin.setup.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import javax.faces.validator.ValidatorException;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;

import oracle.adf.view.rich.component.rich.nav.RichButton;

import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

public class AssetEntryBean {
    private RichPanelHeader getMyPageRootCompenent;
    private RichInputText assetNoBinding;
    private RichInputComboboxListOfValues unitBinding;
    private RichButton editBinding;
    private RichInputText purValBinding;
    private RichInputText depMethodBinding;
    private RichInputText locBinding;
    private RichInputText descBinding;
    private RichInputText modVatbinding;
    private RichInputText openDpBinding;
    private RichInputText assetrefNoBinding;
    private RichInputDate capDateBinding;
    private RichInputDate datePurBinding;
    private RichInputText rateBinding;
    private RichInputText poNoBinding;
    private RichInputText vendBinding;
    private RichInputText venNameBinding;
    private RichInputComboboxListOfValues glCdBinding;
    private RichInputComboboxListOfValues costCentreBinding;
    private RichInputText ccDescBinding;
    private RichInputText invNoBinding;
    private RichInputDate invdateBinding;
    private RichInputDate instdateBinding;
    private RichInputComboboxListOfValues assetCtgBinding;
    private RichInputText ctgDescBinding;
    private RichOutputText bindingOutputText;
    private RichSelectOneChoice depTypeBinding;
    private RichInputComboboxListOfValues approvebybinding;
    private RichInputComboboxListOfValues preparebybinding;
    private RichInputText empNameBinding;
    private RichInputComboboxListOfValues itemGroupBinding;

    public AssetEntryBean() {
    }

    public void saveButtonAL(ActionEvent actionEvent) {
        String empCd=(String)ADFUtils.resolveExpression("#{pageFlowScope.empCode}");
        empCd=empCd!=null?empCd:"Admin";
        ADFUtils.setColumnValue("AssetEntryVO1Iterator","LastUpdatedBy",empCd);
       
                if (assetNoBinding.getValue()==null) {
                    OperationBinding op = ADFUtils.findOperation("getAssetNumber");
                            Object rst = op.execute();
                    ADFUtils.findOperation("Commit").execute();
                    ADFUtils.showMessage("Record Saved Successfully" + "Asset No is::"+ rst, 2);
                }
            
            
           
              else {
                    ADFUtils.findOperation("Commit").execute();
                    ADFUtils.showMessage(" Record Updated Successfully.", 2); 
              }
        
    }

    public void setGetMyPageRootCompenent(RichPanelHeader getMyPageRootCompenent) {
        this.getMyPageRootCompenent = getMyPageRootCompenent;
    }

    public RichPanelHeader getGetMyPageRootCompenent() {
        return getMyPageRootCompenent;
    }

    public void setAssetNoBinding(RichInputText assetNoBinding) {
        this.assetNoBinding = assetNoBinding;
    }

    public RichInputText getAssetNoBinding() {
        return assetNoBinding;
    }

    public void setUnitBinding(RichInputComboboxListOfValues unitBinding) {
        this.unitBinding = unitBinding;
    }

    public RichInputComboboxListOfValues getUnitBinding() {
        return unitBinding;
    }

    public void setEditBinding(RichButton editBinding) {
        this.editBinding = editBinding;
    }

    public RichButton getEditBinding() {
        return editBinding;
    }

    public void setPurValBinding(RichInputText purValBinding) {
        this.purValBinding = purValBinding;
    }

    public RichInputText getPurValBinding() {
        return purValBinding;
    }

    public void setDepMethodBinding(RichInputText depMethodBinding) {
        this.depMethodBinding = depMethodBinding;
    }

    public RichInputText getDepMethodBinding() {
        return depMethodBinding;
    }

    public void setLocBinding(RichInputText locBinding) {
        this.locBinding = locBinding;
    }

    public RichInputText getLocBinding() {
        return locBinding;
    }

    public void setDescBinding(RichInputText descBinding) {
        this.descBinding = descBinding;
    }

    public RichInputText getDescBinding() {
        return descBinding;
    }

    public void setModVatbinding(RichInputText modVatbinding) {
        this.modVatbinding = modVatbinding;
    }

    public RichInputText getModVatbinding() {
        return modVatbinding;
    }

    public void setOpenDpBinding(RichInputText openDpBinding) {
        this.openDpBinding = openDpBinding;
    }

    public RichInputText getOpenDpBinding() {
        return openDpBinding;
    }

    public void setAssetrefNoBinding(RichInputText assetrefNoBinding) {
        this.assetrefNoBinding = assetrefNoBinding;
    }

    public RichInputText getAssetrefNoBinding() {
        return assetrefNoBinding;
    }

    public void setCapDateBinding(RichInputDate capDateBinding) {
        this.capDateBinding = capDateBinding;
    }

    public RichInputDate getCapDateBinding() {
        return capDateBinding;
    }

    public void setDatePurBinding(RichInputDate datePurBinding) {
        this.datePurBinding = datePurBinding;
    }

    public RichInputDate getDatePurBinding() {
        return datePurBinding;
    }

    public void setRateBinding(RichInputText rateBinding) {
        this.rateBinding = rateBinding;
    }

    public RichInputText getRateBinding() {
        return rateBinding;
    }

    public void setPoNoBinding(RichInputText poNoBinding) {
        this.poNoBinding = poNoBinding;
    }

    public RichInputText getPoNoBinding() {
        return poNoBinding;
    }

    public void setVendBinding(RichInputText vendBinding) {
        this.vendBinding = vendBinding;
    }

    public RichInputText getVendBinding() {
        return vendBinding;
    }

    public void setVenNameBinding(RichInputText venNameBinding) {
        this.venNameBinding = venNameBinding;
    }

    public RichInputText getVenNameBinding() {
        return venNameBinding;
    }

    public void setGlCdBinding(RichInputComboboxListOfValues glCdBinding) {
        this.glCdBinding = glCdBinding;
    }

    public RichInputComboboxListOfValues getGlCdBinding() {
        return glCdBinding;
    }

    public void setCostCentreBinding(RichInputComboboxListOfValues costCentreBinding) {
        this.costCentreBinding = costCentreBinding;
    }

    public RichInputComboboxListOfValues getCostCentreBinding() {
        return costCentreBinding;
    }

    public void setCcDescBinding(RichInputText ccDescBinding) {
        this.ccDescBinding = ccDescBinding;
    }

    public RichInputText getCcDescBinding() {
        return ccDescBinding;
    }

    public void setInvNoBinding(RichInputText invNoBinding) {
        this.invNoBinding = invNoBinding;
    }

    public RichInputText getInvNoBinding() {
        return invNoBinding;
    }

    public void setInvdateBinding(RichInputDate invdateBinding) {
        this.invdateBinding = invdateBinding;
    }

    public RichInputDate getInvdateBinding() {
        return invdateBinding;
    }

    public void setInstdateBinding(RichInputDate instdateBinding) {
        this.instdateBinding = instdateBinding;
    }

    public RichInputDate getInstdateBinding() {
        return instdateBinding;
    }

    public void setAssetCtgBinding(RichInputComboboxListOfValues assetCtgBinding) {
        this.assetCtgBinding = assetCtgBinding;
    }

    public RichInputComboboxListOfValues getAssetCtgBinding() {
        return assetCtgBinding;
    }

    public void setCtgDescBinding(RichInputText ctgDescBinding) {
        this.ctgDescBinding = ctgDescBinding;
    }

    public RichInputText getCtgDescBinding() {
        return ctgDescBinding;
    }
    private void cevmodecheck(){
          if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
              makeComponentHierarchyReadOnly(this.getGetMyPageRootCompenent(), true);
              cevModeDisableComponent("V");
          }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
              makeComponentHierarchyReadOnly(this.getGetMyPageRootCompenent(), false);
              cevModeDisableComponent("C");
          }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
              makeComponentHierarchyReadOnly(this.getGetMyPageRootCompenent(), false);
              cevModeDisableComponent("E");
          }
      }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
               try {
                   Method method1 =
                       component.getClass().getMethod("setDisabled", boolean.class);
                   if (method1 != null) {
                       method1.invoke(component, valueComponent);
                   }
               } catch (NoSuchMethodException e) {
                   try {
                       Method method =
                           component.getClass().getMethod("setReadOnly", boolean.class);
                       if (method != null) {
                           method.invoke(component, valueComponent);
                       }
                   } catch (Exception e1) {
                       // e.printStackTrace();//silently eat this exception.
                   }


               } catch (Exception e) {
                   // e.printStackTrace();//silently eat this exception.
               }
               List<UIComponent> childComponents = component.getChildren();
               for (UIComponent comp : childComponents) {
                   makeComponentHierarchyReadOnly(comp, valueComponent);
               }
           }
    
    
    public void cevModeDisableComponent(String mode) {
        
        if(mode.equals("C")){
            getUnitBinding().setDisabled(true);
            getAssetNoBinding().setDisabled(true);
            //getDepTypeBinding().setDisabled(true);
            getApprovebybinding().setDisabled(true);
            getPreparebybinding().setDisabled(true);
            getEmpNameBinding().setDisabled(true);
        }
        if(mode.equals("E")){
            getUnitBinding().setDisabled(true);
            //getDepTypeBinding().setDisabled(true);
            getEditBinding().setDisabled(true);
            getAssetNoBinding().setDisabled(true);
            getApprovebybinding().setDisabled(false);
            getPreparebybinding().setDisabled(true);
            getEmpNameBinding().setDisabled(true);
            
        }
        if(mode.equals("V")){
            
            
            
        }
        
        
        }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void EditButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void depMethodVCL(ValueChangeEvent vce) {
      vce.getComponent().processUpdates(FacesContext.getCurrentInstance());  
      System.out.println("code: "+vce.getNewValue()+" category: "+itemGroupBinding.getValue());
      OperationBinding op= ADFUtils.findOperation("calculateRateAssetEntry");
      op.getParamsMap().put("code", (String)depTypeBinding.getValue());
      op.getParamsMap().put("category", (String)itemGroupBinding.getValue());
      op.execute();
    }

    public void setDepTypeBinding(RichSelectOneChoice depTypeBinding) {
        this.depTypeBinding = depTypeBinding;
    }

    public RichSelectOneChoice getDepTypeBinding() {
        return depTypeBinding;
    }

    public void purchaseValueVCL(ValueChangeEvent valueChangeEvent) {
        openDpBinding.resetValue();
        AdfFacesContext.getCurrentInstance().addPartialTarget(openDpBinding);
    }

    public void openingDepVCL(ValueChangeEvent valueChangeEvent) {
        if(valueChangeEvent!=null){
        BigDecimal openDep=(BigDecimal)valueChangeEvent.getNewValue();
        BigDecimal purVal=(BigDecimal)purValBinding.getValue();
        if(openDep.compareTo(purVal)==1)
        {
                FacesMessage message = new FacesMessage("Opening Dep. must be less than or equal to Purchase Value.");
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(openDpBinding.getClientId(), message);
            }
        }
    }

    public void ApproveByVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());            
                     OperationBinding ob = ADFUtils.findOperation("checkApprovalStatus");
                    ob.getParamsMap().put("formNm", "AE");
                    ob.getParamsMap().put("authoLim", "AP");
                    ob.getParamsMap().put("empcd", vce.getNewValue().toString());
                    ob.execute();
                    System.out.println(" EMP CD"+vce.getNewValue().toString());
                    if((ob.getResult() !=null && ob.getResult().equals("Y")))
                    {
                        Row row=(Row)ADFUtils.evaluateEL("#{bindings.AssetEntryVO1Iterator.currentRow}");
                        row.setAttribute("Approveby", null);
                        ADFUtils.showMessage("You Don't Have Permission To Approve This Record.",0);
                    }

        

        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        }


    public void setApprovebybinding(RichInputComboboxListOfValues approvebybinding) {
        this.approvebybinding = approvebybinding;
    }

    public RichInputComboboxListOfValues getApprovebybinding() {
        return approvebybinding;
    }

    public void setPreparebybinding(RichInputComboboxListOfValues preparebybinding) {
        this.preparebybinding = preparebybinding;
    }

    public RichInputComboboxListOfValues getPreparebybinding() {
        return preparebybinding;
    }

    public void setEmpNameBinding(RichInputText empNameBinding) {
        this.empNameBinding = empNameBinding;
    }

    public RichInputText getEmpNameBinding() {
        return empNameBinding;
    }

    public void setItemGroupBinding(RichInputComboboxListOfValues itemGroupBinding) {
        this.itemGroupBinding = itemGroupBinding;
    }

    public RichInputComboboxListOfValues getItemGroupBinding() {
        return itemGroupBinding;
    }
}
