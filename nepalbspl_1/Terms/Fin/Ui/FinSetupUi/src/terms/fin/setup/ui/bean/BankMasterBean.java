package terms.fin.setup.ui.bean;

import java.lang.reflect.Method;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.validator.ValidatorException;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

public class BankMasterBean {
    private RichInputText bindEndingNo;
    private RichOutputText bindingOutputText;
    private RichPanelGroupLayout getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichButton detailcreateBinding;
    private RichButton detaildeleteBinding;
    private RichInputComboboxListOfValues glCodeBinding;
    private RichTable detailTableBinding;
    private String Message="CM";
    private RichInputText bankCodeBinding;
    private RichInputText pindCodeBinding;
    private RichInputDate approvalDateBinding;
    private RichInputComboboxListOfValues authorityCodeBinding;
    private RichInputComboboxListOfValues unitCodeBinding;

    public BankMasterBean() {
    }

    public void setBindEndingNo(RichInputText bindEndingNo) {
        this.bindEndingNo = bindEndingNo;
    }

    public RichInputText getBindEndingNo() {
        return bindEndingNo;
    }

    public void bankCodeValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (object != null && object.toString().length() > 0) {

            //System.out.println("Value===> "+object+"  And Length ====> "+object.toString().length());
            if (object.toString().length() == 4) {

            } else {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                              "Bank Code Should Be In 4 Characters", null));
            }

        }

    }

    public void bankNameValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (object != null && object.toString().trim().length() > 0) {
            if (object.toString().contains(",")) 
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                              "Please check bank name, comma is not be used", null));
         }

    }

    public void startingNoValidator(FacesContext facesContext, UIComponent uIComponent, Object object) 
    {
        if (object != null && getBindEndingNo().getValue() != null && object.toString().trim().length() > 0 &&
            getBindEndingNo().getValue().toString().trim().length() > 0) 
        {
            if (((Long) getBindEndingNo().getValue()).compareTo((Long) object) == -1)
            {
                System.out.println("**********ERROR***********");
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                              "Starting No. can not be greater then Ending No.", null));
            }
         }

    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText()
    {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelGroupLayout getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelGroupLayout getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setDetailcreateBinding(RichButton detailcreateBinding) {
        this.detailcreateBinding = detailcreateBinding;
    }

    public RichButton getDetailcreateBinding() {
        return detailcreateBinding;
    }

    public void setDetaildeleteBinding(RichButton detaildeleteBinding) {
        this.detaildeleteBinding = detaildeleteBinding;
    }

    public RichButton getDetaildeleteBinding() {
        return detaildeleteBinding;
    }
    
    private void cevmodecheck(){
          if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
              makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
              cevModeDisableComponent("V");
          }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
              makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
              cevModeDisableComponent("C");
          }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
              makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
              cevModeDisableComponent("E");
          }
      }
    
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
               try {
                   Method method1 =
                       component.getClass().getMethod("setDisabled", boolean.class);
                   if (method1 != null) {
                       method1.invoke(component, valueComponent);
                   }
               } catch (NoSuchMethodException e) {
                   try {
                       Method method =
                           component.getClass().getMethod("setReadOnly", boolean.class);
                       if (method != null) {
                           method.invoke(component, valueComponent);
                       }
                   } catch (Exception e1) {
                       // e.printStackTrace();//silently eat this exception.
                   }


               } catch (Exception e) {
                   // e.printStackTrace();//silently eat this exception.
               }
               List<UIComponent> childComponents = component.getChildren();
               for (UIComponent comp : childComponents) {
                   makeComponentHierarchyReadOnly(comp, valueComponent);
               }
           }
    
    
    public void cevModeDisableComponent(String mode) {
            if (mode.equals("E"))
            {
            getHeaderEditBinding().setDisabled(true);
            getDetailcreateBinding().setDisabled(false);
            getDetaildeleteBinding().setDisabled(false);
            getHeaderEditBinding().setDisabled(true);
            getGlCodeBinding().setDisabled(true);
            Message="EM";
            getBankCodeBinding().setDisabled(true);
            getPindCodeBinding().setDisabled(true);
            getApprovalDateBinding().setDisabled(true);
            getAuthorityCodeBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
           }
           else if (mode.equals("C")) 
           {
            getDetailcreateBinding().setDisabled(false);
            getDetaildeleteBinding().setDisabled(false);
            getHeaderEditBinding().setDisabled(true);
            getPindCodeBinding().setDisabled(true);
            getApprovalDateBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
            getAuthorityCodeBinding().setDisabled(true);


           } 
           else if (mode.equals("V")) 
           {
           getDetailcreateBinding().setDisabled(true);
           getDetaildeleteBinding().setDisabled(true);
           }
           
       }


    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setGlCodeBinding(RichInputComboboxListOfValues glCodeBinding) {
        this.glCodeBinding = glCodeBinding;
    }

    public RichInputComboboxListOfValues getGlCodeBinding() {
        return glCodeBinding;
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                    {
                    ADFUtils.findOperation("Delete").execute();
                    System.out.println("Record Delete Successfully");
                    FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                    FacesContext fc = FacesContext.getCurrentInstance();  
                    fc.addMessage(null, Message);  
                    }

              AdfFacesContext.getCurrentInstance().addPartialTarget(detailTableBinding);
    }

    public void setDetailTableBinding(RichTable detailTableBinding) {
        this.detailTableBinding = detailTableBinding;
    }

    public RichTable getDetailTableBinding() {
        return detailTableBinding;
    }

    public void saveButtonAL(ActionEvent actionEvent) 
    {
        String empCd=(String)ADFUtils.resolveExpression("#{pageFlowScope.empCode}");
        empCd=empCd!=null?empCd:"Admin";
        ADFUtils.setColumnValue("BankMasterHeaderVO1Iterator","LastUpdatedBy",empCd);
        if(Message.equalsIgnoreCase("CM"))
        {
            ADFUtils.findOperation("Commit").execute();
            ADFUtils.showMessage("Record Saved Successfully.New Bank Code is "+bankCodeBinding.getValue(), 2);
            Message="EM";
        }
        else if(Message.equalsIgnoreCase("EM"))
        {
            ADFUtils.findOperation("Commit").execute();
            ADFUtils.showMessage("Record Updated Successfully.", 2);
        }
    }

    public void setBankCodeBinding(RichInputText bankCodeBinding) {
        this.bankCodeBinding = bankCodeBinding;
    }

    public RichInputText getBankCodeBinding() {
        return bankCodeBinding;
    }

    public void setPindCodeBinding(RichInputText pindCodeBinding) {
        this.pindCodeBinding = pindCodeBinding;
    }

    public RichInputText getPindCodeBinding() {
        return pindCodeBinding;
    }

    public void setApprovalDateBinding(RichInputDate approvalDateBinding) {
        this.approvalDateBinding = approvalDateBinding;
    }

    public RichInputDate getApprovalDateBinding() {
        return approvalDateBinding;
    }

    public void setAuthorityCodeBinding(RichInputComboboxListOfValues authorityCodeBinding) {
        this.authorityCodeBinding = authorityCodeBinding;
    }

    public RichInputComboboxListOfValues getAuthorityCodeBinding() {
        return authorityCodeBinding;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }
}
