package terms.fin.setup.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.Calendar;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.validator.ValidatorException;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.domain.Timestamp;

public class BillWiseOpeningBean {
    private RichColumn ledgerCodeTableBinding;
    private RichTable deleteBillWiseOpeningTableBinding;
    private RichInputText ledgerCodeTableBinding1;
    private RichInputComboboxListOfValues subLedgerCodeTableBinding;
    private RichInputText bindAmountFC;
    private RichInputText bindCurrRate;
    private RichInputText bindAmountLC;
    private RichInputDate refDateBinding;
    private RichInputDate paymentDueDateBinding;
    private RichInputText dayBinding;
    private RichOutputText bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichButton detailCreateBinding;
    private RichInputText subLedgerNameBinding;
    private RichInputComboboxListOfValues ledgerCodeBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText refNumBinding;
    private RichInputText debitAmountBinding;
    private RichInputText creditAmountBinding;
    private RichInputText differenceAmtBinding;
    private RichInputText bindPartyType;

    public BillWiseOpeningBean() {
    }

    public void setLedgerCodeTableBinding(RichColumn ledgerCodeTableBinding) {
        this.ledgerCodeTableBinding = ledgerCodeTableBinding;
    }

    public RichColumn getLedgerCodeTableBinding() {
        return ledgerCodeTableBinding;
    }

    public void subLedgerCodeValidate(FacesContext facesContext, UIComponent uIComponent, Object object) {
      
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
            if (dialogEvent.getOutcome().name().equals("ok")) {
                ADFUtils.findOperation("Delete").execute();
                System.out.println("Record Delete Successfully");

                FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);

            }
                AdfFacesContext.getCurrentInstance().addPartialTarget(deleteBillWiseOpeningTableBinding);
    }

    public void setDeleteBillWiseOpeningTableBinding(RichTable deleteBillWiseOpeningTableBinding) {
        this.deleteBillWiseOpeningTableBinding = deleteBillWiseOpeningTableBinding;
    }

    public RichTable getDeleteBillWiseOpeningTableBinding() {
        return deleteBillWiseOpeningTableBinding;
    }

    public void setLedgerCodeTableBinding1(RichInputText ledgerCodeTableBinding1) {
        this.ledgerCodeTableBinding1 = ledgerCodeTableBinding1;
    }

    public RichInputText getLedgerCodeTableBinding1() {
        return ledgerCodeTableBinding1;
    }
    
    public void setSubLedgerCodeTableBinding(RichInputComboboxListOfValues subLedgerCodeTableBinding) {
        this.subLedgerCodeTableBinding = subLedgerCodeTableBinding;
    }

    public RichInputComboboxListOfValues getSubLedgerCodeTableBinding() {
        return subLedgerCodeTableBinding;
    }
    
    public void ledgerCodeValidator(FacesContext facesContext, UIComponent uIComponent, Object object) 
    {
    if(subLedgerCodeTableBinding.getValue() !=null && object.toString().isEmpty())
    {
        throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "CHECK LEDGER CODE FOR THIS PARTY...", null));

    }

    }


    public void setBindAmountFC(RichInputText bindAmountFC) {
        this.bindAmountFC = bindAmountFC;
    }

    public RichInputText getBindAmountFC() {
        return bindAmountFC;
    }

    public void setBindCurrRate(RichInputText bindCurrRate) {
        this.bindCurrRate = bindCurrRate;
    }

    public RichInputText getBindCurrRate() {
        return bindCurrRate;
    }

    public void setBindAmountLC(RichInputText bindAmountLC) {
        this.bindAmountLC = bindAmountLC;
    }

    public RichInputText getBindAmountLC() {
        return bindAmountLC;
    }

    public void calculatedAmount(ValueChangeEvent valueChangeEvent) {
        if(valueChangeEvent!=null &&  bindAmountFC.getValue()!=null && bindCurrRate.getValue()!=null)
        {
                BigDecimal AmountFc=(BigDecimal)bindAmountFC.getValue();  
                BigDecimal CurrRate=(BigDecimal)bindCurrRate.getValue();
                BigDecimal AmountLc=AmountFc.multiply(CurrRate);
                bindAmountLC.setValue(AmountLc);
                
        }
    }
    
    /*
     *
     *     Timestamp ts=new Timestamp(System.currentTimeMillis());
    if(getInvDate()!=null)
    {
    ts=getInvDate();
    System.out.println("Timestamp is=======>TS "+ts);
    Calendar cal=Calendar.getInstance();
    cal.setTimeInMillis(ts.getTime());
    cal.add(Calendar.DAY_OF_MONTH, getCrDays().intValue());
    Timestamp ts1=new Timestamp(cal.getTime().getTime());
    System.out.println("Timestamp is=======>TS1  "+ts1);
    setPaymntDueDt(ts1);
    //return ts1;
    }
     *
     *
     */


    public void daysVCE(ValueChangeEvent vce) 
    {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
            if(vce!=null)
            {
                //System.out.println("Before VCE =========>");
                BigDecimal BgDec=(BigDecimal) dayBinding.getValue();
                //System.out.println("Value of Before dayBinding ====>" + dayBinding.getValue());
                int days=BgDec.intValue();
                //System.out.println("Value of After dayBinding ====>" + dayBinding.getValue());
                Timestamp RefDate=(Timestamp)refDateBinding.getValue();
                Calendar cal=Calendar.getInstance();
                cal.setTimeInMillis(RefDate.getTime());
                cal.add(Calendar.DAY_OF_MONTH,days);
                Timestamp PaymentDate=new Timestamp(cal.getTime().getTime());
                //System.out.println("TimesStamp########==> "+PaymentDate);
                paymentDueDateBinding.setValue(PaymentDate);   
                //System.out.println("After VCE =========>"+paymentDueDateBinding.getValue());
            }
    }

    public void setRefDateBinding(RichInputDate refDateBinding) {
        this.refDateBinding = refDateBinding;
    }

    public RichInputDate getRefDateBinding() {
        return refDateBinding;
    }

    public void setPaymentDueDateBinding(RichInputDate paymentDueDateBinding) {
        this.paymentDueDateBinding = paymentDueDateBinding;
    }

    public RichInputDate getPaymentDueDateBinding() {
        return paymentDueDateBinding;
    }

    public void setDayBinding(RichInputText dayBinding) {
        this.dayBinding = dayBinding;
    }

    public RichInputText getDayBinding() {
        return dayBinding;
    }

    public void recordSaveBillWiseOpening(ActionEvent actionEvent) {
        String empCd=(String)ADFUtils.resolveExpression("#{pageFlowScope.empCode}");
        empCd=empCd!=null?empCd:"Admin";
        ADFUtils.setColumnValue("BillWiseOpeningVO1Iterator","LastUpdatedBy",empCd);

//            if(ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("S") || ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("P"))
                  Integer i=0;
                         i=(Integer)bindingOutputText.getValue();
                         System.out.println("EDIT TRANS VALUE"+i);
                            if(i.equals(0))
                                {
                                   FacesMessage Message = new FacesMessage("Record Save Successfully."); 
                                   Message.setSeverity(FacesMessage.SEVERITY_INFO); 
                                   FacesContext fc = FacesContext.getCurrentInstance(); 
                                   fc.addMessage(null, Message);    
                                  
                                }
                                else
                                {
                                    FacesMessage Message = new FacesMessage("Record Updated Successfully."); 
                                    Message.setSeverity(FacesMessage.SEVERITY_INFO); 
                                    FacesContext fc = FacesContext.getCurrentInstance(); 
                                    fc.addMessage(null, Message);    
                                }
    }
    
    public void createBillWiseOpAction(ActionEvent actionEvent) {
        OperationBinding binding = ADFUtils.findOperation("createBillWiseOPBal");
        binding.execute();
    }

    public String saveSubLedAC() {
        DCIteratorBinding binding = ADFUtils.findIterator("BillWiseOpeningVO1Iterator");
        if(binding.getCurrentRow().getAttribute("GlCd") == null)
            ADFUtils.showMessage("CHECK LEDGER CODE FOR THIS PARTY...", 0);
        else
            ADFUtils.findOperation("Commit").execute();
        return null;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    } 

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void EditButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }
    
    private void cevmodecheck(){
            if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
                cevModeDisableComponent("V");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("C");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("E");
            }
        }
    
    public void cevModeDisableComponent(String mode) {
        if (mode.equals("E")) {
                getUnitCodeBinding().setDisabled(true);
                getSubLedgerNameBinding().setDisabled(true);
                getLedgerCodeBinding().setDisabled(true);
                getHeaderEditBinding().setDisabled(true);
                getDetailCreateBinding().setDisabled(false);
                getBindAmountLC().setDisabled(true);
                getDebitAmountBinding().setDisabled(true);
                getCreditAmountBinding().setDisabled(true);
                getDifferenceAmtBinding().setDisabled(true);
                //getBindCurrRate().setDisabled(true);
            } else if (mode.equals("C")) {
               getUnitCodeBinding().setDisabled(true);
                getSubLedgerNameBinding().setDisabled(true);
              //  getLedgerCodeBinding().setDisabled(true);
                getHeaderEditBinding().setDisabled(true);
                getDetailCreateBinding().setDisabled(false);
                getDebitAmountBinding().setDisabled(true);
                getCreditAmountBinding().setDisabled(true);
                getDifferenceAmtBinding().setDisabled(true);
                //getBindCurrRate().setDisabled(true);
               
            } else if (mode.equals("V")) {
                getDetailCreateBinding().setDisabled(true);
            }
        }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setDetailCreateBinding(RichButton detailCreateBinding) {
        this.detailCreateBinding = detailCreateBinding;
    }

    public RichButton getDetailCreateBinding() {
        return detailCreateBinding;
    }

    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
               try {
                   Method method1 =
                       component.getClass().getMethod("setDisabled", boolean.class);
                   if (method1 != null) {
                       method1.invoke(component, valueComponent);
                   }
               } catch (NoSuchMethodException e) {
                   try {
                       Method method =
                           component.getClass().getMethod("setReadOnly", boolean.class);
                       if (method != null) {
                           method.invoke(component, valueComponent);
                       }
                   } catch (Exception e1) {
                       // e.printStackTrace();//silently eat this exception.
                   }


               } catch (Exception e) {
                   // e.printStackTrace();//silently eat this exception.
               }
               List<UIComponent> childComponents = component.getChildren();
               for (UIComponent comp : childComponents) {
                   makeComponentHierarchyReadOnly(comp, valueComponent);
               }
           }

    public void setSubLedgerNameBinding(RichInputText subLedgerNameBinding) {
        this.subLedgerNameBinding = subLedgerNameBinding;
    }

    public RichInputText getSubLedgerNameBinding() {
        return subLedgerNameBinding;
    }

    public void setLedgerCodeBinding(RichInputComboboxListOfValues ledgerCodeBinding) {
        this.ledgerCodeBinding = ledgerCodeBinding;
    }

    public RichInputComboboxListOfValues getLedgerCodeBinding() {
        return ledgerCodeBinding;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setRefNumBinding(RichInputText refNumBinding) {
        this.refNumBinding = refNumBinding;
    }

    public RichInputText getRefNumBinding() {
        return refNumBinding;
    }

    public void partyTypeVCL(ValueChangeEvent valueChangeEvent) {
     
    }

    public void subLedVCL(ValueChangeEvent valueChangeEvent) {
        
//        if(getSubLedgerCodeTableBinding()!=null){
//            if(getBindPartyType().toString().equalsIgnoreCase("C")){
//                getLedgerCodeBinding().setDisabled(true);
//            }
//            else{
//                       getLedgerCodeBinding().setDisabled(false); 
//                  }
//        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(deleteBillWiseOpeningTableBinding);
    }

    public void setDebitAmountBinding(RichInputText debitAmountBinding) {
        this.debitAmountBinding = debitAmountBinding;
    }

    public RichInputText getDebitAmountBinding() {
        return debitAmountBinding;
    }

    public void setCreditAmountBinding(RichInputText creditAmountBinding) {
        this.creditAmountBinding = creditAmountBinding;
    }

    public RichInputText getCreditAmountBinding() {
        return creditAmountBinding;
    }

    public void setDifferenceAmtBinding(RichInputText differenceAmtBinding) {
        this.differenceAmtBinding = differenceAmtBinding;
    }

    public RichInputText getDifferenceAmtBinding() {
        return differenceAmtBinding;
    }

    public void PartyTypeVCE(ValueChangeEvent vce) {
//        if(getSubLedgerCodeTableBinding()!=null){
//        if(vce.getNewValue().toString().equalsIgnoreCase("C")&&vce.getNewValue().toString().equalsIgnoreCase("D")){
//            getLedgerCodeBinding().setDisabled(true);
//        }else{
//            getLedgerCodeBinding().setDisabled(false); 
//        }
//        }
    }

    public void setBindPartyType(RichInputText bindPartyType) {
        this.bindPartyType = bindPartyType;
    }

    public RichInputText getBindPartyType() {
        return bindPartyType;
    }

    public void PatyTypeval(FacesContext facesContext, UIComponent uIComponent, Object object) {
        
//        if(getSubLedgerCodeTableBinding()!=null){
//            if(getBindPartyType().toString().equalsIgnoreCase("C")){
//                getLedgerCodeBinding().setDisabled(true);
//            }
//            else{
//                       getLedgerCodeBinding().setDisabled(false); 
//                  }
//        }
    }
}
