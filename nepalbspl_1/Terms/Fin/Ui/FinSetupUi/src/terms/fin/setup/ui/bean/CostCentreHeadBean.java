package terms.fin.setup.ui.bean;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

public class CostCentreHeadBean {
    private RichTable costCentreHeadTableBinding;
    private String editAction="V";
    String Code="";
    Integer CodeNo=0;
    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public CostCentreHeadBean() {
    }

    public void costCentreHeadDialogDL(DialogEvent dialogEvent) {
            if(dialogEvent.getOutcome().name().equals("ok"))
            {
            OperationBinding op = null;
            ADFUtils.findOperation("Delete").execute();
            op = (OperationBinding) ADFUtils.findOperation("Commit");
            if (getEditAction().equals("V")) {
            Object rst = op.execute();
            }
            System.out.println("Record Delete Successfully");
            if(op.getErrors().isEmpty()){
            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
            } else if (!op.getErrors().isEmpty())
            {
            OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
            Object rstr = opr.execute();
            FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
            Message.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
            }
            }
            AdfFacesContext.getCurrentInstance().addPartialTarget(costCentreHeadTableBinding);
    }

    public void setCostCentreHeadTableBinding(RichTable costCentreHeadTableBinding) {
        this.costCentreHeadTableBinding = costCentreHeadTableBinding;
    }

    public RichTable getCostCentreHeadTableBinding() {
        return costCentreHeadTableBinding;
    }

    public void createCCHeadAL(ActionEvent actionEvent) {
        System.out.println("ENTR BEAN");
        ADFUtils.findOperation("CreateInsert").execute();
        if(Code==null || Code =="")
        {
            System.out.println("Before ");
            OperationBinding opr=ADFUtils.findOperation("getCCHead");
            String obj=(String)opr.execute();
            System.out.println("After"+obj);
            System.out.println("Get Rsult is===>  "+opr.getResult());
            Code=obj;
        }
        else
        {
         String NewCode=Code.substring(1);
         System.out.println("After Sub String is==>"+NewCode);
         if(NewCode !=null)
         {
        CodeNo=Integer.parseInt(NewCode);
        CodeNo=CodeNo+1;
        
        if(CodeNo>=1 && CodeNo<=9)
        NewCode="C000"+CodeNo;
        else if(CodeNo>=10 && CodeNo<=99)
        NewCode="C00"+CodeNo;                
        else if(CodeNo>=100 && CodeNo<=999)
        NewCode="C0"+CodeNo;
        else if(CodeNo >=1000)
        NewCode="C"+CodeNo;
        
        Code=NewCode;
        Row rr=(Row) ADFUtils.evaluateEL("#{bindings.CostCentreHeadVO1Iterator.currentRow}");
        rr.setAttribute("Code",NewCode);            
         }
            
        }
        
    }

    public void cancelButtonAL(ActionEvent actionEvent) {
       ADFUtils.findOperation("Rollback").execute();
       Code="";
    }

    public void SaveAL(ActionEvent actionEvent) {
        try{ADFUtils.findOperation("Commit").execute();
           ADFUtils.setEL("#{pageFlowScope.CostCentreHeadBean.editAction}", "A");
        }
        catch(Exception e){
            ADFUtils.setEL("#{pageFlowScope.CostCentreHeadBean.editAction}", "A");
        }


    }
}
