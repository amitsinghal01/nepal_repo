package terms.fin.setup.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

public class CreateFixedAssetDeletionBean {
    private RichTable detailTableBinding;
    private RichButton headerEditBinding;
    private RichOutputText bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichInputText docNoBinding;
    private RichInputComboboxListOfValues invoiceNoBinding;
    private RichInputText saleQtyBinding;
    private RichInputText saleValueBinding;
    private RichInputText productCdBinding;
    private RichInputText unitCodeBinding;
    private RichInputComboboxListOfValues preparedByBinding;
    private RichSelectOneChoice docTypeBinding;
    private RichInputComboboxListOfValues approvedByBinding;

    public CreateFixedAssetDeletionBean() {
    }

    public void setDetailTableBinding(RichTable detailTableBinding) {
        this.detailTableBinding = detailTableBinding;
    }

    public RichTable getDetailTableBinding() {
        return detailTableBinding;
    }

    public void deletePopupDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
                    ADFUtils.findOperation("Delete").execute();
                    System.out.println("Record Delete Successfully");

                    FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);

                }
                AdfFacesContext.getCurrentInstance().addPartialTarget(detailTableBinding);
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }
    private void cevmodecheck(){
            if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
                cevModeDisableComponent("V");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("C");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("E");
            }
        }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
              try {
                  Method method1 =
                      component.getClass().getMethod("setDisabled", boolean.class);
                  if (method1 != null) {
                      method1.invoke(component, valueComponent);
                  }
              } catch (NoSuchMethodException e) {
                  try {
                      Method method =
                          component.getClass().getMethod("setReadOnly", boolean.class);
                      if (method != null) {
                          method.invoke(component, valueComponent);
                      }
                  } catch (Exception e1) {
                      // e.printStackTrace();//silently eat this exception.
                  }


              } catch (Exception e) {
                  // e.printStackTrace();//silently eat this exception.
              }
              List<UIComponent> childComponents = component.getChildren();
              for (UIComponent comp : childComponents) {
                  makeComponentHierarchyReadOnly(comp, valueComponent);
              }
          } 
    
    
    
    //Set Fields and Button disable.
        public void cevModeDisableComponent(String mode) {
            if (mode.equals("E")) {
              getDocNoBinding().setDisabled(true); 
              getUnitCodeBinding().setDisabled(true);
              getPreparedByBinding().setDisabled(true);
                if(docTypeBinding.getValue()!=null && !docTypeBinding.getValue().equals("SA")){
                 getInvoiceNoBinding().setDisabled(true);
                 getSaleQtyBinding().setDisabled(true);
                 getSaleValueBinding().setDisabled(true);
                 getProductCdBinding().setDisabled(true);
            } 
            }
            else if (mode.equals("C")) {
              getDocNoBinding().setDisabled(true);
              getUnitCodeBinding().setDisabled(true);
              getPreparedByBinding().setDisabled(true);
            } else if (mode.equals("V")) {
                
            
            }
            
        }

    public void editButtonAL(ActionEvent actionEvent) {
        if (approvedByBinding.getValue() == null) {
            cevmodecheck();
        } else {
            ADFUtils.showMessage("Record is Approved Can't be modified !", 2);
            ADFUtils.setEL("#{pageFlowScope.mode}", "V");
        }
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }
    
    public void saveAL(ActionEvent actionEvent) {
        OperationBinding op = ADFUtils.findOperation("docNoGenFixedAssetDeletion");
        Object rst = op.execute();
        if (rst.toString() != null && rst.toString() != "") {
            if (op.getErrors().isEmpty()) {
                ADFUtils.findOperation("Commit").execute();
                ADFUtils.showMessage("Record Saved Successfully. Document Number is " + rst + ".", 2);
            }
        }


        if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ") {
            if (op.getErrors().isEmpty()) {
                ADFUtils.findOperation("Commit").execute();
                ADFUtils.showMessage("Record Update Successfully.", 2);
            }
        }
    }

    public void setDocNoBinding(RichInputText docNoBinding) {
        this.docNoBinding = docNoBinding;
    }

    public RichInputText getDocNoBinding() {
        return docNoBinding;
    }

    public void docTypeVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce!=null){
            String val=(String)vce.getNewValue(); 
            if(!val.equals("SA")){
             getInvoiceNoBinding().setDisabled(true);
             getSaleQtyBinding().setDisabled(true);
             getSaleValueBinding().setDisabled(true);
             getProductCdBinding().setDisabled(true);
            }
            else{
            getInvoiceNoBinding().setDisabled(false);
            getSaleQtyBinding().setDisabled(false);
            getSaleValueBinding().setDisabled(false);
            getProductCdBinding().setDisabled(false);
            }
        }
    }

    public void setInvoiceNoBinding(RichInputComboboxListOfValues invoiceNoBinding) {
        this.invoiceNoBinding = invoiceNoBinding;
    }

    public RichInputComboboxListOfValues getInvoiceNoBinding() {
        return invoiceNoBinding;
    }

    public void setSaleQtyBinding(RichInputText saleQtyBinding) {
        this.saleQtyBinding = saleQtyBinding;
    }

    public RichInputText getSaleQtyBinding() {
        return saleQtyBinding;
    }

    public void setSaleValueBinding(RichInputText saleValueBinding) {
        this.saleValueBinding = saleValueBinding;
    }

    public RichInputText getSaleValueBinding() {
        return saleValueBinding;
    }

    public void setProductCdBinding(RichInputText productCdBinding) {
        this.productCdBinding = productCdBinding;
    }

    public RichInputText getProductCdBinding() {
        return productCdBinding;
    }

    public void setUnitCodeBinding(RichInputText unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputText getUnitCodeBinding() {
        return unitCodeBinding;
    }
    
    public void approveByVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());            
                     OperationBinding ob = ADFUtils.findOperation("checkApprovalStatus");
                    ob.getParamsMap().put("formNm", "FIAD");
                    ob.getParamsMap().put("authoLim", "AP");
                    ob.getParamsMap().put("empcd", vce.getNewValue().toString());
                    ob.execute();
                    System.out.println(" EMP CD"+vce.getNewValue().toString());
                    if((ob.getResult() !=null && ob.getResult().equals("Y")))
                    {
                        Row row=(Row)ADFUtils.evaluateEL("#{bindings.FixedAssetDeletionHeaderVO1Iterator.currentRow}");
                        row.setAttribute("ApprBy", null);
                        ADFUtils.showMessage("You Don't Have Permission To Approve This Record.",0);
                    }

        

        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        }
    
    public void verifyByVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());            
                     OperationBinding ob = ADFUtils.findOperation("checkApprovalStatus");
                    ob.getParamsMap().put("formNm", "FIAD");
                    ob.getParamsMap().put("authoLim", "VR");
                    ob.getParamsMap().put("empcd", vce.getNewValue().toString());
                    ob.execute();
                    System.out.println(" EMP CD"+vce.getNewValue().toString());
                    if((ob.getResult() !=null && ob.getResult().equals("Y")))
                    {
                        Row row=(Row)ADFUtils.evaluateEL("#{bindings.FixedAssetDeletionHeaderVO1Iterator.currentRow}");
                        row.setAttribute("VeryBy", null);
                        ADFUtils.showMessage("You Don't Have Permission To Verify This Record.",0);
                    }

        

        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        }

    public void setPreparedByBinding(RichInputComboboxListOfValues preparedByBinding) {
        this.preparedByBinding = preparedByBinding;
    }

    public RichInputComboboxListOfValues getPreparedByBinding() {
        return preparedByBinding;
    }
    
    public void setDocTypeBinding(RichSelectOneChoice docTypeBinding) {
        this.docTypeBinding = docTypeBinding;
    }

    public RichSelectOneChoice getDocTypeBinding() {
        return docTypeBinding;
    }

    public void setApprovedByBinding(RichInputComboboxListOfValues approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputComboboxListOfValues getApprovedByBinding() {
        return approvedByBinding;
    }
}
