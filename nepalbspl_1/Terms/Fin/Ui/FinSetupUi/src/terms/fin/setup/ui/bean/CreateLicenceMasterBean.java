package terms.fin.setup.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.validator.ValidatorException;

import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class CreateLicenceMasterBean {
    private RichButton headerEditBinding1;
    private RichButton detaildeleteBinding1;
    private RichButton detailCreateBinding1;
    private RichOutputText bindingOutputText1;
    private RichInputText bindDocNo;
    private RichInputDate docDateBinding;
    private RichColumn docNoBinding;
    private RichButton populateBinding;
    private RichInputText bindingDocNoDtl;
    private RichInputText bindDocumentNoDtl2;
    private RichTable dtl2TableBinding;
    private RichSelectOneChoice lcForBinding;
    private String mode="";
    private RichShowDetailItem sionTabBinding;
    private RichShowDetailItem soTabBinding;
    private RichInputComboboxListOfValues productCodeBinding;
    private RichInputText bindItemCode;
    private RichButton bindDelete2;
    private RichTable sionTableBinding;
    private RichPanelHeader getPageRootComponent;
    private String flag="Y";
    private String createFlag="Y";
    private RichInputDate licDateBinding;
    private RichInputText licTypeBinding;
    private RichInputText bindCifValFC;
    private RichInputText fobValFc;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText foBValueINR;
    private RichInputText bingCifValueINR;
    private RichInputText actualQuantity;
    private RichInputText qtyAvailed;
    private RichInputText bindCurrentRate;
    private RichInputText shortNameBinding;
    private RichInputText sionSlNoBinding;
    private RichColumn stdnorminding;
    private RichInputText stDNORMBinding;
    private RichInputText dtlCifValueFC;
    private RichInputText dtlCifValueINR;
    private RichInputDate currencyDateBinding;
    private String srv_flag="Y";


    public CreateLicenceMasterBean() {
       
       
        
    }

   
    

   



    

   

   

   

    
  
    
    private void cevmodecheck(){
          if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
              makeComponentHierarchyReadOnly(this.getPageRootComponent, true);
              cevModeDisableComponent("V");
          }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
              makeComponentHierarchyReadOnly(this.getPageRootComponent, false);
              cevModeDisableComponent("C");
          }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
              makeComponentHierarchyReadOnly(this.getPageRootComponent, false);
              cevModeDisableComponent("E");
          }
      }
    
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
               try {
                   Method method1 =
                       component.getClass().getMethod("setDisabled", boolean.class);
                   if (method1 != null) {
                       method1.invoke(component, valueComponent);
                   }
               } catch (NoSuchMethodException e) {
                   try {
                       Method method =
                           component.getClass().getMethod("setReadOnly", boolean.class);
                       if (method != null) {
                           method.invoke(component, valueComponent);
                       }
                   } catch (Exception e1) {
                       // e.printStackTrace();//silently eat this exception.
                   }


               } catch (Exception e) {
                   // e.printStackTrace();//silently eat this exception.
               }
               List<UIComponent> childComponents = component.getChildren();
               for (UIComponent comp : childComponents) {
                   makeComponentHierarchyReadOnly(comp, valueComponent);
               }
           }
    
    
    public void cevModeDisableComponent(String mode) {
            if (mode.equals("E"))
            {
            //getHeaderEditBinding().setDisabled(true);
            getDetailCreateBinding1().setDisabled(false);
           //getDetaildeleteBinding1().setDisabled(false);
            getHeaderEditBinding1().setDisabled(true);
               getBindDocNo().setDisabled(true);
           getDocDateBinding().setDisabled(true);
               getPopulateBinding().setDisabled(false);
               getBindingDocNoDtl().setDisabled(true);
               getBindDocumentNoDtl2().setDisabled(true);
               getLcForBinding().setDisabled(true);
               getPopulateBinding().setDisabled(true);
               getProductCodeBinding().setDisabled(true);
               getBindItemCode().setDisabled(true);
               getUnitCodeBinding().setDisabled(true);
               getFoBValueINR().setDisabled(false);
               getBingCifValueINR().setDisabled(true);
               getActualQuantity().setDisabled(true);
               getQtyAvailed().setDisabled(true);
               getShortNameBinding().setDisabled(true);
               getSionSlNoBinding().setDisabled(true);
               getStDNORMBinding().setDisabled(true);
               getDtlCifValueFC().setDisabled(true);
               getDtlCifValueINR().setDisabled(true);
               getBindCifValFC().setDisabled(true);
               getFoBValueINR().setDisabled(true);
               getBindCurrentRate().setDisabled(true);
           }
           else if (mode.equals("C")) 
           {
            getDetailCreateBinding1().setDisabled(false);
            //getDetaildeleteBinding1().setDisabled(false);
            getHeaderEditBinding1().setDisabled(true);
            getBindDocNo().setDisabled(true);
               getDocDateBinding().setDisabled(true);
               getPopulateBinding().setDisabled(false);
               getBindingDocNoDtl().setDisabled(true);
               getBindDocumentNoDtl2().setDisabled(true);
               getLcForBinding().setDisabled(false);
               getUnitCodeBinding().setDisabled(true);
               getFoBValueINR().setDisabled(false);
              getBingCifValueINR().setDisabled(true);
              getActualQuantity().setDisabled(true);
              getQtyAvailed().setDisabled(true);
               getShortNameBinding().setDisabled(true);
               getSionSlNoBinding().setDisabled(true);
               getStDNORMBinding().setDisabled(true);
              getDtlCifValueFC().setDisabled(false);
              getDtlCifValueINR().setDisabled(false);
              getBindCifValFC().setDisabled(true);
               getFoBValueINR().setDisabled(true);
               getBindCurrentRate().setDisabled(false);
           } 
           else if (mode.equals("V")) 
           {
           getDetailCreateBinding1().setDisabled(true);
        //   getDetaildeleteBinding1().setDisabled(true);
               getPopulateBinding().setDisabled(true);
               getSionTabBinding().setDisabled(false);
               getSoTabBinding().setDisabled(false);
               getBindItemCode().setDisabled(true);
//              getBindDelete2().setDisabled(true);
               getUnitCodeBinding().setDisabled(true);
               getFoBValueINR().setDisabled(true);
               getBingCifValueINR().setDisabled(true);
               getActualQuantity().setDisabled(true);
               getQtyAvailed().setDisabled(true);
               getShortNameBinding().setDisabled(true);
               getSionSlNoBinding().setDisabled(true);
             getStDNORMBinding().setDisabled(true);
             getBingCifValueINR().setDisabled(true);
               getDtlCifValueFC().setDisabled(true);
               getDtlCifValueINR().setDisabled(true);
               getBindCifValFC().setDisabled(true);
               getFoBValueINR().setDisabled(true);
               getBindCurrentRate().setDisabled(true);
           }
        
           
       }


//    public void SaveAL(ActionEvent actionEvent) {
//       ADFUtils.findOperation("LicenceNo").execute();
//       
//    }

    public void Save1AL(ActionEvent actionEvent) {
        String empCd=(String)ADFUtils.resolveExpression("#{pageFlowScope.empCode}");
        empCd=empCd!=null?empCd:"Admin";
        ADFUtils.setColumnValue("LicenceMasterVO1Iterator","LastUpdatedBy",empCd);
        ADFUtils.findOperation("Commit").execute();
        ADFUtils.findOperation("LicenceNo").execute();
        
        if(this.getLcForBinding().isDisabled()){
//            ADFUtils.findOperation("Commit").execute();
//            ADFUtils.findOperation("LicenceNo").execute();
            ADFUtils.showMessage("Record Updated Succesfully", 2);
            
        }
        else{
            ADFUtils.showMessage("Record Saved Succesfully.New Document No. is "+bindDocNo.getValue(), 2);
        }
        
    }

    public void PopulateAction(ActionEvent actionEvent) {
      ADFUtils.findOperation("ItemCodePopulate").execute();
       flag="N";
        disbledField();
    }

    public void CreateAL(ActionEvent actionEvent) {
//        ADFUtils.findOperation("CreateInsert").execute();
        
}

    public void setHeaderEditBinding1(RichButton headerEditBinding1) {
        this.headerEditBinding1 = headerEditBinding1;
    }

    public RichButton getHeaderEditBinding1() {
        return headerEditBinding1;
    }


    public void setDetaildeleteBinding1(RichButton detaildeleteBinding1) {
        this.detaildeleteBinding1 = detaildeleteBinding1;
    }

    public RichButton getDetaildeleteBinding1() {
        return detaildeleteBinding1;
    }

    public void setDetailCreateBinding1(RichButton detailCreateBinding1) {
        this.detailCreateBinding1 = detailCreateBinding1;
    }

    public RichButton getDetailCreateBinding1() {
        return detailCreateBinding1;
    }

    public void setBindingOutputText1(RichOutputText bindingOutputText1) {
        this.bindingOutputText1 = bindingOutputText1;
    }

    public RichOutputText getBindingOutputText1() {
        cevmodecheck();
        return bindingOutputText1;
    }

    public void setBindDocNo(RichInputText bindDocNo) {
        this.bindDocNo = bindDocNo;
    }

    public RichInputText getBindDocNo() {
        return bindDocNo;
    }

    public void setDocDateBinding(RichInputDate docDateBinding) {
        this.docDateBinding = docDateBinding;
    }

    public RichInputDate getDocDateBinding() {
        return docDateBinding;
    }

    public void setDocNoBinding(RichColumn docNoBinding) {
        this.docNoBinding = docNoBinding;
    }

    public RichColumn getDocNoBinding() {
        return docNoBinding;
    }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setPopulateBinding(RichButton populateBinding) {
        this.populateBinding = populateBinding;
    }

    public RichButton getPopulateBinding() {
        return populateBinding;
    }

    public void setBindingDocNoDtl(RichInputText bindingDocNoDtl) {
        this.bindingDocNoDtl = bindingDocNoDtl;
    }

    public RichInputText getBindingDocNoDtl() {
        return bindingDocNoDtl;
    }

    public void setBindDocumentNoDtl2(RichInputText bindDocumentNoDtl2) {
        this.bindDocumentNoDtl2 = bindDocumentNoDtl2;
    }

    public RichInputText getBindDocumentNoDtl2() {
        return bindDocumentNoDtl2;
    }

    public void deletePopupDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete3").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(dtl2TableBinding);
        }
    

    public void setDtl2TableBinding(RichTable dtl2TableBinding) {
        this.dtl2TableBinding = dtl2TableBinding;
    }

    public RichTable getDtl2TableBinding() {
        return dtl2TableBinding;
    }

    public void SaveAndCloseAL(ActionEvent actionEvent) {
        // Add event code here...
    }

    public void setLcForBinding(RichSelectOneChoice lcForBinding) {
        this.lcForBinding = lcForBinding;
    }

    public RichSelectOneChoice getLcForBinding() {
        return lcForBinding;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getMode() {
        return mode;
    }

    

    public void setSionTabBinding(RichShowDetailItem sionTabBinding) {
        this.sionTabBinding = sionTabBinding;
    }

    public RichShowDetailItem getSionTabBinding() {
        return sionTabBinding;
    }

    public void setSoTabBinding(RichShowDetailItem soTabBinding) {
        this.soTabBinding = soTabBinding;
    }

    public RichShowDetailItem getSoTabBinding() {
        return soTabBinding;
    }

    public void setProductCodeBinding(RichInputComboboxListOfValues productCodeBinding) {
        this.productCodeBinding = productCodeBinding;
    }

    public RichInputComboboxListOfValues getProductCodeBinding() {
        return productCodeBinding;
    }

    public void setBindItemCode(RichInputText bindItemCode) {
        this.bindItemCode = bindItemCode;
    }

    public RichInputText getBindItemCode() {
        return bindItemCode;
    }

    public void setBindDelete2(RichButton bindDelete2) {
        this.bindDelete2 = bindDelete2;
    }

    public RichButton getBindDelete2() {
        return bindDelete2;
    }

    public void sionDeletePopupDL(DialogEvent dialogEvent) {

        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(sionTableBinding);

    }

    public void setSionTableBinding(RichTable sionTableBinding) {
        this.sionTableBinding = sionTableBinding;
    }

    public RichTable getSionTableBinding() {
        return sionTableBinding;
    }

    public void setGetPageRootComponent(RichPanelHeader getPageRootComponent) {
        this.getPageRootComponent = getPageRootComponent;
    }

    public RichPanelHeader getGetPageRootComponent() {
        return getPageRootComponent;
    }

    public void CreateSionAL(ActionEvent actionEvent) {
 
        if((Long)ADFUtils.evaluateEL("#{bindings.LicenceMasterDetailSionVO1Iterator.estimatedRowCount}")<1)
        {
        createFlag="N";
        }
 System.out.println("Create flag is:="+createFlag);
        ADFUtils.findOperation("CreateInsert").execute();
            disbledField();
        //#{bindings.LicenceMasterDetailSionVO1Iterator.estimatedRowCount}
 
 
 
if(createFlag.equalsIgnoreCase("Y"))
{
        if(flag.equalsIgnoreCase("N"))
        {
            getPopulateBinding().setDisabled(true);
        }
        else{
            getPopulateBinding().setDisabled(true);
        }
}
       
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getFlag() {
        return flag;
    }
    

    public void disbledField()
    {
        
        if(((Long)ADFUtils.evaluateEL("#{bindings.LicenceMasterDetailSionVO1Iterator.estimatedRowCount}")>0) || ((Long)ADFUtils.evaluateEL("#{bindings.LicMasterDetailSoVO1Iterator.estimatedRowCount}")>0))
        {
        getLcForBinding().setDisabled(true);
        getProductCodeBinding().setDisabled(true);
       // getLicTypeBinding().setDisabled(true);
        getLicDateBinding().setDisabled(true);
        
        AdfFacesContext.getCurrentInstance().addPartialTarget(productCodeBinding);
            AdfFacesContext.getCurrentInstance().addPartialTarget(licDateBinding);
        
        }

    }

    public void setLicDateBinding(RichInputDate licDateBinding) {
        this.licDateBinding = licDateBinding;
    }

    public RichInputDate getLicDateBinding() {
        return licDateBinding;
    }

    public void setLicTypeBinding(RichInputText licTypeBinding) {
        this.licTypeBinding = licTypeBinding;
    }

    public RichInputText getLicTypeBinding() {
        return licTypeBinding;
    }

    public void createSOAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert1").execute();
        disbledField();
    }

    public void CurrCodeVCE(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(valueChangeEvent!=null){
            OperationBinding op=ADFUtils.findOperation("setCurrencyRate");
            Object ob=op.execute();
            System.out.println("ONE*************result iss==>>"+op.getResult());
            
            if(op.getResult()!=null && op.getResult().toString().equalsIgnoreCase("N")){
                System.out.println("inside if when result is ===>> not IM");
                
                    FacesMessage Message = new FacesMessage("Daily Currency is  not updated for this CuurDate in Currency Master");   
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                    FacesContext fc = FacesContext.getCurrentInstance();   
                    fc.addMessage(null, Message); 
            }
        //            if(op.getResult().toString().equalsIgnoreCase("IM")){
        //
        //                System.out.println("inside if when result is ===>> IM");
        //                getCurrRateBinding().setDisabled(true);
        //            }
        //            else{
        //                getCurrRateBinding().setDisabled(false);
        //            }
       }
        }

    

    public void CurrDate(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(valueChangeEvent!=null){
            OperationBinding op=ADFUtils.findOperation("setCurrencyRate");
            Object ob=op.execute();
            System.out.println("result iss==>>"+op.getResult());
            
            if(op.getResult()!=null && op.getResult().toString().equalsIgnoreCase("N")){
                System.out.println("inside if when result is ===>> not IM");
                
                    FacesMessage Message = new FacesMessage("Daily Currency is  not updated for this CuurDate in Currency Master");   
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                    FacesContext fc = FacesContext.getCurrentInstance();   
                    fc.addMessage(null, Message); 
            }
        //            if(op.getResult().toString().equalsIgnoreCase("IM")){
        //
        //                System.out.println("inside if when result is ===>> IM");
        //                getCurrRateBinding().setDisabled(true);
        //            }
        //            else{
        //                getCurrRateBinding().setDisabled(false);
        //            }
        }
    }

    public void LicFor(ValueChangeEvent vce) {
        System.out.println("vce"+vce.getNewValue());
        if(vce.getNewValue().equals("EXP")){
            
           getBindCifValFC().setDisabled(true);
            getFobValFc().setDisabled(false);
        }
        else {
            getBindCifValFC().setDisabled(false);
            getFobValFc().setDisabled(true);

        }
        
        AdfFacesContext.getCurrentInstance().addPartialTarget(bindCifValFC);
        AdfFacesContext.getCurrentInstance().addPartialTarget(fobValFc);
        AdfFacesContext.getCurrentInstance().addPartialTarget(bindCurrentRate);
       
        

    }

    public void setBindCifValFC(RichInputText bindCifValFC) {
        this.bindCifValFC = bindCifValFC;
    }

    public RichInputText getBindCifValFC() {
        return bindCifValFC;
    }

    public void setFobValFc(RichInputText fobValFc) {
        this.fobValFc = fobValFc;
    }

    public RichInputText getFobValFc() {
        return fobValFc;
    }

    public void CifValFc(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(valueChangeEvent!=null){
            OperationBinding op=ADFUtils.findOperation("calculateNo");
            Object ob=op.execute();
            System.out.println("result iss==>>"+op.getResult());
    }
    AdfFacesContext.getCurrentInstance().addPartialTarget(bingCifValueINR);
}

    public void FobValFc(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(valueChangeEvent!=null){
            OperationBinding op=ADFUtils.findOperation("calculateNo");
            Object ob=op.execute();
            System.out.println("result iss==>>"+op.getResult());
    }
}

    public void QtyAllowed(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(valueChangeEvent!=null){
            OperationBinding op=ADFUtils.findOperation("calculateDtlTbl");
            Object ob=op.execute();
            System.out.println("result iss==>>"+op.getResult());
    }
        
    }
    public void QtyHdr(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(valueChangeEvent!=null){
            OperationBinding op=ADFUtils.findOperation("calculateDtlTbl");
            Object ob=op.execute();
            System.out.println("result iss==>>"+op.getResult());
    }
}

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setFoBValueINR(RichInputText foBValueINR) {
        this.foBValueINR = foBValueINR;
    }

    public RichInputText getFoBValueINR() {
        return foBValueINR;
    }

    public void setBingCifValueINR(RichInputText bingCifValueINR) {
        this.bingCifValueINR = bingCifValueINR;
    }

    public RichInputText getBingCifValueINR() {
        return bingCifValueINR;
    }

    public void setActualQuantity(RichInputText actualQuantity) {
        this.actualQuantity = actualQuantity;
    }

    public RichInputText getActualQuantity() {
        return actualQuantity;
    }

    public void setQtyAvailed(RichInputText qtyAvailed) {
        this.qtyAvailed = qtyAvailed;
    }

    public RichInputText getQtyAvailed() {
        return qtyAvailed;
    }

    public void setBindCurrentRate(RichInputText bindCurrentRate) {
        this.bindCurrentRate = bindCurrentRate;
    }

    public RichInputText getBindCurrentRate() {
        return bindCurrentRate;
    }

    public void CurrentRateVCE(ValueChangeEvent valueChangeEvent) {
        AdfFacesContext.getCurrentInstance().addPartialTarget(bingCifValueINR);
        
    }

    public void setShortNameBinding(RichInputText shortNameBinding) {
        this.shortNameBinding = shortNameBinding;
    }

    public RichInputText getShortNameBinding() {
        return shortNameBinding;
    }

    public void setSionSlNoBinding(RichInputText sionSlNoBinding) {
        this.sionSlNoBinding = sionSlNoBinding;
    }

    public RichInputText getSionSlNoBinding() {
        return sionSlNoBinding;
    }

    public void setStdnorminding(RichColumn stdnorminding) {
        this.stdnorminding = stdnorminding;
    }

    public RichColumn getStdnorminding() {
        return stdnorminding;
    }

    public void setStDNORMBinding(RichInputText stDNORMBinding) {
        this.stDNORMBinding = stDNORMBinding;
    }

    public RichInputText getStDNORMBinding() {
        return stDNORMBinding;
    }

    public void setDtlCifValueFC(RichInputText dtlCifValueFC) {
        this.dtlCifValueFC = dtlCifValueFC;
    }

    public RichInputText getDtlCifValueFC() {
        return dtlCifValueFC;
    }

    public void setDtlCifValueINR(RichInputText dtlCifValueINR) {
        this.dtlCifValueINR = dtlCifValueINR;
    }

    public RichInputText getDtlCifValueINR() {
        return dtlCifValueINR;
    }

    public void CifValueInrVCE(ValueChangeEvent valueChangeEvent) {
       
    }

    public void AvailNormVCE(ValueChangeEvent valueChangeEvent) {
      // AdfFacesContext.getCurrentInstance().addPartialTarget(qtyAvailed);
       AdfFacesContext.getCurrentInstance().addPartialTarget(sionTableBinding);
              
    }

    public void CifRateFCVCE(ValueChangeEvent valueChangeEvent) {
      //  AdfFacesContext.getCurrentInstance().addPartialTarget(bindCifValFC);
       // AdfFacesContext.getCurrentInstance().addPartialTarget(sionTableBinding);
    }

    public void ExRateVCE(ValueChangeEvent valueChangeEvent) {
        AdfFacesContext.getCurrentInstance().addPartialTarget(bingCifValueINR);
        AdfFacesContext.getCurrentInstance().addPartialTarget(bindCifValFC);
        AdfFacesContext.getCurrentInstance().addPartialTarget(sionTableBinding);
    }

    public void CifValueINR(ValueChangeEvent valueChangeEvent) {
        AdfFacesContext.getCurrentInstance().addPartialTarget(bindCifValFC);
    }

    public void CurrCodeVCE1(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(valueChangeEvent!=null){
            OperationBinding op=ADFUtils.findOperation("setCurrencyRate");
            Object ob=op.execute();
            System.out.println("ONE*************result iss==>>"+op.getResult());
            
            if(op.getResult()!=null && op.getResult().toString().equalsIgnoreCase("N")){
                System.out.println("inside if when result is ===>> not IM");
                
                    FacesMessage Message = new FacesMessage("Daily Currency is  not updated for this CuurDate in Currency Master");   
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                    FacesContext fc = FacesContext.getCurrentInstance();   
                    fc.addMessage(null, Message); 
    }
}}

    public void unitcodeVCE(ValueChangeEvent valueChangeEvent) {
//        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
//        if(valueChangeEvent!=null){
//            OperationBinding op=ADFUtils.findOperation("setCurrencyRate");
//            Object ob=op.execute();
//            System.out.println("ONE*************result iss==>>"+op.getResult());
//            
//            if(op.getResult()!=null && op.getResult().toString().equalsIgnoreCase("N")){
//                System.out.println("inside if when result is ===>> not IM");
//                
//                    FacesMessage Message = new FacesMessage("Daily Currency is  not updated for this CuurDate in Currency Master");   
//                    Message.setSeverity(FacesMessage.SEVERITY_INFO);   
//                    FacesContext fc = FacesContext.getCurrentInstance();   
//                    fc.addMessage(null, Message); 
            }
    

    public void setCurrencyDateBinding(RichInputDate currencyDateBinding) {
        this.currencyDateBinding = currencyDateBinding;
    }

    public RichInputDate getCurrencyDateBinding() {
        return currencyDateBinding;
    }

    public void SoNoVce(ValueChangeEvent valueChangeEvent) {
//        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
//        if(valueChangeEvent.getNewValue()!=null)
//        {
//                srv_flag="N";
//      
//
//                OperationBinding opr=ADFUtils.findOperation("checkDuplicateLicMasterSo");
//                opr.getParamsMap().put("SoNo",valueChangeEvent.getNewValue());
//                opr.getParamsMap().put("prodNo",productCodeBinding.getValue());  
//                Object obj=opr.execute();
//                if(opr.getResult().equals("N"))
//                {
//                ADFUtils.showMessage("SoNo Already Exist.",0);
                }


    public void qtyAvailaedVCE(ValueChangeEvent valueChangeEvent) {
      //   AdfFacesContext.getCurrentInstance().addPartialTarget(sionTableBinding);
    }
}
