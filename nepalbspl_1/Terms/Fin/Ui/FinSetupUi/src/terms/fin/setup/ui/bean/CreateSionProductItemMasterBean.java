package terms.fin.setup.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;

import oracle.adf.view.rich.component.rich.input.RichInputText;

import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

public class CreateSionProductItemMasterBean {
    private RichInputComboboxListOfValues bindProductCode;
    private RichInputText bindProductName;
    private RichInputText bindDocumentNumber;
    private RichOutputText bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichButton detailcreateBinding;
    private RichButton detaildeleteBinding;
    private RichTable createSionProductItemMasterTableBinding;
    private String Message="C";
    private RichInputText bindDocumentNumberDetail;
    private RichInputText bindProductCodeDetail;
    private RichInputText bindProductNameDetail;
    private RichInputText bindQualityAllow;
    private RichInputText bindItemName;
    private RichButton bindPopulateButton;
    private RichInputComboboxListOfValues bindUnitCode;
    private RichInputText bindUnitName;
    private RichInputComboboxListOfValues bindPreparedBy;
    private RichInputText bindPreparedByName;
    private RichInputComboboxListOfValues bindApproveBy;
    private RichInputText bindApprovedByName;
    private RichInputComboboxListOfValues bindItemCode;

    public CreateSionProductItemMasterBean() {
    }

    public void saveAL(ActionEvent actionEvent) {
            String empCd=(String)ADFUtils.resolveExpression("#{pageFlowScope.empCode}");
            empCd=empCd!=null?empCd:"Admin";
            ADFUtils.setColumnValue("SionProductItemMasterHeaderVO1Iterator","LastUpdatedBy",empCd);
            if(Message.equalsIgnoreCase("C"))
                   {
                if(bindDocumentNumber.getValue()==null){
            ADFUtils.findOperation("generateDocumentNumber").execute();
                       System.out.println("Mode Message 1"+Message);
                       String DocHeadNo=(String)bindDocumentNumber.getValue(); 
                       System.out.println("Document Head No is"+DocHeadNo);
                    
                       
                       ADFUtils.findOperation("Commit").execute();
                       ADFUtils.showMessage("Record Saved Successfully. New Entry Number is "+bindDocumentNumber.getValue()+".", 2);
                }
                else if (bindDocumentNumber.getValue()!=null)
                {
                        ADFUtils.findOperation("Commit").execute();
                        ADFUtils.showMessage("Record Saved Successfully. New Entry Number is "+bindDocumentNumber.getValue()+".", 2);
                    }
                   }
                   else if(Message.equalsIgnoreCase("E"))
                   {
                       System.out.println("Mode Message 2"+Message);
                       ADFUtils.findOperation("Commit").execute();
                       ADFUtils.showMessage("Record Updated Successfully.", 2);
                   }
        }

    public void setBindProductCode(RichInputComboboxListOfValues bindProductCode) {
        this.bindProductCode = bindProductCode;
    }

    public RichInputComboboxListOfValues getBindProductCode() {
        return bindProductCode;
    }

    public void setBindProductName(RichInputText bindProductName) {
        this.bindProductName = bindProductName;
    }

    public RichInputText getBindProductName() {
        return bindProductName;
    }

    public void setBindDocumentNumber(RichInputText bindDocumentNumber) {
        this.bindDocumentNumber = bindDocumentNumber;
    }

    public RichInputText getBindDocumentNumber() {
        return bindDocumentNumber;
    }



    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setDetailcreateBinding(RichButton detailcreateBinding) {
        this.detailcreateBinding = detailcreateBinding;
    }

    public RichButton getDetailcreateBinding() {
        return detailcreateBinding;
    }

    
    private void cevmodecheck(){
            if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
                cevModeDisableComponent("V");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("C");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("E");
            }
        }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                try {
                    Method method1 =
                        component.getClass().getMethod("setDisabled", boolean.class);
                    if (method1 != null) {
                        method1.invoke(component, valueComponent);
                    }
                } catch (NoSuchMethodException e) {
                    try {
                        Method method =
                            component.getClass().getMethod("setReadOnly", boolean.class);
                        if (method != null) {
                            method.invoke(component, valueComponent);
                        }
                    } catch (Exception e1) {
                        // e.printStackTrace();//silently eat this exception.
                    }


                } catch (Exception e) {
                    // e.printStackTrace();//silently eat this exception.
                }
                List<UIComponent> childComponents = component.getChildren();
                for (UIComponent comp : childComponents) {
                    makeComponentHierarchyReadOnly(comp, valueComponent);
                }
            }
    
    //Set Fields and Button disable.
        public void cevModeDisableComponent(String mode) {
            if (mode.equals("E")) {
                Message="E";
                getBindUnitCode().setDisabled(true);
                getBindUnitName().setDisabled(true);
               // getBindApproveBy().setDisabled(true);
                getBindApprovedByName().setDisabled(true);
              //  getBindPreparedBy().setDisabled(true);
                getBindPreparedByName().setDisabled(true);
                getBindDocumentNumber().setDisabled(true);
                getBindProductName().setDisabled(true);
                getBindDocumentNumberDetail().setDisabled(true);
                getBindDocumentNumberDetail().setDisabled(true);
                getBindProductCodeDetail().setDisabled(true);
                getBindProductNameDetail().setDisabled(true);
               // getBindItemCode().setDisabled(true);
                getBindItemName().setDisabled(true);
               // getBindQualityAllow().setDisabled(true);
                    getHeaderEditBinding().setDisabled(true);
                    getDetailcreateBinding().setDisabled(false);
                    getBindProductCode().setDisabled(true);
                    getBindPopulateButton().setDisabled(true);
                    getBindPreparedBy().setDisabled(true);
               
            } else if (mode.equals("C")) {
                getBindUnitCode().setDisabled(true);
                getBindUnitName().setDisabled(true);
                // getBindApproveBy().setDisabled(true);
                getBindApprovedByName().setDisabled(true);
                //  getBindPreparedBy().setDisabled(true);
                getBindPreparedByName().setDisabled(true);
                getBindDocumentNumber().setDisabled(true);
                getBindProductName().setDisabled(true);
                getBindDocumentNumberDetail().setDisabled(true);
                getBindProductCodeDetail().setDisabled(true);
                getBindProductNameDetail().setDisabled(true);
               // getBindItemCode().setDisabled(true);
                getBindItemName().setDisabled(true);
               // getBindQualityAllow().setDisabled(true);
                getDetailcreateBinding().setDisabled(false);
                getBindApproveBy().setDisabled(true);
             
            } else if (mode.equals("V")) {
                getDetailcreateBinding().setDisabled(true);
               // getDetaildeleteBinding().setDisabled(true);
               // getBindPopulateButton().setDisabled(true);
        }
            
        }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setCreateSionProductItemMasterTableBinding(RichTable createSionProductItemMasterTableBinding) {
        this.createSionProductItemMasterTableBinding = createSionProductItemMasterTableBinding;
    }

    public RichTable getCreateSionProductItemMasterTableBinding() {
        return createSionProductItemMasterTableBinding;
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete").execute();
            OperationBinding op=  ADFUtils.findOperation("Commit");
            Object rst = op.execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }
        
                      AdfFacesContext.getCurrentInstance().addPartialTarget(createSionProductItemMasterTableBinding);
    }



    public void populateButtonAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("populateSinoProductItemDetail").execute();
    }

    public void setBindDocumentNumberDetail(RichInputText bindDocumentNumberDetail) {
        this.bindDocumentNumberDetail = bindDocumentNumberDetail;
    }

    public RichInputText getBindDocumentNumberDetail() {
        return bindDocumentNumberDetail;
    }

    public void setBindProductCodeDetail(RichInputText bindProductCodeDetail) {
        this.bindProductCodeDetail = bindProductCodeDetail;
    }

    public RichInputText getBindProductCodeDetail() {
        return bindProductCodeDetail;
    }

    public void setBindProductNameDetail(RichInputText bindProductNameDetail) {
        this.bindProductNameDetail = bindProductNameDetail;
    }

    public RichInputText getBindProductNameDetail() {
        return bindProductNameDetail;
    }


    public void setBindQualityAllow(RichInputText bindQualityAllow) {
        this.bindQualityAllow = bindQualityAllow;
    }

    public RichInputText getBindQualityAllow() {
        return bindQualityAllow;
    }

    public void setBindItemName(RichInputText bindItemName) {
        this.bindItemName = bindItemName;
    }

    public RichInputText getBindItemName() {
        return bindItemName;
    }

    public void setBindPopulateButton(RichButton bindPopulateButton) {
        this.bindPopulateButton = bindPopulateButton;
    }

    public RichButton getBindPopulateButton() {
        return bindPopulateButton;
    }

    public void setBindUnitCode(RichInputComboboxListOfValues bindUnitCode) {
        this.bindUnitCode = bindUnitCode;
    }

    public RichInputComboboxListOfValues getBindUnitCode() {
        return bindUnitCode;
    }

    public void setBindUnitName(RichInputText bindUnitName) {
        this.bindUnitName = bindUnitName;
    }

    public RichInputText getBindUnitName() {
        return bindUnitName;
    }

    public void setBindPreparedBy(RichInputComboboxListOfValues bindPreparedBy) {
        this.bindPreparedBy = bindPreparedBy;
    }

    public RichInputComboboxListOfValues getBindPreparedBy() {
        return bindPreparedBy;
    }

    public void setBindPreparedByName(RichInputText bindPreparedByName) {
        this.bindPreparedByName = bindPreparedByName;
    }

    public RichInputText getBindPreparedByName() {
        return bindPreparedByName;
    }

    public void setBindApproveBy(RichInputComboboxListOfValues bindApproveBy) {
        this.bindApproveBy = bindApproveBy;
    }

    public RichInputComboboxListOfValues getBindApproveBy() {
        return bindApproveBy;
    }

    public void setBindApprovedByName(RichInputText bindApprovedByName) {
        this.bindApprovedByName = bindApprovedByName;
    }

    public RichInputText getBindApprovedByName() {
        return bindApprovedByName;
    }

    public void setBindItemCode(RichInputComboboxListOfValues bindItemCode) {
        this.bindItemCode = bindItemCode;
    }

    public RichInputComboboxListOfValues getBindItemCode() {
        return bindItemCode;
    }
}
