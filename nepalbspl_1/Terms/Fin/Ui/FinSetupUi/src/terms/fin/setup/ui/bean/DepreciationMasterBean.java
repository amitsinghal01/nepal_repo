package terms.fin.setup.ui.bean;

import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class DepreciationMasterBean {
    private RichOutputText bindingOutputText;
    private String editAction="V";
    private RichTable depreciationTableBinding;
    private RichInputText lifebinding;
    private RichInputText depRateBinding;

    public DepreciationMasterBean() {
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
    if(dialogEvent.getOutcome().name().equals("ok"))
    {
    oracle.adf.model.OperationBinding op = null;
    ADFUtils.findOperation("Delete").execute();
    op = (oracle.adf.model.OperationBinding) ADFUtils.findOperation("Commit");
    if (getEditAction().equals("V")) {
    Object rst = op.execute();
    }
    System.out.println("Record Delete Successfully");
    if(op.getErrors().isEmpty()){
    FacesMessage Message = new FacesMessage("Record Deleted Successfully");
    Message.setSeverity(FacesMessage.SEVERITY_INFO);
    FacesContext fc = FacesContext.getCurrentInstance();
    fc.addMessage(null, Message);
    } else if (!op.getErrors().isEmpty())
    {
    oracle.adf.model.OperationBinding opr = (oracle.adf.model.OperationBinding) ADFUtils.findOperation("Rollback");
    Object rstr = opr.execute();
    FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
    Message.setSeverity(FacesMessage.SEVERITY_ERROR);
    FacesContext fc = FacesContext.getCurrentInstance();
    fc.addMessage(null, Message);
    }
    }
    AdfFacesContext.getCurrentInstance().addPartialTarget(depreciationTableBinding);

    }
//               if(dialogEvent.getOutcome().name().equals("ok")){
//                   OperationBinding op = null;
//                       ADFUtils.findOperation("Delete").execute();
//                       op = (OperationBinding) ADFUtils.findOperation("Commit");
//               if (getEditAction().equals("V")) {
//                        Object rst = op.execute();
//               }
//                               System.out.println("Record Delete Successfully");
//               if(op.getErrors().isEmpty()){
//                    FacesMessage Message = new FacesMessage("Record Deleted Successfully");
//                       Message.setSeverity(FacesMessage.SEVERITY_INFO);
//                    FacesContext fc = FacesContext.getCurrentInstance();
//                        fc.addMessage(null, Message);
//               } else if (!op.getErrors().isEmpty()){
//                    OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
//                         Object rstr = opr.execute();
//                       FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
//                       Message.setSeverity(FacesMessage.SEVERITY_ERROR);
//                       FacesContext fc = FacesContext.getCurrentInstance();
//                            fc.addMessage(null, Message);
//                    }
//               }
//                    AdfFacesContext.getCurrentInstance().addPartialTarget(depreciationTableBinding);
//           }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        return bindingOutputText;
    }

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public void setDepreciationTableBinding(RichTable depreciationTableBinding) {
        this.depreciationTableBinding = depreciationTableBinding;
    }

    public RichTable getDepreciationTableBinding() {
        return depreciationTableBinding;
    }

    public void lifeassetvce(ValueChangeEvent vce) {
        BigDecimal Only= new BigDecimal(95);
       
   }

    public void setLifebinding(RichInputText lifebinding) {
        this.lifebinding = lifebinding;
    }

    public RichInputText getLifebinding() {
        return lifebinding;
    }

    public void categoryVce(ValueChangeEvent vce) {
        if(vce!=null && lifebinding.getValue()!=null){
         ADFUtils.findOperation("getdividething").execute();
        }
    }

    public void setDepRateBinding(RichInputText depRateBinding) {
        this.depRateBinding = depRateBinding;
    }

    public RichInputText getDepRateBinding() {
        return depRateBinding;
    }
}

