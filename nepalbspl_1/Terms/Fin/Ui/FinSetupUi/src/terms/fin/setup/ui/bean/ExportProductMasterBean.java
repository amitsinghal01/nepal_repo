package terms.fin.setup.ui.bean;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

public class ExportProductMasterBean {
    private String editAction="V";
    private RichTable tableBinding;
        String Code="";
        Integer CodeNo=0;


    public ExportProductMasterBean() {
    }

    public void CreateAL(ActionEvent actionEvent) {
//        ADFUtils.findOperation("CreateInsert").execute();
//        ADFUtils.findOperation("getProdCd").execute();
    }

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public void deletePopupDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok")){
            OperationBinding op = null;
                ADFUtils.findOperation("Delete").execute();
                op = (OperationBinding) ADFUtils.findOperation("Commit");
        if (getEditAction().equals("V")) {
                 Object rst = op.execute();
        }
                        System.out.println("Record Delete Successfully");
        if(op.getErrors().isEmpty()){
             FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
             FacesContext fc = FacesContext.getCurrentInstance();
                 fc.addMessage(null, Message);
        } else if (!op.getErrors().isEmpty()){
             OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
                  Object rstr = opr.execute();
                FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc = FacesContext.getCurrentInstance();
                     fc.addMessage(null, Message);
             }
        }
             AdfFacesContext.getCurrentInstance().addPartialTarget(tableBinding);
        }


 

    public void setTableBinding(RichTable tableBinding) {
        this.tableBinding = tableBinding;
    }

    public RichTable getTableBinding() {
        return tableBinding;
    }
    public void CreateAL1(ActionEvent actionEvent)
       {
           ADFUtils.findOperation("CreateInsert").execute();
             try
           {
             if(Code==null || Code =="")
            {
           OperationBinding opr=ADFUtils.findOperation("getProdCd");  

               String obj=(String)opr.execute();
                Code=obj;
            }
            else
            {
             String NewCode=Code.substring(2);
             if(NewCode !=null)
             {
            CodeNo=Integer.parseInt(NewCode);
            System.out.println("CODE NO1="+CodeNo);
            CodeNo=CodeNo+1;
            System.out.println("CODE NO:="+CodeNo);
            //PART000
            if(CodeNo>=1 && CodeNo<=9)
            NewCode="E-0000"+CodeNo;
            else if(CodeNo>=10 && CodeNo<=99)
            NewCode="E-000"+CodeNo;               
            else if(CodeNo>=100 && CodeNo<1000)
            NewCode="E-00"+CodeNo;
           else if(CodeNo>=1000 && CodeNo<10000)
           NewCode="E-0"+CodeNo;
            else if(CodeNo>=10000)
                NewCode="E-"+CodeNo;
            Code=NewCode;
            Row rr=(Row) ADFUtils.evaluateEL("#{bindings.ExportProductMasterVO1Iterator.currentRow}");
            rr.setAttribute("ProdCode",NewCode);           
             }
            }
           }
           catch (Exception ee)
           {
           ee.printStackTrace();
           ADFUtils.showMessage("Problem in number generation.Please Try Again.",0);
           }
      
       }

    public void CancelButtonAL(ActionEvent actionEvent) {
       ADFUtils.findOperation("Rollback").execute();
       Code="";
    }
}
