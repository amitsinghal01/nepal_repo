package terms.fin.setup.ui.bean;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class FaGroupMaster {
    private String editAction="V";
    private RichTable fagroupMasterTableBinding;
    private RichInputText bindGrpCd;
    private RichOutputText bindgrpCode1;

    public FaGroupMaster() {
    }

    public void FaGroupMasterBean(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
        {
        OperationBinding op = null;
        ADFUtils.findOperation("Delete").execute();
        op = (OperationBinding) ADFUtils.findOperation("Commit");
        if (getEditAction().equals("V")) {
        Object rst = op.execute();
        }
        System.out.println("Record Delete Successfully");
        if(op.getErrors().isEmpty()){
        FacesMessage Message = new FacesMessage("Record Deleted Successfully");
        Message.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        } else if (!op.getErrors().isEmpty())
        {
        OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
        Object rstr = opr.execute();
        FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
        Message.setSeverity(FacesMessage.SEVERITY_ERROR);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        }
        }
             AdfFacesContext.getCurrentInstance().addPartialTarget(fagroupMasterTableBinding);
        
        
    }

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public void setFagroupMasterTableBinding(RichTable fagroupMasterTableBinding) {
        this.fagroupMasterTableBinding = fagroupMasterTableBinding;
    }

    public RichTable getFagroupMasterTableBinding() {
        return fagroupMasterTableBinding;
    }

    public void saveAL(ActionEvent actionEvent) {
        System.out.println("Group Cd===>>"+bindGrpCd.getValue());
        if(bindgrpCode1.getValue()==null){
            System.out.println("Grp"+getBindGrpCd().getValue());
            ADFUtils.findOperation("GroupNo").execute();
            ADFUtils.findOperation("Commit").execute();
            ADFUtils.showMessage("Record Saved Sucessfully", 2);
//        OperationBinding op = ADFUtils.findOperation("GroupNo");
//        Object rst = op.execute();
//        
//        ADFUtils.findOperation("Commit").execute();
//        
//        
//        System.out.println("--------Commit-------");
//            System.out.println("value aftr getting result--->?"+rst);
//            
//        
//        
//                    FacesMessage Message = new FacesMessage("Record Saved Successfully. New groupCode is "+rst+".");   
//                    Message.setSeverity(FacesMessage.SEVERITY_INFO);   
//                    FacesContext fc = FacesContext.getCurrentInstance();   
//                    fc.addMessage(null, Message);  
        
                
            }
            
        //            if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ")
           else 
            {
                ADFUtils.findOperation("Commit").execute();
                ADFUtils.showMessage("Record Updated Sucessfully", 2);
            
                

            }
            }

    public void setBindGrpCd(RichInputText bindGrpCd) {
        this.bindGrpCd = bindGrpCd;
    }

    public RichInputText getBindGrpCd() {
        return bindGrpCd;
    }

    public void setBindgrpCode1(RichOutputText bindgrpCode1) {
        this.bindgrpCode1 = bindgrpCode1;
    }

    public RichOutputText getBindgrpCode1() {
        return bindgrpCode1;
    }
}
