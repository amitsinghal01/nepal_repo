package terms.fin.setup.ui.bean;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class FixedAssetItBlockMasterBean {
    private RichInputText docNoBinding;
    private RichTable tableBinding;
    private String editAction="V";
    private RichInputText docNoBindingVal;

    public FixedAssetItBlockMasterBean() {
    }

    public void setDocNoBinding(RichInputText docNoBinding) {
        this.docNoBinding = docNoBinding;
    }

    public RichInputText getDocNoBinding() {
        return docNoBinding;
    }

    public void deleteDialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
        {
        OperationBinding op = null;
        ADFUtils.findOperation("Delete").execute();
        op = (OperationBinding) ADFUtils.findOperation("Commit");
        if (getEditAction().equals("V")) {
        Object rst = op.execute();
        }
        System.out.println("Record Delete Successfully");
        if(op.getErrors().isEmpty()){
        FacesMessage Message = new FacesMessage("Record Deleted Successfully");
        Message.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        } else if (!op.getErrors().isEmpty())
        {
        OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
        Object rstr = opr.execute();
        FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
        Message.setSeverity(FacesMessage.SEVERITY_ERROR);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        }
        }
             AdfFacesContext.getCurrentInstance().addPartialTarget(tableBinding);
    }

    public void setTableBinding(RichTable tableBinding) {
        this.tableBinding = tableBinding;
    }

    public RichTable getTableBinding() {
        return tableBinding;
    }

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public void saveAL(ActionEvent actionEvent) {
        System.out.println("Doc No: "+docNoBindingVal.getValue());
        if(docNoBindingVal.getValue()==null){
            System.out.println("Doc No: "+docNoBindingVal.getValue());
            OperationBinding op=ADFUtils.findOperation("docNoFaItBlockMaster");
            op.execute();
            if(op.getResult()!=null){
            ADFUtils.findOperation("Commit").execute();
            ADFUtils.showMessage("Record Saved Sucessfully", 2);
            }
            else{
                ADFUtils.showMessage("Something Went Wrong!", 0);   
                }
            }
           else 
            {
                ADFUtils.findOperation("Commit").execute();
                ADFUtils.showMessage("Record Updated Sucessfully", 2);
            }
    }

    public void setDocNoBindingVal(RichInputText docNoBindingVal) {
        this.docNoBindingVal = docNoBindingVal;
    }

    public RichInputText getDocNoBindingVal() {
        return docNoBindingVal;
    }
}
