package terms.fin.setup.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;
import javax.faces.validator.ValidatorException;

import oracle.adf.view.rich.component.rich.data.RichTable;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class GeneralLedgerBean {
    private RichTable createTableBinding;
    private RichInputText glCdBinding;
    private RichInputText openingBinding;
    private RichInputComboboxListOfValues currCodeBinding;
    private RichSelectOneChoice slCodeTypeBinding;
    private RichOutputText bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichButton detailcreateBinding;
    
    private RichSelectOneChoice subTypeBinding;
    private RichSelectOneChoice groupBinding;
    private RichInputComboboxListOfValues groupCodeBinding;
    private RichInputComboboxListOfValues finYearBinding;


    public GeneralLedgerBean() {
    }

    public void saveAL(ActionEvent actionEvent) {
        String empCd=(String)ADFUtils.resolveExpression("#{pageFlowScope.empCode}");
        empCd=empCd!=null?empCd:"Admin";
        ADFUtils.setColumnValue("GeneralLedgerHeaderVO1Iterator","LastUpdatedBy",empCd);
        OperationBinding op = ADFUtils.findOperation("getGeneralLedgerCode");
        Object rst = op.execute();
        System.out.println(" before calling");
        OperationBinding op3 = ADFUtils.findOperation("getGeneralLedgerPrefixCode");
        Object rst3 = op3.execute();

        if (rst.toString() != null && rst.toString() != "") {
            if (op.getErrors().isEmpty()) {
                OperationBinding op1 = ADFUtils.findOperation("Commit");
                Object rs = op1.execute();

                FacesMessage Message =
                    new FacesMessage("Record Saved Successfully. New General Ledger Number is " + rst + ".");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            }
        }


        if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ") {
            if (op.getErrors().isEmpty()) {
                ADFUtils.findOperation("Commit").execute();
                FacesMessage Message = new FacesMessage("Record Update Successfully.");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);

            }
        }
    }

    public void setCreateTableBinding(RichTable createTableBinding) {
        this.createTableBinding = createTableBinding;
    }

    public RichTable getCreateTableBinding() {
        return createTableBinding;
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(createTableBinding);
    }

    public void gldescValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (object != null && object.toString().trim().length() > 0) {
            if (object.toString().contains(",")) {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                              "Please check General Ledger name, comma is not be used",
                                                              null));
            }
        }
    }

    public void slCodeTypeValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            System.out.println("General Code Binding value:" + glCdBinding.getValue()+" slcodetypebinding:"+slCodeTypeBinding.getValue());
            OperationBinding op = ADFUtils.findOperation("checkSlCodeType");
            op.getParamsMap().put("glCode", glCdBinding.getValue());
            op.getParamsMap().put("slCodeType", slCodeTypeBinding.getValue());
            Object rst = op.execute();
            System.out.println("checkSlCodeType result rst.toString():" + rst.toString());
            System.out.println("checkSlCodeType result op.getresult():" + op.getResult());
            System.out.println("op.getResult().toString().equalsIgnoreCase(\"false\")" +
                               op.getResult().toString().equalsIgnoreCase("false") + "*********" +
                               op.getResult().toString().equalsIgnoreCase("true"));
            if (op.getResult().toString().equalsIgnoreCase("false")) {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                              "Modification of SL Code Type is not Allowed", null));
            }
        }
    }

    public void setGlCdBinding(RichInputText glCdBinding) {
        this.glCdBinding = glCdBinding;
    }

    public RichInputText getGlCdBinding() {
        return glCdBinding;
    }

    public void currCodeVL(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        System.out.println("VCLCurrCode:" + currCodeBinding.getValue() + "  opening:" + openingBinding.getValue());
        if (valueChangeEvent != null) {
            if (currCodeBinding.getValue() != null && openingBinding.getValue() != null) {
                OperationBinding op = ADFUtils.findOperation("setFcGdYob");
                op.getParamsMap().put("currCode", currCodeBinding.getValue());
                op.getParamsMap().put("opbal", openingBinding.getValue());
                Object rst = op.execute();
                System.out.println("setFcGdYob funcion returning value from amimpl:"+rst);
                if (rst != null && rst.toString().equalsIgnoreCase("F")) {                
                    FacesMessage message = new FacesMessage("Please input currency rate for selected currency.");
                    message.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext context = FacesContext.getCurrentInstance();
                    context.addMessage(currCodeBinding.getClientId(), message);
                }else if (rst != null && rst.toString().equalsIgnoreCase("I")) {
                    FacesMessage message = new FacesMessage("Please validate currency in daily currency table.");
                    message.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext context = FacesContext.getCurrentInstance();
                    context.addMessage(currCodeBinding.getClientId(), message);
                }
            }
        }
    }


    public void setOpeningBinding(RichInputText openingBinding) {
        this.openingBinding = openingBinding;
    }

    public RichInputText getOpeningBinding() {
        return openingBinding;
    }

    public void setCurrCodeBinding(RichInputComboboxListOfValues currCodeBinding) {
        this.currCodeBinding = currCodeBinding;
    }

    public RichInputComboboxListOfValues getCurrCodeBinding() {
        return currCodeBinding;
    }

    public void currCodeValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        System.out.println("CurrCode from binding:" + currCodeBinding.getValue()+"CurrCode FROM OBJECT"+object + "  opening:" + openingBinding.getValue()+" Object:"+object);
        if (object != null) {
            if (currCodeBinding.getValue() != null && openingBinding.getValue() != null) {
                OperationBinding op = ADFUtils.findOperation("checkFcGdYobValidation");
                op.getParamsMap().put("currCode", object);
                op.getParamsMap().put("opbal", openingBinding.getValue());
                Object rst = op.execute();
                System.out.println("setFcGdYob function returning value from amimpl:"+rst);
                if (rst != null && rst.toString().equalsIgnoreCase("F")) {
                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                  "Please input currency rate for selected currency.", null));
                }else if (rst != null && rst.toString().equalsIgnoreCase("I")) {
                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                  "Please validate currency in daily currency table.", null));
                }
            }
        }

    }

    public void setSlCodeTypeBinding(RichSelectOneChoice slCodeTypeBinding) {
        this.slCodeTypeBinding = slCodeTypeBinding;
    }

    public RichSelectOneChoice getSlCodeTypeBinding() {
        return slCodeTypeBinding;
    }

    public void subTypeValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
       
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            System.out.println("General Code Binding value:" + glCdBinding.getValue());
            OperationBinding op = ADFUtils.findOperation("checkSubCodeType");
            op.getParamsMap().put("glCode", glCdBinding.getValue());
            Object rst = op.execute();
            System.out.println("checkSubCodeType result rst.toString():" + rst.toString());
            System.out.println("checkSubCodeType result op.getresult():" + op.getResult());
            System.out.println("op.getResult().toString().equalsIgnoreCase(\"false\")" +
                               op.getResult().toString().equalsIgnoreCase("false") + "*********" +
                               op.getResult().toString().equalsIgnoreCase("true"));
            if (op.getResult().toString().equalsIgnoreCase("false")) {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                              "Modification of Sub Code Type is not Allowed", null));
            }
        }

    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() 
    {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setDetailcreateBinding(RichButton detailcreateBinding) {
        this.detailcreateBinding = detailcreateBinding;
    }

    public RichButton getDetailcreateBinding() {
        return detailcreateBinding;
    }

   
    
    
    
    
    private void cevmodecheck(){
            if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
                cevModeDisableComponent("V");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("C");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("E");
            }
        }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
              try {
                  Method method1 =
                      component.getClass().getMethod("setDisabled", boolean.class);
                  if (method1 != null) {
                      method1.invoke(component, valueComponent);
                  }
              } catch (NoSuchMethodException e) {
                  try {
                      Method method =
                          component.getClass().getMethod("setReadOnly", boolean.class);
                      if (method != null) {
                          method.invoke(component, valueComponent);
                      }
                  } catch (Exception e1) {
                      // e.printStackTrace();//silently eat this exception.
                  }


              } catch (Exception e) {
                  // e.printStackTrace();//silently eat this exception.
              }
              List<UIComponent> childComponents = component.getChildren();
              for (UIComponent comp : childComponents) {
                  makeComponentHierarchyReadOnly(comp, valueComponent);
              }
          } 
 
    
    
    //Set Fields and Button disable.
        public void cevModeDisableComponent(String mode) {
            if (mode.equals("E")) {
                    getHeaderEditBinding().setDisabled(true);
                    getDetailcreateBinding().setDisabled(false);
                    
                    getSlCodeTypeBinding().setDisabled(true);
                    getSubTypeBinding().setDisabled(true);
                    getGlCdBinding().setDisabled(true);
                    // getGroupBinding().setDisabled(true);
                    getFinYearBinding().setDisabled(true);

            } else if (mode.equals("C")) {
                    getGlCdBinding().setDisabled(true);
                    getHeaderEditBinding().setDisabled(true);
                    getDetailcreateBinding().setDisabled(false);
                    getGroupCodeBinding().setDisabled(true);
                    getFinYearBinding().setDisabled(true);
            } else if (mode.equals("V")) {
                getDetailcreateBinding().setDisabled(true);
            
            }
            
        }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setSubTypeBinding(RichSelectOneChoice subTypeBinding) {
        this.subTypeBinding = subTypeBinding;
    }

    public RichSelectOneChoice getSubTypeBinding() {
        return subTypeBinding;
    }

    public void subTypeVCL(ValueChangeEvent vce) {
        System.out.println("subtype vce called");
              if(vce.getNewValue() != null){
                 System.out.println(" vce:"+vce.getNewValue()); 
                 System.out.println(" binding"+subTypeBinding.getValue());
                 if(vce.getNewValue().toString().equalsIgnoreCase("")){
                     System.out.println("blank");
                     getSlCodeTypeBinding().setDisabled(false);
                 }else{
                     System.out.println("not balnk");
                     getSlCodeTypeBinding().setDisabled(true);
                 }
              }
    }

    public void slCodeVCL(ValueChangeEvent valueChangeEvent) {
        System.out.println("slcodetype vce called");
        if(valueChangeEvent.getNewValue()!= null){
                    System.out.println(" valueChangeEvent:"+valueChangeEvent.getNewValue()); 
                    System.out.println(" binding"+slCodeTypeBinding.getValue());
                    if(valueChangeEvent.getNewValue().toString().equalsIgnoreCase("")){
                        System.out.println("blank");
                        getSubTypeBinding().setDisabled(false);
                        
                        
                    }else{
                        System.out.println("not balnk");
                        getSubTypeBinding().setDisabled(true);
                    }
                    
                }
    }


    public void setGroupBinding(RichSelectOneChoice groupBinding) {
        this.groupBinding = groupBinding;
    }

    public RichSelectOneChoice getGroupBinding() {
        return groupBinding;
    }

    public void setGroupCodeBinding(RichInputComboboxListOfValues groupCodeBinding) {
        this.groupCodeBinding = groupCodeBinding;
    }

    public RichInputComboboxListOfValues getGroupCodeBinding() {
        return groupCodeBinding;
    }

    public void groupVCE(ValueChangeEvent vce) 
    {
     vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
     if(vce!=null)
     {
    getGroupCodeBinding().setDisabled(false);
    AdfFacesContext.getCurrentInstance().addPartialTarget(groupCodeBinding);

    
     }
    }

    public void setFinYearBinding(RichInputComboboxListOfValues finYearBinding) {
        this.finYearBinding = finYearBinding;
    }

    public RichInputComboboxListOfValues getFinYearBinding() {
        return finYearBinding;
    }
}
