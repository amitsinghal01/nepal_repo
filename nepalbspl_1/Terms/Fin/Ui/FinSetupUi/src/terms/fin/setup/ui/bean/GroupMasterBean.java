package terms.fin.setup.ui.bean;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import javax.faces.validator.ValidatorException;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class GroupMasterBean {

    private RichTable groupMasterTableBinding;
    private String editAction="V";

    public GroupMasterBean() {
    }
    public void setValueToGrpCodeVCE(ValueChangeEvent vce) 
    {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        ADFUtils.findOperation("groupCodeValues").execute();    
    }

    public void fieldValueValidator(FacesContext facesContext, UIComponent uIComponent, Object object)
        {
            if(object !=null)
            {
           if(object.toString().length()>0)
               {
                //System.out.println("Value===> "+object+"  And Length ====> "+object.toString().length()); && !object.toString().contains(" ")
               if(object.toString().endsWith(".") && object.toString().trim().length()==3 )
                {
                   System.out.println("object.toString().substring(1,2)=>"+object.toString().substring(1,2));
                  
                if(object.toString().substring(1,2).equals("0") || object.toString().substring(1,2).equals("1") || object.toString().substring(1,2).equals("2") || object.toString().substring(1,2).equals("3") || object.toString().substring(1,2).equals("4") || object.toString().substring(1,2).equals("5") || object.toString().substring(1,2).equals("6")|| object.toString().substring(1,2).equals("7") || object.toString().substring(1,2).equals("8") || object.toString().substring(1,2).equals("9"))
                {
                   
                }
                else
                {
                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,"Value must ends with .(dot) and length must be 3 characters and It should not contain specific character.", null));

                }
                   
                }
               else
               {
              
                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Value must ends with .(dot) and length must be 3 characters and It should not contain specific character.", null));
                }  
              
               }
            }

        }


    public void deletePopupDialogDL(DialogEvent dialogEvent) 
    {
        if(dialogEvent.getOutcome().name().equals("ok"))
        {
        OperationBinding op = null;
        ADFUtils.findOperation("Delete").execute();
        op = (OperationBinding) ADFUtils.findOperation("Commit");
        if (getEditAction().equals("V")) {
        Object rst = op.execute();
        }
        System.out.println("Record Delete Successfully");
        if(op.getErrors().isEmpty()){
        FacesMessage Message = new FacesMessage("Record Deleted Successfully");
        Message.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        } else if (!op.getErrors().isEmpty())
        {
        OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
        Object rstr = opr.execute();
        FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
        Message.setSeverity(FacesMessage.SEVERITY_ERROR);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        }
        }
             AdfFacesContext.getCurrentInstance().addPartialTarget(groupMasterTableBinding);
        
    }

    public void setGroupMasterTableBinding(RichTable groupMasterTableBinding) {
        this.groupMasterTableBinding = groupMasterTableBinding;
    }

    public RichTable getGroupMasterTableBinding() {
        return groupMasterTableBinding;
    }

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }
}

