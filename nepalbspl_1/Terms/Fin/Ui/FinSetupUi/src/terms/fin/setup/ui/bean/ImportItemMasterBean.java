package terms.fin.setup.ui.bean;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.JboException;
import oracle.jbo.Row;

public class ImportItemMasterBean {
    private String editAction="V";
    private RichTable tableBinding;
    private RichInputText itemDescName;
    private RichInputText itemCodeBinding;
    String Code="";
    Integer CodeNo=0;
    

    public ImportItemMasterBean() {
    }

    public void deletedialogDL(DialogEvent dialogEvent) {

        if(dialogEvent.getOutcome().name().equals("ok")){
            OperationBinding op = null;
                ADFUtils.findOperation("Delete").execute();
                op = (OperationBinding) ADFUtils.findOperation("Commit");
        if (getEditAction().equals("V")) {
                 Object rst = op.execute();
        }
                        System.out.println("Record Delete Successfully");
        if(op.getErrors().isEmpty()){
             FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
             FacesContext fc = FacesContext.getCurrentInstance();
                 fc.addMessage(null, Message);
        } else if (!op.getErrors().isEmpty()){
             OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
                  Object rstr = opr.execute();
                FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc = FacesContext.getCurrentInstance();
                     fc.addMessage(null, Message);
             }
        }
             AdfFacesContext.getCurrentInstance().addPartialTarget(tableBinding);
        }


    public void setTableBinding(RichTable tableBinding) {
        this.tableBinding = tableBinding;
    }

    public RichTable getTableBinding() {
        return tableBinding;
    }

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public void saveAL(ActionEvent actionEvent) {
//        ADFUtils.findOperation("CreateInsert").execute();
//       ADFUtils.findOperation("getImportItemMaster").execute();
    }

    public void createAL(ActionEvent actionEvent) {
        {
            ADFUtils.findOperation("CreateInsert").execute();
              try
            {
              if(Code==null || Code =="")
             {
            OperationBinding opr=ADFUtils.findOperation("getImportItemMaster");  

                String obj=(String)opr.execute();
                 Code=obj;
                 System.out.println("Sushmita Jha's Code"+Code);
             }
             else
             {
              String NewCode=Code.substring(4);
              if(NewCode !=null)
              {
             CodeNo=Integer.parseInt(NewCode);
             System.out.println("CODE NO1="+CodeNo);
             CodeNo=CodeNo+1;
             System.out.println("CODE NO:="+CodeNo);
             //PART000
             if(CodeNo>=1 && CodeNo<=9)
             NewCode="I-0000"+CodeNo;
             else if(CodeNo>=10 && CodeNo<=99)
             NewCode="I-000"+CodeNo;               
             else if(CodeNo>=100 && CodeNo<1000)
             NewCode="I-00"+CodeNo;
            else if(CodeNo>=1000 && CodeNo<10000)
            NewCode="I-0"+CodeNo;
             else if(CodeNo>=10000)
                 NewCode="I-"+CodeNo;
             Code=NewCode;
             Row rr=(Row) ADFUtils.evaluateEL("#{bindings.ImportItemMasterVO1Iterator.currentRow}");
             rr.setAttribute("ItemCode",NewCode);           
              }
             }
            }
            catch (Exception ee)
            {
            ee.printStackTrace();
            ADFUtils.showMessage("Problem in number generation.Please Try Again.",0);
            }
//        ADFUtils.findOperation("CreateInsert").execute();
//        ADFUtils.findOperation("getImportItemMaster").execute();
       // #{bindings.ImportItemMasterVO1.collectionModel}
        }}

    public void ItemCodeVCE(ValueChangeEvent vce) {
        
    }

    public void setItemDescName(RichInputText itemDescName) {
        this.itemDescName = itemDescName;
    }

    public RichInputText getItemDescName() {
        return itemDescName;
    }

    public void setItemCodeBinding(RichInputText itemCodeBinding) {
        this.itemCodeBinding = itemCodeBinding;
    }

    public RichInputText getItemCodeBinding() {
        return itemCodeBinding;
    }

    public void saveAL1(ActionEvent actionEvent) {
        if(getItemDescName()==null){
      ADFUtils.showMessage("Item Desc must be Required", 2);
        }
        else{
            ADFUtils.findOperation("Commit").execute();
        }
    }

    public void CancelButtonAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("Rollback").execute();
        Code="";
    }
}
