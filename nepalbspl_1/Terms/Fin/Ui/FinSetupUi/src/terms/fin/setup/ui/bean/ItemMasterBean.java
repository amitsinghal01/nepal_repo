package terms.fin.setup.ui.bean;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class ItemMasterBean {
    private String editAction="V";
    private RichTable faItemMasterTableBinding;
    private RichOutputText itemGrpCd1;

    public ItemMasterBean() {
    }

    public void ItemMasterBean(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
        {
        OperationBinding op = null;
        ADFUtils.findOperation("Delete").execute();
        op = (OperationBinding) ADFUtils.findOperation("Commit");
        if (getEditAction().equals("V")) {
        Object rst = op.execute();
        }
        System.out.println("Record Delete Successfully");
        if(op.getErrors().isEmpty()){
        FacesMessage Message = new FacesMessage("Record Deleted Successfully");
        Message.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        } else if (!op.getErrors().isEmpty())
        {
        OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
        Object rstr = opr.execute();
        FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
        Message.setSeverity(FacesMessage.SEVERITY_ERROR);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        }
        }
             AdfFacesContext.getCurrentInstance().addPartialTarget(faItemMasterTableBinding);
        
        
        }

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public void setFaItemMasterTableBinding(RichTable faItemMasterTableBinding) {
        this.faItemMasterTableBinding = faItemMasterTableBinding;
    }

    public RichTable getFaItemMasterTableBinding() {
        return faItemMasterTableBinding;
    }

    public void saveAl(ActionEvent actionEvent) {
        System.out.println("Group Cd===>>"+itemGrpCd1.getValue());
        if(itemGrpCd1.getValue()==null){
            System.out.println("Grp"+itemGrpCd1.getValue());
            ADFUtils.findOperation("ItemGroupcd").execute();
            ADFUtils.findOperation("Commit").execute();
            ADFUtils.showMessage("Record Saved Sucessfully", 2);
        //        OperationBinding op = ADFUtils.findOperation("GroupNo");
        //        Object rst = op.execute();
        //
        //        ADFUtils.findOperation("Commit").execute();
        //
        //
        //        System.out.println("--------Commit-------");
        //            System.out.println("value aftr getting result--->?"+rst);
        //
        //
        //
        //                    FacesMessage Message = new FacesMessage("Record Saved Successfully. New groupCode is "+rst+".");
        //                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
        //                    FacesContext fc = FacesContext.getCurrentInstance();
        //                    fc.addMessage(null, Message);
        
                
            }
            
        //            if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ")
           else 
            {
                ADFUtils.findOperation("Commit").execute();
                ADFUtils.showMessage("Record Updated Sucessfully", 2);
            
                

            }
            }

    public void setItemGrpCd1(RichOutputText itemGrpCd1) {
        this.itemGrpCd1 = itemGrpCd1;
    }

    public RichOutputText getItemGrpCd1() {
        return itemGrpCd1;
    }
}
