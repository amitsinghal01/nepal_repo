package terms.fin.setup.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;

import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;

import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichPanelLabelAndMessage;

import oracle.adf.view.rich.component.rich.nav.RichButton;

import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

public class LcMasterBean {
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText unitNameBinding;
    private RichInputText lcSystemNumberBinding;
    private RichInputText lcNumberBinding;
    private RichInputDate amendmentDateBinding;
    private RichInputText excRateBinding;
    private RichInputComboboxListOfValues bankCodeBinding;
    private RichInputText buyersCreditBinding;
    private RichInputText vendorNameBinding;
    private RichInputDate negotiationDateBinding;
    private RichInputDate lcDateBinding;
    private RichInputText amendmentNumberBinding;
    private RichInputText amountFCBinding;
    private RichInputText daysBinding;
    private RichInputText bankNameBinding;
    private RichInputComboboxListOfValues vendorCodeBinding;
    private RichInputDate expiryDateBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichOutputText bindingOutputText;

    public LcMasterBean() {
    }

    public void saveButtonAL(ActionEvent actionEvent) {

       // ADFUtils.findOperation("CreateInsert").execute();
//        Row rr=(Row)ADFUtils.evaluateEL("#{bindings.AdvanceLicenceDetailVO1Iterator.currentRow}");
//        rr.setAttribute("LicNo",ADFUtils.evaluateEL("#{bindings.LicNo.inputValue}"));
//        System.out.println("Licence Number is"+rst);
        
        

               OperationBinding op = ADFUtils.findOperation("getLcNumber");
               Object rst = op.execute();
               
               System.out.println("op.getResult()"+op.getResult());
               if(op.getResult()!=null && op.getResult().equals("C"))
               {
                   ADFUtils.findOperation("Commit").execute();
                   ADFUtils.showMessage("Record Saved Successfully. LC Number is "+lcNumberBinding.getValue()+".", 2);
                }
                else if(op.getResult()!=null &&op.getResult().equals("U"))
                {
                    ADFUtils.findOperation("Commit").execute();
                    ADFUtils.showMessage("Record Updated Successfully.", 2);
                }
               else if(op.getResult().equals("N"))
               {
                   ADFUtils.showMessage("Problem In Number Generation.Please Check.",0);
               }
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setUnitNameBinding(RichInputText unitNameBinding) {
        this.unitNameBinding = unitNameBinding;
    }

    public RichInputText getUnitNameBinding() {
        return unitNameBinding;
    }

    public void setLcSystemNumberBinding(RichInputText lcSystemNumberBinding) {
        this.lcSystemNumberBinding = lcSystemNumberBinding;
    }

    public RichInputText getLcSystemNumberBinding() {
        return lcSystemNumberBinding;
    }

    public void setLcNumberBinding(RichInputText lcNumberBinding) {
        this.lcNumberBinding = lcNumberBinding;
    }

    public RichInputText getLcNumberBinding() {
        return lcNumberBinding;
    }

    public void setAmendmentDateBinding(RichInputDate amendmentDateBinding) {
        this.amendmentDateBinding = amendmentDateBinding;
    }

    public RichInputDate getAmendmentDateBinding() {
        return amendmentDateBinding;
    }

    public void setExcRateBinding(RichInputText excRateBinding) {
        this.excRateBinding = excRateBinding;
    }

    public RichInputText getExcRateBinding() {
        return excRateBinding;
    }

    public void setBankCodeBinding(RichInputComboboxListOfValues bankCodeBinding) {
        this.bankCodeBinding = bankCodeBinding;
    }

    public RichInputComboboxListOfValues getBankCodeBinding() {
        return bankCodeBinding;
    }

    public void setBuyersCreditBinding(RichInputText buyersCreditBinding) {
        this.buyersCreditBinding = buyersCreditBinding;
    }

    public RichInputText getBuyersCreditBinding() {
        return buyersCreditBinding;
    }

    public void setVendorNameBinding(RichInputText vendorNameBinding) {
        this.vendorNameBinding = vendorNameBinding;
    }

    public RichInputText getVendorNameBinding() {
        return vendorNameBinding;
    }

    public void setNegotiationDateBinding(RichInputDate negotiationDateBinding) {
        this.negotiationDateBinding = negotiationDateBinding;
    }

    public RichInputDate getNegotiationDateBinding() {
        return negotiationDateBinding;
    }

    public void setLcDateBinding(RichInputDate lcDateBinding) {
        this.lcDateBinding = lcDateBinding;
    }

    public RichInputDate getLcDateBinding() {
        return lcDateBinding;
    }

    public void setAmendmentNumberBinding(RichInputText amendmentNumberBinding) {
        this.amendmentNumberBinding = amendmentNumberBinding;
    }

    public RichInputText getAmendmentNumberBinding() {
        return amendmentNumberBinding;
    }

    public void setAmountFCBinding(RichInputText amountFCBinding) {
        this.amountFCBinding = amountFCBinding;
    }

    public RichInputText getAmountFCBinding() {
        return amountFCBinding;
    }

    public void setDaysBinding(RichInputText daysBinding) {
        this.daysBinding = daysBinding;
    }

    public RichInputText getDaysBinding() {
        return daysBinding;
    }

    public void setBankNameBinding(RichInputText bankNameBinding) {
        this.bankNameBinding = bankNameBinding;
    }

    public RichInputText getBankNameBinding() {
        return bankNameBinding;
    }

    public void setVendorCodeBinding(RichInputComboboxListOfValues vendorCodeBinding) {
        this.vendorCodeBinding = vendorCodeBinding;
    }

    public RichInputComboboxListOfValues getVendorCodeBinding() {
        return vendorCodeBinding;
    }

    public void setExpiryDateBinding(RichInputDate expiryDateBinding) {
        this.expiryDateBinding = expiryDateBinding;
    }

    public RichInputDate getExpiryDateBinding() {
        return expiryDateBinding;
    }


    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }
    
    private void cevmodecheck(){
          if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
              makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
              cevModeDisableComponent("V");
          }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
              makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
              cevModeDisableComponent("C");
          }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
              makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
              cevModeDisableComponent("E");
          }
      }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
              try {
                  Method method1 =
                      component.getClass().getMethod("setDisabled", boolean.class);
                  if (method1 != null) {
                      method1.invoke(component, valueComponent);
                  }
              } catch (NoSuchMethodException e) {
                  try {
                      Method method =
                          component.getClass().getMethod("setReadOnly", boolean.class);
                      if (method != null) {
                          method.invoke(component, valueComponent);
                      }
                  } catch (Exception e1) {
                      // e.printStackTrace();//silently eat this exception.
                  }


              } catch (Exception e) {
                  // e.printStackTrace();//silently eat this exception.
              }
              List<UIComponent> childComponents = component.getChildren();
              for (UIComponent comp : childComponents) {
                  makeComponentHierarchyReadOnly(comp, valueComponent);
              }
          }
    
    //Set Fields and Button disable.
        public void cevModeDisableComponent(String mode) {
            if (mode.equals("E")) {
                getUnitCodeBinding().setDisabled(true);
                getUnitNameBinding().setDisabled(true);
                getLcNumberBinding().setDisabled(true);
                getBankNameBinding().setDisabled(true);
                getVendorNameBinding().setDisabled(true);
                getLcDateBinding().setDisabled(true);
                getAmendmentNumberBinding().setDisabled(true);
            } else if (mode.equals("C")) {
                getUnitCodeBinding().setDisabled(true);
                getUnitNameBinding().setDisabled(true);
             getLcNumberBinding().setDisabled(true);
             getBankNameBinding().setDisabled(true);
             getVendorNameBinding().setDisabled(true);
            getAmendmentNumberBinding().setDisabled(true);
            } else if (mode.equals("V")) {
               
            
        }

}

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void amendmentPopupDialogDL(DialogEvent dialogEvent) {
        String LcNumber=lcNumberBinding.getValue().toString();
        String amdNo=amendmentNumberBinding.getValue().toString();
        
        if (dialogEvent.getOutcome().name().equals("ok")) {
         
            if(amendmentNumberBinding != null && lcNumberBinding != null){           
                   // ADFUtils.findOperation("CreateInsert").execute();
                    OperationBinding op = ADFUtils.findOperation("amdDocumentLcMaster");
                    Object rst = op.execute();
                    System.out.println("Record Create Successfully");   
                   // Message="A";
                }
                    

        }
        
                      //AdfFacesContext.getCurrentInstance().addPartialTarget(lcMastertableBinding);
    }
}
