package terms.fin.setup.ui.bean;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class LicenceMasterBean {
    private RichTable tableBinding;

    public LicenceMasterBean() {
    }

    public void deletePopupDL(DialogEvent dialogEvent) {
            if (dialogEvent.getOutcome().name().equals("ok")) {
                ADFUtils.findOperation("Delete").execute();
                System.out.println("Record Delete Successfully");

                FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);

        }
             AdfFacesContext.getCurrentInstance().addPartialTarget(tableBinding);
        }

    public void setTableBinding(RichTable tableBinding) {
        this.tableBinding = tableBinding;
    }

    public RichTable getTableBinding() {
        return tableBinding;
    }
}

