package terms.fin.setup.ui.bean;

import oracle.adf.view.rich.component.rich.data.RichTable;

public class PartyGlMapBean {
    private RichTable bindPartyGlMapTableBinding;
    private String editAction="V";

    public PartyGlMapBean() {
    }

    public void setBindPartyGlMapTableBinding(RichTable bindPartyGlMapTableBinding) {
        this.bindPartyGlMapTableBinding = bindPartyGlMapTableBinding;
    }

    public RichTable getBindPartyGlMapTableBinding() {
        return bindPartyGlMapTableBinding;
    }
    
    public void setEditAction(String editAction) {
            this.editAction = editAction;
        }

        public String getEditAction() {
            return editAction;
        }
}
