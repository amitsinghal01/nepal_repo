package terms.fin.setup.ui.bean;

import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;

public class PartyGlMappingBean {
    private String editAction="V";
    
    private RichTable bindPartyGlMappingTableBinding;

    public PartyGlMappingBean() {
    }

    public void setBindPartyGlMappingTableBinding(RichTable bindPartyGlMappingTableBinding) {
        this.bindPartyGlMappingTableBinding = bindPartyGlMappingTableBinding;
    }

    public RichTable getBindPartyGlMappingTableBinding() {
        return bindPartyGlMappingTableBinding;
    }
    
    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public void glCodeVCE(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
    valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
    System.out.println("valueChangeEvent"+valueChangeEvent.getNewValue());
    if(valueChangeEvent.getNewValue()!=null)
    ADFUtils.findOperation("updateValueInVendUnitpartyGLMapping").execute();
    }
}
