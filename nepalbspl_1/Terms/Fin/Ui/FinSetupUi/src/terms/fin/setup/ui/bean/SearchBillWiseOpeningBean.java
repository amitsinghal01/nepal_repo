package terms.fin.setup.ui.bean;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;


public class SearchBillWiseOpeningBean {
    private RichTable searchBillWiseOpeningTableBinding;
    private RichOutputText refNoTableBinding;

    public SearchBillWiseOpeningBean() {
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
            {
                ADFUtils.findOperation("Delete").execute();
                OperationBinding op = (OperationBinding) ADFUtils.findOperation("Commit");
                Object rst = op.execute();
                System.out.println("Record Delete Successfully");
               
                if(op.getErrors().isEmpty())
                {
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");   
                Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                FacesContext fc = FacesContext.getCurrentInstance();   
                fc.addMessage(null, Message);
                }
                else
                               {
                               FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                               Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                               FacesContext fc = FacesContext.getCurrentInstance();
                               fc.addMessage(null, Message);
                                }
            
            }
        AdfFacesContext.getCurrentInstance().addPartialTarget(searchBillWiseOpeningTableBinding);
    }

    public void setSearchBillWiseOpeningTableBinding(RichTable searchBillWiseOpeningTableBinding) {
        this.searchBillWiseOpeningTableBinding = searchBillWiseOpeningTableBinding;
    }

    public RichTable getSearchBillWiseOpeningTableBinding() {
        return searchBillWiseOpeningTableBinding;
    }

   

    public String editBillwiseAC() {
        OperationBinding binding = (OperationBinding)ADFUtils.findOperation("viewBillWiseOp");
        binding.getParamsMap().put("view_mode", "C");
        binding.execute();
        return "goToNext";

    }

    public String viewBillWiseOpAction() {
        System.out.println("in lionk metghoddddd");
        OperationBinding binding = (OperationBinding)ADFUtils.findOperation("viewBillWiseOp");
        binding.getParamsMap().put("view_mode", "L");
        binding.execute();
        System.out.println("123456778");
        return "goToNext";
    }

    public void setRefNoTableBinding(RichOutputText refNoTableBinding) {
        this.refNoTableBinding = refNoTableBinding;
    }

    public RichOutputText getRefNoTableBinding() {
        return refNoTableBinding;
    }
}
