package terms.fin.setup.ui.bean;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import javax.faces.validator.ValidatorException;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class SearchGeneralLedgerBean {
    private RichTable searchTableBinding;

    public SearchGeneralLedgerBean() {
    }

    public void setSearchTableBinding(RichTable searchTableBinding) {
        this.searchTableBinding = searchTableBinding;
    }

    public RichTable getSearchTableBinding() {
        return searchTableBinding;
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            OperationBinding op1 = ADFUtils.findOperation("checkDeleteGlCodeSearch");
            Object rst1 = op1.execute();

            System.out.println("checkSubCodeType result rst.toString():" + rst1.toString());
            System.out.println("checkSubCodeType result op.getresult():" + op1.getResult());
            System.out.println("op.getResult().toString().equalsIgnoreCase(\"false\")" +
                               op1.getResult().toString().equalsIgnoreCase("false") + "*********" +
                               op1.getResult().toString().equalsIgnoreCase("true"));
            if (op1.getResult().toString().equalsIgnoreCase("false")) {
                FacesMessage Message = new FacesMessage("This record cannot be deleted. GL Code is already mapped.");
                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            } else {

                ADFUtils.findOperation("Delete").execute();
                OperationBinding op = ADFUtils.findOperation("Commit");
                Object rst = op.execute();
                System.out.println("Record Delete Successfully");

                if (op.getErrors().isEmpty()) {
                    FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);
                } else {
                    FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                    Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);
                }
            }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(searchTableBinding);
    }
}
