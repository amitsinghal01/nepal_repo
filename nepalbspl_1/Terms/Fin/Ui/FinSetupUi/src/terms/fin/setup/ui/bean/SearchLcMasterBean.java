package terms.fin.setup.ui.bean;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class SearchLcMasterBean {
    private RichTable lcMastertableBinding;

    public SearchLcMasterBean() {
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete").execute();
            OperationBinding op=  ADFUtils.findOperation("Commit");
            Object rst = op.execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }
        
                      AdfFacesContext.getCurrentInstance().addPartialTarget(lcMastertableBinding);
    }

    public void setLcMastertableBinding(RichTable lcMastertableBinding) {
        this.lcMastertableBinding = lcMastertableBinding;
    }

    public RichTable getLcMastertableBinding() {
        return lcMastertableBinding;
    }
}
