package terms.fin.setup.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputText;

import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;

import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.binding.OperationBinding;

public class VoucherMonthClosingBean {
    private RichInputText voucherTypeBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichOutputText outputTextBinding;
    private RichInputText voucherDescriptionBinding;
    private RichButton headerEditBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputComboboxListOfValues finYearBinding;
    private RichButton populateButtonBinding;

    public VoucherMonthClosingBean() {
    }

    public void populateMonthlyVoucherAL(ActionEvent actionEvent) {
        // Add event code here...
        
         OperationBinding opp=ADFUtils.findOperation("voucherClosedPopulate");
        
        Object rst =opp.execute();
        if(rst!=null && rst.toString().equalsIgnoreCase("N"))
        {
             ADFUtils.showMessage("Financial year is required!!", 0);
        }
    }

    public void saveMonthlyVoucherAL(ActionEvent actionEvent) {
       
        Integer i=0;
        i=(Integer)outputTextBinding.getValue();
        if(i>0)
        {
            
        ADFUtils.findOperation("Commit").execute();
        FacesMessage Message = new FacesMessage("Record Update Successfully");   
        Message.setSeverity(FacesMessage.SEVERITY_INFO);   
        FacesContext fc = FacesContext.getCurrentInstance();   
        fc.addMessage(null, Message);      
         
        }
        else
        
          { 
             
         ADFUtils.findOperation("Commit").execute();
         FacesMessage Message = new FacesMessage("Record Save Successfully");   
         Message.setSeverity(FacesMessage.SEVERITY_INFO);   
         FacesContext fc = FacesContext.getCurrentInstance();   
         fc.addMessage(null, Message);      
         }
    }

    public String saveAndCloseMonthlyVoucherAL(ActionEvent actionEvent) {
       
        ADFUtils.findOperation("saveData").execute(); 
       // ADFUtils.findOperation("Commit").execute(); 
        String param=resolvEl("#{pageFlowScope.mode=='E'}"); 
        System.out.println("Mode is ====> "+param);
        if(param.equalsIgnoreCase("true"))
        {
            
        ADFUtils.findOperation("Commit").execute();
        FacesMessage Message = new FacesMessage("Record Updated Successfully");   
        Message.setSeverity(FacesMessage.SEVERITY_INFO);   
        FacesContext fc = FacesContext.getCurrentInstance();   
        fc.addMessage(null, Message);      
        return "save and cancel";
        }
        else
        
          { 
             
         ADFUtils.findOperation("Commit").execute();
         FacesMessage Message = new FacesMessage("Record Saved Successfully");   
         Message.setSeverity(FacesMessage.SEVERITY_INFO);   
         FacesContext fc = FacesContext.getCurrentInstance();   
         fc.addMessage(null, Message);    
         return "save and cancel";
         }
        //return null;
    }

    public void onPageLoad() {
        OperationBinding opp1=ADFUtils.findOperation("CreateFilter");
        opp1.execute();

          OperationBinding opp=ADFUtils.findOperation("CreateInsertPopululate");
         Object obj=opp.execute(); 
        
    }
    public String resolvEl(String data)
    {
               FacesContext fc = FacesContext.getCurrentInstance();
               Application app = fc.getApplication();
               ExpressionFactory elFactory = app.getExpressionFactory();
               ELContext elContext = fc.getELContext();
               ValueExpression valueExp = elFactory.createValueExpression(elContext, data, Object.class);
               String Message=valueExp.getValue(elContext).toString();
               return Message;
    }



    public void setVoucherTypeBinding(RichInputText voucherTypeBinding) {
        this.voucherTypeBinding = voucherTypeBinding;
    }

    public RichInputText getVoucherTypeBinding() {
        return voucherTypeBinding;
    }


    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setOutputTextBinding(RichOutputText outputTextBinding) {
        this.outputTextBinding = outputTextBinding;
    }

    public RichOutputText getOutputTextBinding() {
        cevmodecheck();
        return outputTextBinding;
    }
    private void cevmodecheck(){
            if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
                cevModeDisableComponent("V");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("C");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("E");
            }
        }
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                try {
                    Method method1 =
                        component.getClass().getMethod("setDisabled", boolean.class);
                    if (method1 != null) {
                        method1.invoke(component, valueComponent);
                    }
                } catch (NoSuchMethodException e) {
                    try {
                        Method method =
                            component.getClass().getMethod("setReadOnly", boolean.class);
                        if (method != null) {
                            method.invoke(component, valueComponent);
                        }
                    } catch (Exception e1) {
                        // e.printStackTrace();//silently eat this exception.
                    }


                } catch (Exception e) {
                    // e.printStackTrace();//silently eat this exception.
                }
                List<UIComponent> childComponents = component.getChildren();
                for (UIComponent comp : childComponents) {
                    makeComponentHierarchyReadOnly(comp, valueComponent);
                }
            }
    
    public void cevModeDisableComponent(String mode) {
     //            FacesContext fctx = FacesContext.getCurrentInstance();
     //            ELContext elctx = fctx.getELContext();
     //            Application jsfApp = fctx.getApplication();
     //            //create a ValueExpression that points to the ADF binding layer
     //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
     //            //
     //            ValueExpression valueExpr = exprFactory.createValueExpression(
     //                                         elctx,
     //                                         "#{pageFlowScope.mode=='E'}",
     //                                          Object.class
     //                                         );
     //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
         if (mode.equals("E")) {
             getUnitCodeBinding().setDisabled(true);
             getFinYearBinding().setDisabled(true);
           getHeaderEditBinding().setDisabled(true);
             getVoucherTypeBinding().setDisabled(true);
             getVoucherDescriptionBinding().setDisabled(true);
             getPopulateButtonBinding().setDisabled(true);
         } else if (mode.equals("C")) {
             getUnitCodeBinding().setDisabled(true);
             getFinYearBinding().setDisabled(false);
             getHeaderEditBinding().setDisabled(true);
             getVoucherTypeBinding().setDisabled(true);
             getVoucherDescriptionBinding().setDisabled(true);
         } else if (mode.equals("V")) {
            
         }
         
     }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setVoucherDescriptionBinding(RichInputText voucherDescriptionBinding) {
        this.voucherDescriptionBinding = voucherDescriptionBinding;
    }

    public RichInputText getVoucherDescriptionBinding() {
        return voucherDescriptionBinding;
    }


    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setFinYearBinding(RichInputComboboxListOfValues finYearBinding) {
        this.finYearBinding = finYearBinding;
    }

    public RichInputComboboxListOfValues getFinYearBinding() {
        return finYearBinding;
    }

    public void setPopulateButtonBinding(RichButton populateButtonBinding) {
        this.populateButtonBinding = populateButtonBinding;
    }

    public RichButton getPopulateButtonBinding() {
        return populateButtonBinding;
    }
}
