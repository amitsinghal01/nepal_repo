package terms.fin.setup.ui.bean;

import java.math.BigDecimal;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import javax.faces.validator.ValidatorException;

import oracle.adf.model.OperationBinding;
import oracle.adf.view.rich.component.rich.RichDialog;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

public class VoucherSeriesBean {
    private RichTable voucherSeriesTableBinding;
  private String editAction="V";
    private RichInputText startNoBinding;
    private RichInputText endNoBinding;

    public VoucherSeriesBean() {
    }

    public void VoucherSeriesDeletePopupDialogDL(DialogEvent dialogEvent) {
            {
            if(dialogEvent.getOutcome().name().equals("ok"))
            {
            OperationBinding op = null;
            ADFUtils.findOperation("Delete").execute();
            op = (OperationBinding) ADFUtils.findOperation("Commit");
            if (getEditAction().equals("V")) {
            Object rst = op.execute();
            }
            System.out.println("Record Delete Successfully");
            if(op.getErrors().isEmpty()){
            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
            } else if (!op.getErrors().isEmpty())
            {
            OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
            Object rstr = opr.execute();
            FacesMessage Message = new FacesMessage("Record cannot be deleted. Child recordfound.");
            Message.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
            }
            }
            AdfFacesContext.getCurrentInstance().addPartialTarget(voucherSeriesTableBinding);
            }
    }
        
    

    public void setVoucherSeriesTableBinding(RichTable voucherSeriesTableBinding){
        this.voucherSeriesTableBinding = voucherSeriesTableBinding;
    }

    public RichTable getVoucherSeriesTableBinding() {
        return voucherSeriesTableBinding;
    }

  


    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public void endNumberValidator(FacesContext facesContext, UIComponent uIComponent, Object object) 
    {
    if(object !=null && startNoBinding.getValue() !=null)
    {
    BigDecimal EndNo=(BigDecimal)object;
    BigDecimal StartNo=(BigDecimal)startNoBinding.getValue();
    if(EndNo.compareTo(StartNo)==-1)
    {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "End No should be greater than Start No.", null));
    }
 
    }

    }

    public void setStartNoBinding(RichInputText startNoBinding) {
        this.startNoBinding = startNoBinding;
    }

    public RichInputText getStartNoBinding() 
    {
        return startNoBinding;
    }

    public void startNoValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if(object !=null && endNoBinding.getValue() !=null)
        {
        BigDecimal StartNo=(BigDecimal)object;
        BigDecimal EndNo=(BigDecimal)endNoBinding.getValue();
        if(StartNo.compareTo(EndNo)==1)
        {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Start No should be less than End No.", null));
        }
        
        }

    }

    public void setEndNoBinding(RichInputText endNoBinding) {
        this.endNoBinding = endNoBinding;
    }

    public RichInputText getEndNoBinding() {
        return endNoBinding;
    }
}
