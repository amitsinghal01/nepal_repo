package terms.fin.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

public class AdvanceLicenceBean {
    private RichInputComboboxListOfValues bindUnitCode;
    private RichInputText bindUnitDescription;
    private RichInputText bindLecenceNumber;
    private RichInputText bindLicenceNumber1;
    private RichInputDate bindLicenceDate;
    private RichInputDate bindEffectiveDate;
    private RichInputDate bindEffectiveTillDate;
    private RichInputComboboxListOfValues bindProductCode;
    private RichInputText bindCustomerProductCode;
    private RichInputText bindQuality;
    private RichInputText bindValue;
    private RichInputText bindDescription;
    private RichOutputText bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichButton detailcreateBinding;
    private RichButton detaildeleteBinding;
    private RichTable advanceLicenceDetailTableBinding;
    private String Message="C";
    private RichSelectOneChoice bindLicenceType;

    public AdvanceLicenceBean() {
    }
    
    private void cevmodecheck(){

     if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
                cevModeDisableComponent("V");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("C");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("E");
            }
        }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
               try {
                   Method method1 =
                       component.getClass().getMethod("setDisabled", boolean.class);
                   if (method1 != null) {
                       method1.invoke(component, valueComponent);
                   }
               } catch (NoSuchMethodException e) {
                   try {
                       Method method =
                           component.getClass().getMethod("setReadOnly", boolean.class);
                       if (method != null) {
                           method.invoke(component, valueComponent);
                       }
                   } catch (Exception e1) {
                       // e.printStackTrace();//silently eat this exception.
                   }


               } catch (Exception e) {
                   // e.printStackTrace();//silently eat this exception.
               }
               List<UIComponent> childComponents = component.getChildren();
               for (UIComponent comp : childComponents) {
                   makeComponentHierarchyReadOnly(comp, valueComponent);
               }
           }
    
    
    //Set Fields and Button disable.
        public void cevModeDisableComponent(String mode) {
        //            FacesContext fctx = FacesContext.getCurrentInstance();
        //            ELContext elctx = fctx.getELContext();
        //            Application jsfApp = fctx.getApplication();
        //            //create a ValueExpression that points to the ADF binding layer
        //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
        //            //
        //            ValueExpression valueExpr = exprFactory.createValueExpression(
        //                                         elctx,
        //                                         "#{pageFlowScope.mode=='E'}",
        //                                          Object.class
        //                                         );
        //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
            if (mode.equals("E")) {
                Message="E";
                    getBindUnitCode().setDisabled(true);
                    getBindUnitDescription().setDisabled(true);
                    getBindLecenceNumber().setDisabled(true);
                    getBindCustomerProductCode().setDisabled(true);
                 //   getBindLicenceDate().setDisabled(true);
                    getHeaderEditBinding().setDisabled(true);
                    getDetailcreateBinding().setDisabled(false);
                    getDetaildeleteBinding().setDisabled(false);
            } else if (mode.equals("C")) {
                getBindUnitCode().setDisabled(true);
                getBindUnitDescription().setDisabled(true);
                getBindLecenceNumber().setDisabled(true);
                getBindCustomerProductCode().setDisabled(true);
              //  getBindLicenceDate().setDisabled(true);
                getHeaderEditBinding().setDisabled(true);
                //getDetailcreateBinding().setDisabled(false);
                //getDetaildeleteBinding().setDisabled(false);
            } else if (mode.equals("V")) {
                getDetailcreateBinding().setDisabled(true);
                getDetaildeleteBinding().setDisabled(true);
            }
            
        }

    public void setBindUnitCode(RichInputComboboxListOfValues bindUnitCode) {
        this.bindUnitCode = bindUnitCode;
    }

    public RichInputComboboxListOfValues getBindUnitCode() {
        return bindUnitCode;
    }

    public void setBindUnitDescription(RichInputText bindUnitDescription) {
        this.bindUnitDescription = bindUnitDescription;
    }

    public RichInputText getBindUnitDescription() {
        return bindUnitDescription;
    }

    public void setBindLecenceNumber(RichInputText bindLecenceNumber) {
        this.bindLecenceNumber = bindLecenceNumber;
    }

    public RichInputText getBindLecenceNumber() {
        return bindLecenceNumber;
    }

    public void setBindLicenceNumber1(RichInputText bindLicenceNumber1) {
        this.bindLicenceNumber1 = bindLicenceNumber1;
    }

    public RichInputText getBindLicenceNumber1() {
        return bindLicenceNumber1;
    }

    public void setBindLicenceDate(RichInputDate bindLicenceDate) {
        this.bindLicenceDate = bindLicenceDate;
    }

    public RichInputDate getBindLicenceDate() {
        return bindLicenceDate;
    }

    public void setBindEffectiveDate(RichInputDate bindEffectiveDate) {
        this.bindEffectiveDate = bindEffectiveDate;
    }

    public RichInputDate getBindEffectiveDate() {
        return bindEffectiveDate;
    }

    public void setBindEffectiveTillDate(RichInputDate bindEffectiveTillDate) {
        this.bindEffectiveTillDate = bindEffectiveTillDate;
    }

    public RichInputDate getBindEffectiveTillDate() {
        return bindEffectiveTillDate;
    }

    public void setBindProductCode(RichInputComboboxListOfValues bindProductCode) {
        this.bindProductCode = bindProductCode;
    }

    public RichInputComboboxListOfValues getBindProductCode() {
        return bindProductCode;
    }

    public void setBindCustomerProductCode(RichInputText bindCustomerProductCode) {
        this.bindCustomerProductCode = bindCustomerProductCode;
    }

    public RichInputText getBindCustomerProductCode() {
        return bindCustomerProductCode;
    }


    public void setBindQuality(RichInputText bindQuality) {
        this.bindQuality = bindQuality;
    }

    public RichInputText getBindQuality() {
        return bindQuality;
    }

    public void setBindValue(RichInputText bindValue) {
        this.bindValue = bindValue;
    }

    public RichInputText getBindValue() {
        return bindValue;
    }

    public void setBindDescription(RichInputText bindDescription) {
        this.bindDescription = bindDescription;
    }

    public RichInputText getBindDescription() {
        return bindDescription;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setDetailcreateBinding(RichButton detailcreateBinding) {
        this.detailcreateBinding = detailcreateBinding;
    }

    public RichButton getDetailcreateBinding() {
        return detailcreateBinding;
    }

    public void setDetaildeleteBinding(RichButton detaildeleteBinding) {
        this.detaildeleteBinding = detaildeleteBinding;
    }

    public RichButton getDetaildeleteBinding() {
        return detaildeleteBinding;
    }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
        // Add event code here...
    }


    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }
        
                      AdfFacesContext.getCurrentInstance().addPartialTarget(advanceLicenceDetailTableBinding);
    }

    public void setAdvanceLicenceDetailTableBinding(RichTable advanceLicenceDetailTableBinding) {
        this.advanceLicenceDetailTableBinding = advanceLicenceDetailTableBinding;
    }

    public RichTable getAdvanceLicenceDetailTableBinding() {
        return advanceLicenceDetailTableBinding;
    }

    public void saveButtonAL(ActionEvent actionEvent) {
        System.out.println("Mode Message"+Message);
        
        if(Message.equalsIgnoreCase("C"))
               {
            if(bindLecenceNumber.getValue()==null){
//                   OperationBinding op = ADFUtils.findOperation("getLicenceNumber");
//                   Object rst = op.execute();
                   
                   System.out.println("Mode Message 1"+Message);
                   ADFUtils.findOperation("Commit").execute();
                   ADFUtils.showMessage("Record Saved Successfully. New Entry Number is "+bindLecenceNumber+".", 2);
            }
            else if (bindLecenceNumber.getValue()!=null)
            {
                    ADFUtils.findOperation("Commit").execute();
                    ADFUtils.showMessage("Record Saved Successfully. New Entry Number is "+bindLecenceNumber.getValue()+".", 2);
                }
               }
               else if(Message.equalsIgnoreCase("E"))
               {
                   System.out.println("Mode Message 2"+Message);
                   ADFUtils.findOperation("Commit").execute();
                   ADFUtils.showMessage("Record Updated Successfully.", 2);
               }
    }

    public void createButtonAL(ActionEvent actionEvent) {
        OperationBinding op = ADFUtils.findOperation("getLicenceNumber");
        Object rst = op.execute();
        ADFUtils.findOperation("CreateInsert").execute();
        Row rr=(Row)ADFUtils.evaluateEL("#{bindings.AdvanceLicenceDetailVO1Iterator.currentRow}");
        rr.setAttribute("LicNo",ADFUtils.evaluateEL("#{bindings.LicNo.inputValue}"));
        System.out.println("Licence Number is"+rst);
    }

    public void setBindLicenceType(RichSelectOneChoice bindLicenceType) {
        this.bindLicenceType = bindLicenceType;
    }

    public RichSelectOneChoice getBindLicenceType() {
        return bindLicenceType;
    }
}
