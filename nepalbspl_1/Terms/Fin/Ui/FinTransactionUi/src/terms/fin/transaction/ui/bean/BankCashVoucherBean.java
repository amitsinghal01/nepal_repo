package terms.fin.transaction.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import javax.faces.validator.ValidatorException;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;

import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;

import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;

import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichButton;


import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.adf.view.rich.event.ReturnPopupEvent;

import oracle.jbo.domain.Date;

import oracle.binding.OperationBinding;

import terms.fin.transaction.model.view.BankCashVoucherHeaderVORowImpl;
import terms.fin.transaction.model.view.BankCashVoucherDetailVORowImpl;


import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;


public class BankCashVoucherBean {
    private RichInputComboboxListOfValues unitBinding;
    private RichInputComboboxListOfValues controlCodeBinding;
    private RichInputDate vouDateBinding;
    private RichPopup bindCondtnPopUp;
    private RichSelectOneChoice vouTypeBind;
    private RichInputText yearBind;
    private RichInputComboboxListOfValues currCodeBind;
    private RichInputText fcAmountBind;
    private RichInputText headerCurrCode;
    private RichSelectOneChoice chequesBinding;
    private RichInputText debitBinding;
    private RichInputText creditBinding;
    private RichInputText glODLimitBinding;
    private RichInputText voucherNoBinding;
    private RichInputText inFavourBinding;
    private RichInputText glBalBinding;
    private RichInputComboboxListOfValues tdsCodeBinding;
    private RichInputComboboxListOfValues preparedByBinding;
    private RichInputText controlCodeDescBinding;
    private RichInputText tdsPayAmtBinding;
    private RichInputText diffBinding;
    private RichInputText voucherAmountBinding;
    private RichInputText seqNoBinding;
    private RichInputComboboxListOfValues acHeadsBinding;
    private RichInputText acDescBinding;
    private RichInputComboboxListOfValues glCodeBinding;
    private RichInputText glDescBinding;
    private RichInputText glBalDetailBinding;
    private RichInputText currRateBinding;
    private RichSelectOneChoice drCrBinding;
    private RichInputComboboxListOfValues costCentreBinding;
    private RichInputText amount;
    private RichInputText narrationBinding;
    private RichInputText budAmtBinding;
    private RichInputComboboxListOfValues projCdBinding;
    private RichInputText finTvouBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichButton detailcreateBinding;
    private RichOutputText outputTextBinding;
    private RichTable bankCashVoucherTableBinding;
    private RichInputComboboxListOfValues verifiedByBinding;
    private RichInputComboboxListOfValues approvedByBinding;
    private RichInputDate approvedOnBinding;
    private RichInputComboboxListOfValues vouSeriesBinding;
    private RichInputText glCreditDebitBinding;
    private String button = "";
    private String saveAndClose = "true";
    private RichInputText transBankCodeBinding;
    private RichInputText checkNoBinding;
    private RichInputDate checkRtgsDateBinding;
    private RichInputText transGlielaBinding;
    private RichShowDetailItem narrationDetailTabBinding;
    private RichShowDetailItem voucherDetailTabBinding;
    private BigDecimal zero = new BigDecimal(0);
    private BigDecimal totalcreditValue;
    private BigDecimal totaldebitValue;
    private BigDecimal diffAmount = zero;

    public BankCashVoucherBean() {
    }

    public void controlCodeVCL(ValueChangeEvent valueChangeEvent) {

        System.out.println("In control code VCL Unit:" + unitBinding.getValue() + "control code:" +
                           controlCodeBinding.getValue() + "Vou Date:" + vouDateBinding.getValue() + " year:" +
                           yearBind.getValue());

        OperationBinding op = ADFUtils.findOperation("balanceBankCash");
        op.getParamsMap().put("unit", unitBinding.getValue().toString());
        op.getParamsMap().put("cntrlCode", controlCodeBinding.getValue().toString());
        op.getParamsMap().put("vouDate", vouDateBinding.getValue());
        op.getParamsMap().put("year", yearBind.getValue());
        op.execute();
        //        if (checkNoBinding.getValue() != null) {
        //            System.out.println("Inside if condition!!");
        //            checkNoBinding.setValue(null);
        //            AdfFacesContext.getCurrentInstance().addPartialTarget(checkNoBinding);
        //        }
    }

    public void setUnitBinding(RichInputComboboxListOfValues unitBinding) {
        this.unitBinding = unitBinding;
    }

    public RichInputComboboxListOfValues getUnitBinding() {
        return unitBinding;
    }

    public void setControlCodeBinding(RichInputComboboxListOfValues controlCodeBinding) {
        this.controlCodeBinding = controlCodeBinding;
    }

    public RichInputComboboxListOfValues getControlCodeBinding() {
        return controlCodeBinding;
    }

    public void setVouDateBinding(RichInputDate vouDateBinding) {
        this.vouDateBinding = vouDateBinding;
    }

    public RichInputDate getVouDateBinding() {
        return vouDateBinding;
    }

    public void bankCashDetailCreate(ActionEvent actionEvent) {
        getVouSeriesBinding().setDisabled(true);
        System.out.println("*************************************In Detail button action******************************");
        DCIteratorBinding dci = ADFUtils.findIterator("BankCashVoucherDetailVO1Iterator");
        DCIteratorBinding dci_tvouch = ADFUtils.findIterator("BankCashVoucherHeaderVO1Iterator");

        if (dci != null) {
            Integer seq_no = fetchMaxLineNumber(dci);
            System.out.println("Sequence Number after fetchMaxLineNumber:" + seq_no);
            RowSetIterator rsi = dci.getRowSetIterator();
            if (rsi != null) {
                Row last = rsi.last();
                int i = rsi.getRangeIndexOf(last);
                BankCashVoucherDetailVORowImpl newRow = (BankCashVoucherDetailVORowImpl) rsi.createRow();
                newRow.setNewRowState(Row.STATUS_INITIALIZED);
                rsi.insertRowAtRangeIndex(i + 1, newRow);
                rsi.setCurrentRow(newRow);
                newRow.setSeqNo(seq_no);
                newRow.setCurrencyCode(dci_tvouch.getCurrentRow().getAttribute("CurrencyCode") != null ?
                                       dci_tvouch.getCurrentRow().getAttribute("CurrencyCode").toString() : null);
                newRow.setFcAmount(new BigDecimal(0));
                newRow.setAmount(new BigDecimal(0));
                newRow.setLastSwitchLovTrans("F");
                newRow.setCurrRate(new BigDecimal(1));

                if (vouSeriesBinding.getValue().toString().equalsIgnoreCase("P"))
                    newRow.setDrCrFlag("D");
                else
                    newRow.setDrCrFlag("C");
                if (voucherNoBinding.getValue() != null) {
                    newRow.setFinTvouchVouNo(voucherNoBinding.getValue().toString());
                }
            }
            rsi.closeRowSetIterator();
            AdfFacesContext.getCurrentInstance().addPartialTarget(bankCashVoucherTableBinding);
        }
    }

    private Integer fetchMaxLineNumber(DCIteratorBinding itr) {
        System.out.println("####################In fetchmaxline number###########################");
        Integer max = new Integer(0);
        ArrayList<Integer> maxArray = new ArrayList<Integer>();
        System.out.println("Starting Max value:" + max);
        BankCashVoucherDetailVORowImpl currRow = (BankCashVoucherDetailVORowImpl) itr.getCurrentRow();
        //        System.out.println("sequence number in bankcashvoucherdetail:" + currRow.getSeqNo());
        if (currRow != null && currRow.getSeqNo() != null)
            max = currRow.getSeqNo();
        System.out.println("Max Value after setting currentseqno:" + max);
        RowSetIterator rsi = itr.getRowSetIterator();
        if (rsi != null) {
            Row[] allRowsInRange = rsi.getAllRowsInRange();
            for (Row rw : allRowsInRange) {
                if (rw != null && rw.getAttribute("SeqNo") != null &&
                    max.compareTo(Integer.parseInt(rw.getAttribute("SeqNo").toString())) < 0)
                    System.out.println("rw.getAttribute(\"SeqNo\")" + rw.getAttribute("SeqNo"));
                max = Integer.parseInt(rw.getAttribute("SeqNo").toString());
                maxArray.add(max);
            }
            rsi.closeRowSetIterator();
            if (!maxArray.isEmpty()) {
                System.out.println("Maximum number: " + Collections.max(maxArray));
                max = Collections.max(maxArray) + new Integer(1);
                System.out.println("max VAlue:" + max);
            } else {
                max = new Integer(1);
            }
        }
        return max;
    }

    public void lastEntry() {
        diffAmount = (BigDecimal) (diffBinding.getValue() != null ? diffBinding.getValue() : zero);
        System.out.println("Diff Amount Value: " + diffAmount);
        if (diffAmount.compareTo(zero) != 0) {
            OperationBinding op = ADFUtils.findOperation("removeRowBankCashVoucher");
            op.getParamsMap().put("glCode", controlCodeBinding.getValue());
            op.execute();
            System.out.println("*************************************In last entry action******************************");
            DCIteratorBinding dci = ADFUtils.findIterator("BankCashVoucherDetailVO1Iterator");
            DCIteratorBinding dci_tvouch = ADFUtils.findIterator("BankCashVoucherHeaderVO1Iterator");
            BigDecimal drAmt = (BigDecimal) dci.getCurrentRow().getAttribute("Debit");
            BigDecimal crAmt = (BigDecimal) dci.getCurrentRow().getAttribute("Credit");
            BigDecimal diff = drAmt.subtract(crAmt).abs();
            System.out.println("*************Printing dr:" + drAmt + " cr:" + crAmt + " diff:" + diff);
            System.out.println("control code value:" +
                               dci_tvouch.getCurrentRow().getAttribute("ControlCode").toString());
            String drcrflag = null;
            if (drAmt.compareTo(crAmt) > 0) {
                drcrflag = "C";
            } else {
                drcrflag = "D";
            }
            System.out.println(" drcrflag:" + drcrflag);

            if (dci != null) {
                Integer seq_no = fetchMaxLineNumber(dci);
                System.out.println("Sequence Number after fetchMaxLineNumber in last entry:" + seq_no);
                RowSetIterator rsi = dci.getRowSetIterator();
                if (rsi != null) {
                    Row last = rsi.last();
                    int i = rsi.getRangeIndexOf(last);
                    BankCashVoucherDetailVORowImpl newRow = (BankCashVoucherDetailVORowImpl) rsi.createRow();
                    newRow.setNewRowState(Row.STATUS_INITIALIZED);
                    rsi.insertRowAtRangeIndex(i + 1, newRow);
                    rsi.setCurrentRow(newRow);
                    newRow.setSeqNo(seq_no);
                    newRow.setLastSwitchLovTrans("L");
                    newRow.setCurrencyCode(dci_tvouch.getCurrentRow().getAttribute("CurrencyCode") != null ?
                                           dci_tvouch.getCurrentRow().getAttribute("CurrencyCode").toString() : null);
                    newRow.setFcAmount(diff.setScale(2, BigDecimal.ROUND_HALF_UP));
                    newRow.setAmount(diff.setScale(2, BigDecimal.ROUND_HALF_UP));
                    newRow.setDrCrFlag(drcrflag);
                    newRow.setGnrlLedGlCd(dci_tvouch.getCurrentRow().getAttribute("ControlCode").toString());
                    newRow.setSubCode(dci_tvouch.getCurrentRow().getAttribute("ControlCode").toString());
                    newRow.setCurrRate(new BigDecimal(1));

                    //                if (vouSeriesBinding.getValue().toString().equalsIgnoreCase("P") )
                    //                    newRow.setDrCrFlag("D");
                    //                else
                    //                    newRow.setDrCrFlag("C");
                }
                System.out.println("Before debit:" + debitBinding.getValue() + " credit:" + creditBinding.getValue());
                totalcreditValue = (BigDecimal) creditBinding.getValue();
                totaldebitValue = (BigDecimal) debitBinding.getValue();

                rsi.closeRowSetIterator();

                creditBinding.setValue(totalcreditValue);
                debitBinding.setValue(totaldebitValue);

                System.out.println("After debit:" + debitBinding.getValue() + " credit:" + creditBinding.getValue());
            }
            System.out.println("Last Entry Method END");
        }
    }

    public void saveAL(ActionEvent actionEvent) {
        button = "S";
        System.out.println("+++++++++++++++++++++++++@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@+++++++++++++++++++=");
        String flag = null;
        if (!debitBinding.getValue().equals(new BigDecimal(0)) || !creditBinding.getValue().equals(new BigDecimal(0))) {
            OperationBinding op3 = ADFUtils.findOperation("checkGlBalanceOverdraft");
            op3.getParamsMap().put("unit", unitBinding.getValue().toString());
            op3.getParamsMap().put("cntrlCode", controlCodeBinding.getValue().toString());
            op3.getParamsMap().put("vouDate", vouDateBinding.getValue());
            op3.getParamsMap().put("year", yearBind.getValue());
            op3.getParamsMap().put("ovrdrftLmt", glODLimitBinding.getValue());
            op3.getParamsMap().put("vouType", vouTypeBind.getValue());
            op3.getParamsMap().put("vouSeries", vouSeriesBinding.getValue());
            op3.getParamsMap().put("debit", debitBinding.getValue());
            op3.getParamsMap().put("credit", creditBinding.getValue());
            Object rst3 = op3.execute();
            System.out.println("checkGlBalanceOverdraft value from finamimpl:" + rst3);
            if (rst3 != null && rst3.toString().equalsIgnoreCase("T")) {
                System.out.println("in first if+++++++");
                OperationBinding op = ADFUtils.findOperation("checkDummyApprovedVoucherStatusBankCash");
                Object rst = op.execute();
                System.out.println("getcheckDummyApprovedVoucherStatus value in bean:" + rst);
                if (rst != null && rst.toString().trim().length() > 0 && rst.toString().equalsIgnoreCase("Y")) {
                    //                System.out.println("in Y Case:");
                    //                OperationBinding opd = ADFUtils.findOperation("detailRowCount");
                    //                Object rst2 = opd.execute();
                    //                System.out.println("Value returned by Amimpl>>>>>>>>" + rst2);
                    //                if (rst2 != null && rst.toString().trim().length() > 0 && rst2.toString().equalsIgnoreCase("T")) {
                    if (approvedByBinding.getValue() != null) {
                        if (approvedOnBinding.getValue() != null) {
                            lastEntry();
                            //                }
                            flag = checkDebitCredit();
                            System.out.println("flAG vALUE:" + flag);

                            if (flag.equalsIgnoreCase("Y")) {
                                System.out.println("in VOCUHER CREATION");

                                ADFUtils.findOperation("checkSameRecordBankCash").execute();
                                OperationBinding op1 = ADFUtils.findOperation("newVoucherFunctionBankCash");
                                Object rst1 = op1.execute();

                                if (rst1 != null && rst1.toString().trim().length() > 0) {
                                    ADFUtils.findOperation("Commit").execute();

                                    //ADFUtils.findOperation("CreateInsert").execute(); //Commented on 01/06/19
                                    AdfFacesContext.getCurrentInstance().addPartialTarget(bankCashVoucherTableBinding);
                                    ADFUtils.showMessage("Record Saved Successfully, New Voucher No. Is " + rst1, 2);
                                    ADFUtils.setEL("#{pageFlowScope.mode}", "V");
                                    cevmodecheck();
                                    AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
                                }

                            }
                        } else {
                            ADFUtils.showMessage("Please select Approval Date.", 0);
                        }
                    } else {
                        System.out.println("In Else loop");
                        FacesMessage Message = new FacesMessage("Please Approve the record.");
                        Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                        FacesContext fc = FacesContext.getCurrentInstance();
                        fc.addMessage(null, Message);
                    }
                } else if (voucherNoBinding.getValue() != null) {
                    if (approvedByBinding.getValue() != null) {
                        if (approvedOnBinding.getValue() != null) {
                            String flag1 = null;
                            //                    OperationBinding opd = ADFUtils.findOperation("detailRowCount");
                            //                    Object rst1 = opd.execute();
                            //                    System.out.println("Value returned by Amimpl>>>>>>>>" + rst);
                            //                    if (rst != null && rst1.toString().equalsIgnoreCase("T")) {
                            lastEntry();
                            //                    }
                            flag1 = checkDebitCredit();
                            if (flag1.equalsIgnoreCase("Y")) {
                                ADFUtils.findOperation("checkSameRecordBankCash").execute();
                                OperationBinding op2 = ADFUtils.findOperation("newVoucherFunctionBankCash");
                                Object rst2 = op2.execute();
                                if (rst2 != null && rst2.toString().trim().length() > 0) {

                                    System.out.println("In Dummy if loop.");
                                    ADFUtils.findOperation("Commit").execute();
                                    ADFUtils.showMessage("Record Saved Successfully, Approved Voucher No. Is " + rst2,
                                                         2);
                                    //ADFUtils.findOperation("CreateInsert").execute(); //Commented on 01/06/19
                                    AdfFacesContext.getCurrentInstance().addPartialTarget(bankCashVoucherTableBinding);
                                    ADFUtils.setEL("#{pageFlowScope.mode}", "V");
                                    cevmodecheck();
                                    AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
                                }
                            }

                        }


                    } else if (approvedByBinding.getValue() == null) {
                        lastEntry();
                        BigDecimal diffAmt = (BigDecimal) debitBinding.getValue();
                        System.out.println("Total Difference Amount: " + diffAmt.abs());
                        OperationBinding op1 = ADFUtils.findOperation("controlAmountValue");
                        op1.getParamsMap().put("val", diffAmt.abs());
                        op1.execute();
                        ADFUtils.findOperation("Commit").execute();
                        ADFUtils.showMessage("Record Updated Successfully.", 2);
                        ADFUtils.setEL("#{pageFlowScope.mode}", "V");
                        cevmodecheck();
                        AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
                    }

                    else {
                        ADFUtils.showMessage("Please Approve the record.", 0);
                    }
                } else {
                    lastEntry();
                    BigDecimal debitAmt = (BigDecimal) debitBinding.getValue();
                    System.out.println("Total Difference Amount: " + debitAmt.abs());
                    OperationBinding op1 = ADFUtils.findOperation("controlAmountValue");
                    op1.getParamsMap().put("val", debitAmt.abs());
                    op1.execute();
                    OperationBinding op2 = ADFUtils.findOperation("dummyBankFunctionBankCash");
                    Object rst2 = op2.execute();
                    if (rst2 != null && rst2.toString().trim().length() > 0) {

                        System.out.println("In Dummy loop.");
                        ADFUtils.findOperation("Commit").execute();
                        ADFUtils.showMessage("Record Saved Successfully, Dummy_Voucher No. Is " + rst2, 2);
                        // ADFUtils.findOperation("CreateInsert").execute(); //Commented on 01/06/19
                        AdfFacesContext.getCurrentInstance().addPartialTarget(bankCashVoucherTableBinding);
                        ADFUtils.setEL("#{pageFlowScope.mode}", "V");
                        cevmodecheck();
                        AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
                    }
                    //   }
                }
            } else {
                FacesMessage Message = new FacesMessage("Insufficient Ledger Balance for payment.");
                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            }
        } else
            ADFUtils.showMessage("Voucher Can't be created with zero amount.", 0);
    }

    public void setBindCondtnPopUp(RichPopup bindCondtnPopUp) {
        this.bindCondtnPopUp = bindCondtnPopUp;
    }

    public RichPopup getBindCondtnPopUp() {
        return bindCondtnPopUp;
    }


    public void chequeNumberValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        System.out.println("TransForDate Value: " + transBankCodeBinding.getValue() + "Cheque Number:" + object +
                           " cheque/rtgs:" + chequesBinding.getValue());
        if (vouSeriesBinding.getValue() != null) {
            if (!vouSeriesBinding.getValue().equals("R")) {
                String str = (String) object;
                if (!str.isEmpty() && chequesBinding.getValue() != null) {
                    System.out.println("In First Bracket");
                    if (chequesBinding.getValue().toString().equalsIgnoreCase("C")) {
                        System.out.println("In Second Bracket");
                        if (str.length() == 6) {
                            OperationBinding op = ADFUtils.findOperation("getChequeNoValidBankCashVoucher");
                            op.getParamsMap().put("chequeno", object);
                            op.getParamsMap().put("bankCode", transBankCodeBinding.getValue());
                            Object rst = op.execute();
                            System.out.println("Value after function for cheque number validation:" + rst);
                            if (rst != null && rst.toString().trim().length() > 0 &&
                                rst.toString().equalsIgnoreCase("N")) {
                                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                              "Entered Cheque No./DD No. Is Not Valid.",
                                                                              null));
                            }

                        } else {
                            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                          "Entered Cheque No./DD No. must be of 6 digits.",
                                                                          null));
                        }
                    }
                }
            }
        }

    }

    public void setVouTypeBind(RichSelectOneChoice vouTypeBind) {
        this.vouTypeBind = vouTypeBind;
    }

    public RichSelectOneChoice getVouTypeBind() {
        return vouTypeBind;
    }

    public void setYearBind(RichInputText yearBind) {
        this.yearBind = yearBind;
    }

    public RichInputText getYearBind() {
        return yearBind;
    }

    //    public void currCodeValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
    //        System.out.println("CurrCode from binding:" + currCodeBind.getValue() + "CurrCode FROM OBJECT" + object +
    //                           "  fcamount:" + fcAmountBind.getValue() + " fc amount Object:" + object);
    //        if (object != null) {
    //            if (currCodeBind.getValue() != null && fcAmountBind.getValue() != null) {
    //                OperationBinding op = ADFUtils.findOperation("checkfcAmountValidation");
    //                op.getParamsMap().put("currCode", object);
    //                op.getParamsMap().put("opbal", fcAmountBind.getValue());
    //                op.getParamsMap().put("hCurrCode", headerCurrCode.getValue());
    //                Object rst = op.execute();
    //                System.out.println("checkfcAmountValidation function returning value from amimpl:" + rst);
    //                if (rst != null && rst.toString().equalsIgnoreCase("F")) {
    //                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
    //                                                                  "Please input currency rate for selected currency.",
    //                                                                  null));
    //                } else if (rst != null && rst.toString().equalsIgnoreCase("I")) {
    //                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
    //                                                                  "Please validate currency in daily currency table.",
    //                                                                  null));
    //                }
    //            }
    //        }
    //    }

    public void currCodeVCL(ValueChangeEvent valueChangeEvent) {
        //        if (approvedByBinding.getValue() != null) {
        //            approvedByBinding.setValue(null);
        //        }
        //        System.out.println("VCLCurrCode:" + currCodeBind.getValue() + "  fcamount:" + fcAmountBind.getValue());
        //        if (valueChangeEvent != null) {
        //            OperationBinding op1 = ADFUtils.findOperation("checkDummyApprovedVoucherStatusBankCash");
        //            op1.execute();
        //            System.out.println("Result: " + op1.getResult().toString());
        //
        //            if (op1.getResult() != null && op1.getResult().toString().equalsIgnoreCase("N")) {
        //                System.out.println("Value of mode: " + ADFUtils.resolveExpression("#{pageFlowScope.mode}"));
        //                if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
        //                    System.out.println("Inside condition!!");
        //                    getApprovedByBinding().setDisabled(true);
        //                } else {
        //                    getApprovedByBinding().setDisabled(false);
        //                }
        //            } else {
        //                getApprovedByBinding().setDisabled(false);
        //            }
        //            if (currCodeBind.getValue() != null && fcAmountBind.getValue() != null) {
        //                OperationBinding op = ADFUtils.findOperation("fcToLcAmount");
        //                op.getParamsMap().put("currCode", currCodeBind.getValue());
        //                op.getParamsMap().put("opbal", fcAmountBind.getValue());
        //                op.getParamsMap().put("hCurrCode", headerCurrCode.getValue());
        //                Object rst = op.execute();
        //                System.out.println("fcToLcAmount funcion returning value from amimpl:" + rst);
        //                if (rst != null && rst.toString().equalsIgnoreCase("F")) {
        //                    FacesMessage message = new FacesMessage("Please input currency rate for selected currency.");
        //                    message.setSeverity(FacesMessage.SEVERITY_ERROR);
        //                    FacesContext context = FacesContext.getCurrentInstance();
        //                    context.addMessage(currCodeBind.getClientId(), message);
        //                } else if (rst != null && rst.toString().equalsIgnoreCase("I")) {
        //                    FacesMessage message = new FacesMessage("Please validate currency in daily currency table.");
        //                    message.setSeverity(FacesMessage.SEVERITY_ERROR);
        //                    FacesContext context = FacesContext.getCurrentInstance();
        //                    context.addMessage(currCodeBind.getClientId(), message);
        //                }
        //            }
        //        }
        if (valueChangeEvent != null) {
            OperationBinding op = ADFUtils.findOperation("fcToLcAmount");
            op.getParamsMap().put("currencyCode", currCodeBind.getValue());
            Object rst = op.execute();
            if (rst != null && rst.toString().equals("F")) {
                FacesMessage message = new FacesMessage("Please validate currency in daily currency table.");
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(currCodeBind.getClientId(), message);
            }
        }
        if (fcAmountBind.getValue() != null) {
            fcAmountBind.setValue(null);
        }

    }

    public void setCurrCodeBind(RichInputComboboxListOfValues currCodeBind) {
        this.currCodeBind = currCodeBind;
    }

    public RichInputComboboxListOfValues getCurrCodeBind() {
        return currCodeBind;
    }

    public void setFcAmountBind(RichInputText fcAmountBind) {
        this.fcAmountBind = fcAmountBind;
    }

    public RichInputText getFcAmountBind() {
        return fcAmountBind;
    }

    public void setHeaderCurrCode(RichInputText headerCurrCode) {
        this.headerCurrCode = headerCurrCode;
    }

    public RichInputText getHeaderCurrCode() {
        return headerCurrCode;
    }

    public void setChequesBinding(RichSelectOneChoice chequesBinding) {
        this.chequesBinding = chequesBinding;
    }

    public RichSelectOneChoice getChequesBinding() {
        return chequesBinding;
    }

    public void amountValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        System.out.println(" object in amount:" + object);
        if (object != null) {
            BigDecimal amt = new BigDecimal(object.toString());
            System.out.println("AMt after conversion:" + amt);
            if (amt.compareTo(new BigDecimal(0)) < 0) {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                              "Amount must be greater than zero.", null));
            }
        }

    }

    public void setDebitBinding(RichInputText debitBinding) {
        this.debitBinding = debitBinding;
    }

    public RichInputText getDebitBinding() {
        return debitBinding;
    }

    public void setCreditBinding(RichInputText creditBinding) {
        this.creditBinding = creditBinding;
    }

    public RichInputText getCreditBinding() {
        return creditBinding;
    }

    public String checkDebitCredit() {
        String flag = "Y";
        System.out.println("debit:" + debitBinding.getValue() + " credit:" + creditBinding.getValue() + " flag:" +
                           flag);
        BigDecimal db = new BigDecimal(debitBinding.getValue().toString());
        BigDecimal cr = new BigDecimal(creditBinding.getValue().toString());
        if (db.compareTo(cr) == 0) {
            OperationBinding op = ADFUtils.findOperation("controlAmountValue");
            op.getParamsMap().put("val", debitBinding.getValue());
            Object rst = op.execute();
            return flag;
        } else {
            flag = "N";
            FacesMessage Message = new FacesMessage("debit and credit must be equal.");
            Message.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }
        return flag;
    }

    public void setGlODLimitBinding(RichInputText glODLimitBinding) {
        this.glODLimitBinding = glODLimitBinding;
    }

    public RichInputText getGlODLimitBinding() {
        return glODLimitBinding;
    }

    public void setVoucherNoBinding(RichInputText voucherNoBinding) {
        this.voucherNoBinding = voucherNoBinding;
    }

    public RichInputText getVoucherNoBinding() {
        return voucherNoBinding;
    }

    public void setInFavourBinding(RichInputText inFavourBinding) {
        this.inFavourBinding = inFavourBinding;
    }

    public RichInputText getInFavourBinding() {
        return inFavourBinding;
    }

    public void setGlBalBinding(RichInputText glBalBinding) {
        this.glBalBinding = glBalBinding;
    }

    public RichInputText getGlBalBinding() {
        return glBalBinding;
    }

    public void setTdsCodeBinding(RichInputComboboxListOfValues tdsCodeBinding) {
        this.tdsCodeBinding = tdsCodeBinding;
    }

    public RichInputComboboxListOfValues getTdsCodeBinding() {
        return tdsCodeBinding;
    }

    public void setPreparedByBinding(RichInputComboboxListOfValues preparedByBinding) {
        this.preparedByBinding = preparedByBinding;
    }

    public RichInputComboboxListOfValues getPreparedByBinding() {
        return preparedByBinding;
    }

    public void setControlCodeDescBinding(RichInputText controlCodeDescBinding) {
        this.controlCodeDescBinding = controlCodeDescBinding;
    }

    public RichInputText getControlCodeDescBinding() {
        return controlCodeDescBinding;
    }

    public void setTdsPayAmtBinding(RichInputText tdsPayAmtBinding) {
        this.tdsPayAmtBinding = tdsPayAmtBinding;
    }

    public RichInputText getTdsPayAmtBinding() {
        return tdsPayAmtBinding;
    }

    public void setDiffBinding(RichInputText diffBinding) {
        this.diffBinding = diffBinding;
    }

    public RichInputText getDiffBinding() {
        return diffBinding;
    }

    public void setVoucherAmountBinding(RichInputText voucherAmountBinding) {
        this.voucherAmountBinding = voucherAmountBinding;
    }

    public RichInputText getVoucherAmountBinding() {
        return voucherAmountBinding;
    }

    public void setSeqNoBinding(RichInputText seqNoBinding) {
        this.seqNoBinding = seqNoBinding;
    }

    public RichInputText getSeqNoBinding() {
        return seqNoBinding;
    }

    public void setAcHeadsBinding(RichInputComboboxListOfValues acHeadsBinding) {
        this.acHeadsBinding = acHeadsBinding;
    }

    public RichInputComboboxListOfValues getAcHeadsBinding() {
        return acHeadsBinding;
    }

    public void setAcDescBinding(RichInputText acDescBinding) {
        this.acDescBinding = acDescBinding;
    }

    public RichInputText getAcDescBinding() {
        return acDescBinding;
    }

    public void setGlCodeBinding(RichInputComboboxListOfValues glCodeBinding) {
        this.glCodeBinding = glCodeBinding;
    }

    public RichInputComboboxListOfValues getGlCodeBinding() {
        return glCodeBinding;
    }

    public void setGlDescBinding(RichInputText glDescBinding) {
        this.glDescBinding = glDescBinding;
    }

    public RichInputText getGlDescBinding() {
        return glDescBinding;
    }

    public void setGlBalDetailBinding(RichInputText glBalDetailBinding) {
        this.glBalDetailBinding = glBalDetailBinding;
    }

    public RichInputText getGlBalDetailBinding() {
        return glBalDetailBinding;
    }

    public void setCurrRateBinding(RichInputText currRateBinding) {
        this.currRateBinding = currRateBinding;
    }

    public RichInputText getCurrRateBinding() {
        return currRateBinding;
    }

    public void setDrCrBinding(RichSelectOneChoice drCrBinding) {
        this.drCrBinding = drCrBinding;
    }

    public RichSelectOneChoice getDrCrBinding() {
        return drCrBinding;
    }

    public void setCostCentreBinding(RichInputComboboxListOfValues costCentreBinding) {
        this.costCentreBinding = costCentreBinding;
    }

    public RichInputComboboxListOfValues getCostCentreBinding() {
        return costCentreBinding;
    }

    public void setAmount(RichInputText amount) {
        this.amount = amount;
    }

    public RichInputText getAmount() {
        return amount;
    }

    public void setNarrationBinding(RichInputText narrationBinding) {
        this.narrationBinding = narrationBinding;
    }

    public RichInputText getNarrationBinding() {
        return narrationBinding;
    }

    public void setBudAmtBinding(RichInputText budAmtBinding) {
        this.budAmtBinding = budAmtBinding;
    }

    public RichInputText getBudAmtBinding() {
        return budAmtBinding;
    }

    public void setProjCdBinding(RichInputComboboxListOfValues projCdBinding) {
        this.projCdBinding = projCdBinding;
    }

    public RichInputComboboxListOfValues getProjCdBinding() {
        return projCdBinding;
    }

    public void setFinTvouBinding(RichInputText finTvouBinding) {
        this.finTvouBinding = finTvouBinding;
    }

    public RichInputText getFinTvouBinding() {
        return finTvouBinding;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setDetailcreateBinding(RichButton detailcreateBinding) {
        this.detailcreateBinding = detailcreateBinding;
    }

    public RichButton getDetailcreateBinding() {
        return detailcreateBinding;
    }

    public void editButtonAL(ActionEvent actionEvent) {
        if (approvedByBinding.getValue() == null) {
            cevmodecheck();
        } else {
            ADFUtils.showMessage("Record is Approved Can't be modified !", 2);
            ADFUtils.setEL("#{pageFlowScope.mode}", "V");
        }
    }

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    } // Add event code here...


    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }

    public void cevModeDisableComponent(String mode) {
        //            FacesContext fctx = FacesContext.getCurrentInstance();
        //            ELContext elctx = fctx.getELContext();
        //            Application jsfApp = fctx.getApplication();
        //            //create a ValueExpression that points to the ADF binding layer
        //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
        //            //
        //            ValueExpression valueExpr = exprFactory.createValueExpression(
        //                                         elctx,
        //                                         "#{pageFlowScope.mode=='E'}",
        //                                          Object.class
        //                                         );
        //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
        if (mode.equals("E")) {
            getInFavourBinding().setDisabled(true);
            getTdsCodeBinding().setDisabled(true);
            getPreparedByBinding().setDisabled(true);
            getTdsPayAmtBinding().setDisabled(true);
            //getAcHeadsBinding().setDisabled(true);
            //getGlCodeBinding().setDisabled(true);

            //getCurrCodeBind().setDisabled(true);
            //getDrCrBinding().setDisabled(true);
            //getCostCentreBinding().setDisabled(true);
            //getFcAmountBind().setDisabled(true);
            getNarrationBinding().setDisabled(true);
            getProjCdBinding().setDisabled(true);
            getFinTvouBinding().setDisabled(true);

            getVoucherNoBinding().setDisabled(true);
            getDebitBinding().setDisabled(true);
            getControlCodeDescBinding().setDisabled(true);
            getCreditBinding().setDisabled(true);
            getGlBalBinding().setDisabled(true);
            getDiffBinding().setDisabled(true);
            getGlODLimitBinding().setDisabled(true);
            getVoucherAmountBinding().setDisabled(true);
            getSeqNoBinding().setDisabled(true);
            getAcDescBinding().setDisabled(true);
            getGlDescBinding().setDisabled(true);
            getGlBalDetailBinding().setDisabled(true);
            getCurrRateBinding().setDisabled(true);
            //getAmount().setDisabled(true);
            getBudAmtBinding().setDisabled(true);
            getVouSeriesBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            getUnitBinding().setDisabled(true);
            getPreparedByBinding().setDisabled(true);
            getGlCreditDebitBinding().setDisabled(true);
            //getApprovedByBinding().setDisabled(true);
            getDetailcreateBinding().setDisabled(false);
            //getApprovedOnBinding().setDisabled(true);
            getVerifiedByBinding().setDisabled(true);
            getVouDateBinding().setDisabled(true);
            getVouTypeBind().setDisabled(true);

        } else if (mode.equals("C")) {
            //getVouDateBinding().setDisabled(true);
            getApprovedByBinding().setDisabled(true);
            getVoucherNoBinding().setDisabled(true);
            getDebitBinding().setDisabled(true);
            getControlCodeDescBinding().setDisabled(true);
            getCreditBinding().setDisabled(true);
            getGlBalBinding().setDisabled(true);
            getDiffBinding().setDisabled(true);
            getGlODLimitBinding().setDisabled(true);
            getVoucherAmountBinding().setDisabled(true);
            getSeqNoBinding().setDisabled(true);
            getAcDescBinding().setDisabled(true);
            getGlDescBinding().setDisabled(true);
            getGlBalDetailBinding().setDisabled(true);
            getCurrRateBinding().setDisabled(true);
            // getAmount().setDisabled(true);
            getBudAmtBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            getUnitBinding().setDisabled(true);
            getPreparedByBinding().setDisabled(true);
            getGlCreditDebitBinding().setDisabled(true);
            getApprovedOnBinding().setDisabled(true);


        } else if (mode.equals("V")) {
            getNarrationDetailTabBinding().setDisabled(false);
            getVoucherDetailTabBinding().setDisabled(false);
            getDetailcreateBinding().setDisabled(true);
        }
    }

    public void setOutputTextBinding(RichOutputText outputTextBinding) {
        this.outputTextBinding = outputTextBinding;
    }

    public RichOutputText getOutputTextBinding() {
        cevmodecheck();
        return outputTextBinding;
    }

    public void deletepopupDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(bankCashVoucherTableBinding);
    }

    public void setBankCashVoucherTableBinding(RichTable bankCashVoucherTableBinding) {
        this.bankCashVoucherTableBinding = bankCashVoucherTableBinding;
    }

    public RichTable getBankCashVoucherTableBinding() {
        return bankCashVoucherTableBinding;
    }

    public void setVerifiedByBinding(RichInputComboboxListOfValues verifiedByBinding) {
        this.verifiedByBinding = verifiedByBinding;
    }

    public RichInputComboboxListOfValues getVerifiedByBinding() {
        return verifiedByBinding;
    }

    public void setApprovedByBinding(RichInputComboboxListOfValues approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputComboboxListOfValues getApprovedByBinding() {
        return approvedByBinding;
    }

    public void setApprovedOnBinding(RichInputDate approvedOnBinding) {
        this.approvedOnBinding = approvedOnBinding;
    }

    public RichInputDate getApprovedOnBinding() {
        return approvedOnBinding;
    }

    public void vouTypeVCL(ValueChangeEvent valueChangeEvent) {
        System.out.println("VouType VCL:" + vouTypeBind.getValue());
        OperationBinding op = ADFUtils.findOperation("valueSetNullInBankCash");
        op.execute();
        if (vouTypeBind.getValue().equals("C")) {
            getChequesBinding().setDisabled(true);
            getCheckNoBinding().setDisabled(true);
            getCheckRtgsDateBinding().setDisabled(true);
        }
        if (vouTypeBind.getValue().equals("B")) {
            getChequesBinding().setDisabled(false);
            getCheckNoBinding().setDisabled(false);
            getCheckRtgsDateBinding().setDisabled(false);
        }
    }

    public void setVouSeriesBinding(RichInputComboboxListOfValues vouSeriesBinding) {
        this.vouSeriesBinding = vouSeriesBinding;
    }

    public RichInputComboboxListOfValues getVouSeriesBinding() {
        return vouSeriesBinding;
    }

    public void setGlCreditDebitBinding(RichInputText glCreditDebitBinding) {
        this.glCreditDebitBinding = glCreditDebitBinding;
    }

    public RichInputText getGlCreditDebitBinding() {
        return glCreditDebitBinding;
    }

    public String saveAndCloseAL() {
        button = "SV";
        System.out.println("+++++++++++++++++++++++++@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@+++++++++++++++++++=");
        String flag = null;
        if (!debitBinding.getValue().equals(new BigDecimal(0)) || !creditBinding.getValue().equals(new BigDecimal(0))) {
            OperationBinding op3 = ADFUtils.findOperation("checkGlBalanceOverdraft");
            op3.getParamsMap().put("unit", unitBinding.getValue().toString());
            op3.getParamsMap().put("cntrlCode", controlCodeBinding.getValue().toString());
            op3.getParamsMap().put("vouDate", vouDateBinding.getValue());
            op3.getParamsMap().put("year", yearBind.getValue());
            op3.getParamsMap().put("ovrdrftLmt", glODLimitBinding.getValue());
            op3.getParamsMap().put("vouType", vouTypeBind.getValue());
            op3.getParamsMap().put("vouSeries", vouSeriesBinding.getValue());
            op3.getParamsMap().put("debit", debitBinding.getValue());
            op3.getParamsMap().put("credit", creditBinding.getValue());
            Object rst3 = op3.execute();
            System.out.println("checkGlBalanceOverdraft value from finamimpl:" + rst3);
            if (rst3 != null && rst3.toString().equalsIgnoreCase("T")) {
                System.out.println("in first if+++++++");
                OperationBinding op = ADFUtils.findOperation("checkDummyApprovedVoucherStatusBankCash");
                Object rst = op.execute();
                System.out.println("getcheckDummyApprovedVoucherStatus value in bean:" + rst);
                if (rst != null && rst.toString().trim().length() > 0 && rst.toString().equalsIgnoreCase("Y")) {
                    //                System.out.println("in Y Case:");
                    //                OperationBinding opd = ADFUtils.findOperation("detailRowCount");
                    //                Object rst2 = opd.execute();
                    //                System.out.println("Value returned by Amimpl>>>>>>>>" + rst2);
                    //                if (rst2 != null && rst.toString().trim().length() > 0 && rst2.toString().equalsIgnoreCase("T")) {
                    if (approvedByBinding.getValue() != null) {
                        if (approvedOnBinding.getValue() != null) {
                            lastEntry();
                            //                }
                            flag = checkDebitCredit();
                            System.out.println("flAG vALUE:" + flag);

                            if (flag.equalsIgnoreCase("Y")) {
                                System.out.println("in VOCUHER CREATION");

                                ADFUtils.findOperation("checkSameRecordBankCash").execute();
                                OperationBinding op1 = ADFUtils.findOperation("newVoucherFunctionBankCash");
                                Object rst1 = op1.execute();

                                if (rst1 != null && rst1.toString().trim().length() > 0) {
                                    ADFUtils.findOperation("Commit").execute();
                                    // ADFUtils.findOperation("CreateInsert").execute(); //Commented on 01/06/19
                                    AdfFacesContext.getCurrentInstance().addPartialTarget(bankCashVoucherTableBinding);
                                    ADFUtils.showMessage("Record Saved Successfully, New Voucher No. Is " + rst1, 2);
                                    return "save and close";
                                }

                            }
                        } else {
                            ADFUtils.showMessage("Please select Approval Date.", 0);
                        }
                    } else {
                        System.out.println("In Else loop");
                        FacesMessage Message = new FacesMessage("Please Approve the record.");
                        Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                        FacesContext fc = FacesContext.getCurrentInstance();
                        fc.addMessage(null, Message);
                    }
                } else if (voucherNoBinding.getValue() != null) {
                    if (approvedByBinding.getValue() != null) {
                        if (approvedOnBinding.getValue() != null) {
                            String flag1 = null;
                            //                    OperationBinding opd = ADFUtils.findOperation("detailRowCount");
                            //                    Object rst1 = opd.execute();
                            //                    System.out.println("Value returned by Amimpl>>>>>>>>" + rst);
                            //                    if (rst != null && rst1.toString().equalsIgnoreCase("T")) {
                            lastEntry();
                            //                    }
                            flag1 = checkDebitCredit();
                            if (flag1.equalsIgnoreCase("Y")) {
                                ADFUtils.findOperation("checkSameRecordBankCash").execute();
                                OperationBinding op2 = ADFUtils.findOperation("newVoucherFunctionBankCash");
                                Object rst2 = op2.execute();
                                if (rst2 != null && rst2.toString().trim().length() > 0) {

                                    System.out.println("In Dummy if loop.");
                                    ADFUtils.findOperation("Commit").execute();
                                    ADFUtils.showMessage("Record Saved Successfully, Dummy_Voucher No. Is " + rst2, 2);
                                    // ADFUtils.findOperation("CreateInsert").execute(); //Commented on 01/06/19
                                    // AdfFacesContext.getCurrentInstance().addPartialTarget(bankCashVoucherTableBinding);
                                    return "save and close";
                                }
                            }

                        } else {
                            ADFUtils.showMessage("Please select Approval Date.", 0);
                        }
                    } else if (approvedByBinding.getValue() == null) {
                        lastEntry();
                        BigDecimal diffAmt = (BigDecimal) debitBinding.getValue();
                        System.out.println("Total Difference Amount: " + diffAmt.abs());
                        OperationBinding op1 = ADFUtils.findOperation("controlAmountValue");
                        op1.getParamsMap().put("val", diffAmt.abs());
                        op1.execute();
                        ADFUtils.findOperation("Commit").execute();
                        ADFUtils.showMessage("Record Updated Successfully.", 2);
                        return "save and close";
                    } else {
                        ADFUtils.showMessage("Please Approve the record.", 0);
                    }
                } else {
                    //String flag1 = null;
                    //                OperationBinding opd = ADFUtils.findOperation("detailRowCount");
                    //                Object rst1 = opd.execute();
                    //                System.out.println("Value returned by Amimpl>>>>>>>>" + rst);
                    //                if (rst != null && rst1.toString().equalsIgnoreCase("T")) {
                    //                    lastEntry();
                    //                }
                    //flag1 = checkDebitCredit();
                    //if (flag1.equalsIgnoreCase("Y")) {
                    // ADFUtils.findOperation("checkSameRecordBankCash").execute();
                    lastEntry();
                    BigDecimal debitAmt = (BigDecimal) debitBinding.getValue();
                    System.out.println("Total Difference Amount: " + debitAmt.abs());
                    OperationBinding op1 = ADFUtils.findOperation("controlAmountValue");
                    op1.getParamsMap().put("val", debitAmt.abs());
                    op1.execute();
                    OperationBinding op2 = ADFUtils.findOperation("dummyBankFunctionBankCash");
                    Object rst2 = op2.execute();
                    if (rst2 != null && rst2.toString().trim().length() > 0) {

                        System.out.println("In Dummy loop.");
                        ADFUtils.findOperation("Commit").execute();
                        ADFUtils.showMessage("Record Saved Successfully, Dummy_Voucher No. Is " + rst2, 2);
                        // ADFUtils.findOperation("CreateInsert").execute(); //Commented on 01/06/19
                        // AdfFacesContext.getCurrentInstance().addPartialTarget(bankCashVoucherTableBinding);
                        return "save and close";
                    }
                    //   }
                }
            } else {
                FacesMessage Message = new FacesMessage("Insufficient Ledger Balance for payment.");
                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            }
        } else
            ADFUtils.showMessage("Voucher Can't be created with zero amount.", 0);
        return null;
    }

    public String dialogYesAction() {
        System.out.println("Yes button popup dialog AL");
        String flag = null;
        getBindCondtnPopUp().hide();
        if (approvedByBinding.getValue() != null) {
            OperationBinding opd = ADFUtils.findOperation("detailRowCount");
            Object rst = opd.execute();
            System.out.println("Value returned by Amimpl>>>>>>>>" + rst);
            if (rst != null && rst.toString().equalsIgnoreCase("T")) {
                lastEntry();
            }
            flag = checkDebitCredit();
            if (flag.equalsIgnoreCase("Y")) {
                ADFUtils.findOperation("checkSameRecordBankCash").execute();
                OperationBinding op2 = ADFUtils.findOperation("newVoucherFunctionBankCash");
                Object rst2 = op2.execute();
                if (rst2 != null && rst2.toString().trim().length() > 0) {
                    System.out.println("In Approved If loop.");
                    ADFUtils.findOperation("Commit").execute();
                    ADFUtils.showMessage("Record Saved Successfully, New Voucher No. Is " + rst2, 2);
                    saveAndClose = "false";
                    if (button == "S") {
                        // ADFUtils.findOperation("CreateInsert").execute(); //Commented on 01/06/19
                        AdfFacesContext.getCurrentInstance().addPartialTarget(bankCashVoucherTableBinding);
                    }
                    return button == "SV" ? "save and close" : null;
                }

            }
        } else {
            System.out.println("In Else loop");
            FacesMessage Message = new FacesMessage("Please Verify and Approve the record.");
            Message.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
        }
        return null;
    }

    public String dialogNoAction() {
        System.out.println("NO button popup dialog AL");
        String flag = null;
        getBindCondtnPopUp().hide();
        //        if (verifiedByBinding.getValue() != null) {
        OperationBinding opd = ADFUtils.findOperation("detailRowCount");
        Object rst = opd.execute();
        System.out.println("Value returned by Amimpl>>>>>>>>" + rst);
        if (rst != null && rst.toString().equalsIgnoreCase("T")) {
            lastEntry();
        }
        flag = checkDebitCredit();
        if (flag.equalsIgnoreCase("Y")) {
            ADFUtils.findOperation("checkSameRecordBankCash").execute();
            OperationBinding op2 = ADFUtils.findOperation("dummyBankFunctionBankCash");
            Object rst2 = op2.execute();
            if (rst2 != null && rst2.toString().trim().length() > 0) {

                System.out.println("In Dummy if loop.");
                ADFUtils.findOperation("Commit").execute();
                ADFUtils.showMessage("Record Saved Successfully, Dummy_Voucher No. Is " + rst2, 2);
                saveAndClose = "false";
                if (button == "S") {
                    //  ADFUtils.findOperation("CreateInsert").execute(); //Commented on 01/06/19
                    AdfFacesContext.getCurrentInstance().addPartialTarget(bankCashVoucherTableBinding);
                }
                return button == "SV" ? "save and close" : null;
            }

        }
        //       }
        //        else {
        //            System.out.println("In Else loop");
        //            FacesMessage Message = new FacesMessage("Please Verify the record.");
        //            Message.setSeverity(FacesMessage.SEVERITY_ERROR);
        //            FacesContext fc = FacesContext.getCurrentInstance();
        //            fc.addMessage(null, Message);
        //        }
        return null;
    }

    public void approveByReturnListner(ReturnPopupEvent returnPopupEvent) {
        System.out.println("Voucher Type: " + vouTypeBind.getValue() + " Type: " + vouSeriesBinding.getValue());
        System.out.println("In Return Listner!!! Unit Code :" + unitBinding.getValue().toString() + "Auth Code: " +
                           approvedByBinding.getValue().toString() + " Amount: " + debitBinding.getValue());
        BigDecimal amount = (BigDecimal) debitBinding.getValue();
        if (amount != null) {
            OperationBinding op = ADFUtils.findOperation("checkApprovalStatusBankCash");
            op.getParamsMap().put("unitCd", unitBinding.getValue());
            op.getParamsMap().put("empCode", approvedByBinding.getValue());
            op.getParamsMap().put("amount", amount.abs());
            if (vouTypeBind.getValue().equals("B") && vouSeriesBinding.getValue().equals("R")) {
                op.getParamsMap().put("formNm", "FINBR");
            }
            if (vouTypeBind.getValue().equals("B") && vouSeriesBinding.getValue().equals("P")) {
                op.getParamsMap().put("formNm", "FINBP");
            }
            if (vouTypeBind.getValue().equals("C") && vouSeriesBinding.getValue().equals("R")) {
                op.getParamsMap().put("formNm", "FINCR");
            }
            if (vouTypeBind.getValue().equals("C") && vouSeriesBinding.getValue().equals("P")) {
                op.getParamsMap().put("formNm", "FINCP");

            }
            op.execute();
        }
    }

    public void amountVCL(ValueChangeEvent vce) {
        System.out.println("VCLCurrCode:" + currCodeBind.getValue() + "  fcamount:" + fcAmountBind.getValue());
        if (vce != null) {
            OperationBinding op1 = ADFUtils.findOperation("checkDummyApprovedVoucherStatusBankCash");
            op1.execute();
            System.out.println("Result: " + op1.getResult().toString());

            if (op1.getResult() != null && op1.getResult().toString().equalsIgnoreCase("N")) {
                System.out.println("Value of mode: " + ADFUtils.resolveExpression("#{pageFlowScope.mode}"));
                if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C") ||
                    ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("S") ||
                    ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("P")) {
                    System.out.println("Inside condition!!");
                    getApprovedByBinding().setDisabled(true);
                } else {
                    getApprovedByBinding().setDisabled(false);
                }
            } else {
                getApprovedByBinding().setDisabled(false);
            }
            BigDecimal rate = (BigDecimal) currRateBinding.getValue();
            BigDecimal AmtFc = (BigDecimal) vce.getNewValue();
            BigDecimal mul = new BigDecimal(0);
            if (rate != null && AmtFc != null) {
                mul = rate.multiply(AmtFc);
                amount.setValue(mul);
                System.out.println("MODE VALUE: " + ADFUtils.resolveExpression("#{pageFlowScope.mode}") +
                                   " And AuthCode Value: " + approvedByBinding.getValue());
                if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E") &&
                    approvedByBinding.getValue() != null) {
                    System.out.println("inside my if condition !!");
                    approvedByBinding.setValue(null);
                }
            }
        }
    }

    public void amountLcVCL(ValueChangeEvent valueChangeEvent) {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E") && approvedByBinding.getValue() != null) {
            System.out.println("inside my if condition !!");
            approvedByBinding.setValue(null);
        }
    }

    public void setTransBankCodeBinding(RichInputText transBankCodeBinding) {
        this.transBankCodeBinding = transBankCodeBinding;
    }

    public RichInputText getTransBankCodeBinding() {
        return transBankCodeBinding;
    }

    public void setCheckNoBinding(RichInputText checkNoBinding) {
        this.checkNoBinding = checkNoBinding;
    }

    public RichInputText getCheckNoBinding() {
        return checkNoBinding;
    }

    public void setCheckRtgsDateBinding(RichInputDate checkRtgsDateBinding) {
        this.checkRtgsDateBinding = checkRtgsDateBinding;
    }

    public RichInputDate getCheckRtgsDateBinding() {
        return checkRtgsDateBinding;
    }

    public void chequeRtgsVCL(ValueChangeEvent valueChangeEvent) {
        checkNoBinding.resetValue();
    }

    public void CostCenterValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        System.out.println("Object Value: " + object.toString() + " GL Code Value: " + glCodeBinding.getValue() +
                           " TransSubCode: " + transGlielaBinding.getValue());
        if (object.toString().isEmpty() && glCodeBinding.getValue() != null &&
            (transGlielaBinding.getValue().equals("E") || transGlielaBinding.getValue().equals("I"))) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Cost Center is required.",
                                                          null));
        }

    }

    public void setTransGlielaBinding(RichInputText transGlielaBinding) {
        this.transGlielaBinding = transGlielaBinding;
    }

    public RichInputText getTransGlielaBinding() {
        return transGlielaBinding;
    }

    public void voucherDateValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (object != null) {
            if (vouTypeBind.getValue() != null && vouSeriesBinding.getValue() != null) {
                oracle.jbo.domain.Date d = null;
                java.util.Date d1 = null;
                String str = object.toString();
                try {
                    d = new oracle.jbo.domain.Date(str);
                    System.out.println("Date converted: " + d);
                } catch (Exception e) {
                    d1 = new java.util.Date(str);
                    System.out.println("Date converted: " + d1);
                }
                System.out.println("in method vou date" + object);
                OperationBinding op1 = ADFUtils.findOperation("checkForMonthAndYearVouSeriesForAllVouchers");
                if (d != null) {
                    op1.getParamsMap().put("vou_dt", d);
                }
                if (d1 != null) {
                    op1.getParamsMap().put("vou_dt", d1);
                }
                op1.getParamsMap().put("FinYear", ADFUtils.resolveExpression("#{pageFlowScope.finYear}"));
                //op1.getParamsMap().put("FinYear", "18-19");
                if (vouTypeBind.getValue().equals("B") && vouSeriesBinding.getValue().equals("R")) {
                    op1.getParamsMap().put("Vou_Type", "B");
                    op1.getParamsMap().put("Vou_Series", "R");
                }
                if (vouTypeBind.getValue().equals("B") && vouSeriesBinding.getValue().equals("P")) {
                    op1.getParamsMap().put("Vou_Type", "B");
                    op1.getParamsMap().put("Vou_Series", "P");
                }
                if (vouTypeBind.getValue().equals("C") && vouSeriesBinding.getValue().equals("R")) {
                    op1.getParamsMap().put("Vou_Type", "C");
                    op1.getParamsMap().put("Vou_Series", "R");

                }
                if (vouTypeBind.getValue().equals("C") && vouSeriesBinding.getValue().equals("P")) {
                    op1.getParamsMap().put("Vou_Type", "C");
                    op1.getParamsMap().put("Vou_Series", "P");
                }
                op1.getParamsMap().put("Unit_Code", unitBinding.getValue());
                op1.execute();
                if (op1.getResult() != null && !op1.getResult().toString().equalsIgnoreCase("Y")) {
                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                  op1.getResult().toString(),
                                                                  "Please Select Another Date."));

                }
            } else {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Please Select Type.",
                                                              null));
            }
        }

    }

    public void chequeNoVCL(ValueChangeEvent valueChangeEvent) {
        //valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vouSeriesBinding.getValue() != null) {
            if (!vouSeriesBinding.getValue().equals("R")) {
                if (checkNoBinding.getValue() != null && valueChangeEvent != null &&
                    chequesBinding.getValue() != null) {
                    if (chequesBinding.getValue().toString().equalsIgnoreCase("C")) {
                        System.out.println("Value after function for cheque number validation:" +
                                           checkNoBinding.getValue());
                        OperationBinding op1 = ADFUtils.findOperation("getChequeNoValidBankCashVoucherDuplicacy");
                        op1.getParamsMap().put("chequeno", checkNoBinding.getValue());
                        Object rst1 = op1.execute();
                        System.out.println("Value after function for cheque number validation:" + rst1);
                        if (rst1 != null && rst1.toString().trim().length() > 0 &&
                            rst1.toString().equalsIgnoreCase("Y")) {
                            ADFUtils.showMessage("Entered Cheque No./DD No. alredy Used.", 0);
                            checkNoBinding.setValue(null);
                            AdfFacesContext.getCurrentInstance().addPartialTarget(checkNoBinding);
                        }
                    }
                }
            }
        }
    }

    public void setNarrationDetailTabBinding(RichShowDetailItem narrationDetailTabBinding) {
        this.narrationDetailTabBinding = narrationDetailTabBinding;
    }

    public RichShowDetailItem getNarrationDetailTabBinding() {
        return narrationDetailTabBinding;
    }

    public void setVoucherDetailTabBinding(RichShowDetailItem voucherDetailTabBinding) {
        this.voucherDetailTabBinding = voucherDetailTabBinding;
    }

    public RichShowDetailItem getVoucherDetailTabBinding() {
        return voucherDetailTabBinding;
    }

    public void copyBankVoucherFromExisting(ActionEvent actionEvent) {
        OperationBinding op = ADFUtils.findOperation("copyBankVoucherFromExisting");
        op.execute();
    }
}


