package terms.fin.transaction.ui.bean;

import java.lang.reflect.Method;

import java.text.DateFormat;

import java.text.SimpleDateFormat;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.validator.ValidatorException;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCDataControl;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;

import terms.fin.transaction.model.applicationModule.FinTransactionAMImpl;
import terms.fin.transaction.model.view.BankRecoDetailVORowImpl;

public class BankRecoStatementBean {
    private RichPanelHeader getMyPageRootComponent;

    private RichButton headerEditBinding;
    private RichOutputText bindingOutputText;
    private RichInputText typeBinding;
    private RichInputText venDescBinding;
    private RichInputText interUnitDescBinding;
    private RichInputDate vouDateBinding;
    private RichInputText drCrBinding;
    private RichInputText transAmtBinding;
    private RichInputText chqNoBinding;
    private RichInputText bankAmtBinding;
    private RichInputText vouNoBinding;
    private RichInputText errBinding;
    private RichInputComboboxListOfValues unitBinding;
    private RichInputText recoNoBinding;
    private RichInputDate recoDateBinding;
    private RichInputComboboxListOfValues bankCodeBinding;
    private RichInputDate fromDateBinding;
    private RichInputDate toDateBinding;
    private RichInputText closingBalBinding;
    private RichInputText closeDrCrBinding;
    private RichColumn detailRecoNoBinding;
    private RichInputDate bankDateBinding;
    private RichTable bankRecoDetailTableBinding;
    private RichInputText passBookBinding;
    private RichSelectOneChoice drCrTypeBinding;
    private RichInputText rtGSNumberBinding;
    private RichInputDate rtgsDate;


    public BankRecoStatementBean() {
    }

    public void saveAL(ActionEvent actionEvent) 
    {
        
        DCIteratorBinding Dcite=ADFUtils.findIterator("BankRecoDetailVO1Iterator");
        BankRecoDetailVORowImpl row=(BankRecoDetailVORowImpl) Dcite.getCurrentRow();
        if((Long)ADFUtils.evaluateEL("#{bindings.BankRecoDetailVO1Iterator.estimatedRowCount}")>=1 )
        {
            System.out.println("row.getBankDate"+row.getBankDate());
            if(row.getBankDate() !=null)
              {
                OperationBinding op = ADFUtils.findOperation("generateRecoNo");
                Object rst = op.execute();
                    
                System.out.println("value aftr getting result--->?" + rst);
                if (rst.toString() != null && rst.toString() != "")
                {
                    if (op.getErrors().isEmpty()) 
                    {
                        try {//apoorv to  se value of recono 
                            DCIteratorBinding setrecono = ADFUtils.findIterator("BankRecoHeaderVO1Iterator");
                            String sss = setrecono.getCurrentRow().getAttribute("RecoNo").toString();
                            //System.out.println("value od sop sss @## "+sss);
                            setrecono.getCurrentRow().setAttribute("RecoNo", rst);
                        } catch (Exception ex) {
                            System.out.println("Exception" + ex);
                        }
                    OperationBinding op1= (OperationBinding) ADFUtils.findOperation("Commit");
                    op1.execute();
                                            
                    FacesMessage Message = new FacesMessage("Record Saved Successfully. New Reco No. is " + rst + ".");
                      Message.setSeverity(FacesMessage.SEVERITY_INFO);
                      FacesContext fc = FacesContext.getCurrentInstance();
                      fc.addMessage(null, Message);
                      
                      
                        ADFUtils.setEL("#{pageFlowScope.mode}","V");
                        cevmodecheck();
                        AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
                    }
                }

                if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ")
                {
                    if (op.getErrors().isEmpty())
                    {
                        ADFUtils.findOperation("Commit").execute();
                        FacesMessage Message = new FacesMessage("Record Update Successfully.");
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);
                        FacesContext fc = FacesContext.getCurrentInstance();
                        fc.addMessage(null, Message);
                        
                        ADFUtils.setEL("#{pageFlowScope.mode}","V");
                        cevmodecheck();
                        AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
                    }
                }
                
            RowSetIterator rsi = Dcite.getRowSetIterator();
            if(rsi!=null)
            {
                System.out.println("============yessssss==========");
                    Row[] allRowsInRange = rsi.getAllRowsInRange();
                    for (Row rw : allRowsInRange) 
                    {
                        System.out.println("allRowsInRange====="+allRowsInRange.length);
                        if (rw != null && rw.getAttribute("BankDate") == null)
                        {
                            System.out.println("------- Row remove method");
                            rw.remove();
                        }
                    }
             }
            rsi.closeRowSetIterator();
        }
        else{
              ADFUtils.showMessage("Bank Date must be required.Select Bank Date which row want to save in the detail table.", 0);    
            }
      }
    }

    public void populateRecoAL(ActionEvent actionEvent) 
    {
        if(fromDateBinding.getValue()!=null)
        { 
            System.out.println("the From date value is "+fromDateBinding.getValue());
        OperationBinding Pop = ADFUtils.findOperation("populateBankReco");
        Object PopObj = Pop.execute();

        if (Pop.getResult() != null && Pop.getResult().toString().equalsIgnoreCase("S")) {
//            FacesMessage Message = new FacesMessage("Record Populated successfully.");
//            Message.setSeverity(FacesMessage.SEVERITY_INFO);
//            FacesContext fc = FacesContext.getCurrentInstance();
//            fc.addMessage(null, Message);
            getBankCodeBinding().setDisabled(true);
            getToDateBinding().setDisabled(true);
            getFromDateBinding().setDisabled(true);
            getPassBookBinding().setDisabled(true);
            getDrCrTypeBinding().setDisabled(true);
            getRecoDateBinding().setDisabled(true);
//            AdfFacesContext.getCurrentInstance().addPartialTarget(bankRecoDetailTableBinding); 
        }

        else if(Pop.getResult() != null && Pop.getResult().toString().equalsIgnoreCase("V"))
        {
            FacesMessage fm=new FacesMessage("No Data Found For this Bank Code");
            fm.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc=FacesContext.getCurrentInstance();
            fc.addMessage(null, fm);
            }
            else if(Pop.getResult() != null && Pop.getResult().toString().equalsIgnoreCase("ERROR"))
            {
            ADFUtils.showMessage("Error occurs while data populate.", 0);
            }
        }
        else{
            ADFUtils.showMessage("All Field should be filled in Header before Populate Data.", 0);
        }

    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }


    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void EditButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {

        cevmodecheck();
        return bindingOutputText;
    }

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }

    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }

    public void cevModeDisableComponent(String mode) {

        if (mode.equals("C")) {
            getHeaderEditBinding().setDisabled(true);
            getCloseDrCrBinding().setDisabled(true);
            getClosingBalBinding().setDisabled(true);
            getRecoNoBinding().setDisabled(true);
            getUnitBinding().setDisabled(true);
            

        }
        if (mode.equals("V")) {


        }
        if (mode.equals("E")) {
            getRtGSNumberBinding().setDisabled(true);
            getRtgsDate().setDisabled(true);
            getRecoDateBinding().setDisabled(true);
            getBankDateBinding().setDisabled(true);
            getDrCrTypeBinding().setDisabled(true);
           getPassBookBinding().setDisabled(true);
            getUnitBinding().setDisabled(true);
            getRecoNoBinding().setDisabled(true);
            getBankCodeBinding().setDisabled(true);
            getFromDateBinding().setDisabled(true);
            getToDateBinding().setDisabled(true);
            getTypeBinding().setDisabled(true);
            getVenDescBinding().setDisabled(true);
            getInterUnitDescBinding().setDisabled(true);
            getTransAmtBinding().setDisabled(true);
            getVouDateBinding().setDisabled(true);
            getBankAmtBinding().setDisabled(true);
            getVouNoBinding().setDisabled(true);
            getErrBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            getChqNoBinding().setDisabled(true);
            getDrCrBinding().setDisabled(true);
            getClosingBalBinding().setDisabled(true);
            getCloseDrCrBinding().setDisabled(true);
        }
    }

    public void setTypeBinding(RichInputText typeBinding) {
        this.typeBinding = typeBinding;
    }

    public RichInputText getTypeBinding() {
        return typeBinding;
    }

    public void setVenDescBinding(RichInputText venDescBinding) {
        this.venDescBinding = venDescBinding;
    }

    public RichInputText getVenDescBinding() {
        return venDescBinding;
    }

    public void setInterUnitDescBinding(RichInputText interUnitDescBinding) {
        this.interUnitDescBinding = interUnitDescBinding;
    }

    public RichInputText getInterUnitDescBinding() {
        return interUnitDescBinding;
    }

    public void setVouDateBinding(RichInputDate vouDateBinding) {
        this.vouDateBinding = vouDateBinding;
    }

    public RichInputDate getVouDateBinding() {
        return vouDateBinding;
    }

    public void setDrCrBinding(RichInputText drCrBinding) {
        this.drCrBinding = drCrBinding;
    }

    public RichInputText getDrCrBinding() {
        return drCrBinding;
    }

    public void setTransAmtBinding(RichInputText transAmtBinding) {
        this.transAmtBinding = transAmtBinding;
    }

    public RichInputText getTransAmtBinding() {
        return transAmtBinding;
    }

    public void setChqNoBinding(RichInputText chqNoBinding) {
        this.chqNoBinding = chqNoBinding;
    }

    public RichInputText getChqNoBinding() {
        return chqNoBinding;
    }

    public void setBankAmtBinding(RichInputText bankAmtBinding) {
        this.bankAmtBinding = bankAmtBinding;
    }

    public RichInputText getBankAmtBinding() {
        return bankAmtBinding;
    }

    public void setVouNoBinding(RichInputText vouNoBinding) {
        this.vouNoBinding = vouNoBinding;
    }

    public RichInputText getVouNoBinding() {
        return vouNoBinding;
    }

    public void setErrBinding(RichInputText errBinding) {
        this.errBinding = errBinding;
    }

    public RichInputText getErrBinding() {
        return errBinding;
    }

    public void setUnitBinding(RichInputComboboxListOfValues unitBinding) {
        this.unitBinding = unitBinding;
    }

    public RichInputComboboxListOfValues getUnitBinding() {
        return unitBinding;
    }

    public void setRecoNoBinding(RichInputText recoNoBinding) {
        this.recoNoBinding = recoNoBinding;
    }

    public RichInputText getRecoNoBinding() {
        return recoNoBinding;
    }

    public void setRecoDateBinding(RichInputDate recoDateBinding) {
        this.recoDateBinding = recoDateBinding;
    }

    public RichInputDate getRecoDateBinding() {
        return recoDateBinding;
    }

    public void setBankCodeBinding(RichInputComboboxListOfValues bankCodeBinding) {
        this.bankCodeBinding = bankCodeBinding;
    }

    public RichInputComboboxListOfValues getBankCodeBinding() {
        return bankCodeBinding;
    }

    public void setFromDateBinding(RichInputDate fromDateBinding) {
        this.fromDateBinding = fromDateBinding;
    }

    public RichInputDate getFromDateBinding() {
        return fromDateBinding;
    }

    public void setToDateBinding(RichInputDate toDateBinding) {
        this.toDateBinding = toDateBinding;
    }

    public RichInputDate getToDateBinding() {
        return toDateBinding;
    }

    public void setClosingBalBinding(RichInputText closingBalBinding) {
        this.closingBalBinding = closingBalBinding;
    }

    public RichInputText getClosingBalBinding() {
        return closingBalBinding;
    }

    public void setCloseDrCrBinding(RichInputText closeDrCrBinding) {
        this.closeDrCrBinding = closeDrCrBinding;
    }

    public RichInputText getCloseDrCrBinding() {
        return closeDrCrBinding;
    }

    public void setDetailRecoNoBinding(RichColumn detailRecoNoBinding) {
        this.detailRecoNoBinding = detailRecoNoBinding;
    }

    public RichColumn getDetailRecoNoBinding() {
        return detailRecoNoBinding;
    }

    public void setBankDateBinding(RichInputDate bankDateBinding) {
        this.bankDateBinding = bankDateBinding;
    }

    public RichInputDate getBankDateBinding() {
        return bankDateBinding;
    }

    public void deletepopupDialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                   {
               
                   ADFUtils.findOperation("Delete1").execute();
                   FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                   Message.setSeverity(FacesMessage.SEVERITY_INFO);
                   FacesContext fc = FacesContext.getCurrentInstance();
                   fc.addMessage(null, Message);
    
               }
               AdfFacesContext.getCurrentInstance().addPartialTarget(bankRecoDetailTableBinding);   
        }

    public void setBankRecoDetailTableBinding(RichTable bankRecoDetailTableBinding) {
        this.bankRecoDetailTableBinding = bankRecoDetailTableBinding;
    }

    public RichTable getBankRecoDetailTableBinding() {
        return bankRecoDetailTableBinding;
    }

    public void setPassBookBinding(RichInputText passBookBinding) {
        this.passBookBinding = passBookBinding;
    }

    public RichInputText getPassBookBinding() {
        return passBookBinding;
    }

    public void setDrCrTypeBinding(RichSelectOneChoice drCrTypeBinding) {
        this.drCrTypeBinding = drCrTypeBinding;
    }

    public RichSelectOneChoice getDrCrTypeBinding() {
        return drCrTypeBinding;
    }

    public String saveandCloseAL() 
    {
        DCIteratorBinding Dcite=ADFUtils.findIterator("BankRecoDetailVO1Iterator");
        BankRecoDetailVORowImpl row=(BankRecoDetailVORowImpl) Dcite.getCurrentRow();
        if((Long)ADFUtils.evaluateEL("#{bindings.BankRecoDetailVO1Iterator.estimatedRowCount}")>=1 )
        {
            System.out.println("row.getBankDate"+row.getBankDate());
            if(row.getBankDate() !=null)
              {
                OperationBinding op = ADFUtils.findOperation("generateRecoNo");
                Object rst = op.execute();
                    
                System.out.println("value aftr getting result--->?" + rst);
                if (rst.toString() != null && rst.toString() != "")
                {
                    if (op.getErrors().isEmpty()) 
                    {
                      ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message = new FacesMessage("Record Saved Successfully. New Reco No. is " + rst + ".");
                      Message.setSeverity(FacesMessage.SEVERITY_INFO);
                      FacesContext fc = FacesContext.getCurrentInstance();
                      fc.addMessage(null, Message);
                      
                      
                        ADFUtils.setEL("#{pageFlowScope.mode}","V");
                        cevmodecheck();
                        AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
                        return "Save And Close";
                    }
                }

                if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ")
                {
                    if (op.getErrors().isEmpty())
                    {
                        ADFUtils.findOperation("Commit").execute();
                        FacesMessage Message = new FacesMessage("Record Update Successfully.");
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);
                        FacesContext fc = FacesContext.getCurrentInstance();
                        fc.addMessage(null, Message);
                        
                        ADFUtils.setEL("#{pageFlowScope.mode}","V");
                        cevmodecheck();
                        AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
                        return "Save And Close"; 
                        
                    }
                }
                
            RowSetIterator rsi = Dcite.getRowSetIterator();
            if(rsi!=null)
            {
                System.out.println("============yessssss==========");
                    Row[] allRowsInRange = rsi.getAllRowsInRange();
                    for (Row rw : allRowsInRange) 
                    {
                        System.out.println("allRowsInRange====="+allRowsInRange.length);
                        if (rw != null && rw.getAttribute("BankDate") == null)
                        {
                            System.out.println("------- Row remove method");
                            rw.remove();
                        }
                    }
             }
            rsi.closeRowSetIterator();
        }
        else{
              ADFUtils.showMessage("Bank Date must be required.Select Bank Date which row want to save in the detail table.", 0);
                return null;
            }
        }
        return null;
    }

    public void setRtGSNumberBinding(RichInputText rtGSNumberBinding) {
        this.rtGSNumberBinding = rtGSNumberBinding;
    }

    public RichInputText getRtGSNumberBinding() {
        return rtGSNumberBinding;
    }

    public void setRtgsDate(RichInputDate rtgsDate) {
        this.rtgsDate = rtgsDate;
    }

    public RichInputDate getRtgsDate() {
        return rtgsDate;
    }

    public void bankDateValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if(vouDateBinding.getValue()!=null && object!=null){
        java.sql.Timestamp vouDate=(java.sql.Timestamp)vouDateBinding.getValue();
        java.sql.Timestamp bankDate=(java.sql.Timestamp)object;
        if(vouDate.compareTo(bankDate)==1){
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Bank Date should be greater than or equal to Voucher Date.", null));
            }

    }
    }

    public void onChangerRecoStartDate(ValueChangeEvent valueChangeEvent) { //apoorvr's method to velidate frome date
        oracle.jbo.domain.Date std = null;
        if (valueChangeEvent.getNewValue() == null || valueChangeEvent.getNewValue() == "") {
        } else {

            if (valueChangeEvent.getNewValue() != null) {
                try {
                    String inputDate = valueChangeEvent.getNewValue().toString();
                    DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

                    java.util.Date date = formatter.parse(inputDate);
                    java.sql.Date sqlDate = new java.sql.Date(date.getTime());
                    oracle.jbo.domain.Date jboDate = new oracle.jbo.domain.Date(sqlDate);
                    System.out.println("Try*******" + jboDate);
                    std = jboDate;

                } catch (Exception e) {
                    e.printStackTrace();
                }
                //        System.out.println("vcl value####### "+jboDate);
                //        std =(oracle.jbo.domain.Date)jboDate;
                //        System.out.println("vcl value####### "+std);
                BindingContext bindingContext = BindingContext.getCurrent();
                DCDataControl dc = bindingContext.findDataControl("FinTransactionAMDataControl");

                FinTransactionAMImpl appM = (FinTransactionAMImpl) dc.getDataProvider();
                String Date = appM.startDate().toString();
                System.out.println("start Date Value ---" + Date);
                oracle.jbo.domain.Date jboDate1 = new oracle.jbo.domain.Date(Date);
                oracle.jbo.domain.Date cmpd = jboDate1;


                if (Date != null) {
                    if (std.compareTo(cmpd) == -1) {


                        System.out.println("You Enter date grater");
                        ADFUtils.showMessage("Please Enter date Between Financial Year", 0);
                        fromDateBinding.setValue(null);
                        AdfFacesContext adfFacesContext = AdfFacesContext.getCurrentInstance();
                        adfFacesContext.addPartialTarget(fromDateBinding);

                    }


                }
            }
        }


    }


    public void onchangeRecoToDate(ValueChangeEvent valueChangeEvent) { //apoorvr's method to velidate to date
        oracle.jbo.domain.Date std = null;
        if (valueChangeEvent.getNewValue() == null || valueChangeEvent.getNewValue() == "") {
        } else {

            if (valueChangeEvent.getNewValue() != null) {
                try {
                    String inputDate = valueChangeEvent.getNewValue().toString();
                    DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

                    java.util.Date date = formatter.parse(inputDate);
                    java.sql.Date sqlDate = new java.sql.Date(date.getTime());
                    oracle.jbo.domain.Date jboDate = new oracle.jbo.domain.Date(sqlDate);
                    System.out.println("Try*******" + jboDate);
                    std = jboDate;

                } catch (Exception e) {
                    e.printStackTrace();
                }
                //        System.out.println("vcl value####### "+jboDate);
                //        std =(oracle.jbo.domain.Date)jboDate;
                //        System.out.println("vcl value####### "+std);
                BindingContext bindingContext = BindingContext.getCurrent();
                DCDataControl dc = bindingContext.findDataControl("FinTransactionAMDataControl");

                FinTransactionAMImpl appM = (FinTransactionAMImpl) dc.getDataProvider();
                String Date = appM.EndDat().toString();
                System.out.println("start Date Value ---" + Date);
                oracle.jbo.domain.Date jboDate1 = new oracle.jbo.domain.Date(Date);
                oracle.jbo.domain.Date cmpd = jboDate1;


                if (Date != null) {
                    if (std.compareTo(cmpd) == 1) {


                        System.out.println("You Enter date grater");
                        ADFUtils.showMessage("Please Enter date Between Financial Year", 0);
                        toDateBinding.setValue(null);
                        AdfFacesContext adfFacesContext = AdfFacesContext.getCurrentInstance();
                        adfFacesContext.addPartialTarget(toDateBinding);

                    }


                }
            }
        }
    
    
    }
        
    
}

