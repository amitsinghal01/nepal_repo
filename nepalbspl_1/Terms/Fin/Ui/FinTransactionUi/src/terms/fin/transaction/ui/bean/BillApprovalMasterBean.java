package terms.fin.transaction.ui.bean;

import java.math.BigDecimal;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.domain.Date;

public class BillApprovalMasterBean 

{
    private String check_value="Y";
    private RichPopup approvePopupBinding;
    private RichInputDate transCurrentDateBinding;

    public BillApprovalMasterBean() {
    }

    public void selectButtonAL(ActionEvent actionEvent) {
        
    if (transCurrentDateBinding.getValue()!=null) {
            OperationBinding opr = ADFUtils.findOperation("selectUnselectBillApproval");
            opr.getParamsMap().put("check_value", check_value);
            opr.getParamsMap().put("employeeCode", ADFUtils.evaluateEL("#{pageFlowScope.empCode}"));
            opr.getParamsMap().put("billdate", transCurrentDateBinding.getValue());
            //opr.getParamsMap().put("employeeCode","SWE161");
            opr.execute();
            //employeeCode
            if (opr.getResult() != null)
                check_value = opr.getResult().toString();
        }else
    {
        ADFUtils.showMessage("Please Select Date For Approval", 0);

    }

    
    }

    public void approveRecordAL(ActionEvent actionEvent) {
//        OperationBinding opr = ADFUtils.findOperation("voucherGenerationBillApproval");
////        opr.getParamsMap().put("UserName","#{pageFlowScope.userName}");
////        opr.getParamsMap().put("empCode","#{pageFlowScope.empCode}");
//        opr.getParamsMap().put("UserName","Rajat");
//        opr.getParamsMap().put("empCode","SWE161");
//        opr.execute();
//        System.out.println("RESULT IS:="+opr.getResult());
//        
//        if(opr.getResult().equals("Y"))
//        {
//        ADFUtils.showMessage("Voucher Generated Successfully.",2);
//        ADFUtils.findOperation("Commit").execute();
//        
//        }
//        else if(opr.getResult().equals("N"))
//        {
//        }

        ADFUtils.showPopup(approvePopupBinding);
    }

    public void approveDL(DialogEvent dialogEvent) 
    {
   if(dialogEvent.getOutcome().name().equals("ok"))
    {
            OperationBinding opr = ADFUtils.findOperation("voucherGenerationBillApproval");
            opr.getParamsMap().put("UserName",ADFUtils.evaluateEL("#{pageFlowScope.userName}"));
            opr.getParamsMap().put("empCode",ADFUtils.evaluateEL("#{pageFlowScope.empCode}"));
//            opr.getParamsMap().put("UserName","Rajat");
//            opr.getParamsMap().put("empCode","SWE161");
            opr.execute();
            System.out.println("RESULT IS:="+opr.getResult());
            
            if(opr.getResult().equals("Y"))
            {
            //ADFUtils.showMessage("Voucher Generated Successfully.",2);
            ADFUtils.findOperation("Commit").execute();
            
            ADFUtils.findIterator("BillApprovalMasterVO1Iterator").executeQuery();
            
            }
            else if(opr.getResult().equals("N"))
            {
            }
        }
        else
        {
            approvePopupBinding.hide();
        }
    
    }

    public void setApprovePopupBinding(RichPopup approvePopupBinding) {
        this.approvePopupBinding = approvePopupBinding;
    }

    public RichPopup getApprovePopupBinding() {
        return approvePopupBinding;
    }

    public void checkBoxValue(ValueChangeEvent valueChangeEvent) 
    {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());

        if (valueChangeEvent.getNewValue()!=null)
        {
            Row row = (Row) ADFUtils.evaluateEL("#{bindings.BillApprovalMasterVO1Iterator.currentRow}");
            System.out.println("Trans Date Value"+transCurrentDateBinding.getValue());
            
            if(transCurrentDateBinding.getValue()!=null) {
                
                System.out.println("YES NO ===>" + row.getAttribute("YN"));
                if (row.getAttribute("YN").equals("Y")) {
                    Date billApprovalDate = (Date)row.getAttribute("TransCurrentDate");
                    Date bill_Date = (Date)row.getAttribute("BillDate");                
                    System.out.println("Bill Approval Date is=>"+billApprovalDate);
                    System.out.println("Bill Date is=>"+bill_Date);
                    System.out.println("billApprovalDate.compareTo(billDate)====>"+billApprovalDate.compareTo(bill_Date));
                    
                    if(!(billApprovalDate.compareTo(bill_Date)==-1)) {
                        OperationBinding opr = ADFUtils.findOperation("checkAuthorityBillApproval");
                        opr.getParamsMap().put("authoLevel", "AP");
                        opr.getParamsMap().put("empCode", ADFUtils.evaluateEL("#{pageFlowScope.empCode}"));
                        opr.getParamsMap().put("formName", "FINBD");
                        opr.getParamsMap().put("unitCode", row.getAttribute("UnitCode"));
                        opr.getParamsMap().put("amount", row.getAttribute("BillAmt"));
                        opr.execute();
                        if (opr.getResult() != null) {
                            if (opr.getResult().equals("AM")) {
                                row.setAttribute("FinMauthAuthCode", ADFUtils.evaluateEL("#{pageFlowScope.empCode}"));
                                row.setAttribute("ApprovalDate", transCurrentDateBinding.getValue());
                            } else {
                                //           row.setAttribute("FinMauthAuthCode",ADFUtils.evaluateEL("#{pageFlowScope.empCode}"));
                                //           row.setAttribute("ApprovalDate",transCurrentDateBinding.getValue());
                                ADFUtils.showMessage("You don't have permission to approved this record.", 0);
                            }
                        }
                    }
                    else
                    {
                    ADFUtils.showMessage("Bill Approval Date must be greater then or equals to Bill Date",0);
                    row.setAttribute("FinMauthAuthCode", null);
                    row.setAttribute("ApprovalDate", null);
                    row.setAttribute("YN", "N");
                    
                    }
                } else {
                    row.setAttribute("FinMauthAuthCode", null);
                    row.setAttribute("ApprovalDate", null);
                    row.setAttribute("YN", "N");
                }
            }
            else
            {
            row.setAttribute("FinMauthAuthCode", null);
            row.setAttribute("ApprovalDate", null);
            row.setAttribute("YN", "N");
            ADFUtils.showMessage("Please Select Date For Approval", 0);
            }
        }
    }

    public void setTransCurrentDateBinding(RichInputDate transCurrentDateBinding) {
        this.transCurrentDateBinding = transCurrentDateBinding;
    }

    public RichInputDate getTransCurrentDateBinding() {
        return transCurrentDateBinding;
    }

    public void billdateVCE(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        System.out.println("Date VCE VALUE"+valueChangeEvent.getNewValue());
        OperationBinding opr= ADFUtils.findOperation("transBillDateValueSetBillApproval");
        opr.getParamsMap().put("billdate", valueChangeEvent.getNewValue());
        opr.execute();
    }
}
