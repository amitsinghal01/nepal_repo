package terms.fin.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;

public class BillDeletionServiceBean {
    private RichPanelHeader getMyPageRootComponent;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputComboboxListOfValues approvedByBinding;
    private RichOutputText outputTextBinding;
    private RichInputText billTypeBinding;
    private RichInputDate billDateBinding;
    private RichInputComboboxListOfValues billNoBinding;

    public BillDeletionServiceBean() {
    }

    public void saveAL(ActionEvent actionEvent) {
        if(approvedByBinding.getValue()!=null)
        {
            OperationBinding opr =(OperationBinding) ADFUtils.findOperation("saveUnApproveBillDeletion");
            opr.execute();
            System.out.println("Result======="+opr.getResult());
            if(opr.getResult()!=null)
            {
            if(opr.getResult().equals("Y"))
            {
                System.out.println("==========When result is Y==========");
                ADFUtils.findOperation("Commit").execute();
                ADFUtils.showMessage("Record saved successfully.", 2);
                ADFUtils.setEL("#{pageFlowScope.mode}","V");
            }
            }
        }else{
            ADFUtils.showMessage("Approved By is required.", 0);
        }
    }
    
    
    private void cevmodecheck(){
            if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
                cevModeDisableComponent("V");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("C");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("E");
            }
        }
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
              try {
                  Method method1 =
                      component.getClass().getMethod("setDisabled", boolean.class);
                  if (method1 != null) {
                      method1.invoke(component, valueComponent);
                  }
              } catch (NoSuchMethodException e) {
                  try {
                      Method method =
                          component.getClass().getMethod("setReadOnly", boolean.class);
                      if (method != null) {
                          method.invoke(component, valueComponent);
                      }
                  } catch (Exception e1) {
                      // e.printStackTrace();//silently eat this exception.
                  }


              } catch (Exception e) {
                  // e.printStackTrace();//silently eat this exception.
              }
              List<UIComponent> childComponents = component.getChildren();
              for (UIComponent comp : childComponents) {
                  makeComponentHierarchyReadOnly(comp, valueComponent);
              }
          }
    //Set Fields and Button disable.
      public void cevModeDisableComponent(String mode) {
      //            FacesContext fctx = FacesContext.getCurrentInstance();
      //            ELContext elctx = fctx.getELContext();
      //            Application jsfApp = fctx.getApplication();
      //            //create a ValueExpression that points to the ADF binding layer
      //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
      //            //
      //            ValueExpression valueExpr = exprFactory.createValueExpression(
      //                                         elctx,
      //                                         "#{pageFlowScope.mode=='E'}",
      //                                          Object.class
      //                                         );
      //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
          if (mode.equals("E")) {
              getBillDateBinding().setDisabled(true);
              getBillTypeBinding().setDisabled(true);
              getUnitCodeBinding().setDisabled(true);
          } else if (mode.equals("C")) {
              getBillDateBinding().setDisabled(true);
              getBillTypeBinding().setDisabled(true);
              getUnitCodeBinding().setDisabled(true);
          } else if (mode.equals("V")) {
           
          }
          
      }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public String saveAndClose() {
        if(approvedByBinding.getValue()!=null)
        {
            OperationBinding opr =(OperationBinding) ADFUtils.findOperation("saveUnApproveBillDeletion");
            opr.execute();
            System.out.println("Result======="+opr.getResult());
            if(opr.getResult()!=null)
            {
            if(opr.getResult().equals("Y"))
            {
                System.out.println("==========When result is Y==========");
                ADFUtils.findOperation("Commit").execute();
                ADFUtils.showMessage("Record saved successfully.", 2);
                ADFUtils.setEL("#{pageFlowScope.mode}","V");
                return "saveAndClose";
            }
            }
        }else{
            ADFUtils.showMessage("Approved By is required.", 0);
            return null;
        }
        return null;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setApprovedByBinding(RichInputComboboxListOfValues approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputComboboxListOfValues getApprovedByBinding() {
        return approvedByBinding;
    }

    public void approveByVCE(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(valueChangeEvent!=null){
//            if(billNoBinding.getValue()!=null){
                ADFUtils.findOperation("checkBillDeletionAuthority").execute();
        //    }
//            else{
//                approvedByBinding.setValue(null);
//                AdfFacesContext.getCurrentInstance().addPartialTarget(approvedByBinding);
//                FacesMessage message = new FacesMessage("Bill No. is required.");
//                message.setSeverity(FacesMessage.SEVERITY_ERROR);
//                FacesContext context = FacesContext.getCurrentInstance();
//                context.addMessage(billNoBinding.getClientId(), message);
//            }
        }
        
    }

    public void setOutputTextBinding(RichOutputText outputTextBinding) {
        this.outputTextBinding = outputTextBinding;
    }

    public RichOutputText getOutputTextBinding() {
        cevmodecheck();
        return outputTextBinding;
    }

    public void editAL(ActionEvent actionEvent) {
        // Add event code here...
    }

    public void setBillTypeBinding(RichInputText billTypeBinding) {
        this.billTypeBinding = billTypeBinding;
    }

    public RichInputText getBillTypeBinding() {
        return billTypeBinding;
    }

    public void setBillDateBinding(RichInputDate billDateBinding) {
        this.billDateBinding = billDateBinding;
    }

    public RichInputDate getBillDateBinding() {
        return billDateBinding;
    }

    public void setBillNoBinding(RichInputComboboxListOfValues billNoBinding) {
        this.billNoBinding = billNoBinding;
    }

    public RichInputComboboxListOfValues getBillNoBinding() {
        return billNoBinding;
    }
}
