package terms.fin.transaction.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.validator.ValidatorException;

import oracle.adf.model.OperationBinding;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

public class BillOfEntryBean {
    private RichPanelHeader getMyPageRootComponent;
    private RichButton editBinding;
    private RichOutputText getBindingOutputText;
    private RichInputText docNoBinding;
    private RichTable bindDetailTable;
    private RichShowDetailItem billEntryBinding;
    private RichShowDetailItem billEntryDetailBinding;
    private RichInputText docNoDetailBinding;
    private RichInputText licNoBinding;
    private RichInputDate licDateBinding;
    private RichInputText prodCodeBinding;
    private RichInputText balQtBinding;
    private RichInputText balAmtBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputComboboxListOfValues preparedByBinding;
    private RichInputComboboxListOfValues approvedByBinding;
    private RichInputDate invDate;
    private RichInputText impAmtBinding;
    private RichInputText boeNoBinding;
    private RichInputText saleInvoiceNoBinding;
    private RichInputText invalidationNoBinding;

    public BillOfEntryBean() {
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }
    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }

    public void cevModeDisableComponent(String mode) {
        if (mode.equals("E")) {
          getDocNoBinding().setDisabled(true);
          getDocNoDetailBinding().setDisabled(true);
          getEditBinding().setDisabled(true); 
         getBalAmtBinding().setDisabled(true);
          getBalQtBinding().setDisabled(true);
//          getProdCodeBinding().setDisabled(true);
          getLicDateBinding().setDisabled(true);
          getUnitCodeBinding().setDisabled(true);
          getPreparedByBinding().setDisabled(true);
          //getLicNoBinding().setDisabled(true);
          getInvDate().setDisabled(true);
        }
        if (mode.equals("C")) {
          getDocNoBinding().setDisabled(true);  
          getDocNoDetailBinding().setDisabled(true);
          getEditBinding().setDisabled(true);  
          getBalAmtBinding().setDisabled(true);
          getBalQtBinding().setDisabled(true);
//          getProdCodeBinding().setDisabled(true);
          getLicDateBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
            getPreparedByBinding().setDisabled(true);
          //getLicNoBinding().setDisabled(true);
          getInvDate().setDisabled(false);
        }
        if (mode.equals("V")) {
          getBillEntryDetailBinding().setDisabled(false);
          getBillEntryBinding().setDisabled(false);

        }

    }

    public void setEditBinding(RichButton editBinding) {
        this.editBinding = editBinding;
    }

    public RichButton getEditBinding() {
        return editBinding;
    }

    public void EditButtonAL(ActionEvent actionEvent) {
        if (approvedByBinding.getValue() == null) {
            cevmodecheck();
        } else {
            ADFUtils.showMessage("Record is Approved Can't be modified !", 2);
            ADFUtils.setEL("#{pageFlowScope.mode}", "V");
        }
    }

    public void setGetBindingOutputText(RichOutputText getBindingOutputText) {
        this.getBindingOutputText = getBindingOutputText;
    }

    public RichOutputText getGetBindingOutputText() {
        cevmodecheck();
        return getBindingOutputText;
    }

    public void setDocNoBinding(RichInputText docNoBinding) {
        this.docNoBinding = docNoBinding;
    }

    public RichInputText getDocNoBinding() {
        return docNoBinding;
    }

    public void saveAL(ActionEvent actionEvent) {
        if(boeNoBinding.getValue() != null || saleInvoiceNoBinding.getValue()!= null || invalidationNoBinding.getValue() != null) 
        {
        OperationBinding op=(OperationBinding)ADFUtils.findOperation("generateDocNoBOE");
                Object obj= op.execute();
                System.out.println("result after function calling: "+op.getResult());
                if(op.getResult()!=null ){
                    if(op.getResult().equals("Y")){
                    ADFUtils.findOperation("Commit").execute();
                    ADFUtils.showMessage("Doc No Generated is "+docNoBinding.getValue(), 2);
                    }
                    else{
                    ADFUtils.findOperation("Commit").execute();
                    ADFUtils.showMessage("Record saved successfully.", 2);
                    }
        //            ADFUtils.findOperation("CreateInsert").execute();
                    }
        }else{
            ADFUtils.showMessage("Please select of these BOE No. or Sale Invoice No or Invalidation No.", 0);
        }
    }

    public String saveAndCloseAL() {
        if(boeNoBinding.getValue() != null || saleInvoiceNoBinding.getValue()!= null || invalidationNoBinding.getValue() != null) 
        {
            OperationBinding op=(OperationBinding)ADFUtils.findOperation("generateDocNoBOE");
            Object obj= op.execute();
            System.out.println("result after function calling: "+op.getResult());
            if(op.getResult()!=null )
            {
                if(op.getResult().equals("Y"))
                {
                    ADFUtils.findOperation("Commit").execute();
                    ADFUtils.showMessage("Doc No Generated is "+docNoBinding.getValue(), 2);
                }
                else{
                    ADFUtils.findOperation("Commit").execute();
                    ADFUtils.showMessage("Record updated successfully.", 2);
                }
            //            ADFUtils.findOperation("CreateInsert").execute();
              return "SaveAndClose";
            }
        }
        else{
            ADFUtils.showMessage("Please select of these BOE No. or Sale Invoice No or Invalidation No.", 0);
            return null;
        }
        return null;
    }

    public void deleteRecordDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
                   ADFUtils.findOperation("Delete").execute();
                   System.out.println("Record Delete Successfully");
                   FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                   Message.setSeverity(FacesMessage.SEVERITY_INFO);
                   FacesContext fc = FacesContext.getCurrentInstance();
                   fc.addMessage(null, Message);
               }

               AdfFacesContext.getCurrentInstance().addPartialTarget(bindDetailTable);
    }

    public void setBindDetailTable(RichTable bindDetailTable) {
        this.bindDetailTable = bindDetailTable;
    }

    public RichTable getBindDetailTable() {
        return bindDetailTable;
    }

    public void setBillEntryBinding(RichShowDetailItem billEntryBinding) {
        this.billEntryBinding = billEntryBinding;
    }

    public RichShowDetailItem getBillEntryBinding() {
        return billEntryBinding;
    }

    public void setBillEntryDetailBinding(RichShowDetailItem billEntryDetailBinding) {
        this.billEntryDetailBinding = billEntryDetailBinding;
    }

    public RichShowDetailItem getBillEntryDetailBinding() {
        return billEntryDetailBinding;
    }

    public void setDocNoDetailBinding(RichInputText docNoDetailBinding) {
        this.docNoDetailBinding = docNoDetailBinding;
    }

    public RichInputText getDocNoDetailBinding() {
        return docNoDetailBinding;
    }

    public void setLicNoBinding(RichInputText licNoBinding) {
        this.licNoBinding = licNoBinding;
    }

    public RichInputText getLicNoBinding() {
        return licNoBinding;
    }

    public void setLicDateBinding(RichInputDate licDateBinding) {
        this.licDateBinding = licDateBinding;
    }

    public RichInputDate getLicDateBinding() {
        return licDateBinding;
    }

    public void setProdCodeBinding(RichInputText prodCodeBinding) {
        this.prodCodeBinding = prodCodeBinding;
    }

    public RichInputText getProdCodeBinding() {
        return prodCodeBinding;
    }

    public void setBalQtBinding(RichInputText balQtBinding) {
        this.balQtBinding = balQtBinding;
    }

    public RichInputText getBalQtBinding() {
        return balQtBinding;
    }

    public void setBalAmtBinding(RichInputText balAmtBinding) {
        this.balAmtBinding = balAmtBinding;
    }
    public RichInputText getBalAmtBinding() {
        return balAmtBinding;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setPreparedByBinding(RichInputComboboxListOfValues preparedByBinding) {
        this.preparedByBinding = preparedByBinding;
    }

    public RichInputComboboxListOfValues getPreparedByBinding() {
        return preparedByBinding;
    }

    public void setApprovedByBinding(RichInputComboboxListOfValues approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputComboboxListOfValues getApprovedByBinding() {
        return approvedByBinding;
    }

    public void setInvDate(RichInputDate invDate) {
        this.invDate = invDate;
    }

    public RichInputDate getInvDate() {
        return invDate;
    }

    public void ImpQtyValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if(object!=null && balQtBinding.getValue()!=null){
            BigDecimal v1=(BigDecimal)balQtBinding.getValue();
            BigDecimal v2=(BigDecimal)object;
            if(v2.compareTo(v1)==1){
                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,"Imp Qty should be less than or equal to Bal Qty.", null));
                }
            }

    }

    public void impAmtValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {

    }

    public void importAmtVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce!=null){
                BigDecimal val=(BigDecimal)vce.getNewValue();
                OperationBinding op=(OperationBinding)ADFUtils.findOperation("importAmtCalcBOE");
                op.getParamsMap().put("value", val);
                op.execute();
                System.out.println("result after function Call: "+op.getResult());
                BigDecimal result=(BigDecimal)op.getResult();
                if(!(result.compareTo(new BigDecimal(0))==0)){
                        FacesMessage message = new FacesMessage("Quantity limit exceed for this Lic No. with "+op.getResult()+" value.");
                        message.setSeverity(FacesMessage.SEVERITY_ERROR);
                        FacesContext context = FacesContext.getCurrentInstance();
                        context.addMessage(impAmtBinding.getClientId(), message);
                    }
                AdfFacesContext.getCurrentInstance().addPartialTarget(bindDetailTable);
            }
    }

    public void setImpAmtBinding(RichInputText impAmtBinding) {
        this.impAmtBinding = impAmtBinding;
    }

    public RichInputText getImpAmtBinding() {
        return impAmtBinding;
    }

    public void setBoeNoBinding(RichInputText boeNoBinding) {
        this.boeNoBinding = boeNoBinding;
    }

    public RichInputText getBoeNoBinding() {
        return boeNoBinding;
    }

    public void setSaleInvoiceNoBinding(RichInputText saleInvoiceNoBinding) {
        this.saleInvoiceNoBinding = saleInvoiceNoBinding;
    }

    public RichInputText getSaleInvoiceNoBinding() {
        return saleInvoiceNoBinding;
    }

    public void setInvalidationNoBinding(RichInputText invalidationNoBinding) {
        this.invalidationNoBinding = invalidationNoBinding;
    }

    public RichInputText getInvalidationNoBinding() {
        return invalidationNoBinding;
    }
    
    // New Method================
   public String requiredAttributes() 
   {
        if(boeNoBinding.getValue() != null || saleInvoiceNoBinding.getValue()!= null || invalidationNoBinding.getValue() != null) 
        {
            return "Y";
        }
       else if(boeNoBinding.getValue() == null)
        {
           ADFUtils.showMessage("BOE No. is required.", 0);
           return "N";
       }
       else if(saleInvoiceNoBinding.getValue() == null){
           ADFUtils.showMessage("Sale Invoice No.is required.", 0);
           return "N";
       }
      else if(invalidationNoBinding.getValue() == null){
           ADFUtils.showMessage("InValidation No. is required.", 0);
           return "N";
       }

       return "N";
   }
}
