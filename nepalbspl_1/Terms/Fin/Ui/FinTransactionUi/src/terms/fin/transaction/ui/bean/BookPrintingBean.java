package terms.fin.transaction.ui.bean;

import java.io.ByteArrayOutputStream;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import java.io.OutputStream;

import java.sql.Connection;

import java.sql.SQLException;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import javax.faces.event.ValueChangeEvent;
import javax.faces.validator.ValidatorException;

import javax.naming.Context;
import javax.naming.InitialContext;

import javax.servlet.http.HttpServletResponse;

import javax.sql.DataSource;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import oracle.adf.model.BindingContext;
import oracle.adf.model.OperationBinding;
import oracle.adf.model.binding.DCIteratorBinding;

import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;

import oracle.binding.BindingContainer;

import oracle.jbo.domain.Date;

public class BookPrintingBean {
    private String BookMode ="";
    private RichSelectOneChoice bookTypeBinding;
    private RichInputDate fromDateBinding;
    private RichInputDate toDateBinding;

    public BookPrintingBean() {
    }
   
    public BindingContainer getBindings()
          {
            return (BindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();
          }
        
        public void downloadJrxml(FacesContext facesContext, OutputStream outputStream) {
            Connection conn=null;
            String book = (String) bookTypeBinding.getValue();
            try {

                String file_name ="";
                System.out.println("book===================>"+book);
                
                if(book.equalsIgnoreCase("B") ||book.equalsIgnoreCase("C"))
                {
                file_name="r_bank_bk.jasper";
                }
                else if(((!book.equalsIgnoreCase("B") ||(!book.equalsIgnoreCase("C"))) && (!book.equalsIgnoreCase("D")) )&& (!book.equalsIgnoreCase("CL")))
                {
                    file_name="r_jour_bk.jasper";
                }
                else if(book.equalsIgnoreCase("D"))
                {
                    file_name="r_day_bk.jasper";
                }
                else if(book.equalsIgnoreCase("CL")){
                    file_name="r_check_list.jasper"; 
                }
               System.out.println("After Set Value File Name is===>"+file_name);
                
                
                oracle.binding.OperationBinding binding = ADFUtils.findOperation("fileNameForPrint");
                binding.getParamsMap().put("fileId","ERP0000000174");
                binding.execute();
                System.out.println("Binding Result :"+binding.getResult());
                if(binding.getResult() != null){
                    String result = binding.getResult().toString();
                    int last_index = result.lastIndexOf("/");
                    String path = result.substring(0,last_index+1)+file_name;
                    System.out.println("FILE PATH IS===>"+path);
                    InputStream input = new FileInputStream(path); 
System.out.println("Book===="+book);
                    DCIteratorBinding poIter = (DCIteratorBinding) getBindings().get("BookPrintingVO1Iterator");
                    oracle.jbo.domain.Date DateFrom = (oracle.jbo.domain.Date) poIter.getCurrentRow().getAttribute("DateFrom");
                    oracle.jbo.domain.Date DateTo=(oracle.jbo.domain.Date) poIter.getCurrentRow().getAttribute("DateTo");
                    String unitCode = (String) poIter.getCurrentRow().getAttribute("UnitCd");
                    String BookType = (String) poIter.getCurrentRow().getAttribute("BookType");
                String BankGL = (String) poIter.getCurrentRow().getAttribute("BankCode");
                String CashGL = (String) poIter.getCurrentRow().getAttribute("CashCode");
                
                System.out.println("DateFrom:=>"+DateFrom+"||DateTo:=>"+DateTo+"||unitCode:=>"+unitCode+"||BookType:=>"+BookType+"||BankGL:=>"+BankGL+"||CashGL"+CashGL);
                 
                    if(book!=null && (book.equalsIgnoreCase("B")||book.equalsIgnoreCase("C")))
                    {
                        System.out.println("Book in bankcash===="+book);
                    Map n = new HashMap();
                    n.put("p_fr_dt", DateFrom);
                    n.put("p_to_dt", DateTo);
                    n.put("p_unit", unitCode);
                    n.put("p_vou_tp", BookType);
                    if(book.equalsIgnoreCase("B"))
                    {
                    n.put("p_gnrl_cd", BankGL);
                    }
                    else if(book.equalsIgnoreCase("C"))
                            {
                        n.put("p_gnrl_cd", CashGL);
                        }
                   

                        conn = getConnection( );
                        
                        JasperReport design = (JasperReport) JRLoader.loadObject(input);
                        System.out.println("Path : " + input + " -------" + design+" param:"+n);
                        
                        @SuppressWarnings("unchecked")
                        net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                        byte pdf[] = JasperExportManager.exportReportToPdf(print);
                        HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                        response.getOutputStream().write(pdf);
                        response.getOutputStream().flush();
                        response.getOutputStream().close();
                        facesContext.responseComplete();
                    }
                    else if(book.equalsIgnoreCase("CL")){
                            Map n = new HashMap();
                                n.put("p_unit", unitCode);
                                n.put("p_fr_dt", DateFrom);
                                n.put("p_to_dt", DateTo);
                                n.put("p_gnrl_cd", CashGL);
                                n.put("p_vou_tp", BookType);
                            conn = getConnection( );
                            
                            JasperReport design = (JasperReport) JRLoader.loadObject(input);
                            System.out.println("Path : " + input + " -------" + design+" param:"+n);
                            
                            @SuppressWarnings("unchecked")
                            net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                            JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                            byte pdf[] = JasperExportManager.exportReportToPdf(print);
                            HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                            response.getOutputStream().write(pdf);
                            response.getOutputStream().flush();
                            response.getOutputStream().close();
                            facesContext.responseComplete();
                        }
                    else if((!book.equalsIgnoreCase("B") ||(!book.equalsIgnoreCase("C")) ||(!book.equalsIgnoreCase("D")))&& (!book.equalsIgnoreCase("CL")))
                    {
                            Map n = new HashMap();
                                n.put("p_fr_dt", DateFrom);
                                n.put("p_to_dt", DateTo);
                                n.put("p_unit", unitCode);
                                n.put("p_vou_tp", BookType);
                                conn = getConnection( );
                                
                                JasperReport design = (JasperReport) JRLoader.loadObject(input);
                                System.out.println("Path : " + input + " -------" + design+" param:"+n);
                                
                                @SuppressWarnings("unchecked")
                                net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                                JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                                byte pdf[] = JasperExportManager.exportReportToPdf(print);
                                HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                                response.getOutputStream().write(pdf);
                                response.getOutputStream().flush();
                                response.getOutputStream().close();
                                facesContext.responseComplete();
                            } 
                        else if(book.equalsIgnoreCase("D"))
                        {
                    Map n = new HashMap();
                    n.put("p_fr_dt", DateFrom);
                    n.put("p_to_dt", DateTo);
                    n.put("p_unit", unitCode);
                    n.put("p_vou_tp", BookType);
                    conn = getConnection( );
                    
                    JasperReport design = (JasperReport) JRLoader.loadObject(input);
                    System.out.println("Path : " + input + " -------" + design+" param:"+n);
                    
                    @SuppressWarnings("unchecked")
                    net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                    byte pdf[] = JasperExportManager.exportReportToPdf(print);
                    HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                    response.getOutputStream().write(pdf);
                    response.getOutputStream().flush();
                    response.getOutputStream().close();
                    facesContext.responseComplete();
                }
                    }else
                    System.out.println("File Name/File Path not found.");
                    } catch (FileNotFoundException fnfe) {
                    // TODO: Add catch code
                    fnfe.printStackTrace();
                    try {
                            System.out.println("in finally connection closed");
                                    conn.close( );
                                    conn = null;
                            
                    } catch( SQLException e ) {
                            e.printStackTrace( );
                    }
                    } catch (Exception e) {
                    // TODO: Add catch code
                    e.printStackTrace();
                    try {
                            System.out.println("in finally connection closed");
                                    conn.close( );
                                    conn = null;
                            
                    } catch( SQLException e1 ) {
                            e1.printStackTrace( );
                    }
                    }finally {
                                try {
                                        System.out.println("in finally connection closed");
                                                conn.close( );
                                                conn = null;
                                        
                                } catch( SQLException e ) {
                                        e.printStackTrace( );
                                }
                        }
            }
  
        
    public void setBookMode(String BookMode) {
        this.BookMode = BookMode;
    }

    public String getBookMode() {
        return BookMode;
    }

    public void setBookTypeBinding(RichSelectOneChoice bookTypeBinding) {
        this.bookTypeBinding = bookTypeBinding;
    }

    public RichSelectOneChoice getBookTypeBinding() {
        return bookTypeBinding;
    }

//    public void DateValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
////        DCIteratorBinding poIter = (DCIteratorBinding) getBindings().get("BookPrintingVO1Iterator");
//        if(fromDateBinding.getValue()!=null &&  toDateBinding.getnew=null){
//        oracle.jbo.domain.Date FromDate = (Date) fromDateBinding.getNewValue;
//        oracle.jbo.domain.Date ToDate = (Date) toDateBinding.getValue();
//        System.out.println("fromDate==="+FromDate+"Todate===="+ToDate);
//        if(FromDate.compareTo(ToDate)==1)
//        {
//                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
//                                                                  "From Date must be less than To Date",
//                                                                  null));
//            }
//
//    }
//    }
    public void setFromDateBinding(RichInputDate fromDateBinding) {
        this.fromDateBinding = fromDateBinding;
    }

    public RichInputDate getFromDateBinding() {
        return fromDateBinding;
    }

    public void setToDateBinding(RichInputDate toDateBinding) {
        this.toDateBinding = toDateBinding;
    }

    public RichInputDate getToDateBinding() {
        return toDateBinding;
    }

    public void DateVCE(ValueChangeEvent vce) {
       if(vce!=null)
       {
           vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
               if(vce.getNewValue()!=null &&  toDateBinding.getValue()!=null){
                       oracle.jbo.domain.Date FromDate = (Date) vce.getNewValue();
                       oracle.jbo.domain.Date ToDate = (Date) toDateBinding.getValue();
                       System.out.println("fromDate==="+FromDate+"Todate===="+ToDate);
                       if(FromDate.compareTo(ToDate)==1)
                       {
                               FacesMessage message = new FacesMessage("From Date must be less than To Date.");
                               message.setSeverity(FacesMessage.SEVERITY_ERROR);
                               FacesContext context = FacesContext.getCurrentInstance();
                               context.addMessage(fromDateBinding.getClientId(), message);                                                  
//                                                                             
                           }
               
                   }
           }
    }

    public void ToDateVCE(ValueChangeEvent vce) {
        if(vce!=null)
        {
            vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
                if(vce.getNewValue()!=null &&  fromDateBinding.getValue()!=null)
                {
                        oracle.jbo.domain.Date ToDate = (Date) vce.getNewValue();
                        oracle.jbo.domain.Date FromDate = (Date) fromDateBinding.getValue();
                        System.out.println("fromDate==="+FromDate+"Todate===="+ToDate);
                        if(FromDate.compareTo(ToDate)==1)
                        {
                    FacesMessage message = new FacesMessage("From Date must be less than To Date.");
                    message.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext context = FacesContext.getCurrentInstance();
                    context.addMessage(toDateBinding.getClientId(), message);
                                                                                  
                }
        }
}
    }
    
    public Connection getConnection() throws Exception{
            Context ctx = null;
            Connection conn = null;
            ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/ApplicationDBDS");
            conn = ds.getConnection();
            return conn;
        }
}
