package terms.fin.transaction.ui.bean;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;

import java.sql.Connection;

import java.sql.SQLException;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.faces.context.FacesContext;

import javax.naming.Context;
import javax.naming.InitialContext;

import javax.servlet.http.HttpServletResponse;

import javax.sql.DataSource;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCIteratorBinding;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

public class ChequePrintingBean {
    public ChequePrintingBean() {
    }

    public BindingContainer getBindings()
      {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
      }
    
    public void downloadJrxml(FacesContext facesContext, OutputStream outputStream) {
        Connection conn = null;
        try {

            //*************Find File name***************//
            OperationBinding binding = ADFUtils.findOperation("fileNameForPrint");
            binding.getParamsMap().put("fileId","ERP0000001899");
            binding.execute();
            //*************End Find File name***********//
            System.out.println("Binding Result :"+binding.getResult());
            if(binding.getResult() != null){
                InputStream input = new FileInputStream(binding.getResult().toString());                
                DCIteratorBinding chequePItr = (DCIteratorBinding) getBindings().get("ChequePrintingDualVVO1Iterator");
//                String vouNo = chequePItr.getCurrentRow().getAttribute("VouNo").toString();
//                String unitCode = chequePItr.getCurrentRow().getAttribute("UnitCode").toString();
//                oracle.jbo.domain.Date vouDate =(oracle.jbo.domain.Date)chequePItr.getCurrentRow().getAttribute("VouDate");
   //             
   //             System.out.println("JV parameters :- VOU NO:"+vouNo+" ||UNIT CODE:"+unitCode+"|| VOU DATE:"+vouDate);
                Map n = new HashMap();
//    //                n.put("p_to_dt", vouDate); UnitCode
                System.out.println("chequePItr.getCurrentRow().getAttribute(\"FromChequeNo\")"+chequePItr.getCurrentRow().getAttribute("FromChequeNo"));
                System.out.println("chequePItr.getCurrentRow().getAttribute(\"UnitCode\")"+chequePItr.getCurrentRow().getAttribute("UnitCode"));
                System.out.println("chequePItr.getCurrentRow().getAttribute(\"ControlCode\")"+chequePItr.getCurrentRow().getAttribute("ControlCode"));
               
                n.put("p_ch_no", chequePItr.getCurrentRow().getAttribute("FromChequeNo"));
                n.put("p_unit", chequePItr.getCurrentRow().getAttribute("UnitCode"));
                n.put("p_bank_code", chequePItr.getCurrentRow().getAttribute("ControlCode"));

                conn = getConnection( );
                
                JasperReport design = (JasperReport) JRLoader.loadObject(input);
                System.out.println("Path : " + input + " -------" + design+" param:"+n);
                
                @SuppressWarnings("unchecked")
                net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                byte pdf[] = JasperExportManager.exportReportToPdf(print);
                HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                response.getOutputStream().write(pdf);
                response.getOutputStream().flush();
                response.getOutputStream().close();
                facesContext.responseComplete();
                }else
                System.out.println("File Name/File Path not found.");
                } catch (FileNotFoundException fnfe) {
                // TODO: Add catch code
                fnfe.printStackTrace();
                try {
                    System.out.println("in finally connection closed");
                            conn.close( );
                            conn = null;
                    
                } catch( SQLException e ) {
                    e.printStackTrace( );
                }
                } catch (Exception e) {
                // TODO: Add catch code
                e.printStackTrace();
                try {
                    System.out.println("in finally connection closed");
                            conn.close( );
                            conn = null;
                    
                } catch( SQLException e1 ) {
                    e1.printStackTrace( );
                }
                }finally {
                        try {
                                System.out.println("in finally connection closed");
                                        conn.close( );
                                        conn = null;
                                
                        } catch( SQLException e ) {
                                e.printStackTrace( );
                        }
                }
    }
    
    public Connection getConnection() throws Exception{
            Context ctx = null;
            Connection conn = null;
            ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/ApplicationDBDS");
            conn = ds.getConnection();
            return conn;
        }
}
