package terms.fin.transaction.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.validator.ValidatorException;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.event.ReturnPopupEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;
import oracle.jbo.domain.Date;

import terms.fin.transaction.model.view.CollectionAdviceDetailVORowImpl;
import terms.fin.transaction.model.view.CollectionAdviceHeaderVORowImpl;


public class CollectionAdviceBean {
    private RichPopup confirmationPopUpBind;
    private RichButton headerEditBinding;
    private RichButton dtlCreateBinding;
    private RichButton dtlDeleteBinding;
    private RichOutputText bindingOutputText;
    private RichPanelHeader myPageRootComponent;
    private RichInputText bindAdviceNo;
    private RichInputComboboxListOfValues bindUnitNo;
    private RichInputComboboxListOfValues bindCustomerCode;
    private RichTable detailTableBinding;
    private RichInputDate collectionDateBinding;
    private RichInputText cityCodeBinding;
    private String CheckVal = "";
    private RichInputText customerNameBinding;
    private RichInputText amountDetailBinding;
    private RichInputText deductionDetailBinding;
    private RichInputDate invDateBinding;
    private RichInputDate debitDateBinding;
    private RichInputText chequeRtgsBinding;
    private RichInputDate chDateBinding;
    private RichSelectOneChoice typeBinding;
    private RichInputComboboxListOfValues glCodeBinding;
    private RichInputText discountAmountBinding;
    private RichInputText adjAmountBinding;
    private RichInputComboboxListOfValues discLedgerBinding;
    private RichInputComboboxListOfValues invoiceNoBinding;
    private RichInputComboboxListOfValues drCrNoBinding;
    private RichInputText transAmountBinding;
    private RichInputComboboxListOfValues approvedByBinding;
    private RichInputText docAmountBinding;
    private RichInputText deductionVouNoBinding;
    private RichInputText collectionVouNoBinding;
    private RichInputText interVouNoBinding;
    private RichSelectOneChoice caTypeBinding;
    private RichInputText totalDedAmtBinding;
    private RichOutputText dedAmtBinding;
    private RichPopup detailDetPopupBinding;
    private RichInputText fcAdjAmtBinding;
    private RichInputComboboxListOfValues currencyBinding;
    String val = "N";
    private RichButton detailsBinding;
    private RichInputText currFlucBinding;
    private RichInputText currRateBinding;
    private RichInputText deductionAmtBinding;
    private RichTable detailDetTableBinding;
    private RichInputDate approvalDateBinding;
    private RichInputText detailDeductionBinding;
    private RichInputText interUnitBinding;
    private RichShowDetailItem headerTabBinding;
    private RichShowDetailItem detailTabBinding;

    public void setConfirmationPopUpBind(RichPopup confirmationPopUpBind) {
        this.confirmationPopUpBind = confirmationPopUpBind;
    }

    public RichPopup getConfirmationPopUpBind() {
        return confirmationPopUpBind;
    }

    public CollectionAdviceBean() {
    }

    public void createAl(ActionEvent actionEvent) {
        DCIteratorBinding dci = ADFUtils.findIterator("CollectionAdviceDetailVO1Iterator");
        if (dci != null) {
            Integer seq_no = fetchMaxLineNumber();
            RowSetIterator rsi = dci.getRowSetIterator();
            if (rsi != null) {
                Row last = rsi.last();
                int i = rsi.getRangeIndexOf(last);
                CollectionAdviceDetailVORowImpl newRow = (CollectionAdviceDetailVORowImpl) rsi.createRow();
                newRow.setNewRowState(Row.STATUS_INITIALIZED);
                rsi.insertRowAtRangeIndex(i + 1, newRow);
                rsi.setCurrentRow(newRow);
                newRow.setSNo(seq_no);
                if (seq_no == 1)
                    newRow.setCaType("D");
                else
                    newRow.setCaType("C");
            }
            rsi.closeRowSetIterator();
        }
        OperationBinding op = ADFUtils.findOperation("collectionAdviceNoGeneration");
        Object rst = op.execute();
        System.out.println("Result after collectionAdviceNoGeneration" + rst);
        if (op.getResult() != null && op.getResult().equals("Y")) {
            System.out.println("Inside if for Save");
            val = "Y";
        }
        getBindCustomerCode().setDisabled(true);
    }

    private Integer fetchMaxLineNumber() {
        Integer max = new Integer(0);
        DCIteratorBinding itr = ADFUtils.findIterator("CollectionAdviceDetailVO1Iterator");
        if (itr != null) {
            CollectionAdviceDetailVORowImpl currRow = (CollectionAdviceDetailVORowImpl) itr.getCurrentRow();
            if (currRow != null && currRow.getSNo() != null)
                max = currRow.getSNo();
            RowSetIterator rsi = itr.getRowSetIterator();
            if (rsi != null) {
                Row[] allRowsInRange = rsi.getAllRowsInRange();
                for (Row rw : allRowsInRange) {
                    if (rw != null && rw.getAttribute("SNo") != null &&
                        max.compareTo(Integer.parseInt(rw.getAttribute("SNo").toString())) < 0)
                        System.out.println();
                    max = Integer.parseInt(rw.getAttribute("SNo").toString());
                }
            }
            rsi.closeRowSetIterator();
        }
        max = max + new Integer(1);
        return max;
    }


    public void custCdeVCE(ValueChangeEvent vce) {
        ADFUtils.findOperation("maxInvoiceDate").execute();

    }


    public void allRecordsAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("allDataFetched").execute();
        OperationBinding op = ADFUtils.findOperation("collectionAdviceNoGeneration");
        Object rst = op.execute();
        System.out.println("Result after collectionAdviceNoGeneration" + rst);
        if (op.getResult() != null && op.getResult().equals("Y")) {
            System.out.println("Inside if for Save");
            val = "Y";
        }
    }

    public void caDateValidation(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (object != null) {
            OperationBinding op = ADFUtils.findOperation("checkForInvoiceDate");
            op.getParamsMap().put("ca_dt", (Date) object);
            op.execute();

            if (op.getResult() != null && op.getResult().toString().equalsIgnoreCase("N")) {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                              "Date Can Not Be Greater Than Sysdate", null));

            }
        }
    }

    public void saveAl(ActionEvent actionEvent) {
        DCIteratorBinding hdr = ADFUtils.findIterator("CollectionAdviceHeaderVO1Iterator");
        CollectionAdviceHeaderVORowImpl currRow = (CollectionAdviceHeaderVORowImpl) hdr.getCurrentRow();
        System.out.println("Doc amount: " + currRow.gettotalAdjAmount().setScale(2, BigDecimal.ROUND_HALF_UP) +
                           " currRow.gettotalAdjAmount(): " +
                           currRow.gettotalAdjAmount().setScale(2, BigDecimal.ROUND_HALF_UP));
        if (currRow.getCaAmount() != null && currRow.gettotalAdjAmount() != null) {
            if (currRow.gettotalAdjAmount().compareTo(new BigDecimal(0)) == 1 ||
                currRow.gettotalAdjAmount().compareTo(new BigDecimal(0)) == 0) {
                if (currRow.getCaAmount().setScale(2,
                                                   BigDecimal.ROUND_HALF_UP).compareTo(currRow.gettotalAdjAmount().setScale(2,
                                                                                                                            BigDecimal.ROUND_HALF_UP)) ==
                    0) {
                    OperationBinding op = ADFUtils.findOperation("toGenerateAdviceNo");
                    op.getParamsMap().put("type", val);
                    Object rst = op.execute();
                    System.out.println("Result after Voucher Generation: " + rst);
                    if (op.getResult() != null && !op.getResult().equals("N") && !op.getResult().equals("A")) {
                        ADFUtils.findOperation("Commit").execute();
                        if (op.getResult().equals("Y")) {
                            if (val.equalsIgnoreCase("Y")) {
                                System.out.println("INSIDE Bean5");
                                ADFUtils.showMessage("Advice no is " + bindAdviceNo.getValue(), 2);
                            } else {
                                System.out.println("INSIDE Bean6");
                                ADFUtils.showMessage("Record updated successfully.", 2);

                            }
                        }
                        ADFUtils.findOperation("CreateInsert").execute();
                        AdfFacesContext.getCurrentInstance().addPartialTarget(detailTableBinding);
                    }
                    if (op.getResult() != null && op.getResult().equals("N")) {
                        ADFUtils.showMessage("Please check Doc.Amount and Deduction Amount.", 0);
                    }
                    if (op.getResult() != null && op.getResult().equals("A")) {
                        ADFUtils.showMessage("Please select Adjustment in Type.", 0);
                    }

                } else
                    ADFUtils.showMessage("There Is Miss Match In Document Amount And Total Adjusted Amount(LC).", 0);
            } else
                ADFUtils.showMessage("Total Adjusted Amount(LC) should not be negative.", 0);
        }
    }

    public boolean totalDocCalculation() {
        boolean Status = true;
        DCIteratorBinding hdr = ADFUtils.findIterator("CollectionAdviceHeaderVO1Iterator");
        CollectionAdviceHeaderVORowImpl currRow = (CollectionAdviceHeaderVORowImpl) hdr.getCurrentRow();
        if (currRow.getAuthCode() != null && currRow.getCaAmount() != null && currRow.gettotalAdjAmount() != null) {
            if (currRow.getCaAmount().compareTo(currRow.gettotalAdjAmount()) == 0)
                return Status;
            else
                ADFUtils.showMessage("There Is Miss Match In Document Amount And Total Adjustment Amount.", 0);
            CheckVal = "false";
            return Status == false;
        }

        //        OperationBinding op = ADFUtils.findOperation("toGenerateAdviceNo");
        //        op.getParamsMap().put("type", "N");
        //        op.execute();
        //        if (op.getResult() != null) {
        //            ADFUtils.findOperation("Commit").execute();
        //            ADFUtils.showMessage("Record saved succesfully.", 2);
        //            Status = false;
        //        }


        OperationBinding op = ADFUtils.findOperation("toGenerateAdviceNo");
        op.getParamsMap().put("type", "N");
        op.execute();
        if (op.getResult() != null && op.getResult().equals("Y")) {
            ADFUtils.findOperation("Commit").execute();
            ADFUtils.showMessage("Record saved succesfully.New Advice No. is " + bindAdviceNo.getValue(), 2);
            ADFUtils.findOperation("CreateInsert").execute();
            AdfFacesContext.getCurrentInstance().addPartialTarget(detailTableBinding);
            CheckVal = "true";
            Status = false;
        } else if (op.getResult() != null && op.getResult().equals("N")) {
            ADFUtils.findOperation("Commit").execute();
            ADFUtils.showMessage("Record Updated succesfully.", 2);
            ADFUtils.findOperation("CreateInsert").execute();
            AdfFacesContext.getCurrentInstance().addPartialTarget(detailTableBinding);
            CheckVal = "true";
            Status = false;
        }
        System.out.println("status at the end" + Status);
        return Status;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setBindAdviceNo(RichInputText bindAdviceNo) {
        this.bindAdviceNo = bindAdviceNo;
    }

    public RichInputText getBindAdviceNo() {
        return bindAdviceNo;
    }

    public void setBindUnitNo(RichInputComboboxListOfValues bindUnitNo) {
        this.bindUnitNo = bindUnitNo;
    }

    public RichInputComboboxListOfValues getBindUnitNo() {
        return bindUnitNo;
    }

    public void setBindCustomerCode(RichInputComboboxListOfValues bindCustomerCode) {
        this.bindCustomerCode = bindCustomerCode;
    }

    public RichInputComboboxListOfValues getBindCustomerCode() {
        return bindCustomerCode;
    }

    public void EditButtonAL(ActionEvent actionEvent) {
        if (approvedByBinding.getValue() == null) {
            cevmodecheck();
        } else {
            ADFUtils.showMessage("Record is Approved Can't be modified !", 2);
            ADFUtils.setEL("#{pageFlowScope.mode}", "V");
        }
    }

    public void setDtlCreateBinding(RichButton dtlCreateBinding) {
        this.dtlCreateBinding = dtlCreateBinding;
    }

    public RichButton getDtlCreateBinding() {
        return dtlCreateBinding;
    }

    public void setDtlDeleteBinding(RichButton dtlDeleteBinding) {
        this.dtlDeleteBinding = dtlDeleteBinding;
    }

    public RichButton getDtlDeleteBinding() {
        return dtlDeleteBinding;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setMyPageRootComponent(RichPanelHeader myPageRootComponent) {
        this.myPageRootComponent = myPageRootComponent;
    }

    public RichPanelHeader getMyPageRootComponent() {
        return myPageRootComponent;
    }

    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }

    public void cevModeDisableComponent(String mode) {
        if (mode.equals("E")) {
            getBindAdviceNo().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            getDtlCreateBinding().setDisabled(false);
            getDtlDeleteBinding().setDisabled(false);
            getBindUnitNo().setDisabled(true);
            getBindCustomerCode().setDisabled(true);
            getCollectionDateBinding().setDisabled(true);
            getCityCodeBinding().setDisabled(true);
            //getBindUnitNo().setDisabled(true);
            getCustomerNameBinding().setDisabled(true);
            getAmountDetailBinding().setDisabled(true);
            //getDeductionDetailBinding().setDisabled(true);
            getInvDateBinding().setDisabled(true);
            getDebitDateBinding().setDisabled(true);
            getTransAmountBinding().setDisabled(true);
            getDeductionVouNoBinding().setDisabled(true);
            getCollectionVouNoBinding().setDisabled(true);
            getInterVouNoBinding().setDisabled(true);
            getCurrRateBinding().setDisabled(true);
            getCurrFlucBinding().setDisabled(true);
            getDeductionAmtBinding().setDisabled(true);
            getAdjAmountBinding().setDisabled(true);
            //getApprovalDateBinding().setDisabled(true);
            if (typeBinding.getValue().equals("C")) {
                getChDateBinding().setDisabled(true);
                getChequeRtgsBinding().setDisabled(true);
                getGlCodeBinding().setDisabled(false);
            }
            if (typeBinding.getValue().equals("A")) {
                getChDateBinding().setDisabled(true);
                getChequeRtgsBinding().setDisabled(true);
                getGlCodeBinding().setDisabled(true);
            }
            if (typeBinding.getValue().equals("B")) {
                getChDateBinding().setDisabled(false);
                getChequeRtgsBinding().setDisabled(false);
                getGlCodeBinding().setDisabled(false);
            }
            getInterUnitBinding().setDisabled(true);
            

        }
        if (mode.equals("C")) {
            getBindAdviceNo().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            getCityCodeBinding().setDisabled(true);
            getBindUnitNo().setDisabled(true);
            getCustomerNameBinding().setDisabled(true);
            getAmountDetailBinding().setDisabled(true);
            //getDeductionDetailBinding().setDisabled(true);
            getInvDateBinding().setDisabled(true);
            getDebitDateBinding().setDisabled(true);
            getTransAmountBinding().setDisabled(true);
            getDeductionVouNoBinding().setDisabled(true);
            getCollectionVouNoBinding().setDisabled(true);
            getInterVouNoBinding().setDisabled(true);
            getCurrRateBinding().setDisabled(true);
            getCurrFlucBinding().setDisabled(true);
            getDeductionAmtBinding().setDisabled(true);
            getAdjAmountBinding().setDisabled(true);
            //getApprovalDateBinding().setDisabled(true);
            getInterUnitBinding().setDisabled(true);
        }
        if (mode.equals("V")) {
            getDtlCreateBinding().setDisabled(true);
            getDtlDeleteBinding().setDisabled(true);
            getHeaderTabBinding().setDisabled(false);
            getDetailTabBinding().setDisabled(false);

        }

    }


    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete1").execute();
            System.out.println("Record Delete Successfully");
            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
        }

        AdfFacesContext.getCurrentInstance().addPartialTarget(detailTableBinding);
    }


    public void setDetailTableBinding(RichTable detailTableBinding) {
        this.detailTableBinding = detailTableBinding;
    }

    public RichTable getDetailTableBinding() {
        return detailTableBinding;
    }

    public void setCollectionDateBinding(RichInputDate collectionDateBinding) {
        this.collectionDateBinding = collectionDateBinding;
    }

    public RichInputDate getCollectionDateBinding() {
        return collectionDateBinding;
    }

    public void setCityCodeBinding(RichInputText cityCodeBinding) {
        this.cityCodeBinding = cityCodeBinding;
    }

    public RichInputText getCityCodeBinding() {
        return cityCodeBinding;
    }


    public String saveAndClose() {
        DCIteratorBinding hdr = ADFUtils.findIterator("CollectionAdviceHeaderVO1Iterator");
        CollectionAdviceHeaderVORowImpl currRow = (CollectionAdviceHeaderVORowImpl) hdr.getCurrentRow();
        if (currRow.getCaAmount() != null && currRow.gettotalAdjAmount() != null) {
            if (currRow.gettotalAdjAmount().compareTo(new BigDecimal(0)) == 1 ||
                currRow.gettotalAdjAmount().compareTo(new BigDecimal(0)) == 0) {
                if (currRow.getCaAmount().compareTo(currRow.gettotalAdjAmount()) == 0) {
                    OperationBinding op = ADFUtils.findOperation("toGenerateAdviceNo");
                    op.getParamsMap().put("type", val);
                    Object rst = op.execute();
                    System.out.println("Result after Voucher Generation: " + rst);
                    if (op.getResult() != null && !op.getResult().equals("N") && !op.getResult().equals("A")) {
                        ADFUtils.findOperation("Commit").execute();
                        if (op.getResult().equals("Y")) {
                            if (val.equalsIgnoreCase("Y")) {
                                System.out.println("INSIDE Bean5");
                                ADFUtils.showMessage("Advice no is " + bindAdviceNo.getValue(), 2);
                            } else {
                                System.out.println("INSIDE Bean6");
                                ADFUtils.showMessage("Record updated successfully.", 2);

                            }
                        }
                        return "SaveAndClose";
                    }
                    if (op.getResult() != null && op.getResult().equals("N")) {
                        ADFUtils.showMessage("Please check Doc.Amount and Deduction Amount.", 0);
                    }
                    if (op.getResult() != null && op.getResult().equals("A")) {
                        ADFUtils.showMessage("Please select Adjustment in Type.", 0);
                    }

                } else
                    ADFUtils.showMessage("There Is Miss Match In Document Amount And Total Adjusted Amount(LC).", 0);
            } else
                ADFUtils.showMessage("Total Adjusted Amount(LC) should not be negative.", 0);
        }
        return null;
    }


    public void setCustomerNameBinding(RichInputText customerNameBinding) {
        this.customerNameBinding = customerNameBinding;
    }

    public RichInputText getCustomerNameBinding() {
        return customerNameBinding;
    }

    public void setAmountDetailBinding(RichInputText amountDetailBinding) {
        this.amountDetailBinding = amountDetailBinding;
    }

    public RichInputText getAmountDetailBinding() {
        return amountDetailBinding;
    }

    public void setDeductionDetailBinding(RichInputText deductionDetailBinding) {
        this.deductionDetailBinding = deductionDetailBinding;
    }

    public RichInputText getDeductionDetailBinding() {
        return deductionDetailBinding;
    }

    public void clearDetailRowVCL(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        ADFUtils.findOperation("clearDetailRowCollectionAdvice").execute();
        AdfFacesContext.getCurrentInstance().addPartialTarget(detailTableBinding);
    }

    public void setInvDateBinding(RichInputDate invDateBinding) {
        this.invDateBinding = invDateBinding;
    }

    public RichInputDate getInvDateBinding() {
        return invDateBinding;
    }

    public void setDebitDateBinding(RichInputDate debitDateBinding) {
        this.debitDateBinding = debitDateBinding;
    }

    public RichInputDate getDebitDateBinding() {
        return debitDateBinding;
    }

    public void TypeVCL(ValueChangeEvent valueChangeEvent) {
        System.out.println("Value of vce: " + typeBinding.getValue());

        if (typeBinding.getValue().equals("C")) {
            ADFUtils.findOperation("clearHeaderCollectionAdvice").execute();
            getChDateBinding().setDisabled(true);
            getChequeRtgsBinding().setDisabled(true);
            getGlCodeBinding().setDisabled(false);
        }
        if (typeBinding.getValue().equals("A")) {
            ADFUtils.findOperation("clearHeaderCollectionAdvice").execute();
            getChDateBinding().setDisabled(true);
            getChequeRtgsBinding().setDisabled(true);
            getGlCodeBinding().setDisabled(true);
        }
        if (typeBinding.getValue().equals("B")) {
            ADFUtils.findOperation("clearHeaderCollectionAdvice").execute();
            getChDateBinding().setDisabled(false);
            getChequeRtgsBinding().setDisabled(false);
            getGlCodeBinding().setDisabled(false);
        }

    }

    public void setChequeRtgsBinding(RichInputText chequeRtgsBinding) {
        this.chequeRtgsBinding = chequeRtgsBinding;
    }

    public RichInputText getChequeRtgsBinding() {
        return chequeRtgsBinding;
    }

    public void setChDateBinding(RichInputDate chDateBinding) {
        this.chDateBinding = chDateBinding;
    }

    public RichInputDate getChDateBinding() {
        return chDateBinding;
    }

    public void setTypeBinding(RichSelectOneChoice typeBinding) {
        this.typeBinding = typeBinding;
    }

    public RichSelectOneChoice getTypeBinding() {
        return typeBinding;
    }

    public void setGlCodeBinding(RichInputComboboxListOfValues glCodeBinding) {
        this.glCodeBinding = glCodeBinding;
    }

    public RichInputComboboxListOfValues getGlCodeBinding() {
        return glCodeBinding;
    }

    public void DiscLedgerValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        //        System.out.println("Discount Amount: "+discountAmountBinding.getValue()+" "+object);
        //        if (object.toString().isEmpty() && discountAmountBinding.getValue() != null) {
        //            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
        //                                                          "Disc Ledger is required.", null));

        //        }











    }

    public void setDiscountAmountBinding(RichInputText discountAmountBinding) {
        this.discountAmountBinding = discountAmountBinding;
    }

    public RichInputText getDiscountAmountBinding() {
        return discountAmountBinding;
    }

    public void discountValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (object != null) {
            discLedgerBinding.resetValue();
            System.out.println("Amount: " + amountDetailBinding.getValue() + " Adjusted: " +
                               adjAmountBinding.getValue() + " Discount: " + object);
            BigDecimal invAmount = (BigDecimal) amountDetailBinding.getValue();
            BigDecimal adjAmt = (BigDecimal) adjAmountBinding.getValue();
            BigDecimal deducted = invAmount.subtract(adjAmt);
            BigDecimal discount = (BigDecimal) object;

            System.out.println("Before if :" + deducted.compareTo(discount));
            if (deducted.compareTo(discount) == -1) {
                System.out.println("Inside if :" + deducted.compareTo(discount));
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                              "Discount Amount must be less than or equal to Deducted.",
                                                              null));
            }
        }
    }

    public void setAdjAmountBinding(RichInputText adjAmountBinding) {
        this.adjAmountBinding = adjAmountBinding;
    }

    public RichInputText getAdjAmountBinding() {
        return adjAmountBinding;
    }

    public void setDiscLedgerBinding(RichInputComboboxListOfValues discLedgerBinding) {
        this.discLedgerBinding = discLedgerBinding;
    }

    public RichInputComboboxListOfValues getDiscLedgerBinding() {
        return discLedgerBinding;
    }

    public void adjustedValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (object != null) {
            if (transAmountBinding.getValue() != null && caTypeBinding.getValue() != null) {
                System.out.println("Amount value: " + transAmountBinding.getValue() + "Adjusted value: " + object);
                BigDecimal amount = (BigDecimal) transAmountBinding.getValue();
                BigDecimal adjusted = (BigDecimal) object;
                System.out.println("Compare to value: " + amount.compareTo(adjusted));
                if (caTypeBinding.getValue() != null && caTypeBinding.getValue().equals("C")) {
                    System.out.println("Inside if:::::::");
                    amount = amount.multiply(new BigDecimal(-1));
                    if (adjusted.compareTo(new BigDecimal(0)) == 1) {
                        throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                      "Adjusted must be less than zero.", null));
                    }
                    adjusted = adjusted.multiply(new BigDecimal(-1));
                    System.out.println("Amount in case credit:" + amount + "Adjusted in case credit:" + adjusted);
                }
                System.out.println("Compare to value After: " + amount.compareTo(adjusted));
                if (amount.compareTo(adjusted) == -1) {
                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                  "Adjusted must be less than or equal to Amount.",
                                                                  null));
                }

            }
            AdfFacesContext.getCurrentInstance().addPartialTarget(detailTableBinding);
        }

    }

    public void invoiceNoValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        //        String str = (String) object;
        //        if (!str.isEmpty()) {
        //            System.out.println("Invoice No: " + str);
        //            OperationBinding op = ADFUtils.findOperation("duplicateCheckCollectionAdvice");
        //            op.getParamsMap().put("val", str);
        //            Object rst = op.execute();
        //            if (rst != null && rst.toString().equals("Y")) {
        //                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Duplicate record found.",
        //                                                              null));
        //
        //            }
        //        }




    }

    public void InvoiceNoVCL(ValueChangeEvent valueChangeEvent) {

        //       valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (valueChangeEvent != null) {
            System.out.println("Invoice No: " + valueChangeEvent.getNewValue());
            OperationBinding op = ADFUtils.findOperation("duplicateCheckCollectionAdvice");
            op.getParamsMap().put("val", valueChangeEvent.getNewValue());
            Object rst = op.execute();
            System.out.println("result : " + rst.toString());
            if (rst != null && rst.equals("Y")) {
                FacesMessage message = new FacesMessage("Duplicate record found.");
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(invoiceNoBinding.getClientId(), message);
            }
            if (rst != null && rst.equals("YA")) {
                FacesMessage message = new FacesMessage("Duplicate record found.");
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(drCrNoBinding.getClientId(), message);
            }
            AdfFacesContext.getCurrentInstance().addPartialTarget(detailTableBinding);
        }
    }

    public void setInvoiceNoBinding(RichInputComboboxListOfValues invoiceNoBinding) {
        this.invoiceNoBinding = invoiceNoBinding;
    }

    public RichInputComboboxListOfValues getInvoiceNoBinding() {
        return invoiceNoBinding;
    }

    public void setDrCrNoBinding(RichInputComboboxListOfValues drCrNoBinding) {
        this.drCrNoBinding = drCrNoBinding;
    }

    public RichInputComboboxListOfValues getDrCrNoBinding() {
        return drCrNoBinding;
    }

    public void setTransAmountBinding(RichInputText transAmountBinding) {
        this.transAmountBinding = transAmountBinding;
    }

    public RichInputText getTransAmountBinding() {
        return transAmountBinding;
    }

    public void approvedByReturnListner(ReturnPopupEvent returnPopupEvent) {
        System.out.println("In Return Listner!!! Unit Code :" + bindUnitNo.getValue() + "Auth Code: " +
                           approvedByBinding.getValue() + " Amount: " + docAmountBinding.getValue());
        if (approvedByBinding.getValue() != null) {
            OperationBinding op = ADFUtils.findOperation("checkApprovalStatusCollectionAdvice");
            op.getParamsMap().put("unitCd", bindUnitNo.getValue());
            op.getParamsMap().put("empCode", approvedByBinding.getValue());
            op.getParamsMap().put("amount", docAmountBinding.getValue());
            op.execute();
        }
    }

    public void setApprovedByBinding(RichInputComboboxListOfValues approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputComboboxListOfValues getApprovedByBinding() {
        return approvedByBinding;
    }

    public void setDocAmountBinding(RichInputText docAmountBinding) {
        this.docAmountBinding = docAmountBinding;
    }

    public RichInputText getDocAmountBinding() {
        return docAmountBinding;
    }

    public void setDeductionVouNoBinding(RichInputText deductionVouNoBinding) {
        this.deductionVouNoBinding = deductionVouNoBinding;
    }

    public RichInputText getDeductionVouNoBinding() {
        return deductionVouNoBinding;
    }

    public void setCollectionVouNoBinding(RichInputText collectionVouNoBinding) {
        this.collectionVouNoBinding = collectionVouNoBinding;
    }

    public RichInputText getCollectionVouNoBinding() {
        return collectionVouNoBinding;
    }

    public void setInterVouNoBinding(RichInputText interVouNoBinding) {
        this.interVouNoBinding = interVouNoBinding;
    }

    public RichInputText getInterVouNoBinding() {
        return interVouNoBinding;
    }

    public void deductionAmtValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (object != null) {
            System.out.println("Amount value: " + transAmountBinding.getValue() + "Adjusted value: " + object);
            BigDecimal amount = (BigDecimal) transAmountBinding.getValue();
            BigDecimal adjusted = (BigDecimal) adjAmountBinding.getValue();
            BigDecimal dedAmt = amount.subtract(adjusted);
            System.out.println("Inside if :" + dedAmt.compareTo(adjusted));
            //            if(typeBinding.getValue()!=null && typeBinding.getValue().equals("C")){
            //            amount=amount.multiply(new BigDecimal(-1));
            //            adjusted=adjusted.multiply(new BigDecimal(-1));
            //            }
            if (dedAmt.compareTo((BigDecimal) object) == -1) {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                              "Deduction must be less than or equal to " + dedAmt,
                                                              null));
            }

            else {
                ADFUtils.showPopup(detailDetPopupBinding);
            }
        }


    }

    public void setCaTypeBinding(RichSelectOneChoice caTypeBinding) {
        this.caTypeBinding = caTypeBinding;
    }

    public RichSelectOneChoice getCaTypeBinding() {
        return caTypeBinding;
    }

    public void setTotalDedAmtBinding(RichInputText totalDedAmtBinding) {
        this.totalDedAmtBinding = totalDedAmtBinding;
    }

    public RichInputText getTotalDedAmtBinding() {
        return totalDedAmtBinding;
    }

    public void detailDetDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            if (totalDedAmtBinding.getValue() != null) {
                BigDecimal t1 =
                    dedAmtBinding.getValue() != null ? (BigDecimal) dedAmtBinding.getValue() : new BigDecimal(0);
                BigDecimal t2 = (BigDecimal) totalDedAmtBinding.getValue();
                if (!(t1.setScale(2, BigDecimal.ROUND_HALF_UP).compareTo(t2.setScale(2, BigDecimal.ROUND_HALF_UP)) ==
                      0)) {
                    ADFUtils.showMessage("Total Deduction Amount must be equal to Deduction Amt.", 0);
                }
            }
        }
    }

    public void setDedAmtBinding(RichOutputText dedAmtBinding) {
        this.dedAmtBinding = dedAmtBinding;
    }

    public RichOutputText getDedAmtBinding() {
        return dedAmtBinding;
    }

    public void setDetailDetPopupBinding(RichPopup detailDetPopupBinding) {
        this.detailDetPopupBinding = detailDetPopupBinding;
    }

    public RichPopup getDetailDetPopupBinding() {
        return detailDetPopupBinding;
    }

    public void DeductedVCL(ValueChangeEvent valueChangeEvent) {
        if (valueChangeEvent != null && currencyBinding.getValue() != null &&
            currencyBinding.getValue().equals("INR")) {
            if (transAmountBinding.getValue() != null && fcAdjAmtBinding.getValue() != null &&
                deductionDetailBinding.getValue() != null) {
                System.out.println("Amount value: " + transAmountBinding.getValue() + "Adjusted value: " +
                                   valueChangeEvent);
                BigDecimal amount = (BigDecimal) transAmountBinding.getValue();
                BigDecimal adjusted = (BigDecimal) fcAdjAmtBinding.getValue();
                BigDecimal dedAmt = amount.subtract(adjusted);
                System.out.println("Inside if :" + dedAmt.compareTo(adjusted));
                //            if(typeBinding.getValue()!=null && typeBinding.getValue().equals("C")){
                //            amount=amount.multiply(new BigDecimal(-1));
                //            adjusted=adjusted.multiply(new BigDecimal(-1));
                //            }
                if (dedAmt.compareTo((BigDecimal) deductionDetailBinding.getValue()) == -1) {
                    //                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    //                                                              "Deduction must be less than or equal to "+dedAmt, null));
                    FacesMessage message = new FacesMessage("Deduction must be less than or equal to " + dedAmt);
                    message.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext context = FacesContext.getCurrentInstance();
                    context.addMessage(deductionDetailBinding.getClientId(), message);
                }
            }

        }
    }

    public void setFcAdjAmtBinding(RichInputText fcAdjAmtBinding) {
        this.fcAdjAmtBinding = fcAdjAmtBinding;
    }

    public RichInputText getFcAdjAmtBinding() {
        return fcAdjAmtBinding;
    }

    public void CurrencyVCL(ValueChangeEvent valueChangeEvent) {
        if (valueChangeEvent != null) {
            OperationBinding op = ADFUtils.findOperation("currencyRateCollectionAdvice");
            op.getParamsMap().put("currencyCode", currencyBinding.getValue());
            Object rst = op.execute();
            if (rst != null && rst.toString().equals("F")) {
                FacesMessage message = new FacesMessage("Please validate currency in daily currency table.");
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(currencyBinding.getClientId(), message);
            }
        }
    }

    public void setCurrencyBinding(RichInputComboboxListOfValues currencyBinding) {
        this.currencyBinding = currencyBinding;
    }

    public RichInputComboboxListOfValues getCurrencyBinding() {
        return currencyBinding;
    }

    public void setDetailsBinding(RichButton detailsBinding) {
        this.detailsBinding = detailsBinding;
    }

    public RichButton getDetailsBinding() {
        return detailsBinding;
    }

    public void setCurrFlucBinding(RichInputText currFlucBinding) {
        this.currFlucBinding = currFlucBinding;
    }

    public RichInputText getCurrFlucBinding() {
        return currFlucBinding;
    }

    public void setCurrRateBinding(RichInputText currRateBinding) {
        this.currRateBinding = currRateBinding;
    }

    public RichInputText getCurrRateBinding() {
        return currRateBinding;
    }

    public void setDeductionAmtBinding(RichInputText deductionAmtBinding) {
        this.deductionAmtBinding = deductionAmtBinding;
    }

    public RichInputText getDeductionAmtBinding() {
        return deductionAmtBinding;
    }

    public void DetailDetDeleteDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete2").execute();
            FacesMessage Message = new FacesMessage("Record Deleted Successfully.");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
        }

        AdfFacesContext.getCurrentInstance().addPartialTarget(detailDetTableBinding);
    }

    public void setDetailDetTableBinding(RichTable detailDetTableBinding) {
        this.detailDetTableBinding = detailDetTableBinding;
    }

    public RichTable getDetailDetTableBinding() {
        return detailDetTableBinding;
    }

    public void setApprovalDateBinding(RichInputDate approvalDateBinding) {
        this.approvalDateBinding = approvalDateBinding;
    }

    public RichInputDate getApprovalDateBinding() {
        return approvalDateBinding;
    }

    public void detailDetAL(ActionEvent actionEvent) {
        BigDecimal dedAmt = (BigDecimal) deductionDetailBinding.getValue();
        if (dedAmt != null) {
            if (dedAmt.compareTo(new BigDecimal(0)) == 1 || dedAmt.compareTo(new BigDecimal(0)) == -1) {
                ADFUtils.showPopup(detailDetPopupBinding);
            } else {
                ADFUtils.showMessage("Deducted Amt should not be 0 for Deduction Detail. ", 0);
            }
        } else {
            ADFUtils.showMessage("Deducted Amt is mandatory for Deduction Detail. ", 0);
        }

    }

    public void setDetailDeductionBinding(RichInputText detailDeductionBinding) {
        this.detailDeductionBinding = detailDeductionBinding;
    }

    public RichInputText getDetailDeductionBinding() {
        return detailDeductionBinding;
    }

    public void setInterUnitBinding(RichInputText interUnitBinding) {
        this.interUnitBinding = interUnitBinding;
    }

    public RichInputText getInterUnitBinding() {
        return interUnitBinding;
    }

    public void createButtonAL(ActionEvent actionEvent) {

        try {
            ADFUtils.findOperation("CreateInsert2").execute();
            detailDeductionBinding.setValue(dedAmtBinding.getValue());
        } catch (Exception e) {
            // TODO: Add catch code
            e.printStackTrace();
        }
        
    }

    public void setHeaderTabBinding(RichShowDetailItem headerTabBinding) {
        this.headerTabBinding = headerTabBinding;
    }

    public RichShowDetailItem getHeaderTabBinding() {
        return headerTabBinding;
    }

    public void setDetailTabBinding(RichShowDetailItem detailTabBinding) {
        this.detailTabBinding = detailTabBinding;
    }

    public RichShowDetailItem getDetailTabBinding() {
        return detailTabBinding;
    }
}

