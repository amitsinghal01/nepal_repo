package terms.fin.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichButton;

import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class CommercialInvoiceBean {
    private RichButton headerEditBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichTable detailTableBinding;
    private RichOutputText outputTextBinding;
    private RichButton detaildeleteBinding;
    private RichButton detailcreateBinding;
    private RichInputText invoiceNoBinding;
    private RichShowDetailItem headerTabBinding;
    private RichShowDetailItem detailTabBinding;
    private RichShowDetailItem otherTabBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputComboboxListOfValues invoiceTypeBinding;
    private RichInputText plantCodeBinding;
    private RichInputText cityCodeBinding;
    private RichInputComboboxListOfValues ackNoBinding;
    private RichInputText ackAmendNoBinding;
    private RichInputComboboxListOfValues customerCodeBinding;
    private RichInputDate dateBinding;
    private RichInputText stockTypeBinding;
    private RichInputText finYearBinding;
    private RichSelectOneChoice cancelBinding;
    private RichSelectOneChoice docTypeBinding;
    private RichInputComboboxListOfValues dispatchBinding;
    private RichInputComboboxListOfValues approvedByBinding;
    private RichInputText netAmountBinding;
    private RichInputText grandTotalBinding;
    private RichInputText quantityBinding;
    private RichInputText priceBinding;
    private RichInputText grossAmountBinding;
    private RichInputText grossAmtBinding;
    private RichInputText headIgstAmountBinding;
    private RichInputText discountAmountBinding;
    private RichInputText igstPersBinding;
    private RichInputText detailIgstAmountBinding;
    private RichInputText dtlGrossAmountBinding;
    private RichInputComboboxListOfValues prodCodeBinding;

    public CommercialInvoiceBean() {
    }

    public void saveAL(ActionEvent actionEvent) 
    {
            OperationBinding op = ADFUtils.findOperation("generateCommercialInvNo");
            op.execute();
            System.out.println("Result is===> "+op.getResult());
            if(op.getResult() != null && op.getResult().toString().equalsIgnoreCase("E"))
            {
                ADFUtils.findOperation("Commit").execute();
                ADFUtils.showMessage(" Record Updated Successfully", 2);
                
                if(approvedByBinding.getValue()!=null || approvedByBinding.getValue() == null)
                {
                      ADFUtils.setEL("#{pageFlowScope.mode}","V");
                      cevmodecheck();
                      AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
                }
            }
            else if (op.getResult() != null && op.getResult().toString() != ""  && op.getResult().equals("S"))
            {
                System.out.println("+++++ in the save mode+++++++++++");
                ADFUtils.findOperation("Commit").execute();
                ADFUtils.showMessage("Record Saved Successfully.Commercial Invoice No.is "+ADFUtils.evaluateEL("#{bindings.Identifier.inputValue}"), 2);
                
                if(approvedByBinding.getValue()!=null || approvedByBinding.getValue() == null)
                {
                      ADFUtils.setEL("#{pageFlowScope.mode}","V");
                      cevmodecheck();
                      AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
                }
            }
            else if(op.getResult() != null && op.getResult().toString().equalsIgnoreCase("N"))
            {
                ADFUtils.showMessage("Invoice No. could not be generated. Try Again !!", 0);
            }
    }

    public String saveAndCloseAL(){
        // Add event code here...
        return null;
    }

    public void createDetailAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert").execute();
        ADFUtils.findOperation("setCommInvoiceNo").execute();
        
        if(!ADFUtils.evaluateEL("#{pageFlowScope.mode}").equals("V")){
            customerCodeBinding.setDisabled(true);
            docTypeBinding.setDisabled(true);
            ackNoBinding.setDisabled(true);
            invoiceTypeBinding.setDisabled(true);
            dateBinding.setDisabled(true);
            cancelBinding.setDisabled(true);
        }
    }
    public void editButtonAL(ActionEvent actionEvent) {
        if(approvedByBinding.getValue()!=null){
            ADFUtils.showMessage("Approved Invoice can not be modified.", 0);
            ADFUtils.setEL("#{pageFlowScope.mode}","V");
        }else{
            cevmodecheck();
        }
       
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }
    
    public void setOutputTextBinding(RichOutputText outputTextBinding) {
        this.outputTextBinding = outputTextBinding;
    }

    public RichOutputText getOutputTextBinding() {
        cevmodecheck();
        return outputTextBinding;
    }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }

    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }

    public void cevModeDisableComponent(String mode) {
        if (mode.equals("E")) 
        {
            
            getCancelBinding().setDisabled(true);
            getDateBinding().setDisabled(true);
            getDtlGrossAmountBinding().setDisabled(true);
            getDocTypeBinding().setDisabled(true);
            getAckNoBinding().setDisabled(true);
            getDetailIgstAmountBinding().setDisabled(true);
            getIgstPersBinding().setDisabled(true);
            getDiscountAmountBinding().setDisabled(true);
            getGrossAmtBinding().setDisabled(true);
            getHeadIgstAmountBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            getFinYearBinding().setDisabled(true);
            getStockTypeBinding().setDisabled(true);
            getAckAmendNoBinding().setDisabled(true);
            getCityCodeBinding().setDisabled(true);
            getPlantCodeBinding().setDisabled(true);
            getNetAmountBinding().setDisabled(true);
            getGrandTotalBinding().setDisabled(true);
            getCustomerCodeBinding().setDisabled(true);
            getInvoiceNoBinding().setDisabled(true);
            getInvoiceTypeBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
            getDetailcreateBinding().setDisabled(false);
        } else if(mode.equals("C")) {
            
            getDtlGrossAmountBinding().setDisabled(true);
            getDetailIgstAmountBinding().setDisabled(true);
            getIgstPersBinding().setDisabled(true);
            getDiscountAmountBinding().setDisabled(true);
            getGrossAmtBinding().setDisabled(true);
            getHeadIgstAmountBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            getFinYearBinding().setDisabled(true);
            getStockTypeBinding().setDisabled(true);
            getNetAmountBinding().setDisabled(true);
            getGrandTotalBinding().setDisabled(true);
            getAckAmendNoBinding().setDisabled(true);
            getCityCodeBinding().setDisabled(true);
            getPlantCodeBinding().setDisabled(true);
            getInvoiceNoBinding().setDisabled(true);
            getDetailcreateBinding().setDisabled(false);
            getUnitCodeBinding().setDisabled(true);

        } else if (mode.equals("V")) {

            getHeaderTabBinding().setDisabled(false);
            getDetailTabBinding().setDisabled(false);
            getOtherTabBinding().setDisabled(false);
            getDetailcreateBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(false);
        }
    }

    public void deleteDialogListenerDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete").execute();
            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(detailTableBinding);
    }

    public void setDetailTableBinding(RichTable detailTableBinding) {
        this.detailTableBinding = detailTableBinding;
    }

    public RichTable getDetailTableBinding() {
        return detailTableBinding;
    }

    public void setDetaildeleteBinding(RichButton detaildeleteBinding) {
        this.detaildeleteBinding = detaildeleteBinding;
    }

    public RichButton getDetaildeleteBinding() {
        return detaildeleteBinding;
    }

    public void setDetailcreateBinding(RichButton detailcreateBinding) {
        this.detailcreateBinding = detailcreateBinding;
    }

    public RichButton getDetailcreateBinding() {
        return detailcreateBinding;
    }

    public void setInvoiceNoBinding(RichInputText invoiceNoBinding) {
        this.invoiceNoBinding = invoiceNoBinding;
    }

    public RichInputText getInvoiceNoBinding() {
        return invoiceNoBinding;
    }

    public void customerCodeVCL(ValueChangeEvent vce) 
    {
     //   vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
//        OperationBinding OppFin=ADFUtils.findOperation("finYearCommercialInvoice");
//        Object ObjFin=OppFin.execute();
        if(vce.getNewValue()!=vce.getOldValue())
        {
               OperationBinding op = ADFUtils.findOperation("setNullInHeaderCommercialInvoice");
               op.execute();
        }
    }

    public void setHeaderTabBinding(RichShowDetailItem headerTabBinding) {
        this.headerTabBinding = headerTabBinding;
    }

    public RichShowDetailItem getHeaderTabBinding() {
        return headerTabBinding;
    }

    public void setDetailTabBinding(RichShowDetailItem detailTabBinding) {
        this.detailTabBinding = detailTabBinding;
    }

    public RichShowDetailItem getDetailTabBinding() {
        return detailTabBinding;
    }

    public void setOtherTabBinding(RichShowDetailItem otherTabBinding) {
        this.otherTabBinding = otherTabBinding;
    }

    public RichShowDetailItem getOtherTabBinding() {
        return otherTabBinding;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setInvoiceTypeBinding(RichInputComboboxListOfValues invoiceTypeBinding) {
        this.invoiceTypeBinding = invoiceTypeBinding;
    }

    public RichInputComboboxListOfValues getInvoiceTypeBinding() {
        return invoiceTypeBinding;
    }

    public void setPlantCodeBinding(RichInputText plantCodeBinding) {
        this.plantCodeBinding = plantCodeBinding;
    }

    public RichInputText getPlantCodeBinding() {
        return plantCodeBinding;
    }

    public void setCityCodeBinding(RichInputText cityCodeBinding) {
        this.cityCodeBinding = cityCodeBinding;
    }

    public RichInputText getCityCodeBinding() {
        return cityCodeBinding;
    }

    public void setAckNoBinding(RichInputComboboxListOfValues ackNoBinding) {
        this.ackNoBinding = ackNoBinding;
    }

    public RichInputComboboxListOfValues getAckNoBinding() {
        return ackNoBinding;
    }

    public void setAckAmendNoBinding(RichInputText ackAmendNoBinding) {
        this.ackAmendNoBinding = ackAmendNoBinding;
    }

    public RichInputText getAckAmendNoBinding() {
        return ackAmendNoBinding;
    }

    public void setCustomerCodeBinding(RichInputComboboxListOfValues customerCodeBinding) {
        this.customerCodeBinding = customerCodeBinding;
    }

    public RichInputComboboxListOfValues getCustomerCodeBinding() {
        return customerCodeBinding;
    }

    public void setDateBinding(RichInputDate dateBinding) {
        this.dateBinding = dateBinding;
    }

    public RichInputDate getDateBinding() {
        return dateBinding;
    }

    public void setStockTypeBinding(RichInputText stockTypeBinding) {
        this.stockTypeBinding = stockTypeBinding;
    }

    public RichInputText getStockTypeBinding() {
        return stockTypeBinding;
    }

    public void setFinYearBinding(RichInputText finYearBinding) {
        this.finYearBinding = finYearBinding;
    }

    public RichInputText getFinYearBinding() {
        return finYearBinding;
    }

    public void invoiceTypeVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        OperationBinding OppFin=ADFUtils.findOperation("finYearCommercialInvoice");
        Object ObjFin=OppFin.execute();
    }

    public void setCancelBinding(RichSelectOneChoice cancelBinding) {
        this.cancelBinding = cancelBinding;
    }

    public RichSelectOneChoice getCancelBinding() {
        return cancelBinding;
    }

    public void setDocTypeBinding(RichSelectOneChoice docTypeBinding) {
        this.docTypeBinding = docTypeBinding;
    }

    public RichSelectOneChoice getDocTypeBinding() {
        return docTypeBinding;
    }

    public void setDispatchBinding(RichInputComboboxListOfValues dispatchBinding) {
        this.dispatchBinding = dispatchBinding;
    }

    public RichInputComboboxListOfValues getDispatchBinding() {
        return dispatchBinding;
    }

    public void setApprovedByBinding(RichInputComboboxListOfValues approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputComboboxListOfValues getApprovedByBinding() {
        return approvedByBinding;
    }

    public void setNetAmountBinding(RichInputText netAmountBinding) {
        this.netAmountBinding = netAmountBinding;
    }

    public RichInputText getNetAmountBinding() {
        return netAmountBinding;
    }

    public void setGrandTotalBinding(RichInputText grandTotalBinding) {
        this.grandTotalBinding = grandTotalBinding;
    }

    public RichInputText getGrandTotalBinding() {
        return grandTotalBinding;
    }

    public void quantityVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce != null)
        {
            if(vce.getNewValue()!=null)
            {
               ADFUtils.findOperation("getProductInvoiceCalculation").execute();   
                OperationBinding op = ADFUtils.findOperation("getProductCodeDetailCI");
                op.getParamsMap().put("product",prodCodeBinding.getValue());
                op.execute(); 
                ADFUtils.findOperation("commercialInvoiceGstCalculation").execute(); 
            }
        }
    }

    public void setQuantityBinding(RichInputText quantityBinding) {
        this.quantityBinding = quantityBinding;
    }

    public RichInputText getQuantityBinding() {
        return quantityBinding;
    }

    public void priceVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce != null)
        {
            ADFUtils.findOperation("getProductInvoiceCalculation").execute();
//            OperationBinding op = ADFUtils.findOperation("getProductCodeDetailCI");
//            op.getParamsMap().put("product",productCodeBinding.getValue());
//            op.execute(); 
            ADFUtils.findOperation("commercialInvoiceGstCalculation").execute(); 
        }
    }

    public void setPriceBinding(RichInputText priceBinding) {
        this.priceBinding = priceBinding;
    }

    public RichInputText getPriceBinding() {
        return priceBinding;
    }

    public void setGrossAmountBinding(RichInputText grossAmountBinding) {
        this.grossAmountBinding = grossAmountBinding;
    }

    public RichInputText getGrossAmountBinding() {
        return grossAmountBinding;
    }

    public void setGrossAmtBinding(RichInputText grossAmtBinding) {
        this.grossAmtBinding = grossAmtBinding;
    }

    public RichInputText getGrossAmtBinding() {
        return grossAmtBinding;
    }

    public void productCodeVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce != null)
        {
            ADFUtils.findOperation("getProductInvoiceCalculation").execute();
            OperationBinding op = ADFUtils.findOperation("getProductCodeDetailCI");
            op.getParamsMap().put("product",prodCodeBinding.getValue());
            op.execute(); 
            ADFUtils.findOperation("commercialInvoiceGstCalculation").execute(); 
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(priceBinding);
        AdfFacesContext.getCurrentInstance().addPartialTarget(quantityBinding);
    }

    public void gstCodeVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce != null)
        {
            ADFUtils.findOperation("getProductInvoiceCalculation").execute();
//            OperationBinding op = ADFUtils.findOperation("getProductCodeDetailCI");
//            op.getParamsMap().put("product",productCodeBinding.getValue());
//            op.execute(); 
            ADFUtils.findOperation("commercialInvoiceGstCalculation").execute(); 
        }
    }

    public void setHeadIgstAmountBinding(RichInputText headIgstAmountBinding) {
        this.headIgstAmountBinding = headIgstAmountBinding;
    }

    public RichInputText getHeadIgstAmountBinding() {
        return headIgstAmountBinding;
    }

    public void setDiscountAmountBinding(RichInputText discountAmountBinding) {
        this.discountAmountBinding = discountAmountBinding;
    }

    public RichInputText getDiscountAmountBinding() {
        return discountAmountBinding;
    }

    public void setIgstPersBinding(RichInputText igstPersBinding) {
        this.igstPersBinding = igstPersBinding;
    }

    public RichInputText getIgstPersBinding() {
        return igstPersBinding;
    }

    public void setDetailIgstAmountBinding(RichInputText detailIgstAmountBinding) {
        this.detailIgstAmountBinding = detailIgstAmountBinding;
    }

    public RichInputText getDetailIgstAmountBinding() {
        return detailIgstAmountBinding;
    }


    public void itemTypeVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce.getNewValue()!=vce.getOldValue())
        {
               OperationBinding op = ADFUtils.findOperation("valueSetNullInDetail");
               op.execute();
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(detailTableBinding);
    }

    public void approvedByVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce!=null)
        {
            vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
            OperationBinding op = (OperationBinding) ADFUtils.findOperation("checkAuthorityForComInvoice");
            op.getParamsMap().put("unitCd", unitCodeBinding.getValue().toString());
            op.getParamsMap().put("empCd", approvedByBinding.getValue());
            op.execute(); 
        }
    }

    public void docTypeVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce.getNewValue()!=vce.getOldValue())
        {
               System.out.println("Item Type VCL:"+docTypeBinding.getValue());
               OperationBinding op = ADFUtils.findOperation("setNullInHeaderCommercialInvoice");
               op.execute();
        }
    }

    public void setDtlGrossAmountBinding(RichInputText dtlGrossAmountBinding) {
        this.dtlGrossAmountBinding = dtlGrossAmountBinding;
    }

    public RichInputText getDtlGrossAmountBinding() {
        return dtlGrossAmountBinding;
    }

    public void setProdCodeBinding(RichInputComboboxListOfValues prodCodeBinding) {
        this.prodCodeBinding = prodCodeBinding;
    }

    public RichInputComboboxListOfValues getProdCodeBinding() {
        return prodCodeBinding;
    }

    public void discountVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce != null)
        {
            ADFUtils.findOperation("getProductInvoiceCalculation").execute();
            ADFUtils.findOperation("commercialInvoiceGstCalculation").execute(); 
        }
    }
}
