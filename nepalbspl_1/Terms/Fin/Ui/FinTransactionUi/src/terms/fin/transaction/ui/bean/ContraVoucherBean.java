package terms.fin.transaction.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import java.util.Map;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;

import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.adf.view.rich.event.ReturnPopupEvent;

import oracle.jbo.domain.Date;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import javax.faces.validator.ValidatorException;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;
import oracle.jbo.domain.Number;

import terms.fin.transaction.model.view.ContraVoucherDetailVORowImpl;

public class ContraVoucherBean {
    private RichInputText bindDiffnceAmunt;
    private RichPopup bindCondtnPopUp;

    private RichInputText fcBindings;
    private String mode;
    private RichPanelHeader myPageRootComponent;
    private RichOutputText bindingOutputText;
    private RichButton headerEditBinding;
    private RichButton detailcreateBinding;
    private RichButton detaildeleteBinding;
    private RichInputText bindingVoucherNo;
    private RichInputComboboxListOfValues bindUnitNo;
    private RichInputDate bindVouDate;
    private RichInputText voucherSeriesBinding;
    private RichInputComboboxListOfValues preparedByBinding;
    private RichInputComboboxListOfValues approvedByBinding;
    private RichInputComboboxListOfValues verifiedByBinding;
    private RichInputText seqNoBinding;
    private RichInputText totalBalanceBinding;
    private RichTable detailTableBinding;
    private String PageMode = "C";
    private RichInputText amountlcBinding;
    private RichInputText currencyRateBinding;
    private RichInputText crAmountBinding;
    private RichInputText drAmountBinding;
    private RichInputText voucherAmountBinding;
    private RichInputComboboxListOfValues currencyCodeBinding;
    private RichInputDate approvedOnBinding;
    private RichInputText chequeNoBinding;
    private RichInputText subTypeBinding;
    private RichSelectOneChoice chequeRtgsBinding;
    private RichInputDate chDateBinding;
    private RichSelectOneChoice drCrFlagBinding;
    private RichInputComboboxListOfValues glCodeBinding;


    public ContraVoucherBean() {
    }

    public void setBindDiffnceAmunt(RichInputText bindDiffnceAmunt) {
        this.bindDiffnceAmunt = bindDiffnceAmunt;
    }

    public RichInputText getBindDiffnceAmunt() {
        return bindDiffnceAmunt;
    }

    public void setBindCondtnPopUp(RichPopup bindCondtnPopUp) {
        this.bindCondtnPopUp = bindCondtnPopUp;
    }

    public RichPopup getBindCondtnPopUp() {
        return bindCondtnPopUp;
    }

    public void setFcBindings(RichInputText fcBindings) {
        this.fcBindings = fcBindings;
    }

    public RichInputText getFcBindings() {
        return fcBindings;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getMode() {
        return mode;
    }

    public void frequencyVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        OperationBinding op1 = ADFUtils.findOperation("getcheckApprovedSatus");
        op1.execute();
        System.out.println("Result: " + op1.getResult().toString());

        if (op1.getResult() != null && op1.getResult().toString().equalsIgnoreCase("N")) {
            System.out.println("Value of mode: " + ADFUtils.resolveExpression("#{pageFlowScope.mode}"));
            if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C") ||
                ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("S") ||
                ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("P")) {
                System.out.println("Inside condition!!");
                getApprovedByBinding().setDisabled(true);
                //authCodeBinding.setDisabled(true);
            } else {
                getApprovedByBinding().setDisabled(false);
            }
        } else {
            getApprovedByBinding().setDisabled(false);
        }
        System.out.println("MODE VALUE: " + ADFUtils.resolveExpression("#{pageFlowScope.mode}") +
                           " And AuthCode Value: " + approvedByBinding.getValue());
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E") && approvedByBinding.getValue() != null) {
            System.out.println("inside my if condition !!");
            approvedByBinding.setValue(null);
        }
        
    }


    public void createDtlAl(ActionEvent actionEvent) {
        getChequeRtgsBinding().setDisabled(true);
        DCIteratorBinding dci = ADFUtils.findIterator("ContraVoucherDetailVO1Iterator");
        if (dci != null) {
            Integer seq_no = fetchMaxLineNumber();
            RowSetIterator rsi = dci.getRowSetIterator();
            if (rsi != null) {
                Row last = rsi.last();
                int i = rsi.getRangeIndexOf(last);
                ContraVoucherDetailVORowImpl newRow = (ContraVoucherDetailVORowImpl) rsi.createRow();
                newRow.setNewRowState(Row.STATUS_INITIALIZED);
                rsi.insertRowAtRangeIndex(i + 1, newRow);
                rsi.setCurrentRow(newRow);
                newRow.setSeqNo(seq_no);
                if (seq_no == 1)
                    newRow.setDrCrFlag("D");
                else
                    newRow.setDrCrFlag("C");
            }
            rsi.closeRowSetIterator();
        }
    }

    private Integer fetchMaxLineNumber() {
        Integer max = new Integer(0);
        DCIteratorBinding itr = ADFUtils.findIterator("ContraVoucherDetailVO1Iterator");
        if (itr != null) {
            ContraVoucherDetailVORowImpl currRow = (ContraVoucherDetailVORowImpl) itr.getCurrentRow();
            if (currRow != null && currRow.getSeqNo() != null)
                max = currRow.getSeqNo();
            RowSetIterator rsi = itr.getRowSetIterator();
            if (rsi != null) {
                Row[] allRowsInRange = rsi.getAllRowsInRange();
                for (Row rw : allRowsInRange) {
                    if (rw != null && rw.getAttribute("SeqNo") != null &&
                        max.compareTo(Integer.parseInt(rw.getAttribute("SeqNo").toString())) < 0)
                        max = Integer.parseInt(rw.getAttribute("SeqNo").toString());
                }
            }
            rsi.closeRowSetIterator();
        }
        max = max + new Integer(1);
        return max;
    }

    public void chequeNoValidation(FacesContext facesContext, UIComponent uIComponent, Object object) {
        System.out.println("Value of Dr Cr Flag: " + drCrFlagBinding.getValue());
        System.out.println("Cheque/RTGS value: "+chequeRtgsBinding.getValue());
        if (object.toString().isEmpty() && chequeRtgsBinding.getValue()!=null && subTypeBinding.getValue()!=null) {
            if (chequeRtgsBinding.getValue().equals("C") && subTypeBinding.getValue().equals("BA") &&
                drCrFlagBinding.getValue().equals(0)) {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ch.No./RTGS is required.",
                                                              null));

            }
        }

        if (!object.toString().isEmpty()) {
            if (chequeRtgsBinding.getValue().equals("C") && drCrFlagBinding.getValue().equals(0)) {
                String str = (String) object;
                System.out.println("SubType Value: " + subTypeBinding.getValue());
                if (subTypeBinding.getValue().equals("BA")) {
                    if (str.length() == 6) {
                        System.out.println("GL code Value: " + glCodeBinding.getValue());
                        OperationBinding opr = ADFUtils.findOperation("getBankCodeForCV");
                        opr.getParamsMap().put("glCode", glCodeBinding.getValue());
                        Object result = opr.execute();
                        if (result != null && result.toString().trim().length() > 0 &&
                            !result.toString().equalsIgnoreCase("N")) {
                            System.out.println("Value of Bank Code received: " + result.toString());
                            OperationBinding op = ADFUtils.findOperation("getChequeNoValid");
                            op.getParamsMap().put("chequeno", object);
                            op.getParamsMap().put("bankCode", result.toString());
                            Object rst = op.execute();
                            if (rst != null && rst.toString().trim().length() > 0 &&
                                rst.toString().equalsIgnoreCase("N")) {
                                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                              "Entered Cheque No./DD No. Is Not Valid.",
                                                                              null));
                            }
                            else{
                                    System.out.println("Value after function for cheque number validation:" + object);
                                    OperationBinding op1 = ADFUtils.findOperation("getChequeNoValidBankCashVoucherDuplicacy");
                                    op1.getParamsMap().put("chequeno", object);
                                    Object rst1 = op1.execute();
                                    System.out.println("Value after function for cheque number validation:" + rst);
                                    if (rst1 != null && rst1.toString().trim().length() > 0 && rst1.toString().equalsIgnoreCase("Y")) {
                                        throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                                      "Entered Cheque No./DD No. alredy Used.", null));
                                
                                }
                            }
                            
                        } else {
                            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                          "No Entry available in Bank for this Ledger.",
                                                                          null));
                        }

                    } else {
                        throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                      "Entered Cheque No./DD No. must be of 6 digits.",
                                                                      null));
                    }
                }
            }
        }
    }

    public void saveAl(ActionEvent actionEvent) {
        mode = "S";
        commitAL();
    }

    public Boolean missMatchcheckOut() {
        Boolean Status = true;
        if (bindDiffnceAmunt.getValue() != null && bindDiffnceAmunt.getValue().toString().trim().length() > 0) {
            BigDecimal diff = (BigDecimal) bindDiffnceAmunt.getValue();
            if (diff.compareTo(new BigDecimal(0)) == 0) {
                OperationBinding op = ADFUtils.findOperation("controlAmountValueContraVoucher");
                op.getParamsMap().put("val", drAmountBinding.getValue());
                op.execute();
            } else {
                bindCondtnPopUp.cancel();
                ADFUtils.showMessage("Their Is Miss Match In Cr. and Dr. Amount Kindly Re-Check It.", 0);
                Status = false;
                mode = "S";
            }
        }
        return Status;
    }

    public String yesAc() {
        bindCondtnPopUp.hide();
        if (verifiedByBinding.getValue() != null && approvedByBinding.getValue() != null) {
            OperationBinding op = ADFUtils.findOperation("getVouNo");
            Object rst = op.execute();
            if (rst != null && rst.toString().trim().length() > 0) {
                Object execute = ADFUtils.findOperation("Commit").execute();
                ADFUtils.showMessage("Record Saved Successfully, Voucher No. Is " + rst, 2);
                if (mode.equalsIgnoreCase("S")) {
                    ADFUtils.findOperation("CreateInsert1").execute();
                    AdfFacesContext.getCurrentInstance().addPartialTarget(detailTableBinding);
                }
                return mode == "S" ? null : "saveAndReturn";
            } else {
                Object execute = ADFUtils.findOperation("Commit").execute();
                ADFUtils.showMessage("Record Updated Successfully", 2);
                if (mode.equalsIgnoreCase("S")) {
                    ADFUtils.findOperation("CreateInsert1").execute();
                    AdfFacesContext.getCurrentInstance().addPartialTarget(detailTableBinding);
                }

            }
        } else {
            System.out.println("In Else loop");
            FacesMessage Message = new FacesMessage("Please Verify and Approve the record.");
            Message.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }
        return null;
    }

    public String noAC() {
        bindCondtnPopUp.hide();
        //        if (verifiedByBinding.getValue() != null) {
        OperationBinding op = ADFUtils.findOperation("getdummyFunction");
        Object rst = op.execute();
        if (rst != null && rst.toString().trim().length() > 0) {
            ADFUtils.findOperation("Commit").execute();
            ADFUtils.showMessage("Record Saved Successfully, Dummy_Voucher No. Is " + rst, 2);
            if (mode.equalsIgnoreCase("S")) {
                ADFUtils.findOperation("CreateInsert1").execute();

                AdfFacesContext.getCurrentInstance().addPartialTarget(detailTableBinding);


            }
            return mode == "S" ? null : "saveAndReturn";
        } else {
            Object execute = ADFUtils.findOperation("Commit").execute();
            ADFUtils.showMessage("Record Updated Successfully ", 2);
            if (mode.equalsIgnoreCase("S")) {
                ADFUtils.findOperation("CreateInsert1").execute();
                AdfFacesContext.getCurrentInstance().addPartialTarget(detailTableBinding);
            }
        }
        //        }
        //        else {
        //            System.out.println("In Else loop");
        //            FacesMessage Message = new FacesMessage("Please Verify and Approve the record.");
        //            Message.setSeverity(FacesMessage.SEVERITY_ERROR);
        //            FacesContext fc = FacesContext.getCurrentInstance();
        //            fc.addMessage(null, Message);
        //        }
        return null;
    }

    public void commitAL() {

        try {
            System.out.println("mode is" + ADFUtils.resolveExpression("#{pageFlowScope.mode}") + "PageMode" + PageMode);
            if (PageMode.equalsIgnoreCase("E")) {
                System.out.println("Inside Edit Mode Call method");
                ADFUtils.findOperation("setInEditModeContraVoucher").execute();
            }
        } catch (Exception e) {
            System.out.println("Inside Error");
        }


        boolean result = missMatchcheckOut();
        if (result) {
            if (!drAmountBinding.getValue().equals(new BigDecimal(0)) ||
                !crAmountBinding.getValue().equals(new BigDecimal(0))) {
                OperationBinding op = ADFUtils.findOperation("getcheckApprovedSatus");
                op.execute();
                Object rst = op.execute();
                System.out.println("**************OP Result is*************" + op.getResult());
                if (rst != null && rst.toString().trim().length() > 0 && rst.toString().equalsIgnoreCase("N")) {
                    //                RichPopup.PopupHints hints = new RichPopup.PopupHints();
                    //                getBindCondtnPopUp().show(hints);
                    if (bindingVoucherNo.getValue() != null) {
                        if (approvedByBinding.getValue() != null) {
                            if (approvedOnBinding.getValue() != null) {
                                OperationBinding op1 = ADFUtils.findOperation("getVouNo");
                                Object rst1 = op1.execute();
                                if (rst1 != null && rst1.toString().trim().length() > 0) {
                                    ADFUtils.findOperation("Commit").execute();
                                    ADFUtils.showMessage("Record Saved Successfully,Approved Voucher No. Is " + rst1,
                                                         2);
                                    ADFUtils.findOperation("CreateInsert1").execute();
                                    AdfFacesContext.getCurrentInstance().addPartialTarget(detailTableBinding);
                                }
                            } else {
                                ADFUtils.showMessage("Please select Approval Date.", 0);
                            }
                        } 
                        else if(approvedByBinding.getValue() == null){
                                ADFUtils.findOperation("Commit").execute();
                                ADFUtils.showMessage("Record Updated Successfully.", 2);
                        }
                        
                        else {
                            ADFUtils.showMessage("Please Approve the record.", 0);
                        }
                    } else {
                        OperationBinding op1 = ADFUtils.findOperation("getdummyFunction");
                        Object rst1 = op1.execute();
                        if (rst1 != null && rst1.toString().trim().length() > 0) {
                            ADFUtils.findOperation("Commit").execute();
                            ADFUtils.showMessage("Record Saved Successfully, Dummy_Voucher No. Is " + rst1, 2);
                            //            if (mode.equalsIgnoreCase("S")) {
                            ADFUtils.findOperation("CreateInsert1").execute();

                            AdfFacesContext.getCurrentInstance().addPartialTarget(detailTableBinding);
                        }

                        //            }
                        //            return mode == "S" ? null : "saveAndReturn";
                    }
                } else {

                    if (approvedByBinding.getValue() != null) {
                        if (approvedOnBinding.getValue() != null) {
                            OperationBinding op1 = ADFUtils.findOperation("getVouNo");
                            Object rst1 = op1.execute();
                            if (rst1 != null && rst1.toString().trim().length() > 0) {
                                ADFUtils.findOperation("Commit").execute();
                                ADFUtils.showMessage("Record Saved Successfully, Voucher No. Is " + rst1, 2);
                                ADFUtils.findOperation("CreateInsert1").execute();
                                AdfFacesContext.getCurrentInstance().addPartialTarget(detailTableBinding);
                            } else {
                                ADFUtils.findOperation("Commit").execute();
                                ADFUtils.showMessage("Record Updated Successfully.", 2);
                            }

                        } else {
                            ADFUtils.showMessage("Please select Approval Date.", 0);
                        }
                    } else {
                        ADFUtils.showMessage("Please Approve The Record.", 0);

                    }
                }
            } else
                ADFUtils.showMessage("Voucher Can't be created with zero amount.", 0);
        }
          
    }

    public void voucherDateValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (object != null) {
            oracle.jbo.domain.Date d = null;
            java.util.Date d1 = null;
            String str = object.toString();
            try {
                d = new oracle.jbo.domain.Date(str);
                System.out.println("Date converted: " + d);
            } catch (Exception e) {
                d1 = new java.util.Date(str);
                System.out.println("Date converted: " + d1);
            }
            System.out.println("in method vou date" + object);
            OperationBinding op1 = ADFUtils.findOperation("checkForMonthAndYearVouSeriesForAllVouchers");
            if (d != null) {
                op1.getParamsMap().put("vou_dt", d);
            }
            if (d1 != null) {
                op1.getParamsMap().put("vou_dt", d1);
            }
            op1.getParamsMap().put("FinYear", ADFUtils.resolveExpression("#{pageFlowScope.finYear}"));
            //op1.getParamsMap().put("FinYear", "18-19");
            op1.getParamsMap().put("Vou_Type", "R");
            op1.getParamsMap().put("Vou_Series", "V");
            op1.getParamsMap().put("Unit_Code", bindUnitNo.getValue());
            op1.execute();

            if (op1.getResult() != null && !op1.getResult().toString().equalsIgnoreCase("Y")) {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, op1.getResult().toString(),
                                                              "Please Select Another Date."));

            }
        }
    }

    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getMyPageRootComponent(), true);
            cevModeDisableComponent("V");
            PageMode = "V";
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getMyPageRootComponent(), false);
            cevModeDisableComponent("C");
            PageMode = "C";
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getMyPageRootComponent(), false);
            cevModeDisableComponent("E");
            PageMode = "E";
        }
    }

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }

    public void cevModeDisableComponent(String mode) {
        if (mode.equals("E")) {
            getBindVouDate().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            getDetaildeleteBinding().setDisabled(false);
            getDetailcreateBinding().setDisabled(false);
            getBindUnitNo().setDisabled(true);
            getBindingVoucherNo().setDisabled(true);
            //getApprovedByBinding().setDisabled(true);
            getVerifiedByBinding().setDisabled(true);
            getPreparedByBinding().setDisabled(true);
            getTotalBalanceBinding().setDisabled(true);
            getSeqNoBinding().setDisabled(true);
            getPreparedByBinding().setDisabled(true);
            getVoucherSeriesBinding().setDisabled(true);
            getCrAmountBinding().setDisabled(true);
            getDrAmountBinding().setDisabled(true);
            getVoucherAmountBinding().setDisabled(true);
            getBindDiffnceAmunt().setDisabled(true);
            getCurrencyRateBinding().setDisabled(true);
            //getApprovedOnBinding().setDisabled(true);
            getChequeRtgsBinding().setDisabled(true);
        }
        if (mode.equals("C")) {
            //getBindVouDate().setDisabled(true);
            getBindUnitNo().setDisabled(true);
            getBindingVoucherNo().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            getApprovedByBinding().setDisabled(true);
            //getVerifiedByBinding().setDisabled(true);
            getBindingVoucherNo().setDisabled(true);
            getTotalBalanceBinding().setDisabled(true);
            getSeqNoBinding().setDisabled(true);
            getPreparedByBinding().setDisabled(true);
            getVoucherSeriesBinding().setDisabled(true);
            getCrAmountBinding().setDisabled(true);
            getDrAmountBinding().setDisabled(true);
            getVoucherAmountBinding().setDisabled(true);
            getBindDiffnceAmunt().setDisabled(true);
            getCurrencyRateBinding().setDisabled(true);
            getApprovedOnBinding().setDisabled(true);

        }
        if (mode.equals("V")) {
            getDetailcreateBinding().setDisabled(true);
            getDetaildeleteBinding().setDisabled(true);

        }

    }

    public void setMyPageRootComponent(RichPanelHeader myPageRootComponent) {
        this.myPageRootComponent = myPageRootComponent;
    }

    public RichPanelHeader getMyPageRootComponent() {
        return myPageRootComponent;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setDetailcreateBinding(RichButton detailcreateBinding) {
        this.detailcreateBinding = detailcreateBinding;
    }

    public RichButton getDetailcreateBinding() {
        return detailcreateBinding;
    }

    public void setDetaildeleteBinding(RichButton detaildeleteBinding) {
        this.detaildeleteBinding = detaildeleteBinding;
    }

    public RichButton getDetaildeleteBinding() {
        return detaildeleteBinding;
    }

    public void editButtonAL(ActionEvent actionEvent) {
        if (approvedByBinding.getValue() == null) {
            cevmodecheck();
        } else {
            ADFUtils.showMessage("Record is Approved Can't be modified !", 2);
            ADFUtils.setEL("#{pageFlowScope.mode}", "V");
        }
    }

    public void setBindingVoucherNo(RichInputText bindingVoucherNo) {
        this.bindingVoucherNo = bindingVoucherNo;
    }

    public RichInputText getBindingVoucherNo() {
        return bindingVoucherNo;
    }

    public void setBindUnitNo(RichInputComboboxListOfValues bindUnitNo) {
        this.bindUnitNo = bindUnitNo;
    }

    public RichInputComboboxListOfValues getBindUnitNo() {
        return bindUnitNo;
    }

    public void setBindVouDate(RichInputDate bindVouDate) {
        this.bindVouDate = bindVouDate;
    }

    public RichInputDate getBindVouDate() {
        return bindVouDate;
    }

    public void setVoucherSeriesBinding(RichInputText voucherSeriesBinding) {
        this.voucherSeriesBinding = voucherSeriesBinding;
    }

    public RichInputText getVoucherSeriesBinding() {
        return voucherSeriesBinding;
    }

    public void setPreparedByBinding(RichInputComboboxListOfValues preparedByBinding) {
        this.preparedByBinding = preparedByBinding;
    }

    public RichInputComboboxListOfValues getPreparedByBinding() {
        return preparedByBinding;
    }

    public void setApprovedByBinding(RichInputComboboxListOfValues approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputComboboxListOfValues getApprovedByBinding() {
        return approvedByBinding;
    }

    public void setVerifiedByBinding(RichInputComboboxListOfValues verifiedByBinding) {
        this.verifiedByBinding = verifiedByBinding;
    }

    public RichInputComboboxListOfValues getVerifiedByBinding() {
        return verifiedByBinding;
    }

    public void setSeqNoBinding(RichInputText seqNoBinding) {
        this.seqNoBinding = seqNoBinding;
    }

    public RichInputText getSeqNoBinding() {
        return seqNoBinding;
    }

    public void setTotalBalanceBinding(RichInputText totalBalanceBinding) {
        this.totalBalanceBinding = totalBalanceBinding;
    }

    public RichInputText getTotalBalanceBinding() {
        return totalBalanceBinding;
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete").execute();
            System.out.println("Record Delete Successfully");
            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(detailTableBinding);

    }

    public void setDetailTableBinding(RichTable detailTableBinding) {
        this.detailTableBinding = detailTableBinding;
    }

    public RichTable getDetailTableBinding() {
        return detailTableBinding;
    }

    public void setAmountlcBinding(RichInputText amountlcBinding) {
        this.amountlcBinding = amountlcBinding;
    }

    public RichInputText getAmountlcBinding() {
        return amountlcBinding;
    }

    public void setCurrencyRateBinding(RichInputText currencyRateBinding) {
        this.currencyRateBinding = currencyRateBinding;
    }

    public RichInputText getCurrencyRateBinding() {
        return currencyRateBinding;
    }

    public void setCrAmountBinding(RichInputText crAmountBinding) {
        this.crAmountBinding = crAmountBinding;
    }

    public RichInputText getCrAmountBinding() {
        return crAmountBinding;
    }

    public void setDrAmountBinding(RichInputText drAmountBinding) {
        this.drAmountBinding = drAmountBinding;
    }

    public RichInputText getDrAmountBinding() {
        return drAmountBinding;
    }

    public void setVoucherAmountBinding(RichInputText voucherAmountBinding) {
        this.voucherAmountBinding = voucherAmountBinding;
    }

    public RichInputText getVoucherAmountBinding() {
        return voucherAmountBinding;
    }

    public String SaveCloseAction() {
        mode = "SR";
        try {
            System.out.println("mode is" + ADFUtils.resolveExpression("#{pageFlowScope.mode}") + "PageMode" + PageMode);
            if (PageMode.equalsIgnoreCase("E")) {
                System.out.println("Inside Edit Mode Call method");
                ADFUtils.findOperation("setInEditModeContraVoucher").execute();
            }
        } catch (Exception e) {
            System.out.println("Inside Error");
        }


        boolean result = missMatchcheckOut();
        //        if (result) {
        //            OperationBinding op = ADFUtils.findOperation("getcheckApprovedSatus");
        //            op.execute();
        //            Object rst = op.execute();
        //            System.out.println("**************OP Result is*************" + op.getResult());
        //            if (rst != null && rst.toString().trim().length() > 0 && rst.toString().equalsIgnoreCase("N")) {
        //                RichPopup.PopupHints hints = new RichPopup.PopupHints();
        //                getBindCondtnPopUp().show(hints);
        //            } else {
        //
        //                if (verifiedByBinding.getValue() != null && approvedByBinding.getValue() != null) {
        //
        //                    OperationBinding op1 = ADFUtils.findOperation("getVouNo");
        //                    Object rst1 = op1.execute();
        //                    if (rst1 != null && rst1.toString().trim().length() > 0) {
        //                        ADFUtils.findOperation("Commit").execute();
        //                        ADFUtils.showMessage("Record Saved Successfully, Voucher No. Is " + rst1, 2);
        //                    } else {
        //                        ADFUtils.findOperation("Commit").execute();
        //                        ADFUtils.showMessage("Record Updated Successfully.", 2);
        //                    }
        //                    return "saveAndReturn";
        //
        //                } else {
        //                    ADFUtils.showMessage("Please Verifiy And Approve The Record.", 0);
        //
        //                }
        //            }
        //        }
        if (result) {
            if (!drAmountBinding.getValue().equals(new BigDecimal(0)) ||
                !crAmountBinding.getValue().equals(new BigDecimal(0))) {
                OperationBinding op = ADFUtils.findOperation("getcheckApprovedSatus");
                op.execute();
                Object rst = op.execute();
                System.out.println("**************OP Result is*************" + op.getResult());
                if (rst != null && rst.toString().trim().length() > 0 && rst.toString().equalsIgnoreCase("N")) {
                    //                RichPopup.PopupHints hints = new RichPopup.PopupHints();
                    //                getBindCondtnPopUp().show(hints);
                    if (bindingVoucherNo.getValue() != null) {
                        if (approvedByBinding.getValue() != null) {
                            if (approvedOnBinding.getValue() != null) {
                                OperationBinding op1 = ADFUtils.findOperation("getVouNo");
                                Object rst1 = op1.execute();
                                if (rst1 != null && rst1.toString().trim().length() > 0) {
                                    ADFUtils.findOperation("Commit").execute();
                                    ADFUtils.showMessage("Record Saved Successfully, Approved Voucher No. Is " + rst1,
                                                         2);
                                    ADFUtils.findOperation("CreateInsert1").execute();
                                    AdfFacesContext.getCurrentInstance().addPartialTarget(detailTableBinding);
                                    return "saveAndReturn";
                                }
                            } else {
                                ADFUtils.showMessage("Please select Approval Date.", 0);
                            }
                        }
                        else if(approvedByBinding.getValue() == null){
                                ADFUtils.findOperation("Commit").execute();
                                ADFUtils.showMessage("Record Updated Successfully.", 2);
                                return "saveAndReturn";
                        }
                        
                        else {
                            ADFUtils.showMessage("Please Approve the record.", 0);
                        }
                    } else {
                        OperationBinding op1 = ADFUtils.findOperation("getdummyFunction");
                        Object rst1 = op1.execute();
                        if (rst1 != null && rst1.toString().trim().length() > 0) {
                            ADFUtils.findOperation("Commit").execute();
                            ADFUtils.showMessage("Record Saved Successfully, Dummy_Voucher No. Is " + rst1, 2);
                            //            if (mode.equalsIgnoreCase("S")) {
                            ADFUtils.findOperation("CreateInsert1").execute();
                            AdfFacesContext.getCurrentInstance().addPartialTarget(detailTableBinding);
                            return "saveAndReturn";
                        }

                        //            }
                        //            return mode == "S" ? null : "saveAndReturn";
                    }
                } else {

                    if (approvedByBinding.getValue() != null) {
                        if (approvedOnBinding.getValue() != null) {
                            OperationBinding op1 = ADFUtils.findOperation("getVouNo");
                            Object rst1 = op1.execute();
                            if (rst1 != null && rst1.toString().trim().length() > 0) {
                                ADFUtils.findOperation("Commit").execute();
                                ADFUtils.showMessage("Record Saved Successfully, Voucher No. Is " + rst1, 2);
                                ADFUtils.findOperation("CreateInsert1").execute();
                                AdfFacesContext.getCurrentInstance().addPartialTarget(detailTableBinding);
                                return "saveAndReturn";
                            } else {
                                ADFUtils.findOperation("Commit").execute();
                                ADFUtils.showMessage("Record Updated Successfully.", 2);
                            }

                        } else {
                            ADFUtils.showMessage("Please select Approval Date.", 0);
                        }
                    } else {
                        ADFUtils.showMessage("Please Approve The Record.", 0);

                    }
                }
            } else
                ADFUtils.showMessage("Voucher Can't be created with zero amount.", 0);
        }


        return null;
    }

    public void currencyCodeVCL(ValueChangeEvent valueChangeEvent) {
        if (valueChangeEvent != null) {
            OperationBinding op = ADFUtils.findOperation("CurrencyRateContraV");
            op.getParamsMap().put("currencyCode", currencyCodeBinding.getValue());
            Object rst = op.execute();
            if (rst != null && rst.toString().equals("F")) {
                FacesMessage message = new FacesMessage("Please validate currency in daily currency table.");
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(currencyCodeBinding.getClientId(), message);
            }
            if (fcBindings.getValue() != null) {
                fcBindings.setValue(null);
            }

        }
    }

    public void setCurrencyCodeBinding(RichInputComboboxListOfValues currencyCodeBinding) {
        this.currencyCodeBinding = currencyCodeBinding;
    }

    public RichInputComboboxListOfValues getCurrencyCodeBinding() {
        return currencyCodeBinding;
    }

    public void setApprovedOnBinding(RichInputDate approvedOnBinding) {
        this.approvedOnBinding = approvedOnBinding;
    }

    public RichInputDate getApprovedOnBinding() {
        return approvedOnBinding;
    }

    public void approvedByReturnListner(ReturnPopupEvent returnPopupEvent) {
        System.out.println("In Return Listner!!! Unit Code :" + bindUnitNo.getValue() + "Auth Code: " +
                           approvedByBinding.getValue() + " Amount: " +
                           drAmountBinding.getValue());
        if (approvedByBinding.getValue() != null) {
            OperationBinding op = ADFUtils.findOperation("checkApprovalStatusContraVoucher");
        //  OperationBinding op = ADFUtils.findOperation("checkApprovalStatus");
            op.getParamsMap().put("unitCd", bindUnitNo.getValue());
            op.getParamsMap().put("empCode", approvedByBinding.getValue());
            op.getParamsMap().put("amount", drAmountBinding.getValue());
            op.execute();
        }
    }

    public void amountLcVCL(ValueChangeEvent valueChangeEvent) {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E") && approvedByBinding.getValue() != null) {
            System.out.println("inside my if condition !!");
            approvedByBinding.setValue(null);
        }
    }

    public void setChequeNoBinding(RichInputText chequeNoBinding) {
        this.chequeNoBinding = chequeNoBinding;
    }

    public RichInputText getChequeNoBinding() {
        return chequeNoBinding;
    }

    public void setSubTypeBinding(RichInputText subTypeBinding) {
        this.subTypeBinding = subTypeBinding;
    }

    public RichInputText getSubTypeBinding() {
        return subTypeBinding;
    }

    public void setChequeRtgsBinding(RichSelectOneChoice chequeRtgsBinding) {
        this.chequeRtgsBinding = chequeRtgsBinding;
    }

    public RichSelectOneChoice getChequeRtgsBinding() {
        return chequeRtgsBinding;
    }

    public void ChequeDateValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (object==null && chequeRtgsBinding.getValue()!=null && subTypeBinding.getValue()!=null) {
        if (chequeRtgsBinding.getValue().equals("C") && subTypeBinding.getValue().equals("BA") &&
            drCrFlagBinding.getValue().equals(0)) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ch.Date is required.", null));
        }
    }
    }

    public void setChDateBinding(RichInputDate chDateBinding) {
        this.chDateBinding = chDateBinding;
    }

    public RichInputDate getChDateBinding() {
        return chDateBinding;
    }

    public void setDrCrFlagBinding(RichSelectOneChoice drCrFlagBinding) {
        this.drCrFlagBinding = drCrFlagBinding;
    }

    public RichSelectOneChoice getDrCrFlagBinding() {
        return drCrFlagBinding;
    }

    public void setGlCodeBinding(RichInputComboboxListOfValues glCodeBinding) {
        this.glCodeBinding = glCodeBinding;
    }

    public RichInputComboboxListOfValues getGlCodeBinding() {
        return glCodeBinding;
    }

    public void glCodeVCL(ValueChangeEvent valueChangeEvent) {
        chequeNoBinding.resetValue();
        chDateBinding.resetValue();
        AdfFacesContext.getCurrentInstance().addPartialTarget(chequeNoBinding);
        AdfFacesContext.getCurrentInstance().addPartialTarget(chDateBinding);
    }

    public void amountValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
//        if(glCodeBinding.getValue()!=null && totalBalanceBinding.getValue()!=null){
//        Integer i=(Integer)drCrFlagBinding.getValue();
//        BigDecimal ledBalance=(BigDecimal)totalBalanceBinding.getValue();
//        BigDecimal amt=(BigDecimal)object;
//        System.out.println("Dr/CR: "+i+" ledgerBalance: "+ledBalance+"amount: "+amt+"ledBalance.compareTo(amt)"+ledBalance.compareTo(amt));
//        if(ledBalance.compareTo(amt)==-1 && i==0){
//               throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Amount Should be less than or equal to Led. Balance.",
//                                                             null));
//
//           }
//        }

    }
}

