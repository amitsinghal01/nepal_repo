package terms.fin.transaction.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;

import terms.fin.transaction.model.view.AdvanceCollectionDetailVORowImpl;

public class CreateAdvanceCollectionBean {
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText chequeRtgsBinding;
    private RichInputDate chDateBinding;
    private RichInputDate approvalDateBinding;
    private RichButton editBinding;
    private RichTable bindDetailTable;
    private RichPanelHeader getMyPageRootComponent;
    private RichInputComboboxListOfValues approvedByBinding;
    private RichOutputText getBindingOutputText;
    private RichSelectOneChoice typeBinding;
    private RichInputText cityCodeBinding;
    private RichInputText adviceNoBinding;
    private RichInputDate vouDateBinding;
    private RichInputText qtyBinding;
    private RichInputText rateBinding;
    private RichInputText sgstAmtBinding;
    private RichInputText cgstAmtBinding;
    private RichInputText igstAmtBinding;
    private RichInputText advanceVouNoBinding;
    private RichInputText totalAmountBinding;
    private RichInputText advanceAmtBinding;
    private RichInputText snoBinding;
    private RichInputText sgstRateBinding;
    private RichInputText cgstRate;
    private RichInputText igstRateBinding;
    private RichInputText amdNoBinding;

    public CreateAdvanceCollectionBean() {
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setChequeRtgsBinding(RichInputText chequeRtgsBinding) {
        this.chequeRtgsBinding = chequeRtgsBinding;
    }

    public RichInputText getChequeRtgsBinding() {
        return chequeRtgsBinding;
    }

    public void setChDateBinding(RichInputDate chDateBinding) {
        this.chDateBinding = chDateBinding;
    }

    public RichInputDate getChDateBinding() {
        return chDateBinding;
    }

    public void setApprovalDateBinding(RichInputDate approvalDateBinding) {
        this.approvalDateBinding = approvalDateBinding;
    }

    public RichInputDate getApprovalDateBinding() {
        return approvalDateBinding;
    }

    public void setEditBinding(RichButton editBinding) {
        this.editBinding = editBinding;
    }

    public RichButton getEditBinding() {
        return editBinding;
    }

    public void EditButtonAL(ActionEvent actionEvent) {
        if (approvedByBinding.getValue() == null) {
            cevmodecheck();
        } else {
            ADFUtils.showMessage("Record is Approved Can't be modified !", 2);
            ADFUtils.setEL("#{pageFlowScope.mode}", "V");
        }
    }

    public void deleteRecordDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete").execute();
            System.out.println("Record Delete Successfully");
            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
        }

        AdfFacesContext.getCurrentInstance().addPartialTarget(bindDetailTable);

    }

    public void setBindDetailTable(RichTable bindDetailTable) {
        this.bindDetailTable = bindDetailTable;
    }

    public RichTable getBindDetailTable() {
        return bindDetailTable;
    }

    public void createAL(ActionEvent actionEvent) {
        DCIteratorBinding dci = ADFUtils.findIterator("AdvanceCollectionDetailVO1Iterator");
        if (dci != null) {
            Integer seq_no = fetchMaxLineNumber();
            RowSetIterator rsi = dci.getRowSetIterator();
            if (rsi != null) {
                Row last = rsi.last();
                int i = rsi.getRangeIndexOf(last);
                AdvanceCollectionDetailVORowImpl newRow = (AdvanceCollectionDetailVORowImpl) rsi.createRow();
                newRow.setNewRowState(Row.STATUS_INITIALIZED);
                rsi.insertRowAtRangeIndex(i + 1, newRow);
                rsi.setCurrentRow(newRow);
                newRow.setSNo(seq_no);
                newRow.setCaType("T");
            }
            rsi.closeRowSetIterator();
        }
    }

    private Integer fetchMaxLineNumber() {
        Integer max = new Integer(0);
        DCIteratorBinding itr = ADFUtils.findIterator("AdvanceCollectionDetailVO1Iterator");
        if (itr != null) {
            AdvanceCollectionDetailVORowImpl currRow = (AdvanceCollectionDetailVORowImpl) itr.getCurrentRow();
            if (currRow != null && currRow.getSNo() != null)
                max = currRow.getSNo();
            RowSetIterator rsi = itr.getRowSetIterator();
            if (rsi != null) {
                Row[] allRowsInRange = rsi.getAllRowsInRange();
                for (Row rw : allRowsInRange) {
                    if (rw != null && rw.getAttribute("SNo") != null &&
                        max.compareTo(Integer.parseInt(rw.getAttribute("SNo").toString())) < 0)
                        System.out.println();
                    max = Integer.parseInt(rw.getAttribute("SNo").toString());
                }
            }
            rsi.closeRowSetIterator();
        }
        max = max + new Integer(1);
        return max;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }

    public void cevModeDisableComponent(String mode) {
        if (mode.equals("E")) {
            getUnitCodeBinding().setDisabled(true);
            getApprovalDateBinding().setDisabled(true);
            getCityCodeBinding().setDisabled(true);
            getAdviceNoBinding().setDisabled(true);
            getVouDateBinding().setDisabled(true);
            if (typeBinding.getValue().equals("C")) {
                getChDateBinding().setDisabled(true);
                getChequeRtgsBinding().setDisabled(true);
            }
            if (typeBinding.getValue().equals("B")) {
                getChDateBinding().setDisabled(false);
                getChequeRtgsBinding().setDisabled(false);
            }
            getAdvanceVouNoBinding().setDisabled(true);
            getTotalAmountBinding().setDisabled(true);
            getSnoBinding().setDisabled(true);
            getSgstRateBinding().setDisabled(true);
            getSgstAmtBinding().setDisabled(true);
            getCgstRate().setDisabled(true);
            getCgstAmtBinding().setDisabled(true);
            getIgstRateBinding().setDisabled(true);
            getIgstAmtBinding().setDisabled(true);
            getAmdNoBinding().setDisabled(true);
        }
        if (mode.equals("C")) {
            getUnitCodeBinding().setDisabled(true);
            getApprovalDateBinding().setDisabled(true);
            getCityCodeBinding().setDisabled(true);
            getAdviceNoBinding().setDisabled(true);
            getVouDateBinding().setDisabled(true);
            getAdvanceVouNoBinding().setDisabled(true);
            getTotalAmountBinding().setDisabled(true);
            getSnoBinding().setDisabled(true);
            getSgstRateBinding().setDisabled(true);
            getSgstAmtBinding().setDisabled(true);
            getCgstRate().setDisabled(true);
            getCgstAmtBinding().setDisabled(true);
            getIgstRateBinding().setDisabled(true);
            getIgstAmtBinding().setDisabled(true);
            getAmdNoBinding().setDisabled(true);
        }
        if (mode.equals("V")) {


        }

    }

    public void setApprovedByBinding(RichInputComboboxListOfValues approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputComboboxListOfValues getApprovedByBinding() {
        return approvedByBinding;
    }

    public void setGetBindingOutputText(RichOutputText getBindingOutputText) {
        this.getBindingOutputText = getBindingOutputText;
    }

    public RichOutputText getGetBindingOutputText() {
        cevmodecheck();
        return getBindingOutputText;
    }

    public void setTypeBinding(RichSelectOneChoice typeBinding) {
        this.typeBinding = typeBinding;
    }

    public RichSelectOneChoice getTypeBinding() {
        return typeBinding;
    }

    public void typeVCL(ValueChangeEvent valueChangeEvent) {
        if (typeBinding.getValue().equals("C")) {
            getChDateBinding().setDisabled(true);
            getChequeRtgsBinding().setDisabled(true);
        }
        if (typeBinding.getValue().equals("B")) {
            getChDateBinding().setDisabled(false);
            getChequeRtgsBinding().setDisabled(false);
        }
    }

    public void setCityCodeBinding(RichInputText cityCodeBinding) {
        this.cityCodeBinding = cityCodeBinding;
    }

    public RichInputText getCityCodeBinding() {
        return cityCodeBinding;
    }

    public void setAdviceNoBinding(RichInputText adviceNoBinding) {
        this.adviceNoBinding = adviceNoBinding;
    }

    public RichInputText getAdviceNoBinding() {
        return adviceNoBinding;
    }

    public void setVouDateBinding(RichInputDate vouDateBinding) {
        this.vouDateBinding = vouDateBinding;
    }

    public RichInputDate getVouDateBinding() {
        return vouDateBinding;
    }

    public void setQtyBinding(RichInputText qtyBinding) {
        this.qtyBinding = qtyBinding;
    }

    public RichInputText getQtyBinding() {
        return qtyBinding;
    }

    public void setRateBinding(RichInputText rateBinding) {
        this.rateBinding = rateBinding;
    }

    public RichInputText getRateBinding() {
        return rateBinding;
    }

    public void ProdCodeVCL(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        ADFUtils.findOperation("amountCalculateAc").execute();
        //AdfFacesContext.getCurrentInstance().addPartialTarget(bindDetailTable);
    }

    public void saveAL(ActionEvent actionEvent) {
        BigDecimal advAmt=(BigDecimal)advanceAmtBinding.getValue();
        BigDecimal totalAmt=(BigDecimal)totalAmountBinding.getValue();
        if(advAmt.compareTo(totalAmt)==0){
        OperationBinding op = ADFUtils.findOperation("advanceCollectionNoGeneration");
        Object rst = op.execute();
        System.out.println("Result after Voucher Generation: " + rst);
        if (op.getResult() != null && !op.getResult().equals("NA")) {
            ADFUtils.findOperation("Commit").execute();
            if (op.getResult().equals("Y")) {
                ADFUtils.showMessage("Advice No. is " + adviceNoBinding.getValue(), 2);
            }
            if (op.getResult().equals("N")) {
                ADFUtils.showMessage("Record updated successfully.", 2);
            }
            ADFUtils.findOperation("CreateInsert").execute();
            AdfFacesContext.getCurrentInstance().addPartialTarget(bindDetailTable);
        }
        }
        else{
                ADFUtils.showMessage("Please check the total amount and Advance Amt.", 0);
            }
    }

    public void setSgstAmtBinding(RichInputText sgstAmtBinding) {
        this.sgstAmtBinding = sgstAmtBinding;
    }

    public RichInputText getSgstAmtBinding() {
        return sgstAmtBinding;
    }

    public void setCgstAmtBinding(RichInputText cgstAmtBinding) {
        this.cgstAmtBinding = cgstAmtBinding;
    }

    public RichInputText getCgstAmtBinding() {
        return cgstAmtBinding;
    }

    public void setIgstAmtBinding(RichInputText igstAmtBinding) {
        this.igstAmtBinding = igstAmtBinding;
    }

    public RichInputText getIgstAmtBinding() {
        return igstAmtBinding;
    }

    public String saveAndCloseAL() {
        BigDecimal advAmt=(BigDecimal)advanceAmtBinding.getValue();
        BigDecimal totalAmt=(BigDecimal)totalAmountBinding.getValue();
        if(advAmt.compareTo(totalAmt)==0){
        OperationBinding op = ADFUtils.findOperation("advanceCollectionNoGeneration");
        Object rst = op.execute();
        System.out.println("Result after Voucher Generation: " + rst);
        if (op.getResult() != null && !op.getResult().equals("NA")) {
            ADFUtils.findOperation("Commit").execute();
            if (op.getResult().equals("Y")) {
                ADFUtils.showMessage("Advice No. is " + adviceNoBinding.getValue(), 2);
            }
            if (op.getResult().equals("N")) {
                ADFUtils.showMessage("Record updated successfully.", 2);
            }
            return "SaveAndClose";
        }
        }
        else{
                ADFUtils.showMessage("Please check the total amount and Advance Amt.", 0);
            }
        return null;
    }

    public void setAdvanceVouNoBinding(RichInputText advanceVouNoBinding) {
        this.advanceVouNoBinding = advanceVouNoBinding;
    }

    public RichInputText getAdvanceVouNoBinding() {
        return advanceVouNoBinding;
    }

    public void setTotalAmountBinding(RichInputText totalAmountBinding) {
        this.totalAmountBinding = totalAmountBinding;
    }

    public RichInputText getTotalAmountBinding() {
        return totalAmountBinding;
    }

    public void setAdvanceAmtBinding(RichInputText advanceAmtBinding) {
        this.advanceAmtBinding = advanceAmtBinding;
    }

    public RichInputText getAdvanceAmtBinding() {
        return advanceAmtBinding;
    }

    public void setSnoBinding(RichInputText snoBinding) {
        this.snoBinding = snoBinding;
    }

    public RichInputText getSnoBinding() {
        return snoBinding;
    }

    public void setSgstRateBinding(RichInputText sgstRateBinding) {
        this.sgstRateBinding = sgstRateBinding;
    }

    public RichInputText getSgstRateBinding() {
        return sgstRateBinding;
    }

    public void setCgstRate(RichInputText cgstRate) {
        this.cgstRate = cgstRate;
    }

    public RichInputText getCgstRate() {
        return cgstRate;
    }

    public void setIgstRateBinding(RichInputText igstRateBinding) {
        this.igstRateBinding = igstRateBinding;
    }

    public RichInputText getIgstRateBinding() {
        return igstRateBinding;
    }

    public void setAmdNoBinding(RichInputText amdNoBinding) {
        this.amdNoBinding = amdNoBinding;
    }

    public RichInputText getAmdNoBinding() {
        return amdNoBinding;
    }
}
