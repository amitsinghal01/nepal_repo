package terms.fin.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;

import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class CreateAnnexurea {
    private RichInputText docNoBinding;
    private RichInputDate docDateBinding;
    private RichInputText challanNoBinding;
    private RichInputText challanLicNameBinding;
    private RichInputDate iecCodeNoBinding;
    private RichInputText panNoBinding;
    private RichInputText exportContractNoBinding;
    private RichInputText exportNameBinding;
    private RichInputText exportLicNoBinding;
    private RichInputDate exportDateBinding;
    private RichOutputText bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton editButtonBinding;
    private RichInputComboboxListOfValues unitCdBinding;
    private RichInputComboboxListOfValues prepByBinding;
    private RichShowDetailItem annexDetailBinding;
    private RichShowDetailItem invoiceTabBinding;
    private RichShowDetailItem itemwiseTabBinding;
    private RichShowDetailItem quotaTabBinding;
    private RichInputComboboxListOfValues invoiceNoBinding;
    private RichInputText slNoDtlBinding;
    private RichInputText itemCodeDBinding;
    private RichInputText itemDescDBinding;
    private RichInputText qtyDBinding;
    private RichInputText acUnitDBinding;
    private RichInputText unitPriceDBinding;
    private RichInputText slNoddBinding;
    private RichInputText itemCodeddBinding;
    private RichInputText ptrQtyddBinding;
    private RichInputText pmvExItemBinding;
    private RichTable detailDtlTableBinding;
    private RichInputText netAmountBinding;
    private RichInputText authDelCodeBinding;

    public CreateAnnexurea() {
    }

    public void saveButtonAL(ActionEvent actionEvent) 
    {
        OperationBinding opr= ADFUtils.findOperation("documentNumberGenerationAnnexurea");
        opr.execute();
        if(opr.getResult().equals("Y"))
        {
        ADFUtils.showMessage("Record Save Successfully.New Document No. is "+docNoBinding.getValue(),2);
        ADFUtils.findOperation("Commit").execute();
        }
        else if(opr.getResult().equals("N"))
        {
            ADFUtils.showMessage("Record Update Successfully.",2);
            ADFUtils.findOperation("Commit").execute();
        }
        else
        {
            ADFUtils.showMessage("Problem in Document No. Generation,Please Check.", 0);
        }
    }

    public void setDocNoBinding(RichInputText docNoBinding) {
        this.docNoBinding = docNoBinding;
    }

    public RichInputText getDocNoBinding() {
        return docNoBinding;
    }

    public void setDocDateBinding(RichInputDate docDateBinding) {
        this.docDateBinding = docDateBinding;
    }

    public RichInputDate getDocDateBinding() {
        return docDateBinding;
    }

    public void setChallanNoBinding(RichInputText challanNoBinding) {
        this.challanNoBinding = challanNoBinding;
    }

    public RichInputText getChallanNoBinding() {
        return challanNoBinding;
    }

    public void setChallanLicNameBinding(RichInputText challanLicNameBinding) {
        this.challanLicNameBinding = challanLicNameBinding;
    }

    public RichInputText getChallanLicNameBinding() {
        return challanLicNameBinding;
    }

    public void setIecCodeNoBinding(RichInputDate iecCodeNoBinding) {
        this.iecCodeNoBinding = iecCodeNoBinding;
    }

    public RichInputDate getIecCodeNoBinding() {
        return iecCodeNoBinding;
    }

    public void setPanNoBinding(RichInputText panNoBinding) {
        this.panNoBinding = panNoBinding;
    }

    public RichInputText getPanNoBinding() {
        return panNoBinding;
    }

    public void setExportContractNoBinding(RichInputText exportContractNoBinding) {
        this.exportContractNoBinding = exportContractNoBinding;
    }

    public RichInputText getExportContractNoBinding() {
        return exportContractNoBinding;
    }

    public void setExportNameBinding(RichInputText exportNameBinding) {
        this.exportNameBinding = exportNameBinding;
    }

    public RichInputText getExportNameBinding() {
        return exportNameBinding;
    }

    public void setExportLicNoBinding(RichInputText exportLicNoBinding) {
        this.exportLicNoBinding = exportLicNoBinding;
    }

    public RichInputText getExportLicNoBinding() {
        return exportLicNoBinding;
    }

    public void setExportDateBinding(RichInputDate exportDateBinding) {
        this.exportDateBinding = exportDateBinding;
    }

    public RichInputDate getExportDateBinding() {
        return exportDateBinding;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText()
    {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }
    
    
    
    private void cevmodecheck(){
            if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
                cevModeDisableComponent("V");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("C");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("E");
            }
        }

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
               try {
                   Method method1 =
                       component.getClass().getMethod("setDisabled", boolean.class);
                   if (method1 != null) {
                       method1.invoke(component, valueComponent);
                   }
               } catch (NoSuchMethodException e) {
                   try {
                       Method method =
                           component.getClass().getMethod("setReadOnly", boolean.class);
                       if (method != null) {
                           method.invoke(component, valueComponent);
                       }
                   } catch (Exception e1) {
                       // e.printStackTrace();//silently eat this exception.
                   }


               } catch (Exception e) {
                   // e.printStackTrace();//silently eat this exception.
               }
               List<UIComponent> childComponents = component.getChildren();
               for (UIComponent comp : childComponents) {
                   makeComponentHierarchyReadOnly(comp, valueComponent);
               }
           }
    
    
    //Set Fields and Button disable.
        public void cevModeDisableComponent(String mode) {

            if (mode.equals("E")) {
                getDocNoBinding().setDisabled(true);
                getDocDateBinding().setDisabled(true);
                getChallanNoBinding().setDisabled(true);
                getChallanLicNameBinding().setDisabled(true);
                getPanNoBinding().setDisabled(true);
                getExportContractNoBinding().setDisabled(true);
                getExportLicNoBinding().setDisabled(true);
                getExportNameBinding().setDisabled(true);
                getEditButtonBinding().setDisabled(true);
                getUnitCdBinding().setDisabled(true);
                getPrepByBinding().setDisabled(true);

                getItemCodeDBinding().setDisabled(true);
                getItemDescDBinding().setDisabled(true);
                getSlNoDtlBinding().setDisabled(true);
                getAcUnitDBinding().setDisabled(true);
                getUnitPriceDBinding().setDisabled(true);
                getQtyDBinding().setDisabled(true);

                getSlNoddBinding().setDisabled(true);
                getItemCodeddBinding().setDisabled(true);
//                getPtrQtyddBinding().setDisabled(true);
                getPmvExItemBinding().setDisabled(true);
//                getNetAmountBinding().setDisabled(true);
                getAuthDelCodeBinding().setDisabled(true);


            } else if (mode.equals("C")) 
            {
            getDocNoBinding().setDisabled(true);
            getEditButtonBinding().setDisabled(true);
            getUnitCdBinding().setDisabled(true);
            getPrepByBinding().setDisabled(true);
            
            getItemCodeDBinding().setDisabled(true);
            getItemDescDBinding().setDisabled(true);
            getSlNoDtlBinding().setDisabled(true);
            getAcUnitDBinding().setDisabled(true);
            getUnitPriceDBinding().setDisabled(true);
            getQtyDBinding().setDisabled(true);
            
            getSlNoddBinding().setDisabled(true);
            getItemCodeddBinding().setDisabled(true);
//            getPtrQtyddBinding().setDisabled(true);
            getPmvExItemBinding().setDisabled(true);
//            getNetAmountBinding().setDisabled(true);
            getAuthDelCodeBinding().setDisabled(true);

            } else if (mode.equals("V")) 
            {
                getInvoiceTabBinding().setDisabled(false);
                getAnnexDetailBinding().setDisabled(false);
                getItemwiseTabBinding().setDisabled(false);
                getQuotaTabBinding().setDisabled(false);
            }
            
        }

    public void setEditButtonBinding(RichButton editButtonBinding) {
        this.editButtonBinding = editButtonBinding;
    }

    public RichButton getEditButtonBinding() {
        return editButtonBinding;
    }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setUnitCdBinding(RichInputComboboxListOfValues unitCdBinding) {
        this.unitCdBinding = unitCdBinding;
    }

    public RichInputComboboxListOfValues getUnitCdBinding() {
        return unitCdBinding;
    }

    public void setPrepByBinding(RichInputComboboxListOfValues prepByBinding) {
        this.prepByBinding = prepByBinding;
    }

    public RichInputComboboxListOfValues getPrepByBinding() {
        return prepByBinding;
    }

    public void onPageLoad() {
        ADFUtils.findOperation("CreateInsert").execute();
        
        String unitCode = (String)ADFUtils.resolveExpression("#{pageFlowScope.unitCode}");
        if(unitCode!=null){
         OperationBinding op = ADFUtils.findOperation("getExporterDetailAnnex");             
        //op.getParamsMap().put("unitCd", unitCdBinding.getValue());
         op.getParamsMap().put("unitCd", ADFUtils.resolveExpression("#{pageFlowScope.unitCode}"));
        op.execute();
        }else{
            System.out.println("Unit wise filter is not allow=====");
        }
    }

    public void setAnnexDetailBinding(RichShowDetailItem annexDetailBinding) {
        this.annexDetailBinding = annexDetailBinding;
    }

    public RichShowDetailItem getAnnexDetailBinding() {
        return annexDetailBinding;
    }

    public void setInvoiceTabBinding(RichShowDetailItem invoiceTabBinding) {
        this.invoiceTabBinding = invoiceTabBinding;
    }

    public RichShowDetailItem getInvoiceTabBinding() {
        return invoiceTabBinding;
    }

    public void setItemwiseTabBinding(RichShowDetailItem itemwiseTabBinding) {
        this.itemwiseTabBinding = itemwiseTabBinding;
    }

    public RichShowDetailItem getItemwiseTabBinding() {
        return itemwiseTabBinding;
    }

    public void setQuotaTabBinding(RichShowDetailItem quotaTabBinding) {
        this.quotaTabBinding = quotaTabBinding;
    }

    public RichShowDetailItem getQuotaTabBinding() {
        return quotaTabBinding;
    }

    public void invoiceNoVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce!=null)
        {
            System.out.println("Invoice No is ===="+vce.getNewValue()+"herere we are===="+invoiceNoBinding.getValue());
            OperationBinding opr = ADFUtils.findOperation("populateAnnexureaDetail");             
            opr.getParamsMap().put("invoiceNo", invoiceNoBinding.getValue());
            opr.execute();
            System.out.println("Invoice  METHOD RESULT IS====>"+opr.getResult());
            if (opr.getResult().equals("Y")) 
            {
                System.out.println("Data is populated====");
            }
            OperationBinding op1 = ADFUtils.findOperation("populateAnnexaDtlToItemDtl");             
            op1.getParamsMap().put("invNo", invoiceNoBinding.getValue());
            op1.execute();
            System.out.println("Invoice================>"+op1.getResult());
            if (op1.getResult().equals("N")) 
            {
                ADFUtils.showMessage("Problem in Populating data in Detail to Detail,Please Check.", 0);
            }
        }
     }

    public void setInvoiceNoBinding(RichInputComboboxListOfValues invoiceNoBinding) {
        this.invoiceNoBinding = invoiceNoBinding;
    }

    public RichInputComboboxListOfValues getInvoiceNoBinding() {
        return invoiceNoBinding;
    }

    public void setSlNoDtlBinding(RichInputText slNoDtlBinding) {
        this.slNoDtlBinding = slNoDtlBinding;
    }

    public RichInputText getSlNoDtlBinding() {
        return slNoDtlBinding;
    }

    public void setItemCodeDBinding(RichInputText itemCodeDBinding) {
        this.itemCodeDBinding = itemCodeDBinding;
    }

    public RichInputText getItemCodeDBinding() {
        return itemCodeDBinding;
    }

    public void setItemDescDBinding(RichInputText itemDescDBinding) {
        this.itemDescDBinding = itemDescDBinding;
    }

    public RichInputText getItemDescDBinding() {
        return itemDescDBinding;
    }

    public void setQtyDBinding(RichInputText qtyDBinding) {
        this.qtyDBinding = qtyDBinding;
    }

    public RichInputText getQtyDBinding() {
        return qtyDBinding;
    }

    public void setAcUnitDBinding(RichInputText acUnitDBinding) {
        this.acUnitDBinding = acUnitDBinding;
    }

    public RichInputText getAcUnitDBinding() {
        return acUnitDBinding;
    }

    public void setUnitPriceDBinding(RichInputText unitPriceDBinding) {
        this.unitPriceDBinding = unitPriceDBinding;
    }

    public RichInputText getUnitPriceDBinding() {
        return unitPriceDBinding;
    }

    public void setSlNoddBinding(RichInputText slNoddBinding) {
        this.slNoddBinding = slNoddBinding;
    }

    public RichInputText getSlNoddBinding() {
        return slNoddBinding;
    }

    public void setItemCodeddBinding(RichInputText itemCodeddBinding) {
        this.itemCodeddBinding = itemCodeddBinding;
    }

    public RichInputText getItemCodeddBinding() {
        return itemCodeddBinding;
    }
    public void setPtrQtyddBinding(RichInputText ptrQtyddBinding) {
        this.ptrQtyddBinding = ptrQtyddBinding;
    }

    public RichInputText getPtrQtyddBinding() {
        return ptrQtyddBinding;
    }

    public void setPmvExItemBinding(RichInputText pmvExItemBinding) {
        this.pmvExItemBinding = pmvExItemBinding;
    }

    public RichInputText getPmvExItemBinding() {
        return pmvExItemBinding;
    }

    public void deleteDetailRecordDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete").execute();
            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(detailDtlTableBinding);
    }

    public void setDetailDtlTableBinding(RichTable detailDtlTableBinding) {
        this.detailDtlTableBinding = detailDtlTableBinding;
    }

    public RichTable getDetailDtlTableBinding() {
        return detailDtlTableBinding;
    }

    public void setNetAmountBinding(RichInputText netAmountBinding) {
        this.netAmountBinding = netAmountBinding;
    }

    public RichInputText getNetAmountBinding() {
        return netAmountBinding;
    }

    public void setAuthDelCodeBinding(RichInputText authDelCodeBinding) {
        this.authDelCodeBinding = authDelCodeBinding;
    }

    public RichInputText getAuthDelCodeBinding() {
        return authDelCodeBinding;
    }
}
