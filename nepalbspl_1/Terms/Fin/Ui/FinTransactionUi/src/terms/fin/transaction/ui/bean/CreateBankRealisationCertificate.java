package terms.fin.transaction.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;

import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

public class CreateBankRealisationCertificate {
    private RichInputText docNoBinding;
    private RichInputDate docDateBinding;
    private RichInputComboboxListOfValues invNoBinding;
    private RichInputDate invDateBinding;
    private RichInputText invValueInrBinding;
    private RichInputText invValFcBinding;
    private RichInputText shippBillNoBinding;
    private RichInputDate shippBillDateBinding;
    private RichOutputText bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton editButtonBinding;
    private RichInputDate brcDtBinding;
    private RichInputDate leoDtBinding;
    private RichInputComboboxListOfValues leoCurrVCL;
    private String brc_check="B";
    private oracle.jbo.domain.Date brc_date=null;
    private String currCode="N";
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputComboboxListOfValues preByBinding;
    private RichInputText fobValFC;
    private RichInputText fobValINR;
    private RichInputText meisRateBinding;
    private RichInputText meisAmountBinding;


    public CreateBankRealisationCertificate() {
    }

    public void saveButtonAL(ActionEvent actionEvent)
    
    {
//    System.out.println("Save currCode"+currCode+"||brc_date"+brc_date+"||brc_check"+brc_check);
//        OperationBinding op2=ADFUtils.findOperation("CheckFromDailyCurrBrc");
//        op2.getParamsMap().put("brccurr",currCode);
//        op2.getParamsMap().put("brcDt",brc_date);
//        op2.getParamsMap().put("check",brc_check);
//        op2.execute();
        //    private String brc_check="B";currCode

        
     //   Object ob=op2.execute();
//        System.out.println("result iss==>>"+op2.getResult());
//        if(op2.getResult()!=null && op2.getResult().toString().equalsIgnoreCase("Y")){
        
        OperationBinding opr= ADFUtils.findOperation("documentNumberGenerationBRC");
        opr.execute();
        if(opr.getResult().equals("Y"))
        {
        ADFUtils.showMessage("Record Save Successfully.New Document No. is "+docNoBinding.getValue(),2);
        ADFUtils.findOperation("Commit").execute();
        }
        else if(opr.getResult().equals("N"))
        {
            ADFUtils.showMessage("Record Update Successfully.",2);
            ADFUtils.findOperation("Commit").execute();
        }
        else
        {
            ADFUtils.showMessage("Problem in Document No. Generation,Please Check.", 0);
        }
//    }
//    else{
//        FacesMessage Message = new FacesMessage("Daily Currency is  not updated for this BRC Date in Currency Master");   
//        Message.setSeverity(FacesMessage.SEVERITY_INFO);   
//        FacesContext fc = FacesContext.getCurrentInstance();   
//        fc.addMessage(null, Message); 
//    }
    }

    public void setDocNoBinding(RichInputText docNoBinding) {
        this.docNoBinding = docNoBinding;
    }

    public RichInputText getDocNoBinding() {
        return docNoBinding;
    }

    public void setDocDateBinding(RichInputDate docDateBinding) {
        this.docDateBinding = docDateBinding;
    }

    public RichInputDate getDocDateBinding() {
        return docDateBinding;
    }

    public void setInvNoBinding(RichInputComboboxListOfValues invNoBinding) {
        this.invNoBinding = invNoBinding;
    }

    public RichInputComboboxListOfValues getInvNoBinding() {
        return invNoBinding;
    }

    public void setInvDateBinding(RichInputDate invDateBinding) {
        this.invDateBinding = invDateBinding;
    }

    public RichInputDate getInvDateBinding() {
        return invDateBinding;
    }

    public void setInvValueInrBinding(RichInputText invValueInrBinding) {
        this.invValueInrBinding = invValueInrBinding;
    }

    public RichInputText getInvValueInrBinding() {
        return invValueInrBinding;
    }

    public void setInvValFcBinding(RichInputText invValFcBinding) {
        this.invValFcBinding = invValFcBinding;
    }

    public RichInputText getInvValFcBinding() {
        return invValFcBinding;
    }

    public void setShippBillNoBinding(RichInputText shippBillNoBinding) {
        this.shippBillNoBinding = shippBillNoBinding;
    }

    public RichInputText getShippBillNoBinding() {
        return shippBillNoBinding;
    }

    public void setShippBillDateBinding(RichInputDate shippBillDateBinding) {
        this.shippBillDateBinding = shippBillDateBinding;
    }

    public RichInputDate getShippBillDateBinding() {
        return shippBillDateBinding;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }


    private void cevmodecheck(){
            if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
                cevModeDisableComponent("V");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("C");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("E");
            }
        }

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                try {
                    Method method1 =
                        component.getClass().getMethod("setDisabled", boolean.class);
                    if (method1 != null) {
                        method1.invoke(component, valueComponent);
                    }
                } catch (NoSuchMethodException e) {
                    try {
                        Method method =
                            component.getClass().getMethod("setReadOnly", boolean.class);
                        if (method != null) {
                            method.invoke(component, valueComponent);
                        }
                    } catch (Exception e1) {
                        // e.printStackTrace();//silently eat this exception.
                    }


                } catch (Exception e) {
                    // e.printStackTrace();//silently eat this exception.
                }
                List<UIComponent> childComponents = component.getChildren();
                for (UIComponent comp : childComponents) {
                    makeComponentHierarchyReadOnly(comp, valueComponent);
                }
            }
    
    
    
    //Set Fields and Button disable.
        public void cevModeDisableComponent(String mode) {
            if (mode.equals("E")) 
            {
                getFobValFC().setDisabled(true);
                getFobValINR().setDisabled(true);
                getDocNoBinding().setDisabled(true);
                getDocDateBinding().setDisabled(true);
                getInvNoBinding().setDisabled(true);
                getInvDateBinding().setDisabled(true);                
                getEditButtonBinding().setDisabled(true);
                getInvValueInrBinding().setDisabled(true);
                getInvValFcBinding().setDisabled(true);
                getUnitCodeBinding().setDisabled(true);
                getPreByBinding().setDisabled(true);
                
                getShippBillNoBinding().setDisabled(true);
                getShippBillDateBinding().setDisabled(true);
            }
            else if (mode.equals("C")) 
            {
                getFobValFC().setDisabled(true);
                getFobValINR().setDisabled(true);
                getDocNoBinding().setDisabled(true);
                getEditButtonBinding().setDisabled(true);
                getInvDateBinding().setDisabled(true);
                getInvValueInrBinding().setDisabled(true);
                getInvValFcBinding().setDisabled(true);
                getUnitCodeBinding().setDisabled(true);
                getPreByBinding().setDisabled(true);
                
                getShippBillNoBinding().setDisabled(true);
                getShippBillDateBinding().setDisabled(true);
            }
            else if (mode.equals("V"))
            {
                getEditButtonBinding().setDisabled(false);
            }
            
        }


    public void editButtonAL(ActionEvent actionEvent) 
    {
    cevmodecheck();
    }

    public void setEditButtonBinding(RichButton editButtonBinding) {
        this.editButtonBinding = editButtonBinding;
    }

    public RichButton getEditButtonBinding() {
        return editButtonBinding;
    }

    public void BrcCurrVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
          if(vce!=null){
              System.out.println("in case of B leoDtBinding.getValue()"+brcDtBinding.getValue());
              currCode = (String)vce.getNewValue();
              brc_check="B";
              brc_date=(oracle.jbo.domain.Date)brcDtBinding.getValue();

              OperationBinding op=ADFUtils.findOperation("CheckFromDailyCurrBrc");
              op.getParamsMap().put("brcCurrCd",vce.getNewValue());
              op.getParamsMap().put("brcDt",brcDtBinding.getValue());
              op.getParamsMap().put("check",brc_check);
              Object ob=op.execute();
              System.out.println("result iss==>>"+op.getResult());
              
              if(op.getResult()!=null && op.getResult().toString().equalsIgnoreCase("N")){
                  System.out.println("inside if when result is ===>> not IM");
                  
                      FacesMessage Message = new FacesMessage("Daily Currency is  not updated for this BRC Date in Currency Master");   
                      Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                      FacesContext fc = FacesContext.getCurrentInstance();   
                      fc.addMessage(null, Message); 
              }

          }
    }

    public void setBrcDtBinding(RichInputDate brcDtBinding) {
        this.brcDtBinding = brcDtBinding;
    }

    public RichInputDate getBrcDtBinding() {
        return brcDtBinding;
    }

    public void setLeoDtBinding(RichInputDate leoDtBinding) {
        this.leoDtBinding = leoDtBinding;
    }

    public RichInputDate getLeoDtBinding() {
        return leoDtBinding;
    }

    public void setLeoCurrVCL(RichInputComboboxListOfValues leoCurrVCL) {
        this.leoCurrVCL = leoCurrVCL;
    }

    public RichInputComboboxListOfValues getLeoCurrVCL() {
        return leoCurrVCL;
    }

    public void leoCurrVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
          if(vce!=null){
              currCode = (String)vce.getNewValue();
              brc_check="L";
              System.out.println("in case of L leoDtBinding.getValue()"+leoDtBinding.getValue());
              brc_date=(oracle.jbo.domain.Date)leoDtBinding.getValue();
              OperationBinding op=ADFUtils.findOperation("CheckFromDailyCurrBrc");
              op.getParamsMap().put("brcCurrCd",vce.getNewValue());
              op.getParamsMap().put("brcDt",leoDtBinding.getValue());
              op.getParamsMap().put("check",brc_check);
              Object ob=op.execute();
              System.out.println("result iss==>>"+op.getResult());
              
              if(op.getResult()!=null && op.getResult().toString().equalsIgnoreCase("N")){
                  System.out.println("inside if when result is ===>> not IM");
                  
                      FacesMessage Message = new FacesMessage("Daily Currency is  not updated for this BRC Date in Currency Master");   
                      Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                      FacesContext fc = FacesContext.getCurrentInstance();   
                      fc.addMessage(null, Message); 
              }

          }
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setPreByBinding(RichInputComboboxListOfValues preByBinding) {
        this.preByBinding = preByBinding;
    }

    public RichInputComboboxListOfValues getPreByBinding() {
        return preByBinding;
    }

    public void setFobValFC(RichInputText fobValFC) {
        this.fobValFC = fobValFC;
    }

    public RichInputText getFobValFC() {
        return fobValFC;
    }

    public void setFobValINR(RichInputText fobValINR) {
        this.fobValINR = fobValINR;
    }

    public RichInputText getFobValINR() {
        return fobValINR;
    }

    public void invoiceNoVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
          if(vce!=null)
          {
              System.out.println("innnnnnnnnnnnn");
              ADFUtils.findOperation("brcShippingBillNo").execute();
          }
          if(vce.getNewValue()!=null)
          {
            try {
                ADFUtils.findOperation("currencyCodeValuesFromInvoiceBRC").execute();
            } catch (Exception e) {
                e.printStackTrace();
            }
            }
    }

    public void ActFreightValueVCE(ValueChangeEvent valueChangeEvent) 
    {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(fobValINR.getValue()!=null)
        {
            try {
                BigDecimal fob_value = (BigDecimal) fobValINR.getValue();
                BigDecimal mesi_rate =
                    (BigDecimal) (meisRateBinding.getValue() != null ? meisRateBinding.getValue() : new BigDecimal(0));
                BigDecimal result = (fob_value.multiply(mesi_rate)).divide(new BigDecimal(100));
                result = result.setScale(5, BigDecimal.ROUND_HALF_UP);
                meisAmountBinding.setValue(result);
                AdfFacesContext.getCurrentInstance().addPartialTarget(meisAmountBinding);
                System.out.println("==============result================" + result);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
}

    public void setMeisRateBinding(RichInputText meisRateBinding) {
        this.meisRateBinding = meisRateBinding;
    }

    public RichInputText getMeisRateBinding() {
        return meisRateBinding;
    }

    public void setMeisAmountBinding(RichInputText meisAmountBinding) {
        this.meisAmountBinding = meisAmountBinding;
    }

    public RichInputText getMeisAmountBinding() {
        return meisAmountBinding;
    }
}
