package terms.fin.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichPanelLabelAndMessage;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichButton;

import oracle.jbo.JboException;

public class CreateBillOfLadingBean {
    private RichPanelLabelAndMessage bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichInputText docNoBinding;
    private RichInputComboboxListOfValues prepareByBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichShowDetailItem tab1Binding;
    private RichShowDetailItem tab2Binding;
    private RichInputDate shipBillDateBinding;
    private RichInputComboboxListOfValues bindInvNo;

    public CreateBillOfLadingBean() {
    }

    public void saveAL(ActionEvent actionEvent) {
        if(prepareByBinding.getValue()!=null){
        oracle.binding.OperationBinding op = ADFUtils.findOperation("generateDocNoForBillOfLading");
        Object rst = op.execute();

        System.out.println("--------Commit-------");
        System.out.println("value aftr getting result--->?" + rst);


        if (rst.toString() != null && rst.toString() != "") {
            if (op.getErrors().isEmpty()) {
                ADFUtils.findOperation("Commit").execute();
                FacesMessage Message = new FacesMessage("Record Saved Successfully.Advice No is " + rst + ".");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);

            }
        }

        if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ") {
            if (op.getErrors().isEmpty()) {
                ADFUtils.findOperation("Commit").execute();
                FacesMessage Message = new FacesMessage("Record Updated Successfully.");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
             }
           }
        }else{
            ADFUtils.showMessage("Prepared By is required.", 0);
        }
    }

    public void setBindingOutputText(RichPanelLabelAndMessage bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichPanelLabelAndMessage getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }
    private void cevmodecheck(){
          if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
              makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
              cevModeDisableComponent("V");
          }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
              makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
              cevModeDisableComponent("C");
          }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
              makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
              cevModeDisableComponent("E");
          }
      }
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
              try {
                  Method method1 =
                      component.getClass().getMethod("setDisabled", boolean.class);
                  if (method1 != null) {
                      method1.invoke(component, valueComponent);
                  }
              } catch (NoSuchMethodException e) {
                  try {
                      Method method =
                          component.getClass().getMethod("setReadOnly", boolean.class);
                      if (method != null) {
                          method.invoke(component, valueComponent);
                      }
                  } catch (Exception e1) {
                      // e.printStackTrace();//silently eat this exception.
                  }


              } catch (Exception e) {
                  // e.printStackTrace();//silently eat this exception.
              }
              List<UIComponent> childComponents = component.getChildren();
              for (UIComponent comp : childComponents) {
                  makeComponentHierarchyReadOnly(comp, valueComponent);
              }
          }


 
    //Set Fields and Button disable.
      public void cevModeDisableComponent(String mode) {
      //            FacesContext fctx = FacesContext.getCurrentInstance();
      //            ELContext elctx = fctx.getELContext();
      //            Application jsfApp = fctx.getApplication();
      //            //create a ValueExpression that points to the ADF binding layer
      //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
      //            //
      //            ValueExpression valueExpr = exprFactory.createValueExpression(
      //                                         elctx,
      //                                         "#{pageFlowScope.mode=='E'}",
      //                                          Object.class
      //                                         );
      //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
          if (mode.equals("E")) {
              
              getShipBillDateBinding().setDisabled(true);
                getDocNoBinding().setDisabled(true);
                  getHeaderEditBinding().setDisabled(true);
                  getUnitCodeBinding().setDisabled(true);
                  getPrepareByBinding().setDisabled(true);
                 
          } else if (mode.equals("C")) {
              getShipBillDateBinding().setDisabled(true);
              getDocNoBinding().setDisabled(true);
              getUnitCodeBinding().setDisabled(true);
              getPrepareByBinding().setDisabled(true);
          } else if (mode.equals("V")) {
             getTab1Binding().setDisabled(false);
             getTab2Binding().setDisabled(false);
          }
      }

    public void editButtonAL(ActionEvent actionEvent) {
        
        cevmodecheck();
    }

    public void setDocNoBinding(RichInputText docNoBinding) {
        this.docNoBinding = docNoBinding;
    }

    public RichInputText getDocNoBinding() {
        return docNoBinding;
    }

    public void setPrepareByBinding(RichInputComboboxListOfValues prepareByBinding) {
        this.prepareByBinding = prepareByBinding;
    }

    public RichInputComboboxListOfValues getPrepareByBinding() {
        return prepareByBinding;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setTab1Binding(RichShowDetailItem tab1Binding) {
        this.tab1Binding = tab1Binding;
    }

    public RichShowDetailItem getTab1Binding() {
        return tab1Binding;
    }

    public void setTab2Binding(RichShowDetailItem tab2Binding) {
        this.tab2Binding = tab2Binding;
    }

    public RichShowDetailItem getTab2Binding() {
        return tab2Binding;
    }

    public void setShipBillDateBinding(RichInputDate shipBillDateBinding) {
        this.shipBillDateBinding = shipBillDateBinding;
    }

    public RichInputDate getShipBillDateBinding() {
        return shipBillDateBinding;
    }

    public void InvNoVcl(ValueChangeEvent vce) {
//        if(vce.getNewValue()!=null && bindInvNo.getValue()==null){
//         OperationBinding opr=  (OperationBinding)ADFUtils.findOperation("generateSeqNo");
//         opr.execute();
//        }else{
//             throw new JboException("Invoice Number has been used before");
//         }
    }

    public void setBindInvNo(RichInputComboboxListOfValues bindInvNo) {
        this.bindInvNo = bindInvNo;
    }

    public RichInputComboboxListOfValues getBindInvNo() {
        return bindInvNo;
    }
}
