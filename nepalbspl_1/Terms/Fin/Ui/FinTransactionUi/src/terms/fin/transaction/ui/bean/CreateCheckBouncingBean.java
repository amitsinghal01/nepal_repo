package terms.fin.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;

public class CreateCheckBouncingBean {
    private RichPanelHeader getMyPageRootComponent;
    private RichButton editBinding;
    private String mode = "V";
    private RichInputComboboxListOfValues unitBinding;
    private RichInputText entryNoBinding;
    private RichInputText outputTextBinding;
    private RichInputText partyCodeBinding;
    private RichInputText glCodeBinding;
    private RichInputText glDescBinding;
    private RichInputText vouNoBinding;
    private RichInputDate vouDateBinding;
    private RichInputComboboxListOfValues bankGlCodeBinding;
    private RichInputText interUnitVouBinding;
    private RichInputText docNoBinding;
    private RichInputDate docDateBinding;
    private RichInputDate approvedDateBinding;
    private RichInputText reversalVouBinding;
    private RichInputComboboxListOfValues chkBinding;
    private RichInputComboboxListOfValues approvedByBinding;
    private RichInputDate bounceDateBinding;

    public CreateCheckBouncingBean() {
    }

    public void SaveAL(ActionEvent actionEvent) {
        OperationBinding op=(OperationBinding)ADFUtils.findOperation("getEntryNo");
        Object obj=op.execute();
        System.out.println("Result in Bean after function Call: "+op.getResult());
        if(op.getResult()!=null){
            ADFUtils.findOperation("Commit").execute();
            if(op.getResult().equals("T")){
            ADFUtils.showMessage("Record Updated Successfully!", 2);
            }
            if(op.getResult().equals("TA")){
            ADFUtils.showMessage("Entry No. Generated is "+entryNoBinding.getValue(), 2);
            }
            ADFUtils.findOperation("CreateInsert").execute();
        }
    }

    public void EditButtonAL(ActionEvent actionEvent) 
    {        
        if (approvedByBinding.getValue() == null) {
            cevmodecheck();
        } else {
            ADFUtils.showMessage("Record is Approved Can't be modified !", 2);
            ADFUtils.setEL("#{pageFlowScope.mode}", "V");
        }
    }
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }

    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }

    public void cevModeDisableComponent(String mode) {

        if (mode.equals("C")) {
            getEditBinding().setDisabled(true);
            getPartyCodeBinding().setDisabled(true);
            getUnitBinding().setDisabled(true);
            getEntryNoBinding().setDisabled(true);
            getGlCodeBinding().setDisabled(true);  
            getReversalVouBinding().setDisabled(true);
            getApprovedDateBinding().setDisabled(true);
            getDocDateBinding().setDisabled(true);
            getDocNoBinding().setDisabled(true);
            getInterUnitVouBinding().setDisabled(true);
            getBankGlCodeBinding().setDisabled(true);
            getVouDateBinding().setDisabled(true);
            getVouNoBinding().setDisabled(true);
            getGlDescBinding().setDisabled(true);

        }
        if (mode.equals("V")) {


        }
        if (mode.equals("E")) {
            getEditBinding().setDisabled(true);
            getPartyCodeBinding().setDisabled(true);        
            getUnitBinding().setDisabled(true);
            getEntryNoBinding().setDisabled(true);
            getPartyCodeBinding().setDisabled(true);
            getGlCodeBinding().setDisabled(true);
            getReversalVouBinding().setDisabled(true);
            getApprovedDateBinding().setDisabled(true);
            getDocDateBinding().setDisabled(true);
            getDocNoBinding().setDisabled(true);
            getInterUnitVouBinding().setDisabled(true);
            getBankGlCodeBinding().setDisabled(true);
            getVouDateBinding().setDisabled(true);
            getVouNoBinding().setDisabled(true);
            getGlDescBinding().setDisabled(true);
//            
        }
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setEditBinding(RichButton editBinding) {
        this.editBinding = editBinding;
    }

    public RichButton getEditBinding() {
        return editBinding;
    }

    public void setUnitBinding(RichInputComboboxListOfValues unitBinding) {
        this.unitBinding = unitBinding;
    }

    public RichInputComboboxListOfValues getUnitBinding() {
        return unitBinding;
    }

    public void setEntryNoBinding(RichInputText entryNoBinding) {
        this.entryNoBinding = entryNoBinding;
    }

    public RichInputText getEntryNoBinding() {
        return entryNoBinding;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getMode() {
        return mode;
    }

    public void setOutputTextBinding(RichInputText outputTextBinding) {
        this.outputTextBinding = outputTextBinding;
    }

    public RichInputText getOutputTextBinding() {
        cevmodecheck();
        return outputTextBinding;
    }

    public void setPartyCodeBinding(RichInputText partyCodeBinding) {
        this.partyCodeBinding = partyCodeBinding;
    }

    public RichInputText getPartyCodeBinding() {
        return partyCodeBinding;
    }

    public void DoctTypeVCE(ValueChangeEvent valueChangeEvent) {
        bounceDateBinding.resetValue();
        ADFUtils.findOperation("clearSelectedRowOnChangeDocType").execute();
        }

    public String saveAndCloseAL() {
        OperationBinding op=(OperationBinding)ADFUtils.findOperation("getEntryNo");
        Object obj=op.execute();
        System.out.println("Result after function Call: "+op.getResult());
        if(op.getResult()!=null){
            ADFUtils.findOperation("Commit").execute();
            if(op.getResult().equals("T")){
            ADFUtils.showMessage("Record Updated Successfully!", 2);
            }
            if(op.getResult().equals("TA")){
            ADFUtils.showMessage("Entry No. Generated is "+entryNoBinding.getValue(), 2);
            }
            ADFUtils.findOperation("CreateInsert").execute();
            return "Save and Close";
        }
        return null;
    }

    public void setGlCodeBinding(RichInputText glCodeBinding) {
        this.glCodeBinding = glCodeBinding;
    }

    public RichInputText getGlCodeBinding() {
        return glCodeBinding;
    }

    public void setGlDescBinding(RichInputText glDescBinding) {
        this.glDescBinding = glDescBinding;
    }

    public RichInputText getGlDescBinding() {
        return glDescBinding;
    }

    public void setVouNoBinding(RichInputText vouNoBinding) {
        this.vouNoBinding = vouNoBinding;
    }

    public RichInputText getVouNoBinding() {
        return vouNoBinding;
    }

    public void setVouDateBinding(RichInputDate vouDateBinding) {
        this.vouDateBinding = vouDateBinding;
    }

    public RichInputDate getVouDateBinding() {
        return vouDateBinding;
    }

    public void setBankGlCodeBinding(RichInputComboboxListOfValues bankGlCodeBinding) {
        this.bankGlCodeBinding = bankGlCodeBinding;
    }

    public RichInputComboboxListOfValues getBankGlCodeBinding() {
        return bankGlCodeBinding;
    }

    public void setInterUnitVouBinding(RichInputText interUnitVouBinding) {
        this.interUnitVouBinding = interUnitVouBinding;
    }

    public RichInputText getInterUnitVouBinding() {
        return interUnitVouBinding;
    }

    public void setDocNoBinding(RichInputText docNoBinding) {
        this.docNoBinding = docNoBinding;
    }

    public RichInputText getDocNoBinding() {
        return docNoBinding;
    }

    public void setDocDateBinding(RichInputDate docDateBinding) {
        this.docDateBinding = docDateBinding;
    }

    public RichInputDate getDocDateBinding() {
        return docDateBinding;
    }

    public void setApprovedDateBinding(RichInputDate approvedDateBinding) {
        this.approvedDateBinding = approvedDateBinding;
    }

    public RichInputDate getApprovedDateBinding() {
        return approvedDateBinding;
    }

    public void setReversalVouBinding(RichInputText reversalVouBinding) {
        this.reversalVouBinding = reversalVouBinding;
    }

    public RichInputText getReversalVouBinding() {
        return reversalVouBinding;
    }

    public void setChkBinding(RichInputComboboxListOfValues chkBinding) {
        this.chkBinding = chkBinding;
    }

    public RichInputComboboxListOfValues getChkBinding() {
        return chkBinding;
    }

    public void setApprovedByBinding(RichInputComboboxListOfValues approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputComboboxListOfValues getApprovedByBinding() {
        return approvedByBinding;
    }

    public void setBounceDateBinding(RichInputDate bounceDateBinding) {
        this.bounceDateBinding = bounceDateBinding;
    }

    public RichInputDate getBounceDateBinding() {
        return bounceDateBinding;
    }
}
