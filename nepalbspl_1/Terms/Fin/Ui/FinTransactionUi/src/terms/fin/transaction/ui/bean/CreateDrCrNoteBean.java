package terms.fin.transaction.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.math.RoundingMode;

import java.text.DecimalFormat;

import java.util.List;

import java.util.Map;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import javax.faces.validator.ValidatorException;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichPanelLabelAndMessage;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.adf.view.rich.event.ReturnPopupEvent;

import oracle.binding.OperationBinding;

import org.apache.myfaces.trinidad.event.DisclosureEvent;

import terms.fin.transaction.model.view.DrCrDetailItemVORowImpl;
import terms.fin.transaction.model.view.DrCrDetailSrvNoVORowImpl;
import terms.fin.transaction.model.view.DrCrNoteDetailVORowImpl;
import terms.fin.transaction.model.view.DrCrNoteHeaderVORowImpl;

public class CreateDrCrNoteBean {
    private RichTable createDrCrTable;
    private RichTable createDrCrSrvTable;
    private RichOutputText bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private String PageMode = "C";
    private RichButton headerEditBinding;
    private RichButton detailcreateBinding;
    private RichButton detailDeleteBinding;
    private RichButton detailcreateSrvBinding;
    private RichButton detailDeleteSrvBinding;
    private RichTable itemDetailsTable;
    private RichInputText bindQty;
    private RichInputText bindRate;
    private RichInputText bindAmt;
    private RichInputText bindDisc;
    private RichInputText bindDiscount;
    private RichInputComboboxListOfValues bindunit;
    private RichInputComboboxListOfValues bindParty;
    private RichInputComboboxListOfValues bindProdCode;
    private RichInputText hsnNoBinding;
    private RichShowDetailItem drCrNoteTabBinding;
    private RichShowDetailItem itemDetailTabBinding;
    private RichButton createItemButtonBinding;
    private RichButton deleteItemButtonBinding;
    private RichInputComboboxListOfValues gstCodeBinding;
    private RichInputText paymentAmtBinding;
    private RichInputComboboxListOfValues srvBinding;
    private RichInputComboboxListOfValues bindSrvNumb;
    private RichSelectOneChoice noteTypeBinding;
    private RichInputDate billDateBinding;
    private RichSelectOneChoice documentTypeBinding;
    private RichInputText dnCnNoBinding;
    private RichInputDate dateBinding;
    private RichInputComboboxListOfValues voucherUnitCode;
    private RichInputText tdsAmtBinding;
    private RichInputText ecessAmtBinding;
    private RichInputText sgstAmtBinding;
    private RichInputText sgstRateBinding;
    private RichInputText cgstRateBinding;
    private RichInputText cgstAmtBinding;
    private RichInputText igstRateBinding;
    private RichInputText igstAmtBinding;
    private RichInputText totalSumBinding;
    private RichInputText netTotalBinding;
    private RichInputDate srvDateBinding;
    private RichInputText challanNoBinding;
    private RichInputText tdsPerBinding;
    private RichInputText tdsSurBinding;
    private RichInputText tdsEcessBinding;
    private RichInputText tdsGlCdBinding;
    private RichInputText ecessGlBinding;
    private RichInputText slcodeTypeBinding;
    private RichPanelGroupLayout itemDetailPgBinding;
    private RichInputComboboxListOfValues glCodeBinding;
    private RichSelectOneChoice drCrFlagBinding;
    private RichInputComboboxListOfValues glcddetBinding;
    private RichInputComboboxListOfValues authByBinding;
    private RichShowDetailItem srvDetailTabBinding;
    private RichShowDetailItem tdsDetailTabBinding;
    private RichInputText drCrIdBinding;
    private RichOutputText bindTotalAmtOt;
    private RichSelectOneChoice gstApplicaBinding;
    private RichInputText diffAmtBinding;
    private RichInputText billNoBinding;
    private RichInputText voucherNoBinding;
    private RichInputText tdsNoBinding;
    private String srv_flag = "Y";


    public CreateDrCrNoteBean() {
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {

            ADFUtils.findOperation("Delete").execute();
            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
            //                       if(op.getErrors().isEmpty())
            //                       {
            //                       FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            //                       Message.setSeverity(FacesMessage.SEVERITY_INFO);
            //                       FacesContext fc = FacesContext.getCurrentInstance();
            //                       fc.addMessage(null, Message);
            //                       }
            //                       else
            //                       {
            //                       FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
            //                       Message.setSeverity(FacesMessage.SEVERITY_ERROR);
            //                       FacesContext fc = FacesContext.getCurrentInstance();
            //                       fc.addMessage(null, Message);
            //                        }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(createDrCrTable);
    }


    public void DeletePopupDialogSrvDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete1").execute();
            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
            //                       if(op.getErrors().isEmpty())
            //                       {
            //                       FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            //                       Message.setSeverity(FacesMessage.SEVERITY_INFO);
            //                       FacesContext fc = FacesContext.getCurrentInstance();
            //                       fc.addMessage(null, Message);
            //                       }
            //                       else
            //                       {
            //                       FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
            //                       Message.setSeverity(FacesMessage.SEVERITY_ERROR);
            //                       FacesContext fc = FacesContext.getCurrentInstance();
            //                       fc.addMessage(null, Message);
            //                        }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(createDrCrSrvTable);

    }

    public void setCreateDrCrTable(RichTable createDrCrTable) {
        this.createDrCrTable = createDrCrTable;
    }

    public RichTable getCreateDrCrTable() {
        return createDrCrTable;
    }

    public void setCreateDrCrSrvTable(RichTable createDrCrSrvTable) {
        this.createDrCrSrvTable = createDrCrSrvTable;
    }

    public RichTable getCreateDrCrSrvTable() {
        return createDrCrSrvTable;
    }

    public String resolvEl(String data) {
        FacesContext fc = FacesContext.getCurrentInstance();
        Application app = fc.getApplication();
        ExpressionFactory elFactory = app.getExpressionFactory();
        ELContext elContext = fc.getELContext();
        ValueExpression valueExp = elFactory.createValueExpression(elContext, data, Object.class);
        String Message = valueExp.getValue(elContext).toString();
        return Message;
    }
    ADFContext context1 = ADFContext.getCurrent();
    Map sessionScope = context1.getCurrent().getSessionScope();

    public void SaveAL(ActionEvent actionEvent) {
        System.out.println("SRVFlaggg" + srv_flag);
        if (srv_flag.equalsIgnoreCase("Y")) {

            //    Integer edit = (Integer) bindingOutputText.getValue();
            BigDecimal diff_amount = new BigDecimal(0);
            String voucher_no = "";
            String Gst = "";
            String App = (String) gstApplicaBinding.getValue() == null ? "" : (String) gstApplicaBinding.getValue();
            System.out.println("Apppp" + App);
            DCIteratorBinding Dcite = ADFUtils.findIterator("DrCrDetailItemVO1Iterator");
            DrCrDetailItemVORowImpl row = (DrCrDetailItemVORowImpl) Dcite.getCurrentRow();
            if ((Long) ADFUtils.evaluateEL("#{bindings.DrCrDetailItemVO1Iterator.estimatedRowCount}") >= 1) {
                if (row.getDgstCode() != null) {
                    Gst = row.getDgstCode();
                }
            }
            System.out.println("Gstttttttssss" + Gst);
            if (App.equalsIgnoreCase("Y") && (Gst == null || Gst.isEmpty())) {
                FacesMessage Message = new FacesMessage("Gst Code is Required in case of Gst Applicable");
                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            }
             else {
                diff_amount =
                    (BigDecimal) (diffAmtBinding.getValue() != null ? diffAmtBinding.getValue() : new BigDecimal(0));
                System.out.println("Diffff" + diff_amount);
                System.out.println("GlCd Detaillll" + glcddetBinding.getValue() != null);
                if (diff_amount.compareTo(new BigDecimal(0)) == 0) {
                    if (PageMode.equalsIgnoreCase("C")) {
                        OperationBinding Opr = ADFUtils.findOperation("generateDnCnNo");
                        Object Obj = Opr.execute();
                        System.out.println("******Result is for dcNo:" + Opr.getResult());
                        try {
                            voucher_no = vouNumberForMessage();
                            System.out.println("VoucherMethoddd");
                        } catch (Exception ee) {
                            ee.printStackTrace();
                        }
                        if (Opr.getResult() != null && Opr.getResult().toString().equalsIgnoreCase("S")) {
                            System.out.println("Get value From Session--->" + sessionScope.get("EmpID")); //888
                            ADFUtils.setEL("#{bindings.PrepBy.inputValue}", sessionScope.get("EmpID"));
                            ADFUtils.findOperation("Commit").execute();
                            ADFUtils.showMessage("Record Saved Successfully.Debit Credit Number is " +
                                                 ADFUtils.evaluateEL("#{bindings.DnCnNo.inputValue}") + "" + voucher_no,
                                                 2);
                            if (authByBinding.getValue() != null) {
                                ADFUtils.setEL("#{pageFlowScope.mode}", "V");
                                cevmodecheck();
                                AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
                            }
                        } else if (Opr.getResult() != null && Opr.getResult().toString().equalsIgnoreCase("U")) {
                            System.out.println("Voucher" + voucher_no);
                            ADFUtils.findOperation("Commit").execute();
                            ADFUtils.showMessage("Record Updated Successfully." + voucher_no, 2);
                            if (authByBinding.getValue() != null) {
                                ADFUtils.setEL("#{pageFlowScope.mode}", "V");
                                cevmodecheck();
                                AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
                            }
                        } else if (Opr.getResult() != null && Opr.getResult().toString().equalsIgnoreCase("VR")) {
                            ADFUtils.showMessage("Problem In Generate Voucher No.", 0);
                        }
                    }
                    if (PageMode.equalsIgnoreCase("E")) {
                        //          if(authByBinding.getValue()!=null)
                        //          {
                        OperationBinding Opr = ADFUtils.findOperation("generateDnCnNo");
                        Object Obj = Opr.execute();
                        System.out.println("******Result is:" + Opr.getResult());
                        try {
                            voucher_no = vouNumberForMessage();
                            System.out.println("vouvcherrrr" + voucher_no);
                        } catch (Exception ee) {
                            ee.printStackTrace();
                        }

                        if (Opr.getResult() != null && Opr.getResult().toString().equalsIgnoreCase("S")) {
                            ADFUtils.findOperation("Commit").execute();
                            System.out.println("voucherif" + voucher_no);
                            ADFUtils.showMessage("Record Saved Successfully.New Number is " +ADFUtils.evaluateEL("#{bindings.DnCnNo.inputValue}") + "" + voucher_no,2);
                        } else if (Opr.getResult() != null && Opr.getResult().toString().equalsIgnoreCase("U")) {
                            System.out.println("resultSVoucherrr" + voucher_no);
                            ADFUtils.findOperation("Commit").execute();
                            ADFUtils.showMessage("Record Updated Successfully." + voucher_no, 2);
                            if (authByBinding.getValue() != null) {
                                ADFUtils.setEL("#{pageFlowScope.mode}", "V");
                                cevmodecheck();
                                AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
                            }
                        }

                        else if (Opr.getResult() != null && Opr.getResult().toString().equalsIgnoreCase("VR")) {
                            ADFUtils.showMessage("Problem In Generate Voucher No.", 2);

                            //              }
                        }
                    }
                }


                else {
                    FacesMessage Message = new FacesMessage("Total Amount must be equal to netAmount");
                    Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);

                }
            }
        } else {
            ADFUtils.showMessage("You have change the srv No, Please click on populate item detail button.", 0);
        }
    }


    public String vouNumberForMessage() {
        String tds_voucher_no = "";
        String bill_voucher_no = "";
        String message = "";
        try {
            tds_voucher_no = (String) ADFUtils.evaluateEL("#{bindings.FinTvouchTdsJv.inputValue}");
            System.out.println("tdsin methoddd" + tds_voucher_no);
            bill_voucher_no = (String) ADFUtils.evaluateEL("#{bindings.FinTvouchVouNo.inputValue}");
            System.out.println("Voucher i method" + bill_voucher_no);


            if (bill_voucher_no != null) {
                message = ".Dr Cr Voucher No. is " + bill_voucher_no;
            }
            if (tds_voucher_no != null) {
                message = message + ".And TDS Voucher No is " + tds_voucher_no;
                System.out.println("Message is :" + message);
            }

        } catch (Exception ee) {
            ee.printStackTrace();
        }
        return message;

    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void EditBttonActionAL(ActionEvent actionEvent) {

        if (authByBinding.getValue() != null) {
            ADFUtils.showMessage("Debit Credit Note Has Been Authorised,So you can not modify it.", 2);
            ADFUtils.setEL("#{pageFlowScope.mode}", "V");
        } else {
            cevmodecheck();
        }
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setDetailcreateBinding(RichButton detailcreateBinding) {
        this.detailcreateBinding = detailcreateBinding;
    }

    public RichButton getDetailcreateBinding() {
        return detailcreateBinding;
    }

    public void setDetailDeleteBinding(RichButton detailDeleteBinding) {
        this.detailDeleteBinding = detailDeleteBinding;
    }

    public RichButton getDetailDeleteBinding() {
        return detailDeleteBinding;
    }

    public void setDetailcreateSrvBinding(RichButton detailcreateSrvBinding) {
        this.detailcreateSrvBinding = detailcreateSrvBinding;
    }

    public RichButton getDetailcreateSrvBinding() {
        return detailcreateSrvBinding;
    }

    public void setDetailDeleteSrvBinding(RichButton detailDeleteSrvBinding) {
        this.detailDeleteSrvBinding = detailDeleteSrvBinding;
    }

    public RichButton getDetailDeleteSrvBinding() {
        return detailDeleteSrvBinding;
    }

    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }

    public void cevModeDisableComponent(String mode) {
        if (mode.equalsIgnoreCase("E")) {
            PageMode = "E";

            getHsnNoBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            getDetailcreateBinding().setDisabled(false);
            getCreateItemButtonBinding().setDisabled(false);
            getNoteTypeBinding().setDisabled(true);
            getBillDateBinding().setDisabled(true);
            getBillNoBinding().setDisabled(true);
            getBindunit().setDisabled(true);
            getDocumentTypeBinding().setDisabled(true);
            getDnCnNoBinding().setDisabled(true);
            getDateBinding().setDisabled(true);
            getVoucherUnitCode().setDisabled(true);
            getTdsAmtBinding().setDisabled(true);
            getEcessAmtBinding().setDisabled(true);
            getBindAmt().setDisabled(true);
            getBindDiscount().setDisabled(true);
            getSgstAmtBinding().setDisabled(true);
            getSgstRateBinding().setDisabled(true);
            getIgstAmtBinding().setDisabled(true);
            getIgstRateBinding().setDisabled(true);
            getCgstAmtBinding().setDisabled(true);
            getCgstRateBinding().setDisabled(true);
            getTotalSumBinding().setDisabled(true);
            getNetTotalBinding().setDisabled(true);
            getSrvDateBinding().setDisabled(true);
            getChallanNoBinding().setDisabled(true);
            getTdsPerBinding().setDisabled(true);
            getTdsSurBinding().setDisabled(true);
            getTdsGlCdBinding().setDisabled(true);
            getTdsEcessBinding().setDisabled(true);
            getEcessGlBinding().setDisabled(true);
            getSlcodeTypeBinding().setDisabled(true);
            getGlCodeBinding().setDisabled(true);
            getVoucherNoBinding().setDisabled(true);
            getTdsNoBinding().setDisabled(true);

        } else if (mode.equalsIgnoreCase("C")) {
            PageMode = "C";
            getHsnNoBinding().setDisabled(true);
            getDetailcreateBinding().setDisabled(false);
            getBindunit().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            getDnCnNoBinding().setDisabled(true);
            getVoucherUnitCode().setDisabled(true);
            getTdsAmtBinding().setDisabled(true);
            getEcessAmtBinding().setDisabled(true);
            getBindAmt().setDisabled(true);
            getBindDiscount().setDisabled(true);
            getSgstAmtBinding().setDisabled(true);
            getSgstRateBinding().setDisabled(true);
            getIgstAmtBinding().setDisabled(true);
            getIgstRateBinding().setDisabled(true);
            getCgstAmtBinding().setDisabled(true);
            getCgstRateBinding().setDisabled(true);
            getTotalSumBinding().setDisabled(true);
            getNetTotalBinding().setDisabled(true);
            getSrvDateBinding().setDisabled(true);
            getChallanNoBinding().setDisabled(true);
            getTdsPerBinding().setDisabled(true);
            getTdsSurBinding().setDisabled(true);
            getTdsGlCdBinding().setDisabled(true);
            getTdsEcessBinding().setDisabled(true);
            getEcessGlBinding().setDisabled(true);
            getSlcodeTypeBinding().setDisabled(true);
            getGlCodeBinding().setDisabled(true);
            getVoucherNoBinding().setDisabled(true);
            getTdsNoBinding().setDisabled(true);

        } else if (mode.equalsIgnoreCase("V")) {
            PageMode = "V";
            getSrvDetailTabBinding().setDisabled(false);
            getDrCrNoteTabBinding().setDisabled(false);
            getItemDetailTabBinding().setDisabled(false);
            getTdsDetailTabBinding().setDisabled(false);
            getDetailcreateBinding().setDisabled(true);
            getDetailcreateSrvBinding().setDisabled(true);
            getCreateItemButtonBinding().setDisabled(true);

        }
    }

    public void DeletePopupDialogItemDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete2").execute();
            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
            //               if(op.getErrors().isEmpty())
            //               {
            //               FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            //               Message.setSeverity(FacesMessage.SEVERITY_INFO);
            //               FacesContext fc = FacesContext.getCurrentInstance();
            //               fc.addMessage(null, Message);
            //               }
            //               else
            //               {
            //               FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
            //               Message.setSeverity(FacesMessage.SEVERITY_ERROR);
            //               FacesContext fc = FacesContext.getCurrentInstance();
            //               fc.addMessage(null, Message);
            //                }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(itemDetailsTable);

    }

    public void setItemDetailsTable(RichTable itemDetailsTable) {
        this.itemDetailsTable = itemDetailsTable;
    }

    public RichTable getItemDetailsTable() {
        return itemDetailsTable;
    }

    public void CalculateAmt(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());

        if (vce != null) {
            BigDecimal Qty =
                (BigDecimal) ((BigDecimal) bindQty.getValue() == null ? new BigDecimal(0) : bindQty.getValue());
            System.out.println("Quantity in bean----" + Qty);
            BigDecimal Rate = (BigDecimal) (bindRate.getValue() == null ? new BigDecimal(0) : bindRate.getValue());
            System.out.println("Rate in bean---" + Rate);
            BigDecimal Amt = (BigDecimal) bindAmt.getValue();
            if (Qty != null && Rate != null) {
                Amt = Qty.multiply(Rate);
                System.out.println("Amt after Multiplication" + Amt);
                bindAmt.setValue(Amt);
            }
            if (Amt != null) {
                BigDecimal Disc = (BigDecimal) (bindDisc.getValue() == null ? new BigDecimal(0) : bindDisc.getValue());
                Amt = (BigDecimal) (bindAmt.getValue() == null ? new BigDecimal(0) : bindAmt.getValue());
                BigDecimal Discount = new BigDecimal(0);
                BigDecimal BB = new BigDecimal(100);
                if (Disc != null && Amt != null) {
                    BigDecimal Ab = Disc.multiply(Amt);
                    Discount = Ab.divide(BB);
                    BigDecimal RounDiscount = Discount.setScale(2, RoundingMode.HALF_UP);
                    bindDiscount.setValue(RounDiscount);

                }
            }
            vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
            String Unit = bindunit.getValue().toString();
            System.out.println("bean Unit" + Unit);
            String vencd = bindParty.getValue().toString();
            System.out.println("bean Vencd" + vencd);
            OperationBinding ob = ADFUtils.findOperation("RefreshAmount");
            ob.getParamsMap().put("Unit", Unit);
            ob.getParamsMap().put("vencd", vencd);
            ob.execute();
            OperationBinding ob1 = ADFUtils.findOperation("CalculateTotalSum");
            ob1.execute();
            AdfFacesContext.getCurrentInstance().addPartialTarget(bindTotalAmtOt);
        }

    }

    public void setBindQty(RichInputText bindQty) {
        this.bindQty = bindQty;
    }

    public RichInputText getBindQty() {
        return bindQty;
    }

    public void setBindRate(RichInputText bindRate) {
        this.bindRate = bindRate;
    }

    public RichInputText getBindRate() {
        return bindRate;
    }

    public void setBindAmt(RichInputText bindAmt) {
        this.bindAmt = bindAmt;
    }

    public RichInputText getBindAmt() {
        return bindAmt;
    }

    public void setBindDisc(RichInputText bindDisc) {
        this.bindDisc = bindDisc;
    }

    public RichInputText getBindDisc() {
        return bindDisc;
    }

    public void calculateDiscount(ValueChangeEvent vce) {
        if (vce != null) {
            vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
            BigDecimal Disc = (BigDecimal) (bindDisc.getValue() == null ? new BigDecimal(0) : bindDisc.getValue());
            BigDecimal Amt = (BigDecimal) (bindAmt.getValue() == null ? new BigDecimal(0) : bindAmt.getValue());
            BigDecimal Discount = new BigDecimal(0);
            BigDecimal BB = new BigDecimal(100);
            if (Disc != null && Amt != null) {
                BigDecimal Ab = Disc.multiply(Amt);
                Discount = Ab.divide(BB);
                BigDecimal RounDiscount = Discount.setScale(2, RoundingMode.HALF_UP);
                bindDiscount.setValue(RounDiscount);
            }
            String Unit = bindunit.getValue().toString();
            System.out.println("bean Unit" + Unit);
            String vencd = bindParty.getValue().toString();
            System.out.println("bean Vencd" + vencd);
            OperationBinding ob = ADFUtils.findOperation("RefreshAmount");
            ob.getParamsMap().put("Unit", Unit);
            ob.getParamsMap().put("vencd", vencd);
            ob.execute();
            OperationBinding ob1 = ADFUtils.findOperation("CalculateTotalSum");
            ob1.execute();
            //            AdfFacesContext.getCurrentInstance().addPartialTarget(itemDetailsTable);
        }

    }

    public void setBindDiscount(RichInputText bindDiscount) {
        this.bindDiscount = bindDiscount;
    }

    public RichInputText getBindDiscount() {
        return bindDiscount;
    }

    public void CalculateGsts(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        String Unit = bindunit.getValue().toString();
        System.out.println("bean Unit" + Unit);
        String vencd = bindParty.getValue().toString();
        System.out.println("bean Vencd" + vencd);
        OperationBinding ob = ADFUtils.findOperation("RefreshAmount");
        ob.getParamsMap().put("Unit", Unit);
        ob.getParamsMap().put("vencd", vencd);
        ob.execute();

        OperationBinding ob1 = ADFUtils.findOperation("CalculateTotalSum");
        ob1.execute();
        //        AdfFacesContext.getCurrentInstance().addPartialTarget(itemDetailsTable);
    }

    public void setBindunit(RichInputComboboxListOfValues bindunit) {
        this.bindunit = bindunit;
    }

    public RichInputComboboxListOfValues getBindunit() {
        return bindunit;
    }

    public void setBindParty(RichInputComboboxListOfValues bindParty) {
        this.bindParty = bindParty;
    }

    public RichInputComboboxListOfValues getBindParty() {
        return bindParty;
    }


    public void GstFromItem(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        //            AdfFacesContext.getCurrentInstance().addPartialTarget(detailSaleOrderTableBinding);
        if (bindProdCode.getValue() != null) {
            String ProdCode = bindProdCode.getValue().toString();
            OperationBinding opp = ADFUtils.findOperation("gethsnno");
            opp.getParamsMap().put("ProdCode", ProdCode);
            opp.execute();
            //                AdfFacesContext.getCurrentInstance().addPartialTarget(itemDetailsTable);
        }
        if (hsnNoBinding.getValue() != null) {
            String HsnNo = (String) hsnNoBinding.getValue();
            OperationBinding op = ADFUtils.findOperation("GstCodefromProdcode");

            System.out.println("--------Hsnno------" + HsnNo);
            op.getParamsMap().put("HsnNo", HsnNo);
            op.execute();
            //            AdfFacesContext.getCurrentInstance().addPartialTarget(itemDetailsTable);
        }
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        String Unit = bindunit.getValue().toString();
        System.out.println("bean Unit" + Unit);
        String vencd = bindParty.getValue().toString();
        System.out.println("bean Vencd" + vencd);
        OperationBinding ob = ADFUtils.findOperation("RefreshAmount");
        ob.getParamsMap().put("Unit", Unit);
        ob.getParamsMap().put("vencd", vencd);
        ob.execute();
        //        AdfFacesContext.getCurrentInstance().addPartialTarget(itemDetailsTable);
    }

    public void setBindProdCode(RichInputComboboxListOfValues bindProdCode) {
        this.bindProdCode = bindProdCode;
    }

    public RichInputComboboxListOfValues getBindProdCode() {
        return bindProdCode;
    }

    public void setHsnNoBinding(RichInputText hsnNoBinding) {
        this.hsnNoBinding = hsnNoBinding;
    }

    public RichInputText getHsnNoBinding() {
        return hsnNoBinding;
    }

    public void setDrCrNoteTabBinding(RichShowDetailItem drCrNoteTabBinding) {
        this.drCrNoteTabBinding = drCrNoteTabBinding;
    }

    public RichShowDetailItem getDrCrNoteTabBinding() {
        return drCrNoteTabBinding;
    }

    public void setItemDetailTabBinding(RichShowDetailItem itemDetailTabBinding) {
        this.itemDetailTabBinding = itemDetailTabBinding;
    }

    public RichShowDetailItem getItemDetailTabBinding() {
        return itemDetailTabBinding;
    }

    public void setCreateItemButtonBinding(RichButton createItemButtonBinding) {
        this.createItemButtonBinding = createItemButtonBinding;
    }

    public RichButton getCreateItemButtonBinding() {
        return createItemButtonBinding;
    }

    public void setDeleteItemButtonBinding(RichButton deleteItemButtonBinding) {
        this.deleteItemButtonBinding = deleteItemButtonBinding;
    }

    public RichButton getDeleteItemButtonBinding() {
        return deleteItemButtonBinding;
    }

    public void getSrvToItemDetailAL(DisclosureEvent disclosureEvent) {
        if (disclosureEvent.isExpanded()) {
            getGstApplicaBinding().setDisabled(true);
            getNoteTypeBinding().setDisabled(true);
            getBindParty().setDisabled(true);
            //
            //            DCIteratorBinding Dc=ADFUtils.findIterator("DrCrDetailSrvNoVO1Iterator");
            //        DrCrDetailSrvNoVORowImpl row=(DrCrDetailSrvNoVORowImpl) Dc.getCurrentRow();
            //
            //                if((Long)ADFUtils.evaluateEL("#{bindings.DrCrDetailSrvNoVO1Iterator.estimatedRowCount}")>=1 )
            //                {
            //                String Srv = row.getSrvNo();
            //                System.out.println("SRVV"+row.getSrvNo());
            //                System.out.println("26732356754"+Srv);
            //                    if(Srv!=null)
            //                    {
            //                    System.out.println("innnnnnn");
            //                    OperationBinding op=ADFUtils.findOperation("srvToItem");
            //       op.execute();
            //                AdfFacesContext.getCurrentInstance().addPartialTarget(itemDetailsTable);
            //            }
            //                }
        }
    }

    public void ItemTabAL(DisclosureEvent disclosureEvent) {
        Integer Seq = 0;
        if (disclosureEvent.isExpanded()) {
            getGstApplicaBinding().setDisabled(true);
            getNoteTypeBinding().setDisabled(true);
            getBindParty().setDisabled(true);
            System.out.println("pagemode inpopulate itemDetail" + PageMode);
            if (PageMode.equalsIgnoreCase("C") || PageMode.equalsIgnoreCase("E")) {
                DCIteratorBinding Dcite = ADFUtils.findIterator("DrCrDetailItemVO1Iterator");
                DrCrDetailItemVORowImpl row = (DrCrDetailItemVORowImpl) Dcite.getCurrentRow();
                if ((Long) ADFUtils.evaluateEL("#{bindings.DrCrDetailItemVO1Iterator.estimatedRowCount}") >= 1) {
                    if (row.getRate() != null) {
                        OperationBinding ob1 = ADFUtils.findOperation("populateDetailFormItem");
                        ob1.execute();
                        if (ob1.getResult().toString().equalsIgnoreCase("Y")) {
                            FacesMessage Message = new FacesMessage("Debit Credit Detail is populated.");
                            Message.setSeverity(FacesMessage.SEVERITY_INFO);
                            FacesContext fc = FacesContext.getCurrentInstance();
                            fc.addMessage(null, Message);

                        } else {
                            ADFUtils.showMessage("Data Not Available in GST_GL_MAPPING for this Entry.", 0);
                        }
                        OperationBinding op1 = (OperationBinding) ADFUtils.findOperation("generatedrcrid").execute();

                    }
                }
                DCIteratorBinding Dc = ADFUtils.findIterator("DrCrNoteDetailVO1Iterator");
                DrCrNoteDetailVORowImpl row1 = (DrCrNoteDetailVORowImpl) Dc.getCurrentRow();

                if ((Long) ADFUtils.evaluateEL("#{bindings.DrCrNoteDetailVO1Iterator.estimatedRowCount}") >= 1 &&
                    row1.getTotalAmount() != null) {
                    OperationBinding ob1 = ADFUtils.findOperation("TdsCalculation");
                    ob1.execute();
                }
                AdfFacesContext.getCurrentInstance().addPartialTarget(createDrCrTable);
            }
        }

    }

    public void setGstCodeBinding(RichInputComboboxListOfValues gstCodeBinding) {
        this.gstCodeBinding = gstCodeBinding;
    }

    public RichInputComboboxListOfValues getGstCodeBinding() {
        return gstCodeBinding;
    }

    public void setPaymentAmtBinding(RichInputText paymentAmtBinding) {
        this.paymentAmtBinding = paymentAmtBinding;
    }

    public RichInputText getPaymentAmtBinding() {
        return paymentAmtBinding;
    }


    public void setBindSrvNumb(RichInputComboboxListOfValues bindSrvNumb) {
        this.bindSrvNumb = bindSrvNumb;
    }

    public RichInputComboboxListOfValues getBindSrvNumb() {
        return bindSrvNumb;
    }

    public void setNoteTypeBinding(RichSelectOneChoice noteTypeBinding) {
        this.noteTypeBinding = noteTypeBinding;
    }

    public RichSelectOneChoice getNoteTypeBinding() {
        return noteTypeBinding;
    }

    public void setBillDateBinding(RichInputDate billDateBinding) {
        this.billDateBinding = billDateBinding;
    }

    public RichInputDate getBillDateBinding() {
        return billDateBinding;
    }

    public void setDocumentTypeBinding(RichSelectOneChoice documentTypeBinding) {
        this.documentTypeBinding = documentTypeBinding;
    }

    public RichSelectOneChoice getDocumentTypeBinding() {
        return documentTypeBinding;
    }

    public void setDnCnNoBinding(RichInputText dnCnNoBinding) {
        this.dnCnNoBinding = dnCnNoBinding;
    }

    public RichInputText getDnCnNoBinding() {
        return dnCnNoBinding;
    }

    public void setDateBinding(RichInputDate dateBinding) {
        this.dateBinding = dateBinding;
    }

    public RichInputDate getDateBinding() {
        return dateBinding;
    }

    public void setVoucherUnitCode(RichInputComboboxListOfValues voucherUnitCode) {
        this.voucherUnitCode = voucherUnitCode;
    }

    public RichInputComboboxListOfValues getVoucherUnitCode() {
        return voucherUnitCode;
    }

    public void setTdsAmtBinding(RichInputText tdsAmtBinding) {
        this.tdsAmtBinding = tdsAmtBinding;
    }

    public RichInputText getTdsAmtBinding() {
        return tdsAmtBinding;
    }

    public void setEcessAmtBinding(RichInputText ecessAmtBinding) {
        this.ecessAmtBinding = ecessAmtBinding;
    }

    public RichInputText getEcessAmtBinding() {
        return ecessAmtBinding;
    }

    public void setSgstAmtBinding(RichInputText sgstAmtBinding) {
        this.sgstAmtBinding = sgstAmtBinding;
    }

    public RichInputText getSgstAmtBinding() {
        return sgstAmtBinding;
    }

    public void setSgstRateBinding(RichInputText sgstRateBinding) {
        this.sgstRateBinding = sgstRateBinding;
    }

    public RichInputText getSgstRateBinding() {
        return sgstRateBinding;
    }

    public void setCgstRateBinding(RichInputText cgstRateBinding) {
        this.cgstRateBinding = cgstRateBinding;
    }

    public RichInputText getCgstRateBinding() {
        return cgstRateBinding;
    }

    public void setCgstAmtBinding(RichInputText cgstAmtBinding) {
        this.cgstAmtBinding = cgstAmtBinding;
    }

    public RichInputText getCgstAmtBinding() {
        return cgstAmtBinding;
    }

    public void setIgstRateBinding(RichInputText igstRateBinding) {
        this.igstRateBinding = igstRateBinding;
    }

    public RichInputText getIgstRateBinding() {
        return igstRateBinding;
    }

    public void setIgstAmtBinding(RichInputText igstAmtBinding) {
        this.igstAmtBinding = igstAmtBinding;
    }

    public RichInputText getIgstAmtBinding() {
        return igstAmtBinding;
    }

    public void setTotalSumBinding(RichInputText totalSumBinding) {
        this.totalSumBinding = totalSumBinding;
    }

    public RichInputText getTotalSumBinding() {
        return totalSumBinding;
    }

    public void setNetTotalBinding(RichInputText netTotalBinding) {
        this.netTotalBinding = netTotalBinding;
    }

    public RichInputText getNetTotalBinding() {
        return netTotalBinding;
    }

    public void setSrvDateBinding(RichInputDate srvDateBinding) {
        this.srvDateBinding = srvDateBinding;
    }

    public RichInputDate getSrvDateBinding() {
        return srvDateBinding;
    }

    public void setChallanNoBinding(RichInputText challanNoBinding) {
        this.challanNoBinding = challanNoBinding;
    }

    public RichInputText getChallanNoBinding() {
        return challanNoBinding;
    }

    public void setTdsPerBinding(RichInputText tdsPerBinding) {
        this.tdsPerBinding = tdsPerBinding;
    }

    public RichInputText getTdsPerBinding() {
        return tdsPerBinding;
    }

    public void setTdsSurBinding(RichInputText tdsSurBinding) {
        this.tdsSurBinding = tdsSurBinding;
    }

    public RichInputText getTdsSurBinding() {
        return tdsSurBinding;
    }

    public void setTdsEcessBinding(RichInputText tdsEcessBinding) {
        this.tdsEcessBinding = tdsEcessBinding;
    }

    public RichInputText getTdsEcessBinding() {
        return tdsEcessBinding;
    }

    public void setTdsGlCdBinding(RichInputText tdsGlCdBinding) {
        this.tdsGlCdBinding = tdsGlCdBinding;
    }

    public RichInputText getTdsGlCdBinding() {
        return tdsGlCdBinding;
    }

    public void setEcessGlBinding(RichInputText ecessGlBinding) {
        this.ecessGlBinding = ecessGlBinding;
    }

    public RichInputText getEcessGlBinding() {
        return ecessGlBinding;
    }

    public void setSlcodeTypeBinding(RichInputText slcodeTypeBinding) {
        this.slcodeTypeBinding = slcodeTypeBinding;
    }

    public RichInputText getSlcodeTypeBinding() {
        return slcodeTypeBinding;
    }

    public void CreateItemAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert2").execute();
        AdfFacesContext.getCurrentInstance().addPartialTarget(itemDetailPgBinding);
    }

    public void setItemDetailPgBinding(RichPanelGroupLayout itemDetailPgBinding) {
        this.itemDetailPgBinding = itemDetailPgBinding;
    }

    public RichPanelGroupLayout getItemDetailPgBinding() {
        return itemDetailPgBinding;
    }

    public void setGlCodeBinding(RichInputComboboxListOfValues glCodeBinding) {
        this.glCodeBinding = glCodeBinding;
    }

    public RichInputComboboxListOfValues getGlCodeBinding() {
        return glCodeBinding;
    }

    public void currcodevce(ValueChangeEvent vce) {
        if (vce != null) {
            vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
            OperationBinding op = ADFUtils.findOperation("curreRate");
            Object rst = op.execute();

        }
    }

    public void CreateDetailAL(ActionEvent actionEvent) {
        OperationBinding op = (OperationBinding) ADFUtils.findOperation("CreateInsert").execute();
        getGstApplicaBinding().setDisabled(true);
        getNoteTypeBinding().setDisabled(true);
        getBindParty().setDisabled(true);

        OperationBinding op1 = (OperationBinding) ADFUtils.findOperation("generatedrcrid").execute();


        //     AdfFacesContext.getCurrentInstance().addPartialTarget(glcddetBinding);
        //        String c="C";
        //        String D="D";
        //       if(noteTypeBinding.getValue().toString().equalsIgnoreCase("D"))
        //       {
        //           drCrFlagBinding.setValue(c);
        //           }
        //       else
        //       drCrFlagBinding.setValue(D);
    }


    public void setDrCrFlagBinding(RichSelectOneChoice drCrFlagBinding) {
        this.drCrFlagBinding = drCrFlagBinding;
    }

    public RichSelectOneChoice getDrCrFlagBinding() {
        return drCrFlagBinding;
    }

    public void setGlcddetBinding(RichInputComboboxListOfValues glcddetBinding) {
        this.glcddetBinding = glcddetBinding;
    }

    public RichInputComboboxListOfValues getGlcddetBinding() {
        return glcddetBinding;
    }

    public void setAuthByBinding(RichInputComboboxListOfValues authByBinding) {
        this.authByBinding = authByBinding;
    }

    public RichInputComboboxListOfValues getAuthByBinding() {
        return authByBinding;
    }

    public void CheckAuthority(ReturnPopupEvent returnPopupEvent) {
        OperationBinding op = ADFUtils.findOperation("CheckAuthority");
        op.getParamsMap().put("unitCd", bindunit.getValue());
        op.getParamsMap().put("EmpCd", authByBinding.getValue());
        op.getParamsMap().put("NoteType", noteTypeBinding.getValue());
        op.execute();
    }

    public void SrvToItemVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());

        if (vce.getNewValue() != null) {
            srv_flag = "N";

            //            OperationBinding op=ADFUtils.findOperation("srvToItem");
            //            op.execute();
            //            AdfFacesContext.getCurrentInstance().addPartialTarget(itemDetailsTable);
        }
    }

    public void setSrvDetailTabBinding(RichShowDetailItem srvDetailTabBinding) {
        this.srvDetailTabBinding = srvDetailTabBinding;
    }

    public RichShowDetailItem getSrvDetailTabBinding() {
        return srvDetailTabBinding;
    }

    public void setTdsDetailTabBinding(RichShowDetailItem tdsDetailTabBinding) {
        this.tdsDetailTabBinding = tdsDetailTabBinding;
    }

    public RichShowDetailItem getTdsDetailTabBinding() {
        return tdsDetailTabBinding;
    }

    public void populateSrvToItem(ActionEvent actionEvent) {
        srv_flag = "Y";
        OperationBinding op = ADFUtils.findOperation("srvToItem");
        op.execute();
        if (op.getResult().toString().equalsIgnoreCase("Y")) {
            FacesMessage Message = new FacesMessage("Item Detail is populated.");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }

        String Unit = bindunit.getValue().toString();
        System.out.println("bean Unit" + Unit);
        String vencd = bindParty.getValue().toString();
        System.out.println("bean Vencd" + vencd);
        OperationBinding ob = ADFUtils.findOperation("RefreshAmount");
        ob.getParamsMap().put("Unit", Unit);
        ob.getParamsMap().put("vencd", vencd);
        ob.execute();
        AdfFacesContext.getCurrentInstance().addPartialTarget(itemDetailsTable);
    }


    public void setDrCrIdBinding(RichInputText drCrIdBinding) {
        this.drCrIdBinding = drCrIdBinding;
    }

    public RichInputText getDrCrIdBinding() {
        return drCrIdBinding;
    }

    public void setBindTotalAmtOt(RichOutputText bindTotalAmtOt) {
        this.bindTotalAmtOt = bindTotalAmtOt;
    }

    public RichOutputText getBindTotalAmtOt() {
        return bindTotalAmtOt;
    }

    public void ValidateGlCode(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (object != null) {
            System.out.println("Party-----" + bindParty.getValue());
            System.out.println("GlCode is----" + glCodeBinding.getValue());
            if (bindParty.getValue() != null && glCodeBinding.getValue() == null) {
                System.out.println("Inside ifffff");
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                              "Gl Code is Required.Please Select another Party", null));

            }
        }
    }

    //    public void ValidateGst(FacesContext facesContext, UIComponent uIComponent, Object object) {
    //        if(object!=null)
    //        {
    //            String App = (String) gstApplicaBinding.getValue();
    //            if(App.equalsIgnoreCase("Y")&& gstCodeBinding.getValue()==null && ((Long)ADFUtils.evaluateEL("#{bindings.DrCrNoteDetailVO1Iterator.estimatedRowCount}")>=1 ))
    //            {
    //
    //                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
    //                                                                      "Gst Code is Required in case of Gst Applicable",
    //                                                                      null)) ;               }
    //            }
    //
    //    }

    public void setGstApplicaBinding(RichSelectOneChoice gstApplicaBinding) {
        this.gstApplicaBinding = gstApplicaBinding;
    }

    public RichSelectOneChoice getGstApplicaBinding() {
        return gstApplicaBinding;
    }

    public void SrvDetailTabDL(DisclosureEvent disclosureEvent) {
        if (disclosureEvent.isExpanded()) {
            getGstApplicaBinding().setDisabled(true);
            getNoteTypeBinding().setDisabled(true);
            getBindParty().setDisabled(true);
        }
    }

    public void setDiffAmtBinding(RichInputText diffAmtBinding) {
        this.diffAmtBinding = diffAmtBinding;
    }

    public RichInputText getDiffAmtBinding() {
        return diffAmtBinding;
    }

    public void DrCrFlagVCE(ValueChangeEvent valueChangeEvent) {
        if (valueChangeEvent != null) {
            valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
            AdfFacesContext.getCurrentInstance().addPartialTarget(createDrCrTable);
        }
    }

    public void DrCrNoteDetailAL(ActionEvent actionEvent) {
        DCIteratorBinding Dcite = ADFUtils.findIterator("DrCrDetailItemVO1Iterator");
        DrCrDetailItemVORowImpl row = (DrCrDetailItemVORowImpl) Dcite.getCurrentRow();
        if ((Long) ADFUtils.evaluateEL("#{bindings.DrCrDetailItemVO1Iterator.estimatedRowCount}") >= 1) {
            if (row.getRate() != null) {
                OperationBinding ob1 = ADFUtils.findOperation("populateDetailFormItem");
                ob1.execute();
                if (ob1.getResult().toString().equalsIgnoreCase("Y")) {
                    FacesMessage Message = new FacesMessage("Debit Credit Detail is populated.");
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);

                }
                OperationBinding op1 = (OperationBinding) ADFUtils.findOperation("generatedrcrid").execute();

            }
        }
        DCIteratorBinding Dc = ADFUtils.findIterator("DrCrNoteDetailVO1Iterator");
        DrCrNoteDetailVORowImpl row1 = (DrCrNoteDetailVORowImpl) Dc.getCurrentRow();

        if ((Long) ADFUtils.evaluateEL("#{bindings.DrCrNoteDetailVO1Iterator.estimatedRowCount}") >= 1 &&
            row1.getTotalAmount() != null) {
            OperationBinding ob1 = ADFUtils.findOperation("TdsCalculation");
            ob1.execute();
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(createDrCrTable);
    }

    public void TDSCalculationvce(ValueChangeEvent vce) {
        if (vce != null) {
            vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
            DCIteratorBinding Dc = ADFUtils.findIterator("DrCrNoteDetailVO1Iterator");
            DrCrNoteDetailVORowImpl row1 = (DrCrNoteDetailVORowImpl) Dc.getCurrentRow();
            if ((Long) ADFUtils.evaluateEL("#{bindings.DrCrNoteDetailVO1Iterator.estimatedRowCount}") >= 1 &&
                row1.getTotalAmount() != null) {
                OperationBinding ob1 = ADFUtils.findOperation("TdsCalculation");
                ob1.execute();
            }

            AdfFacesContext.getCurrentInstance().addPartialTarget(createDrCrTable);
        }
    }

    public void drcrflgvce(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());

        if (vce != null) {
            AdfFacesContext.getCurrentInstance().addPartialTarget(createDrCrTable);
        }
    }

    public void setBillNoBinding(RichInputText billNoBinding) {
        this.billNoBinding = billNoBinding;
    }

    public RichInputText getBillNoBinding() {
        return billNoBinding;
    }

    public String SaveAndCloseAC() {
        if (srv_flag.equalsIgnoreCase("Y")) {


            BigDecimal diff_amount = new BigDecimal(0);
            String voucher_no = "";
            String Gst = "";
            String App = (String) gstApplicaBinding.getValue() == null ? "" : (String) gstApplicaBinding.getValue();
            System.out.println("Apppp" + App);
            DCIteratorBinding Dcite = ADFUtils.findIterator("DrCrDetailItemVO1Iterator");
            DrCrDetailItemVORowImpl row = (DrCrDetailItemVORowImpl) Dcite.getCurrentRow();
            if ((Long) ADFUtils.evaluateEL("#{bindings.DrCrDetailItemVO1Iterator.estimatedRowCount}") >= 1) {
                if (row.getDgstCode() != null) {
                    Gst = row.getDgstCode();
                }
            }
            System.out.println("Gstttttttssss" + Gst);


            if (App.equalsIgnoreCase("Y") && (Gst == null || Gst.isEmpty())) {
                FacesMessage Message = new FacesMessage("Gst Code is Required in case of Gst Applicable");
                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
                return null;

            }


            else {
                diff_amount =
                    (BigDecimal) (diffAmtBinding.getValue() != null ? diffAmtBinding.getValue() : new BigDecimal(0));
                System.out.println("Diffff" + diff_amount);
                System.out.println("GlCd Detaillll" + glcddetBinding.getValue() != null);
                if (diff_amount.compareTo(new BigDecimal(0)) == 0) {

                    if (PageMode.equalsIgnoreCase("C")) {

                        OperationBinding Opr = ADFUtils.findOperation("generateDnCnNo");
                        Object Obj = Opr.execute();
                        System.out.println("******Result is:" + Opr.getResult());
                        try {
                            voucher_no = vouNumberForMessage();
                            System.out.println("VoucherMethoddd");
                        } catch (Exception ee) {
                            ee.printStackTrace();
                        }
                        if (Opr.getResult() != null && Opr.getResult().toString().equalsIgnoreCase("S")) {
                            ADFUtils.findOperation("Commit").execute();
                            ADFUtils.showMessage("Record Saved Successfully.Debit Credit Number is " +
                                                 ADFUtils.evaluateEL("#{bindings.DnCnNo.inputValue}") + "" + voucher_no,
                                                 2);
                            if (authByBinding.getValue() != null) {
                                ADFUtils.setEL("#{pageFlowScope.mode}", "V");
                                cevmodecheck();
                                AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
                            }
                            return "SaveAndClose";
                        } else if (Opr.getResult() != null && Opr.getResult().toString().equalsIgnoreCase("U")) {
                            System.out.println("Voucher" + voucher_no);
                            ADFUtils.findOperation("Commit").execute();
                            ADFUtils.showMessage("Record Updated Successfully." + voucher_no, 2);
                            if (authByBinding.getValue() != null) {
                                ADFUtils.setEL("#{pageFlowScope.mode}", "V");
                                cevmodecheck();
                                AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
                            }
                            return "SaveAndClose";
                        } else if (Opr.getResult() != null && Opr.getResult().toString().equalsIgnoreCase("VR")) {
                            ADFUtils.showMessage("Problem In Generate Voucher No.", 0);
                            return null;
                        }
                    }


                    if (PageMode.equalsIgnoreCase("E")) {
                        //          if(authByBinding.getValue()!=null)
                        //          {
                        OperationBinding Opr = ADFUtils.findOperation("generateDnCnNo");
                        Object Obj = Opr.execute();
                        System.out.println("******Result is:" + Opr.getResult());
                        try {
                            voucher_no = vouNumberForMessage();
                            System.out.println("vouvcherrrr" + voucher_no);
                        } catch (Exception ee) {
                            ee.printStackTrace();
                        }

                        if (Opr.getResult() != null && Opr.getResult().toString().equalsIgnoreCase("S")) {
                            ADFUtils.findOperation("Commit").execute();
                            System.out.println("voucherif" + voucher_no);
                            ADFUtils.showMessage("Record Saved Successfully.New Number is " +
                                                 ADFUtils.evaluateEL("#{bindings.DnCnNo.inputValue}") + "" + voucher_no,
                                                 2);
                            return "SaveAndClose";
                        } else if (Opr.getResult() != null && Opr.getResult().toString().equalsIgnoreCase("U")) {
                            System.out.println("resultSVoucherrr" + voucher_no);
                            ADFUtils.findOperation("Commit").execute();
                            ADFUtils.showMessage("Record Updated Successfully." + voucher_no, 2);
                            if (authByBinding.getValue() != null) {
                                ADFUtils.setEL("#{pageFlowScope.mode}", "V");
                                cevmodecheck();
                                AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
                            }
                            return "SaveAndClose";
                        }

                        else if (Opr.getResult() != null && Opr.getResult().toString().equalsIgnoreCase("VR")) {
                            ADFUtils.showMessage("Problem In Generate Voucher No.", 2);
                            return null;
                            //              }
                        }
                    }
                }


                else {
                    FacesMessage Message = new FacesMessage("Total Amount must be equal to netAmount");
                    Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);
                    return null;
                }
            }

            return "SaveAndClose";
        } else {
            ADFUtils.showMessage("You have change the SRV No, Please click on populate item detail button.", 0);
            return null;

        }
    }

    public void setVoucherNoBinding(RichInputText voucherNoBinding) {
        this.voucherNoBinding = voucherNoBinding;
    }

    public RichInputText getVoucherNoBinding() {
        return voucherNoBinding;
    }

    public void setTdsNoBinding(RichInputText tdsNoBinding) {
        this.tdsNoBinding = tdsNoBinding;
    }

    public RichInputText getTdsNoBinding() {
        return tdsNoBinding;
    }
}
