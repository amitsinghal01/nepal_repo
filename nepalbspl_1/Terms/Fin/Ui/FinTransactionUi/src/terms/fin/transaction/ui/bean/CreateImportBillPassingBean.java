package terms.fin.transaction.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import javax.faces.validator.ValidatorException;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.domain.Date;

import org.apache.myfaces.trinidad.event.DisclosureEvent;

public class CreateImportBillPassingBean {
    private RichOutputText customDutyOutputBinding;
    private RichOutputText costPerUnitOutputBinding;
    private RichOutputText netPerOutputBinding;
    private RichOutputText purValOutputBinding;
    private RichOutputText cessOutputBinding;
    private RichOutputText ecessOutputBinding;
    private RichOutputText scessOutputBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichOutputText bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichShowDetailItem itemDetailTabBinding;
    private RichShowDetailItem srvTabBinding;
    private RichShowDetailItem importBillSummaryBinding;
    private RichShowDetailItem importPBTabBinding;
    private RichInputText billNoBinding;
    private RichInputDate billDateBinding;
    private RichInputDate srvDateBinding;
    private RichInputText srvPoNoBinding;
    private RichInputText srvAmdNoBinding;
    private RichInputText itemPoNoBinding;
    private RichInputText itemAmdNoBinding;
    private RichInputText itemDetailBinding;
    private RichInputText quantityBinding;
    private RichInputText conversionRateBinding;
    private RichInputText billValueFCBinding;
    private RichInputText billValueInrBinding;
    private RichInputText customDutyAmtBinding;
    private RichInputText cessOnBcdBinding;
    private RichInputText scessOnBcdBinding;
    private RichInputText ecessOnBcdBinding;
    private RichInputText totDutyBinding;
    private RichInputText igstPerBinding;
    private RichInputText igstAmtBinding;
    private RichInputText freightBinding;
    private RichInputText insuranceBinding;
    private RichInputText clearForwardBinding;
    private RichInputComboboxListOfValues assentNoBinding;
    private RichInputComboboxListOfValues assetNoBinding;
    private RichInputText totalPurValueBinding;
    private RichInputText netPurValueBinding;
    private RichInputText costPerUnitBinding;
    private RichInputText vouNoBinding;
    private RichInputText subCodeBinding;
    private RichInputText glCodeBinding;
    private RichTable itemDetailTableBinding;
    private RichTable srvTableBinding;
    private RichSelectOneChoice drCrFlagBinding;
    private RichButton populateButtonBinding;
    private String populateFlag="N";
    private Date currentDate=null;
    private RichInputComboboxListOfValues approvedByBinding;
    private RichInputDate approvedOnBinding;
    private String pageMode="C";
    private String tabFlag="N";
    private RichInputComboboxListOfValues srvNoSrvBinding;
    private RichInputText rateBinding;
    private RichInputText assValBinding;
    private RichInputComboboxListOfValues purGlCodeBinding;
    private RichInputText customDutyBinding;
    private RichInputText cessPerBinding;
    private RichInputText scessPerBinding;
    private RichInputText ecessPerBinding;
    private RichInputComboboxListOfValues gstCodeBinding;
    private RichInputComboboxListOfValues partyCodeBinding;
    private RichInputText invoiceNoBinding;
    private RichInputDate invoiceDateBinding;
    private RichInputText beNoBinding;
    private RichInputDate beDateBinding;
    private RichSelectOneChoice currencyCodeBinding;
    private RichInputText convRateHeaderBinding;
    private RichInputText freightHeaderBinding;
    private RichInputText insuranceHeaderBinding;
    private RichInputText loadingForwardingBinding;
    private RichInputText narrationBinding;
    private String makerChecker ="N";
    private RichInputComboboxListOfValues costCenterBinding;
    private RichTable billTableBinding;


    public CreateImportBillPassingBean() {
    }

    public void saveButtonAL(ActionEvent actionEvent) {
        String checkApproved="Y";
        System.out.println("USER NAME IN BEAN:="+ADFUtils.evaluateEL("#{pageFlowScope.userName}"));
        if(populateFlag.equalsIgnoreCase("Y"))
        {
            if(tabFlag.equalsIgnoreCase("Y"))
            {
                
       
       if(approvedByBinding.getValue()==null && pageMode.equalsIgnoreCase("E"))
       {
            checkApproved="N";
       }
       if(makerChecker.equalsIgnoreCase("N") && approvedByBinding.getValue()==null && pageMode.equalsIgnoreCase("C"))
       {
           checkApproved="N";
       }
       
       if(!checkApproved.equalsIgnoreCase("N"))
       {

       OperationBinding opr=ADFUtils.findOperation("generateDocumentNoImportBillPassing");
       opr.getParamsMap().put("userName",ADFUtils.evaluateEL("#{pageFlowScope.userName}"));
       opr.execute();
       System.out.println("Result in Bean:"+opr.getResult());
       if(opr.getResult()!=null && opr.getResult().equals("C"))
       {
        ADFUtils.showMessage("Record Save Successfully.New No. is "+billNoBinding.getValue(), 2);
        ADFUtils.findOperation("Commit").execute();
        }
       else if(opr.getResult()!=null && opr.getResult().equals("U"))
       {
        ADFUtils.showMessage("Record Updated Successfully.", 2);
        ADFUtils.findOperation("Commit").execute();
        }
        else if(opr.getResult()!=null && opr.getResult().equals("E"))
        {
         ADFUtils.showMessage("Problem in Voucher Generation.", 0);
         }
       else
        ADFUtils.showMessage("Problem in Number Generation.", 0);
        
        }
        else
       {
            ADFUtils.showMessage("Please Select Approved By,If Have Permisson To Approved This Record.", 0);
        }
        
        }
        else
        {
            ADFUtils.showMessage("Please Open Import Bill Summmary Tab Before Save Data.", 0);
        }
        }
        else
        {
            ADFUtils.showMessage("Please Click On Populate Item Detail Button in SRV Tab.", 0);
        }
        
    }

    public void populateItemDetailAL(ActionEvent actionEvent) 
    {
        ADFUtils.findOperation("populateDataInDetailAndCalulateImportBillPassing").execute();
        if((Long)ADFUtils.evaluateEL("#{bindings.ImportBillPassingDetailVO1Iterator.estimatedRowCount}")>0)
        {
            populateFlag="Y";
            ADFUtils.showMessage("Data Successfully Populate in Item Detail.", 2);
        }
        
        ADFUtils.findOperation("populateDataSrvToImportBillHeader").execute();
        
    }

    public void basicCustomDutyAmount() 
    {
        Row row=(Row)ADFUtils.evaluateEL("#{bindings.ImportBillPassingDetailVO1Iterator.currentRow}");
        if(row.getAttribute("CustomDuty")!=null)
        {
        BigDecimal zero=new BigDecimal(0);
        BigDecimal custom_duty_amt=zero;
        BigDecimal ass_value=(BigDecimal)(row.getAttribute("AssValue")!=null?row.getAttribute("AssValue"):zero);
        BigDecimal custom_duty=(BigDecimal)(row.getAttribute("CustomDuty")!=null?row.getAttribute("CustomDuty"):zero);
        custom_duty_amt=(ass_value.multiply(custom_duty)).divide(new BigDecimal(100));
        custom_duty_amt=custom_duty_amt.setScale(0, BigDecimal.ROUND_HALF_UP);
        row.setAttribute("CustomDutyAmt",custom_duty_amt);
        
        AdfFacesContext.getCurrentInstance().addPartialTarget(customDutyAmtBinding);
        AdfFacesContext.getCurrentInstance().addPartialTarget(customDutyOutputBinding);
        AdfFacesContext.getCurrentInstance().addPartialTarget(cessOutputBinding);
        }
        
    }




    public void cessBcdPer() 
    {
     
        Row row=(Row)ADFUtils.evaluateEL("#{bindings.ImportBillPassingDetailVO1Iterator.currentRow}");
        if(row.getAttribute("CessBcdPer")!=null)
        {
        BigDecimal zero=new BigDecimal(0);
        BigDecimal cess_bcd_amount=zero;
        BigDecimal cess_bcd_per=(BigDecimal)(row.getAttribute("CessBcdPer")!=null?row.getAttribute("CessBcdPer"):zero);
        BigDecimal custom_duty_amount=(BigDecimal)(row.getAttribute("CustomDutyAmt")!=null?row.getAttribute("CustomDutyAmt"):zero);;
        cess_bcd_amount = (custom_duty_amount.multiply(cess_bcd_per)).divide(new BigDecimal(100));
        cess_bcd_amount=cess_bcd_amount.setScale(0, BigDecimal.ROUND_HALF_UP);
        row.setAttribute("CessBcdAmt",cess_bcd_amount);
        AdfFacesContext.getCurrentInstance().addPartialTarget(cessOutputBinding);
        }

    }
    

    public void ecessBcdPer() 
    {
        Row row=(Row)ADFUtils.evaluateEL("#{bindings.ImportBillPassingDetailVO1Iterator.currentRow}");
        if(row.getAttribute("EcessBcd")!=null)
        {
        BigDecimal zero=new BigDecimal(0);
        BigDecimal ecess_bcd_amount=zero;
        BigDecimal custom_duty_amount=(BigDecimal)(row.getAttribute("CustomDutyAmt")!=null?row.getAttribute("CustomDutyAmt"):zero);
        BigDecimal cess_bcd_amount=(BigDecimal)(row.getAttribute("CessBcdAmt")!=null?row.getAttribute("CessBcdAmt"):zero);
        BigDecimal scess_bcd_amount=(BigDecimal)(row.getAttribute("ScessBcdAmt")!=null?row.getAttribute("ScessBcdAmt"):zero);
        BigDecimal ecess_bcd=(BigDecimal)(row.getAttribute("EcessBcd")!=null?row.getAttribute("EcessBcd"):zero);
        ecess_bcd_amount=(custom_duty_amount.add(cess_bcd_amount).add(scess_bcd_amount)).multiply(ecess_bcd).multiply(new BigDecimal(.01));
        ecess_bcd_amount=ecess_bcd_amount.setScale(0, BigDecimal.ROUND_HALF_UP);
        System.out.println("ECESS BCD AMOUNT IS ecess_bcd_amount:"+ecess_bcd_amount);
        row.setAttribute("EcessBcdAmt",ecess_bcd_amount);
        AdfFacesContext.getCurrentInstance().addPartialTarget(ecessOutputBinding);
        }

    }


    public void scessBcdPer() 
    {
        Row row=(Row)ADFUtils.evaluateEL("#{bindings.ImportBillPassingDetailVO1Iterator.currentRow}");    
        if(row.getAttribute("ScessBcdPer")!=null)
        {
        BigDecimal zero=new BigDecimal(0);
        BigDecimal scess_bcd_amount=zero;
        BigDecimal custom_duty_amount = (BigDecimal)(row.getAttribute("CustomDutyAmt")!=null?row.getAttribute("CustomDutyAmt"):zero);
        BigDecimal scess_bcd_per = (BigDecimal)(row.getAttribute("ScessBcdPer")!=null?row.getAttribute("ScessBcdPer"):zero);
        scess_bcd_amount=(custom_duty_amount.multiply(scess_bcd_per)).divide(new BigDecimal(100));
        scess_bcd_amount = scess_bcd_amount.setScale(0, BigDecimal.ROUND_HALF_UP);
        row.setAttribute("ScessBcdAmt",scess_bcd_amount);
        AdfFacesContext.getCurrentInstance().addPartialTarget(scessOutputBinding);
        }

    }



    public void gstCode()
    {
        Row row=(Row)ADFUtils.evaluateEL("#{bindings.ImportBillPassingDetailVO1Iterator.currentRow}");    
        if(row.getAttribute("Cvd")!=null)
        {
        BigDecimal zero=new BigDecimal(0);
        BigDecimal cvd_amount=zero;
        BigDecimal cvd= (BigDecimal)(row.getAttribute("Cvd")!=null?row.getAttribute("Cvd"):zero);
        BigDecimal ass_value = (BigDecimal)(row.getAttribute("AssValue")!=null?row.getAttribute("AssValue"):zero);
        BigDecimal custom_duty_amount=(BigDecimal)(row.getAttribute("CustomDutyAmt")!=null?row.getAttribute("CustomDutyAmt"):zero);
        BigDecimal cess_bcd_amount=(BigDecimal)(row.getAttribute("CessBcdAmt")!=null?row.getAttribute("CessBcdAmt"):zero);      
        BigDecimal ecess_bcd_amount=(BigDecimal)(row.getAttribute("EcessBcdAmt")!=null?row.getAttribute("EcessBcdAmt"):zero);
        BigDecimal scess_bcd_amount=(BigDecimal)(row.getAttribute("ScessBcdAmt")!=null?row.getAttribute("ScessBcdAmt"):zero);
        BigDecimal total_duty_amount=(BigDecimal)(row.getAttribute("TotDuty")!=null?row.getAttribute("TotDuty"):zero);

//        System.out.println("Ass Value:"+ass_value+"\nCustom Duty Amount:"+custom_duty_amount+"\nCess BCD Amount:"+cess_bcd_amount+"\nEcess BCD AMOUNT:"+ecess_bcd_amount+"\nTotal Duty Amount:="+ecess_bcd_amount);
        cvd_amount=((ass_value.add(total_duty_amount)).multiply(cvd)).divide(new BigDecimal(100));
        System.out.println("CVD Amount Without Round:"+cvd_amount);
        
        cvd_amount = cvd_amount.setScale(0, BigDecimal.ROUND_HALF_UP);
        System.out.println("CVD Amount After Round:"+cvd_amount);

        //row.setAttribute("TotDuty",total_duty_amount);
        row.setAttribute("CvdAmt",cvd_amount);
        
            try
            {
                ADFUtils.findOperation("insertHeadVoucherDataImportBillPassing").execute();
            }
            catch(Exception ee)
            {
                ee.printStackTrace();
            }
        }
    }
    
    
    
    
    
    
    
    
    
    
    public void calculateValues()
    {
        try {
            Row row = (Row) ADFUtils.evaluateEL("#{bindings.ImportBillPassingDetailVO1Iterator.currentRow}");
            BigDecimal zero = new BigDecimal(0);
            BigDecimal total_par_value = new BigDecimal(0);
            BigDecimal net_par_value = new BigDecimal(0);
            BigDecimal cost_par_unit = new BigDecimal(0);

            BigDecimal bill_value_inr =
                (BigDecimal) (row.getAttribute("BillValueInr") != null ? row.getAttribute("BillValueInr") : zero);
            BigDecimal total_duty =
                (BigDecimal) (row.getAttribute("TotDuty") != null ? row.getAttribute("TotDuty") : zero);
            BigDecimal cvd_amount =
                (BigDecimal) (row.getAttribute("CvdAmt") != null ? row.getAttribute("CvdAmt") : zero);
            BigDecimal freight =
                (BigDecimal) (row.getAttribute("Freight") != null ? row.getAttribute("Freight") : zero);
            BigDecimal insurance =
                (BigDecimal) (row.getAttribute("Insurence") != null ? row.getAttribute("Insurence") : zero);
            BigDecimal clear_forwarding =
                (BigDecimal) (row.getAttribute("ClearForward") != null ? row.getAttribute("ClearForward") : zero);
            BigDecimal qty =
                (BigDecimal) (row.getAttribute("Qty") != null ? row.getAttribute("Qty") : new BigDecimal(1));
            total_par_value =
                bill_value_inr.add(total_duty).add(cvd_amount).add(freight).add(insurance).add(clear_forwarding);
            net_par_value = total_par_value.subtract(cvd_amount);
            System.out.println("net_par_value====="+net_par_value+"||qty=============>"+qty);
            try {
                cost_par_unit = net_par_value.divide(qty);
                cost_par_unit = cost_par_unit.setScale(2, BigDecimal.ROUND_HALF_UP);
            } catch (Exception e) {
                // TODO: Add catch code
                e.printStackTrace();
            }
            row.setAttribute("TotPurValue", total_par_value);
            row.setAttribute("NetPurValue", net_par_value);
            row.setAttribute("CostPerUnit", cost_par_unit);
        } catch (Exception e) {
            // TODO: Add catch code
            e.printStackTrace();
        }

    }

    public void setCustomDutyOutputBinding(RichOutputText customDutyOutputBinding) {
        this.customDutyOutputBinding = customDutyOutputBinding;
    }

    public RichOutputText getCustomDutyOutputBinding() {
        return customDutyOutputBinding;
    }

    public void setCostPerUnitOutputBinding(RichOutputText costPerUnitOutputBinding) {
        this.costPerUnitOutputBinding = costPerUnitOutputBinding;
    }

    public RichOutputText getCostPerUnitOutputBinding() {
        return costPerUnitOutputBinding;
    }

    public void setNetPerOutputBinding(RichOutputText netPerOutputBinding) {
        this.netPerOutputBinding = netPerOutputBinding;
    }

    public RichOutputText getNetPerOutputBinding() {
        return netPerOutputBinding;
    }

    public void setPurValOutputBinding(RichOutputText purValOutputBinding) {
        this.purValOutputBinding = purValOutputBinding;
    }

    public RichOutputText getPurValOutputBinding() {
        return purValOutputBinding;
    }

    public void setCessOutputBinding(RichOutputText cessOutputBinding) {
        this.cessOutputBinding = cessOutputBinding;
    }

    public RichOutputText getCessOutputBinding() {
        return cessOutputBinding;
    }

    public void setEcessOutputBinding(RichOutputText ecessOutputBinding) {
        this.ecessOutputBinding = ecessOutputBinding;
    }

    public RichOutputText getEcessOutputBinding() {
        return ecessOutputBinding;
    }

    public void setScessOutputBinding(RichOutputText scessOutputBinding) {
        this.scessOutputBinding = scessOutputBinding;
    }

    public RichOutputText getScessOutputBinding() {
        return scessOutputBinding;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() 
    {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void editButtonAL(ActionEvent actionEvent) 
    {
        if(approvedByBinding.getValue()==null)
        {
        cevmodecheck();
        }
        else
        {
        ADFUtils.showMessage("Approved Record Can't Be Modified.",2);
        ADFUtils.setEL("#{pageFlowScope.mode}","V");
        }
    }
    
    
    private void cevmodecheck(){
           if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
               cevModeDisableComponent("V");
           }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
               cevModeDisableComponent("C");
           }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
               cevModeDisableComponent("E");
           }
       }
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
               try {
                   Method method1 =
                       component.getClass().getMethod("setDisabled", boolean.class);
                   if (method1 != null) {
                       method1.invoke(component, valueComponent);
                   }
               } catch (NoSuchMethodException e) {
                   try {
                       Method method =
                           component.getClass().getMethod("setReadOnly", boolean.class);
                       if (method != null) {
                           method.invoke(component, valueComponent);
                       }
                   } catch (Exception e1) {
                       // e.printStackTrace();//silently eat this exception.
                   }


               } catch (Exception e) {
                   // e.printStackTrace();//silently eat this exception.
               }
               List<UIComponent> childComponents = component.getChildren();
               for (UIComponent comp : childComponents) {
                   makeComponentHierarchyReadOnly(comp, valueComponent);
               }
           }
    
    
    
    //Set Fields and Button disable.
        public void cevModeDisableComponent(String mode) {
            if (mode.equals("E")) {
                    populateFlag="Y";
                    pageMode="E";
                    getHeaderEditBinding().setDisabled(true);
                    getUnitCodeBinding().setDisabled(true);
                    getBillNoBinding().setDisabled(true);
                    getBillDateBinding().setDisabled(true);
                    getGlCodeBinding().setDisabled(true);
                    getPartyCodeBinding().setDisabled(true);
                    getInvoiceNoBinding().setDisabled(true);
                    getInvoiceDateBinding().setDisabled(true);
                    getBeNoBinding().setDisabled(true);
                    getBeDateBinding().setDisabled(true);
                    getConvRateHeaderBinding().setDisabled(true);
                    getFreightHeaderBinding().setDisabled(true);
                    getInsuranceHeaderBinding().setDisabled(true);
                    getLoadingForwardingBinding().setDisabled(true);
                    getCurrencyCodeBinding().setDisabled(true);
                    getCostCenterBinding().setDisabled(true);

                    
                    
                    

                    
                    //SRV TAB
                    getSrvDateBinding().setDisabled(true);
                    getSrvAmdNoBinding().setDisabled(true);
                    getSrvPoNoBinding().setDisabled(true);
                    getSrvNoSrvBinding().setDisabled(true);
    
               
                //Item Detail Tab
                
                getItemPoNoBinding().setDisabled(true);
                getItemAmdNoBinding().setDisabled(true);
                getItemDetailBinding().setDisabled(true);
                getQuantityBinding().setDisabled(true);
//                getConversionRateBinding().setDisabled(true);
//                getBillValueFCBinding().setDisabled(true);
//                getBillValueInrBinding().setDisabled(true);
//                getCustomDutyAmtBinding().setDisabled(true);
//                getCessOnBcdBinding().setDisabled(true);
//                getScessOnBcdBinding().setDisabled(true);
//                getEcessOnBcdBinding().setDisabled(true);
                getIgstPerBinding().setDisabled(true);
//                getIgstAmtBinding().setDisabled(true);
//                getFreightBinding().setDisabled(true);
//                getInsuranceBinding().setDisabled(true);
                getTotalPurValueBinding().setDisabled(true);
                getNetPurValueBinding().setDisabled(true);
                getCostPerUnitBinding().setDisabled(true);
                getTotDutyBinding().setDisabled(true);
//                getClearForwardBinding().setDisabled(true);
                getRateBinding().setDisabled(true);
                getAssValBinding().setDisabled(true);
                getPurGlCodeBinding().setDisabled(true);
             //   getCustomDutyBinding().setDisabled(true);
             //   getCessPerBinding().setDisabled(true);
             //   getScessPerBinding().setDisabled(true);
             //   getEcessPerBinding().setDisabled(true);
                getGstCodeBinding().setDisabled(true);
                getAssetNoBinding().setDisabled(true);
                

                
                
                
                getDrCrFlagBinding().setDisabled(true);
                
                //summary
                getVouNoBinding().setDisabled(true);
                getSubCodeBinding().setDisabled(true);

            } else if (mode.equals("C")) {
                    pageMode="C";
                    getUnitCodeBinding().setDisabled(true);
                    //getBillDateBinding().setDisabled(true);
                    getBillNoBinding().setDisabled(true);
                    getGlCodeBinding().setDisabled(true);
                    
                    //SRV TAB
                    getSrvDateBinding().setDisabled(true);
                    getSrvAmdNoBinding().setDisabled(true);
                    getSrvPoNoBinding().setDisabled(true);
                    
                    
                    
                    //Item Detail Tab
                    
                    getItemPoNoBinding().setDisabled(true);
                    getItemAmdNoBinding().setDisabled(true);
                    getItemDetailBinding().setDisabled(true);
                    getQuantityBinding().setDisabled(true);
//                    getConversionRateBinding().setDisabled(true);
//                    getBillValueFCBinding().setDisabled(true);
//                    getBillValueInrBinding().setDisabled(true);
//                    getCustomDutyAmtBinding().setDisabled(true);
//                    getCessOnBcdBinding().setDisabled(true);
//                    getScessOnBcdBinding().setDisabled(true);
//                    getEcessOnBcdBinding().setDisabled(true);
                    getIgstPerBinding().setDisabled(true);
//                    getIgstAmtBinding().setDisabled(true);
//                    getFreightBinding().setDisabled(true);
//                    getInsuranceBinding().setDisabled(true);
                    getTotalPurValueBinding().setDisabled(true);
                    getNetPurValueBinding().setDisabled(true);
                    getCostPerUnitBinding().setDisabled(true);
              //      getTotDutyBinding().setDisabled(true);
//                    getClearForwardBinding().setDisabled(true);
                    getDrCrFlagBinding().setDisabled(true);

                    //Summary Tab
                    getVouNoBinding().setDisabled(true);
                    getSubCodeBinding().setDisabled(true);
                    getApprovedByBinding().setDisabled(true);
                    getApprovedOnBinding().setDisabled(true);
                

            } else if (mode.equals("V")) 
            {
                populateFlag="Y";
                pageMode="V";
                tabFlag="Y";
                getImportPBTabBinding().setDisabled(false);
                getSrvTabBinding().setDisabled(false);
                getItemDetailTabBinding().setDisabled(false);
                getImportBillSummaryBinding().setDisabled(false);
            }
            
        }


    public void setItemDetailTabBinding(RichShowDetailItem itemDetailTabBinding) {
        this.itemDetailTabBinding = itemDetailTabBinding;
    }

    public RichShowDetailItem getItemDetailTabBinding() {
        return itemDetailTabBinding;
    }

    public void setSrvTabBinding(RichShowDetailItem srvTabBinding) {
        this.srvTabBinding = srvTabBinding;
    }

    public RichShowDetailItem getSrvTabBinding() {
        return srvTabBinding;
    }

    public void setImportBillSummaryBinding(RichShowDetailItem importBillSummaryBinding) {
        this.importBillSummaryBinding = importBillSummaryBinding;
    }

    public RichShowDetailItem getImportBillSummaryBinding() {
        return importBillSummaryBinding;
    }

    public void setImportPBTabBinding(RichShowDetailItem importPBTabBinding) {
        this.importPBTabBinding = importPBTabBinding;
    }

    public RichShowDetailItem getImportPBTabBinding()
    {
        return importPBTabBinding;
    }

    public void setBillNoBinding(RichInputText billNoBinding) {
        this.billNoBinding = billNoBinding;
    }

    public RichInputText getBillNoBinding() {
        return billNoBinding;
    }

    public void setBillDateBinding(RichInputDate billDateBinding) {
        this.billDateBinding = billDateBinding;
    }

    public RichInputDate getBillDateBinding() {
        return billDateBinding;
    }

    public void setSrvDateBinding(RichInputDate srvDateBinding) {
        this.srvDateBinding = srvDateBinding;
    }

    public RichInputDate getSrvDateBinding() {
        return srvDateBinding;
    }

    public void setSrvPoNoBinding(RichInputText srvPoNoBinding) {
        this.srvPoNoBinding = srvPoNoBinding;
    }

    public RichInputText getSrvPoNoBinding() {
        return srvPoNoBinding;
    }

    public void setSrvAmdNoBinding(RichInputText srvAmdNoBinding) {
        this.srvAmdNoBinding = srvAmdNoBinding;
    }

    public RichInputText getSrvAmdNoBinding() {
        return srvAmdNoBinding;
    }

    public void setItemPoNoBinding(RichInputText itemPoNoBinding) {
        this.itemPoNoBinding = itemPoNoBinding;
    }

    public RichInputText getItemPoNoBinding() {
        return itemPoNoBinding;
    }

    public void setItemAmdNoBinding(RichInputText itemAmdNoBinding) {
        this.itemAmdNoBinding = itemAmdNoBinding;
    }

    public RichInputText getItemAmdNoBinding() {
        return itemAmdNoBinding;
    }

    public void setItemDetailBinding(RichInputText itemDetailBinding) {
        this.itemDetailBinding = itemDetailBinding;
    }

    public RichInputText getItemDetailBinding() {
        return itemDetailBinding;
    }

    public void setQuantityBinding(RichInputText quantityBinding) {
        this.quantityBinding = quantityBinding;
    }

    public RichInputText getQuantityBinding() {
        return quantityBinding;
    }

    public void setConversionRateBinding(RichInputText conversionRateBinding) {
        this.conversionRateBinding = conversionRateBinding;
    }

    public RichInputText getConversionRateBinding() {
        return conversionRateBinding;
    }

    public void setBillValueFCBinding(RichInputText billValueFCBinding) {
        this.billValueFCBinding = billValueFCBinding;
    }

    public RichInputText getBillValueFCBinding() {
        return billValueFCBinding;
    }

    public void setBillValueInrBinding(RichInputText billValueInrBinding) {
        this.billValueInrBinding = billValueInrBinding;
    }

    public RichInputText getBillValueInrBinding() {
        return billValueInrBinding;
    }

    public void setCustomDutyAmtBinding(RichInputText customDutyAmtBinding) {
        this.customDutyAmtBinding = customDutyAmtBinding;
    }

    public RichInputText getCustomDutyAmtBinding() {
        return customDutyAmtBinding;
    }

    public void setCessOnBcdBinding(RichInputText cessOnBcdBinding) {
        this.cessOnBcdBinding = cessOnBcdBinding;
    }

    public RichInputText getCessOnBcdBinding() {
        return cessOnBcdBinding;
    }

    public void setScessOnBcdBinding(RichInputText scessOnBcdBinding) {
        this.scessOnBcdBinding = scessOnBcdBinding;
    }

    public RichInputText getScessOnBcdBinding() {
        return scessOnBcdBinding;
    }

    public void setEcessOnBcdBinding(RichInputText ecessOnBcdBinding) {
        this.ecessOnBcdBinding = ecessOnBcdBinding;
    }

    public RichInputText getEcessOnBcdBinding() {
        return ecessOnBcdBinding;
    }

    public void setTotDutyBinding(RichInputText totDutyBinding) {
        this.totDutyBinding = totDutyBinding;
    }

    public RichInputText getTotDutyBinding() {
        return totDutyBinding;
    }

    public void setIgstPerBinding(RichInputText igstPerBinding) {
        this.igstPerBinding = igstPerBinding;
    }

    public RichInputText getIgstPerBinding() {
        return igstPerBinding;
    }

    public void setIgstAmtBinding(RichInputText igstAmtBinding) {
        this.igstAmtBinding = igstAmtBinding;
    }

    public RichInputText getIgstAmtBinding() {
        return igstAmtBinding;
    }

    public void setFreightBinding(RichInputText freightBinding) {
        this.freightBinding = freightBinding;
    }

    public RichInputText getFreightBinding() {
        return freightBinding;
    }

    public void setInsuranceBinding(RichInputText insuranceBinding) {
        this.insuranceBinding = insuranceBinding;
    }

    public RichInputText getInsuranceBinding() {
        return insuranceBinding;
    }

    public void setClearForwardBinding(RichInputText clearForwardBinding) {
        this.clearForwardBinding = clearForwardBinding;
    }

    public RichInputText getClearForwardBinding() {
        return clearForwardBinding;
    }

    public void setAssentNoBinding(RichInputComboboxListOfValues assentNoBinding) {
        this.assentNoBinding = assentNoBinding;
    }

    public RichInputComboboxListOfValues getAssentNoBinding() {
        return assentNoBinding;
    }

    public void setAssetNoBinding(RichInputComboboxListOfValues assetNoBinding) {
        this.assetNoBinding = assetNoBinding;
    }

    public RichInputComboboxListOfValues getAssetNoBinding() {
        return assetNoBinding;
    }

    public void setTotalPurValueBinding(RichInputText totalPurValueBinding) {
        this.totalPurValueBinding = totalPurValueBinding;
    }

    public RichInputText getTotalPurValueBinding() {
        return totalPurValueBinding;
    }

    public void setNetPurValueBinding(RichInputText netPurValueBinding) {
        this.netPurValueBinding = netPurValueBinding;
    }

    public RichInputText getNetPurValueBinding() {
        return netPurValueBinding;
    }

    public void setCostPerUnitBinding(RichInputText costPerUnitBinding) {
        this.costPerUnitBinding = costPerUnitBinding;
    }

    public RichInputText getCostPerUnitBinding() {
        return costPerUnitBinding;
    }

    public void setVouNoBinding(RichInputText vouNoBinding) {
        this.vouNoBinding = vouNoBinding;
    }

    public RichInputText getVouNoBinding() {
        return vouNoBinding;
    }

    public void setSubCodeBinding(RichInputText subCodeBinding) {
        this.subCodeBinding = subCodeBinding;
    }

    public RichInputText getSubCodeBinding() {
        return subCodeBinding;
    }

    public void setGlCodeBinding(RichInputText glCodeBinding) {
        this.glCodeBinding = glCodeBinding;
    }

    public RichInputText getGlCodeBinding() {
        return glCodeBinding;
    }

    public void srvDeletePopupDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                    {
                    ADFUtils.findOperation("Delete1").execute();
                    System.out.println("SRV Record Delete Successfully");
                    FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                    FacesContext fc = FacesContext.getCurrentInstance();  
                    fc.addMessage(null, Message);  
                    
                        ADFUtils.findOperation("removeImportDetailImportBillPvRow").execute();
                    
                    }
                    AdfFacesContext.getCurrentInstance().addPartialTarget(srvTableBinding); 

    }

    public void itemDetailPopupBinding(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                    {
                    ADFUtils.findOperation("Delete").execute();
                    System.out.println("Record Delete Successfully");
                    FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                    FacesContext fc = FacesContext.getCurrentInstance();  
                    fc.addMessage(null, Message);  
                    }
                    AdfFacesContext.getCurrentInstance().addPartialTarget(itemDetailTableBinding); 

    }

    public void setItemDetailTableBinding(RichTable itemDetailTableBinding) {
        this.itemDetailTableBinding = itemDetailTableBinding;
    }

    public RichTable getItemDetailTableBinding() {
        return itemDetailTableBinding;
    }

    public void setSrvTableBinding(RichTable srvTableBinding) {
        this.srvTableBinding = srvTableBinding;
    }

    public RichTable getSrvTableBinding() {
        return srvTableBinding;
    }

    public void glCodeItemDetailVCE(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        try {
            allRefreshDataMethod();
            ADFUtils.findOperation("insertHeadVoucherDataImportBillPassing").execute();
        } catch (Exception e) {
            // TODO: Add catch code
            e.printStackTrace();
        }

    }

    public void setDrCrFlagBinding(RichSelectOneChoice drCrFlagBinding) {
        this.drCrFlagBinding = drCrFlagBinding;
    }

    public RichSelectOneChoice getDrCrFlagBinding() {
        return drCrFlagBinding;
    }

    public void setPopulateButtonBinding(RichButton populateButtonBinding) {
        this.populateButtonBinding = populateButtonBinding;
    }

    public RichButton getPopulateButtonBinding() {
        return populateButtonBinding;
    }

    public String saveCloseAction() {
        String checkApproved="Y";
        System.out.println("USER NAME IN BEAN:="+ADFUtils.evaluateEL("#{pageFlowScope.userName}"));
        if(populateFlag.equalsIgnoreCase("Y"))
        {
            if(tabFlag.equalsIgnoreCase("Y"))
            {
                
       
       if(approvedByBinding.getValue()==null && pageMode.equalsIgnoreCase("E"))
       {
            checkApproved="N";
       }
       if(makerChecker.equalsIgnoreCase("N") && approvedByBinding.getValue()==null && pageMode.equalsIgnoreCase("C"))
       {
           checkApproved="N";
       }
       
       if(!checkApproved.equalsIgnoreCase("N"))
       {

       OperationBinding opr=ADFUtils.findOperation("generateDocumentNoImportBillPassing");
       opr.getParamsMap().put("userName",ADFUtils.evaluateEL("#{pageFlowScope.userName}"));
       opr.execute();
       System.out.println("Result in Bean:"+opr.getResult());
       if(opr.getResult()!=null && opr.getResult().equals("C"))
       {
        ADFUtils.showMessage("Record Save Successfully.New No. is "+billNoBinding.getValue(), 2);
        ADFUtils.findOperation("Commit").execute();
        return "saveAndClose";
        }
       else if(opr.getResult()!=null && opr.getResult().equals("U"))
       {
        ADFUtils.showMessage("Record Updated Successfully.", 2);
        ADFUtils.findOperation("Commit").execute();
        return "saveAndClose";
        }
        else if(opr.getResult()!=null && opr.getResult().equals("E"))
        {
         ADFUtils.showMessage("Problem in Voucher Generation.", 0);
         return null;
         }
       else
       {
        ADFUtils.showMessage("Problem in Number Generation.", 0);
        return null;
       }
        
        }
        else
       {
            ADFUtils.showMessage("Please Select Approved By,If Have Permisson To Approved This Record.", 0);
           return null;
        }
        
        }
        else
        {
            ADFUtils.showMessage("Please Open Import Bill Summmary Tab Before Save Data.", 0);
                return null;
        }
        }
        else
        {
            ADFUtils.showMessage("Please Click On Populate Item Detail Button in SRV Tab.", 0);
            return null;
        }
       
    }

    public void billDateValidator(FacesContext facesContext, UIComponent uIComponent, Object object) 
    {
        if(currentDate==null && billDateBinding.getValue()!=null)
        {
        currentDate=(Date)billDateBinding.getValue();
        }
        System.out.println("Current Date:="+currentDate+"\nBill Date:="+billDateBinding.getValue());
        
        if(object!=null && currentDate!=null)
        {
            Date bill_date=(Date)object;
            if(!(bill_date.compareTo(currentDate)==1))
            {
            OperationBinding op1 = ADFUtils.findOperation("checkForMonthAndYearVouSeriesForAllVouchers");
            op1.getParamsMap().put("vou_dt",object);
            op1.getParamsMap().put("FinYear",ADFUtils.resolveExpression("#{pageFlowScope.finYear}"));
            op1.getParamsMap().put("Vou_Type","P");
            op1.getParamsMap().put("Vou_Series","V");
            op1.getParamsMap().put("Unit_Code",unitCodeBinding.getValue());
            op1.execute();
            System.out.println("GET RESULT:"+op1.getResult());
            if(op1.getResult()!=null && !op1.getResult().equals("Y"))
            {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,op1.getResult().toString(),"Please Select Another Date."));  
            }
            }
            else
            {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,"Bill Date Must Be Less Than or Equals To Current Date","Please Select Another Date."));  
            }
        }
    }
    
    public void refreshAmountBasedOnRate()
    {
        try {
            BigDecimal zero = new BigDecimal(0);
            BigDecimal bill_value_fc = new BigDecimal(0);
            BigDecimal bill_value_inr = new BigDecimal(0);
            Row row = (Row) ADFUtils.evaluateEL("#{bindings.ImportBillPassingDetailVO1Iterator.currentRow}");
            BigDecimal qty = (BigDecimal) (row.getAttribute("Qty") != null ? row.getAttribute("Qty") : zero);
            BigDecimal rate = (BigDecimal) (row.getAttribute("Rate") != null ? row.getAttribute("Rate") : zero);
            bill_value_fc = qty.multiply(rate);
            BigDecimal con_rate =
                (BigDecimal) (row.getAttribute("ConvRate") != null ? row.getAttribute("ConvRate") : zero);
            bill_value_inr = bill_value_fc.multiply(con_rate);
            row.setAttribute("BillValueFc", bill_value_fc.setScale(4,BigDecimal.ROUND_HALF_UP));
            row.setAttribute("BillValueInr", bill_value_inr.setScale(4,BigDecimal.ROUND_HALF_UP));
        } catch (Exception e) {
            // TODO: Add catch code
            e.printStackTrace();
        }
    }
    
    public void allRefreshDataMethod()
    {
        refreshAmountBasedOnRate();
       // basicCustomDutyAmount();    
//        cessBcdPer(); 
//        ecessBcdPer(); 
//        scessBcdPer(); 
        gstCode();
        totalDutyAmount();
        calculateValues();
    }

    
    public void totalDutyAmount()
    {
        
        BigDecimal zero=new BigDecimal(0);
        Row row=(Row)ADFUtils.evaluateEL("#{bindings.ImportBillPassingDetailVO1Iterator.currentRow}");  
        BigDecimal custom_duty_amount=(BigDecimal)(row.getAttribute("CustomDutyAmt")!=null?row.getAttribute("CustomDutyAmt"):zero);
        BigDecimal cess_bcd_amount=(BigDecimal)(row.getAttribute("CessBcdAmt")!=null?row.getAttribute("CessBcdAmt"):zero);      
        BigDecimal ecess_bcd_amount=(BigDecimal)(row.getAttribute("EcessBcdAmt")!=null?row.getAttribute("EcessBcdAmt"):zero);
        BigDecimal scess_bcd_amount=(BigDecimal)(row.getAttribute("ScessBcdAmt")!=null?row.getAttribute("ScessBcdAmt"):zero);
        BigDecimal total_duty_amount=custom_duty_amount.add(cess_bcd_amount).add(ecess_bcd_amount).add(scess_bcd_amount);
        total_duty_amount = total_duty_amount.setScale(0, BigDecimal.ROUND_HALF_UP);
        row.setAttribute("TotDuty",total_duty_amount);
        AdfFacesContext.getCurrentInstance().addPartialTarget(totDutyBinding);
    
    
    }

    public void calculateAndRefreshValueVCE(ValueChangeEvent valueChangeEvent) 
    {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        allRefreshDataMethod();
    }

    public void setApprovedByBinding(RichInputComboboxListOfValues approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputComboboxListOfValues getApprovedByBinding() {
        return approvedByBinding;
    }

    public void setApprovedOnBinding(RichInputDate approvedOnBinding) {
        this.approvedOnBinding = approvedOnBinding;
    }

    public RichInputDate getApprovedOnBinding() {
        return approvedOnBinding;
    }

    public void importBillSummaryDCL(DisclosureEvent disclosureEvent) {
        
        tabFlag="Y";
        OperationBinding opr=ADFUtils.findOperation("makerCheckerForImportBillPassing");
        opr.execute();
        System.out.println("Maker And Check Result is:="+opr.getResult());
        if(opr.getResult().equals("Y") && pageMode.equalsIgnoreCase("C"))
        {
            makerChecker="Y";
            getApprovedByBinding().setDisabled(true);
            getApprovedOnBinding().setDisabled(true);
        }
        else if(opr.getResult().equals("N") && pageMode.equalsIgnoreCase("C"))
        {
            makerChecker="N";
            getApprovedByBinding().setDisabled(false);
            getApprovedOnBinding().setDisabled(false);
        }
            
        AdfFacesContext.getCurrentInstance().addPartialTarget(approvedByBinding);
        AdfFacesContext.getCurrentInstance().addPartialTarget(approvedOnBinding);
        
    }

    public void itemDetailDCL(DisclosureEvent disclosureEvent) {
        tabFlag="N";
    }

    public void srvDCL(DisclosureEvent disclosureEvent) {
        tabFlag="N";
    }

    public void importPBDCL(DisclosureEvent disclosureEvent) {
        tabFlag="N";
    }

    public void setSrvNoSrvBinding(RichInputComboboxListOfValues srvNoSrvBinding) {
        this.srvNoSrvBinding = srvNoSrvBinding;
    }

    public RichInputComboboxListOfValues getSrvNoSrvBinding() {
        return srvNoSrvBinding;
    }

    public void setRateBinding(RichInputText rateBinding) {
        this.rateBinding = rateBinding;
    }

    public RichInputText getRateBinding() {
        return rateBinding;
    }

    public void setAssValBinding(RichInputText assValBinding) {
        this.assValBinding = assValBinding;
    }

    public RichInputText getAssValBinding() {
        return assValBinding;
    }

    public void setPurGlCodeBinding(RichInputComboboxListOfValues purGlCodeBinding) {
        this.purGlCodeBinding = purGlCodeBinding;
    }

    public RichInputComboboxListOfValues getPurGlCodeBinding() {
        return purGlCodeBinding;
    }

    public void setCustomDutyBinding(RichInputText customDutyBinding) {
        this.customDutyBinding = customDutyBinding;
    }

    public RichInputText getCustomDutyBinding() {
        return customDutyBinding;
    }

    public void setCessPerBinding(RichInputText cessPerBinding) {
        this.cessPerBinding = cessPerBinding;
    }

    public RichInputText getCessPerBinding() {
        return cessPerBinding;
    }

    public void setScessPerBinding(RichInputText scessPerBinding) {
        this.scessPerBinding = scessPerBinding;
    }

    public RichInputText getScessPerBinding() {
        return scessPerBinding;
    }

    public void setEcessPerBinding(RichInputText ecessPerBinding) {
        this.ecessPerBinding = ecessPerBinding;
    }

    public RichInputText getEcessPerBinding() {
        return ecessPerBinding;
    }

    public void setGstCodeBinding(RichInputComboboxListOfValues gstCodeBinding) {
        this.gstCodeBinding = gstCodeBinding;
    }

    public RichInputComboboxListOfValues getGstCodeBinding() {
        return gstCodeBinding;
    }

    public void setPartyCodeBinding(RichInputComboboxListOfValues partyCodeBinding) {
        this.partyCodeBinding = partyCodeBinding;
    }

    public RichInputComboboxListOfValues getPartyCodeBinding() {
        return partyCodeBinding;
    }

    public void setInvoiceNoBinding(RichInputText invoiceNoBinding) {
        this.invoiceNoBinding = invoiceNoBinding;
    }

    public RichInputText getInvoiceNoBinding() {
        return invoiceNoBinding;
    }

    public void setInvoiceDateBinding(RichInputDate invoiceDateBinding) {
        this.invoiceDateBinding = invoiceDateBinding;
    }

    public RichInputDate getInvoiceDateBinding() {
        return invoiceDateBinding;
    }

    public void setBeNoBinding(RichInputText beNoBinding) {
        this.beNoBinding = beNoBinding;
    }

    public RichInputText getBeNoBinding() {
        return beNoBinding;
    }

    public void setBeDateBinding(RichInputDate beDateBinding) {
        this.beDateBinding = beDateBinding;
    }

    public RichInputDate getBeDateBinding() {
        return beDateBinding;
    }

    public void setCurrencyCodeBinding(RichSelectOneChoice currencyCodeBinding) {
        this.currencyCodeBinding = currencyCodeBinding;
    }

    public RichSelectOneChoice getCurrencyCodeBinding() {
        return currencyCodeBinding;
    }

    public void setConvRateHeaderBinding(RichInputText convRateHeaderBinding) {
        this.convRateHeaderBinding = convRateHeaderBinding;
    }

    public RichInputText getConvRateHeaderBinding() {
        return convRateHeaderBinding;
    }

    public void setFreightHeaderBinding(RichInputText freightHeaderBinding) {
        this.freightHeaderBinding = freightHeaderBinding;
    }

    public RichInputText getFreightHeaderBinding() {
        return freightHeaderBinding;
    }

    public void setInsuranceHeaderBinding(RichInputText insuranceHeaderBinding) {
        this.insuranceHeaderBinding = insuranceHeaderBinding;
    }

    public RichInputText getInsuranceHeaderBinding() {
        return insuranceHeaderBinding;
    }

    public void setLoadingForwardingBinding(RichInputText loadingForwardingBinding) {
        this.loadingForwardingBinding = loadingForwardingBinding;
    }

    public RichInputText getLoadingForwardingBinding() {
        return loadingForwardingBinding;
    }

    public void setNarrationBinding(RichInputText narrationBinding) {
        this.narrationBinding = narrationBinding;
    }

    public RichInputText getNarrationBinding() {
        return narrationBinding;
    }

    public void approvedByVCE(ValueChangeEvent valueChangeEvent) 
    {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        OperationBinding OprAutho=ADFUtils.findOperation("checkAuthoForImportBillPassing");
        OprAutho.execute();

        Row row =(Row)ADFUtils.evaluateEL("#{bindings.ImportBillPassingHeaderVO1Iterator.currentRow}"); 
        if(!OprAutho.getResult().equals("Y"))
        {
            row.setAttribute("FinMauthAuthCode",null);   
            row.setAttribute("ApprovalDate", null);
            ADFUtils.showMessage("You Don't Have Permission To Approved This Record.",0);        
        }
    //checkAuthoForImportBillPassing  
    }

    public void setCostCenterBinding(RichInputComboboxListOfValues costCenterBinding) {
        this.costCenterBinding = costCenterBinding;
    }

    public RichInputComboboxListOfValues getCostCenterBinding() {
        return costCenterBinding;
    }

    public void billPopupDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                    {
                    ADFUtils.findOperation("Delete2").execute();
                    System.out.println("Record Delete Successfully");
                    FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                    FacesContext fc = FacesContext.getCurrentInstance();  
                    fc.addMessage(null, Message);  
                    }
                    AdfFacesContext.getCurrentInstance().addPartialTarget(billTableBinding); 
    }

    public void setBillTableBinding(RichTable billTableBinding) {
        this.billTableBinding = billTableBinding;
    }

    public RichTable getBillTableBinding() {
        return billTableBinding;
    }

    public void costCenterVCE(ValueChangeEvent valueChangeEvent) 
    {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if((Long)ADFUtils.evaluateEL("#{bindings.ImportBillPassingDetailVO1Iterator.estimatedRowCount}")>0)
        {
        ADFUtils.findOperation("insertHeadVoucherDataImportBillPassing").execute();
        }
    }

    public void freightVCE(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        ADFUtils.findOperation("insertHeadVoucherDataImportBillPassing").execute();

    }

    public void cessVCE(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        cessBcdPer();
        ADFUtils.findOperation("insertHeadVoucherDataImportBillPassing").execute();
        totalDutyAmount();

    }

    public void scessVCE(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        scessBcdPer();
        ADFUtils.findOperation("insertHeadVoucherDataImportBillPassing").execute();
        totalDutyAmount();

    }

    public void ecessVCE(ValueChangeEvent valueChangeEvent) 
    {
    valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
    ecessBcdPer();
        ADFUtils.findOperation("insertHeadVoucherDataImportBillPassing").execute();
        totalDutyAmount();

    }

    public void customDutyVCE(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        basicCustomDutyAmount();
        ADFUtils.findOperation("insertHeadVoucherDataImportBillPassing").execute();
        totalDutyAmount();

    }
}
