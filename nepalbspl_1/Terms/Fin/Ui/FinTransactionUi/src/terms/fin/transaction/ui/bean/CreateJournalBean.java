package terms.fin.transaction.ui.bean;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import java.io.InputStreamReader;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.nio.charset.StandardCharsets;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.validator.ValidatorException;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCDataControl;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputFile;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichToolbar;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.event.ReturnPopupEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;

import oracle.jbo.domain.Date;
import oracle.jbo.server.ViewObjectImpl;

import org.apache.myfaces.trinidad.model.UploadedFile;

import terms.fin.transaction.model.applicationModule.FinTransactionAMImpl;
import terms.fin.transaction.model.view.GenLedFinTvouchVOImpl;
import terms.fin.transaction.model.view.GenLedFinTvouchVORowImpl;

public class CreateJournalBean {

    private RichPopup approveVouWarningPopUpBinding;
    private RichToolbar toolBarbinding;
    private String mode = "V";
    //private RichOutputText bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton detailcreateBinding;
    private RichButton detaildeleteBinding;
    private RichButton headerEditBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichOutputText getOutputText;
    private RichInputText voucherNoBinding;
    private RichInputText voucherSeriesBinding;
    private RichInputComboboxListOfValues verifiedByBinding;
    private RichInputComboboxListOfValues authCodeBinding;
    private RichTable detailTableBinding;
    private RichInputText jvDescriptionBinding;
    private RichInputText tdsDescriptionBinding;
    private RichInputText acDescriptionDtlBinding;
    private RichInputText unitDescBinding;
    private RichInputText tdsAmountBinding;
    private RichInputComboboxListOfValues tdsCodeBinding;
    private RichInputDate vouDateBinding;
    private RichInputText glBalTransBinding;
    private RichInputText amountLCBinding;
    private RichInputText currencyRateBinding;
    private RichInputText amountBinding;
    private RichInputComboboxListOfValues acCodeBinding;
    private RichInputComboboxListOfValues glCodeBinding;
    private RichInputComboboxListOfValues preparedByBinding;
    private RichInputText debitAmountBinding;
    private RichInputText creditAmountBinding;
    private RichInputText diffAmountBinding;
    private RichInputComboboxListOfValues currencyCodeBinding;
    private RichInputDate approvedOnBinding;
    private RichInputText glDescBinding;
    private RichInputText transSubCodeTypeBinding;
    private RichInputText snoBinding;
    private RichInputText narrationBinding;
    private RichInputFile inputFileBind;

    public CreateJournalBean() {

    }

    public String saveAndCloseAction() {
        mode = "SV";
//        if (checkDuplicateAcc().toString().equalsIgnoreCase("N")) {
            OperationBinding binding = ADFUtils.findOperation("checkDifferenceAmount");
            binding.execute();
            System.out.println("Before  Before Popup");
            if (binding.getResult() != null && binding.getResult().toString().equalsIgnoreCase("Y")) {
                if (!debitAmountBinding.getValue().equals(0)) {
                    OperationBinding op = ADFUtils.findOperation("checkVoucherSeries");
                    op.execute();
                    System.out.println("Before Popup");
                    System.out.println("PopUp Result" + op.getResult().toString());

                    if (op.getResult() != null && op.getResult().toString().equalsIgnoreCase("N")) {
                        if (voucherNoBinding.getValue() != null) {
                            if (authCodeBinding.getValue() != null) {
                                if (approvedOnBinding.getValue() != null) {
                                    System.out.println("When Authorized By is Not Null");
                                    ADFUtils.findOperation("checkSameRecord").execute();
                                    OperationBinding findOperation = ADFUtils.findOperation("fetchVouNumber");
                                    findOperation.getParamsMap().put("vou_mode", "N");
                                    findOperation.execute();
                                    System.out.println("Result After Function Call: " +
                                                       findOperation.getResult().toString());
                                    if (!findOperation.getResult().toString().equalsIgnoreCase("ERROR")) {
                                        ADFUtils.showMessage("Voucher has been generated successfully, generated voucher no is - " +
                                                             voucherNoBinding.getValue(), 2);
                                        return "ret_to_srch";
                                    } else {
                                        ADFUtils.showMessage("Voucher No. could not be generated. Try Again !!", 0);
                                    }
                                } else {
                                    ADFUtils.showMessage("Please select Approval Date.", 0);
                                }
                            } 
                           if(authCodeBinding.getValue() == null){
                                    OperationBinding op1 = ADFUtils.findOperation("controlAmountValueJV");
                                    op1.getParamsMap().put("val", debitAmountBinding.getValue());
                                    op1.execute();
                                    ADFUtils.findOperation("voucherNoInUpdateModeJournalVoucher").execute();
                                    ADFUtils.findOperation("Commit").execute();
                                    ADFUtils.showMessage("Record Updated Successfully.", 2);
                                    return "ret_to_srch";
                                }
                            
                        } else {
                            OperationBinding findOperation = ADFUtils.findOperation("fetchVouNumber");
                            findOperation.getParamsMap().put("vou_mode", "D");
                            findOperation.execute();
                            System.out.println("Result After Function Call: " + findOperation.getResult().toString());
                            if (!findOperation.getResult().toString().equalsIgnoreCase("ERROR")) {
                                ADFUtils.showMessage("Voucher has been generated successfully, generated voucher no is - " +
                                                     voucherNoBinding.getValue(), 2);
                                return "ret_to_srch";
                            } else {
                                ADFUtils.showMessage("Voucher No. could not be generated. Try Again !!", 0);
                            }
                        }

                    } else {
                        if (authCodeBinding.getValue() != null) {
                            if (approvedOnBinding.getValue() != null) {
                                ADFUtils.findOperation("checkSameRecord").execute();
                                OperationBinding findOperation = ADFUtils.findOperation("fetchVouNumber");
                                findOperation.getParamsMap().put("vou_mode", "N");
                                findOperation.execute();
                                System.out.println("Result After Function Call: " +
                                                   findOperation.getResult().toString());
                                if (!findOperation.getResult().toString().equalsIgnoreCase("ERROR")) {
                                    ADFUtils.showMessage("Voucher has been generated successfully, generated voucher no is - " +
                                                         voucherNoBinding.getValue(), 2);
                                    return "ret_to_srch";
                                } else {
                                    ADFUtils.showMessage("Voucher No. could not be generated. Try Again !!", 0);
                                }
                            } else {
                                ADFUtils.showMessage("Please select Approval Date.", 0);
                            }
                        } else {
                            ADFUtils.showMessage("Please Approve the record .", 0);
                        }
                    }
                } else
                    ADFUtils.showMessage("Voucher Can't be created with zero amount.", 0);

            } else
                ADFUtils.showMessage("There is a mismatch between Dr amount and Cr Amount.So can not proceed.", 1);

//        } else
//            ADFUtils.showMessage("Selected A/C is already exists.Please select another account", 0);

        return null;
    }

    public void disableFieldMethod(Boolean disable_val) {
        for (String form_id : new String[] { "pfl3", "pfl4", "pfl5" }) {
            UIComponent component = getUIComponent(form_id);
            List<UIComponent> children = component.getChildren();
            for (UIComponent uiComponent : children) {
                if (uiComponent instanceof RichInputText)
                    ((RichInputText) uiComponent).setDisabled(disable_val);
                else if (uiComponent instanceof RichInputComboboxListOfValues)
                    ((RichInputComboboxListOfValues) uiComponent).setDisabled(disable_val);
                else if (uiComponent instanceof RichInputDate)
                    ((RichInputDate) uiComponent).setDisabled(disable_val);

            }
        }
    }

    private UIComponent getUIComponent(String id) {
        FacesContext facesCtx = FacesContext.getCurrentInstance();
        return findComponent(facesCtx.getViewRoot(), id);
    }

    private UIComponent findComponent(UIComponent base, String id) {
        if (id.equals(base.getId())) {
            return base;
        }
        UIComponent children = null;
        UIComponent result = null;
        Iterator childrens = base.getFacetsAndChildren();
        while (childrens.hasNext() && (result == null)) {
            children = (UIComponent) childrens.next();
            if (id.equals(children.getId())) {
                result = children;
                break;
            }
            result = findComponent(children, id);
            if (result != null) {
                break;
            }
        }
        return result;
    }

    public String createJVDetailsAction() {
        if(getNarrationBinding().getValue() != null){
            DCIteratorBinding dci = ADFUtils.findIterator("GenLedFinTvouchVOIterator");

            DCIteratorBinding dci_tvouch = ADFUtils.findIterator("FinTvouchVOIterator");

            if (dci != null) {

                Integer seq_no = fetchMaxLineNumber(dci);
                RowSetIterator rsi = dci.getRowSetIterator();
                if (rsi != null) {
                    Row last = rsi.last();
                    int i = rsi.getRangeIndexOf(last);
                    GenLedFinTvouchVORowImpl newRow = (GenLedFinTvouchVORowImpl) rsi.createRow();
                    newRow.setNewRowState(Row.STATUS_INITIALIZED);
                    rsi.insertRowAtRangeIndex(i + 1, newRow);
                    rsi.setCurrentRow(newRow);
                    newRow.setSeqNo(seq_no);
                    newRow.setCurrencyCode(dci_tvouch.getCurrentRow().getAttribute("CurrencyCode") != null ?
                                           dci_tvouch.getCurrentRow().getAttribute("CurrencyCode").toString() : null);

                    //                       System.out.println("*****************dci_tvouch.getCurrentRow().getAttribute(\"CurrencyCode\")"+dci_tvouch.getCurrentRow().getAttribute("CurrencyCode"));
                    //
                    //                       System.out.println("******Curremt Row****");
                    //
                    //                       Row rr=(Row)  ADFunitCodeBindingUtils.evaluateEL("#{bindings.FinTvouchVOIterator.currentRow}");
                    //
                    //                      System.out.println("*********************CurrCode***********"+rr.getAttribute("UnitCurr"));
                    //                       System.out.println("*********************CurrCode1***********"+rr.getAttribute("CurrencyCode1"));


                    newRow.setFcAmount(new BigDecimal(0));
                    newRow.setAmount(new BigDecimal(0));

                    if (seq_no == 1)
                        newRow.setDrCrFlag("D");
                    else
                        newRow.setDrCrFlag("C");
                }
                rsi.closeRowSetIterator();
            }
            //        } else
            //            ADFUtils.showMessage("Selected A/C is already exists.Please select another account", 0);
            AdfFacesContext.getCurrentInstance().addPartialTarget(toolBarbinding);
            disabledFields();
            return null;
        }else{
            System.out.println("inside narration null check ");
            FacesMessage Message = new FacesMessage("Enter the Narration Firstly.");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
            return null;
        }
            
        //Row rr=(Row)  ADFUtils.evaluateEL("#{bindings.FinTvouchVOIterator.currentRow}");
        //System.out.println("*********************CurrCode***********"+rr.getAttribute("UnitCurr"));
        //if(rr.getAttribute("UnitCurr")!=null && rr.getAttribute("CurrencyCode")==null)
        //{
        //rr.setAttribute("CurrencyCode",rr.getAttribute("UnitCurr"));
        //}


//        if (checkDuplicateAcc().toString().equalsIgnoreCase("N")) {
    }


    public String deleteJVDetailsAction() {
        DCIteratorBinding iteratorBinding = ADFUtils.findIterator("GenLedFinTvouchVOIterator");
        if (iteratorBinding.getCurrentRow() != null) {
            OperationBinding findOperation = ADFUtils.findOperation("Delete");
            findOperation.execute();
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(toolBarbinding);

        return null;
    }

    public String saveButtonAction() {
//        if (checkDuplicateAcc().toString().equalsIgnoreCase("N")) {
            OperationBinding binding = ADFUtils.findOperation("checkDifferenceAmount");
            binding.execute();
            System.out.println("Before  Before Popup");
            if (binding.getResult() != null && binding.getResult().toString().equalsIgnoreCase("Y")) {
                if (!debitAmountBinding.getValue().equals(0)) {
                    OperationBinding op = ADFUtils.findOperation("checkVoucherSeries");
                    op.execute();
                    System.out.println("Before Popup");
                    System.out.println("PopUp Result" + op.getResult().toString());

                    if (op.getResult() != null && op.getResult().toString().equalsIgnoreCase("N")) {
                        if (voucherNoBinding.getValue() != null) {
                            if (authCodeBinding.getValue() != null) {
                                if (approvedOnBinding.getValue() != null) {
                                System.out.println("When Authorized By is Not Null");
                                ADFUtils.findOperation("checkSameRecord").execute();
                                OperationBinding findOperation = ADFUtils.findOperation("fetchVouNumber");
                                findOperation.getParamsMap().put("vou_mode", "N");
                                findOperation.execute();
                                System.out.println("Result After Function Call: " +
                                                   findOperation.getResult().toString());
                                if (!findOperation.getResult().toString().equalsIgnoreCase("ERROR")) {
                                    ADFUtils.showMessage("Voucher has been generated successfully, generated voucher no is - " +
                                                         voucherNoBinding.getValue(), 2);
                                    ADFUtils.findOperation("CreateInsert1").execute();
                                } else {
                                    ADFUtils.showMessage("Voucher No. could not be generated. Try Again !!", 0);
                                }
                            }
                         else {
                                  ADFUtils.showMessage("Please select Approval Date.", 0);
                                 }
                            }
                            else if(authCodeBinding.getValue() == null){
                                    OperationBinding op1 = ADFUtils.findOperation("controlAmountValueJV");
                                    op1.getParamsMap().put("val", debitAmountBinding.getValue());
                                    op1.execute();
                                    ADFUtils.findOperation("voucherNoInUpdateModeJournalVoucher").execute();
                                    ADFUtils.findOperation("Commit").execute();
                                    ADFUtils.showMessage("Record Updated Successfully.", 2);
                                }
                                
                                else {
                                ADFUtils.showMessage("Please Approve the record.", 0);
                            }
                           
                        } else {
                            System.out.println("In Dummy voucher Generation!");
                            OperationBinding findOperation = ADFUtils.findOperation("fetchVouNumber");
                            findOperation.getParamsMap().put("vou_mode", "D");
                            findOperation.execute();
                            System.out.println("Result After Function Call: " + findOperation.getResult().toString());
                            if (!findOperation.getResult().toString().equalsIgnoreCase("ERROR")) {
                                ADFUtils.showMessage("Voucher has been generated successfully, generated voucher no is - " +
                                                     voucherNoBinding.getValue(), 2);
                                ADFUtils.findOperation("CreateInsert1").execute();
                            } else {
                                ADFUtils.showMessage("Voucher No. could not be generated. Try Again !!", 0);
                            }
                        }

                    } else {
                        if (authCodeBinding.getValue() != null) {
                            if (approvedOnBinding.getValue() != null) {
                            ADFUtils.findOperation("checkSameRecord").execute();
                            OperationBinding findOperation = ADFUtils.findOperation("fetchVouNumber");
                            findOperation.getParamsMap().put("vou_mode", "N");
                            findOperation.execute();
                            System.out.println("Result After Function Call: " + findOperation.getResult().toString());
                            if (!findOperation.getResult().toString().equalsIgnoreCase("ERROR")) {
                                ADFUtils.showMessage("Voucher has been generated successfully, generated voucher no is - " +
                                                     voucherNoBinding.getValue(), 2);
                                ADFUtils.findOperation("CreateInsert1").execute();
                            } else {
                                ADFUtils.showMessage("Voucher No. could not be generated. Try Again !!", 0);
                            }

                        }
                            else {
                                  ADFUtils.showMessage("Please select Approval Date.", 0);
                               }
                        }else {
                            ADFUtils.showMessage("Please Approve the record.", 0);
                        }
                    }
                } else
                    ADFUtils.showMessage("Voucher Can't be created with zero amount.", 0);

            } else
                ADFUtils.showMessage("There is a mismatch between Dr amount and Cr Amount.So can not proceed.", 1);
//        } else
//            ADFUtils.showMessage("Selected A/C is already exists.Please select another account", 0);
        return null;
    }


    private Integer fetchMaxLineNumber(DCIteratorBinding itr) {

        Integer max = new Integer(0);
        GenLedFinTvouchVORowImpl currRow = (GenLedFinTvouchVORowImpl) itr.getCurrentRow();
        if (currRow != null && currRow.getSeqNo() != null)
            max = currRow.getSeqNo();
        RowSetIterator rsi = itr.getRowSetIterator();
        if (rsi != null) {
            Row[] allRowsInRange = rsi.getAllRowsInRange();
            for (Row rw : allRowsInRange) {
                if (rw != null && rw.getAttribute("SeqNo") != null &&
                    max.compareTo(Integer.parseInt(rw.getAttribute("SeqNo").toString())) < 0)
                    max = Integer.parseInt(rw.getAttribute("SeqNo").toString());
            }
        }
        rsi.closeRowSetIterator();
        max = max + new Integer(1);
        return max;

    }

    public String checkDuplicateAcc() {
        OperationBinding binding = ADFUtils.findOperation("checkForDuplicateAcc");
        binding.getParamsMap().put("sub_code", null);
        binding.execute();
        return binding.getResult() != null ? binding.getResult().toString() : "N";
    }


    public void setApproveVouWarningPopUpBinding(RichPopup approveVouWarningPopUpBinding) {
        this.approveVouWarningPopUpBinding = approveVouWarningPopUpBinding;
    }

    public RichPopup getApproveVouWarningPopUpBinding() {
        return approveVouWarningPopUpBinding;
    }

    public void setToolBarbinding(RichToolbar toolBarbinding) {
        this.toolBarbinding = toolBarbinding;
    }

    public RichToolbar getToolBarbinding() {
        return toolBarbinding;
    }

//    public void currencyCodeValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
//
//        if (object != null) {
//
//            DCIteratorBinding itr = ADFUtils.findIterator("GenLedFinTvouchVOIterator");
//            GenLedFinTvouchVORowImpl row = (GenLedFinTvouchVORowImpl) itr.getCurrentRow();
//            System.out.println("row.getFinTvouchVouDate()" + row.getFinTvouchVouDate());
//            System.out.println("row.getFinTvouchUnitCode()" + row.getFinTvouchUnitCode());
//
//            OperationBinding binding = ADFUtils.findOperation("fetchCurrRate");
//            binding.getParamsMap().put("vou_dt", row.getFinTvouchVouDate());
//            binding.getParamsMap().put("unit_code", row.getFinTvouchUnitCode());
//            binding.getParamsMap().put("curr_code", object.toString());
//            binding.execute();
//            System.out.println("***********************END***********");
//            if (binding.getResult() == null)
//                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
//                                                              "Currency rate is not define for selected currency",
//                                                              "please slecet another currency"));
//        }
//    }

    public String genrateApprovedVouAction() {
        System.out.println("Yes button popup dialog Action.");
        approveVouWarningPopUpBinding.hide();
        if (verifiedByBinding.getValue() != null && getAuthCodeBinding().getValue() != null) {
            ADFUtils.findOperation("checkSameRecord").execute();
            OperationBinding findOperation = ADFUtils.findOperation("fetchVouNumber");
            findOperation.getParamsMap().put("vou_mode", "N");
            findOperation.execute();

            if (mode == "S") {
                OperationBinding operation = ADFUtils.findOperation("CreateInsert1");
                operation.execute();
            }
            return mode == "SV" ? "ret_to_srch" : null;
        } else {
            ADFUtils.showMessage("Please Verify and Approve the record.", 0);
        }

        return null;
    }

    public String genOnlyDummyVouAction() {
        System.out.println("No button popup dialog Action.");
        approveVouWarningPopUpBinding.hide();
        ADFUtils.findOperation("checkSameRecord").execute();
        OperationBinding findOperation = ADFUtils.findOperation("fetchVouNumber");
        findOperation.getParamsMap().put("vou_mode", "D");
        findOperation.execute();
        if (mode == "S") {
            OperationBinding operation = ADFUtils.findOperation("CreateInsert1");
            operation.execute();
        }
        return mode == "SV" ? "ret_to_srch" : null;
    }

    public void amountValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (object == null || ((BigDecimal) object).compareTo(new BigDecimal(0)) <= 0) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                          "Amount must be greater than Zero(0)",
                                                          "Please enter a valid amount"));
        }
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getMode() {
        return mode;
    }

    public void voucherDateValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if(object!=null){
        oracle.jbo.domain.Date d=null;
        java.util.Date d1=null;
        String str = object.toString();
        try{
         d = new oracle.jbo.domain.Date(str);
//            System.out.println("Date converted: " + d);
        }
        catch(Exception e){
            d1=new java.util.Date(str);
//            System.out.println("Date converted: " + d1);
        }
//        System.out.println("in method vou date" + object);
        OperationBinding op1 = ADFUtils.findOperation("checkForMonthAndYearVouSeriesForAllVouchers");
        if(d!=null){
        op1.getParamsMap().put("vou_dt", d);
        }
        if(d1!=null){
        op1.getParamsMap().put("vou_dt", d1);
        }
        op1.getParamsMap().put("FinYear",ADFUtils.resolveExpression("#{pageFlowScope.finYear}"));
        //op1.getParamsMap().put("FinYear", "18-19");
        op1.getParamsMap().put("Vou_Type", "J");
        op1.getParamsMap().put("Vou_Series", "V");
        op1.getParamsMap().put("Unit_Code", unitCodeBinding.getValue());
        op1.execute();   
//        System.out.println("fin year="+ADFUtils.resolveExpression("#{pageFlowScope.finYear}"));
        if (op1.getResult() != null && !op1.getResult().toString().equalsIgnoreCase("Y")) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, op1.getResult().toString(),
                                                          "Please Select Another Date."));
        }
        }
    }


    //    public void setBindingOutputText(RichOutputText bindingOutputText) {
    //        this.bindingOutputText = bindingOutputText;
    //    }

    //    public RichOutputText getBindingOutputText() {
    //        cevmodecheck();
    //        return bindingOutputText;
    //    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setDetailcreateBinding(RichButton detailcreateBinding) {
        this.detailcreateBinding = detailcreateBinding;
    }

    public RichButton getDetailcreateBinding() {
        return detailcreateBinding;
    }

    public void setDetaildeleteBinding(RichButton detaildeleteBinding) {
        this.detaildeleteBinding = detaildeleteBinding;
    }

    public RichButton getDetaildeleteBinding() {
        return detaildeleteBinding;
    }

    private void cevmodecheck() {
        System.out.println(ADFUtils.resolveExpression("#{pageFlowScope.mode}")+"mode value in jr bean");
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }


    //Set Fields and Button disable.
    public void cevModeDisableComponent(String mode) {
        //            FacesContext fctx = FacesContext.getCurrentInstance();
        //            ELContext elctx = fctx.getELContext();
        //            Application jsfApp = fctx.getApplication();
        //            //create a ValueExpression that points to the ADF binding layer
        //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
        //            //
        //            ValueExpression valueExpr = exprFactory.createValueExpression(
        //                                         elctx,
        //                                         "#{pageFlowScope.mode=='E'}",
        //                                          Object.class
        //                                         );
        //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
        if (mode.equals("E")) {
//            getDetailcreateBinding().setDisabled(false);
            getDetaildeleteBinding().setDisabled(false);
            getHeaderEditBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
            getVoucherNoBinding().setDisabled(true);
            getVoucherSeriesBinding().setDisabled(true);
            //            if (verifiedByBinding.getValue() != null) {
            //                getVerifiedByBinding().setDisabled(true);
            //            }
            //            if (authCodeBinding.getValue() != null) {
            //                getAuthCodeBinding().setDisabled(true);
            //            }
            getJvDescriptionBinding().setDisabled(true);
            getTdsDescriptionBinding().setDisabled(true);
            getAcDescriptionDtlBinding().setDisabled(true);
            getVouDateBinding().setDisabled(true);
            getTdsCodeBinding().setDisabled(true);
            getTdsAmountBinding().setDisabled(true);
            getUnitDescBinding().setDisabled(true);
            getGlBalTransBinding().setDisabled(true);
            getAmountLCBinding().setDisabled(false);
            //getAcCodeBinding().setDisabled(true);
            getPreparedByBinding().setDisabled(true);
            getDebitAmountBinding().setDisabled(true);
            getCreditAmountBinding().setDisabled(true);
            getDiffAmountBinding().setDisabled(true);
            //getApprovedOnBinding().setDisabled(true);
            getCurrencyRateBinding().setDisabled(true);
            getGlDescBinding().setDisabled(true);
            getSnoBinding().setDisabled(true);
            System.out.println("after click on E");
        } else if (mode.equals("C")) {

            getHeaderEditBinding().setDisabled(true);
            //                                    getDetailcreateBinding().setDisabled(false);
            //                                    getDetaildeleteBinding().setDisabled(false);
            getUnitCodeBinding().setDisabled(true);
            getSnoBinding().setDisabled(true);
            getGlDescBinding().setDisabled(true);
            getCurrencyRateBinding().setDisabled(true);
            getApprovedOnBinding().setDisabled(true);
            //getVouDateBinding().setDisabled(true);
            getVoucherNoBinding().setDisabled(true);
            getVoucherSeriesBinding().setDisabled(true);
            getVerifiedByBinding().setDisabled(false);
            getAuthCodeBinding().setDisabled(true);
            getJvDescriptionBinding().setDisabled(true);
            getTdsDescriptionBinding().setDisabled(true);
            getAcDescriptionDtlBinding().setDisabled(true);
            getUnitDescBinding().setDisabled(true);
            getPreparedByBinding().setDisabled(true);
            getGlBalTransBinding().setDisabled(true);
            getAmountLCBinding().setDisabled(false);
            getDebitAmountBinding().setDisabled(true);
            getCreditAmountBinding().setDisabled(true);
            getDiffAmountBinding().setDisabled(true);
        } else if (mode.equals("V")) {
//            getDetailcreateBinding().setDisabled(true);
            getDetaildeleteBinding().setDisabled(true);
        }

    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void editButtonAL(ActionEvent actionEvent) {
        if (authCodeBinding.getValue() == null) {
            cevmodecheck();
        } else {
            ADFUtils.showMessage("Record is Approved Can't be modified !", 2);
            ADFUtils.setEL("#{pageFlowScope.mode}", "V");
        }
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setGetOutputText(RichOutputText getOutputText) {
        this.getOutputText = getOutputText;
    }

    public RichOutputText getGetOutputText() {
        cevmodecheck();
        return getOutputText;
    }

    public void setVoucherNoBinding(RichInputText voucherNoBinding) {
        this.voucherNoBinding = voucherNoBinding;
    }

    public RichInputText getVoucherNoBinding() {
        return voucherNoBinding;
    }

    public void setVoucherSeriesBinding(RichInputText voucherSeriesBinding) {
        this.voucherSeriesBinding = voucherSeriesBinding;
    }

    public RichInputText getVoucherSeriesBinding() {
        return voucherSeriesBinding;
    }

    public void setVerifiedByBinding(RichInputComboboxListOfValues verifiedByBinding) {
        this.verifiedByBinding = verifiedByBinding;
    }

    public RichInputComboboxListOfValues getVerifiedByBinding() {
        return verifiedByBinding;
    }

    public void setAuthCodeBinding(RichInputComboboxListOfValues authCodeBinding) {
        this.authCodeBinding = authCodeBinding;
    }

    public RichInputComboboxListOfValues getAuthCodeBinding() {
        return authCodeBinding;
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete").execute();
            System.out.println("Record Delete Successfully");
            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(detailTableBinding);

    }

    public void setDetailTableBinding(RichTable detailTableBinding) {
        this.detailTableBinding = detailTableBinding;
    }

    public RichTable getDetailTableBinding() {
        return detailTableBinding;
    }

    public void setJvDescriptionBinding(RichInputText jvDescriptionBinding) {
        this.jvDescriptionBinding = jvDescriptionBinding;
    }

    public RichInputText getJvDescriptionBinding() {
        return jvDescriptionBinding;
    }

    public void setTdsDescriptionBinding(RichInputText tdsDescriptionBinding) {
        this.tdsDescriptionBinding = tdsDescriptionBinding;
    }

    public RichInputText getTdsDescriptionBinding() {
        return tdsDescriptionBinding;
    }

    public void setAcDescriptionDtlBinding(RichInputText acDescriptionDtlBinding) {
        this.acDescriptionDtlBinding = acDescriptionDtlBinding;
    }

    public RichInputText getAcDescriptionDtlBinding() {
        return acDescriptionDtlBinding;
    }

    public void setUnitDescBinding(RichInputText unitDescBinding) {
        this.unitDescBinding = unitDescBinding;
    }

    public RichInputText getUnitDescBinding() {
        return unitDescBinding;
    }

    public void setTdsAmountBinding(RichInputText tdsAmountBinding) {
        this.tdsAmountBinding = tdsAmountBinding;
    }

    public RichInputText getTdsAmountBinding() {
        return tdsAmountBinding;
    }

    public void setTdsCodeBinding(RichInputComboboxListOfValues tdsCodeBinding) {
        this.tdsCodeBinding = tdsCodeBinding;
    }

    public RichInputComboboxListOfValues getTdsCodeBinding() {
        return tdsCodeBinding;
    }

    public void setVouDateBinding(RichInputDate vouDateBinding) {
        this.vouDateBinding = vouDateBinding;
    }

    public RichInputDate getVouDateBinding() {
        return vouDateBinding;
    }

    public void disabledFields() {
        getVouDateBinding().setDisabled(true);
        getTdsCodeBinding().setDisabled(true);
        getTdsAmountBinding().setDisabled(true);
    }

    public void setGlBalTransBinding(RichInputText glBalTransBinding) {
        this.glBalTransBinding = glBalTransBinding;
    }

    public RichInputText getGlBalTransBinding() {
        return glBalTransBinding;
    }

    public void setAmountLCBinding(RichInputText amountLCBinding) {
        this.amountLCBinding = amountLCBinding;
    }

    public RichInputText getAmountLCBinding() {
        return amountLCBinding;
    }

    public void CurrencyRateVCE(ValueChangeEvent vce) {
        BigDecimal rate = (BigDecimal) vce.getNewValue();
        BigDecimal AmtFc = (BigDecimal) amountBinding.getValue();
        if (rate != null && AmtFc != null) {
            rate = rate.multiply(AmtFc);
            amountLCBinding.setValue(rate);
        }

    }

    public void setCurrencyRateBinding(RichInputText currencyRateBinding) {
        this.currencyRateBinding = currencyRateBinding;
    }

    public RichInputText getCurrencyRateBinding() {
        return currencyRateBinding;
    }

    public void setAmountBinding(RichInputText amountBinding) {
        this.amountBinding = amountBinding;
    }

    public RichInputText getAmountBinding() {
        return amountBinding;
    }

    public void AmountFcVCE(ValueChangeEvent vce) {
        OperationBinding op = ADFUtils.findOperation("checkVoucherSeries");
        op.execute();
        System.out.println("Result: " + op.getResult().toString());

        if (op.getResult() != null && op.getResult().toString().equalsIgnoreCase("N")) {
            System.out.println("Value of mode: " + ADFUtils.resolveExpression("#{pageFlowScope.mode}"));
            if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C") ||
                ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("S") ||
                ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("P")) {
                System.out.println("Inside condition!!");
                getAuthCodeBinding().setDisabled(true);
                //authCodeBinding.setDisabled(true);
            } else {
                getAuthCodeBinding().setDisabled(false);
            }
        } else {
            getAuthCodeBinding().setDisabled(false);
        }
        BigDecimal rate = (BigDecimal) currencyRateBinding.getValue();
        BigDecimal AmtFc = (BigDecimal) vce.getNewValue();
        BigDecimal mul = new BigDecimal(0);
        if (rate != null && AmtFc != null) {
            mul = rate.multiply(AmtFc);
            amountLCBinding.setValue(mul);
            System.out.println("MODE VALUE: " + ADFUtils.resolveExpression("#{pageFlowScope.mode}") +
                               " And AuthCode Value: " + authCodeBinding.getValue());
            if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E") && authCodeBinding.getValue() != null) {
                System.out.println("inside my if condition !!");
                authCodeBinding.setValue(null);
            }
        }
    }

    //    public void dialogYesActionListener(ActionEvent actionEvent) {
    //
    //    }


    //    public void dialogNoActioListener(ActionEvent actionEvent) {
    //        getAuthCodeBinding().setDisabled(true);
    //    }

    public void setAcCodeBinding(RichInputComboboxListOfValues acCodeBinding) {
        this.acCodeBinding = acCodeBinding;
    }

    public RichInputComboboxListOfValues getAcCodeBinding() {
        return acCodeBinding;
    }

    public void setGlCodeBinding(RichInputComboboxListOfValues glCodeBinding) {
        this.glCodeBinding = glCodeBinding;
    }

    public RichInputComboboxListOfValues getGlCodeBinding() {
        return glCodeBinding;
    }

    //    public void AcCodeVCL(ValueChangeEvent valueChangeEvent) {
    //        System.out.println("Value of A/C Code: " + acCodeBinding.getValue());
    //        if (valueChangeEvent.getNewValue() != valueChangeEvent.getOldValue()) {
    //            glCodeBinding.setValue(null);
    //        }
    //    }

    public void setPreparedByBinding(RichInputComboboxListOfValues preparedByBinding) {
        this.preparedByBinding = preparedByBinding;
    }

    public RichInputComboboxListOfValues getPreparedByBinding() {
        return preparedByBinding;
    }

    public void setDebitAmountBinding(RichInputText debitAmountBinding) {
        this.debitAmountBinding = debitAmountBinding;
    }

    public RichInputText getDebitAmountBinding() {
        return debitAmountBinding;
    }

    public void setCreditAmountBinding(RichInputText creditAmountBinding) {
        this.creditAmountBinding = creditAmountBinding;
    }

    public RichInputText getCreditAmountBinding() {
        return creditAmountBinding;
    }

    public void setDiffAmountBinding(RichInputText diffAmountBinding) {
        this.diffAmountBinding = diffAmountBinding;
    }

    public RichInputText getDiffAmountBinding() {
        return diffAmountBinding;
    }

    public void ApprovedByReturnListener(ReturnPopupEvent returnPopupEvent) {
        System.out.println("In Return Listner!!! Unit Code :" + unitCodeBinding.getValue().toString() + "Auth Code: " +
                           authCodeBinding.getValue()+ " Amount: " +
                           debitAmountBinding.getValue().toString());
        if (authCodeBinding.getValue() != null) {
            OperationBinding op = ADFUtils.findOperation("checkApprovalStatusInCheckAutho");
            op.getParamsMap().put("unitCd", unitCodeBinding.getValue().toString());
            op.getParamsMap().put("empCode", authCodeBinding.getValue().toString());
            op.getParamsMap().put("amount", debitAmountBinding.getValue().toString());
            op.execute();
        }
    }

    public void setCurrencyCodeBinding(RichInputComboboxListOfValues currencyCodeBinding) {
        this.currencyCodeBinding = currencyCodeBinding;
    }

    public RichInputComboboxListOfValues getCurrencyCodeBinding() {
        return currencyCodeBinding;
    }

    public void setApprovedOnBinding(RichInputDate approvedOnBinding) {
        this.approvedOnBinding = approvedOnBinding;
    }

    public RichInputDate getApprovedOnBinding() {
        return approvedOnBinding;
    }

    public void setGlDescBinding(RichInputText glDescBinding) {
        this.glDescBinding = glDescBinding;
    }

    public RichInputText getGlDescBinding() {
        return glDescBinding;
    }

    public void currCodeVCL(ValueChangeEvent valueChangeEvent) {
        if (valueChangeEvent != null) {
            OperationBinding op = ADFUtils.findOperation("fetchCurrRate");
            op.getParamsMap().put("currencyCode", currencyCodeBinding.getValue());
            Object rst = op.execute();
            if (rst != null && rst.toString().equals("F")) {
                FacesMessage message = new FacesMessage("Please validate currency in daily currency table.");
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(currencyCodeBinding.getClientId(), message);
            }
        }
    }

    public void amountLcVCL(ValueChangeEvent valueChangeEvent) {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E") && authCodeBinding.getValue() != null) {
            System.out.println("inside my if condition !!");
            authCodeBinding.setValue(null);
        }
    }

    public void CostCenterValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        System.out.println("Object Value: "+object.toString()+" GL Code Value: "+glCodeBinding.getValue()+" TransSubCode: "+transSubCodeTypeBinding.getValue());
        if(object.toString().isEmpty()&& glCodeBinding.getValue()!=null &&(transSubCodeTypeBinding.getValue().equals("E")||transSubCodeTypeBinding.getValue().equals("I"))){
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Cost Center is required.",
                                                              null));
            }

    }

    public void setTransSubCodeTypeBinding(RichInputText transSubCodeTypeBinding) {
        this.transSubCodeTypeBinding = transSubCodeTypeBinding;
    }

    public RichInputText getTransSubCodeTypeBinding() {
        return transSubCodeTypeBinding;
    }

    public void setSnoBinding(RichInputText snoBinding) {
        this.snoBinding = snoBinding;
    }

    public RichInputText getSnoBinding() {
        return snoBinding;
    }

    public void copyVoucherFromExisting(ActionEvent actionEvent) {
        OperationBinding op = ADFUtils.findOperation("copyVoucherFromExisting");
        op.getParamsMap().put("vouDate", vouDateBinding.getValue().toString());
        op.execute();
    }

    public void setNarrationBinding(RichInputText narrationBinding) {
        this.narrationBinding = narrationBinding;
    }

    public RichInputText getNarrationBinding() {
        return narrationBinding;
    }


    public void setInputFileBind(RichInputFile inputFileBind) {
        this.inputFileBind = inputFileBind;
    }

    public RichInputFile getInputFileBind() {
        return inputFileBind;
    }
    private UploadedFile file;
    private InputStream inputstream;
    private String errorMessage = null;

    public void inputFileVCL(ValueChangeEvent valueChangeEvent) {
        try {
                 errorMessage = null;
                 System.out.println("Cleared error");
                 file = (UploadedFile)valueChangeEvent.getNewValue();
                 inputstream = file.getInputStream();
                 System.out.println("FILE IS ::" + getInputFileBind());
                 System.out.println("File name is :" + file.getFilename());
                 System.out.println("Content type is: " + file.getContentType());
                 if (!file.getContentType().contains("csv")) {
                     System.out.println("Wrong file type. Please ensure that you only upload CSV files");
                     FacesMessage facesMessage =
                         new FacesMessage(FacesMessage.SEVERITY_INFO, "Wrong file type. " + "Please ensure that you only upload CSV files",
                                          "");
                     FacesContext.getCurrentInstance().addMessage(null, facesMessage);
                     errorMessage = "ERROR :: Wrong file type. Please ensure that you only upload CSV files";
                 }
             } catch (Exception e) {
                 System.out.println(e.getMessage());
             }
        }

    public void uploadButtonAL(ActionEvent actionEvent) {
        if(getNarrationBinding().getValue() != null){
            try {            
                OperationBinding ob = ADFUtils.findOperation("uploadJouralVoucher");
                ob.getParamsMap().put("inputstream", convertInputStreamToString(inputstream));
                ob.getParamsMap().put("vouDate", vouDateBinding.getValue().toString());
                ob.execute();
                inputstream.close();
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        else{
                FacesMessage Message = new FacesMessage("Enter the Narration Firstly.");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
                }                    
    }
    
    public static final int DEFAULT_BUFFER_SIZE = 8192;
    private static String convertInputStreamToString(InputStream is) throws IOException {

            ByteArrayOutputStream result = new ByteArrayOutputStream();
            byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
            int length;
            while ((length = is.read(buffer)) != -1) {
                result.write(buffer, 0, length);
            }
            return result.toString(StandardCharsets.UTF_8.name());

        }
}

