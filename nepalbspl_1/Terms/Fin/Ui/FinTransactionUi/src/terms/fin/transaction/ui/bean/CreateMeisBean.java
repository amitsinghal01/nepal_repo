package terms.fin.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class CreateMeisBean {
    private RichButton editButtonBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText docNoBinding;
    private RichInputDate docDateBinding;
    private RichInputDate meisDateBinding;
    private RichInputText meisFcrNoBinding;
    private RichInputDate meisFcrDateBinding;
    private RichInputText meisNoBinding;
    private RichInputText meisValBinding;
    private RichInputComboboxListOfValues invoiceNoBinding;
    private RichInputDate invoiceDateBinding;
    private RichInputText shippingBillNoBinding;
    private RichInputDate shippingBillDateBinding;
    private RichInputText shippBillValueBinding;
    private RichInputText brcValueBinding;
    private RichInputText initialValueBinding;
    private RichInputText actualReceiveBinding;
    private RichOutputText bindingOutputText;
    private RichPanelHeader myPageRootComponent;
    private RichTable meisTableBinding;
    private RichInputText sumOfRecTransBinding;

    public CreateMeisBean() {
    }

    public void setEditButtonBinding(RichButton editButtonBinding) {
        this.editButtonBinding = editButtonBinding;
    }

    public RichButton getEditButtonBinding() {
        return editButtonBinding;
    }

    public void editButtonAL(ActionEvent actionEvent) {
    cevmodecheck();
}

    public void saveButtonAL(ActionEvent actionEvent) {
        OperationBinding opr = ADFUtils.findOperation("generateDocNoInMesi");
        opr.execute();
        System.out.println("Get Result is:"+opr.getResult());
        if(opr.getResult()!=null)
        {
            if(opr.getResult().toString().equals("Y"))
            {
            ADFUtils.findOperation("Commit").execute();
            ADFUtils.showMessage("Record Save Succesfully.New Doc. No. is "+docNoBinding.getValue(),2);
            }
            else
            {
            ADFUtils.findOperation("Commit").execute();
            ADFUtils.showMessage("Record Updated Successfully",2);
            }
            }
            else
            {
            ADFUtils.showMessage("Problem in Doc. No. generation,Please check.",0);
            }
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setDocNoBinding(RichInputText docNoBinding) {
        this.docNoBinding = docNoBinding;
    }

    public RichInputText getDocNoBinding() {
        return docNoBinding;
    }

    public void setDocDateBinding(RichInputDate docDateBinding) {
        this.docDateBinding = docDateBinding;
    }

    public RichInputDate getDocDateBinding() {
        return docDateBinding;
    }

    public void setMeisDateBinding(RichInputDate meisDateBinding) {
        this.meisDateBinding = meisDateBinding;
    }

    public RichInputDate getMeisDateBinding() {
        return meisDateBinding;
    }

    public void setMeisFcrNoBinding(RichInputText meisFcrNoBinding) {
        this.meisFcrNoBinding = meisFcrNoBinding;
    }

    public RichInputText getMeisFcrNoBinding() {
        return meisFcrNoBinding;
    }

    public void setMeisFcrDateBinding(RichInputDate meisFcrDateBinding) {
        this.meisFcrDateBinding = meisFcrDateBinding;
    }

    public RichInputDate getMeisFcrDateBinding() {
        return meisFcrDateBinding;
    }

    public void setMeisNoBinding(RichInputText meisNoBinding) {
        this.meisNoBinding = meisNoBinding;
    }

    public RichInputText getMeisNoBinding() {
        return meisNoBinding;
    }

    public void setMeisValBinding(RichInputText meisValBinding) {
        this.meisValBinding = meisValBinding;
    }

    public RichInputText getMeisValBinding() {
        return meisValBinding;
    }

    public void setInvoiceNoBinding(RichInputComboboxListOfValues invoiceNoBinding) {
        this.invoiceNoBinding = invoiceNoBinding;
    }

    public RichInputComboboxListOfValues getInvoiceNoBinding() {
        return invoiceNoBinding;
    }

    public void setInvoiceDateBinding(RichInputDate invoiceDateBinding) {
        this.invoiceDateBinding = invoiceDateBinding;
    }

    public RichInputDate getInvoiceDateBinding() {
        return invoiceDateBinding;
    }

    public void setShippingBillNoBinding(RichInputText shippingBillNoBinding) {
        this.shippingBillNoBinding = shippingBillNoBinding;
    }

    public RichInputText getShippingBillNoBinding() {
        return shippingBillNoBinding;
    }

    public void setShippingBillDateBinding(RichInputDate shippingBillDateBinding) {
        this.shippingBillDateBinding = shippingBillDateBinding;
    }

    public RichInputDate getShippingBillDateBinding() {
        return shippingBillDateBinding;
    }

    public void setShippBillValueBinding(RichInputText shippBillValueBinding) {
        this.shippBillValueBinding = shippBillValueBinding;
    }

    public RichInputText getShippBillValueBinding() {
        return shippBillValueBinding;
    }

    public void setBrcValueBinding(RichInputText brcValueBinding) {
        this.brcValueBinding = brcValueBinding;
    }

    public RichInputText getBrcValueBinding() {
        return brcValueBinding;
    }

    public void setInitialValueBinding(RichInputText initialValueBinding) {
        this.initialValueBinding = initialValueBinding;
    }

    public RichInputText getInitialValueBinding() {
        return initialValueBinding;
    }

    public void setActualReceiveBinding(RichInputText actualReceiveBinding) {
        this.actualReceiveBinding = actualReceiveBinding;
    }

    public RichInputText getActualReceiveBinding() {
        return actualReceiveBinding;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setMyPageRootComponent(RichPanelHeader myPageRootComponent) {
        this.myPageRootComponent = myPageRootComponent;
    }

    public RichPanelHeader getMyPageRootComponent() {
        return myPageRootComponent;
    }
    
    private void cevmodecheck(){
          if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
              makeComponentHierarchyReadOnly(this.getMyPageRootComponent(), true);
              cevModeDisableComponent("V");
          }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
              makeComponentHierarchyReadOnly(this.getMyPageRootComponent(), false);
              cevModeDisableComponent("C");
          }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
              makeComponentHierarchyReadOnly(this.getMyPageRootComponent(), false);
              cevModeDisableComponent("E");
          }
      }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
              try {
                  Method method1 =
                      component.getClass().getMethod("setDisabled", boolean.class);
                  if (method1 != null) {
                      method1.invoke(component, valueComponent);
                  }
              } catch (NoSuchMethodException e) {
                  try {
                      Method method =
                          component.getClass().getMethod("setReadOnly", boolean.class);
                      if (method != null) {
                          method.invoke(component, valueComponent);
                      }
                  } catch (Exception e1) {
                      // e.printStackTrace();//silently eat this exception.
                  }


              } catch (Exception e) {
                  // e.printStackTrace();//silently eat this exception.
              }
              List<UIComponent> childComponents = component.getChildren();
              for (UIComponent comp : childComponents) {
                  makeComponentHierarchyReadOnly(comp, valueComponent);
              }
          }
    
    
    public void cevModeDisableComponent(String mode) {
           if (mode.equals("E")) {
               getUnitCodeBinding().setDisabled(true);
               getDocNoBinding().setDisabled(true);
              // getEditButtonBinding().setDisabled(true);
//               getInvoiceNoBinding().setDisabled(true);
               getInvoiceDateBinding().setDisabled(true);
               getShippingBillNoBinding().setDisabled(true);
               getShippingBillDateBinding().setDisabled(true);
               getShippBillValueBinding().setDisabled(true);
               getBrcValueBinding().setDisabled(true);
               getInitialValueBinding().setDisabled(true);
           } else if (mode.equals("C")) 
           {
                getUnitCodeBinding().setDisabled(true);
               getDocNoBinding().setDisabled(true);
               getDocDateBinding().setDisabled(true);
//               getMeisNoBinding().setDisabled(true);
//               getMeisDateBinding().setDisabled(true);
//               getMeisFcrNoBinding().setDisabled(true);
//               getMeisFcrDateBinding().setDisabled(true);
               //getEditButtonBinding().setDisabled(true);
               getInvoiceDateBinding().setDisabled(true);
               getShippingBillNoBinding().setDisabled(true);
               getShippingBillDateBinding().setDisabled(true);
               getShippBillValueBinding().setDisabled(true);
               getBrcValueBinding().setDisabled(true);
               getInitialValueBinding().setDisabled(true);
           } else if (mode.equals("V")) 
           {
               //getEditButtonBinding().setDisabled(false);
           }
           
       }


    public void deleteButtonDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                    {
                    ADFUtils.findOperation("Delete").execute();
                    System.out.println("Record Delete Successfully");
                    ADFUtils.showMessage("Record Deleted Successfully",2);
 
                    }
                    AdfFacesContext.getCurrentInstance().addPartialTarget(meisTableBinding); 

    }

    public void setMeisTableBinding(RichTable meisTableBinding) {
        this.meisTableBinding = meisTableBinding;
    }

    public RichTable getMeisTableBinding() {
        return meisTableBinding;
    }

    public void setSumOfRecTransBinding(RichInputText sumOfRecTransBinding) {
        this.sumOfRecTransBinding = sumOfRecTransBinding;
    }

    public RichInputText getSumOfRecTransBinding() {
        return sumOfRecTransBinding;
    }

    public void valueOfAceReceiveVCE(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(valueChangeEvent!=null){
            meisValBinding.setValue(sumOfRecTransBinding.getValue());
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(meisValBinding);

    }

    public void invVCE(ValueChangeEvent valueChangeEvent)
    {

        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(valueChangeEvent!=null){
            actualReceiveBinding.setValue(initialValueBinding.getValue());
            meisValBinding.setValue(sumOfRecTransBinding.getValue());
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(actualReceiveBinding);
        AdfFacesContext.getCurrentInstance().addPartialTarget(meisValBinding);

    }
}
