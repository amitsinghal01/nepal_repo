package terms.fin.transaction.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;

import oracle.binding.OperationBinding;

public class CreateMemorendumVouBean {
    private RichOutputText bindingOutputText;
    private RichPanelHeader myPageRootComponent;
    private RichButton editButtonBinding;
    private RichInputDate detailMemoDateBinding;
    private RichInputDate memoDateHeaderBinding;
    private RichInputText docNoBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichTable detailTableBinding;
    private RichInputText totalCreditBinding;
    private RichInputText totalDebitBinding;
    private RichInputComboboxListOfValues preparedByBinding;
    private RichInputComboboxListOfValues approvedByBinding;
    private RichInputText diffBinding;

    public CreateMemorendumVouBean() {
    }

    public void saveButtonAL(ActionEvent actionEvent) 
    {
    
    OperationBinding opr = ADFUtils.findOperation("diffValueMemorendumVoucherDetail");
    opr.execute();
    System.out.println("Result is----------->"+opr.getResult());
    if(opr.getResult()!=null)
    {
        BigDecimal result = (BigDecimal)opr.getResult();
    if(result.compareTo(new BigDecimal(0))==0)
    {
        System.out.println("---------------Inside----------------");
        OperationBinding oprDocNo = ADFUtils.findOperation("docNoGenerateMemorendumVoucher");
        oprDocNo.execute();
        if(oprDocNo.getResult()!=null)
        {
         if(oprDocNo.getResult().toString().equals("Y"))
         {             
        ADFUtils.findOperation("Commit").execute();
        ADFUtils.showMessage("Record Save Successfully.New Doc. No. is "+docNoBinding.getValue(),2);
         }
         else
         {
            ADFUtils.findOperation("Commit").execute();
            ADFUtils.showMessage("Record Update Successfully",2);
         }
        }
        else
        {
            ADFUtils.showMessage("Problem in Doc. No. generation method,Please check.", 0);
        
        }
        
    }
    else
    {
    ADFUtils.showMessage("Debit-Credit value must be equals.", 0);
    }
    }
    
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setMyPageRootComponent(RichPanelHeader myPageRootComponent) {
        this.myPageRootComponent = myPageRootComponent;
    }

    public RichPanelHeader getMyPageRootComponent() {
        return myPageRootComponent;
    }

    public void setEditButtonBinding(RichButton editButtonBinding) {
        this.editButtonBinding = editButtonBinding;
    }

    public RichButton getEditButtonBinding() {
        return editButtonBinding;
    }

    public void editButtonAL(ActionEvent actionEvent) 
    {
        if (approvedByBinding.getValue() == null) {
            cevmodecheck();
        } else {
            ADFUtils.showMessage("Record is Approved Can't be modified !", 2);
            ADFUtils.setEL("#{pageFlowScope.mode}", "V");
        }
    }
    
    private void cevmodecheck(){
            if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
                makeComponentHierarchyReadOnly(this.getMyPageRootComponent(), true);
                cevModeDisableComponent("V");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
                makeComponentHierarchyReadOnly(this.getMyPageRootComponent(), false);
                cevModeDisableComponent("C");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
                makeComponentHierarchyReadOnly(this.getMyPageRootComponent(), false);
                cevModeDisableComponent("E");
            }
        }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                try {
                    Method method1 =
                        component.getClass().getMethod("setDisabled", boolean.class);
                    if (method1 != null) {
                        method1.invoke(component, valueComponent);
                    }
                } catch (NoSuchMethodException e) {
                    try {
                        Method method =
                            component.getClass().getMethod("setReadOnly", boolean.class);
                        if (method != null) {
                            method.invoke(component, valueComponent);
                        }
                    } catch (Exception e1) {
                        // e.printStackTrace();//silently eat this exception.
                    }


                } catch (Exception e) {
                    // e.printStackTrace();//silently eat this exception.
                }
                List<UIComponent> childComponents = component.getChildren();
                for (UIComponent comp : childComponents) {
                    makeComponentHierarchyReadOnly(comp, valueComponent);
                }
            }
    
    public void cevModeDisableComponent(String mode) 
    {
            if (mode.equals("E"))
            {
                getDocNoBinding().setDisabled(true);
                getMemoDateHeaderBinding().setDisabled(true);
                getDetailMemoDateBinding().setDisabled(true);
                getUnitCodeBinding().setDisabled(true);
                getEditButtonBinding().setDisabled(true);
                getTotalCreditBinding().setDisabled(true);
                getTotalDebitBinding().setDisabled(true);                
                getPreparedByBinding().setDisabled(true);
                getDiffBinding().setDisabled(true);
            } 
            else if (mode.equals("C"))
            {
                getDocNoBinding().setDisabled(true);
                getDetailMemoDateBinding().setDisabled(true);
                getUnitCodeBinding().setDisabled(true);
                getEditButtonBinding().setDisabled(true);
                getTotalCreditBinding().setDisabled(true);
                getTotalDebitBinding().setDisabled(true);
                getDiffBinding().setDisabled(true);
                getPreparedByBinding().setDisabled(true);

            } else if (mode.equals("V"))
            {
               // getEditButtonBinding().setDisabled(false); 
            }
            
     }

    public void setDetailMemoDateBinding(RichInputDate detailMemoDateBinding) {
        this.detailMemoDateBinding = detailMemoDateBinding;
    }

    public RichInputDate getDetailMemoDateBinding() {
        return detailMemoDateBinding;
    }


    public void setMemoDateHeaderBinding(RichInputDate memoDateHeaderBinding) {
        this.memoDateHeaderBinding = memoDateHeaderBinding;
    }

    public RichInputDate getMemoDateHeaderBinding() {
        return memoDateHeaderBinding;
    }

    public String saveAndCloseButtonAL()
    {
    
    OperationBinding opr = ADFUtils.findOperation("diffValueMemorendumVoucherDetail");
    opr.execute();
    System.out.println("Result is----------->"+opr.getResult());
    if(opr.getResult()!=null)
    {
        BigDecimal result = (BigDecimal)opr.getResult();
    if(result.compareTo(new BigDecimal(0))==0)
    {
        System.out.println("---------------Inside----------------");
        OperationBinding oprDocNo = ADFUtils.findOperation("docNoGenerateMemorendumVoucher");
        oprDocNo.execute();
        System.out.println("Doc No Result:"+oprDocNo.getResult());
        
        if(oprDocNo.getResult()!=null)
        {
         if(oprDocNo.getResult().toString().equals("Y"))
         {             
        ADFUtils.findOperation("Commit").execute();
        ADFUtils.showMessage("Record Save Successfully.New Doc. No. is "+docNoBinding.getValue(),2);
        return "SaveAndClose";
         }
         else
         {
            ADFUtils.findOperation("Commit").execute();
            ADFUtils.showMessage("Record Update Successfully",2);
             return "SaveAndClose";
         }
        }
        else
        {
            ADFUtils.showMessage("Problem in Doc. No. generation method,Please check.", 0);
            return "SaveAndClose";
        }
        
    }
    else
    {
    ADFUtils.showMessage("Debit-Credit value must be equals.", 0);
    }
    }
    return null;
    }

    public void setDocNoBinding(RichInputText docNoBinding) {
        this.docNoBinding = docNoBinding;
    }

    public RichInputText getDocNoBinding() {
        return docNoBinding;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void deleteButtonDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                    {
                    ADFUtils.findOperation("Delete").execute();
                    System.out.println("Record Delete Successfully");
                    ADFUtils.showMessage("Record Deleted Successfully",2);
        
                    }
                    AdfFacesContext.getCurrentInstance().addPartialTarget(detailTableBinding); 
    }

    public void setDetailTableBinding(RichTable detailTableBinding) {
        this.detailTableBinding = detailTableBinding;
    }

    public RichTable getDetailTableBinding() {
        return detailTableBinding;
    }

    public void setTotalCreditBinding(RichInputText totalCreditBinding) {
        this.totalCreditBinding = totalCreditBinding;
    }

    public RichInputText getTotalCreditBinding() {
        return totalCreditBinding;
    }

    public void setTotalDebitBinding(RichInputText totalDebitBinding) {
        this.totalDebitBinding = totalDebitBinding;
    }

    public RichInputText getTotalDebitBinding() {
        return totalDebitBinding;
    }

    public void ApproveByVCE(ValueChangeEvent valueChangeEvent) {

        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        OperationBinding ob = ADFUtils.findOperation("checkApprovalStatus");
        ob.getParamsMap().put("formNm", "FINMMV");
        ob.getParamsMap().put("authoLim", "AP");
        ob.getParamsMap().put("empcd", valueChangeEvent.getNewValue().toString());
        ob.execute();
        System.out.println(" EMP CD" + valueChangeEvent.getNewValue().toString());
        if ((ob.getResult() != null && ob.getResult().equals("Y"))) {
            Row row = (Row) ADFUtils.evaluateEL("#{bindings.MemorendumVoucherHeaderVO1Iterator.currentRow}");
            row.setAttribute("ApprovedBy", null);
            ADFUtils.showMessage("You Don't Have Permission To Approve This Record.", 0);
        }

        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
    }

    public void setPreparedByBinding(RichInputComboboxListOfValues preparedByBinding) {
        this.preparedByBinding = preparedByBinding;
    }

    public RichInputComboboxListOfValues getPreparedByBinding() {
        return preparedByBinding;
    }

    public void setApprovedByBinding(RichInputComboboxListOfValues approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputComboboxListOfValues getApprovedByBinding() {
        return approvedByBinding;
    }

    public void VerifiedByVCE(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        OperationBinding ob = ADFUtils.findOperation("checkApprovalStatus");
        ob.getParamsMap().put("formNm", "FINMMV");
        ob.getParamsMap().put("authoLim", "VR");
        ob.getParamsMap().put("empcd", valueChangeEvent.getNewValue().toString());
        ob.execute();
        System.out.println(" EMP CD" + valueChangeEvent.getNewValue().toString());
        if ((ob.getResult() != null && ob.getResult().equals("Y"))) {
            Row row = (Row) ADFUtils.evaluateEL("#{bindings.MemorendumVoucherHeaderVO1Iterator.currentRow}");
            row.setAttribute("VerifiedBy", null);
            ADFUtils.showMessage("You Don't Have Permission To Verify This Record.", 0);
        }
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
    }

    public void setDiffBinding(RichInputText diffBinding) {
        this.diffBinding = diffBinding;
    }

    public RichInputText getDiffBinding() {
        return diffBinding;
    }
}
