package terms.fin.transaction.ui.bean;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.application.FacesMessage;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import javax.faces.validator.ValidatorException;

import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

import oracle.jbo.domain.Date;

import org.apache.myfaces.trinidad.event.DisclosureEvent;

public class CreatePurchaseBillBean implements Serializable{
    @SuppressWarnings("compatibility:-1663076405977982050")
    private static final long serialVersionUID = -6124339926329223753L;
    private RichTable detailTableBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText billNoBinding;
    private RichInputDate billEntryDateBinding;
    private RichPopup contractOrderPopupBinding;
    private RichInputText poNoBinding;
    private RichInputText valueBinding;
    private RichInputText sgstPerBinding;
    private RichInputText cgstPerBinding;
    private RichInputText igstPerBinding;
    private RichInputText hsnNoBinding;
    private RichSelectOneChoice typeBinding;
    private RichInputDate srvDateBinding;
    private RichInputComboboxListOfValues vendCodeBinding;
    private RichShowDetailItem srvDetailTabBinding;
    private RichShowDetailItem itemDetailBinding;
    private RichShowDetailItem incomeTaxTabBinding;
    private RichShowDetailItem purchaseBillTabBinding;
    private RichInputText mtlCostBinding;
    private RichInputText appMtlCostBinding;
    private RichInputText igstCostBinding;
    private RichInputText appIgstBinding;
    private RichInputText cgstCostBinding;
    private RichInputText appCgstBinding;
    private RichInputText sgstCostBinding;
    private RichInputText appSgstBinding;
    private RichInputText othersBinding;
    private RichInputText appOthersBinding;
    private RichInputText netTotalBinding;
    private RichInputText dspApprBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichOutputText bindingOutputText;
    private RichButton headerEditBinding;
    private RichInputText tdsPerBinding;
    private RichInputText tdsAmountBinding;
    private RichInputText sgstAmountBinding;
    private RichInputText cgstAmountBinding;
    private RichInputText igstAmountBinding;
    private RichInputText billAmountBinding;
    private RichInputText vendorBillAmountBinding;
    private RichInputText glCodeBinding;
    private RichInputText vendorTypeBinding;
    private RichInputDate dueDateBinding;
    private RichInputText approvedBillAmountBinding;
    private RichInputText ecessBinding;
    private RichInputText tdsCodeBinding;
    private RichInputText tdsSurBinding;
    private RichInputText ecessGlCodeBinding;
    private RichInputComboboxListOfValues tdsPaymentCodeBinding;
    private RichInputDate billEntryDate1Binding;
    private RichInputText tdsAmount1Binding;
    private RichTable srvTableBinding;
    private RichInputText debitAmountBinding;
    private RichInputText ecessAmountPBSBinding;
    private RichInputText roundOffBinding;
    private RichInputText vendorTotalTrans;
    private RichInputComboboxListOfValues venBillNoBinding;
    private RichShowDetailItem purchaseBillDetailTabBinding;
    private RichInputComboboxListOfValues preparedByBinding;
    private RichInputComboboxListOfValues approvedByBinding;
    //   private RichOutputText totalValueBinding;
    private RichOutputText totalValueOutPutBinding;
    private RichButton populateItemDetailButtonBinding;
    private String PageMode = "C";
    private RichInputDate lastBillPassingDateBinding;
    private RichButton detailDeleteButtonBinding;
    private String DateFlag = "";
    private RichInputComboboxListOfValues srvNoBinding;
    private RichInputDate srvDateSrvHeadBinding;
    private RichInputText invoiceNoBinding;
    private RichInputDate invoiceDateBinding;
    private RichInputText challanNoBinding;
    private RichInputDate challanDateBinding;
    private RichInputDate approvalDateBinding;
    private RichInputText vouNo1Binding;
    private RichInputText tdsLimitBinding;
    private RichOutputText diffrenceValueBinding;
    private RichInputText editTransBinding;
    private RichTable billPVTableBinding;
    private RichPopup billJVPopupBinding;
    private RichSelectOneChoice gstExemptedBinding;
    private RichSelectOneChoice gstRCMApplyBinding;
    private RichInputText parentBillNoBinding;
    private String tab_flag = "N";
    private RichButton billJVCreateBinding;
    private RichButton billJVDeleteButtonBinding;
    private RichInputText billVouchNoBinding;
    private RichInputText tdsVouchNoBinding;
    private String srv_flag = "Y";
    private RichInputText costCenterDebitNoteBinding;
    private RichInputText amountDebitNoteBinding;
    private RichSelectOneChoice drCrflagDebitNoteBinding;
    private RichInputText subCodeDebitNoteBinding;
    private RichInputText glCodeDebitNoteBinding;
    private RichShowDetailItem debitNoteTabBinding;
    private RichInputText amdNoBinding;


    public CreatePurchaseBillBean() {
    }

    private static ADFLogger logger = ADFLogger.createADFLogger(CreatePurchaseBillBean.class);
    
    public void deletPopupDL(DialogEvent dialogEvent) 
    {
        if (dialogEvent.getOutcome().name().equals("ok")) 
        {
            ADFUtils.findOperation("Delete1").execute();
            OperationBinding Opr = ADFUtils.findOperation("changeGstCalculationValuePurchaseBill");
            Opr.getParamsMap().put("TypeVal", "Del");
            Opr.execute();
            ADFUtils.findOperation("setItemDetailDataInBillJVPurchaseBill").execute();
            logger.info("Record Delete Successfully");
            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(detailTableBinding);
    }

    public void setDetailTableBinding(RichTable detailTableBinding) {
        this.detailTableBinding = detailTableBinding;
    }

    public RichTable getDetailTableBinding() {
        return detailTableBinding;
    }

    public void saveButtonAL(ActionEvent actionEvent) 
    {
        try 
        {
            ADFUtils.findOperation("narrationValuePurchaseBill").execute();
        } catch (Exception e) {
            // TODO: Add catch code
            e.printStackTrace();
        }

        if (srv_flag.equalsIgnoreCase("Y")) 
        {
            if (tab_flag.equalsIgnoreCase("Y")) 
            {
                String ResultCM = "";
                BigDecimal diff_amount = new BigDecimal(0);

                try {
                    OperationBinding Opr1 = ADFUtils.findOperation("makerCheckerForPurchseBill");
                    Object Obj1 = Opr1.execute();
                    if (Opr1.getResult() != null) {
                        ResultCM = Opr1.getResult().toString();
                    }
                } catch (Exception ee) {
                    ee.printStackTrace();
                }
                System.out.println("difference amounttttt amit "+diffrenceValueBinding.getValue());
                diff_amount =(BigDecimal) (diffrenceValueBinding.getValue() != null ? diffrenceValueBinding.getValue() :
                                                                                        new BigDecimal(0));
                if (diff_amount.compareTo(new BigDecimal(0)) == 0) 
                {
                    logger.info("RESULT ResultCM IS:" + ResultCM + "   Approval Value:" +
                                       approvedByBinding.getValue() + "  PAGE MODE:" + PageMode);
                    if ((ResultCM.equalsIgnoreCase("Y") && approvedByBinding.getValue() == null && PageMode.equals("C")) ||
                        ((ResultCM.equals("N")) && PageMode.equalsIgnoreCase("C") &&
                         approvedByBinding.getValue() != null)) 
                    {
                        OperationBinding Opr = ADFUtils.findOperation("generatePurchaseBillNo");
                        //Opr.getParamsMap().put("UserName","SWE161");
                        logger.info("USER NAME IN BEAN:" +ADFUtils.resolveExpression("#{pageFlowScope.empCode}"));
                        
                        Opr.getParamsMap().put("UserName", ADFUtils.resolveExpression("#{pageFlowScope.empCode}"));
                        Object Obj = Opr.execute();
                        System.out.println("******Result is:" + Opr.getResult());
                       
                        if (Opr.getResult() != null && Opr.getResult().equals("S")) 
                        {
                            ADFUtils.findOperation("Commit").execute();
                            ADFUtils.showMessage("Record Saved Successfully.New Purchase Bill No. is " +
                                                 ADFUtils.evaluateEL("#{bindings.BillNo.inputValue}"), 2);
                            vouNumberForMessage();
                            if (getApprovedByBinding().getValue() != null) 
                            {
                                ADFUtils.setEL("#{pageFlowScope.mode}", "V");
                                cevmodecheck();
                                AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
                            }
                        } else if (Opr.getResult() != null && Opr.getResult().equals("U")) {
                            ADFUtils.findOperation("Commit").execute();
                            ADFUtils.showMessage("Record Updated Successfully.", 2);
                            vouNumberForMessage();
                            // ADFUtils.showMessage(voucher_no, 2);
                            if (getApprovedByBinding().getValue() != null) {
                                ADFUtils.setEL("#{pageFlowScope.mode}", "V");
                                cevmodecheck();
                                AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
                            }
                        } else if (Opr.getResult() != null && Opr.getResult().equals("SrvError")) {
                            ADFUtils.showMessage("Problem In Data Save In SRV.", 0);
                        } else if (Opr.getResult() != null && Opr.getResult().equals("VR")) {
                            ADFUtils.showMessage("Problem In Generate Voucher No.", 0);
                        }
                    } else {
                        if (ResultCM.equalsIgnoreCase("N") && approvedByBinding.getValue() == null) {
                            ADFUtils.showMessage("Please Approved This Record.", 0);
                        }
                    }
                    if (PageMode.equalsIgnoreCase("E")) 
                    {

                        OperationBinding Opr = ADFUtils.findOperation("generatePurchaseBillNo");
                        Opr.getParamsMap().put("UserName", ADFUtils.resolveExpression("#{pageFlowScope.empCode}"));
                        Object Obj = Opr.execute();
                        System.out.println("******Result is:" + Opr.getResult());
                        //                    try
                        //                    {
                        //                    voucher_no=vouNumberForMessage();
                        //                    }
                        //                    catch (Exception ee)
                        //                    {
                        //                    ee.printStackTrace();
                        //                    }

                        if (Opr.getResult() != null && Opr.getResult().equals("S")) {
                            ADFUtils.findOperation("Commit").execute();
                            ADFUtils.showMessage("Record Saved Successfully.New Purchase Bill No. is " +
                                                     ADFUtils.evaluateEL("#{bindings.BillNo.inputValue}"), 2);
                            vouNumberForMessage();
                            //ADFUtils.showMessage(voucher_no, 2);

                        } else if (Opr.getResult() != null && Opr.getResult().equals("U")) {
                            ADFUtils.findOperation("Commit").execute();
                            ADFUtils.showMessage("Record Updated Successfully.", 2);
                            vouNumberForMessage();
                            //ADFUtils.showMessage(voucher_no, 2);

                            if (getApprovedByBinding().getValue() != null) {
                                ADFUtils.setEL("#{pageFlowScope.mode}", "V");
                                cevmodecheck();
                                AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
                            }
                        } else if (Opr.getResult() != null && Opr.getResult().equals("SrvError")) {
                            ADFUtils.showMessage("Problem In Data Save In SRV.", 2);
                        } else if (Opr.getResult() != null && Opr.getResult().equals("VR")) {
                            ADFUtils.showMessage("Problem In Generate Voucher No.", 2);
                        }
                    }
                } else {
                    ADFUtils.showMessage("Debit Credit Amount Not Equals.Please Check", 0);
                }
            } else {
                ADFUtils.showMessage("Please open Purchase Bill Summary Tab.", 0);
            }
        } else {
            ADFUtils.showMessage("Please Click On Populate Item Detail Button in SRV Detail Tab.", 0);
        }
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setBillNoBinding(RichInputText billNoBinding) {
        this.billNoBinding = billNoBinding;
    }

    public RichInputText getBillNoBinding() {
        return billNoBinding;
    }

    public void setBillEntryDateBinding(RichInputDate billEntryDateBinding) {
        this.billEntryDateBinding = billEntryDateBinding;
    }

    public RichInputDate getBillEntryDateBinding() {
        return billEntryDateBinding;
    }

    public void setContractOrderPopupBinding(RichPopup contractOrderPopupBinding) {
        this.contractOrderPopupBinding = contractOrderPopupBinding;
    }

    public RichPopup getContractOrderPopupBinding() {
        return contractOrderPopupBinding;
    }

    public void contractOrderVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (ADFUtils.evaluateEL("#{bindings.ListContract.inputValue}").equals("Y")) {
            ADFUtils.showPopup(contractOrderPopupBinding);
        }
    }


    public void contractOrderPopulateData() {
        ADFUtils.findOperation("CreateInsert1").execute();
    }

    public void roundOffVCE(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        BigDecimal zero = new BigDecimal(0);
        String check_value = "N";
        if (valueChangeEvent.getNewValue() != null) {
            BigDecimal Check = (BigDecimal) valueChangeEvent.getNewValue();
            if (Check.compareTo(zero) == 0) {
                check_value = "Y";
            }


            System.out.println("Check Value = " + Check);
            System.out.println("Compare value = " + Check.compareTo(new BigDecimal(1)));
            if (Check.compareTo(new BigDecimal(1)) < 1)
                ADFUtils.findOperation("roundOffPurchaseBill").execute();
        }

        if ((valueChangeEvent.getNewValue() == null || check_value.equalsIgnoreCase("Y"))) {
            refreshValueOnChangeOfTdsAndRoundOff("Y");
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(billPVTableBinding);
    }

    public void gstCalculationVCE(ValueChangeEvent vce) 
    {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce != null) 
        {
            ADFUtils.findOperation("gstCalculationPurchaseBill").execute();
            ADFUtils.findOperation("setItemDetailDataInBillJVPurchaseBill").execute();
        }
    }

    public void srvNoVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce != null) {
            OperationBinding opr1 = ADFUtils.findOperation("checkRejectionSumPurchaseBill");
            opr1.execute();
            System.out.println("REJ METHOD RESULT IS====>" + opr1.getResult());
            if (opr1.getResult().equals("Y")) {
                ADFUtils.findOperation("populateSrvDetailDataPurchaseBill").execute();
                //   ADFUtils.findOperation("venLedgerPurchaseBill").execute();
                ADFUtils.findOperation("populateDataInItemDetailPurchaseBill").execute();
                //    AdfFacesContext.getCurrentInstance().addPartialTarget(detailTableBinding);
                getVenBillNoBinding().setDisabled(true);
                getBillEntryDateBinding().setDisabled(true);
                getLastBillPassingDateBinding().setDisabled(true);
                AdfFacesContext.getCurrentInstance().addPartialTarget(venBillNoBinding);
                AdfFacesContext.getCurrentInstance().addPartialTarget(billEntryDateBinding);
                AdfFacesContext.getCurrentInstance().addPartialTarget(lastBillPassingDateBinding);

                if ((Long) ADFUtils.evaluateEL("#{bindings.PurchaseBillDetailVO1Iterator.estimatedRowCount}") > 0) {
                    ADFUtils.showMessage("Data Successfully Populated In Item Detail.", 2);
                } else {
                    ADFUtils.showMessage("Not Data Found For Item Detail.", 2);
                }

                try {
                    ADFUtils.findOperation("tdsCalculationPurchaseBill").execute();
                } catch (Exception e) {
                    // TODO: Add catch code
                    e.printStackTrace();
                }

            } else {
                ADFUtils.showMessage("Bill Can't be pass due to rejection of quantity.", 2);
            }

            try {
                ADFUtils.findOperation("narrationValuePurchaseBill").execute();
            } catch (Exception e) {
                // TODO: Add catch code
                e.printStackTrace();
            }

        }
    }

    public void purchaseBillDCL(DisclosureEvent disclosureEvent) {
        try {
            ADFUtils.findOperation("changeValuesOnPurchaseBill").execute();
        } catch (Exception e) {
            // TODO: Add catch code
            e.printStackTrace();
        }
        tab_flag = "Y";
        try {
            OperationBinding Opr1 = ADFUtils.findOperation("makerCheckerForPurchseBill");
            Object Obj1 = Opr1.execute();
            System.out.println("MAKER CHECKER VALUE IS :" + Opr1.getResult());
            if (Opr1.getResult() != null && Opr1.getResult().equals("Y") && PageMode.equalsIgnoreCase("C")) {
                getApprovedByBinding().setDisabled(true);
                getApprovalDateBinding().setDisabled(true);
            }
        } catch (Exception ee) {
            System.out.println("Problem In purchaseBillDCL");
            ee.printStackTrace();
        }

    }

    public void tdsPaymentCodeVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        refreshValueOnChangeOfTdsAndRoundOff("N");
        //   ADFUtils.findOperation("tdsCalculationPurchaseBill").execute();
        ADFUtils.findOperation("tdsCalculationOnPaymentAmountPurchaseBill").execute();

    }

    public void setPoNoBinding(RichInputText poNoBinding) {
        this.poNoBinding = poNoBinding;
    }

    public RichInputText getPoNoBinding() {
        return poNoBinding;
    }

    public void setValueBinding(RichInputText valueBinding) {
        this.valueBinding = valueBinding;
    }

    public RichInputText getValueBinding() {
        return valueBinding;
    }

    public void setSgstPerBinding(RichInputText sgstPerBinding) {
        this.sgstPerBinding = sgstPerBinding;
    }

    public RichInputText getSgstPerBinding() {
        return sgstPerBinding;
    }

    public void setCgstPerBinding(RichInputText cgstPerBinding) {
        this.cgstPerBinding = cgstPerBinding;
    }

    public RichInputText getCgstPerBinding() {
        return cgstPerBinding;
    }

    public void setIgstPerBinding(RichInputText igstPerBinding) {
        this.igstPerBinding = igstPerBinding;
    }

    public RichInputText getIgstPerBinding() {
        return igstPerBinding;
    }

    public void setHsnNoBinding(RichInputText hsnNoBinding) {
        this.hsnNoBinding = hsnNoBinding;
    }

    public RichInputText getHsnNoBinding() {
        return hsnNoBinding;
    }

    public void setTypeBinding(RichSelectOneChoice typeBinding) {
        this.typeBinding = typeBinding;
    }

    public RichSelectOneChoice getTypeBinding() {
        return typeBinding;
    }

    public void setSrvDateBinding(RichInputDate srvDateBinding) {
        this.srvDateBinding = srvDateBinding;
    }

    public RichInputDate getSrvDateBinding() {
        return srvDateBinding;
    }

    public void setVendCodeBinding(RichInputComboboxListOfValues vendCodeBinding) {
        this.vendCodeBinding = vendCodeBinding;
    }

    public RichInputComboboxListOfValues getVendCodeBinding() {
        return vendCodeBinding;
    }

    public void setSrvDetailTabBinding(RichShowDetailItem srvDetailTabBinding) {
        this.srvDetailTabBinding = srvDetailTabBinding;
    }

    public RichShowDetailItem getSrvDetailTabBinding() {
        return srvDetailTabBinding;
    }

    public void setItemDetailBinding(RichShowDetailItem itemDetailBinding) {
        this.itemDetailBinding = itemDetailBinding;
    }

    public RichShowDetailItem getItemDetailBinding() {
        return itemDetailBinding;
    }

    public void setIncomeTaxTabBinding(RichShowDetailItem incomeTaxTabBinding) {
        this.incomeTaxTabBinding = incomeTaxTabBinding;
    }

    public RichShowDetailItem getIncomeTaxTabBinding() {
        return incomeTaxTabBinding;
    }

    public void setPurchaseBillTabBinding(RichShowDetailItem purchaseBillTabBinding) {
        this.purchaseBillTabBinding = purchaseBillTabBinding;
    }

    public RichShowDetailItem getPurchaseBillTabBinding() {
        return purchaseBillTabBinding;
    }

    public void setMtlCostBinding(RichInputText mtlCostBinding) {
        this.mtlCostBinding = mtlCostBinding;
    }

    public RichInputText getMtlCostBinding() {
        return mtlCostBinding;
    }

    public void setAppMtlCostBinding(RichInputText appMtlCostBinding) {
        this.appMtlCostBinding = appMtlCostBinding;
    }

    public RichInputText getAppMtlCostBinding() {
        return appMtlCostBinding;
    }

    public void setIgstCostBinding(RichInputText igstCostBinding) {
        this.igstCostBinding = igstCostBinding;
    }

    public RichInputText getIgstCostBinding() {
        return igstCostBinding;
    }

    public void setAppIgstBinding(RichInputText appIgstBinding) {
        this.appIgstBinding = appIgstBinding;
    }

    public RichInputText getAppIgstBinding() {
        return appIgstBinding;
    }

    public void setCgstCostBinding(RichInputText cgstCostBinding) {
        this.cgstCostBinding = cgstCostBinding;
    }

    public RichInputText getCgstCostBinding() {
        return cgstCostBinding;
    }

    public void setAppCgstBinding(RichInputText appCgstBinding) {
        this.appCgstBinding = appCgstBinding;
    }

    public RichInputText getAppCgstBinding() {
        return appCgstBinding;
    }

    public void setSgstCostBinding(RichInputText sgstCostBinding) {
        this.sgstCostBinding = sgstCostBinding;
    }

    public RichInputText getSgstCostBinding() {
        return sgstCostBinding;
    }

    public void setAppSgstBinding(RichInputText appSgstBinding) {
        this.appSgstBinding = appSgstBinding;
    }

    public RichInputText getAppSgstBinding() {
        return appSgstBinding;
    }

    public void setOthersBinding(RichInputText othersBinding) {
        this.othersBinding = othersBinding;
    }

    public RichInputText getOthersBinding() {
        return othersBinding;
    }

    public void setAppOthersBinding(RichInputText appOthersBinding) {
        this.appOthersBinding = appOthersBinding;
    }

    public RichInputText getAppOthersBinding() {
        return appOthersBinding;
    }

    public void setNetTotalBinding(RichInputText netTotalBinding) {
        this.netTotalBinding = netTotalBinding;
    }

    public RichInputText getNetTotalBinding() {
        return netTotalBinding;
    }

    public void setDspApprBinding(RichInputText dspApprBinding) {
        this.dspApprBinding = dspApprBinding;
    }

    public RichInputText getDspApprBinding() {
        return dspApprBinding;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }


    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }

    //Set Fields and Button disable.
    public void cevModeDisableComponent(String mode) {
        if (mode.equals("E")) {
            PageMode = "E";
            getHeaderEditBinding().setDisabled(true);
            //Purchase Bill Detail
            getUnitCodeBinding().setDisabled(true);

            getTypeBinding().setDisabled(true);
            getBillNoBinding().setDisabled(true);
            getSrvDateBinding().setDisabled(true);
            getVendCodeBinding().setDisabled(true);
            getBillAmountBinding().setDisabled(true);
            getVendorBillAmountBinding().setDisabled(true);
            getVendorTypeBinding().setDisabled(true);
            getDueDateBinding().setDisabled(true);
            getGlCodeBinding().setDisabled(true);
            getVendorTotalTrans().setDisabled(true);
            getVenBillNoBinding().setDisabled(true);
            getPreparedByBinding().setDisabled(true);
            getBillEntryDateBinding().setDisabled(true);
            getLastBillPassingDateBinding().setDisabled(true);
            getTdsLimitBinding().setDisabled(true);
            getBillVouchNoBinding().setDisabled(true);
            getTdsVouchNoBinding().setDisabled(true);
            getAmdNoBinding().setDisabled(true);
            //Item Detail
            getPoNoBinding().setDisabled(true);
            getValueBinding().setDisabled(true);
            getSgstPerBinding().setDisabled(true);
            getCgstPerBinding().setDisabled(true);
            getIgstPerBinding().setDisabled(true);
            getSgstAmountBinding().setDisabled(true);
            getCgstAmountBinding().setDisabled(true);
            getIgstAmountBinding().setDisabled(true);
            //Income-Tax -TDS
            getTdsPerBinding().setDisabled(true);
            getTdsAmountBinding().setDisabled(true);
            getEcessBinding().setDisabled(true);
            getEcessGlCodeBinding().setDisabled(true);
            getTdsCodeBinding().setDisabled(true);
            getTdsSurBinding().setDisabled(true);
            getTdsPerBinding().setDisabled(true);
            // getTdsPaymentCodeBinding().setDisabled(true);
            //Purchase Bill Summary
            getMtlCostBinding().setDisabled(true);
            getAppMtlCostBinding().setDisabled(true);
            getIgstCostBinding().setDisabled(true);
            getAppIgstBinding().setDisabled(true);
            getCgstCostBinding().setDisabled(true);
            getAppCgstBinding().setDisabled(true);
            getSgstCostBinding().setDisabled(true);
            getAppSgstBinding().setDisabled(true);
            getOthersBinding().setDisabled(true);
            getAppOthersBinding().setDisabled(true);
            getNetTotalBinding().setDisabled(true);
            getDspApprBinding().setDisabled(true);
            getApprovedBillAmountBinding().setDisabled(true);
            getBillEntryDate1Binding().setDisabled(true);
            getTdsAmount1Binding().setDisabled(true);
            getEcessAmountPBSBinding().setDisabled(true);
            getDebitAmountBinding().setDisabled(true);
            //getApprovalDateBinding().setDisabled(true);
            getGstExemptedBinding().setDisabled(true);
            getGstRCMApplyBinding().setDisabled(true);
            getRoundOffBinding().setDisabled(true);
            getBillJVCreateBinding().setDisabled(true);
            getBillJVDeleteButtonBinding().setDisabled(true);


            //      SRV Detail
            getPopulateItemDetailButtonBinding().setDisabled(true);
            getSrvNoBinding().setDisabled(true);
            getSrvDateSrvHeadBinding().setDisabled(true);
            getInvoiceNoBinding().setDisabled(true);
            getInvoiceDateBinding().setDisabled(true);
            getChallanNoBinding().setDisabled(true);
            getChallanDateBinding().setDisabled(true);
            getParentBillNoBinding().setDisabled(true);

            //Toolabr Button

            //                    if(getApprovedByBinding().getValue()!=null)
            //                    {
            //                    getSaveButtonBinding().setDisabled(true);
            //                    getSaveAndCloseButtonBinding().setDisabled(true);
            //                    }
            //Debit Note Tab

            getGlCodeDebitNoteBinding().setDisabled(true);
            getSubCodeDebitNoteBinding().setDisabled(true);
            //getDrCrflagDebitNoteBinding().setDisabled(true);
            //getAmountDebitNoteBinding().setDisabled(true);
            getCostCenterDebitNoteBinding().setDisabled(true);

        } else if (mode.equals("C")) {
            PageMode = "C";
            getHeaderEditBinding().setDisabled(true);
            //Purchase Bill Detail
            getUnitCodeBinding().setDisabled(true);
            getTypeBinding().setDisabled(true);
            getBillNoBinding().setDisabled(true);
            getSrvDateBinding().setDisabled(true);
            getVendCodeBinding().setDisabled(true);
            getBillAmountBinding().setDisabled(true);
            getVendorBillAmountBinding().setDisabled(true);
            getVendorTypeBinding().setDisabled(true);
            getDueDateBinding().setDisabled(true);
            getGlCodeBinding().setDisabled(true);
            getVendorTotalTrans().setDisabled(true);
            getPreparedByBinding().setDisabled(true);
            getTdsLimitBinding().setDisabled(true);
            getParentBillNoBinding().setDisabled(true);
            getBillVouchNoBinding().setDisabled(true);
            getTdsVouchNoBinding().setDisabled(true);
            getAmdNoBinding().setDisabled(true);

            //Item Detail
            getPoNoBinding().setDisabled(true);
            getValueBinding().setDisabled(true);
            getSgstPerBinding().setDisabled(true);
            getCgstPerBinding().setDisabled(true);
            getIgstPerBinding().setDisabled(true);
            //                  getSgstAmountBinding().setDisabled(true);
            //                  getCgstAmountBinding().setDisabled(true);
            //                  getIgstAmountBinding().setDisabled(true);
            //Income-Tax -TDS
            getTdsPerBinding().setDisabled(true);
            getTdsAmountBinding().setDisabled(true);
            getEcessBinding().setDisabled(true);
            getEcessGlCodeBinding().setDisabled(true);
            getTdsCodeBinding().setDisabled(true);
            getTdsSurBinding().setDisabled(true);
            getTdsPerBinding().setDisabled(true);
            //Purchase Bill Summary
            getMtlCostBinding().setDisabled(true);
            getAppMtlCostBinding().setDisabled(true);
            getIgstCostBinding().setDisabled(true);
            getAppIgstBinding().setDisabled(true);
            getCgstCostBinding().setDisabled(true);
            getAppCgstBinding().setDisabled(true);
            getSgstCostBinding().setDisabled(true);
            getAppSgstBinding().setDisabled(true);
            getOthersBinding().setDisabled(true);
            getAppOthersBinding().setDisabled(true);
            getNetTotalBinding().setDisabled(true);
            getDspApprBinding().setDisabled(true);
            getApprovedBillAmountBinding().setDisabled(true);
            getBillEntryDate1Binding().setDisabled(true);
            getTdsAmount1Binding().setDisabled(true);
            getEcessAmountPBSBinding().setDisabled(true);
            getDebitAmountBinding().setDisabled(true);
            //getApprovalDateBinding().setDisabled(true);
            getBillJVCreateBinding().setDisabled(true);
            getBillJVDeleteButtonBinding().setDisabled(true);


            //SRV DETAIL
            //getSrvNoBinding().setDisabled(true);
            getSrvDateSrvHeadBinding().setDisabled(true);
            getInvoiceNoBinding().setDisabled(true);
            getInvoiceDateBinding().setDisabled(true);
            getChallanNoBinding().setDisabled(true);
            getChallanDateBinding().setDisabled(true);


            //Debit Note Tab

            getGlCodeDebitNoteBinding().setDisabled(true);
            getSubCodeDebitNoteBinding().setDisabled(true);
            //getDrCrflagDebitNoteBinding().setDisabled(true);
            // getAmountDebitNoteBinding().setDisabled(true);
            getCostCenterDebitNoteBinding().setDisabled(true);

        } else if (mode.equals("V")) {
            //  PageMode="V";
            //                getDetailcreateBinding().setDisabled(true);
            //                getDetaildeleteBinding().setDisabled(true);
            getSrvDetailTabBinding().setDisabled(false);
            getItemDetailBinding().setDisabled(false);
            getPurchaseBillTabBinding().setDisabled(false);
            getIncomeTaxTabBinding().setDisabled(false);
            getPurchaseBillDetailTabBinding().setDisabled(false);
            getDetailDeleteButtonBinding().setDisabled(true);
            populateItemDetailButtonBinding.setDisabled(true);
            getDebitNoteTabBinding().setDisabled(false);
        }

    }


    public void editButtonAL(ActionEvent actionEvent) {
        if (approvedByBinding.getValue() == null) {
            cevmodecheck();
        } else {
            ADFUtils.showMessage("Approved Record Can't Be Modify.", 2);
            ADFUtils.setEL("#{pageFlowScope.mode}", "V");
        }
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setTdsPerBinding(RichInputText tdsPerBinding) {
        this.tdsPerBinding = tdsPerBinding;
    }

    public RichInputText getTdsPerBinding() {
        return tdsPerBinding;
    }

    public void setTdsAmountBinding(RichInputText tdsAmountBinding) {
        this.tdsAmountBinding = tdsAmountBinding;
    }

    public RichInputText getTdsAmountBinding() {
        return tdsAmountBinding;
    }

    public void setSgstAmountBinding(RichInputText sgstAmountBinding) {
        this.sgstAmountBinding = sgstAmountBinding;
    }

    public RichInputText getSgstAmountBinding() {
        return sgstAmountBinding;
    }

    public void setCgstAmountBinding(RichInputText cgstAmountBinding) {
        this.cgstAmountBinding = cgstAmountBinding;
    }

    public RichInputText getCgstAmountBinding() {
        return cgstAmountBinding;
    }

    public void setIgstAmountBinding(RichInputText igstAmountBinding) {
        this.igstAmountBinding = igstAmountBinding;
    }

    public RichInputText getIgstAmountBinding() {
        return igstAmountBinding;
    }

    public void setBillAmountBinding(RichInputText billAmountBinding) {
        this.billAmountBinding = billAmountBinding;
    }

    public RichInputText getBillAmountBinding() {
        return billAmountBinding;
    }

    public void setVendorBillAmountBinding(RichInputText vendorBillAmountBinding) {
        this.vendorBillAmountBinding = vendorBillAmountBinding;
    }

    public RichInputText getVendorBillAmountBinding() {
        return vendorBillAmountBinding;
    }

    public void setGlCodeBinding(RichInputText glCodeBinding) {
        this.glCodeBinding = glCodeBinding;
    }

    public RichInputText getGlCodeBinding() {
        return glCodeBinding;
    }

    public void setVendorTypeBinding(RichInputText vendorTypeBinding) {
        this.vendorTypeBinding = vendorTypeBinding;
    }

    public RichInputText getVendorTypeBinding() {
        return vendorTypeBinding;
    }

    public void setDueDateBinding(RichInputDate dueDateBinding) {
        this.dueDateBinding = dueDateBinding;
    }

    public RichInputDate getDueDateBinding() {
        return dueDateBinding;
    }

    public void setApprovedBillAmountBinding(RichInputText approvedBillAmountBinding) {
        this.approvedBillAmountBinding = approvedBillAmountBinding;
    }

    public RichInputText getApprovedBillAmountBinding() {
        return approvedBillAmountBinding;
    }

    public void setEcessBinding(RichInputText ecessBinding) {
        this.ecessBinding = ecessBinding;
    }

    public RichInputText getEcessBinding() {
        return ecessBinding;
    }

    public void setTdsCodeBinding(RichInputText tdsCodeBinding) {
        this.tdsCodeBinding = tdsCodeBinding;
    }

    public RichInputText getTdsCodeBinding() {
        return tdsCodeBinding;
    }

    public void setTdsSurBinding(RichInputText tdsSurBinding) {
        this.tdsSurBinding = tdsSurBinding;
    }

    public RichInputText getTdsSurBinding() {
        return tdsSurBinding;
    }

    public void setEcessGlCodeBinding(RichInputText ecessGlCodeBinding) {
        this.ecessGlCodeBinding = ecessGlCodeBinding;
    }

    public RichInputText getEcessGlCodeBinding() {
        return ecessGlCodeBinding;
    }

    public void setTdsPaymentCodeBinding(RichInputComboboxListOfValues tdsPaymentCodeBinding) {
        this.tdsPaymentCodeBinding = tdsPaymentCodeBinding;
    }

    public RichInputComboboxListOfValues getTdsPaymentCodeBinding() {
        return tdsPaymentCodeBinding;
    }

    public void setBillEntryDate1Binding(RichInputDate billEntryDate1Binding) {
        this.billEntryDate1Binding = billEntryDate1Binding;
    }

    public RichInputDate getBillEntryDate1Binding() {
        return billEntryDate1Binding;
    }

    public void setTdsAmount1Binding(RichInputText tdsAmount1Binding) {
        this.tdsAmount1Binding = tdsAmount1Binding;
    }

    public RichInputText getTdsAmount1Binding() {
        return tdsAmount1Binding;
    }

    public void deletePopupDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete").execute();
            System.out.println("Record Delete Successfully");
            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
            srv_flag = "N";

        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(srvTableBinding);

    }

    public void setSrvTableBinding(RichTable srvTableBinding) {
        this.srvTableBinding = srvTableBinding;
    }

    public RichTable getSrvTableBinding() {
        return srvTableBinding;
    }

    public void setDebitAmountBinding(RichInputText debitAmountBinding) {
        this.debitAmountBinding = debitAmountBinding;
    }

    public RichInputText getDebitAmountBinding() {
        return debitAmountBinding;
    }

    public void setEcessAmountPBSBinding(RichInputText ecessAmountPBSBinding) {
        this.ecessAmountPBSBinding = ecessAmountPBSBinding;
    }

    public RichInputText getEcessAmountPBSBinding() {
        return ecessAmountPBSBinding;
    }

    public void setRoundOffBinding(RichInputText roundOffBinding) {
        this.roundOffBinding = roundOffBinding;
    }

    public RichInputText getRoundOffBinding() {
        return roundOffBinding;
    }

    public void gstRoundOffVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        OperationBinding Opr = ADFUtils.findOperation("changeGstCalculationValuePurchaseBill");
        Opr.getParamsMap().put("TypeVal", ADFUtils.evaluateEL("#{bindings.SerRoff.inputValue}"));
        Opr.execute();
    }

    public void setVendorTotalTrans(RichInputText vendorTotalTrans) {
        this.vendorTotalTrans = vendorTotalTrans;
    }

    public RichInputText getVendorTotalTrans() {
        return vendorTotalTrans;
    }

    public void setVenBillNoBinding(RichInputComboboxListOfValues venBillNoBinding) {
        this.venBillNoBinding = venBillNoBinding;
    }

    public RichInputComboboxListOfValues getVenBillNoBinding() {
        return venBillNoBinding;
    }

    public void setPurchaseBillDetailTabBinding(RichShowDetailItem purchaseBillDetailTabBinding) {
        this.purchaseBillDetailTabBinding = purchaseBillDetailTabBinding;
    }

    public RichShowDetailItem getPurchaseBillDetailTabBinding() {
        return purchaseBillDetailTabBinding;
    }

    public void setPreparedByBinding(RichInputComboboxListOfValues preparedByBinding) {
        this.preparedByBinding = preparedByBinding;
    }

    public RichInputComboboxListOfValues getPreparedByBinding() {
        return preparedByBinding;
    }

    public void setApprovedByBinding(RichInputComboboxListOfValues approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputComboboxListOfValues getApprovedByBinding() {
        return approvedByBinding;
    }


    public String saveAndCloseButtonAL() 
    {
        try 
        {
            ADFUtils.findOperation("narrationValuePurchaseBill").execute();
        } catch (Exception e) {
            // TODO: Add catch code
            e.printStackTrace();
        }

        if (srv_flag.equalsIgnoreCase("Y")) 
        {
            if (tab_flag.equalsIgnoreCase("Y")) 
            {
                String ResultCM = "";
                // String voucher_no="";
                BigDecimal diff_amount = new BigDecimal(0);

                try 
                {
                    OperationBinding Opr1 = ADFUtils.findOperation("makerCheckerForPurchseBill");
                    Object Obj1 = Opr1.execute();
                    if (Opr1.getResult() != null) 
                    {
                        ResultCM = Opr1.getResult().toString();
                    }
                } catch (Exception ee) {
                    ee.printStackTrace();
                }

                diff_amount =(BigDecimal) (diffrenceValueBinding.getValue() != null ? diffrenceValueBinding.getValue() :
                                                                                                 new BigDecimal(0));
                if (diff_amount.compareTo(new BigDecimal(0)) == 0)
                {
                    System.out.println("ResultCM IS:" + ResultCM + "Approval Value:" +approvedByBinding.getValue() + "*PAGE MODE:" + PageMode);
                    
                    if ((ResultCM.equalsIgnoreCase("Y") && approvedByBinding.getValue() == null && PageMode.equals("C")) ||
                        ((ResultCM.equals("N")) && PageMode.equalsIgnoreCase("C") &&
                         approvedByBinding.getValue() != null)) 
                    {
                        System.out.println("*******************AFTER************************");
                        OperationBinding Opr = ADFUtils.findOperation("generatePurchaseBillNo");
                        //Opr.getParamsMap().put("UserName","SWE161");
                        Opr.getParamsMap().put("UserName", ADFUtils.resolveExpression("#{pageFlowScope.empCode}"));
                        Object Obj = Opr.execute();
                        System.out.println("******Result is:" + Opr.getResult());

                        if (Opr.getResult() != null && Opr.getResult().equals("S")) 
                        {
                            ADFUtils.findOperation("Commit").execute();
                            ADFUtils.showMessage("Record Saved Successfully.New Purchase Bill No. is " +
                                                 ADFUtils.evaluateEL("#{bindings.BillNo.inputValue}"), 2);
                            vouNumberForMessage();
                            return "SaveAndClose";
                        } 
                        else if (Opr.getResult() != null && Opr.getResult().equals("U"))
                        {
                            ADFUtils.findOperation("Commit").execute();
                            ADFUtils.showMessage("Record Updated Successfully.", 2);
                            vouNumberForMessage();
                            return "SaveAndClose";

                        } 
                        else if (Opr.getResult() != null && Opr.getResult().equals("SrvError")) 
                        {
                            ADFUtils.showMessage("Problem In Data Save In SRV.", 0);
                            return null;
                        } 
                        else if (Opr.getResult() != null && Opr.getResult().equals("VR")) 
                        {
                            ADFUtils.showMessage("Problem In Generate Voucher No.", 0);
                            return null;
                        }
                    } else 
                    {
                        if (ResultCM.equalsIgnoreCase("N") && approvedByBinding.getValue() == null) 
                        {
                            ADFUtils.showMessage("Please Approved This Record.", 0);
                            return null;
                        }
                    }

                    if (PageMode.equalsIgnoreCase("E")) 
                    {

                        OperationBinding Opr = ADFUtils.findOperation("generatePurchaseBillNo");
                        Opr.getParamsMap().put("UserName", ADFUtils.resolveExpression("#{pageFlowScope.empCode}"));
                        Object Obj = Opr.execute();
                        System.out.println("******Result is:" + Opr.getResult());
                        //                     try
                        //                     {
                        //                     voucher_no=vouNumberForMessage();
                        //                     }
                        //                     catch (Exception ee)
                        //                     {
                        //                     ee.printStackTrace();
                        //                     }

                        if (Opr.getResult() != null && Opr.getResult().equals("S")) 
                        {
                            ADFUtils.findOperation("Commit").execute();
                            ADFUtils.showMessage("Record Saved Successfully.New Purchase Bill No. is " +
                                                 ADFUtils.evaluateEL("#{bindings.BillNo.inputValue}"), 2);
                            vouNumberForMessage();
                            //ADFUtils.showMessage(voucher_no, 2);
                            return "SaveAndClose";
                        } 
                        else if (Opr.getResult() != null && Opr.getResult().equals("U")) 
                        {
                            ADFUtils.findOperation("Commit").execute();
                            ADFUtils.showMessage("Record Updated Successfully.", 2);
                            vouNumberForMessage();
                            //ADFUtils.showMessage(voucher_no, 2);
                            return "SaveAndClose";
                        } 
                        else if (Opr.getResult() != null && Opr.getResult().equals("SrvError")) 
                        {
                            ADFUtils.showMessage("Problem In Data Save In SRV.", 2);
                            return null;
                        } 
                        else if (Opr.getResult() != null && Opr.getResult().equals("VR")) 
                        {
                            ADFUtils.showMessage("Problem In Generate Voucher No.", 2);
                            return null;
                        }
                    }

                } else {
                    ADFUtils.showMessage("Debit Credit Amount Not Equals.Please Check", 0);
                    return null;
                }
                return null;
            } else {
                ADFUtils.showMessage("Please Open Purchase Bill Summary Tab.", 0);
                return null;
            }
        } else {
            ADFUtils.showMessage("Please Click On Populate Item Detail Button in SRV Detail Tab.", 0);
            return null;
        }
    }

    public void setTotalValueOutPutBinding(RichOutputText totalValueOutPutBinding) {
        this.totalValueOutPutBinding = totalValueOutPutBinding;
    }

    public RichOutputText getTotalValueOutPutBinding() {
        return totalValueOutPutBinding;
    }

    public void IncomeTaxDCL(DisclosureEvent disclosureEvent) 
    {
        tab_flag = "N";
        //    System.out.println("Before Calling Method In         System.out.println(\"Before Calling Method InchangeGstCalculationValuePurchaseBill");
        //    OperationBinding Opr=ADFUtils.findOperation("changeGstCalculationValuePurchaseBill");
        //    Opr.getParamsMap().put("TypeVal","A");
        //    Opr.execute();
        //    System.out.println("After Calling Method In         System.out.println(\"Before Calling Method InchangeGstCalculationValuePurchaseBill");

    }

    public void populateDataAL(ActionEvent actionEvent) 
    {
        srv_flag = "Y";
        ADFUtils.findOperation("multipleSrvCasePurchaseBill").execute();
        ADFUtils.findOperation("populateDataInItemDetailPurchaseBill").execute();
        if ((Long) ADFUtils.evaluateEL("#{bindings.PurchaseBillDetailVO1Iterator.estimatedRowCount}") > 0) 
        {
            ADFUtils.showMessage("Data Successfully Populated In Item Detail.", 2);
        } 
        else {
            ADFUtils.showMessage("Not Data Found For Item Detail.", 2);
        }
        ADFUtils.findOperation("changeGstCalculationValuePurchaseBill").execute();
        ADFUtils.findOperation("setItemDetailDataInBillJVPurchaseBill").execute();


    }

    public void setPopulateItemDetailButtonBinding(RichButton populateItemDetailButtonBinding) {
        this.populateItemDetailButtonBinding = populateItemDetailButtonBinding;
    }

    public RichButton getPopulateItemDetailButtonBinding() {
        return populateItemDetailButtonBinding;
    }

    public void approvedByVCE(ValueChangeEvent valueChangeEvent) 
    {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        try 
        {

            OperationBinding OprAutho = ADFUtils.findOperation("checkApprovalStatus");
            OprAutho.getParamsMap().put("emp_code", valueChangeEvent.getNewValue());
            OprAutho.getParamsMap().put("form_name", "FINBD");
            OprAutho.getParamsMap().put("unit_code", unitCodeBinding.getValue());
            OprAutho.getParamsMap().put("amount", billAmountBinding.getValue());
            OprAutho.getParamsMap().put("autho_level", "AP");
            OprAutho.execute();
            Row row = (Row) ADFUtils.evaluateEL("#{bindings.PurchaseBillHeaderVO1Iterator.currentRow}");
            if (OprAutho.getResult().equals("Y")) 
            {
                row.setAttribute("ApprovalDate", billEntryDateBinding.getValue());
            } 
            else 
            {
                row.setAttribute("FinMauthAuthCode", null);
                row.setAttribute("ApprovalDate", null);
                ADFUtils.showMessage("You Don't Have Permission To Approved This Record.", 0);
            }
            /*             OperationBinding OprAutho=ADFUtils.findOperation("checkAuthorityForApprovalPurchaseBill");
            OprAutho.getParamsMap().put("EmpCode",ADFUtils.evaluateEL("#{pageFlowScope.empCode}"));
            // OprAutho.getParamsMap().put("EmpCode","SWE161");
            Object OprResult=OprAutho.execute();
            System.out.println("Result When Chnage Value Of Approved By:"+OprAutho.getResult());
            if(OprAutho.getResult()!=null && OprAutho.getResult().equals("N"))
            {
                ADFUtils.showMessage("You Don't Have Permission To Approved This Record.",0);
            }
            else
            {
                System.out.println("Else Case Result When Chnage Value Of Approved By:"+OprAutho.getResult());
                //  ADFUtils.setEL("#{pageFlowScope.mode}","V");
                //  AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
            try
            {
                 cevmodecheck();
            }
            catch(Exception ee)
            {
                System.out.println("Error When Set View Mode in Save Button");
                ee.printStackTrace();
            }
            } */

        } catch (Exception ee) {
            ee.printStackTrace();
        }

    }

    public void setLastBillPassingDateBinding(RichInputDate lastBillPassingDateBinding) {
        this.lastBillPassingDateBinding = lastBillPassingDateBinding;
    }

    public RichInputDate getLastBillPassingDateBinding() {
        return lastBillPassingDateBinding;
    }

    public void setDetailDeleteButtonBinding(RichButton detailDeleteButtonBinding) {
        this.detailDeleteButtonBinding = detailDeleteButtonBinding;
    }

    public RichButton getDetailDeleteButtonBinding() {
        return detailDeleteButtonBinding;
    }

    public void BillEntryDateValidator(FacesContext facesContext, UIComponent uIComponent, Object object) 
    {
        if (object != null) 
        {
            //                    String Str=object.toString();
            //                    Date JboDate=null;
            //                    java.util.Date UtilDate=null;
            //                    String Flag="Y";
            //                    try
            //                    {
            //                    JboDate=new Date(Str);
            //                    Flag="Y";
            //                    }
            //                    catch(Exception ee)
            //                    {
            //                    System.out.println("Continue");
            //                    UtilDate=new java.util.Date(Str);
            //                    Flag="N";
            //                    }
            //                    System.out.println("JBO Date :"+JboDate);
            //                    System.out.println("Util Date :"+UtilDate);
            //
            //                    System.out.println("FLAG IS "+Flag);
            //                    OperationBinding op1 = ADFUtils.findOperation("checkForMonthAndYearVouSeriesForAllVouchers");
            //                    if(Flag.equalsIgnoreCase("Y"))
            //                    op1.getParamsMap().put("vou_dt",JboDate);
            //                    if(Flag.equalsIgnoreCase("N"))
            //                    op1.getParamsMap().put("vou_dt",UtilDate);
            //                    //op1.getParamsMap().put("FinYear","18-19");
            //                    op1.getParamsMap().put("FinYear",ADFUtils.resolveExpression("#{pageFlowScope.finYear}"));
            //                    op1.getParamsMap().put("Vou_Type","P");
            //                    op1.getParamsMap().put("Vou_Series","V");
            //                    op1.getParamsMap().put("Unit_Code",unitCodeBinding.getValue());
            //                    // op1.getParamsMap().put("FinYear","18-19");
            //                    op1.execute();
            //                    System.out.println("GET RESULT:"+op1.getResult());
            //                    if(op1.getResult()!=null && !op1.getResult().equals("Y"))
            //                    {
            //                        throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,op1.getResult().toString(),"Please Select Another Date."));
            //                    }
            System.out.println("Bill Entry Date=====>" + object);
        }
    }

    public void setSrvNoBinding(RichInputComboboxListOfValues srvNoBinding) {
        this.srvNoBinding = srvNoBinding;
    }

    public RichInputComboboxListOfValues getSrvNoBinding() {
        return srvNoBinding;
    }

    public void setSrvDateSrvHeadBinding(RichInputDate srvDateSrvHeadBinding) {
        this.srvDateSrvHeadBinding = srvDateSrvHeadBinding;
    }

    public RichInputDate getSrvDateSrvHeadBinding() {
        return srvDateSrvHeadBinding;
    }

    public void setInvoiceNoBinding(RichInputText invoiceNoBinding) {
        this.invoiceNoBinding = invoiceNoBinding;
    }

    public RichInputText getInvoiceNoBinding() {
        return invoiceNoBinding;
    }

    public void setInvoiceDateBinding(RichInputDate invoiceDateBinding) {
        this.invoiceDateBinding = invoiceDateBinding;
    }

    public RichInputDate getInvoiceDateBinding() {
        return invoiceDateBinding;
    }

    public void setChallanNoBinding(RichInputText challanNoBinding) {
        this.challanNoBinding = challanNoBinding;
    }

    public RichInputText getChallanNoBinding() {
        return challanNoBinding;
    }

    public void setChallanDateBinding(RichInputDate challanDateBinding) {
        this.challanDateBinding = challanDateBinding;
    }

    public RichInputDate getChallanDateBinding() {
        return challanDateBinding;
    }

    public void setApprovalDateBinding(RichInputDate approvalDateBinding) {
        this.approvalDateBinding = approvalDateBinding;
    }

    public RichInputDate getApprovalDateBinding() {
        return approvalDateBinding;
    }

    public void generateVoucherButtonAL(ActionEvent actionEvent) 
    {
        ADFUtils.findOperation("insertDataInPurchaseBillJV").execute();
        System.out.println("After Calling insertFromServiceBillToBillJv Method");
    }

    public void glCodeVCE(ValueChangeEvent valueChangeEvent) 
    {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        System.out.println("Gl Code Value Change Event" + valueChangeEvent.getNewValue());
        if (valueChangeEvent.getNewValue() != null) 
        {
            ADFUtils.findOperation("setItemDetailDataInBillJVPurchaseBill").execute();
            ADFUtils.findOperation("calculateDebitNotePurchaseBill").execute();

            //calculateDebitNotePurchaseBill
        }
    }

    public void costCenterVCE(ValueChangeEvent valueChangeEvent) 
    {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        ADFUtils.findOperation("changeGstCalculationValuePurchaseBill").execute();
        //End
        ADFUtils.findOperation("setItemDetailDataInBillJVPurchaseBill").execute();
    }

    public void setVouNo1Binding(RichInputText vouNo1Binding) {
        this.vouNo1Binding = vouNo1Binding;
    }

    public RichInputText getVouNo1Binding() {
        return vouNo1Binding;
    }


    public String vouNumberForMessage() 
    {
        String bill_voucher_no = "";
        String rcm_invoice_no = "";
        String debit_note_no = "";
        String message = "";
        try 
        {
            //        tds_voucher_no=(String) ADFUtils.evaluateEL("#{bindings.VouNo1.inputValue}");
            bill_voucher_no = (String) ADFUtils.evaluateEL("#{bindings.VouNo.inputValue}");
            rcm_invoice_no = (String) ADFUtils.evaluateEL("#{bindings.ParentBillNo.inputValue}");
            debit_note_no = (String) ADFUtils.evaluateEL("#{bindings.VouNo1.inputValue}");

            //        if(rcm_invoice_no!=null)
            //        {
            //        message="And RCM Invoice No. is "+rcm_invoice_no;
            //        System.out.println("Message is :"+message);
            //        }
            //
            //        if(tds_voucher_no!=null)
            //        {
            //        message=message+".And TDS Voucher No is "+tds_voucher_no;
            //        System.out.println("Message is :"+message);
            //        }
            //        if(bill_voucher_no!=null)
            //        {
            //            message=message+".And Bill Voucher No. is "+bill_voucher_no;
            //        }


System.out.println("Bill voucher no is:?????????&*&*&*&*" + bill_voucher_no );
            

            if (rcm_invoice_no != null) {
                message = "RCM Invoice No. is " + rcm_invoice_no;
                ADFUtils.showMessage(message, 2);
                System.out.println("Message is :" + message);
            }

            if (bill_voucher_no != null) {
                message = "Bill Voucher No. is " + bill_voucher_no;
                ADFUtils.showMessage(message, 2);
            }
            if (debit_note_no != null) {
                message = "Debit Note Voucher No is " + debit_note_no;
                ADFUtils.showMessage(message, 2);
            }

        } catch (Exception ee) {
            ee.printStackTrace();
        }
        return message;

    }


    public void setTdsLimitBinding(RichInputText tdsLimitBinding) {
        this.tdsLimitBinding = tdsLimitBinding;
    }

    public RichInputText getTdsLimitBinding() {
        return tdsLimitBinding;
    }

    public void setDiffrenceValueBinding(RichOutputText diffrenceValueBinding) {
        this.diffrenceValueBinding = diffrenceValueBinding;
    }

    public RichOutputText getDiffrenceValueBinding() {
        return diffrenceValueBinding;
    }

    public void billPVCreateButtonAL(ActionEvent actionEvent) 
    {
        ADFUtils.findOperation("CreateInsert2").execute();
        Row selectedRow = (Row) ADFUtils.evaluateEL("#{bindings.BillPurchaseVoucherVO2Iterator.currentRow}");
        selectedRow.setAttribute("EditValue", "0");
    }

    public void setEditTransBinding(RichInputText editTransBinding) {
        this.editTransBinding = editTransBinding;
    }

    public RichInputText getEditTransBinding() {
        return editTransBinding;
    }

    public void deletPopupBillPVDL(DialogEvent dialogEvent) 
    {
        if (dialogEvent.getOutcome().name().equals("ok")) 
        {
            Row selectedRow = (Row) ADFUtils.evaluateEL("#{bindings.BillPurchaseVoucherVO2Iterator.currentRow}");
            Integer check_value =(Integer) (selectedRow.getAttribute("EditValue") != null ? selectedRow.getAttribute("EditValue") : 1);
            System.out.println("check_valuecheck_valuecheck_value:" + check_value);
            if (check_value == 0 || check_value == 2) 
            {
                ADFUtils.findOperation("Delete2").execute();
                ADFUtils.showMessage("Record Deleted Successfully.", 2);
            } 
            else {
                ADFUtils.showMessage("You Can't Delete This Record", 0);
                billJVPopupBinding.cancel();
            }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(billPVTableBinding);
    }

    public void setBillPVTableBinding(RichTable billPVTableBinding) {
        this.billPVTableBinding = billPVTableBinding;
    }

    public RichTable getBillPVTableBinding() {
        return billPVTableBinding;
    }

    public void setBillJVPopupBinding(RichPopup billJVPopupBinding) {
        this.billJVPopupBinding = billJVPopupBinding;
    }

    public RichPopup getBillJVPopupBinding() {
        return billJVPopupBinding;
    }

    public void gstExemptedVCE(ValueChangeEvent valueChangeEvent)
    {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        ADFUtils.findOperation("changeGstCalculationValuePurchaseBill").execute();
        ADFUtils.findOperation("setItemDetailDataInBillJVPurchaseBill").execute();
        if (valueChangeEvent.getNewValue().equals("Y")) 
        {
            getGstRCMApplyBinding().setDisabled(true);
        } 
        else {
            getGstRCMApplyBinding().setDisabled(false);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(gstRCMApplyBinding);

    }

    public void gstRcmApplyVCE(ValueChangeEvent valueChangeEvent) 
    {
        System.out.println("Inside VCE gstRcmApplyVCE");
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        ADFUtils.findOperation("changeGstCalculationValuePurchaseBill").execute();

        ADFUtils.findOperation("setItemDetailDataInBillJVPurchaseBill").execute();
        if (valueChangeEvent.getNewValue().equals("Y")) {
            getGstExemptedBinding().setDisabled(true);
        } else {
            getGstExemptedBinding().setDisabled(false);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(gstExemptedBinding);

        System.out.println("Before Calling Method tdsCalculationPurchaseBill");
        //changeGstCalculationValuePurchaseBill
        ADFUtils.findOperation("tdsCalculationPurchaseBill").execute();
        System.out.println("After  Calling Method tdsCalculationPurchaseBill");
    }

    public void sgstAmountVCE(ValueChangeEvent valueChangeEvent)
    {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        OperationBinding Opr = ADFUtils.findOperation("changeGstCalculationValuePurchaseBill");
        Opr.getParamsMap().put("TypeVal", "A");
        Opr.execute();
        ADFUtils.findOperation("setItemDetailDataInBillJVPurchaseBill").execute();

    }

    public void setGstExemptedBinding(RichSelectOneChoice gstExemptedBinding) {
        this.gstExemptedBinding = gstExemptedBinding;
    }

    public RichSelectOneChoice getGstExemptedBinding() {
        return gstExemptedBinding;
    }

    public void setGstRCMApplyBinding(RichSelectOneChoice gstRCMApplyBinding) {
        this.gstRCMApplyBinding = gstRCMApplyBinding;
    }

    public RichSelectOneChoice getGstRCMApplyBinding() {
        return gstRCMApplyBinding;
    }

    public void setParentBillNoBinding(RichInputText parentBillNoBinding) {
        this.parentBillNoBinding = parentBillNoBinding;
    }

    public RichInputText getParentBillNoBinding() {
        return parentBillNoBinding;
    }

    public void srvDetailTabDCL(DisclosureEvent disclosureEvent) {
        tab_flag = "N";
        if (PageMode.equalsIgnoreCase("C") && billNoBinding.getValue() != null) 
        {
            ADFUtils.findOperation("filterSrvAfterSetDataPurchaseBill").execute();
            getSrvNoBinding().setDisabled(true);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(srvDetailTabBinding);
        AdfFacesContext.getCurrentInstance().addPartialTarget(srvTableBinding);

    }

    public void itemDetailTabDCL(DisclosureEvent disclosureEvent) {
        tab_flag = "N";
    }

    public void setBillJVCreateBinding(RichButton billJVCreateBinding) {
        this.billJVCreateBinding = billJVCreateBinding;
    }

    public RichButton getBillJVCreateBinding() {
        return billJVCreateBinding;
    }

    public void setBillJVDeleteButtonBinding(RichButton billJVDeleteButtonBinding) {
        this.billJVDeleteButtonBinding = billJVDeleteButtonBinding;
    }

    public RichButton getBillJVDeleteButtonBinding() {
        return billJVDeleteButtonBinding;
    }

    public void paymentCodeVCE(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        refreshValueOnChangeOfTdsAndRoundOff("N");
        ADFUtils.findOperation("tdsCalculationOnPaymentAmountPurchaseBill").execute();
    }

    public void purchaseBillDetailTabDCL(DisclosureEvent disclosureEvent) {
        try {
            ADFUtils.findOperation("narrationValuePurchaseBill").execute();
        } catch (Exception e) {
            // TODO: Add catch code
            e.printStackTrace();
        }
        tab_flag = "N";
    }

    public void setBillVouchNoBinding(RichInputText billVouchNoBinding) {
        this.billVouchNoBinding = billVouchNoBinding;
    }

    public RichInputText getBillVouchNoBinding() {
        return billVouchNoBinding;
    }

    public void setTdsVouchNoBinding(RichInputText tdsVouchNoBinding) {
        this.tdsVouchNoBinding = tdsVouchNoBinding;
    }

    public RichInputText getTdsVouchNoBinding() {
        return tdsVouchNoBinding;
    }

    public void srvNoInSrvTabVCE(ValueChangeEvent valueChangeEvent) 
    {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (valueChangeEvent.getNewValue() != null) 
        {
            srv_flag = "N";
            populateItemDetailButtonBinding.setDisabled(false);
            AdfFacesContext.getCurrentInstance().addPartialTarget(populateItemDetailButtonBinding);

            OperationBinding opr = ADFUtils.findOperation("checkDuplicateSrvPurchaseBill");
            opr.getParamsMap().put("srvNo", valueChangeEvent.getNewValue());
            Object obj = opr.execute();
            if (opr.getResult().equals("N")) {
                ADFUtils.showMessage("SRV Number Already Exist.", 0);
            }
        }
    }

    public void setCostCenterDebitNoteBinding(RichInputText costCenterDebitNoteBinding) {
        this.costCenterDebitNoteBinding = costCenterDebitNoteBinding;
    }

    public RichInputText getCostCenterDebitNoteBinding() {
        return costCenterDebitNoteBinding;
    }

    public void setAmountDebitNoteBinding(RichInputText amountDebitNoteBinding) {
        this.amountDebitNoteBinding = amountDebitNoteBinding;
    }

    public RichInputText getAmountDebitNoteBinding() {
        return amountDebitNoteBinding;
    }

    public void setDrCrflagDebitNoteBinding(RichSelectOneChoice drCrflagDebitNoteBinding) {
        this.drCrflagDebitNoteBinding = drCrflagDebitNoteBinding;
    }

    public RichSelectOneChoice getDrCrflagDebitNoteBinding() {
        return drCrflagDebitNoteBinding;
    }

    public void setSubCodeDebitNoteBinding(RichInputText subCodeDebitNoteBinding) {
        this.subCodeDebitNoteBinding = subCodeDebitNoteBinding;
    }

    public RichInputText getSubCodeDebitNoteBinding() {
        return subCodeDebitNoteBinding;
    }

    public void setGlCodeDebitNoteBinding(RichInputText glCodeDebitNoteBinding) {
        this.glCodeDebitNoteBinding = glCodeDebitNoteBinding;
    }

    public RichInputText getGlCodeDebitNoteBinding() {
        return glCodeDebitNoteBinding;
    }

    public void setDebitNoteTabBinding(RichShowDetailItem debitNoteTabBinding) {
        this.debitNoteTabBinding = debitNoteTabBinding;
    }

    public RichShowDetailItem getDebitNoteTabBinding() {
        return debitNoteTabBinding;
    }

    public void debitNoteYesNoVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce.getNewValue() != null) {
            if (vce.getNewValue().equals("Y")) {
                ADFUtils.findOperation("diffrenceValueInBillDrPvPurchaseBill").execute();
            } else {
                ADFUtils.findOperation("removeDebitNoteRowInPurchaseBill").execute();
            }
        }
    }

    public void setAmdNoBinding(RichInputText amdNoBinding) {
        this.amdNoBinding = amdNoBinding;
    }

    public RichInputText getAmdNoBinding() {
        return amdNoBinding;
    }

    public void srvDetailCreateInsertButtonAL(ActionEvent actionEvent) 
    {
        ADFUtils.findOperation("CreateInsert").execute();
        Row row = (Row) ADFUtils.evaluateEL("#{bindings.StoreReceiptVoucherHeadVO1Iterator.currentRow}");
        row.setAttribute("VendCodeTrans", vendCodeBinding.getValue());
        logger.info("Vend Code Binding Value=" + vendCodeBinding.getValue());
        logger.info("Vend Code Trans Value=" + row.getAttribute("VendCodeTrans"));

    }

    public void refreshValueOnChangeOfTdsAndRoundOff(String flag) 
    {
        BigDecimal zero = new BigDecimal(0);
        Row row = (Row) ADFUtils.evaluateEL("#{bindings.PurchaseBillHeaderVO1Iterator.currentRow}");
        BigDecimal DspApprAmt =
            (BigDecimal) (row.getAttribute("DspApprAmt") != null ? row.getAttribute("DspApprAmt") : zero);
        BigDecimal venBillAmt =
            (BigDecimal) (row.getAttribute("DspBillAmt") != null ? row.getAttribute("DspBillAmt") : zero);
        BigDecimal tdsAmt = (BigDecimal) (row.getAttribute("TdsAmount") != null ? row.getAttribute("TdsAmount") : zero);
        BigDecimal debitAmt = (BigDecimal) (row.getAttribute("DspCrDr") != null ? row.getAttribute("DspCrDr") : zero);
        venBillAmt = DspApprAmt.subtract(tdsAmt.add(debitAmt));
        
        row.setAttribute("DspBillAmt", DspApprAmt);
        row.setAttribute("ApprovedAmount", venBillAmt);
        if (flag.equalsIgnoreCase("Y")){
            ADFUtils.findOperation("setItemDetailDataInBillJVPurchaseBill").execute();
        }

    }

    public void approvalDateValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (object != null) 
        {
            String str_timestamp = object.toString();
            Date date = new Date(str_timestamp.substring(0, 10));
            Date curr_date = new Date(new Date().getCurrentDate().dateValue().toString().substring(0, 10));
            logger.info("========Date" + date + "\n Current Date:=>" + curr_date);
            if (date.compareTo(curr_date) == 1) {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                              "Approval Date Should Be Less Than Current Date.", null));
            }
        }
    }
    
}
