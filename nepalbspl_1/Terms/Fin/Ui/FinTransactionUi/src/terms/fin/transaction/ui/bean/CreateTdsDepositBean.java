package terms.fin.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;


public class CreateTdsDepositBean {

    private RichTable tdsDepositDetailTableBinding;
    private RichInputText bindingDepositNo;
    private RichInputComboboxListOfValues bindingUnitCode;
    private RichInputDate bindingDepositDate;
    private RichInputDate bindingFromDate;
    private RichInputDate bindingToDate;
    private RichSelectOneChoice bindingType;
    private RichInputComboboxListOfValues bindingTdsGlCode;
    private RichInputComboboxListOfValues bindingApprovedBy;
    private RichSelectOneChoice bindingTdsFlag;
    private RichInputText bindingSrNo;
    private RichInputText bindingPayDocNo;
    private RichInputDate bindingPaymentDate;
    private RichInputText bindingPaymentAmt;
    private RichInputText bindingTdsRate;
    private RichInputText bindingTdsAmt;
    private RichInputText bindingTdsCode;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichButton detailPopuldateBinding;
    private RichOutputText bindingOutputText;
    private RichInputText bindingSection1;
    private RichInputText partyNameBinding;

    public CreateTdsDepositBean() {
    }

    public void saveButtonAL(ActionEvent actionEvent) 
    {
       OperationBinding Opp=ADFUtils.findOperation("generateTdsDepositNumber");
       Object obj=Opp.execute();
       System.out.println("Result is===> "+Opp.getResult());   
       if(Opp.getResult()!=null && !Opp.getResult().equals("N"))
       {
           ADFUtils.findOperation("Commit").execute();
           ADFUtils.showMessage("Record Save Successfully.Deposit Number is "+Opp.getResult(), 2);
          // FacesMessage Message = new FacesMessage("Record Save Successfully.Deposit Number is "+Opp.getResult());
//           Message.setSeverity(FacesMessage.SEVERITY_INFO);
//           FacesContext fc = FacesContext.getCurrentInstance();
//           fc.addMessage(null, Message);   
           if(bindingApprovedBy.getValue()!=null || bindingApprovedBy.getValue() == null)
           {
            ADFUtils.setEL("#{pageFlowScope.mode}","V");
            cevmodecheck();
            AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
           }
       }
       else
       {
           ADFUtils.findOperation("Commit").execute();
           ADFUtils.showMessage(" Record Updated Successfully", 2);
           if(bindingApprovedBy.getValue()!=null)
           {
            ADFUtils.setEL("#{pageFlowScope.mode}","V");
            cevmodecheck();
            AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
           }
       }
     
}

    public void populateDataAL(ActionEvent actionEvent) 
    {
        OperationBinding OppFin=ADFUtils.findOperation("finYearTdsDeposit");
        Object ObjFin=OppFin.execute();
        
        if(OppFin.getResult()!=null && OppFin.getResult().equals("Y"))
        {
            OperationBinding PopData=ADFUtils.findOperation("populdateTdsDepositDetailData");
            Object ObjPop=PopData.execute();
            if(PopData.getResult()!=null && PopData.getResult().equals("S"))
            {
                ADFUtils.showMessage("Data Populate Successfully.", 2);
                getBindingDepositDate().setDisabled(true);
                getBindingFromDate().setDisabled(true);
                getBindingToDate().setDisabled(true);
                getBindingType().setDisabled(true);
                getBindingTdsCode().setDisabled(true);
                getBindingTdsGlCode().setDisabled(true);
                getBindingTdsFlag().setDisabled(true);
            }
            else if(PopData.getResult()!=null && PopData.getResult().equals("N")){
                ADFUtils.showMessage("This TDS GL Code Table does not have any record.", 0);
            }
            else if(PopData.getResult()!=null && PopData.getResult().equals("E")){
                ADFUtils.showMessage("Tds Deposit Function Error.", 0);
            }
        }
        else
        {
            ADFUtils.showMessage("Fin Year Not Found For This Entry.", 0);
        }
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent)
        {
            if(dialogEvent.getOutcome().name().equals("ok"))
                {
                ADFUtils.findOperation("Delete").execute();
                    OperationBinding op=  ADFUtils.findOperation("Commit");
                    Object rst = op.execute();
                System.out.println("Record Delete Successfully");
               
                    if(op.getErrors().isEmpty())
                    {
                    FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                    FacesContext fc = FacesContext.getCurrentInstance();  
                    fc.addMessage(null, Message);
                    }
                    else
                    {
                    FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                    Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);
                     }
            }
            AdfFacesContext.getCurrentInstance().addPartialTarget(tdsDepositDetailTableBinding);
        }


    public void setTdsDepositDetailTableBinding(RichTable tdsDepositDetailTableBinding) {
        this.tdsDepositDetailTableBinding = tdsDepositDetailTableBinding;
    }

    public RichTable getTdsDepositDetailTableBinding() {
        return tdsDepositDetailTableBinding;
    }

    public void setBindingDepositNo(RichInputText bindingDepositNo) {
        this.bindingDepositNo = bindingDepositNo;
    }

    public RichInputText getBindingDepositNo() {
        return bindingDepositNo;
    }

    public void setBindingUnitCode(RichInputComboboxListOfValues bindingUnitCode) {
        this.bindingUnitCode = bindingUnitCode;
    }

    public RichInputComboboxListOfValues getBindingUnitCode() {
        return bindingUnitCode;
    }

    public void setBindingDepositDate(RichInputDate bindingDepositDate) {
        this.bindingDepositDate = bindingDepositDate;
    }

    public RichInputDate getBindingDepositDate() {
        return bindingDepositDate;
    }

    public void setBindingFromDate(RichInputDate bindingFromDate) {
        this.bindingFromDate = bindingFromDate;
    }

    public RichInputDate getBindingFromDate() {
        return bindingFromDate;
    }

    public void setBindingToDate(RichInputDate bindingToDate) {
        this.bindingToDate = bindingToDate;
    }

    public RichInputDate getBindingToDate() {
        return bindingToDate;
    }

    public void setBindingType(RichSelectOneChoice bindingType) {
        this.bindingType = bindingType;
    }

    public RichSelectOneChoice getBindingType() {
        return bindingType;
    }

    public void setBindingTdsGlCode(RichInputComboboxListOfValues bindingTdsGlCode) {
        this.bindingTdsGlCode = bindingTdsGlCode;
    }

    public RichInputComboboxListOfValues getBindingTdsGlCode() {
        return bindingTdsGlCode;
    }

   

    public void setBindingApprovedBy(RichInputComboboxListOfValues bindingApprovedBy) {
        this.bindingApprovedBy = bindingApprovedBy;
    }

    public RichInputComboboxListOfValues getBindingApprovedBy() {
        return bindingApprovedBy;
    }

    public void setBindingTdsFlag(RichSelectOneChoice bindingTdsFlag) {
        this.bindingTdsFlag = bindingTdsFlag;
    }

    public RichSelectOneChoice getBindingTdsFlag() {
        return bindingTdsFlag;
    }

    public void setBindingSrNo(RichInputText bindingSrNo) {
        this.bindingSrNo = bindingSrNo;
    }

    public RichInputText getBindingSrNo() {
        return bindingSrNo;
    }

    public void setBindingPayDocNo(RichInputText bindingPayDocNo) {
        this.bindingPayDocNo = bindingPayDocNo;
    }

    public RichInputText getBindingPayDocNo() {
        return bindingPayDocNo;
    }

    public void setBindingPaymentDate(RichInputDate bindingPaymentDate) {
        this.bindingPaymentDate = bindingPaymentDate;
    }

    public RichInputDate getBindingPaymentDate() {
        return bindingPaymentDate;
    }

    public void setBindingPaymentAmt(RichInputText bindingPaymentAmt) {
        this.bindingPaymentAmt = bindingPaymentAmt;
    }

    public RichInputText getBindingPaymentAmt() {
        return bindingPaymentAmt;
    }

    public void setBindingTdsRate(RichInputText bindingTdsRate) {
        this.bindingTdsRate = bindingTdsRate;
    }

    public RichInputText getBindingTdsRate() {
        return bindingTdsRate;
    }

    public void setBindingTdsAmt(RichInputText bindingTdsAmt) {
        this.bindingTdsAmt = bindingTdsAmt;
    }

    public RichInputText getBindingTdsAmt() {
        return bindingTdsAmt;
    }

    public void setBindingTdsCode(RichInputText bindingTdsCode) {
        this.bindingTdsCode = bindingTdsCode;
    }

    public RichInputText getBindingTdsCode() {
        return bindingTdsCode;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setDetailPopuldateBinding(RichButton detailPopuldateBinding) {
        this.detailPopuldateBinding = detailPopuldateBinding;
    }

    public RichButton getDetailPopuldateBinding() {
        return detailPopuldateBinding;
    }
  
    private void cevmodecheck(){
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }
    
    
    
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                try {
                    Method method1 =
                        component.getClass().getMethod("setDisabled", boolean.class);
                    if (method1 != null) {
                        method1.invoke(component, valueComponent);
                    }
                } catch (NoSuchMethodException e) {
                    try {
                        Method method =
                            component.getClass().getMethod("setReadOnly", boolean.class);
                        if (method != null) {
                            method.invoke(component, valueComponent);
                        }
                    } catch (Exception e1) {
                        // e.printStackTrace();//silently eat this exception.
                    }


                } catch (Exception e) {
                    // e.printStackTrace();//silently eat this exception.
                }
                List<UIComponent> childComponents = component.getChildren();
                for (UIComponent comp : childComponents) {
                    makeComponentHierarchyReadOnly(comp, valueComponent);
                }
            }
    
    
    
    public void cevModeDisableComponent(String mode) {
      //            FacesContext fctx = FacesContext.getCurrentInstance();
      //            ELContext elctx = fctx.getELContext();
      //            Application jsfApp = fctx.getApplication();
      //            //create a ValueExpression that points to the ADF binding layer
      //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
      //            //
      //            ValueExpression valueExpr = exprFactory.createValueExpression(
      //                                         elctx,
      //                                         "#{pageFlowScope.mode=='E'}",
      //                                          Object.class
      //                                         );
      //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
          if (mode.equals("E")) {

                getHeaderEditBinding().setDisabled(true);
                getBindingDepositNo().setDisabled(true);
                getBindingUnitCode().setDisabled(true);                
                getBindingDepositDate().setDisabled(true);
                getBindingFromDate().setDisabled(true);
                getBindingToDate().setDisabled(true);
                getBindingType().setDisabled(true);
                getBindingTdsGlCode().setDisabled(true);
                getBindingSection1().setDisabled(true);
                getBindingApprovedBy().setDisabled(true);
                getBindingTdsFlag().setDisabled(true);
                getPartyNameBinding().setDisabled(true);
                getBindingSrNo().setDisabled(true);
                getBindingPayDocNo().setDisabled(true);
                getBindingPaymentDate().setDisabled(true);
                getBindingPaymentAmt().setDisabled(true);
                getBindingTdsRate().setDisabled(true);
                getBindingTdsAmt().setDisabled(true);
                getBindingTdsCode().setDisabled(true);
                getBindingApprovedBy().setDisabled(false);

                  }
          else if (mode.equals("C")) 
          {
            //getDetailPopuldateBinding().setDisabled(false);
              getHeaderEditBinding().setDisabled(true);
              getBindingDepositNo().setDisabled(true);
              getBindingSection1().setDisabled(true); 
              getBindingUnitCode().setDisabled(true);
            //getBindingApprovedBy().setDisabled(true);

              getPartyNameBinding().setDisabled(true);
              getBindingSrNo().setDisabled(true);
              getBindingPayDocNo().setDisabled(true);
              getBindingPaymentDate().setDisabled(true);
              getBindingPaymentAmt().setDisabled(true);
              getBindingTdsRate().setDisabled(true);
              getBindingTdsAmt().setDisabled(true);
              getBindingTdsCode().setDisabled(true);
              
              
          } else if (mode.equals("V")) 
          {
            getDetailPopuldateBinding().setDisabled(true);
              getHeaderEditBinding().setDisabled(false);
                          
          }
          
      }
    
    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() 
    {
        cevmodecheck();
        return bindingOutputText;
    }

    public void editButtonAL(ActionEvent actionEvent)
    {
        if (bindingApprovedBy.getValue() == null) {
            cevmodecheck();
        } else {
            ADFUtils.showMessage("Record is Approved Can't be modified !", 2);
            ADFUtils.setEL("#{pageFlowScope.mode}", "V");
        }
    }

    public void setBindingSection1(RichInputText bindingSection1) {
        this.bindingSection1 = bindingSection1;
    }

    public RichInputText getBindingSection1() {
        return bindingSection1;
    }

    public void setPartyNameBinding(RichInputText partyNameBinding) {
        this.partyNameBinding = partyNameBinding;
    }

    public RichInputText getPartyNameBinding() {
        return partyNameBinding;
    }
}
