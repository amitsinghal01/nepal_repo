package terms.fin.transaction.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.validator.ValidatorException;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.layout.RichToolbar;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.domain.Date;
import oracle.jbo.domain.Number;

import org.apache.myfaces.trinidad.component.UIXSwitcher;

import terms.fin.transaction.model.view.PaymtsVORowImpl;


public class CreateVendorPaymentBean {
    private UIXSwitcher switcherBinding;
    private RichPopup tdsPopUpBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichSelectOneChoice advanceTypeBinding;
    private RichSelectOneChoice chequeBinding;
    private RichOutputText bindingOutputText;
    private RichButton editButtonBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichInputText docNumberBinding;
    private RichInputText docAmountBinding;
    private RichButton populateButtonBinding;
    private RichInputText otherAdvanceBinding;
    private RichInputText tdsAmountBinding;
    private RichInputText subCodeTypeBinding;
    private RichButton selectPOBinding;
    private RichSelectOneChoice docTypeBinding;
    private RichInputText chequeNoBinding;
    private RichInputDate chequeDateBinding;
    private RichSelectOneChoice paymentThroughBinding;
    private RichInputDate docDateBinding;
    private RichInputDate pdcDateBInding;
  //  private RichSelectOneChoice bankBinding;
    private RichInputText glCodeBinding;
    private RichInputDate asOnDateBinding;
    private RichInputText bankChargesBinding;
    private RichInputComboboxListOfValues costCenterBinding;
    private RichInputText jvNoBinding;
    private RichInputText voucherNoBinding;
    private RichInputText interUnitBinding;
    private RichInputComboboxListOfValues crSubCodeBinding;
    private RichInputComboboxListOfValues crGlCodeBinding;
    private RichInputText crGlUnitBinding;
    private RichInputComboboxListOfValues preparedByBinding;
    private RichInputComboboxListOfValues approvedByBinding;
    private RichInputDate approvalDateBinding;
    private RichInputText favourOfBinding;
    private RichInputText remarksBinding;
    private RichInputComboboxListOfValues subCodeBinding;
    private RichInputComboboxListOfValues glCodeSubCodeBinding;
    private RichButton applyTdsBinding;
    private RichInputText crGlDesBinding;
    private String select_value="Y";
    private RichTable paymentDetailBinding;
    private RichButton selectAllButtonBinding;
    private RichInputComboboxListOfValues bankBinding;
    private RichButton okPopupButtonBinding;
    private RichInputText bankCashBinding;
    private RichInputText flcAmountBinding;
    private RichInputText flcVouNoBinding;
    private RichShowDetailItem firstTabBinding;
    private RichShowDetailItem secondTabBinding;
    private RichShowDetailItem thirdTabBinding;
    private RichInputComboboxListOfValues currCodeBinding;
    private RichInputText currRateBinding;
    private RichToolbar toolbarBinding;
    private RichInputText amtToPayBinding;
    private RichInputText partyBalBinding;
    private RichInputText interUnitCdBinding;
    private RichInputText cgstBinding;
    private RichInputText sgstBinding;
    private RichInputText igstBinding;
    private RichInputText totalBillAmtBinding;
    private RichInputText adjustedAmtBinding;
    private RichInputText glBalBindings;
    private RichInputText glBalTypBinding;
    //select_value

    public CreateVendorPaymentBean() {
    }

    private String switcher_mode = "H";

    public String editVendorPaymentAction() {
        // Add event code here...
        return null;
    }

    public String deleteVendorPaymentAction() {
        OperationBinding binding = ADFUtils.findOperation("Delete");
        binding.execute();
        return null;
    }

//    public String saveVendorPayemntAction() {
//        OperationBinding binding = ADFUtils.findOperation("generatePayDocNoAndSave");
//        binding.execute();
//        if (binding.getResult() != null && binding.getResult().toString().equalsIgnoreCase("Y"))
//            ADFUtils.showMessage("Record saved and updated successfully", 2);
//        return null;
//    }

    public String saveVendorPayemntAction() {
        

                System.out.println("PAYMENT DETAIL ROW IS:"+ADFUtils.evaluateEL("#{bindings.PaymtsDetailVOIterator.estimatedRowCount}"));
                if(((Long)ADFUtils.evaluateEL("#{bindings.PaymtsDetailVOIterator.estimatedRowCount}")>0 && ADFUtils.evaluateEL("#{bindings.PayType.inputValue}").equals("PS"))|| ADFUtils.evaluateEL("#{bindings.PayType.inputValue}").equals("AV"))
                {
                OperationBinding binding = ADFUtils.findOperation("generatePayDocNoAndSave");
                binding.execute();
                
        //        System.out.println("Result is***************"+binding.getResult());
        //        if (binding.getResult() != null && binding.getResult().toString().equalsIgnoreCase("Y"))
        //            ADFUtils.showMessage("Record saved and updated successfully", 2);
        //        return null;

                System.out.println("Result is***************"+binding.getResult());
                
                
                            if (binding.getResult().equals("Y"))
                            {
                                ADFUtils.showMessage("Record saved successfully.New Payment No. is "+ADFUtils.evaluateEL("#{bindings.DocNo.inputValue}"), 2);
                                try
                                {
                                    System.out.println("Before commit");
                                ADFUtils.findOperation("Commit").execute();
                                    System.out.println("After commit");
                                    return "go_back";


                                }
                                catch(Exception ee)
                                {
                                    System.out.println("Error when commit data:"+ee);
                                ee.printStackTrace();
                                return null;
                                }
                            }
                            
                            else  if (binding.getResult().equals("U"))
                            {
                                ADFUtils.showMessage("Record updated successfully", 2);
                                ADFUtils.findOperation("Commit").execute();
                                return "go_back";

                            }
                }
                else
                {
                ADFUtils.showMessage("Please Populate Data in Detail Table",0);
                    return null;

                }
           
return null;
    }

    public String cancelVendorPayemntAction() {
        OperationBinding binding = ADFUtils.findOperation("Rollback");
        binding.execute();
        return "go_back";
    }

    public String selectPayDetailAction() {
       try
       {
            disabledFieldsOnConditions();
        }
       catch(Exception ee)
       {
        ee.printStackTrace();
        }
        
        
        DCIteratorBinding itr = ADFUtils.findIterator("PaymtsDetailVOIterator");
//        if (itr.getAllRowsInRange().length == 0) {
//            OperationBinding binding = ADFUtils.findOperation("populateAndInsertRecordIntoPayDetails");
//            binding.execute();
//        }
       
       System.out.println("Page Flow Scope is :"+ADFUtils.evaluateEL("#{pageFlowScope.mode}").equals("C"));
        if (ADFUtils.evaluateEL("#{pageFlowScope.mode}").equals("C")) {
            OperationBinding binding = ADFUtils.findOperation("populateAndInsertRecordIntoPayDetails");
            binding.execute();
        }
       
        switcher_mode = "D";
        AdfFacesContext.getCurrentInstance().addPartialTarget(switcherBinding);
        return null;
    }

    public String backToHeaderAction() {
//        try
//        {
//            flcAmountBinding.setValue(ttlFlcAmountBinding);
//        }        catch(Exception ee){ee.printStackTrace();}
        
        OperationBinding binding = ADFUtils.findOperation("checkForValidBillAmount");
        binding.execute();
      
      Number docAmount= (Number)ADFUtils.evaluateEL("#{bindings.DocAmount.inputValue}");
      
        System.out.println("ADFUtils.evaluateEL(\"#{bindings.PaymentThrough.inputValue}\")"+ADFUtils.evaluateEL("#{bindings.PaymentThrough.inputValue}"));
        System.out.println("ADFUtils.evaluateEL(\"#{bindings.TransTotalBillAmount.inputValue}\")"+ADFUtils.evaluateEL("#{bindings.TransTotalBillAmount.inputValue}"));
        
        
        
        System.out.println("RESULT backToHeaderAction IS :"+binding.getResult());
        System.out.println("Result docAmount.compareTo(new Number(0)) is :"+docAmount.compareTo(new Number(0)));
        if(!ADFUtils.evaluateEL("#{bindings.PaymentThrough.inputValue}").equals("A"))
        {
        
        if (binding.getResult() != null && binding.getResult().toString().equalsIgnoreCase("Y")  && docAmount.compareTo(new Number(0))==1) {
            switcher_mode = "H";
            AdfFacesContext.getCurrentInstance().addPartialTarget(switcherBinding);
        } 
        else
        {
            if((Long)ADFUtils.evaluateEL("#{bindings.PaymtsDetailVOIterator.estimatedRowCount}")>0)
            {
            ADFUtils.showMessage("Total Bill amount must be greater than zero(0).", 1);
            }
            else
            {
                switcher_mode = "H";
                AdfFacesContext.getCurrentInstance().addPartialTarget(switcherBinding);
            }
        }
        return null;
        }
        else
        {
            if(ADFUtils.evaluateEL("#{bindings.TransTotalBillAmount.inputValue}").equals("0"))
            {
            switcher_mode = "H";
            AdfFacesContext.getCurrentInstance().addPartialTarget(switcherBinding);
            }
            else
            {
                ADFUtils.showMessage("Total Bill amount must be zero(0).", 1); 
            }
        }
        return null;
    }

    public void setSwitcher_mode(String switcher_mode) {
        this.switcher_mode = switcher_mode;
    }

    public String getSwitcher_mode() {
        return switcher_mode;
    }

    public void setSwitcherBinding(UIXSwitcher switcherBinding) {
        this.switcherBinding = switcherBinding;
    }

    public UIXSwitcher getSwitcherBinding() {
        return switcherBinding;
    }

//Method Commented By Vikas Deep    

//    public void docDateValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
//        if (object != null) {
//            Date date = new Date();
//            if (((Date) object).compareTo(date.getCurrentDate().dateValue()) > 0)
//                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
//                                                              "Doc Date must be less then or equal to current date",
//                                                              "please select a valid date."));
//        } else
//            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Doc date required.",
//                                                          "please select a  date."));
//
//    }
    
    
//Method Modified By Vikas Deep    
    public void docDateValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (object != null) {
            String Str=object.toString();
            Date JboDate=null;
            java.util.Date UtilDate=null;
            String Flag="Y";
            try
            {
            JboDate=new Date(Str);
            Flag="Y";
            }
            catch(Exception ee)
            {
            System.out.println("Continue");
            UtilDate=new java.util.Date(Str);
            Flag="N";
            }
            System.out.println("JBO Date :"+JboDate);
            System.out.println("Util Date :"+UtilDate);

            System.out.println("FLAG IS "+Flag);
            OperationBinding op1 = ADFUtils.findOperation("checkForMonthAndYearVouSeriesForAllVouchers");
            if(Flag.equalsIgnoreCase("Y"))
            op1.getParamsMap().put("vou_dt",JboDate);
            if(Flag.equalsIgnoreCase("N"))
            op1.getParamsMap().put("vou_dt",UtilDate);
          //  op1.getParamsMap().put("FinYear","18-19");
            op1.getParamsMap().put("FinYear",ADFUtils.resolveExpression("#{pageFlowScope.finYear}"));
           // op1.getParamsMap().put("Vou_Type","B");
           op1.getParamsMap().put("Vou_Type","D");
            op1.getParamsMap().put("Vou_Series","P");
            op1.getParamsMap().put("Unit_Code",ADFUtils.evaluateEL("#{bindings.UnitCode.inputValue}"));
            // op1.getParamsMap().put("FinYear","18-19");
            op1.execute();
            System.out.println("GET RESULT:"+op1.getResult());
            if(op1.getResult()!=null && !op1.getResult().equals("Y"))
            {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,op1.getResult().toString(),"Please Select Another Date."));  
            }

        }
    }

    public void pdcDateValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (object != null) {
            DCIteratorBinding binding = ADFUtils.findIterator("PaymtsVOIterator");
            PaymtsVORowImpl currentRow = (PaymtsVORowImpl) binding.getCurrentRow();
            if (((Date) object).compareTo(currentRow.getDocDate()) < 0)
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                              "PDC date must be greater then or equal to Doc date",
                                                              "please select a valid date."));
        }

    }

    public void ocAmountValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (object != null) {
            if (((Number) object).compareTo(new Number(0)) < 0)
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                              "Other charges amount can not be less than Zero(0).",
                                                              ""));
        }

    }

    public void bankChargesValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (object != null) {
            if (((Number) object).compareTo(new Number(0)) < 0)
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                              "Bank charges amount can not be less than Zero(0).", ""));
        }


    }

    public void payDetailsChkBoxVCL(ValueChangeEvent vce) {
        //        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        //        OperationBinding binding = ADFUtils.findOperation("calculateTotalBillAmount");
        //        binding.execute();
    }

    public String applyTDSButtonAction() {
        
        try
        {
        disabledFieldsOnConditions();
        }
        catch(Exception ee)
        {
        ee.printStackTrace();
        }  
        
        PaymtsVORowImpl currentRow = (PaymtsVORowImpl) ADFUtils.findIterator("PaymtsVOIterator").getCurrentRow();
        if (currentRow.getDocAmount().compareTo(new Number(0)) > 0)
            ADFUtils.showPopup(getTdsPopUpBinding());
        else
            ADFUtils.showMessage("Document amount must greater than Zero(0)", 1);
        return null;
    }

    public void setTdsPopUpBinding(RichPopup tdsPopUpBinding) {
        this.tdsPopUpBinding = tdsPopUpBinding;
    }

    public RichPopup getTdsPopUpBinding() {
        return tdsPopUpBinding;
    }

    public void tdsPopUpDialogListener(DialogEvent dialogEvent) {
        System.out.println("*****EVENT WHEN OK");
        if(dialogEvent.getOutcome().name().equals("ok"))
        {
            System.out.println("EVENT WHEN OK");
            tdsPopUpBinding.hide();
        }
    }

    public void tdsAmountValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (object != null && object.toString().trim().length() > 0) {
            if (((Number) object).compareTo(new Number(0)) <= 0)
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                              "Tds amount must be greater than Zero(0).", ""));
            else {
                PaymtsVORowImpl currentRow =
                    (PaymtsVORowImpl) ADFUtils.findIterator("PaymtsVOIterator").getCurrentRow();
                if (((Number) object).compareTo(currentRow.getTransTotalAmountForTds() != null ?
                                                currentRow.getTransTotalAmountForTds() : new Number(0)) > 0)
                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                  "Tds amount must be less than or equal to total amount.",
                                                                  ""));
            }
        } else {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Tds code is required.",
                                                          "Please select tds code."));
        }

    }

    public void saveButtonAL(ActionEvent actionEvent){
        System.out.println("PAYMENT DETAIL ROW IS:"+ADFUtils.evaluateEL("#{bindings.PaymtsDetailVOIterator.estimatedRowCount}"));
        if(((Long)ADFUtils.evaluateEL("#{bindings.PaymtsDetailVOIterator.estimatedRowCount}")>0 && ADFUtils.evaluateEL("#{bindings.PayType.inputValue}").equals("PS"))|| ADFUtils.evaluateEL("#{bindings.PayType.inputValue}").equals("AV"))
        {
        OperationBinding binding = ADFUtils.findOperation("generatePayDocNoAndSave");
        binding.execute();
        
//        System.out.println("Result is***************"+binding.getResult());
//        if (binding.getResult() != null && binding.getResult().toString().equalsIgnoreCase("Y"))
//            ADFUtils.showMessage("Record saved and updated successfully", 2);
//        return null;

        System.out.println("Result is***************"+binding.getResult());
        
        
                    if (binding.getResult().equals("Y"))
                    {
                        ADFUtils.showMessage("Record saved successfully.New Payment No. is "+ADFUtils.evaluateEL("#{bindings.DocNo.inputValue}"), 2);
                        try
                        {
                            System.out.println("Before commit");
                        ADFUtils.findOperation("Commit").execute();
                            System.out.println("After commit");
                            if(approvedByBinding.getValue()!=null)
                            {
                                ADFUtils.setEL("#{pageFlowScope.mode}","V");
                                cevmodecheck();
                                AdfFacesContext.getCurrentInstance().addPartialTarget(toolbarBinding);
                            }

                        }
                        catch(Exception ee)
                        {
                            System.out.println("Error when commit data:"+ee);
                        ee.printStackTrace();
                        }
                    }
                    
                    else  if (binding.getResult().equals("U"))
                    {
                        ADFUtils.showMessage("Record updated successfully", 2);
                        ADFUtils.findOperation("Commit").execute();
                        if(approvedByBinding.getValue()!=null)
                        {
                            ADFUtils.setEL("#{pageFlowScope.mode}","V");
                            cevmodecheck();
                            AdfFacesContext.getCurrentInstance().addPartialTarget(toolbarBinding);
                        }

                    }
        }
        else
        {
        ADFUtils.showMessage("Please Populate Data in Detail Table",0);
        }
    }

    public void approvedByVCE(ValueChangeEvent valueChangeEvent)     {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        
                    try
                    {
                        OperationBinding OprAutho=ADFUtils.findOperation("checkAuthorityForApprovalVendorPayments");
                        OprAutho.getParamsMap().put("EmpCode",ADFUtils.evaluateEL("#{pageFlowScope.empCode}"));
                       // OprAutho.getParamsMap().put("EmpCode","SWE161");
                        Object OprResult=OprAutho.execute();
                    }
                    catch(Exception ee)
                    {
                    ee.printStackTrace();
                    }    
        
    }

    public void chequeNoVCE(ValueChangeEvent valueChangeEvent)
    {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(chequeBinding.getValue()!=null && chequeBinding.getValue().equals("C"))
        {
            OperationBinding opr= ADFUtils.findOperation("checkChequeRangeVendorPayments");
            opr.execute();
            if(opr.getResult().equals("N"))
            ADFUtils.showMessage("Invalid Check Number,Please Check.",0);
        }
    
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setAdvanceTypeBinding(RichSelectOneChoice advanceTypeBinding) {
        this.advanceTypeBinding = advanceTypeBinding;
    }

    public RichSelectOneChoice getAdvanceTypeBinding() {
        return advanceTypeBinding;
    }

    public void setChequeBinding(RichSelectOneChoice chequeBinding) {
        this.chequeBinding = chequeBinding;
    }

    public RichSelectOneChoice getChequeBinding() {
        return chequeBinding;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setEditButtonBinding(RichButton editButtonBinding) {
        this.editButtonBinding = editButtonBinding;
    }

    public RichButton getEditButtonBinding() 
    {
        return editButtonBinding;
    }

    public void editButtonAL(ActionEvent actionEvent) {
        if(approvedByBinding.getValue()==null)
        {
        cevmodecheck();
        }
        else
        {
        ADFUtils.showMessage("Approved Record Can't Be Modify.",2);
        ADFUtils.setEL("#{pageFlowScope.mode}","V");
        }
        
    }
    
    
    private void cevmodecheck(){
        System.out.println("**********Method calling**********");
        System.out.println("PAGE MODE IS:"+ADFUtils.resolveExpression("#{pageFlowScope.mode}"));
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }
    
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
               try {
                   Method method1 =
                       component.getClass().getMethod("setDisabled", boolean.class);
                   if (method1 != null) {
                       method1.invoke(component, valueComponent);
                   }
               } catch (NoSuchMethodException e) {
                   try {
                       Method method =
                           component.getClass().getMethod("setReadOnly", boolean.class);
                       if (method != null) {
                           method.invoke(component, valueComponent);
                       }
                   } catch (Exception e1) {
                       // e.printStackTrace();//silently eat this exception.
                   }


               } catch (Exception e) {
                   // e.printStackTrace();//silently eat this exception.
               }
               List<UIComponent> childComponents = component.getChildren();
               for (UIComponent comp : childComponents) {
                   makeComponentHierarchyReadOnly(comp, valueComponent);
               }
           }
    
    public void cevModeDisableComponent(String mode)
    {
            if (mode.equals("E")) 
            {
                getInterUnitCdBinding().setDisabled(true);
                getDocNumberBinding().setDisabled(true);
                getDocAmountBinding().setDisabled(true);
                getSubCodeTypeBinding().setDisabled(true);
             //   getPopulateButtonBinding().setDisabled(true);
              //  getOtherAdvanceBinding().setDisabled(true);
                getTdsAmountBinding().setDisabled(true);
                getJvNoBinding().setDisabled(true);
                getVoucherNoBinding().setDisabled(true);
                getInterUnitBinding().setDisabled(true);
                getCrSubCodeBinding().setDisabled(true);
                getCrGlCodeBinding().setDisabled(true);
                getCrGlUnitBinding().setDisabled(true);
                getCrGlDesBinding().setDisabled(true);
                
                getUnitCodeBinding().setDisabled(true);
                getDocTypeBinding().setDisabled(true);
                getAdvanceTypeBinding().setDisabled(true);
                getPaymentThroughBinding().setDisabled(true);
                getPdcDateBInding().setDisabled(true);
                //getBankBinding().setDisabled(true);
                //getChequeNoBinding().setDisabled(true);
                //getChequeDateBinding().setDisabled(true);
                //getChequeBinding().setDisabled(true);
                getSubCodeBinding().setDisabled(true);
                getGlCodeBinding().setDisabled(true);
                getGlCodeSubCodeBinding().setDisabled(true);
                getAsOnDateBinding().setDisabled(true);
                getSelectAllButtonBinding().setDisabled(true);
                //getFavourOfBinding().setDisabled(true);
                getGlCodeBinding().setDisabled(true);
                getPreparedByBinding().setDisabled(true);
                getPdcDateBInding().setDisabled(true);
                getGlCodeSubCodeBinding().setDisabled(true);
                getBankCashBinding().setDisabled(true);
                getDocDateBinding().setDisabled(true);
                getFlcVouNoBinding().setDisabled(true);
                getCurrCodeBinding().setDisabled(true);
                getCurrRateBinding().setDisabled(true);
                getEditButtonBinding().setDisabled(true);
                getPartyBalBinding().setDisabled(true);
                getGlBalBindings().setDisabled(true);
                getGlBalTypBinding().setDisabled(true);

                if(docTypeBinding.getValue().equals("PS"))
                {
                populateButtonBinding.setDisabled(false);
                }
                else
                {
                populateButtonBinding.setDisabled(true);
                }
                
                if(docTypeBinding.getValue().equals("AV") && advanceTypeBinding.getValue().equals("PO"))
                {
                getSelectPOBinding().setDisabled(false);
                getApplyTdsBinding().setDisabled(true);
                }
                else if(docTypeBinding.getValue().equals("PS") && advanceTypeBinding.getValue().equals("OA"))
                {
                getSelectPOBinding().setDisabled(true);
                getApplyTdsBinding().setDisabled(false);
                
                }


                System.out.println("Edit Mode is:"+mode);
            } 
            else if (mode.equals("C"))
            {
            getInterUnitCdBinding().setDisabled(true);
            getPartyBalBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
            getDocNumberBinding().setDisabled(true);
            getDocAmountBinding().setDisabled(true);
            getSubCodeTypeBinding().setDisabled(true);
            //getPopulateButtonBinding().setDisabled(true);
            getOtherAdvanceBinding().setDisabled(true);
            getTdsAmountBinding().setDisabled(true);
            getSelectPOBinding().setDisabled(true);
            getApplyTdsBinding().setDisabled(true);
            getFlcVouNoBinding().setDisabled(true);
                getJvNoBinding().setDisabled(true);
                getVoucherNoBinding().setDisabled(true);
                getInterUnitBinding().setDisabled(true);
                getCrSubCodeBinding().setDisabled(true);
                getCrGlCodeBinding().setDisabled(true);
                getCrGlUnitBinding().setDisabled(true);
                getCrGlDesBinding().setDisabled(true);
                getGlCodeBinding().setDisabled(true);
                //getFavourOfBinding().setDisabled(true);
                getGlCodeBinding().setDisabled(true);
                getPreparedByBinding().setDisabled(true);
                getGlCodeSubCodeBinding().setDisabled(true);
                getPdcDateBInding().setDisabled(true);
                getAdvanceTypeBinding().setDisabled(true);
                getBankCashBinding().setDisabled(true);
                getEditButtonBinding().setDisabled(true);
                getGlBalBindings().setDisabled(true);
                getGlBalTypBinding().setDisabled(true);
                
            System.out.println("Create Mode is:"+mode);
            } else if (mode.equals("V"))
            {
                getFirstTabBinding().setDisabled(false);
                getSecondTabBinding().setDisabled(false);
                getThirdTabBinding().setDisabled(false);
                getPopulateButtonBinding().setDisabled(false);

            System.out.println("View Mode is:"+mode);
            
            }
            
        }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setDocNumberBinding(RichInputText docNumberBinding) {
        this.docNumberBinding = docNumberBinding;
    }

    public RichInputText getDocNumberBinding() {
        return docNumberBinding;
    }

    public void setDocAmountBinding(RichInputText docAmountBinding) {
        this.docAmountBinding = docAmountBinding;
    }

    public RichInputText getDocAmountBinding() {
        return docAmountBinding;
    }


    public void setPopulateButtonBinding(RichButton populateButtonBinding) {
        this.populateButtonBinding = populateButtonBinding;
    }

    public RichButton getPopulateButtonBinding() {
        return populateButtonBinding;
    }

    public void setOtherAdvanceBinding(RichInputText otherAdvanceBinding) {
        this.otherAdvanceBinding = otherAdvanceBinding;
    }

    public RichInputText getOtherAdvanceBinding() {
        return otherAdvanceBinding;
    }

    public void setTdsAmountBinding(RichInputText tdsAmountBinding) {
        this.tdsAmountBinding = tdsAmountBinding;
    }

    public RichInputText getTdsAmountBinding() {
        return tdsAmountBinding;
    }

    public void setSubCodeTypeBinding(RichInputText subCodeTypeBinding) {
        this.subCodeTypeBinding = subCodeTypeBinding;
    }

    public RichInputText getSubCodeTypeBinding() {
        return subCodeTypeBinding;
    }

    public void setSelectPOBinding(RichButton selectPOBinding) {
        this.selectPOBinding = selectPOBinding;
    }

    public RichButton getSelectPOBinding() {
        return selectPOBinding;
    }

    public void setDocTypeBinding(RichSelectOneChoice docTypeBinding) {
        this.docTypeBinding = docTypeBinding;
    }

    public RichSelectOneChoice getDocTypeBinding() {
        return docTypeBinding;
    }

    public void docTypeVCE(ValueChangeEvent valueChangeEvent) 
    {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        System.out.println("DOC TYPE NEW VALUE :"+valueChangeEvent.getNewValue());
        if(valueChangeEvent.getNewValue()!=null)
        {
        if(valueChangeEvent.getNewValue().equals("AV"))
        {
            getAdvanceTypeBinding().setDisabled(false);
            //getSelectPOBinding().setDisabled(false);
            getPopulateButtonBinding().setDisabled(true);
            getAdvanceTypeBinding().setRequired(true);
            getApplyTdsBinding().setDisabled(false);
        }
        else if(valueChangeEvent.getNewValue().equals("PS"))
        {
            getAdvanceTypeBinding().setDisabled(true);
            getSelectPOBinding().setDisabled(true);
            getPopulateButtonBinding().setDisabled(false);
            getApplyTdsBinding().setDisabled(true);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(populateButtonBinding);
        AdfFacesContext.getCurrentInstance().addPartialTarget(advanceTypeBinding);
        AdfFacesContext.getCurrentInstance().addPartialTarget(applyTdsBinding);
        AdfFacesContext.getCurrentInstance().addPartialTarget(asOnDateBinding);
        }
        disableAndEnabledOtherAdvance();
    }

    public void setChequeNoBinding(RichInputText chequeNoBinding) {
        this.chequeNoBinding = chequeNoBinding;
    }

    public RichInputText getChequeNoBinding() {
        return chequeNoBinding;
    }

    public void setChequeDateBinding(RichInputDate chequeDateBinding) {
        this.chequeDateBinding = chequeDateBinding;
    }

    public RichInputDate getChequeDateBinding() {
        return chequeDateBinding;
    }

    public void chequeVCE(ValueChangeEvent valueChangeEvent) 
    {
    valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        chequeNoBinding.resetValue();
        if(valueChangeEvent.equals("R")){
                chequeNoBinding.setValue("RTGS");
            }
    }

    public void setPaymentThroughBinding(RichSelectOneChoice paymentThroughBinding) {
        this.paymentThroughBinding = paymentThroughBinding;
    }

    public RichSelectOneChoice getPaymentThroughBinding() {
        return paymentThroughBinding;
    }

    public void paymentsThroughVCE(ValueChangeEvent valueChangeEvent) 
    {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        chequeNoBinding.resetValue();
        System.out.println("NEW VALUE PAYMENT THROUGH :"+valueChangeEvent.getNewValue());
        if(valueChangeEvent.getNewValue()!=null)
        {
        if(valueChangeEvent.getNewValue().equals("B"))
        {
                    getChequeBinding().setDisabled(false);
                    getChequeNoBinding().setDisabled(false);
                    getChequeDateBinding().setDisabled(false);
                    getBankBinding().setDisabled(false);
                    //getFavourOfBinding().setDisabled(false);
                    getPdcDateBInding().setDisabled(false);

        }
        else if(valueChangeEvent.getNewValue().equals("C"))
        {
            getChequeBinding().setDisabled(true);
            getChequeNoBinding().setDisabled(true);
            getChequeDateBinding().setDisabled(true);
            getBankBinding().setDisabled(false);
            //getFavourOfBinding().setDisabled(true);
            getPdcDateBInding().setDisabled(true);

        }
        else if(valueChangeEvent.getNewValue().equals("A"))
        {
            getChequeBinding().setDisabled(true);
            getChequeNoBinding().setDisabled(true);
            getChequeDateBinding().setDisabled(true);
            getBankBinding().setDisabled(true);
            //getFavourOfBinding().setDisabled(true);
            getPdcDateBInding().setDisabled(true);

        }
            AdfFacesContext.getCurrentInstance().addPartialTarget(pdcDateBInding);
            AdfFacesContext.getCurrentInstance().addPartialTarget(favourOfBinding);
            AdfFacesContext.getCurrentInstance().addPartialTarget(chequeBinding);
            AdfFacesContext.getCurrentInstance().addPartialTarget(chequeNoBinding);
            AdfFacesContext.getCurrentInstance().addPartialTarget(chequeDateBinding);
            AdfFacesContext.getCurrentInstance().addPartialTarget(bankBinding);

        }
        
        
    }

    public void setDocDateBinding(RichInputDate docDateBinding) {
        this.docDateBinding = docDateBinding;
    }

    public RichInputDate getDocDateBinding() {
        return docDateBinding;
    }

    public void setPdcDateBInding(RichInputDate pdcDateBInding) {
        this.pdcDateBInding = pdcDateBInding;
    }

    public RichInputDate getPdcDateBInding() {
        return pdcDateBInding;
    }

    public void setGlCodeBinding(RichInputText glCodeBinding) {
        this.glCodeBinding = glCodeBinding;
    }

    public RichInputText getGlCodeBinding() {
        return glCodeBinding;
    }

    public void setAsOnDateBinding(RichInputDate asOnDateBinding) {
        this.asOnDateBinding = asOnDateBinding;
    }

    public RichInputDate getAsOnDateBinding() {
        return asOnDateBinding;
    }

    public void setBankChargesBinding(RichInputText bankChargesBinding) {
        this.bankChargesBinding = bankChargesBinding;
    }

    public RichInputText getBankChargesBinding() {
        return bankChargesBinding;
    }

    public void setCostCenterBinding(RichInputComboboxListOfValues costCenterBinding) {
        this.costCenterBinding = costCenterBinding;
    }

    public RichInputComboboxListOfValues getCostCenterBinding() {
        return costCenterBinding;
    }

    public void setJvNoBinding(RichInputText jvNoBinding) {
        this.jvNoBinding = jvNoBinding;
    }

    public RichInputText getJvNoBinding() {
        return jvNoBinding;
    }

    public void setVoucherNoBinding(RichInputText voucherNoBinding) {
        this.voucherNoBinding = voucherNoBinding;
    }

    public RichInputText getVoucherNoBinding() {
        return voucherNoBinding;
    }

    public void setInterUnitBinding(RichInputText interUnitBinding) {
        this.interUnitBinding = interUnitBinding;
    }

    public RichInputText getInterUnitBinding() {
        return interUnitBinding;
    }

    public void setCrSubCodeBinding(RichInputComboboxListOfValues crSubCodeBinding) {
        this.crSubCodeBinding = crSubCodeBinding;
    }

    public RichInputComboboxListOfValues getCrSubCodeBinding() {
        return crSubCodeBinding;
    }

    public void setCrGlCodeBinding(RichInputComboboxListOfValues crGlCodeBinding) {
        this.crGlCodeBinding = crGlCodeBinding;
    }

    public RichInputComboboxListOfValues getCrGlCodeBinding() {
        return crGlCodeBinding;
    }

    public void setCrGlUnitBinding(RichInputText crGlUnitBinding) {
        this.crGlUnitBinding = crGlUnitBinding;
    }

    public RichInputText getCrGlUnitBinding() {
        return crGlUnitBinding;
    }

    public void setPreparedByBinding(RichInputComboboxListOfValues preparedByBinding) {
        this.preparedByBinding = preparedByBinding;
    }

    public RichInputComboboxListOfValues getPreparedByBinding() {
        return preparedByBinding;
    }

    public void setApprovedByBinding(RichInputComboboxListOfValues approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputComboboxListOfValues getApprovedByBinding() {
        return approvedByBinding;
    }

    public void setApprovalDateBinding(RichInputDate approvalDateBinding) {
        this.approvalDateBinding = approvalDateBinding;
    }

    public RichInputDate getApprovalDateBinding() {
        return approvalDateBinding;
    }


//Disbled Field According To Some Conditions    
    public void disabledFieldsOnConditions()
    {
        getDocDateBinding().setDisabled(true);
        getDocTypeBinding().setDisabled(true);
        getAdvanceTypeBinding().setDisabled(true);
        getPaymentThroughBinding().setDisabled(true);
        getPdcDateBInding().setDisabled(true);
        getBankBinding().setDisabled(true);
        getChequeBinding().setDisabled(true);
        getChequeNoBinding().setDisabled(true);
        getChequeDateBinding().setDisabled(true);
        //getFavourOfBinding().setDisabled(true);
        //getRemarksBinding().setDisabled(true);
        getSubCodeBinding().setDisabled(true);
        getGlCodeSubCodeBinding().setDisabled(true);
        getPdcDateBInding().setDisabled(true);
        
        
    }

    public void setFavourOfBinding(RichInputText favourOfBinding) {
        this.favourOfBinding = favourOfBinding;
    }

    public RichInputText getFavourOfBinding() {
        return favourOfBinding;
    }

    public void setRemarksBinding(RichInputText remarksBinding) {
        this.remarksBinding = remarksBinding;
    }

    public RichInputText getRemarksBinding() {
        return remarksBinding;
    }

    public void setSubCodeBinding(RichInputComboboxListOfValues subCodeBinding) {
        this.subCodeBinding = subCodeBinding;
    }

    public RichInputComboboxListOfValues getSubCodeBinding() {
        return subCodeBinding;
    }

    public void setGlCodeSubCodeBinding(RichInputComboboxListOfValues glCodeSubCodeBinding) {
        this.glCodeSubCodeBinding = glCodeSubCodeBinding;
    }

    public RichInputComboboxListOfValues getGlCodeSubCodeBinding() {
        return glCodeSubCodeBinding;
    }

    public void advanceTypeVCE(ValueChangeEvent valueChangeEvent)
    {
    valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
    disableAndEnabledOtherAdvance();
    }
    
    public void disableAndEnabledOtherAdvance()
    {
        System.out.println("PAY TYPE:"+ADFUtils.evaluateEL("#{bindings.PayType.inputValue}"));
        System.out.println("Advance TYPE:"+ADFUtils.evaluateEL("#{bindings.AdvType.inputValue}"));

    if(ADFUtils.evaluateEL("#{bindings.PayType.inputValue}").equals("AV") && ADFUtils.evaluateEL("#{bindings.AdvType.inputValue}").equals("OA"))
    {
        getOtherAdvanceBinding().setDisabled(false);
        getSelectPOBinding().setDisabled(true);
    }
    else if(ADFUtils.evaluateEL("#{bindings.PayType.inputValue}").equals("AV") && ADFUtils.evaluateEL("#{bindings.AdvType.inputValue}").equals("PO"))
    {
        getOtherAdvanceBinding().setDisabled(true);
        getSelectPOBinding().setDisabled(false);
    }
        AdfFacesContext.getCurrentInstance().addPartialTarget(selectPOBinding);
        AdfFacesContext.getCurrentInstance().addPartialTarget(otherAdvanceBinding);
  
    }

    public void otherAmountVCE(ValueChangeEvent valueChangeEvent) 
    {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(valueChangeEvent.getNewValue()!=null)
        {
        Row row=(Row) ADFUtils.evaluateEL("#{bindings.PaymtsVOIterator.currentRow}");
        row.setAttribute("PaymentAmount",valueChangeEvent.getNewValue());
        row.setAttribute("TransTotalAmountForTds",valueChangeEvent.getNewValue());
        amountCalcVCL(valueChangeEvent);
        }
    }

    public void paymentAmountVCE(ValueChangeEvent valueChangeEvent) 
    {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        Row row=(Row) ADFUtils.evaluateEL("#{bindings.PaymtsVOIterator.currentRow}");
        row.setAttribute("TransTotalAmountForTds",valueChangeEvent.getNewValue());
        if(ADFUtils.evaluateEL("#{bindings.TdsPaymentCode.inputValue}")!=null && ADFUtils.evaluateEL("#{bindings.PaymentAmount.inputValue}")!=null)
        {
        ADFUtils.findOperation("tdsAmountForVendorPayment").execute();
        }  
        amountCalcVCL(valueChangeEvent);
    }

    public void tdsCodeVCE(ValueChangeEvent valueChangeEvent) {

    }

    public void setApplyTdsBinding(RichButton applyTdsBinding) {
        this.applyTdsBinding = applyTdsBinding;
    }

    public RichButton getApplyTdsBinding() {
        return applyTdsBinding;
    }

    public void setCrGlDesBinding(RichInputText crGlDesBinding) {
        this.crGlDesBinding = crGlDesBinding;
    }

    public RichInputText getCrGlDesBinding() {
        return crGlDesBinding;
    }

    public void selectAllAL(ActionEvent actionEvent) {
        System.out.println("select_value:"+select_value);
        OperationBinding   opr=ADFUtils.findOperation("selectAndUnSelectAllVendorPayment");
        opr.getParamsMap().put("mode",select_value); 
        opr.execute();
        
        if(selectAllButtonBinding.equals("Select All"))
        {
        selectAllButtonBinding.setText("UnSelect All");
        }
        else
        {
            selectAllButtonBinding.setText("Select All");
        }
            
        
        
        if(select_value.equalsIgnoreCase("Y"))
        {
            select_value="N";
        }
        else
        {
            select_value="Y";
        }
        System.out.println("After change of select_value:"+select_value);
        AdfFacesContext.getCurrentInstance().addPartialTarget(paymentDetailBinding);
    }

    public void setPaymentDetailBinding(RichTable paymentDetailBinding) {
        this.paymentDetailBinding = paymentDetailBinding;
    }

    public RichTable getPaymentDetailBinding() {
        return paymentDetailBinding;
    }

    public void setSelectAllButtonBinding(RichButton selectAllButtonBinding) {
        this.selectAllButtonBinding = selectAllButtonBinding;
    }

    public RichButton getSelectAllButtonBinding() {
        return selectAllButtonBinding;
    }

    public void docTypeValidator(FacesContext facesContext, UIComponent uIComponent, Object object) 
    {
        if(object!=null && ADFUtils.evaluateEL("#{bindings.PaymentThrough.inputValue}")!=null)
        {
        if(ADFUtils.evaluateEL("#{bindings.PaymentThrough.inputValue}").equals("A"))
        {
            if(!object.equals("PS"))
            {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,"Adjustment Allow Only Against Payment Schedule.","Please Check."));  
            }
        }
        }
    }

    public void pyamentThroughValidator(FacesContext facesContext, UIComponent uIComponent, Object object) 
    {
       if(object!=null && ADFUtils.evaluateEL("#{bindings.PayType.inputValue}")!=null)
        {
        if(object.equals("A"))
        {
            if(!ADFUtils.evaluateEL("#{bindings.PayType.inputValue}").equals("PS"))
            {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,"Adjustment Allow Only Against Payment Schedule.","Please Check."));  
            }
        }
        }

    }

    public void setBankBinding(RichInputComboboxListOfValues bankBinding) {
        this.bankBinding = bankBinding;
    }

    public RichInputComboboxListOfValues getBankBinding() {
        return bankBinding;
    }

    public void approvalDateValidator(FacesContext facesContext, UIComponent uIComponent, Object object) 
    {
        
        
        if(!paymentThroughBinding.getValue().equals("A"))
        {
         if(object!=null)
         {
          
          
          //Call Max Vou Date Code Here   
          String Str=object.toString();
          Date JboDate=null;
          java.util.Date UtilDate=null;
          String Flag="Y";
          try
          {
          JboDate=new Date(Str);
          Flag="Y";
          }
          catch(Exception ee)
          {
          System.out.println("Continue");
          UtilDate=new java.util.Date(Str);
          Flag="N";
          }
          System.out.println("JBO Date :"+JboDate);
          System.out.println("Util Date :"+UtilDate);

          System.out.println("FLAG IS "+Flag);
          OperationBinding op1 = ADFUtils.findOperation("checkForMonthAndYearVouSeriesForAllVouchers");
          if(Flag.equalsIgnoreCase("Y"))
          op1.getParamsMap().put("vou_dt",JboDate);
          if(Flag.equalsIgnoreCase("N"))
          op1.getParamsMap().put("vou_dt",UtilDate);
          //op1.getParamsMap().put("FinYear","18-19");
           op1.getParamsMap().put("FinYear",ADFUtils.resolveExpression("#{pageFlowScope.finYear}"));
          op1.getParamsMap().put("Vou_Type",paymentThroughBinding.getValue());
          op1.getParamsMap().put("Vou_Series","P");
          op1.getParamsMap().put("Unit_Code",ADFUtils.evaluateEL("#{bindings.UnitCode.inputValue}"));
          // op1.getParamsMap().put("FinYear","18-19");
          op1.execute();
          System.out.println("GET RESULT APPROVAL VALIDATOR :"+op1.getResult());
         if(op1.getResult()!=null && op1.getResult().equals("Y"))
          {    
         Date pdc_date=null;
         Date approval_date=(Date)object;
         Date doc_date=(Date)docDateBinding.getValue();
        if(pdcDateBInding.getValue()!=null)
        {
        pdc_date=(Date)pdcDateBInding.getValue();
        }
        System.out.println("Approval Date :"+approval_date);
        System.out.println("PDC DATE IS:"+pdc_date);
        System.out.println("DOC DATE:"+doc_date);
            if(pdc_date!=null)
            {
                System.out.println("pdc_date.compareTo(approval_date)"+pdc_date.compareTo(approval_date));
            if(pdc_date.compareTo(approval_date)==0)
            {
            //
                System.out.println("PDC DATE IF");
            }
            else
            {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,"Approval Date must be equals to PDC Date.","Please Check."));  
    
            }
            }
            else
            {
                System.out.println("DOC DATE ELSE ");
                System.out.println("approval_date.compareTo(doc_date)"+approval_date.compareTo(doc_date));
                if(approval_date.compareTo(doc_date)==-1)
                {
                    System.out.println("DOC DATE IF ");

                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,"Approval Date must be Greater than or equals to Document date.","Please Check."));  

                }
            
            
            }
            
            
         }
         else
           {
               throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,op1.getResult().toString(),"Please Select Another Date."));  
           }  
            
         }
        }
    }

    public void okPopupButtonAL(ActionEvent actionEvent) {
        System.out.println("popup Button AL ");
        tdsPopUpBinding.hide();
    }

    public void setOkPopupButtonBinding(RichButton okPopupButtonBinding) {
        this.okPopupButtonBinding = okPopupButtonBinding;
    }

    public RichButton getOkPopupButtonBinding() {
        return okPopupButtonBinding;
    }

    public void bankCashVCE(ValueChangeEvent valueChangeEvent)
    {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(valueChangeEvent.getNewValue()!=null)
        {
            ADFUtils.findOperation("checkPrifixVendorPayment").execute();
        }
    }

    public void setBankCashBinding(RichInputText bankCashBinding) {
        this.bankCashBinding = bankCashBinding;
    }

    public RichInputText getBankCashBinding() {
        return bankCashBinding;
    }

    public void setFlcAmountBinding(RichInputText flcAmountBinding) {
        this.flcAmountBinding = flcAmountBinding;
    }

    public RichInputText getFlcAmountBinding() {
        return flcAmountBinding;
    }

    public void setFlcVouNoBinding(RichInputText flcVouNoBinding) {
        this.flcVouNoBinding = flcVouNoBinding;
    }

    public RichInputText getFlcVouNoBinding() {
        return flcVouNoBinding;
    }

    public void setFirstTabBinding(RichShowDetailItem firstTabBinding) {
        this.firstTabBinding = firstTabBinding;
    }

    public RichShowDetailItem getFirstTabBinding() {
        return firstTabBinding;
    }

    public void setSecondTabBinding(RichShowDetailItem secondTabBinding) {
        this.secondTabBinding = secondTabBinding;
    }

    public RichShowDetailItem getSecondTabBinding() {
        return secondTabBinding;
    }

    public void setThirdTabBinding(RichShowDetailItem thirdTabBinding) {
        this.thirdTabBinding = thirdTabBinding;
    }

    public RichShowDetailItem getThirdTabBinding() {
        return thirdTabBinding;
    }

    public void setCurrCodeBinding(RichInputComboboxListOfValues currCodeBinding) {
        this.currCodeBinding = currCodeBinding;
    }

    public RichInputComboboxListOfValues getCurrCodeBinding() {
        return currCodeBinding;
    }

    public void setCurrRateBinding(RichInputText currRateBinding) {
        this.currRateBinding = currRateBinding;
    }

    public RichInputText getCurrRateBinding() {
        return currRateBinding;
    }

    public void setToolbarBinding(RichToolbar toolbarBinding) {
        this.toolbarBinding = toolbarBinding;
    }

    public RichToolbar getToolbarBinding() {
        return toolbarBinding;
    }

    public void adjustAmtValidator(FacesContext facesContext, UIComponent uIComponent, Object object) 
    {
        if(object!=null && amtToPayBinding.getValue()!=null)
        {
        Number adjAmt=(Number)object;
        Number amtToPay = (Number) amtToPayBinding.getValue();
        
        System.out.println("ADJ AMT:=>"+adjAmt+"\nAMT TO PAY:=>"+amtToPay);
        System.out.println("adjAmt.compareTo(amtToPay)="+adjAmt.compareTo(amtToPay));
        if(adjAmt.compareTo(amtToPay)==1)
        {
        throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                      "Adjust Amt. must be less than or equals to Amount To Pay",
                                                      "please check"));
        }
        }
    }

    public void setAmtToPayBinding(RichInputText amtToPayBinding) {
        this.amtToPayBinding = amtToPayBinding;
    }

    public RichInputText getAmtToPayBinding() {
        return amtToPayBinding;
    }

    public void chequeNoValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        String str = (String) object;
        if (!str.isEmpty() && chequeBinding.getValue() != null && bankBinding.getValue()!=null) {
            System.out.println("In First Bracket");
            if (chequeBinding.getValue().toString().equalsIgnoreCase("C")) {
                System.out.println("In Second Bracket");
                if (str.length() == 6) {
                }
                   else {
                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                  "Entered Cheque No./DD No. must be of 6 digits.",
                                                                  null));
                }
            }
        }
    }

    public void setPartyBalBinding(RichInputText partyBalBinding) {
        this.partyBalBinding = partyBalBinding;
    }

    public RichInputText getPartyBalBinding() {
        return partyBalBinding;
    }

    public void setInterUnitCdBinding(RichInputText interUnitCdBinding) {
        this.interUnitCdBinding = interUnitCdBinding;
    }

    public RichInputText getInterUnitCdBinding() {
        return interUnitCdBinding;
    }

    public void amountCalcVCL(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        Number Amount=(Number)(totalBillAmtBinding.getValue()!=null?totalBillAmtBinding.getValue():new Number(0));
        BigDecimal Igst=(BigDecimal)(igstBinding.getValue()!=null?igstBinding.getValue():new BigDecimal(0));
        BigDecimal Cgst=(BigDecimal)((cgstBinding.getValue()!=null?cgstBinding.getValue():new BigDecimal(0)));
        BigDecimal Sgst= (BigDecimal)((sgstBinding.getValue()!=null?sgstBinding.getValue():new BigDecimal(0)));
        Number BankCharges=(Number)(bankChargesBinding.getValue() != null ? bankChargesBinding.getValue() :new Number(0));
        Number otherAmount=(Number)(otherAdvanceBinding.getValue() != null ? otherAdvanceBinding.getValue() :new Number(0));
        Number tdsAmount=(Number)(tdsAmountBinding.getValue() != null ? tdsAmountBinding.getValue() :new Number(0)); 
        System.out.println("Amount: "+Amount+" Igst: "+Igst+" Cgst: "+Cgst+" Sgst: "+Sgst+" BankCharges: "+BankCharges);
        Number a=new Number(Igst.intValue());
        Number b=new Number(Cgst.intValue());
        Number c=new Number(Sgst.intValue());
        Number total=Amount.add(a.add(b.add(c.add(BankCharges.add(otherAmount)))));
        total=total.subtract(tdsAmount);
        System.out.println("Total Amount In Bean: "+total);
         docAmountBinding.setValue(total);
        AdfFacesContext.getCurrentInstance().addPartialTarget(docAmountBinding);
         
    }

    public void setCgstBinding(RichInputText cgstBinding) {
        this.cgstBinding = cgstBinding;
    }

    public RichInputText getCgstBinding() {
        return cgstBinding;
    }

    public void setSgstBinding(RichInputText sgstBinding) {
        this.sgstBinding = sgstBinding;
    }

    public RichInputText getSgstBinding() {
        return sgstBinding;
    }

    public void setIgstBinding(RichInputText igstBinding) {
        this.igstBinding = igstBinding;
    }

    public RichInputText getIgstBinding() {
        return igstBinding;
    }

    public void setTotalBillAmtBinding(RichInputText totalBillAmtBinding) {
        this.totalBillAmtBinding = totalBillAmtBinding;
    }

    public RichInputText getTotalBillAmtBinding() {
        return totalBillAmtBinding;
    }

    public void setAdjustedAmtBinding(RichInputText adjustedAmtBinding) {
        this.adjustedAmtBinding = adjustedAmtBinding;
    }

    public RichInputText getAdjustedAmtBinding() {
        return adjustedAmtBinding;
    }

    public void adjustedAmtValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if(object!=null){
           Number amtpy=(Number)amtToPayBinding.getValue();
           Number adjamt=(Number)object;
           
           if(amtpy!=null && adjamt.compareTo(amtpy)==1){
                   throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                 "Adjusted Amount should be less than or equal to Amount To Pay.",
                                                                 null));
               }
         
         }

    }

    public void setGlBalBindings(RichInputText glBalBindings) {
        this.glBalBindings = glBalBindings;
    }

    public RichInputText getGlBalBindings() {
        return glBalBindings;
    }

    public void setGlBalTypBinding(RichInputText glBalTypBinding) {
        this.glBalTypBinding = glBalTypBinding;
    }

    public RichInputText getGlBalTypBinding() {
        return glBalTypBinding;
    }
}
