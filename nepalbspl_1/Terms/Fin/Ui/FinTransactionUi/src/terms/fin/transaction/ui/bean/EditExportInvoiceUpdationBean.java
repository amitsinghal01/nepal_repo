package terms.fin.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectManyChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

public class EditExportInvoiceUpdationBean {
    private RichButton headerEditBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichOutputText outputTextBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText unitNameBinding;
    private RichInputText invoiceNoBinding;
    private RichInputDate invoiceDateBinding;
    private RichInputText consigneAddressBinding;
    private RichInputText buyerAddressBinding;
    private RichInputText notifyAddressBinding;
    private RichInputText carriageBinding;
    private RichInputText receiptBinding;
    private RichInputText loadingBinding;
    private RichInputText dischargeBinding;
    private RichInputText destinationBinding;
    private RichSelectManyChoice bindRemarksMulti;

    public EditExportInvoiceUpdationBean() {
    }

    public void SaveAL(ActionEvent actionEvent) {
        Integer i=0;
                i=(Integer)outputTextBinding.getValue();
                    System.out.println("EDIT TRANS VALUE"+i);
        if(i.equals(0)){
                FacesMessage Message = new FacesMessage("Record Save Successfully");
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                        fc.addMessage(null, Message);
        }else{
            FacesMessage Message = new FacesMessage("Record Updated Successfully");   
                         Message.setSeverity(FacesMessage.SEVERITY_INFO);   
            FacesContext fc = FacesContext.getCurrentInstance();   
                            fc.addMessage(null, Message);      
        }
         
    }

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
            try {
                Method method1 =
                    component.getClass().getMethod("setDisabled", boolean.class);
                if (method1 != null) {
                    method1.invoke(component, valueComponent);
                }
            } catch (NoSuchMethodException e) {
                try {
                    Method method =
                        component.getClass().getMethod("setReadOnly", boolean.class);
                    if (method != null) {
                        method.invoke(component, valueComponent);
                    }
                } catch (Exception e1) {
                    // e.printStackTrace();//silently eat this exception.
                }


            } catch (Exception e) {
                // e.printStackTrace();//silently eat this exception.
            }
            List<UIComponent> childComponents = component.getChildren();
            for (UIComponent comp : childComponents) {
                makeComponentHierarchyReadOnly(comp, valueComponent);
            }
        }
        
        private void cevmodecheck(){
             if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
                    makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
                          cevModeDisableComponent("V");
                        }
                    else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
                           makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                                cevModeDisableComponent("E");
                            }
                        }
        
    public void cevModeDisableComponent(String mode) {
        if (mode.equals("E")) {
            getHeaderEditBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
            getUnitNameBinding().setDisabled(true);
            getInvoiceNoBinding().setDisabled(true);
            getInvoiceDateBinding().setDisabled(true);
            getConsigneAddressBinding().setDisabled(true);
            getBuyerAddressBinding().setDisabled(true);
            getNotifyAddressBinding().setDisabled(true);
            getCarriageBinding().setDisabled(true);
            getReceiptBinding().setDisabled(true);
            getLoadingBinding().setDisabled(true);
            getDischargeBinding().setDisabled(true);
            getDestinationBinding().setDisabled(true);
        }  
    } 

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void EditButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setOutputTextBinding(RichOutputText outputTextBinding) {
        cevmodecheck();
        this.outputTextBinding = outputTextBinding;
    }

    public RichOutputText getOutputTextBinding() {
        return outputTextBinding;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setUnitNameBinding(RichInputText unitNameBinding) {
        this.unitNameBinding = unitNameBinding;
    }

    public RichInputText getUnitNameBinding() {
        return unitNameBinding;
    }

    public void setInvoiceNoBinding(RichInputText invoiceNoBinding) {
        this.invoiceNoBinding = invoiceNoBinding;
    }

    public RichInputText getInvoiceNoBinding() {
        return invoiceNoBinding;
    }

    public void setInvoiceDateBinding(RichInputDate invoiceDateBinding) {
        this.invoiceDateBinding = invoiceDateBinding;
    }

    public RichInputDate getInvoiceDateBinding() {
        return invoiceDateBinding;
    }

    public void setConsigneAddressBinding(RichInputText consigneAddressBinding) {
        this.consigneAddressBinding = consigneAddressBinding;
    }

    public RichInputText getConsigneAddressBinding() {
        return consigneAddressBinding;
    }

    public void setBuyerAddressBinding(RichInputText buyerAddressBinding) {
        this.buyerAddressBinding = buyerAddressBinding;
    }

    public RichInputText getBuyerAddressBinding() {
        return buyerAddressBinding;
    }

    public void setNotifyAddressBinding(RichInputText notifyAddressBinding) {
        this.notifyAddressBinding = notifyAddressBinding;
    }

    public RichInputText getNotifyAddressBinding() {
        return notifyAddressBinding;
    }

    public void setCarriageBinding(RichInputText carriageBinding) {
        this.carriageBinding = carriageBinding;
    }

    public RichInputText getCarriageBinding() {
        return carriageBinding;
    }

    public void setReceiptBinding(RichInputText receiptBinding) {
        this.receiptBinding = receiptBinding;
    }

    public RichInputText getReceiptBinding() {
        return receiptBinding;
    }

    public void setLoadingBinding(RichInputText loadingBinding) {
        this.loadingBinding = loadingBinding;
    }

    public RichInputText getLoadingBinding() {
        return loadingBinding;
    }

    public void setDischargeBinding(RichInputText dischargeBinding) {
        this.dischargeBinding = dischargeBinding;
    }

    public RichInputText getDischargeBinding() {
        return dischargeBinding;
    }

    public void setDestinationBinding(RichInputText destinationBinding) {
        this.destinationBinding = destinationBinding;
    }

    public RichInputText getDestinationBinding() {
        return destinationBinding;
    }

    public void setBindRemarksMulti(RichSelectManyChoice bindRemarksMulti) {
        this.bindRemarksMulti = bindRemarksMulti;
    }

    public RichSelectManyChoice getBindRemarksMulti() {
        return bindRemarksMulti;
    }
}
