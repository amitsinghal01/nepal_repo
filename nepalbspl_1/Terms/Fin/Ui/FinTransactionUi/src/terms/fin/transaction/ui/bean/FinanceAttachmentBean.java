package terms.fin.transaction.ui.bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.util.ResetUtils;

import oracle.binding.OperationBinding;

import org.apache.myfaces.trinidad.model.UploadedFile;

public class FinanceAttachmentBean {
    private RichTable financeAttachmentTableBinding;
    private RichInputText pathBinding;
    private RichButton popUpDialogDL;
    private String editAction = "V";
    private RichPanelHeader myPageRootComponent;
    private RichButton headerEditBinding;
    private RichInputText outputtextBinding;
    private RichInputText vouTypeBinding;
    private RichInputText vouNoBinding;
    private RichInputText vouSeriesBinding;
    private RichInputDate vouDateBinding;
    private RichInputText controlAmountBinding;
    private RichInputText controlCodeBinding;
    private RichInputText currRateBinding;
    private RichInputText currencyCodeBinding;
    private RichInputText chequeDraftNoBinding;
    private RichInputText chequesBinding;
    private RichInputText chequeBookBinding;
    private RichInputDate approvalDateBinding;
    private RichInputText finMauthAuthCodeBinding;
    private RichInputText preparedByBinding;
    private RichInputText verifiedByBinding;
    private RichInputText typeofVoucherBinding;
    private RichInputText narrationBinding;
    private RichInputText yearBinding;
    private RichInputDate chequeDraftDateBinding;
    private RichOutputText unitCodeBinding;
    private RichInputText refDocNoBinding;
    private RichInputText refDocTypeBinding;
    private RichInputText docSlNodtlBinding;
    private RichInputText unitcodedtlBinding;
    private RichInputText refDocnodtlBinding;
    private RichInputText refDocTypedtlBinding;
    private RichInputText docFileNamedtlBinding;
    private RichInputDate refDocDatedtlBinding;


    public FinanceAttachmentBean() {
    }

    public void setFinanceAttachmentTableBinding(RichTable financeAttachmentTableBinding) {
        this.financeAttachmentTableBinding = financeAttachmentTableBinding;
    }

    public RichTable getFinanceAttachmentTableBinding() {
        return financeAttachmentTableBinding;
    }

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }
    private void cevmodecheck(){
            if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
                makeComponentHierarchyReadOnly(this.getMyPageRootComponent(), true);
                cevModeDisableComponent("V");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
                makeComponentHierarchyReadOnly(this.getMyPageRootComponent(), false);
                cevModeDisableComponent("C");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
                makeComponentHierarchyReadOnly(this.getMyPageRootComponent(), false);
                cevModeDisableComponent("E");
            }
        }
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
            try {
                Method method1 =
                    component.getClass().getMethod("setDisabled", boolean.class);
                if (method1 != null) {
                    method1.invoke(component, valueComponent);
                }
            } catch (NoSuchMethodException e) {
                try {
                    Method method =
                        component.getClass().getMethod("setReadOnly", boolean.class);
                    if (method != null) {
                        method.invoke(component, valueComponent);
                    }
                } catch (Exception e1) {
                    // e.printStackTrace();//silently eat this exception.
                }


            } catch (Exception e) {
                // e.printStackTrace();//silently eat this exception.
            }
            List<UIComponent> childComponents = component.getChildren();
            for (UIComponent comp : childComponents) {
                makeComponentHierarchyReadOnly(comp, valueComponent);
            }
        }
    public void cevModeDisableComponent(String mode) {
    //            FacesContext fctx = FacesContext.getCurrentInstance();
    //            ELContext elctx = fctx.getELContext();
    //            Application jsfApp = fctx.getApplication();
    //            //create a ValueExpression that points to the ADF binding layer
    //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
    //            //
    //            ValueExpression valueExpr = exprFactory.createValueExpression(
    //                                         elctx,
    //                                         "#{pageFlowScope.mode=='E'}",
    //                                          Object.class
    //                                         );
    //             
    
            if (mode.equals("E")) {                
                //getRefDocNoBinding().setDisabled(true); 
                getVouTypeBinding().setDisabled(true);
                getVouNoBinding().setDisabled(true);
                getVouSeriesBinding().setDisabled(true);
                getVouDateBinding().setDisabled(true);
                getControlAmountBinding().setDisabled(true);
                getControlCodeBinding().setDisabled(true);
                getCurrRateBinding().setDisabled(true);
                getCurrencyCodeBinding().setDisabled(true);
                getChequeDraftDateBinding().setDisabled(true);
                getChequeDraftNoBinding().setDisabled(true);
                getChequesBinding().setDisabled(true);
                getChequeBookBinding().setDisabled(true);
                getApprovalDateBinding().setDisabled(true);
                getFinMauthAuthCodeBinding().setDisabled(true);
                getPreparedByBinding().setDisabled(true);
                getVerifiedByBinding().setDisabled(true);
                getTypeofVoucherBinding().setDisabled(true);
                getNarrationBinding().setDisabled(true);
                getYearBinding().setDisabled(true);
                getRefDocNoBinding().setDisabled(true);
                getRefDocTypeBinding().setDisabled(true);
                        getDocSlNodtlBinding().setDisabled(true);
                        getUnitcodedtlBinding().setDisabled(true);
                        getRefDocnodtlBinding().setDisabled(true);
                        getRefDocTypedtlBinding().setDisabled(true);
                        getDocFileNamedtlBinding().setDisabled(true);
                        getRefDocDatedtlBinding().setDisabled(true);
                        
                
                
            } else if (mode.equals("C")) {
                
            } else if (mode.equals("V")) {
                
            }
            
        }
    
    public void popUpDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            OperationBinding op = null;
            ADFUtils.findOperation("Delete").execute();
            op = (OperationBinding) ADFUtils.findOperation("Commit");
            if (getEditAction().equals("V")) {
                Object rst = op.execute();
            }
            System.out.println("Record Delete Successfully");
            if (op.getErrors().isEmpty()) {
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            } else if (!op.getErrors().isEmpty()) {
                OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
                Object rstr = opr.execute();
                FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(financeAttachmentTableBinding);
    }

    public void uploadAction(ValueChangeEvent vce) {
        if (vce.getNewValue() != null) {
            //Get File Object from VC Event
            UploadedFile fileVal = (UploadedFile) vce.getNewValue();
            System.out.println("File Value==============<>" + fileVal);
            //Upload File to path- Return actual server path
            String path = ADFUtils.uploadFile(fileVal);
            System.out.println(fileVal.getContentType());
            System.out.println("Content Type==>>" + fileVal.getContentType() + "Path==>>" + path + "File name==>>" +
                               fileVal.getFilename());
            //Method to insert data in table to keep track of uploaded files
            OperationBinding ob = ADFUtils.findOperation("setFileDataPI");
            ob.getParamsMap().put("name", fileVal.getFilename());
            ob.getParamsMap().put("path", path);
            ob.getParamsMap().put("contTyp", fileVal.getContentType());
            ob.getParamsMap().put("SrNo",
                                  ADFUtils.evaluateEL("#{bindings.FinanceAttachmentDetailVO1Iterator.estimatedRowCount+1}"));
            ob.execute();
            // Reset inputFile component after upload
            ResetUtils.reset(vce.getComponent());
        }


    }

    public void downloadAction(FacesContext facesContext, OutputStream outputStream) throws Exception {
        File filed = new File(pathBinding.getValue().toString());
        FileInputStream fis;
        byte[] b;
        try {
            fis = new FileInputStream(filed);

            int n;
            while ((n = fis.available()) > 0) {
                b = new byte[n];
                int result = fis.read(b);
                outputStream.write(b, 0, b.length);
                if (result == -1)
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        outputStream.flush();

    }

    public void setPathBinding(RichInputText pathBinding) {
        this.pathBinding = pathBinding;
    }

    public RichInputText getPathBinding() {
        return pathBinding;
    }


    public void setPopUpDialogDL(RichButton popUpDialogDL) {
        this.popUpDialogDL = popUpDialogDL;
    }

    public RichButton getPopUpDialogDL() {
        return popUpDialogDL;
    }

  


    public void setMyPageRootComponent(RichPanelHeader myPageRootComponent) {
        this.myPageRootComponent = myPageRootComponent;
    }

    public RichPanelHeader getMyPageRootComponent() {
        return myPageRootComponent;
    }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
        
    }

    public void setOutputtextBinding(RichInputText outputtextBinding) {
        this.outputtextBinding = outputtextBinding;
    }

    public RichInputText getOutputtextBinding() {
        cevmodecheck();
        return outputtextBinding;
    }




    public void setVouTypeBinding(RichInputText vouTypeBinding) {
        this.vouTypeBinding = vouTypeBinding;
    }

    public RichInputText getVouTypeBinding() {
        return vouTypeBinding;
    }

    public void setVouNoBinding(RichInputText vouNoBinding) {
        this.vouNoBinding = vouNoBinding;
    }

    public RichInputText getVouNoBinding() {
        return vouNoBinding;
    }

    public void setVouSeriesBinding(RichInputText vouSeriesBinding) {
        this.vouSeriesBinding = vouSeriesBinding;
    }

    public RichInputText getVouSeriesBinding() {
        return vouSeriesBinding;
    }

    public void setVouDateBinding(RichInputDate vouDateBinding) {
        this.vouDateBinding = vouDateBinding;
    }

    public RichInputDate getVouDateBinding() {
        return vouDateBinding;
    }

    public void setControlAmountBinding(RichInputText controlAmountBinding) {
        this.controlAmountBinding = controlAmountBinding;
    }

    public RichInputText getControlAmountBinding() {
        return controlAmountBinding;
    }

    public void setControlCodeBinding(RichInputText controlCodeBinding) {
        this.controlCodeBinding = controlCodeBinding;
    }

    public RichInputText getControlCodeBinding() {
        return controlCodeBinding;
    }

    public void setCurrRateBinding(RichInputText currRateBinding) {
        this.currRateBinding = currRateBinding;
    }

    public RichInputText getCurrRateBinding() {
        return currRateBinding;
    }

    public void setCurrencyCodeBinding(RichInputText currencyCodeBinding) {
        this.currencyCodeBinding = currencyCodeBinding;
    }

    public RichInputText getCurrencyCodeBinding() {
        return currencyCodeBinding;
    }

    public void setChequeDraftNoBinding(RichInputText chequeDraftNoBinding) {
        this.chequeDraftNoBinding = chequeDraftNoBinding;
    }

    public RichInputText getChequeDraftNoBinding() {
        return chequeDraftNoBinding;
    }

    public void setChequesBinding(RichInputText chequesBinding) {
        this.chequesBinding = chequesBinding;
    }

    public RichInputText getChequesBinding() {
        return chequesBinding;
    }

    public void setChequeBookBinding(RichInputText chequeBookBinding) {
        this.chequeBookBinding = chequeBookBinding;
    }

    public RichInputText getChequeBookBinding() {
        return chequeBookBinding;
    }

    public void setApprovalDateBinding(RichInputDate approvalDateBinding) {
        this.approvalDateBinding = approvalDateBinding;
    }

    public RichInputDate getApprovalDateBinding() {
        return approvalDateBinding;
    }

    public void setFinMauthAuthCodeBinding(RichInputText finMauthAuthCodeBinding) {
        this.finMauthAuthCodeBinding = finMauthAuthCodeBinding;
    }

    public RichInputText getFinMauthAuthCodeBinding() {
        return finMauthAuthCodeBinding;
    }

    public void setPreparedByBinding(RichInputText preparedByBinding) {
        this.preparedByBinding = preparedByBinding;
    }

    public RichInputText getPreparedByBinding() {
        return preparedByBinding;
    }

    public void setVerifiedByBinding(RichInputText verifiedByBinding) {
        this.verifiedByBinding = verifiedByBinding;
    }

    public RichInputText getVerifiedByBinding() {
        return verifiedByBinding;
    }

    public void setTypeofVoucherBinding(RichInputText typeofVoucherBinding) {
        this.typeofVoucherBinding = typeofVoucherBinding;
    }

    public RichInputText getTypeofVoucherBinding() {
        return typeofVoucherBinding;
    }

    public void setNarrationBinding(RichInputText narrationBinding) {
        this.narrationBinding = narrationBinding;
    }

    public RichInputText getNarrationBinding() {
        return narrationBinding;
    }

    public void setYearBinding(RichInputText yearBinding) {
        this.yearBinding = yearBinding;
    }

    public RichInputText getYearBinding() {
        return yearBinding;
    }

    public void setChequeDraftDateBinding(RichInputDate chequeDraftDateBinding) {
        this.chequeDraftDateBinding = chequeDraftDateBinding;
    }

    public RichInputDate getChequeDraftDateBinding() {
        return chequeDraftDateBinding;
    }

    public void setUnitCodeBinding(RichOutputText unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichOutputText getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setRefDocNoBinding(RichInputText refDocNoBinding) {
        this.refDocNoBinding = refDocNoBinding;
    }

    public RichInputText getRefDocNoBinding() {
        return refDocNoBinding;
    }

    public void setRefDocTypeBinding(RichInputText refDocTypeBinding) {
        this.refDocTypeBinding = refDocTypeBinding;
    }

    public RichInputText getRefDocTypeBinding() {
        return refDocTypeBinding;
    }

    public void setDocSlNodtlBinding(RichInputText docSlNodtlBinding) {
        this.docSlNodtlBinding = docSlNodtlBinding;
    }

    public RichInputText getDocSlNodtlBinding() {
        return docSlNodtlBinding;
    }

    public void setUnitcodedtlBinding(RichInputText unitcodedtlBinding) {
        this.unitcodedtlBinding = unitcodedtlBinding;
    }

    public RichInputText getUnitcodedtlBinding() {
        return unitcodedtlBinding;
    }

    public void setRefDocnodtlBinding(RichInputText refDocnodtlBinding) {
        this.refDocnodtlBinding = refDocnodtlBinding;
    }

    public RichInputText getRefDocnodtlBinding() {
        return refDocnodtlBinding;
    }

    public void setRefDocTypedtlBinding(RichInputText refDocTypedtlBinding) {
        this.refDocTypedtlBinding = refDocTypedtlBinding;
    }

    public RichInputText getRefDocTypedtlBinding() {
        return refDocTypedtlBinding;
    }

    public void setDocFileNamedtlBinding(RichInputText docFileNamedtlBinding) {
        this.docFileNamedtlBinding = docFileNamedtlBinding;
    }

    public RichInputText getDocFileNamedtlBinding() {
        return docFileNamedtlBinding;
    }

    public void setRefDocDatedtlBinding(RichInputDate refDocDatedtlBinding) {
        this.refDocDatedtlBinding = refDocDatedtlBinding;
    }

    public RichInputDate getRefDocDatedtlBinding() {
        return refDocDatedtlBinding;
    }

    public void docAttachPopupDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                    {
                    ADFUtils.findOperation("Delete").execute();
                    ADFUtils.showMessage("Record Deleted Successfully", 2);
                    }
                   AdfFacesContext.getCurrentInstance().addPartialTarget(financeAttachmentTableBinding); 
    }
}
