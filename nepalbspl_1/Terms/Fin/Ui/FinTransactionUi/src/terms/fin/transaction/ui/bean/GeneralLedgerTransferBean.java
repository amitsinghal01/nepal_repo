package terms.fin.transaction.ui.bean;

import javax.faces.event.ActionEvent;

public class GeneralLedgerTransferBean {
    public GeneralLedgerTransferBean() {
    }

    public void RunAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("generalLedgerTransfer").execute();
    }
}
