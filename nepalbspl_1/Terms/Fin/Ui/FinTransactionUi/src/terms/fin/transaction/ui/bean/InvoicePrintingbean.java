package terms.fin.transaction.ui.bean;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;

import java.math.BigDecimal;

import java.sql.Connection;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import javax.naming.Context;
import javax.naming.InitialContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.sql.DataSource;


import net.sf.jasperreports.engine.JasperExportManager;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

import oracle.adf.model.BindingContext;
import oracle.adf.model.OperationBinding;
import oracle.adf.model.binding.DCIteratorBinding;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.nav.RichCommandButton;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.BindingContainer;

import oracle.jbo.JboException;
import oracle.jbo.Row;
import oracle.jbo.server.ViewObjectImpl;


public class InvoicePrintingbean {
   
   ActionEvent ac;
    private RichPanelHeader pageRootBinding;  
    private String mode = "A";

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getMode() {
        return mode;
    }

    public InvoicePrintingbean() {
    }

    public BindingContainer getBindings() {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
    }
    public void downloadJrxml(FacesContext facesContext, OutputStream outputStream) {
        Connection conn=null;
        try {
            String file_name = "tax_invoice.jasper";
            //*************Find File name***************//
            oracle.binding.OperationBinding binding = ADFUtils.findOperation("fileNameForPrint");
            binding.getParamsMap().put("fileId", "ERP0000001935");
            binding.execute();
            
            
            //*************End Find File name***********//
            System.out.println("Binding Result :" + binding.getResult());
            if (binding.getResult() != null) {
                DCIteratorBinding pvIter =(DCIteratorBinding) getBindings().get("InvoicePrintVO1Iterator");
                String result = binding.getResult().toString();
                int last_index = result.lastIndexOf("/");
                if(last_index==-1)
                {
                    last_index=result.lastIndexOf("\\");
                }
                String path = result.substring(0, last_index + 1) + file_name;
                System.out.println("FILE PATH IS===>" + path);
                InputStream input = new FileInputStream(path);
                System.out.println("UnitCode: "+pvIter.getCurrentRow().getAttribute("UnitCode")+pvIter.getCurrentRow().getAttribute("InvParam")+pvIter.getCurrentRow().getAttribute("InvoiceNo"));
                //                 String Cust =(String)pvIter.getCurrentRow().getAttribute("CustCode");
                Map n = new HashMap();
                    n.put("p_inv_no", pvIter.getCurrentRow().getAttribute("InvoiceNo"));
                                    n.put("p_unit", pvIter.getCurrentRow().getAttribute("UnitCode"));
                                    n.put("p_tp", pvIter.getCurrentRow().getAttribute("InvParam"));   

                ADFUtils.findOperation("Commit").execute();
                cmdBtnAL(ac);

                
                conn = getConnection();                
                JasperReport design = (JasperReport) JRLoader.loadObject(input);
                System.out.println("Path : " + input + " -------" + design+" param:"+n);                
                @SuppressWarnings("unchecked")
                net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                byte pdf[] = JasperExportManager.exportReportToPdf(print);
                HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                response.getOutputStream().write(pdf);
                response.getOutputStream().flush();
                response.getOutputStream().close();
                facesContext.responseComplete();
                }else
                System.out.println("File Name/File Path not found.");
                } catch (FileNotFoundException fnfe) {
                // TODO: Add catch code
                fnfe.printStackTrace();
                try {
                    System.out.println("in finally connection closed-");
                            conn.close();
                            conn = null;
                    
                } catch(SQLException e) {
                    e.printStackTrace();
                }
                } catch (Exception e) {
                // TODO: Add catch code
                e.printStackTrace();
                try {
                    System.out.println("in finally connection closed--");
                            conn.close();
                            conn = null;
                    
                } catch(SQLException e1) {
                    e1.printStackTrace();
                }
                }finally {
                        try {
                                System.out.println("in finally connection closed---");
                            AdfFacesContext.getCurrentInstance().addPartialTarget(pageRootBinding);
                                        conn.close();
                                        conn = null;
                                
                        } catch(SQLException e) {
                                e.printStackTrace();
                        }
                }

    } 

    public Connection getConnection() throws Exception {
        Context ctx = null;
        Connection conn = null;
        ctx = new InitialContext();
        DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/ApplicationDBDS");
        conn = ds.getConnection();
        return conn;
    }

     public void callButton() {
          FacesContext facesContext = FacesContext.getCurrentInstance();
          if (facesContext != null) {
              UIComponent root = facesContext.getViewRoot();
             UIComponent component = findComponent(root, "b2"); // button_id will be id of invisible button
              ADFUtils.findOperation("Commit").execute();
          
          RichCommandButton button = (RichCommandButton) component;
          ActionEvent actionEvent = new ActionEvent(button);
          actionEvent.queue();
          }

      }

      public static UIComponent findComponent(UIComponent base, String id) {
          if (id.equals(base.getId()))
              return base;
          UIComponent children = null;
          UIComponent result = null;
          Iterator childrens = base.getFacetsAndChildren();
          while (childrens.hasNext() && (result == null)) {
              children = (UIComponent) childrens.next();
              if (id.equals(children.getId())) {
                  result = children;
                  break;
              }
              result = findComponent(children, id);
              if (result != null) {
                  break;
              }
          }
          return result;
      }

      public String methodForComit() {
          callButton();
          return null;
      }

    public void cmdBtnAL(ActionEvent actionEvent) {
        // Add event code here...
        System.out.println("In Create Insert");
        ADFUtils.findOperation("CreateInsert").execute();

        mode="B";
        AdfFacesContext.getCurrentInstance().addPartialTarget(pageRootBinding);
    }

    public void setPageRootBinding(RichPanelHeader pageRootBinding) {
        this.pageRootBinding = pageRootBinding;
    }

    public RichPanelHeader getPageRootBinding() {
        return pageRootBinding;
    }

    public void invoiceVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce.getNewValue() != null)
        {
            System.out.println("Select New Invoice----VCL");
         mode="A";
            AdfFacesContext.getCurrentInstance().addPartialTarget(pageRootBinding);
        }
    }

}
