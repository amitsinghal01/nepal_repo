package terms.fin.transaction.ui.bean;

import java.math.BigDecimal;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class MemorendumVoucherBean {
    private RichInputDate selectDateBinding;
    private RichInputDate memoDateBinding;
    private RichInputText totalDiffTransBinding;
    private RichTable memoTableBinding;
    private RichTable showMemoBinding;

    public MemorendumVoucherBean() {
    }

    public void setSelectDateBinding(RichInputDate selectDateBinding) {
        this.selectDateBinding = selectDateBinding;
    }

    public RichInputDate getSelectDateBinding() {
        return selectDateBinding;
    }

    public void createButtonAL(ActionEvent actionEvent) 
    {

            java.util.Date date = (java.util.Date) selectDateBinding.getValue();
            SimpleDateFormat yyyyMMdd = new SimpleDateFormat("yyyy-MM-dd");
            String final_format = yyyyMMdd.format(date);
            oracle.jbo.domain.Date job_date = new oracle.jbo.domain.Date(final_format);
            if(date!=null)
            {
             ADFUtils.findOperation("CreateInsert").execute();
             memoDateBinding.setValue(job_date);
            }
            else
            {
            ADFUtils.showMessage("Please select Date.", 0);
            }
    }

    public void setMemoDateBinding(RichInputDate memoDateBinding) {
        this.memoDateBinding = memoDateBinding;
    }

    public RichInputDate getMemoDateBinding() {
        return memoDateBinding;
    }

    public void flagVCE(ValueChangeEvent valueChangeEvent) 
    {
    valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
    AdfFacesContext.getCurrentInstance().addPartialTarget(totalDiffTransBinding); 

    }

    public void setTotalDiffTransBinding(RichInputText totalDiffTransBinding) {
        this.totalDiffTransBinding = totalDiffTransBinding;
    }

    public RichInputText getTotalDiffTransBinding() {
        return totalDiffTransBinding;
    }
    
    public void saveButtonAL(ActionEvent actionEvent) 
    {
        BigDecimal total_diff = (BigDecimal)totalDiffTransBinding.getValue();
        System.out.println("---total_diff----:="+total_diff);
        if(( total_diff.compareTo(new BigDecimal(0)) ==0 || total_diff.compareTo(new BigDecimal(-0)) ==0))
        {
        ADFUtils.findOperation("Commit").execute();
        ADFUtils.findOperation("refreshMemorendumVoucher").execute();
        AdfFacesContext.getCurrentInstance().addPartialTarget(memoTableBinding);
        AdfFacesContext.getCurrentInstance().addPartialTarget(showMemoBinding);
        AdfFacesContext.getCurrentInstance().addPartialTarget(selectDateBinding);
        }
        else
        {
        ADFUtils.showMessage("Debit-Credit Diffrence Amount Must Be Zero", 0);
        }
        
    }

    public void setMemoTableBinding(RichTable memoTableBinding) {
        this.memoTableBinding = memoTableBinding;
    }

    public RichTable getMemoTableBinding() {
        return memoTableBinding;
    }

    public void setShowMemoBinding(RichTable showMemoBinding) {
        this.showMemoBinding = showMemoBinding;
    }

    public RichTable getShowMemoBinding() {
        return showMemoBinding;
    }

    public void deleteDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                    {
                    ADFUtils.findOperation("Delete").execute();
                    System.out.println("Record Delete Successfully");
                    FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                    FacesContext fc = FacesContext.getCurrentInstance();  
                    fc.addMessage(null, Message);  
                    }
                    AdfFacesContext.getCurrentInstance().addPartialTarget(memoTableBinding);     }

    public void showDeleteDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok")){
               oracle.adf.model.OperationBinding op = null;
                   ADFUtils.findOperation("Delete1").execute();
                   op = (oracle.adf.model.OperationBinding) ADFUtils.findOperation("Commit");
                   op.execute();
          
                           System.out.println("Record Delete Successfully");
           if(op.getErrors().isEmpty()){
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                   Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);
           } else if (!op.getErrors().isEmpty()){
                oracle.adf.model.OperationBinding opr = (oracle.adf.model.OperationBinding) ADFUtils.findOperation("Rollback");
                     Object rstr = opr.execute();
                   FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                   Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                   FacesContext fc = FacesContext.getCurrentInstance();
                        fc.addMessage(null, Message);
             }
        }
                AdfFacesContext.getCurrentInstance().addPartialTarget(showMemoBinding); 
   }
}
