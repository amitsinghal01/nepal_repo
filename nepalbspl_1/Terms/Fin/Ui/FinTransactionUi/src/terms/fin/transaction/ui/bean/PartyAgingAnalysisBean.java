package terms.fin.transaction.ui.bean;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;

import java.sql.Connection;

import java.sql.SQLException;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.faces.context.FacesContext;

import javax.naming.Context;
import javax.naming.InitialContext;

import javax.servlet.http.HttpServletResponse;

import javax.sql.DataSource;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import oracle.adf.model.BindingContext;
import oracle.adf.model.OperationBinding;
import oracle.adf.model.binding.DCIteratorBinding;

import oracle.binding.BindingContainer;

import oracle.jbo.domain.Date;

public class PartyAgingAnalysisBean {
    public PartyAgingAnalysisBean() {
    }

    public BindingContainer getBindings()
         {
           return BindingContext.getCurrent().getCurrentBindingsEntry();
         }
       
       public void downloadJrxml(FacesContext facesContext, OutputStream outputStream) {
           Connection conn=null;
           try {
               OperationBinding binding = (OperationBinding) ADFUtils.findOperation("fileNameForPrint");
               binding.getParamsMap().put("fileId","ERP0000001900");
               binding.execute();
               System.out.println("Binding Result "+binding.getResult());
               if(binding.getResult() != null){
                   InputStream input = new FileInputStream(binding.getResult().toString());                   
                   DCIteratorBinding pvIter = (DCIteratorBinding) getBindings().get("PartyAgingAnalysisVVO1Iterator");
                  
                 String LV_SID= oracle.jbo.server.uniqueid.UniqueIdHelper.getNextId().toString();
                   System.out.println("SID======>"+LV_SID);
                   String LV_UNIT = pvIter.getCurrentRow().getAttribute("UnitCode").toString();
                   oracle.jbo.domain.Date FRDATE = (Date) pvIter.getCurrentRow().getAttribute("DateFrom");
                   oracle.jbo.domain.Date TODATE=(oracle.jbo.domain.Date) pvIter.getCurrentRow().getAttribute("DateTo");
                   String LV_BANK_CODE = (String) pvIter.getCurrentRow().getAttribute("BankCode");
                   System.out.println("LV_UNIT"+LV_UNIT +"FRDATE"+FRDATE+"TODATE"+TODATE+"LV_BANK_CODE"+LV_BANK_CODE+"LV_SID"+LV_SID);
                   OperationBinding binding1 = (OperationBinding) ADFUtils.findOperation("BankRecoSummaryProcedureCalling");
                   binding1.getParamsMap().put("FRDATE",FRDATE);
                   binding1.getParamsMap().put("TODATE",TODATE);
                   binding1.getParamsMap().put("LV_BANK_CODE",LV_BANK_CODE);
                   binding1.getParamsMap().put("LV_UNIT",LV_UNIT);
                   binding1.getParamsMap().put("LV_SID",LV_SID);
                   binding1.execute();
                   Map n = new HashMap();
                   n.put("p_unit", LV_UNIT);
                   n.put("p_fdt", FRDATE);
                    n.put("p_sid",LV_SID );
                   n.put("p_td", TODATE);
                   n.put("p_gl", LV_BANK_CODE);
                   conn = getConnection( );
                   
                   JasperReport design = (JasperReport) JRLoader.loadObject(input);
                   System.out.println("Path : " + input + " -------" + design+" param:"+n);
                   
                   @SuppressWarnings("unchecked")
                   net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                   ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                   JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                   byte pdf[] = JasperExportManager.exportReportToPdf(print);
                   HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                   response.getOutputStream().write(pdf);
                   response.getOutputStream().flush();
                   response.getOutputStream().close();
                   facesContext.responseComplete();
                   }else
                   System.out.println("File Name/File Path not found.");
                   } catch (FileNotFoundException fnfe) {
                   // TODO: Add catch code
                   fnfe.printStackTrace();
                   try {
                       System.out.println("in finally connection closed");
                               conn.close( );
                               conn = null;
                       
                   } catch( SQLException e ) {
                       e.printStackTrace( );
                   }
                   } catch (Exception e) {
                   // TODO: Add catch code
                   e.printStackTrace();
                   try {
                       System.out.println("in finally connection closed");
                               conn.close( );
                               conn = null;
                       
                   } catch( SQLException e1 ) {
                       e1.printStackTrace( );
                   }
                   }finally {
                           try {
                                   System.out.println("in finally connection closed");
                                           conn.close( );
                                           conn = null;
                                   
                           } catch( SQLException e ) {
                                   e.printStackTrace( );
                           }
                   }
       }
    
    public Connection getConnection() throws Exception{
            Context ctx = null;
            Connection conn = null;
            ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/ApplicationDBDS");
            conn = ds.getConnection();
            return conn;
        }
}
