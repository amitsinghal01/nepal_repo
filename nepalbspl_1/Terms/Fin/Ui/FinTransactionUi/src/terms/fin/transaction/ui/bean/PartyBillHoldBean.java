package terms.fin.transaction.ui.bean;

import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.output.RichPanelCollection;

public class PartyBillHoldBean {
    private RichPanelCollection tableBinding;
    private String editAction = "V";

    public PartyBillHoldBean() {
    }

    public void populateButtonAL(ActionEvent actionEvent) {
        
        System.out.println("In teh AL *****");
        ADFUtils.findOperation("partyBillHoldPopulateData").execute();
        System.out.println("After the AL ************");
    }

    public void setTableBinding(RichPanelCollection tableBinding) {
        this.tableBinding = tableBinding;
    }

    public RichPanelCollection getTableBinding() {
        return tableBinding;
    }
    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }
    public void isEnableEdit(ActionEvent actionEvent) {
        // Add event code here...
        this.editAction = getEditAction();
    }
}
