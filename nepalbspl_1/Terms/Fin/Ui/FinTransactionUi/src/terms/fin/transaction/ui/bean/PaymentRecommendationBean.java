package terms.fin.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectBooleanCheckbox;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;

import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class PaymentRecommendationBean {
    private RichPanelHeader getMyPageRootComponent;
    private RichOutputText bindingOutputText;
    private RichButton headerEditBinding;
    private RichTable detailTableBinding;
    private RichInputText docmentNoBinding;
    private RichInputDate docDateBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputComboboxListOfValues vendorCodeBinding;
    private RichInputComboboxListOfValues preparedByBinding;
    private RichSelectOneChoice poTypeBinding;
    private RichInputComboboxListOfValues approvedByBinding;
    private RichInputText mrnNoBinding;
    private RichInputDate mrnDateBinding;
    private RichInputText billNoBinding;
    private RichInputDate billDateBinding;
    private RichInputText billAmountBinding;
    private RichInputText passedAmountBinding;
    private RichInputText deductionBinding;
    private RichInputDate dueDateBinding;
    private RichInputText remarkBinding;
    private RichSelectBooleanCheckbox checkboxBinding;
    private RichInputDate approvalDateBinding;

    public PaymentRecommendationBean() {
    }

    public void saveAL(ActionEvent actionEvent) 
    {
        if ((Long) ADFUtils.evaluateEL("#{bindings.PaymentRecommendDetailVO1Iterator.estimatedRowCount}")>0) 
        {
            OperationBinding op1 = ADFUtils.findOperation("checkPaymentRecommendationStatus");
            op1.execute();
            System.out.println("Status is------===:>> "+op1.getResult().toString());
            if(op1.getResult()!=null && op1.getResult().toString().equalsIgnoreCase("P"))
            {
                System.out.println("inside the P ");
                OperationBinding op = ADFUtils.findOperation("generatePaymentRecomDocNo");
                op.execute();
                System.out.println("Result is===> "+op.getResult());
                if(op.getResult() != null && op.getResult().toString().equalsIgnoreCase("E"))
                {
                    ADFUtils.findOperation("Commit").execute();
                    ADFUtils.showMessage(" Record Updated Successfully", 2);
                    
                }
                else if (op.getResult() != null && op.getResult().toString() != ""  && op.getResult().equals("S"))
                {
                    System.out.println("+++++ in the save mode+++++++++++");
                    ADFUtils.findOperation("Commit").execute();
                    ADFUtils.showMessage("Record Saved Successfully.Payment Recommendation Document No.is "+ADFUtils.evaluateEL("#{bindings.DocNo.inputValue}"), 2);
                }
                else if(op.getResult() != null && op.getResult().toString().equalsIgnoreCase("N"))
                {
                    ADFUtils.showMessage("Payment Recommendation Document No. could not be generated. Try Again !!", 0);
                }
            }
            else{
                ADFUtils.showMessage("Please select any row through Check Box.", 0);
            }
        }
        else{
            ADFUtils.showMessage("Enter the data in detail table", 0);
        }
    }

    public void populatePaymentRecomAL(ActionEvent actionEvent) 
    { 
        if(vendorCodeBinding.getValue()!=null && poTypeBinding.getValue()!=null)
        {
                OperationBinding Pop = ADFUtils.findOperation("populatePaymentRecommendation");
                Object PopObj = Pop.execute();
    
                if(Pop.getResult() != null && Pop.getResult().toString().equalsIgnoreCase("N"))
                {
                    ADFUtils.showMessage("No Data Found For this Vendor Code.", 0);
                }
                else if(Pop.getResult() != null && Pop.getResult().toString().equalsIgnoreCase("ERROR"))
                {
                    ADFUtils.showMessage("Error occurs while data populate.", 0);
                }
                else if(Pop.getResult() != null && Pop.getResult().toString().equalsIgnoreCase("Y"))
                {
                    getPoTypeBinding().setDisabled(true);
                    getVendorCodeBinding().setDisabled(true);
                    System.out.println("------Successfully data populat------");
                }
        }
        else{
            ADFUtils.showMessage("PO Type and Vendor Code must be required.", 0);
        }

    }

    public void editButtonAL(ActionEvent actionEvent) {
        if(approvedByBinding.getValue()!=null)
        {
            ADFUtils.showMessage("Approved Record can not be modified.", 0);
            ADFUtils.setEL("#{pageFlowScope.mode}","V");
        }else{
            CheckAllCheckBoxinPaymentRecommendation();
            cevmodecheck();
            
        }
    }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }

    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }

    public void cevModeDisableComponent(String mode) {

        if (mode.equals("C")) {
            getDocmentNoBinding().setDisabled(true);
            getDocDateBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
            getPreparedByBinding().setDisabled(true);
            getApprovedByBinding().setDisabled(true);
            getApprovalDateBinding().setDisabled(true);
            
            getMrnNoBinding().setDisabled(true);
            getMrnDateBinding().setDisabled(true);
            getBillNoBinding().setDisabled(true);
            getBillDateBinding().setDisabled(true);
            getBillAmountBinding().setDisabled(true);
            getDeductionBinding().setDisabled(true);
            getPassedAmountBinding().setDisabled(true);
            getDueDateBinding().setDisabled(true);

        }
        if (mode.equals("V")) {


        }
        if (mode.equals("E")) {
           getDocmentNoBinding().setDisabled(true);
           getDocDateBinding().setDisabled(true);
           getPoTypeBinding().setDisabled(true);
           getVendorCodeBinding().setDisabled(true);
           getUnitCodeBinding().setDisabled(true);
           getPreparedByBinding().setDisabled(true);
           
           getMrnNoBinding().setDisabled(true);
           getMrnDateBinding().setDisabled(true);
           getBillNoBinding().setDisabled(true);
           getBillDateBinding().setDisabled(true);
           getBillAmountBinding().setDisabled(true);
           getDeductionBinding().setDisabled(true);
           getPassedAmountBinding().setDisabled(true);
           getDueDateBinding().setDisabled(true);
           getCheckboxBinding().setDisabled(true);
           getApprovalDateBinding().setDisabled(true);
        }
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void deleteDetailDialogDL(DialogEvent dialogEvent) {
    if(dialogEvent.getOutcome().name().equals("ok"))
    {
        ADFUtils.findOperation("Delete").execute();
        FacesMessage Message = new FacesMessage("Record Deleted Successfully");
        Message.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
    
    }
    AdfFacesContext.getCurrentInstance().addPartialTarget(detailTableBinding);   
    }

    public String saveAndCloseAL() 
    {
        if ((Long) ADFUtils.evaluateEL("#{bindings.PaymentRecommendDetailVO1Iterator.estimatedRowCount}")>0) 
        {
            OperationBinding op = ADFUtils.findOperation("generatePaymentRecomDocNo");
            op.execute();
            System.out.println("Result is===> "+op.getResult());
            if(op.getResult() != null && op.getResult().toString().equalsIgnoreCase("E"))
            {
                ADFUtils.findOperation("Commit").execute();
                ADFUtils.showMessage(" Record Updated Successfully", 2);
                return "saveAndClose";
            }
            else if (op.getResult() != null && op.getResult().toString() != ""  && op.getResult().equals("S"))
            {
                System.out.println("+++++ in the save mode+++++++++++");
                ADFUtils.findOperation("Commit").execute();
                ADFUtils.showMessage("Record Saved Successfully.Payment Recommendation Document No.is "+ADFUtils.evaluateEL("#{bindings.DocNo.inputValue}"), 2);
                return "saveAndClose";
            }
            else if(op.getResult() != null && op.getResult().toString().equalsIgnoreCase("N"))
            {
                ADFUtils.showMessage("Payment Recommendation Document No. could not be generated. Try Again !!", 0);
                return null;
            }
        }else{
            ADFUtils.showMessage("Enter the data in detail table", 0);
            return null;
        }
        return null;
    }

    public void setDetailTableBinding(RichTable detailTableBinding) {
        this.detailTableBinding = detailTableBinding;
    }

    public RichTable getDetailTableBinding() {
        return detailTableBinding;
    }

    public void setDocmentNoBinding(RichInputText docmentNoBinding) {
        this.docmentNoBinding = docmentNoBinding;
    }

    public RichInputText getDocmentNoBinding() {
        return docmentNoBinding;
    }

    public void setDocDateBinding(RichInputDate docDateBinding) {
        this.docDateBinding = docDateBinding;
    }

    public RichInputDate getDocDateBinding() {
        return docDateBinding;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setVendorCodeBinding(RichInputComboboxListOfValues vendorCodeBinding) {
        this.vendorCodeBinding = vendorCodeBinding;
    }

    public RichInputComboboxListOfValues getVendorCodeBinding() {
        return vendorCodeBinding;
    }

    public void setPreparedByBinding(RichInputComboboxListOfValues preparedByBinding) {
        this.preparedByBinding = preparedByBinding;
    }

    public RichInputComboboxListOfValues getPreparedByBinding() {
        return preparedByBinding;
    }

    public void setPoTypeBinding(RichSelectOneChoice poTypeBinding) {
        this.poTypeBinding = poTypeBinding;
    }

    public RichSelectOneChoice getPoTypeBinding() {
        return poTypeBinding;
    }

    public void setApprovedByBinding(RichInputComboboxListOfValues approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputComboboxListOfValues getApprovedByBinding() {
        return approvedByBinding;
    }

    public void setMrnNoBinding(RichInputText mrnNoBinding) {
        this.mrnNoBinding = mrnNoBinding;
    }

    public RichInputText getMrnNoBinding() {
        return mrnNoBinding;
    }

    public void setMrnDateBinding(RichInputDate mrnDateBinding) {
        this.mrnDateBinding = mrnDateBinding;
    }

    public RichInputDate getMrnDateBinding() {
        return mrnDateBinding;
    }

    public void setBillNoBinding(RichInputText billNoBinding) {
        this.billNoBinding = billNoBinding;
    }

    public RichInputText getBillNoBinding() {
        return billNoBinding;
    }

    public void setBillDateBinding(RichInputDate billDateBinding) {
        this.billDateBinding = billDateBinding;
    }

    public RichInputDate getBillDateBinding() {
        return billDateBinding;
    }

    public void setBillAmountBinding(RichInputText billAmountBinding) {
        this.billAmountBinding = billAmountBinding;
    }

    public RichInputText getBillAmountBinding() {
        return billAmountBinding;
    }

    public void setPassedAmountBinding(RichInputText passedAmountBinding) {
        this.passedAmountBinding = passedAmountBinding;
    }

    public RichInputText getPassedAmountBinding() {
        return passedAmountBinding;
    }

    public void setDeductionBinding(RichInputText deductionBinding) {
        this.deductionBinding = deductionBinding;
    }

    public RichInputText getDeductionBinding() {
        return deductionBinding;
    }

    public void setDueDateBinding(RichInputDate dueDateBinding) {
        this.dueDateBinding = dueDateBinding;
    }

    public RichInputDate getDueDateBinding() {
        return dueDateBinding;
    }

    public void setRemarkBinding(RichInputText remarkBinding) {
        this.remarkBinding = remarkBinding;
    }

    public RichInputText getRemarkBinding() {
        return remarkBinding;
    }
    public void CheckAllCheckBoxinPaymentRecommendation() {
            OperationBinding op=(OperationBinding)ADFUtils.findOperation("CheckAllCheckbox");
            op.execute();
        }

    public void setCheckboxBinding(RichSelectBooleanCheckbox checkboxBinding) {
        this.checkboxBinding = checkboxBinding;
    }

    public RichSelectBooleanCheckbox getCheckboxBinding() {
        return checkboxBinding;
    }

    public void setApprovalDateBinding(RichInputDate approvalDateBinding) {
        this.approvalDateBinding = approvalDateBinding;
    }

    public RichInputDate getApprovalDateBinding() {
        return approvalDateBinding;
    }
}
