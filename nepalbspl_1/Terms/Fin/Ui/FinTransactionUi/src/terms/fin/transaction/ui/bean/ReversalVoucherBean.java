package terms.fin.transaction.ui.bean;

import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichSelectBooleanCheckbox;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;

import java.sql.Connection;

import java.sql.SQLException;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import javax.faces.context.FacesContext;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.binding.BindingContainer;

public class ReversalVoucherBean {
    private RichPopup popupBinding;
    private RichInputDate reverseApprovalDateBinding;
    private RichSelectBooleanCheckbox checkApproveBinding;
    private RichOutputText vouTypeBinding;
    private RichOutputText vouSeriesBinding;
    private RichOutputText amountBinding;
    private RichOutputText unitCodeBinding;

    public ReversalVoucherBean() {
    }

    public void resetActionAL(ActionEvent actionEvent) {
        OperationBinding op=(OperationBinding)ADFUtils.findOperation("unselectAllReversalVoucherApproval");
        op.execute();
    }
    
    public void reverseVoucherDialogDL(DialogEvent dialogEvent) 
    {
        OperationBinding op1=(OperationBinding)ADFUtils.findOperation("reversalVoucherApproval");
        op1.getParamsMap().put("userName", ADFUtils.resolveExpression("#{pageFlowScope.userName}"));
        op1.execute();

        if(op1.getResult()!=null && op1.getResult().equals("S"))
        {
            ADFUtils.findOperation("Commit").execute();
            ADFUtils.showMessage("Reversal Voucher is generated Successfully. Please Check Reversal Voucher History.", 2);
        }
        else if(op1.getResult()!=null && op1.getResult().equals("RB"))
        {
            ADFUtils.showMessage("Reversal Voucher is not generated.", 0);
        }
        else if(op1.getResult()!=null && op1.getResult().equals("E"))
        {
            ADFUtils.showMessage("Reversal Voucher Function Error.", 0);
        }
        else if(op1.getResult()!=null && op1.getResult().equals("PP"))
        {
            popupBinding.hide();
      //      ADFUtils.showMessage("At a time,Select only one voucher for reversal.", 0);
            ADFUtils.showMessage("Please Select Voucher for Reversal.", 1);
        }
        
    }

    public void setPopupBinding(RichPopup popupBinding) {
        this.popupBinding = popupBinding;
    }

    public RichPopup getPopupBinding() {
        return popupBinding;
    }
    //***************************************REPORT CODE*********************************
    
    public BindingContainer getBindings()
      {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
      }
    
    public void downloadJrxml(FacesContext facesContext, OutputStream outputStream) {
        Connection conn=null;
        try {
            OperationBinding binding = ADFUtils.findOperation("fileNameForPrint");
            binding.getParamsMap().put("fileId","ERP0000001865");
            binding.execute();
            String a="A";
            System.out.println("Binding Result :"+binding.getResult());
            if(binding.getResult() != null){
                InputStream input = new FileInputStream(binding.getResult().toString());
               // InputStream input = new FileInputStream("/home/beta15/Jasper/r_jour_prn.jrxml");                
                DCIteratorBinding rvIter = (DCIteratorBinding) getBindings().get("SearchReversalVoucherHeaderVO1Iterator");
                String vouNo = (String)rvIter.getCurrentRow().getAttribute("VouNo");
                String unitCode = (String)rvIter.getCurrentRow().getAttribute("UnitCode");
                String vouTp=(String)rvIter.getCurrentRow().getAttribute("VouType");
                oracle.jbo.domain.Date vouDate =(oracle.jbo.domain.Date)rvIter.getCurrentRow().getAttribute("VouDate");
                
                System.out.println("JV parameters :- VOU NO:"+vouNo+" ||UNIT CODE:"+unitCode+"|| VOU DATE:"+vouDate);
                Map n = new HashMap();
                n.put("p_vou_fr", vouNo);
                n.put("p_vou_to", vouNo);
                n.put("p_fr_dt", vouDate);
               n.put("p_to_dt", vouDate);
                n.put("p_voutp", vouTp);
                n.put("p_unit", unitCode);
                conn = getConnection( );
                
                JasperReport design = (JasperReport) JRLoader.loadObject(input);
                System.out.println("Path : " + input + " -------" + design+" param:"+n);
                
                @SuppressWarnings("unchecked")
                net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                byte pdf[] = JasperExportManager.exportReportToPdf(print);
                HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                response.getOutputStream().write(pdf);
                response.getOutputStream().flush();
                response.getOutputStream().close();
                facesContext.responseComplete();
                }else
                System.out.println("File Name/File Path not found.");
                } catch (FileNotFoundException fnfe) {
                // TODO: Add catch code
                fnfe.printStackTrace();
                try {
                    System.out.println("in finally connection closed");
                            conn.close( );
                            conn = null;
                    
                } catch( SQLException e ) {
                    e.printStackTrace( );
                }
                } catch (Exception e) {
                // TODO: Add catch code
                e.printStackTrace();
                try {
                    System.out.println("in finally connection closed");
                            conn.close( );
                            conn = null;
                    
                } catch( SQLException e1 ) {
                    e1.printStackTrace( );
                }
                }finally {
                        try {
                                System.out.println("in finally connection closed");
                                        conn.close( );
                                        conn = null;
                                
                        } catch( SQLException e ) {
                                e.printStackTrace( );
                        }
                }
    }
    
    public Connection getConnection() throws Exception{
            Context ctx = null;
            Connection conn = null;
            ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/ApplicationDBDS");
            conn = ds.getConnection();
            return conn;
        }
    
    
    //*************************************END REPORT CODE*******************************
    public void setReverseApprovalDateBinding(RichInputDate reverseApprovalDateBinding) {
        this.reverseApprovalDateBinding = reverseApprovalDateBinding;
    }

    public RichInputDate getReverseApprovalDateBinding() {
        return reverseApprovalDateBinding;
    }

    public void reverseApprovalVCE(ValueChangeEvent valueChangeEvent) 
    {
        oracle.jbo.domain.Date date=(oracle.jbo.domain.Date)valueChangeEvent.getNewValue();
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        oracle.adf.model.OperationBinding op=(oracle.adf.model.OperationBinding)ADFUtils.findOperation("reverseDateVoucherApproval");
        op.getParamsMap().put("val", date);
        op.execute();
    }

    public void voucherApprovalCheck(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(valueChangeEvent!=null){
        System.out.println("Inside Vcl Voucher Type:"+vouTypeBinding.getValue()+" Vou Series: "+vouSeriesBinding.getValue()+" Unit Code: "+unitCodeBinding.getValue());
            oracle.adf.model.OperationBinding op = (oracle.adf.model.OperationBinding)ADFUtils.findOperation("checkApprovalAuthority");
            op.getParamsMap().put("emp_code", ADFUtils.evaluateEL("#{pageFlowScope.empCode}"));
//            op.getParamsMap().put("emp_code", "SWE161");
        /*  if (vouTypeBinding.getValue().equals("B") && vouSeriesBinding.getValue().equals("R")) {
            op.getParamsMap().put("form_name", "FINBR");     
        }
        if (vouTypeBinding.getValue().equals("B") && vouSeriesBinding.getValue().equals("P")) {
            op.getParamsMap().put("form_name", "FINBP");
        }
        if (vouTypeBinding.getValue().equals("C") && vouSeriesBinding.getValue().equals("R")) {
            op.getParamsMap().put("form_name", "FINCR");
        }
        if (vouTypeBinding.getValue().equals("C") && vouSeriesBinding.getValue().equals("P")) {
            op.getParamsMap().put("form_name", "FINCP"); 
        } */
        if (vouTypeBinding.getValue().equals("J")){
            op.getParamsMap().put("form_name", "FINJV");
            }
        if (vouTypeBinding.getValue().equals("R")){
            op.getParamsMap().put("form_name","FINRV");    
            }
            op.getParamsMap().put("unit_code", unitCodeBinding.getValue());
            op.getParamsMap().put("autho_level", "AP");
            op.execute();
            System.out.println("Result after Approval Check: "+op.getResult());
            if(op.getResult()!=null && op.getResult().equals("Y")){
                    oracle.adf.model.OperationBinding op1 = (oracle.adf.model.OperationBinding)ADFUtils.findOperation("checkApprovalStatus");
                    op1.getParamsMap().put("emp_code", ADFUtils.evaluateEL("#{pageFlowScope.empCode}"));
                  //  op1.getParamsMap().put("emp_code", "SWE161");
                    /*      if (vouTypeBinding.getValue().equals("B") && vouSeriesBinding.getValue().equals("R")) {
                    op1.getParamsMap().put("form_name", "FINBR");     
                    }
                    if (vouTypeBinding.getValue().equals("B") && vouSeriesBinding.getValue().equals("P")) {
                    op1.getParamsMap().put("form_name", "FINBP");
                    }
                    if (vouTypeBinding.getValue().equals("C") && vouSeriesBinding.getValue().equals("R")) {
                    op1.getParamsMap().put("form_name", "FINCR");
                    }
                    if (vouTypeBinding.getValue().equals("C") && vouSeriesBinding.getValue().equals("P")) {
                    op.getParamsMap().put("form_name", "FINCP");
                    } */
                    if (vouTypeBinding.getValue().equals("J")){
                    op1.getParamsMap().put("form_name", "FINJV");
                    }
                    if (vouTypeBinding.getValue().equals("R")){
                    op1.getParamsMap().put("form_name","FINRV");    
                    }
                    op1.getParamsMap().put("unit_code", unitCodeBinding.getValue());
                    op1.getParamsMap().put("autho_level", "AP");
                    op1.getParamsMap().put("amount", amountBinding.getValue());
                    op1.execute();
                    System.out.println("Result after amount Check: "+op1.getResult());
                    if(op1.getResult()!=null && op1.getResult().equals("Y")){
                        if(reverseApprovalDateBinding.getValue()!=null){
                            oracle.jbo.domain.Date date=(oracle.jbo.domain.Date)reverseApprovalDateBinding.getValue();
                            oracle.adf.model.OperationBinding op2 =(oracle.adf.model.OperationBinding) ADFUtils.findOperation("checkForMonthAndYearVouSeriesForAllVouchers");
                            op2.getParamsMap().put("vou_dt", date);
                            op2.getParamsMap().put("FinYear", ADFUtils.resolveExpression("#{pageFlowScope.finYear}"));
                          //  op2.getParamsMap().put("FinYear", "18-19");
                            /*                             if (vouTypeBinding.getValue().equals("B") && vouSeriesBinding.getValue().equals("R")) {
                                op2.getParamsMap().put("Vou_Type", "B");
                                op2.getParamsMap().put("Vou_Series", "R"); 
                            }
                            if (vouTypeBinding.getValue().equals("B") && vouSeriesBinding.getValue().equals("P")) {
                                op2.getParamsMap().put("Vou_Type", "B");
                                op2.getParamsMap().put("Vou_Series", "P");
                            }
                            if (vouTypeBinding.getValue().equals("C") && vouSeriesBinding.getValue().equals("R")) {
                                op2.getParamsMap().put("Vou_Type", "C");
                                op2.getParamsMap().put("Vou_Series", "R");

                            }
                            if (vouTypeBinding.getValue().equals("C") && vouSeriesBinding.getValue().equals("P")) {
                                op2.getParamsMap().put("Vou_Type", "C");
                                op2.getParamsMap().put("Vou_Series", "P");
                            } */
                            if(vouTypeBinding.getValue().equals("J") ){
                                op2.getParamsMap().put("Vou_Type", "J");
                                op2.getParamsMap().put("Vou_Series", "V");
                                }
                            if(vouTypeBinding.getValue().equals("R") ){
                                op2.getParamsMap().put("Vou_Type", "R");
                                op2.getParamsMap().put("Vou_Series", "V");
                                }
                            op2.getParamsMap().put("Unit_Code", unitCodeBinding.getValue());
                            op2.execute();
                            if (op2.getResult() != null && !op2.getResult().toString().equalsIgnoreCase("Y")) {
                                ADFUtils.showMessage(op2.getResult().toString()+". Please Select Another Date.", 0);
                                checkApproveBinding.setValue(null);

                            }
                            
                        }
                    }
                    if(op1.getResult()!=null && op1.getResult().equals("N")){
                            ADFUtils.showMessage("You don't have Authority to approve this Amount.", 0);
                            checkApproveBinding.setValue(null);
                        }
                    
                }
            else{
                ADFUtils.showMessage("You don't have Authority to approve this Voucher.", 0);
                checkApproveBinding.setValue(null);
                }
        }
    }

    public void setCheckApproveBinding(RichSelectBooleanCheckbox checkApproveBinding) {
        this.checkApproveBinding = checkApproveBinding;
    }

    public RichSelectBooleanCheckbox getCheckApproveBinding() {
        return checkApproveBinding;
    }

    public void setVouTypeBinding(RichOutputText vouTypeBinding) {
        this.vouTypeBinding = vouTypeBinding;
    }

    public RichOutputText getVouTypeBinding() {
        return vouTypeBinding;
    }

    public void setVouSeriesBinding(RichOutputText vouSeriesBinding) {
        this.vouSeriesBinding = vouSeriesBinding;
    }

    public RichOutputText getVouSeriesBinding() {
        return vouSeriesBinding;
    }

    public void setAmountBinding(RichOutputText amountBinding) {
        this.amountBinding = amountBinding;
    }

    public RichOutputText getAmountBinding() {
        return amountBinding;
    }

    public void setUnitCodeBinding(RichOutputText unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichOutputText getUnitCodeBinding() {
        return unitCodeBinding;
    }
}
