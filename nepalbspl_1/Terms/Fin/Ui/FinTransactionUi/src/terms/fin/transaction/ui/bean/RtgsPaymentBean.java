package terms.fin.transaction.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectBooleanCheckbox;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;

import terms.fin.transaction.model.view.BankRecoDetailVORowImpl;
import terms.fin.transaction.model.view.RtgsDetailVORowImpl;
import terms.fin.transaction.model.view.RtgsHeaderVORowImpl;

public class RtgsPaymentBean {
    private RichTable createRtgsPaymentTableBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText docNoBinding;
    private RichInputText glCodeBinding;
    private RichInputText voucherNoBinding;
    private RichOutputText bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichInputComboboxListOfValues approvedByBinding;
    private RichInputDate approvedDateBinding;
    private RichInputDate docDateBinding;
    private RichInputText docAmountBinding;
    private RichSelectOneChoice payThroughBinding;
//    private RichInputText vendorCodeBinding;
    private RichInputText vendorNameBinding;
    private RichInputText paymentDocNoBinding;
    private RichInputDate paymentDocDate;
    private RichInputText vendorType;
    private RichInputText paytype;
    private RichInputText docAmtHeader;
    private RichSelectBooleanCheckbox checkAllBinding;
    private RichSelectBooleanCheckbox checkBinding;
    private RichInputComboboxListOfValues preparedByBinding;
    private RichInputText ifscCodeBinding;
    private RichInputComboboxListOfValues bankCodeBinding;
    private RichButton detailCreateButtonBinding;
    private RichInputComboboxListOfValues vendorCdBinding;

    public RtgsPaymentBean() {
    }

    public void DeletepopupDialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                   {
               
                   ADFUtils.findOperation("Delete").execute();
                   FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                   Message.setSeverity(FacesMessage.SEVERITY_INFO);
                   FacesContext fc = FacesContext.getCurrentInstance();
                   fc.addMessage(null, Message);
     
               }
               AdfFacesContext.getCurrentInstance().addPartialTarget(createRtgsPaymentTableBinding);   
        }


    public void PopulateAL(ActionEvent actionEvent) {
        System.out.println("intoSOP===");
       OperationBinding op = (OperationBinding) ADFUtils.findOperation("PopulateRtgsDetail");
   op.execute();
   getDocDateBinding().setDisabled(true);
   getVendorCdBinding().setDisabled(true);
   getIfscCodeBinding().setDisabled(true);
   getPaymentDocNoBinding().setDisabled(true);
   getPaymentDocDate().setDisabled(true);
   getDocAmountBinding().setDisabled(true);
   getVendorType().setDisabled(true);
   getPaytype().setDisabled(true);
        AdfFacesContext.getCurrentInstance().addPartialTarget(createRtgsPaymentTableBinding);
   
    }

    public void setCreateRtgsPaymentTableBinding(RichTable createRtgsPaymentTableBinding) {
        this.createRtgsPaymentTableBinding = createRtgsPaymentTableBinding;
    }

    public RichTable getCreateRtgsPaymentTableBinding() {
        return createRtgsPaymentTableBinding;
    }

    public void SaveAL(ActionEvent actionEvent) {
        DCIteratorBinding Dcite=ADFUtils.findIterator("RtgsDetailVO1Iterator");
        RtgsDetailVORowImpl row=(RtgsDetailVORowImpl) Dcite.getCurrentRow();
        DCIteratorBinding Dcite1=ADFUtils.findIterator("RtgsHeaderVO1Iterator");
        RtgsHeaderVORowImpl row1=(RtgsHeaderVORowImpl) Dcite1.getCurrentRow();
        if((Long)ADFUtils.evaluateEL("#{bindings.RtgsDetailVO1Iterator.estimatedRowCount}")>=1 )
        {
            System.out.println("row.getBankDate"+row.getTransCheck());
            System.out.println("row.getTotalDocAmt()"+row.getTotalDocAmt());
            System.out.println("header docccc a"+docAmtHeader.getValue());
            System.out.println("header docccc rowwwa"+row1.getDocAmount());
            if(row.getTransCheck() .equalsIgnoreCase("Y"))
              {System.out.println("Amount check : "+row.getTotalDocAmt().setScale(2, BigDecimal.ROUND_HALF_DOWN).equals(row1.getDocAmount().setScale(2, BigDecimal.ROUND_HALF_DOWN)));
                if(row.getTotalDocAmt().setScale(2, BigDecimal.ROUND_HALF_DOWN).equals(row1.getDocAmount().setScale(2, BigDecimal.ROUND_HALF_DOWN)))
                {
        oracle.binding.OperationBinding op = ADFUtils.findOperation("generateRtgsNo");
        Object rst = op.execute();

        System.out.println("--------Commit-------");
        System.out.println("value aftr getting result--->?" + rst);


        if ((rst.toString() != null && rst.toString() != "" ) && (row.getTransCheck() .equalsIgnoreCase("Y"))){
            System.out.println("commit actionnn");
            if (op.getErrors().isEmpty()) {
                if(!rst.toString().equalsIgnoreCase("ERROR") ){
                
                ADFUtils.findOperation("Commit").execute();
                FacesMessage Message = new FacesMessage("Record Saved Successfully.Doc No is " + rst + ".");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);

                ADFUtils.setEL("#{pageFlowScope.mode}","V");
                cevmodecheck();
                AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
                }
//                else{
//                        FacesMessage Message = new FacesMessage("Error occurs while Voucher Generation.");
//                        Message.setSeverity(FacesMessage.SEVERITY_INFO);
//                        FacesContext fc = FacesContext.getCurrentInstance();
//                        fc.addMessage(null, Message);
//                    }
            }
        }

        if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ") {
            if (op.getErrors().isEmpty()) {
                if(!rst.toString().equalsIgnoreCase("ERROR") ){
                ADFUtils.findOperation("Commit").execute();
                FacesMessage Message = new FacesMessage("Record Updated Successfully.");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
                
                ADFUtils.setEL("#{pageFlowScope.mode}","V");
                cevmodecheck();
                AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
                }
                else{
                        FacesMessage Message = new FacesMessage("Error occurs while Voucher Generation.");
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);
                        FacesContext fc = FacesContext.getCurrentInstance();
                        fc.addMessage(null, Message);
                    }

            }
        }
                
//            RowSetIterator rsi = Dcite.getRowSetIterator();
//            if(rsi!=null)
//            {
//                System.out.println("============yessssss==========");
//                    Row[] allRowsInRange = rsi.getAllRowsInRange();
//                    for (Row rw : allRowsInRange) 
//                    {
//                        System.out.println("allRowsInRange====="+allRowsInRange.length);
//                        if (rw != null && rw.getAttribute("TransCheck").toString().equalsIgnoreCase("N"))
//                        {
//                            System.out.println("------- Row remove method");
//                            rw.remove();
//                        }
//                    }
//            }
//            rsi.closeRowSetIterator();
        }else
                {
                        ADFUtils.showMessage("Total Doc Amount must be equal to Doc Amount", 0);
                    }
              }
            else{
                  ADFUtils.showMessage("Trans Check must be required. check row to which you want to save in the detail table.", 0);    
                }
        }
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setDocNoBinding(RichInputText docNoBinding) {
        this.docNoBinding = docNoBinding;
    }

    public RichInputText getDocNoBinding() {
        return docNoBinding;
    }

    public void setGlCodeBinding(RichInputText glCodeBinding) {
        this.glCodeBinding = glCodeBinding;
    }

    public RichInputText getGlCodeBinding() {
        return glCodeBinding;
    }

    public void setVoucherNoBinding(RichInputText voucherNoBinding) {
        this.voucherNoBinding = voucherNoBinding;
    }

    public RichInputText getVoucherNoBinding() {
        return voucherNoBinding;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }
    private void cevmodecheck(){
           if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
               cevModeDisableComponent("V");
           }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
               cevModeDisableComponent("C");
           }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
               cevModeDisableComponent("E");
           }
       }
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
               try {
                   Method method1 =
                       component.getClass().getMethod("setDisabled", boolean.class);
                   if (method1 != null) {
                       method1.invoke(component, valueComponent);
                   }
               } catch (NoSuchMethodException e) {
                   try {
                       Method method =
                           component.getClass().getMethod("setReadOnly", boolean.class);
                       if (method != null) {
                           method.invoke(component, valueComponent);
                       }
                   } catch (Exception e1) {
                       // e.printStackTrace();//silently eat this exception.
                   }


               } catch (Exception e) {
                   // e.printStackTrace();//silently eat this exception.
               }
               List<UIComponent> childComponents = component.getChildren();
               for (UIComponent comp : childComponents) {
                   makeComponentHierarchyReadOnly(comp, valueComponent);
               }
           }

       public void cevModeDisableComponent(String mode) {
        if (mode.equals("E")) {
           // getIfscCodeBinding().setDisabled(true);
            getPreparedByBinding().setDisabled(true);
           // getVendorCodeBinding().setDisabled(true);
           // getVendorType().setDisabled(true);
           // getPaymentDocNoBinding().setDisabled(true);
           // getPaymentDocDate().setDisabled(true);
           // getPaytype().setDisabled(true);
            getPayThroughBinding().setDisabled(true);
           // getDocAmountBinding().setDisabled(true);
            getDocNoBinding().setDisabled(true);
                  getUnitCodeBinding().setDisabled(true);
                  getGlCodeBinding().setDisabled(true);
                   getHeaderEditBinding().setDisabled(true);
//                   getApprovedDateBinding().setDisabled(true);
                  // getCheckAllBinding().setDisabled(true);
                  // getCheckBinding().setDisabled(true);
                  getVendorCdBinding().setDisabled(true);
                  getIfscCodeBinding().setDisabled(true);
                  getPaymentDocNoBinding().setDisabled(true);
                  getPaymentDocDate().setDisabled(true);
                  getDocAmountBinding().setDisabled(true);
                  getVendorType().setDisabled(true);
                  getPaytype().setDisabled(true);
                 
           } else if (mode.equals("C")) {
              // getIfscCodeBinding().setDisabled(true);
               getPreparedByBinding().setDisabled(true);
             //  getVendorCodeBinding().setDisabled(true);
             //  getVendorType().setDisabled(true);
             //  getPaymentDocNoBinding().setDisabled(true);
             //  getPaymentDocDate().setDisabled(true);
             //  getPaytype().setDisabled(true);
               getPayThroughBinding().setDisabled(true);
              // getDocAmountBinding().setDisabled(true);
               getDocNoBinding().setDisabled(true);
              // getPayThroughBinding().setDisabled(true);
              // getDocAmountBinding().setDisabled(true);
               getDocNoBinding().setDisabled(true);
//               getApprovedDateBinding().setDisabled(true);
               getUnitCodeBinding().setDisabled(true);
               getGlCodeBinding().setDisabled(true);
              
           } else if (mode.equals("V")) {
         
           }
           
       }

  

    public void editAL(ActionEvent actionEvent) {
        if(approvedByBinding.getValue() !=null) 
        {
            ADFUtils.showMessage("RTGS Paymeent Has Been Authorised,So you can not modify it.", 2);
            ADFUtils.setEL("#{pageFlowScope.mode}", "V");
        }
        else
        {
        cevmodecheck();
           ADFUtils.findOperation("checkTransRTGSPayment").execute();
        }
    }

    public void setApprovedByBinding(RichInputComboboxListOfValues approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputComboboxListOfValues getApprovedByBinding() {
        return approvedByBinding;
    }

    public void setApprovedDateBinding(RichInputDate approvedDateBinding) {
        this.approvedDateBinding = approvedDateBinding;
    }

    public RichInputDate getApprovedDateBinding() {
        return approvedDateBinding;
    }

    public void setDocDateBinding(RichInputDate docDateBinding) {
        this.docDateBinding = docDateBinding;
    }

    public RichInputDate getDocDateBinding() {
        return docDateBinding;
    }

    public void setDocAmountBinding(RichInputText docAmountBinding) {
        this.docAmountBinding = docAmountBinding;
    }

    public RichInputText getDocAmountBinding() {
        return docAmountBinding;
    }

    public void setPayThroughBinding(RichSelectOneChoice payThroughBinding) {
        this.payThroughBinding = payThroughBinding;
    }

    public RichSelectOneChoice getPayThroughBinding() {
        return payThroughBinding;
    }

//    public void setVendorCodeBinding(RichInputText vendorCodeBinding) {
//        this.vendorCodeBinding = vendorCodeBinding;
//    }
//
//    public RichInputText getVendorCodeBinding() {
//        return vendorCodeBinding;
//    }

    public void setVendorNameBinding(RichInputText vendorNameBinding) {
        this.vendorNameBinding = vendorNameBinding;
    }

    public RichInputText getVendorNameBinding() {
        return vendorNameBinding;
    }

    public void setPaymentDocNoBinding(RichInputText paymentDocNoBinding) {
        this.paymentDocNoBinding = paymentDocNoBinding;
    }

    public RichInputText getPaymentDocNoBinding() {
        return paymentDocNoBinding;
    }

    public void setPaymentDocDate(RichInputDate paymentDocDate) {
        this.paymentDocDate = paymentDocDate;
    }

    public RichInputDate getPaymentDocDate() {
        return paymentDocDate;
    }

    public void setVendorType(RichInputText vendorType) {
        this.vendorType = vendorType;
    }

    public RichInputText getVendorType() {
        return vendorType;
    }

    public void setPaytype(RichInputText paytype) {
        this.paytype = paytype;
    }

    public RichInputText getPaytype() {
        return paytype;
    }

    public void setDocAmtHeader(RichInputText docAmtHeader) {
        this.docAmtHeader = docAmtHeader;
    }

    public RichInputText getDocAmtHeader() {
        return docAmtHeader;
    }

    public String SaveAndCloseAC() {
        DCIteratorBinding Dcite=ADFUtils.findIterator("RtgsDetailVO1Iterator");
        RtgsDetailVORowImpl row=(RtgsDetailVORowImpl) Dcite.getCurrentRow();
        DCIteratorBinding Dcite1=ADFUtils.findIterator("RtgsHeaderVO1Iterator");
        RtgsHeaderVORowImpl row1=(RtgsHeaderVORowImpl) Dcite1.getCurrentRow();
        if((Long)ADFUtils.evaluateEL("#{bindings.RtgsDetailVO1Iterator.estimatedRowCount}")>=1 )
        {
            System.out.println("row.getBankDate"+row.getTransCheck());
            System.out.println("row.getTotalDocAmt()"+row.getTotalDocAmt());
            System.out.println("header docccc a"+docAmtHeader.getValue());
            System.out.println("header docccc rowwwa"+row1.getDocAmount());
            if(row.getTransCheck() .equalsIgnoreCase("Y"))
              {System.out.println("Amount check : "+row.getTotalDocAmt().setScale(2, BigDecimal.ROUND_HALF_DOWN).equals(row1.getDocAmount().setScale(2, BigDecimal.ROUND_HALF_DOWN)));
                if(row.getTotalDocAmt().setScale(2, BigDecimal.ROUND_HALF_DOWN).equals(row1.getDocAmount().setScale(2, BigDecimal.ROUND_HALF_DOWN)))
                {
        oracle.binding.OperationBinding op = ADFUtils.findOperation("generateRtgsNo");
        Object rst = op.execute();

        System.out.println("--------Commit-------");
        System.out.println("value aftr getting result--->?" + rst);


        if ((rst.toString() != null && rst.toString() != "" ) && (row.getTransCheck() .equalsIgnoreCase("Y"))){
            System.out.println("commit actionnn");
            if (op.getErrors().isEmpty()) {
                if(!rst.toString().equalsIgnoreCase("ERROR") ){
                
                ADFUtils.findOperation("Commit").execute();
                FacesMessage Message = new FacesMessage("Record Saved Successfully.Doc No is " + rst + ".");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
                return "Save And Cancel";
                }
                else{
                        FacesMessage Message = new FacesMessage("Error occurs while Voucher Generation.");
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);
                        FacesContext fc = FacesContext.getCurrentInstance();
                        fc.addMessage(null, Message);
                    }
            }
        }

        if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ") {
            if (op.getErrors().isEmpty()) {
                if(!rst.toString().equalsIgnoreCase("ERROR") ){
                ADFUtils.findOperation("Commit").execute();
                FacesMessage Message = new FacesMessage("Record Updated Successfully.");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
                return "Save And Cancel";
                }
                else{
                        FacesMessage Message = new FacesMessage("Error occurs while Voucher Generation.");
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);
                        FacesContext fc = FacesContext.getCurrentInstance();
                        fc.addMessage(null, Message);
                    }

            }
        }
                
        //            RowSetIterator rsi = Dcite.getRowSetIterator();
        //            if(rsi!=null)
        //            {
        //                System.out.println("============yessssss==========");
        //                    Row[] allRowsInRange = rsi.getAllRowsInRange();
        //                    for (Row rw : allRowsInRange)
        //                    {
        //                        System.out.println("allRowsInRange====="+allRowsInRange.length);
        //                        if (rw != null && rw.getAttribute("TransCheck").toString().equalsIgnoreCase("N"))
        //                        {
        //                            System.out.println("------- Row remove method");
        //                            rw.remove();
        //                        }
        //                    }
        //            }
        //            rsi.closeRowSetIterator();
        }else
                {
                        ADFUtils.showMessage("Total Doc Amount must be equal to Doc Amount", 0);
                    }
              }
            else{
                  ADFUtils.showMessage("Trans Check must be required. check row to which you want to save in the detail table.", 0);    
            }
        }
            return null;
        
    }

    public void checkALLVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        System.out.println("vce.getNewValue()"+vce.getNewValue());
        if(vce.getNewValue()!=null)
        {
                oracle.binding.OperationBinding Opr = ADFUtils.findOperation("checkALLRTGS");
                 Object Obj=Opr.execute();
        //            if(bindCheckAll.getValue()!=null && bindCheckAll.getValue().equals("Y")){
        //            vouGenBinding.setValue("Y");
            }
        AdfFacesContext.getCurrentInstance().addPartialTarget(createRtgsPaymentTableBinding);   

        
    }

    public void setCheckAllBinding(RichSelectBooleanCheckbox checkAllBinding) {
        this.checkAllBinding = checkAllBinding;
    }

    public RichSelectBooleanCheckbox getCheckAllBinding() {
        return checkAllBinding;
    }

    public void setCheckBinding(RichSelectBooleanCheckbox checkBinding) {
        this.checkBinding = checkBinding;
    }

    public RichSelectBooleanCheckbox getCheckBinding() {
        return checkBinding;
    }

    public void setPreparedByBinding(RichInputComboboxListOfValues preparedByBinding) {
        this.preparedByBinding = preparedByBinding;
    }

    public RichInputComboboxListOfValues getPreparedByBinding() {
        return preparedByBinding;
    }

    public void setIfscCodeBinding(RichInputText ifscCodeBinding) {
        this.ifscCodeBinding = ifscCodeBinding;
    }

    public RichInputText getIfscCodeBinding() {
        return ifscCodeBinding;
    }

    public void setBankCodeBinding(RichInputComboboxListOfValues bankCodeBinding) {
        this.bankCodeBinding = bankCodeBinding;
    }

    public RichInputComboboxListOfValues getBankCodeBinding() {
        return bankCodeBinding;
    }

    public void typeVCL(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        bankCodeBinding.resetValue();
        docAmtHeader.resetValue();
        AdfFacesContext.getCurrentInstance().addPartialTarget(detailCreateButtonBinding);
    }

    public void setDetailCreateButtonBinding(RichButton detailCreateButtonBinding) {
        this.detailCreateButtonBinding = detailCreateButtonBinding;
    }

    public RichButton getDetailCreateButtonBinding() {
        return detailCreateButtonBinding;
    }

    public void setVendorCdBinding(RichInputComboboxListOfValues vendorCdBinding) {
        this.vendorCdBinding = vendorCdBinding;
    }

    public RichInputComboboxListOfValues getVendorCdBinding() {
        return vendorCdBinding;
    }
}
