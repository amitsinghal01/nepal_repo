package terms.fin.transaction.ui.bean;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

public class SearchAdvanceLicenceBean {
    private RichTable searchAdvanceLicencetableBinding;

    public SearchAdvanceLicenceBean() {
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }
        
                      AdfFacesContext.getCurrentInstance().addPartialTarget(searchAdvanceLicencetableBinding);
    }

    public void setSearchAdvanceLicencetableBinding(RichTable searchAdvanceLicencetableBinding) {
        this.searchAdvanceLicencetableBinding = searchAdvanceLicencetableBinding;
    }

    public RichTable getSearchAdvanceLicencetableBinding() {
        return searchAdvanceLicencetableBinding;
    }
}
