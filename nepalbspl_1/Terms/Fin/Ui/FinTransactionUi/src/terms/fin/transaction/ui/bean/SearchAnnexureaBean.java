package terms.fin.transaction.ui.bean;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;

import java.sql.Connection;

import java.sql.SQLException;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import javax.naming.Context;
import javax.naming.InitialContext;

import javax.servlet.http.HttpServletResponse;

import javax.sql.DataSource;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

public class SearchAnnexureaBean {
    private RichTable annexureaTableBinding;
    private String invoceMode ="";

    public SearchAnnexureaBean() {
    }

    public void deletePopupDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
              {
               OperationBinding op = null;
               ADFUtils.findOperation("Delete").execute();
               op = (OperationBinding) ADFUtils.findOperation("Commit");
               Object rst = op.execute();
               System.out.println("Record Delete Successfully");
                   if(op.getErrors().isEmpty()){
                       FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                       Message.setSeverity(FacesMessage.SEVERITY_INFO);
                       FacesContext fc = FacesContext.getCurrentInstance();
                       fc.addMessage(null, Message);
                   } else if (!op.getErrors().isEmpty())
                   {
                      OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
                      Object rstr = opr.execute();
                      FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                      Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                      FacesContext fc = FacesContext.getCurrentInstance();
                      fc.addMessage(null, Message);
                    }
                  AdfFacesContext.getCurrentInstance().addPartialTarget(annexureaTableBinding);
             }   
    }

    public void setAnnexureaTableBinding(RichTable annexureaTableBinding) {
        this.annexureaTableBinding = annexureaTableBinding;
    }

    public RichTable getAnnexureaTableBinding() {
        return annexureaTableBinding;
    }
    public BindingContainer getBindings()
      {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
      }
    
    public void downloadJrxml(FacesContext facesContext, OutputStream outputStream) {
        Connection conn=null;
            try {
                String file_name ="";
                System.out.println("invoceMode===================>"+invoceMode);
                
                if(invoceMode.equals("ANNA"))
                {
                file_name="r_annexure.jasper";
                }
                else if(invoceMode.equals("ANNC"))
                {
                    file_name="r_annx_c.jasper";
                }
                
                System.out.println("After Set Value File Name is===>"+file_name);
                
                OperationBinding binding = ADFUtils.findOperation("fileNameForPrint");
                binding.getParamsMap().put("fileId","ERP0000001882");
                binding.execute();
                System.out.println("Binding Result :"+binding.getResult());
                if(binding.getResult() != null){
                    String result = binding.getResult().toString();
                    int last_index = result.lastIndexOf("/");
                    String path = result.substring(0,last_index+1)+file_name;
                    System.out.println("FILE PATH IS===>"+path);
                    InputStream input = new FileInputStream(path);                     
                    DCIteratorBinding poIter = (DCIteratorBinding) getBindings().get("SearchAnnexureaVO1Iterator");
                    String docNo = poIter.getCurrentRow().getAttribute("DocNo").toString();
                    //   String unitCode = poIter.getCurrentRow().getAttribute("UnitCd").toString();
                    System.out.println("Annexure Number :- "+docNo);
                    Map n = new HashMap();
                    n.put("p_docno", docNo);
                    //       n.put("p_unit", unitCode);
                    if(invoceMode.equals("ANNC"))
                    {
                        System.out.println("--------Inside the Annexure C Report----------");
                        n.put("p_docn", docNo);
                    }
                    conn = getConnection( );
                    
                    JasperReport design = (JasperReport) JRLoader.loadObject(input);
                    System.out.println("Path : " + input + " -------" + design+" param:"+n);
                    
                    @SuppressWarnings("unchecked")
                    net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                    byte pdf[] = JasperExportManager.exportReportToPdf(print);
                    HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                    response.getOutputStream().write(pdf);
                    response.getOutputStream().flush();
                    response.getOutputStream().close();
                    facesContext.responseComplete();
                    }else
                    System.out.println("File Name/File Path not found.");
                    } catch (FileNotFoundException fnfe) {
                    // TODO: Add catch code
                    fnfe.printStackTrace();
                    try {
                        System.out.println("in finally connection closed");
                                conn.close( );
                                conn = null;
                        
                    } catch( SQLException e ) {
                        e.printStackTrace( );
                    }
                    } catch (Exception e) {
                    // TODO: Add catch code
                    e.printStackTrace();
                    try {
                        System.out.println("in finally connection closed");
                                conn.close( );
                                conn = null;
                        
                    } catch( SQLException e1 ) {
                        e1.printStackTrace( );
                    }
                    }finally {
                            try {
                                    System.out.println("in finally connection closed");
                                            conn.close( );
                                            conn = null;
                                    
                            } catch( SQLException e ) {
                                    e.printStackTrace( );
                            }
                    }
        }

    public void setInvoceMode(String invoceMode) {
        this.invoceMode = invoceMode;
    }

    public String getInvoceMode() {
        return invoceMode;
    }
    
    public Connection getConnection() throws Exception{
            Context ctx = null;
            Connection conn = null;
            ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/ApplicationDBDS");
            conn = ds.getConnection();
            return conn;
        }
}
