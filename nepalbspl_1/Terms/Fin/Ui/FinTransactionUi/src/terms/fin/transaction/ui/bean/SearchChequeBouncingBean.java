package terms.fin.transaction.ui.bean;

import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import java.sql.Connection;

import java.sql.SQLException;

import java.util.Hashtable;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import javax.naming.Context;

import oracle.adf.model.binding.DCIteratorBinding;
import javax.sql.DataSource;
import oracle.adf.model.BindingContext;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import javax.naming.InitialContext;

import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JasperReport;

import net.sf.jasperreports.engine.util.JRLoader;

import oracle.jbo.domain.Date;
import oracle.jbo.domain.Timestamp;

public class SearchChequeBouncingBean {
    private RichTable tableBinding;

    public SearchChequeBouncingBean() {
    }

    public void DeletePopupDialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                               {
                               OperationBinding op = null;
                               ADFUtils.findOperation("Delete").execute();
                               op = (OperationBinding) ADFUtils.findOperation("Commit");
                               Object rst = op.execute();
                               System.out.println("Record Delete Successfully");
                                   if(op.getErrors().isEmpty()){
                                       FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                                       Message.setSeverity(FacesMessage.SEVERITY_INFO);
                                       FacesContext fc = FacesContext.getCurrentInstance();
                                       fc.addMessage(null, Message);
                                   } else if (!op.getErrors().isEmpty())
                                   {
                                      OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
                                      Object rstr = opr.execute();
                                      FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                                      Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                                      FacesContext fc = FacesContext.getCurrentInstance();
                                      fc.addMessage(null, Message);
                                    }
                               }

               AdfFacesContext.getCurrentInstance().addPartialTarget(tableBinding);
           
       
    }

    public void setTableBinding(RichTable tableBinding) {
        this.tableBinding = tableBinding;
    }

    public RichTable getTableBinding() {
        return tableBinding;
    }

    public void downloadJrxml(FacesContext facesContext, OutputStream outputStream) {
        Connection conn=null;
                try {
        String voucher_type=null;
                    oracle.adf.model.OperationBinding binding = (oracle.adf.model.OperationBinding) ADFUtils.findOperation("fileNameForPrint");
                    binding.getParamsMap().put("fileId","ERP0000001690");
                    binding.execute();
                    System.out.println("Binding Result "+binding.getResult());
                    if(binding.getResult() != null){
                        InputStream input = new FileInputStream(binding.getResult().toString());                        
                        DCIteratorBinding pvIter = (DCIteratorBinding) getBindings().get("SearchChequeBouncingCancelationVO1Iterator");
                      
                      voucher_type="B";
                      String p_unit =
                    (String) (pvIter.getCurrentRow().getAttribute("UnitCode").toString() != null ?
                              pvIter.getCurrentRow().getAttribute("UnitCode").toString() : "10001");
                        String VouNo = (String) pvIter.getCurrentRow().getAttribute("FinTvouchBp");
                       
                       Timestamp vouDT =(Timestamp) pvIter.getCurrentRow().getAttribute("BounceDate");
                       String str_date = vouDT.toString();
                       Date vouDate = new Date(str_date.substring(0, 10));
                        System.out.println("voucher_type"+voucher_type +"LV_UNIT"+p_unit +"VouNo" +VouNo+"Vou Date:"+vouDate);

                       
                        Map n = new HashMap();
                        n.put("p_unit", p_unit);
                        n.put("p_vou_fr", VouNo);
                        n.put("p_vou_to", VouNo);
                        n.put("p_fr_dt",vouDate );
                        n.put("p_to_dt", vouDate);
                        n.put("p_voutp", voucher_type);
                        conn = getConnection( );
                        
                        JasperReport design = (JasperReport) JRLoader.loadObject(input);
                        System.out.println("Path : " + input + " -------" + design+" param:"+n);
                        
                        @SuppressWarnings("unchecked")
                        net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                        byte pdf[] = JasperExportManager.exportReportToPdf(print);
                        HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                        response.getOutputStream().write(pdf);
                        response.getOutputStream().flush();
                        response.getOutputStream().close();
                        facesContext.responseComplete();
                        }else
                        System.out.println("File Name/File Path not found.");
                        } catch (FileNotFoundException fnfe) {
                        // TODO: Add catch code
                        fnfe.printStackTrace();
                        try {
                            System.out.println("in finally connection closed");
                                    conn.close( );
                                    conn = null;
                            
                        } catch( SQLException e ) {
                            e.printStackTrace( );
                        }
                        } catch (Exception e) {
                        // TODO: Add catch code
                        e.printStackTrace();
                        try {
                            System.out.println("in finally connection closed");
                                    conn.close( );
                                    conn = null;
                            
                        } catch( SQLException e1 ) {
                            e1.printStackTrace( );
                        }
                        }finally {
                                try {
                                        System.out.println("in finally connection closed");
                                                conn.close( );
                                                conn = null;
                                        
                                } catch( SQLException e ) {
                                        e.printStackTrace( );
                                }
                        }
            }
    
    public BindingContainer getBindings()
          {
            return BindingContext.getCurrent().getCurrentBindingsEntry();
          }
    
    public Connection getConnection() throws Exception{
            Context ctx = null;
            Connection conn = null;
            ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/ApplicationDBDS");
            conn = ds.getConnection();
            return conn;
        }
}
