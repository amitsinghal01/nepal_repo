package terms.fin.transaction.ui.bean;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;

import javax.servlet.http.HttpServletResponse;

import javax.sql.DataSource;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.domain.Date;

public class SearchCollectionAdviceBean {
    private RichTable bindSearchTable;
    private String paymentMode ="";

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public SearchCollectionAdviceBean() {
    }

    public void setBindSearchTable(RichTable bindSearchTable) {
        this.bindSearchTable = bindSearchTable;
    }

    public RichTable getBindSearchTable() {
        return bindSearchTable;
    }

    public void deleteRecordDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                               {
                               OperationBinding op = null;
                               ADFUtils.findOperation("Delete").execute();
                               op = (OperationBinding) ADFUtils.findOperation("Commit");
                               Object rst = op.execute();
                               System.out.println("Record Delete Successfully");
                                   if(op.getErrors().isEmpty()){
                                       FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                                       Message.setSeverity(FacesMessage.SEVERITY_INFO);
                                       FacesContext fc = FacesContext.getCurrentInstance();
                                       fc.addMessage(null, Message);
                                   } else if (!op.getErrors().isEmpty())
                                   {
                                      OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
                                      Object rstr = opr.execute();
                                      FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                                      Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                                      FacesContext fc = FacesContext.getCurrentInstance();
                                      fc.addMessage(null, Message);
                                    }
                               }

               AdfFacesContext.getCurrentInstance().addPartialTarget(bindSearchTable);
           
    }
    //***************************************REPORT CODE*********************************
    
    public BindingContainer getBindings()
      {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
      }
    
    public void downloadJrxml1(FacesContext facesContext, OutputStream outputStream) {
        Connection conn=null;
        try {
            DCIteratorBinding caIter = (DCIteratorBinding) getBindings().get("SearchCollectionAdviceVO1Iterator");
            String vouNo = (String)caIter.getCurrentRow().getAttribute("VouNo1");
            String vouTp=(String)caIter.getCurrentRow().getAttribute("Type");
            String unitCode = caIter.getCurrentRow().getAttribute("Unit").toString();
            oracle.jbo.domain.Date approvalDate =(oracle.jbo.domain.Date)caIter.getCurrentRow().getAttribute("ApprovalDate");
            
            System.out.println("JV parameters :- VOU NO:"+vouNo+" ||UNIT CODE:"+unitCode+"|| VOU DATE:"+approvalDate);
            String file_name ="";
            
            if(vouTp.equals("B"))
            {
            file_name="r_bank_prn.jasper";
            }
            else if(vouTp.equals("C"))
            {
                file_name="r_cash_prn.jasper";
            }
            System.out.println("After Set Value File Name is===>"+file_name);
            //*************Find File name***************//
            OperationBinding binding = ADFUtils.findOperation("fileNameForPrint");
            binding.getParamsMap().put("fileId","ERP0000000137");
            binding.execute();
            String a="A";
            //*************End Find File name***********//
            System.out.println("Binding Result :"+binding.getResult());
            if(binding.getResult() != null){
                String result = binding.getResult().toString();
                int last_index = result.lastIndexOf("/");
                String path = result.substring(0,last_index+1)+file_name;
                System.out.println("FILE PATH IS===>"+path);
                InputStream input = new FileInputStream(path);                
                Map n = new HashMap();
                n.put("p_unit", unitCode);
                n.put("p_vou_fr", vouNo);
                n.put("p_vou_to", vouNo);
                n.put("p_fr_dt", approvalDate);
                n.put("p_to_dt", approvalDate);
                n.put("p_voutp", vouTp);
                conn = getConnection( );
                
                JasperReport design = (JasperReport) JRLoader.loadObject(input);
                System.out.println("Path : " + input + " -------" + design+" param:"+n);
                
                @SuppressWarnings("unchecked")
                net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                byte pdf[] = JasperExportManager.exportReportToPdf(print);
                HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                response.getOutputStream().write(pdf);
                response.getOutputStream().flush();
                response.getOutputStream().close();
                facesContext.responseComplete();
                }else
                System.out.println("File Name/File Path not found.");
                } catch (FileNotFoundException fnfe) {
                // TODO: Add catch code
                fnfe.printStackTrace();
                try {
                        System.out.println("in finally connection closed");
                                conn.close( );
                                conn = null;
                        
                } catch( SQLException e ) {
                        e.printStackTrace( );
                }
                } catch (Exception e) {
                // TODO: Add catch code
                e.printStackTrace();
                try {
                        System.out.println("in finally connection closed");
                                conn.close( );
                                conn = null;
                        
                } catch( SQLException e1 ) {
                        e1.printStackTrace( );
                }
                }finally {
                            try {
                                    System.out.println("in finally connection closed");
                                            conn.close( );
                                            conn = null;
                                    
                            } catch( SQLException e ) {
                                    e.printStackTrace( );
                            }
                    }
    }
    public void downloadJrxml2(FacesContext facesContext, OutputStream outputStream) {
        Connection conn=null;
        try {
            DCIteratorBinding caIter = (DCIteratorBinding) getBindings().get("SearchCollectionAdviceVO1Iterator");
            String interUnitVouNo=(String)caIter.getCurrentRow().getAttribute("VouNoUnit");
            String interUnitCode = (String)caIter.getCurrentRow().getAttribute("InterUnitCode");
            oracle.jbo.domain.Date vouDate =(oracle.jbo.domain.Date)caIter.getCurrentRow().getAttribute("ApprovalDate");
            
            System.out.println("CA parameters :- VOU NO:"+interUnitVouNo+" ||UNIT CODE:"+interUnitCode+"|| VOU DATE:"+vouDate);
            String file_name ="";
             file_name="r_jour_prn.jasper";    
            System.out.println("After Set Value File Name is===>"+file_name);

            //*************Find File name***************//
            OperationBinding binding = ADFUtils.findOperation("fileNameForPrint");
            binding.getParamsMap().put("fileId","ERP0000000137");
            binding.execute();
            String a="A";
            //*************End Find File name***********//
            System.out.println("Binding Result :"+binding.getResult());
            if(binding.getResult() != null){
                String result = binding.getResult().toString();
                int last_index = result.lastIndexOf("/");
                String path = result.substring(0,last_index+1)+file_name;
                System.out.println("FILE PATH IS===>"+path);
                InputStream input = new FileInputStream(path);                
                Map n = new HashMap();
                n.put("p_vou_fr", interUnitVouNo);
                n.put("p_vou_to", interUnitVouNo);
                n.put("p_fr_dt", vouDate);
                n.put("p_to_dt", vouDate);
                n.put("p_voutp", "J"); 
                n.put("p_unit", interUnitCode);
                conn = getConnection( );
                
                JasperReport design = (JasperReport) JRLoader.loadObject(input);
                System.out.println("Path : " + input + " -------" + design+" param:"+n);
                
                @SuppressWarnings("unchecked")
                net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                byte pdf[] = JasperExportManager.exportReportToPdf(print);
                HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                response.getOutputStream().write(pdf);
                response.getOutputStream().flush();
                response.getOutputStream().close();
                facesContext.responseComplete();
                }else
                System.out.println("File Name/File Path not found.");
                } catch (FileNotFoundException fnfe) {
                // TODO: Add catch code
                fnfe.printStackTrace();
                try {
                        System.out.println("in finally connection closed");
                                conn.close( );
                                conn = null;
                        
                } catch( SQLException e ) {
                        e.printStackTrace( );
                }
                } catch (Exception e) {
                // TODO: Add catch code
                e.printStackTrace();
                try {
                        System.out.println("in finally connection closed");
                                conn.close( );
                                conn = null;
                        
                } catch( SQLException e1 ) {
                        e1.printStackTrace( );
                }
                }finally {
                            try {
                                    System.out.println("in finally connection closed");
                                            conn.close( );
                                            conn = null;
                                    
                            } catch( SQLException e ) {
                                    e.printStackTrace( );
                            }
                    }
    
    }
 
 
    //Report Code
    
    public void downloadJrxml(FacesContext facesContext, OutputStream outputStream) {
        Connection conn=null;
    try {
        String file_name ="";
        String payment_Type="";
        String vou_type="";
        Date voucherDate =null;
        Date approvalDate =null;
        String str_date ="";
        System.out.println("Payment  Mode===================>"+paymentMode);
        
        if(paymentMode.equals("B"))
        {
            System.out.println("inside the BBBBBBB");
            file_name="r_bank_prn.jasper";
        }
        else if(paymentMode.equals("I"))
        {
            System.out.println("inside the IIIIIIIIII");
            file_name="r_bank_prn.jasper";
        }
        else if(paymentMode.equals("D"))
        {
            System.out.println("inside the DDDDDDDDD");
            file_name="r_jour_prn.jasper";
        }
        System.out.println("******After Set Value File Name Is******:"+file_name);
                     
        OperationBinding binding = ADFUtils.findOperation("fileNameForPrint");
        binding.getParamsMap().put("fileId","ERP0000000137");
        binding.execute();
        System.out.println("Binding Result :"+binding.getResult());

        if(binding.getResult() != null)
        {
            String result = binding.getResult().toString();            
            DCIteratorBinding collAd_itr = (DCIteratorBinding) getBindings().get("SearchCollectionAdviceVO1Iterator");
            
            String interUnitVouNo=(String)collAd_itr.getCurrentRow().getAttribute("VouNoUnit");
//            String interUnitCode = (String)collAd_itr.getCurrentRow().getAttribute("InterUnitCode");
            String deductnVouNo = (String)collAd_itr.getCurrentRow().getAttribute("VouNo");
//           oracle.jbo.domain.Date vouDateApp =(oracle.jbo.domain.Date)collAd_itr.getCurrentRow().getAttribute("ApprovalDate");
            
// oracle.jbo.domain.Date approvalDate =(oracle.jbo.domain.Date)collAd_itr.getCurrentRow().getAttribute("ApprovalDate");
            approvalDate =(Date)collAd_itr.getCurrentRow().getAttribute("ApprovalDate");
            str_date = approvalDate.toString();
            voucherDate = new Date(str_date.substring(0, 10));
            
            System.out.println("collAd_itr.getCurrentRow().getAttribute(\"ApprovalDate\")"+collAd_itr.getCurrentRow().getAttribute("ApprovalDate")+"===="+voucherDate);
            if(collAd_itr.getCurrentRow().getAttribute("InterUnit")!=null && collAd_itr.getCurrentRow().getAttribute("InterUnit").equals("Y") && paymentMode.equals("B"))
            {
                file_name="r_jour_prn.jasper";
                payment_Type="J";
                System.out.println("===========Journal YYY===:"+payment_Type);
            }
            else
            {
                if(collAd_itr.getCurrentRow().getAttribute("Type")!=null && collAd_itr.getCurrentRow().getAttribute("InterUnit")!=null){
                    if(collAd_itr.getCurrentRow().getAttribute("Type").equals("B") && collAd_itr.getCurrentRow().getAttribute("InterUnit").equals("N"))
                    {
                        System.out.println("===========Bank && NNN===:"+payment_Type);
                        file_name="r_bank_prn.jasper";
                        payment_Type="B";
                    }
                    if(collAd_itr.getCurrentRow().getAttribute("Type").equals("C") && collAd_itr.getCurrentRow().getAttribute("InterUnit").equals("N"))
                    {  System.out.println("===========Cash && NNN===:"+payment_Type);
                        file_name="r_cash_prn.jasper";
                        payment_Type="C";
                    }
                }
            }
           
            int last_index = result.lastIndexOf("/");
            String path = result.substring(0,last_index+1)+file_name;
            System.out.println("*******************File Path Is***************:"+path);
            
            InputStream input = new FileInputStream(path); 
               
            Map n = new HashMap();
            if(paymentMode.equals("D"))
            {
                if(deductnVouNo!=null)
                {
                    n.put("p_vou_fr", deductnVouNo);
                    n.put("p_vou_to", deductnVouNo);
                    n.put("p_fr_dt", voucherDate);
                    n.put("p_to_dt", voucherDate);
                    n.put("p_voutp", "J"); 
                    n.put("p_unit", collAd_itr.getCurrentRow().getAttribute("Unit"));
                    conn = getConnection( );
                    
                    JasperReport design = (JasperReport) JRLoader.loadObject(input);
                    System.out.println("Path : " + input + " -------" + design+" param:"+n);
                    
                    @SuppressWarnings("unchecked")
                    net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                    byte pdf[] = JasperExportManager.exportReportToPdf(print);
                    HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                    response.getOutputStream().write(pdf);
                    response.getOutputStream().flush();
                    response.getOutputStream().close();
                    facesContext.responseComplete();
                }
            }
            else if(paymentMode.equals("B"))
            {
                if(collAd_itr.getCurrentRow().getAttribute("VouNo1")!=null)
                {
                    n.put("p_vou_fr", collAd_itr.getCurrentRow().getAttribute("VouNo1"));
                    n.put("p_vou_to", collAd_itr.getCurrentRow().getAttribute("VouNo1"));
                    n.put("p_fr_dt", voucherDate);
                    n.put("p_to_dt", voucherDate);
                    System.out.println("Tyepe is======"+payment_Type);
                    n.put("p_voutp", payment_Type); 
                    n.put("p_unit", collAd_itr.getCurrentRow().getAttribute("Unit"));
                    conn = getConnection( );
                    
                    JasperReport design = (JasperReport) JRLoader.loadObject(input);
                    System.out.println("Path : " + input + " -------" + design+" param:"+n);
                    
                    @SuppressWarnings("unchecked")
                    net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                    byte pdf[] = JasperExportManager.exportReportToPdf(print);
                    HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                    response.getOutputStream().write(pdf);
                    response.getOutputStream().flush();
                    response.getOutputStream().close();
                    facesContext.responseComplete();
                }
            }
            else if(paymentMode.equals("I"))
            {
                if(collAd_itr.getCurrentRow().getAttribute("VouNoUnit")!=null)
                {
                    n.put("p_unit", collAd_itr.getCurrentRow().getAttribute("InterUnitCode"));
                    n.put("p_vou_fr", collAd_itr.getCurrentRow().getAttribute("VouNoUnit"));
                    n.put("p_vou_to", collAd_itr.getCurrentRow().getAttribute("VouNoUnit"));
                    n.put("p_fr_dt", voucherDate);
                    n.put("p_to_dt", voucherDate);
                    n.put("p_voutp", "B");
                    conn = getConnection( );
                    
                    JasperReport design = (JasperReport) JRLoader.loadObject(input);
                    System.out.println("Path : " + input + " -------" + design+" param:"+n);
                    
                    @SuppressWarnings("unchecked")
                    net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                    byte pdf[] = JasperExportManager.exportReportToPdf(print);
                    HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                    response.getOutputStream().write(pdf);
                    response.getOutputStream().flush();
                    response.getOutputStream().close();
                    facesContext.responseComplete();
                }
            }
            }else
            System.out.println("File Name/File Path not found.");
            } catch (FileNotFoundException fnfe) {
            // TODO: Add catch code
            fnfe.printStackTrace();
            try {
                    System.out.println("in finally connection closed");
                            conn.close( );
                            conn = null;
                    
            } catch( SQLException e ) {
                    e.printStackTrace( );
            }
            } catch (Exception e) {
            // TODO: Add catch code
            e.printStackTrace();
            try {
                    System.out.println("in finally connection closed");
                            conn.close( );
                            conn = null;
                    
            } catch( SQLException e1 ) {
                    e1.printStackTrace( );
            }
            }finally {
                        try {
                                System.out.println("in finally connection closed");
                                        conn.close( );
                                        conn = null;
                                
                        } catch( SQLException e ) {
                                e.printStackTrace( );
                        }
                }
}   
    
    public Connection getConnection() throws Exception{
            Context ctx = null;
            Connection conn = null;
            ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/ApplicationDBDS");
            conn = ds.getConnection();
            return conn;
        }
}
