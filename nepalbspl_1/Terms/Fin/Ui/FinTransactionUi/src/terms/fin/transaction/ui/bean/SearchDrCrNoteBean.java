package terms.fin.transaction.ui.bean;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;

import java.sql.Connection;

import java.sql.SQLException;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import javax.naming.Context;
import javax.naming.InitialContext;

import javax.servlet.http.HttpServletResponse;

import javax.sql.DataSource;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.domain.Timestamp;

public class SearchDrCrNoteBean {
    private RichTable searchDrCrNoteTable;

    public SearchDrCrNoteBean() {
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        {
            if (dialogEvent.getOutcome().name().equals("ok")) {
                OperationBinding op = null;
                ADFUtils.findOperation("Delete").execute();
                ADFUtils.findOperation("Commit").execute();
                if (op.getErrors().isEmpty()) {
                    FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);
                } else if (!op.getErrors().isEmpty()) {
                    ADFUtils.findOperation("Rollback").execute();
                    FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                    Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);
                }
            }

            AdfFacesContext.getCurrentInstance().addPartialTarget(searchDrCrNoteTable);

        }


    }

    public void setSearchDrCrNoteTable(RichTable searchDrCrNoteTable) {
        this.searchDrCrNoteTable = searchDrCrNoteTable;
    }

    public RichTable getSearchDrCrNoteTable() {
        return searchDrCrNoteTable;
    }

    public void SearchToCreateAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert").execute();
    }


    //***************************************REPORT CODE*********************************

    public BindingContainer getBindings() {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
    }

    public void downloadJrxml(FacesContext facesContext, OutputStream outputStream) {
        Connection conn = null;
        try {
            conn = getConnection();
            OperationBinding binding = ADFUtils.findOperation("fileNameForPrint");
            binding.getParamsMap().put("fileId", "ERP0000001348");
            binding.execute();
            if (binding.getResult() != null) {
                String vouNo = null;
                InputStream input = new FileInputStream(binding.getResult().toString());
//                InputStream input = new FileInputStream("//home//lenovo//Documents//workspace//iReport//erp-reports//Common//dr_cr_note.jasper");
                DCIteratorBinding drcrIter = (DCIteratorBinding) getBindings().get("SearchDrCrNoteHeaderVO1Iterator");
                if (drcrIter != null) {
                    vouNo = (String) drcrIter.getCurrentRow().getAttribute("DnCnNo");
                }
                String unitCode = (String) drcrIter.getCurrentRow().getAttribute("DnCnUnit");
                Timestamp vou_time = (Timestamp) drcrIter.getCurrentRow().getAttribute("FinTvouchVouDate");
                String str_date = vou_time == null ? null : vou_time.toString();
                oracle.jbo.domain.Date vouDate = null;
                if(str_date!=null){
                    vouDate = new oracle.jbo.domain.Date(str_date.substring(0, 10));
                }

                System.out.println("Debit Credit Note parameters :- VOU NO:" + vouNo + " ||UNIT CODE:" + unitCode +
                                   "|| VOU DATE:" + vouDate);

                if (vouNo != null) {
                    Map params = new HashMap();
                    params.put("p_vou_fr", vouNo);
                    params.put("p_vou_to", vouNo);
                    params.put("p_fr_dt", vouDate);
                    params.put("p_to_dt", vouDate);
                    params.put("p_voutp", "J");
                    params.put("p_unit", unitCode);
                    conn = getConnection();

                    JasperReport design = (JasperReport) JRLoader.loadObject(input);
                    System.out.println("Path : " + input + " -------" + design + " param:" + params);

                    @SuppressWarnings("unchecked")
                    JasperPrint print = JasperFillManager.fillReport(design, params, conn);
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                    byte pdf[] = JasperExportManager.exportReportToPdf(print);
                    HttpServletResponse response =
                        (HttpServletResponse) facesContext.getExternalContext().getResponse();
                    OutputStream os = response.getOutputStream();
                    os.write(pdf);
                    os.flush();
                    os.close();
                    facesContext.responseComplete();
                }
            }
        } catch (Exception fnfe) {
            fnfe.printStackTrace();
            FacesMessage Message = new FacesMessage(fnfe.getCause() + "\n" + fnfe.getMessage());
            Message.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                    conn = null;
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public Connection getConnection() throws Exception {
        Context ctx = null;
        Connection conn = null;
        ctx = new InitialContext();
        DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/ApplicationDBDS");
        conn = ds.getConnection();
        return conn;
    }


}
