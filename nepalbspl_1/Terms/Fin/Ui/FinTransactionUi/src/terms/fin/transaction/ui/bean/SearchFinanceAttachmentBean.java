package terms.fin.transaction.ui.bean;

import oracle.adf.view.rich.component.rich.data.RichTable;

public class SearchFinanceAttachmentBean {
    private RichTable searchFinanceAttachmentTableBinding;

    public SearchFinanceAttachmentBean() {
    }

    public void setSearchFinanceAttachmentTableBinding(RichTable searchFinanceAttachmentTableBinding) {
        this.searchFinanceAttachmentTableBinding = searchFinanceAttachmentTableBinding;
    }

    public RichTable getSearchFinanceAttachmentTableBinding() {
        return searchFinanceAttachmentTableBinding;
    }
}
