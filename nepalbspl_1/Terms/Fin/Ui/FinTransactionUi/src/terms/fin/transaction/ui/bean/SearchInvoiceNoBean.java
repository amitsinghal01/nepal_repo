package terms.fin.transaction.ui.bean;

import javax.faces.application.FacesMessage;

import javax.faces.context.FacesContext;

import oracle.adf.model.OperationBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

public class SearchInvoiceNoBean {
    private RichTable searchInvoiceTablebinding;

    public SearchInvoiceNoBean() {
    }

    public void deletePopUpDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                      {
                       //OperationBinding op = null;
                       ADFUtils.findOperation("Delete").execute();
                       ADFUtils.findOperation("Commit").execute();
                      // Object rst = op.execute();
                       System.out.println("Record Delete Successfully");                          
                               FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                               Message.setSeverity(FacesMessage.SEVERITY_INFO);
                               FacesContext fc = FacesContext.getCurrentInstance();
                               fc.addMessage(null, Message);
                                                    
                          AdfFacesContext.getCurrentInstance().addPartialTarget(searchInvoiceTablebinding);
                     }  
    }

    public void setSearchInvoiceTablebinding(RichTable searchInvoiceTablebinding) {
        this.searchInvoiceTablebinding = searchInvoiceTablebinding;
    }

    public RichTable getSearchInvoiceTablebinding() {
        return searchInvoiceTablebinding;
    }
}
