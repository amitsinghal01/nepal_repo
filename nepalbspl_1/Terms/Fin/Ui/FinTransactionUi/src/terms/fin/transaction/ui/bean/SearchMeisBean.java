package terms.fin.transaction.ui.bean;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class SearchMeisBean {
    private RichTable searchMeisTableBinding;

    public SearchMeisBean() {
    }

    public void deleteButtonDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
              {
               OperationBinding op = null;
               ADFUtils.findOperation("Delete").execute();
               op = (OperationBinding) ADFUtils.findOperation("Commit");
               Object rst = op.execute();
               System.out.println("Record Delete Successfully");
                   if(op.getErrors().isEmpty()){
                       ADFUtils.showMessage("Record Deleted Successfully",2);
                   } else if (!op.getErrors().isEmpty())
                   {
                      OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
                      Object rstr = opr.execute();
                      ADFUtils.showMessage("Record cannot be deleted. Child record found.",2);
                    }
                   AdfFacesContext.getCurrentInstance().addPartialTarget(searchMeisTableBinding);
             }
    }

    public void setSearchMeisTableBinding(RichTable searchMeisTableBinding) {
        this.searchMeisTableBinding = searchMeisTableBinding;
    }

    public RichTable getSearchMeisTableBinding() {
        return searchMeisTableBinding;
    }
}
