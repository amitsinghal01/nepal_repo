package terms.fin.transaction.ui.bean;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;

import java.sql.Connection;

import java.sql.SQLException;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.faces.context.FacesContext;

import javax.naming.Context;
import javax.naming.InitialContext;

import javax.servlet.http.HttpServletResponse;

import javax.sql.DataSource;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.domain.Date;

public class SearchPaymentRecommendBean {
    private RichTable detailTableBinding;

    public SearchPaymentRecommendBean() {
    }

    public void setDetailTableBinding(RichTable detailTableBinding) {
        this.detailTableBinding = detailTableBinding;
    }

    public RichTable getDetailTableBinding() {
        return detailTableBinding;
    }
    public void deleteButtonDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
              {
               OperationBinding op = null;
               ADFUtils.findOperation("Delete").execute();
               op = (OperationBinding) ADFUtils.findOperation("Commit");
               Object rst = op.execute();
               System.out.println("Record Delete Successfully");
                   if(op.getErrors().isEmpty()){
                       ADFUtils.showMessage("Record Deleted Successfully",2);
                   } else if (!op.getErrors().isEmpty())
                   {
                      OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
                      Object rstr = opr.execute();
                      ADFUtils.showMessage("Record cannot be deleted. Child record found.",2);
                    }
                   AdfFacesContext.getCurrentInstance().addPartialTarget(detailTableBinding);
             }
    }
    
    //*********************Report Calling Code***********************//
    
    public BindingContainer getBindings()
      {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
      }
    
    public void downloadJrxml(FacesContext facesContext, OutputStream outputStream) {
        Connection conn=null;
        try {
            OperationBinding binding = ADFUtils.findOperation("fileNameForPrint");
            binding.getParamsMap().put("fileId","ERP0000001917");
            binding.execute();
            System.out.println("Binding Result :"+binding.getResult().toString());
            if(binding.getResult() != null){
                InputStream input = new FileInputStream(binding.getResult().toString());                
                DCIteratorBinding poIter = (DCIteratorBinding) getBindings().get("SearchPaymentRecommendHeaderVO1Iterator");
                String docno = poIter.getCurrentRow().getAttribute("DocNo").toString();
                String unitCode = poIter.getCurrentRow().getAttribute("UnitCode").toString();
                Date  docDate  =(Date)poIter.getCurrentRow().getAttribute("DocDate");
                System.out.println("Document No :- "+docno+" Unit Code is "+unitCode+" and the Documet Date is:"+docDate);
                Map n = new HashMap();
                n.put("p_unit", unitCode);
                n.put("p_doc_no", docno);
                n.put("p_doc_dt", docDate);
                conn = getConnection( );
                
                JasperReport design = (JasperReport) JRLoader.loadObject(input);
                System.out.println("Path : " + input + " -------" + design+" param:"+n);
                
                @SuppressWarnings("unchecked")
                net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                byte pdf[] = JasperExportManager.exportReportToPdf(print);
                HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                response.getOutputStream().write(pdf);
                response.getOutputStream().flush();
                response.getOutputStream().close();
                facesContext.responseComplete();
                }else
                System.out.println("File Name/File Path not found.");
                } catch (FileNotFoundException fnfe) {
                // TODO: Add catch code
                fnfe.printStackTrace();
                try {
                    System.out.println("in finally connection closed");
                            conn.close( );
                            conn = null;
                    
                } catch( SQLException e ) {
                    e.printStackTrace( );
                }
                } catch (Exception e) {
                // TODO: Add catch code
                e.printStackTrace();
                try {
                    System.out.println("in finally connection closed");
                            conn.close( );
                            conn = null;
                    
                } catch( SQLException e1 ) {
                    e1.printStackTrace( );
                }
                }finally {
                        try {
                                System.out.println("in finally connection closed");
                                        conn.close( );
                                        conn = null;
                                
                        } catch( SQLException e ) {
                                e.printStackTrace( );
                        }
                }
    }
    
    public Connection getConnection() throws Exception{
            Context ctx = null;
            Connection conn = null;
            ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/ApplicationDBDS");
            conn = ds.getConnection();
            return conn;
        }
}
