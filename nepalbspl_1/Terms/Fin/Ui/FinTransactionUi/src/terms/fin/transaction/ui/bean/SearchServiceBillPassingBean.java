package terms.fin.transaction.ui.bean;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;

import java.sql.Connection;

import java.sql.SQLException;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import javax.naming.Context;
import javax.naming.InitialContext;

import javax.servlet.http.HttpServletResponse;

import javax.sql.DataSource;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.domain.Date;
import oracle.jbo.domain.Timestamp;

public class SearchServiceBillPassingBean {
    private RichTable tableBinding;
    private String vouType ="";
    
    public SearchServiceBillPassingBean() {
    }

    public void deleteSearchDialogListenerDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
              {
               OperationBinding op = null;
               ADFUtils.findOperation("Delete").execute();
               op = (OperationBinding) ADFUtils.findOperation("Commit");
               Object rst = op.execute();
               System.out.println("Record Delete Successfully");
                   if(op.getErrors().isEmpty()){
                       FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                       Message.setSeverity(FacesMessage.SEVERITY_INFO);
                       FacesContext fc = FacesContext.getCurrentInstance();
                       fc.addMessage(null, Message);
                   } else if (!op.getErrors().isEmpty())
                   {
                      OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
                      Object rstr = opr.execute();
                      FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                      Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                      FacesContext fc = FacesContext.getCurrentInstance();
                      fc.addMessage(null, Message);
                    }
                   AdfFacesContext.getCurrentInstance().addPartialTarget(tableBinding);
             }
    }

    public void setTableBinding(RichTable tableBinding) {
        this.tableBinding = tableBinding;
    }

    public RichTable getTableBinding() {
        return tableBinding;
    }
    
//--------------- Print The Report-------------------------
    
    public void downloadJrxml(FacesContext facesContext, OutputStream outputStream) {
        Connection conn=null;
            try {
                String file_name ="";
                System.out.println("invoceMode===================>"+vouType);
                String vouNo =null;
                String voucher_type="";
               System.out.println("After Set Value File Name is===>"+file_name);
                OperationBinding binding = ADFUtils.findOperation("fileNameForPrint");
                binding.getParamsMap().put("fileId","GST0000000005");
                binding.execute();
                System.out.println("Binding Result :"+binding.getResult());
                if(binding.getResult() != null){

                    InputStream input = new FileInputStream(binding.getResult().toString()); 
                    DCIteratorBinding pvIter = (DCIteratorBinding) getBindings().get("SearchServiceBillPassingHeaderVO1Iterator");
                    voucher_type="J";
                    if(pvIter.getCurrentRow().getAttribute("VouNo")!=null)
                    vouNo = pvIter.getCurrentRow().getAttribute("VouNo").toString();

                    String unitCode = pvIter.getCurrentRow().getAttribute("UnitCode").toString();
                    Date  vouDate  =(Date)pvIter.getCurrentRow().getAttribute("VouDate");
 
                    System.out.println("VOUCHER TYPE parameters :- VOU NO:"+vouNo+" ||UNIT CODE:"+unitCode+"|| VOU DATE:"+vouDate+" VOUCHER TYPE=>"+voucher_type+" VOUCHER MODE TYPE=>"+vouType);
                    if(vouNo!=null && vouType.equalsIgnoreCase("JV"))
                    {                    
                        Map n = new HashMap();
                        n.put("p_vou_fr", vouNo);
                        n.put("p_vou_to", vouNo);
                        n.put("p_fr_dt", vouDate);
                        n.put("p_to_dt", vouDate);
                        n.put("p_voutp",voucher_type);
                        n.put("p_unit", unitCode);
                        conn = getConnection( );
                        
                        JasperReport design = (JasperReport) JRLoader.loadObject(input);
                        System.out.println("Path : " + input + " -------" + design+" param:"+n);
                        
                        @SuppressWarnings("unchecked")
                        net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                        byte pdf[] = JasperExportManager.exportReportToPdf(print);
                        HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                        response.getOutputStream().write(pdf);
                        response.getOutputStream().flush();
                        response.getOutputStream().close();
                        facesContext.responseComplete();    
                    }
                    else if(vouNo == null && vouType.equalsIgnoreCase("JV"))
                    {
                        String result = binding.getResult().toString();
                        int last_index = result.lastIndexOf("/");
                        String path = result.substring(0,last_index+1)+"r_bill_prn.jasper";
                        System.out.println("--------JV------Path-----------"+path);
                        InputStream input1 = new FileInputStream(path);
                        
                        Map n = new HashMap();
                        n.put("p_vou_fr", pvIter.getCurrentRow().getAttribute("BillId"));
                        n.put("p_vou_to", pvIter.getCurrentRow().getAttribute("BillId"));
                        n.put("p_fr_dt", pvIter.getCurrentRow().getAttribute("BillDate"));
                        n.put("p_to_dt", pvIter.getCurrentRow().getAttribute("BillDate"));
                        n.put("p_voutp","J");
                        n.put("p_unit", unitCode);
                        conn = getConnection( );
                        
                        JasperReport design = (JasperReport) JRLoader.loadObject(input);
                        System.out.println("Path : " + input + " -------" + design+" param:"+n);
                        
                        @SuppressWarnings("unchecked")
                        net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                        byte pdf[] = JasperExportManager.exportReportToPdf(print);
                        HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                        response.getOutputStream().write(pdf);
                        response.getOutputStream().flush();
                        response.getOutputStream().close();
                        facesContext.responseComplete();     
                        
                    }
                    else if(vouType.equalsIgnoreCase("RCM"))
                    {
                        String result = binding.getResult().toString();
                        int last_index = result.lastIndexOf("/");
                        String path = result.substring(0,last_index+1)+"r_rcm_tax.jasper";
                        System.out.println("----------RCM----Path-----------"+path);
                        InputStream input1 = new FileInputStream(path);
                        
                        Map n = new HashMap();
                        n.put("p_doc_no", pvIter.getCurrentRow().getAttribute("ParentBillNo"));
                        conn = getConnection( );
                        
                        JasperReport design = (JasperReport) JRLoader.loadObject(input);
                        System.out.println("Path : " + input + " -------" + design+" param:"+n);
                        
                        @SuppressWarnings("unchecked")
                        net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                        byte pdf[] = JasperExportManager.exportReportToPdf(print);
                        HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                        response.getOutputStream().write(pdf);
                        response.getOutputStream().flush();
                        response.getOutputStream().close();
                        facesContext.responseComplete();        
                    }
                    }else
                    System.out.println("File Name/File Path not found.");
                    } catch (FileNotFoundException fnfe) {
                    // TODO: Add catch code
                    fnfe.printStackTrace();
                    try {
                        System.out.println("in finally connection closed");
                                conn.close( );
                                conn = null;
                        
                    } catch( SQLException e ) {
                        e.printStackTrace( );
                    }
                    } catch (Exception e) {
                    // TODO: Add catch code
                    e.printStackTrace();
                    try {
                        System.out.println("in finally connection closed");
                                conn.close( );
                                conn = null;
                        
                    } catch( SQLException e1 ) {
                        e1.printStackTrace( );
                    }
                    }finally {
                            try {
                                    System.out.println("in finally connection closed");
                                            conn.close( );
                                            conn = null;
                                    
                            } catch( SQLException e ) {
                                    e.printStackTrace( );
                            }
                    }
    }

    public BindingContainer getBindings()
          {
            return BindingContext.getCurrent().getCurrentBindingsEntry();
          }


    public void setVouType(String vouType) {
        this.vouType = vouType;
    }

    public String getVouType() {
        return vouType;
    }
    
    public Connection getConnection() throws Exception{
            Context ctx = null;
            Connection conn = null;
            ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/ApplicationDBDS");
            conn = ds.getConnection();
            return conn;
        }
}
