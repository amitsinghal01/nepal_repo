package terms.fin.transaction.ui.bean;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import javax.naming.Context;
import javax.naming.InitialContext;

import javax.servlet.http.HttpServletResponse;

import javax.sql.DataSource;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.domain.Date;

import org.apache.myfaces.trinidad.context.RequestContext;

public class SearchVendorPaymentBean {
    private RichTable searchPaymentsTableBinding;
    private String paymentMode = "";


    public SearchVendorPaymentBean() {
    }

    public String createVendorPayment() {
        RequestContext.getCurrentInstance().getPageFlowScope().put("PARAM_MODE", "A");
        return "go_to_page";
    }

    public String viewDocumentAction() {
        System.out.println("Request View Request is:");
        RequestContext.getCurrentInstance().getPageFlowScope().put("PARAM_MODE", "V");
        System.out.println("Request View Request is!");
        return "go_to_page";
    }

    public void deletePopupDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            OperationBinding op = null;
            ADFUtils.findOperation("Delete").execute();
            op = (OperationBinding) ADFUtils.findOperation("Commit");
            Object rst = op.execute();
            System.out.println("Record Delete Successfully");
            if (op.getErrors().isEmpty()) {
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            } else if (!op.getErrors().isEmpty()) {
                OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
                Object rstr = opr.execute();
                FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            }
            AdfFacesContext.getCurrentInstance().addPartialTarget(searchPaymentsTableBinding);
        }
    }

    public void setSearchPaymentsTableBinding(RichTable searchPaymentsTableBinding) {
        this.searchPaymentsTableBinding = searchPaymentsTableBinding;
    }

    public RichTable getSearchPaymentsTableBinding() {
        return searchPaymentsTableBinding;
    }


    //Report Code

    public void downloadJrxml(FacesContext facesContext, OutputStream outputStream) {
        Connection conn=null;
        try {
            String file_name = "";
            String payment_through = "";
            String vouType = null;
            Date vouDate = null;
            Timestamp vouDT = null;
            String str_date = "";
            System.out.println("Payment  Mode===================>" + paymentMode);

            if (paymentMode.equals("B")) {
                file_name = "r_bank_prn.jasper";
            } else if (paymentMode.equals("I")) {
                //file_name="r_jour_prn.jasper";
                file_name = "r_bank_prn.jasper";
            } else if (paymentMode.equals("D")) {
                file_name = "r_payadv.jasper";
            } else if (paymentMode.equals("DT")) {
                file_name = "r_jour_prn.jasper";
            }
            System.out.println("******After Set Value File Name Is******:" + file_name);


            OperationBinding binding = ADFUtils.findOperation("fileNameForPrint");
            binding.getParamsMap().put("fileId", "ERP0000000134");
            binding.execute();
            System.out.println("Binding Result :" + binding.getResult());


            if (binding.getResult() != null) {
                conn = getConnection( );
                String result = binding.getResult().toString();
//                String result ="//home//lenovo//oracle//jasper";
                DCIteratorBinding vp_itr = (DCIteratorBinding) getBindings().get("SearchPaymtsVOIterator");
                if (vp_itr.getCurrentRow().getAttribute("PaymentThrough") != null && paymentMode.equals("B")) {
                    if (vp_itr.getCurrentRow().getAttribute("PaymentThrough").equals("B"))
                        file_name = "r_bank_prn.jasper";
                    if (vp_itr.getCurrentRow().getAttribute("PaymentThrough").equals("C"))
                        file_name = "r_cash_prn.jasper";
                }

                int last_index = result.lastIndexOf("/");
                String path = result.substring(0, last_index + 1) + file_name;
                System.out.println("*******************File Path Is***************:" + path);

                InputStream input = new FileInputStream(path);
                System.out.println("UnitCode                :=" + vp_itr.getCurrentRow().getAttribute("UnitCode"));
                System.out.println("FinTvouchVouNo          :=" +
                                   vp_itr.getCurrentRow().getAttribute("FinTvouchVouNo"));
                System.out.println("FinTvouchVouDate        :=" +
                                   vp_itr.getCurrentRow().getAttribute("FinTvouchVouDate"));
                System.out.println("FinTvouchVouNoUnit      :=" +
                                   vp_itr.getCurrentRow().getAttribute("FinTvouchVouNoUnit"));
                System.out.println("FinTvouchVouDateUnit    :=" +
                                   vp_itr.getCurrentRow().getAttribute("FinTvouchVouDateUnit"));
                System.out.println("PaymentThrough          :=" +
                                   vp_itr.getCurrentRow().getAttribute("PaymentThrough"));
                System.out.println("InterUnit          :=" +
                                   vp_itr.getCurrentRow().getAttribute("InterUnit"));

                Map n = new HashMap();
                if (paymentMode.equals("D")) {
                    n.put("p_doc_no", vp_itr.getCurrentRow().getAttribute("DocNo"));
                    n.put("p_doc_dt", vp_itr.getCurrentRow().getAttribute("DocDate"));
                    n.put("p_unit_cd", vp_itr.getCurrentRow().getAttribute("UnitCode"));
                    
                    JasperReport design = (JasperReport) JRLoader.loadObject(input);
                    System.out.println("Path : " + input + " -------" + design+" param:"+n);
                    
                    @SuppressWarnings("unchecked")
                    net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                    byte pdf[] = JasperExportManager.exportReportToPdf(print);
                    HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                    response.getOutputStream().write(pdf);
                    response.getOutputStream().flush();
                    response.getOutputStream().close();
                    facesContext.responseComplete();    
                } else if (paymentMode.equals("B")) {
                    if (vp_itr.getCurrentRow().getAttribute("FinTvouchVouNo") != null) {
                        vouDT = (Timestamp) vp_itr.getCurrentRow().getAttribute("FinTvouchVouDate");
                        str_date = vouDT.toString();
                        vouDate = new Date(str_date.substring(0, 10));
                        if(vp_itr.getCurrentRow().getAttribute("InterUnit")!=null && vp_itr.getCurrentRow().getAttribute("InterUnit").equals("Y")){
                            vouType="J";
                            }
                        else{
                            vouType=(String)vp_itr.getCurrentRow().getAttribute("PaymentThrough");
                            }
                        System.out.println("vouDate In Case Of B/C====" + vouDate);
                        n.put("p_unit", vp_itr.getCurrentRow().getAttribute("UnitCode"));
                        n.put("p_vou_fr", vp_itr.getCurrentRow().getAttribute("FinTvouchVouNo"));
                        n.put("p_vou_to", vp_itr.getCurrentRow().getAttribute("FinTvouchVouNo"));
                        n.put("p_fr_dt", vouDate);
                        n.put("p_to_dt", vouDate);
                        n.put("p_voutp", vouType);
                        
                        JasperReport design = (JasperReport) JRLoader.loadObject(input);
                        System.out.println("Path : " + input + " -------" + design+" param:"+n);
                        
                        @SuppressWarnings("unchecked")
                        net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                        byte pdf[] = JasperExportManager.exportReportToPdf(print);
                        HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                        response.getOutputStream().write(pdf);
                        response.getOutputStream().flush();
                        response.getOutputStream().close();
                        facesContext.responseComplete();    
                    }
                } else if (paymentMode.equals("I")) {
                    System.out.println("--------------In Case I---------------- ");
                    if (vp_itr.getCurrentRow().getAttribute("FinTvouchVouNoUnit") != null) {
                        vouDT = (Timestamp) vp_itr.getCurrentRow().getAttribute("FinTvouchVouDate");
                        str_date = vouDT.toString();
                        vouDate = new Date(str_date.substring(0, 10));
                        System.out.println("vouDate IN Case Of I====" + vouDate);

                        System.out.println("----------Inter Unit Code-------------" +
                                           vp_itr.getCurrentRow().getAttribute("InterUnitCode"));
                        n.put("p_unit", vp_itr.getCurrentRow().getAttribute("InterUnitCode"));
                        n.put("p_vou_fr", vp_itr.getCurrentRow().getAttribute("FinTvouchVouNoUnit"));
                        n.put("p_vou_to", vp_itr.getCurrentRow().getAttribute("FinTvouchVouNoUnit"));
                        n.put("p_fr_dt", vouDate);
                        n.put("p_to_dt", vouDate);
                        //n.put("p_voutp", "J");
                        n.put("p_voutp", "B");
                        
                        JasperReport design = (JasperReport) JRLoader.loadObject(input);
                        System.out.println("Path : " + input + " -------" + design+" param:"+n);
                        
                        @SuppressWarnings("unchecked")
                        net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                        byte pdf[] = JasperExportManager.exportReportToPdf(print);
                        HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                        response.getOutputStream().write(pdf);
                        response.getOutputStream().flush();
                        response.getOutputStream().close();
                        facesContext.responseComplete();    
                    }
                } else if (paymentMode.equals("DT")) {
                    if (vp_itr.getCurrentRow().getAttribute("FinTvouchVouNoUnit") != null) {
                        vouDT = (Timestamp) vp_itr.getCurrentRow().getAttribute("FinTvouchVouDate");
                        str_date = vouDT.toString();
                        vouDate = new Date(str_date.substring(0, 10));
                        System.out.println("vouDate IN Case Of I====" + vouDate);

                        n.put("p_unit", vp_itr.getCurrentRow().getAttribute("UnitCode"));
                        n.put("p_vou_fr", vp_itr.getCurrentRow().getAttribute("FlcVouNo"));
                        n.put("p_vou_to", vp_itr.getCurrentRow().getAttribute("FlcVouNo"));
                        n.put("p_fr_dt", vp_itr.getCurrentRow().getAttribute("ApprovalDate"));
                        n.put("p_to_dt", vp_itr.getCurrentRow().getAttribute("ApprovalDate"));
                        n.put("p_voutp", "J");
                        
                        JasperReport design = (JasperReport) JRLoader.loadObject(input);
                        System.out.println("Path : " + input + " -------" + design+" param:"+n);
                        
                        @SuppressWarnings("unchecked")
                        net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                        byte pdf[] = JasperExportManager.exportReportToPdf(print);
                        HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                        response.getOutputStream().write(pdf);
                        response.getOutputStream().flush();
                        response.getOutputStream().close();
                        facesContext.responseComplete();    
                    }
                }
            }
        } catch (Exception fnfe) {
                fnfe.printStackTrace();                
                } 
                finally {
                            try {
                                    System.out.println("in finally connection closed");
                                            conn.close( );
                                            conn = null;
                                    
                            } catch( SQLException e ) {
                                    e.printStackTrace( );
                            }
                    }
    }

    public BindingContainer getBindings() {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
    }


    //End Report Code


    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getPaymentMode() {
        return paymentMode;
    }
    public Connection getConnection() throws Exception{
            Context ctx = null;
            Connection conn = null;
            ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/ApplicationDBDS");
            conn = ds.getConnection();
            return conn;
        }
}
