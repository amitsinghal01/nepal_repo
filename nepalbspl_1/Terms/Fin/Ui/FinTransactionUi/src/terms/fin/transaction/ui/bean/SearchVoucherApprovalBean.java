package terms.fin.transaction.ui.bean;

import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import javax.faces.validator.ValidatorException;

import oracle.adf.model.OperationBinding;
import oracle.adf.view.rich.component.rich.RichDialog;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichSelectBooleanCheckbox;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

public class SearchVoucherApprovalBean {
    private RichOutputText vouTypeBinding;
    private RichTable headerTableBinding;
    private RichOutputText vouSeriesBinding;
    private RichOutputText unitCodeBinding;
    private RichOutputText amountBinding;
    private RichSelectBooleanCheckbox approveBinding;
    private RichTable detailTableBinding;
    private RichInputDate approvalDateBinding;

    public SearchVoucherApprovalBean() {
    }

    public void voucherApprovalDL(DialogEvent dialogEvent) {
        OperationBinding op=(OperationBinding)ADFUtils.findOperation("allVoucherApproval");
        op.getParamsMap().put("empCd", ADFUtils.evaluateEL("#{pageFlowScope.empCode}"));
      //  op.getParamsMap().put("empCd","SWE161");
        op.execute();
        System.out.println("After function Call Result:"+op.getResult());
        AdfFacesContext.getCurrentInstance().addPartialTarget(headerTableBinding);
        AdfFacesContext.getCurrentInstance().addPartialTarget(detailTableBinding);
        if(op.getResult()!=null && op.getResult().equals("Y")){
            ADFUtils.findOperation("Commit").execute();
            ADFUtils.showMessage("Voucher Approved Successfully.", 2);
            }
        if(op.getResult()!=null && op.getResult().equals("N")){
            ADFUtils.showMessage("Please select Voucher for Approval.", 1);
            }
    }

    public void selectAllAL(ActionEvent actionEvent) {
        OperationBinding op=(OperationBinding)ADFUtils.findOperation("selectAllVoucherApproval");
        op.getParamsMap().put("emp_code", ADFUtils.evaluateEL("#{pageFlowScope.empCode}"));
       //op.getParamsMap().put("emp_code", "SWE161");
        op.execute();
    }

    public void clearAll(ActionEvent actionEvent) {
        OperationBinding op=(OperationBinding)ADFUtils.findOperation("unselectAllVoucherApproval");
        op.execute();
    }

    public void approveVCL(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(valueChangeEvent!=null){
        System.out.println("Inside Vcl Voucher Type:"+vouTypeBinding.getValue()+" Vou Series: "+vouSeriesBinding.getValue()+" Unit Code: "+unitCodeBinding.getValue());
            OperationBinding op = (OperationBinding)ADFUtils.findOperation("checkApprovalAuthority");
            op.getParamsMap().put("emp_code", ADFUtils.evaluateEL("#{pageFlowScope.empCode}"));
            //op.getParamsMap().put("emp_code", "SWE161");
        if (vouTypeBinding.getValue().equals("B") && vouSeriesBinding.getValue().equals("R")) {
            op.getParamsMap().put("form_name", "FINBR");     
        }
        if (vouTypeBinding.getValue().equals("B") && vouSeriesBinding.getValue().equals("P")) {
            op.getParamsMap().put("form_name", "FINBP");
        }
        if (vouTypeBinding.getValue().equals("C") && vouSeriesBinding.getValue().equals("R")) {
            op.getParamsMap().put("form_name", "FINCR");
        }
        if (vouTypeBinding.getValue().equals("C") && vouSeriesBinding.getValue().equals("P")) {
            op.getParamsMap().put("form_name", "FINCP");
        }
        if (vouTypeBinding.getValue().equals("J")){
            op.getParamsMap().put("form_name", "FINJV");
            }
        if (vouTypeBinding.getValue().equals("R")){
            op.getParamsMap().put("form_name","FINRV");    
            }
            op.getParamsMap().put("unit_code", unitCodeBinding.getValue());
            op.getParamsMap().put("autho_level", "AP");
            op.execute();
            System.out.println("Result after Approval Check: "+op.getResult());
            if(op.getResult()!=null && op.getResult().equals("Y")){
                    OperationBinding op1 = (OperationBinding)ADFUtils.findOperation("checkApprovalStatus");
                    op1.getParamsMap().put("emp_code", ADFUtils.evaluateEL("#{pageFlowScope.empCode}"));
                    //op1.getParamsMap().put("emp_code", "SWE161");
                    if (vouTypeBinding.getValue().equals("B") && vouSeriesBinding.getValue().equals("R")) {
                    op1.getParamsMap().put("form_name", "FINBR");     
                    }
                    if (vouTypeBinding.getValue().equals("B") && vouSeriesBinding.getValue().equals("P")) {
                    op1.getParamsMap().put("form_name", "FINBP");
                    }
                    if (vouTypeBinding.getValue().equals("C") && vouSeriesBinding.getValue().equals("R")) {
                    op1.getParamsMap().put("form_name", "FINCR");
                    }
                    if (vouTypeBinding.getValue().equals("C") && vouSeriesBinding.getValue().equals("P")) {
                    op.getParamsMap().put("form_name", "FINCP");
                    }
                    if (vouTypeBinding.getValue().equals("J")){
                    op1.getParamsMap().put("form_name", "FINJV");
                    }
                    if (vouTypeBinding.getValue().equals("R")){
                    op1.getParamsMap().put("form_name","FINRV");    
                    }
                    op1.getParamsMap().put("unit_code", unitCodeBinding.getValue());
                    op1.getParamsMap().put("autho_level", "AP");
                    op1.getParamsMap().put("amount", amountBinding.getValue());
                    op1.execute();
                    System.out.println("Result after amount Check: "+op1.getResult());
                    if(op1.getResult()!=null && op1.getResult().equals("Y")){
                        if(approvalDateBinding.getValue()!=null){
                            oracle.jbo.domain.Date date=(oracle.jbo.domain.Date)approvalDateBinding.getValue();
                            OperationBinding op2 =(OperationBinding) ADFUtils.findOperation("checkForMonthAndYearVouSeriesForAllVouchers");
                            op2.getParamsMap().put("vou_dt", date);
                            op2.getParamsMap().put("FinYear", ADFUtils.resolveExpression("#{pageFlowScope.finYear}"));
                            //op2.getParamsMap().put("FinYear", "18-19");
                            if (vouTypeBinding.getValue().equals("B") && vouSeriesBinding.getValue().equals("R")) {
                                op2.getParamsMap().put("Vou_Type", "B");
                                op2.getParamsMap().put("Vou_Series", "R"); 
                            }
                            if (vouTypeBinding.getValue().equals("B") && vouSeriesBinding.getValue().equals("P")) {
                                op2.getParamsMap().put("Vou_Type", "B");
                                op2.getParamsMap().put("Vou_Series", "P");
                            }
                            if (vouTypeBinding.getValue().equals("C") && vouSeriesBinding.getValue().equals("R")) {
                                op2.getParamsMap().put("Vou_Type", "C");
                                op2.getParamsMap().put("Vou_Series", "R");

                            }
                            if (vouTypeBinding.getValue().equals("C") && vouSeriesBinding.getValue().equals("P")) {
                                op2.getParamsMap().put("Vou_Type", "C");
                                op2.getParamsMap().put("Vou_Series", "P");
                            }
                            if(vouTypeBinding.getValue().equals("J") ){
                                op2.getParamsMap().put("Vou_Type", "J");
                                op2.getParamsMap().put("Vou_Series", "V");
                                }
                            if(vouTypeBinding.getValue().equals("R") ){
                                op2.getParamsMap().put("Vou_Type", "R");
                                op2.getParamsMap().put("Vou_Series", "V");
                                }
                            op2.getParamsMap().put("Unit_Code", unitCodeBinding.getValue());
                            op2.execute();
                            if (op2.getResult() != null && !op2.getResult().toString().equalsIgnoreCase("Y")) {
                                ADFUtils.showMessage(op2.getResult().toString()+". Please Select Another Date.", 0);
                                approveBinding.setValue(null);

                            }
                            
                        }
                    }
                    if(op1.getResult()!=null && op1.getResult().equals("N")){
                            ADFUtils.showMessage("You don't have Authority to approve this Amount.", 0);
                            approveBinding.setValue(null);
                        }
                    
                }
            else{
                ADFUtils.showMessage("You don't have Authority to approve this Voucher.", 0);
                approveBinding.setValue(null);
                }
        }
        }

    public void setVouSeriesBinding(RichOutputText vouSeriesBinding) {
        this.vouSeriesBinding = vouSeriesBinding;
    }

    public RichOutputText getVouSeriesBinding() {
        return vouSeriesBinding;
    }

    public void setUnitCodeBinding(RichOutputText unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichOutputText getUnitCodeBinding() {
        return unitCodeBinding;
    }


    public void setVouTypeBinding(RichOutputText vouTypeBinding) {
        this.vouTypeBinding = vouTypeBinding;
    }

    public RichOutputText getVouTypeBinding() {
        return vouTypeBinding;
    }

    public void setHeaderTableBinding(RichTable headerTableBinding) {
        this.headerTableBinding = headerTableBinding;
    }

    public RichTable getHeaderTableBinding() {
        return headerTableBinding;
    }

    public void setAmountBinding(RichOutputText amountBinding) {
        this.amountBinding = amountBinding;
    }

    public RichOutputText getAmountBinding() {
        return amountBinding;
    }

    public void setApproveBinding(RichSelectBooleanCheckbox approveBinding) {
        this.approveBinding = approveBinding;
    }

    public RichSelectBooleanCheckbox getApproveBinding() {
        return approveBinding;
    }

    public void setDetailTableBinding(RichTable detailTableBinding) {
        this.detailTableBinding = detailTableBinding;
    }

    public RichTable getDetailTableBinding() {
        return detailTableBinding;
    }

    public void approvalDateVCL(ValueChangeEvent valueChangeEvent) {
        oracle.jbo.domain.Date date=(oracle.jbo.domain.Date)valueChangeEvent.getNewValue();
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        OperationBinding op=(OperationBinding)ADFUtils.findOperation("ApprovalDateVoucherApproval");
        op.getParamsMap().put("val", date);
        op.execute();
    }

    public void setApprovalDateBinding(RichInputDate approvalDateBinding) {
        this.approvalDateBinding = approvalDateBinding;
    }

    public RichInputDate getApprovalDateBinding() {
        return approvalDateBinding;
    }
}
