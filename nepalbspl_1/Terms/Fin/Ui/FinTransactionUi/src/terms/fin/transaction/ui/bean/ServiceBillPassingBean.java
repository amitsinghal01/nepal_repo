package terms.fin.transaction.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import javax.faces.validator.ValidatorException;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.component.rich.output.RichPanelCollection;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.domain.Date;

import org.apache.myfaces.trinidad.event.DisclosureEvent;

import terms.fin.transaction.model.view.BankRecoDetailVORowImpl;
import terms.fin.transaction.model.view.ServiceBillContractOrderVORowImpl;

public class ServiceBillPassingBean {
    private RichButton headerEditBinding;
    private RichButton detailcreateBinding;
    private RichButton detaildeleteBinding;
    private RichOutputText outputTextBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichTable detailTableBinding;
    private RichPopup contractPopupBinding;
    private RichSelectOneChoice contractYNBinding;
    private RichInputComboboxListOfValues contractOrderNoBinding;
    private RichShowDetailItem showdetailTabBinding;
    private RichShowDetailItem showHeaderTabBinding;
    private RichSelectOneChoice billTypeBinding;
    private RichInputText billNoBinding;
    private RichInputDate billEntryDateBinding;
    private RichInputComboboxListOfValues venderCodeBinding;
    private RichInputText glCdBinding;
    private RichInputText vendorTypeBinding;
    private RichInputText vendorBillNoBinding;
    private RichInputDate vendorBillDateBinding;
    private RichInputComboboxListOfValues itemCodeBinding;
    private RichInputText quantityBinding;
    private RichInputText rateBinding;
    private RichInputText valueBinding;
    private RichInputText dspBillAmtBinding;
    private RichShowDetailItem summaryTabBinding;
    private RichInputText basicAmtBinding;
    private RichInputText billAmtBinding;
    private RichInputText mtlCostBinding;
    private RichInputText appMtlCostBinding;
    private RichInputText igstCostBinding;
    private RichInputText appIgstCostBinding;
    private RichInputText cgstCostBinding;
    private RichInputText appCgstCostBinding;
    private RichInputText sgstCostBinding;
    private RichInputText appSgstCostBinding;
    private RichInputText apprOthersBinding;
    private RichInputText netTotalBinding;
    private RichInputText apprTotalBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputDate dueDateBinding;
    private RichShowDetailItem tdsTabBinding;
    private RichInputText otherDeducBinding;
    private RichInputText tdsGlCodeBinding;
    private RichInputText tdsPerBinding;
    private RichInputText billhsnCodeBinding;
    private RichInputText sgstPerBinding;
    private RichInputText cgstPerBinding;
    private RichInputText igstBinding;
    private RichInputText igstAmtBinding;
    private RichInputText cgstAmtBinding;
    private RichInputText sgstAmtBinding;
    private RichInputText paymentAmtBinding;
    private RichInputText tdsAmountBinding;
    private RichInputText approvedBillAmtBinding;
    private RichInputText ecessAmtBinding;
    private RichOutputText totalValueBinding;
    private RichInputComboboxListOfValues tdsCodeBinding;
    private RichInputText roundoffBinding;
    private RichInputText tdsAmt1Binding;
    private RichInputDate tdsPaymentDateBinding;
    private RichInputComboboxListOfValues approvedByBinding;
    private RichButton saveButtonBinding;
    private RichButton saveandcloseButtonBinding;
    private RichInputComboboxListOfValues costCenterBinding;
    private RichInputComboboxListOfValues verifiedByBinding;
    private RichInputComboboxListOfValues gstCodeBinding;
    private RichInputText debitAmountBinding;
    private RichInputText creditAmountBinding;
    private RichInputText diffAmountBinding;
    private RichInputText vouNo1Binding;
    private RichInputText vouNoBinding;
    private RichTable billPvTableBinding;
    private RichPopup billJvPopupBinding;
    private RichSelectOneChoice gstExemptedBinding;
    private RichSelectOneChoice gstRcmApplyBinding;
    private RichInputText editValueBinding;
    private RichInputComboboxListOfValues detailGlCodeBinding;
    private String tabflag="N";
    private RichButton billVouDeleteButtonBinding;
    private RichButton billVouCreateButtonBinding;
    private RichInputDate approvalDateBinding;
    private String flag="N";
    private RichInputText parentBillNoBinding;
    private RichShowDetailItem billInvoiceTabBinding;
    private RichPanelCollection invoiceTableBinding;
    private RichTable contractTableBinding;
    private RichButton contractButtonBinding;
    private RichInputComboboxListOfValues glCodeBinding;
    private RichSelectOneChoice interPartyTypeBinding;
    private RichInputText contractAmdNoBinding;
    private RichInputDate contractDateBinding;
    private RichInputDate contractAmdDateBinding;
    private RichInputText amountBinding;
    private RichInputText contractNoBinding;
    private RichInputText jobDetailBinding;
    private RichSelectOneChoice drcrBillPvBinding;
    private RichInputText amtBillPvBinding;
    private RichButton invoiceDeleteBtnBinding;
    private RichButton invoiceCreateBtnBinding;
    private RichInputText venNameBinding;
    private RichInputText gstRegdNoBinding;
    private RichShowDetailItem billLocationAllocTabBinding;
    private RichTable allocationTableBinding;
    private RichInputText narrationBinding;
    private RichInputComboboxListOfValues locationCodeBinding;


    public ServiceBillPassingBean() {
    }

    public void saveBillAL(ActionEvent actionEvent) {
        
    if(tabflag.equalsIgnoreCase("Y"))
    {
        String voucher_no="";
        BigDecimal diff_amount=new BigDecimal(0);
        
      //  ADFUtils.findOperation("insertDataDetailToHeader").execute();
        if (getTdsCodeBinding().getValue() != null) {
            BigDecimal payAmt = (BigDecimal) getPaymentAmtBinding().getValue();
            OperationBinding op = ADFUtils.findOperation("TdsCalculationForSB");
            op.getParamsMap().put("totalAmt", payAmt);
            op.execute();
        }
        if ((Long) ADFUtils.evaluateEL("#{bindings.ServiceBillPassingDetailVO1Iterator.estimatedRowCount}") == 0) {
            ADFUtils.showMessage(" Fill Item Details", 0);
        } 
     else
     {
        diff_amount=(BigDecimal) (diffAmountBinding.getValue()!=null?diffAmountBinding.getValue():new BigDecimal(0));
      if(diff_amount.compareTo(new BigDecimal(0))==0)
      {

     OperationBinding op1 = ADFUtils.findOperation("checkApprovedServiceBillStatus");
     op1.execute();
     Object rst = op1.execute();
     if (rst != null && rst.toString().trim().length() > 0 && rst.toString().equalsIgnoreCase("N"))
     {
        System.out.println("+++++++++++++++++ NNNNNNNNNNNNNNN ++++++++++++++++++++");
        if(approvedByBinding.getValue()!=null && verifiedByBinding.getValue()!=null){
            System.out.println("HHHHHHHHHHHHHHHH");
            OperationBinding op = ADFUtils.findOperation("getServiceBillNumber");
            //op.getParamsMap().put("userName", "SWE161");
            op.getParamsMap().put("userName",ADFUtils.resolveExpression("#{pageFlowScope.empCode}"));
            op.execute();
            try{
            voucher_no=vouNumberForMessage();
            }catch(Exception e){
            e.printStackTrace();
            }
            System.out.println("**************RESULT IS*****:"+op.getResult());
            if (op.getResult() != null && op.getResult().toString().equalsIgnoreCase("E")) {
                if (op.getErrors().isEmpty()) {
                    ADFUtils.findOperation("Commit").execute();
                    ADFUtils.showMessage(" Record Updated Successfully"+voucher_no, 2);
                 }
                if(getApprovedByBinding().getValue()!=null)
                {
                ADFUtils.setEL("#{pageFlowScope.mode}","V");
                cevmodecheck();
                AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
                }
            } 
            else if (op.getResult() != null && op.getResult().toString() != ""  && op.getResult().equals("S")){
                    if (op.getErrors().isEmpty()) {
                    System.out.println("+++++ in the save mode+++++++++++");
                    ADFUtils.findOperation("Commit").execute();
                    ADFUtils.showMessage("Record Saved Successfully.Service Bill No. is "+ADFUtils.evaluateEL("#{bindings.BillNo.inputValue}")+""+voucher_no, 2);
                }
                if(getApprovedByBinding().getValue()!=null)
                {
                ADFUtils.setEL("#{pageFlowScope.mode}","V");
                cevmodecheck();
                AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
                }
            }  
//            else if(op.getResult()!=null && op.getResult().equals("VR")){
//                    ADFUtils.showMessage("Problem in Voucher generation.",0);
//            }
          else if(op.getResult()!=null && op.getResult().equals("BR")){
                  ADFUtils.showMessage("Problem in Tds Bill generation.",0);
             }
            }
            else{
                    ADFUtils.showMessage("Bill can not be save without Approval.", 0);
                }
        }
        else if(rst != null && rst.toString().trim().length() > 0 && rst.toString().equalsIgnoreCase("Y"))
        {
            System.out.println("+++++++++++++++++ YYYYYYYYYYYYYYYYY ++++++++++++++++++++");
            if(verifiedByBinding.getValue()!=null)
            {
                 OperationBinding op = ADFUtils.findOperation("getServiceBillNumber");
                 //op.getParamsMap().put("userName", "SWE161");
                 op.getParamsMap().put("userName",ADFUtils.resolveExpression("#{pageFlowScope.empCode}"));
                 op.execute();
                    try{
                    voucher_no=vouNumberForMessage();
                    System.out.println("---->>>>in the message method-----");
                    }catch(Exception e){
                    e.printStackTrace();
                    }
                 if (op.getResult() != null && op.getResult().toString().equalsIgnoreCase("E") ) {
                     if (op.getErrors().isEmpty()) {
                         ADFUtils.findOperation("Commit").execute();
                         //ADFUtils.showMessage(" Record Updated Successfully.", 2);
                         ADFUtils.showMessage(" Record Updated Successfully"+voucher_no, 2);
                     }
                     if(getApprovedByBinding().getValue()!=null)
                     {
                     ADFUtils.setEL("#{pageFlowScope.mode}","V");
                     cevmodecheck();
                     AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
                     }
                 } 
                 else if (op.getResult() != null && op.getResult().toString() != ""  && op.getResult().equals("S")){
                    if (op.getErrors().isEmpty()) 
                    {
                         System.out.println("+++++ When Y+++ save mode+++++++++++");
                         ADFUtils.findOperation("Commit").execute();
ADFUtils.showMessage("Record Saved Successfully.Service Bill No. is "+ADFUtils.evaluateEL("#{bindings.BillNo.inputValue}")+""+voucher_no, 2);

                    }
                    if(getApprovedByBinding().getValue()==null)
                    {
                    ADFUtils.setEL("#{pageFlowScope.mode}","V");
                    cevmodecheck();
                    AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
                    }
                }
//                else if(op.getResult()!=null && op.getResult().equals("VR")){
//                         ADFUtils.showMessage("Problem in Voucher generation.",0);
//                }
                else if(op.getResult()!=null && op.getResult().equals("BR")){
                         ADFUtils.showMessage("Problem in Voucher generation.",0);
                }
            }
            else{
                   ADFUtils.showMessage("Bill can not save without Verified.", 0);
                }
        }
        else{
                  ADFUtils.showMessage("You are not authorized maker checker.You can't save record.", 0);
            }
         }
        else
         {
            ADFUtils.showMessage("Debit Credit Amount Not Equals.Please Check", 0);
        }
     }
    }else{
           ADFUtils.showMessage("Please Open Service Bill Summary Tab.", 0);
         }
}

    public String saveAndClose() {

     if(tabflag.equalsIgnoreCase("Y"))
     {
        String voucher_no="";
        BigDecimal diff_amount=new BigDecimal(0);

        if (getTdsCodeBinding().getValue() != null) {
            BigDecimal payAmt = (BigDecimal) getPaymentAmtBinding().getValue();
            OperationBinding op = ADFUtils.findOperation("TdsCalculationForSB");
            op.getParamsMap().put("totalAmt", payAmt);
            op.execute();
        }
        if ((Long) ADFUtils.evaluateEL("#{bindings.ServiceBillPassingDetailVO1Iterator.estimatedRowCount}") == 0) {
            ADFUtils.showMessage(" Fill Item Details", 0);
        } 
        else
        {
        diff_amount=(BigDecimal) (diffAmountBinding.getValue()!=null?diffAmountBinding.getValue():new BigDecimal(0));
        if(diff_amount.compareTo(new BigDecimal(0))==0)
        {

        OperationBinding op1 = ADFUtils.findOperation("checkApprovedServiceBillStatus");
        op1.execute();
        Object rst = op1.execute();
        if (rst != null && rst.toString().trim().length() > 0 && rst.toString().equalsIgnoreCase("N"))
        {
        System.out.println("+++++++++++++++++ NNNNNNNNNNNNNNN ++++++++++++++++++++");
        if(approvedByBinding.getValue()!=null && verifiedByBinding.getValue()!=null){
            System.out.println("HHHHHHHHHHHHHHHH");
            OperationBinding op = ADFUtils.findOperation("getServiceBillNumber");
            //op.getParamsMap().put("userName", "SWE161");
            op.getParamsMap().put("userName",ADFUtils.resolveExpression("#{pageFlowScope.empCode}"));
            op.execute();
            try{
            voucher_no=vouNumberForMessage();
            System.out.println("---->>>>in the message method-----");
            }catch(Exception e){
            e.printStackTrace();
            }
            System.out.println("**************RESULT IS*****:"+op.getResult());
            if (op.getResult() != null && op.getResult().toString().equalsIgnoreCase("E")) {
                if (op.getErrors().isEmpty()) {
                    System.out.println("+++++ in the edit mode+++++++++++");
                    ADFUtils.findOperation("Commit").execute();
                    ADFUtils.showMessage(" Record Updated Successfully"+voucher_no, 2);
                    return "saveandclose";
                 }
            } 
            else if (op.getResult() != null && op.getResult().toString() != ""  && op.getResult().equals("S")){
                    if (op.getErrors().isEmpty()) {
                    System.out.println("+++++ in the save mode+++++++++++");
                    ADFUtils.findOperation("Commit").execute();
                    ADFUtils.showMessage("Record Saved Successfully.Service Bill No. is "+ADFUtils.evaluateEL("#{bindings.BillNo.inputValue}")+""+voucher_no, 2);
                    return "saveandclose";
                }
            }  
//            else if(op.getResult()!=null && op.getResult().equals("VR")){
//                    ADFUtils.showMessage("Problem in Voucher generation.",0);
//                return null;
//            }
                else if(op.getResult()!=null && op.getResult().equals("BR")){
                        ADFUtils.showMessage("Problem in Voucher generation.",0);
                    return null;
                }
            }
            else{
                    ADFUtils.showMessage("Bill can not be save without Approval.", 0);
                    return null;
                }
        }
        else if(rst != null && rst.toString().trim().length() > 0 && rst.toString().equalsIgnoreCase("Y"))
        {
            System.out.println("+++++++++++++++++ YYYYYYYYYYYYYYYYY ++++++++++++++++++++");
            if(verifiedByBinding.getValue()!=null)
            {
                 OperationBinding op = ADFUtils.findOperation("getServiceBillNumber");
                 //op.getParamsMap().put("userName", "SWE161");
                 op.getParamsMap().put("userName",ADFUtils.resolveExpression("#{pageFlowScope.empCode}"));
                 op.execute();
                    try{
                    voucher_no=vouNumberForMessage();
                    System.out.println("---->>>>in the message method-----");
                    }catch(Exception e){
                    e.printStackTrace();
                    }
                 if (op.getResult() != null && op.getResult().toString().equalsIgnoreCase("E") ) {
                     if (op.getErrors().isEmpty()) {
                         System.out.println("+++++ in the edit********** mode+++++++++++");
                         ADFUtils.findOperation("Commit").execute();
                         ADFUtils.showMessage(" Record Updated Successfully"+voucher_no, 2);
                         return "saveandclose";
                     }
                 } 
                 else if (op.getResult() != null && op.getResult().toString() != ""  && op.getResult().equals("S")){
                    if (op.getErrors().isEmpty()) 
                    {
                         System.out.println("+++++ in the ********save********** mode+++++++++++");
                         ADFUtils.findOperation("Commit").execute();
        ADFUtils.showMessage("Record Saved Successfully.Service Bill No. is "+ADFUtils.evaluateEL("#{bindings.BillNo.inputValue}")+""+voucher_no, 2);
                        return "saveandclose";  
                    }
                }
//                else if(op.getResult()!=null && op.getResult().equals("VR")){
//                      ADFUtils.showMessage("Problem in Voucher generation.",0);
//                      return null;
//                }
                else if(op.getResult()!=null && op.getResult().equals("BR")){
                      ADFUtils.showMessage("Problem in Voucher generation.",0);
                      return null;
                }
            }
            else{
                   ADFUtils.showMessage("Bill can not save without Verified.", 0);
                    return null;
                }
        }
        else{
                ADFUtils.showMessage("You are not authorized maker checker.You can't save record.", 0);
                return null;
            }
         }
        else
         {
            ADFUtils.showMessage("Debit Credit Amount Not Equals.Please Check", 0);
            return null;
        }
    }
     }
     else{
         ADFUtils.showMessage("Please Open Service Bill Summary Tab.", 0);
         return null;
         }
        return null;
 }

    public void createDetailAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert").execute();
        ADFUtils.findOperation("setServiceBillNumber").execute();
    }

    public void editButtonAL(ActionEvent actionEvent) {
        OperationBinding op1 = ADFUtils.findOperation("checkApprovedServiceBillStatus");
        op1.execute();
        Object rst = op1.execute();
        System.out.println("rst.toString() from Parameter v10"+rst.toString());
        if (rst != null && rst.toString().trim().length() > 0 && rst.toString().equalsIgnoreCase("N")){
            if(approvedByBinding.getValue()!=null){
                ADFUtils.showMessage("Approved Bill can't be modified.", 2);
                ADFUtils.setEL("#{pageFlowScope.mode}", "V");
            }
//            else{
//                ADFUtils.showMessage("You are not authorized maker checker.You can't save record.", 0);
//                ADFUtils.setEL("#{pageFlowScope.mode}", "V");
//            }
        }           
        else if(rst != null && rst.toString().trim().length() > 0 && rst.toString().equalsIgnoreCase("Y"))
        {
                if(approvedByBinding.getValue()==null)
                {
                    cevmodecheck();
                }
                else{
                    ADFUtils.showMessage("Approved Bill can't be modified.", 2);
                    ADFUtils.setEL("#{pageFlowScope.mode}", "V");
                }
        }
//                OperationBinding op2 = ADFUtils.findOperation("checkServiceBillAutho");
////            op2.getParamsMap().put("ename", "Rajat");
////            op2.getParamsMap().put("empCd", "SWE161");
//                op2.getParamsMap().put("ename", ADFUtils.resolveExpression("#{pageFlowScope.userName}"));
//                op2.getParamsMap().put("empCd", ADFUtils.resolveExpression("#{pageFlowScope.empCode}"));
//                Object rst1 = op2.execute();
//        if(rst1 != null && rst1.toString().trim().length() > 0 && rst1.toString().equalsIgnoreCase("Y") 
//                && approvedByBinding.getValue()==null){
 //               cevmodecheck();
//        }
//        else if(rst1 != null && rst1.toString().trim().length() > 0 && rst1.toString().equalsIgnoreCase("N")){
//                System.out.println("------ When authority return NNNNN------");
//                ADFUtils.showMessage("You have not Authority to Modified or Approve this Bill.", 0);
//                ADFUtils.setEL("#{pageFlowScope.mode}", "V");
//                }        
//        else{
//                ADFUtils.showMessage("Approved Bill can not be modified.", 2);
//                ADFUtils.setEL("#{pageFlowScope.mode}", "V");
//            }
//        }else{
//                //System.out.println("------ when there is no check----YN-");
//                ADFUtils.showMessage("You have not Authority to Modified or Approve this Bill.", 0);
//                ADFUtils.setEL("#{pageFlowScope.mode}", "V");
//        }
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setDetailcreateBinding(RichButton detailcreateBinding) {
        this.detailcreateBinding = detailcreateBinding;
    }

    public RichButton getDetailcreateBinding() {
        return detailcreateBinding;
    }

    public void setDetaildeleteBinding(RichButton detaildeleteBinding) {
        this.detaildeleteBinding = detaildeleteBinding;
    }

    public RichButton getDetaildeleteBinding() {
        return detaildeleteBinding;
    }

    public void deleteDetailDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete").execute();
            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
        }
        if ((Long) ADFUtils.evaluateEL("#{bindings.ServiceBillPassingDetailVO1Iterator.estimatedRowCount}")>0)
        {
//            ADFUtils.findOperation("itemCodeDuplicateServiceBill").execute();
//            ADFUtils.findOperation("getGstHsnCodeForItem").execute();
//            ADFUtils.findOperation("GSTCalculateValue").execute();
            
            ADFUtils.findOperation("clearTDSValue").execute();
            ADFUtils.findOperation("tdsValueSet").execute();
            ADFUtils.findOperation("claerRoundOffValue").execute();
            ADFUtils.findOperation("insertDataDetailToHeader").execute();
            ADFUtils.findOperation("populateVoucherDetails").execute();
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(detailTableBinding);
    }

    public void setOutputTextBinding(RichOutputText outputTextBinding) {
        this.outputTextBinding = outputTextBinding;
    }

    public RichOutputText getOutputTextBinding() {
        cevmodecheck();
        return outputTextBinding;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }

    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }

    public void cevModeDisableComponent(String mode) {
        if (mode.equals("E")) {
            
            if(contractYNBinding.getValue().equals("Y")){
              getContractButtonBinding().setDisabled(false);
            }else{
                getContractButtonBinding().setDisabled(true);
            }
            
           // getLocationCodeBinding().setDisabled(true);
            getVenNameBinding().setDisabled(true);
            getJobDetailBinding().setDisabled(true);
            getVouNoBinding().setDisabled(true);
            getVouNo1Binding().setDisabled(true);
            getParentBillNoBinding().setDisabled(true);
            getInterPartyTypeBinding().setDisabled(true);
            getCostCenterBinding().setDisabled(true);
            getBillEntryDateBinding().setDisabled(true);
            getBillVouDeleteButtonBinding().setDisabled(true);
            getBillVouCreateButtonBinding().setDisabled(true);
            //getApprovalDateBinding().setDisabled(true);
            getDiffAmountBinding().setDisabled(true);
            getCreditAmountBinding().setDisabled(true);
            getDebitAmountBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            getVendorBillNoBinding().setDisabled(true);
            getVerifiedByBinding().setDisabled(true);
            getTdsPaymentDateBinding().setDisabled(true);
            getTdsAmt1Binding().setDisabled(true);
            getTdsAmountBinding().setDisabled(true);
            getEcessAmtBinding().setDisabled(true);
            getApprovedBillAmtBinding().setDisabled(true);
            getContractYNBinding().setDisabled(true);
            getVenderCodeBinding().setDisabled(true);
            getVendorBillDateBinding().setDisabled(true);
            if(contractYNBinding.getValue().equals("Y"))
            {
                getDueDateBinding().setDisabled(false);
            }else{
                 getDueDateBinding().setDisabled(true);
            }
            getCgstPerBinding().setDisabled(true);
            getIgstBinding().setDisabled(true);
            getSgstPerBinding().setDisabled(true);
            getBillhsnCodeBinding().setDisabled(true);
            getValueBinding().setDisabled(true);
            getTdsPerBinding().setDisabled(true);
            getTdsGlCodeBinding().setDisabled(true);
            getGlCodeBinding().setDisabled(true);
            getVendorTypeBinding().setDisabled(true);
            getBillNoBinding().setDisabled(true);
            getBillTypeBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
            getApprTotalBinding().setDisabled(true);
            getNetTotalBinding().setDisabled(true);
            getOtherDeducBinding().setDisabled(true);
            getApprOthersBinding().setDisabled(true);
            getAppSgstCostBinding().setDisabled(true);
            getSgstCostBinding().setDisabled(true);
            getAppCgstCostBinding().setDisabled(true);
            getCgstCostBinding().setDisabled(true);
            getAppIgstCostBinding().setDisabled(true);
            getIgstCostBinding().setDisabled(true);
            getAppMtlCostBinding().setDisabled(true);
            getMtlCostBinding().setDisabled(true);
            getBasicAmtBinding().setDisabled(true);
            getBillAmtBinding().setDisabled(true);
            getDspBillAmtBinding().setDisabled(true);
            getDetailcreateBinding().setDisabled(false);
            if(vendorTypeBinding.getValue().equals("V") && !ADFUtils.evaluateEL("#{pageFlowScope.mode}").equals("V")){
            getIgstAmtBinding().setDisabled(true);
            getCgstAmtBinding().setDisabled(false);
            getSgstAmtBinding().setDisabled(false);
            }
         if(vendorTypeBinding.getValue().equals("C") && !ADFUtils.evaluateEL("#{pageFlowScope.mode}").equals("V")){
            getIgstAmtBinding().setDisabled(false);
            getCgstAmtBinding().setDisabled(true);
            getSgstAmtBinding().setDisabled(true);
            } 
         if(vendorTypeBinding.getValue().equals("E") && !ADFUtils.evaluateEL("#{pageFlowScope.mode}").equals("V"))
            {
                if(interPartyTypeBinding.getValue().equals("V") && !ADFUtils.evaluateEL("#{pageFlowScope.mode}").equals("V")){
                getIgstAmtBinding().setDisabled(true);
                getCgstAmtBinding().setDisabled(false);
                getSgstAmtBinding().setDisabled(false);
                }
                else 
                {
                getIgstAmtBinding().setDisabled(false);
                getCgstAmtBinding().setDisabled(true);
                getSgstAmtBinding().setDisabled(true);
                } 
            }
            if(gstExemptedBinding.getValue().equals("Y") && !ADFUtils.evaluateEL("#{pageFlowScope.mode}").equals("V")){
                getGstRcmApplyBinding().setDisabled(true);
            }
            if(gstRcmApplyBinding.getValue().equals("Y") && !ADFUtils.evaluateEL("#{pageFlowScope.mode}").equals("V")){
            getGstExemptedBinding().setDisabled(true);
            } 
            if (contractYNBinding.getValue().equals("Y")&& !ADFUtils.evaluateEL("#{pageFlowScope.mode}").equals("V")) {
                getDetailcreateBinding().setDisabled(true);
                getDetaildeleteBinding().setDisabled(true);
                getItemCodeBinding().setDisabled(true);
//                getQuantityBinding().setDisabled(true);
//                getRateBinding().setDisabled(true);
            }
            
            getAmtBillPvBinding().setDisabled(true);
            getDrcrBillPvBinding().setDisabled(true);
            getContractNoBinding().setDisabled(true);
            getContractAmdDateBinding().setDisabled(true);
            getContractAmdNoBinding().setDisabled(true);
            getContractDateBinding().setDisabled(true);
            getAmountBinding().setDisabled(true);
            getInvoiceDeleteBtnBinding().setDisabled(false);
            getInvoiceCreateBtnBinding().setDisabled(false);
            
            getGstRegdNoBinding().setDisabled(true);
            
        } else if(mode.equals("C")) {

        
            if(contractYNBinding.getValue().equals("N") && !ADFUtils.evaluateEL("#{pageFlowScope.mode}").equals("V"))
            {
                getContractButtonBinding().setDisabled(true);
            }
            
           // getLocationCodeBinding().setDisabled(true);
            getVenNameBinding().setDisabled(true);
            getJobDetailBinding().setDisabled(true);
            getVouNoBinding().setDisabled(true);
            getVouNo1Binding().setDisabled(true);
            getParentBillNoBinding().setDisabled(true);

            getBillVouDeleteButtonBinding().setDisabled(true);
            getBillVouCreateButtonBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            //getApprovalDateBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
            getDiffAmountBinding().setDisabled(true);
            getCreditAmountBinding().setDisabled(true);
            getDebitAmountBinding().setDisabled(true);
            getVerifiedByBinding().setDisabled(true);
            getTdsPaymentDateBinding().setDisabled(true);
            getTdsAmt1Binding().setDisabled(true);
            getTdsAmountBinding().setDisabled(true);
            getEcessAmtBinding().setDisabled(true);
            getApprovedBillAmtBinding().setDisabled(true);
            //getGlCdBinding().setDisabled(true);
            getVendorTypeBinding().setDisabled(true);
            getCgstPerBinding().setDisabled(true);
            getIgstBinding().setDisabled(true);
            getBillTypeBinding().setDisabled(true);
            getSgstPerBinding().setDisabled(true);
            getBillhsnCodeBinding().setDisabled(true);
            getValueBinding().setDisabled(true);
            getBillNoBinding().setDisabled(true);
            getOtherDeducBinding().setDisabled(true);
            getApprTotalBinding().setDisabled(true);
            getTdsPerBinding().setDisabled(true);
            getTdsGlCodeBinding().setDisabled(true);
            getNetTotalBinding().setDisabled(true);
            getApprOthersBinding().setDisabled(true);
            getAppSgstCostBinding().setDisabled(true);
            getSgstCostBinding().setDisabled(true);
            getAppCgstCostBinding().setDisabled(true);
            getCgstCostBinding().setDisabled(true);
            getAppIgstCostBinding().setDisabled(true);
            getIgstCostBinding().setDisabled(true);
            getAppMtlCostBinding().setDisabled(true);
            getMtlCostBinding().setDisabled(true);
            getBasicAmtBinding().setDisabled(true);
            getBillAmtBinding().setDisabled(true);
            getDspBillAmtBinding().setDisabled(true);
            getDetailcreateBinding().setDisabled(false);
            
            getAmtBillPvBinding().setDisabled(true);
            getDrcrBillPvBinding().setDisabled(true);
            getContractNoBinding().setDisabled(true);
            getContractAmdDateBinding().setDisabled(true);
            getContractAmdNoBinding().setDisabled(true);
            getContractDateBinding().setDisabled(true);
            getAmountBinding().setDisabled(true);
            getInvoiceDeleteBtnBinding().setDisabled(false);
            getInvoiceCreateBtnBinding().setDisabled(false);
            
            getGstRegdNoBinding().setDisabled(true);

        } else if (mode.equals("V")){
            if(contractYNBinding.getValue().equals("Y"))
            {
              getContractButtonBinding().setDisabled(false);
            }
            getBillLocationAllocTabBinding().setDisabled(false);
            getInvoiceDeleteBtnBinding().setDisabled(true);
            getInvoiceCreateBtnBinding().setDisabled(true);
            getBillInvoiceTabBinding().setDisabled(false);
            getTdsTabBinding().setDisabled(false);
            getSummaryTabBinding().setDisabled(false);
            getShowHeaderTabBinding().setDisabled(false);
            getShowdetailTabBinding().setDisabled(false);
            getDetailcreateBinding().setDisabled(true);
          //getDetaildeleteBinding().setDisabled(true);
          //getHeaderEditBinding().setDisabled(false);
        }
    }

    public void setDetailTableBinding(RichTable detailTableBinding) {
        this.detailTableBinding = detailTableBinding;
    }

    public RichTable getDetailTableBinding() {
        return detailTableBinding;
    }

    public void setContractPopupBinding(RichPopup contractPopupBinding) {
        this.contractPopupBinding = contractPopupBinding;
    }

    public RichPopup getContractPopupBinding() {
        return contractPopupBinding;
    }

    public void setContractYNBinding(RichSelectOneChoice contractYNBinding) {
        this.contractYNBinding = contractYNBinding;
    }

    public RichSelectOneChoice getContractYNBinding() {
        return contractYNBinding;
    }

    public void contractOrderYnVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce != null) {
            if (contractYNBinding.getValue().equals("Y")) {
//                System.out.println("--------Contract order Binding Value is -----: " + contractYNBinding.getValue());
//                RichPopup.PopupHints hints = new RichPopup.PopupHints();
//                getContractPopupBinding().show(hints);
                getContractButtonBinding().setDisabled(false);
                getDetailcreateBinding().setDisabled(true);
                getDetaildeleteBinding().setDisabled(true);
                getItemCodeBinding().setDisabled(true);
//                getQuantityBinding().setDisabled(true);
//                getRateBinding().setDisabled(true);
                
            }
            else{
                getContractButtonBinding().setDisabled(true);
                getDetailcreateBinding().setDisabled(false);
                getDetaildeleteBinding().setDisabled(false);
                getItemCodeBinding().setDisabled(false);
                getQuantityBinding().setDisabled(false);
                getRateBinding().setDisabled(false);
                }
        }
    }

    public void setContractOrderNoBinding(RichInputComboboxListOfValues contractOrderNoBinding) {
        this.contractOrderNoBinding = contractOrderNoBinding;
    }

    public RichInputComboboxListOfValues getContractOrderNoBinding() {
        return contractOrderNoBinding;
    }

    public void contractpopupDialogListener(DialogEvent dialogEvent) {
//        System.out.println("--------Contract Order No. is -----" + contractOrderNoBinding.getValue());
//        if (dialogEvent.getOutcome().name().equals("ok")) {
//
//            OperationBinding op = ADFUtils.findOperation("itemCodeDuplicateServiceBill");
//            op.getParamsMap().put("joNo", contractOrderNoBinding.getValue());
//            op.execute();
//            ADFUtils.findOperation("itemCodeDuplicateServiceBill").execute();
//            
//            ADFUtils.findOperation("getGstHsnCodeForItem").execute();
//            ADFUtils.findOperation("GSTCalculateValue").execute();
//
//            valueBinding.setDisabled(true);
//           // contractYNBinding.setDisabled(true);
//            venderCodeBinding.setDisabled(true);
//           // glCdBinding.setDisabled(true);
//            vendorTypeBinding.setDisabled(true);
//            vendorBillNoBinding.setDisabled(true);
//            vendorBillDateBinding.setDisabled(true);
//            
//        }
//        AdfFacesContext.getCurrentInstance().addPartialTarget(showHeaderTabBinding);
    }

    public void setShowdetailTabBinding(RichShowDetailItem showdetailTabBinding) {
        this.showdetailTabBinding = showdetailTabBinding;
    }

    public RichShowDetailItem getShowdetailTabBinding() {
        return showdetailTabBinding;
    }

    public void setShowHeaderTabBinding(RichShowDetailItem showHeaderTabBinding) {
        this.showHeaderTabBinding = showHeaderTabBinding;
    }

    public RichShowDetailItem getShowHeaderTabBinding() {
        return showHeaderTabBinding;
    }

    public void setBillTypeBinding(RichSelectOneChoice billTypeBinding) {
        this.billTypeBinding = billTypeBinding;
    }

    public RichSelectOneChoice getBillTypeBinding() {
        return billTypeBinding;
    }

    public void setBillNoBinding(RichInputText billNoBinding) {
        this.billNoBinding = billNoBinding;
    }

    public RichInputText getBillNoBinding() {
        return billNoBinding;
    }

    public void setBillEntryDateBinding(RichInputDate billEntryDateBinding) {
        this.billEntryDateBinding = billEntryDateBinding;
    }

    public RichInputDate getBillEntryDateBinding() {
        return billEntryDateBinding;
    }

    public void setVenderCodeBinding(RichInputComboboxListOfValues venderCodeBinding) {
        this.venderCodeBinding = venderCodeBinding;
    }

    public RichInputComboboxListOfValues getVenderCodeBinding() {
        return venderCodeBinding;
    }

    public void setGlCdBinding(RichInputText glCdBinding) {
        this.glCdBinding = glCdBinding;
    }

    public RichInputText getGlCdBinding() {
        return glCdBinding;
    }

    public void setVendorTypeBinding(RichInputText vendorTypeBinding) {
        this.vendorTypeBinding = vendorTypeBinding;
    }

    public RichInputText getVendorTypeBinding() {
        return vendorTypeBinding;
    }

    public void setVendorBillNoBinding(RichInputText vendorBillNoBinding) {
        this.vendorBillNoBinding = vendorBillNoBinding;
    }

    public RichInputText getVendorBillNoBinding() {
        return vendorBillNoBinding;
    }

    public void setVendorBillDateBinding(RichInputDate vendorBillDateBinding) {
        this.vendorBillDateBinding = vendorBillDateBinding;
    }

    public RichInputDate getVendorBillDateBinding() {
        return vendorBillDateBinding;
    }

    public void setItemCodeBinding(RichInputComboboxListOfValues itemCodeBinding) {
        this.itemCodeBinding = itemCodeBinding;
    }

    public RichInputComboboxListOfValues getItemCodeBinding() {
        return itemCodeBinding;
    }

    public void setQuantityBinding(RichInputText quantityBinding) {
        this.quantityBinding = quantityBinding;
    }

    public RichInputText getQuantityBinding() {
        return quantityBinding;
    }

    public void gstCodeVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce != null) {
            //System.out.println("------ Inside Gst VCL Code for Calculations------->>>");
            ADFUtils.findOperation("getGstHsnCodeForItem").execute();
            ADFUtils.findOperation("GSTCalculateValue").execute();
            ADFUtils.findOperation("clearTDSValue").execute();
            ADFUtils.findOperation("tdsValueSet").execute();
            ADFUtils.findOperation("claerRoundOffValue").execute();
            ADFUtils.findOperation("insertDataDetailToHeader").execute();
            ADFUtils.findOperation("populateVoucherDetails").execute();

            //AdfFacesContext.getCurrentInstance().addPartialTarget(detailTableBinding);
        }
    }

    public void setRateBinding(RichInputText rateBinding) {
        this.rateBinding = rateBinding;
    }

    public RichInputText getRateBinding() {
        return rateBinding;
    }

    public void setValueBinding(RichInputText valueBinding) {
        this.valueBinding = valueBinding;
    }

    public RichInputText getValueBinding() {
        return valueBinding;
    }

    public void rateVCL(ValueChangeEvent vce){
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        ADFUtils.findOperation("getGstHsnCodeForItem").execute();
        ADFUtils.findOperation("GSTCalculateValue").execute();
        ADFUtils.findOperation("clearTDSValue").execute();
        ADFUtils.findOperation("tdsValueSet").execute();
        ADFUtils.findOperation("claerRoundOffValue").execute();
        ADFUtils.findOperation("insertDataDetailToHeader").execute();
        ADFUtils.findOperation("populateVoucherDetails").execute();
        AdfFacesContext.getCurrentInstance().addPartialTarget(rateBinding);
    }

    public void quantityVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce != null){
            if(vce.getNewValue()!=null){
                ADFUtils.findOperation("getGstHsnCodeForItem").execute();
                ADFUtils.findOperation("GSTCalculateValue").execute();
                ADFUtils.findOperation("clearTDSValue").execute();
                ADFUtils.findOperation("tdsValueSet").execute();
                ADFUtils.findOperation("claerRoundOffValue").execute();
                ADFUtils.findOperation("insertDataDetailToHeader").execute();
                ADFUtils.findOperation("populateVoucherDetails").execute();
            }
        }
        //ADFUtils.findOperation("tdsValueSet").execute();
    }

    public void itemCodeVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        String item = (String) vce.getNewValue();
        if (vce != null)
        {
            /* OperationBinding opr=ADFUtils.findOperation("duplicateItemCheckInDetailSB");
            opr.getParamsMap().put("itemCode",vce.getNewValue());
            Object obj=opr.execute();
            if(opr.getResult().equals("N"))
            {
                ADFUtils.showMessage("Item Code Already Exist.",0);
            }  
            else{ */
            
            
            OperationBinding op = ADFUtils.findOperation("getItemCodeDetailForBP");
            op.getParamsMap().put("itemCode", item);
            op.execute();
               //New thing 
            ADFUtils.findOperation("getGstHsnCodeForItem").execute();
            ADFUtils.findOperation("GSTCalculateValue").execute();
                
            ADFUtils.findOperation("clearTDSValue").execute();
            ADFUtils.findOperation("tdsValueSet").execute();
            ADFUtils.findOperation("claerRoundOffValue").execute();
            ADFUtils.findOperation("insertDataDetailToHeader").execute();
            ADFUtils.findOperation("populateVoucherDetails").execute();
            }
            
            
            if ((Long)ADFUtils.evaluateEL("#{bindings.ServiceBillPassingDetailVO1Iterator.estimatedRowCount}") > 0)
            {
                System.out.println("when Detail there is one or more row====");
                //valueBinding.setDisabled(true);
                contractYNBinding.setDisabled(true);
                venderCodeBinding.setDisabled(true);
               
                vendorTypeBinding.setDisabled(true);
                vendorBillNoBinding.setDisabled(true);
                vendorBillDateBinding.setDisabled(true);
                billEntryDateBinding.setDisabled(true);
                costCenterBinding.setDisabled(true);
                dueDateBinding.setDisabled(true);
                
                if(vendorTypeBinding.getValue().equals("E")){
                    glCodeBinding.setDisabled(true);
                    interPartyTypeBinding.setDisabled(true);
                }
            }
        //}
      //AdfFacesContext.getCurrentInstance().addPartialTarget(detailTableBinding);
    }

    public void tabSelectedDL(DisclosureEvent disclosureEvent) {
        if (disclosureEvent.isExpanded()) {
            tabflag="Y";
            if ((Long) ADFUtils.evaluateEL("#{bindings.ServiceBillPassingDetailVO1Iterator.estimatedRowCount}")>0){
                ADFUtils.findOperation("insertDataDetailToHeader").execute();
                BigDecimal payAmt = (BigDecimal) getPaymentAmtBinding().getValue();
                OperationBinding op = ADFUtils.findOperation("TdsCalculationForSB");
                op.getParamsMap().put("totalAmt", payAmt);
                op.execute();
                
                OperationBinding op1 = ADFUtils.findOperation("checkApprovedServiceBillStatus");
                op1.execute();
                Object rst = op1.execute();
                if (rst != null && rst.toString().trim().length() > 0 && rst.toString().equalsIgnoreCase("Y")
                    && ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")){
                    System.out.println("************ MAKER CHECKER STATUS IS ************>> "+rst);
                    approvedByBinding.setDisabled(true);
                    approvalDateBinding.setDisabled(true);
                }
            }
            else{
                    ADFUtils.findOperation("clearHeaderFieldValue").execute();
                    ADFUtils.showMessage("Please Enter any record in the Item Detail tab.", 2);
                    AdfFacesContext.getCurrentInstance().addPartialTarget(tdsTabBinding);
                }
            
            if (contractYNBinding.getValue().equals("Y") && (Long) ADFUtils.evaluateEL("#{bindings.ServiceBillContractOrderVO1Iterator.estimatedRowCount}")>0)
            {
                DCIteratorBinding Dcite=ADFUtils.findIterator("ServiceBillContractOrderVO1Iterator");
                ServiceBillContractOrderVORowImpl row=(ServiceBillContractOrderVORowImpl) Dcite.getCurrentRow();
                System.out.println("Contract OrderNo. ===="+row.getCoagreeContrNo());
                
                System.out.println("the Newwwwww is=====>"+contractOrderNoBinding.getValue());
//                OperationBinding ops = ADFUtils.findOperation("locationWiseAllocDetails");             
//                ops.getParamsMap().put("joBillNo", row.getCoagreeContrNo());
//                ops.execute();
//                System.out.println("Bill Location  METHOD RESULT IS====>"+ops.getResult());
//                if (ops.getResult()!=null && ops.getResult().equals("Y")) 
//                {
//                    System.out.println("Data is populated====");
//                }
            }
        }
    }

    public void setDspBillAmtBinding(RichInputText dspBillAmtBinding) {
        this.dspBillAmtBinding = dspBillAmtBinding;
    }

    public RichInputText getDspBillAmtBinding() {
        return dspBillAmtBinding;
    }

    public void setSummaryTabBinding(RichShowDetailItem summaryTabBinding) {
        this.summaryTabBinding = summaryTabBinding;
    }

    public RichShowDetailItem getSummaryTabBinding() {
        return summaryTabBinding;
    }

    public void setBasicAmtBinding(RichInputText basicAmtBinding) {
        this.basicAmtBinding = basicAmtBinding;
    }

    public RichInputText getBasicAmtBinding() {
        return basicAmtBinding;
    }

    public void setBillAmtBinding(RichInputText billAmtBinding) {
        this.billAmtBinding = billAmtBinding;
    }

    public RichInputText getBillAmtBinding() {
        return billAmtBinding;
    }

    public void setMtlCostBinding(RichInputText mtlCostBinding) {
        this.mtlCostBinding = mtlCostBinding;
    }

    public RichInputText getMtlCostBinding() {
        return mtlCostBinding;
    }

    public void setAppMtlCostBinding(RichInputText appMtlCostBinding) {
        this.appMtlCostBinding = appMtlCostBinding;
    }

    public RichInputText getAppMtlCostBinding() {
        return appMtlCostBinding;
    }

    public void setIgstCostBinding(RichInputText igstCostBinding) {
        this.igstCostBinding = igstCostBinding;
    }

    public RichInputText getIgstCostBinding() {
        return igstCostBinding;
    }

    public void setAppIgstCostBinding(RichInputText appIgstCostBinding) {
        this.appIgstCostBinding = appIgstCostBinding;
    }

    public RichInputText getAppIgstCostBinding() {
        return appIgstCostBinding;
    }

    public void setCgstCostBinding(RichInputText cgstCostBinding) {
        this.cgstCostBinding = cgstCostBinding;
    }

    public RichInputText getCgstCostBinding() {
        return cgstCostBinding;
    }

    public void setAppCgstCostBinding(RichInputText appCgstCostBinding) {
        this.appCgstCostBinding = appCgstCostBinding;
    }

    public RichInputText getAppCgstCostBinding() {
        return appCgstCostBinding;
    }

    public void setSgstCostBinding(RichInputText sgstCostBinding) {
        this.sgstCostBinding = sgstCostBinding;
    }

    public RichInputText getSgstCostBinding() {
        return sgstCostBinding;
    }

    public void setAppSgstCostBinding(RichInputText appSgstCostBinding) {
        this.appSgstCostBinding = appSgstCostBinding;
    }

    public RichInputText getAppSgstCostBinding() {
        return appSgstCostBinding;
    }

    public void setApprOthersBinding(RichInputText apprOthersBinding) {
        this.apprOthersBinding = apprOthersBinding;
    }

    public RichInputText getApprOthersBinding() {
        return apprOthersBinding;
    }

    public void setNetTotalBinding(RichInputText netTotalBinding) {
        this.netTotalBinding = netTotalBinding;
    }

    public RichInputText getNetTotalBinding() {
        return netTotalBinding;
    }

    public void setApprTotalBinding(RichInputText apprTotalBinding) {
        this.apprTotalBinding = apprTotalBinding;
    }

    public RichInputText getApprTotalBinding() {
        return apprTotalBinding;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }
    
    public void itemTabDisclosureDL(DisclosureEvent disclosureEvent) {
        if (disclosureEvent.isExpanded()) {
            tabflag="N";
            
            if(vendorTypeBinding.getValue().equals("V") && !ADFUtils.evaluateEL("#{pageFlowScope.mode}").equals("V")){
            getIgstAmtBinding().setDisabled(true);
            getCgstAmtBinding().setDisabled(false);
            getSgstAmtBinding().setDisabled(false);
            }
        if(vendorTypeBinding.getValue().equals("C") && !ADFUtils.evaluateEL("#{pageFlowScope.mode}").equals("V")){
            getIgstAmtBinding().setDisabled(false);
            getCgstAmtBinding().setDisabled(true);
            getSgstAmtBinding().setDisabled(true);
            }
       if(vendorTypeBinding.getValue().equals("E") && !ADFUtils.evaluateEL("#{pageFlowScope.mode}").equals("V"))
            {
                if(interPartyTypeBinding.getValue().equals("V") && !ADFUtils.evaluateEL("#{pageFlowScope.mode}").equals("V")){
                getIgstAmtBinding().setDisabled(true);
                getCgstAmtBinding().setDisabled(false);
                getSgstAmtBinding().setDisabled(false);
                }
                else
                {
                    getIgstAmtBinding().setDisabled(false);
                    getCgstAmtBinding().setDisabled(true);
                    getSgstAmtBinding().setDisabled(true);
                } 
            }
        if(contractYNBinding.getValue().equals("Y") 
                && (Long) ADFUtils.evaluateEL("#{bindings.ServiceBillContractOrderVO1Iterator.estimatedRowCount}")==0)
           {
                ADFUtils.showMessage("Please Enter any record in Contract Order Detail", 2);
           }
        }
    }

    public void roundOffVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce != null){
            if(vce.getNewValue()!=null){
            BigDecimal Check=(BigDecimal)vce.getNewValue();
//            System.out.println("Compare value = "+Check.compareTo(new BigDecimal(1)));
//            System.out.println("Minus value is:"+Check.compareTo(new BigDecimal(-1)));
            
            if(Check.compareTo(new BigDecimal(1))<1 && Check.compareTo(new BigDecimal(-1))==1)
             ADFUtils.findOperation("roundOffCalc").execute();
             ADFUtils.findOperation("insertDataDetailToHeader").execute();
             }  
            }
        if(roundoffBinding.getValue()==null){
            //System.out.println("++++++++ when round of is null +++++++++++");
            ADFUtils.findOperation("refreshRoundOffGL").execute();
            ADFUtils.findOperation("insertDataDetailToHeader").execute();
            BigDecimal payAmt = (BigDecimal) getPaymentAmtBinding().getValue();
            OperationBinding op = ADFUtils.findOperation("TdsCalculationForSB");
            op.getParamsMap().put("totalAmt", payAmt);
            op.execute();
            
        }
        ADFUtils.findOperation("getTdsVoucherCalculationsSB").execute();
        AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
        AdfFacesContext.getCurrentInstance().addPartialTarget(billPvTableBinding);
    }

    public void vendorCodeVCL(ValueChangeEvent vce){
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        //ADFUtils.findOperation("billDueDate").execute();
//        if(glCodeBinding.getValue()==null){
//            ADFUtils.showMessage("GL Code must be required. Fill Again with GL Code.",0);
//            getShowdetailTabBinding().setDisabled(true);
//            getSummaryTabBinding().setDisabled(true);
//            getTdsTabBinding().setDisabled(true);  
//            getBillInvoiceTabBinding().setDisabled(true);
//        }
//        else{
//            getShowdetailTabBinding().setDisabled(false);
//            getSummaryTabBinding().setDisabled(false);
//            getTdsTabBinding().setDisabled(false);  
//            getBillInvoiceTabBinding().setDisabled(false);
//        }
        if(!vendorTypeBinding.getValue().equals("E")){
            glCodeBinding.setDisabled(true);
        }
        else{
            glCodeBinding.setDisabled(false);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
    }

    public void setDueDateBinding(RichInputDate dueDateBinding) {
        this.dueDateBinding = dueDateBinding;
    }

    public RichInputDate getDueDateBinding() {
        return dueDateBinding;
    }

    public void setTdsTabBinding(RichShowDetailItem tdsTabBinding) {
        this.tdsTabBinding = tdsTabBinding;
    }

    public RichShowDetailItem getTdsTabBinding() {
        return tdsTabBinding;
    }

    public void setOtherDeducBinding(RichInputText otherDeducBinding) {
        this.otherDeducBinding = otherDeducBinding;
    }

    public RichInputText getOtherDeducBinding() {
        return otherDeducBinding;
    }

    public void setTdsGlCodeBinding(RichInputText tdsGlCodeBinding) {
        this.tdsGlCodeBinding = tdsGlCodeBinding;
    }

    public RichInputText getTdsGlCodeBinding() {
        return tdsGlCodeBinding;
    }

    public void setTdsPerBinding(RichInputText tdsPerBinding) {
        this.tdsPerBinding = tdsPerBinding;
    }

    public RichInputText getTdsPerBinding() {
        return tdsPerBinding;
    }
    public void setBillhsnCodeBinding(RichInputText billhsnCodeBinding) {
        this.billhsnCodeBinding = billhsnCodeBinding;
    }

    public RichInputText getBillhsnCodeBinding() {
        return billhsnCodeBinding;
    }

    public void setSgstPerBinding(RichInputText sgstPerBinding) {
        this.sgstPerBinding = sgstPerBinding;
    }

    public RichInputText getSgstPerBinding() {
        return sgstPerBinding;
    }

    public void setCgstPerBinding(RichInputText cgstPerBinding) {
        this.cgstPerBinding = cgstPerBinding;
    }

    public RichInputText getCgstPerBinding() {
        return cgstPerBinding;
    }

    public void setIgstBinding(RichInputText igstBinding) {
        this.igstBinding = igstBinding;
    }

    public RichInputText getIgstBinding() {
        return igstBinding;
    }

    public void setIgstAmtBinding(RichInputText igstAmtBinding) {
        this.igstAmtBinding = igstAmtBinding;
    }

    public RichInputText getIgstAmtBinding() {
        return igstAmtBinding;
    }

    public void setCgstAmtBinding(RichInputText cgstAmtBinding) {
        this.cgstAmtBinding = cgstAmtBinding;
    }

    public RichInputText getCgstAmtBinding() {
        return cgstAmtBinding;
    }

    public void setSgstAmtBinding(RichInputText sgstAmtBinding) {
        this.sgstAmtBinding = sgstAmtBinding;
    }

    public RichInputText getSgstAmtBinding() {
        return sgstAmtBinding;
    }
    
    public void tdsCodeVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce != null) {
            BigDecimal payAmt = (BigDecimal) getPaymentAmtBinding().getValue();
            OperationBinding op = ADFUtils.findOperation("TdsCalculationForSB");
            op.getParamsMap().put("totalAmt", payAmt);
            op.execute();
            
            ADFUtils.findOperation("getTdsVoucherCalculationsSB").execute();
            
            if(roundoffBinding.getValue()!=null){
                   ADFUtils.findOperation("claerRoundOffValue").execute();
               }
        }
    }
    public void paymentAmountVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce != null) {
            BigDecimal value = (BigDecimal) vce.getOldValue();
            BigDecimal totalval = (BigDecimal) getTotalValueBinding().getValue();
            BigDecimal payAmt = (BigDecimal) getPaymentAmtBinding().getValue();
            if (value != totalval && getTdsCodeBinding().getValue() != null) {
                ADFUtils.findOperation("claerRoundOffValue").execute();
                ADFUtils.findOperation("getTdsVoucherCalculationsSB").execute();  
                OperationBinding op = ADFUtils.findOperation("TdsCalculationForSB");
                op.getParamsMap().put("totalAmt", payAmt);
                op.execute();
                
            }
            if(tdsCodeBinding.getValue()!=null)
            {
                System.out.println("when change of Payment Amount while tds code is not null---1");
                ADFUtils.findOperation("getTdsVoucherCalculationsSB").execute();     
            }
            
            if(tdsCodeBinding.getValue()!=null && roundoffBinding.getValue()!=null){
                    ADFUtils.findOperation("claerRoundOffValue").execute();
                }
        }
    }

    public void tdsTabSelectDL(DisclosureEvent disclosureEvent) {
        if (disclosureEvent.isExpanded()) {
            tabflag="N";
            if ((Long) ADFUtils.evaluateEL("#{bindings.ServiceBillPassingDetailVO1Iterator.estimatedRowCount}") > 0) {
                if (getTdsCodeBinding().getValue() == null &&
                    !ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
                    ADFUtils.findOperation("tdsValueSet").execute();
                }
//                if (getTdsCodeBinding().getValue() != null &&
//                    !ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
//                    BigDecimal payAmt = (BigDecimal) getPaymentAmtBinding().getValue();
//                    OperationBinding op = ADFUtils.findOperation("TdsCalculationForSB");
//                    op.getParamsMap().put("totalAmt", payAmt);
//                    op.execute();
//                }
            } else {
                ADFUtils.showMessage("Please Enter any record in the Item Detail tab.", 2);
            }
        }
    }

    public void setPaymentAmtBinding(RichInputText paymentAmtBinding) {
        this.paymentAmtBinding = paymentAmtBinding;
    }

    public RichInputText getPaymentAmtBinding() {
        return paymentAmtBinding;
    }

    public void setTdsAmountBinding(RichInputText tdsAmountBinding) {
        this.tdsAmountBinding = tdsAmountBinding;
    }

    public RichInputText getTdsAmountBinding() {
        return tdsAmountBinding;
    }

    public void setApprovedBillAmtBinding(RichInputText approvedBillAmtBinding) {
        this.approvedBillAmtBinding = approvedBillAmtBinding;
    }

    public RichInputText getApprovedBillAmtBinding() {
        return approvedBillAmtBinding;
    }

    public void setEcessAmtBinding(RichInputText ecessAmtBinding) {
        this.ecessAmtBinding = ecessAmtBinding;
    }

    public RichInputText getEcessAmtBinding() {
        return ecessAmtBinding;
    }
    public void setTotalValueBinding(RichOutputText totalValueBinding) {
        this.totalValueBinding = totalValueBinding;
    }

    public RichOutputText getTotalValueBinding() {
        return totalValueBinding;
    }

    public void setTdsCodeBinding(RichInputComboboxListOfValues tdsCodeBinding) {
        this.tdsCodeBinding = tdsCodeBinding;
    }

    public RichInputComboboxListOfValues getTdsCodeBinding() {
        return tdsCodeBinding;
    }

    public void setRoundoffBinding(RichInputText roundoffBinding) {
        this.roundoffBinding = roundoffBinding;
    }

    public RichInputText getRoundoffBinding() {
        return roundoffBinding;
    }

    public void setTdsAmt1Binding(RichInputText tdsAmt1Binding) {
        this.tdsAmt1Binding = tdsAmt1Binding;
    }

    public RichInputText getTdsAmt1Binding() {
        return tdsAmt1Binding;
    }

    public void setTdsPaymentDateBinding(RichInputDate tdsPaymentDateBinding) {
        this.tdsPaymentDateBinding = tdsPaymentDateBinding;
    }

    public RichInputDate getTdsPaymentDateBinding() {
        return tdsPaymentDateBinding;
    }

    public void setApprovedByBinding(RichInputComboboxListOfValues approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputComboboxListOfValues getApprovedByBinding() {
        return approvedByBinding;
    }

    public void setSaveButtonBinding(RichButton saveButtonBinding) {
        this.saveButtonBinding = saveButtonBinding;
    }

    public RichButton getSaveButtonBinding() {
        return saveButtonBinding;
    }

    public void setSaveandcloseButtonBinding(RichButton saveandcloseButtonBinding) {
        this.saveandcloseButtonBinding = saveandcloseButtonBinding;
    }

    public RichButton getSaveandcloseButtonBinding() {
        return saveandcloseButtonBinding;
    }

    public void setCostCenterBinding(RichInputComboboxListOfValues costCenterBinding) {
        this.costCenterBinding = costCenterBinding;
    }

    public RichInputComboboxListOfValues getCostCenterBinding() {
        return costCenterBinding;
    }

    public void setVerifiedByBinding(RichInputComboboxListOfValues verifiedByBinding) {
        this.verifiedByBinding = verifiedByBinding;
    }

    public RichInputComboboxListOfValues getVerifiedByBinding() {
        return verifiedByBinding;
    }

    public void setGstCodeBinding(RichInputComboboxListOfValues gstCodeBinding) {
        this.gstCodeBinding = gstCodeBinding;
    }
    public RichInputComboboxListOfValues getGstCodeBinding() {
        return gstCodeBinding;
    }
    public void poTypeVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce != null){ 

          if ((Long) ADFUtils.evaluateEL("#{bindings.ServiceBillPassingDetailVO1Iterator.estimatedRowCount}") > 0) {
                ADFUtils.findOperation("clearTDSValue").execute();
                ADFUtils.findOperation("tdsValueSet").execute();
                ADFUtils.findOperation("claerRoundOffValue").execute();
                ADFUtils.findOperation("insertDataDetailToHeader").execute();
                ADFUtils.findOperation("populateVoucherDetails").execute();

                if(gstRcmApplyBinding.getValue().equals("Y") && gstExemptedBinding.getValue().equals("N")){
                    getGstExemptedBinding().setDisabled(true);
                }
                else if(gstRcmApplyBinding.getValue().equals("N")){
                    getGstExemptedBinding().setDisabled(false);
                }
          }else{
                   ADFUtils.showMessage("Before Any Changes,Fill Item Details.", 0);
          }
            AdfFacesContext.getCurrentInstance().addPartialTarget(detailTableBinding);
        }
            AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
        }

    public void sgstVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
          if (vce != null) {    
                if(vce.getNewValue()!=null){
                    ADFUtils.findOperation("clearTDSValue").execute();
                    ADFUtils.findOperation("tdsValueSet").execute();
                    ADFUtils.findOperation("claerRoundOffValue").execute();
                    ADFUtils.findOperation("insertDataDetailToHeader").execute();
                    ADFUtils.findOperation("populateVoucherDetails").execute();
                }
                AdfFacesContext.getCurrentInstance().addPartialTarget(detailTableBinding);
            }
    }

    public void cgstVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
          if (vce != null) {    
                if(vce.getNewValue()!=null){
                    ADFUtils.findOperation("clearTDSValue").execute();
                    ADFUtils.findOperation("tdsValueSet").execute();
                    ADFUtils.findOperation("claerRoundOffValue").execute();
                    ADFUtils.findOperation("insertDataDetailToHeader").execute();
                    ADFUtils.findOperation("populateVoucherDetails").execute();
                }
                AdfFacesContext.getCurrentInstance().addPartialTarget(detailTableBinding);
            }
    }

    public void igstVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
          if (vce != null) {    
                if(vce.getNewValue()!=null){
                    ADFUtils.findOperation("clearTDSValue").execute();
                    ADFUtils.findOperation("tdsValueSet").execute();
                    ADFUtils.findOperation("claerRoundOffValue").execute();
                    ADFUtils.findOperation("insertDataDetailToHeader").execute();
                    ADFUtils.findOperation("populateVoucherDetails").execute();
                }
                AdfFacesContext.getCurrentInstance().addPartialTarget(detailTableBinding);
            }
    }

    public void billDateValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (object != null)
        {
            String Str=object.toString();
            Date JboDate=null;
            java.util.Date UtilDate=null;
            String Flag="Y";
            try
            {
                JboDate=new Date(Str);
                Flag="Y";
            }
            catch(Exception ee)
            {
                System.out.println("Continue");
                UtilDate=new java.util.Date(Str);
                Flag="N";
            }
                System.out.println("JBO Date :"+JboDate);
                System.out.println("Util Date :"+UtilDate);
    
                System.out.println("FLAG IS "+Flag);
                System.out.println("amit 1111 "+ADFUtils.resolveExpression("#{pageFlowScope.finYear}"));
                System.out.println("amit 2222 "+unitCodeBinding.getValue());
                OperationBinding op1 = ADFUtils.findOperation("checkForMonthAndYearVouSeriesForAllVouchers");
            if(Flag.equalsIgnoreCase("Y"))
                op1.getParamsMap().put("vou_dt",JboDate);
            if(Flag.equalsIgnoreCase("N"))
                op1.getParamsMap().put("vou_dt",UtilDate);
             //op1.getParamsMap().put("FinYear","18-19");
            op1.getParamsMap().put("FinYear",ADFUtils.resolveExpression("#{pageFlowScope.finYear}"));
            op1.getParamsMap().put("Vou_Type","J");
            op1.getParamsMap().put("Vou_Series","V");
            op1.getParamsMap().put("Unit_Code",unitCodeBinding.getValue());
            op1.execute();
            System.out.println("GET RESULT:"+op1.getResult());
            if(op1.getResult()!=null && !op1.getResult().equals("Y"))
            {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,op1.getResult().toString(),"Please Select Another Date."));  
            }
        }
    }
    
    public String vouNumberForMessage()
        {
            String tds_voucher_no="";
            String bill_voucher_no="";
            //String rcm_Invoice_no="";
            String message="";
            try
            {
            tds_voucher_no=(String) ADFUtils.evaluateEL("#{bindings.VouNo1.inputValue}");
            bill_voucher_no=(String) ADFUtils.evaluateEL("#{bindings.VouNo.inputValue}");
            //rcm_Invoice_no=(String) ADFUtils.evaluateEL("#{bindings.ParentBillNo.inputValue}");
            
            if(bill_voucher_no!=null)
            {
                message=".And Bill Voucher No. is "+bill_voucher_no;
                System.out.println("Message is :"+message);
            }
            if(tds_voucher_no!=null)
            {
                message=message+".And TDS Voucher No is "+tds_voucher_no;
                System.out.println("Message is :"+message);
            }
//                if(rcm_Invoice_no!=null)
//                {
//                    message=message+".And RCM Invoice No is "+rcm_Invoice_no;
//                    System.out.println("Message is :"+message);
//                }
            }
            catch(Exception ee)
            {
                ee.printStackTrace();
            }    
            return message;
        
        }
    
    public void glCodeVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        ADFUtils.findOperation("clearTDSValue").execute();
        ADFUtils.findOperation("tdsValueSet").execute();
        ADFUtils.findOperation("claerRoundOffValue").execute();
        ADFUtils.findOperation("insertDataDetailToHeader").execute();
        ADFUtils.findOperation("populateVoucherDetails").execute();
        //AdfFacesContext.getCurrentInstance().addPartialTarget(detailTableBinding);
    }

    public void setDebitAmountBinding(RichInputText debitAmountBinding) {
        this.debitAmountBinding = debitAmountBinding;
    }

    public RichInputText getDebitAmountBinding() {
        return debitAmountBinding;
    }

    public void setCreditAmountBinding(RichInputText creditAmountBinding) {
        this.creditAmountBinding = creditAmountBinding;
    }

    public RichInputText getCreditAmountBinding() {
        return creditAmountBinding;
    }

    public void setDiffAmountBinding(RichInputText diffAmountBinding) {
        this.diffAmountBinding = diffAmountBinding;
    }

    public RichInputText getDiffAmountBinding() {
        return diffAmountBinding;
    }

    public void setVouNo1Binding(RichInputText vouNo1Binding) {
        this.vouNo1Binding = vouNo1Binding;
    }

    public RichInputText getVouNo1Binding() {
        return vouNo1Binding;
    }

    public void setVouNoBinding(RichInputText vouNoBinding) {
        this.vouNoBinding = vouNoBinding;
    }

    public RichInputText getVouNoBinding() {
        return vouNoBinding;
    }

    public void createNewGlCodeAL(ActionEvent actionEvent) 
    {
        ADFUtils.findOperation("createBillJvRowServiceBill").execute();
        editValueBinding.setValue(0);
        AdfFacesContext.getCurrentInstance().addPartialTarget(billPvTableBinding);
    }

    public void billVoucherDialogListener(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
          {
            Row selectedRow =(Row) ADFUtils.evaluateEL("#{bindings.BillServiceVoucherVO1Iterator.currentRow}");
            //System.out.println("Value For Selected Row :"+selectedRow.getAttribute("EditValue"));
            Integer check_value=(Integer)(selectedRow.getAttribute("EditValue")!=null?selectedRow.getAttribute("EditValue"):1);
            
            if(check_value==0 || check_value ==2)
            {
                ADFUtils.findOperation("Delete1").execute();
                ADFUtils.showMessage("Record Deleted Successfully.",2); 
            }
            else
            {
                ADFUtils.showMessage("You Can't Delete This Record",0);
                billJvPopupBinding.cancel();
            }
        }
            AdfFacesContext.getCurrentInstance().addPartialTarget(billPvTableBinding);
    }

    public void setBillPvTableBinding(RichTable billPvTableBinding) {
        this.billPvTableBinding = billPvTableBinding;
    }

    public RichTable getBillPvTableBinding() {
        return billPvTableBinding;
    }
    
    public void venCodeValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if(object!=null){
//            if(glCdBinding.getValue()==null){
//                 throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "GL Code must be  required.",
//                                                               "Please enter GL Code."));
//              }
         }
     }

    public void setBillJvPopupBinding(RichPopup billJvPopupBinding) {
        this.billJvPopupBinding = billJvPopupBinding;
    }

    public RichPopup getBillJvPopupBinding() {
        return billJvPopupBinding;
    }

    public void setGstExemptedBinding(RichSelectOneChoice gstExemptedBinding) {
        this.gstExemptedBinding = gstExemptedBinding;
    }

    public RichSelectOneChoice getGstExemptedBinding() {
        return gstExemptedBinding;
    }

    public void GstExemptedVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce != null){ 
        if ((Long) ADFUtils.evaluateEL("#{bindings.ServiceBillPassingDetailVO1Iterator.estimatedRowCount}") > 0) 
        {
              ADFUtils.findOperation("clearTDSValue").execute();
              ADFUtils.findOperation("tdsValueSet").execute();
              ADFUtils.findOperation("claerRoundOffValue").execute();
              ADFUtils.findOperation("insertDataDetailToHeader").execute();
              ADFUtils.findOperation("populateVoucherDetails").execute();

              if(gstRcmApplyBinding.getValue().equals("N") && gstExemptedBinding.getValue().equals("Y")){
                  getGstRcmApplyBinding().setDisabled(true);
              }
              else if(gstExemptedBinding.getValue().equals("N")){
                  getGstRcmApplyBinding().setDisabled(false);
              }
        }else{
                 ADFUtils.showMessage("Before Any Changes,Fill Item Details.", 0);
        }
          AdfFacesContext.getCurrentInstance().addPartialTarget(detailTableBinding);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
    }

    public void setGstRcmApplyBinding(RichSelectOneChoice gstRcmApplyBinding) {
        this.gstRcmApplyBinding = gstRcmApplyBinding;
    }

    public RichSelectOneChoice getGstRcmApplyBinding() {
        return gstRcmApplyBinding;
    }

    public void showheaderTabDL(DisclosureEvent disclosureEvent) {
    if (disclosureEvent.isExpanded()) {
            tabflag="N";
    
//              if(contractYNBinding.getValue().equals("Y") 
//                      && (Long) ADFUtils.evaluateEL("#{bindings.ServiceBillContractOrderVO1Iterator.estimatedRowCount}")==0)
//                 {
//                      ADFUtils.showMessage("Please Enter any record in Contract Order Detail", 2);
//                 }
//              else{
            if ((Long) ADFUtils.evaluateEL("#{bindings.ServiceBillPassingDetailVO1Iterator.estimatedRowCount}")>0){
                ADFUtils.findOperation("insertDataDetailToHeader").execute();
            }
            else{
                ADFUtils.showMessage("Please Enter any record in the Item Detail tab.", 2);
                }
             // }
          }
    }

    public void setEditValueBinding(RichInputText editValueBinding) {
        this.editValueBinding = editValueBinding;
    }

    public RichInputText getEditValueBinding() {
        return editValueBinding;
    }

    public void setDetailGlCodeBinding(RichInputComboboxListOfValues detailGlCodeBinding) {
        this.detailGlCodeBinding = detailGlCodeBinding;
    }

    public RichInputComboboxListOfValues getDetailGlCodeBinding() {
        return detailGlCodeBinding;
    }

    public void costCenterVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(glCodeBinding.getValue()==null){
            ADFUtils.showMessage("GL Code must be required. Fill Again with GL Code.",0);
//            getShowdetailTabBinding().setDisabled(true);
//            getSummaryTabBinding().setDisabled(true);
//            getTdsTabBinding().setDisabled(true);
//            getBillInvoiceTabBinding().setDisabled(true);
//            getBillLocationAllocTabBinding().setDisabled(true);
            Row rr =(Row) ADFUtils.evaluateEL("#{bindings.ServiceBillPassingHeaderVO1Iterator.currentRow}");
            rr.setAttribute("CostCentrCcCd", null);
        }
        else{
            getShowdetailTabBinding().setDisabled(false);
            getSummaryTabBinding().setDisabled(false);
            getTdsTabBinding().setDisabled(false);
            getBillInvoiceTabBinding().setDisabled(false);
            getBillLocationAllocTabBinding().setDisabled(false);
        }
        OperationBinding ops=ADFUtils.findOperation("checkDuplicateVendorBillNo");
        //ops.getParamsMap().put("finyear","18-19");
        ops.getParamsMap().put("finyear",ADFUtils.resolveExpression("#{pageFlowScope.finYear}"));
        Object obj=ops.execute();
        System.out.println("======== Result is ====="+ops.getResult());
        if(ops.getResult().equals("Y") && contractYNBinding.getValue().equals("Y"))
        {
            ADFUtils.showMessage("This Vendor Bill Number is Already Passed.",0);
            //System.out.println("======== inside the contract Order Yes=======");
        }else{
            //System.out.println("Vendor Bill No is not Same----");
        }
//        if(ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")){
//            System.out.println("In the Edit Mode When change the =====");
//            ADFUtils.findOperation("populateVoucherDetails").execute();
//        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
    }

    public void setBillVouDeleteButtonBinding(RichButton billVouDeleteButtonBinding) {
        this.billVouDeleteButtonBinding = billVouDeleteButtonBinding;
    }

    public RichButton getBillVouDeleteButtonBinding() {
        return billVouDeleteButtonBinding;
    }

    public void setBillVouCreateButtonBinding(RichButton billVouCreateButtonBinding) {
        this.billVouCreateButtonBinding = billVouCreateButtonBinding;
    }

    public RichButton getBillVouCreateButtonBinding() {
        return billVouCreateButtonBinding;
    }

    public void VendorBillDateVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce!=null){
            Date newValue = (Date)vce.getNewValue();
            Date oldValue = (Date)vce.getOldValue();
            //System.out.println("NEW VALUE IS >>>> "+newValue+"OLD VALUE IS >>>>:"+oldValue);
            if(vendorBillDateBinding.getValue()!=oldValue){
               Date sb= (Date)vendorBillDateBinding.getValue();
                dueDateBinding.setValue(sb);
            }
            //System.out.println("++++++ New Value is :"+newValue+"+++Old value ++"+oldValue);
            this.getVendorBillDateBinding().resetValue();
    }
    }

    public void approvedByVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce!=null){
           try
             {

                  OperationBinding OprAutho=ADFUtils.findOperation("checkApprovalStatus");
                  OprAutho.getParamsMap().put("emp_code",vce.getNewValue());
                  OprAutho.getParamsMap().put("form_name","FINBS");
                  OprAutho.getParamsMap().put("unit_code",unitCodeBinding.getValue());
                  OprAutho.getParamsMap().put("amount",billAmtBinding.getValue());
                  OprAutho.getParamsMap().put("autho_level","AP");
                  OprAutho.execute();
                  Row row =(Row)ADFUtils.evaluateEL("#{bindings.ServiceBillPassingHeaderVO1Iterator.currentRow}"); 
                  if(OprAutho.getResult().equals("Y"))
                  {
                      String oldValue = (String)vce.getOldValue();
                      if(approvedByBinding.getValue()!=oldValue){
                         Date sb= (Date)billEntryDateBinding.getValue();
                          approvalDateBinding.setValue(sb);
                      }
                      this.getBillEntryDateBinding().resetValue();
                      
                      if(approvedByBinding.getValue()==null){
                          approvalDateBinding.setValue(null);
                      }
                    //  row.setAttribute("ApprovalDate",billEntryDateBinding.getValue());
                  }
                  else
                  {
                      row.setAttribute("FinMauthAuthCode",null);   
                      row.setAttribute("ApprovalDate", null);
                      ADFUtils.showMessage("You Don't Have Permission To Approved This Record.",0);        
                  }
              }
              catch(Exception ee)
              {
              ee.printStackTrace();
              }    
            //String newValue = (String)vce.getNewValue();
//            String oldValue = (String)vce.getOldValue();
//            if(approvedByBinding.getValue()!=oldValue){
//               Date sb= (Date)billEntryDateBinding.getValue();
//                approvalDateBinding.setValue(sb);
//            }
//            this.getBillEntryDateBinding().resetValue();
//            
//            if(approvedByBinding.getValue()==null){
//                approvalDateBinding.setValue(null);
//            }
       }
    }

    public void setApprovalDateBinding(RichInputDate approvalDateBinding) {
        this.approvalDateBinding = approvalDateBinding;
    }

    public RichInputDate getApprovalDateBinding() {
        return approvalDateBinding;
    }

    public void vendorBillNoVCE(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(valueChangeEvent!=null)
        {
            flag="N";
                OperationBinding ops=ADFUtils.findOperation("checkDuplicateVendorBillNo");
                //ops.getParamsMap().put("finyear","18-19");
                ops.getParamsMap().put("finyear",ADFUtils.resolveExpression("#{pageFlowScope.finYear}"));
                Object obj=ops.execute();
             System.out.println("======== Result is ====="+ops.getResult());
                if(ops.getResult().equals("Y"))
                {
                    ADFUtils.showMessage("This Vendor Bill Number is Already Passed.",0);
                }else{
                    //System.out.println("Vendor Bill No is not Same----");
                }
         }
    }

    public void setParentBillNoBinding(RichInputText parentBillNoBinding) {
        this.parentBillNoBinding = parentBillNoBinding;
    }

    public RichInputText getParentBillNoBinding() {
        return parentBillNoBinding;
    }

    public void approvalDateValidate(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (object != null)
        {
            String Str=object.toString();
            Date JboDate=null;
            java.util.Date UtilDate=null;
            String Flag="Y";
            try
            {
                JboDate=new Date(Str);
                Flag="Y";
            }
            catch(Exception ee)
            {
                System.out.println("Continue");
                UtilDate=new java.util.Date(Str);
                Flag="N";
            }
                System.out.println("JBO Date :"+JboDate);
                System.out.println("Util Date :"+UtilDate);
        
                System.out.println("FLAG IS "+Flag);
                OperationBinding op1 = ADFUtils.findOperation("checkForMonthAndYearVouSeriesForAllVouchers");
            if(Flag.equalsIgnoreCase("Y"))
                op1.getParamsMap().put("vou_dt",JboDate);
            if(Flag.equalsIgnoreCase("N"))
                op1.getParamsMap().put("vou_dt",UtilDate);
            //op1.getParamsMap().put("FinYear","18-19");
            op1.getParamsMap().put("FinYear",ADFUtils.resolveExpression("#{pageFlowScope.finYear}"));
            op1.getParamsMap().put("Vou_Type","J");
            op1.getParamsMap().put("Vou_Series","V");
            op1.getParamsMap().put("Unit_Code",unitCodeBinding.getValue());
            op1.execute();
            System.out.println("GET RESULT:"+op1.getResult());
            if(op1.getResult()!=null && !op1.getResult().equals("Y"))
            {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,op1.getResult().toString(),"Please Select Another Date."));  
            }
        }
    }

    public void createBillInvoiceAL(ActionEvent actionEvent) 
    {
        ADFUtils.findOperation("CreateInsert1").execute();
        ADFUtils.findOperation("setServiceBillNumber").execute();
    }

    public void createBillContractOrderAL(ActionEvent actionEvent) 
    {
        ADFUtils.findOperation("CreateInsert2").execute();
        ADFUtils.findOperation("setServiceBillNumber").execute();

        contractYNBinding.setDisabled(true);
        venderCodeBinding.setDisabled(true);
        vendorBillNoBinding.setDisabled(true);
        vendorBillDateBinding.setDisabled(true);
        billEntryDateBinding.setDisabled(true);
        costCenterBinding.setDisabled(true);
            
        AdfFacesContext.getCurrentInstance().addPartialTarget(showHeaderTabBinding);
    }

    public void setBillInvoiceTabBinding(RichShowDetailItem billInvoiceTabBinding) {
        this.billInvoiceTabBinding = billInvoiceTabBinding;
    }

    public RichShowDetailItem getBillInvoiceTabBinding() {
        return billInvoiceTabBinding;
    }

    public void deleteInvoiceDataDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete2").execute();
            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(invoiceTableBinding);
    }

    public void setInvoiceTableBinding(RichPanelCollection invoiceTableBinding) {
        this.invoiceTableBinding = invoiceTableBinding;
    }

    public RichPanelCollection getInvoiceTableBinding() {
        return invoiceTableBinding;
    }

    public void deleteContractDataDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete3").execute();
            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }
        if ((Long) ADFUtils.evaluateEL("#{bindings.ServiceBillContractOrderVO1Iterator.estimatedRowCount}")>0)
        {
            ADFUtils.findOperation("itemCodeDuplicateServiceBill").execute();
            ADFUtils.findOperation("getGstHsnCodeForItem").execute();
            ADFUtils.findOperation("GSTCalculateValue").execute();
            
            ADFUtils.findOperation("clearTDSValue").execute();
            ADFUtils.findOperation("tdsValueSet").execute();
            ADFUtils.findOperation("claerRoundOffValue").execute();
            ADFUtils.findOperation("insertDataDetailToHeader").execute();
            ADFUtils.findOperation("populateVoucherDetails").execute();
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(contractTableBinding);
    }

    public void setContractTableBinding(RichTable contractTableBinding) {
        this.contractTableBinding = contractTableBinding;
    }

    public RichTable getContractTableBinding() {
        return contractTableBinding;
    }

    public void setContractButtonBinding(RichButton contractButtonBinding) {
        this.contractButtonBinding = contractButtonBinding;
    }

    public RichButton getContractButtonBinding() {
        return contractButtonBinding;
    }

    public void contractOrderVCL(ValueChangeEvent valueChangeEvent)
    {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(valueChangeEvent!=null)
        {

            OperationBinding opr=ADFUtils.findOperation("checkDuplicateContractSB");
            opr.getParamsMap().put("contractNo",valueChangeEvent.getNewValue());
            Object obj=opr.execute();
            if(opr.getResult().equals("N"))
            {
                ADFUtils.showMessage("Contract Order Already Exist.",0);
            }  
            else{
            
        //            DCIteratorBinding Dcite=ADFUtils.findIterator("ServiceBillContractOrderVO1Iterator");
        //            ServiceBillContractOrderVORowImpl row=(ServiceBillContractOrderVORowImpl) Dcite.getCurrentRow();
        //            System.out.println("Invoice No is ===="+valueChangeEvent.getNewValue());
        
        //            OperationBinding op = ADFUtils.findOperation("populateContractServiceBillData");
        //            op.getParamsMap().put("joNo", row.getCoagreeContrNo());
        //            op.execute();
            
                    ADFUtils.findOperation("itemCodeDuplicateServiceBill").execute();
             
                    ADFUtils.findOperation("getGstHsnCodeForItem").execute();
                    ADFUtils.findOperation("GSTCalculateValue").execute();
                    ADFUtils.findOperation("clearTDSValue").execute();
                    ADFUtils.findOperation("tdsValueSet").execute();
                    ADFUtils.findOperation("claerRoundOffValue").execute();
                    ADFUtils.findOperation("insertDataDetailToHeader").execute();
                    ADFUtils.findOperation("populateVoucherDetails").execute();
    
        }
        if ((Long) ADFUtils.evaluateEL("#{bindings.ServiceBillContractOrderVO1Iterator.estimatedRowCount}")>0)
        {
//            DCIteratorBinding Dcite=ADFUtils.findIterator("ServiceBillContractOrderVO1Iterator");
//            ServiceBillContractOrderVORowImpl row=(ServiceBillContractOrderVORowImpl) Dcite.getCurrentRow();        
//            vendorBillNoBinding.setValue(row.getVenBillNo());
//            vendorBillDateBinding.setValue(row.getVenBillDate());

            //System.out.println("when there is one or more row====");
            //valueBinding.setDisabled(true);
            contractYNBinding.setDisabled(true);
            venderCodeBinding.setDisabled(true);
           // glCdBinding.setDisabled(true);
            vendorTypeBinding.setDisabled(true);
            vendorBillNoBinding.setDisabled(true);
            vendorBillDateBinding.setDisabled(true);
            billEntryDateBinding.setDisabled(true);
            costCenterBinding.setDisabled(true);
//            dueDateBinding.setDisabled(true);
        }
    }
    AdfFacesContext.getCurrentInstance().addPartialTarget(showHeaderTabBinding);

    }


    public void setGlCodeBinding(RichInputComboboxListOfValues glCodeBinding) {
        this.glCodeBinding = glCodeBinding;
    }

    public RichInputComboboxListOfValues getGlCodeBinding() {
        return glCodeBinding;
    }

    public void glCodeHeaderVCE(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(glCodeBinding.getValue()!=null)
        {
            getShowdetailTabBinding().setDisabled(false);
            getSummaryTabBinding().setDisabled(false);
            getTdsTabBinding().setDisabled(false);
            getBillInvoiceTabBinding().setDisabled(false);
            getBillLocationAllocTabBinding().setDisabled(false);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
    }

    public void setInterPartyTypeBinding(RichSelectOneChoice interPartyTypeBinding) {
        this.interPartyTypeBinding = interPartyTypeBinding;
    }

    public RichSelectOneChoice getInterPartyTypeBinding() {
        return interPartyTypeBinding;
    }

    public void setContractAmdNoBinding(RichInputText contractAmdNoBinding) {
        this.contractAmdNoBinding = contractAmdNoBinding;
    }

    public RichInputText getContractAmdNoBinding() {
        return contractAmdNoBinding;
    }

    public void setContractDateBinding(RichInputDate contractDateBinding) {
        this.contractDateBinding = contractDateBinding;
    }

    public RichInputDate getContractDateBinding() {
        return contractDateBinding;
    }

    public void setContractAmdDateBinding(RichInputDate contractAmdDateBinding) {
        this.contractAmdDateBinding = contractAmdDateBinding;
    }

    public RichInputDate getContractAmdDateBinding() {
        return contractAmdDateBinding;
    }

    public void setAmountBinding(RichInputText amountBinding) {
        this.amountBinding = amountBinding;
    }

    public RichInputText getAmountBinding() {
        return amountBinding;
    }

    public void setContractNoBinding(RichInputText contractNoBinding) {
        this.contractNoBinding = contractNoBinding;
    }

    public RichInputText getContractNoBinding() {
        return contractNoBinding;
    }

    public void setJobDetailBinding(RichInputText jobDetailBinding) {
        this.jobDetailBinding = jobDetailBinding;
    }

    public RichInputText getJobDetailBinding() {
        return jobDetailBinding;
    }

    public void setDrcrBillPvBinding(RichSelectOneChoice drcrBillPvBinding) {
        this.drcrBillPvBinding = drcrBillPvBinding;
    }

    public RichSelectOneChoice getDrcrBillPvBinding() {
        return drcrBillPvBinding;
    }

    public void setAmtBillPvBinding(RichInputText amtBillPvBinding) {
        this.amtBillPvBinding = amtBillPvBinding;
    }

    public RichInputText getAmtBillPvBinding() {
        return amtBillPvBinding;
    }

    public void setInvoiceDeleteBtnBinding(RichButton invoiceDeleteBtnBinding) {
        this.invoiceDeleteBtnBinding = invoiceDeleteBtnBinding;
    }

    public RichButton getInvoiceDeleteBtnBinding() {
        return invoiceDeleteBtnBinding;
    }

    public void setInvoiceCreateBtnBinding(RichButton invoiceCreateBtnBinding) {
        this.invoiceCreateBtnBinding = invoiceCreateBtnBinding;
    }

    public RichButton getInvoiceCreateBtnBinding() {
        return invoiceCreateBtnBinding;
    }

    public void setVenNameBinding(RichInputText venNameBinding) {
        this.venNameBinding = venNameBinding;
    }

    public RichInputText getVenNameBinding() {
        return venNameBinding;
    }

    public void setGstRegdNoBinding(RichInputText gstRegdNoBinding) {
        this.gstRegdNoBinding = gstRegdNoBinding;
    }

    public RichInputText getGstRegdNoBinding() {
        return gstRegdNoBinding;
    }

    public void contractDialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
        {
                try
                {	
                    if(vendorBillNoBinding.getValue()==null)
                    {
                        ADFUtils.findOperation("contractDetailCopyInSB").execute();
                    }
                    
                    DCIteratorBinding Dcite=ADFUtils.findIterator("ServiceBillContractOrderVO1Iterator");
                    ServiceBillContractOrderVORowImpl row=(ServiceBillContractOrderVORowImpl) Dcite.getCurrentRow();
                    
                    OperationBinding ops = ADFUtils.findOperation("locationWiseAllocDetails");             
                    ops.getParamsMap().put("joBillNo", row.getCoagreeContrNo());
                    ops.execute();
                    if (ops.getResult()!=null && ops.getResult().equals("Y")) 
                    {
                    }
                }
                catch(Exception e)
                {
                // TODO: Add catch code
                e.printStackTrace();
                }
            
            try {
                if(narrationBinding.getValue()==null)
                {
                    //System.out.println("------When the value is null---------");
                    ADFUtils.findOperation("narrationValueServiceBill").execute();
                }
                
            } catch (Exception e) {
                // TODO: Add catch code
                e.printStackTrace();
            } 
        }
    }

    public void setBillLocationAllocTabBinding(RichShowDetailItem billLocationAllocTabBinding) {
        this.billLocationAllocTabBinding = billLocationAllocTabBinding;
    }

    public RichShowDetailItem getBillLocationAllocTabBinding() {
        return billLocationAllocTabBinding;
    }

    public void locationWiseAllocationDL(DisclosureEvent disclosureEvent) {
        if (disclosureEvent.isExpanded())
        {
            tabflag="N";
            if (contractYNBinding.getValue().equals("Y") && (Long) ADFUtils.evaluateEL("#{bindings.ServiceBillContractOrderVO1Iterator.estimatedRowCount}")>0)
            {
                DCIteratorBinding Dcite=ADFUtils.findIterator("ServiceBillContractOrderVO1Iterator");
                ServiceBillContractOrderVORowImpl row=(ServiceBillContractOrderVORowImpl) Dcite.getCurrentRow();
                //System.out.println("Contract OrderNo. ===="+row.getCoagreeContrNo());
//                OperationBinding ops = ADFUtils.findOperation("locationWiseAllocDetails");             
//                ops.getParamsMap().put("joBillNo", row.getCoagreeContrNo());
//                ops.execute();
////                System.out.println("Bill Location  METHOD RESULT IS====>"+ops.getResult());
//                if (ops.getResult()!=null && ops.getResult().equals("Y")) 
//                {
//                   // System.out.println("Data is populated====");
//                }
            }
        }
    }

    public void deleteLocationAllocDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete4").execute();
            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(allocationTableBinding);
    }

    public void setAllocationTableBinding(RichTable allocationTableBinding) {
        this.allocationTableBinding = allocationTableBinding;
    }

    public RichTable getAllocationTableBinding() {
        return allocationTableBinding;
    }


    public void setNarrationBinding(RichInputText narrationBinding) {
        this.narrationBinding = narrationBinding;
    }

    public RichInputText getNarrationBinding() {
        return narrationBinding;
    }

    public void subCodeVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce!=null)
        {
            Row row=(Row)ADFUtils.evaluateEL("#{bindings.BillServiceVoucherVO1Iterator.currentRow}");
            String sub_code=(String)vce.getOldValue();
            if(row.getAttribute("DrCrFlag").equals("C"))
            {
                row.setAttribute("SubCode", null);
                ADFUtils.showMessage("You can not change already mapped Sub Code.",0);
                
                if(row.getAttribute("SubCode")==null){
                row.setAttribute("SubCode", sub_code);
                }
            }
            else{
               // System.out.println("---------Else Block----------");
            }
        }
    }

    public void setLocationCodeBinding(RichInputComboboxListOfValues locationCodeBinding) {
        this.locationCodeBinding = locationCodeBinding;
    }

    public RichInputComboboxListOfValues getLocationCodeBinding() {
        return locationCodeBinding;
    }
}
