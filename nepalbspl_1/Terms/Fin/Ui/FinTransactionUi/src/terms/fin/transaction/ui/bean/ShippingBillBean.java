package terms.fin.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

public class ShippingBillBean {
    private RichButton headerEditBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichOutputText outputTextBinding;
    private RichInputText docNoBinding;
    private RichInputDate licDateBinding;
    private RichInputText fcrNoBinding;
    private RichInputDate fcrDateBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputComboboxListOfValues prepareByBinding;
    private RichInputComboboxListOfValues approvedByBinding;
    private RichInputText dutyDrawbackAmountBinding;
    private RichInputText bindMeisval;
    private RichInputComboboxListOfValues licNoBinding;

    public ShippingBillBean() {
    }

    public void saveAL(ActionEvent actionEvent) {
        
        if(prepareByBinding.getValue()!=null)
        {
        OperationBinding op = ADFUtils.findOperation("generateShippingBillNo");
        op.execute();
        System.out.println("Result is===> "+op.getResult());
        if(op.getResult() != null && op.getResult().toString().equalsIgnoreCase("E"))
        {
            ADFUtils.findOperation("Commit").execute();
            ADFUtils.showMessage(" Record Updated Successfully", 2);
            
            if(!docNoBinding.getValue().equals("."))
            {
                  ADFUtils.setEL("#{pageFlowScope.mode}","V");
                  cevmodecheck();
                  AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
            }
        }
        else if (op.getResult() != null && op.getResult().toString() != ""  && op.getResult().equals("S"))
        {
            System.out.println("+++++ in the save mode+++++++++++");
            ADFUtils.findOperation("Commit").execute();
            ADFUtils.showMessage("Record Saved Successfully.Shipping Bill No.is "+ADFUtils.evaluateEL("#{bindings.DocNo.inputValue}"), 2);
            
            if(!docNoBinding.getValue().equals("."))
            {
                  ADFUtils.setEL("#{pageFlowScope.mode}","V");
                  cevmodecheck();
                  AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
            }
        }
        else if(op.getResult() != null && op.getResult().toString().equalsIgnoreCase("N"))
        {
            ADFUtils.showMessage("Shipping Bill No. could not be generated. Try Again !!", 0);
        }
        }else{
            ADFUtils.showMessage("Prepared By is required.", 0);
        }
    }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setOutputTextBinding(RichOutputText outputTextBinding) {
        this.outputTextBinding = outputTextBinding;
    }

    public RichOutputText getOutputTextBinding() {
        cevmodecheck();
        return outputTextBinding;
    }
    
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }

    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }

    public void cevModeDisableComponent(String mode) {
        if (mode.equals("E")) 
        {
        
        } 
        else if(mode.equals("C")) {
//      getDocNoBinding().setDisabled(true);
//            getDocNoBinding().setDisabled(true);
            
         
           
        } else if (mode.equals("V"))
        {

        }
    }

    public void setDocNoBinding(RichInputText docNoBinding) {
        this.docNoBinding = docNoBinding;
    }

    public RichInputText getDocNoBinding() {
        return docNoBinding;
    }

    public String saveAndCloseAL() 
    {
        if(prepareByBinding.getValue()!=null){
        OperationBinding op = ADFUtils.findOperation("generateShippingBillNo");
        op.execute();
        System.out.println("Result is===> "+op.getResult());
        if(op.getResult() != null && op.getResult().toString().equalsIgnoreCase("E"))
        {
            ADFUtils.findOperation("Commit").execute();
            ADFUtils.showMessage(" Record Updated Successfully", 2);
            return "saveAndClose";  

        }
        else if (op.getResult() != null && op.getResult().toString() != ""  && op.getResult().equals("S"))
        {
            System.out.println("+++++ in the save mode+++++++++++");
            ADFUtils.findOperation("Commit").execute();
            ADFUtils.showMessage("Record Saved Successfully.Shipping Bill No.is "+ADFUtils.evaluateEL("#{bindings.DocNo.inputValue}"), 2);
            return "saveAndClose";  
        }
        else if(op.getResult() != null && op.getResult().toString().equalsIgnoreCase("N"))
        {
            ADFUtils.showMessage("Shipping Bill No. could not be generated. Try Again !!", 0);
            return null;
        }
        }else{
            ADFUtils.showMessage("Prepared By is required.", 0);
            return null;
        }
        return null;
    }

    public void setLicDateBinding(RichInputDate licDateBinding) {
        this.licDateBinding = licDateBinding;
    }

    public RichInputDate getLicDateBinding() {
        return licDateBinding;
    }

    public void setFcrNoBinding(RichInputText fcrNoBinding) {
        this.fcrNoBinding = fcrNoBinding;
    }

    public RichInputText getFcrNoBinding() {
        return fcrNoBinding;
    }

    public void setFcrDateBinding(RichInputDate fcrDateBinding) {
        this.fcrDateBinding = fcrDateBinding;
    }

    public RichInputDate getFcrDateBinding() {
        return fcrDateBinding;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setPrepareByBinding(RichInputComboboxListOfValues prepareByBinding) {
        this.prepareByBinding = prepareByBinding;
    }

    public RichInputComboboxListOfValues getPrepareByBinding() {
        return prepareByBinding;
    }

    public void setApprovedByBinding(RichInputComboboxListOfValues approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputComboboxListOfValues getApprovedByBinding() {
        return approvedByBinding;
    }

    public void setDutyDrawbackAmountBinding(RichInputText dutyDrawbackAmountBinding) {
        this.dutyDrawbackAmountBinding = dutyDrawbackAmountBinding;
    }

    public RichInputText getDutyDrawbackAmountBinding() {
        return dutyDrawbackAmountBinding;
    }

    public void setBindMeisval(RichInputText bindMeisval) {
        this.bindMeisval = bindMeisval;
    }

    public RichInputText getBindMeisval() {
        return bindMeisval;
    }

    public void DocTypeVCE(ValueChangeEvent vce) {
        if(vce.getNewValue().equals("Dr")){
          getLicNoBinding().setDisabled(true);
        }
        else {
            getLicNoBinding().setDisabled(false);
        }
}

    public void setLicNoBinding(RichInputComboboxListOfValues licNoBinding) {
        this.licNoBinding = licNoBinding;
    }

    public RichInputComboboxListOfValues getLicNoBinding() {
        return licNoBinding;
    }
}
