
package terms.fin.transaction.ui.bean;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;

import java.sql.Connection;

import java.sql.SQLException;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.application.ViewHandler;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.naming.Context;
import javax.naming.InitialContext;

import javax.servlet.http.HttpServletResponse;

import javax.sql.DataSource;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCDataControl;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import terms.fin.transaction.model.applicationModule.FinTransactionAMImpl;


public class SubLedgerCallingBean {
    private RichInputText unitNameBinding;
    private RichInputDate strDt;
    private RichInputDate endDateReset;

    public SubLedgerCallingBean() {
    }

    public void setUnitNameBinding(RichInputText unitNameBinding) {
        this.unitNameBinding = unitNameBinding;
    }

    public RichInputText getUnitNameBinding() {
        return unitNameBinding;
    }
    public BindingContainer getBindings()
      {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
      }
    
    public void downloadJrxml(FacesContext facesContext, OutputStream outputStream) {
        Connection conn=null;
        try {
            OperationBinding binding = ADFUtils.findOperation("fileNameForPrint");
            binding.getParamsMap().put("fileId","ERP0000000176");
            binding.execute();
            System.out.println("Binding Result :"+binding.getResult());
            if(binding.getResult() != null){ 
                InputStream input = new FileInputStream(binding.getResult().toString());                
                DCIteratorBinding pvIter = (DCIteratorBinding) getBindings().get("SubLedgerCallingVVO1Iterator");
                oracle.jbo.domain.Date fromDate =(oracle.jbo.domain.Date)pvIter.getCurrentRow().getAttribute("StartDate");
                oracle.jbo.domain.Date toDate =(oracle.jbo.domain.Date)pvIter.getCurrentRow().getAttribute("EndDate");
                String unitCode = (String)pvIter.getCurrentRow().getAttribute("UnitCode");
                String subCodeFr = (String)pvIter.getCurrentRow().getAttribute("AccountCodeFrom");
                String subCodeTo = (String)pvIter.getCurrentRow().getAttribute("AccountCodeTo");
                String glCode = (String)pvIter.getCurrentRow().getAttribute("GlHeads");
                String sId=oracle.jbo.server.uniqueid.UniqueIdHelper.getNextId().toString();
                System.out.println("Parameters :- fromDate:"+fromDate+" ||UNIT CODE:"+unitCode+"|| toDate:"+toDate+" Gl Code: "+glCode+" subCodeFr: "+subCodeFr+" subCodeTo: "+subCodeTo+" SID: "+sId);                
                OperationBinding op=ADFUtils.findOperation("SubLedgerCallingProc");
                op.getParamsMap().put("glCd", glCode);
                op.getParamsMap().put("unitCd", unitCode);
                op.getParamsMap().put("sId", sId);
                op.getParamsMap().put("frDate", fromDate);
                op.getParamsMap().put("toDate", toDate);
                op.getParamsMap().put("subCodeFr", subCodeFr);
                op.getParamsMap().put("subCodeTo", subCodeTo);
                op.execute();
                Map n = new HashMap();
                n.put("p_unit", unitCode);
                n.put("p_fdt", fromDate);
                n.put("p_tdt",toDate);
                n.put("p_gl", glCode);
                n.put("p_sid", sId);
                n.put("p_led_fr", subCodeFr);
                n.put("p_led_to", subCodeTo);
                conn = getConnection( );
                
                JasperReport design = (JasperReport) JRLoader.loadObject(input);
                System.out.println("Path : " + input + " -------" + design+" param:"+n);
                
                @SuppressWarnings("unchecked")
                net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                byte pdf[] = JasperExportManager.exportReportToPdf(print);
                HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                response.getOutputStream().write(pdf);
                response.getOutputStream().flush();
                response.getOutputStream().close();
                facesContext.responseComplete();
                }else
                System.out.println("File Name/File Path not found.");
                } catch (FileNotFoundException fnfe) {
                // TODO: Add catch code
                fnfe.printStackTrace();
                try {
                    System.out.println("in finally connection closed");
                            conn.close( );
                            conn = null;
                    
                } catch( SQLException e ) {
                    e.printStackTrace( );
                }
                } catch (Exception e) {
                // TODO: Add catch code
                e.printStackTrace();
                try {
                    System.out.println("in finally connection closed");
                            conn.close( );
                            conn = null;
                    
                } catch( SQLException e1 ) {
                    e1.printStackTrace( );
                }
                }finally {
                        try {
                                System.out.println("in finally connection closed");
                                        conn.close( );
                                        conn = null;
                                
                        } catch( SQLException e ) {
                                e.printStackTrace( );
                        }
                }
    }
    
    
    public Connection getConnection() throws Exception{
            Context ctx = null;
            Connection conn = null;
            ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/ApplicationDBDS");
            conn = ds.getConnection();
            return conn;
        }

    //Apoorv's method to velidate endate
    public void onChangeStartDate(ValueChangeEvent valueChangeEvent) {
            if (valueChangeEvent.getNewValue() == null || valueChangeEvent.getNewValue() == "") {
            } else {
                oracle.jbo.domain.Date std = (oracle.jbo.domain.Date) valueChangeEvent.getNewValue();
                BindingContext bindingContext = BindingContext.getCurrent();
                DCDataControl dc = bindingContext.findDataControl("FinTransactionAMDataControl");

                FinTransactionAMImpl appM = (FinTransactionAMImpl) dc.getDataProvider();
                String Date = appM.startDate().toString();
              //  System.out.println("start Date Value ---" + Date);
                oracle.jbo.domain.Date jboDate1 = new oracle.jbo.domain.Date(Date);
                oracle.jbo.domain.Date cmpd = jboDate1;


                if (Date != null) {
                    if (std.compareTo(cmpd) == -1) {


                       // System.out.println("You Enter date smaller then 16-07-21");
                        ADFUtils.showMessage("Please Enter date Between Financial Year", 0);
                        strDt.setValue(null);
                        AdfFacesContext adfFacesContext = AdfFacesContext.getCurrentInstance();
                        adfFacesContext.addPartialTarget(strDt);

                    }


                }

            }
        }
    //Apoorv's method to velidate endate
    public void onChangeEndDate(ValueChangeEvent valueChangeEvent) {
        if (valueChangeEvent.getNewValue() == null || valueChangeEvent.getNewValue() == "") {
        } else {
            oracle.jbo.domain.Date std = (oracle.jbo.domain.Date) valueChangeEvent.getNewValue();
            BindingContext bindingContext = BindingContext.getCurrent();
            DCDataControl dc = bindingContext.findDataControl("FinTransactionAMDataControl");

            FinTransactionAMImpl appM = (FinTransactionAMImpl) dc.getDataProvider();
            String Date = appM.EndDat().toString();
            //System.out.println("start Date Value ---" + Date);
            oracle.jbo.domain.Date jboDate1 = new oracle.jbo.domain.Date(Date);
            oracle.jbo.domain.Date cmpd1 = jboDate1;
            


            if (Date != null) {
                if (std.compareTo(cmpd1) == 1) {


                    //System.out.println("You Enter date grater");
                    ADFUtils.showMessage("Please Enter date Between Financial Year", 0);
                    endDateReset.setValue(null);
                    AdfFacesContext adfFacesContext = AdfFacesContext.getCurrentInstance();
                    adfFacesContext.addPartialTarget(endDateReset);

                }


            }
        }
    }


    public void setStrDt(RichInputDate strDt) {
        this.strDt = strDt;
    }

    public RichInputDate getStrDt() {
        return strDt;
    }

    public void setEndDateReset(RichInputDate endDateReset) {
        this.endDateReset = endDateReset;
    }

    public RichInputDate getEndDateReset() {
        return endDateReset;
    }

    
}
