package terms.fin.transaction.ui.bean;

import javax.faces.event.ActionEvent;

public class SubLedgerOpeningTransferBean {
    public SubLedgerOpeningTransferBean() {
    }

    public void runAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("SubLedgerTransfer").execute();
    }
}
