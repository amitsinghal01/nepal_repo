package terms.fin.transaction.ui.bean;

import oracle.adf.view.rich.component.rich.input.RichInputText;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;

import java.sql.Connection;

import java.sql.SQLException;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import javax.faces.context.FacesContext;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

public class SubLedgerTrailBean {
    public SubLedgerTrailBean() {
    }
    public BindingContainer getBindings()
      {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
      }
    public void downloadJrxml(FacesContext facesContext, OutputStream outputStream) {
        Connection conn=null;
        try {
            OperationBinding binding = ADFUtils.findOperation("fileNameForPrint");
            binding.getParamsMap().put("fileId","FIN0000000072");
            binding.execute();
            System.out.println("Binding Result :"+binding.getResult());
            if(binding.getResult() != null){ 
              //  String str="/home/beta15/Jasper/r_sl_tri.jrxml";
                InputStream input = new FileInputStream(binding.getResult().toString());                
                DCIteratorBinding pvIter = (DCIteratorBinding) getBindings().get("SubLedgerTrailVVO1Iterator");
                oracle.jbo.domain.Date fromDate =(oracle.jbo.domain.Date)pvIter.getCurrentRow().getAttribute("FromDate");
                oracle.jbo.domain.Date toDate =(oracle.jbo.domain.Date)pvIter.getCurrentRow().getAttribute("ToDate");
                String unitCode = (String)pvIter.getCurrentRow().getAttribute("UnitCode");
                String glCode = (String)pvIter.getCurrentRow().getAttribute("Code");
                String finYear = (String)pvIter.getCurrentRow().getAttribute("Year");
                String sId=oracle.jbo.server.uniqueid.UniqueIdHelper.getNextId().toString();
                System.out.println("Parameters :- fromDate:"+fromDate+" ||UNIT CODE:"+unitCode+"|| toDate:"+toDate+" Gl Code: "+glCode+" SID: "+sId+" Year:"+finYear);                
                OperationBinding op=ADFUtils.findOperation("SubLedgerTrailProc");
                
                op.getParamsMap().put("unitCd", unitCode);
                op.getParamsMap().put("glCd", glCode);
                op.getParamsMap().put("sId", sId);
                op.getParamsMap().put("year", finYear);
                op.getParamsMap().put("frDate", fromDate);
                op.getParamsMap().put("toDate", toDate);

                op.execute();
                Map n = new HashMap();
                n.put("p_unit", unitCode);
                n.put("p_fdt", fromDate);
                n.put("p_tdt",toDate);
                n.put("p_gl", glCode);
                n.put("p_sid", sId);
                conn = getConnection( );
                
                JasperReport design = (JasperReport) JRLoader.loadObject(input);
                System.out.println("Path : " + input + " -------" + design+" param:"+n);
                
                @SuppressWarnings("unchecked")
                net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                byte pdf[] = JasperExportManager.exportReportToPdf(print);
                HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                response.getOutputStream().write(pdf);
                response.getOutputStream().flush();
                response.getOutputStream().close();
                facesContext.responseComplete();
                }else
                System.out.println("File Name/File Path not found.");
                } catch (FileNotFoundException fnfe) {
                // TODO: Add catch code
                fnfe.printStackTrace();
                try {
                    System.out.println("in finally connection closed");
                            conn.close( );
                            conn = null;
                    
                } catch( SQLException e ) {
                    e.printStackTrace( );
                }
                } catch (Exception e) {
                // TODO: Add catch code
                e.printStackTrace();
                try {
                    System.out.println("in finally connection closed");
                            conn.close( );
                            conn = null;
                    
                } catch( SQLException e1 ) {
                    e1.printStackTrace( );
                }
                }finally {
                        try {
                                System.out.println("in finally connection closed");
                                        conn.close( );
                                        conn = null;
                                
                        } catch( SQLException e ) {
                                e.printStackTrace( );
                        }
                }
    }
    
    
    public Connection getConnection() throws Exception{
            Context ctx = null;
            Connection conn = null;
            ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/ApplicationDBDS");
            conn = ds.getConnection();
            return conn;
        }
}
