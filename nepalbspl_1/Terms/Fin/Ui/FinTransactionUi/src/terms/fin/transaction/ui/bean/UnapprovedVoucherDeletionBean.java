package terms.fin.transaction.ui.bean;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichSelectBooleanCheckbox;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;

import terms.fin.transaction.model.view.UnapprovedVoucherDeletionVVORowImpl;

public class UnapprovedVoucherDeletionBean {
    private RichSelectBooleanCheckbox deleteChekBinding;
    private RichTable tableBinding;

    public UnapprovedVoucherDeletionBean() {
    }
    private String editAction="V";
    public void isEnableEdit(ActionEvent actionEvent) {
    // Add event code here...
    this.editAction=getEditAction();
    }
    public void setEditAction(String editAction) {
    this.editAction = editAction;
   
    }
    public String getEditAction() {
    return editAction;
    }

    public void SaveAL(ActionEvent actionEvent) {
        DCIteratorBinding Dcite=ADFUtils.findIterator("UnapprovedVoucherDeletionVVO1Iterator");
        UnapprovedVoucherDeletionVVORowImpl row=(UnapprovedVoucherDeletionVVORowImpl) Dcite.getCurrentRow();
        if((Long)ADFUtils.evaluateEL("#{bindings.UnapprovedVoucherDeletionVVO1Iterator.estimatedRowCount}")>=1 )
        {
            System.out.println("Vou Gen Flag: "+row.getDeleteCheck2());
            if(row.getDeleteCheck2() !=null)
              {
            OperationBinding op=ADFUtils.findOperation("unapprovedVouDeletion");
            op.execute();
            System.out.println("After Function Call From Amimpl: "+op.getResult());
                if (op.getResult() != null && op.getResult().equals("Y"))
                {
                    if (op.getErrors().isEmpty()) 
                    {
                      ADFUtils.findOperation("Commit").execute();
                      ADFUtils.showMessage("Record Saved Successfully.", 2);
                    }
                }

            RowSetIterator rsi = Dcite.getRowSetIterator();
            if(rsi!=null)
            {
                System.out.println("============yessssss==========");
                    Row[] allRowsInRange = rsi.getAllRowsInRange();
                    for (Row rw : allRowsInRange) 
                    {
                        System.out.println("allRowsInRange====="+allRowsInRange.length);
                        if (rw != null && (rw.getAttribute("DeleteCheck2") == null|| rw.getAttribute("DeleteCheck2").equals("N")))
                        {
                            System.out.println("------- Row remove method");
                            rw.remove();
                        }
                    }
             }
            rsi.closeRowSetIterator();
        }
        else{
              ADFUtils.showMessage("No record is selected. Please Check!", 0);    
            }
        }
        
    }

    public void setDeleteChekBinding(RichSelectBooleanCheckbox deleteChekBinding) {
        this.deleteChekBinding = deleteChekBinding;
    }

    public RichSelectBooleanCheckbox getDeleteChekBinding() {
        return deleteChekBinding;
    }

    public void editAL(ActionEvent actionEvent) {
        AdfFacesContext.getCurrentInstance().addPartialTarget(tableBinding);
    }

    public void setTableBinding(RichTable tableBinding) {
        this.tableBinding = tableBinding;
    }

    public RichTable getTableBinding() {
        return tableBinding;
    }

    public void DeleteAllVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce!=null)
        {
                OperationBinding Opr = ADFUtils.findOperation("checkEditSelect");
                 Object Obj=Opr.execute();
        //            if(bindCheckAll.getValue()!=null && bindCheckAll.getValue().equals("Y")){
        //            vouGenBinding.setValue("Y");
            }
    }
}
