package terms.fin.transaction.ui.bean;

import oracle.adf.view.rich.component.rich.data.RichTable;

public class UpdateCostCentreBean {
    private RichTable bindUpdateCostCentreTableBinding;
    private String editAction="V";

    public UpdateCostCentreBean() {
    }

    public void setBindUpdateCostCentreTableBinding(RichTable bindUpdateCostCentreTableBinding) {
        this.bindUpdateCostCentreTableBinding = bindUpdateCostCentreTableBinding;
    }

    public RichTable getBindUpdateCostCentreTableBinding() {
        return bindUpdateCostCentreTableBinding;
    }
    
    public void setEditAction(String editAction) {
    this.editAction = editAction;
    }
    
    public String getEditAction() {
    return editAction;
    }
}
