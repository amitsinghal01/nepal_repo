package terms.fin.transaction.ui.bean;

import oracle.adf.view.rich.component.rich.data.RichTable;

public class UpdateForRcmBean {
    private RichTable updateForRcmTableBinding;
    private String editAction="V";

    public UpdateForRcmBean() {
    }

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public void setUpdateForRcmTableBinding(RichTable updateForRcmTableBinding) {
        this.updateForRcmTableBinding = updateForRcmTableBinding;
    }

    public RichTable getUpdateForRcmTableBinding() {
        return updateForRcmTableBinding;
    }
}
