package terms.fin.transaction.ui.bean;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;

import java.sql.Connection;

import java.sql.SQLException;

import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import javax.faces.validator.ValidatorException;

import javax.naming.Context;
import javax.naming.InitialContext;

import javax.servlet.http.HttpServletResponse;

import javax.sql.DataSource;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;

import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;

import oracle.adf.view.rich.component.rich.nav.RichButton;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.Row;

public class VoucherPrintingBean {
    private RichInputComboboxListOfValues bankCodeBean;
    private RichInputComboboxListOfValues cashCodeBean;
    private String vou_type ="";
    private RichSelectOneChoice voucherTypeBinding;
    private RichPopup popupBinding;
    private RichButton bankBtnBinding;
    private RichSelectOneChoice approvedbyBinding;
    private RichInputDate fromDateBinding;
    private RichInputDate dateToBinding;


    public VoucherPrintingBean() {
    }

    public void vouchertypevce(ValueChangeEvent valueChangeEvent) 
    {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(valueChangeEvent.getNewValue()!=null)
        {
            if(valueChangeEvent.getNewValue().equals("J"))
            {
                vou_type="J";
            }
            else if(valueChangeEvent.getNewValue().equals("S"))
            {
                vou_type="J";
            }
            else if(valueChangeEvent.getNewValue().equals("B")){
                vou_type="B";
            }
            else if(valueChangeEvent.getNewValue().equals("C")){
                vou_type="C";
            }
            else if(valueChangeEvent.getNewValue().equals("R")){
                vou_type="J";
            }
            else if(valueChangeEvent.getNewValue().equals("P")){
                vou_type="J";
            }
        }
        
        
        
        
        Row row =(Row) ADFUtils.evaluateEL("#{bindings.VoucherPrintingVO1Iterator.currentRow}");
        System.out.println("VoucherType:"+row.getAttribute("VoucherType"));
        System.out.println("BankCode1:"+row.getAttribute("BankCode1"));
        System.out.println("SeriesDescription:"+row.getAttribute("SeriesDescription"));
    }

    public void setBankCodeBean(RichInputComboboxListOfValues bankCodeBean) {
        this.bankCodeBean = bankCodeBean;
    }

    public RichInputComboboxListOfValues getBankCodeBean() {
        return bankCodeBean;
    }

    public void setCashCodeBean(RichInputComboboxListOfValues cashCodeBean) {
        this.cashCodeBean = cashCodeBean;
    }

    public RichInputComboboxListOfValues getCashCodeBean() {
        return cashCodeBean;
    }

    public void SeiesDscVCE(ValueChangeEvent valueChangeEvent) {
       
    }
    
    
    public void downloadJrxml(FacesContext facesContext, OutputStream outputStream) {
        Connection conn=null;
            try {

                String file_name ="";
                System.out.println("vou_type===================>"+vou_type);
                
                if(vou_type.equals("B"))
                {
                file_name="r_bank_prn.jasper";
                }
                else if(vou_type.equals("C"))
                {
                    file_name="r_cash_prn.jasper";
                }
                else if(vou_type.equals("J"))
                {
                    file_name="r_jour_prn.jasper";
                }
                

                System.out.println("------------File Name-------->"+file_name);
                
                
                OperationBinding binding = ADFUtils.findOperation("fileNameForPrint");
                binding.getParamsMap().put("fileId","ERP0000000164");
                binding.execute();
                System.out.println("Binding Result :"+binding.getResult());
                if(binding.getResult() != null){
                    String result = binding.getResult().toString();
                    int last_index = result.lastIndexOf("/");
                    String path = result.substring(0,last_index+1)+file_name;
                    System.out.println("------------File Path Is---------------"+path);
                    InputStream input = new FileInputStream(path); 
                    DCIteratorBinding poIter = (DCIteratorBinding) getBindings().get("VoucherPrintingVO1Iterator");
        //            if(approvedbyBinding.equals("A")){
                if(approvedbyBinding.getValue().equals("A")){
                    String vouNoFrom = poIter.getCurrentRow().getAttribute("VoucherFrom").toString();
                    String vouNoTo = poIter.getCurrentRow().getAttribute("VoucherTo").toString();
                    String unitCode = poIter.getCurrentRow().getAttribute("UnitCode").toString();
                    oracle.jbo.domain.Date DateFrom=(oracle.jbo.domain.Date)poIter.getCurrentRow().getAttribute("DateFrom");
                    oracle.jbo.domain.Date DateFrom1=(oracle.jbo.domain.Date)poIter.getCurrentRow().getAttribute("DateTo");
                    
                
    System.out.println("-------------------------In case of If--------------------------------------------");
    System.out.println("-------------------------Voucher No. From------------"+vouNoFrom);
    System.out.println("-------------------------Voucher No. To  ------------"+vouNoTo);
    System.out.println("-------------------------From Date       ------------"+DateFrom);
    System.out.println("-------------------------To Date         ------------"+DateFrom);
    System.out.println("-------------------------Unit Code       ------------"+unitCode);
    System.out.println("-------------------------Voucher Type    ------------"+vou_type);
    System.out.println("-------------------------File Name       ------------"+file_name);   
    System.out.println("-------------------------Approved By     ------------"+approvedbyBinding.getValue());
    System.out.println("-------------------------End           --------------------------------------------");

                        
                    Map n = new HashMap();
                    n.put("p_vou_to", vouNoTo);
                    n.put("p_vou_fr", vouNoFrom);
                    n.put("p_voutp",vou_type);
                  //  n.put("p_unit", unitCode);
                    n.put("p_fr_dt", DateFrom);
                    n.put("p_to_dt", DateFrom1);
                    n.put("p_unit", unitCode);

                        conn = getConnection( );
                        
                        JasperReport design = (JasperReport) JRLoader.loadObject(input);
                        System.out.println("Path : " + input + " -------" + design+" param:"+n);
                        
                        @SuppressWarnings("unchecked")
                        net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                        byte pdf[] = JasperExportManager.exportReportToPdf(print);
                        HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                        response.getOutputStream().write(pdf);
                        response.getOutputStream().flush();
                        response.getOutputStream().close();
                        facesContext.responseComplete();
                    }
//                    else if(poIter.getCurrentRow().getAttribute("VoucherFrom1")==null && poIter.getCurrentRow().getAttribute("VoucherTo1")==null) {
                    else if(approvedbyBinding.getValue().equals("UA")) {
                        String invoiceNum2 = (String)poIter.getCurrentRow().getAttribute("VoucherFrom1");
                        String invoiceNum3 =(String) poIter.getCurrentRow().getAttribute("VoucherTo1");
                        String unitCode =(String) poIter.getCurrentRow().getAttribute("UnitCode");
                        
                        oracle.jbo.domain.Date DateFrom=(oracle.jbo.domain.Date)poIter.getCurrentRow().getAttribute("DateFrom");
                        oracle.jbo.domain.Date DateFrom1=(oracle.jbo.domain.Date)poIter.getCurrentRow().getAttribute("DateTo");

                        System.out.println("-------------------------In case of Else--------------------------------------------");
                        System.out.println("-------------------------Voucher No. From------------"+invoiceNum2);
                        System.out.println("-------------------------Voucher No. To  ------------"+invoiceNum3);
                        System.out.println("-------------------------From Date       ------------"+DateFrom);
                        System.out.println("-------------------------To Date         ------------"+DateFrom);
                        System.out.println("-------------------------Unit Code       ------------"+unitCode);
                        System.out.println("-------------------------Voucher Type    ------------"+vou_type);
                        System.out.println("-------------------------File Name       ------------"+file_name);   
                        System.out.println("-------------------------Approved By     ------------"+approvedbyBinding.getValue());
                        System.out.println("-------------------------End           --------------------------------------------");

                      
                        Map n = new HashMap();
                        n.put("p_vou_to", invoiceNum2);
                        n.put("p_vou_fr", invoiceNum3);
                        n.put("p_voutp",vou_type);
                        //  n.put("p_unit", unitCode);
                        n.put("p_fr_dt", DateFrom);
                        n.put("p_to_dt", DateFrom1);
                        n.put("p_unit", unitCode);

                        conn = getConnection( );
                        
                        JasperReport design = (JasperReport) JRLoader.loadObject(input);
                        System.out.println("Path : " + input + " -------" + design+" param:"+n);
                        
                        @SuppressWarnings("unchecked")
                        net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                        byte pdf[] = JasperExportManager.exportReportToPdf(print);
                        HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                        response.getOutputStream().write(pdf);
                        response.getOutputStream().flush();
                        response.getOutputStream().close();
                        facesContext.responseComplete();
                        
                    }
                    }else
                    System.out.println("File Name/File Path not found.");
                    } catch (FileNotFoundException fnfe) {
                    // TODO: Add catch code
                    fnfe.printStackTrace();
                    try {
                            System.out.println("in finally connection closed");
                                    conn.close( );
                                    conn = null;
                            
                    } catch( SQLException e ) {
                            e.printStackTrace( );
                    }
                    } catch (Exception e) {
                    // TODO: Add catch code
                    e.printStackTrace();
                    try {
                            System.out.println("in finally connection closed");
                                    conn.close( );
                                    conn = null;
                            
                    } catch( SQLException e1 ) {
                            e1.printStackTrace( );
                    }
                    }finally {
                                try {
                                        System.out.println("in finally connection closed");
                                                conn.close( );
                                                conn = null;
                                        
                                } catch( SQLException e ) {
                                        e.printStackTrace( );
                                }
                        }
        }

    public BindingContainer getBindings()
          {
            return BindingContext.getCurrent().getCurrentBindingsEntry();
          }
    
    
    public void setVou_type(String vou_type) {
        this.vou_type = vou_type;
    }

    public String getVou_type() {
        return vou_type;
    }

    public void seriesDescValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if(object!=null && voucherTypeBinding.getValue()!=null){
            String vouType=(String)voucherTypeBinding.getValue();
            String vouSeries=(String)object;
            if((vouType.equalsIgnoreCase("B")||vouType.equalsIgnoreCase("C")) && vouSeries.equalsIgnoreCase("V")){
                System.out.println("In My Condition!");
                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                  "Series Description Can't be V. ",
                                                                  null));
                }
            }

    }

    public void setVoucherTypeBinding(RichSelectOneChoice voucherTypeBinding) {
        this.voucherTypeBinding = voucherTypeBinding;
    }

    public RichSelectOneChoice getVoucherTypeBinding() {
        return voucherTypeBinding;
    }

    public void printVoucherAL(ActionEvent actionEvent) {
       
    }

    public void setPopupBinding(RichPopup popupBinding) {
        this.popupBinding = popupBinding;
    }

    public RichPopup getPopupBinding() {
        return popupBinding;
    }

    public void setBankBtnBinding(RichButton bankBtnBinding) {
        this.bankBtnBinding = bankBtnBinding;
    }

    public RichButton getBankBtnBinding() {
        return bankBtnBinding;
    }

    public void setApprovedbyBinding(RichSelectOneChoice approvedbyBinding) {
        this.approvedbyBinding = approvedbyBinding;
    }

    public RichSelectOneChoice getApprovedbyBinding() {
        return approvedbyBinding;
    }

    public void Bankcodevce1(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
    }

    public void FromDateBean(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if(object==null && dateToBinding.getValue()!=null){
            oracle.jbo.domain.Date DateFrom=(oracle.jbo.domain.Date)object;
            
              oracle.jbo.domain.Date DateTo=(oracle.jbo.domain.Date)dateToBinding.getValue();
                    System.out.println("In !"+DateFrom);
                    System.out.println("In My !"+DateTo);
                if (!(DateFrom.compareTo(DateTo)<=0) ){
                System.out.println("In My Condition!");
                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                  "FromDate Must less than ToDate ",
                                                    null));
                
                }
                   
                    
                }
}

    public void setFromDateBinding(RichInputDate fromDateBinding) {
        this.fromDateBinding = fromDateBinding;
    }

    public RichInputDate getFromDateBinding() {
        return fromDateBinding;
    }

    public void setDateToBinding(RichInputDate dateToBinding) {
        this.dateToBinding = dateToBinding;
    }

    public RichInputDate getDateToBinding() {
        return dateToBinding;
    }

    public void ForHoBankAL(ActionEvent actionEvent) {
        if(voucherTypeBinding.getValue().equals("B")){
                     vou_type="B";
                        System.out.println("vou_type"+voucherTypeBinding.getValue());
                    }
     
        
        
    }
    public void downloadJrxml1(FacesContext facesContext, OutputStream outputStream) {
        Connection conn=null;
            try {
                String file_name ="";
                System.out.println("vou_type===================>"+vou_type);
                
                if(vou_type.equals("B"))
                {
//                file_name="r_bank_prn.jasper";
                                    file_name="r_bank_prnho.jasper";
                }
    System.out.println("------------File Name-------->"+file_name);
    
    
    OperationBinding binding = ADFUtils.findOperation("fileNameForPrint");
    binding.getParamsMap().put("fileId","ERP0000000164");
    binding.execute();
    System.out.println("Binding Result :"+binding.getResult());
    if(binding.getResult() != null){
        String result = binding.getResult().toString();
        int last_index = result.lastIndexOf("/");
        String path = result.substring(0,last_index+1)+file_name;
        System.out.println("------------File Path Is---------------"+path);
        InputStream input = new FileInputStream(path); 
        DCIteratorBinding poIter = (DCIteratorBinding) getBindings().get("VoucherPrintingVO1Iterator");
    //            if(approvedbyBinding.equals("A")){
    if(approvedbyBinding.getValue().equals("A")){
        String vouNoFrom = poIter.getCurrentRow().getAttribute("VoucherFrom").toString();
        String vouNoTo = poIter.getCurrentRow().getAttribute("VoucherTo").toString();
        String unitCode = poIter.getCurrentRow().getAttribute("UnitCode").toString();
        oracle.jbo.domain.Date DateFrom=(oracle.jbo.domain.Date)poIter.getCurrentRow().getAttribute("DateFrom");
        oracle.jbo.domain.Date DateFrom1=(oracle.jbo.domain.Date)poIter.getCurrentRow().getAttribute("DateTo");
        
    
    System.out.println("-------------------------In case of If--------------------------------------------");
    System.out.println("-------------------------Voucher No. From------------"+vouNoFrom);
    System.out.println("-------------------------Voucher No. To  ------------"+vouNoTo);
    System.out.println("-------------------------From Date       ------------"+DateFrom);
    System.out.println("-------------------------To Date         ------------"+DateFrom);
    System.out.println("-------------------------Unit Code       ------------"+unitCode);
    System.out.println("-------------------------Voucher Type    ------------"+vou_type);
    System.out.println("-------------------------File Name       ------------"+file_name);
    System.out.println("-------------------------Approved By     ------------"+approvedbyBinding.getValue());
    System.out.println("-------------------------End           --------------------------------------------");

            
        Map n = new HashMap();
        n.put("p_vou_to", vouNoTo);
        n.put("p_vou_fr", vouNoFrom);
        n.put("p_voutp",vou_type);
      //  n.put("p_unit", unitCode);
        n.put("p_fr_dt", DateFrom);
        n.put("p_to_dt", DateFrom1);
        n.put("p_unit", unitCode);

            conn = getConnection( );
            
            JasperReport design = (JasperReport) JRLoader.loadObject(input);
            System.out.println("Path : " + input + " -------" + design+" param:"+n);
            
            @SuppressWarnings("unchecked")
            net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
            byte pdf[] = JasperExportManager.exportReportToPdf(print);
            HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
            response.getOutputStream().write(pdf);
            response.getOutputStream().flush();
            response.getOutputStream().close();
            facesContext.responseComplete();
        }
    //                    else if(poIter.getCurrentRow().getAttribute("VoucherFrom1")==null && poIter.getCurrentRow().getAttribute("VoucherTo1")==null) {
        else if(approvedbyBinding.getValue().equals("UA")) {
            String invoiceNum2 = (String)poIter.getCurrentRow().getAttribute("VoucherFrom1");
            String invoiceNum3 =(String) poIter.getCurrentRow().getAttribute("VoucherTo1");
            String unitCode =(String) poIter.getCurrentRow().getAttribute("UnitCode");
            
            oracle.jbo.domain.Date DateFrom=(oracle.jbo.domain.Date)poIter.getCurrentRow().getAttribute("DateFrom");
            oracle.jbo.domain.Date DateFrom1=(oracle.jbo.domain.Date)poIter.getCurrentRow().getAttribute("DateTo");

            System.out.println("-------------------------In case of Else--------------------------------------------");
            System.out.println("-------------------------Voucher No. From------------"+invoiceNum2);
            System.out.println("-------------------------Voucher No. To  ------------"+invoiceNum3);
            System.out.println("-------------------------From Date       ------------"+DateFrom);
            System.out.println("-------------------------To Date         ------------"+DateFrom);
            System.out.println("-------------------------Unit Code       ------------"+unitCode);
            System.out.println("-------------------------Voucher Type    ------------"+vou_type);
            System.out.println("-------------------------File Name       ------------"+file_name);   
            System.out.println("-------------------------Approved By     ------------"+approvedbyBinding.getValue());
            System.out.println("-------------------------End           --------------------------------------------");

          
            Map n = new HashMap();
            n.put("p_vou_to", invoiceNum2);
            n.put("p_vou_fr", invoiceNum3);
            n.put("p_voutp",vou_type);
            //  n.put("p_unit", unitCode);
            n.put("p_fr_dt", DateFrom);
            n.put("p_to_dt", DateFrom1);
            n.put("p_unit", unitCode);

            conn = getConnection( );
            
            JasperReport design = (JasperReport) JRLoader.loadObject(input);
            System.out.println("Path : " + input + " -------" + design+" param:"+n);
            
            @SuppressWarnings("unchecked")
            net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
            byte pdf[] = JasperExportManager.exportReportToPdf(print);
            HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
            response.getOutputStream().write(pdf);
            response.getOutputStream().flush();
            response.getOutputStream().close();
            facesContext.responseComplete();
            
        }
        }else
        System.out.println("File Name/File Path not found.");
        } catch (FileNotFoundException fnfe) {
        // TODO: Add catch code
        fnfe.printStackTrace();
        try {
                System.out.println("in finally connection closed");
                        conn.close( );
                        conn = null;
                
        } catch( SQLException e ) {
                e.printStackTrace( );
        }
        } catch (Exception e) {
        // TODO: Add catch code
        e.printStackTrace();
        try {
                System.out.println("in finally connection closed");
                        conn.close( );
                        conn = null;
                
        } catch( SQLException e1 ) {
                e1.printStackTrace( );
        }
        }finally {
                    try {
                            System.out.println("in finally connection closed");
                                    conn.close( );
                                    conn = null;
                            
                    } catch( SQLException e ) {
                            e.printStackTrace( );
                    }
            }
    }

    public Connection getConnection() throws Exception{
            Context ctx = null;
            Connection conn = null;
            ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/ApplicationDBDS");
            conn = ds.getConnection();
            return conn;
        }

}
