package terms.fin.transaction.ui.bean;

import oracle.adf.view.rich.component.rich.data.RichTable;

public class searchExportInvoiceUpdateBinding {
    private RichTable searchTableBinding;

    public searchExportInvoiceUpdateBinding() {
    }

    public void setSearchTableBinding(RichTable searchTableBinding) {
        this.searchTableBinding = searchTableBinding;
    }

    public RichTable getSearchTableBinding() {
        return searchTableBinding;
    }
}
