package terms.hrm.setup.model.view;

import java.math.BigDecimal;

import oracle.jbo.RowSet;
import oracle.jbo.domain.Number;
import oracle.jbo.domain.Timestamp;
import oracle.jbo.server.AttributeDefImpl;
import oracle.jbo.server.EntityImpl;
import oracle.jbo.server.ViewRowImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Mon Dec 11 14:53:03 IST 2017
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class CategoryMasterVORowImpl extends ViewRowImpl {


    public static final int ENTITY_CATEGORYMASTEREO = 0;
    public static final int ENTITY_UNITEO = 1;
    public static final int ENTITY_LEVELMASTEREO = 2;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        Basic {
            public Object get(CategoryMasterVORowImpl obj) {
                return obj.getBasic();
            }

            public void put(CategoryMasterVORowImpl obj, Object value) {
                obj.setBasic((BigDecimal) value);
            }
        }
        ,
        Code {
            public Object get(CategoryMasterVORowImpl obj) {
                return obj.getCode();
            }

            public void put(CategoryMasterVORowImpl obj, Object value) {
                obj.setCode((String) value);
            }
        }
        ,
        CreatedBy {
            public Object get(CategoryMasterVORowImpl obj) {
                return obj.getCreatedBy();
            }

            public void put(CategoryMasterVORowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        CreationDate {
            public Object get(CategoryMasterVORowImpl obj) {
                return obj.getCreationDate();
            }

            public void put(CategoryMasterVORowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        Description {
            public Object get(CategoryMasterVORowImpl obj) {
                return obj.getDescription();
            }

            public void put(CategoryMasterVORowImpl obj, Object value) {
                obj.setDescription((String) value);
            }
        }
        ,
        LastUpdateDate {
            public Object get(CategoryMasterVORowImpl obj) {
                return obj.getLastUpdateDate();
            }

            public void put(CategoryMasterVORowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        LastUpdatedBy {
            public Object get(CategoryMasterVORowImpl obj) {
                return obj.getLastUpdatedBy();
            }

            public void put(CategoryMasterVORowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        LevelFrom {
            public Object get(CategoryMasterVORowImpl obj) {
                return obj.getLevelFrom();
            }

            public void put(CategoryMasterVORowImpl obj, Object value) {
                obj.setLevelFrom((String) value);
            }
        }
        ,
        LevelTo {
            public Object get(CategoryMasterVORowImpl obj) {
                return obj.getLevelTo();
            }

            public void put(CategoryMasterVORowImpl obj, Object value) {
                obj.setLevelTo((String) value);
            }
        }
        ,
        ObjectVersionNumber {
            public Object get(CategoryMasterVORowImpl obj) {
                return obj.getObjectVersionNumber();
            }

            public void put(CategoryMasterVORowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        PayFrom {
            public Object get(CategoryMasterVORowImpl obj) {
                return obj.getPayFrom();
            }

            public void put(CategoryMasterVORowImpl obj, Object value) {
                obj.setPayFrom((BigDecimal) value);
            }
        }
        ,
        PayTo {
            public Object get(CategoryMasterVORowImpl obj) {
                return obj.getPayTo();
            }

            public void put(CategoryMasterVORowImpl obj, Object value) {
                obj.setPayTo((BigDecimal) value);
            }
        }
        ,
        ScalePay {
            public Object get(CategoryMasterVORowImpl obj) {
                return obj.getScalePay();
            }

            public void put(CategoryMasterVORowImpl obj, Object value) {
                obj.setScalePay((BigDecimal) value);
            }
        }
        ,
        UnitCd {
            public Object get(CategoryMasterVORowImpl obj) {
                return obj.getUnitCd();
            }

            public void put(CategoryMasterVORowImpl obj, Object value) {
                obj.setUnitCd((String) value);
            }
        }
        ,
        Name {
            public Object get(CategoryMasterVORowImpl obj) {
                return obj.getName();
            }

            public void put(CategoryMasterVORowImpl obj, Object value) {
                obj.setName((String) value);
            }
        }
        ,
        Code1 {
            public Object get(CategoryMasterVORowImpl obj) {
                return obj.getCode1();
            }

            public void put(CategoryMasterVORowImpl obj, Object value) {
                obj.setCode1((String) value);
            }
        }
        ,
        ObjectVersionNumber1 {
            public Object get(CategoryMasterVORowImpl obj) {
                return obj.getObjectVersionNumber1();
            }

            public void put(CategoryMasterVORowImpl obj, Object value) {
                obj.setObjectVersionNumber1((Integer) value);
            }
        }
        ,
        EditTrans {
            public Object get(CategoryMasterVORowImpl obj) {
                return obj.getEditTrans();
            }

            public void put(CategoryMasterVORowImpl obj, Object value) {
                obj.setEditTrans((Integer) value);
            }
        }
        ,
        BaskAmt {
            public Object get(CategoryMasterVORowImpl obj) {
                return obj.getBaskAmt();
            }

            public void put(CategoryMasterVORowImpl obj, Object value) {
                obj.setBaskAmt((BigDecimal) value);
            }
        }
        ,
        LevelFrom1 {
            public Object get(CategoryMasterVORowImpl obj) {
                return obj.getLevelFrom1();
            }

            public void put(CategoryMasterVORowImpl obj, Object value) {
                obj.setLevelFrom1((String) value);
            }
        }
        ,
        Description1 {
            public Object get(CategoryMasterVORowImpl obj) {
                return obj.getDescription1();
            }

            public void put(CategoryMasterVORowImpl obj, Object value) {
                obj.setDescription1((String) value);
            }
        }
        ,
        LvlMastLevel {
            public Object get(CategoryMasterVORowImpl obj) {
                return obj.getLvlMastLevel();
            }

            public void put(CategoryMasterVORowImpl obj, Object value) {
                obj.setLvlMastLevel((String) value);
            }
        }
        ,
        UnitVO1 {
            public Object get(CategoryMasterVORowImpl obj) {
                return obj.getUnitVO1();
            }

            public void put(CategoryMasterVORowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        LevelMasterVO1 {
            public Object get(CategoryMasterVORowImpl obj) {
                return obj.getLevelMasterVO1();
            }

            public void put(CategoryMasterVORowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ;
        static AttributesEnum[] vals = null;
        ;
        private static final int firstIndex = 0;

        public abstract Object get(CategoryMasterVORowImpl object);

        public abstract void put(CategoryMasterVORowImpl object, Object value);

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int BASIC = AttributesEnum.Basic.index();
    public static final int CODE = AttributesEnum.Code.index();
    public static final int CREATEDBY = AttributesEnum.CreatedBy.index();
    public static final int CREATIONDATE = AttributesEnum.CreationDate.index();
    public static final int DESCRIPTION = AttributesEnum.Description.index();
    public static final int LASTUPDATEDATE = AttributesEnum.LastUpdateDate.index();
    public static final int LASTUPDATEDBY = AttributesEnum.LastUpdatedBy.index();
    public static final int LEVELFROM = AttributesEnum.LevelFrom.index();
    public static final int LEVELTO = AttributesEnum.LevelTo.index();
    public static final int OBJECTVERSIONNUMBER = AttributesEnum.ObjectVersionNumber.index();
    public static final int PAYFROM = AttributesEnum.PayFrom.index();
    public static final int PAYTO = AttributesEnum.PayTo.index();
    public static final int SCALEPAY = AttributesEnum.ScalePay.index();
    public static final int UNITCD = AttributesEnum.UnitCd.index();
    public static final int NAME = AttributesEnum.Name.index();
    public static final int CODE1 = AttributesEnum.Code1.index();
    public static final int OBJECTVERSIONNUMBER1 = AttributesEnum.ObjectVersionNumber1.index();
    public static final int EDITTRANS = AttributesEnum.EditTrans.index();
    public static final int BASKAMT = AttributesEnum.BaskAmt.index();
    public static final int LEVELFROM1 = AttributesEnum.LevelFrom1.index();
    public static final int DESCRIPTION1 = AttributesEnum.Description1.index();
    public static final int LVLMASTLEVEL = AttributesEnum.LvlMastLevel.index();
    public static final int UNITVO1 = AttributesEnum.UnitVO1.index();
    public static final int LEVELMASTERVO1 = AttributesEnum.LevelMasterVO1.index();

    /**
     * This is the default constructor (do not remove).
     */
    public CategoryMasterVORowImpl() {
    }

    /**
     * Gets CategoryMasterEO entity object.
     * @return the CategoryMasterEO
     */
    public EntityImpl getCategoryMasterEO() {
        return (EntityImpl) getEntity(ENTITY_CATEGORYMASTEREO);
    }

    /**
     * Gets UnitEO entity object.
     * @return the UnitEO
     */
    public EntityImpl getUnitEO() {
        return (EntityImpl) getEntity(ENTITY_UNITEO);
    }


    /**
     * Gets LevelMasterEO entity object.
     * @return the LevelMasterEO
     */
    public EntityImpl getLevelMasterEO() {
        return (EntityImpl) getEntity(ENTITY_LEVELMASTEREO);
    }

    /**
     * Gets the attribute value for BASIC using the alias name Basic.
     * @return the BASIC
     */
    public BigDecimal getBasic() {
        return (BigDecimal) getAttributeInternal(BASIC);
    }

    /**
     * Sets <code>value</code> as attribute value for BASIC using the alias name Basic.
     * @param value value to set the BASIC
     */
    public void setBasic(BigDecimal value) {
        setAttributeInternal(BASIC, value);
    }

    /**
     * Gets the attribute value for CODE using the alias name Code.
     * @return the CODE
     */
    public String getCode() {
        return (String) getAttributeInternal(CODE);
    }

    /**
     * Sets <code>value</code> as attribute value for CODE using the alias name Code.
     * @param value value to set the CODE
     */
    public void setCode(String value) {
        setAttributeInternal(CODE, value);
    }

    /**
     * Gets the attribute value for CREATED_BY using the alias name CreatedBy.
     * @return the CREATED_BY
     */
    public String getCreatedBy() {
        return (String) getAttributeInternal(CREATEDBY);
    }

    /**
     * Gets the attribute value for CREATION_DATE using the alias name CreationDate.
     * @return the CREATION_DATE
     */
    public Timestamp getCreationDate() {
        return (Timestamp) getAttributeInternal(CREATIONDATE);
    }

    /**
     * Gets the attribute value for DESCRIPTION using the alias name Description.
     * @return the DESCRIPTION
     */
    public String getDescription() {
        return (String) getAttributeInternal(DESCRIPTION);
    }

    /**
     * Sets <code>value</code> as attribute value for DESCRIPTION using the alias name Description.
     * @param value value to set the DESCRIPTION
     */
    public void setDescription(String value) {
        setAttributeInternal(DESCRIPTION, value);
    }

    /**
     * Gets the attribute value for LAST_UPDATE_DATE using the alias name LastUpdateDate.
     * @return the LAST_UPDATE_DATE
     */
    public Timestamp getLastUpdateDate() {
        return (Timestamp) getAttributeInternal(LASTUPDATEDATE);
    }

    /**
     * Gets the attribute value for LAST_UPDATED_BY using the alias name LastUpdatedBy.
     * @return the LAST_UPDATED_BY
     */
    public String getLastUpdatedBy() {
        return (String) getAttributeInternal(LASTUPDATEDBY);
    }

    /**
     * Gets the attribute value for LEVEL_FROM using the alias name LevelFrom.
     * @return the LEVEL_FROM
     */
    public String getLevelFrom() {
        return (String) getAttributeInternal(LEVELFROM);
    }

    /**
     * Sets <code>value</code> as attribute value for LEVEL_FROM using the alias name LevelFrom.
     * @param value value to set the LEVEL_FROM
     */
    public void setLevelFrom(String value) {
        setAttributeInternal(LEVELFROM, value);
    }

    /**
     * Gets the attribute value for LEVEL_TO using the alias name LevelTo.
     * @return the LEVEL_TO
     */
    public String getLevelTo() {
        return (String) getAttributeInternal(LEVELTO);
    }

    /**
     * Sets <code>value</code> as attribute value for LEVEL_TO using the alias name LevelTo.
     * @param value value to set the LEVEL_TO
     */
    public void setLevelTo(String value) {
        setAttributeInternal(LEVELTO, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER);
    }

    /**
     * Gets the attribute value for PAY_FROM using the alias name PayFrom.
     * @return the PAY_FROM
     */
    public BigDecimal getPayFrom() {
        return (BigDecimal) getAttributeInternal(PAYFROM);
    }

    /**
     * Sets <code>value</code> as attribute value for PAY_FROM using the alias name PayFrom.
     * @param value value to set the PAY_FROM
     */
    public void setPayFrom(BigDecimal value) {
        setAttributeInternal(PAYFROM, value);
    }

    /**
     * Gets the attribute value for PAY_TO using the alias name PayTo.
     * @return the PAY_TO
     */
    public BigDecimal getPayTo() {
        return (BigDecimal) getAttributeInternal(PAYTO);
    }

    /**
     * Sets <code>value</code> as attribute value for PAY_TO using the alias name PayTo.
     * @param value value to set the PAY_TO
     */
    public void setPayTo(BigDecimal value) {
        setAttributeInternal(PAYTO, value);
    }

    /**
     * Gets the attribute value for SCALE_PAY using the alias name ScalePay.
     * @return the SCALE_PAY
     */
    public BigDecimal getScalePay() {
        return (BigDecimal) getAttributeInternal(SCALEPAY);
    }

    /**
     * Sets <code>value</code> as attribute value for SCALE_PAY using the alias name ScalePay.
     * @param value value to set the SCALE_PAY
     */
    public void setScalePay(BigDecimal value) {
        setAttributeInternal(SCALEPAY, value);
    }

    /**
     * Gets the attribute value for UNIT_CD using the alias name UnitCd.
     * @return the UNIT_CD
     */
    public String getUnitCd() {
        return (String) getAttributeInternal(UNITCD);
    }

    /**
     * Sets <code>value</code> as attribute value for UNIT_CD using the alias name UnitCd.
     * @param value value to set the UNIT_CD
     */
    public void setUnitCd(String value) {
        setAttributeInternal(UNITCD, value);
    }

    /**
     * Gets the attribute value for NAME using the alias name Name.
     * @return the NAME
     */
    public String getName() {
        return (String) getAttributeInternal(NAME);
    }

    /**
     * Sets <code>value</code> as attribute value for NAME using the alias name Name.
     * @param value value to set the NAME
     */
    public void setName(String value) {
        setAttributeInternal(NAME, value);
    }

    /**
     * Gets the attribute value for CODE using the alias name Code1.
     * @return the CODE
     */
    public String getCode1() {
        return (String) getAttributeInternal(CODE1);
    }

    /**
     * Sets <code>value</code> as attribute value for CODE using the alias name Code1.
     * @param value value to set the CODE
     */
    public void setCode1(String value) {
        setAttributeInternal(CODE1, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber1.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber1() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER1);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber1.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber1(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER1, value);
    }

    /**
     * Gets the attribute value for the calculated attribute EditTrans.
     * @return the EditTrans
     */
    public Integer getEditTrans() {
        byte entityState = this.getEntity(0).getEntityState();
        return new Integer(entityState);
       
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute EditTrans.
     * @param value value to set the  EditTrans
     */
    public void setEditTrans(Integer value) {
        setAttributeInternal(EDITTRANS, value);
    }

    /**
     * Gets the attribute value for BASK_AMT using the alias name BaskAmt.
     * @return the BASK_AMT
     */
    public BigDecimal getBaskAmt() {
        return (BigDecimal) getAttributeInternal(BASKAMT);
    }

    /**
     * Sets <code>value</code> as attribute value for BASK_AMT using the alias name BaskAmt.
     * @param value value to set the BASK_AMT
     */
    public void setBaskAmt(BigDecimal value) {
        setAttributeInternal(BASKAMT, value);
    }


    /**
     * Gets the attribute value for LEVEL_FROM using the alias name LevelFrom1.
     * @return the LEVEL_FROM
     */
    public String getLevelFrom1() {
        return (String) getAttributeInternal(LEVELFROM1);
    }

    /**
     * Sets <code>value</code> as attribute value for LEVEL_FROM using the alias name LevelFrom1.
     * @param value value to set the LEVEL_FROM
     */
    public void setLevelFrom1(String value) {
        setAttributeInternal(LEVELFROM1, value);
    }

    /**
     * Gets the attribute value for DESCRIPTION using the alias name Description1.
     * @return the DESCRIPTION
     */
    public String getDescription1() {
        return (String) getAttributeInternal(DESCRIPTION1);
    }

    /**
     * Sets <code>value</code> as attribute value for DESCRIPTION using the alias name Description1.
     * @param value value to set the DESCRIPTION
     */
    public void setDescription1(String value) {
        setAttributeInternal(DESCRIPTION1, value);
    }

    /**
     * Gets the attribute value for LVL_MAST_LEVEL using the alias name LvlMastLevel.
     * @return the LVL_MAST_LEVEL
     */
    public String getLvlMastLevel() {
        return (String) getAttributeInternal(LVLMASTLEVEL);
    }

    /**
     * Sets <code>value</code> as attribute value for LVL_MAST_LEVEL using the alias name LvlMastLevel.
     * @param value value to set the LVL_MAST_LEVEL
     */
    public void setLvlMastLevel(String value) {
        setAttributeInternal(LVLMASTLEVEL, value);
    }

    /**
     * Gets the view accessor <code>RowSet</code> UnitVO1.
     */
    public RowSet getUnitVO1() {
        return (RowSet) getAttributeInternal(UNITVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> LevelMasterVO1.
     */
    public RowSet getLevelMasterVO1() {
        return (RowSet) getAttributeInternal(LEVELMASTERVO1);
    }

    /**
     * getAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param attrDef the attribute

     * @return the attribute value
     * @throws Exception
     */
    protected Object getAttrInvokeAccessor(int index, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            return AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].get(this);
        }
        return super.getAttrInvokeAccessor(index, attrDef);
    }

    /**
     * setAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param value the value to assign to the attribute
     * @param attrDef the attribute

     * @throws Exception
     */
    protected void setAttrInvokeAccessor(int index, Object value, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].put(this, value);
            return;
        }
        super.setAttrInvokeAccessor(index, value, attrDef);
    }
}

