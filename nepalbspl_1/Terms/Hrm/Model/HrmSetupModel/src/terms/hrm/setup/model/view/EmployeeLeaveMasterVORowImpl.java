package terms.hrm.setup.model.view;

import java.math.BigDecimal;

import java.sql.Timestamp;

import oracle.jbo.RowSet;
import oracle.jbo.server.EntityImpl;
import oracle.jbo.server.ViewRowImpl;

import terms.hrm.setup.model.entity.EmployeeMasterEOImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Wed Jan 23 12:07:13 IST 2019
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class EmployeeLeaveMasterVORowImpl extends ViewRowImpl {


    public static final int ENTITY_EMPLOYEELEAVEMASTEREO = 0;
    public static final int ENTITY_UNITEO = 1;
    public static final int ENTITY_EMPLOYEEMASTEREO = 2;
    public static final int ENTITY_LEAVEMASTERHEADEREO = 3;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        CreatedBy,
        CreationDate,
        CreditLeave,
        CurrYearLeave,
        EmpLeaveMastId,
        EmpmembEmpMasterEmpNumber,
        FinYear,
        LastUpdateDate,
        LastUpdatedBy,
        LeaveAvailed,
        LeaveDescLeaveDescType,
        ObjectVersionNumber,
        StartDate,
        UnitCd,
        Name,
        Code,
        ObjectVersionNumber1,
        EmpFirstName,
        EmpLastName,
        EmpNumber,
        ObjectVersionNumber2,
        EditTrans,
        Description,
        LeaveMastCodeId,
        EmpmembEmpMasterEmpNumber1,
        UnitVO1,
        LeaveMasterHeaderVO1,
        EmpViewVVO1;
        static AttributesEnum[] vals = null;
        ;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int CREATEDBY = AttributesEnum.CreatedBy.index();
    public static final int CREATIONDATE = AttributesEnum.CreationDate.index();
    public static final int CREDITLEAVE = AttributesEnum.CreditLeave.index();
    public static final int CURRYEARLEAVE = AttributesEnum.CurrYearLeave.index();
    public static final int EMPLEAVEMASTID = AttributesEnum.EmpLeaveMastId.index();
    public static final int EMPMEMBEMPMASTEREMPNUMBER = AttributesEnum.EmpmembEmpMasterEmpNumber.index();
    public static final int FINYEAR = AttributesEnum.FinYear.index();
    public static final int LASTUPDATEDATE = AttributesEnum.LastUpdateDate.index();
    public static final int LASTUPDATEDBY = AttributesEnum.LastUpdatedBy.index();
    public static final int LEAVEAVAILED = AttributesEnum.LeaveAvailed.index();
    public static final int LEAVEDESCLEAVEDESCTYPE = AttributesEnum.LeaveDescLeaveDescType.index();
    public static final int OBJECTVERSIONNUMBER = AttributesEnum.ObjectVersionNumber.index();
    public static final int STARTDATE = AttributesEnum.StartDate.index();
    public static final int UNITCD = AttributesEnum.UnitCd.index();
    public static final int NAME = AttributesEnum.Name.index();
    public static final int CODE = AttributesEnum.Code.index();
    public static final int OBJECTVERSIONNUMBER1 = AttributesEnum.ObjectVersionNumber1.index();
    public static final int EMPFIRSTNAME = AttributesEnum.EmpFirstName.index();
    public static final int EMPLASTNAME = AttributesEnum.EmpLastName.index();
    public static final int EMPNUMBER = AttributesEnum.EmpNumber.index();
    public static final int OBJECTVERSIONNUMBER2 = AttributesEnum.ObjectVersionNumber2.index();
    public static final int EDITTRANS = AttributesEnum.EditTrans.index();
    public static final int DESCRIPTION = AttributesEnum.Description.index();
    public static final int LEAVEMASTCODEID = AttributesEnum.LeaveMastCodeId.index();
    public static final int EMPMEMBEMPMASTEREMPNUMBER1 = AttributesEnum.EmpmembEmpMasterEmpNumber1.index();
    public static final int UNITVO1 = AttributesEnum.UnitVO1.index();
    public static final int LEAVEMASTERHEADERVO1 = AttributesEnum.LeaveMasterHeaderVO1.index();
    public static final int EMPVIEWVVO1 = AttributesEnum.EmpViewVVO1.index();

    /**
     * This is the default constructor (do not remove).
     */
    public EmployeeLeaveMasterVORowImpl() {
    }

    /**
     * Gets EmployeeLeaveMasterEO entity object.
     * @return the EmployeeLeaveMasterEO
     */
    public EntityImpl getEmployeeLeaveMasterEO() {
        return (EntityImpl) getEntity(ENTITY_EMPLOYEELEAVEMASTEREO);
    }

    /**
     * Gets UnitEO entity object.
     * @return the UnitEO
     */
    public EntityImpl getUnitEO() {
        return (EntityImpl) getEntity(ENTITY_UNITEO);
    }

    /**
     * Gets EmployeeMasterEO entity object.
     * @return the EmployeeMasterEO
     */
    public EmployeeMasterEOImpl getEmployeeMasterEO() {
        return (EmployeeMasterEOImpl) getEntity(ENTITY_EMPLOYEEMASTEREO);
    }


    /**
     * Gets LeaveMasterHeaderEO entity object.
     * @return the LeaveMasterHeaderEO
     */
    public EntityImpl getLeaveMasterHeaderEO() {
        return (EntityImpl) getEntity(ENTITY_LEAVEMASTERHEADEREO);
    }

    /**
     * Gets the attribute value for CREATED_BY using the alias name CreatedBy.
     * @return the CREATED_BY
     */
    public String getCreatedBy() {
        return (String) getAttributeInternal(CREATEDBY);
    }


    /**
     * Gets the attribute value for CREATION_DATE using the alias name CreationDate.
     * @return the CREATION_DATE
     */
    public Timestamp getCreationDate() {
        return (Timestamp) getAttributeInternal(CREATIONDATE);
    }


    /**
     * Gets the attribute value for CREDIT_LEAVE using the alias name CreditLeave.
     * @return the CREDIT_LEAVE
     */
    public BigDecimal getCreditLeave() {
        return (BigDecimal) getAttributeInternal(CREDITLEAVE);
    }

    /**
     * Sets <code>value</code> as attribute value for CREDIT_LEAVE using the alias name CreditLeave.
     * @param value value to set the CREDIT_LEAVE
     */
    public void setCreditLeave(BigDecimal value) {
        setAttributeInternal(CREDITLEAVE, value);
    }

    /**
     * Gets the attribute value for CURR_YEAR_LEAVE using the alias name CurrYearLeave.
     * @return the CURR_YEAR_LEAVE
     */
    public BigDecimal getCurrYearLeave() {
        return (BigDecimal) getAttributeInternal(CURRYEARLEAVE);
    }

    /**
     * Sets <code>value</code> as attribute value for CURR_YEAR_LEAVE using the alias name CurrYearLeave.
     * @param value value to set the CURR_YEAR_LEAVE
     */
    public void setCurrYearLeave(BigDecimal value) {
        setAttributeInternal(CURRYEARLEAVE, value);
    }

    /**
     * Gets the attribute value for EMP_LEAVE_MAST_ID using the alias name EmpLeaveMastId.
     * @return the EMP_LEAVE_MAST_ID
     */
    public Long getEmpLeaveMastId() {
        return (Long) getAttributeInternal(EMPLEAVEMASTID);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_LEAVE_MAST_ID using the alias name EmpLeaveMastId.
     * @param value value to set the EMP_LEAVE_MAST_ID
     */
    public void setEmpLeaveMastId(Long value) {
        setAttributeInternal(EMPLEAVEMASTID, value);
    }

    /**
     * Gets the attribute value for EMPMEMB_EMP_MASTER_EMP_NUMBER using the alias name EmpmembEmpMasterEmpNumber.
     * @return the EMPMEMB_EMP_MASTER_EMP_NUMBER
     */
    public String getEmpmembEmpMasterEmpNumber() {
        return (String) getAttributeInternal(EMPMEMBEMPMASTEREMPNUMBER);
    }

    /**
     * Sets <code>value</code> as attribute value for EMPMEMB_EMP_MASTER_EMP_NUMBER using the alias name EmpmembEmpMasterEmpNumber.
     * @param value value to set the EMPMEMB_EMP_MASTER_EMP_NUMBER
     */
    public void setEmpmembEmpMasterEmpNumber(String value) {
        setAttributeInternal(EMPMEMBEMPMASTEREMPNUMBER, value);
    }

    /**
     * Gets the attribute value for FIN_YEAR using the alias name FinYear.
     * @return the FIN_YEAR
     */
    public String getFinYear() {
        return (String) getAttributeInternal(FINYEAR);
    }

    /**
     * Sets <code>value</code> as attribute value for FIN_YEAR using the alias name FinYear.
     * @param value value to set the FIN_YEAR
     */
    public void setFinYear(String value) {
        setAttributeInternal(FINYEAR, value);
    }

    /**
     * Gets the attribute value for LAST_UPDATE_DATE using the alias name LastUpdateDate.
     * @return the LAST_UPDATE_DATE
     */
    public Timestamp getLastUpdateDate() {
        return (Timestamp) getAttributeInternal(LASTUPDATEDATE);
    }


    /**
     * Gets the attribute value for LAST_UPDATED_BY using the alias name LastUpdatedBy.
     * @return the LAST_UPDATED_BY
     */
    public String getLastUpdatedBy() {
        return (String) getAttributeInternal(LASTUPDATEDBY);
    }


    /**
     * Gets the attribute value for LEAVE_AVAILED using the alias name LeaveAvailed.
     * @return the LEAVE_AVAILED
     */
    public BigDecimal getLeaveAvailed() {
        return (BigDecimal) getAttributeInternal(LEAVEAVAILED);
    }

    /**
     * Sets <code>value</code> as attribute value for LEAVE_AVAILED using the alias name LeaveAvailed.
     * @param value value to set the LEAVE_AVAILED
     */
    public void setLeaveAvailed(BigDecimal value) {
        setAttributeInternal(LEAVEAVAILED, value);
    }

    /**
     * Gets the attribute value for LEAVE_DESC_LEAVE_DESC_TYPE using the alias name LeaveDescLeaveDescType.
     * @return the LEAVE_DESC_LEAVE_DESC_TYPE
     */
    public String getLeaveDescLeaveDescType() {
        return (String) getAttributeInternal(LEAVEDESCLEAVEDESCTYPE);
    }

    /**
     * Sets <code>value</code> as attribute value for LEAVE_DESC_LEAVE_DESC_TYPE using the alias name LeaveDescLeaveDescType.
     * @param value value to set the LEAVE_DESC_LEAVE_DESC_TYPE
     */
    public void setLeaveDescLeaveDescType(String value) {
        setAttributeInternal(LEAVEDESCLEAVEDESCTYPE, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER);
    }


    /**
     * Gets the attribute value for START_DATE using the alias name StartDate.
     * @return the START_DATE
     */
    public Timestamp getStartDate() {
        return (Timestamp) getAttributeInternal(STARTDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for START_DATE using the alias name StartDate.
     * @param value value to set the START_DATE
     */
    public void setStartDate(Timestamp value) {
        setAttributeInternal(STARTDATE, value);
    }

    /**
     * Gets the attribute value for UNIT_CD using the alias name UnitCd.
     * @return the UNIT_CD
     */
    public String getUnitCd() {
        return (String) getAttributeInternal(UNITCD);
    }

    /**
     * Sets <code>value</code> as attribute value for UNIT_CD using the alias name UnitCd.
     * @param value value to set the UNIT_CD
     */
    public void setUnitCd(String value) {
        setAttributeInternal(UNITCD, value);
    }

    /**
     * Gets the attribute value for NAME using the alias name Name.
     * @return the NAME
     */
    public String getName() {
        return (String) getAttributeInternal(NAME);
    }

    /**
     * Sets <code>value</code> as attribute value for NAME using the alias name Name.
     * @param value value to set the NAME
     */
    public void setName(String value) {
        setAttributeInternal(NAME, value);
    }

    /**
     * Gets the attribute value for CODE using the alias name Code.
     * @return the CODE
     */
    public String getCode() {
        return (String) getAttributeInternal(CODE);
    }

    /**
     * Sets <code>value</code> as attribute value for CODE using the alias name Code.
     * @param value value to set the CODE
     */
    public void setCode(String value) {
        setAttributeInternal(CODE, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber1.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber1() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER1);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber1.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber1(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER1, value);
    }

    /**
     * Gets the attribute value for EMP_FIRST_NAME using the alias name EmpFirstName.
     * @return the EMP_FIRST_NAME
     */
    public String getEmpFirstName() {
            if((getAttributeInternal(EMPFIRSTNAME)!=null) && (getAttributeInternal(EMPLASTNAME)!=null)){
                return (String) getAttributeInternal(EMPFIRSTNAME) + " " + (String) getAttributeInternal(EMPLASTNAME);
            }else{
            return (String) getAttributeInternal(EMPFIRSTNAME);
            }
        } 

    /**
     * Sets <code>value</code> as attribute value for EMP_FIRST_NAME using the alias name EmpFirstName.
     * @param value value to set the EMP_FIRST_NAME
     */
    public void setEmpFirstName(String value) {
        setAttributeInternal(EMPFIRSTNAME, value);
    }

    /**
     * Gets the attribute value for EMP_LAST_NAME using the alias name EmpLastName.
     * @return the EMP_LAST_NAME
     */
    public String getEmpLastName() {
        return (String) getAttributeInternal(EMPLASTNAME);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_LAST_NAME using the alias name EmpLastName.
     * @param value value to set the EMP_LAST_NAME
     */
    public void setEmpLastName(String value) {
        setAttributeInternal(EMPLASTNAME, value);
    }

    /**
     * Gets the attribute value for EMP_NUMBER using the alias name EmpNumber.
     * @return the EMP_NUMBER
     */
    public String getEmpNumber() {
        return (String) getAttributeInternal(EMPNUMBER);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_NUMBER using the alias name EmpNumber.
     * @param value value to set the EMP_NUMBER
     */
    public void setEmpNumber(String value) {
        setAttributeInternal(EMPNUMBER, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber2.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber2() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER2);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber2.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber2(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER2, value);
    }

    /**
     * Gets the attribute value for the calculated attribute EditTrans.
     * @return the EditTrans
     */
    public Integer getEditTrans() {
        byte entityState = this.getEntity(0).getEntityState();
        return new Integer(entityState);
        //return (Integer) getAttributeInternal(EDITTRANS);
        
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute EditTrans.
     * @param value value to set the  EditTrans
     */
    public void setEditTrans(Integer value) {
        setAttributeInternal(EDITTRANS, value);
    }

    /**
     * Gets the attribute value for DESCRIPTION using the alias name Description.
     * @return the DESCRIPTION
     */
    public String getDescription() {
        return (String) getAttributeInternal(DESCRIPTION);
    }

    /**
     * Sets <code>value</code> as attribute value for DESCRIPTION using the alias name Description.
     * @param value value to set the DESCRIPTION
     */
    public void setDescription(String value) {
        setAttributeInternal(DESCRIPTION, value);
    }

    /**
     * Gets the attribute value for LEAVE_MAST_CODE_ID using the alias name LeaveMastCodeId.
     * @return the LEAVE_MAST_CODE_ID
     */
    public Long getLeaveMastCodeId() {
        return (Long) getAttributeInternal(LEAVEMASTCODEID);
    }

    /**
     * Sets <code>value</code> as attribute value for LEAVE_MAST_CODE_ID using the alias name LeaveMastCodeId.
     * @param value value to set the LEAVE_MAST_CODE_ID
     */
    public void setLeaveMastCodeId(Long value) {
        setAttributeInternal(LEAVEMASTCODEID, value);
    }

    /**
     * Gets the attribute value for EMPMEMB_EMP_MASTER_EMP_NUMBER using the alias name EmpmembEmpMasterEmpNumber1.
     * @return the EMPMEMB_EMP_MASTER_EMP_NUMBER
     */
    public String getEmpmembEmpMasterEmpNumber1() {
        return (String) getAttributeInternal(EMPMEMBEMPMASTEREMPNUMBER1);
    }

    /**
     * Sets <code>value</code> as attribute value for EMPMEMB_EMP_MASTER_EMP_NUMBER using the alias name EmpmembEmpMasterEmpNumber1.
     * @param value value to set the EMPMEMB_EMP_MASTER_EMP_NUMBER
     */
    public void setEmpmembEmpMasterEmpNumber1(String value) {
        setAttributeInternal(EMPMEMBEMPMASTEREMPNUMBER1, value);
    }

    /**
     * Gets the view accessor <code>RowSet</code> UnitVO1.
     */
    public RowSet getUnitVO1() {
        return (RowSet) getAttributeInternal(UNITVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> EmpViewVVO1.
     */
    public RowSet getEmpViewVVO1() {
        return (RowSet) getAttributeInternal(EMPVIEWVVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> LeaveMasterHeaderVO1.
     */
    public RowSet getLeaveMasterHeaderVO1() {
        return (RowSet) getAttributeInternal(LEAVEMASTERHEADERVO1);
    }

}

