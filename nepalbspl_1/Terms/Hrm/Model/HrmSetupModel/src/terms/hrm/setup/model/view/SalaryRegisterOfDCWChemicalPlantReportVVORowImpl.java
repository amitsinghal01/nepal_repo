package terms.hrm.setup.model.view;

import oracle.jbo.Row;
import oracle.jbo.RowSet;
import oracle.jbo.server.ViewRowImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Wed Oct 30 15:37:45 IST 2019
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class SalaryRegisterOfDCWChemicalPlantReportVVORowImpl extends ViewRowImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        Dummy,
        unitCd,
        unitName,
        month,
        year,
        lvlCd,
        lvlDescrp,
        UnitVO1,
        UnitVO2,
        SecControlVO1,
        SalaryDSWLevelCodeVVO1,
        LevelMasterVO1;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int DUMMY = AttributesEnum.Dummy.index();
    public static final int UNITCD = AttributesEnum.unitCd.index();
    public static final int UNITNAME = AttributesEnum.unitName.index();
    public static final int MONTH = AttributesEnum.month.index();
    public static final int YEAR = AttributesEnum.year.index();
    public static final int LVLCD = AttributesEnum.lvlCd.index();
    public static final int LVLDESCRP = AttributesEnum.lvlDescrp.index();
    public static final int UNITVO1 = AttributesEnum.UnitVO1.index();
    public static final int UNITVO2 = AttributesEnum.UnitVO2.index();
    public static final int SECCONTROLVO1 = AttributesEnum.SecControlVO1.index();
    public static final int SALARYDSWLEVELCODEVVO1 = AttributesEnum.SalaryDSWLevelCodeVVO1.index();
    public static final int LEVELMASTERVO1 = AttributesEnum.LevelMasterVO1.index();

    /**
     * This is the default constructor (do not remove).
     */
    public SalaryRegisterOfDCWChemicalPlantReportVVORowImpl() {
    }

    /**
     * Gets the attribute value for the calculated attribute Dummy.
     * @return the Dummy
     */
    public String getDummy() {
        return (String) getAttributeInternal(DUMMY);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute Dummy.
     * @param value value to set the  Dummy
     */
    public void setDummy(String value) {
        setAttributeInternal(DUMMY, value);
    }

    /**
     * Gets the attribute value for the calculated attribute unitCd.
     * @return the unitCd
     */
    public String getunitCd() {
        return (String) getAttributeInternal(UNITCD);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute unitCd.
     * @param value value to set the  unitCd
     */
    public void setunitCd(String value) {
        setAttributeInternal(UNITCD, value);
    }

    /**
     * Gets the attribute value for the calculated attribute unitName.
     * @return the unitName
     */
    public String getunitName() {
        if(getunitCd()!=null){
                   Row[] row=this.getUnitVO2().getFilteredRows("Code", getunitCd());
                   if(row.length>0){
                       System.out.println(row.length+" Row aai hai!...");
                       if(row[0].getAttribute("Name")!=null)
                           return row[0].getAttribute("Name").toString();
                   }
               }
        return (String) getAttributeInternal(UNITNAME);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute unitName.
     * @param value value to set the  unitName
     */
    public void setunitName(String value) {
        setAttributeInternal(UNITNAME, value);
    }

    /**
     * Gets the attribute value for the calculated attribute month.
     * @return the month
     */
    public String getmonth() {
        return (String) getAttributeInternal(MONTH);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute month.
     * @param value value to set the  month
     */
    public void setmonth(String value) {
        setAttributeInternal(MONTH, value);
    }

    /**
     * Gets the attribute value for the calculated attribute year.
     * @return the year
     */
    public String getyear() {
        return (String) getAttributeInternal(YEAR);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute year.
     * @param value value to set the  year
     */
    public void setyear(String value) {
        setAttributeInternal(YEAR, value);
    }

    /**
     * Gets the attribute value for the calculated attribute lvlCd.
     * @return the lvlCd
     */
    public String getlvlCd() {
        return (String) getAttributeInternal(LVLCD);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute lvlCd.
     * @param value value to set the  lvlCd
     */
    public void setlvlCd(String value) {
        setAttributeInternal(LVLCD, value);
    }

    /**
     * Gets the attribute value for the calculated attribute lvlDescrp.
     * @return the lvlDescrp
     */
    public String getlvlDescrp() {
        return (String) getAttributeInternal(LVLDESCRP);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute lvlDescrp.
     * @param value value to set the  lvlDescrp
     */
    public void setlvlDescrp(String value) {
        setAttributeInternal(LVLDESCRP, value);
    }

    /**
     * Gets the view accessor <code>RowSet</code> UnitVO1.
     */
    public RowSet getUnitVO1() {
        return (RowSet) getAttributeInternal(UNITVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> UnitVO2.
     */
    public RowSet getUnitVO2() {
        return (RowSet) getAttributeInternal(UNITVO2);
    }

    /**
     * Gets the view accessor <code>RowSet</code> SecControlVO1.
     */
    public RowSet getSecControlVO1() {
        return (RowSet) getAttributeInternal(SECCONTROLVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> SalaryDSWLevelCodeVVO1.
     */
    public RowSet getSalaryDSWLevelCodeVVO1() {
        return (RowSet) getAttributeInternal(SALARYDSWLEVELCODEVVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> LevelMasterVO1.
     */
    public RowSet getLevelMasterVO1() {
        return (RowSet) getAttributeInternal(LEVELMASTERVO1);
    }

}

