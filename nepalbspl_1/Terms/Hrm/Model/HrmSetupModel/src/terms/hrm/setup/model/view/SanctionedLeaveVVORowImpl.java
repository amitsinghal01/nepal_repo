package terms.hrm.setup.model.view;

import oracle.jbo.Row;
import oracle.jbo.RowSet;
import oracle.jbo.domain.Date;
import oracle.jbo.server.ViewRowImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Tue Nov 26 13:22:02 IST 2019
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class SanctionedLeaveVVORowImpl extends ViewRowImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        Dummy,
        UnitCode,
        UnitName,
        Shift,
        todate,
        fromdate,
        UnitVO1,
        UnitVO2;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }

    public static final int DUMMY = AttributesEnum.Dummy.index();
    public static final int UNITCODE = AttributesEnum.UnitCode.index();
    public static final int UNITNAME = AttributesEnum.UnitName.index();
    public static final int SHIFT = AttributesEnum.Shift.index();
    public static final int TODATE = AttributesEnum.todate.index();
    public static final int FROMDATE = AttributesEnum.fromdate.index();
    public static final int UNITVO1 = AttributesEnum.UnitVO1.index();
    public static final int UNITVO2 = AttributesEnum.UnitVO2.index();

    /**
     * This is the default constructor (do not remove).
     */
    public SanctionedLeaveVVORowImpl() {
    }

    /**
     * Gets the attribute value for the calculated attribute Dummy.
     * @return the Dummy
     */
    public String getDummy() {
        return (String) getAttributeInternal(DUMMY);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute Dummy.
     * @param value value to set the  Dummy
     */
    public void setDummy(String value) {
        setAttributeInternal(DUMMY, value);
    }

    /**
     * Gets the attribute value for the calculated attribute UnitCode.
     * @return the UnitCode
     */
    public String getUnitCode() {
        return (String) getAttributeInternal(UNITCODE);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute UnitCode.
     * @param value value to set the  UnitCode
     */
    public void setUnitCode(String value) {
        setAttributeInternal(UNITCODE, value);
    }

    /**
     * Gets the attribute value for the calculated attribute UnitName.
     * @return the UnitName
     */
    public String getUnitName() {
        if(getUnitCode()!=null){
            
                           Row[] row=this.getUnitVO1().getFilteredRows("Code", getUnitCode());
                           if(row.length>0){
                               System.out.println(row.length+" Row aai hai!...");
                               if(row[0].getAttribute("Name")!=null)
                                   return row[0].getAttribute("Name").toString();
                           }
                       }
        return (String) getAttributeInternal(UNITNAME);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute UnitName.
     * @param value value to set the  UnitName
     */
    public void setUnitName(String value) {
        setAttributeInternal(UNITNAME, value);
    }

    /**
     * Gets the attribute value for the calculated attribute Shift.
     * @return the Shift
     */
    public String getShift() {
        return (String) getAttributeInternal(SHIFT);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute Shift.
     * @param value value to set the  Shift
     */
    public void setShift(String value) {
        setAttributeInternal(SHIFT, value);
    }

    /**
     * Gets the attribute value for the calculated attribute todate.
     * @return the todate
     */
    public Date gettodate() {
        return (Date) getAttributeInternal(TODATE);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute todate.
     * @param value value to set the  todate
     */
    public void settodate(Date value) {
        setAttributeInternal(TODATE, value);
    }

    /**
     * Gets the attribute value for the calculated attribute fromdate.
     * @return the fromdate
     */
    public Date getfromdate() {
        return (Date) getAttributeInternal(FROMDATE);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute fromdate.
     * @param value value to set the  fromdate
     */
    public void setfromdate(Date value) {
        setAttributeInternal(FROMDATE, value);
    }

    /**
     * Gets the view accessor <code>RowSet</code> UnitVO1.
     */
    public RowSet getUnitVO1() {
        return (RowSet) getAttributeInternal(UNITVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> UnitVO2.
     */
    public RowSet getUnitVO2() {
        return (RowSet) getAttributeInternal(UNITVO2);
    }
}

