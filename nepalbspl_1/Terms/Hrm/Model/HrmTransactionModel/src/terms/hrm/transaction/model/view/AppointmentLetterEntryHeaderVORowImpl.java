package terms.hrm.transaction.model.view;

import oracle.jbo.RowIterator;
import oracle.jbo.RowSet;
import oracle.jbo.domain.Date;
import oracle.jbo.domain.Number;
import oracle.jbo.domain.Timestamp;
import oracle.jbo.server.AttributeDefImpl;
import oracle.jbo.server.EntityImpl;
import oracle.jbo.server.ViewRowImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Tue Apr 02 16:13:23 IST 2019
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class AppointmentLetterEntryHeaderVORowImpl extends ViewRowImpl {

    public static final int ENTITY_APPOINTMENTLETTERENTRYHEADER1 = 0;
    public static final int ENTITY_UNITEO = 1;
    public static final int ENTITY_BIODATAENTRYEO = 2;
    public static final int ENTITY_CATEGORYMASTERHEADEREO = 3;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        AppointLetterMastId {
            public Object get(AppointmentLetterEntryHeaderVORowImpl obj) {
                return obj.getAppointLetterMastId();
            }

            public void put(AppointmentLetterEntryHeaderVORowImpl obj, Object value) {
                obj.setAppointLetterMastId((Long) value);
            }
        }
        ,
        CategoryCd {
            public Object get(AppointmentLetterEntryHeaderVORowImpl obj) {
                return obj.getCategoryCd();
            }

            public void put(AppointmentLetterEntryHeaderVORowImpl obj, Object value) {
                obj.setCategoryCd((String) value);
            }
        }
        ,
        CreatedBy {
            public Object get(AppointmentLetterEntryHeaderVORowImpl obj) {
                return obj.getCreatedBy();
            }

            public void put(AppointmentLetterEntryHeaderVORowImpl obj, Object value) {
                obj.setCreatedBy((String) value);
            }
        }
        ,
        CreationDate {
            public Object get(AppointmentLetterEntryHeaderVORowImpl obj) {
                return obj.getCreationDate();
            }

            public void put(AppointmentLetterEntryHeaderVORowImpl obj, Object value) {
                obj.setCreationDate((Timestamp) value);
            }
        }
        ,
        DesigCd {
            public Object get(AppointmentLetterEntryHeaderVORowImpl obj) {
                return obj.getDesigCd();
            }

            public void put(AppointmentLetterEntryHeaderVORowImpl obj, Object value) {
                obj.setDesigCd((String) value);
            }
        }
        ,
        EntryDt {
            public Object get(AppointmentLetterEntryHeaderVORowImpl obj) {
                return obj.getEntryDt();
            }

            public void put(AppointmentLetterEntryHeaderVORowImpl obj, Object value) {
                obj.setEntryDt((Date) value);
            }
        }
        ,
        EntryNo {
            public Object get(AppointmentLetterEntryHeaderVORowImpl obj) {
                return obj.getEntryNo();
            }

            public void put(AppointmentLetterEntryHeaderVORowImpl obj, Object value) {
                obj.setEntryNo((String) value);
            }
        }
        ,
        IntvEntryNo {
            public Object get(AppointmentLetterEntryHeaderVORowImpl obj) {
                return obj.getIntvEntryNo();
            }

            public void put(AppointmentLetterEntryHeaderVORowImpl obj, Object value) {
                obj.setIntvEntryNo((String) value);
            }
        }
        ,
        LastUpdateDate {
            public Object get(AppointmentLetterEntryHeaderVORowImpl obj) {
                return obj.getLastUpdateDate();
            }

            public void put(AppointmentLetterEntryHeaderVORowImpl obj, Object value) {
                obj.setLastUpdateDate((Timestamp) value);
            }
        }
        ,
        LastUpdatedBy {
            public Object get(AppointmentLetterEntryHeaderVORowImpl obj) {
                return obj.getLastUpdatedBy();
            }

            public void put(AppointmentLetterEntryHeaderVORowImpl obj, Object value) {
                obj.setLastUpdatedBy((String) value);
            }
        }
        ,
        LevelCode {
            public Object get(AppointmentLetterEntryHeaderVORowImpl obj) {
                return obj.getLevelCode();
            }

            public void put(AppointmentLetterEntryHeaderVORowImpl obj, Object value) {
                obj.setLevelCode((String) value);
            }
        }
        ,
        ObjectVersionNumber {
            public Object get(AppointmentLetterEntryHeaderVORowImpl obj) {
                return obj.getObjectVersionNumber();
            }

            public void put(AppointmentLetterEntryHeaderVORowImpl obj, Object value) {
                obj.setObjectVersionNumber((Number) value);
            }
        }
        ,
        PayScale {
            public Object get(AppointmentLetterEntryHeaderVORowImpl obj) {
                return obj.getPayScale();
            }

            public void put(AppointmentLetterEntryHeaderVORowImpl obj, Object value) {
                obj.setPayScale((String) value);
            }
        }
        ,
        ProbationaryPeriod {
            public Object get(AppointmentLetterEntryHeaderVORowImpl obj) {
                return obj.getProbationaryPeriod();
            }

            public void put(AppointmentLetterEntryHeaderVORowImpl obj, Object value) {
                obj.setProbationaryPeriod((String) value);
            }
        }
        ,
        RefLetterDt {
            public Object get(AppointmentLetterEntryHeaderVORowImpl obj) {
                return obj.getRefLetterDt();
            }

            public void put(AppointmentLetterEntryHeaderVORowImpl obj, Object value) {
                obj.setRefLetterDt((Date) value);
            }
        }
        ,
        RefLetterNo {
            public Object get(AppointmentLetterEntryHeaderVORowImpl obj) {
                return obj.getRefLetterNo();
            }

            public void put(AppointmentLetterEntryHeaderVORowImpl obj, Object value) {
                obj.setRefLetterNo((String) value);
            }
        }
        ,
        UnitCd {
            public Object get(AppointmentLetterEntryHeaderVORowImpl obj) {
                return obj.getUnitCd();
            }

            public void put(AppointmentLetterEntryHeaderVORowImpl obj, Object value) {
                obj.setUnitCd((String) value);
            }
        }
        ,
        Name {
            public Object get(AppointmentLetterEntryHeaderVORowImpl obj) {
                return obj.getName();
            }

            public void put(AppointmentLetterEntryHeaderVORowImpl obj, Object value) {
                obj.setName((String) value);
            }
        }
        ,
        Code {
            public Object get(AppointmentLetterEntryHeaderVORowImpl obj) {
                return obj.getCode();
            }

            public void put(AppointmentLetterEntryHeaderVORowImpl obj, Object value) {
                obj.setCode((String) value);
            }
        }
        ,
        ObjectVersionNumber1 {
            public Object get(AppointmentLetterEntryHeaderVORowImpl obj) {
                return obj.getObjectVersionNumber1();
            }

            public void put(AppointmentLetterEntryHeaderVORowImpl obj, Object value) {
                obj.setObjectVersionNumber1((Integer) value);
            }
        }
        ,
        PresentAdd1 {
            public Object get(AppointmentLetterEntryHeaderVORowImpl obj) {
                return obj.getPresentAdd1();
            }

            public void put(AppointmentLetterEntryHeaderVORowImpl obj, Object value) {
                obj.setPresentAdd1((String) value);
            }
        }
        ,
        BioBankId {
            public Object get(AppointmentLetterEntryHeaderVORowImpl obj) {
                return obj.getBioBankId();
            }

            public void put(AppointmentLetterEntryHeaderVORowImpl obj, Object value) {
                obj.setBioBankId((Long) value);
            }
        }
        ,
        LastName {
            public Object get(AppointmentLetterEntryHeaderVORowImpl obj) {
                return obj.getLastName();
            }

            public void put(AppointmentLetterEntryHeaderVORowImpl obj, Object value) {
                obj.setLastName((String) value);
            }
        }
        ,
        FatherName {
            public Object get(AppointmentLetterEntryHeaderVORowImpl obj) {
                return obj.getFatherName();
            }

            public void put(AppointmentLetterEntryHeaderVORowImpl obj, Object value) {
                obj.setFatherName((String) value);
            }
        }
        ,
        FirstName {
            public Object get(AppointmentLetterEntryHeaderVORowImpl obj) {
                return obj.getFirstName();
            }

            public void put(AppointmentLetterEntryHeaderVORowImpl obj, Object value) {
                obj.setFirstName((String) value);
            }
        }
        ,
        LvlMastLvlMastLevel {
            public Object get(AppointmentLetterEntryHeaderVORowImpl obj) {
                return obj.getLvlMastLvlMastLevel();
            }

            public void put(AppointmentLetterEntryHeaderVORowImpl obj, Object value) {
                obj.setLvlMastLvlMastLevel((String) value);
            }
        }
        ,
        Description {
            public Object get(AppointmentLetterEntryHeaderVORowImpl obj) {
                return obj.getDescription();
            }

            public void put(AppointmentLetterEntryHeaderVORowImpl obj, Object value) {
                obj.setDescription((String) value);
            }
        }
        ,
        CategoryMastId {
            public Object get(AppointmentLetterEntryHeaderVORowImpl obj) {
                return obj.getCategoryMastId();
            }

            public void put(AppointmentLetterEntryHeaderVORowImpl obj, Object value) {
                obj.setCategoryMastId((Number) value);
            }
        }
        ,
        IntvEntryNo1 {
            public Object get(AppointmentLetterEntryHeaderVORowImpl obj) {
                return obj.getIntvEntryNo1();
            }

            public void put(AppointmentLetterEntryHeaderVORowImpl obj, Object value) {
                obj.setIntvEntryNo1((String) value);
            }
        }
        ,
        EmpRefNo {
            public Object get(AppointmentLetterEntryHeaderVORowImpl obj) {
                return obj.getEmpRefNo();
            }

            public void put(AppointmentLetterEntryHeaderVORowImpl obj, Object value) {
                obj.setEmpRefNo((String) value);
            }
        }
        ,
        First {
            public Object get(AppointmentLetterEntryHeaderVORowImpl obj) {
                return obj.getFirst();
            }

            public void put(AppointmentLetterEntryHeaderVORowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        LastName1 {
            public Object get(AppointmentLetterEntryHeaderVORowImpl obj) {
                return obj.getLastName1();
            }

            public void put(AppointmentLetterEntryHeaderVORowImpl obj, Object value) {
                obj.setLastName1((String) value);
            }
        }
        ,
        EditTrans {
            public Object get(AppointmentLetterEntryHeaderVORowImpl obj) {
                return obj.getEditTrans();
            }

            public void put(AppointmentLetterEntryHeaderVORowImpl obj, Object value) {
                obj.setEditTrans((Integer) value);
            }
        }
        ,
        DesigDesc {
            public Object get(AppointmentLetterEntryHeaderVORowImpl obj) {
                return obj.getDesigDesc();
            }

            public void put(AppointmentLetterEntryHeaderVORowImpl obj, Object value) {
                obj.setDesigDesc((String) value);
            }
        }
        ,
        LevelDesc {
            public Object get(AppointmentLetterEntryHeaderVORowImpl obj) {
                return obj.getLevelDesc();
            }

            public void put(AppointmentLetterEntryHeaderVORowImpl obj, Object value) {
                obj.setLevelDesc((String) value);
            }
        }
        ,
        AppointmentLetterEntryDetailVO {
            public Object get(AppointmentLetterEntryHeaderVORowImpl obj) {
                return obj.getAppointmentLetterEntryDetailVO();
            }

            public void put(AppointmentLetterEntryHeaderVORowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        UnitVO1 {
            public Object get(AppointmentLetterEntryHeaderVORowImpl obj) {
                return obj.getUnitVO1();
            }

            public void put(AppointmentLetterEntryHeaderVORowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        CategoryMasterHeaderVO1 {
            public Object get(AppointmentLetterEntryHeaderVORowImpl obj) {
                return obj.getCategoryMasterHeaderVO1();
            }

            public void put(AppointmentLetterEntryHeaderVORowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        AppointmentLetterIntvNoVVO1 {
            public Object get(AppointmentLetterEntryHeaderVORowImpl obj) {
                return obj.getAppointmentLetterIntvNoVVO1();
            }

            public void put(AppointmentLetterEntryHeaderVORowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ;
        static AttributesEnum[] vals = null;
        ;
        private static final int firstIndex = 0;

        public abstract Object get(AppointmentLetterEntryHeaderVORowImpl object);

        public abstract void put(AppointmentLetterEntryHeaderVORowImpl object, Object value);

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int APPOINTLETTERMASTID = AttributesEnum.AppointLetterMastId.index();
    public static final int CATEGORYCD = AttributesEnum.CategoryCd.index();
    public static final int CREATEDBY = AttributesEnum.CreatedBy.index();
    public static final int CREATIONDATE = AttributesEnum.CreationDate.index();
    public static final int DESIGCD = AttributesEnum.DesigCd.index();
    public static final int ENTRYDT = AttributesEnum.EntryDt.index();
    public static final int ENTRYNO = AttributesEnum.EntryNo.index();
    public static final int INTVENTRYNO = AttributesEnum.IntvEntryNo.index();
    public static final int LASTUPDATEDATE = AttributesEnum.LastUpdateDate.index();
    public static final int LASTUPDATEDBY = AttributesEnum.LastUpdatedBy.index();
    public static final int LEVELCODE = AttributesEnum.LevelCode.index();
    public static final int OBJECTVERSIONNUMBER = AttributesEnum.ObjectVersionNumber.index();
    public static final int PAYSCALE = AttributesEnum.PayScale.index();
    public static final int PROBATIONARYPERIOD = AttributesEnum.ProbationaryPeriod.index();
    public static final int REFLETTERDT = AttributesEnum.RefLetterDt.index();
    public static final int REFLETTERNO = AttributesEnum.RefLetterNo.index();
    public static final int UNITCD = AttributesEnum.UnitCd.index();
    public static final int NAME = AttributesEnum.Name.index();
    public static final int CODE = AttributesEnum.Code.index();
    public static final int OBJECTVERSIONNUMBER1 = AttributesEnum.ObjectVersionNumber1.index();
    public static final int PRESENTADD1 = AttributesEnum.PresentAdd1.index();
    public static final int BIOBANKID = AttributesEnum.BioBankId.index();
    public static final int LASTNAME = AttributesEnum.LastName.index();
    public static final int FATHERNAME = AttributesEnum.FatherName.index();
    public static final int FIRSTNAME = AttributesEnum.FirstName.index();
    public static final int LVLMASTLVLMASTLEVEL = AttributesEnum.LvlMastLvlMastLevel.index();
    public static final int DESCRIPTION = AttributesEnum.Description.index();
    public static final int CATEGORYMASTID = AttributesEnum.CategoryMastId.index();
    public static final int INTVENTRYNO1 = AttributesEnum.IntvEntryNo1.index();
    public static final int EMPREFNO = AttributesEnum.EmpRefNo.index();
    public static final int FIRST = AttributesEnum.First.index();
    public static final int LASTNAME1 = AttributesEnum.LastName1.index();
    public static final int EDITTRANS = AttributesEnum.EditTrans.index();
    public static final int DESIGDESC = AttributesEnum.DesigDesc.index();
    public static final int LEVELDESC = AttributesEnum.LevelDesc.index();
    public static final int APPOINTMENTLETTERENTRYDETAILVO = AttributesEnum.AppointmentLetterEntryDetailVO.index();
    public static final int UNITVO1 = AttributesEnum.UnitVO1.index();
    public static final int CATEGORYMASTERHEADERVO1 = AttributesEnum.CategoryMasterHeaderVO1.index();
    public static final int APPOINTMENTLETTERINTVNOVVO1 = AttributesEnum.AppointmentLetterIntvNoVVO1.index();

    /**
     * This is the default constructor (do not remove).
     */
    public AppointmentLetterEntryHeaderVORowImpl() {
    }

    /**
     * Gets AppointmentLetterEntryHeader1 entity object.
     * @return the AppointmentLetterEntryHeader1
     */
    public EntityImpl getAppointmentLetterEntryHeader1() {
        return (EntityImpl) getEntity(ENTITY_APPOINTMENTLETTERENTRYHEADER1);
    }

    /**
     * Gets UnitEO entity object.
     * @return the UnitEO
     */
    public EntityImpl getUnitEO() {
        return (EntityImpl) getEntity(ENTITY_UNITEO);
    }

    /**
     * Gets BioDataEntryEO entity object.
     * @return the BioDataEntryEO
     */
    public EntityImpl getBioDataEntryEO() {
        return (EntityImpl) getEntity(ENTITY_BIODATAENTRYEO);
    }


    /**
     * Gets CategoryMasterHeaderEO entity object.
     * @return the CategoryMasterHeaderEO
     */
    public EntityImpl getCategoryMasterHeaderEO() {
        return (EntityImpl) getEntity(ENTITY_CATEGORYMASTERHEADEREO);
    }

    /**
     * Gets the attribute value for APPOINT_LETTER_MAST_ID using the alias name AppointLetterMastId.
     * @return the APPOINT_LETTER_MAST_ID
     */
    public Long getAppointLetterMastId() {
        return (Long) getAttributeInternal(APPOINTLETTERMASTID);
    }

    /**
     * Sets <code>value</code> as attribute value for APPOINT_LETTER_MAST_ID using the alias name AppointLetterMastId.
     * @param value value to set the APPOINT_LETTER_MAST_ID
     */
    public void setAppointLetterMastId(Long value) {
        setAttributeInternal(APPOINTLETTERMASTID, value);
    }

    /**
     * Gets the attribute value for CATEGORY_CD using the alias name CategoryCd.
     * @return the CATEGORY_CD
     */
    public String getCategoryCd() {
        return (String) getAttributeInternal(CATEGORYCD);
    }

    /**
     * Sets <code>value</code> as attribute value for CATEGORY_CD using the alias name CategoryCd.
     * @param value value to set the CATEGORY_CD
     */
    public void setCategoryCd(String value) {
        setAttributeInternal(CATEGORYCD, value);
    }

    /**
     * Gets the attribute value for CREATED_BY using the alias name CreatedBy.
     * @return the CREATED_BY
     */
    public String getCreatedBy() {
        return (String) getAttributeInternal(CREATEDBY);
    }

    /**
     * Sets <code>value</code> as attribute value for CREATED_BY using the alias name CreatedBy.
     * @param value value to set the CREATED_BY
     */
    public void setCreatedBy(String value) {
        setAttributeInternal(CREATEDBY, value);
    }

    /**
     * Gets the attribute value for CREATION_DATE using the alias name CreationDate.
     * @return the CREATION_DATE
     */
    public Timestamp getCreationDate() {
        return (Timestamp) getAttributeInternal(CREATIONDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for CREATION_DATE using the alias name CreationDate.
     * @param value value to set the CREATION_DATE
     */
    public void setCreationDate(Timestamp value) {
        setAttributeInternal(CREATIONDATE, value);
    }

    /**
     * Gets the attribute value for DESIG_CD using the alias name DesigCd.
     * @return the DESIG_CD
     */
    public String getDesigCd() {
        return (String) getAttributeInternal(DESIGCD);
    }

    /**
     * Sets <code>value</code> as attribute value for DESIG_CD using the alias name DesigCd.
     * @param value value to set the DESIG_CD
     */
    public void setDesigCd(String value) {
        
        setAttributeInternal(DESIGCD, value);
    }

    /**
     * Gets the attribute value for ENTRY_DT using the alias name EntryDt.
     * @return the ENTRY_DT
     */
    public Date getEntryDt() {
        return (Date) getAttributeInternal(ENTRYDT);
    }

    /**
     * Sets <code>value</code> as attribute value for ENTRY_DT using the alias name EntryDt.
     * @param value value to set the ENTRY_DT
     */
    public void setEntryDt(Date value) {
        setAttributeInternal(ENTRYDT, value);
    }

    /**
     * Gets the attribute value for ENTRY_NO using the alias name EntryNo.
     * @return the ENTRY_NO
     */
    public String getEntryNo() {
        return (String) getAttributeInternal(ENTRYNO);
    }

    /**
     * Sets <code>value</code> as attribute value for ENTRY_NO using the alias name EntryNo.
     * @param value value to set the ENTRY_NO
     */
    public void setEntryNo(String value) {
        setAttributeInternal(ENTRYNO, value);
    }

    /**
     * Gets the attribute value for INTV_ENTRY_NO using the alias name IntvEntryNo.
     * @return the INTV_ENTRY_NO
     */
    public String getIntvEntryNo() {
        return (String) getAttributeInternal(INTVENTRYNO);
    }

    /**
     * Sets <code>value</code> as attribute value for INTV_ENTRY_NO using the alias name IntvEntryNo.
     * @param value value to set the INTV_ENTRY_NO
     */
    public void setIntvEntryNo(String value) {
        setAttributeInternal(INTVENTRYNO, value);
    }

    /**
     * Gets the attribute value for LAST_UPDATE_DATE using the alias name LastUpdateDate.
     * @return the LAST_UPDATE_DATE
     */
    public oracle.jbo.domain.Timestamp getLastUpdateDate() {
        return (oracle.jbo.domain.Timestamp) getAttributeInternal(LASTUPDATEDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for LAST_UPDATE_DATE using the alias name LastUpdateDate.
     * @param value value to set the LAST_UPDATE_DATE
     */
    public void setLastUpdateDate(oracle.jbo.domain.Timestamp value) {
        setAttributeInternal(LASTUPDATEDATE, value);
    }

    /**
     * Gets the attribute value for LAST_UPDATED_BY using the alias name LastUpdatedBy.
     * @return the LAST_UPDATED_BY
     */
    public String getLastUpdatedBy() {
        return (String) getAttributeInternal(LASTUPDATEDBY);
    }

    /**
     * Sets <code>value</code> as attribute value for LAST_UPDATED_BY using the alias name LastUpdatedBy.
     * @param value value to set the LAST_UPDATED_BY
     */
    public void setLastUpdatedBy(String value) {
        setAttributeInternal(LASTUPDATEDBY, value);
    }

    /**
     * Gets the attribute value for LEVEL_CODE using the alias name LevelCode.
     * @return the LEVEL_CODE
     */
    public String getLevelCode() {
        return (String) getAttributeInternal(LEVELCODE);
    }

    /**
     * Sets <code>value</code> as attribute value for LEVEL_CODE using the alias name LevelCode.
     * @param value value to set the LEVEL_CODE
     */
    public void setLevelCode(String value) {
        
        setAttributeInternal(LEVELCODE, value);
    
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Number getObjectVersionNumber() {
        return (Number) getAttributeInternal(OBJECTVERSIONNUMBER);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber(Number value) {
        setAttributeInternal(OBJECTVERSIONNUMBER, value);
    }

    /**
     * Gets the attribute value for PAY_SCALE using the alias name PayScale.
     * @return the PAY_SCALE
     */
    public String getPayScale() {
        return (String) getAttributeInternal(PAYSCALE);
    }

    /**
     * Sets <code>value</code> as attribute value for PAY_SCALE using the alias name PayScale.
     * @param value value to set the PAY_SCALE
     */
    public void setPayScale(String value) {
        setAttributeInternal(PAYSCALE, value);
    }

    /**
     * Gets the attribute value for PROBATIONARY_PERIOD using the alias name ProbationaryPeriod.
     * @return the PROBATIONARY_PERIOD
     */
    public String getProbationaryPeriod() {
        return (String) getAttributeInternal(PROBATIONARYPERIOD);
    }

    /**
     * Sets <code>value</code> as attribute value for PROBATIONARY_PERIOD using the alias name ProbationaryPeriod.
     * @param value value to set the PROBATIONARY_PERIOD
     */
    public void setProbationaryPeriod(String value) {
        setAttributeInternal(PROBATIONARYPERIOD, value);
    }

    /**
     * Gets the attribute value for REF_LETTER_DT using the alias name RefLetterDt.
     * @return the REF_LETTER_DT
     */
    public Date getRefLetterDt() {
        return (Date) getAttributeInternal(REFLETTERDT);
    }

    /**
     * Sets <code>value</code> as attribute value for REF_LETTER_DT using the alias name RefLetterDt.
     * @param value value to set the REF_LETTER_DT
     */
    public void setRefLetterDt(Date value) {
        setAttributeInternal(REFLETTERDT, value);
    }

    /**
     * Gets the attribute value for REF_LETTER_NO using the alias name RefLetterNo.
     * @return the REF_LETTER_NO
     */
    public String getRefLetterNo() {
        return (String) getAttributeInternal(REFLETTERNO);
    }

    /**
     * Sets <code>value</code> as attribute value for REF_LETTER_NO using the alias name RefLetterNo.
     * @param value value to set the REF_LETTER_NO
     */
    public void setRefLetterNo(String value) {
        setAttributeInternal(REFLETTERNO, value);
    }

    /**
     * Gets the attribute value for UNIT_CD using the alias name UnitCd.
     * @return the UNIT_CD
     */
    public String getUnitCd() {
        return (String) getAttributeInternal(UNITCD);
    }

    /**
     * Sets <code>value</code> as attribute value for UNIT_CD using the alias name UnitCd.
     * @param value value to set the UNIT_CD
     */
    public void setUnitCd(String value) {
        setAttributeInternal(UNITCD, value);
    }

    /**
     * Gets the attribute value for NAME using the alias name Name.
     * @return the NAME
     */
    public String getName() {
        return (String) getAttributeInternal(NAME);
    }

    /**
     * Sets <code>value</code> as attribute value for NAME using the alias name Name.
     * @param value value to set the NAME
     */
    public void setName(String value) {
        setAttributeInternal(NAME, value);
    }

    /**
     * Gets the attribute value for CODE using the alias name Code.
     * @return the CODE
     */
    public String getCode() {
        return (String) getAttributeInternal(CODE);
    }

    /**
     * Sets <code>value</code> as attribute value for CODE using the alias name Code.
     * @param value value to set the CODE
     */
    public void setCode(String value) {
        setAttributeInternal(CODE, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber1.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber1() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER1);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber1.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber1(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER1, value);
    }

    /**
     * Gets the attribute value for PRESENT_ADD1 using the alias name PresentAdd1.
     * @return the PRESENT_ADD1
     */
    public String getPresentAdd1() {
        return (String) getAttributeInternal(PRESENTADD1);
    }

    /**
     * Sets <code>value</code> as attribute value for PRESENT_ADD1 using the alias name PresentAdd1.
     * @param value value to set the PRESENT_ADD1
     */
    public void setPresentAdd1(String value) {
        setAttributeInternal(PRESENTADD1, value);
    }

    /**
     * Gets the attribute value for BIO_BANK_ID using the alias name BioBankId.
     * @return the BIO_BANK_ID
     */
    public Long getBioBankId() {
        return (Long) getAttributeInternal(BIOBANKID);
    }

    /**
     * Sets <code>value</code> as attribute value for BIO_BANK_ID using the alias name BioBankId.
     * @param value value to set the BIO_BANK_ID
     */
    public void setBioBankId(Long value) {
        setAttributeInternal(BIOBANKID, value);
    }

    /**
     * Gets the attribute value for LAST_NAME using the alias name LastName.
     * @return the LAST_NAME
     */
    public String getLastName() {
        return (String) getAttributeInternal(LASTNAME);
    }

    /**
     * Sets <code>value</code> as attribute value for LAST_NAME using the alias name LastName.
     * @param value value to set the LAST_NAME
     */
    public void setLastName(String value) {
        setAttributeInternal(LASTNAME, value);
    }

    /**
     * Gets the attribute value for FATHER_NAME using the alias name FatherName.
     * @return the FATHER_NAME
     */
    public String getFatherName() {
        return (String) getAttributeInternal(FATHERNAME);
    }

    /**
     * Sets <code>value</code> as attribute value for FATHER_NAME using the alias name FatherName.
     * @param value value to set the FATHER_NAME
     */
    public void setFatherName(String value) {
        setAttributeInternal(FATHERNAME, value);
    }

    /**
     * Gets the attribute value for FIRST_NAME using the alias name FirstName.
     * @return the FIRST_NAME
     */
    public String getFirstName() {
        return (String) getAttributeInternal(FIRSTNAME);
    }

    /**
     * Sets <code>value</code> as attribute value for FIRST_NAME using the alias name FirstName.
     * @param value value to set the FIRST_NAME
     */
    public void setFirstName(String value) {
        setAttributeInternal(FIRSTNAME, value);
    }


    /**
     * Gets the attribute value for LVL_MAST_LVL_MAST_LEVEL using the alias name LvlMastLvlMastLevel.
     * @return the LVL_MAST_LVL_MAST_LEVEL
     */
    public String getLvlMastLvlMastLevel() {
        return (String) getAttributeInternal(LVLMASTLVLMASTLEVEL);
    }

    /**
     * Sets <code>value</code> as attribute value for LVL_MAST_LVL_MAST_LEVEL using the alias name LvlMastLvlMastLevel.
     * @param value value to set the LVL_MAST_LVL_MAST_LEVEL
     */
    public void setLvlMastLvlMastLevel(String value) {
        setAttributeInternal(LVLMASTLVLMASTLEVEL, value);
    }

    /**
     * Gets the attribute value for DESCRIPTION using the alias name Description.
     * @return the DESCRIPTION
     */
    public String getDescription() {
        return (String) getAttributeInternal(DESCRIPTION);
    }

    /**
     * Sets <code>value</code> as attribute value for DESCRIPTION using the alias name Description.
     * @param value value to set the DESCRIPTION
     */
    public void setDescription(String value) {
        setAttributeInternal(DESCRIPTION, value);
    }

    /**
     * Gets the attribute value for CATEGORY_MAST_ID using the alias name CategoryMastId.
     * @return the CATEGORY_MAST_ID
     */
    public Number getCategoryMastId() {
        return (Number) getAttributeInternal(CATEGORYMASTID);
    }

    /**
     * Sets <code>value</code> as attribute value for CATEGORY_MAST_ID using the alias name CategoryMastId.
     * @param value value to set the CATEGORY_MAST_ID
     */
    public void setCategoryMastId(Number value) {
        setAttributeInternal(CATEGORYMASTID, value);
    }

    /**
     * Gets the attribute value for the calculated attribute EditTrans.
     * @return the EditTrans
     */
    public Integer getEditTrans() {
        byte entityState = this.getEntity(0).getEntityState();
                return new Integer(entityState);
        //return (Integer) getAttributeInternal(EDITTRANS);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute EditTrans.
     * @param value value to set the  EditTrans
     */
    public void setEditTrans(Integer value) {
        setAttributeInternal(EDITTRANS, value);
    }

    /**
     * Gets the attribute value for the calculated attribute DesigDesc.
     * @return the DesigDesc
     */
    public String getDesigDesc() {
        return (String) getAttributeInternal(DESIGDESC);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute DesigDesc.
     * @param value value to set the  DesigDesc
     */
    public void setDesigDesc(String value) {
        setAttributeInternal(DESIGDESC, value);
    }

    /**
     * Gets the attribute value for the calculated attribute LevelDesc.
     * @return the LevelDesc
     */
    public String getLevelDesc() {
        return (String) getAttributeInternal(LEVELDESC);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute LevelDesc.
     * @param value value to set the  LevelDesc
     */
    public void setLevelDesc(String value) {
        setAttributeInternal(LEVELDESC, value);
    }

    /**
     * Gets the attribute value for INTV_ENTRY_NO using the alias name IntvEntryNo1.
     * @return the INTV_ENTRY_NO
     */
    public String getIntvEntryNo1() {
        return (String) getAttributeInternal(INTVENTRYNO1);
    }

    /**
     * Sets <code>value</code> as attribute value for INTV_ENTRY_NO using the alias name IntvEntryNo1.
     * @param value value to set the INTV_ENTRY_NO
     */
    public void setIntvEntryNo1(String value) {
        setAttributeInternal(INTVENTRYNO1, value);
    }

    /**
     * Gets the attribute value for EMP_REF_NO using the alias name EmpRefNo.
     * @return the EMP_REF_NO
     */
    public String getEmpRefNo() {
        return (String) getAttributeInternal(EMPREFNO);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_REF_NO using the alias name EmpRefNo.
     * @param value value to set the EMP_REF_NO
     */
    public void setEmpRefNo(String value) {
        setAttributeInternal(EMPREFNO, value);
    }


    /**
     * Gets the attribute value for the calculated attribute First.
     * @return the First
     */
    public String getFirst() {
        return (String) getAttributeInternal(FIRST);
    }

    /**
     * Gets the attribute value for LAST_NAME using the alias name LastName1.
     * @return the LAST_NAME
     */
    public String getLastName1() {
        return (String) getAttributeInternal(LASTNAME1);
    }

    /**
     * Sets <code>value</code> as attribute value for LAST_NAME using the alias name LastName1.
     * @param value value to set the LAST_NAME
     */
    public void setLastName1(String value) {
        setAttributeInternal(LASTNAME1, value);
    }

    /**
     * Gets the associated <code>RowIterator</code> using master-detail link AppointmentLetterEntryDetailVO.
     */
    public RowIterator getAppointmentLetterEntryDetailVO() {
        return (RowIterator) getAttributeInternal(APPOINTMENTLETTERENTRYDETAILVO);
    }

    /**
     * Gets the view accessor <code>RowSet</code> UnitVO1.
     */
    public RowSet getUnitVO1() {
        return (RowSet) getAttributeInternal(UNITVO1);
    }


    /**
     * Gets the view accessor <code>RowSet</code> CategoryMasterHeaderVO1.
     */
    public RowSet getCategoryMasterHeaderVO1() {
        return (RowSet) getAttributeInternal(CATEGORYMASTERHEADERVO1);
    }


    /**
     * Gets the view accessor <code>RowSet</code> AppointmentLetterIntvNoVVO1.
     */
    public RowSet getAppointmentLetterIntvNoVVO1() {
        return (RowSet) getAttributeInternal(APPOINTMENTLETTERINTVNOVVO1);
    }

    /**
     * getAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param attrDef the attribute

     * @return the attribute value
     * @throws Exception
     */
    protected Object getAttrInvokeAccessor(int index, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            return AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].get(this);
        }
        return super.getAttrInvokeAccessor(index, attrDef);
    }

    /**
     * setAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param value the value to assign to the attribute
     * @param attrDef the attribute

     * @throws Exception
     */
    protected void setAttrInvokeAccessor(int index, Object value, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].put(this, value);
            return;
        }
        super.setAttrInvokeAccessor(index, value, attrDef);
    }
}

