package terms.hrm.transaction.model.view;

import java.math.BigDecimal;

import oracle.jbo.RowSetIterator;
import oracle.jbo.domain.Number;
import oracle.jbo.domain.Timestamp;
import oracle.jbo.server.AttributeDefImpl;
import oracle.jbo.server.EntityImpl;
import oracle.jbo.server.ViewRowImpl;

import terms.hrm.setup.model.entity.SalaryHeadMasterEOImpl;
import terms.hrm.transaction.model.applicationModule.HrmTransactionAMImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Sat Jun 29 14:32:30 IST 2019
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class ArrearEntryDetailDednVORowImpl extends ViewRowImpl {

    public static final int ENTITY_ARREARENTRYDETAILDEDNEO = 0;
    public static final int ENTITY_SALARYHEADMASTEREO = 1;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        Amount {
            public Object get(ArrearEntryDetailDednVORowImpl obj) {
                return obj.getAmount();
            }

            public void put(ArrearEntryDetailDednVORowImpl obj, Object value) {
                obj.setAmount((BigDecimal) value);
            }
        }
        ,
        ArrearDetailId {
            public Object get(ArrearEntryDetailDednVORowImpl obj) {
                return obj.getArrearDetailId();
            }

            public void put(ArrearEntryDetailDednVORowImpl obj, Object value) {
                obj.setArrearDetailId((Number) value);
            }
        }
        ,
        ArrearDetailLineId {
            public Object get(ArrearEntryDetailDednVORowImpl obj) {
                return obj.getArrearDetailLineId();
            }

            public void put(ArrearEntryDetailDednVORowImpl obj, Object value) {
                obj.setArrearDetailLineId((Number) value);
            }
        }
        ,
        ArrearMonthYear {
            public Object get(ArrearEntryDetailDednVORowImpl obj) {
                return obj.getArrearMonthYear();
            }

            public void put(ArrearEntryDetailDednVORowImpl obj, Object value) {
                obj.setArrearMonthYear((Number) value);
            }
        }
        ,
        ArrearOfMonth {
            public Object get(ArrearEntryDetailDednVORowImpl obj) {
                return obj.getArrearOfMonth();
            }

            public void put(ArrearEntryDetailDednVORowImpl obj, Object value) {
                obj.setArrearOfMonth((String) value);
            }
        }
        ,
        CreatedBy {
            public Object get(ArrearEntryDetailDednVORowImpl obj) {
                return obj.getCreatedBy();
            }

            public void put(ArrearEntryDetailDednVORowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        CreationDate {
            public Object get(ArrearEntryDetailDednVORowImpl obj) {
                return obj.getCreationDate();
            }

            public void put(ArrearEntryDetailDednVORowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        EmpNo {
            public Object get(ArrearEntryDetailDednVORowImpl obj) {
                return obj.getEmpNo();
            }

            public void put(ArrearEntryDetailDednVORowImpl obj, Object value) {
                obj.setEmpNo((String) value);
            }
        }
        ,
        FinYear {
            public Object get(ArrearEntryDetailDednVORowImpl obj) {
                return obj.getFinYear();
            }

            public void put(ArrearEntryDetailDednVORowImpl obj, Object value) {
                obj.setFinYear((String) value);
            }
        }
        ,
        LastUpdateDate {
            public Object get(ArrearEntryDetailDednVORowImpl obj) {
                return obj.getLastUpdateDate();
            }

            public void put(ArrearEntryDetailDednVORowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        LastUpdatedBy {
            public Object get(ArrearEntryDetailDednVORowImpl obj) {
                return obj.getLastUpdatedBy();
            }

            public void put(ArrearEntryDetailDednVORowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        ObjectVersionNumber {
            public Object get(ArrearEntryDetailDednVORowImpl obj) {
                return obj.getObjectVersionNumber();
            }

            public void put(ArrearEntryDetailDednVORowImpl obj, Object value) {
                obj.setObjectVersionNumber((Number) value);
            }
        }
        ,
        SalHdCode {
            public Object get(ArrearEntryDetailDednVORowImpl obj) {
                return obj.getSalHdCode();
            }

            public void put(ArrearEntryDetailDednVORowImpl obj, Object value) {
                obj.setSalHdCode((String) value);
            }
        }
        ,
        SalHdSubcode {
            public Object get(ArrearEntryDetailDednVORowImpl obj) {
                return obj.getSalHdSubcode();
            }

            public void put(ArrearEntryDetailDednVORowImpl obj, Object value) {
                obj.setSalHdSubcode((Number) value);
            }
        }
        ,
        SalHdSubcode1 {
            public Object get(ArrearEntryDetailDednVORowImpl obj) {
                return obj.getSalHdSubcode1();
            }

            public void put(ArrearEntryDetailDednVORowImpl obj, Object value) {
                obj.setSalHdSubcode1((String) value);
            }
        }
        ,
        SalMonth {
            public Object get(ArrearEntryDetailDednVORowImpl obj) {
                return obj.getSalMonth();
            }

            public void put(ArrearEntryDetailDednVORowImpl obj, Object value) {
                obj.setSalMonth((String) value);
            }
        }
        ,
        SalYear {
            public Object get(ArrearEntryDetailDednVORowImpl obj) {
                return obj.getSalYear();
            }

            public void put(ArrearEntryDetailDednVORowImpl obj, Object value) {
                obj.setSalYear((Number) value);
            }
        }
        ,
        SalhdMastType {
            public Object get(ArrearEntryDetailDednVORowImpl obj) {
                return obj.getSalhdMastType();
            }

            public void put(ArrearEntryDetailDednVORowImpl obj, Object value) {
                obj.setSalhdMastType((String) value);
            }
        }
        ,
        Type {
            public Object get(ArrearEntryDetailDednVORowImpl obj) {
                return obj.getType();
            }

            public void put(ArrearEntryDetailDednVORowImpl obj, Object value) {
                obj.setType((String) value);
            }
        }
        ,
        UnitCode {
            public Object get(ArrearEntryDetailDednVORowImpl obj) {
                return obj.getUnitCode();
            }

            public void put(ArrearEntryDetailDednVORowImpl obj, Object value) {
                obj.setUnitCode((String) value);
            }
        }
        ,
        TotalTrans {
            public Object get(ArrearEntryDetailDednVORowImpl obj) {
                return obj.getTotalTrans();
            }

            public void put(ArrearEntryDetailDednVORowImpl obj, Object value) {
                obj.setTotalTrans((BigDecimal) value);
            }
        }
        ,
        Description {
            public Object get(ArrearEntryDetailDednVORowImpl obj) {
                return obj.getDescription();
            }

            public void put(ArrearEntryDetailDednVORowImpl obj, Object value) {
                obj.setDescription((String) value);
            }
        }
        ,
        Code {
            public Object get(ArrearEntryDetailDednVORowImpl obj) {
                return obj.getCode();
            }

            public void put(ArrearEntryDetailDednVORowImpl obj, Object value) {
                obj.setCode((String) value);
            }
        }
        ,
        SalHdMastId {
            public Object get(ArrearEntryDetailDednVORowImpl obj) {
                return obj.getSalHdMastId();
            }

            public void put(ArrearEntryDetailDednVORowImpl obj, Object value) {
                obj.setSalHdMastId((Long) value);
            }
        }
        ;
        static AttributesEnum[] vals = null;
        ;
        private static final int firstIndex = 0;

        public abstract Object get(ArrearEntryDetailDednVORowImpl object);

        public abstract void put(ArrearEntryDetailDednVORowImpl object, Object value);

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int AMOUNT = AttributesEnum.Amount.index();
    public static final int ARREARDETAILID = AttributesEnum.ArrearDetailId.index();
    public static final int ARREARDETAILLINEID = AttributesEnum.ArrearDetailLineId.index();
    public static final int ARREARMONTHYEAR = AttributesEnum.ArrearMonthYear.index();
    public static final int ARREAROFMONTH = AttributesEnum.ArrearOfMonth.index();
    public static final int CREATEDBY = AttributesEnum.CreatedBy.index();
    public static final int CREATIONDATE = AttributesEnum.CreationDate.index();
    public static final int EMPNO = AttributesEnum.EmpNo.index();
    public static final int FINYEAR = AttributesEnum.FinYear.index();
    public static final int LASTUPDATEDATE = AttributesEnum.LastUpdateDate.index();
    public static final int LASTUPDATEDBY = AttributesEnum.LastUpdatedBy.index();
    public static final int OBJECTVERSIONNUMBER = AttributesEnum.ObjectVersionNumber.index();
    public static final int SALHDCODE = AttributesEnum.SalHdCode.index();
    public static final int SALHDSUBCODE = AttributesEnum.SalHdSubcode.index();
    public static final int SALHDSUBCODE1 = AttributesEnum.SalHdSubcode1.index();
    public static final int SALMONTH = AttributesEnum.SalMonth.index();
    public static final int SALYEAR = AttributesEnum.SalYear.index();
    public static final int SALHDMASTTYPE = AttributesEnum.SalhdMastType.index();
    public static final int TYPE = AttributesEnum.Type.index();
    public static final int UNITCODE = AttributesEnum.UnitCode.index();
    public static final int TOTALTRANS = AttributesEnum.TotalTrans.index();
    public static final int DESCRIPTION = AttributesEnum.Description.index();
    public static final int CODE = AttributesEnum.Code.index();
    public static final int SALHDMASTID = AttributesEnum.SalHdMastId.index();

    /**
     * This is the default constructor (do not remove).
     */
    public ArrearEntryDetailDednVORowImpl() {
    }

    /**
     * Gets ArrearEntryDetailDednEO entity object.
     * @return the ArrearEntryDetailDednEO
     */
    public EntityImpl getArrearEntryDetailDednEO() {
        return (EntityImpl) getEntity(ENTITY_ARREARENTRYDETAILDEDNEO);
    }

    /**
     * Gets SalaryHeadMasterEO entity object.
     * @return the SalaryHeadMasterEO
     */
    public SalaryHeadMasterEOImpl getSalaryHeadMasterEO() {
        return (SalaryHeadMasterEOImpl) getEntity(ENTITY_SALARYHEADMASTEREO);
    }

    /**
     * Gets the attribute value for AMOUNT using the alias name Amount.
     * @return the AMOUNT
     */
    public BigDecimal getAmount() {
        return (BigDecimal) getAttributeInternal(AMOUNT);
    }

    /**
     * Sets <code>value</code> as attribute value for AMOUNT using the alias name Amount.
     * @param value value to set the AMOUNT
     */
    public void setAmount(BigDecimal value) {
        setAttributeInternal(AMOUNT, value);
    }

    /**
     * Gets the attribute value for ARREAR_DETAIL_ID using the alias name ArrearDetailId.
     * @return the ARREAR_DETAIL_ID
     */
    public Number getArrearDetailId() {
        return (Number) getAttributeInternal(ARREARDETAILID);
    }

    /**
     * Sets <code>value</code> as attribute value for ARREAR_DETAIL_ID using the alias name ArrearDetailId.
     * @param value value to set the ARREAR_DETAIL_ID
     */
    public void setArrearDetailId(Number value) {
        setAttributeInternal(ARREARDETAILID, value);
    }

    /**
     * Gets the attribute value for ARREAR_DETAIL_LINE_ID using the alias name ArrearDetailLineId.
     * @return the ARREAR_DETAIL_LINE_ID
     */
    public Number getArrearDetailLineId() {
        return (Number) getAttributeInternal(ARREARDETAILLINEID);
    }

    /**
     * Sets <code>value</code> as attribute value for ARREAR_DETAIL_LINE_ID using the alias name ArrearDetailLineId.
     * @param value value to set the ARREAR_DETAIL_LINE_ID
     */
    public void setArrearDetailLineId(Number value) {
        setAttributeInternal(ARREARDETAILLINEID, value);
    }

    /**
     * Gets the attribute value for ARREAR_MONTH_YEAR using the alias name ArrearMonthYear.
     * @return the ARREAR_MONTH_YEAR
     */
    public Number getArrearMonthYear() {
        return (Number) getAttributeInternal(ARREARMONTHYEAR);
    }

    /**
     * Sets <code>value</code> as attribute value for ARREAR_MONTH_YEAR using the alias name ArrearMonthYear.
     * @param value value to set the ARREAR_MONTH_YEAR
     */
    public void setArrearMonthYear(Number value) {
        setAttributeInternal(ARREARMONTHYEAR, value);
    }

    /**
     * Gets the attribute value for ARREAR_OF_MONTH using the alias name ArrearOfMonth.
     * @return the ARREAR_OF_MONTH
     */
    public String getArrearOfMonth() {
        return (String) getAttributeInternal(ARREAROFMONTH);
    }

    /**
     * Sets <code>value</code> as attribute value for ARREAR_OF_MONTH using the alias name ArrearOfMonth.
     * @param value value to set the ARREAR_OF_MONTH
     */
    public void setArrearOfMonth(String value) {
        setAttributeInternal(ARREAROFMONTH, value);
    }

    /**
     * Gets the attribute value for CREATED_BY using the alias name CreatedBy.
     * @return the CREATED_BY
     */
    public String getCreatedBy() {
        return (String) getAttributeInternal(CREATEDBY);
    }

    /**
     * Gets the attribute value for CREATION_DATE using the alias name CreationDate.
     * @return the CREATION_DATE
     */
    public Timestamp getCreationDate() {
        return (Timestamp) getAttributeInternal(CREATIONDATE);
    }

    /**
     * Gets the attribute value for EMP_NO using the alias name EmpNo.
     * @return the EMP_NO
     */
    public String getEmpNo() {
        return (String) getAttributeInternal(EMPNO);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_NO using the alias name EmpNo.
     * @param value value to set the EMP_NO
     */
    public void setEmpNo(String value) {
        setAttributeInternal(EMPNO, value);
    }

    /**
     * Gets the attribute value for FIN_YEAR using the alias name FinYear.
     * @return the FIN_YEAR
     */
    public String getFinYear() {
        return (String) getAttributeInternal(FINYEAR);
    }

    /**
     * Sets <code>value</code> as attribute value for FIN_YEAR using the alias name FinYear.
     * @param value value to set the FIN_YEAR
     */
    public void setFinYear(String value) {
        setAttributeInternal(FINYEAR, value);
    }

    /**
     * Gets the attribute value for LAST_UPDATE_DATE using the alias name LastUpdateDate.
     * @return the LAST_UPDATE_DATE
     */
    public Timestamp getLastUpdateDate() {
        return (Timestamp) getAttributeInternal(LASTUPDATEDATE);
    }

    /**
     * Gets the attribute value for LAST_UPDATED_BY using the alias name LastUpdatedBy.
     * @return the LAST_UPDATED_BY
     */
    public String getLastUpdatedBy() {
        return (String) getAttributeInternal(LASTUPDATEDBY);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Number getObjectVersionNumber() {
        return (Number) getAttributeInternal(OBJECTVERSIONNUMBER);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber(Number value) {
        setAttributeInternal(OBJECTVERSIONNUMBER, value);
    }

    /**
     * Gets the attribute value for SAL_HD_CODE using the alias name SalHdCode.
     * @return the SAL_HD_CODE
     */
    public String getSalHdCode() {
        return (String) getAttributeInternal(SALHDCODE);
    }

    /**
     * Sets <code>value</code> as attribute value for SAL_HD_CODE using the alias name SalHdCode.
     * @param value value to set the SAL_HD_CODE
     */
    public void setSalHdCode(String value) {
        setAttributeInternal(SALHDCODE, value);
    }

    /**
     * Gets the attribute value for SAL_HD_SUBCODE using the alias name SalHdSubcode.
     * @return the SAL_HD_SUBCODE
     */
    public Number getSalHdSubcode() {
        return (Number) getAttributeInternal(SALHDSUBCODE);
    }

    /**
     * Sets <code>value</code> as attribute value for SAL_HD_SUBCODE using the alias name SalHdSubcode.
     * @param value value to set the SAL_HD_SUBCODE
     */
    public void setSalHdSubcode(Number value) {
        setAttributeInternal(SALHDSUBCODE, value);
    }

    /**
     * Gets the attribute value for SAL_HD_SUBCODE1 using the alias name SalHdSubcode1.
     * @return the SAL_HD_SUBCODE1
     */
    public String getSalHdSubcode1() {
        return (String) getAttributeInternal(SALHDSUBCODE1);
    }

    /**
     * Sets <code>value</code> as attribute value for SAL_HD_SUBCODE1 using the alias name SalHdSubcode1.
     * @param value value to set the SAL_HD_SUBCODE1
     */
    public void setSalHdSubcode1(String value) {
        setAttributeInternal(SALHDSUBCODE1, value);
    }

    /**
     * Gets the attribute value for SAL_MONTH using the alias name SalMonth.
     * @return the SAL_MONTH
     */
    public String getSalMonth() {
        return (String) getAttributeInternal(SALMONTH);
    }

    /**
     * Sets <code>value</code> as attribute value for SAL_MONTH using the alias name SalMonth.
     * @param value value to set the SAL_MONTH
     */
    public void setSalMonth(String value) {
        setAttributeInternal(SALMONTH, value);
    }

    /**
     * Gets the attribute value for SAL_YEAR using the alias name SalYear.
     * @return the SAL_YEAR
     */
    public Number getSalYear() {
        return (Number) getAttributeInternal(SALYEAR);
    }

    /**
     * Sets <code>value</code> as attribute value for SAL_YEAR using the alias name SalYear.
     * @param value value to set the SAL_YEAR
     */
    public void setSalYear(Number value) {
        setAttributeInternal(SALYEAR, value);
    }

    /**
     * Gets the attribute value for SALHD_MAST_TYPE using the alias name SalhdMastType.
     * @return the SALHD_MAST_TYPE
     */
    public String getSalhdMastType() {
        return (String) getAttributeInternal(SALHDMASTTYPE);
    }

    /**
     * Sets <code>value</code> as attribute value for SALHD_MAST_TYPE using the alias name SalhdMastType.
     * @param value value to set the SALHD_MAST_TYPE
     */
    public void setSalhdMastType(String value) {
        setAttributeInternal(SALHDMASTTYPE, value);
    }

    /**
     * Gets the attribute value for TYPE using the alias name Type.
     * @return the TYPE
     */
    public String getType() {
        return (String) getAttributeInternal(TYPE);
    }

    /**
     * Sets <code>value</code> as attribute value for TYPE using the alias name Type.
     * @param value value to set the TYPE
     */
    public void setType(String value) {
        setAttributeInternal(TYPE, value);
    }

    /**
     * Gets the attribute value for UNIT_CODE using the alias name UnitCode.
     * @return the UNIT_CODE
     */
    public String getUnitCode() {
        return (String) getAttributeInternal(UNITCODE);
    }

    /**
     * Sets <code>value</code> as attribute value for UNIT_CODE using the alias name UnitCode.
     * @param value value to set the UNIT_CODE
     */
    public void setUnitCode(String value) {
        setAttributeInternal(UNITCODE, value);
    }

    /**
     * Gets the attribute value for the calculated attribute TotalTrans.
     * @return the TotalTrans
     */
    public BigDecimal getTotalTrans() {
        HrmTransactionAMImpl am = (HrmTransactionAMImpl) this.getApplicationModule();
         BigDecimal wages=new  BigDecimal(0);
        RowSetIterator itr=am.getArrearEntryDetailDednVO1().createRowSetIterator(null);
        while(itr.hasNext())
        {
                ArrearEntryDetailDednVORowImpl next = (ArrearEntryDetailDednVORowImpl) itr.next();
            if(next.getAmount()!=null){
              wages=wages.add(next.getAmount()); 
              
            }
            
        }
        itr.closeRowSetIterator();
        return wages;
        //return (BigDecimal) getAttributeInternal(TOTALTRANS);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute TotalTrans.
     * @param value value to set the  TotalTrans
     */
    public void setTotalTrans(BigDecimal value) {
        setAttributeInternal(TOTALTRANS, value);
    }

    /**
     * Gets the attribute value for DESCRIPTION using the alias name Description.
     * @return the DESCRIPTION
     */
    public String getDescription() {
        return (String) getAttributeInternal(DESCRIPTION);
    }

    /**
     * Sets <code>value</code> as attribute value for DESCRIPTION using the alias name Description.
     * @param value value to set the DESCRIPTION
     */
    public void setDescription(String value) {
        setAttributeInternal(DESCRIPTION, value);
    }

    /**
     * Gets the attribute value for CODE using the alias name Code.
     * @return the CODE
     */
    public String getCode() {
        return (String) getAttributeInternal(CODE);
    }

    /**
     * Sets <code>value</code> as attribute value for CODE using the alias name Code.
     * @param value value to set the CODE
     */
    public void setCode(String value) {
        setAttributeInternal(CODE, value);
    }

    /**
     * Gets the attribute value for SAL_HD_MAST_ID using the alias name SalHdMastId.
     * @return the SAL_HD_MAST_ID
     */
    public Long getSalHdMastId() {
        return (Long) getAttributeInternal(SALHDMASTID);
    }

    /**
     * Sets <code>value</code> as attribute value for SAL_HD_MAST_ID using the alias name SalHdMastId.
     * @param value value to set the SAL_HD_MAST_ID
     */
    public void setSalHdMastId(Long value) {
        setAttributeInternal(SALHDMASTID, value);
    }

    /**
     * getAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param attrDef the attribute

     * @return the attribute value
     * @throws Exception
     */
    protected Object getAttrInvokeAccessor(int index, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            return AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].get(this);
        }
        return super.getAttrInvokeAccessor(index, attrDef);
    }

    /**
     * setAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param value the value to assign to the attribute
     * @param attrDef the attribute

     * @throws Exception
     */
    protected void setAttrInvokeAccessor(int index, Object value, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].put(this, value);
            return;
        }
        super.setAttrInvokeAccessor(index, value, attrDef);
    }
}

