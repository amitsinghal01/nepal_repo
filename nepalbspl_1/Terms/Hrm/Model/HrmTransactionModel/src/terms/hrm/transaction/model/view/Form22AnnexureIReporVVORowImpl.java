package terms.hrm.transaction.model.view;

import oracle.jbo.Row;
import oracle.jbo.RowSet;
import oracle.jbo.server.AttributeDefImpl;
import oracle.jbo.server.ViewObjectImpl;
import oracle.jbo.server.ViewRowImpl;

import terms.hrm.transaction.model.applicationModule.HrmTransactionAMImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Tue Nov 05 13:04:32 IST 2019
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class Form22AnnexureIReporVVORowImpl extends ViewRowImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        Dummy {
            public Object get(Form22AnnexureIReporVVORowImpl obj) {
                return obj.getDummy();
            }

            public void put(Form22AnnexureIReporVVORowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        Unitcd {
            public Object get(Form22AnnexureIReporVVORowImpl obj) {
                return obj.getUnitcd();
            }

            public void put(Form22AnnexureIReporVVORowImpl obj, Object value) {
                obj.setUnitcd((String) value);
            }
        }
        ,
        unitname {
            public Object get(Form22AnnexureIReporVVORowImpl obj) {
                return obj.getunitname();
            }

            public void put(Form22AnnexureIReporVVORowImpl obj, Object value) {
                obj.setunitname((String) value);
            }
        }
        ,
        UnitVO1 {
            public Object get(Form22AnnexureIReporVVORowImpl obj) {
                return obj.getUnitVO1();
            }

            public void put(Form22AnnexureIReporVVORowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        public abstract Object get(Form22AnnexureIReporVVORowImpl object);

        public abstract void put(Form22AnnexureIReporVVORowImpl object, Object value);

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }
    public static final int DUMMY = AttributesEnum.Dummy.index();
    public static final int UNITCD = AttributesEnum.Unitcd.index();
    public static final int UNITNAME = AttributesEnum.unitname.index();
    public static final int UNITVO1 = AttributesEnum.UnitVO1.index();

    /**
     * This is the default constructor (do not remove).
     */
    public Form22AnnexureIReporVVORowImpl() {
    }

    /**
     * Gets the attribute value for the calculated attribute Dummy.
     * @return the Dummy
     */
    public String getDummy() {
        return (String) getAttributeInternal(DUMMY);
    }

    /**
     * Gets the attribute value for the calculated attribute Unitcd.
     * @return the Unitcd
     */
    public String getUnitcd() {
        return (String) getAttributeInternal(UNITCD);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute Unitcd.
     * @param value value to set the  Unitcd
     */
    public void setUnitcd(String value) {
        setAttributeInternal(UNITCD, value);
    }

    /**
     * Gets the attribute value for the calculated attribute unitname.
     * @return the unitname
     */
    public String getunitname() {
        if(getAttributeInternal(UNITNAME) == null){
                HrmTransactionAMImpl am = (HrmTransactionAMImpl) this.getApplicationModule();
                ViewObjectImpl vo=am.getUnitVO1();
                Row[] r1 = vo.getFilteredRows("Code",getUnitcd());
                if (r1.length > 0) {
                    String name = (String) r1[0].getAttribute("Name");
                    setunitname(name); 
                } 
        }
        return (String) getAttributeInternal(UNITNAME);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute unitname.
     * @param value value to set the  unitname
     */
    public void setunitname(String value) {
        setAttributeInternal(UNITNAME, value);
    }

    /**
     * Gets the view accessor <code>RowSet</code> UnitVO1.
     */
    public RowSet getUnitVO1() {
        return (RowSet) getAttributeInternal(UNITVO1);
    }

    /**
     * getAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param attrDef the attribute

     * @return the attribute value
     * @throws Exception
     */
    protected Object getAttrInvokeAccessor(int index, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            return AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].get(this);
        }
        return super.getAttrInvokeAccessor(index, attrDef);
    }

    /**
     * setAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param value the value to assign to the attribute
     * @param attrDef the attribute

     * @throws Exception
     */
    protected void setAttrInvokeAccessor(int index, Object value, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].put(this, value);
            return;
        }
        super.setAttrInvokeAccessor(index, value, attrDef);
    }
}

