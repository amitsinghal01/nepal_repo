package terms.hrm.transaction.model.view;

import java.math.BigDecimal;

import oracle.jbo.RowSet;
import oracle.jbo.domain.BFileDomain;
import oracle.jbo.domain.Date;
import oracle.jbo.domain.Number;
import oracle.jbo.domain.Timestamp;
import oracle.jbo.server.AttributeDefImpl;
import oracle.jbo.server.EntityImpl;
import oracle.jbo.server.ViewRowImpl;

import terms.hrm.setup.model.entity.EmployeeMasterEOImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Fri Mar 29 12:55:16 IST 2019
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class LeaveCancelAndVerificationVORowImpl extends ViewRowImpl {

    public static final int ENTITY_LEAVECANCELANDVERIFICATIONEO = 0;
    public static final int ENTITY_UNITEO = 1;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        AccCheck {
            public Object get(LeaveCancelAndVerificationVORowImpl obj) {
                return obj.getAccCheck();
            }

            public void put(LeaveCancelAndVerificationVORowImpl obj, Object value) {
                obj.setAccCheck((Number) value);
            }
        }
        ,
        AppDate {
            public Object get(LeaveCancelAndVerificationVORowImpl obj) {
                return obj.getAppDate();
            }

            public void put(LeaveCancelAndVerificationVORowImpl obj, Object value) {
                obj.setAppDate((Date) value);
            }
        }
        ,
        AppNo {
            public Object get(LeaveCancelAndVerificationVORowImpl obj) {
                return obj.getAppNo();
            }

            public void put(LeaveCancelAndVerificationVORowImpl obj, Object value) {
                obj.setAppNo((String) value);
            }
        }
        ,
        ApprCancelDt {
            public Object get(LeaveCancelAndVerificationVORowImpl obj) {
                return obj.getApprCancelDt();
            }

            public void put(LeaveCancelAndVerificationVORowImpl obj, Object value) {
                obj.setApprCancelDt((Date) value);
            }
        }
        ,
        ApprCancelHead {
            public Object get(LeaveCancelAndVerificationVORowImpl obj) {
                return obj.getApprCancelHead();
            }

            public void put(LeaveCancelAndVerificationVORowImpl obj, Object value) {
                obj.setApprCancelHead((String) value);
            }
        }
        ,
        CancelDays {
            public Object get(LeaveCancelAndVerificationVORowImpl obj) {
                return obj.getCancelDays();
            }

            public void put(LeaveCancelAndVerificationVORowImpl obj, Object value) {
                obj.setCancelDays((BigDecimal) value);
            }
        }
        ,
        CancelFrdt {
            public Object get(LeaveCancelAndVerificationVORowImpl obj) {
                return obj.getCancelFrdt();
            }

            public void put(LeaveCancelAndVerificationVORowImpl obj, Object value) {
                obj.setCancelFrdt((Timestamp) value);
            }
        }
        ,
        CancelTodt {
            public Object get(LeaveCancelAndVerificationVORowImpl obj) {
                return obj.getCancelTodt();
            }

            public void put(LeaveCancelAndVerificationVORowImpl obj, Object value) {
                obj.setCancelTodt((Timestamp) value);
            }
        }
        ,
        ChkVerBy {
            public Object get(LeaveCancelAndVerificationVORowImpl obj) {
                return obj.getChkVerBy();
            }

            public void put(LeaveCancelAndVerificationVORowImpl obj, Object value) {
                obj.setChkVerBy((String) value);
            }
        }
        ,
        CreatedBy {
            public Object get(LeaveCancelAndVerificationVORowImpl obj) {
                return obj.getCreatedBy();
            }

            public void put(LeaveCancelAndVerificationVORowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        CreationDate {
            public Object get(LeaveCancelAndVerificationVORowImpl obj) {
                return obj.getCreationDate();
            }

            public void put(LeaveCancelAndVerificationVORowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        EmpName {
            public Object get(LeaveCancelAndVerificationVORowImpl obj) {
                return obj.getEmpName();
            }

            public void put(LeaveCancelAndVerificationVORowImpl obj, Object value) {
                obj.setEmpName((String) value);
            }
        }
        ,
        EmpNumber {
            public Object get(LeaveCancelAndVerificationVORowImpl obj) {
                return obj.getEmpNumber();
            }

            public void put(LeaveCancelAndVerificationVORowImpl obj, Object value) {
                obj.setEmpNumber((String) value);
            }
        }
        ,
        EndDate {
            public Object get(LeaveCancelAndVerificationVORowImpl obj) {
                return obj.getEndDate();
            }

            public void put(LeaveCancelAndVerificationVORowImpl obj, Object value) {
                obj.setEndDate((Timestamp) value);
            }
        }
        ,
        FromDate {
            public Object get(LeaveCancelAndVerificationVORowImpl obj) {
                return obj.getFromDate();
            }

            public void put(LeaveCancelAndVerificationVORowImpl obj, Object value) {
                obj.setFromDate((Timestamp) value);
            }
        }
        ,
        LastUpdateDate {
            public Object get(LeaveCancelAndVerificationVORowImpl obj) {
                return obj.getLastUpdateDate();
            }

            public void put(LeaveCancelAndVerificationVORowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        LastUpdatedBy {
            public Object get(LeaveCancelAndVerificationVORowImpl obj) {
                return obj.getLastUpdatedBy();
            }

            public void put(LeaveCancelAndVerificationVORowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        LeaveApprCancelTempId {
            public Object get(LeaveCancelAndVerificationVORowImpl obj) {
                return obj.getLeaveApprCancelTempId();
            }

            public void put(LeaveCancelAndVerificationVORowImpl obj, Object value) {
                obj.setLeaveApprCancelTempId((Number) value);
            }
        }
        ,
        LeaveDescType {
            public Object get(LeaveCancelAndVerificationVORowImpl obj) {
                return obj.getLeaveDescType();
            }

            public void put(LeaveCancelAndVerificationVORowImpl obj, Object value) {
                obj.setLeaveDescType((String) value);
            }
        }
        ,
        LeaveGranted {
            public Object get(LeaveCancelAndVerificationVORowImpl obj) {
                return obj.getLeaveGranted();
            }

            public void put(LeaveCancelAndVerificationVORowImpl obj, Object value) {
                obj.setLeaveGranted((String) value);
            }
        }
        ,
        LeaveGrantedPre {
            public Object get(LeaveCancelAndVerificationVORowImpl obj) {
                return obj.getLeaveGrantedPre();
            }

            public void put(LeaveCancelAndVerificationVORowImpl obj, Object value) {
                obj.setLeaveGrantedPre((String) value);
            }
        }
        ,
        ObjectVersionNumber {
            public Object get(LeaveCancelAndVerificationVORowImpl obj) {
                return obj.getObjectVersionNumber();
            }

            public void put(LeaveCancelAndVerificationVORowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        Remarks {
            public Object get(LeaveCancelAndVerificationVORowImpl obj) {
                return obj.getRemarks();
            }

            public void put(LeaveCancelAndVerificationVORowImpl obj, Object value) {
                obj.setRemarks((String) value);
            }
        }
        ,
        UnitCd {
            public Object get(LeaveCancelAndVerificationVORowImpl obj) {
                return obj.getUnitCd();
            }

            public void put(LeaveCancelAndVerificationVORowImpl obj, Object value) {
                obj.setUnitCd((String) value);
            }
        }
        ,
        VerBy {
            public Object get(LeaveCancelAndVerificationVORowImpl obj) {
                return obj.getVerBy();
            }

            public void put(LeaveCancelAndVerificationVORowImpl obj, Object value) {
                obj.setVerBy((String) value);
            }
        }
        ,
        Code {
            public Object get(LeaveCancelAndVerificationVORowImpl obj) {
                return obj.getCode();
            }

            public void put(LeaveCancelAndVerificationVORowImpl obj, Object value) {
                obj.setCode((String) value);
            }
        }
        ,
        ObjectVersionNumber1 {
            public Object get(LeaveCancelAndVerificationVORowImpl obj) {
                return obj.getObjectVersionNumber1();
            }

            public void put(LeaveCancelAndVerificationVORowImpl obj, Object value) {
                obj.setObjectVersionNumber1((Integer) value);
            }
        }
        ,
        Name {
            public Object get(LeaveCancelAndVerificationVORowImpl obj) {
                return obj.getName();
            }

            public void put(LeaveCancelAndVerificationVORowImpl obj, Object value) {
                obj.setName((String) value);
            }
        }
        ,
        EditTrans {
            public Object get(LeaveCancelAndVerificationVORowImpl obj) {
                return obj.getEditTrans();
            }

            public void put(LeaveCancelAndVerificationVORowImpl obj, Object value) {
                obj.setEditTrans((Integer) value);
            }
        }
        ,
        Name_Emp {
            public Object get(LeaveCancelAndVerificationVORowImpl obj) {
                return obj.getName_Emp();
            }

            public void put(LeaveCancelAndVerificationVORowImpl obj, Object value) {
                obj.setName_Emp((String) value);
            }
        }
        ,
        EmpViewVVO1 {
            public Object get(LeaveCancelAndVerificationVORowImpl obj) {
                return obj.getEmpViewVVO1();
            }

            public void put(LeaveCancelAndVerificationVORowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        UnitVO2 {
            public Object get(LeaveCancelAndVerificationVORowImpl obj) {
                return obj.getUnitVO2();
            }

            public void put(LeaveCancelAndVerificationVORowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        SecControlVO1 {
            public Object get(LeaveCancelAndVerificationVORowImpl obj) {
                return obj.getSecControlVO1();
            }

            public void put(LeaveCancelAndVerificationVORowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ;
        static AttributesEnum[] vals = null;
        ;
        private static final int firstIndex = 0;

        public abstract Object get(LeaveCancelAndVerificationVORowImpl object);

        public abstract void put(LeaveCancelAndVerificationVORowImpl object, Object value);

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int ACCCHECK = AttributesEnum.AccCheck.index();
    public static final int APPDATE = AttributesEnum.AppDate.index();
    public static final int APPNO = AttributesEnum.AppNo.index();
    public static final int APPRCANCELDT = AttributesEnum.ApprCancelDt.index();
    public static final int APPRCANCELHEAD = AttributesEnum.ApprCancelHead.index();
    public static final int CANCELDAYS = AttributesEnum.CancelDays.index();
    public static final int CANCELFRDT = AttributesEnum.CancelFrdt.index();
    public static final int CANCELTODT = AttributesEnum.CancelTodt.index();
    public static final int CHKVERBY = AttributesEnum.ChkVerBy.index();
    public static final int CREATEDBY = AttributesEnum.CreatedBy.index();
    public static final int CREATIONDATE = AttributesEnum.CreationDate.index();
    public static final int EMPNAME = AttributesEnum.EmpName.index();
    public static final int EMPNUMBER = AttributesEnum.EmpNumber.index();
    public static final int ENDDATE = AttributesEnum.EndDate.index();
    public static final int FROMDATE = AttributesEnum.FromDate.index();
    public static final int LASTUPDATEDATE = AttributesEnum.LastUpdateDate.index();
    public static final int LASTUPDATEDBY = AttributesEnum.LastUpdatedBy.index();
    public static final int LEAVEAPPRCANCELTEMPID = AttributesEnum.LeaveApprCancelTempId.index();
    public static final int LEAVEDESCTYPE = AttributesEnum.LeaveDescType.index();
    public static final int LEAVEGRANTED = AttributesEnum.LeaveGranted.index();
    public static final int LEAVEGRANTEDPRE = AttributesEnum.LeaveGrantedPre.index();
    public static final int OBJECTVERSIONNUMBER = AttributesEnum.ObjectVersionNumber.index();
    public static final int REMARKS = AttributesEnum.Remarks.index();
    public static final int UNITCD = AttributesEnum.UnitCd.index();
    public static final int VERBY = AttributesEnum.VerBy.index();
    public static final int CODE = AttributesEnum.Code.index();
    public static final int OBJECTVERSIONNUMBER1 = AttributesEnum.ObjectVersionNumber1.index();
    public static final int NAME = AttributesEnum.Name.index();
    public static final int EDITTRANS = AttributesEnum.EditTrans.index();
    public static final int NAME_EMP = AttributesEnum.Name_Emp.index();
    public static final int EMPVIEWVVO1 = AttributesEnum.EmpViewVVO1.index();
    public static final int UNITVO2 = AttributesEnum.UnitVO2.index();
    public static final int SECCONTROLVO1 = AttributesEnum.SecControlVO1.index();

    /**
     * This is the default constructor (do not remove).
     */
    public LeaveCancelAndVerificationVORowImpl() {
    }

    /**
     * Gets LeaveCancelAndVerificationEO entity object.
     * @return the LeaveCancelAndVerificationEO
     */
    public EntityImpl getLeaveCancelAndVerificationEO() {
        return (EntityImpl) getEntity(ENTITY_LEAVECANCELANDVERIFICATIONEO);
    }

    /**
     * Gets UnitEO entity object.
     * @return the UnitEO
     */
    public EntityImpl getUnitEO() {
        return (EntityImpl) getEntity(ENTITY_UNITEO);
    }


    /**
     * Gets the attribute value for ACC_CHECK using the alias name AccCheck.
     * @return the ACC_CHECK
     */
    public Number getAccCheck() {
        return (Number) getAttributeInternal(ACCCHECK);
    }

    /**
     * Sets <code>value</code> as attribute value for ACC_CHECK using the alias name AccCheck.
     * @param value value to set the ACC_CHECK
     */
    public void setAccCheck(Number value) {
        setAttributeInternal(ACCCHECK, value);
    }

    /**
     * Gets the attribute value for APP_DATE using the alias name AppDate.
     * @return the APP_DATE
     */
    public Date getAppDate() {
        return (Date) getAttributeInternal(APPDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for APP_DATE using the alias name AppDate.
     * @param value value to set the APP_DATE
     */
    public void setAppDate(Date value) {
        setAttributeInternal(APPDATE, value);
    }

    /**
     * Gets the attribute value for APP_NO using the alias name AppNo.
     * @return the APP_NO
     */
    public String getAppNo() {
        return (String) getAttributeInternal(APPNO);
    }

    /**
     * Sets <code>value</code> as attribute value for APP_NO using the alias name AppNo.
     * @param value value to set the APP_NO
     */
    public void setAppNo(String value) {
        setAttributeInternal(APPNO, value);
    }

    /**
     * Gets the attribute value for APPR_CANCEL_DT using the alias name ApprCancelDt.
     * @return the APPR_CANCEL_DT
     */
    public Date getApprCancelDt() {
        return (Date) getAttributeInternal(APPRCANCELDT);
    }

    /**
     * Sets <code>value</code> as attribute value for APPR_CANCEL_DT using the alias name ApprCancelDt.
     * @param value value to set the APPR_CANCEL_DT
     */
    public void setApprCancelDt(Date value) {
        setAttributeInternal(APPRCANCELDT, value);
    }

    /**
     * Gets the attribute value for APPR_CANCEL_HEAD using the alias name ApprCancelHead.
     * @return the APPR_CANCEL_HEAD
     */
    public String getApprCancelHead() {
        return (String) getAttributeInternal(APPRCANCELHEAD);
    }

    /**
     * Sets <code>value</code> as attribute value for APPR_CANCEL_HEAD using the alias name ApprCancelHead.
     * @param value value to set the APPR_CANCEL_HEAD
     */
    public void setApprCancelHead(String value) {
        setAttributeInternal(APPRCANCELHEAD, value);
    }

    /**
     * Gets the attribute value for CANCEL_DAYS using the alias name CancelDays.
     * @return the CANCEL_DAYS
     */
    public BigDecimal getCancelDays() {
        
//                System.out.println("From date 1"+getAttributeInternal(CANCELFRDT));
//                  System.out.println("To date 1"+getAttributeInternal(CANCELTODT));
//        oracle.jbo.domain.Number days=null;;
//                  
//                  if(getAttributeInternal(CANCELFRDT) != null && getAttributeInternal(CANCELTODT) !=null)
//                  {
//                          
//                //            oracle.jbo.domain.Date FrDt=getCancelFrdt();
//                //                oracle.jbo.domain.Date ToDt=getCancelTodt();
//                //                System.out.println("From date 2"+FrDt);
//                //                System.out.println("To date 2"+ToDt);
//                //
//                //                Timestamp Fd = FrDt.timestampValue();
//                //                Timestamp Td = ToDt.timestampValue();
//                          
//                            oracle.jbo.domain.Timestamp Fd=getCancelFrdt();
//                            oracle.jbo.domain.Timestamp Td=getCancelTodt();
//                          
//                          
//                          System.out.println("Timestamp From date 3--> "+Fd);
//                          System.out.println("Timestamp To date 3 ---> "+Td);
//                          
//                          long ndays=((Td.getTime()-Fd.getTime())/86400000)+1;
//                          System.out.println("Number of days"+ndays);
//                          days = new oracle.jbo.domain.Number(ndays);
//                          System.out.println("Number of days 2 --> "+days);
//                        setAttributeInternal(CANCELDAYS, days);
//                     // return days;
//        
//                      }
        return (BigDecimal) getAttributeInternal(CANCELDAYS);
       
    }

    /**
     * Sets <code>value</code> as attribute value for CANCEL_DAYS using the alias name CancelDays.
     * @param value value to set the CANCEL_DAYS
     */
    public void setCancelDays(BigDecimal value) {
        setAttributeInternal(CANCELDAYS, value);
    }

    /**
     * Gets the attribute value for CANCEL_FRDT using the alias name CancelFrdt.
     * @return the CANCEL_FRDT
     */
    public Timestamp getCancelFrdt() {
        return (Timestamp) getAttributeInternal(CANCELFRDT);
    }

    /**
     * Sets <code>value</code> as attribute value for CANCEL_FRDT using the alias name CancelFrdt.
     * @param value value to set the CANCEL_FRDT
     */
    public void setCancelFrdt(Timestamp value) {
        setAttributeInternal(CANCELFRDT, value);
    }

    /**
     * Gets the attribute value for CANCEL_TODT using the alias name CancelTodt.
     * @return the CANCEL_TODT
     */
    public Timestamp getCancelTodt() {
        return (Timestamp) getAttributeInternal(CANCELTODT);
    }

    /**
     * Sets <code>value</code> as attribute value for CANCEL_TODT using the alias name CancelTodt.
     * @param value value to set the CANCEL_TODT
     */
    public void setCancelTodt(Timestamp value) {
        setAttributeInternal(CANCELTODT, value);
    }

    /**
     * Gets the attribute value for CHK_VER_BY using the alias name ChkVerBy.
     * @return the CHK_VER_BY
     */
    public String getChkVerBy() {
        return (String) getAttributeInternal(CHKVERBY);
    }

    /**
     * Sets <code>value</code> as attribute value for CHK_VER_BY using the alias name ChkVerBy.
     * @param value value to set the CHK_VER_BY
     */
    public void setChkVerBy(String value) {
        setAttributeInternal(CHKVERBY, value);
    }

    /**
     * Gets the attribute value for CREATED_BY using the alias name CreatedBy.
     * @return the CREATED_BY
     */
    public String getCreatedBy() {
        return (String) getAttributeInternal(CREATEDBY);
    }

    /**
     * Gets the attribute value for CREATION_DATE using the alias name CreationDate.
     * @return the CREATION_DATE
     */
    public Timestamp getCreationDate() {
        return (Timestamp) getAttributeInternal(CREATIONDATE);
    }

    /**
     * Gets the attribute value for EMP_NAME using the alias name EmpName.
     * @return the EMP_NAME
     */
    public String getEmpName() {
        return (String) getAttributeInternal(EMPNAME);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_NAME using the alias name EmpName.
     * @param value value to set the EMP_NAME
     */
    public void setEmpName(String value) {
        setAttributeInternal(EMPNAME, value);
    }

    /**
     * Gets the attribute value for EMP_NUMBER using the alias name EmpNumber.
     * @return the EMP_NUMBER
     */
    public String getEmpNumber() {
        return (String) getAttributeInternal(EMPNUMBER);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_NUMBER using the alias name EmpNumber.
     * @param value value to set the EMP_NUMBER
     */
    public void setEmpNumber(String value) {
        setAttributeInternal(EMPNUMBER, value);
    }

    /**
     * Gets the attribute value for END_DATE using the alias name EndDate.
     * @return the END_DATE
     */
    public Timestamp getEndDate() {
        return (Timestamp) getAttributeInternal(ENDDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for END_DATE using the alias name EndDate.
     * @param value value to set the END_DATE
     */
    public void setEndDate(Timestamp value) {
        setAttributeInternal(ENDDATE, value);
    }

    /**
     * Gets the attribute value for FROM_DATE using the alias name FromDate.
     * @return the FROM_DATE
     */
    public Timestamp getFromDate() {
        return (Timestamp) getAttributeInternal(FROMDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for FROM_DATE using the alias name FromDate.
     * @param value value to set the FROM_DATE
     */
    public void setFromDate(Timestamp value) {
        setAttributeInternal(FROMDATE, value);
    }

    /**
     * Gets the attribute value for LAST_UPDATE_DATE using the alias name LastUpdateDate.
     * @return the LAST_UPDATE_DATE
     */
    public Timestamp getLastUpdateDate() {
        return (Timestamp) getAttributeInternal(LASTUPDATEDATE);
    }

    /**
     * Gets the attribute value for LAST_UPDATED_BY using the alias name LastUpdatedBy.
     * @return the LAST_UPDATED_BY
     */
    public String getLastUpdatedBy() {
        return (String) getAttributeInternal(LASTUPDATEDBY);
    }

    /**
     * Gets the attribute value for LEAVE_APPR_CANCEL_TEMP_ID using the alias name LeaveApprCancelTempId.
     * @return the LEAVE_APPR_CANCEL_TEMP_ID
     */
    public Number getLeaveApprCancelTempId() {
        return (Number) getAttributeInternal(LEAVEAPPRCANCELTEMPID);
    }

    /**
     * Sets <code>value</code> as attribute value for LEAVE_APPR_CANCEL_TEMP_ID using the alias name LeaveApprCancelTempId.
     * @param value value to set the LEAVE_APPR_CANCEL_TEMP_ID
     */
    public void setLeaveApprCancelTempId(Number value) {
        setAttributeInternal(LEAVEAPPRCANCELTEMPID, value);
    }

    /**
     * Gets the attribute value for LEAVE_DESC_TYPE using the alias name LeaveDescType.
     * @return the LEAVE_DESC_TYPE
     */
    public String getLeaveDescType() {
        return (String) getAttributeInternal(LEAVEDESCTYPE);
    }

    /**
     * Sets <code>value</code> as attribute value for LEAVE_DESC_TYPE using the alias name LeaveDescType.
     * @param value value to set the LEAVE_DESC_TYPE
     */
    public void setLeaveDescType(String value) {
        setAttributeInternal(LEAVEDESCTYPE, value);
    }

    /**
     * Gets the attribute value for LEAVE_GRANTED using the alias name LeaveGranted.
     * @return the LEAVE_GRANTED
     */
    public String getLeaveGranted() {
        return (String) getAttributeInternal(LEAVEGRANTED);
    }

    /**
     * Sets <code>value</code> as attribute value for LEAVE_GRANTED using the alias name LeaveGranted.
     * @param value value to set the LEAVE_GRANTED
     */
    public void setLeaveGranted(String value) {
        setAttributeInternal(LEAVEGRANTED, value);
        //setAttributeInternal(LEAVEGRANTEDPRE, value);
        
    }

    /**
     * Gets the attribute value for LEAVE_GRANTED_PRE using the alias name LeaveGrantedPre.
     * @return the LEAVE_GRANTED_PRE
     */
    public String getLeaveGrantedPre() {
        return (String) getAttributeInternal(LEAVEGRANTEDPRE);
    }

    /**
     * Sets <code>value</code> as attribute value for LEAVE_GRANTED_PRE using the alias name LeaveGrantedPre.
     * @param value value to set the LEAVE_GRANTED_PRE
     */
    public void setLeaveGrantedPre(String value) {
        setAttributeInternal(LEAVEGRANTEDPRE, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Number getObjectVersionNumber() {
        return (Number) getAttributeInternal(OBJECTVERSIONNUMBER);
    }

    /**
     * Gets the attribute value for REMARKS using the alias name Remarks.
     * @return the REMARKS
     */
    public String getRemarks() {
        return (String) getAttributeInternal(REMARKS);
    }

    /**
     * Sets <code>value</code> as attribute value for REMARKS using the alias name Remarks.
     * @param value value to set the REMARKS
     */
    public void setRemarks(String value) {
        setAttributeInternal(REMARKS, value);
    }

    /**
     * Gets the attribute value for UNIT_CD using the alias name UnitCd.
     * @return the UNIT_CD
     */
    public String getUnitCd() {
        return (String) getAttributeInternal(UNITCD);
    }

    /**
     * Sets <code>value</code> as attribute value for UNIT_CD using the alias name UnitCd.
     * @param value value to set the UNIT_CD
     */
    public void setUnitCd(String value) {
        setAttributeInternal(UNITCD, value);
    }

    /**
     * Gets the attribute value for VER_BY using the alias name VerBy.
     * @return the VER_BY
     */
    public String getVerBy() {
        return (String) getAttributeInternal(VERBY);
    }

    /**
     * Sets <code>value</code> as attribute value for VER_BY using the alias name VerBy.
     * @param value value to set the VER_BY
     */
    public void setVerBy(String value) {
        setAttributeInternal(VERBY, value);
    }

    /**
     * Gets the attribute value for CODE using the alias name Code.
     * @return the CODE
     */
    public String getCode() {
        return (String) getAttributeInternal(CODE);
    }

    /**
     * Sets <code>value</code> as attribute value for CODE using the alias name Code.
     * @param value value to set the CODE
     */
    public void setCode(String value) {
        setAttributeInternal(CODE, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber1.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber1() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER1);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber1.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber1(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER1, value);
    }

    /**
     * Gets the attribute value for NAME using the alias name Name.
     * @return the NAME
     */
    public String getName() {
        return (String) getAttributeInternal(NAME);
    }

    /**
     * Sets <code>value</code> as attribute value for NAME using the alias name Name.
     * @param value value to set the NAME
     */
    public void setName(String value) {
        setAttributeInternal(NAME, value);
    }

    /**
     * Gets the attribute value for the calculated attribute EditTrans.
     * @return the EditTrans
     */
    public Integer getEditTrans() {
        byte entityState = this.getEntity(0).getEntityState();
        return new Integer(entityState);
        //return (Number) getAttributeInternal(EDITTRANS);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute EditTrans.
     * @param value value to set the  EditTrans
     */
    public void setEditTrans(Integer value) {
        setAttributeInternal(EDITTRANS, value);
    }


    /**
     * Gets the attribute value for the calculated attribute Name_Emp.
     * @return the Name_Emp
     */
    public String getName_Emp() {
        return (String) getAttributeInternal(NAME_EMP);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute Name_Emp.
     * @param value value to set the  Name_Emp
     */
    public void setName_Emp(String value) {
        setAttributeInternal(NAME_EMP, value);
    }

    /**
     * Gets the view accessor <code>RowSet</code> EmpViewVVO1.
     */
    public RowSet getEmpViewVVO1() {
        return (RowSet) getAttributeInternal(EMPVIEWVVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> UnitVO2.
     */
    public RowSet getUnitVO2() {
        return (RowSet) getAttributeInternal(UNITVO2);
    }


    /**
     * Gets the view accessor <code>RowSet</code> SecControlVO1.
     */
    public RowSet getSecControlVO1() {
        return (RowSet) getAttributeInternal(SECCONTROLVO1);
    }

    /**
     * getAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param attrDef the attribute

     * @return the attribute value
     * @throws Exception
     */
    protected Object getAttrInvokeAccessor(int index, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            return AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].get(this);
        }
        return super.getAttrInvokeAccessor(index, attrDef);
    }

    /**
     * setAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param value the value to assign to the attribute
     * @param attrDef the attribute

     * @throws Exception
     */
    protected void setAttrInvokeAccessor(int index, Object value, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].put(this, value);
            return;
        }
        super.setAttrInvokeAccessor(index, value, attrDef);
    }
    
}

