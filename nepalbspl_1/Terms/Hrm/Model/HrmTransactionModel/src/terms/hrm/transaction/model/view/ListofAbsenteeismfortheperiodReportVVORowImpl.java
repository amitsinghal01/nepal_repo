package terms.hrm.transaction.model.view;

import oracle.jbo.Row;
import oracle.jbo.RowSet;
import oracle.jbo.server.AttributeDefImpl;
import oracle.jbo.server.ViewObjectImpl;
import oracle.jbo.server.ViewRowImpl;

import terms.hrm.transaction.model.applicationModule.HrmTransactionAMImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Thu Oct 24 10:47:04 IST 2019
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class ListofAbsenteeismfortheperiodReportVVORowImpl extends ViewRowImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        Dummy {
            public Object get(ListofAbsenteeismfortheperiodReportVVORowImpl obj) {
                return obj.getDummy();
            }

            public void put(ListofAbsenteeismfortheperiodReportVVORowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        Unitcd {
            public Object get(ListofAbsenteeismfortheperiodReportVVORowImpl obj) {
                return obj.getUnitcd();
            }

            public void put(ListofAbsenteeismfortheperiodReportVVORowImpl obj, Object value) {
                obj.setUnitcd((String) value);
            }
        }
        ,
        Unitname {
            public Object get(ListofAbsenteeismfortheperiodReportVVORowImpl obj) {
                return obj.getUnitname();
            }

            public void put(ListofAbsenteeismfortheperiodReportVVORowImpl obj, Object value) {
                obj.setUnitname((String) value);
            }
        }
        ,
        Month {
            public Object get(ListofAbsenteeismfortheperiodReportVVORowImpl obj) {
                return obj.getMonth();
            }

            public void put(ListofAbsenteeismfortheperiodReportVVORowImpl obj, Object value) {
                obj.setMonth((String) value);
            }
        }
        ,
        Year {
            public Object get(ListofAbsenteeismfortheperiodReportVVORowImpl obj) {
                return obj.getYear();
            }

            public void put(ListofAbsenteeismfortheperiodReportVVORowImpl obj, Object value) {
                obj.setYear((String) value);
            }
        }
        ,
        empcd {
            public Object get(ListofAbsenteeismfortheperiodReportVVORowImpl obj) {
                return obj.getempcd();
            }

            public void put(ListofAbsenteeismfortheperiodReportVVORowImpl obj, Object value) {
                obj.setempcd((String) value);
            }
        }
        ,
        empname {
            public Object get(ListofAbsenteeismfortheperiodReportVVORowImpl obj) {
                return obj.getempname();
            }

            public void put(ListofAbsenteeismfortheperiodReportVVORowImpl obj, Object value) {
                obj.setempname((String) value);
            }
        }
        ,
        EmpViewVVO1 {
            public Object get(ListofAbsenteeismfortheperiodReportVVORowImpl obj) {
                return obj.getEmpViewVVO1();
            }

            public void put(ListofAbsenteeismfortheperiodReportVVORowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        SecControlVO1 {
            public Object get(ListofAbsenteeismfortheperiodReportVVORowImpl obj) {
                return obj.getSecControlVO1();
            }

            public void put(ListofAbsenteeismfortheperiodReportVVORowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        UnitVO1 {
            public Object get(ListofAbsenteeismfortheperiodReportVVORowImpl obj) {
                return obj.getUnitVO1();
            }

            public void put(ListofAbsenteeismfortheperiodReportVVORowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        public abstract Object get(ListofAbsenteeismfortheperiodReportVVORowImpl object);

        public abstract void put(ListofAbsenteeismfortheperiodReportVVORowImpl object, Object value);

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }
    public static final int DUMMY = AttributesEnum.Dummy.index();
    public static final int UNITCD = AttributesEnum.Unitcd.index();
    public static final int UNITNAME = AttributesEnum.Unitname.index();
    public static final int MONTH = AttributesEnum.Month.index();
    public static final int YEAR = AttributesEnum.Year.index();
    public static final int EMPCD = AttributesEnum.empcd.index();
    public static final int EMPNAME = AttributesEnum.empname.index();
    public static final int EMPVIEWVVO1 = AttributesEnum.EmpViewVVO1.index();
    public static final int SECCONTROLVO1 = AttributesEnum.SecControlVO1.index();
    public static final int UNITVO1 = AttributesEnum.UnitVO1.index();

    /**
     * This is the default constructor (do not remove).
     */
    public ListofAbsenteeismfortheperiodReportVVORowImpl() {
    }

    /**
     * Gets the attribute value for the calculated attribute Dummy.
     * @return the Dummy
     */
    public String getDummy() {
        return (String) getAttributeInternal(DUMMY);
    }

    /**
     * Gets the attribute value for the calculated attribute Unitcd.
     * @return the Unitcd
     */
    public String getUnitcd() {
        return (String) getAttributeInternal(UNITCD);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute Unitcd.
     * @param value value to set the  Unitcd
     */
    public void setUnitcd(String value) {
        setAttributeInternal(UNITCD, value);
    }

    /**
     * Gets the attribute value for the calculated attribute Unitname.
     * @return the Unitname
     */
    public String getUnitname() {
        if(getAttributeInternal(UNITNAME) == null){
                HrmTransactionAMImpl am = (HrmTransactionAMImpl) this.getApplicationModule();
                ViewObjectImpl vo=am.getUnitVO1();
                Row[] r1 = vo.getFilteredRows("Code",getUnitcd());
                if (r1.length > 0) {
                    String name = (String) r1[0].getAttribute("Name");
                    setUnitname(name); 
                } 
            }
        return (String) getAttributeInternal(UNITNAME);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute Unitname.
     * @param value value to set the  Unitname
     */
    public void setUnitname(String value) {
        setAttributeInternal(UNITNAME, value);
    }

    /**
     * Gets the attribute value for the calculated attribute Month.
     * @return the Month
     */
    public String getMonth() {
        return (String) getAttributeInternal(MONTH);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute Month.
     * @param value value to set the  Month
     */
    public void setMonth(String value) {
        setAttributeInternal(MONTH, value);
    }

    /**
     * Gets the attribute value for the calculated attribute Year.
     * @return the Year
     */
    public String getYear() {
        return (String) getAttributeInternal(YEAR);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute Year.
     * @param value value to set the  Year
     */
    public void setYear(String value) {
        setAttributeInternal(YEAR, value);
    }

    /**
     * Gets the attribute value for the calculated attribute empcd.
     * @return the empcd
     */
    public String getempcd() {
        return (String) getAttributeInternal(EMPCD);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute empcd.
     * @param value value to set the  empcd
     */
    public void setempcd(String value) {
        setAttributeInternal(EMPCD, value);
    }

    /**
     * Gets the attribute value for the calculated attribute empname.
     * @return the empname
     */
    public String getempname() {
        return (String) getAttributeInternal(EMPNAME);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute empname.
     * @param value value to set the  empname
     */
    public void setempname(String value) {
        setAttributeInternal(EMPNAME, value);
    }

    /**
     * Gets the view accessor <code>RowSet</code> EmpViewVVO1.
     */
    public RowSet getEmpViewVVO1() {
        return (RowSet) getAttributeInternal(EMPVIEWVVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> SecControlVO1.
     */
    public RowSet getSecControlVO1() {
        return (RowSet) getAttributeInternal(SECCONTROLVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> UnitVO1.
     */
    public RowSet getUnitVO1() {
        return (RowSet) getAttributeInternal(UNITVO1);
    }

    /**
     * getAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param attrDef the attribute

     * @return the attribute value
     * @throws Exception
     */
    protected Object getAttrInvokeAccessor(int index, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            return AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].get(this);
        }
        return super.getAttrInvokeAccessor(index, attrDef);
    }

    /**
     * setAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param value the value to assign to the attribute
     * @param attrDef the attribute

     * @throws Exception
     */
    protected void setAttrInvokeAccessor(int index, Object value, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].put(this, value);
            return;
        }
        super.setAttrInvokeAccessor(index, value, attrDef);
    }
}

