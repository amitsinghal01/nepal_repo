package terms.hrm.transaction.model.view;

import oracle.jbo.RowSet;
import oracle.jbo.domain.Date;
import oracle.jbo.domain.Number;
import oracle.jbo.domain.Timestamp;
import oracle.jbo.server.AttributeDefImpl;
import oracle.jbo.server.EntityImpl;
import oracle.jbo.server.ViewRowImpl;

import terms.hrm.setup.model.entity.EmployeeMasterEOImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Fri Jun 28 12:43:13 IST 2019
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class PFTransferAndWithdrawalEntryVORowImpl extends ViewRowImpl {
    public static final int ENTITY_PFTRANSFERANDWITHDRAWALENTRY1 = 0;
    public static final int ENTITY_UNITEO = 1;
    public static final int ENTITY_EMPLOYEEMASTEREO = 2;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        CreatedBy {
            public Object get(PFTransferAndWithdrawalEntryVORowImpl obj) {
                return obj.getCreatedBy();
            }

            public void put(PFTransferAndWithdrawalEntryVORowImpl obj, Object value) {
                obj.setCreatedBy((String) value);
            }
        }
        ,
        CreationDate {
            public Object get(PFTransferAndWithdrawalEntryVORowImpl obj) {
                return obj.getCreationDate();
            }

            public void put(PFTransferAndWithdrawalEntryVORowImpl obj, Object value) {
                obj.setCreationDate((Timestamp) value);
            }
        }
        ,
        EmpNo {
            public Object get(PFTransferAndWithdrawalEntryVORowImpl obj) {
                return obj.getEmpNo();
            }

            public void put(PFTransferAndWithdrawalEntryVORowImpl obj, Object value) {
                obj.setEmpNo((String) value);
            }
        }
        ,
        EmpPfTrnfWithdId {
            public Object get(PFTransferAndWithdrawalEntryVORowImpl obj) {
                return obj.getEmpPfTrnfWithdId();
            }

            public void put(PFTransferAndWithdrawalEntryVORowImpl obj, Object value) {
                obj.setEmpPfTrnfWithdId((Long) value);
            }
        }
        ,
        EntryDate {
            public Object get(PFTransferAndWithdrawalEntryVORowImpl obj) {
                return obj.getEntryDate();
            }

            public void put(PFTransferAndWithdrawalEntryVORowImpl obj, Object value) {
                obj.setEntryDate((Date) value);
            }
        }
        ,
        EntryNo {
            public Object get(PFTransferAndWithdrawalEntryVORowImpl obj) {
                return obj.getEntryNo();
            }

            public void put(PFTransferAndWithdrawalEntryVORowImpl obj, Object value) {
                obj.setEntryNo((String) value);
            }
        }
        ,
        JoinDate {
            public Object get(PFTransferAndWithdrawalEntryVORowImpl obj) {
                return obj.getJoinDate();
            }

            public void put(PFTransferAndWithdrawalEntryVORowImpl obj, Object value) {
                obj.setJoinDate((Date) value);
            }
        }
        ,
        LastUpdateDate {
            public Object get(PFTransferAndWithdrawalEntryVORowImpl obj) {
                return obj.getLastUpdateDate();
            }

            public void put(PFTransferAndWithdrawalEntryVORowImpl obj, Object value) {
                obj.setLastUpdateDate((Timestamp) value);
            }
        }
        ,
        LastUpdatedBy {
            public Object get(PFTransferAndWithdrawalEntryVORowImpl obj) {
                return obj.getLastUpdatedBy();
            }

            public void put(PFTransferAndWithdrawalEntryVORowImpl obj, Object value) {
                obj.setLastUpdatedBy((String) value);
            }
        }
        ,
        LeavingDate {
            public Object get(PFTransferAndWithdrawalEntryVORowImpl obj) {
                return obj.getLeavingDate();
            }

            public void put(PFTransferAndWithdrawalEntryVORowImpl obj, Object value) {
                obj.setLeavingDate((Date) value);
            }
        }
        ,
        LetterRefDate {
            public Object get(PFTransferAndWithdrawalEntryVORowImpl obj) {
                return obj.getLetterRefDate();
            }

            public void put(PFTransferAndWithdrawalEntryVORowImpl obj, Object value) {
                obj.setLetterRefDate((Date) value);
            }
        }
        ,
        LetterRefNo {
            public Object get(PFTransferAndWithdrawalEntryVORowImpl obj) {
                return obj.getLetterRefNo();
            }

            public void put(PFTransferAndWithdrawalEntryVORowImpl obj, Object value) {
                obj.setLetterRefNo((String) value);
            }
        }
        ,
        NewParmtAddress1 {
            public Object get(PFTransferAndWithdrawalEntryVORowImpl obj) {
                return obj.getNewParmtAddress1();
            }

            public void put(PFTransferAndWithdrawalEntryVORowImpl obj, Object value) {
                obj.setNewParmtAddress1((String) value);
            }
        }
        ,
        NewParmtAddress2 {
            public Object get(PFTransferAndWithdrawalEntryVORowImpl obj) {
                return obj.getNewParmtAddress2();
            }

            public void put(PFTransferAndWithdrawalEntryVORowImpl obj, Object value) {
                obj.setNewParmtAddress2((String) value);
            }
        }
        ,
        NewParmtCity {
            public Object get(PFTransferAndWithdrawalEntryVORowImpl obj) {
                return obj.getNewParmtCity();
            }

            public void put(PFTransferAndWithdrawalEntryVORowImpl obj, Object value) {
                obj.setNewParmtCity((String) value);
            }
        }
        ,
        NewPfNumber {
            public Object get(PFTransferAndWithdrawalEntryVORowImpl obj) {
                return obj.getNewPfNumber();
            }

            public void put(PFTransferAndWithdrawalEntryVORowImpl obj, Object value) {
                obj.setNewPfNumber((String) value);
            }
        }
        ,
        ObjectVersionNumber {
            public Object get(PFTransferAndWithdrawalEntryVORowImpl obj) {
                return obj.getObjectVersionNumber();
            }

            public void put(PFTransferAndWithdrawalEntryVORowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        PermanentAddress1 {
            public Object get(PFTransferAndWithdrawalEntryVORowImpl obj) {
                return obj.getPermanentAddress1();
            }

            public void put(PFTransferAndWithdrawalEntryVORowImpl obj, Object value) {
                obj.setPermanentAddress1((String) value);
            }
        }
        ,
        PermanentAddress2 {
            public Object get(PFTransferAndWithdrawalEntryVORowImpl obj) {
                return obj.getPermanentAddress2();
            }

            public void put(PFTransferAndWithdrawalEntryVORowImpl obj, Object value) {
                obj.setPermanentAddress2((String) value);
            }
        }
        ,
        PermanentCity {
            public Object get(PFTransferAndWithdrawalEntryVORowImpl obj) {
                return obj.getPermanentCity();
            }

            public void put(PFTransferAndWithdrawalEntryVORowImpl obj, Object value) {
                obj.setPermanentCity((String) value);
            }
        }
        ,
        PfNumber {
            public Object get(PFTransferAndWithdrawalEntryVORowImpl obj) {
                return obj.getPfNumber();
            }

            public void put(PFTransferAndWithdrawalEntryVORowImpl obj, Object value) {
                obj.setPfNumber((String) value);
            }
        }
        ,
        PfType {
            public Object get(PFTransferAndWithdrawalEntryVORowImpl obj) {
                return obj.getPfType();
            }

            public void put(PFTransferAndWithdrawalEntryVORowImpl obj, Object value) {
                obj.setPfType((String) value);
            }
        }
        ,
        UnitCd {
            public Object get(PFTransferAndWithdrawalEntryVORowImpl obj) {
                return obj.getUnitCd();
            }

            public void put(PFTransferAndWithdrawalEntryVORowImpl obj, Object value) {
                obj.setUnitCd((String) value);
            }
        }
        ,
        Name {
            public Object get(PFTransferAndWithdrawalEntryVORowImpl obj) {
                return obj.getName();
            }

            public void put(PFTransferAndWithdrawalEntryVORowImpl obj, Object value) {
                obj.setName((String) value);
            }
        }
        ,
        Code {
            public Object get(PFTransferAndWithdrawalEntryVORowImpl obj) {
                return obj.getCode();
            }

            public void put(PFTransferAndWithdrawalEntryVORowImpl obj, Object value) {
                obj.setCode((String) value);
            }
        }
        ,
        ObjectVersionNumber1 {
            public Object get(PFTransferAndWithdrawalEntryVORowImpl obj) {
                return obj.getObjectVersionNumber1();
            }

            public void put(PFTransferAndWithdrawalEntryVORowImpl obj, Object value) {
                obj.setObjectVersionNumber1((Integer) value);
            }
        }
        ,
        JoinDate1 {
            public Object get(PFTransferAndWithdrawalEntryVORowImpl obj) {
                return obj.getJoinDate1();
            }

            public void put(PFTransferAndWithdrawalEntryVORowImpl obj, Object value) {
                obj.setJoinDate1((Timestamp) value);
            }
        }
        ,
        EmpFirstName {
            public Object get(PFTransferAndWithdrawalEntryVORowImpl obj) {
                return obj.getEmpFirstName();
            }

            public void put(PFTransferAndWithdrawalEntryVORowImpl obj, Object value) {
                obj.setEmpFirstName((String) value);
            }
        }
        ,
        EmpLastName {
            public Object get(PFTransferAndWithdrawalEntryVORowImpl obj) {
                return obj.getEmpLastName();
            }

            public void put(PFTransferAndWithdrawalEntryVORowImpl obj, Object value) {
                obj.setEmpLastName((String) value);
            }
        }
        ,
        PostTillDate {
            public Object get(PFTransferAndWithdrawalEntryVORowImpl obj) {
                return obj.getPostTillDate();
            }

            public void put(PFTransferAndWithdrawalEntryVORowImpl obj, Object value) {
                obj.setPostTillDate((Timestamp) value);
            }
        }
        ,
        PfNumber1 {
            public Object get(PFTransferAndWithdrawalEntryVORowImpl obj) {
                return obj.getPfNumber1();
            }

            public void put(PFTransferAndWithdrawalEntryVORowImpl obj, Object value) {
                obj.setPfNumber1((String) value);
            }
        }
        ,
        PermanentAdd1 {
            public Object get(PFTransferAndWithdrawalEntryVORowImpl obj) {
                return obj.getPermanentAdd1();
            }

            public void put(PFTransferAndWithdrawalEntryVORowImpl obj, Object value) {
                obj.setPermanentAdd1((String) value);
            }
        }
        ,
        PermanentAdd2 {
            public Object get(PFTransferAndWithdrawalEntryVORowImpl obj) {
                return obj.getPermanentAdd2();
            }

            public void put(PFTransferAndWithdrawalEntryVORowImpl obj, Object value) {
                obj.setPermanentAdd2((String) value);
            }
        }
        ,
        EmpNumber {
            public Object get(PFTransferAndWithdrawalEntryVORowImpl obj) {
                return obj.getEmpNumber();
            }

            public void put(PFTransferAndWithdrawalEntryVORowImpl obj, Object value) {
                obj.setEmpNumber((String) value);
            }
        }
        ,
        ObjectVersionNumber2 {
            public Object get(PFTransferAndWithdrawalEntryVORowImpl obj) {
                return obj.getObjectVersionNumber2();
            }

            public void put(PFTransferAndWithdrawalEntryVORowImpl obj, Object value) {
                obj.setObjectVersionNumber2((Integer) value);
            }
        }
        ,
        EditTrans {
            public Object get(PFTransferAndWithdrawalEntryVORowImpl obj) {
                return obj.getEditTrans();
            }

            public void put(PFTransferAndWithdrawalEntryVORowImpl obj, Object value) {
                obj.setEditTrans((Integer) value);
            }
        }
        ,
        EmployeeMasterVO1 {
            public Object get(PFTransferAndWithdrawalEntryVORowImpl obj) {
                return obj.getEmployeeMasterVO1();
            }

            public void put(PFTransferAndWithdrawalEntryVORowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        UnitVO1 {
            public Object get(PFTransferAndWithdrawalEntryVORowImpl obj) {
                return obj.getUnitVO1();
            }

            public void put(PFTransferAndWithdrawalEntryVORowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        SecControlVO1 {
            public Object get(PFTransferAndWithdrawalEntryVORowImpl obj) {
                return obj.getSecControlVO1();
            }

            public void put(PFTransferAndWithdrawalEntryVORowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ;
        static AttributesEnum[] vals = null;
        ;
        private static final int firstIndex = 0;

        public abstract Object get(PFTransferAndWithdrawalEntryVORowImpl object);

        public abstract void put(PFTransferAndWithdrawalEntryVORowImpl object, Object value);

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int CREATEDBY = AttributesEnum.CreatedBy.index();
    public static final int CREATIONDATE = AttributesEnum.CreationDate.index();
    public static final int EMPNO = AttributesEnum.EmpNo.index();
    public static final int EMPPFTRNFWITHDID = AttributesEnum.EmpPfTrnfWithdId.index();
    public static final int ENTRYDATE = AttributesEnum.EntryDate.index();
    public static final int ENTRYNO = AttributesEnum.EntryNo.index();
    public static final int JOINDATE = AttributesEnum.JoinDate.index();
    public static final int LASTUPDATEDATE = AttributesEnum.LastUpdateDate.index();
    public static final int LASTUPDATEDBY = AttributesEnum.LastUpdatedBy.index();
    public static final int LEAVINGDATE = AttributesEnum.LeavingDate.index();
    public static final int LETTERREFDATE = AttributesEnum.LetterRefDate.index();
    public static final int LETTERREFNO = AttributesEnum.LetterRefNo.index();
    public static final int NEWPARMTADDRESS1 = AttributesEnum.NewParmtAddress1.index();
    public static final int NEWPARMTADDRESS2 = AttributesEnum.NewParmtAddress2.index();
    public static final int NEWPARMTCITY = AttributesEnum.NewParmtCity.index();
    public static final int NEWPFNUMBER = AttributesEnum.NewPfNumber.index();
    public static final int OBJECTVERSIONNUMBER = AttributesEnum.ObjectVersionNumber.index();
    public static final int PERMANENTADDRESS1 = AttributesEnum.PermanentAddress1.index();
    public static final int PERMANENTADDRESS2 = AttributesEnum.PermanentAddress2.index();
    public static final int PERMANENTCITY = AttributesEnum.PermanentCity.index();
    public static final int PFNUMBER = AttributesEnum.PfNumber.index();
    public static final int PFTYPE = AttributesEnum.PfType.index();
    public static final int UNITCD = AttributesEnum.UnitCd.index();
    public static final int NAME = AttributesEnum.Name.index();
    public static final int CODE = AttributesEnum.Code.index();
    public static final int OBJECTVERSIONNUMBER1 = AttributesEnum.ObjectVersionNumber1.index();
    public static final int JOINDATE1 = AttributesEnum.JoinDate1.index();
    public static final int EMPFIRSTNAME = AttributesEnum.EmpFirstName.index();
    public static final int EMPLASTNAME = AttributesEnum.EmpLastName.index();
    public static final int POSTTILLDATE = AttributesEnum.PostTillDate.index();
    public static final int PFNUMBER1 = AttributesEnum.PfNumber1.index();
    public static final int PERMANENTADD1 = AttributesEnum.PermanentAdd1.index();
    public static final int PERMANENTADD2 = AttributesEnum.PermanentAdd2.index();
    public static final int EMPNUMBER = AttributesEnum.EmpNumber.index();
    public static final int OBJECTVERSIONNUMBER2 = AttributesEnum.ObjectVersionNumber2.index();
    public static final int EDITTRANS = AttributesEnum.EditTrans.index();
    public static final int EMPLOYEEMASTERVO1 = AttributesEnum.EmployeeMasterVO1.index();
    public static final int UNITVO1 = AttributesEnum.UnitVO1.index();
    public static final int SECCONTROLVO1 = AttributesEnum.SecControlVO1.index();

    /**
     * This is the default constructor (do not remove).
     */
    public PFTransferAndWithdrawalEntryVORowImpl() {
    }

    /**
     * Gets PFTransferAndWithdrawalEntry1 entity object.
     * @return the PFTransferAndWithdrawalEntry1
     */
    public EntityImpl getPFTransferAndWithdrawalEntry1() {
        return (EntityImpl) getEntity(ENTITY_PFTRANSFERANDWITHDRAWALENTRY1);
    }

    /**
     * Gets UnitEO entity object.
     * @return the UnitEO
     */
    public EntityImpl getUnitEO() {
        return (EntityImpl) getEntity(ENTITY_UNITEO);
    }

    /**
     * Gets EmployeeMasterEO entity object.
     * @return the EmployeeMasterEO
     */
    public EmployeeMasterEOImpl getEmployeeMasterEO() {
        return (EmployeeMasterEOImpl) getEntity(ENTITY_EMPLOYEEMASTEREO);
    }

    /**
     * Gets the attribute value for CREATED_BY using the alias name CreatedBy.
     * @return the CREATED_BY
     */
    public String getCreatedBy() {
        return (String) getAttributeInternal(CREATEDBY);
    }

    /**
     * Sets <code>value</code> as attribute value for CREATED_BY using the alias name CreatedBy.
     * @param value value to set the CREATED_BY
     */
    public void setCreatedBy(String value) {
        setAttributeInternal(CREATEDBY, value);
    }

    /**
     * Gets the attribute value for CREATION_DATE using the alias name CreationDate.
     * @return the CREATION_DATE
     */
    public Timestamp getCreationDate() {
        return (Timestamp) getAttributeInternal(CREATIONDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for CREATION_DATE using the alias name CreationDate.
     * @param value value to set the CREATION_DATE
     */
    public void setCreationDate(Timestamp value) {
        setAttributeInternal(CREATIONDATE, value);
    }

    /**
     * Gets the attribute value for EMP_NO using the alias name EmpNo.
     * @return the EMP_NO
     */
    public String getEmpNo() {
        return (String) getAttributeInternal(EMPNO);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_NO using the alias name EmpNo.
     * @param value value to set the EMP_NO
     */
    public void setEmpNo(String value) {
        setAttributeInternal(EMPNO, value);
    }

    /**
     * Gets the attribute value for EMP_PF_TRNF_WITHD_ID using the alias name EmpPfTrnfWithdId.
     * @return the EMP_PF_TRNF_WITHD_ID
     */
    public Long getEmpPfTrnfWithdId() {
        return (Long) getAttributeInternal(EMPPFTRNFWITHDID);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_PF_TRNF_WITHD_ID using the alias name EmpPfTrnfWithdId.
     * @param value value to set the EMP_PF_TRNF_WITHD_ID
     */
    public void setEmpPfTrnfWithdId(Long value) {
        setAttributeInternal(EMPPFTRNFWITHDID, value);
    }

    /**
     * Gets the attribute value for ENTRY_DATE using the alias name EntryDate.
     * @return the ENTRY_DATE
     */
    public Date getEntryDate() {
        return (Date) getAttributeInternal(ENTRYDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for ENTRY_DATE using the alias name EntryDate.
     * @param value value to set the ENTRY_DATE
     */
    public void setEntryDate(Date value) {
        setAttributeInternal(ENTRYDATE, value);
    }

    /**
     * Gets the attribute value for ENTRY_NO using the alias name EntryNo.
     * @return the ENTRY_NO
     */
    public String getEntryNo() {
        return (String) getAttributeInternal(ENTRYNO);
    }

    /**
     * Sets <code>value</code> as attribute value for ENTRY_NO using the alias name EntryNo.
     * @param value value to set the ENTRY_NO
     */
    public void setEntryNo(String value) {
        setAttributeInternal(ENTRYNO, value);
    }

    /**
     * Gets the attribute value for JOIN_DATE using the alias name JoinDate.
     * @return the JOIN_DATE
     */
    public Date getJoinDate() {
        return (Date) getAttributeInternal(JOINDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for JOIN_DATE using the alias name JoinDate.
     * @param value value to set the JOIN_DATE
     */
    public void setJoinDate(Date value) {
        setAttributeInternal(JOINDATE, value);
    }

    /**
     * Gets the attribute value for LAST_UPDATE_DATE using the alias name LastUpdateDate.
     * @return the LAST_UPDATE_DATE
     */
    public oracle.jbo.domain.Timestamp getLastUpdateDate() {
        return (oracle.jbo.domain.Timestamp) getAttributeInternal(LASTUPDATEDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for LAST_UPDATE_DATE using the alias name LastUpdateDate.
     * @param value value to set the LAST_UPDATE_DATE
     */
    public void setLastUpdateDate(oracle.jbo.domain.Timestamp value) {
        setAttributeInternal(LASTUPDATEDATE, value);
    }

    /**
     * Gets the attribute value for LAST_UPDATED_BY using the alias name LastUpdatedBy.
     * @return the LAST_UPDATED_BY
     */
    public String getLastUpdatedBy() {
        return (String) getAttributeInternal(LASTUPDATEDBY);
    }

    /**
     * Sets <code>value</code> as attribute value for LAST_UPDATED_BY using the alias name LastUpdatedBy.
     * @param value value to set the LAST_UPDATED_BY
     */
    public void setLastUpdatedBy(String value) {
        setAttributeInternal(LASTUPDATEDBY, value);
    }

    /**
     * Gets the attribute value for LEAVING_DATE using the alias name LeavingDate.
     * @return the LEAVING_DATE
     */
    public Date getLeavingDate() {
        return (Date) getAttributeInternal(LEAVINGDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for LEAVING_DATE using the alias name LeavingDate.
     * @param value value to set the LEAVING_DATE
     */
    public void setLeavingDate(Date value) {
        setAttributeInternal(LEAVINGDATE, value);
    }

    /**
     * Gets the attribute value for LETTER_REF_DATE using the alias name LetterRefDate.
     * @return the LETTER_REF_DATE
     */
    public Date getLetterRefDate() {
        return (Date) getAttributeInternal(LETTERREFDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for LETTER_REF_DATE using the alias name LetterRefDate.
     * @param value value to set the LETTER_REF_DATE
     */
    public void setLetterRefDate(Date value) {
        setAttributeInternal(LETTERREFDATE, value);
    }

    /**
     * Gets the attribute value for LETTER_REF_NO using the alias name LetterRefNo.
     * @return the LETTER_REF_NO
     */
    public String getLetterRefNo() {
        return (String) getAttributeInternal(LETTERREFNO);
    }

    /**
     * Sets <code>value</code> as attribute value for LETTER_REF_NO using the alias name LetterRefNo.
     * @param value value to set the LETTER_REF_NO
     */
    public void setLetterRefNo(String value) {
        setAttributeInternal(LETTERREFNO, value);
    }

    /**
     * Gets the attribute value for NEW_PARMT_ADDRESS1 using the alias name NewParmtAddress1.
     * @return the NEW_PARMT_ADDRESS1
     */
    public String getNewParmtAddress1() {
        return (String) getAttributeInternal(NEWPARMTADDRESS1);
    }

    /**
     * Sets <code>value</code> as attribute value for NEW_PARMT_ADDRESS1 using the alias name NewParmtAddress1.
     * @param value value to set the NEW_PARMT_ADDRESS1
     */
    public void setNewParmtAddress1(String value) {
        setAttributeInternal(NEWPARMTADDRESS1, value);
    }

    /**
     * Gets the attribute value for NEW_PARMT_ADDRESS2 using the alias name NewParmtAddress2.
     * @return the NEW_PARMT_ADDRESS2
     */
    public String getNewParmtAddress2() {
        return (String) getAttributeInternal(NEWPARMTADDRESS2);
    }

    /**
     * Sets <code>value</code> as attribute value for NEW_PARMT_ADDRESS2 using the alias name NewParmtAddress2.
     * @param value value to set the NEW_PARMT_ADDRESS2
     */
    public void setNewParmtAddress2(String value) {
        setAttributeInternal(NEWPARMTADDRESS2, value);
    }

    /**
     * Gets the attribute value for NEW_PARMT_CITY using the alias name NewParmtCity.
     * @return the NEW_PARMT_CITY
     */
    public String getNewParmtCity() {
        return (String) getAttributeInternal(NEWPARMTCITY);
    }

    /**
     * Sets <code>value</code> as attribute value for NEW_PARMT_CITY using the alias name NewParmtCity.
     * @param value value to set the NEW_PARMT_CITY
     */
    public void setNewParmtCity(String value) {
        setAttributeInternal(NEWPARMTCITY, value);
    }

    /**
     * Gets the attribute value for NEW_PF_NUMBER using the alias name NewPfNumber.
     * @return the NEW_PF_NUMBER
     */
    public String getNewPfNumber() {
        return (String) getAttributeInternal(NEWPFNUMBER);
    }

    /**
     * Sets <code>value</code> as attribute value for NEW_PF_NUMBER using the alias name NewPfNumber.
     * @param value value to set the NEW_PF_NUMBER
     */
    public void setNewPfNumber(String value) {
        setAttributeInternal(NEWPFNUMBER, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Number getObjectVersionNumber() {
        return (Number) getAttributeInternal(OBJECTVERSIONNUMBER);
    }

    /**
     * Gets the attribute value for PERMANENT_ADDRESS1 using the alias name PermanentAddress1.
     * @return the PERMANENT_ADDRESS1
     */
    public String getPermanentAddress1() {
        return (String) getAttributeInternal(PERMANENTADDRESS1);
    }

    /**
     * Sets <code>value</code> as attribute value for PERMANENT_ADDRESS1 using the alias name PermanentAddress1.
     * @param value value to set the PERMANENT_ADDRESS1
     */
    public void setPermanentAddress1(String value) {
        setAttributeInternal(PERMANENTADDRESS1, value);
    }

    /**
     * Gets the attribute value for PERMANENT_ADDRESS2 using the alias name PermanentAddress2.
     * @return the PERMANENT_ADDRESS2
     */
    public String getPermanentAddress2() {
        return (String) getAttributeInternal(PERMANENTADDRESS2);
    }

    /**
     * Sets <code>value</code> as attribute value for PERMANENT_ADDRESS2 using the alias name PermanentAddress2.
     * @param value value to set the PERMANENT_ADDRESS2
     */
    public void setPermanentAddress2(String value) {
        setAttributeInternal(PERMANENTADDRESS2, value);
    }

    /**
     * Gets the attribute value for PERMANENT_CITY using the alias name PermanentCity.
     * @return the PERMANENT_CITY
     */
    public String getPermanentCity() {
        return (String) getAttributeInternal(PERMANENTCITY);
    }

    /**
     * Sets <code>value</code> as attribute value for PERMANENT_CITY using the alias name PermanentCity.
     * @param value value to set the PERMANENT_CITY
     */
    public void setPermanentCity(String value) {
        setAttributeInternal(PERMANENTCITY, value);
    }

    /**
     * Gets the attribute value for PF_NUMBER using the alias name PfNumber.
     * @return the PF_NUMBER
     */
    public String getPfNumber() {
        return (String) getAttributeInternal(PFNUMBER);
    }

    /**
     * Sets <code>value</code> as attribute value for PF_NUMBER using the alias name PfNumber.
     * @param value value to set the PF_NUMBER
     */
    public void setPfNumber(String value) {
        setAttributeInternal(PFNUMBER, value);
    }

    /**
     * Gets the attribute value for PF_TYPE using the alias name PfType.
     * @return the PF_TYPE
     */
    public String getPfType() {
        return (String) getAttributeInternal(PFTYPE);
    }

    /**
     * Sets <code>value</code> as attribute value for PF_TYPE using the alias name PfType.
     * @param value value to set the PF_TYPE
     */
    public void setPfType(String value) {
        setAttributeInternal(PFTYPE, value);
    }

    /**
     * Gets the attribute value for UNIT_CD using the alias name UnitCd.
     * @return the UNIT_CD
     */
    public String getUnitCd() {
        return (String) getAttributeInternal(UNITCD);
    }

    /**
     * Sets <code>value</code> as attribute value for UNIT_CD using the alias name UnitCd.
     * @param value value to set the UNIT_CD
     */
    public void setUnitCd(String value) {
        setAttributeInternal(UNITCD, value);
    }

    /**
     * Gets the attribute value for NAME using the alias name Name.
     * @return the NAME
     */
    public String getName() {
        return (String) getAttributeInternal(NAME);
    }

    /**
     * Sets <code>value</code> as attribute value for NAME using the alias name Name.
     * @param value value to set the NAME
     */
    public void setName(String value) {
        setAttributeInternal(NAME, value);
    }

    /**
     * Gets the attribute value for CODE using the alias name Code.
     * @return the CODE
     */
    public String getCode() {
        return (String) getAttributeInternal(CODE);
    }

    /**
     * Sets <code>value</code> as attribute value for CODE using the alias name Code.
     * @param value value to set the CODE
     */
    public void setCode(String value) {
        setAttributeInternal(CODE, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber1.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber1() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER1);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber1.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber1(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER1, value);
    }

    /**
     * Gets the attribute value for JOIN_DATE using the alias name JoinDate1.
     * @return the JOIN_DATE
     */
    public Timestamp getJoinDate1() {
        return (Timestamp) getAttributeInternal(JOINDATE1);
    }

    /**
     * Sets <code>value</code> as attribute value for JOIN_DATE using the alias name JoinDate1.
     * @param value value to set the JOIN_DATE
     */
    public void setJoinDate1(Timestamp value) {
        setAttributeInternal(JOINDATE1, value);
    }

    /**
     * Gets the attribute value for EMP_FIRST_NAME using the alias name EmpFirstName.
     * @return the EMP_FIRST_NAME
     */
    public String getEmpFirstName() {
        if((getAttributeInternal(EMPFIRSTNAME)!=null) &&
                (getAttributeInternal(EMPLASTNAME)!=null)){
                             return (String) getAttributeInternal(EMPFIRSTNAME)
        + " " +
                (String) getAttributeInternal(EMPLASTNAME);
                         }else{
                             return (String) getAttributeInternal(EMPFIRSTNAME);
                         }
       // return (String) getAttributeInternal(EMPFIRSTNAME);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_FIRST_NAME using the alias name EmpFirstName.
     * @param value value to set the EMP_FIRST_NAME
     */
    public void setEmpFirstName(String value) {
        setAttributeInternal(EMPFIRSTNAME, value);
    }

    /**
     * Gets the attribute value for EMP_LAST_NAME using the alias name EmpLastName.
     * @return the EMP_LAST_NAME
     */
    public String getEmpLastName() {
        return (String) getAttributeInternal(EMPLASTNAME);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_LAST_NAME using the alias name EmpLastName.
     * @param value value to set the EMP_LAST_NAME
     */
    public void setEmpLastName(String value) {
        setAttributeInternal(EMPLASTNAME, value);
    }

    /**
     * Gets the attribute value for POST_TILL_DATE using the alias name PostTillDate.
     * @return the POST_TILL_DATE
     */
    public Timestamp getPostTillDate() {
        return (Timestamp) getAttributeInternal(POSTTILLDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for POST_TILL_DATE using the alias name PostTillDate.
     * @param value value to set the POST_TILL_DATE
     */
    public void setPostTillDate(Timestamp value) {
        setAttributeInternal(POSTTILLDATE, value);
    }

    /**
     * Gets the attribute value for PF_NUMBER using the alias name PfNumber1.
     * @return the PF_NUMBER
     */
    public String getPfNumber1() {
        return (String) getAttributeInternal(PFNUMBER1);
    }

    /**
     * Sets <code>value</code> as attribute value for PF_NUMBER using the alias name PfNumber1.
     * @param value value to set the PF_NUMBER
     */
    public void setPfNumber1(String value) {
        setAttributeInternal(PFNUMBER1, value);
    }

    /**
     * Gets the attribute value for PERMANENT_ADD1 using the alias name PermanentAdd1.
     * @return the PERMANENT_ADD1
     */
    public String getPermanentAdd1() {
        return (String) getAttributeInternal(PERMANENTADD1);
    }

    /**
     * Sets <code>value</code> as attribute value for PERMANENT_ADD1 using the alias name PermanentAdd1.
     * @param value value to set the PERMANENT_ADD1
     */
    public void setPermanentAdd1(String value) {
        setAttributeInternal(PERMANENTADD1, value);
    }

    /**
     * Gets the attribute value for PERMANENT_ADD2 using the alias name PermanentAdd2.
     * @return the PERMANENT_ADD2
     */
    public String getPermanentAdd2() {
        return (String) getAttributeInternal(PERMANENTADD2);
    }

    /**
     * Sets <code>value</code> as attribute value for PERMANENT_ADD2 using the alias name PermanentAdd2.
     * @param value value to set the PERMANENT_ADD2
     */
    public void setPermanentAdd2(String value) {
        setAttributeInternal(PERMANENTADD2, value);
    }

    /**
     * Gets the attribute value for EMP_NUMBER using the alias name EmpNumber.
     * @return the EMP_NUMBER
     */
    public String getEmpNumber() {
        return (String) getAttributeInternal(EMPNUMBER);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_NUMBER using the alias name EmpNumber.
     * @param value value to set the EMP_NUMBER
     */
    public void setEmpNumber(String value) {
        setAttributeInternal(EMPNUMBER, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber2.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber2() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER2);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber2.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber2(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER2, value);
    }

    /**
     * Gets the attribute value for the calculated attribute EditTrans.
     * @return the EditTrans
     */
    public Integer getEditTrans() {
        byte entityState = this.getEntity(0).getEntityState();
        System.out.println("Entity State:"+entityState);       
               return new Integer(entityState);
       // return (Integer) getAttributeInternal(EDITTRANS);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute EditTrans.
     * @param value value to set the  EditTrans
     */
    public void setEditTrans(Integer value) {
        setAttributeInternal(EDITTRANS, value);
    }

    /**
     * Gets the view accessor <code>RowSet</code> EmployeeMasterVO1.
     */
    public RowSet getEmployeeMasterVO1() {
        return (RowSet) getAttributeInternal(EMPLOYEEMASTERVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> UnitVO1.
     */
    public RowSet getUnitVO1() {
        return (RowSet) getAttributeInternal(UNITVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> SecControlVO1.
     */
    public RowSet getSecControlVO1() {
        return (RowSet) getAttributeInternal(SECCONTROLVO1);
    }

    /**
     * getAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param attrDef the attribute

     * @return the attribute value
     * @throws Exception
     */
    protected Object getAttrInvokeAccessor(int index, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            return AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].get(this);
        }
        return super.getAttrInvokeAccessor(index, attrDef);
    }

    /**
     * setAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param value the value to assign to the attribute
     * @param attrDef the attribute

     * @throws Exception
     */
    protected void setAttrInvokeAccessor(int index, Object value, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].put(this, value);
            return;
        }
        super.setAttrInvokeAccessor(index, value, attrDef);
    }
}

