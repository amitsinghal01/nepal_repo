package terms.hrm.transaction.model.view;

import oracle.jbo.Row;
import oracle.jbo.RowSet;
import oracle.jbo.server.AttributeDefImpl;
import oracle.jbo.server.ViewRowImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Tue Nov 12 15:39:49 IST 2019
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class PfTransferReportVVORowImpl extends ViewRowImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        Dummy {
            public Object get(PfTransferReportVVORowImpl obj) {
                return obj.getDummy();
            }

            public void put(PfTransferReportVVORowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        EmpCode {
            public Object get(PfTransferReportVVORowImpl obj) {
                return obj.getEmpCode();
            }

            public void put(PfTransferReportVVORowImpl obj, Object value) {
                obj.setEmpCode((String) value);
            }
        }
        ,
        EmpName {
            public Object get(PfTransferReportVVORowImpl obj) {
                return obj.getEmpName();
            }

            public void put(PfTransferReportVVORowImpl obj, Object value) {
                obj.setEmpName((String) value);
            }
        }
        ,
        UnitCode {
            public Object get(PfTransferReportVVORowImpl obj) {
                return obj.getUnitCode();
            }

            public void put(PfTransferReportVVORowImpl obj, Object value) {
                obj.setUnitCode((String) value);
            }
        }
        ,
        UnitName {
            public Object get(PfTransferReportVVORowImpl obj) {
                return obj.getUnitName();
            }

            public void put(PfTransferReportVVORowImpl obj, Object value) {
                obj.setUnitName((String) value);
            }
        }
        ,
        UnitVO1 {
            public Object get(PfTransferReportVVORowImpl obj) {
                return obj.getUnitVO1();
            }

            public void put(PfTransferReportVVORowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        EmpViewVVO1 {
            public Object get(PfTransferReportVVORowImpl obj) {
                return obj.getEmpViewVVO1();
            }

            public void put(PfTransferReportVVORowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        PFTransferAndWithdrawalEntryVO1 {
            public Object get(PfTransferReportVVORowImpl obj) {
                return obj.getPFTransferAndWithdrawalEntryVO1();
            }

            public void put(PfTransferReportVVORowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        public abstract Object get(PfTransferReportVVORowImpl object);

        public abstract void put(PfTransferReportVVORowImpl object, Object value);

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int DUMMY = AttributesEnum.Dummy.index();
    public static final int EMPCODE = AttributesEnum.EmpCode.index();
    public static final int EMPNAME = AttributesEnum.EmpName.index();
    public static final int UNITCODE = AttributesEnum.UnitCode.index();
    public static final int UNITNAME = AttributesEnum.UnitName.index();
    public static final int UNITVO1 = AttributesEnum.UnitVO1.index();
    public static final int EMPVIEWVVO1 = AttributesEnum.EmpViewVVO1.index();
    public static final int PFTRANSFERANDWITHDRAWALENTRYVO1 = AttributesEnum.PFTransferAndWithdrawalEntryVO1.index();

    /**
     * This is the default constructor (do not remove).
     */
    public PfTransferReportVVORowImpl() {
    }

    /**
     * Gets the attribute value for the calculated attribute Dummy.
     * @return the Dummy
     */
    public String getDummy() {
        return (String) getAttributeInternal(DUMMY);
    }

    /**
     * Gets the attribute value for the calculated attribute EmpCode.
     * @return the EmpCode
     */
    public String getEmpCode() {
        return (String) getAttributeInternal(EMPCODE);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute EmpCode.
     * @param value value to set the  EmpCode
     */
    public void setEmpCode(String value) {
        setAttributeInternal(EMPCODE, value);
    }

    /**
     * Gets the attribute value for the calculated attribute EmpName.
     * @return the EmpName
     */
    public String getEmpName() {
        return (String) getAttributeInternal(EMPNAME);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute EmpName.
     * @param value value to set the  EmpName
     */
    public void setEmpName(String value) {
        setAttributeInternal(EMPNAME, value);
    }

    /**
     * Gets the attribute value for the calculated attribute UnitCode.
     * @return the UnitCode
     */
    public String getUnitCode() {
        return (String) getAttributeInternal(UNITCODE);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute UnitCode.
     * @param value value to set the  UnitCode
     */
    public void setUnitCode(String value) {
        setAttributeInternal(UNITCODE, value);
    }

    /**
     * Gets the attribute value for the calculated attribute UnitName.
     * @return the UnitName
     */
    public String getUnitName() {
        if(getUnitCode()!=null){
            Row[] row=this.getUnitVO1().getFilteredRows("Code", getUnitCode());
            if(row.length>0){
                if(row[0].getAttribute("Name")!=null)
                    return row[0].getAttribute("Name").toString();
            }
        }
        return (String) getAttributeInternal(UNITNAME);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute UnitName.
     * @param value value to set the  UnitName
     */
    public void setUnitName(String value) {
        setAttributeInternal(UNITNAME, value);
    }

    /**
     * Gets the view accessor <code>RowSet</code> UnitVO1.
     */
    public RowSet getUnitVO1() {
        return (RowSet) getAttributeInternal(UNITVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> EmpViewVVO1.
     */
    public RowSet getEmpViewVVO1() {
        return (RowSet) getAttributeInternal(EMPVIEWVVO1);
    }


    /**
     * Gets the view accessor <code>RowSet</code> PFTransferAndWithdrawalEntryVO1.
     */
    public RowSet getPFTransferAndWithdrawalEntryVO1() {
        return (RowSet) getAttributeInternal(PFTRANSFERANDWITHDRAWALENTRYVO1);
    }

    /**
     * getAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param attrDef the attribute

     * @return the attribute value
     * @throws Exception
     */
    protected Object getAttrInvokeAccessor(int index, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            return AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].get(this);
        }
        return super.getAttrInvokeAccessor(index, attrDef);
    }

    /**
     * setAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param value the value to assign to the attribute
     * @param attrDef the attribute

     * @throws Exception
     */
    protected void setAttrInvokeAccessor(int index, Object value, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].put(this, value);
            return;
        }
        super.setAttrInvokeAccessor(index, value, attrDef);
    }
}

