package terms.hrm.transaction.model.view;

import java.math.BigDecimal;

import oracle.jbo.RowIterator;
import oracle.jbo.RowSet;
import oracle.jbo.domain.Date;
import oracle.jbo.domain.Number;
import oracle.jbo.domain.Timestamp;
import oracle.jbo.server.EntityImpl;
import oracle.jbo.server.ViewRowImpl;

import terms.hrm.setup.model.entity.EmployeeMasterEOImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Thu May 14 15:41:45 IST 2020
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class SalaryIncrementArrearHeaderVORowImpl extends ViewRowImpl {
    public static final int ENTITY_SALARYINCREMENTARREARHEADERE1 = 0;
    public static final int ENTITY_UNITEO = 1;
    public static final int ENTITY_EMPLOYEEMASTEREO = 2;
    public static final int ENTITY_DESIGNATIONMASTEREO = 3;
    public static final int ENTITY_CATEGORYMASTEREO = 4;
    public static final int ENTITY_LEVELMASTEREO = 5;
    public static final int ENTITY_EMPLOYEEMASTEREO1 = 6;
    public static final int ENTITY_DESIGNATIONMASTEREO1 = 7;
    public static final int ENTITY_CATEGORYMASTEREO1 = 8;
    public static final int ENTITY_LEVELMASTEREO1 = 9;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        CreatedBy,
        CreationDate,
        EffectiveDate,
        EmpNo,
        IncArrearId,
        IncDate,
        LastUpdateDate,
        LastUpdatedBy,
        Month,
        NewCatCd,
        NewDesigCd,
        NewLevelCd,
        ObjectVersionNumber,
        OldCatCd,
        OldDesigCd,
        OldLevelCd,
        UnitCd,
        Year,
        Name,
        Code,
        ObjectVersionNumber1,
        EmpFirstName,
        EmpNumber,
        ObjectVersionNumber2,
        Description,
        Code1,
        ObjectVersionNumber3,
        Description1,
        Code2,
        ObjectVersionNumber4,
        Description2,
        LvlMastLevel,
        DesigCode,
        EmpNumber1,
        ObjectVersionNumber5,
        Category,
        DesigLvlMastLvlMastLevel,
        Description3,
        Description4,
        Description5,
        Description6,
        Code3,
        ObjectVersionNumber6,
        Description7,
        Code4,
        ObjectVersionNumber7,
        Description8,
        LvlMastLevel1,
        EditTrans,
        EmpNo1,
        EmpLastName,
        NewTransDesignCd,
        SalaryIncrementArrearDetailVO,
        SalaryIncrementArrearHeaderVVO1,
        SalaryIncrementArrearHeader2VVO1,
        UnitVO1,
        SecControlVO1;
        static AttributesEnum[] vals = null;
        ;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }

    public static final int CREATEDBY = AttributesEnum.CreatedBy.index();
    public static final int CREATIONDATE = AttributesEnum.CreationDate.index();
    public static final int EFFECTIVEDATE = AttributesEnum.EffectiveDate.index();
    public static final int EMPNO = AttributesEnum.EmpNo.index();
    public static final int INCARREARID = AttributesEnum.IncArrearId.index();
    public static final int INCDATE = AttributesEnum.IncDate.index();
    public static final int LASTUPDATEDATE = AttributesEnum.LastUpdateDate.index();
    public static final int LASTUPDATEDBY = AttributesEnum.LastUpdatedBy.index();
    public static final int MONTH = AttributesEnum.Month.index();
    public static final int NEWCATCD = AttributesEnum.NewCatCd.index();
    public static final int NEWDESIGCD = AttributesEnum.NewDesigCd.index();
    public static final int NEWLEVELCD = AttributesEnum.NewLevelCd.index();
    public static final int OBJECTVERSIONNUMBER = AttributesEnum.ObjectVersionNumber.index();
    public static final int OLDCATCD = AttributesEnum.OldCatCd.index();
    public static final int OLDDESIGCD = AttributesEnum.OldDesigCd.index();
    public static final int OLDLEVELCD = AttributesEnum.OldLevelCd.index();
    public static final int UNITCD = AttributesEnum.UnitCd.index();
    public static final int YEAR = AttributesEnum.Year.index();
    public static final int NAME = AttributesEnum.Name.index();
    public static final int CODE = AttributesEnum.Code.index();
    public static final int OBJECTVERSIONNUMBER1 = AttributesEnum.ObjectVersionNumber1.index();
    public static final int EMPFIRSTNAME = AttributesEnum.EmpFirstName.index();
    public static final int EMPNUMBER = AttributesEnum.EmpNumber.index();
    public static final int OBJECTVERSIONNUMBER2 = AttributesEnum.ObjectVersionNumber2.index();
    public static final int DESCRIPTION = AttributesEnum.Description.index();
    public static final int CODE1 = AttributesEnum.Code1.index();
    public static final int OBJECTVERSIONNUMBER3 = AttributesEnum.ObjectVersionNumber3.index();
    public static final int DESCRIPTION1 = AttributesEnum.Description1.index();
    public static final int CODE2 = AttributesEnum.Code2.index();
    public static final int OBJECTVERSIONNUMBER4 = AttributesEnum.ObjectVersionNumber4.index();
    public static final int DESCRIPTION2 = AttributesEnum.Description2.index();
    public static final int LVLMASTLEVEL = AttributesEnum.LvlMastLevel.index();
    public static final int DESIGCODE = AttributesEnum.DesigCode.index();
    public static final int EMPNUMBER1 = AttributesEnum.EmpNumber1.index();
    public static final int OBJECTVERSIONNUMBER5 = AttributesEnum.ObjectVersionNumber5.index();
    public static final int CATEGORY = AttributesEnum.Category.index();
    public static final int DESIGLVLMASTLVLMASTLEVEL = AttributesEnum.DesigLvlMastLvlMastLevel.index();
    public static final int DESCRIPTION3 = AttributesEnum.Description3.index();
    public static final int DESCRIPTION4 = AttributesEnum.Description4.index();
    public static final int DESCRIPTION5 = AttributesEnum.Description5.index();
    public static final int DESCRIPTION6 = AttributesEnum.Description6.index();
    public static final int CODE3 = AttributesEnum.Code3.index();
    public static final int OBJECTVERSIONNUMBER6 = AttributesEnum.ObjectVersionNumber6.index();
    public static final int DESCRIPTION7 = AttributesEnum.Description7.index();
    public static final int CODE4 = AttributesEnum.Code4.index();
    public static final int OBJECTVERSIONNUMBER7 = AttributesEnum.ObjectVersionNumber7.index();
    public static final int DESCRIPTION8 = AttributesEnum.Description8.index();
    public static final int LVLMASTLEVEL1 = AttributesEnum.LvlMastLevel1.index();
    public static final int EDITTRANS = AttributesEnum.EditTrans.index();
    public static final int EMPNO1 = AttributesEnum.EmpNo1.index();
    public static final int EMPLASTNAME = AttributesEnum.EmpLastName.index();
    public static final int NEWTRANSDESIGNCD = AttributesEnum.NewTransDesignCd.index();
    public static final int SALARYINCREMENTARREARDETAILVO = AttributesEnum.SalaryIncrementArrearDetailVO.index();
    public static final int SALARYINCREMENTARREARHEADERVVO1 = AttributesEnum.SalaryIncrementArrearHeaderVVO1.index();
    public static final int SALARYINCREMENTARREARHEADER2VVO1 = AttributesEnum.SalaryIncrementArrearHeader2VVO1.index();
    public static final int UNITVO1 = AttributesEnum.UnitVO1.index();
    public static final int SECCONTROLVO1 = AttributesEnum.SecControlVO1.index();

    /**
     * This is the default constructor (do not remove).
     */
    public SalaryIncrementArrearHeaderVORowImpl() {
    }

    /**
     * Gets SalaryIncrementArrearHeaderE1 entity object.
     * @return the SalaryIncrementArrearHeaderE1
     */
    public EntityImpl getSalaryIncrementArrearHeaderE1() {
        return (EntityImpl) getEntity(ENTITY_SALARYINCREMENTARREARHEADERE1);
    }

    /**
     * Gets UnitEO entity object.
     * @return the UnitEO
     */
    public EntityImpl getUnitEO() {
        return (EntityImpl) getEntity(ENTITY_UNITEO);
    }

    /**
     * Gets EmployeeMasterEO entity object.
     * @return the EmployeeMasterEO
     */
    public EmployeeMasterEOImpl getEmployeeMasterEO() {
        return (EmployeeMasterEOImpl) getEntity(ENTITY_EMPLOYEEMASTEREO);
    }

    /**
     * Gets DesignationMasterEO entity object.
     * @return the DesignationMasterEO
     */
    public EntityImpl getDesignationMasterEO() {
        return (EntityImpl) getEntity(ENTITY_DESIGNATIONMASTEREO);
    }

    /**
     * Gets CategoryMasterEO entity object.
     * @return the CategoryMasterEO
     */
    public EntityImpl getCategoryMasterEO() {
        return (EntityImpl) getEntity(ENTITY_CATEGORYMASTEREO);
    }

    /**
     * Gets LevelMasterEO entity object.
     * @return the LevelMasterEO
     */
    public EntityImpl getLevelMasterEO() {
        return (EntityImpl) getEntity(ENTITY_LEVELMASTEREO);
    }

    /**
     * Gets EmployeeMasterEO1 entity object.
     * @return the EmployeeMasterEO1
     */
    public EmployeeMasterEOImpl getEmployeeMasterEO1() {
        return (EmployeeMasterEOImpl) getEntity(ENTITY_EMPLOYEEMASTEREO1);
    }

    /**
     * Gets DesignationMasterEO1 entity object.
     * @return the DesignationMasterEO1
     */
    public EntityImpl getDesignationMasterEO1() {
        return (EntityImpl) getEntity(ENTITY_DESIGNATIONMASTEREO1);
    }

    /**
     * Gets CategoryMasterEO1 entity object.
     * @return the CategoryMasterEO1
     */
    public EntityImpl getCategoryMasterEO1() {
        return (EntityImpl) getEntity(ENTITY_CATEGORYMASTEREO1);
    }

    /**
     * Gets LevelMasterEO1 entity object.
     * @return the LevelMasterEO1
     */
    public EntityImpl getLevelMasterEO1() {
        return (EntityImpl) getEntity(ENTITY_LEVELMASTEREO1);
    }

    /**
     * Gets the attribute value for CREATED_BY using the alias name CreatedBy.
     * @return the CREATED_BY
     */
    public String getCreatedBy() {
        return (String) getAttributeInternal(CREATEDBY);
    }

    /**
     * Gets the attribute value for CREATION_DATE using the alias name CreationDate.
     * @return the CREATION_DATE
     */
    public Timestamp getCreationDate() {
        return (Timestamp) getAttributeInternal(CREATIONDATE);
    }

    /**
     * Gets the attribute value for EFFECTIVE_DATE using the alias name EffectiveDate.
     * @return the EFFECTIVE_DATE
     */
    public Date getEffectiveDate() {
        if (getAttributeInternal(EFFECTIVEDATE) == null) {
                    setAttributeInternal(EFFECTIVEDATE,
                                         getJboFirstDateOfMonth(new oracle.jbo.domain.Date(oracle.jbo.domain.Date.getCurrentDate())));
                    //getJboFirstDateOfMonth(new oracle.jbo.domain.Date(oracle.jbo.domain.Date.getCurrentDate()));
                }
                return (Date) getAttributeInternal(EFFECTIVEDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for EFFECTIVE_DATE using the alias name EffectiveDate.
     * @param value value to set the EFFECTIVE_DATE
     */
    public oracle.jbo.domain.Date getJboFirstDateOfMonth(oracle.jbo.domain.Date jbo) {
           System.out.println("jbo date parameter:" + jbo);
           java.util.GregorianCalendar cal = new java.util.GregorianCalendar();
           cal.setTime(jbo.dateValue());
           cal.set(java.util.Calendar.DAY_OF_MONTH, cal.getActualMinimum(java.util.Calendar.DAY_OF_MONTH));
           System.out.println("in  function firstdate:" +
                              new oracle.jbo.domain.Date(new java.sql.Date(cal.getTimeInMillis())));
           return new oracle.jbo.domain.Date(new java.sql.Date(cal.getTimeInMillis()));
       }
    public void setEffectiveDate(Date value) {
        setAttributeInternal(EFFECTIVEDATE, value);
    }

    /**
     * Gets the attribute value for EMP_NO using the alias name EmpNo.
     * @return the EMP_NO
     */
    public String getEmpNo() {
        return (String) getAttributeInternal(EMPNO);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_NO using the alias name EmpNo.
     * @param value value to set the EMP_NO
     */
    public void setEmpNo(String value) {
        setAttributeInternal(EMPNO, value);
    }

    /**
     * Gets the attribute value for INC_ARREAR_ID using the alias name IncArrearId.
     * @return the INC_ARREAR_ID
     */
    public Number getIncArrearId() {
        return (Number) getAttributeInternal(INCARREARID);
    }

    /**
     * Sets <code>value</code> as attribute value for INC_ARREAR_ID using the alias name IncArrearId.
     * @param value value to set the INC_ARREAR_ID
     */
    public void setIncArrearId(Number value) {
        setAttributeInternal(INCARREARID, value);
    }

    /**
     * Gets the attribute value for INC_DATE using the alias name IncDate.
     * @return the INC_DATE
     */
    public Date getIncDate() {
        return (Date) getAttributeInternal(INCDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for INC_DATE using the alias name IncDate.
     * @param value value to set the INC_DATE
     */
    public void setIncDate(Date value) {
        setAttributeInternal(INCDATE, value);
    }

    /**
     * Gets the attribute value for LAST_UPDATE_DATE using the alias name LastUpdateDate.
     * @return the LAST_UPDATE_DATE
     */
    public Timestamp getLastUpdateDate() {
        return (Timestamp) getAttributeInternal(LASTUPDATEDATE);
    }

    /**
     * Gets the attribute value for LAST_UPDATED_BY using the alias name LastUpdatedBy.
     * @return the LAST_UPDATED_BY
     */
    public String getLastUpdatedBy() {
        return (String) getAttributeInternal(LASTUPDATEDBY);
    }

    /**
     * Gets the attribute value for MONTH using the alias name Month.
     * @return the MONTH
     */
    public String getMonth() {
        return (String) getAttributeInternal(MONTH);
    }

    /**
     * Sets <code>value</code> as attribute value for MONTH using the alias name Month.
     * @param value value to set the MONTH
     */
    public void setMonth(String value) {
        setAttributeInternal(MONTH, value);
    }

    /**
     * Gets the attribute value for NEW_CAT_CD using the alias name NewCatCd.
     * @return the NEW_CAT_CD
     */
    public String getNewCatCd() {
        return (String) getAttributeInternal(NEWCATCD);
    }

    /**
     * Sets <code>value</code> as attribute value for NEW_CAT_CD using the alias name NewCatCd.
     * @param value value to set the NEW_CAT_CD
     */
    public void setNewCatCd(String value) {
        setAttributeInternal(NEWCATCD, value);
    }

    /**
     * Gets the attribute value for NEW_DESIG_CD using the alias name NewDesigCd.
     * @return the NEW_DESIG_CD
     */
    public String getNewDesigCd() {
        return (String) getAttributeInternal(NEWDESIGCD);
    }

    /**
     * Sets <code>value</code> as attribute value for NEW_DESIG_CD using the alias name NewDesigCd.
     * @param value value to set the NEW_DESIG_CD
     */
    public void setNewDesigCd(String value) {
        setAttributeInternal(NEWDESIGCD, value);
    }

    /**
     * Gets the attribute value for NEW_LEVEL_CD using the alias name NewLevelCd.
     * @return the NEW_LEVEL_CD
     */
    public String getNewLevelCd() {
        return (String) getAttributeInternal(NEWLEVELCD);
    }

    /**
     * Sets <code>value</code> as attribute value for NEW_LEVEL_CD using the alias name NewLevelCd.
     * @param value value to set the NEW_LEVEL_CD
     */
    public void setNewLevelCd(String value) {
        setAttributeInternal(NEWLEVELCD, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Number getObjectVersionNumber() {
        return (Number) getAttributeInternal(OBJECTVERSIONNUMBER);
    }

    /**
     * Gets the attribute value for OLD_CAT_CD using the alias name OldCatCd.
     * @return the OLD_CAT_CD
     */
    public String getOldCatCd() {
        return (String) getAttributeInternal(OLDCATCD);
    }

    /**
     * Sets <code>value</code> as attribute value for OLD_CAT_CD using the alias name OldCatCd.
     * @param value value to set the OLD_CAT_CD
     */
    public void setOldCatCd(String value) {
        setAttributeInternal(OLDCATCD, value);
    }

    /**
     * Gets the attribute value for OLD_DESIG_CD using the alias name OldDesigCd.
     * @return the OLD_DESIG_CD
     */
    public String getOldDesigCd() {
        return (String) getAttributeInternal(OLDDESIGCD);
    }

    /**
     * Sets <code>value</code> as attribute value for OLD_DESIG_CD using the alias name OldDesigCd.
     * @param value value to set the OLD_DESIG_CD
     */
    public void setOldDesigCd(String value) {
        setAttributeInternal(OLDDESIGCD, value);
    }

    /**
     * Gets the attribute value for OLD_LEVEL_CD using the alias name OldLevelCd.
     * @return the OLD_LEVEL_CD
     */
    public String getOldLevelCd() {
        return (String) getAttributeInternal(OLDLEVELCD);
    }

    /**
     * Sets <code>value</code> as attribute value for OLD_LEVEL_CD using the alias name OldLevelCd.
     * @param value value to set the OLD_LEVEL_CD
     */
    public void setOldLevelCd(String value) {
        setAttributeInternal(OLDLEVELCD, value);
    }

    /**
     * Gets the attribute value for UNIT_CD using the alias name UnitCd.
     * @return the UNIT_CD
     */
    public String getUnitCd() {
        return (String) getAttributeInternal(UNITCD);
    }

    /**
     * Sets <code>value</code> as attribute value for UNIT_CD using the alias name UnitCd.
     * @param value value to set the UNIT_CD
     */
    public void setUnitCd(String value) {
        setAttributeInternal(UNITCD, value);
    }

    /**
     * Gets the attribute value for YEAR using the alias name Year.
     * @return the YEAR
     */
    public BigDecimal getYear() {
        return (BigDecimal) getAttributeInternal(YEAR);
    }

    /**
     * Sets <code>value</code> as attribute value for YEAR using the alias name Year.
     * @param value value to set the YEAR
     */
    public void setYear(BigDecimal value) {
        setAttributeInternal(YEAR, value);
    }

    /**
     * Gets the attribute value for NAME using the alias name Name.
     * @return the NAME
     */
    public String getName() {
        return (String) getAttributeInternal(NAME);
    }

    /**
     * Sets <code>value</code> as attribute value for NAME using the alias name Name.
     * @param value value to set the NAME
     */
    public void setName(String value) {
        setAttributeInternal(NAME, value);
    }

    /**
     * Gets the attribute value for CODE using the alias name Code.
     * @return the CODE
     */
    public String getCode() {
        return (String) getAttributeInternal(CODE);
    }

    /**
     * Sets <code>value</code> as attribute value for CODE using the alias name Code.
     * @param value value to set the CODE
     */
    public void setCode(String value) {
        setAttributeInternal(CODE, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber1.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber1() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER1);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber1.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber1(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER1, value);
    }

    /**
     * Gets the attribute value for EMP_FIRST_NAME using the alias name EmpFirstName.
     * @return the EMP_FIRST_NAME
     */
    public String getEmpFirstName() {
        if((getAttributeInternal(EMPFIRSTNAME)!=null) &&
                       (getAttributeInternal(EMPLASTNAME)!=null)){
                                    return (String) getAttributeInternal(EMPFIRSTNAME) + " " +
                       (String) getAttributeInternal(EMPLASTNAME);
                                }else{
                                    return (String) getAttributeInternal(EMPFIRSTNAME);
                                } 
       // return (String) getAttributeInternal(EMPFIRSTNAME);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_FIRST_NAME using the alias name EmpFirstName.
     * @param value value to set the EMP_FIRST_NAME
     */
    public void setEmpFirstName(String value) {
        setAttributeInternal(EMPFIRSTNAME, value);
    }

    /**
     * Gets the attribute value for EMP_NUMBER using the alias name EmpNumber.
     * @return the EMP_NUMBER
     */
    public String getEmpNumber() {
        return (String) getAttributeInternal(EMPNUMBER);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_NUMBER using the alias name EmpNumber.
     * @param value value to set the EMP_NUMBER
     */
    public void setEmpNumber(String value) {
        setAttributeInternal(EMPNUMBER, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber2.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber2() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER2);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber2.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber2(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER2, value);
    }

    /**
     * Gets the attribute value for DESCRIPTION using the alias name Description.
     * @return the DESCRIPTION
     */
    public String getDescription() {
        return (String) getAttributeInternal(DESCRIPTION);
    }

    /**
     * Sets <code>value</code> as attribute value for DESCRIPTION using the alias name Description.
     * @param value value to set the DESCRIPTION
     */
    public void setDescription(String value) {
        setAttributeInternal(DESCRIPTION, value);
    }

    /**
     * Gets the attribute value for CODE using the alias name Code1.
     * @return the CODE
     */
    public String getCode1() {
        return (String) getAttributeInternal(CODE1);
    }

    /**
     * Sets <code>value</code> as attribute value for CODE using the alias name Code1.
     * @param value value to set the CODE
     */
    public void setCode1(String value) {
        setAttributeInternal(CODE1, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber3.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber3() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER3);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber3.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber3(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER3, value);
    }

    /**
     * Gets the attribute value for DESCRIPTION using the alias name Description1.
     * @return the DESCRIPTION
     */
    public String getDescription1() {
        return (String) getAttributeInternal(DESCRIPTION1);
    }

    /**
     * Sets <code>value</code> as attribute value for DESCRIPTION using the alias name Description1.
     * @param value value to set the DESCRIPTION
     */
    public void setDescription1(String value) {
        setAttributeInternal(DESCRIPTION1, value);
    }

    /**
     * Gets the attribute value for CODE using the alias name Code2.
     * @return the CODE
     */
    public String getCode2() {
        return (String) getAttributeInternal(CODE2);
    }

    /**
     * Sets <code>value</code> as attribute value for CODE using the alias name Code2.
     * @param value value to set the CODE
     */
    public void setCode2(String value) {
        setAttributeInternal(CODE2, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber4.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber4() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER4);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber4.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber4(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER4, value);
    }

    /**
     * Gets the attribute value for DESCRIPTION using the alias name Description2.
     * @return the DESCRIPTION
     */
    public String getDescription2() {
        return (String) getAttributeInternal(DESCRIPTION2);
    }

    /**
     * Sets <code>value</code> as attribute value for DESCRIPTION using the alias name Description2.
     * @param value value to set the DESCRIPTION
     */
    public void setDescription2(String value) {
        setAttributeInternal(DESCRIPTION2, value);
    }

    /**
     * Gets the attribute value for LVL_MAST_LEVEL using the alias name LvlMastLevel.
     * @return the LVL_MAST_LEVEL
     */
    public String getLvlMastLevel() {
        return (String) getAttributeInternal(LVLMASTLEVEL);
    }

    /**
     * Sets <code>value</code> as attribute value for LVL_MAST_LEVEL using the alias name LvlMastLevel.
     * @param value value to set the LVL_MAST_LEVEL
     */
    public void setLvlMastLevel(String value) {
        setAttributeInternal(LVLMASTLEVEL, value);
    }

    /**
     * Gets the attribute value for DESIG_CODE using the alias name DesigCode.
     * @return the DESIG_CODE
     */
    public String getDesigCode() {
        return (String) getAttributeInternal(DESIGCODE);
    }

    /**
     * Sets <code>value</code> as attribute value for DESIG_CODE using the alias name DesigCode.
     * @param value value to set the DESIG_CODE
     */
    public void setDesigCode(String value) {
        setAttributeInternal(DESIGCODE, value);
    }

    /**
     * Gets the attribute value for EMP_NUMBER using the alias name EmpNumber1.
     * @return the EMP_NUMBER
     */
    public String getEmpNumber1() {
        return (String) getAttributeInternal(EMPNUMBER1);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_NUMBER using the alias name EmpNumber1.
     * @param value value to set the EMP_NUMBER
     */
    public void setEmpNumber1(String value) {
        setAttributeInternal(EMPNUMBER1, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber5.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber5() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER5);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber5.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber5(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER5, value);
    }

    /**
     * Gets the attribute value for CATEGORY using the alias name Category.
     * @return the CATEGORY
     */
    public String getCategory() {
        return (String) getAttributeInternal(CATEGORY);
    }

    /**
     * Sets <code>value</code> as attribute value for CATEGORY using the alias name Category.
     * @param value value to set the CATEGORY
     */
    public void setCategory(String value) {
        setAttributeInternal(CATEGORY, value);
    }

    /**
     * Gets the attribute value for DESIG_LVL_MAST_LVL_MAST_LEVEL using the alias name DesigLvlMastLvlMastLevel.
     * @return the DESIG_LVL_MAST_LVL_MAST_LEVEL
     */
    public String getDesigLvlMastLvlMastLevel() {
        return (String) getAttributeInternal(DESIGLVLMASTLVLMASTLEVEL);
    }

    /**
     * Sets <code>value</code> as attribute value for DESIG_LVL_MAST_LVL_MAST_LEVEL using the alias name DesigLvlMastLvlMastLevel.
     * @param value value to set the DESIG_LVL_MAST_LVL_MAST_LEVEL
     */
    public void setDesigLvlMastLvlMastLevel(String value) {
        setAttributeInternal(DESIGLVLMASTLVLMASTLEVEL, value);
    }

    /**
     * Gets the attribute value for DESCRIPTION using the alias name Description3.
     * @return the DESCRIPTION
     */
    public String getDescription3() {
        return (String) getAttributeInternal(DESCRIPTION3);
    }

    /**
     * Sets <code>value</code> as attribute value for DESCRIPTION using the alias name Description3.
     * @param value value to set the DESCRIPTION
     */
    public void setDescription3(String value) {
        setAttributeInternal(DESCRIPTION3, value);
    }

    /**
     * Gets the attribute value for DESCRIPTION using the alias name Description4.
     * @return the DESCRIPTION
     */
    public String getDescription4() {
        return (String) getAttributeInternal(DESCRIPTION4);
    }

    /**
     * Sets <code>value</code> as attribute value for DESCRIPTION using the alias name Description4.
     * @param value value to set the DESCRIPTION
     */
    public void setDescription4(String value) {
        setAttributeInternal(DESCRIPTION4, value);
    }

    /**
     * Gets the attribute value for DESCRIPTION using the alias name Description5.
     * @return the DESCRIPTION
     */
    public String getDescription5() {
        return (String) getAttributeInternal(DESCRIPTION5);
    }

    /**
     * Sets <code>value</code> as attribute value for DESCRIPTION using the alias name Description5.
     * @param value value to set the DESCRIPTION
     */
    public void setDescription5(String value) {
        setAttributeInternal(DESCRIPTION5, value);
    }

    /**
     * Gets the attribute value for DESCRIPTION using the alias name Description6.
     * @return the DESCRIPTION
     */
    public String getDescription6() {
        return (String) getAttributeInternal(DESCRIPTION6);
    }

    /**
     * Sets <code>value</code> as attribute value for DESCRIPTION using the alias name Description6.
     * @param value value to set the DESCRIPTION
     */
    public void setDescription6(String value) {
        setAttributeInternal(DESCRIPTION6, value);
    }

    /**
     * Gets the attribute value for CODE using the alias name Code3.
     * @return the CODE
     */
    public String getCode3() {
        return (String) getAttributeInternal(CODE3);
    }

    /**
     * Sets <code>value</code> as attribute value for CODE using the alias name Code3.
     * @param value value to set the CODE
     */
    public void setCode3(String value) {
        setAttributeInternal(CODE3, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber6.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber6() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER6);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber6.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber6(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER6, value);
    }

    /**
     * Gets the attribute value for DESCRIPTION using the alias name Description7.
     * @return the DESCRIPTION
     */
    public String getDescription7() {
        return (String) getAttributeInternal(DESCRIPTION7);
    }

    /**
     * Sets <code>value</code> as attribute value for DESCRIPTION using the alias name Description7.
     * @param value value to set the DESCRIPTION
     */
    public void setDescription7(String value) {
        setAttributeInternal(DESCRIPTION7, value);
    }

    /**
     * Gets the attribute value for CODE using the alias name Code4.
     * @return the CODE
     */
    public String getCode4() {
        return (String) getAttributeInternal(CODE4);
    }

    /**
     * Sets <code>value</code> as attribute value for CODE using the alias name Code4.
     * @param value value to set the CODE
     */
    public void setCode4(String value) {
        setAttributeInternal(CODE4, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber7.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber7() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER7);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber7.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber7(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER7, value);
    }

    /**
     * Gets the attribute value for DESCRIPTION using the alias name Description8.
     * @return the DESCRIPTION
     */
    public String getDescription8() {
        return (String) getAttributeInternal(DESCRIPTION8);
    }

    /**
     * Sets <code>value</code> as attribute value for DESCRIPTION using the alias name Description8.
     * @param value value to set the DESCRIPTION
     */
    public void setDescription8(String value) {
        setAttributeInternal(DESCRIPTION8, value);
    }

    /**
     * Gets the attribute value for LVL_MAST_LEVEL using the alias name LvlMastLevel1.
     * @return the LVL_MAST_LEVEL
     */
    public String getLvlMastLevel1() {
        return (String) getAttributeInternal(LVLMASTLEVEL1);
    }

    /**
     * Sets <code>value</code> as attribute value for LVL_MAST_LEVEL using the alias name LvlMastLevel1.
     * @param value value to set the LVL_MAST_LEVEL
     */
    public void setLvlMastLevel1(String value) {
        setAttributeInternal(LVLMASTLEVEL1, value);
    }

    /**
     * Gets the attribute value for the calculated attribute EditTrans.
     * @return the EditTrans
     */
    public Integer getEditTrans() {
        byte entityState = this.getEntity(0).getEntityState();
        return new Integer(entityState);
        //return (Integer) getAttributeInternal(EDITTRANS);
    }

    /**
     * Gets the attribute value for EMP_NO using the alias name EmpNo1.
     * @return the EMP_NO
     */
    public String getEmpNo1() {
        return (String) getAttributeInternal(EMPNO1);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_NO using the alias name EmpNo1.
     * @param value value to set the EMP_NO
     */
    public void setEmpNo1(String value) {
        setAttributeInternal(EMPNO1, value);
    }

    /**
     * Gets the attribute value for EMP_LAST_NAME using the alias name EmpLastName.
     * @return the EMP_LAST_NAME
     */
    public String getEmpLastName() {
        return (String) getAttributeInternal(EMPLASTNAME);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_LAST_NAME using the alias name EmpLastName.
     * @param value value to set the EMP_LAST_NAME
     */
    public void setEmpLastName(String value) {
        setAttributeInternal(EMPLASTNAME, value);
    }

    /**
     * Gets the attribute value for the calculated attribute NewTransDesignCd.
     * @return the NewTransDesignCd
     */
    public String getNewTransDesignCd() {
        if(getAttributeInternal(NEWTRANSDESIGNCD)==null){
            return (String) getAttributeInternal(NEWDESIGCD);
        }
        return (String) getAttributeInternal(NEWTRANSDESIGNCD);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute NewTransDesignCd.
     * @param value value to set the  NewTransDesignCd
     */
    public void setNewTransDesignCd(String value) {
        setAttributeInternal(NEWTRANSDESIGNCD, value);
    }

    /**
     * Gets the associated <code>RowIterator</code> using master-detail link SalaryIncrementArrearDetailVO.
     */
    public RowIterator getSalaryIncrementArrearDetailVO() {
        return (RowIterator) getAttributeInternal(SALARYINCREMENTARREARDETAILVO);
    }

    /**
     * Gets the view accessor <code>RowSet</code> SalaryIncrementArrearHeaderVVO1.
     */
    public RowSet getSalaryIncrementArrearHeaderVVO1() {
        return (RowSet) getAttributeInternal(SALARYINCREMENTARREARHEADERVVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> SalaryIncrementArrearHeader2VVO1.
     */
    public RowSet getSalaryIncrementArrearHeader2VVO1() {
        return (RowSet) getAttributeInternal(SALARYINCREMENTARREARHEADER2VVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> UnitVO1.
     */
    public RowSet getUnitVO1() {
        return (RowSet) getAttributeInternal(UNITVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> SecControlVO1.
     */
    public RowSet getSecControlVO1() {
        return (RowSet) getAttributeInternal(SECCONTROLVO1);
    }
}

