package terms.hrm.transaction.model.view;

import oracle.jbo.Row;
import oracle.jbo.RowIterator;
import oracle.jbo.RowSet;
import oracle.jbo.domain.Date;
import oracle.jbo.domain.Number;
import oracle.jbo.domain.Timestamp;
import oracle.jbo.server.AttributeDefImpl;
import oracle.jbo.server.EntityImpl;
import oracle.jbo.server.ViewRowImpl;

import terms.hrm.transaction.model.applicationModule.HrmTransactionAMImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Tue Jun 25 15:58:38 IST 2019
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class UniformDistributionCheckListVORowImpl extends ViewRowImpl {


    public static final int ENTITY_UNIFORMDISTRIBUTIONCHECKLIST1 = 0;
    public static final int ENTITY_UNITEO = 1;
    public static final int ENTITY_DEPARTMENTMASTEREO = 2;
    public static final int ENTITY_DESIGNATIONMASTEREO = 3;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        EntryNo {
            public Object get(UniformDistributionCheckListVORowImpl obj) {
                return obj.getEntryNo();
            }

            public void put(UniformDistributionCheckListVORowImpl obj, Object value) {
                obj.setEntryNo((String) value);
            }
        }
        ,
        EntryDate {
            public Object get(UniformDistributionCheckListVORowImpl obj) {
                return obj.getEntryDate();
            }

            public void put(UniformDistributionCheckListVORowImpl obj, Object value) {
                obj.setEntryDate((Date) value);
            }
        }
        ,
        EmpNo {
            public Object get(UniformDistributionCheckListVORowImpl obj) {
                return obj.getEmpNo();
            }

            public void put(UniformDistributionCheckListVORowImpl obj, Object value) {
                obj.setEmpNo((String) value);
            }
        }
        ,
        DesigCd {
            public Object get(UniformDistributionCheckListVORowImpl obj) {
                return obj.getDesigCd();
            }

            public void put(UniformDistributionCheckListVORowImpl obj, Object value) {
                obj.setDesigCd((String) value);
            }
        }
        ,
        DeptCd {
            public Object get(UniformDistributionCheckListVORowImpl obj) {
                return obj.getDeptCd();
            }

            public void put(UniformDistributionCheckListVORowImpl obj, Object value) {
                obj.setDeptCd((String) value);
            }
        }
        ,
        ConfirmDate {
            public Object get(UniformDistributionCheckListVORowImpl obj) {
                return obj.getConfirmDate();
            }

            public void put(UniformDistributionCheckListVORowImpl obj, Object value) {
                obj.setConfirmDate((Date) value);
            }
        }
        ,
        Sets {
            public Object get(UniformDistributionCheckListVORowImpl obj) {
                return obj.getSets();
            }

            public void put(UniformDistributionCheckListVORowImpl obj, Object value) {
                obj.setSets((Number) value);
            }
        }
        ,
        PantMr {
            public Object get(UniformDistributionCheckListVORowImpl obj) {
                return obj.getPantMr();
            }

            public void put(UniformDistributionCheckListVORowImpl obj, Object value) {
                obj.setPantMr((Number) value);
            }
        }
        ,
        ShirtMr {
            public Object get(UniformDistributionCheckListVORowImpl obj) {
                return obj.getShirtMr();
            }

            public void put(UniformDistributionCheckListVORowImpl obj, Object value) {
                obj.setShirtMr((Number) value);
            }
        }
        ,
        ShoesNo {
            public Object get(UniformDistributionCheckListVORowImpl obj) {
                return obj.getShoesNo();
            }

            public void put(UniformDistributionCheckListVORowImpl obj, Object value) {
                obj.setShoesNo((String) value);
            }
        }
        ,
        FinYear {
            public Object get(UniformDistributionCheckListVORowImpl obj) {
                return obj.getFinYear();
            }

            public void put(UniformDistributionCheckListVORowImpl obj, Object value) {
                obj.setFinYear((String) value);
            }
        }
        ,
        UnitCd {
            public Object get(UniformDistributionCheckListVORowImpl obj) {
                return obj.getUnitCd();
            }

            public void put(UniformDistributionCheckListVORowImpl obj, Object value) {
                obj.setUnitCd((String) value);
            }
        }
        ,
        CreatedBy {
            public Object get(UniformDistributionCheckListVORowImpl obj) {
                return obj.getCreatedBy();
            }

            public void put(UniformDistributionCheckListVORowImpl obj, Object value) {
                obj.setCreatedBy((String) value);
            }
        }
        ,
        CreationDate {
            public Object get(UniformDistributionCheckListVORowImpl obj) {
                return obj.getCreationDate();
            }

            public void put(UniformDistributionCheckListVORowImpl obj, Object value) {
                obj.setCreationDate((Timestamp) value);
            }
        }
        ,
        LastUpdatedBy {
            public Object get(UniformDistributionCheckListVORowImpl obj) {
                return obj.getLastUpdatedBy();
            }

            public void put(UniformDistributionCheckListVORowImpl obj, Object value) {
                obj.setLastUpdatedBy((String) value);
            }
        }
        ,
        LastUpdateDate {
            public Object get(UniformDistributionCheckListVORowImpl obj) {
                return obj.getLastUpdateDate();
            }

            public void put(UniformDistributionCheckListVORowImpl obj, Object value) {
                obj.setLastUpdateDate((Timestamp) value);
            }
        }
        ,
        ObjectVersionNumber {
            public Object get(UniformDistributionCheckListVORowImpl obj) {
                return obj.getObjectVersionNumber();
            }

            public void put(UniformDistributionCheckListVORowImpl obj, Object value) {
                obj.setObjectVersionNumber((Number) value);
            }
        }
        ,
        EmpUnifmDistId {
            public Object get(UniformDistributionCheckListVORowImpl obj) {
                return obj.getEmpUnifmDistId();
            }

            public void put(UniformDistributionCheckListVORowImpl obj, Object value) {
                obj.setEmpUnifmDistId((Number) value);
            }
        }
        ,
        Name {
            public Object get(UniformDistributionCheckListVORowImpl obj) {
                return obj.getName();
            }

            public void put(UniformDistributionCheckListVORowImpl obj, Object value) {
                obj.setName((String) value);
            }
        }
        ,
        Code {
            public Object get(UniformDistributionCheckListVORowImpl obj) {
                return obj.getCode();
            }

            public void put(UniformDistributionCheckListVORowImpl obj, Object value) {
                obj.setCode((String) value);
            }
        }
        ,
        ObjectVersionNumber1 {
            public Object get(UniformDistributionCheckListVORowImpl obj) {
                return obj.getObjectVersionNumber1();
            }

            public void put(UniformDistributionCheckListVORowImpl obj, Object value) {
                obj.setObjectVersionNumber1((Integer) value);
            }
        }
        ,
        EditTrans {
            public Object get(UniformDistributionCheckListVORowImpl obj) {
                return obj.getEditTrans();
            }

            public void put(UniformDistributionCheckListVORowImpl obj, Object value) {
                obj.setEditTrans((Integer) value);
            }
        }
        ,
        Name1 {
            public Object get(UniformDistributionCheckListVORowImpl obj) {
                return obj.getName1();
            }

            public void put(UniformDistributionCheckListVORowImpl obj, Object value) {
                obj.setName1((String) value);
            }
        }
        ,
        Code1 {
            public Object get(UniformDistributionCheckListVORowImpl obj) {
                return obj.getCode1();
            }

            public void put(UniformDistributionCheckListVORowImpl obj, Object value) {
                obj.setCode1((String) value);
            }
        }
        ,
        UnitCode {
            public Object get(UniformDistributionCheckListVORowImpl obj) {
                return obj.getUnitCode();
            }

            public void put(UniformDistributionCheckListVORowImpl obj, Object value) {
                obj.setUnitCode((String) value);
            }
        }
        ,
        EntryNo1 {
            public Object get(UniformDistributionCheckListVORowImpl obj) {
                return obj.getEntryNo1();
            }

            public void put(UniformDistributionCheckListVORowImpl obj, Object value) {
                obj.setEntryNo1((String) value);
            }
        }
        ,
        Description {
            public Object get(UniformDistributionCheckListVORowImpl obj) {
                return obj.getDescription();
            }

            public void put(UniformDistributionCheckListVORowImpl obj, Object value) {
                obj.setDescription((String) value);
            }
        }
        ,
        Code2 {
            public Object get(UniformDistributionCheckListVORowImpl obj) {
                return obj.getCode2();
            }

            public void put(UniformDistributionCheckListVORowImpl obj, Object value) {
                obj.setCode2((String) value);
            }
        }
        ,
        ObjectVersionNumber2 {
            public Object get(UniformDistributionCheckListVORowImpl obj) {
                return obj.getObjectVersionNumber2();
            }

            public void put(UniformDistributionCheckListVORowImpl obj, Object value) {
                obj.setObjectVersionNumber2((Integer) value);
            }
        }
        ,
        EmpName {
            public Object get(UniformDistributionCheckListVORowImpl obj) {
                return obj.getEmpName();
            }

            public void put(UniformDistributionCheckListVORowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        FinYearVO1 {
            public Object get(UniformDistributionCheckListVORowImpl obj) {
                return obj.getFinYearVO1();
            }

            public void put(UniformDistributionCheckListVORowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        UnitVO1 {
            public Object get(UniformDistributionCheckListVORowImpl obj) {
                return obj.getUnitVO1();
            }

            public void put(UniformDistributionCheckListVORowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        EmployeeMasterVO1 {
            public Object get(UniformDistributionCheckListVORowImpl obj) {
                return obj.getEmployeeMasterVO1();
            }

            public void put(UniformDistributionCheckListVORowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ;
        static AttributesEnum[] vals = null;
        ;
        private static final int firstIndex = 0;

        public abstract Object get(UniformDistributionCheckListVORowImpl object);

        public abstract void put(UniformDistributionCheckListVORowImpl object, Object value);

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int ENTRYNO = AttributesEnum.EntryNo.index();
    public static final int ENTRYDATE = AttributesEnum.EntryDate.index();
    public static final int EMPNO = AttributesEnum.EmpNo.index();
    public static final int DESIGCD = AttributesEnum.DesigCd.index();
    public static final int DEPTCD = AttributesEnum.DeptCd.index();
    public static final int CONFIRMDATE = AttributesEnum.ConfirmDate.index();
    public static final int SETS = AttributesEnum.Sets.index();
    public static final int PANTMR = AttributesEnum.PantMr.index();
    public static final int SHIRTMR = AttributesEnum.ShirtMr.index();
    public static final int SHOESNO = AttributesEnum.ShoesNo.index();
    public static final int FINYEAR = AttributesEnum.FinYear.index();
    public static final int UNITCD = AttributesEnum.UnitCd.index();
    public static final int CREATEDBY = AttributesEnum.CreatedBy.index();
    public static final int CREATIONDATE = AttributesEnum.CreationDate.index();
    public static final int LASTUPDATEDBY = AttributesEnum.LastUpdatedBy.index();
    public static final int LASTUPDATEDATE = AttributesEnum.LastUpdateDate.index();
    public static final int OBJECTVERSIONNUMBER = AttributesEnum.ObjectVersionNumber.index();
    public static final int EMPUNIFMDISTID = AttributesEnum.EmpUnifmDistId.index();
    public static final int NAME = AttributesEnum.Name.index();
    public static final int CODE = AttributesEnum.Code.index();
    public static final int OBJECTVERSIONNUMBER1 = AttributesEnum.ObjectVersionNumber1.index();
    public static final int EDITTRANS = AttributesEnum.EditTrans.index();
    public static final int NAME1 = AttributesEnum.Name1.index();
    public static final int CODE1 = AttributesEnum.Code1.index();
    public static final int UNITCODE = AttributesEnum.UnitCode.index();
    public static final int ENTRYNO1 = AttributesEnum.EntryNo1.index();
    public static final int DESCRIPTION = AttributesEnum.Description.index();
    public static final int CODE2 = AttributesEnum.Code2.index();
    public static final int OBJECTVERSIONNUMBER2 = AttributesEnum.ObjectVersionNumber2.index();
    public static final int EMPNAME = AttributesEnum.EmpName.index();
    public static final int FINYEARVO1 = AttributesEnum.FinYearVO1.index();
    public static final int UNITVO1 = AttributesEnum.UnitVO1.index();
    public static final int EMPLOYEEMASTERVO1 = AttributesEnum.EmployeeMasterVO1.index();

    /**
     * This is the default constructor (do not remove).
     */
    public UniformDistributionCheckListVORowImpl() {
    }

    /**
     * Gets UniformDistributionCheckList1 entity object.
     * @return the UniformDistributionCheckList1
     */
    public EntityImpl getUniformDistributionCheckList1() {
        return (EntityImpl) getEntity(ENTITY_UNIFORMDISTRIBUTIONCHECKLIST1);
    }

    /**
     * Gets UnitEO entity object.
     * @return the UnitEO
     */
    public EntityImpl getUnitEO() {
        return (EntityImpl) getEntity(ENTITY_UNITEO);
    }

    /**
     * Gets DepartmentMasterEO entity object.
     * @return the DepartmentMasterEO
     */
    public EntityImpl getDepartmentMasterEO() {
        return (EntityImpl) getEntity(ENTITY_DEPARTMENTMASTEREO);
    }

    /**
     * Gets DesignationMasterEO entity object.
     * @return the DesignationMasterEO
     */
    public EntityImpl getDesignationMasterEO() {
        return (EntityImpl) getEntity(ENTITY_DESIGNATIONMASTEREO);
    }

    /**
     * Gets the attribute value for ENTRY_NO using the alias name EntryNo.
     * @return the ENTRY_NO
     */
    public String getEntryNo() {
     
        return (String) getAttributeInternal(ENTRYNO);
    }

    /**
     * Sets <code>value</code> as attribute value for ENTRY_NO using the alias name EntryNo.
     * @param value value to set the ENTRY_NO
     */
    public void setEntryNo(String value) {
        setAttributeInternal(ENTRYNO, value);
    }

    /**
     * Gets the attribute value for ENTRY_DATE using the alias name EntryDate.
     * @return the ENTRY_DATE
     */
    public Date getEntryDate() {
        return (Date) getAttributeInternal(ENTRYDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for ENTRY_DATE using the alias name EntryDate.
     * @param value value to set the ENTRY_DATE
     */
    public void setEntryDate(Date value) {
        setAttributeInternal(ENTRYDATE, value);
    }

    /**
     * Gets the attribute value for EMP_NO using the alias name EmpNo.
     * @return the EMP_NO
     */
    public String getEmpNo() {
        return (String) getAttributeInternal(EMPNO);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_NO using the alias name EmpNo.
     * @param value value to set the EMP_NO
     */
    public void setEmpNo(String value) {
        setAttributeInternal(EMPNO, value);
    }

    /**
     * Gets the attribute value for DESIG_CD using the alias name DesigCd.
     * @return the DESIG_CD
     */
    public String getDesigCd() {
        return (String) getAttributeInternal(DESIGCD);
    }

    /**
     * Sets <code>value</code> as attribute value for DESIG_CD using the alias name DesigCd.
     * @param value value to set the DESIG_CD
     */
    public void setDesigCd(String value) {
        setAttributeInternal(DESIGCD, value);
    }

    /**
     * Gets the attribute value for DEPT_CD using the alias name DeptCd.
     * @return the DEPT_CD
     */
    public String getDeptCd() {
        return (String) getAttributeInternal(DEPTCD);
    }

    /**
     * Sets <code>value</code> as attribute value for DEPT_CD using the alias name DeptCd.
     * @param value value to set the DEPT_CD
     */
    public void setDeptCd(String value) {
        setAttributeInternal(DEPTCD, value);
    }

    /**
     * Gets the attribute value for CONFIRM_DATE using the alias name ConfirmDate.
     * @return the CONFIRM_DATE
     */
    public Date getConfirmDate() {
        return (Date) getAttributeInternal(CONFIRMDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for CONFIRM_DATE using the alias name ConfirmDate.
     * @param value value to set the CONFIRM_DATE
     */
    public void setConfirmDate(Date value) {
        setAttributeInternal(CONFIRMDATE, value);
    }

    /**
     * Gets the attribute value for SETS using the alias name Sets.
     * @return the SETS
     */
    public Number getSets() {
        return (Number) getAttributeInternal(SETS);
    }

    /**
     * Sets <code>value</code> as attribute value for SETS using the alias name Sets.
     * @param value value to set the SETS
     */
    public void setSets(Number value) {
        setAttributeInternal(SETS, value);
    }

    /**
     * Gets the attribute value for PANT_MR using the alias name PantMr.
     * @return the PANT_MR
     */
    public Number getPantMr() {
        return (Number) getAttributeInternal(PANTMR);
    }

    /**
     * Sets <code>value</code> as attribute value for PANT_MR using the alias name PantMr.
     * @param value value to set the PANT_MR
     */
    public void setPantMr(Number value) {
        setAttributeInternal(PANTMR, value);
    }

    /**
     * Gets the attribute value for SHIRT_MR using the alias name ShirtMr.
     * @return the SHIRT_MR
     */
    public Number getShirtMr() {
        return (Number) getAttributeInternal(SHIRTMR);
    }

    /**
     * Sets <code>value</code> as attribute value for SHIRT_MR using the alias name ShirtMr.
     * @param value value to set the SHIRT_MR
     */
    public void setShirtMr(Number value) {
        setAttributeInternal(SHIRTMR, value);
    }

    /**
     * Gets the attribute value for SHOES_NO using the alias name ShoesNo.
     * @return the SHOES_NO
     */
    public String getShoesNo() {
        return (String) getAttributeInternal(SHOESNO);
    }

    /**
     * Sets <code>value</code> as attribute value for SHOES_NO using the alias name ShoesNo.
     * @param value value to set the SHOES_NO
     */
    public void setShoesNo(String value) {
        setAttributeInternal(SHOESNO, value);
    }

    /**
     * Gets the attribute value for FIN_YEAR using the alias name FinYear.
     * @return the FIN_YEAR
     */
    public String getFinYear() {
        return (String) getAttributeInternal(FINYEAR);
    }

    /**
     * Sets <code>value</code> as attribute value for FIN_YEAR using the alias name FinYear.
     * @param value value to set the FIN_YEAR
     */
    public void setFinYear(String value) {
        setAttributeInternal(FINYEAR, value);
    }

    /**
     * Gets the attribute value for UNIT_CD using the alias name UnitCd.
     * @return the UNIT_CD
     */
    public String getUnitCd() {
        return (String) getAttributeInternal(UNITCD);
    }

    /**
     * Sets <code>value</code> as attribute value for UNIT_CD using the alias name UnitCd.
     * @param value value to set the UNIT_CD
     */
    public void setUnitCd(String value) {
        setAttributeInternal(UNITCD, value);
    }

    /**
     * Gets the attribute value for CREATED_BY using the alias name CreatedBy.
     * @return the CREATED_BY
     */
    public String getCreatedBy() {
        return (String) getAttributeInternal(CREATEDBY);
    }


    /**
     * Sets <code>value</code> as attribute value for CREATED_BY using the alias name CreatedBy.
     * @param value value to set the CREATED_BY
     */
    public void setCreatedBy(String value) {
        setAttributeInternal(CREATEDBY, value);
    }

    /**
     * Gets the attribute value for CREATION_DATE using the alias name CreationDate.
     * @return the CREATION_DATE
     */
    public Timestamp getCreationDate() {
        return (Timestamp) getAttributeInternal(CREATIONDATE);
    }


    /**
     * Sets <code>value</code> as attribute value for CREATION_DATE using the alias name CreationDate.
     * @param value value to set the CREATION_DATE
     */
    public void setCreationDate(Timestamp value) {
        setAttributeInternal(CREATIONDATE, value);
    }

    /**
     * Gets the attribute value for LAST_UPDATED_BY using the alias name LastUpdatedBy.
     * @return the LAST_UPDATED_BY
     */
    public String getLastUpdatedBy() {
        return (String) getAttributeInternal(LASTUPDATEDBY);
    }


    /**
     * Sets <code>value</code> as attribute value for LAST_UPDATED_BY using the alias name LastUpdatedBy.
     * @param value value to set the LAST_UPDATED_BY
     */
    public void setLastUpdatedBy(String value) {
        setAttributeInternal(LASTUPDATEDBY, value);
    }

    /**
     * Gets the attribute value for LAST_UPDATE_DATE using the alias name LastUpdateDate.
     * @return the LAST_UPDATE_DATE
     */
    public oracle.jbo.domain.Timestamp getLastUpdateDate() {
        return (oracle.jbo.domain.Timestamp) getAttributeInternal(LASTUPDATEDATE);
    }


    /**
     * Sets <code>value</code> as attribute value for LAST_UPDATE_DATE using the alias name LastUpdateDate.
     * @param value value to set the LAST_UPDATE_DATE
     */
    public void setLastUpdateDate(oracle.jbo.domain.Timestamp value) {
        setAttributeInternal(LASTUPDATEDATE, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Number getObjectVersionNumber() {
        return (Number) getAttributeInternal(OBJECTVERSIONNUMBER);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber(Number value) {
        setAttributeInternal(OBJECTVERSIONNUMBER, value);
    }

    /**
     * Gets the attribute value for EMP_UNIFM_DIST_ID using the alias name EmpUnifmDistId.
     * @return the EMP_UNIFM_DIST_ID
     */
    public Number getEmpUnifmDistId() {
        return (Number) getAttributeInternal(EMPUNIFMDISTID);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_UNIFM_DIST_ID using the alias name EmpUnifmDistId.
     * @param value value to set the EMP_UNIFM_DIST_ID
     */
    public void setEmpUnifmDistId(Number value) {
        setAttributeInternal(EMPUNIFMDISTID, value);
    }

    /**
     * Gets the attribute value for NAME using the alias name Name.
     * @return the NAME
     */
    public String getName() {
        return (String) getAttributeInternal(NAME);
    }

    /**
     * Sets <code>value</code> as attribute value for NAME using the alias name Name.
     * @param value value to set the NAME
     */
    public void setName(String value) {
        setAttributeInternal(NAME, value);
    }

    /**
     * Gets the attribute value for CODE using the alias name Code.
     * @return the CODE
     */
    public String getCode() {
        return (String) getAttributeInternal(CODE);
    }

    /**
     * Sets <code>value</code> as attribute value for CODE using the alias name Code.
     * @param value value to set the CODE
     */
    public void setCode(String value) {
        setAttributeInternal(CODE, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber1.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber1() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER1);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber1.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber1(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER1, value);
    }


    /**
     * Gets the attribute value for the calculated attribute EditTrans.
     * @return the EditTrans
     */
    public Integer getEditTrans() {
        byte entityState = this.getEntity(0).getEntityState();
                return new Integer(entityState);
        //return (Integer) getAttributeInternal(EDITTRANS);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute EditTrans.
     * @param value value to set the  EditTrans
     */
    public void setEditTrans(Integer value) {
        setAttributeInternal(EDITTRANS, value);
    }

    /**
     * Gets the attribute value for NAME using the alias name Name1.
     * @return the NAME
     */
    public String getName1() {
        return (String) getAttributeInternal(NAME1);
    }

    /**
     * Sets <code>value</code> as attribute value for NAME using the alias name Name1.
     * @param value value to set the NAME
     */
    public void setName1(String value) {
        setAttributeInternal(NAME1, value);
    }

    /**
     * Gets the attribute value for CODE using the alias name Code1.
     * @return the CODE
     */
    public String getCode1() {
        return (String) getAttributeInternal(CODE1);
    }

    /**
     * Sets <code>value</code> as attribute value for CODE using the alias name Code1.
     * @param value value to set the CODE
     */
    public void setCode1(String value) {
        setAttributeInternal(CODE1, value);
    }

    /**
     * Gets the attribute value for UNIT_CODE using the alias name UnitCode.
     * @return the UNIT_CODE
     */
    public String getUnitCode() {
        return (String) getAttributeInternal(UNITCODE);
    }

    /**
     * Sets <code>value</code> as attribute value for UNIT_CODE using the alias name UnitCode.
     * @param value value to set the UNIT_CODE
     */
    public void setUnitCode(String value) {
        setAttributeInternal(UNITCODE, value);
    }

    /**
     * Gets the attribute value for ENTRY_NO using the alias name EntryNo1.
     * @return the ENTRY_NO
     */
    public String getEntryNo1() {
        return (String) getAttributeInternal(ENTRYNO1);
    }

    /**
     * Sets <code>value</code> as attribute value for ENTRY_NO using the alias name EntryNo1.
     * @param value value to set the ENTRY_NO
     */
    public void setEntryNo1(String value) {
        setAttributeInternal(ENTRYNO1, value);
    }

    /**
     * Gets the attribute value for DESCRIPTION using the alias name Description.
     * @return the DESCRIPTION
     */
    public String getDescription() {
        return (String) getAttributeInternal(DESCRIPTION);
    }

    /**
     * Sets <code>value</code> as attribute value for DESCRIPTION using the alias name Description.
     * @param value value to set the DESCRIPTION
     */
    public void setDescription(String value) {
        setAttributeInternal(DESCRIPTION, value);
    }

    /**
     * Gets the attribute value for CODE using the alias name Code2.
     * @return the CODE
     */
    public String getCode2() {
        return (String) getAttributeInternal(CODE2);
    }

    /**
     * Sets <code>value</code> as attribute value for CODE using the alias name Code2.
     * @param value value to set the CODE
     */
    public void setCode2(String value) {
        setAttributeInternal(CODE2, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber2.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber2() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER2);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber2.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber2(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER2, value);
    }

    /**
     * Gets the attribute value for the calculated attribute EmpName.
     * @return the EmpName
     */
    public String getEmpName() {
        String name = "";
        HrmTransactionAMImpl am = (HrmTransactionAMImpl) this.getApplicationModule();
        if (getEmpNo() != null && getAttributeInternal(EMPNAME) == null) {
            Row r[] = am.getEmployeeMasterVO1().getFilteredRows("EmpNumber", getEmpNo());
            if (r.length > 0) {
                name = (String) r[0].getAttribute("EmpFirstName");
                System.out.println("Employee Name Create Page: " + name);
                return name;
            }
        }
        return (String) getAttributeInternal(EMPNAME);
    }


    /**
     * Gets the view accessor <code>RowSet</code> FinYearVO1.
     */
    public RowSet getFinYearVO1() {
        return (RowSet) getAttributeInternal(FINYEARVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> UnitVO1.
     */
    public RowSet getUnitVO1() {
        return (RowSet) getAttributeInternal(UNITVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> EmployeeMasterVO1.
     */
    public RowSet getEmployeeMasterVO1() {
        return (RowSet) getAttributeInternal(EMPLOYEEMASTERVO1);
    }

    /**
     * getAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param attrDef the attribute

     * @return the attribute value
     * @throws Exception
     */
    protected Object getAttrInvokeAccessor(int index, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            return AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].get(this);
        }
        return super.getAttrInvokeAccessor(index, attrDef);
    }

    /**
     * setAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param value the value to assign to the attribute
     * @param attrDef the attribute

     * @throws Exception
     */
    protected void setAttrInvokeAccessor(int index, Object value, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].put(this, value);
            return;
        }
        super.setAttrInvokeAccessor(index, value, attrDef);
    }
}

