package terms.hrm.setup.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.application.ViewHandler;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectBooleanCheckbox;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichPanelLabelAndMessage;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;
import oracle.jbo.ViewObject;

import oracle.jbo.server.ViewObjectImpl;

import terms.hrm.setup.model.view.CategoryMasterDetailVORow;
import terms.hrm.setup.model.view.CategoryMasterDetailVORowImpl;

public class CreateCategoryMasterBean {
    private RichButton headerEditBinding;
    private RichButton detailCreateBinding;
    private RichButton detailDeleteBinding;
    private RichTable createCategoryMasterTableBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputComboboxListOfValues levelFromBinding;
    private RichInputText codeBinding;
    private RichInputText categoryDescriptionBinding;
    private RichInputComboboxListOfValues unitCodeDetailBinding;
    private RichInputText codeDetailBinding;
    private RichSelectOneChoice headerTypeDetailBinding;
    private RichInputText headSubCodeDetailBinding;
    private RichInputText headSubCode1Binding;
    private RichInputText amountDetailBinding;
    private RichColumn showFlagDetailBinding;
    private String message="C";
    private RichPanelLabelAndMessage bindingOutputText;
    private RichSelectBooleanCheckbox flagBinding;
    private RichInputText hraOfficerBinding;
    private RichInputText hraAmountBinding;
    private RichInputText basketAmountBinding;
    private RichInputText hfMaBinding;
    private RichInputText convenceAmountNoBinding;
    private RichInputText convenceAmountYesBinding;
    private RichInputText lvldessBinding;
    private RichInputText headCodeDetailBinding;
    private RichOutputText headDescriptionDetailBinding;

    public CreateCategoryMasterBean() {
    }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void saveButtonAL(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedBy("CategoryMasterHeaderVO1Iterator","LastUpdatedBy");
        System.out.println("In Save Button saveButtonAL");
        ADFUtils.findOperation("generateCategoryMasterCode").execute();
        OperationBinding opr = ADFUtils.findOperation("Commit");
        Object obj = opr.execute();
        
        System.out.println("outside error block");
         System.out.println("Category Code value is"+codeBinding.getValue());
         String CatCode=(String) codeBinding.getValue();

         //   if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
         
        //       if(leaveCodeHeaderBinding.getValue() !=null){
        //if (message.equalsIgnoreCase("C")){
        if (CatCode != null && message.equalsIgnoreCase("C")){
                System.out.println("S####");
//                OperationBinding opr = ADFUtils.findOperation("Commit");
//                Object obj = opr.execute();
                FacesMessage Message = new FacesMessage("Record Save Successfully. New Category Code is "+codeBinding.getValue());
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);

            } else if (message.equalsIgnoreCase("E")) {
                FacesMessage Message = new FacesMessage("Record Update Successfully");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            }
    }

    public void setDetailCreateBinding(RichButton detailCreateBinding) {
        this.detailCreateBinding = detailCreateBinding;
    }

    public RichButton getDetailCreateBinding() {
        return detailCreateBinding;
    }

    public void setDetailDeleteBinding(RichButton detailDeleteBinding) {
        this.detailDeleteBinding = detailDeleteBinding;
    }

    public RichButton getDetailDeleteBinding() {
        return detailDeleteBinding;
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                            {
                            ADFUtils.findOperation("Delete").execute();
                                OperationBinding op=  ADFUtils.findOperation("Commit");
                                Object rst = op.execute();
                            System.out.println("Record Delete Successfully");
                           
                                if(op.getErrors().isEmpty())
                                {
                                FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                                Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                                FacesContext fc = FacesContext.getCurrentInstance();  
                                fc.addMessage(null, Message);
                                }
                                else
                                {
                                FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                                FacesContext fc = FacesContext.getCurrentInstance();
                                fc.addMessage(null, Message);
                                 }
                        }
                        AdfFacesContext.getCurrentInstance().addPartialTarget(createCategoryMasterTableBinding);
        
    }

    public void setCreateCategoryMasterTableBinding(RichTable createCategoryMasterTableBinding) {
        this.createCategoryMasterTableBinding = createCategoryMasterTableBinding;
    }

    public RichTable getCreateCategoryMasterTableBinding() {
        return createCategoryMasterTableBinding;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setLevelFromBinding(RichInputComboboxListOfValues levelFromBinding) {
        this.levelFromBinding = levelFromBinding;
    }

    public RichInputComboboxListOfValues getLevelFromBinding() {
        return levelFromBinding;
    }

    public void setCodeBinding(RichInputText codeBinding) {
        this.codeBinding = codeBinding;
    }

    public RichInputText getCodeBinding() {
        return codeBinding;
    }

    public void setCategoryDescriptionBinding(RichInputText categoryDescriptionBinding) {
        this.categoryDescriptionBinding = categoryDescriptionBinding;
    }

    public RichInputText getCategoryDescriptionBinding() {
        return categoryDescriptionBinding;
    }

    public void setUnitCodeDetailBinding(RichInputComboboxListOfValues unitCodeDetailBinding) {
        this.unitCodeDetailBinding = unitCodeDetailBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeDetailBinding() {
        return unitCodeDetailBinding;
    }

    public void setCodeDetailBinding(RichInputText codeDetailBinding) {
        this.codeDetailBinding = codeDetailBinding;
    }

    public RichInputText getCodeDetailBinding() {
        return codeDetailBinding;
    }

    public void setHeaderTypeDetailBinding(RichSelectOneChoice headerTypeDetailBinding) {
        this.headerTypeDetailBinding = headerTypeDetailBinding;
    }

    public RichSelectOneChoice getHeaderTypeDetailBinding() {
        return headerTypeDetailBinding;
    }

    public void setHeadSubCodeDetailBinding(RichInputText headSubCodeDetailBinding) {
        this.headSubCodeDetailBinding = headSubCodeDetailBinding;
    }

    public RichInputText getHeadSubCodeDetailBinding() {
        return headSubCodeDetailBinding;
    }

    public void setHeadSubCode1Binding(RichInputText headSubCode1Binding) {
        this.headSubCode1Binding = headSubCode1Binding;
    }

    public RichInputText getHeadSubCode1Binding() {
        return headSubCode1Binding;
    }

    public void setAmountDetailBinding(RichInputText amountDetailBinding) {
        this.amountDetailBinding = amountDetailBinding;
    }

    public RichInputText getAmountDetailBinding() {
        return amountDetailBinding;
    }

    public void setShowFlagDetailBinding(RichColumn showFlagDetailBinding) {
        this.showFlagDetailBinding = showFlagDetailBinding;
    }

    public RichColumn getShowFlagDetailBinding() {
        return showFlagDetailBinding;
    }
    
    private void cevmodecheck(){
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                try {
                    Method method1 =
                        component.getClass().getMethod("setDisabled", boolean.class);
                    if (method1 != null) {
                        method1.invoke(component, valueComponent);
                    }
                } catch (NoSuchMethodException e) {
                    try {
                        Method method =
                            component.getClass().getMethod("setReadOnly", boolean.class);
                        if (method != null) {
                            method.invoke(component, valueComponent);
                        }
                    } catch (Exception e1) {
                        // e.printStackTrace();//silently eat this exception.
                    }


                } catch (Exception e) {
                    // e.printStackTrace();//silently eat this exception.
                }
                List<UIComponent> childComponents = component.getChildren();
                for (UIComponent comp : childComponents) {
                    makeComponentHierarchyReadOnly(comp, valueComponent);
                }
            }
    
    public void cevModeDisableComponent(String mode) {
          if (mode.equals("E")) {
              getUnitCodeBinding().setDisabled(true);
              getLevelFromBinding().setDisabled(false);
              getCodeBinding().setDisabled(true);
              getCategoryDescriptionBinding().setDisabled(false);
              getUnitCodeDetailBinding().setDisabled(true);
              getHeaderTypeDetailBinding().setDisabled(true);       
              getCodeDetailBinding().setDisabled(true);
              getHeadCodeDetailBinding().setDisabled(true);
              getHeadSubCode1Binding().setDisabled(true);
              getHeadSubCodeDetailBinding().setDisabled(true);
              getAmountDetailBinding().setDisabled(false);
             // getHraAmountBinding().setDisabled(false);
              //getHraOfficerBinding().setDisabled(true);
              String Desc=(String)lvldessBinding.getValue();
              if(Desc.equalsIgnoreCase("Officer"))
              {
                  getHraOfficerBinding().setDisabled(false);
              }
              else
              {
                  hraOfficerBinding.setValue(null);
                  getHraOfficerBinding().setDisabled(true);
                 
              }
              getConvenceAmountYesBinding().setDisabled(false);
              getConvenceAmountNoBinding().setDisabled(false);
              getHfMaBinding().setDisabled(false);
              getBasketAmountBinding().setDisabled(false);
              message="E";
                
                  
          } else if (mode.equals("C")) {
              getUnitCodeBinding().setDisabled(true);
              getLevelFromBinding().setDisabled(false);
              getCodeBinding().setDisabled(true);
              getCategoryDescriptionBinding().setDisabled(false);
              getUnitCodeDetailBinding().setDisabled(true);
            
//              if(headerTypeDetailBinding.getValue()==null){
//              getHeadCodeDetailBinding().setDisabled(true);}
              getHeaderTypeDetailBinding().setDisabled(true);
              getCodeDetailBinding().setDisabled(true);
              getHeadCodeDetailBinding().setDisabled(true);
              getHeadSubCode1Binding().setDisabled(true);
              getHeadSubCodeDetailBinding().setDisabled(true);
              getAmountDetailBinding().setDisabled(false);
              //getHraAmountBinding().setDisabled(false);
              getHraOfficerBinding().setDisabled(true);
              getConvenceAmountYesBinding().setDisabled(false);
              getConvenceAmountNoBinding().setDisabled(false);
              getHfMaBinding().setDisabled(false);
              getBasketAmountBinding().setDisabled(false);
                  
          } else if (mode.equals("V")) {
    //              getDetailcreateBinding().setDisabled(true);
    //              getDetaildeleteBinding().setDisabled(true);
            //  getFlagBinding().setDisabled(true);
                
          
          }
    }

    public void setBindingOutputText(RichPanelLabelAndMessage bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichPanelLabelAndMessage getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void createButtonAL(ActionEvent actionEvent) {
       ADFUtils.findOperation("CreateInsert").execute();

//        ADFUtils.findOperation("generateCategoryMasterCode").execute();
   
      //  Object rst = op.execute();
        
      //  System.out.println("Result is===> "+op.getResult());
        
        
       // System.out.println("--------Commit-------");
            //System.out.println("value aftr getting result--->?"+rst);
            
        
//            if (rst.toString() != null && rst.toString() != "" && rst.toString()!="N") {
//                if (op.getErrors().isEmpty()) {
//                    ADFUtils.findOperation("Commit").execute();
//                    FacesMessage Message = new FacesMessage("Record Saved Successfully. New Entry Number is "+rst+".");   
//                    Message.setSeverity(FacesMessage.SEVERITY_INFO);   
//                    FacesContext fc = FacesContext.getCurrentInstance();   
//                    fc.addMessage(null, Message);  
//        
//                }
//            }
//            
//            if (rst.toString().equalsIgnoreCase("N")) {
//                if (op.getErrors().isEmpty()) {
//                    ADFUtils.findOperation("Commit").execute();
//                    FacesMessage Message = new FacesMessage("Record Update Successfully.");   
//                    Message.setSeverity(FacesMessage.SEVERITY_INFO);   
//                    FacesContext fc = FacesContext.getCurrentInstance();   
//                    fc.addMessage(null, Message);  
//            
//                }
//
//            
//            }
    }

    public void setFlagBinding(RichSelectBooleanCheckbox flagBinding) {
        this.flagBinding = flagBinding;
    }

    public RichSelectBooleanCheckbox getFlagBinding() {
        return flagBinding;
    }

    public void setHraOfficerBinding(RichInputText hraOfficerBinding) {
        this.hraOfficerBinding = hraOfficerBinding;
    }

    public RichInputText getHraOfficerBinding() {
        return hraOfficerBinding;
    }

    public void setHraAmountBinding(RichInputText hraAmountBinding) {
        this.hraAmountBinding = hraAmountBinding;
    }

    public RichInputText getHraAmountBinding() {
        return hraAmountBinding;
    }

    public void setBasketAmountBinding(RichInputText basketAmountBinding) {
        this.basketAmountBinding = basketAmountBinding;
    }

    public RichInputText getBasketAmountBinding() {
        return basketAmountBinding;
    }

    public void setHfMaBinding(RichInputText hfMaBinding) {
        this.hfMaBinding = hfMaBinding;
    }

    public RichInputText getHfMaBinding() {
        return hfMaBinding;
    }

    public void setConvenceAmountNoBinding(RichInputText convenceAmountNoBinding) {
        this.convenceAmountNoBinding = convenceAmountNoBinding;
    }

    public RichInputText getConvenceAmountNoBinding() {
        return convenceAmountNoBinding;
    }

    public void setConvenceAmountYesBinding(RichInputText convenceAmountYesBinding) {
        this.convenceAmountYesBinding = convenceAmountYesBinding;
    }

    public RichInputText getConvenceAmountYesBinding() {
        return convenceAmountYesBinding;
    }

    public void haedTypeVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        System.out.println("Hello from Head type VCE");
         
    }

    public void catDesBean(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
         System.out.println("Before Method");
        String des=  (String)valueChangeEvent.getNewValue();
        System.out.println(des);
        String lvldes=(String)lvldessBinding.getValue();
        System.out.println(lvldes);
        if(lvldes.equalsIgnoreCase("Officer")){
            System.out.println("in if"+lvldes);
            getHraOfficerBinding().setDisabled(false);
        }
        else{
            System.out.println("in else"+lvldes);
            hraOfficerBinding.setValue(null);
            getHraOfficerBinding().setDisabled(true);
            
        }
    }

    public void setLvldessBinding(RichInputText lvldessBinding) {
        this.lvldessBinding = lvldessBinding;
    }

    public RichInputText getLvldessBinding() {
        return lvldessBinding;
    }

    public void hraForOfficerVCL(ValueChangeEvent valueChangeEvent) {
        System.out.println("hraForOfficerVCL--->");
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
         System.out.println("Before Method in hra officer");
        BigDecimal HRA=  (BigDecimal)valueChangeEvent.getNewValue();
        System.out.println("hra"+HRA);
        String lvldes=(String)lvldessBinding.getValue();
        System.out.println(lvldes);
        if(lvldes.equalsIgnoreCase("Officer") && HRA != null){
            getHraAmountBinding().setDisabled(true);   
        }
        else if(lvldes.equalsIgnoreCase("Officer") && HRA == null ){
            getHraAmountBinding().setDisabled(false); 
            hraOfficerBinding.setValue(null);
          //  lvldessBinding.setValue(null);
        }
        
        
        if (message.equalsIgnoreCase("E")) {
        
        System.out.println("hraForOfficerVCL");
        System.out.println("hraAmountVCL"+valueChangeEvent.getNewValue());    
      
        DCIteratorBinding dci1 = ADFUtils.findIterator("CategoryMasterHeaderVO1Iterator");
        RowSetIterator rsi1=dci1.getViewObject().createRowSetIterator(null);
            
            String lvlFrm=(String)dci1.getCurrentRow().getAttribute("LevelFrom");    
            System.out.println("getLevelFromBindinghraForOfficerVCL"+lvlFrm);
            String lvlFrmDes=(String)getLvldessBinding().getValue();    
            System.out.println("getLvldessBindinghraForOfficerVCL"+lvlFrmDes);     
            String unitcd=(String)dci1.getCurrentRow().getAttribute("UnitCd");
            System.out.println("getUnitCodeBindinghraForOfficerVCL"+unitcd);  
            BigDecimal HrAmt= (BigDecimal) dci1.getCurrentRow().getAttribute("HrAmt");
            System.out.println("getHraAmountBindinghraForOfficerVCL"+HrAmt); 
            BigDecimal HrPr= (BigDecimal) dci1.getCurrentRow().getAttribute("HrPer");     
            System.out.println("getHrPrAmountBindinghraForOfficerVCL"+HrPr); 
            
//        String lvlFrm=(String)getLevelFromBinding().getValue();    
//        System.out.println("getLevelFromBindinghraForOfficerVCL"+lvlFrm);
//        String lvlFrmDes=(String)getLvldessBinding().getValue();    
//        System.out.println("getLvldessBindinghraForOfficerVCL"+lvlFrmDes);     
//        String unitcd=(String)getUnitCodeBinding().getValue();
//        System.out.println("getUnitCodeBindinghraForOfficerVCL"+unitcd);  
//        BigDecimal HrAmt= (BigDecimal) getHraAmountBinding().getValue();
//        System.out.println("getHraAmountBindinghraForOfficerVCL"+HrAmt);  
//        BigDecimal HrPr= (BigDecimal) getHraOfficerBinding().getValue();    
//        System.out.println("getHraOfficerBinding"+HrPr);  
//        String hdCd=(String)getHeadCodeDetailBinding().getValue();    
//        System.out.println("getLevelFromBinding"+hdCd);
//        String hdCdDes=(String)getHeadDescriptionDetailBinding().getValue();    
//        System.out.println("getLvldessBinding"+hdCdDes);      
//        if(hdCd!=null && hdCdDes.equalsIgnoreCase("Basic")){
//            
//            BigDecimal amtDtl= (BigDecimal) getAmountDetailBinding().getValue();
//            System.out.println("Amount Value"+amtDtl);
                DCIteratorBinding dci = ADFUtils.findIterator("CategoryMasterDetailVO1Iterator");
               RowSetIterator rsi=dci.getViewObject().createRowSetIterator(null);
               while(rsi.hasNext()) {
                   Row r = rsi.next();
                
                   System.out.println("CategoryMasterDetailVO1IteratorNew");
                   String hdCd=(String) r.getAttribute("HeadCode");   
                  // String hdCd=(String)rsi.getCurrentRow().getAttribute("HeadCode");
                   System.out.println("getLevelFromBindinghraForOfficer--->"+hdCd);
                   String hdCdDes=(String) getHeadDescriptionDetailBinding().getValue();    
                  System.out.println("getLvldessBindinghraForOfficerVCL"+hdCdDes); 
//                   System.out.println("CategoryMasterDetailVO1Iterator");
//                   String hdCd=(String) getHeadCodeDetailBinding().getValue();    
//                   System.out.println("getLevelFromBindinghraForOfficerVCL"+hdCd);
//                   String hdCdDes=(String) getHeadDescriptionDetailBinding().getValue();    
//                   System.out.println("getLvldessBindinghraForOfficerVCL"+hdCdDes); 
                   
                 
                   
//                   BigDecimal amtDtl= (BigDecimal) getAmountDetailBinding().getValue();
//                   System.out.println("Amount Value hraForOfficerVCL"+amtDtl);
//                   
////                   Row r = rsi.next();
//                   if(hdCdDes.equalsIgnoreCase("Basic"))
                              if(hdCd!=null && hdCd.equalsIgnoreCase("001")){
//                   if(hdCd!=null && hdCdDes.equalsIgnoreCase("Basic")){
                   {                         
//                           BigDecimal amtDtl2= (BigDecimal) getAmountDetailBinding().getValue();
//                           System.out.println("Amount Value2 -->"+amtDtl2);
                           
                           BigDecimal amtDtl2= (BigDecimal)rsi.getCurrentRow().getAttribute("Amount");
                           System.out.println("Amount Value2 -->"+amtDtl2);
                           OperationBinding op=ADFUtils.findOperation("populateAmountValueInCategoryDetail");
                           op.getParamsMap().put("unitcd", unitcd);
                           op.getParamsMap().put("lvlFrm", lvlFrm);
                           op.getParamsMap().put("HrPr", HRA);
                           op.getParamsMap().put("HrAmt", HrAmt);
                           op.getParamsMap().put("basAmt", amtDtl2);
                           System.out.println("In hraForOfficerVCL Bean End 1");
                           op.execute();
                           System.out.println("In  hraForOfficerVCL Bean End");              
                           op.getResult();
                           System.out.println("Value in Bean"+op.getResult());                      
                       }
               }
       /// } 
        
               }
               rsi.closeRowSetIterator();
               rsi1.closeRowSetIterator();
        }
    }

    public void hraAmountVCL(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
         System.out.println("Before Method in hra amount");
        BigDecimal HRA1=  (BigDecimal)valueChangeEvent.getNewValue();
        System.out.println("hraamount"+HRA1);
        String lvldes=(String)lvldessBinding.getValue();
        System.out.println(lvldes);
        if(lvldes!=null && lvldes.equalsIgnoreCase("Officer") && HRA1 != null){
            getHraOfficerBinding().setDisabled(true);
        }
        else if(lvldes!=null && lvldes.equalsIgnoreCase("Officer") && HRA1 == null ){
            getHraOfficerBinding().setDisabled(false);
            hraAmountBinding.setValue(null);
        }
        else{
            System.out.println("hraamount"+HRA1);
        }      
    }
    
    
    public void refreshPage() {
          FacesContext fctx = FacesContext.getCurrentInstance();
          String page = fctx.getViewRoot().getViewId();
          ViewHandler ViewH = fctx.getApplication().getViewHandler();
          UIViewRoot UIV = ViewH.createView(fctx, page);
          UIV.setViewId(page);
          fctx.setViewRoot(UIV);
      }

    public void populateButtonAL(ActionEvent actionEvent) {
        System.out.println("In Bean");
        String unitcd=(String)getUnitCodeBinding().getValue();
        String lvlFrm=(String)getLevelFromBinding().getValue();
        String lvlFrmDesc=(String)getLvldessBinding().getValue();
        BigDecimal HrAmt= (BigDecimal) getHraAmountBinding().getValue();
        BigDecimal HrPr= (BigDecimal) getHraOfficerBinding().getValue();
        BigDecimal conYes= (BigDecimal) getConvenceAmountYesBinding().getValue();
        BigDecimal conNo= (BigDecimal) getConvenceAmountNoBinding().getValue();
        BigDecimal hfMa= (BigDecimal) getHfMaBinding().getValue();
        BigDecimal basAmt= (BigDecimal) getBasketAmountBinding().getValue();
        String entryBy="SWE161";
        
        System.out.println("Value of Unit Cd" + unitcd +"Value of lvlFrm " + lvlFrm +"Value of HrAmt " + HrAmt +"Value of HrPr " + HrPr +"Value of conYes " + conYes
                           +"Value of conNo " + conNo+"Value of hfMa " + hfMa +"Value of basAmt " + basAmt +"Value of entryBy " + entryBy);
        
        System.out.println("unitcd Value"+unitcd);
        System.out.println("lvlFrm Value"+lvlFrm);
        
        
        OperationBinding op=ADFUtils.findOperation("populateSalaryHeadsInCategoryMaster");
        op.getParamsMap().put("unitcd", unitcd);
        op.getParamsMap().put("lvlFrm", lvlFrm);
        op.getParamsMap().put("HrAmt", HrAmt);
        op.getParamsMap().put("HrPr", HrPr);
        op.getParamsMap().put("conYes", conYes);
        op.getParamsMap().put("conNo", conNo);
        op.getParamsMap().put("hfMa", hfMa);
        op.getParamsMap().put("basAmt", basAmt);
        //op.getParamsMap().put("entryBy", entryBy);
        System.out.println("In Bean populate ButtonAL 1");
        op.execute();
        System.out.println("In Bean populate ButtonAL");
        
//        AdfFacesContext.getCurrentInstance().addPartialTarget(createCategoryMasterTableBinding);
//        refreshPage();
        DCIteratorBinding dci1 = ADFUtils.findIterator("CategoryMasterDetailVO1Iterator");
        RowSetIterator rsi1=dci1.getViewObject().createRowSetIterator(null);
        rsi1.getEstimatedRangePageCount();
        System.out.println(" rsi1.getEstimatedRangePageCount()"+ rsi1.getEstimatedRangePageCount());
        if(unitcd!=null && lvlFrm!=null && rsi1.getEstimatedRangePageCount() > 0)
        {
           
             System.out.println("In If for detail HRA Amt");
              BigDecimal amtDtl2= (BigDecimal) getAmountDetailBinding().getValue();
              System.out.println("Amount Value2 -->"+amtDtl2);
              System.out.println("Values unitcd"+unitcd + " " + lvlFrm + " " + HrPr +" "+ HrAmt + " "+amtDtl2);
              OperationBinding op1=ADFUtils.findOperation("populateAmountValueInCategoryDetail2");
              op1.getParamsMap().put("unitcd", unitcd);
              op1.getParamsMap().put("lvlFrm", lvlFrm);
              op1.getParamsMap().put("HrPr", HrPr);
              op1.getParamsMap().put("HrAmt", HrAmt);
              op1.getParamsMap().put("basAmt", amtDtl2);
              System.out.println("In Bean populateButtonAL ");
              op1.execute();
              //refreshPage();
              System.out.println("In Bean populateButtonAL populateAmountValueInCategoryDetail");              
              op1.getResult();
              //refreshPage();
              System.out.println("Value in Bean populateButtonAL populateAmountValueInCategoryDetail "+op.getResult());  
          }
        rsi1.closeRowSetIterator();
//        refreshPage();
        // AdfFacesContext.getCurrentInstance().addPartialTarget(createCategoryMasterTableBinding);
    }
   

    public void detailAmountVCL(ValueChangeEvent vce) {
        System.out.println("detailAmountVCL"+vce.getNewValue());    
        DCIteratorBinding dci = ADFUtils.findIterator("CategoryMasterDetailVO1Iterator");
        RowSetIterator rsi=dci.getViewObject().createRowSetIterator(null);
        String lvlFrm=(String)getLevelFromBinding().getValue();    
        System.out.println("getLevelFromBinding detailAmountVCL"+lvlFrm);
        String lvlFrmDes=(String)getLvldessBinding().getValue();    
        System.out.println("getLvldessBinding detailAmountVCL"+lvlFrmDes);     
        String unitcd=(String)getUnitCodeBinding().getValue();
        System.out.println("unitcd detailAmountVCL-->"+unitcd );
        BigDecimal HrAmt= (BigDecimal) getHraAmountBinding().getValue();
        System.out.println("HrAmt detailAmountVCL-->"+HrAmt);
        BigDecimal HrPr= (BigDecimal) getHraOfficerBinding().getValue();  
        System.out.println("HrPr detailAmountVCL -->"+HrPr );
        String hdCd=(String)getHeadCodeDetailBinding().getValue();    
        System.out.println("getHeadCodeDetailBinding detailAmountVCL"+hdCd);
        String hdCdDes=(String)getHeadDescriptionDetailBinding().getValue();    
        System.out.println("getHeadDescriptionDetailBinding detailAmountVCL"+hdCdDes);      
        if(hdCd!=null && hdCdDes.equalsIgnoreCase("Basic")){
            
            BigDecimal amtDtl= (BigDecimal) getAmountDetailBinding().getValue();
            System.out.println("Amount Value detailAmountVCL"+amtDtl);
               while(rsi.hasNext()) {
                   System.out.println("CategoryMasterDetailVO1Iterator detailAmountVCL");
                   Row r = rsi.next();
                   if(hdCdDes.equalsIgnoreCase("Basic"))
                   {                         
                           BigDecimal amtDtl2= (BigDecimal) getAmountDetailBinding().getValue();
                           System.out.println("Amount Value2 detailAmountVCL-->"+amtDtl2);
                           OperationBinding op=ADFUtils.findOperation("populateAmountValueInCategoryDetail");
                           op.getParamsMap().put("unitcd", unitcd);
                           op.getParamsMap().put("lvlFrm", lvlFrm);
                           op.getParamsMap().put("HrPr", HrPr);
                           op.getParamsMap().put("HrAmt", HrAmt);
                           op.getParamsMap().put("basAmt", amtDtl2);
                           System.out.println("In BeanEnd 1 detailAmountVCL");
                           op.execute();
                           System.out.println("In BeanEnd detailAmountVCL");              
                           op.getResult();
                           System.out.println("Value in Bean"+op.getResult());                      
                       }
               }
               rsi.closeRowSetIterator();
        } 
        rsi.closeRowSetIterator();
    }

    public void setHeadCodeDetailBinding(RichInputText headCodeDetailBinding) {
        this.headCodeDetailBinding = headCodeDetailBinding;
    }

    public RichInputText getHeadCodeDetailBinding() {
        return headCodeDetailBinding;
    }

    public void setHeadDescriptionDetailBinding(RichOutputText headDescriptionDetailBinding) {
        this.headDescriptionDetailBinding = headDescriptionDetailBinding;
    }

    public RichOutputText getHeadDescriptionDetailBinding() {
        return headDescriptionDetailBinding;
    }
}
