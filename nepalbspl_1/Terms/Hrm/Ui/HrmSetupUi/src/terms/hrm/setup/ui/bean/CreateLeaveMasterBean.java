package terms.hrm.setup.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.RichDialog;
import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class CreateLeaveMasterBean {
    private RichTable createLeaveMasterTableBinding;
    private RichTable createLeaveMasterTableBinding1;
    private RichInputComboboxListOfValues unitCodeLeaveHeaderBinding;
    private RichInputText leaveCodeHeaderBinding;
    private RichInputText leaveDescriptionBinding;
    private RichSelectOneChoice payModeBinding;
    private RichInputText leaveLimitBinding;
    private RichInputText maxLeaveBinding;
    private RichInputText minLeaveBinding;
    private RichInputText leaveEncashBinding;
    private RichInputComboboxListOfValues unitCodeLeaveDetailBinding;
    private RichInputText leaveCodeDetailBinding;
    private RichInputComboboxListOfValues salaryHeadCodeBinding;
    private RichOutputText bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichButton detailcreateBinding;
    private RichButton detaildeleteBinding;
    private RichInputComboboxListOfValues unitCodeLeaveDetailDetailBinding;
    private RichInputText leaveCodeDetailDetailBinding;
    private RichInputComboboxListOfValues salaryHeadCodeDetailDetailBinding;
    private RichButton detailDetailCreateBinding;
    private RichButton detailDetailDeleteBinding;
    private String message = "C";
    private RichInputText salaryHeadSubCodeBinding;
    private RichInputText salaryHeadSubCode1Binding;

    public CreateLeaveMasterBean() {
    }

    public void deletePopUpDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete").execute();
            OperationBinding op = ADFUtils.findOperation("Commit");
            Object rst = op.execute();
            System.out.println("Record Delete Successfully");

            if (op.getErrors().isEmpty()) {
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            } else {
                FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(createLeaveMasterTableBinding);

    }

    public void setCreateLeaveMasterTableBinding(RichTable createLeaveMasterTableBinding) {
        this.createLeaveMasterTableBinding = createLeaveMasterTableBinding;
    }

    public RichTable getCreateLeaveMasterTableBinding() {
        return createLeaveMasterTableBinding;
    }

    public void setCreateLeaveMasterTableBinding1(RichTable createLeaveMasterTableBinding1) {
        this.createLeaveMasterTableBinding1 = createLeaveMasterTableBinding1;
    }

    public RichTable getCreateLeaveMasterTableBinding1() {
        return createLeaveMasterTableBinding1;
    }


    public void deletePopupDL1(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete1").execute();
            OperationBinding op = ADFUtils.findOperation("Commit1");
            Object rst = op.execute();
            System.out.println("Record Delete Successfully");

            if (op.getErrors().isEmpty()) {
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            } else {
                FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(createLeaveMasterTableBinding1);
    }

    public void setUnitCodeLeaveHeaderBinding(RichInputComboboxListOfValues unitCodeLeaveHeaderBinding) {
        this.unitCodeLeaveHeaderBinding = unitCodeLeaveHeaderBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeLeaveHeaderBinding() {
        return unitCodeLeaveHeaderBinding;
    }

    public void setLeaveCodeHeaderBinding(RichInputText leaveCodeHeaderBinding) {
        this.leaveCodeHeaderBinding = leaveCodeHeaderBinding;
    }

    public RichInputText getLeaveCodeHeaderBinding() {
        return leaveCodeHeaderBinding;
    }

    public void setLeaveDescriptionBinding(RichInputText leaveDescriptionBinding) {
        this.leaveDescriptionBinding = leaveDescriptionBinding;
    }

    public RichInputText getLeaveDescriptionBinding() {
        return leaveDescriptionBinding;
    }

    public void setPayModeBinding(RichSelectOneChoice payModeBinding) {
        this.payModeBinding = payModeBinding;
    }

    public RichSelectOneChoice getPayModeBinding() {
        return payModeBinding;
    }

    public void setLeaveLimitBinding(RichInputText leaveLimitBinding) {
        this.leaveLimitBinding = leaveLimitBinding;
    }

    public RichInputText getLeaveLimitBinding() {
        return leaveLimitBinding;
    }

    public void setMaxLeaveBinding(RichInputText maxLeaveBinding) {
        this.maxLeaveBinding = maxLeaveBinding;
    }

    public RichInputText getMaxLeaveBinding() {
        return maxLeaveBinding;
    }

    public void setMinLeaveBinding(RichInputText minLeaveBinding) {
        this.minLeaveBinding = minLeaveBinding;
    }

    public RichInputText getMinLeaveBinding() {
        return minLeaveBinding;
    }

    public void setLeaveEncashBinding(RichInputText leaveEncashBinding) {
        this.leaveEncashBinding = leaveEncashBinding;
    }

    public RichInputText getLeaveEncashBinding() {
        return leaveEncashBinding;
    }

    public void setUnitCodeLeaveDetailBinding(RichInputComboboxListOfValues unitCodeLeaveDetailBinding) {
        this.unitCodeLeaveDetailBinding = unitCodeLeaveDetailBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeLeaveDetailBinding() {
        return unitCodeLeaveDetailBinding;
    }

    public void setLeaveCodeDetailBinding(RichInputText leaveCodeDetailBinding) {
        this.leaveCodeDetailBinding = leaveCodeDetailBinding;
    }

    public RichInputText getLeaveCodeDetailBinding() {
        return leaveCodeDetailBinding;
    }

    public void setSalaryHeadCodeBinding(RichInputComboboxListOfValues salaryHeadCodeBinding) {
        this.salaryHeadCodeBinding = salaryHeadCodeBinding;
    }

    public RichInputComboboxListOfValues getSalaryHeadCodeBinding() {
        return salaryHeadCodeBinding;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setDetailcreateBinding(RichButton detailcreateBinding) {
        this.detailcreateBinding = detailcreateBinding;
    }

    public RichButton getDetailcreateBinding() {
        return detailcreateBinding;
    }

    public void setDetaildeleteBinding(RichButton detaildeleteBinding) {
        this.detaildeleteBinding = detaildeleteBinding;
    }

    public RichButton getDetaildeleteBinding() {
        return detaildeleteBinding;
    }

    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }

    public void cevModeDisableComponent(String mode) {
        if (mode.equals("E")) {
            getDetailDetailCreateBinding().setDisabled(false);
            getDetailDetailDeleteBinding().setDisabled(false);
            getUnitCodeLeaveHeaderBinding().setDisabled(true);
            getLeaveCodeHeaderBinding().setDisabled(false);
            getLeaveDescriptionBinding().setDisabled(false);
            getPayModeBinding().setDisabled(false);
            getLeaveLimitBinding().setDisabled(false);
            getMaxLeaveBinding().setDisabled(false);
            getMinLeaveBinding().setDisabled(false);
          //getLeaveEncashBinding().setDisabled(false);
            getUnitCodeLeaveDetailBinding().setDisabled(true);
            getLeaveCodeDetailBinding().setDisabled(true);
            getSalaryHeadSubCodeBinding().setDisabled(true);
            getSalaryHeadSubCode1Binding().setDisabled(true);
            getSalaryHeadCodeBinding().setDisabled(false);
            getUnitCodeLeaveDetailDetailBinding().setDisabled(true);
            getLeaveCodeDetailDetailBinding().setDisabled(true);
            getSalaryHeadCodeDetailDetailBinding().setDisabled(false);
            message = "E";


        } else if (mode.equals("C")) {
            getUnitCodeLeaveHeaderBinding().setDisabled(true);
            getLeaveCodeHeaderBinding().setDisabled(false);
            getLeaveDescriptionBinding().setDisabled(false);
            getPayModeBinding().setDisabled(false);
            getLeaveLimitBinding().setDisabled(false);
            getMaxLeaveBinding().setDisabled(false);
            getMinLeaveBinding().setDisabled(false);
            getUnitCodeLeaveDetailBinding().setDisabled(true);
            getLeaveCodeDetailBinding().setDisabled(true);
            getSalaryHeadCodeBinding().setDisabled(false);
            getUnitCodeLeaveDetailDetailBinding().setDisabled(true);
            getLeaveCodeDetailDetailBinding().setDisabled(true);
            getSalaryHeadCodeDetailDetailBinding().setDisabled(false);
            getSalaryHeadSubCodeBinding().setDisabled(true);
            getSalaryHeadSubCode1Binding().setDisabled(true);
        } else if (mode.equals("V")) {
            //              getDetailcreateBinding().setDisabled(true);
            //              getDetaildeleteBinding().setDisabled(true);
            getDetailDetailCreateBinding().setDisabled(true);
            getDetailDetailDeleteBinding().setDisabled(true);


        }

    }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void saveButtonAL(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedBy("LeaveMasterHeaderVO1Iterator","LastUpdatedBy");
        OperationBinding opr = ADFUtils.findOperation("Commit");
        Object obj = opr.execute();

        System.out.println("outside error block");
        System.out.println("Level Code value is" + leaveCodeHeaderBinding.getValue());

        //   if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {

        //       if(leaveCodeHeaderBinding.getValue() !=null){
        if (message.equalsIgnoreCase("C")) {
            System.out.println("S####");
            FacesMessage Message = new FacesMessage("Record Save Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        } else if (message.equalsIgnoreCase("E")) {
            FacesMessage Message = new FacesMessage("Record Update Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
        }

    }

    public void setUnitCodeLeaveDetailDetailBinding(RichInputComboboxListOfValues unitCodeLeaveDetailDetailBinding) {
        this.unitCodeLeaveDetailDetailBinding = unitCodeLeaveDetailDetailBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeLeaveDetailDetailBinding() {
        return unitCodeLeaveDetailDetailBinding;
    }

    public void setLeaveCodeDetailDetailBinding(RichInputText leaveCodeDetailDetailBinding) {
        this.leaveCodeDetailDetailBinding = leaveCodeDetailDetailBinding;
    }

    public RichInputText getLeaveCodeDetailDetailBinding() {
        return leaveCodeDetailDetailBinding;
    }

    public void setSalaryHeadCodeDetailDetailBinding(RichInputComboboxListOfValues salaryHeadCodeDetailDetailBinding) {
        this.salaryHeadCodeDetailDetailBinding = salaryHeadCodeDetailDetailBinding;
    }

    public RichInputComboboxListOfValues getSalaryHeadCodeDetailDetailBinding() {
        return salaryHeadCodeDetailDetailBinding;
    }

    public void setDetailDetailCreateBinding(RichButton detailDetailCreateBinding) {
        this.detailDetailCreateBinding = detailDetailCreateBinding;
    }

    public RichButton getDetailDetailCreateBinding() {
        return detailDetailCreateBinding;
    }

    public void setDetailDetailDeleteBinding(RichButton detailDetailDeleteBinding) {
        this.detailDetailDeleteBinding = detailDetailDeleteBinding;
    }

    public RichButton getDetailDetailDeleteBinding() {
        return detailDetailDeleteBinding;
    }

    public void setSalaryHeadSubCodeBinding(RichInputText salaryHeadSubCodeBinding) {
        this.salaryHeadSubCodeBinding = salaryHeadSubCodeBinding;
    }

    public RichInputText getSalaryHeadSubCodeBinding() {
        return salaryHeadSubCodeBinding;
    }

    public void setSalaryHeadSubCode1Binding(RichInputText salaryHeadSubCode1Binding) {
        this.salaryHeadSubCode1Binding = salaryHeadSubCode1Binding;
    }

    public RichInputText getSalaryHeadSubCode1Binding() {
        return salaryHeadSubCode1Binding;
    }
}
