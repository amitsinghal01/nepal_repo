package terms.hrm.setup.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.binding.OperationBinding;

public class CreateLevelWiseLeaveMasterBean {
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputComboboxListOfValues levelCodeBinding;
    private RichInputComboboxListOfValues leaveCodeBinding;
    private RichInputText dayMonthYearBinding;
    private RichInputText maximumRateBinding;
    private RichInputText maximumFrequencyBinding;
    private RichInputText accumulationDayBinding;
    private RichInputText minEncashmentBinding;
    private RichInputText maxCarryForwardDayBinding;
    private RichInputText minRateBinding;
    private RichInputText minFrequencyBinding;
    private RichInputText numberOfTimeInYearBinding;
    private RichInputText maxEncashmentBinding;
    private RichOutputText bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private String message = "C";
    private RichSelectOneChoice dmyTypeBinding;
    private RichSelectOneChoice listTypeBinding;

    public CreateLevelWiseLeaveMasterBean() {
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setLevelCodeBinding(RichInputComboboxListOfValues levelCodeBinding) {
        this.levelCodeBinding = levelCodeBinding;
    }

    public RichInputComboboxListOfValues getLevelCodeBinding() {
        return levelCodeBinding;
    }

    public void setLeaveCodeBinding(RichInputComboboxListOfValues leaveCodeBinding) {
        this.leaveCodeBinding = leaveCodeBinding;
    }

    public RichInputComboboxListOfValues getLeaveCodeBinding() {
        return leaveCodeBinding;
    }

    public void setDayMonthYearBinding(RichInputText dayMonthYearBinding) {
        this.dayMonthYearBinding = dayMonthYearBinding;
    }

    public RichInputText getDayMonthYearBinding() {
        return dayMonthYearBinding;
    }

    public void setMaximumRateBinding(RichInputText maximumRateBinding) {
        this.maximumRateBinding = maximumRateBinding;
    }

    public RichInputText getMaximumRateBinding() {
        return maximumRateBinding;
    }

    public void setMaximumFrequencyBinding(RichInputText maximumFrequencyBinding) {
        this.maximumFrequencyBinding = maximumFrequencyBinding;
    }

    public RichInputText getMaximumFrequencyBinding() {
        return maximumFrequencyBinding;
    }

    public void setAccumulationDayBinding(RichInputText accumulationDayBinding) {
        this.accumulationDayBinding = accumulationDayBinding;
    }

    public RichInputText getAccumulationDayBinding() {
        return accumulationDayBinding;
    }

    public void setMinEncashmentBinding(RichInputText minEncashmentBinding) {
        this.minEncashmentBinding = minEncashmentBinding;
    }

    public RichInputText getMinEncashmentBinding() {
        return minEncashmentBinding;
    }

    public void setMaxCarryForwardDayBinding(RichInputText maxCarryForwardDayBinding) {
        this.maxCarryForwardDayBinding = maxCarryForwardDayBinding;
    }

    public RichInputText getMaxCarryForwardDayBinding() {
        return maxCarryForwardDayBinding;
    }


    public void setMinRateBinding(RichInputText minRateBinding) {
        this.minRateBinding = minRateBinding;
    }

    public RichInputText getMinRateBinding() {
        return minRateBinding;
    }

    public void setMinFrequencyBinding(RichInputText minFrequencyBinding) {
        this.minFrequencyBinding = minFrequencyBinding;
    }

    public RichInputText getMinFrequencyBinding() {
        return minFrequencyBinding;
    }

    public void setNumberOfTimeInYearBinding(RichInputText numberOfTimeInYearBinding) {
        this.numberOfTimeInYearBinding = numberOfTimeInYearBinding;
    }

    public RichInputText getNumberOfTimeInYearBinding() {
        return numberOfTimeInYearBinding;
    }

    public void setMaxEncashmentBinding(RichInputText maxEncashmentBinding) {
        this.maxEncashmentBinding = maxEncashmentBinding;
    }

    public RichInputText getMaxEncashmentBinding() {
        return maxEncashmentBinding;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }
    //Set Fields and Button disable.
    public void cevModeDisableComponent(String mode) {
        //            FacesContext fctx = FacesContext.getCurrentInstance();
        //            ELContext elctx = fctx.getELContext();
        //            Application jsfApp = fctx.getApplication();
        //            //create a ValueExpression that points to the ADF binding layer
        //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
        //            //
        //            ValueExpression valueExpr = exprFactory.createValueExpression(
        //                                         elctx,
        //                                         "#{pageFlowScope.mode=='E'}",
        //                                          Object.class
        //                                         );
        //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
        if (mode.equals("E")) {
            message = "E";
            getUnitCodeBinding().setDisabled(true);
            getLevelCodeBinding().setDisabled(true);
            getLeaveCodeBinding().setDisabled(true);
            getDayMonthYearBinding().setDisabled(false);
            getMaximumRateBinding().setDisabled(false);
            getDayMonthYearBinding().setDisabled(false);
            getMaximumRateBinding().setDisabled(false);
            getMaximumFrequencyBinding().setDisabled(false);
            getAccumulationDayBinding().setDisabled(false);
            getMinEncashmentBinding().setDisabled(true);
            getMaxCarryForwardDayBinding().setDisabled(false);
            getDmyTypeBinding().setDisabled(false);
            getMinRateBinding().setDisabled(false);
            getMinFrequencyBinding().setDisabled(false);
            getMaxEncashmentBinding().setDisabled(true);

        } else if (mode.equals("C")) {

            getUnitCodeBinding().setDisabled(true);
            getLevelCodeBinding().setDisabled(false);
            getLeaveCodeBinding().setDisabled(false);
            getDayMonthYearBinding().setDisabled(false);
            getMaximumRateBinding().setDisabled(false);
            getDayMonthYearBinding().setDisabled(false);
            getMaximumRateBinding().setDisabled(false);
            getMaximumFrequencyBinding().setDisabled(false);
            getAccumulationDayBinding().setDisabled(false);
            getMinEncashmentBinding().setDisabled(true);
            getMaxCarryForwardDayBinding().setDisabled(false);
            getDmyTypeBinding().setDisabled(false);
            getMinRateBinding().setDisabled(false);
            getMinFrequencyBinding().setDisabled(false);
            getMaxEncashmentBinding().setDisabled(true);


        } else if (mode.equals("V")) {

        }

    }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void saveButtonAL(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedBy("LevelWiseLeaveMasterVO1Iterator","LastUpdatedBy");
        OperationBinding opr = ADFUtils.findOperation("Commit");
        Object obj = opr.execute();

        System.out.println("outside error block");


        //   if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {

        //       if(leaveCodeHeaderBinding.getValue() !=null){
        if (message.equalsIgnoreCase("C")) {
            System.out.println("S####");
            FacesMessage Message = new FacesMessage("Record Save Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        } else if (message.equalsIgnoreCase("E")) {
            FacesMessage Message = new FacesMessage("Record Update Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
        }
    }

    public void setDmyTypeBinding(RichSelectOneChoice dmyTypeBinding) {
        this.dmyTypeBinding = dmyTypeBinding;
    }

    public RichSelectOneChoice getDmyTypeBinding() {
        return dmyTypeBinding;
    }

    public void setListTypeBinding(RichSelectOneChoice listTypeBinding) {
        this.listTypeBinding = listTypeBinding;
    }

    public RichSelectOneChoice getListTypeBinding() {
        return listTypeBinding;
    }

    public void ListItemVCL(ValueChangeEvent vce) {
        System.out.println("Inside List Item VCL");
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
//        String v=(String)listTypeBinding.getValue();
//        if(v=="Y")
        if(listTypeBinding.getValue().equals("Y"))
        {
            System.out.println("Perform Enabling...!");
                getMinEncashmentBinding().setDisabled(false);
                getMaxEncashmentBinding().setDisabled(false);
                System.out.println("Done1");
                          }
        else
        {
                getMinEncashmentBinding().setDisabled(true);
                getMaxEncashmentBinding().setDisabled(true);
            getMinEncashmentBinding().setValue(0);
            getMaxEncashmentBinding().setValue(0);
            
            }
        System.out.println("Done2");
    }
}
