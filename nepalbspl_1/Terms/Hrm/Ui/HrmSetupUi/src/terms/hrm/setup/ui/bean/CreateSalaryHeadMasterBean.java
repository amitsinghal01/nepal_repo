package terms.hrm.setup.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;
import javax.faces.validator.ValidatorException;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;

public class CreateSalaryHeadMasterBean {
    private RichPanelHeader getMyPageRootComponent;
    private RichOutputText getbindingOutputText;
    private RichButton headerEditBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText headCodeBinding;
    private RichButton saveBinding;
    private RichButton saveAndCloseBinding;
    private RichTable salaryDetailTableBinding;
    private RichSelectOneChoice netPercentageBinding;
    private RichInputComboboxListOfValues basketGlCodeBinding;
    private RichInputText glDescriptionBinding;
    private RichSelectOneChoice partOfBasketBinding;

    public CreateSalaryHeadMasterBean() {
    }

    public void saveButtonAL(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedBy("SalaryHeadMasterVO1Iterator","LastUpdatedBy");
        DCIteratorBinding dci=ADFUtils.getBindingIterator("SalaryHeadMasterDetailVO1Iterator");
        Row[] row=dci.getAllRowsInRange();
        System.out.println("Rows In detail table: "+row.length+" Net/Percentage Value: "+netPercentageBinding.getValue());
        
        if(row.length==0 && netPercentageBinding.getValue().equals("P")){
           ADFUtils.showMessage("Please enter row in detail table.", 0);
        }       
        else if(row.length>0 && netPercentageBinding.getValue().equals("N")){
            ADFUtils.showMessage("Please delete row in detail table.", 0);    
        }
        else{
        if (!headCodeBinding.isDisabled()) {
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message = new FacesMessage("Record Save Successfully");
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);
                } else {
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message = new FacesMessage("Record Update Successfully");
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);

                }
        }

    }

    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }

    public void cevModeDisableComponent(String mode) {

        if (mode.equals("C")) {

            getHeaderEditBinding().setDisabled(true);
            getSaveBinding().setDisabled(false);
            getSaveAndCloseBinding().setDisabled(false);
            getUnitCodeBinding().setDisabled(true);
            getGlDescriptionBinding().setDisabled(true);
            System.out.println(""+getPartOfBasketBinding().getValue());
            getBasketGlCodeBinding().setDisabled(true);           
            
        }

        if (mode.equals("E")) {

            getHeaderEditBinding().setDisabled(true);
            getHeadCodeBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
            getSaveBinding().setDisabled(false);
            getSaveAndCloseBinding().setDisabled(false);
            getGlDescriptionBinding().setDisabled(true);
         
            getBasketGlCodeBinding().setDisabled(true); 
        }

        if (mode.equals("V")) {
            getSaveBinding().setDisabled(true);
            getSaveAndCloseBinding().setDisabled(true);
        }


    }

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }


    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setGetbindingOutputText(RichOutputText getbindingOutputText) {
        this.getbindingOutputText = getbindingOutputText;
    }

    public RichOutputText getGetbindingOutputText() {

        cevmodecheck();
        return getbindingOutputText;
    }

    public void EditButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setHeadCodeBinding(RichInputText headCodeBinding) {
        this.headCodeBinding = headCodeBinding;
    }

    public RichInputText getHeadCodeBinding() {
        return headCodeBinding;
    }

    public void setSaveBinding(RichButton saveBinding) {
        this.saveBinding = saveBinding;
    }

    public RichButton getSaveBinding() {
        return saveBinding;
    }

    public void setSaveAndCloseBinding(RichButton saveAndCloseBinding) {
        this.saveAndCloseBinding = saveAndCloseBinding;
    }

    public RichButton getSaveAndCloseBinding() {
        return saveAndCloseBinding;
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                            {
                            ADFUtils.findOperation("Delete").execute();
                            System.out.println("Record Delete Successfully");
                                FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                                Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                                FacesContext fc = FacesContext.getCurrentInstance();  
                                fc.addMessage(null, Message);
                        }
                        AdfFacesContext.getCurrentInstance().addPartialTarget(salaryDetailTableBinding);
    }

    public void setSalaryDetailTableBinding(RichTable salaryDetailTableBinding) {
        this.salaryDetailTableBinding = salaryDetailTableBinding;
    }

    public RichTable getSalaryDetailTableBinding() {
        return salaryDetailTableBinding;
    }

    public void PercentagefValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if(object!=null){
        BigDecimal a=(BigDecimal)object;
        System.out.println("Number Value: "+a);
        if(a.compareTo(new BigDecimal(100))==1){
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Percentage should be less than 100.",null));
            }
        }
        

    }

    public String saveAndCloseAction() { 
        ADFUtils.setLastUpdatedBy("SalaryHeadMasterVO1Iterator","LastUpdatedBy");
            DCIteratorBinding dci=ADFUtils.getBindingIterator("SalaryHeadMasterDetailVO1Iterator");
            Row[] row=dci.getAllRowsInRange();
            System.out.println("Rows In detail table: "+row.length+" Net/Percentage Value: "+netPercentageBinding.getValue());
            
            if(row.length==0 && netPercentageBinding.getValue().equals("P")){
               ADFUtils.showMessage("Please enter row in detail table.", 0);
            }       
            if(row.length>0 && netPercentageBinding.getValue().equals("N")){
                ADFUtils.showMessage("Please delete row in detail table.", 0);    
            }
            else{
            if (!headCodeBinding.isDisabled()) {
                        ADFUtils.findOperation("Commit").execute();
                        FacesMessage Message = new FacesMessage("Record Save Successfully");
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);
                        FacesContext fc = FacesContext.getCurrentInstance();
                        fc.addMessage(null, Message);
                    } else {
                        ADFUtils.findOperation("Commit").execute();
                        FacesMessage Message = new FacesMessage("Record Update Successfully");
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);
                        FacesContext fc = FacesContext.getCurrentInstance();
                        fc.addMessage(null, Message);

                    }
                return "Save And Close";
            }
        return null;
    }

    public void setNetPercentageBinding(RichSelectOneChoice netPercentageBinding) {
        this.netPercentageBinding = netPercentageBinding;
    }

    public RichSelectOneChoice getNetPercentageBinding() {
        return netPercentageBinding;
    }

    public void setBasketGlCodeBinding(RichInputComboboxListOfValues basketGlCodeBinding) {
        this.basketGlCodeBinding = basketGlCodeBinding;
    }

    public RichInputComboboxListOfValues getBasketGlCodeBinding() {
        return basketGlCodeBinding;
    }

    public void setGlDescriptionBinding(RichInputText glDescriptionBinding) {
        this.glDescriptionBinding = glDescriptionBinding;
    }

    public RichInputText getGlDescriptionBinding() {
        return glDescriptionBinding;
    }

    public void setPartOfBasketBinding(RichSelectOneChoice partOfBasketBinding) {
        this.partOfBasketBinding = partOfBasketBinding;
    }

    public RichSelectOneChoice getPartOfBasketBinding() {
        return partOfBasketBinding;
    }

    public void partOfBasketVCL(ValueChangeEvent vc) {
        
        System.out.println("Basket Value"+vc.getNewValue());
        
        if(vc.getNewValue().equals("Y"))
        {
            System.out.println("Inside if");
            getBasketGlCodeBinding().setDisabled(false);
            }
        else
        {
                System.out.println("Inside else");
                getBasketGlCodeBinding().setDisabled(true); 
            }
    }
}
