package terms.hrm.setup.ui.bean;

import javax.faces.application.FacesMessage;

import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.adfinternal.view.faces.context.AdfFacesContextImpl;

import oracle.jbo.domain.Date;
import oracle.jbo.domain.Timestamp;


public class EmployeeLeaveMasterBean {
    private RichTable employeeLeaveMasterTableBinding;
    private String editAction = "V";
    private RichInputText empbinding;
    private RichInputText leaveTypebinding;
    private RichInputText finyearbinding;
    String val = "C";
    private RichButton deletepopupbinding;
    private RichPopup binddelete;
    private RichInputDate startDateBinding;
    private RichInputDate sstartDataBinding;

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public EmployeeLeaveMasterBean() {
    }

    public void setEmployeeLeaveMasterTableBinding(RichTable employeeLeaveMasterTableBinding) {
        this.employeeLeaveMasterTableBinding = employeeLeaveMasterTableBinding;
    }

    public RichTable getEmployeeLeaveMasterTableBinding() {
        return employeeLeaveMasterTableBinding;
    }

    public void popUpDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            oracle.binding.OperationBinding op = null;
            ADFUtils.findOperation("Delete").execute();
            op = (OperationBinding) ADFUtils.findOperation("Commit").execute();


            System.out.println("Record Delete Successfully");
            if (op != null) {
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            }
        }
        AdfFacesContextImpl.getCurrentInstance().addPartialTarget(employeeLeaveMasterTableBinding);
    }

    public void setEmpbinding(RichInputText empbinding) {
        this.empbinding = empbinding;
    }

    public RichInputText getEmpbinding() {
        return empbinding;
    }

    public void setLeaveTypebinding(RichInputText leaveTypebinding) {
        this.leaveTypebinding = leaveTypebinding;
    }

    public RichInputText getLeaveTypebinding() {
        return leaveTypebinding;
    }

    public void setFinyearbinding(RichInputText finyearbinding) {
        this.finyearbinding = finyearbinding;
    }

    public RichInputText getFinyearbinding() {
        return finyearbinding;
    }

    public void DeleteButton(ActionEvent actionEvent) {
        java.sql.Timestamp Date = (java.sql.Timestamp) sstartDataBinding.getValue();
        String  Date1= Date.toString().substring(0,4);        
        System.out.println("Date is: "+Date1);
        OperationBinding op1 = (OperationBinding) ADFUtils.findOperation("filterDelete");
        op1.getParamsMap().put("EmpmembEmpMasterEmpNumber", empbinding.getValue());
        op1.getParamsMap().put("LeaveDescLeaveDescType", leaveTypebinding.getValue());
        System.out.println("dgsvc" + leaveTypebinding.getValue());
        op1.getParamsMap().put("CurrYearLeave", Date1);
        op1.execute();
        System.out.println("bvfywfn" + op1);
        if (op1.getResult().equals("y")) {
            ADFUtils.showMessage("Record cannot be deleted. Child record found.", 0);
        } else {
            val = "c";
            if (val.equals("c")) {
                ADFUtils.showPopup(binddelete);

            }
        }

        System.out.println("Record Delete Successfully");
    }

    public String deletepopupbean() {


        return null;
    }

    public void setDeletepopupbinding(RichButton deletepopupbinding) {
        this.deletepopupbinding = deletepopupbinding;
    }

    public RichButton getDeletepopupbinding() {
        return deletepopupbinding;
    }

    public void setBinddelete(RichPopup binddelete) {
        this.binddelete = binddelete;
    }

    public RichPopup getBinddelete() {
        return binddelete;
    }

    public void editButton(ActionEvent actionEvent) {
        java.sql.Timestamp Date = (java.sql.Timestamp) sstartDataBinding.getValue();
        String  Date1= Date.toString().substring(0,4);        
        System.out.println("Date is: "+Date1);
        OperationBinding op1 = (OperationBinding) ADFUtils.findOperation("filterDelete");
        op1.getParamsMap().put("EmpmembEmpMasterEmpNumber", empbinding.getValue());
        op1.getParamsMap().put("LeaveDescLeaveDescType", leaveTypebinding.getValue());
        System.out.println("dgsvc" + leaveTypebinding.getValue());
        op1.getParamsMap().put("CurrYearLeave", Date1);
        op1.execute();
        System.out.println("bvfywfn" + op1);
        if (op1.getResult().equals("y")) {
            ADFUtils.showMessage("Record cannot be Edit. Child record found.", 0);
            editAction = "V";
        } else {
            val = "v";
            if (val.equals("v")) {
               
            }
        }

        System.out.println("Record Edit Successfully");
    }

    public void setStartDateBinding(RichInputDate startDateBinding) {
        this.startDateBinding = startDateBinding;
    }

    public RichInputDate getStartDateBinding() {
        return startDateBinding;
    }

    public void setSstartDataBinding(RichInputDate sstartDataBinding) {
        this.sstartDataBinding = sstartDataBinding;
    }

    public RichInputDate getSstartDataBinding() {
        return sstartDataBinding;
    }
}
