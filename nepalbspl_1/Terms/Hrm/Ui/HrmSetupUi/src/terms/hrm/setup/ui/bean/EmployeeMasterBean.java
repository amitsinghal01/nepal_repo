package terms.hrm.setup.ui.bean;

import java.awt.image.BufferedImage;

import java.io.File;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.text.DateFormat;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;

import java.util.Calendar;
import java.util.Date;

import java.util.GregorianCalendar;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.application.ViewHandler;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import javax.faces.validator.ValidatorException;

import javax.imageio.ImageIO;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputFile;

import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectBooleanCheckbox;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.JboException;
import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;

import oracle.jbo.ValidationException;
import oracle.jbo.domain.Timestamp;

import org.apache.myfaces.trinidad.event.DisclosureEvent;
import org.apache.myfaces.trinidad.model.UploadedFile;

public class EmployeeMasterBean {
    private RichInputFile uploadFileBind;
    private RichOutputText bindingOutputText;
    private RichButton headerEditBinding;
    private RichPanelGroupLayout myPageRootComponent1;
    private RichPanelGroupLayout myPageRootComponent2;
    private RichOutputText bindingOutputText2;
    private RichInputText perStateBinding;
    private RichInputText preStateBinding;
    private RichInputText designationDescBinding;
    private RichInputText levelBinding;
    private RichInputText categoryBinding;
    private RichInputText deptDescBinding;
    private RichInputText bankNameBinding;
    private RichInputText bankAdd1Binding;
    private RichInputText bankAdd2Binding;
    private RichInputText nationNameBinding;
    private RichInputDate resigDateBinding;
    private RichInputDate dateOfMrgBinding;
    private RichInputDate relievingDateBinding;
    private RichSelectOneChoice pfRelievingReasonBinding;
    private RichSelectOneChoice esiRelievingReasonBinding;
    private RichInputText empNumberBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText unitNameBinding;
    private RichSelectOneChoice schoolBusBinding;
    private RichInputText schoolBusRateBinding;
    private RichSelectOneChoice apfBinding;
    private RichInputText apfPercentageBinding;
    private RichInputComboboxListOfValues unitCodeDetailBinding;
    private RichInputText employeeCodeDetailBinding;
    private RichInputText subCodeDetailBinding;
    private RichInputText headSubCodeDetailBinding;
    private String editAction = "V";
    private RichTable employeeMasterDetailBinding;
    private RichTable employeeQualificationTableBinding;
    private RichTable employeeDependentTableBinding;
    private RichSelectOneChoice superannuationBinding;
    private RichSelectOneChoice basketBinding;
    String StatusEmp="F",status="F";
    private RichInputComboboxListOfValues levelCodeBinding;
    private RichInputComboboxListOfValues categoryCodeBinding;
    private RichInputDate yearFromBinding;
    private RichInputDate yearToBinding;
    private RichOutputText bindingOutputText3;
    private RichPanelGroupLayout myPageRootComponent3;
    private RichInputComboboxListOfValues desigCodeBinding;
    private RichInputComboboxListOfValues locationBinding;
    private RichInputText locationNameBinding;
    private RichInputText reportingNameBinding;
    private RichOutputText bindingOutputText4;
    private RichPanelGroupLayout myPageRootComponent4;
    private RichSelectOneChoice statusBinding;
    private RichInputText pfNumberBinding;
    private RichInputDate pfMembershipDateBinding;
    private RichSelectBooleanCheckbox pfApplicableBinding;
    private RichInputText esiNumberBinding;
    private RichInputDate esiMembershipDateBinding;
    private RichInputText cardNoBinding;
    private RichSelectBooleanCheckbox esiApplicableBinding;
    private RichInputComboboxListOfValues approveByCodeBinding;
    private RichInputDate approveByDateBinding;
    private RichInputText approveByNameBinding;
    private RichButton saveButtonBinding;
    private RichShowDetailItem empInfoTabBinding;
    private RichPanelHeader myPageRootComponent;
    private RichSelectOneChoice empTypeBinding;

    public EmployeeMasterBean() {
//        File dir = new File("/tmp/pht");
//        File savedir = new File("/home/beta4/pics");
//        if (!dir.exists()) {
//            try {
//                dir.mkdir();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//        if (!savedir.exists()) {
//            try {
//                savedir.mkdir();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
    }

    public void setUploadFileBind(RichInputFile uploadFileBind) {
        this.uploadFileBind = uploadFileBind;
    }

    public RichInputFile getUploadFileBind() {
        return uploadFileBind;
    }

    InputStream inputStream = null;
    InputStream SaveInputStream = null;
    private UploadedFile PhotoFile = null;
    private String AbsoluteFilePath = "";
//    String imagePath = "";
    Boolean check = false;
    private UploadedFile imageFile;

        public void setImageFile(UploadedFile imageFile) {
            this.imageFile = imageFile;
        }

        public UploadedFile getImageFile() {
            return imageFile;
        }
    //To Store path of uploaded Image file
    String imagePath = null;

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void PhotoFileVCL(ValueChangeEvent vce) {
//        if (vce.getNewValue() != null) {
//            setPhotoFile((UploadedFile) vce.getNewValue());
//            UploadedFile myfile = (UploadedFile) vce.getNewValue();
//            String path = null;
//
//            if (myfile == null) {
//
//            } else {
//                File dir = new File("/tmp/pht");
//                File savedir = new File("/home/beta4/pics");
//                if (!dir.exists()) {
//                    try {
//                        dir.mkdir();
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//                if (!savedir.exists()) {
//                    try {
//                        savedir.mkdir();
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//
//                // All uploaded files will be stored in below path
//                path = "/home/beta4/pics/" + myfile.getFilename();
//                //                Imagepath=path;
//                File f = new File(path);
//                if (!f.exists()) {
//                    try {
//                        f.createNewFile();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }
//                inputStream = null;
//                try {
//
//                    inputStream = myfile.getInputStream();
//                    SaveInputStream = myfile.getInputStream();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                check = true;
//                //                System.out.println("check in bean" + check);
//                Imagepath = uploadFile(PhotoFile, "/tmp/pht/" + PhotoFile.getFilename());
//
//            }
//        }
    }
//    String Imagepath = "";

    public void SaveAL(ActionEvent actionEvent) {
        
        levelCodeBinding.setDisabled(true);
        AdfFacesContext adfFacesContext = AdfFacesContext.getCurrentInstance();
        adfFacesContext.addPartialTarget(levelCodeBinding);
        
        ADFUtils.setLastUpdatedBy("EmployeeMasterVO1Iterator","LastUpdatedBy");
            if(status.equalsIgnoreCase("T"))
            {
                
                System.out.println("In if T");
                ADFUtils.showMessage("Total Pf Amount Share cannot be greater than 100.", 0);
            }else
            {
            System.out.println("In else F");
        check = false;
        String path;
                
           
//        if (getPhotoFile() != null) {
//            path = "/home/beta4/pics/" + PhotoFile.getFilename();
//            Imagepath = SaveuploadFile(PhotoFile, path);
//            //        System.out.println("path " + Imagepath);
//            OperationBinding ob = ADFUtils.findOperation("AddImagePath");
//            ob.getParamsMap().put("ImagePath", Imagepath);
//            ob.execute();
//        }
//
//
//        File directory = new File("/tmp/pht");
//        //get all the files from a directory
//        File[] fList = directory.listFiles();
//        for (File file : fList) {
//            //Delete all previously uploaded files
//            // if (!"NoImage.png".equalsIgnoreCase(file.getName())) {
//            file.delete();
//            //}
//        }
       


        System.out.println("outside error block:-> "+StatusEmp);
        if(StatusEmp.equalsIgnoreCase("T"))
        {
            
            System.out.println("In else FF");
            System.out.println("IN get error block"+ADFUtils.resolveExpression("#{pageFlowScope.mode}"));
            //if (empNumberBinding.getValue()==null) {
//                OperationBinding op=ADFUtils.findOperation("generateEmployeeCode");
//                op.execute();
                
//                String Res =(String)op.getResult();
//                System.out.println("Value of the Res is----> "+Res);
                
            
                System.out.println("Emp Header Value"+empNumberBinding.getValue());
                
                String EmpVal=(String) empNumberBinding.getValue();
                
                System.out.println("Emp Header Value"+EmpVal);
                
                System.out.println("Emp Detail Value"+employeeCodeDetailBinding.getValue());
                //employeeCodeDetailBinding.getValue();
                if(EmpVal !=null)
                              {
                employeeCodeDetailBinding.setValue(EmpVal);
              System.out.println("Emp Detail Value 2-->"+employeeCodeDetailBinding.getValue());         

            // if(employeeCodeDetailBinding.getValue() != null)
            // {
                OperationBinding opr = ADFUtils.findOperation("Commit");
                Object obj = opr.execute();
                ADFUtils.findOperation("Commit");
                System.out.println("S####");
                FacesMessage Message = new FacesMessage("Record Save Successfully. New Employee Code is "+empNumberBinding.getValue());
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            // }
                              }
            //} 
            else {
                System.out.println("S####CCC");
                ADFUtils.findOperation("Commit").execute();
                FacesMessage Message = new FacesMessage("Record Update Successfully");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            }
        }
        else
        {
            if (!empNumberBinding.isDisabled()) {
                ADFUtils.findOperation("Commit").execute();
                System.out.println("S####");
                FacesMessage Message = new FacesMessage("Record Save Successfully. New Employee Code is "+empNumberBinding.getValue());
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);

            } else {
                System.out.println("S####SS");
                ADFUtils.findOperation("Commit").execute();
                FacesMessage Message = new FacesMessage("Record Update Successfully");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            }
        }
        }
    }

    public void setCheck(Boolean check) {
        this.check = check;
    }

    public Boolean getCheck() {
        return check;
    }

    public void setPhotoFile(UploadedFile PhotoFile) {
        this.PhotoFile = PhotoFile;
    }

    public UploadedFile getPhotoFile() {
        return PhotoFile;
    }

    private String uploadFile(UploadedFile file, String path) {

        UploadedFile myfile = file;

        try {

            FileOutputStream out = new FileOutputStream(path);
            byte[] buffer = new byte[8192];
            int bytesRead = 0;
            while ((bytesRead = inputStream.read(buffer, 0, 8192)) != -1) {
                out.write(buffer, 0, bytesRead);
            }
            out.flush();
            out.close();
        } catch (Exception ex) {
            // handle exception
            ex.printStackTrace();
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
            }
        }


        //Returns the path where file is stored
        return path;
    }

    private String SaveuploadFile(UploadedFile file, String path) {

        UploadedFile myfile = file;

        try {

            FileOutputStream out = new FileOutputStream(path);
            byte[] buffer = new byte[8192];
            int bytesRead = 0;
            while ((bytesRead = SaveInputStream.read(buffer, 0, 8192)) != -1) {
                out.write(buffer, 0, bytesRead);
            }
            out.flush();
            out.close();
        } catch (Exception ex) {
            // handle exception
            ex.printStackTrace();
        } finally {
            try {
                SaveInputStream.close();
            } catch (IOException e) {
            }
        }


        //Returns the path where file is stored
        return path;
    }


//    public void setImagepath(String Imagepath) {
//        this.Imagepath = Imagepath;
//    }
//
//    public String getImagepath() {
//        return Imagepath;
//    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getMyPageRootComponent1(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getMyPageRootComponent1(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getMyPageRootComponent1(), false);
            cevModeDisableComponent("E");
        }
    }

    private void cevmodecheck2() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getMyPageRootComponent2(), true);
            cevModeDisableComponent2("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getMyPageRootComponent2(), false);
            cevModeDisableComponent2("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getMyPageRootComponent2(), false);
            cevModeDisableComponent2("E");
        }
    }
    
    private void cevmodecheck3() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getMyPageRootComponent3(), true);
            cevModeDisableComponent3("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getMyPageRootComponent3(), false);
            cevModeDisableComponent3("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getMyPageRootComponent3(), false);
            cevModeDisableComponent3("E");
        }
    }
    
    
    private void cevmodecheck4() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getMyPageRootComponent4(), true);
            cevModeDisableComponent4("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getMyPageRootComponent4(), false);
            cevModeDisableComponent4("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getMyPageRootComponent4(), false);
            cevModeDisableComponent4("E");
        }
    }

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }

    public void cevModeDisableComponent(String mode) {

        if (mode.equals("E")) {
            getHeaderEditBinding().setDisabled(true);
            getPerStateBinding().setDisabled(true);
            getPreStateBinding().setDisabled(true);
            getNationNameBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
            getEmpNumberBinding().setDisabled(true);
            getEmpTypeBinding().setDisabled(true);
            getCardNoBinding().setDisabled(false);
//            getResigDateBinding().setDisabled(true);
//            getRelievingDateBinding().setDisabled(true);
            getUnitNameBinding().setDisabled(true);
            getLevelCodeBinding().setDisabled(false);
            System.out.println("PF Applicable"+pfNumberBinding.getValue());
            //            getDesignationDescBinding().setDisabled(true);
            //            getLevelBinding().setDisabled(true);
            //            getCategoryBinding().setDisabled(true);
            //            getDeptDescBinding().setDisabled(true);
            //            getBankNameBinding().setDisabled(true);
            //            getBankAdd1Binding().setDisabled(true);
            //            getBankAdd2Binding().setDisabled(true);
            //            getApfPercentageBinding().setDisabled(true);
            //            getSchoolBusRateBinding().setDisabled(true);

            //            getUnitCodeDetailBinding().setDisabled(true);
            //            getEmployeeCodeDetailBinding().setDisabled(true);
            getApproveByCodeBinding().setDisabled(false); 
            getApproveByDateBinding().setDisabled(true);
            getApproveByNameBinding().setDisabled(true);

        } else if (mode.equals("C")) {
            getUnitCodeBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            getPerStateBinding().setDisabled(true);
            getPreStateBinding().setDisabled(true);
            getNationNameBinding().setDisabled(true);
            getCardNoBinding().setDisabled(false);
//            getResigDateBinding().setDisabled(true);
//            getRelievingDateBinding().setDisabled(true);
            getApproveByCodeBinding().setDisabled(true);
            getApproveByDateBinding().setDisabled(true);
            getApproveByNameBinding().setDisabled(true);
            if(StatusEmp.equalsIgnoreCase("T"))
            {
                System.out.println("AAAAAA");
                empNumberBinding.setDisabled(true);
            }else
            {
                System.out.println("BBBBBB");
                empNumberBinding.setDisabled(false);
            }
            OperationBinding op= (OperationBinding)ADFUtils.findOperation("checkParameterEmp");
            op.execute();
            if(op.getResult()!=null && op.getResult().equals("A"))
            {
                empNumberBinding.setDisabled(true);
            }else
            {
                empNumberBinding.setDisabled(false);
            }
//            if(getCategoryBinding().getValue() != null && getDesigCodeBinding().getValue()!=null)
//            { 
//                System.out.println("Inside If to check Category and designation binding");
//            getLevelCodeBinding().setDisabled(true);
//            }
            
            
//            if(getEmpNumberBinding().getValue() != null)
//            {      
//            getLevelCodeBinding().setDisabled(true);
//            }
            //            getDesignationDescBinding().setDisabled(true);
            //            getLevelBinding().setDisabled(true);
            //            getCategoryBinding().setDisabled(true);
            //            getDeptDescBinding().setDisabled(true);
            //            getBankNameBinding().setDisabled(true);
            //            getBankAdd1Binding().setDisabled(true);
            //            getBankAdd2Binding().setDisabled(true);


            //            getUnitCodeDetailBinding().setDisabled(true);
            //            getEmployeeCodeDetailBinding().setDisabled(true);
            //            getHeadSubCodeDetailBinding().setDisabled(true);
            //            getSubCodeDetailBinding().setDisabled(true);
            //            getUnitCodeDetailBinding().setDisabled(true);
            //            getEmployeeCodeDetailBinding().setDisabled(true);
        } else if (mode.equals("V")) {

        }

    }

    public void cevModeDisableComponent2(String mode) {

        if (mode.equals("E")) {
            getHeaderEditBinding().setDisabled(true);
            //            getPerStateBinding().setDisabled(true);
            //            getPreStateBinding().setDisabled(true);
            getDesignationDescBinding().setDisabled(true);
            getLevelBinding().setDisabled(false);
            getCategoryBinding().setDisabled(true);
            getDeptDescBinding().setDisabled(true);
            getBankNameBinding().setDisabled(true);
            getBankAdd1Binding().setDisabled(true);
            getBankAdd2Binding().setDisabled(true);
            //getLocationBinding().setDisabled(true);
            getCategoryCodeBinding().setDisabled(true);
            getReportingNameBinding().setDisabled(true);
        } else if (mode.equals("C")) {
            getHeaderEditBinding().setDisabled(true);
            //            getPerStateBinding().setDisabled(true);
            //            getPreStateBinding().setDisabled(true);
            getDesignationDescBinding().setDisabled(true);
            getLevelBinding().setDisabled(true);
            getCategoryBinding().setDisabled(true);
            getDeptDescBinding().setDisabled(true);
            getBankNameBinding().setDisabled(true);
            getBankAdd1Binding().setDisabled(true);
            getBankAdd2Binding().setDisabled(true);
           // getLocationBinding().setDisabled(true);
          //  getPfRelievingReasonBinding().setDisabled(true);
           // getEsiRelievingReasonBinding().setDisabled(true);
            getCategoryCodeBinding().setDisabled(true);
            getReportingNameBinding().setDisabled(true);
           // getApfPercentageBinding().setDisabled(true);
            //getSchoolBusRateBinding().setDisabled(true);

        } else if (mode.equals("V")) {

        }

    }
    
    
    public void cevModeDisableComponent3(String mode) {

        if (mode.equals("E")) {
            getHeaderEditBinding().setDisabled(true);
            //            getPerStateBinding().setDisabled(true);
            //            getPreStateBinding().setDisabled(true);
//            getDesignationDescBinding().setDisabled(true);
//            getLevelBinding().setDisabled(true);
//            getCategoryBinding().setDisabled(true);
//            getDeptDescBinding().setDisabled(true);
//            getBankNameBinding().setDisabled(true);
//            getBankAdd1Binding().setDisabled(true);
//            getBankAdd2Binding().setDisabled(true);
//            getLocationBinding().setDisabled(true);
        } else if (mode.equals("C")) {
            getHeaderEditBinding().setDisabled(true);
            //            getPerStateBinding().setDisabled(true);
            //            getPreStateBinding().setDisabled(true);
//            getDesignationDescBinding().setDisabled(true);
//            getLevelBinding().setDisabled(true);
//            getCategoryBinding().setDisabled(true);
//            getDeptDescBinding().setDisabled(true);
//            getBankNameBinding().setDisabled(true);
//            getBankAdd1Binding().setDisabled(true);
//            getBankAdd2Binding().setDisabled(true);
//            getLocationBinding().setDisabled(true);
//            getPfRelievingReasonBinding().setDisabled(true);
//            getEsiRelievingReasonBinding().setDisabled(true);

            getApfPercentageBinding().setDisabled(true);
            getSchoolBusRateBinding().setDisabled(true);

        } else if (mode.equals("V")) {

        }

    }
    
    
    public void cevModeDisableComponent4(String mode) {

        if (mode.equals("E")) {
            getHeaderEditBinding().setDisabled(true);
            if(pfApplicableBinding.getValue().equals(true))
            {
                 pfNumberBinding.setDisabled(false);
                 pfMembershipDateBinding.setDisabled(false);
                 pfRelievingReasonBinding.setDisabled(false);
            }else
            {
                 pfNumberBinding.setDisabled(true);
                 pfMembershipDateBinding.setDisabled(true);
                 pfRelievingReasonBinding.setDisabled(true);
            }
            if(esiApplicableBinding.getValue().equals(true))
            {
                esiNumberBinding.setDisabled(false);
                esiMembershipDateBinding.setDisabled(false);
                esiRelievingReasonBinding.setDisabled(false); 
            }
            else
            {
                esiNumberBinding.setDisabled(true);
                esiMembershipDateBinding.setDisabled(true);
                esiRelievingReasonBinding.setDisabled(true);
            }
            //            getPerStateBinding().setDisabled(true);
            //            getPreStateBinding().setDisabled(true);
    //            getDesignationDescBinding().setDisabled(true);
    //            getLevelBinding().setDisabled(true);
    //            getCategoryBinding().setDisabled(true);
    //            getDeptDescBinding().setDisabled(true);
    //            getBankNameBinding().setDisabled(true);
    //            getBankAdd1Binding().setDisabled(true);
    //            getBankAdd2Binding().setDisabled(true);
    //            getLocationBinding().setDisabled(true);
        } else if (mode.equals("C")) {
            getHeaderEditBinding().setDisabled(true);
            if(pfApplicableBinding.getValue().equals(true))
            {
                 pfNumberBinding.setDisabled(false);
                 pfMembershipDateBinding.setDisabled(false);
                 pfRelievingReasonBinding.setDisabled(false);
            }else
            {
                 pfNumberBinding.setDisabled(true);
                 pfMembershipDateBinding.setDisabled(true);
                 pfRelievingReasonBinding.setDisabled(true);
            }
            if(esiApplicableBinding.getValue().equals(true))
            {
                esiNumberBinding.setDisabled(false);
                esiMembershipDateBinding.setDisabled(false);
                esiRelievingReasonBinding.setDisabled(false); 
            }
            else
            {
                esiNumberBinding.setDisabled(true);
                esiMembershipDateBinding.setDisabled(true);
                esiRelievingReasonBinding.setDisabled(true);
            }
            //            getPerStateBinding().setDisabled(true);
            //            getPreStateBinding().setDisabled(true);
    //            getDesignationDescBinding().setDisabled(true);
    //            getLevelBinding().setDisabled(true);
    //            getCategoryBinding().setDisabled(true);
    //            getDeptDescBinding().setDisabled(true);
    //            getBankNameBinding().setDisabled(true);
    //            getBankAdd1Binding().setDisabled(true);
    //            getBankAdd2Binding().setDisabled(true);
    //            getLocationBinding().setDisabled(true);
                getPfRelievingReasonBinding().setDisabled(true);
                getEsiRelievingReasonBinding().setDisabled(true);
//
//            getApfPercentageBinding().setDisabled(true);
//            getSchoolBusRateBinding().setDisabled(true);

        } else if (mode.equals("V")) {

        }

    }

    public void editButtonAL(ActionEvent actionEvent) {
        
//        if (approveByCodeBinding.getValue() != null) {
//                      ADFUtils.showMessage("Data cannot be modified after Approval.", 2);
//                      ADFUtils.setEL("#{pageFlowScope.mode}", "V");
//                  
//              } else {
//                  cevmodecheck();
//              }
        String unitCd=(String) getUnitCodeBinding().getValue();
        OperationBinding op= (OperationBinding)ADFUtils.findOperation("checkParameterEmployeeApprovrForEditMode");
        op.getParamsMap().put("UnitCode", unitCd);
        op.execute();
       
        if(op.getResult()!=null && op.getResult().equals("N") && approveByCodeBinding.getValue() != null)
        {
            System.out.println("Not Approval");
            ADFUtils.showMessage("Data cannot be modified after Approval.", 2);
            ADFUtils.setEL("#{pageFlowScope.mode}", "V");
            
        }else
        {
           
            System.out.println("Approval");
            cevmodecheck();
            cevmodecheck2();
            cevmodecheck3();
            cevmodecheck4();
        }       
    }

    public void setMyPageRootComponent1(RichPanelGroupLayout myPageRootComponent1) {
        this.myPageRootComponent1 = myPageRootComponent1;
    }

    public RichPanelGroupLayout getMyPageRootComponent1() {
        return myPageRootComponent1;
    }

    public void setMyPageRootComponent2(RichPanelGroupLayout myPageRootComponent2) {
        this.myPageRootComponent2 = myPageRootComponent2;
    }

    public RichPanelGroupLayout getMyPageRootComponent2() {
        return myPageRootComponent2;
    }

    public void setBindingOutputText2(RichOutputText bindingOutputText2) {
        this.bindingOutputText2 = bindingOutputText2;
    }

    public RichOutputText getBindingOutputText2() {
        cevmodecheck2();
        return bindingOutputText2;
    }

    public void setPerStateBinding(RichInputText perStateBinding) {
        this.perStateBinding = perStateBinding;
    }

    public RichInputText getPerStateBinding() {
        return perStateBinding;
    }

    public void setPreStateBinding(RichInputText preStateBinding) {
        this.preStateBinding = preStateBinding;
    }

    public RichInputText getPreStateBinding() {
        return preStateBinding;
    }

    public void setDesignationDescBinding(RichInputText designationDescBinding) {
        this.designationDescBinding = designationDescBinding;
    }

    public RichInputText getDesignationDescBinding() {
        return designationDescBinding;
    }

    public void setLevelBinding(RichInputText levelBinding) {
        this.levelBinding = levelBinding;
    }

    public RichInputText getLevelBinding() {
        return levelBinding;
    }

    public void setCategoryBinding(RichInputText categoryBinding) {
        this.categoryBinding = categoryBinding;
    }

    public RichInputText getCategoryBinding() {
        return categoryBinding;
    }

    public void setDeptDescBinding(RichInputText deptDescBinding) {
        this.deptDescBinding = deptDescBinding;
    }

    public RichInputText getDeptDescBinding() {
        return deptDescBinding;
    }

    public void setBankNameBinding(RichInputText bankNameBinding) {
        this.bankNameBinding = bankNameBinding;
    }

    public RichInputText getBankNameBinding() {
        return bankNameBinding;
    }

    public void setBankAdd1Binding(RichInputText bankAdd1Binding) {
        this.bankAdd1Binding = bankAdd1Binding;
    }

    public RichInputText getBankAdd1Binding() {
        return bankAdd1Binding;
    }

    public void setBankAdd2Binding(RichInputText bankAdd2Binding) {
        this.bankAdd2Binding = bankAdd2Binding;
    }

    public RichInputText getBankAdd2Binding() {
        return bankAdd2Binding;
    }

    public void setNationNameBinding(RichInputText nationNameBinding) {
        this.nationNameBinding = nationNameBinding;
    }

    public RichInputText getNationNameBinding() {
        return nationNameBinding;
    }

    public void setResigDateBinding(RichInputDate resigDateBinding) {
        this.resigDateBinding = resigDateBinding;
    }

    public RichInputDate getResigDateBinding() {
        return resigDateBinding;
    }

    public void setDateOfMrgBinding(RichInputDate dateOfMrgBinding) {
        this.dateOfMrgBinding = dateOfMrgBinding;
    }

    public RichInputDate getDateOfMrgBinding() {
        return dateOfMrgBinding;
    }

    public void setRelievingDateBinding(RichInputDate relievingDateBinding) {
        this.relievingDateBinding = relievingDateBinding;
    }

    public RichInputDate getRelievingDateBinding() {
        return relievingDateBinding;
    }

    public void setPfRelievingReasonBinding(RichSelectOneChoice pfRelievingReasonBinding) {
        this.pfRelievingReasonBinding = pfRelievingReasonBinding;
    }

    public RichSelectOneChoice getPfRelievingReasonBinding() {
        return pfRelievingReasonBinding;
    }

    public void setEsiRelievingReasonBinding(RichSelectOneChoice esiRelievingReasonBinding) {
        this.esiRelievingReasonBinding = esiRelievingReasonBinding;
    }

    public RichSelectOneChoice getEsiRelievingReasonBinding() {
        return esiRelievingReasonBinding;
    }

    public void maritalStatusVCL(ValueChangeEvent valueChangeEvent) {
        if (valueChangeEvent.getNewValue() != null) {
            System.out.println("maritial status:" + valueChangeEvent.getNewValue());
            if (valueChangeEvent.getNewValue().toString().equalsIgnoreCase("S")) {
                getDateOfMrgBinding().setDisabled(true);
            } else {
                getDateOfMrgBinding().setDisabled(false);
            }
        }
    }

    public void setEmpNumberBinding(RichInputText empNumberBinding) {
        this.empNumberBinding = empNumberBinding;
    }

    public RichInputText getEmpNumberBinding() {
        return empNumberBinding;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setUnitNameBinding(RichInputText unitNameBinding) {
        this.unitNameBinding = unitNameBinding;
    }

    public RichInputText getUnitNameBinding() {
        return unitNameBinding;
    }

    public void setSchoolBusBinding(RichSelectOneChoice schoolBusBinding) {
        this.schoolBusBinding = schoolBusBinding;
    }

    public RichSelectOneChoice getSchoolBusBinding() {
        return schoolBusBinding;
    }

    public void setSchoolBusRateBinding(RichInputText schoolBusRateBinding) {
        this.schoolBusRateBinding = schoolBusRateBinding;
    }

    public RichInputText getSchoolBusRateBinding() {
        return schoolBusRateBinding;
    }

    public void setApfBinding(RichSelectOneChoice apfBinding) {
        this.apfBinding = apfBinding;
    }

    public RichSelectOneChoice getApfBinding() {
        return apfBinding;
    }

    public void setApfPercentageBinding(RichInputText apfPercentageBinding) {
        this.apfPercentageBinding = apfPercentageBinding;
    }

    public RichInputText getApfPercentageBinding() {
        return apfPercentageBinding;
    }

    public void schoolBusVCL(ValueChangeEvent valueChangeEvent) {
        System.out.println("School Bus Statue" + valueChangeEvent.getNewValue());
        if (valueChangeEvent.getNewValue() == null) {
            getSchoolBusRateBinding().setDisabled(true);
        } else if (valueChangeEvent.getNewValue().toString().equalsIgnoreCase("N")) {
            getSchoolBusRateBinding().setDisabled(true);
        } else {
            getSchoolBusRateBinding().setDisabled(false);
        }

    }

    public void apfVCL(ValueChangeEvent valueChangeEvent) {
        System.out.println("APF Statue" + valueChangeEvent.getNewValue());
        if (valueChangeEvent.getNewValue() == null) {
            getApfPercentageBinding().setDisabled(true);
        } else if (valueChangeEvent.getNewValue().toString().equalsIgnoreCase("N")) {
            getApfPercentageBinding().setDisabled(true);
        } else {
            getApfPercentageBinding().setDisabled(false);
        }
    }

    public void setUnitCodeDetailBinding(RichInputComboboxListOfValues unitCodeDetailBinding) {
        this.unitCodeDetailBinding = unitCodeDetailBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeDetailBinding() {
        return unitCodeDetailBinding;
    }

    public void setEmployeeCodeDetailBinding(RichInputText employeeCodeDetailBinding) {
        this.employeeCodeDetailBinding = employeeCodeDetailBinding;
    }

    public RichInputText getEmployeeCodeDetailBinding() {
        return employeeCodeDetailBinding;
    }

    public void setSubCodeDetailBinding(RichInputText subCodeDetailBinding) {
        this.subCodeDetailBinding = subCodeDetailBinding;
    }

    public RichInputText getSubCodeDetailBinding() {
        return subCodeDetailBinding;
    }

    public void setHeadSubCodeDetailBinding(RichInputText headSubCodeDetailBinding) {
        this.headSubCodeDetailBinding = headSubCodeDetailBinding;
    }

    public RichInputText getHeadSubCodeDetailBinding() {
        return headSubCodeDetailBinding;
    }

    public void popUpDialogDetailDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            OperationBinding op = null;
            ADFUtils.findOperation("Delete").execute();
            op = (OperationBinding) ADFUtils.findOperation("Commit");
            if (getEditAction().equals("V")) {
                Object rst = op.execute();
            }
            System.out.println("Record Delete Successfully");
            if (op.getErrors().isEmpty()) {
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            } else if (!op.getErrors().isEmpty()) {
                OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
                Object rstr = opr.execute();
                FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(employeeMasterDetailBinding);
    }

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public void setEmployeeMasterDetailBinding(RichTable employeeMasterDetailBinding) {
        this.employeeMasterDetailBinding = employeeMasterDetailBinding;
    }

    public RichTable getEmployeeMasterDetailBinding() {
        return employeeMasterDetailBinding;
    }


    public void setEmployeeQualificationTableBinding(RichTable employeeQualificationTableBinding) {
        this.employeeQualificationTableBinding = employeeQualificationTableBinding;
    }

    public RichTable getEmployeeQualificationTableBinding() {
        return employeeQualificationTableBinding;
    }

    public void setEmployeeDependentTableBinding(RichTable employeeDependentTableBinding) {
        this.employeeDependentTableBinding = employeeDependentTableBinding;
    }

    public RichTable getEmployeeDependentTableBinding() {
        return employeeDependentTableBinding;
    }

    public void popUpDialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
        {
        OperationBinding op = null;
        ADFUtils.findOperation("Delete2").execute();
        op = (OperationBinding) ADFUtils.findOperation("Commit2");
        if (getEditAction().equals("V")) {
        Object rst = op.execute();
        }
        System.out.println("Record Delete Successfully");
        if(op.getErrors().isEmpty()){
        FacesMessage Message = new FacesMessage("Record Deleted Successfully");
        Message.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        } else if (!op.getErrors().isEmpty())
        {
        OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
        Object rstr = opr.execute();
        FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
        Message.setSeverity(FacesMessage.SEVERITY_ERROR);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(employeeDependentTableBinding);
    }

    public void popUpDialogQualDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
        {
        OperationBinding op = null;
        ADFUtils.findOperation("Delete1").execute();
        op = (OperationBinding) ADFUtils.findOperation("Commit1");
        if (getEditAction().equals("V")) {
        Object rst = op.execute();
        }
        System.out.println("Record Delete Successfully");
        if(op.getErrors().isEmpty()){
        FacesMessage Message = new FacesMessage("Record Deleted Successfully");
        Message.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        } else if (!op.getErrors().isEmpty())
        {
        OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
        Object rstr = opr.execute();
        FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
        Message.setSeverity(FacesMessage.SEVERITY_ERROR);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(employeeQualificationTableBinding);
    }



    public void setSuperannuationBinding(RichSelectOneChoice superannuationBinding) {
        this.superannuationBinding = superannuationBinding;
    }

    public RichSelectOneChoice getSuperannuationBinding() {
        return superannuationBinding;
    }

    public void setBasketBinding(RichSelectOneChoice basketBinding) {
        this.basketBinding = basketBinding;
    }

    public RichSelectOneChoice getBasketBinding() {
        return basketBinding;
    }
    
    public void functionReturn()
    {
        String Value="";
//        OperationBinding opr = ADFUtils.findOperation("CreateInsert3");
//        Object obj = opr.execute();
//        System.out.println("After Create Insert");
System.out.println("After Calling Check Function");
//        OperationBinding op=ADFUtils.findOperation("EmployeeMasterAutoGenCheck");
//        op.execute();       
//        System.out.println("After Calling Check Function");
        
//        Value=op.getResult().toString();
           Value="A";

        System.out.println("Value in bean---->>"+Value);
//        if(Value.substring(2).equalsIgnoreCase("A"))
        if(Value.equalsIgnoreCase("A"))
        {
            System.out.println("AAAAAA");
//            empNumberBinding.setDisabled(true);
            StatusEmp="T";
        }else
        {
            System.out.println("BBBBBB");
//            empNumberBinding.setDisabled(false);
            StatusEmp="F";
        }
 
        
    }

    public void populateDataAL(ActionEvent actionEvent) {
        String CatVal=(String)getCategoryCodeBinding().getValue();
        System.out.println("Category Value"+CatVal);
       OperationBinding op=ADFUtils.findOperation("populateEarnDedDataForEmpMaster");
        op.getParamsMap().put("CatVal", CatVal);
        op.execute();
    }

    public void levelCodeVCL(ValueChangeEvent vce) {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
           
        } else {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        String Lvl=(String)vce.getNewValue();
        System.out.println("Level Value in Bean"+Lvl);
        System.out.println("Calling Autogenerate Method For Employee Code!");
        OperationBinding op=ADFUtils.findOperation("generateEmployeeCode");
        op.getParamsMap().put("Lvl", Lvl);
        op.execute();
        System.out.println("Value of Employee code: "+empNumberBinding.getValue());
        if(empNumberBinding.getValue()!=null)
        getEmpTypeBinding().setDisabled(true);
        //generated card number
        String empVal=(String)empNumberBinding.getValue();
        String unit=(String)unitCodeBinding.getValue();
        System.out.println("employee and unit Value new -->"+empVal+unit);
        OperationBinding op1=ADFUtils.findOperation("generateCardNoForEmployee");
        op1.getParamsMap().put("empVal", empVal);
        op1.getParamsMap().put("unit", unit);
        op1.execute();
        //end
        System.out.println("Inside Bean:Back From method!");
        System.out.println("Category Code: "+getCategoryBinding().getValue());
        System.out.println("Designation Code: "+getDesigCodeBinding().getValue());
//        if(getCategoryBinding().getValue() != null && getDesigCodeBinding().getValue()!=null)
//        { 
//            System.out.println("Inside If to check Category and designation binding");
//        getLevelCodeBinding().setDisabled(true);
//        }
    }
    }

    public void setLevelCodeBinding(RichInputComboboxListOfValues levelCodeBinding) {    
        this.levelCodeBinding = levelCodeBinding;
    }

    public RichInputComboboxListOfValues getLevelCodeBinding() {
        return levelCodeBinding;
    }

    public void setCategoryCodeBinding(RichInputComboboxListOfValues categoryCodeBinding) {
        this.categoryCodeBinding = categoryCodeBinding;
    }

    public RichInputComboboxListOfValues getCategoryCodeBinding() {
        return categoryCodeBinding;
    }

    public void yearFromVCL(ValueChangeEvent vce) {
        
//        System.out.println("INSIDE yearFromVCL  $$$$$$$ ");
//        
//        try
//        {
//        if(vce != null) {
//            String str_date = (String)vce.getNewValue();
//            System.out.println("str_ date #### " + (String)vce.getNewValue());
//            
////            SimpleDateFormat date_format1 = new SimpleDateFormat(YEARFROM);
////            java.util.Date date = date_format.parse(str_date);
//            System.out.println("inside try #### ");
//            
//            SimpleDateFormat date_format = new SimpleDateFormat("yyyy-MM-dd");
//            Date date = new SimpleDateFormat("yyyy-MM-dd").parse(str_date);
//            Calendar cal = GregorianCalendar.getInstance();
//            cal.setTime(date);
//            DateFormat df = new SimpleDateFormat("yyyy");
//            System.out.println("Df Value #### v" + df);
//            //            String currentMonthAsSting = df.format(cal.getTime());
//            
//            //For First Next Month
//            cal.set(Calendar.YEAR, cal.get(Calendar.YEAR));
//            String next_yr = df.format(cal.getTime());
//            System.out.println("DF Value #### " + df);
//            System.out.println("Actual Date ### " + df.format(cal.getTime()));
//            
//            yearFromBinding.setValue(next_yr);
//        }
//        }catch (ParseException pe) {
//            // TODO: Add catch code
//            pe.printStackTrace();
//        }
    }

    public void setYearFromBinding(RichInputDate yearFromBinding) {
        this.yearFromBinding = yearFromBinding;
    }

    public RichInputDate getYearFromBinding() {
        return yearFromBinding;
    }

    public void yearToVCL(ValueChangeEvent vce) {
//        vce.getNewValue();
//        System.out.println("To Year Value"+vce.getNewValue());
//        String FrYr=(String)vce.getNewValue();
//        System.out.println("To Year Value 2 -->"+FrYr);
//        String FrYr2 =FrYr.substring(0, 4);
//        System.out.println("To Year Value 3 -->"+FrYr2);
//        yearToBinding.setValue(FrYr2);
    }

    public void setYearToBinding(RichInputDate yearToBinding) {
        this.yearToBinding = yearToBinding;
    }

    public RichInputDate getYearToBinding() {
        return yearToBinding;
    }

    public void setBindingOutputText3(RichOutputText bindingOutputText3) {
        this.bindingOutputText3 = bindingOutputText3;
    }

    public RichOutputText getBindingOutputText3() {
        cevmodecheck3();
        return bindingOutputText3;
    }

    public void setMyPageRootComponent3(RichPanelGroupLayout myPageRootComponent3) {
        this.myPageRootComponent3 = myPageRootComponent3;
    }

    public RichPanelGroupLayout getMyPageRootComponent3() {
        return myPageRootComponent3;
    }

    public void setDesigCodeBinding(RichInputComboboxListOfValues desigCodeBinding) {
        this.desigCodeBinding = desigCodeBinding;
    }

    public RichInputComboboxListOfValues getDesigCodeBinding() {
        return desigCodeBinding;
    }

    public void desigCodeVCL(ValueChangeEvent vc) {
        vc.getComponent().processUpdates(FacesContext.getCurrentInstance());
       String dc=(String) vc.getNewValue();
       System.out.println("DC: "+dc);
           if(dc!=null && dc!="")
                   {
                       System.out.println("Inside If to check Category and designation binding");
                   getLevelCodeBinding().setDisabled(true);
                   }
    }

    public void setLocationBinding(RichInputComboboxListOfValues locationBinding) {
        this.locationBinding = locationBinding;
    }

    public RichInputComboboxListOfValues getLocationBinding() {
        return locationBinding;
    }

    public void setLocationNameBinding(RichInputText locationNameBinding) {
        this.locationNameBinding = locationNameBinding;
    }

    public RichInputText getLocationNameBinding() {
        return locationNameBinding;
    }

    public void setReportingNameBinding(RichInputText reportingNameBinding) {
        this.reportingNameBinding = reportingNameBinding;
    }

    public RichInputText getReportingNameBinding() {
        return reportingNameBinding;
        }
    public void createButtonAL(ActionEvent actionEvent) {

        ADFUtils.findOperation("CreateInsert2").execute();
        Row row=(Row)ADFUtils.evaluateEL("#{bindings.EmployeeMasterDependentVO3Iterator.currentRow}");
        row.setAttribute("SrlNo", ADFUtils.evaluateEL("#{bindings.EmployeeMasterDependentVO3Iterator.estimatedRowCount}"));
//        ADFUtils.findOperation("CreateInsert2").execute();
//        OperationBinding ob= ADFUtils.findOperation("serialNoInsert1");
//        ob.getParamsMap().put("SrlNo", ADFUtils.evaluateEL("#{bindings.EmployeeMasterDependentVO3Iterator.estimatedRowCount}"));
//        ob.execute();
    }


    public void setMyPageRootComponent4(RichPanelGroupLayout myPageRootComponent4) {
        this.myPageRootComponent4 = myPageRootComponent4;
    }

    public RichPanelGroupLayout getMyPageRootComponent4() {
        return myPageRootComponent4;
    }
    
    public void setBindingOutputText4(RichOutputText bindingOutputText4) {
        this.bindingOutputText4 = bindingOutputText4;
    }

    public RichOutputText getBindingOutputText4() {
        cevmodecheck4();
        return bindingOutputText4;
    }

    public void amtValidate(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if(object!=null)
        {
//            BigDecimal amt=(BigDecimal)object;
//            if(amt.compareTo(new BigDecimal(100))==1)
//            {
//                ADFUtils.showMessage("Total Pf Amount Share cannot be greater than zero.", 0);
//                status="T";
////                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,"Total Pf Amount Share cannot be greater than zero.",null));
////                throw new ValidationException("asbjsdgwbxsj bcyusfc");
//            }else
//            {
//                status="F";
//            }
        }

    }

    public void sharePfAmtValidator(FacesContext facesContext, UIComponent uIComponent, Object ob) {
        

    }

    public void amtVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        BigDecimal amt=(BigDecimal)ADFUtils.resolveExpression("#{bindings.PfAmountShareTrans.inputValue}");
        System.out.println("TOTALSDWFWEFWF1=====>"+amt);
        if(amt!=null && amt.compareTo(new BigDecimal(100))==1)
        {
            ADFUtils.showMessage("Total Pf Amount Share cannot be greater than 100.", 0);
                            status="T";
                        }else
                        {
                            status="F";
                        }
    }

    public String saveCloseAL() {
        ADFUtils.setLastUpdatedBy("EmployeeMasterVO1Iterator","LastUpdatedBy");
            if(status.equalsIgnoreCase("T"))
            {
                
                System.out.println("In if T");
                ADFUtils.showMessage("Total Pf Amount Share cannot be greater than 100.", 0);
            }else
            {
            System.out.println("In else F");
        check = false;
        String path;
                
           
        //        if (getPhotoFile() != null) {
        //            path = "/home/beta4/pics/" + PhotoFile.getFilename();
        //            Imagepath = SaveuploadFile(PhotoFile, path);
        //            //        System.out.println("path " + Imagepath);
        //            OperationBinding ob = ADFUtils.findOperation("AddImagePath");
        //            ob.getParamsMap().put("ImagePath", Imagepath);
        //            ob.execute();
        //        }
        //
        //
        //        File directory = new File("/tmp/pht");
        //        //get all the files from a directory
        //        File[] fList = directory.listFiles();
        //        for (File file : fList) {
        //            //Delete all previously uploaded files
        //            // if (!"NoImage.png".equalsIgnoreCase(file.getName())) {
        //            file.delete();
        //            //}
        //        }
        


        System.out.println("outside error block:-> "+StatusEmp);
        if(StatusEmp.equalsIgnoreCase("T"))
        {
            
            System.out.println("In else FF");
            System.out.println("IN get error block"+ADFUtils.resolveExpression("#{pageFlowScope.mode}"));
            //if (empNumberBinding.getValue()==null) {
        //                OperationBinding op=ADFUtils.findOperation("generateEmployeeCode");
        //                op.execute();
                
        //                String Res =(String)op.getResult();
        //                System.out.println("Value of the Res is----> "+Res);
                
            
                System.out.println("Emp Header Value"+empNumberBinding.getValue());
                
                String EmpVal=(String) empNumberBinding.getValue();
                
                System.out.println("Emp Header Value"+EmpVal);
                
                System.out.println("Emp Detail Value"+employeeCodeDetailBinding.getValue());
                //employeeCodeDetailBinding.getValue();
                if(EmpVal !=null)
                              {
                employeeCodeDetailBinding.setValue(EmpVal);
              System.out.println("Emp Detail Value 2-->"+employeeCodeDetailBinding.getValue());         

            // if(employeeCodeDetailBinding.getValue() != null)
            // {
                OperationBinding opr = ADFUtils.findOperation("Commit");
                Object obj = opr.execute();
                ADFUtils.findOperation("Commit");
                System.out.println("S####");
                FacesMessage Message = new FacesMessage("Record Save Successfully. New Employee Code is "+empNumberBinding.getValue());
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
                                  return "Save and Close";
            // }
                              }
            //} 
            else {
                System.out.println("S####CCC");
                ADFUtils.findOperation("Commit").execute();
                FacesMessage Message = new FacesMessage("Record Update Successfully");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
                return "Save and Close";
            }
        }
        else
        {
            if (!empNumberBinding.isDisabled()) {
                ADFUtils.findOperation("Commit").execute();
                System.out.println("S####");
                FacesMessage Message = new FacesMessage("Record Save Successfully. New Employee Code is "+empNumberBinding.getValue());
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
                return "Save and Close";

            } else {
                System.out.println("S####SS");
                ADFUtils.findOperation("Commit").execute();
                FacesMessage Message = new FacesMessage("Record Update Successfully");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
                return "Save and Close";
            }
        }
        }
        return null;
    }


    public void setStatusBinding(RichSelectOneChoice statusBinding) {
        this.statusBinding = statusBinding;
    }

    public RichSelectOneChoice getStatusBinding() {
        return statusBinding;
    }

    public void relievingDateVCL(ValueChangeEvent vce) {
      System.out.println("relievingDateVCL Value"+vce.getNewValue());   
      if(vce.getNewValue() !=null)
      {
          getStatusBinding().getValue();         
          System.out.println("Sataus Value"+getStatusBinding().getValue());        
          String staVal=(String)getStatusBinding().getValue();         
          statusBinding.setValue('S');                 
          }
    }

    public void statusVCL(ValueChangeEvent vce) {
       String staVal=(String)vce.getNewValue();    
       System.out.println("Status New Value"+vce.getNewValue());     
       if(staVal.equalsIgnoreCase("S"))
       {
           
           
           resigDateBinding.getValue();
           relievingDateBinding.getValue();
           
           System.out.println("Resig Valur-->"+ resigDateBinding.getValue());
               System.out.println("Relvining Valur-->"+ relievingDateBinding.getValue());
           
           java.sql.Timestamp dateTime=new java.sql.Timestamp(System.currentTimeMillis());           
           System.out.println("Syatem Date--->"+dateTime);
           
            oracle.jbo.domain.Timestamp datm=new oracle.jbo.domain.Timestamp(dateTime);
            System.out.println("Sastem Date Timestamp-->"+datm);
           
           if(resigDateBinding.getValue()==null)
           {
               System.out.println("resigDateBinding");
                   resigDateBinding.setValue(datm);
               }
           
               if(relievingDateBinding.getValue()==null)
               {
                       System.out.println("relievingDateBinding");
                       relievingDateBinding.setValue(datm);
                   }
           
           
           }
       
       
    }

    public void setPfNumberBinding(RichInputText pfNumberBinding) {
        this.pfNumberBinding = pfNumberBinding;
    }

    public RichInputText getPfNumberBinding() {
        return pfNumberBinding;
    }

    public void setPfMembershipDateBinding(RichInputDate pfMembershipDateBinding) {
        this.pfMembershipDateBinding = pfMembershipDateBinding;
    }

    public RichInputDate getPfMembershipDateBinding() {
        return pfMembershipDateBinding;
    }

    public void pfApplicableVCL(ValueChangeEvent vce) {
       vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
       System.out.println("PF Applicable"+vce.getNewValue());
       if(vce.getNewValue().equals(true))
       {
            pfNumberBinding.setDisabled(false);
            pfMembershipDateBinding.setDisabled(false);
            pfRelievingReasonBinding.setDisabled(false);
        }else
       {
            pfNumberBinding.setDisabled(true);
            pfMembershipDateBinding.setDisabled(true);
            pfRelievingReasonBinding.setDisabled(true);
        }
    }

    public void setPfApplicableBinding(RichSelectBooleanCheckbox pfApplicableBinding) {
        this.pfApplicableBinding = pfApplicableBinding;
    }

    public RichSelectBooleanCheckbox getPfApplicableBinding() {
        return pfApplicableBinding;
    }

    public void pfEsiDL(DisclosureEvent disclosureEvent) {
//        if(pfApplicableBinding.getValue().equals(true) && ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E"))
//        {
//             pfNumberBinding.setDisabled(false);
//             pfMembershipDateBinding.setDisabled(false);
//             pfRelievingReasonBinding.setDisabled(false);
//        }else
//        {
//             pfNumberBinding.setDisabled(true);
//             pfMembershipDateBinding.setDisabled(true);
//             pfRelievingReasonBinding.setDisabled(true);
//        }
//        if(esiApplicableBinding.getValue().equals(true) && ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E"))
//        {
//            esiNumberBinding.setDisabled(false);
//            esiMembershipDateBinding.setDisabled(false);
//            esiRelievingReasonBinding.setDisabled(false); 
//        }
//        else
//        {
//            esiNumberBinding.setDisabled(true);
//            esiMembershipDateBinding.setDisabled(true);
//            esiRelievingReasonBinding.setDisabled(true);
//        }
    }

    public void esiApplicableVCL(ValueChangeEvent vce) {
      vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
      if(vce.getNewValue().equals(true))
      {
          esiNumberBinding.setDisabled(false);
          esiMembershipDateBinding.setDisabled(false);
          esiRelievingReasonBinding.setDisabled(false);
          }else
          {
          esiNumberBinding.setDisabled(true);
          esiMembershipDateBinding.setDisabled(true);
          esiRelievingReasonBinding.setDisabled(true);
          }
    }

    public void setEsiNumberBinding(RichInputText esiNumberBinding) {
        this.esiNumberBinding = esiNumberBinding;
    }

    public RichInputText getEsiNumberBinding() {
        return esiNumberBinding;
    }

    public void setEsiMembershipDateBinding(RichInputDate esiMembershipDateBinding) {
        this.esiMembershipDateBinding = esiMembershipDateBinding;
    }

    public RichInputDate getEsiMembershipDateBinding() {
        return esiMembershipDateBinding;
    }

    public void employeeCodeVCL(ValueChangeEvent vce)   
    {
        String empVal=(String)vce.getNewValue();
        System.out.println("Employee Value-->"+empVal);
        String crdval=(String)cardNoBinding.getValue();
        System.out.println("Card Value-->"+crdval);
        String unit=(String)unitCodeBinding.getValue();
        System.out.println("employee and unit Value-->"+empVal+unit);
        if(empVal!=null && unit !=null)
        {
               // cardNoBinding.setValueBinding("CardNo", empVal);
            System.out.println("Inside Id EmpVCL");
                OperationBinding op1=ADFUtils.findOperation("generateCardNoForEmployee");
                op1.getParamsMap().put("empVal", empVal);
                op1.getParamsMap().put("unit", unit);
                op1.execute();
                getEmpNumberBinding().setDisabled(true);
                getEmpTypeBinding().setDisabled(true);
                System.out.println("After Calling function EmpVCL");
            }
        
        
    }

    public void setCardNoBinding(RichInputText cardNoBinding) {
        this.cardNoBinding = cardNoBinding;
    }

    public RichInputText getCardNoBinding() {
        return cardNoBinding;
    }

    public void setEsiApplicableBinding(RichSelectBooleanCheckbox esiApplicableBinding) {
        this.esiApplicableBinding = esiApplicableBinding;
    }

    public RichSelectBooleanCheckbox getEsiApplicableBinding() {
        return esiApplicableBinding;
    }

    public void setApproveByCodeBinding(RichInputComboboxListOfValues approveByCodeBinding) {
        this.approveByCodeBinding = approveByCodeBinding;
    }

    public RichInputComboboxListOfValues getApproveByCodeBinding() {
        return approveByCodeBinding;
    }

    public void setApproveByDateBinding(RichInputDate approveByDateBinding) {
        this.approveByDateBinding = approveByDateBinding;
    }

    public RichInputDate getApproveByDateBinding() {
        return approveByDateBinding;
    }

    public void approveByVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
           if (vce.getNewValue() != null) {
               OperationBinding op = (OperationBinding) ADFUtils.findOperation("checkApprovalAuthority");
               op.getParamsMap().put("unitCd", getUnitCodeBinding().getValue().toString());
               op.getParamsMap().put("empCode", vce.getNewValue());
               op.getParamsMap().put("authLevel", "AP");
               op.getParamsMap().put("formName", "EMA");
               op.execute();
               if (op.getResult() != null && op.getResult().equals("N")) {
                   Row r = (Row) ADFUtils.evaluateEL("#{bindings.EmployeeMasterVO1Iterator.currentRow}");
                   r.setAttribute("Approval", null);
                   r.setAttribute("ApprDate", null);
                   //bindFreeze.setValue("N");
                   FacesMessage Message =
                       new FacesMessage("Sorry You Are Not Authorized To Approve this Employee Data.");
                   Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                   FacesContext fc = FacesContext.getCurrentInstance();
                   fc.addMessage(null, Message);
               } 
//               else {
//                   System.out.println("Before Page Refresh");
//                   Timestamp dt = new Timestamp(System.currentTimeMillis());
//                   approveByDateBinding.setValue(dt);
//                   
//                 
//                   System.out.println("After Page Refresh");
                  // bindFreeze.setValue("Y");A
//               }
               //AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent());
//               refreshPage();
//               refreshPage();
//               AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent1());
//               AdfFacesContext.getCurrentInstance().addPartialTarget(saveButtonBinding);



           }
    }
    
    public void refreshPage() {
          FacesContext fctx = FacesContext.getCurrentInstance();
          String page = fctx.getViewRoot().getViewId();
          ViewHandler ViewH = fctx.getApplication().getViewHandler();
          UIViewRoot UIV = ViewH.createView(fctx, page);
          UIV.setViewId(page);
          fctx.setViewRoot(UIV);
      }

    public void setApproveByNameBinding(RichInputText approveByNameBinding) {
        this.approveByNameBinding = approveByNameBinding;
    }

    public RichInputText getApproveByNameBinding() {
        return approveByNameBinding;
    }
    public void uploadImageFileAction(ActionEvent actionEvent) {
        String flag = uploadImage(imageFile);
        System.out.println("Inside Action Listner: "+flag);
                if ("NO".equalsIgnoreCase(flag)) {
                    FacesMessage msg =
                        new FacesMessage("This is not an Image file, Please upload supported file type (.jpg,.png etc)");
                    msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext.getCurrentInstance().addMessage(null, msg);
                }
    }
    private String uploadImage(UploadedFile file) {
        try{
            UploadedFile myfile = file;
            System.out.println("MY File: "+myfile.getFilename());
    //        imageNameBinding.setValue(myfile.getFilename());
            if (myfile == null) {

            } else {
                if (myfile.getContentType().equalsIgnoreCase("image/jpeg") ||
                    myfile.getContentType().equalsIgnoreCase("image/png") ||
                    myfile.getContentType().equalsIgnoreCase("image/bmp") ||
                    myfile.getContentType().equalsIgnoreCase("image/gif")) {

                    //Path of folder on drive
                    OperationBinding op=ADFUtils.findOperation("getServerPath");
                    op.getParamsMap().put("module", "EMP_IMG_UPLOAD");
                    op.execute();
                    System.out.println("Result: "+op.getResult());
                    if(op.getResult()!=null){
                    String path = (String)op.getResult();
                    System.out.println("Server Path after function Call: "+path);
                    String type = "PNG";
                    String TypeVal = ".png";
                    if (myfile.getContentType().equalsIgnoreCase("image/jpeg")) {
                        type = "JPEG";
                        TypeVal = ".jpeg";
                    } else if (myfile.getContentType().equalsIgnoreCase("image/png")) {
                        type = "PNG";
                        TypeVal = ".png";
                    } else if (myfile.getContentType().equalsIgnoreCase("image/bmp")) {
                        type = "PNG";
                        TypeVal = ".png";
                    } else if (myfile.getContentType().equalsIgnoreCase("image/gif")) {
                        type = "GIF";
                        TypeVal = ".gif";
                    }

                    InputStream inputStream = null;
                    try {
                        //Generate a unique name for uploaded image with date time
                        DateFormat dateFormat = new SimpleDateFormat("ddMMyy_HHmmss");
                        Date date = new Date();
                        String dtTime = dateFormat.format(date);
                        dtTime = dtTime.replace(" ", "_");
                        String enamee=(String)empNumberBinding.getValue();
                        enamee=enamee.replace("/", "_");
                        System.out.println("ENAMEE"+enamee);
    //                  
                        String name =enamee+"_"+ dtTime + TypeVal;
    //                    System.out.println("File name is-" + name);
                        DCIteratorBinding dci=ADFUtils.findIterator("EmployeeMasterVO1Iterator");
                        dci.getCurrentRow().setAttribute("PhotoFile", name);
                        dci.getCurrentRow().setAttribute("PhotoAttached", "Y");

                        inputStream = myfile.getInputStream();
                        BufferedImage input = ImageIO.read(inputStream);
                        //Writing file to path
                        File outputFile = new File(path + name);
                        ImageIO.write(input, type, outputFile);
                        imagePath = outputFile.getAbsolutePath();
                        System.out.println("Image Path: "+imagePath);
                    

                    } catch (Exception ex) {
                        // handle exception
                        ex.printStackTrace();
                    } 
                    finally {
                        try {
                            inputStream.close();
                        } catch (IOException e) {
                        }
                    }
                }
                else{
                    ADFUtils.showMessage("No Server Path Defined.", 0);
                    }
            }else {
                    imagePath = "NO";
                }
            }
            setImageFile(null);
            
        }
        catch(Exception e){
            ADFUtils.showMessage("Please select File for upload.", 0);
            e.printStackTrace();
        }
            return imagePath;
        }

    public void setSaveButtonBinding(RichButton saveButtonBinding) {
        this.saveButtonBinding = saveButtonBinding;
    }

    public RichButton getSaveButtonBinding() {
        return saveButtonBinding;
    }

    public void setEmpInfoTabBinding(RichShowDetailItem empInfoTabBinding) {
        this.empInfoTabBinding = empInfoTabBinding;
    }

    public RichShowDetailItem getEmpInfoTabBinding() {
        return empInfoTabBinding;
    }

    public void setMyPageRootComponent(RichPanelHeader myPageRootComponent) {
        this.myPageRootComponent = myPageRootComponent;
    }

    public RichPanelHeader getMyPageRootComponent() {
        return myPageRootComponent;
    }

    public void setEmpTypeBinding(RichSelectOneChoice empTypeBinding) {
        this.empTypeBinding = empTypeBinding;
    }

    public RichSelectOneChoice getEmpTypeBinding() {
        return empTypeBinding;
    }


//    public void cardNoVCL(ValueChangeEvent vce) {
//        System.out.println("Inside card number VCL!");
//        String cd=(String)vce.getNewValue();
//        System.out.println("Card No: "+cd);
//        String uc=(String)unitCodeBinding.getValue();
//        System.out.println("Unit Code: "+uc);
//        //       if(vce.getNewValue()!=null)
//        
//           OperationBinding ob=(OperationBinding)ADFUtils.findOperation("checkduplicatecardno");
//           ob.getParamsMap().put("UnitCd",uc);
//           ob.getParamsMap().put("CrdNo", cd);
//           ob.execute();
//           System.out.println("Back to bean in crdno vcl");
//           String res=(String)ob.getResult();
//           System.out.println("Returned result: "+res);
//           if(res.equals("E-Unit can not blank...") ){
//               //throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,res.substring(2), null));
//               FacesMessage Message = new FacesMessage("Unit can not blank...");
//               Message.setSeverity(FacesMessage.SEVERITY_ERROR);
//               FacesContext fc = FacesContext.getCurrentInstance();
//               fc.addMessage(null, Message);}
//           else if(res.equals("E-This card number already saved, please check ...")){
//               //throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,res.substring(2), null));
//               FacesMessage Message = new FacesMessage("This card number already saved, please check ...");
//               Message.setSeverity(FacesMessage.SEVERITY_ERROR);
//               FacesContext fc = FacesContext.getCurrentInstance();
//               fc.addMessage(null, Message);
//               }
//           else
//           { System.out.println("All good!");}
//           
//        }
    }
