package terms.hrm.setup.ui.bean;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import java.io.InputStream;
import java.io.OutputStream;

import java.sql.Connection;

import java.sql.SQLException;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.faces.context.FacesContext;

import javax.naming.Context;
import javax.naming.InitialContext;

import javax.servlet.http.HttpServletResponse;

import javax.sql.DataSource;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;

import net.sf.jasperreports.engine.util.JRLoader;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;


public class EmployeeMasterReportParameterBean {
    private RichPanelHeader myRootBinding;
    private RichShowDetailItem tab1Binding;
    private RichShowDetailItem tab2Binding;
    private RichShowDetailItem tab3Binding;
    private RichShowDetailItem tab4Binding;
    private RichShowDetailItem tab5Binding;
    private RichShowDetailItem tab6Binding;
    private RichShowDetailItem tab7Binding;
    private RichShowDetailItem tab8Binding;
    private RichShowDetailItem tab9Binding;
    private RichShowDetailItem tab10Binding;
    private RichShowDetailItem tab11Binding;
    private RichShowDetailItem tab12Binding;
    private String p_mode = "";
    String file_name="";

    public EmployeeMasterReportParameterBean() {
    }

    public void setMyRootBinding(RichPanelHeader myRootBinding) {
        this.myRootBinding = myRootBinding;
    }

    public RichPanelHeader getMyRootBinding() {
        return myRootBinding;
    }

    public void setTab1Binding(RichShowDetailItem tab1Binding) {
        this.tab1Binding = tab1Binding;
    }

    public RichShowDetailItem getTab1Binding() {
        return tab1Binding;
    }

    public void setTab2Binding(RichShowDetailItem tab2Binding) {
        this.tab2Binding = tab2Binding;
    }

    public RichShowDetailItem getTab2Binding() {
        return tab2Binding;
    }

    public void setTab3Binding(RichShowDetailItem tab3Binding) {
        this.tab3Binding = tab3Binding;
    }

    public RichShowDetailItem getTab3Binding() {
        return tab3Binding;
    }

    public void setTab4Binding(RichShowDetailItem tab4Binding) {
        this.tab4Binding = tab4Binding;
    }

    public RichShowDetailItem getTab4Binding() {
        return tab4Binding;
    }

    public void setTab5Binding(RichShowDetailItem tab5Binding) {
        this.tab5Binding = tab5Binding;
    }

    public RichShowDetailItem getTab5Binding() {
        return tab5Binding;
    }

    public void setTab6Binding(RichShowDetailItem tab6Binding) {
        this.tab6Binding = tab6Binding;
    }

    public RichShowDetailItem getTab6Binding() {
        return tab6Binding;
    }

    public void setTab7Binding(RichShowDetailItem tab7Binding) {
        this.tab7Binding = tab7Binding;
    }

    public RichShowDetailItem getTab7Binding() {
        return tab7Binding;
    }

    public void setTab8Binding(RichShowDetailItem tab8Binding) {
        this.tab8Binding = tab8Binding;
    }

    public RichShowDetailItem getTab8Binding() {
        return tab8Binding;
    }

    public void setTab9Binding(RichShowDetailItem tab9Binding) {
        this.tab9Binding = tab9Binding;
    }

    public RichShowDetailItem getTab9Binding() {
        return tab9Binding;
    }

    public void setTab10Binding(RichShowDetailItem tab10Binding) {
        this.tab10Binding = tab10Binding;
    }

    public RichShowDetailItem getTab10Binding() {
        return tab10Binding;
    }

    public void setTab11Binding(RichShowDetailItem tab11Binding) {
        this.tab11Binding = tab11Binding;
    }

    public RichShowDetailItem getTab11Binding() {
        return tab11Binding;
    }

    public void setTab12Binding(RichShowDetailItem tab12Binding) {
        this.tab12Binding = tab12Binding;
    }

    public RichShowDetailItem getTab12Binding() {
        return tab12Binding;
    }
    
    public BindingContainer getBindings()
          {
            return BindingContext.getCurrent().getCurrentBindingsEntry();
          }
    
    public void downloadJrxml(FacesContext facesContext, OutputStream outputStream) {
        Connection conn=null;
        try {
            
            System.out.println("p_mode: "+p_mode);          
            
            Map n = new HashMap();
            if(p_mode != null){
              if(p_mode.equalsIgnoreCase("A")){
                 file_name ="r_join_emp.jasper";
                 System.out.println("p_mode: "+p_mode);
                 System.out.println("File Name: "+file_name);
                DCIteratorBinding nje = (DCIteratorBinding) getBindings().get("NewlyJoinedEmployeeReportVO1Iterator");
                n.put("p_unit", nje.getCurrentRow().getAttribute("unitCd").toString());
                n.put("p_emp_no",  nje.getCurrentRow().getAttribute("empCd")== null?'%':nje.getCurrentRow().getAttribute("empCd").toString());
                n.put("p_from_dt", nje.getCurrentRow().getAttribute("frmDt"));
                n.put("p_to_dt", nje.getCurrentRow().getAttribute("toDt"));
                
                System.out.println("unit: "+ nje.getCurrentRow().getAttribute("unitCd")
                                   +" Emp: "+ nje.getCurrentRow().getAttribute("empCd")+
                " From: "+ nje.getCurrentRow().getAttribute("frmDt")
                                                       +" To: "+ nje.getCurrentRow().getAttribute("toDt"));
                
            }else if(p_mode.equalsIgnoreCase("B")){
                 file_name ="r_left_emp.jasper";
                 System.out.println("p_mode: "+p_mode);
                 System.out.println("File Name: "+file_name);
                DCIteratorBinding le = (DCIteratorBinding) getBindings().get("LeftEmployeeReportVO1Iterator");
                n.put("p_unit", le.getCurrentRow().getAttribute("unitcd").toString());
                n.put("p_emp_no",  le.getCurrentRow().getAttribute("empNo")== null?'%':le.getCurrentRow().getAttribute("empNo").toString());
                n.put("p_from_dt", le.getCurrentRow().getAttribute("fromDt"));
                n.put("p_to_dt", le.getCurrentRow().getAttribute("toDt"));
                
                System.out.println("unit: "+ le.getCurrentRow().getAttribute("unitcd")
                                   +" Emp: "+ le.getCurrentRow().getAttribute("empNo")+
                " From: "+ le.getCurrentRow().getAttribute("fromDt")
                                                       +" To: "+ le.getCurrentRow().getAttribute("toDt"));

            }else if(p_mode.equalsIgnoreCase("C")){
                 file_name ="r_blood_group_simple.jasper";
                 System.out.println("p_mode: "+p_mode);
                 System.out.println("File Name: "+file_name);
                DCIteratorBinding bg = (DCIteratorBinding) getBindings().get("EmployeeBloodGroupWiseReportVo1Iterator");                    
                n.put("p_unit", bg.getCurrentRow().getAttribute("unitCd").toString());
                n.put("p_blod_grp",  bg.getCurrentRow().getAttribute("bloodGrp")== null?'%':bg.getCurrentRow().getAttribute("bloodGrp").toString());
                n.put("p_dept",  bg.getCurrentRow().getAttribute("dept")== null?'%':bg.getCurrentRow().getAttribute("dept").toString());
                
                System.out.println("Unit: "+ bg.getCurrentRow().getAttribute("unitCd")
                                   +" Dept: "+ bg.getCurrentRow().getAttribute("dept")+
                " Blood: "+ bg.getCurrentRow().getAttribute("bloodGrp"));
                
            }else if(p_mode.equalsIgnoreCase("D")){
                 file_name ="r_blood_grp_wise.jasper";
                 System.out.println("p_mode: "+p_mode);
                 System.out.println("File Name: "+file_name);
                DCIteratorBinding bgu = (DCIteratorBinding) getBindings().get("EmployeeBloodGroupWiseGroupWiseReportVo1Iterator");                    
                n.put("p_unit", bgu.getCurrentRow().getAttribute("unitCd").toString());
                n.put("p_blod_grp",  bgu.getCurrentRow().getAttribute("bloodgrp")== null?'%':bgu.getCurrentRow().getAttribute("bloodgrp").toString());
                
                System.out.println("Unit: "+ bgu.getCurrentRow().getAttribute("unitCd")+
                " Blood: "+ bgu.getCurrentRow().getAttribute("bloodgrp"));
                
            }else if(p_mode.equalsIgnoreCase("E")){
                 file_name ="r_grade_wise.jasper";
                 System.out.println("p_mode: "+p_mode);
                 System.out.println("File Name: "+file_name);
                DCIteratorBinding g = (DCIteratorBinding) getBindings().get("EmployeeGradeCategoryWiseReportVo1Iterator");                    
                n.put("p_unit", g.getCurrentRow().getAttribute("unitCd").toString());
                n.put("p_cat",  g.getCurrentRow().getAttribute("grade")== null?'%':g.getCurrentRow().getAttribute("grade").toString());
                
                System.out.println("Unit: "+ g.getCurrentRow().getAttribute("unitCd")+
                " Grade: "+ g.getCurrentRow().getAttribute("grade"));
                
            }else if(p_mode.equalsIgnoreCase("F")){
                 file_name ="r_qualifi_wise.jasper";
                 System.out.println("p_mode: "+p_mode);
                 System.out.println("File Name: "+file_name);
                DCIteratorBinding q = (DCIteratorBinding) getBindings().get("EmployeeQualificationWiseReportVo1Iterator");                    
                n.put("p_unit", q.getCurrentRow().getAttribute("unitCd").toString());
                n.put("p_qualification",  q.getCurrentRow().getAttribute("qualification")== null?'%':q.getCurrentRow().getAttribute("qualification").toString());
                
                System.out.println("Unit: "+ q.getCurrentRow().getAttribute("unitCd")+
                " Qualification: "+ q.getCurrentRow().getAttribute("qualification"));
                
            }else if(p_mode.equalsIgnoreCase("G")){
                 file_name ="r_serv_yr_wise.jasper";
                 System.out.println("p_mode: "+p_mode);
                 System.out.println("File Name: "+file_name);
                DCIteratorBinding sy = (DCIteratorBinding) getBindings().get("EmployeeServiceYearWiseReportVo1Iterator");                    
                n.put("p_unit", sy.getCurrentRow().getAttribute("unitCd").toString());
                
                System.out.println("Unit: "+ sy.getCurrentRow().getAttribute("unitCd"));
                
            }else if(p_mode.equalsIgnoreCase("H")){
                 file_name ="r_age_wise.jasper";
                 System.out.println("p_mode: "+p_mode);
                 System.out.println("File Name: "+file_name);
                DCIteratorBinding aw = (DCIteratorBinding) getBindings().get("EmployeeAgeWiseReportVo1Iterator");                    
                n.put("p_unit", aw.getCurrentRow().getAttribute("unitcd").toString());
                
                System.out.println("Unit: "+ aw.getCurrentRow().getAttribute("unitcd"));
                
            }else if(p_mode.equalsIgnoreCase("I")){
                 file_name ="r_actual_strength_lvl.jasper";
                 System.out.println("p_mode: "+p_mode);
                 System.out.println("File Name: "+file_name);
                DCIteratorBinding as = (DCIteratorBinding) getBindings().get("ActualStrengthOfEmployeesLevelWiseReportVo1Iterator");                    
                n.put("p_unit", as.getCurrentRow().getAttribute("unitCd").toString());
                n.put("p_as_on", as.getCurrentRow().getAttribute("asOnDt"));
                
                System.out.println("Unit: "+ as.getCurrentRow().getAttribute("unitCd")+
                " As On: "+ as.getCurrentRow().getAttribute("asOnDt"));
                
            }else if(p_mode.equalsIgnoreCase("J")){
                 file_name ="r_gradewise_strenth_simple.jasper";
                 System.out.println("p_mode: "+p_mode);
                 System.out.println("File Name: "+file_name);
                DCIteratorBinding gsw = (DCIteratorBinding) getBindings().get("GradeWiseStrengthOfEmployee1Iterator");                    
                n.put("p_unit", gsw.getCurrentRow().getAttribute("unitCd").toString());
                n.put("p_as_on_date", gsw.getCurrentRow().getAttribute("asOnDt"));
                
                System.out.println("Unit: "+ gsw.getCurrentRow().getAttribute("unitCd")+
                " As On: "+ gsw.getCurrentRow().getAttribute("asOnDt"));
                
            }else if(p_mode.equalsIgnoreCase("K")){
                 file_name ="r_gradewise_strength_unit.jasper";
                 System.out.println("p_mode: "+p_mode);
                 System.out.println("File Name: "+file_name);
                DCIteratorBinding gw = (DCIteratorBinding) getBindings().get("GradeWiseStrengthOfEmployeeAccordingUnitReportVo1Iterator");                    
                n.put("p_unit", gw.getCurrentRow().getAttribute("unitCd").toString());
                
                System.out.println("Unit: "+ gw.getCurrentRow().getAttribute("unitCd"));
                
            }else if(p_mode.equalsIgnoreCase("L")){
                 file_name ="r_service_age_emp.jasper";
                 System.out.println("p_mode: "+p_mode);
                 System.out.println("File Name: "+file_name);
                DCIteratorBinding sae = (DCIteratorBinding) getBindings().get("StatementOfAgeOfServiceOfEmployeesReportVo1Iterator");                    
                n.put("p_unit", sae.getCurrentRow().getAttribute("unitCd").toString());
                n.put("p_as_on", sae.getCurrentRow().getAttribute("asOnDt"));
                
                System.out.println("Unit: "+ sae.getCurrentRow().getAttribute("unitCd")+
                " As On: "+ sae.getCurrentRow().getAttribute("asOnDt"));                
            }      
        }
            
            //*************Find File name***************//
            OperationBinding binding = ADFUtils.findOperation("fileNameForPrint");
            binding.getParamsMap().put("fileId","HRM0000000218");
            binding.execute();
            //*************End Find File name***********//
            System.out.println("Binding Result :"+binding.getResult());
            if(binding.getResult() != null){
                String result = binding.getResult().toString();
                                  int last_index = result.lastIndexOf("/");
                if(last_index==-1)
                {
                    last_index=result.lastIndexOf("\\");
                }
                                                      String path = result.substring(0,last_index+1)+file_name;
                                                      System.out.println("FILE PATH IS===>"+path);
                                  InputStream input = new FileInputStream(path);
            //    InputStream input = new FileInputStream("//home//beta3//Jasper//Jour_prn1.jrxml");
                conn = getConnection( );
                
                JasperReport design = (JasperReport) JRLoader.loadObject(input);
                System.out.println("Path : " + input + " -------" + design+" param:"+n);
                
                @SuppressWarnings("unchecked")
                net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                byte pdf[] = JasperExportManager.exportReportToPdf(print);
                HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                response.getOutputStream().write(pdf);
                response.getOutputStream().flush();
                response.getOutputStream().close();
                facesContext.responseComplete();
                }else
                System.out.println("File Name/File Path not found.");
                } catch (FileNotFoundException fnfe) {
                // TODO: Add catch code
                fnfe.printStackTrace();
                try {
                    System.out.println("in finally connection closed");
                            conn.close( );
                            conn = null;
                    
                } catch( SQLException e ) {
                    e.printStackTrace( );
                }
                } catch (Exception e) {
                // TODO: Add catch code
                e.printStackTrace();
                try {
                    System.out.println("in finally connection closed");
                            conn.close( );
                            conn = null;
                    
                } catch( SQLException e1 ) {
                    e1.printStackTrace( );
                }
                }finally {
                        try {
                                System.out.println("in finally connection closed");
                                        conn.close( );
                                        conn = null;
                                
                        } catch( SQLException e ) {
                                e.printStackTrace( );
                        }
                }

    }   

    public void setP_mode(String p_mode) {
        this.p_mode = p_mode;
    }

    public String getP_mode() {
        return p_mode;
    }
    
    public Connection getConnection() throws Exception{
            Context ctx = null;
            Connection conn = null;
            ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/ApplicationDBDS");
            conn = ds.getConnection();
            return conn;
        }
}
