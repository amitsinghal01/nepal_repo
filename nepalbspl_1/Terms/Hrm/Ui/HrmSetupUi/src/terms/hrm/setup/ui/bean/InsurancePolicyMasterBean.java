package terms.hrm.setup.ui.bean;

import java.math.BigDecimal;

import java.util.Calendar;

import java.util.Date;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;

import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class InsurancePolicyMasterBean {
    private RichTable insurancePolicyMasterTableBinding;
    private String editAction = "V";
    private RichInputDate stopDateBinding;
    private RichInputDate startDateBinding;

    public InsurancePolicyMasterBean() {
    }

    public void setInsurancePolicyMasterTableBinding(RichTable insurancePolicyMasterTableBinding) {
        this.insurancePolicyMasterTableBinding = insurancePolicyMasterTableBinding;
    }

    public RichTable getInsurancePolicyMasterTableBinding() {
        return insurancePolicyMasterTableBinding;
    }

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public void pupUpDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            OperationBinding op = null;
            ADFUtils.findOperation("Delete").execute();
            op = (OperationBinding) ADFUtils.findOperation("Commit");
            if (getEditAction().equals("V")) {
                Object rst = op.execute();
            }
            System.out.println("Record Delete Successfully");
            if (op.getErrors().isEmpty()) {
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            } else if (!op.getErrors().isEmpty()) {
                OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
                Object rstr = opr.execute();
                FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(insurancePolicyMasterTableBinding);
    }

    public void createButtonInsurancePolicyMasterAL(ActionEvent actionEvent) {
        //ADFUtils.findOperation("CreateInsert").execute();
        ADFUtils.findOperation("generateInsuranceEntryNumber").execute();
        ADFUtils.findOperation("Commit").execute();
    }

    public void policyDateVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
            if(vce!=null && getStartDateBinding().getValue()!=null){
                int i = 50;              
                Calendar cal = Calendar.getInstance();
                    System.out.println("Current Date & Time is: "+cal.getTime());
                cal.add(Calendar.YEAR,i);
                java.util.Date nxt=cal.getTime();
                    System.out.println("After 50 Years: "+nxt);                
                getStopDateBinding().setValue(nxt);
            }
    }

    public void setStopDateBinding(RichInputDate stopDateBinding) {
        this.stopDateBinding = stopDateBinding;
    }

    public RichInputDate getStopDateBinding() {
        return stopDateBinding;
    }

    public void setStartDateBinding(RichInputDate startDateBinding) {
        this.startDateBinding = startDateBinding;
    }

    public RichInputDate getStartDateBinding() {
        return startDateBinding;
    }
}
