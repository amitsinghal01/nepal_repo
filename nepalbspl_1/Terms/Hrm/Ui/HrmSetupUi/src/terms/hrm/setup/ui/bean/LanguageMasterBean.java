package terms.hrm.setup.ui.bean;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.output.RichPanelCollection;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class LanguageMasterBean {
    private RichTable laguageMasterTableBinding;
    private RichPanelCollection pcBinding;

    public LanguageMasterBean() {
    }

    public void popupDialogListener(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            OperationBinding op = null;
            ADFUtils.findOperation("Delete").execute();
            op = (OperationBinding) ADFUtils.findOperation("Commit");
            if (getEditAction().equals("V")) {
                Object rst = op.execute();
            }
            System.out.println("Record Delete Successfully");
            if (op.getErrors().isEmpty()) {
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            } else if (!op.getErrors().isEmpty()) {
                OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
                Object rstr = opr.execute();
                FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(laguageMasterTableBinding);
        AdfFacesContext.getCurrentInstance().addPartialTarget(pcBinding);

    }

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public void setLaguageMasterTableBinding(RichTable laguageMasterTableBinding) {
        this.laguageMasterTableBinding = laguageMasterTableBinding;
    }

    public RichTable getLaguageMasterTableBinding() {
        return laguageMasterTableBinding;
    }

    private String editAction = "V";


    public void setPcBinding(RichPanelCollection pcBinding) {
        this.pcBinding = pcBinding;
    }

    public RichPanelCollection getPcBinding() {
        return pcBinding;
    }

    public void saveButtonAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("generateLanguageCode").execute();
        ADFUtils.findOperation("Commit").execute();
    }
}
