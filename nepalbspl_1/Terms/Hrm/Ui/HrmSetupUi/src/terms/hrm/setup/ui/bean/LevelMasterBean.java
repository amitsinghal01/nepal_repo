package terms.hrm.setup.ui.bean;

import javax.faces.application.FacesMessage;

import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.Row;

public class LevelMasterBean {
    private RichTable levelMasterTableBinding;
    private RichSelectOneChoice empPrefixCd;

    public LevelMasterBean() {
    }

    public void popUpDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            OperationBinding op = null;
            ADFUtils.findOperation("Delete").execute();
            op = (OperationBinding) ADFUtils.findOperation("Commit");
            if (getEditAction().equals("V")) {
                Object rst = op.execute();
            }
            System.out.println("Record Delete Successfully");
            if (op.getErrors().isEmpty()) {
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            } else if (!op.getErrors().isEmpty()) {
                OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
                Object rstr = opr.execute();
                FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(levelMasterTableBinding);
    }
    private String editAction = "V";

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }


    /*     public void deletePopupDialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                                        {
                                        ADFUtils.findOperation("Delete").execute();
                                        //ADFUtils.findOperation("Commit").execute();
                                        OperationBinding op=  ADFUtils.findOperation("Commit");
                                                        Object rst = op.execute();
                                        System.out.println("Record Delete Successfully");
                                            if(op.getErrors().isEmpty())
                                                           {
                                            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                                            Message.setSeverity(FacesMessage.SEVERITY_INFO);
                                            FacesContext fc = FacesContext.getCurrentInstance();
                                            fc.addMessage(null, Message);
                                         }

                                            else{
                                            FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                                            Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                                            FacesContext fc = FacesContext.getCurrentInstance();
                                            fc.addMessage(null, Message);
                                            }

                                        }
             AdfFacesContext.getCurrentInstance().addPartialTarget(levelMasterTableBinding);
    } */

    public void setLevelMasterTableBinding(RichTable levelMasterTableBinding) {
        this.levelMasterTableBinding = levelMasterTableBinding;
    }

    public RichTable getLevelMasterTableBinding() {
        return levelMasterTableBinding;
    }

    public void saveButtonAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("generateLevelCode").execute();
        ADFUtils.findOperation("Commit").execute();
    }

    public void empPrefixCdVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce.getOldValue()!=null && vce.getNewValue()!=vce.getOldValue())
        {
        Integer i=(Integer)vce.getOldValue();
        String val=i.equals(0) ? "E":i.equals(1)? "T" : i.equals(2) ? "A" : "C";
        OperationBinding op= ADFUtils.findOperation("checkEmpPrefixCategory");
        op.execute();
        System.out.println("op.getResul==>"+op.getResult());
        if(op.getResult()!=null && op.getResult().equals("Y"))
        {
        ADFUtils.showMessage("For the respective Employee Code Pre fix,Level Code exist in Employee Master and it cannot be changed.", 2);
        Row r = (Row) ADFUtils.evaluateEL("#{bindings.LevelMasterVO1Iterator.currentRow}");
        r.setAttribute("EmpCodePreFix",val);
        }
            AdfFacesContext.getCurrentInstance().addPartialTarget(empPrefixCd);
        }
    }

    public void setEmpPrefixCd(RichSelectOneChoice empPrefixCd) {
        this.empPrefixCd = empPrefixCd;
    }

    public RichSelectOneChoice getEmpPrefixCd() {
        return empPrefixCd;
    }
}
