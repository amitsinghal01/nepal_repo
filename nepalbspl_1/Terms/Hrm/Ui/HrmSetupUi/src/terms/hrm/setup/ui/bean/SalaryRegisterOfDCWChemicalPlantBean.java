package terms.hrm.setup.ui.bean;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;

import java.sql.Connection;

import java.sql.SQLException;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import javax.naming.Context;
import javax.naming.InitialContext;

import javax.servlet.http.HttpServletResponse;

import javax.sql.DataSource;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

public class SalaryRegisterOfDCWChemicalPlantBean {
    private RichPanelHeader myRootPageBinding;
    String file_name="";
    private RichInputComboboxListOfValues lvlCdBinding;
    private RichSelectOneChoice monthBinding;
    private RichInputText yearBinding;
    private RichInputComboboxListOfValues unitBinding;

    public SalaryRegisterOfDCWChemicalPlantBean() {
    }

    public void setMyRootPageBinding(RichPanelHeader myRootPageBinding) {
        this.myRootPageBinding = myRootPageBinding;
    }

    public RichPanelHeader getMyRootPageBinding() {
        return myRootPageBinding;
    }
    
    public BindingContainer getBindings()
         {
           return BindingContext.getCurrent().getCurrentBindingsEntry();
         }
    
    public void downloadJrxml(FacesContext facesContext, OutputStream outputStream) {
        Connection conn=null;
        if(unitBinding.getValue()!=null && monthBinding.getValue()!=null && yearBinding.getValue()!=null && lvlCdBinding.getValue()!=null){
            OperationBinding opr = ADFUtils.findOperation("salaryDSW");                    
                opr.execute(); 
                    System.out.println("Result is: "+opr.getResult().toString());
        if(opr.getResult().equals("Y")){
            try {
                    System.out.println("Level Code: "+lvlCdBinding.getValue());                         
                Map n = new HashMap();
                if(lvlCdBinding.getValue() != null){
                if(lvlCdBinding.getValue().equals("101")){
                    file_name ="r_sal_reg_dsw_staff.jasper";
                                 System.out.println("Level Code: "+lvlCdBinding.getValue());
                                 System.out.println("File Name: "+file_name);
                    DCIteratorBinding a = (DCIteratorBinding) getBindings().get("SalaryRegisterOfDCWChemicalPlantReportVVO1Iterator");                    
                    n.put("p_unit", a.getCurrentRow().getAttribute("unitCd").toString());
                    n.put("p_month", a.getCurrentRow().getAttribute("month"));
                    n.put("p_year", a.getCurrentRow().getAttribute("year"));
                    n.put("p_level", a.getCurrentRow().getAttribute("year"));
                                   System.out.println("unit: "+ a.getCurrentRow().getAttribute("unitCd")+
                                                      " Month: "+ a.getCurrentRow().getAttribute("month")+
                                                      " Year: "+ a.getCurrentRow().getAttribute("year")+
                                                      " Level: "+ a.getCurrentRow().getAttribute("lvlCd"));
                                
                    }else if(lvlCdBinding.getValue().equals("102")){
                       file_name ="r_sal_reg_dsw_officer.jasper";
                                 System.out.println("Level Code: "+lvlCdBinding.getValue());
                                 System.out.println("File Name: "+file_name);
                       DCIteratorBinding b = (DCIteratorBinding) getBindings().get("SalaryRegisterOfDCWChemicalPlantReportVVO1Iterator");                    
                       n.put("p_unit", b.getCurrentRow().getAttribute("unitCd").toString());
                       n.put("p_month", b.getCurrentRow().getAttribute("month"));
                       n.put("p_year", b.getCurrentRow().getAttribute("year"));
                       n.put("p_level", b.getCurrentRow().getAttribute("lvlCd"));
                                    System.out.println("unit: "+ b.getCurrentRow().getAttribute("unitCd")+
                                                       " Month: "+ b.getCurrentRow().getAttribute("month")+
                                                       " Year: "+ b.getCurrentRow().getAttribute("year")+
                                                       " Level: "+ b.getCurrentRow().getAttribute("lvlCd"));
                                
                     }else if(lvlCdBinding.getValue().equals("104")){
                        file_name ="r_sal_reg_dsw_trainee.jasper";
                                 System.out.println("Level Code: "+lvlCdBinding.getValue());
                                 System.out.println("File Name: "+file_name);
                        DCIteratorBinding c = (DCIteratorBinding) getBindings().get("SalaryRegisterOfDCWChemicalPlantReportVVO1Iterator");                    
                        n.put("p_unit", c.getCurrentRow().getAttribute("unitCd").toString());
                        n.put("p_month", c.getCurrentRow().getAttribute("month"));
                        n.put("p_year", c.getCurrentRow().getAttribute("year"));
                        n.put("p_level", c.getCurrentRow().getAttribute("year"));
                                    System.out.println("Unit: "+ c.getCurrentRow().getAttribute("unitCd")+
                                                       " Month: "+ c.getCurrentRow().getAttribute("month")+
                                                       " Year: "+ c.getCurrentRow().getAttribute("year")+
                                                       " Level: "+ c.getCurrentRow().getAttribute("lvlCd"));                
                        }
                }
                //*************Find File name***************//
                OperationBinding binding = ADFUtils.findOperation("fileNameForPrint");
                binding.getParamsMap().put("fileId","HRM0000000227");
                binding.execute();
                //*************End Find File name***********//
                        System.out.println("Binding Result :"+binding.getResult());
                if(binding.getResult() != null){
                    String result = binding.getResult().toString();
                                      int last_index = result.lastIndexOf("/");
                    if(last_index==-1) {
                        last_index=result.lastIndexOf("\\");
                    }
                        String path = result.substring(0,last_index+1)+file_name;
                                    System.out.println("FILE PATH IS===>"+path);
                    InputStream input = new FileInputStream(path);
                    conn = getConnection( );
                    
                    JasperReport design = (JasperReport) JRLoader.loadObject(input);
                    System.out.println("Path : " + input + " -------" + design+" param:"+n);
                    
                    @SuppressWarnings("unchecked")
                    net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                    byte pdf[] = JasperExportManager.exportReportToPdf(print);
                    HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                    response.getOutputStream().write(pdf);
                    response.getOutputStream().flush();
                    response.getOutputStream().close();
                    facesContext.responseComplete();
                    }else
                    System.out.println("File Name/File Path not found.");
                    } catch (FileNotFoundException fnfe) {
                    // TODO: Add catch code
                    fnfe.printStackTrace();
                    try {
                        System.out.println("in finally connection closed");
                                conn.close( );
                                conn = null;
                        
                    } catch( SQLException e ) {
                        e.printStackTrace( );
                    }
                    } catch (Exception e) {
                    // TODO: Add catch code
                    e.printStackTrace();
                    try {
                        System.out.println("in finally connection closed");
                                conn.close( );
                                conn = null;
                        
                    } catch( SQLException e1 ) {
                        e1.printStackTrace( );
                    }
                    }finally {
                            try {
                                    System.out.println("in finally connection closed");
                                            conn.close( );
                                            conn = null;
                                    
                            } catch( SQLException e ) {
                                    e.printStackTrace( );
                            }
                    }
        } else {
                FacesMessage Message = new FacesMessage("Error, Please check Level Code.");
                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
                    System.out.println("Result is: "+opr.getResult().toString());
          }
        }
    }

    public void setLvlCdBinding(RichInputComboboxListOfValues lvlCdBinding) {
        this.lvlCdBinding = lvlCdBinding;
    }

    public RichInputComboboxListOfValues getLvlCdBinding() {
        return lvlCdBinding;
    }

    public void setMonthBinding(RichSelectOneChoice monthBinding) {
        this.monthBinding = monthBinding;
    }

    public RichSelectOneChoice getMonthBinding() {
        return monthBinding;
    }

    public void setYearBinding(RichInputText yearBinding) {
        this.yearBinding = yearBinding;
    }

    public RichInputText getYearBinding() {
        return yearBinding;
    }

    public void setUnitBinding(RichInputComboboxListOfValues unitBinding) {
        this.unitBinding = unitBinding;
    }

    public RichInputComboboxListOfValues getUnitBinding() {
        return unitBinding;
    }
    
    public Connection getConnection() throws Exception{
            Context ctx = null;
            Connection conn = null;
            ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/ApplicationDBDS");
            conn = ds.getConnection();
            return conn;
        }

}