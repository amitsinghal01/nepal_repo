package terms.hrm.setup.ui.bean;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

public class ShiftMasterBean {
    private RichTable shiftMasterBinding;
    private RichInputDate startTimeBind;
    private RichInputDate endTimeBind;
    private RichInputDate lunchStartTimeBind;
    private RichInputDate lunchEndTimeBind;

    public ShiftMasterBean() {
    }

    public void popUpDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            oracle.adf.model.OperationBinding op = null;
            ADFUtils.findOperation("Delete").execute();
            op = (oracle.adf.model.OperationBinding) ADFUtils.findOperation("Commit");
            if (getEditAction().equals("V")) {
                Object rst = op.execute();
            }
            System.out.println("Record Delete Successfully");
            if (op.getErrors().isEmpty()) {
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            } else if (!op.getErrors().isEmpty()) {
                oracle.adf.model.OperationBinding opr =
                    (oracle.adf.model.OperationBinding) ADFUtils.findOperation("Rollback");
                Object rstr = opr.execute();
                FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(shiftMasterBinding);
    }

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }


    public void setShiftMasterBinding(RichTable shiftMasterBinding) {
        this.shiftMasterBinding = shiftMasterBinding;
    }

    public RichTable getShiftMasterBinding() {
        return shiftMasterBinding;
    }

    private String editAction = "V";

    public void setStartTimeBind(RichInputDate startTimeBind) {
        this.startTimeBind = startTimeBind;
    }

    public RichInputDate getStartTimeBind() {
        return startTimeBind;
    }

    public void setEndTimeBind(RichInputDate endTimeBind) {
        this.endTimeBind = endTimeBind;
    }

    public RichInputDate getEndTimeBind() {
        return endTimeBind;
    }

    public void setLunchStartTimeBind(RichInputDate lunchStartTimeBind) {
        this.lunchStartTimeBind = lunchStartTimeBind;
    }

    public RichInputDate getLunchStartTimeBind() {
        return lunchStartTimeBind;
    }

    public void setLunchEndTimeBind(RichInputDate lunchEndTimeBind) {
        this.lunchEndTimeBind = lunchEndTimeBind;
    }

    public RichInputDate getLunchEndTimeBind() {
        return lunchEndTimeBind;
    }

}

