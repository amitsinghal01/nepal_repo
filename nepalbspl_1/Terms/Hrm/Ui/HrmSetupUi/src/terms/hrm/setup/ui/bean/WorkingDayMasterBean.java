package terms.hrm.setup.ui.bean;

import java.math.BigDecimal;

import java.time.LocalDate;

import java.time.YearMonth;
import java.time.format.DateTimeFormatter;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.domain.Timestamp;


public class WorkingDayMasterBean {
    private RichTable workingDayMasterTableBinding;
    private String editAction="V";
    private RichSelectOneChoice monthBinding;
    private RichInputText workDaysBinding;
    private RichInputDate salFromBinding;
    private RichInputDate salToBinding;
    private RichInputText yearBinding;

    public WorkingDayMasterBean() {
    }

    public void setWorkingDayMasterTableBinding(RichTable workingDayMasterTableBinding) {
        this.workingDayMasterTableBinding = workingDayMasterTableBinding;
    }

    public RichTable getWorkingDayMasterTableBinding() {
        return workingDayMasterTableBinding;
    }

    public void popUpDialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
        {
        OperationBinding op = null;
        ADFUtils.findOperation("Delete").execute();
        op = (OperationBinding) ADFUtils.findOperation("Commit");
        if (getEditAction().equals("V")) {
        Object rst = op.execute();
        }
        System.out.println("Record Delete Successfully");
        if(op.getErrors().isEmpty()){
        FacesMessage Message = new FacesMessage("Record Deleted Successfully");
        Message.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        } else if (!op.getErrors().isEmpty())
        {
        OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
        Object rstr = opr.execute();
        FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
        Message.setSeverity(FacesMessage.SEVERITY_ERROR);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(workingDayMasterTableBinding);
    }

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public void createAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert").execute();
        OperationBinding ob= ADFUtils.findOperation("serialNoInsert");
        ob.getParamsMap().put("SrNo", ADFUtils.evaluateEL("#{bindings.WorkingDaysMasterVO1Iterator.estimatedRowCount}"));
        ob.execute();
    }

    public void monthChangeVL(ValueChangeEvent mv) {
        mv.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(mv!=null){
        int mon=((Integer)mv.getNewValue());
            String i = (String)(mon==1 ?"JAN": mon==2  ?"FEB": mon==3  ?"MAR": mon==4 ?"APR" :
                                mon==5 ?"MAY": mon==6  ?"JUN": mon==7  ?"JUL": mon==8 ?"AUG" :
                                mon==9 ?"SEP": mon==10 ?"OCT": mon==11 ?"NOV": "DEC");
            workDaysBinding.setValue(
                             i.equals("JAN")?31: i.equals("FEB")?28: i.equals("MAR")?31: i.equals("APR")?30:
                             i.equals("MAY")?31: i.equals("JUN")?30: i.equals("JUL")?31: i.equals("AUG")?31:
                             i.equals("SEP")?30: i.equals("OCT")?31: i.equals("NOV")?30: 31);            
        }
        Integer mn = (Integer) mv.getNewValue();
        String month = 
            (mn.equals(1) ? "01" :
             mn.equals(2) ? "02" :
             mn.equals(3) ? "03" :
             mn.equals(4) ? "04" :
             mn.equals(5) ? "05" :
             mn.equals(6) ? "06" :
             mn.equals(7) ? "07" :
             mn.equals(8) ? "08" :
             mn.equals(9) ? "09" :
             mn.equals(10)? "10" :
             mn.equals(11)? "11" : "12");
        System.out.println("Month is======> " + month);
        String from_date = yearBinding.getValue() + "-" + month + "-01";
        System.out.println("From Date is======> " + from_date);
        oracle.jbo.domain.Date from_date_jbo = new oracle.jbo.domain.Date(from_date);

        String dateFormat =month+"/"+yearBinding.getValue();
        System.out.println("Date Format For Last Date Of Month=> "+dateFormat);
        DateTimeFormatter pattern = DateTimeFormatter.ofPattern("MM/yyyy");
        YearMonth yearMonth = YearMonth.parse(dateFormat, pattern);
        System.out.println("Year Month=> "+yearMonth);
        LocalDate date = yearMonth.atEndOfMonth();
        System.out.println("Last Date Of Month is==> "+date);
        System.out.println("Last Date of Month is Second==> "+date.lengthOfMonth());

        String to_date = yearBinding.getValue() + "-" + month+"-"+date.lengthOfMonth();
        System.out.println("To Date IS=> "+to_date);
        oracle.jbo.domain.Date to_date_jbo = new oracle.jbo.domain.Date(to_date);
        System.out.println("To Date in JBO=> "+to_date_jbo);
        
        salFromBinding.setValue(from_date_jbo);
        salToBinding.setValue(to_date_jbo);
        
    }
    
    public void setMonthBinding(RichSelectOneChoice monthBinding) {
        this.monthBinding = monthBinding;
    }

    public RichSelectOneChoice getMonthBinding() {
        return monthBinding;
    }

    public void setWorkDaysBinding(RichInputText workDaysBinding) {
        this.workDaysBinding = workDaysBinding;
    }

    public RichInputText getWorkDaysBinding() {
        return workDaysBinding;
    }

    public void fromdateVl(ValueChangeEvent fd) {
        fd.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(fd!=null && getSalFromBinding().getValue()!=null && getSalToBinding().getValue()!=null){
            oracle.jbo.domain.Date st = (oracle.jbo.domain.Date)getSalFromBinding().getValue();
            oracle.jbo.domain.Date et = (oracle.jbo.domain.Date)getSalToBinding().getValue();
            
            java.sql.Date s1=st.dateValue();
            java.sql.Date s2=et.dateValue();
            int n = (int) ((s2.getTime() - s1.getTime()) / (1000 * 60 * 60 * 24))+1;
            System.out.println("no of days: "+n);
//            Long t = (Long)(et.getTime() - st.getTime()+1)/ 86400000;
//            int t1=t.intValue();
//            System.out.println("diff: "+(t1));
            getWorkDaysBinding().setValue(new BigDecimal(n));
        }
    }


    public void setSalFromBinding(RichInputDate salFromBinding) {
        this.salFromBinding = salFromBinding;
    }

    public RichInputDate getSalFromBinding() {
        return salFromBinding;
    }

    public void setSalToBinding(RichInputDate salToBinding) {
        this.salToBinding = salToBinding;
    }

    public RichInputDate getSalToBinding() {
        return salToBinding;
    }

    public void setYearBinding(RichInputText yearBinding) {
        this.yearBinding = yearBinding;
    }

    public RichInputText getYearBinding() {
        return yearBinding;
    }
}
