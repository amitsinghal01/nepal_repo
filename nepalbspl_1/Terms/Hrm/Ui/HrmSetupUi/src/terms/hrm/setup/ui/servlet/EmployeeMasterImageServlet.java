package terms.hrm.setup.ui.servlet;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.naming.InitialContext;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import javax.sql.DataSource;

@WebServlet(name = "EmployeeMasterImageServlet", urlPatterns = { "/employeemasterimageservlet" })
public class EmployeeMasterImageServlet extends HttpServlet {
    private static final String CONTENT_TYPE = "text/html; charset=UTF-8";

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType(CONTENT_TYPE);
        String path = (request.getParameter("path"));
        System.out.println("Path: " + path);


        OutputStream os = response.getOutputStream();
        //If path is null or file is not an image
        //        if (path.equalsIgnoreCase("No")) {
        //            path = "D:\\ADF\\NoImage.png";
        //        }
        //        if (request.getParameter("path") == "") {
        //            path = "D:\\ADF\\NoImage.png";
        //        }
        InputStream inputStream = null;

        try {
            File outputFile = new File(path);
            inputStream = new FileInputStream(outputFile);
            BufferedInputStream in = new BufferedInputStream(inputStream);
            int b;
            byte[] buffer = new byte[10240];
            while ((b = in.read(buffer, 0, 10240)) != -1) {
                os.write(buffer, 0, b);
            }


        } catch (Exception e) {

            System.out.println(e);
        } finally {
            if (os != null) {
                os.close();
            }
            if (inputStream != null) {
                inputStream.close();
            }

        }


    }
}
