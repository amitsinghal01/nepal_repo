package terms.hrm.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;

import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;

import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichPanelLabelAndMessage;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

public class ApplicationForShiftChangeBean {
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText entryNumberBinding;
    private RichInputComboboxListOfValues employeeCodeBinding;
    private RichInputDate shiftDateBinding;
    private RichInputText changedShiftBinding;
    private RichInputComboboxListOfValues recommendedByBinding;
    private RichInputText approvedByBinding;
    private RichInputDate entryDateBinding;
    private RichInputDate recommendedDateBinding;
    private RichInputDate approvedDateBinding;
    private RichPanelLabelAndMessage bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichInputText allocatedShiftBinding;
    private RichInputComboboxListOfValues approvedByBinding1;
    private RichInputComboboxListOfValues chnageShiftBinding;


    public ApplicationForShiftChangeBean() {
    }

    public void ApplicationForShiftChangeSaveAL(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedBy("ApplicationForShiftChangeVO1Iterator","LastUpdatedBy");
        OperationBinding op = ADFUtils.findOperation("getApplicationForShiftChangeEntryNumber");
        Object rst = op.execute();

        System.out.println("Result is===> " + op.getResult());


        System.out.println("--------Commit-------");
        System.out.println("value aftr getting result--->?" + rst);


        if (rst.toString() != null && rst.toString() != "" && rst.toString() != "N") {
            if (op.getErrors().isEmpty()) {
                ADFUtils.findOperation("Commit").execute();
                FacesMessage Message = new FacesMessage("Record Saved Successfully. New Entry No. is " + rst + ".");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
                ADFUtils.setEL("#{pageFlowScope.mode}", "V");
                cevmodecheck();
                AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);

            }
        }

        if (rst.toString().equalsIgnoreCase("N")) {
            if (op.getErrors().isEmpty()) {
                ADFUtils.findOperation("Commit").execute();
                FacesMessage Message = new FacesMessage("Record Update Successfully.");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
                ADFUtils.setEL("#{pageFlowScope.mode}", "V");
                cevmodecheck();
                AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);

            }


        }
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setEntryNumberBinding(RichInputText entryNumberBinding) {
        this.entryNumberBinding = entryNumberBinding;
    }

    public RichInputText getEntryNumberBinding() {
        return entryNumberBinding;
    }

    public void setEmployeeCodeBinding(RichInputComboboxListOfValues employeeCodeBinding) {
        this.employeeCodeBinding = employeeCodeBinding;
    }

    public RichInputComboboxListOfValues getEmployeeCodeBinding() {
        return employeeCodeBinding;
    }

    public void setShiftDateBinding(RichInputDate shiftDateBinding) {
        this.shiftDateBinding = shiftDateBinding;
    }

    public RichInputDate getShiftDateBinding() {
        return shiftDateBinding;
    }


    public void setChangedShiftBinding(RichInputText changedShiftBinding) {
        this.changedShiftBinding = changedShiftBinding;
    }

    public RichInputText getChangedShiftBinding() {
        return changedShiftBinding;
    }

    public void setRecommendedByBinding(RichInputComboboxListOfValues recommendedByBinding) {
        this.recommendedByBinding = recommendedByBinding;
    }

    public RichInputComboboxListOfValues getRecommendedByBinding() {
        return recommendedByBinding;
    }

    public void setApprovedByBinding(RichInputText approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputText getApprovedByBinding() {
        return approvedByBinding;
    }

    public void setEntryDateBinding(RichInputDate entryDateBinding) {
        this.entryDateBinding = entryDateBinding;
    }

    public RichInputDate getEntryDateBinding() {
        return entryDateBinding;
    }

    public void setRecommendedDateBinding(RichInputDate recommendedDateBinding) {
        this.recommendedDateBinding = recommendedDateBinding;
    }

    public RichInputDate getRecommendedDateBinding() {
        return recommendedDateBinding;
    }

    public void setApprovedDateBinding(RichInputDate approvedDateBinding) {
        this.approvedDateBinding = approvedDateBinding;
    }

    public RichInputDate getApprovedDateBinding() {
        return approvedDateBinding;
    }


    public void setBindingOutputText(RichPanelLabelAndMessage bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichPanelLabelAndMessage getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }

    public void cevModeDisableComponent(String mode) {
        if (mode.equals("E")) {
            getUnitCodeBinding().setDisabled(true);
            getEntryNumberBinding().setDisabled(true);
            getEntryDateBinding().setDisabled(true);
            getEmployeeCodeBinding().setDisabled(false);
            getShiftDateBinding().setDisabled(false);
            // getChangedShiftBinding().setDisabled(false);
            getRecommendedByBinding().setDisabled(false);
            getRecommendedDateBinding().setDisabled(true);
            //getApprovedByBinding().setDisabled(false);
            getApprovedDateBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            getAllocatedShiftBinding().setDisabled(true);

        } else if (mode.equals("C")) {

            getUnitCodeBinding().setDisabled(true);
            getEntryNumberBinding().setDisabled(true);
            getEntryDateBinding().setDisabled(true);
            getEmployeeCodeBinding().setDisabled(false);
            getShiftDateBinding().setDisabled(false);
            // getChangedShiftBinding().setDisabled(false);
            getRecommendedByBinding().setDisabled(false);
            getRecommendedDateBinding().setDisabled(true);

            // getApprovedByBinding().setDisabled(true);
            getApprovedDateBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            getAllocatedShiftBinding().setDisabled(false);
            getAllocatedShiftBinding().setDisabled(true);
            getApprovedByBinding1().setDisabled(true);

        } else if (mode.equals("V")) {
            getUnitCodeBinding().setDisabled(true);
        }

    }

    public void editButtonAL(ActionEvent actionEvent) {
        if (getApprovedByBinding1().getValue() != null) {
            ADFUtils.showMessage("Approved shift can not be edited.", 2);
        }

        else {
            //        OperationBinding op = ADFUtils.findOperation("validateAuthForapplicationForShiftChange");
            //          Object rst =   op.execute();
            //            System.out.println("Result is===> " + op.getResult());
            //            if(op.getResult().toString().equalsIgnoreCase("Y")){
            cevmodecheck();
            //            }else{
            //                ADFUtils.showMessage("You are not authorized to approve.", 2);
            //            }
        }
    }

    public void setAllocatedShiftBinding(RichInputText allocatedShiftBinding) {
        this.allocatedShiftBinding = allocatedShiftBinding;
    }

    public RichInputText getAllocatedShiftBinding() {
        return allocatedShiftBinding;
    }

    public void setApprovedByBinding1(RichInputComboboxListOfValues approvedByBinding1) {
        this.approvedByBinding1 = approvedByBinding1;
    }

    public RichInputComboboxListOfValues getApprovedByBinding1() {
        return approvedByBinding1;
    }

    public void shiftDateVCL(ValueChangeEvent valueChangeEvent) {
        oracle.jbo.domain.Timestamp d = (oracle.jbo.domain.Timestamp) valueChangeEvent.getNewValue();
        System.out.println(d);

        OperationBinding op = ADFUtils.findOperation("getShift");
        op.getParamsMap().put("date", d);
        Object rst = op.execute();
    }

    public void approvedByVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce.getNewValue() != null) {
            OperationBinding op = ADFUtils.findOperation("validateAuthForapplicationForShiftChange");
            Object rst = op.execute();
            System.out.println("Result is===> " + op.getResult());
            if (op.getResult().toString().equalsIgnoreCase("N")) {
                Row row = (Row) ADFUtils.evaluateEL("#{bindings.ApplicationForShiftChangeVO1Iterator.currentRow}");
                row.setAttribute("ApprovedDate", null);
                row.setAttribute("ApprovedBy", null);
                ADFUtils.showMessage("You are not authorized to approve.", 2);
            }
        }
    }

    public void changeTypeVCL(ValueChangeEvent valueChangeEvent) {

        if (valueChangeEvent != null) {
            System.out.println("Shift Type--->" + valueChangeEvent.getNewValue());
            String flag = (String) valueChangeEvent.getNewValue();
            if (flag.equalsIgnoreCase("SS") || flag.equalsIgnoreCase("OS")) {
                getChnageShiftBinding().setDisabled(false);
            } else
                //                String value = "WO";
                getChnageShiftBinding().setDisabled(true);

        }
    }

    public void setChnageShiftBinding(RichInputComboboxListOfValues chnageShiftBinding) {
        this.chnageShiftBinding = chnageShiftBinding;
    }

    public RichInputComboboxListOfValues getChnageShiftBinding() {
        return chnageShiftBinding;
    }

}
