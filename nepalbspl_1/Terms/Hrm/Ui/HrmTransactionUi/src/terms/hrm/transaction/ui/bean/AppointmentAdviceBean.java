package terms.hrm.transaction.ui.bean;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;

import java.sql.Connection;

import java.sql.SQLException;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.faces.application.FacesMessage;

import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import javax.naming.Context;
import javax.naming.InitialContext;

import javax.servlet.http.HttpServletResponse;

import javax.sql.DataSource;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.output.RichPanelCollection;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.adfinternal.view.faces.context.AdfFacesContextImpl;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

public class AppointmentAdviceBean {
    private RichPanelCollection appointmentAdviceBeanTableBinding;
    private String editAction = "V";
    private String pMode = "";
    String file_name = "";

    public BindingContainer getBindings() {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
    }

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public AppointmentAdviceBean() {
    }

    public void setAppointmentAdviceBeanTableBinding(RichPanelCollection appointmentAdviceBeanTableBinding) {
        this.appointmentAdviceBeanTableBinding = appointmentAdviceBeanTableBinding;
    }

    public RichPanelCollection getAppointmentAdviceBeanTableBinding() {
        return appointmentAdviceBeanTableBinding;
    }

    public void popUpDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            OperationBinding op = null;
            ADFUtils.findOperation("Delete").execute();
            op = (OperationBinding) ADFUtils.findOperation("Commit");
            if (getEditAction().equals("V")) {
                Object rst = op.execute();
            }
            System.out.println("Record Delete Successfully");
            if (op.getErrors().isEmpty()) {
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            } else if (!op.getErrors().isEmpty()) {
                OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
                Object rstr = opr.execute();
                FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            }
        }
        AdfFacesContextImpl.getCurrentInstance().addPartialTarget(appointmentAdviceBeanTableBinding);
    }

    public void saveButtonAppointmentAdviceAL(ActionEvent actionEvent) {
        OperationBinding op = ADFUtils.findOperation("generateAppointmentEntryNumber");
        Object rst = op.execute();

        System.out.println("Result is===> " + op.getResult());


        System.out.println("--------Commit-------");
        System.out.println("value aftr getting result--->?" + rst);


        if (rst.toString() != null && rst.toString() != "" && rst.toString() != "N") {
            if (op.getErrors().isEmpty()) {
                ADFUtils.findOperation("Commit").execute();
                FacesMessage Message = new FacesMessage("Record Saved Successfully. New Entry Number is " + rst + ".");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);

            }
        }

        if (rst.toString().equalsIgnoreCase("N")) {
            if (op.getErrors().isEmpty()) {
                ADFUtils.findOperation("Commit").execute();
                FacesMessage Message = new FacesMessage("Record Update Successfully.");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);

            }


        }
    }


    public void downloadJrxml(FacesContext facesContext, OutputStream outputStream) {
        Connection conn=null;
        try {

            System.out.println(" pmode:" + pMode);

            Map n = new HashMap();
            if (pMode != null) {
                if (pMode.equalsIgnoreCase("A")) {
                    file_name = "r_appointment_advice_ho_staff.jasper";
                    DCIteratorBinding paySlip = (DCIteratorBinding) getBindings().get("AppointmentAdviceVO1Iterator");
                    System.out.println("Unit: "+paySlip.getCurrentRow().getAttribute("UnitCd").toString());
                    System.out.println("Level: "+paySlip.getCurrentRow().getAttribute("LevelCode"));
                    System.out.println("EntryNO: "+paySlip.getCurrentRow().getAttribute("EntryNo").toString());
                    n.put("p_unit", paySlip.getCurrentRow().getAttribute("UnitCd").toString());
                    n.put("p_lvl", paySlip.getCurrentRow().getAttribute("LevelCode"));
                    n.put("p_entry_no", paySlip.getCurrentRow().getAttribute("EntryNo").toString());

                } else if (pMode.equalsIgnoreCase("B")) {
                    file_name = "r_appt_advice_ho_officer.jasper";
                    DCIteratorBinding paySlip = (DCIteratorBinding) getBindings().get("AppointmentAdviceVO1Iterator");
                    n.put("p_unit", paySlip.getCurrentRow().getAttribute("UnitCd").toString());
                    n.put("p_lvl", paySlip.getCurrentRow().getAttribute("LevelCode"));
                    n.put("p_entry_no", paySlip.getCurrentRow().getAttribute("EntryNo").toString());
                } else if (pMode.equalsIgnoreCase("C")) {
                    file_name = "r_appt_advice_dol_officr.jasper";
                    DCIteratorBinding paySlip = (DCIteratorBinding) getBindings().get("AppointmentAdviceVO1Iterator");
                    n.put("p_unit", paySlip.getCurrentRow().getAttribute("UnitCd").toString());
                    n.put("p_lvl", paySlip.getCurrentRow().getAttribute("LevelCode"));
                    n.put("p_entry_no", paySlip.getCurrentRow().getAttribute("EntryNo").toString());
                }
            }

            OperationBinding binding = ADFUtils.findOperation("fileNameForPrint");
            binding.getParamsMap().put("fileId", "ERP0000000120");
            binding.execute();
            System.out.println("Binding Result :" + binding.getResult());
            if (binding.getResult() != null) {
                String result = binding.getResult().toString();
                int last_index = result.lastIndexOf("/");
                if (last_index == -1) {
                    last_index = result.lastIndexOf("\\");
                }
                String path = result.substring(0, last_index + 1) + file_name;
                System.out.println("FILE PATH IS===>" + path);
                InputStream input = new FileInputStream(path);
                //    InputStream input = new FileInputStream("//home//beta3//Jasper//Jour_prn1.jrxml");

                conn = getConnection( );
                
                JasperReport design = (JasperReport) JRLoader.loadObject(input);
                System.out.println("Path : " + input + " -------" + design+" param:"+n);
                
                @SuppressWarnings("unchecked")
                net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                byte pdf[] = JasperExportManager.exportReportToPdf(print);
                HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                response.getOutputStream().write(pdf);
                response.getOutputStream().flush();
                response.getOutputStream().close();
                facesContext.responseComplete();
                }else
                System.out.println("File Name/File Path not found.");
                } catch (FileNotFoundException fnfe) {
                // TODO: Add catch code
                fnfe.printStackTrace();
                try {
                    System.out.println("in finally connection closed");
                            conn.close( );
                            conn = null;
                    
                } catch( SQLException e ) {
                    e.printStackTrace( );
                }
                } catch (Exception e) {
                // TODO: Add catch code
                e.printStackTrace();
                try {
                    System.out.println("in finally connection closed");
                            conn.close( );
                            conn = null;
                    
                } catch( SQLException e1 ) {
                    e1.printStackTrace( );
                }
                }finally {
                        try {
                                System.out.println("in finally connection closed");
                                        conn.close( );
                                        conn = null;
                                
                        } catch( SQLException e ) {
                                e.printStackTrace( );
                        }
                }
    }

    public void setPMode(String pMode) {
        this.pMode = pMode;
    }

    public String getPMode() {
        return pMode;
    }
    
    public Connection getConnection() throws Exception{
            Context ctx = null;
            Connection conn = null;
            ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/ApplicationDBDS");
            conn = ds.getConnection();
            return conn;
        }
}


