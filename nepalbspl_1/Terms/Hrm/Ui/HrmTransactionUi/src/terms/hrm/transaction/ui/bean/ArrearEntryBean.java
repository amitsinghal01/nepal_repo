package terms.hrm.transaction.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichPopup;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;
import oracle.jbo.ViewObject;

import terms.hrm.transaction.model.view.ArrearEntryDaysDetailVORowImpl;

public class ArrearEntryBean {
    private RichPopup arrearDaysDetailPopup;
    private RichInputText nodaysBinding;
    private RichSelectOneChoice arrearTypeBinding;
    private RichInputComboboxListOfValues unitcdBinding;
    private RichInputComboboxListOfValues empnoBinding;
    private RichSelectOneChoice salmonthBinding;
    private RichInputText salyearBinding;
    private RichSelectOneChoice arrmonthBinding;
    private RichInputText arryearBinding;
    private RichInputText subcodeearBinding;
    private RichInputText subcode1earBinding;
    private RichInputText headcodeearBinding;
    private RichInputText amountearBinding;
    private RichInputText subcodededBinding;
    private RichInputText subcode1dedBinding;
    private RichInputText headcodededBinding;
    private RichInputText amountdedBinding;
    private RichOutputText bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton detailbtnBinding;
    public String localmode="C";
    private RichInputText oldnoofdays;
    private RichButton populatebtnBinding;
    private RichSelectOneChoice typeBinding;
    private RichSelectOneChoice dayTypeBinding;
    private RichOutputText noOfDaysBinding;
    private RichSelectOneChoice daysTypeBinding;
    private RichInputComboboxListOfValues dayDetailTypeBinding;
    private RichInputComboboxListOfValues daysTypeDetailBinding;
    private RichSelectOneChoice first2ndHalfBinding;
    private boolean flag=false;
  

    public ArrearEntryBean() {
    }

    public void detailPopBtnAL(ActionEvent actionEvent) {
        System.out.println("IN arrearEntryDetailPop1");
        OperationBinding op1 = ADFUtils.findOperation("arrearEntryDetailPop1");
        op1.execute();
        System.out.println("ob.getResult() in bean:"+op1.getResult());
        
        if(op1.getResult()!= null){
            String result=op1.getResult().toString().substring(0,1);
            String msgg=op1.getResult().toString().substring(op1.getResult().toString().indexOf("-")+1);
            System.out.println("result after substring:"+result);
            System.out.println("msg:"+msgg);
            if(result.equals("Y")){
              OperationBinding op = ADFUtils.findOperation("arrearEntryDetailPop");
                op.execute();
                oldnoofdays.setValue(nodaysBinding.getValue());
//                FacesMessage msg=new FacesMessage(msgg);
//                msg.setSeverity(FacesMessage.SEVERITY_INFO);
//                FacesContext fc=FacesContext.getCurrentInstance();
//                fc.addMessage(null, msg);
            }else{
                FacesMessage msg=new FacesMessage(msgg);
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc=FacesContext.getCurrentInstance();
                fc.addMessage(null, msg);
            }
        }
    }

    public void populateBtnAL(ActionEvent actionEvent) {
//        if(flag){
            
            if(empnoBinding.getValue() != null && salmonthBinding.getValue() != null && salyearBinding.getValue() != null
            && arrmonthBinding.getValue() != null && arryearBinding.getValue() != null) {
                System.out.println("IN arrearEntryDaysDetailPop");
                DCIteratorBinding binding = ADFUtils.findIterator("ArrearEntryDaysDetailVO1Iterator");
                if (binding.getEstimatedRowCount() < 1 && binding.getCurrentRow() == null){
                OperationBinding op = ADFUtils.findOperation("arrearEntryDaysDetailPop");
                op.execute();
                
                empnoBinding.setDisabled(true);
                salmonthBinding.setDisabled(true);
                salyearBinding.setDisabled(true);
                arrmonthBinding.setDisabled(true);
                arryearBinding.setDisabled(true);
                }
                RichPopup.PopupHints pop=new RichPopup.PopupHints();
                arrearDaysDetailPopup.show(pop);
            }
           
            
//        }else{
//            ADFUtils.showMessage("Arrear Year can't be greater than Salary Year", 2);
//        }
    }

    public void setArrearDaysDetailPopup(RichPopup arrearDaysDetailPopup) {
        this.arrearDaysDetailPopup = arrearDaysDetailPopup;
    }

    public RichPopup getArrearDaysDetailPopup() {
        return arrearDaysDetailPopup;
    }

    public void arrearDaysDetailDL(DialogEvent dialogEvent) {
    if(dialogEvent.getOutcome().name().equals("ok"))
    {
                   BigDecimal Sum= new BigDecimal(0);
                   DCIteratorBinding binding = ADFUtils.findIterator("ArrearEntryDaysDetailVO1Iterator");
                   if (binding.getEstimatedRowCount() > 0 && binding.getCurrentRow() != null) {  
        ViewObject voh = binding.getViewObject();
                       RowSetIterator row1=voh.createRowSetIterator(null); 
                                                while (row1.hasNext()) {
                      ArrearEntryDaysDetailVORowImpl row = (ArrearEntryDaysDetailVORowImpl)row1.next() ;
//                       ViewObject voh = binding.getViewObject();
//                     
//                              Row r2=row1.next();row1
                                                    System.out.println("Check VALue1: "+row.getNoOfDays());
                                                    Sum=Sum.add(row.getNoOfDays()!=null?row.getNoOfDays():new BigDecimal(0));
                                                    System.out.println("Check VALue2: "+row.getNoOfDays());
                              System.out.println("no of Days=="+Sum);
                   }
                                                row1.closeRowSetIterator();
                            nodaysBinding.setValue(Sum);
                            
                            if(Sum.compareTo(new BigDecimal(0))==-1){
                                arrearTypeBinding.setValue("D");
                            }else{
                                arrearTypeBinding.setValue("E");
                            }
                            
        if(nodaysBinding.getValue()== null){
            getDetailbtnBinding().setDisabled(true);
            System.out.println("if condn");
        }else{
            System.out.println("else condn");
            getDetailbtnBinding().setDisabled(false);
        }
    }
                   
}
    }

    public void setNodaysBinding(RichInputText nodaysBinding) {
        this.nodaysBinding = nodaysBinding;
    }

    public RichInputText getNodaysBinding() {
        return nodaysBinding;
    }

    public void SaveAL(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedBy("ArrearEntryHeaderVO1Iterator","LastUpdatedBy");
        System.out.println("#{pageFlowScope.mode}"+ADFUtils.resolveExpression("#{pageFlowScope.mode}"));
        System.out.println("nodaysBinding.getValue() && oldnoofdays.getValue():"+nodaysBinding.getValue()+"  "+oldnoofdays.getValue());
        if(nodaysBinding.getValue()!= null && oldnoofdays.getValue()!= null){
         BigDecimal noofdays = (BigDecimal) nodaysBinding.getValue();
         BigDecimal oldnoofday = (BigDecimal) oldnoofdays.getValue();
            if(noofdays.compareTo(oldnoofday)==0){
                System.out.println("in condition"+noofdays.compareTo(oldnoofday));
        if (localmode.equalsIgnoreCase("C")){
            System.out.println("Before Commit");
            ADFUtils.findOperation("Commit").execute();
            ADFUtils.showMessage("Record Save Successfully.", 2);
           ADFUtils.setEL("#{pageFlowScope.mode}", "V");
           System.out.println("after setting"+ADFUtils.resolveExpression("#{pageFlowScope.mode}"));
           cevmodecheck();
           AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
       } else if(localmode.equalsIgnoreCase("E")) {
               System.out.println("Before Commit");
            ADFUtils.findOperation("Commit").execute();
           ADFUtils.showMessage(" Record Updated Successfully", 2);
               ADFUtils.setEL("#{pageFlowScope.mode}", "V");
               System.out.println("after setting"+ADFUtils.resolveExpression("#{pageFlowScope.mode}"));
               cevmodecheck();
               AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
           }}else{
                
               ADFUtils.showMessage("Please click on Detail button.", 0);
           }
        }else{
               ADFUtils.showMessage("Please click on Detail button.", 0);
           }
    }

    public void setArrearTypeBinding(RichSelectOneChoice arrearTypeBinding) {
        this.arrearTypeBinding = arrearTypeBinding;
    }

    public RichSelectOneChoice getArrearTypeBinding() {
        return arrearTypeBinding;
    }

    public void setUnitcdBinding(RichInputComboboxListOfValues unitcdBinding) {
        this.unitcdBinding = unitcdBinding;
    }

    public RichInputComboboxListOfValues getUnitcdBinding() {
        return unitcdBinding;
    }

    public void setEmpnoBinding(RichInputComboboxListOfValues empnoBinding) {
        this.empnoBinding = empnoBinding;
    }

    public RichInputComboboxListOfValues getEmpnoBinding() {
        return empnoBinding;
    }

    public void setSalmonthBinding(RichSelectOneChoice salmonthBinding) {
        this.salmonthBinding = salmonthBinding;
    }

    public RichSelectOneChoice getSalmonthBinding() {
        return salmonthBinding;
    }

    public void setSalyearBinding(RichInputText salyearBinding) {
        this.salyearBinding = salyearBinding;
    }

    public RichInputText getSalyearBinding() {
        return salyearBinding;
    }

    public void setArrmonthBinding(RichSelectOneChoice arrmonthBinding) {
        this.arrmonthBinding = arrmonthBinding;
    }

    public RichSelectOneChoice getArrmonthBinding() {
        return arrmonthBinding;
    }

    public void setArryearBinding(RichInputText arryearBinding) {
        this.arryearBinding = arryearBinding;
    }

    public RichInputText getArryearBinding() {
        return arryearBinding;
    }

    public void setSubcodeearBinding(RichInputText subcodeearBinding) {
        this.subcodeearBinding = subcodeearBinding;
    }

    public RichInputText getSubcodeearBinding() {
        return subcodeearBinding;
    }

    public void setSubcode1earBinding(RichInputText subcode1earBinding) {
        this.subcode1earBinding = subcode1earBinding;
    }

    public RichInputText getSubcode1earBinding() {
        return subcode1earBinding;
    }

    public void setHeadcodeearBinding(RichInputText headcodeearBinding) {
        this.headcodeearBinding = headcodeearBinding;
    }

    public RichInputText getHeadcodeearBinding() {
        return headcodeearBinding;
    }

    public void setAmountearBinding(RichInputText amountearBinding) {
        this.amountearBinding = amountearBinding;
    }

    public RichInputText getAmountearBinding() {
        return amountearBinding;
    }

    public void setSubcodededBinding(RichInputText subcodededBinding) {
        this.subcodededBinding = subcodededBinding;
    }

    public RichInputText getSubcodededBinding() {
        return subcodededBinding;
    }

    public void setSubcode1dedBinding(RichInputText subcode1dedBinding) {
        this.subcode1dedBinding = subcode1dedBinding;
    }

    public RichInputText getSubcode1dedBinding() {
        return subcode1dedBinding;
    }

    public void setHeadcodededBinding(RichInputText headcodededBinding) {
        this.headcodededBinding = headcodededBinding;
    }

    public RichInputText getHeadcodededBinding() {
        return headcodededBinding;
    }

    public void setAmountdedBinding(RichInputText amountdedBinding) {
        this.amountdedBinding = amountdedBinding;
    }

    public RichInputText getAmountdedBinding() {
        return amountdedBinding;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }
    
    private void cevmodecheck(){
            if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
                cevModeDisableComponent("V");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("C");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("E");
            }
        }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                try {
                    Method method1 =
                        component.getClass().getMethod("setDisabled", boolean.class);
                    if (method1 != null) {
                        method1.invoke(component, valueComponent);
                    }
                } catch (NoSuchMethodException e) {
                    try {
                        Method method =
                            component.getClass().getMethod("setReadOnly", boolean.class);
                        if (method != null) {
                            method.invoke(component, valueComponent);
                        }
                    } catch (Exception e1) {
                        // e.printStackTrace();//silently eat this exception.
                    }


                } catch (Exception e) {
                    // e.printStackTrace();//silently eat this exception.
                }
                List<UIComponent> childComponents = component.getChildren();
                for (UIComponent comp : childComponents) {
                    makeComponentHierarchyReadOnly(comp, valueComponent);
                }
            }
    
        public void cevModeDisableComponent(String mode) {
            if (mode.equals("E")) {
                localmode="E";
                
                getNodaysBinding().setDisabled(true);
                getArrearTypeBinding().setDisabled(true);
                getUnitcdBinding().setDisabled(true);
                getEmpnoBinding().setDisabled(true);
                getSalmonthBinding().setDisabled(true);
                getSalyearBinding().setDisabled(true);
                getArrmonthBinding().setDisabled(true);
                getArryearBinding().setDisabled(true);
                oldnoofdays.setValue(nodaysBinding.getValue());
                getDetailbtnBinding().setDisabled(false);
                getPopulatebtnBinding().setDisabled(false);
                
            } else if (mode.equals("C")) {
                localmode="C";
                if(nodaysBinding.getValue()== null){
                    getDetailbtnBinding().setDisabled(true);
                    System.out.println("if condn");
                }else{
                    System.out.println("else condn");
                    getDetailbtnBinding().setDisabled(false);
                }
                getUnitcdBinding().setDisabled(true);
                getNodaysBinding().setDisabled(true);
                getArrearTypeBinding().setDisabled(true);
                getPopulatebtnBinding().setDisabled(false);
                
            } else if (mode.equals("V")) {
                localmode="V";
                getDetailbtnBinding().setDisabled(true);
                getPopulatebtnBinding().setDisabled(false);
            }
            
        }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setDetailbtnBinding(RichButton detailbtnBinding) {
        this.detailbtnBinding = detailbtnBinding;
    }

    public RichButton getDetailbtnBinding() {
        return detailbtnBinding;
    }

    public void setOldnoofdays(RichInputText oldnoofdays) {
        this.oldnoofdays = oldnoofdays;
    }

    public RichInputText getOldnoofdays() {
        return oldnoofdays;
    }

    public String SaveAndCloseAC() {
        ADFUtils.setLastUpdatedBy("ArrearEntryHeaderVO1Iterator","LastUpdatedBy");
        System.out.println("#{pageFlowScope.mode}"+ADFUtils.resolveExpression("#{pageFlowScope.mode}"));
        System.out.println("nodaysBinding.getValue() && oldnoofdays.getValue():"+nodaysBinding.getValue()+"  "+oldnoofdays.getValue());
        if(nodaysBinding.getValue()!= null && oldnoofdays.getValue()!= null){
         BigDecimal noofdays = (BigDecimal) nodaysBinding.getValue();
         BigDecimal oldnoofday = (BigDecimal) oldnoofdays.getValue();
            if(noofdays.compareTo(oldnoofday)==0){
                System.out.println("in condition"+noofdays.compareTo(oldnoofday));
        if (localmode.equalsIgnoreCase("C")){
            System.out.println("Before Commit");
            ADFUtils.findOperation("Commit").execute();
            ADFUtils.showMessage("Record Save Successfully.", 2);
            ADFUtils.setEL("#{pageFlowScope.mode}", "V");
            System.out.println("after setting"+ADFUtils.resolveExpression("#{pageFlowScope.mode}"));
            cevmodecheck();
            AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
            
        } else if(localmode.equalsIgnoreCase("E")) {
               System.out.println("Before Commit");
            ADFUtils.findOperation("Commit").execute();
           ADFUtils.showMessage(" Record Updated Successfully", 2);
               ADFUtils.setEL("#{pageFlowScope.mode}", "V");
               System.out.println("after setting"+ADFUtils.resolveExpression("#{pageFlowScope.mode}"));
               cevmodecheck();
               AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
           }
            return "SaveAndClose";
            }else{
                
               ADFUtils.showMessage("Please click on Detail button.", 0);
                return null;
           }
        }else{
               ADFUtils.showMessage("Please click on Detail button.", 0);
               return null;
           }
        
    }

    public void setPopulatebtnBinding(RichButton populatebtnBinding) {
        this.populatebtnBinding = populatebtnBinding;
    }

    public RichButton getPopulatebtnBinding() {
        return populatebtnBinding;
    }

    public void setTypeBinding(RichSelectOneChoice typeBinding) {
        this.typeBinding = typeBinding;
    }

    public RichSelectOneChoice getTypeBinding() {
        return typeBinding;
    }

    public void setDayTypeBinding(RichSelectOneChoice dayTypeBinding) {
        this.dayTypeBinding = dayTypeBinding;
    }

    public RichSelectOneChoice getDayTypeBinding() {
        return dayTypeBinding;
    }

    public void setNoOfDaysBinding(RichOutputText noOfDaysBinding) {
        this.noOfDaysBinding = noOfDaysBinding;
    }

    public RichOutputText getNoOfDaysBinding() {
        return noOfDaysBinding;
    }

    public void setDaysTypeBinding(RichSelectOneChoice daysTypeBinding) {
        this.daysTypeBinding = daysTypeBinding;
    }

    public RichSelectOneChoice getDaysTypeBinding() {
        return daysTypeBinding;
    }

//    public void daysTypeVCL(ValueChangeEvent vce) {
//        System.out.println("INSIDE vce ##### ");
//        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
//        
//        String type = "F";
//        String type1 = "H";
//        String ob1 = "3";
//        String ob = "Full Day";
//        System.out.println("INSIDE EEEE");
//        if(vce.getNewValue() != null) {
//            System.out.println("inside #### " + vce.getNewValue());
//            if(vce.getNewValue().equals(type)) {
//                System.out.println("INSIDE IF ##### ");
//                
//                daysTypeDetailBinding.setValue(ob1);
//            }
//            else if(vce.getNewValue().equals(type1)) {
//                System.out.println("INSIDE ELSE IF ##### ");
//                
//                daysTypeDetailBinding.setValue(dayTypeBinding);
//            }
//        }
//    }

    public void setDayDetailTypeBinding(RichInputComboboxListOfValues dayDetailTypeBinding) {
        this.dayDetailTypeBinding = dayDetailTypeBinding;
    }

    public RichInputComboboxListOfValues getDayDetailTypeBinding() {
        return dayDetailTypeBinding;
    }

    public void setDaysTypeDetailBinding(RichInputComboboxListOfValues daysTypeDetailBinding) {
        this.daysTypeDetailBinding = daysTypeDetailBinding;
    }

    public RichInputComboboxListOfValues getDaysTypeDetailBinding() {
        return daysTypeDetailBinding;
    }

    public void dayTypeVCL(ValueChangeEvent vce) {
        System.out.println("INSIDE dayTypeVCL #### ");
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        
        String dayType = "Full Day";
        String dayType1 = "3";
        
        if(first2ndHalfBinding.getValue().equals(dayType1) || first2ndHalfBinding.equals(dayType)) {
            System.out.println("inside if binding #### ");
            first2ndHalfBinding.setDisabled(true);
        }
    }

    public void setFirst2ndHalfBinding(RichSelectOneChoice first2ndHalfBinding) {
        this.first2ndHalfBinding = first2ndHalfBinding;
    }

    public RichSelectOneChoice getFirst2ndHalfBinding() {
        return first2ndHalfBinding;
    }

    public void arrearMonthBinding(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
    }

    public void arrearYearVCL(ValueChangeEvent vce) {
        System.out.println("INSIDE arrearYearVCL ##### ");
        
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        
        Integer salYear = (Integer)salyearBinding.getValue();
        Integer arrYear = (Integer)arryearBinding.getValue();
       
    
        if(vce.getNewValue() != null && arrmonthBinding.getValue() != null && salmonthBinding.getValue() != null && salyearBinding.getValue() != null && arryearBinding.getValue() != null) {
        int i = salmonthBinding.getValue().toString().equalsIgnoreCase("JAN")?1:
                salmonthBinding.getValue().toString().equalsIgnoreCase("FEB")?2:
                salmonthBinding.getValue().toString().equalsIgnoreCase("MAR")?3:
                salmonthBinding.getValue().toString().equalsIgnoreCase("APR")?4:
                salmonthBinding.getValue().toString().equalsIgnoreCase("MAY")?5:
                salmonthBinding.getValue().toString().equalsIgnoreCase("JUN")?6:
                salmonthBinding.getValue().toString().equalsIgnoreCase("JUL")?7:
                salmonthBinding.getValue().toString().equalsIgnoreCase("AUG")?8:
                salmonthBinding.getValue().toString().equalsIgnoreCase("SEP")?9:
                salmonthBinding.getValue().toString().equalsIgnoreCase("OCT")?10:
                salmonthBinding.getValue().toString().equalsIgnoreCase("NOV")?11:12;                
        System.out.println("INSIDE IF i ##### " + i);
        int j = arrmonthBinding.getValue().toString().equalsIgnoreCase("JAN")?1:
                arrmonthBinding.getValue().toString().equalsIgnoreCase("FEB")?2:
                arrmonthBinding.getValue().toString().equalsIgnoreCase("MAR")?3:
                arrmonthBinding.getValue().toString().equalsIgnoreCase("APR")?4:
                arrmonthBinding.getValue().toString().equalsIgnoreCase("MAY")?5:
                arrmonthBinding.getValue().toString().equalsIgnoreCase("JUN")?6:
                arrmonthBinding.getValue().toString().equalsIgnoreCase("JUL")?7:
                arrmonthBinding.getValue().toString().equalsIgnoreCase("AUG")?8:
                arrmonthBinding.getValue().toString().equalsIgnoreCase("SEP")?9:
                arrmonthBinding.getValue().toString().equalsIgnoreCase("OCT")?10:
                arrmonthBinding.getValue().toString().equalsIgnoreCase("NOV")?11:12; 
        System.out.println("INSIDE IF j ##### " + j);
        if(vce.getNewValue() != null && arrmonthBinding.getValue() != null && 
        salmonthBinding.getValue() != null && salyearBinding.getValue() != null && arryearBinding.getValue() != null)
        {
        if(((arrYear.compareTo(salYear) == -1) && (j > i)) || ((arrYear.compareTo(salYear) == 0) && (j < i))
        || ((arrYear.compareTo(salYear) == -1) && (j <= i))) {
                System.out.println("INSIDE IF #### ");
                flag=true;
        } else {
                flag=false;
                arrmonthBinding.setValue(null);
                System.out.println("INSIDE IF CONDITION YEAR #### ");
                ADFUtils.showMessage("Arrear Month and Year can't be greater than Salary Month and Year", 0);
           
                getPopulatebtnBinding().setDisabled(true);
                
        }
        }
        else {
            flag=false;
            arrmonthBinding.setValue(null);
            System.out.println("INSIDE ELSE IF CONDITION YEAR #### ");
            ADFUtils.showMessage("Arrear Month and Year can't be greater than Salary Month and Year", 0);
        }
        System.out.println("Month Value #### " + salmonthBinding.getValue() + "VCL Value " + vce.getNewValue() + "Binding " + arrmonthBinding.getValue());
    }  
//       if(flag==false)
//       {
//           getPopulatebtnBinding().setDisabled(true);
//           }
    }
        
//        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
//        
//        Integer salYear = (Integer)salyearBinding.getValue();
//        Integer arrYear = (Integer)arryearBinding.getValue();
//        System.out.println("Salary Year #### " + salYear + "Arrear Year " + arrYear);
//        
//        if(vce.getNewValue() != null) {
//            if(arrYear > salYear) {
//                System.out.println("INSIDE IF #### ");
//                ADFUtils.showMessage("Arrear Year can't be greater than Salary Year", 2);
//            }
//        }
//    }
}
