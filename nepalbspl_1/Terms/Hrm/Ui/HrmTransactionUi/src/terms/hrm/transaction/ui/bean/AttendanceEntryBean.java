package terms.hrm.transaction.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;
import javax.faces.validator.ValidatorException;

import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.model.binding.DCIteratorBindingDef;
import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;

import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.BindingContainer;
    import oracle.binding.ControlBinding;
   
import oracle.binding.OperationBinding;

public class AttendanceEntryBean {
    private RichInputText bindCL;
    private RichInputText bindSL;
    private RichInputText bindEL;
    private RichInputText bindLW;
    private RichInputText bindFO;
    private RichInputText bindOD;
    private RichInputText bindCO;
    private RichInputText bindWO;
    private RichInputText bindGP;
    private RichInputText bindShortDays;
    private RichInputText bindHO;
    private RichInputText bindML;
    private RichInputText bindAB;
    private RichInputText bindPR;
    private RichInputText bindD;
    private RichPanelHeader getMyPageRootComponent;
    private RichOutputText bindingOutputText;
    private RichButton headerEditBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText yearBinding;
    private RichSelectOneChoice month2Binding;
    private RichInputText employeeNumberBind;
    private RichInputText employeeNameBind;
    private RichInputText departmentNameBind;
    private RichInputText totalDaysBind;
    private RichInputText totalPaidDayBind;
    private RichButton detailcreateBinding;
    private RichButton detaildeleteBinding;
    private String Message="C";
    private RichInputComboboxListOfValues unitCode1Binding;
    private RichInputComboboxListOfValues unitCodeBinding1;
    private RichInputText yearBinding1;
    private RichInputComboboxListOfValues month2Binding1;
    private RichTable TABLEBINDING;
    private RichInputText bindEmployeeName;


    public AttendanceEntryBean() {
    }

    public void PopulateAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("CallProcedure1").execute();
        ADFUtils.findOperation("alldatafound").execute();
        
        
//    --------------------------------------------------------------------------------    
//        System.out.println("Unit Code value outside if is"+unitCodeBinding.getValue());
//        System.out.println("Year value outside if is"+yearBinding.getValue());
//        System.out.println("Month value outside if is"+month2Binding.getValue());
//        
//
//
//        if (unitCodeBinding1.getValue() != null || yearBinding1.getValue() != null || month2Binding1.getValue() != null) {
//            System.out.println("Unit Code value inside if is"+unitCodeBinding1.getValue());
//            System.out.println("Year value inside if is"+yearBinding1.getValue());
//            System.out.println("Month value inside if is"+month2Binding1.getValue());
//            
//            
//            String Unit = unitCodeBinding1.getValue().toString();
//            String Year1 =yearBinding1.getValue().toString();
//            String Mon = month2Binding1.getValue().toString();
//
//
//            System.out.println("data " + Unit + "," + Year1 + Mon);
//            OperationBinding ob = ADFUtils.findOperation("PopulateAttEntryData");
//            ob.getParamsMap().put("Unit", Unit);
//            ob.getParamsMap().put("Year1", Year1);
//            ob.getParamsMap().put("Mon", Mon);
////          AdfFacesContext.getCurrentInstance().addPartialTarget(bindPR);
//           // AdfFacesContext.getCurrentInstance().addPartialTarget(unitCodeBinding);  
//           // AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent); 
//            ob.execute();
//          //  Object op=ob.execute();
//            System.out.println("If record found.");
//            if(ob.getResult()!=null && ob.getResult().equals("N")) {
////                FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
////                               Message.setSeverity(FacesMessage.SEVERITY_ERROR);
////                               FacesContext fc = FacesContext.getCurrentInstance();
////                               fc.addMessage(null, Message);
//                System.out.println("If No record found.");
//                FacesMessage msg=new FacesMessage("Record not found.");
//                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
//                FacesContext fc=FacesContext.getCurrentInstance();
//                fc.addMessage(null, msg);
//                
//                
//            }
//        }
    }
    public void clVCE(ValueChangeEvent vce) {
//        if(vce!= null){
//        
//        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
//       
//        BigDecimal sum=new BigDecimal(0);
//        BigDecimal updatedPr=new BigDecimal(0);
//        BigDecimal updatedTotalPaidDays=new BigDecimal(0);
//        
//        Object cl=getBindCL().getValue()==null?'0':getBindCL().getValue();
//        Object sl=getBindSL().getValue()==null?'0':getBindSL().getValue();
//        Object el=getBindEL().getValue()==null?'0':getBindEL().getValue();
//        Object lw=getBindLW().getValue()==null?'0':getBindLW().getValue();
//        Object fo=getBindFO().getValue()==null?'0':getBindFO().getValue();
//        Object od=getBindOD().getValue()==null?'0':getBindOD().getValue();
//        Object co=getBindCO().getValue()==null?'0':getBindCO().getValue();
//        Object wo=getBindWO().getValue()==null?'0':getBindWO().getValue();
//        Object gp=getBindGP().getValue()==null?'0':getBindGP().getValue();
//        Object shortdays=getBindShortDays().getValue()==null?'0':getBindShortDays().getValue();
//        Object ho=getBindHO().getValue()==null?'0':getBindHO().getValue();
//        Object ml=getBindML().getValue()==null?'0':getBindML().getValue();
//        Object ab=getBindAB().getValue()==null?'0':getBindAB().getValue();
//       // Object d=getBindD().getValue()==null?'0':getBindD().getValue();
//        
//        String mon="", year="";
//        Integer i=0 , y;
//        int d=0;
//        year=(String)getYearBinding().getValue();
//        y=Integer.parseInt(year);
//        mon=(String)getMonth2Binding().getValue();
//        if(!mon.isEmpty() && mon.equals("JAN"))
//        {
//                
//            Calendar cal=new GregorianCalendar(y,i,1);
//            d=cal.getActualMaximum(Calendar.DAY_OF_MONTH);
//                System.out.println("Value of Month days in the bean in Jan is "+d);
//            }
//        else if(!mon.isEmpty() && mon.equals("FEB"))
//        {
//            i=1;
//            Calendar cal=new GregorianCalendar(y,i,1);
//            d=cal.getActualMaximum(Calendar.DAY_OF_MONTH);
//                System.out.println("Value of Month days in the bean in Feb is "+d);
//            }
//        else if(!mon.isEmpty() && mon.equals("MAR"))
//        {
//            i=2;
//            Calendar cal=new GregorianCalendar(y,i,1);
//            d=cal.getActualMaximum(Calendar.DAY_OF_MONTH);
//                System.out.println("Value of Month days in the bean in March is "+d);
//            }
//        else if(!mon.isEmpty() && mon.equals("APR"))
//        {
//            i=3;
//            Calendar cal=new GregorianCalendar(y,i,1);
//            d=cal.getActualMaximum(Calendar.DAY_OF_MONTH);
//                System.out.println("Value of Month days in the bean in April is "+d);
//            }
//        else if(!mon.isEmpty() && mon.equals("MAY"))
//        {
//            i=4;
//            Calendar cal=new GregorianCalendar(y,i,1);
//            d=cal.getActualMaximum(Calendar.DAY_OF_MONTH);
//                System.out.println("Value of Month days in the bean in May is "+d);
//            }
//        else if(!mon.isEmpty() && mon.equals("JUN"))
//        {
//            i=5;
//            Calendar cal=new GregorianCalendar(y,i,1);
//            d=cal.getActualMaximum(Calendar.DAY_OF_MONTH);
//                System.out.println("Value of Month days in the bean in June is "+d);
//            }
//        else if(!mon.isEmpty() && mon.equals("JUL"))
//        {
//            i=6;
//            Calendar cal=new GregorianCalendar(y,i,1);
//            d=cal.getActualMaximum(Calendar.DAY_OF_MONTH);
//                System.out.println("Value of Month days in the bean in July is "+d);
//            }
//        else if(!mon.isEmpty() && mon.equals("AUG"))
//        {
//            i=7;
//            Calendar cal=new GregorianCalendar(y,i,1);
//            d=cal.getActualMaximum(Calendar.DAY_OF_MONTH);
//                System.out.println("Value of Month days in the bean in Aug is "+d);
//            }
//        else if(!mon.isEmpty() && mon.equals("SEP"))
//        {
//            i=8;
//            Calendar cal=new GregorianCalendar(y,i,1);
//            d=cal.getActualMaximum(Calendar.DAY_OF_MONTH);
//                System.out.println("Value of Month days in the bean in Sep is "+d);
//            }
//        else if(!mon.isEmpty() && mon.equals("OCT"))
//        {
//            i=9;
//            Calendar cal=new GregorianCalendar(y,i,1);
//            d=cal.getActualMaximum(Calendar.DAY_OF_MONTH);
//                System.out.println("Value of Month days in the bean in Oct is "+d);
//            }        
//        else if(!mon.isEmpty() && mon.equals("NOV"))
//        {
//            i=10;
//            Calendar cal=new GregorianCalendar(y,i,1);
//            d=cal.getActualMaximum(Calendar.DAY_OF_MONTH);
//                System.out.println("Value of Month days in the bean in Nov is "+d);
//            }
//        else if(!mon.isEmpty() && mon.equals("DEC"))
//        {
//            i=11;
//            Calendar cal=new GregorianCalendar(y,i,1);
//            d=cal.getActualMaximum(Calendar.DAY_OF_MONTH);
//                System.out.println("Value of Month days in the bean in Dec is "+d);
//            }
//        
//        BigDecimal cl1=new BigDecimal(cl.toString());
//        BigDecimal sl1=new BigDecimal(sl.toString());
//        BigDecimal el1=new BigDecimal(el.toString());
//        BigDecimal lw1=new BigDecimal(lw.toString());
//        BigDecimal fo1=new BigDecimal(fo.toString());
//        BigDecimal od1=new BigDecimal(od.toString());
//        BigDecimal co1=new BigDecimal(co.toString());
//        BigDecimal wo1=new BigDecimal(wo.toString());
//        BigDecimal gp1=new BigDecimal(gp.toString());
//        BigDecimal shortdays1=new BigDecimal(shortdays.toString());
//        BigDecimal ho1=new BigDecimal(ho.toString());
//        BigDecimal ml1=new BigDecimal(ml.toString());
//        BigDecimal ab1=new BigDecimal(ab.toString());
//        
//        System.out.println("Value of Month days in the bean is 2 "+d);
//        BigDecimal d1=new BigDecimal(d);
//        
//        bindD.setValue(d1);
//        totalDaysBind.setValue(d1);
//      
//        
//        
//        System.out.println("Value of fo1 in the bean is 3 "+fo1);
//        System.out.println("Value of ab1 in the bean is 3 "+ab1);
//        System.out.println("Value of Month days in the bean is 3 "+d1);
//        sum=cl1.add(sl1.add(el1.add(lw1.add(fo1.add(od1.add(co1.add(wo1.add(gp1.add(shortdays1.add(ml1.add(ho1.add(ab1))))))))))));
//        System.out.println("sum" +sum);
//        
//        updatedPr=d1.subtract(sum);
//        System.out.println("updatedPr" + updatedPr);
//        bindPR.setValue(updatedPr);
//       
//       System.out.println("Total LW leaves"+lw1);
//        updatedTotalPaidDays=d1.subtract(lw1);
//        System.out.println("updatedTotalPaidDays" + updatedTotalPaidDays);
//        System.out.println("updatedTotalPaidDays 2 -->" + updatedTotalPaidDays.toString());
//        totalPaidDayBind.setValue(updatedTotalPaidDays);
//        System.out.println("Get Value"+totalPaidDayBind.getValue());     
//    }    
    }

    public void setBindCL(RichInputText bindCL) {
        this.bindCL = bindCL;
    }

    public RichInputText getBindCL() {
        return bindCL;
    }

    public void setBindSL(RichInputText bindSL) {
        this.bindSL = bindSL;
    }

    public RichInputText getBindSL() {
        return bindSL;
    }

    public void setBindEL(RichInputText bindEL) {
        this.bindEL = bindEL;
    }

    public RichInputText getBindEL() {
        return bindEL;
    }

    public void setBindLW(RichInputText bindLW) {
        this.bindLW = bindLW;
    }

    public RichInputText getBindLW() {
        return bindLW;
    }

    public void setBindFO(RichInputText bindFO) {
        this.bindFO = bindFO;
    }

    public RichInputText getBindFO() {
        return bindFO;
    }

    public void setBindOD(RichInputText bindOD) {
        this.bindOD = bindOD;
    }

    public RichInputText getBindOD() {
        return bindOD;
    }

    public void setBindCO(RichInputText bindCO) {
        this.bindCO = bindCO;
    }

    public RichInputText getBindCO() {
        return bindCO;
    }

    public void setBindWO(RichInputText bindWO) {
        this.bindWO = bindWO;
    }

    public RichInputText getBindWO() {
        return bindWO;
    }

    public void setBindGP(RichInputText bindGP) {
        this.bindGP = bindGP;
    }

    public RichInputText getBindGP() {
        return bindGP;
    }

    public void setBindShortDays(RichInputText bindShortDays) {
        this.bindShortDays = bindShortDays;
    }

    public RichInputText getBindShortDays() {
        return bindShortDays;
    }

    public void setBindHO(RichInputText bindHO) {
        this.bindHO = bindHO;
    }

    public RichInputText getBindHO() {
        return bindHO;
    }

    public void setBindML(RichInputText bindML) {
        this.bindML = bindML;
    }

    public RichInputText getBindML() {
        return bindML;
    }

    public void setBindAB(RichInputText bindAB) {
        this.bindAB = bindAB;
    }

    public RichInputText getBindAB() {
        return bindAB;
    }

    public void setBindPR(RichInputText bindPR) {
        this.bindPR = bindPR;
    }

    public RichInputText getBindPR() {
        return bindPR;
    }


    public void setBindD(RichInputText bindD) {
        this.bindD = bindD;
    }

    public RichInputText getBindD() {
        return bindD;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }
    
    private void cevmodecheck(){
            if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
                cevModeDisableComponent("V");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("C");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("E");
            }
        }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                try {
                    Method method1 =
                        component.getClass().getMethod("setDisabled", boolean.class);
                    if (method1 != null) {
                        method1.invoke(component, valueComponent);
                    }
                } catch (NoSuchMethodException e) {
                    try {
                        Method method =
                            component.getClass().getMethod("setReadOnly", boolean.class);
                        if (method != null) {
                            method.invoke(component, valueComponent);
                        }
                    } catch (Exception e1) {
                        // e.printStackTrace();//silently eat this exception.
                    }


                } catch (Exception e) {
                    // e.printStackTrace();//silently eat this exception.
                }
                List<UIComponent> childComponents = component.getChildren();
                for (UIComponent comp : childComponents) {
                    makeComponentHierarchyReadOnly(comp, valueComponent);
                }
            }
    
    public void cevModeDisableComponent(String mode) {
        
        if(mode.equals("E")){
            System.out.println("Mode message in Edit "+Message);
                Message="E";
                getUnitCodeBinding().setDisabled(true);
                getUnitCodeBinding1().setDisabled(true);
                getEmployeeNumberBind().setDisabled(true);
                getEmployeeNameBind().setDisabled(true);
                getDepartmentNameBind().setDisabled(true);
                getBindEmployeeName().setDisabled(true);
//                getBindCL().setDisabled(true);
//                getBindSL().setDisabled(true);
//                getBindEL().setDisabled(true);
//                getBindLW().setDisabled(true);
//                getBindFO().setDisabled(true);
//                getBindOD().setDisabled(true);
//                getBindCO().setDisabled(true);
//                getBindWO().setDisabled(true);
//                getBindGP().setDisabled(true);
//                getBindShortDays().setDisabled(true);
//                getBindHO().setDisabled(true);
//                getBindML().setDisabled(true);
//                getBindAB().setDisabled(true);
                getBindPR().setDisabled(true);
                getBindD().setDisabled(true);
                getTotalDaysBind().setDisabled(true);
                getTotalPaidDayBind().setDisabled(true);
               // getHeaderEditBinding().setDisabled(true);
                getDetailcreateBinding().setDisabled(true);
                getDetaildeleteBinding().setDisabled(true);
            
            }
        
        else if (mode.equals("C")) {
                    System.out.println("Mode message in Create "+Message);
                    getUnitCodeBinding().setDisabled(true);
                    getUnitCodeBinding1().setDisabled(true);
                    getEmployeeNumberBind().setDisabled(true);
                    getEmployeeNameBind().setDisabled(true);
                    getBindEmployeeName().setDisabled(true);
                   // getBindEmployeeName().setDisabled(true);
//                    getDepartmentNameBind().setDisabled(true);
//                    getBindCL().setDisabled(true);
//                    getBindSL().setDisabled(true);
//                    getBindEL().setDisabled(true);
//                    getBindLW().setDisabled(true);
//                    getBindFO().setDisabled(true);
//                    getBindOD().setDisabled(true);
//                    getBindCO().setDisabled(true);
//                    getBindWO().setDisabled(true);
//                    getBindGP().setDisabled(true);
//                    getBindShortDays().setDisabled(true);
//                    getBindHO().setDisabled(true);
//                    getBindML().setDisabled(true);
//                    getBindAB().setDisabled(true);
                    getBindPR().setDisabled(true);
                    getBindD().setDisabled(true);
                    getTotalDaysBind().setDisabled(true);
                    getTotalPaidDayBind().setDisabled(true);
                    getHeaderEditBinding().setDisabled(true);
                    getDetailcreateBinding().setDisabled(true);
                    getDetaildeleteBinding().setDisabled(true);
                
                }
        else if (mode.equals("V")) {
                    getDetailcreateBinding().setDisabled(true);
                    getDetaildeleteBinding().setDisabled(true);
                    getBindEmployeeName().setDisabled(true);
                }
    }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setYearBinding(RichInputText yearBinding) {
        this.yearBinding = yearBinding;
    }

    public RichInputText getYearBinding() {
        return yearBinding;
    }

    public void setMonth2Binding(RichSelectOneChoice month2Binding) {
        this.month2Binding = month2Binding;
    }

    public RichSelectOneChoice getMonth2Binding() {
        return month2Binding;
    }

    public void setEmployeeNumberBind(RichInputText employeeNumberBind) {
        this.employeeNumberBind = employeeNumberBind;
    }

    public RichInputText getEmployeeNumberBind() {
        return employeeNumberBind;
    }

    public void setEmployeeNameBind(RichInputText employeeNameBind) {
        this.employeeNameBind = employeeNameBind;
    }

    public RichInputText getEmployeeNameBind() {
        return employeeNameBind;
    }

    public void setDepartmentNameBind(RichInputText departmentNameBind) {
        this.departmentNameBind = departmentNameBind;
    }

    public RichInputText getDepartmentNameBind() {
        return departmentNameBind;
    }

    public void setTotalDaysBind(RichInputText totalDaysBind) {
        this.totalDaysBind = totalDaysBind;
    }

    public RichInputText getTotalDaysBind() {
        return totalDaysBind;
    }

    public void setTotalPaidDayBind(RichInputText totalPaidDayBind) {
        this.totalPaidDayBind = totalPaidDayBind;
    }

    public RichInputText getTotalPaidDayBind() {
        return totalPaidDayBind;
    }

    public void setDetailcreateBinding(RichButton detailcreateBinding) {
        this.detailcreateBinding = detailcreateBinding;
    }

    public RichButton getDetailcreateBinding() {
        return detailcreateBinding;
    }

    public void setDetaildeleteBinding(RichButton detaildeleteBinding) {
        this.detaildeleteBinding = detaildeleteBinding;
    }

    public RichButton getDetaildeleteBinding() {
        return detaildeleteBinding;
    }
    public void saveButtonAL(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedBy("AttendanceEntryVO1Iterator","LastUpdatedBy");
        String code=getUnitCodeBinding1().getValue().toString();
        String tt=getTABLEBINDING().getValue().toString();
        System.out.println("ANkit Chalega yeh Tension mat le"+tt.length());
        System.out.println("jhjhjk"+getTABLEBINDING().getEstimatedRowCount());
        System.out.println("Mode Message"+Message);
        if(Message.equalsIgnoreCase("C")&& getTABLEBINDING().getEstimatedRowCount()!=0)
      
               {
                   System.out.println("Mode Message 1"+Message);
                   ADFUtils.findOperation("Commit").execute();
                   ADFUtils.showMessage("Record Saved Successfully.", 2);
                  // Message="E";
               }
               else if(Message.equalsIgnoreCase("C")&& getTABLEBINDING().getEstimatedRowCount()==0)
               {
                   System.out.println("Mode Message 2"+Message);
                   //ADFUtils.findOperation("Commit").execute();
                   ADFUtils.showMessage("Data Not Found in Table", 2);
               }

    }
    public void setUnitCode1Binding(RichInputComboboxListOfValues unitCode1Binding) {
        this.unitCode1Binding = unitCode1Binding;
    }
    public RichInputComboboxListOfValues getUnitCode1Binding() {
        return unitCode1Binding;
    }

    public void setUnitCodeBinding1(RichInputComboboxListOfValues unitCodeBinding1) {
        this.unitCodeBinding1 = unitCodeBinding1;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding1() {
        return unitCodeBinding1;
    }

    public void setYearBinding1(RichInputText yearBinding1) {
        this.yearBinding1 = yearBinding1;
    }

    public RichInputText getYearBinding1() {
        return yearBinding1;
    }

    public void setMonth2Binding1(RichInputComboboxListOfValues month2Binding1) {
        this.month2Binding1 = month2Binding1;
    }

    public RichInputComboboxListOfValues getMonth2Binding1() {
        return month2Binding1;
    }

    public void setTABLEBINDING(RichTable TABLEBINDING) {
        this.TABLEBINDING = TABLEBINDING;
    }

    public RichTable getTABLEBINDING() {
        return TABLEBINDING;
    }

    public void setBindEmployeeName(RichInputText bindEmployeeName) {
        this.bindEmployeeName = bindEmployeeName;
    }

    public RichInputText getBindEmployeeName() {
        return bindEmployeeName;
    }
}
