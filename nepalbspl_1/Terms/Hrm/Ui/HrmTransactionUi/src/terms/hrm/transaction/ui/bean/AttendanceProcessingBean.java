package terms.hrm.transaction.ui.bean;

import java.util.Arrays;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.event.ActionEvent;

import javax.faces.validator.ValidatorException;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;

import oracle.adf.view.rich.component.rich.input.RichInputText;

import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.util.ResetUtils;

import oracle.binding.OperationBinding;

public class AttendanceProcessingBean {
    private RichInputDate bindingFromDate;
    private RichInputDate bindingToDate;
    private RichInputComboboxListOfValues monthBinding;
    private RichInputText yearBinding;
    private RichInputComboboxListOfValues unitBinding;
    private RichInputComboboxListOfValues empCodeBinding;
    private RichPanelGroupLayout rootPanelBinding;

    public AttendanceProcessingBean() {
    }

    public void AttendanceProcessingAL(ActionEvent actionEvent) {
        OperationBinding op = ADFUtils.findOperation("AttendanceProcessing");
        Object ob = op.execute();
        System.out.println("result==>>" + op.getResult());
        if (getEmpCodeBinding().getValue() == null){
            ADFUtils.showMessage("Employee Code must have some value.", 0);
        }else if(getUnitBinding().getValue() == null){
            ADFUtils.showMessage("Unit Code must have some value.", 0);
        } else if (op.getResult().equals("Y")) {
            ADFUtils.showMessage("Salary has been already processed for this month so attendance can not be process.",2);
        } else if (op.getResult().equals("X")) {
            ADFUtils.showMessage("Salary has been already freezed for this month so attendance can not be process.", 2);
        } else if (op.getResult().equals("S")) {
            ADFUtils.showMessage("Please define start date and end date in total working days master.", 2);
        } else if(getEmpCodeBinding().getValue() != null && getUnitBinding().getValue() != null){
            if ((getBindingFromDate() != null && getBindingToDate() != null) || (getMonthBinding() != null && getYearBinding() != null)) {
                System.out.println("Calling CallProcedure");
                OperationBinding binding = ADFUtils.findOperation("CallProcedure");
                binding.getParamsMap().put("EmpCode", getEmpCodeBinding().getValue().toString());
                binding.getParamsMap().put("UnitCode", getUnitBinding().getValue().toString());
                if (getBindingFromDate() != null && getBindingToDate() != null)      {                     
                    binding.getParamsMap().put("FromDate", (oracle.jbo.domain.Date) getBindingFromDate().getValue());
                    binding.getParamsMap().put("ToDate", (oracle.jbo.domain.Date) getBindingToDate().getValue());
                }else if(getMonthBinding() != null && getYearBinding() != null){
                    binding.getParamsMap().put("FromDate", "");
                    binding.getParamsMap().put("ToDate", "");
                }
                binding.execute();
                System.out.println("Final Binding Value :- "+binding.getErrors()+ "  " + binding.getResult().toString());
                if ((binding.getResult().equals("Y"))) {
                    ADFUtils.showMessage("Attendance has been processed successfully.", 2);
                }
                //ADFUtils.findOperation("Rollback").execute();
                UIComponent uiComp = actionEvent.getComponent();
                ResetUtils.reset(uiComp);
            }   
        }
    }

    public void setBindingFromDate(RichInputDate bindingFromDate) {
        this.bindingFromDate = bindingFromDate;
    }

    public RichInputDate getBindingFromDate() {
        return bindingFromDate;
    }

    public void setBindingToDate(RichInputDate bindingToDate) {
        this.bindingToDate = bindingToDate;
    }

    public RichInputDate getBindingToDate() {
        return bindingToDate;
    }

    public void setMonthBinding(RichInputComboboxListOfValues monthBinding) {
        this.monthBinding = monthBinding;
    }

    public RichInputComboboxListOfValues getMonthBinding() {
        return monthBinding;
    }

    public void setYearBinding(RichInputText yearBinding) {
        this.yearBinding = yearBinding;
    }

    public RichInputText getYearBinding() {
        return yearBinding;
    }

    public void setUnitBinding(RichInputComboboxListOfValues unitBinding) {
        this.unitBinding = unitBinding;
    }

    public RichInputComboboxListOfValues getUnitBinding() {
        return unitBinding;
    }

    public void setEmpCodeBinding(RichInputComboboxListOfValues empCodeBinding) {
        this.empCodeBinding = empCodeBinding;
    }

    public RichInputComboboxListOfValues getEmpCodeBinding() {
        return empCodeBinding;
    }

    public void setRootPanelBinding(RichPanelGroupLayout rootPanelBinding) {
        this.rootPanelBinding = rootPanelBinding;
    }

    public RichPanelGroupLayout getRootPanelBinding() {
        return rootPanelBinding;
    }
}
