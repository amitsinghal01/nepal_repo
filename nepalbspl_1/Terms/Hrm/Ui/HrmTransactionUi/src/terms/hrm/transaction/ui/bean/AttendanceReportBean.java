package terms.hrm.transaction.ui.bean;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;

import java.sql.Connection;

import java.sql.SQLException;

import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.faces.context.FacesContext;

import javax.naming.Context;
import javax.naming.InitialContext;

import javax.servlet.http.HttpServletResponse;

import javax.sql.DataSource;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCIteratorBinding;

import oracle.adf.view.rich.component.rich.input.RichInputDate;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

public class AttendanceReportBean {
    private String pMode="";
       String file_name="";
    private RichInputDate frmDtAttendShiftWise;
    private RichInputDate toDtAttendShiftWise;
    private RichInputDate frmDtAttendDeptWise;
    private RichInputDate toDtAttendDeptWise;
    private RichInputDate frmDtAttendEmpWise;
    private RichInputDate toDtAttendEmpWise;

    public AttendanceReportBean() {
    }


    public void setPMode(String pMode) {
        this.pMode = pMode;
    }

    public String getPMode() {
        return pMode;
    }
    
    public BindingContainer getBindings()
            {
              return BindingContext.getCurrent().getCurrentBindingsEntry();
            }

        public void downloadJrxml(FacesContext facesContext, OutputStream outputStream) {
            Connection conn=null;
            try {
                
                System.out.println(" pmode:"+pMode);
                
                
                Map n = new HashMap();
                if(pMode != null){
                  if(pMode.equalsIgnoreCase("Y")){
                     file_name ="r_yearly_attn_det.jasper";
                    DCIteratorBinding paySlip = (DCIteratorBinding) getBindings().get("AttendDetYearlyWiseReportVVO1Iterator");
                    n.put("p_unit", paySlip.getCurrentRow().getAttribute("UnitCode"));
                    n.put("p_year", (paySlip.getCurrentRow().getAttribute("Year")!=null?paySlip.getCurrentRow().getAttribute("Year").toString():"%"));
                    
                    System.out.println("unit:"+ paySlip.getCurrentRow().getAttribute("UnitCode")
                                       +" year:"+ (paySlip.getCurrentRow().getAttribute("Year")!=null?paySlip.getCurrentRow().getAttribute("Year").toString():"%"));
                    
                }else if(pMode.equalsIgnoreCase("D")){
                     file_name ="r_attn_det_dept.jasper";
                    DCIteratorBinding paySlip = (DCIteratorBinding) getBindings().get("AttendDetDeptWiseReportVVO1Iterator");   
                    n.put("p_unit", paySlip.getCurrentRow().getAttribute("UnitCode").toString());
                    n.put("p_dept", (paySlip.getCurrentRow().getAttribute("DeptCode")!=null?paySlip.getCurrentRow().getAttribute("DeptCode").toString():"%"));
                    n.put("p_from_dt", paySlip.getCurrentRow().getAttribute("FromDt"));
                    n.put("p_to_dt", paySlip.getCurrentRow().getAttribute("ToDt"));
                    
                    System.out.println("Dept unit:"+ paySlip.getCurrentRow().getAttribute("UnitCode")
                                       +" dept:"+ (paySlip.getCurrentRow().getAttribute("DeptCode")!=null?paySlip.getCurrentRow().getAttribute("DeptCode").toString():"%")+
                    "fromdt:"+ paySlip.getCurrentRow().getAttribute("FromDt")
                                                           +" todt:"+ paySlip.getCurrentRow().getAttribute("ToDt"));

                }else if(pMode.equalsIgnoreCase("E")){
                     file_name ="r_emp_attn_det.jasper";
                    DCIteratorBinding paySlip = (DCIteratorBinding) getBindings().get("AttendEmpWiseReportVVO1Iterator");                    
                    n.put("p_unit", paySlip.getCurrentRow().getAttribute("UnitCode").toString());
                    n.put("p_shift", (paySlip.getCurrentRow().getAttribute("Shift")!=null?paySlip.getCurrentRow().getAttribute("Shift").toString():"%"));
                    n.put("p_from_dt", paySlip.getCurrentRow().getAttribute("FromDt"));
                    n.put("p_to_dt", paySlip.getCurrentRow().getAttribute("ToDt"));
                    
                    System.out.println("emp unit:"+ paySlip.getCurrentRow().getAttribute("UnitCode")
                                       +" shift:"+ (paySlip.getCurrentRow().getAttribute("Shift")!=null?paySlip.getCurrentRow().getAttribute("Shift").toString():"%")+
                    "fromdt:"+ paySlip.getCurrentRow().getAttribute("FromDt")
                                                           +" todt:"+ paySlip.getCurrentRow().getAttribute("ToDt"));
                    
                }else if(pMode.equalsIgnoreCase("S")){
                     file_name ="r_attn_det_shift.jasper";
                    DCIteratorBinding paySlip = (DCIteratorBinding) getBindings().get("AttendDetShiftWiseReportVVO1Iterator");                    
                    n.put("p_unit", paySlip.getCurrentRow().getAttribute("UnitCode").toString());
                    n.put("p_shift", (paySlip.getCurrentRow().getAttribute("Shift")!=null?paySlip.getCurrentRow().getAttribute("Shift").toString():"%"));
                    n.put("p_from_dt", paySlip.getCurrentRow().getAttribute("FromDt"));
                    n.put("p_to_dt", paySlip.getCurrentRow().getAttribute("ToDt"));
                    
                    System.out.println("shift unit:"+ paySlip.getCurrentRow().getAttribute("UnitCode")
                                       +" shift:"+ (paySlip.getCurrentRow().getAttribute("Shift")!=null?paySlip.getCurrentRow().getAttribute("Shift").toString():"%")+
                    "fromdt:"+ paySlip.getCurrentRow().getAttribute("FromDt")
                                                           +" todt:"+ paySlip.getCurrentRow().getAttribute("ToDt"));
                    
                }
                }
                
                OperationBinding binding = ADFUtils.findOperation("fileNameForPrint");
                binding.getParamsMap().put("fileId","ERP0000000120");
                binding.execute();
                System.out.println("Binding Result :"+binding.getResult());
                if(binding.getResult() != null){
                    String result = binding.getResult().toString();
                                      int last_index = result.lastIndexOf("/");
                    if(last_index==-1)
                    {
                        last_index=result.lastIndexOf("\\");
                    }
                                                          String path = result.substring(0,last_index+1)+file_name;
                                                          System.out.println("FILE PATH IS===>"+path);
                                      InputStream input = new FileInputStream(path);
                //    InputStream input = new FileInputStream("//home//beta3//Jasper//Jour_prn1.jrxml");
                    conn = getConnection( );
                    
                    JasperReport design = (JasperReport) JRLoader.loadObject(input);
                    System.out.println("Path : " + input + " -------" + design+" param:"+n);
                    
                    @SuppressWarnings("unchecked")
                    net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                    byte pdf[] = JasperExportManager.exportReportToPdf(print);
                    HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                    response.getOutputStream().write(pdf);
                    response.getOutputStream().flush();
                    response.getOutputStream().close();
                    facesContext.responseComplete();
                    }else
                    System.out.println("File Name/File Path not found.");
                    } catch (FileNotFoundException fnfe) {
                    // TODO: Add catch code
                    fnfe.printStackTrace();
                    try {
                        System.out.println("in finally connection closed");
                                conn.close( );
                                conn = null;
                        
                    } catch( SQLException e ) {
                        e.printStackTrace( );
                    }
                    } catch (Exception e) {
                    // TODO: Add catch code
                    e.printStackTrace();
                    try {
                        System.out.println("in finally connection closed");
                                conn.close( );
                                conn = null;
                        
                    } catch( SQLException e1 ) {
                        e1.printStackTrace( );
                    }
                    }finally {
                            try {
                                    System.out.println("in finally connection closed");
                                            conn.close( );
                                            conn = null;
                                    
                            } catch( SQLException e ) {
                                    e.printStackTrace( );
                            }
                    }
        }

    public void setFrmDtAttendShiftWise(RichInputDate frmDtAttendShiftWise) {
        this.frmDtAttendShiftWise = frmDtAttendShiftWise;
    }

    public RichInputDate getFrmDtAttendShiftWise() {
        return frmDtAttendShiftWise;
    }

    public void setToDtAttendShiftWise(RichInputDate toDtAttendShiftWise) {
        this.toDtAttendShiftWise = toDtAttendShiftWise;
    }

    public RichInputDate getToDtAttendShiftWise() {
        return toDtAttendShiftWise;
    }

    public void setFrmDtAttendDeptWise(RichInputDate frmDtAttendDeptWise) {
        this.frmDtAttendDeptWise = frmDtAttendDeptWise;
    }

    public RichInputDate getFrmDtAttendDeptWise() {
        return frmDtAttendDeptWise;
    }

    public void setToDtAttendDeptWise(RichInputDate toDtAttendDeptWise) {
        this.toDtAttendDeptWise = toDtAttendDeptWise;
    }

    public RichInputDate getToDtAttendDeptWise() {
        return toDtAttendDeptWise;
    }

    public void setFrmDtAttendEmpWise(RichInputDate frmDtAttendEmpWise) {
        this.frmDtAttendEmpWise = frmDtAttendEmpWise;
    }

    public RichInputDate getFrmDtAttendEmpWise() {
        return frmDtAttendEmpWise;
    }

    public void setToDtAttendEmpWise(RichInputDate toDtAttendEmpWise) {
        this.toDtAttendEmpWise = toDtAttendEmpWise;
    }

    public RichInputDate getToDtAttendEmpWise() {
        return toDtAttendEmpWise;
    }
    
    public Connection getConnection() throws Exception{
            Context ctx = null;
            Connection conn = null;
            ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/ApplicationDBDS");
            conn = ds.getConnection();
            return conn;
        }
}
