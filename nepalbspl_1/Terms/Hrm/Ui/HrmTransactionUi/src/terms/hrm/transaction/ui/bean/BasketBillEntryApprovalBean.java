package terms.hrm.transaction.ui.bean;

import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;

import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.binding.OperationBinding;

public class BasketBillEntryApprovalBean {
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText unitNameBinding;
    private RichSelectOneChoice statusBinding;
    private RichInputText checkVerifyApprovalBinding;
    private RichInputText empLoginTransBinding;
    private RichOutputText checkStatusBinding;
    private RichSelectOneChoice statusBindings;
    private RichInputText checkBindings;
    private RichInputText apprBindings;

    public BasketBillEntryApprovalBean() {
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setUnitNameBinding(RichInputText unitNameBinding) {
        this.unitNameBinding = unitNameBinding;
    }

    public RichInputText getUnitNameBinding() {
        return unitNameBinding;
    }

    public void populateDataAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("populateDataForBasketBillEntryApproval").execute();
        ADFUtils.findOperation("generateBasketBillEntryApprovalProc").execute();
    }

    public void setStatusBinding(RichSelectOneChoice statusBinding) {
        this.statusBinding = statusBinding;
    }

    public RichSelectOneChoice getStatusBinding() {
        return statusBinding;
    }

    public void setCheckVerifyApprovalBinding(RichInputText checkVerifyApprovalBinding) {
        this.checkVerifyApprovalBinding = checkVerifyApprovalBinding;
    }

    public RichInputText getCheckVerifyApprovalBinding() {
        return checkVerifyApprovalBinding;
    }

    public void setEmpLoginTransBinding(RichInputText empLoginTransBinding) {
        this.empLoginTransBinding = empLoginTransBinding;
    }

    public RichInputText getEmpLoginTransBinding() {
        return empLoginTransBinding;
    }

    public void authorityAL(ActionEvent actionEvent) {
//        ADFUtils.findOperation("authorityForBasketBillEntryApproval").execute();
        OperationBinding ob = ADFUtils.findOperation("authorityForBasketBillEntryApproval");
        ob.execute();
        System.out.println("INSIDE " + ob.getResult()+"OB"+ob);
        if(!ob.getResult().toString().equalsIgnoreCase("Y-") && !ob.getResult().toString().equalsIgnoreCase("Y")) 
        {
            System.out.println("INSIDE IF #### " + !ob.equals(false));
            ADFUtils.showMessage((String) ob.getResult(), 0);
        }else
        {
            ADFUtils.findOperation("setAuthorityCdNameForBasketBillEntryProc").execute();
        }
//        ADFUtils.findOperation("setAuthorityCdNameForBasketBillEntryProc").execute();
    }

    public void saveAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("setSaveDataForBasketBillEntryProc").execute();
//        System.out.println("Approval Status"+apprBindings.getValue() +"Approval Code"+ statusBindings.getValue());
//        if(statusBindings.getValue()!=null && apprBindings.getValue()!=null && statusBindings.getValue().equals("A"))
//        {
//         ADFUtils.findOperation("getVoucherGenerationForMonthlyApproval").execute();
//        }
        ADFUtils.findOperation("Commit").execute();
    }

    public void setCheckStatusBinding(RichOutputText checkStatusBinding) {
        this.checkStatusBinding = checkStatusBinding;
    }

    public RichOutputText getCheckStatusBinding() {
        return checkStatusBinding;
    }

    public void setStatusBindings(RichSelectOneChoice statusBindings) {
        this.statusBindings = statusBindings;
    }

    public RichSelectOneChoice getStatusBindings() {
        return statusBindings;
    }

    public void setCheckBindings(RichInputText checkBindings) {
        this.checkBindings = checkBindings;
    }

    public RichInputText getCheckBindings() {
        return checkBindings;
    }

    public void setApprBindings(RichInputText apprBindings) {
        this.apprBindings = apprBindings;
    }

    public RichInputText getApprBindings() {
        return apprBindings;
    }
}
