package terms.hrm.transaction.ui.bean;

import javax.faces.application.FacesMessage;

import javax.faces.context.FacesContext;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class BasketEntitelmentMasterBean {
    private RichTable basketEntitelmentMasterTableBinding;
    private String editAction="V";
    private RichInputText basDayBinding;

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public BasketEntitelmentMasterBean() {
    }

    public void popUpDialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
        {
        OperationBinding op = null;
        ADFUtils.findOperation("Delete").execute();
        op = (OperationBinding) ADFUtils.findOperation("Commit");
        if (getEditAction().equals("V")) {
        Object rst = op.execute();
        }
        System.out.println("Record Delete Successfully");
        if(op.getErrors().isEmpty()){
        FacesMessage Message = new FacesMessage("Record Deleted Successfully");
        Message.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        } else if (!op.getErrors().isEmpty())
        {
        OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
        Object rstr = opr.execute();
        FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
        Message.setSeverity(FacesMessage.SEVERITY_ERROR);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(basketEntitelmentMasterTableBinding);
    }

    public void setBasketEntitelmentMasterTableBinding(RichTable basketEntitelmentMasterTableBinding) {
        this.basketEntitelmentMasterTableBinding = basketEntitelmentMasterTableBinding;
    }

    public RichTable getBasketEntitelmentMasterTableBinding() {
        return basketEntitelmentMasterTableBinding;
    }

    public void employeeCodeVCL(ValueChangeEvent vce) {
        System.out.println("In VCE before method call");
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        ADFUtils.findOperation("valueSetForBasketAmt").execute();
        System.out.println("In VCE after method call");
       OperationBinding op=(OperationBinding) ADFUtils.findOperation("valueSetForBasketDay");
        System.out.println("In VCE after method  2 call");
        op.execute();
        op.getResult();
        System.out.println(" op.getResult"+ op.getResult());
        java.lang.Integer days=(java.lang.Integer)op.getResult();
        System.out.println("days"+days);
        basDayBinding.setValue(days);
        System.out.println("In Last");
        
        System.out.println("Before call 3 method");
        OperationBinding op2=(OperationBinding) ADFUtils.findOperation("valueSetForEmployeeMode");
        op2.execute();
        System.out.println("After call 3 method");
        
    }

    public void setBasDayBinding(RichInputText basDayBinding) {
        this.basDayBinding = basDayBinding;
    }

    public RichInputText getBasDayBinding() {
        return basDayBinding;
    }
}
