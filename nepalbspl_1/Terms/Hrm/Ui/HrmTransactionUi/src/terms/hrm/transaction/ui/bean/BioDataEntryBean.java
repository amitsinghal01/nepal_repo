package terms.hrm.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;

import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;

import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;

import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichPanelLabelAndMessage;

import oracle.adf.view.rich.component.rich.nav.RichButton;

import oracle.binding.OperationBinding;

public class BioDataEntryBean {
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText entryNumberBinding;
    private RichInputDate entryDateBinding;
    private RichSelectOneChoice categoryBinding;
    private RichInputComboboxListOfValues departmentCodeBinding;
    private RichInputText firstNameBinding;
    private RichInputText lastNameBinding;
    private RichInputText fatherNameBinding;
    private RichInputDate birthDateBinding;
    private RichSelectOneChoice sexBinding;
    private RichSelectOneChoice religionBinding;
    private RichSelectOneChoice maritalStatusBinding;
    private RichInputText presentAddress1Binding;
    private RichInputText presentAddress2Binding;
    private RichInputText permanentAddress1Binding;
    private RichInputText permanentAddress2Binding;
    private RichInputText pinCodeBinding;
    private RichInputText permanentPinCodeBinding;
    private RichInputText phoneNumberBinding;
    private RichInputText permanentPhoneNumberBinding;
    private RichInputText residencePhoneNumberBinding;
    private RichInputComboboxListOfValues qualificationCodeBinding;
    private RichInputComboboxListOfValues employeeReferenceNumberBinding;
    private RichInputComboboxListOfValues employeeLevelCodeBinding;
    private RichInputComboboxListOfValues designationCodeBinding;
    private RichInputText expectedSalaryBinding;
    private RichInputText experienceBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichPanelLabelAndMessage bindingOutputText;

    public BioDataEntryBean() {
    }

    public void SaveButtonAL(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedBy("BioDataEntryVO1Iterator","LastUpdatedBy");
        OperationBinding op = ADFUtils.findOperation("getBioDataEntryNo");
        Object rst = op.execute();
        
        System.out.println("Result is===> "+op.getResult());
        
        
        System.out.println("--------Commit-------");
            System.out.println("value aftr getting result--->?"+rst);
            
        
            if (rst.toString() != null && rst.toString() != "" && rst.toString()!="N") {
                if (op.getErrors().isEmpty()) {
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message = new FacesMessage("Record Saved Successfully. New Entry Number is "+rst+".");   
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                    FacesContext fc = FacesContext.getCurrentInstance();   
                    fc.addMessage(null, Message);  
        
                }
            }
            
            if (rst.toString().equalsIgnoreCase("N")) {
                if (op.getErrors().isEmpty()) {
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message = new FacesMessage("Record Update Successfully.");   
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                    FacesContext fc = FacesContext.getCurrentInstance();   
                    fc.addMessage(null, Message);  
            
                }

            
            }
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setEntryNumberBinding(RichInputText entryNumberBinding) {
        this.entryNumberBinding = entryNumberBinding;
    }

    public RichInputText getEntryNumberBinding() {
        return entryNumberBinding;
    }

    public void setEntryDateBinding(RichInputDate entryDateBinding) {
        this.entryDateBinding = entryDateBinding;
    }

    public RichInputDate getEntryDateBinding() {
        return entryDateBinding;
    }

    public void setCategoryBinding(RichSelectOneChoice categoryBinding) {
        this.categoryBinding = categoryBinding;
    }

    public RichSelectOneChoice getCategoryBinding() {
        return categoryBinding;
    }

    public void setDepartmentCodeBinding(RichInputComboboxListOfValues departmentCodeBinding) {
        this.departmentCodeBinding = departmentCodeBinding;
    }

    public RichInputComboboxListOfValues getDepartmentCodeBinding() {
        return departmentCodeBinding;
    }

    public void setFirstNameBinding(RichInputText firstNameBinding) {
        this.firstNameBinding = firstNameBinding;
    }

    public RichInputText getFirstNameBinding() {
        return firstNameBinding;
    }

    public void setLastNameBinding(RichInputText lastNameBinding) {
        this.lastNameBinding = lastNameBinding;
    }

    public RichInputText getLastNameBinding() {
        return lastNameBinding;
    }

    public void setFatherNameBinding(RichInputText fatherNameBinding) {
        this.fatherNameBinding = fatherNameBinding;
    }

    public RichInputText getFatherNameBinding() {
        return fatherNameBinding;
    }

    public void setBirthDateBinding(RichInputDate birthDateBinding) {
        this.birthDateBinding = birthDateBinding;
    }

    public RichInputDate getBirthDateBinding() {
        return birthDateBinding;
    }

    public void setSexBinding(RichSelectOneChoice sexBinding) {
        this.sexBinding = sexBinding;
    }

    public RichSelectOneChoice getSexBinding() {
        return sexBinding;
    }

    public void setReligionBinding(RichSelectOneChoice religionBinding) {
        this.religionBinding = religionBinding;
    }

    public RichSelectOneChoice getReligionBinding() {
        return religionBinding;
    }

    public void setMaritalStatusBinding(RichSelectOneChoice maritalStatusBinding) {
        this.maritalStatusBinding = maritalStatusBinding;
    }

    public RichSelectOneChoice getMaritalStatusBinding() {
        return maritalStatusBinding;
    }

    public void setPresentAddress1Binding(RichInputText presentAddress1Binding) {
        this.presentAddress1Binding = presentAddress1Binding;
    }

    public RichInputText getPresentAddress1Binding() {
        return presentAddress1Binding;
    }

    public void setPresentAddress2Binding(RichInputText presentAddress2Binding) {
        this.presentAddress2Binding = presentAddress2Binding;
    }

    public RichInputText getPresentAddress2Binding() {
        return presentAddress2Binding;
    }

    public void setPermanentAddress1Binding(RichInputText permanentAddress1Binding) {
        this.permanentAddress1Binding = permanentAddress1Binding;
    }

    public RichInputText getPermanentAddress1Binding() {
        return permanentAddress1Binding;
    }

    public void setPermanentAddress2Binding(RichInputText permanentAddress2Binding) {
        this.permanentAddress2Binding = permanentAddress2Binding;
    }

    public RichInputText getPermanentAddress2Binding() {
        return permanentAddress2Binding;
    }

    public void setPinCodeBinding(RichInputText pinCodeBinding) {
        this.pinCodeBinding = pinCodeBinding;
    }

    public RichInputText getPinCodeBinding() {
        return pinCodeBinding;
    }

    public void setPermanentPinCodeBinding(RichInputText permanentPinCodeBinding) {
        this.permanentPinCodeBinding = permanentPinCodeBinding;
    }

    public RichInputText getPermanentPinCodeBinding() {
        return permanentPinCodeBinding;
    }

    public void setPhoneNumberBinding(RichInputText phoneNumberBinding) {
        this.phoneNumberBinding = phoneNumberBinding;
    }

    public RichInputText getPhoneNumberBinding() {
        return phoneNumberBinding;
    }

    public void setPermanentPhoneNumberBinding(RichInputText permanentPhoneNumberBinding) {
        this.permanentPhoneNumberBinding = permanentPhoneNumberBinding;
    }

    public RichInputText getPermanentPhoneNumberBinding() {
        return permanentPhoneNumberBinding;
    }

    public void setResidencePhoneNumberBinding(RichInputText residencePhoneNumberBinding) {
        this.residencePhoneNumberBinding = residencePhoneNumberBinding;
    }

    public RichInputText getResidencePhoneNumberBinding() {
        return residencePhoneNumberBinding;
    }

    public void setQualificationCodeBinding(RichInputComboboxListOfValues qualificationCodeBinding) {
        this.qualificationCodeBinding = qualificationCodeBinding;
    }

    public RichInputComboboxListOfValues getQualificationCodeBinding() {
        return qualificationCodeBinding;
    }

    public void setEmployeeReferenceNumberBinding(RichInputComboboxListOfValues employeeReferenceNumberBinding) {
        this.employeeReferenceNumberBinding = employeeReferenceNumberBinding;
    }

    public RichInputComboboxListOfValues getEmployeeReferenceNumberBinding() {
        return employeeReferenceNumberBinding;
    }

    public void setEmployeeLevelCodeBinding(RichInputComboboxListOfValues employeeLevelCodeBinding) {
        this.employeeLevelCodeBinding = employeeLevelCodeBinding;
    }

    public RichInputComboboxListOfValues getEmployeeLevelCodeBinding() {
        return employeeLevelCodeBinding;
    }

    public void setDesignationCodeBinding(RichInputComboboxListOfValues designationCodeBinding) {
        this.designationCodeBinding = designationCodeBinding;
    }

    public RichInputComboboxListOfValues getDesignationCodeBinding() {
        return designationCodeBinding;
    }

    public void setExpectedSalaryBinding(RichInputText expectedSalaryBinding) {
        this.expectedSalaryBinding = expectedSalaryBinding;
    }

    public RichInputText getExpectedSalaryBinding() {
        return expectedSalaryBinding;
    }

    public void setExperienceBinding(RichInputText experienceBinding) {
        this.experienceBinding = experienceBinding;
    }

    public RichInputText getExperienceBinding() {
        return experienceBinding;
    }


    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }
    
    private void cevmodecheck(){
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
               try {
                   Method method1 =
                       component.getClass().getMethod("setDisabled", boolean.class);
                   if (method1 != null) {
                       method1.invoke(component, valueComponent);
                   }
               } catch (NoSuchMethodException e) {
                   try {
                       Method method =
                           component.getClass().getMethod("setReadOnly", boolean.class);
                       if (method != null) {
                           method.invoke(component, valueComponent);
                       }
                   } catch (Exception e1) {
                       // e.printStackTrace();//silently eat this exception.
                   }


               } catch (Exception e) {
                   // e.printStackTrace();//silently eat this exception.
               }
               List<UIComponent> childComponents = component.getChildren();
               for (UIComponent comp : childComponents) {
                   makeComponentHierarchyReadOnly(comp, valueComponent);
               }
           }
    
    public void cevModeDisableComponent(String mode) {
    //            FacesContext fctx = FacesContext.getCurrentInstance();
    //            ELContext elctx = fctx.getELContext();
    //            Application jsfApp = fctx.getApplication();
    //            //create a ValueExpression that points to the ADF binding layer
    //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
    //            //
    //            ValueExpression valueExpr = exprFactory.createValueExpression(
    //                                         elctx,
    //                                         "#{pageFlowScope.mode=='E'}",
    //                                          Object.class
    //                                         );
    //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
        if (mode.equals("E")) {
                getUnitCodeBinding().setDisabled(true);
                getEntryNumberBinding().setDisabled(true);
                getEntryDateBinding().setDisabled(true);
                getCategoryBinding().setDisabled(false);
                getDepartmentCodeBinding().setDisabled(false);
                getFirstNameBinding().setDisabled(false);
                getLastNameBinding().setDisabled(false);
                getFatherNameBinding().setDisabled(false);
                getBirthDateBinding().setDisabled(false);
                getSexBinding().setDisabled(false);
                getReligionBinding().setDisabled(false);
                getMaritalStatusBinding().setDisabled(false);
                getPresentAddress1Binding().setDisabled(false);
                getPresentAddress2Binding().setDisabled(false);
                getPermanentAddress1Binding().setDisabled(false);
                getPermanentAddress2Binding().setDisabled(false);
                getPinCodeBinding().setDisabled(false);
                getPermanentPinCodeBinding().setDisabled(false);
                getPhoneNumberBinding().setDisabled(false);
                getPermanentPhoneNumberBinding().setDisabled(false);
                getResidencePhoneNumberBinding().setDisabled(false);
                getQualificationCodeBinding().setDisabled(false);
                getEmployeeReferenceNumberBinding().setDisabled(false);
                getEmployeeLevelCodeBinding().setDisabled(false);
                getDesignationCodeBinding().setDisabled(false);
                getExpectedSalaryBinding().setDisabled(false);
                getExperienceBinding().setDisabled(false);

        } else if (mode.equals("C")) {
            getUnitCodeBinding().setDisabled(true);
            getEntryNumberBinding().setDisabled(true);
            getEntryDateBinding().setDisabled(true);
            getCategoryBinding().setDisabled(false);
            getDepartmentCodeBinding().setDisabled(false);
            getFirstNameBinding().setDisabled(false);
            getLastNameBinding().setDisabled(false);
            getFatherNameBinding().setDisabled(false);
            getBirthDateBinding().setDisabled(false);
            getSexBinding().setDisabled(false);
            getReligionBinding().setDisabled(false);
            getMaritalStatusBinding().setDisabled(false);
            getPresentAddress1Binding().setDisabled(false);
            getPresentAddress2Binding().setDisabled(false);
            getPermanentAddress1Binding().setDisabled(false);
            getPermanentAddress2Binding().setDisabled(false);
            getPinCodeBinding().setDisabled(false);
            getPermanentPinCodeBinding().setDisabled(false);
            getPhoneNumberBinding().setDisabled(false);
            getPermanentPhoneNumberBinding().setDisabled(false);
            getResidencePhoneNumberBinding().setDisabled(false);
            getQualificationCodeBinding().setDisabled(false);
            getEmployeeReferenceNumberBinding().setDisabled(false);
            getEmployeeLevelCodeBinding().setDisabled(false);
            getDesignationCodeBinding().setDisabled(false);
            getExpectedSalaryBinding().setDisabled(false);
            getExperienceBinding().setDisabled(false);
        } else if (mode.equals("V")) {
          
        }
        
    }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setBindingOutputText(RichPanelLabelAndMessage bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichPanelLabelAndMessage getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }
}
