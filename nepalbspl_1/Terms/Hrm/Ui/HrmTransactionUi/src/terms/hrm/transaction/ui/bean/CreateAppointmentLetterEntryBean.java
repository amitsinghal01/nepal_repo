package terms.hrm.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichPanelLabelAndMessage;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class CreateAppointmentLetterEntryBean {
    private RichTable appointmentLetterEntryTableBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText entryNumberBinding;
    private RichInputDate entryDateBinding;
    private RichInputComboboxListOfValues interviewEntryNumberBinding;
    private RichInputComboboxListOfValues categoryCodeBinding;
    private RichInputText payScaleBinding;
    private RichInputText letterReferenceNumberBinding;
    private RichInputDate letterReferenceDateBinding;
    private RichInputText probationPeroidBinding;
    private RichInputComboboxListOfValues unitCodeDetailBinding;
    private RichInputDate entryDateDetailBinding;
    private RichInputText entryNumberDetailBinding;
    private RichInputText headDescriptionDetailBinding;
    private RichInputText salaryAmountDetailBinding;
    private RichPanelLabelAndMessage bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichButton detailcreateBinding;
    private RichButton detaildeleteBinding;
    private String message="C";
    private RichInputText desigCodeBinding;
    private RichInputText desigNameBinding;
    private RichInputText levelCodeBinding;
    private RichInputText levelDescBinding;
    private RichInputText unitNameBinding;

    public CreateAppointmentLetterEntryBean() {
    }

    public void saveButtonAL(ActionEvent actionEvent) {
    ADFUtils.setLastUpdatedBy("AppointmentLetterEntryHeaderVO1Iterator","LastUpdatedBy");
    ADFUtils.findOperation("generateAppointmentLetterEntryNumber").execute();
            OperationBinding opr = ADFUtils.findOperation("Commit");
            Object obj = opr.execute();

            System.out.println("outside error block");
             System.out.println("Level Code value is"+entryNumberBinding.getValue());

             //   if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {

            //       if(leaveCodeHeaderBinding.getValue() !=null){
            if (message.equalsIgnoreCase("C")){
                    System.out.println("S####");
                    FacesMessage Message = new FacesMessage("Record Save Successfully. New Category Code is "+entryNumberBinding.getValue());
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);

                } else if (message.equalsIgnoreCase("E")) {
                    FacesMessage Message = new FacesMessage("Record Update Successfully");
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);
                }
        }


    public void popUpDialogDetailDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                                    {
        ADFUtils.findOperation("Delete").execute();
                                        OperationBinding op= ADFUtils.findOperation("Commit");
                                        Object rst = op.execute();
                                    System.out.println("Record Delete Successfully");

                                        if(op.getErrors().isEmpty())
                                        {
                                        FacesMessage Message = new FacesMessage("Record Deleted Successfully");
        Message.setSeverity(FacesMessage.SEVERITY_INFO);
                                        FacesContext fc = FacesContext.getCurrentInstance();
                                        fc.addMessage(null, Message);
                                        }
                                        else
                                        {
                                        FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
        Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                                        FacesContext fc = FacesContext.getCurrentInstance();
                                        fc.addMessage(null, Message);
                                         }
                                }
        AdfFacesContext.getCurrentInstance().addPartialTarget(appointmentLetterEntryTableBinding);

    }

    public void setAppointmentLetterEntryTableBinding(RichTable appointmentLetterEntryTableBinding) {
        this.appointmentLetterEntryTableBinding = appointmentLetterEntryTableBinding;
    }

    public RichTable getAppointmentLetterEntryTableBinding() {
        return appointmentLetterEntryTableBinding;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setEntryNumberBinding(RichInputText entryNumberBinding) {
        this.entryNumberBinding = entryNumberBinding;
    }

    public RichInputText getEntryNumberBinding() {
        return entryNumberBinding;
    }

    public void setEntryDateBinding(RichInputDate entryDateBinding) {
        this.entryDateBinding = entryDateBinding;
    }

    public RichInputDate getEntryDateBinding() {
        return entryDateBinding;
    }

    public void setInterviewEntryNumberBinding(RichInputComboboxListOfValues interviewEntryNumberBinding) {
        this.interviewEntryNumberBinding = interviewEntryNumberBinding;
    }

    public RichInputComboboxListOfValues getInterviewEntryNumberBinding() {
        return interviewEntryNumberBinding;
    }

    public void setCategoryCodeBinding(RichInputComboboxListOfValues categoryCodeBinding) {
        this.categoryCodeBinding = categoryCodeBinding;
    }

    public RichInputComboboxListOfValues getCategoryCodeBinding() {
        return categoryCodeBinding;
    }

    public void setPayScaleBinding(RichInputText payScaleBinding) {
        this.payScaleBinding = payScaleBinding;
    }

    public RichInputText getPayScaleBinding() {
        return payScaleBinding;
    }

    public void setLetterReferenceNumberBinding(RichInputText letterReferenceNumberBinding) {
        this.letterReferenceNumberBinding = letterReferenceNumberBinding;
    }

    public RichInputText getLetterReferenceNumberBinding() {
        return letterReferenceNumberBinding;
    }

    public void setLetterReferenceDateBinding(RichInputDate letterReferenceDateBinding) {
        this.letterReferenceDateBinding = letterReferenceDateBinding;
    }

    public RichInputDate getLetterReferenceDateBinding() {
        return letterReferenceDateBinding;
    }

    public void setProbationPeroidBinding(RichInputText probationPeroidBinding) {
        this.probationPeroidBinding = probationPeroidBinding;
    }

    public RichInputText getProbationPeroidBinding() {
        return probationPeroidBinding;
    }

    public void setUnitCodeDetailBinding(RichInputComboboxListOfValues unitCodeDetailBinding) {
        this.unitCodeDetailBinding = unitCodeDetailBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeDetailBinding() {
        return unitCodeDetailBinding;
    }

    public void setEntryDateDetailBinding(RichInputDate entryDateDetailBinding) {
        this.entryDateDetailBinding = entryDateDetailBinding;
    }

    public RichInputDate getEntryDateDetailBinding() {
        return entryDateDetailBinding;
    }

    public void setEntryNumberDetailBinding(RichInputText entryNumberDetailBinding) {
        this.entryNumberDetailBinding = entryNumberDetailBinding;
    }

    public RichInputText getEntryNumberDetailBinding() {
        return entryNumberDetailBinding;
    }

    public void setHeadDescriptionDetailBinding(RichInputText headDescriptionDetailBinding) {
        this.headDescriptionDetailBinding = headDescriptionDetailBinding;
    }

    public RichInputText getHeadDescriptionDetailBinding() {
        return headDescriptionDetailBinding;
    }

    public void setSalaryAmountDetailBinding(RichInputText salaryAmountDetailBinding) {
        this.salaryAmountDetailBinding = salaryAmountDetailBinding;
    }

    public RichInputText getSalaryAmountDetailBinding() {
        return salaryAmountDetailBinding;
    }
    public void setBindingOutputText(RichPanelLabelAndMessage bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichPanelLabelAndMessage getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setDetailcreateBinding(RichButton detailcreateBinding) {
        this.detailcreateBinding = detailcreateBinding;
    }

    public RichButton getDetailcreateBinding() {
        return detailcreateBinding;
    }

    public void setDetaildeleteBinding(RichButton detaildeleteBinding) {
        this.detaildeleteBinding = detaildeleteBinding;
    }

    public RichButton getDetaildeleteBinding() {
        return detaildeleteBinding;
    }
    
    private void cevmodecheck(){
           if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
               cevModeDisableComponent("V");
           }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
               cevModeDisableComponent("C");
           }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
               cevModeDisableComponent("E");
           }
       }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                try {
                    Method method1 =
                        component.getClass().getMethod("setDisabled", boolean.class);
                    if (method1 != null) {
                        method1.invoke(component, valueComponent);
                    }
                } catch (NoSuchMethodException e) {
                    try {
                        Method method =
                            component.getClass().getMethod("setReadOnly", boolean.class);
                        if (method != null) {
                            method.invoke(component, valueComponent);
                        }
                    } catch (Exception e1) {
                        // e.printStackTrace();//silently eat this exception.
                    }


                } catch (Exception e) {
                    // e.printStackTrace();//silently eat this exception.
                }
                List<UIComponent> childComponents = component.getChildren();
                for (UIComponent comp : childComponents) {
                    makeComponentHierarchyReadOnly(comp, valueComponent);
                }
            }
    
    public void cevModeDisableComponent(String mode) {
    if (mode.equals("E")) {
                    getUnitCodeBinding().setDisabled(true);
                    getEntryNumberBinding().setDisabled(true);
                    getEntryDateBinding().setDisabled(true);
                    getInterviewEntryNumberBinding().setDisabled(false);
                    getCategoryCodeBinding().setDisabled(false);
                    getPayScaleBinding().setDisabled(true);
                    getLetterReferenceNumberBinding().setDisabled(false);
                    getLetterReferenceDateBinding().setDisabled(false);
                    getProbationPeroidBinding().setDisabled(false);
                getDesigCodeBinding().setDisabled(true);
                getDesigNameBinding().setDisabled(true);
                getLevelCodeBinding().setDisabled(true);
                getLevelDescBinding().setDisabled(true);
                    
                    getUnitCodeDetailBinding().setDisabled(true);
                    getEntryDateDetailBinding().setDisabled(true);
                    getEntryNumberDetailBinding().setDisabled(true);
                    getHeadDescriptionDetailBinding().setDisabled(false);
                    getSalaryAmountDetailBinding().setDisabled(false);
                    getHeaderEditBinding().setDisabled(true);
                    getDetailcreateBinding().setDisabled(false);
                    getDetaildeleteBinding().setDisabled(false);
                    message="E";
            } else if (mode.equals("C")) {
        
                getUnitCodeBinding().setDisabled(true);
                getEntryNumberBinding().setDisabled(true);
                getEntryDateBinding().setDisabled(true);
                getInterviewEntryNumberBinding().setDisabled(false);
                getCategoryCodeBinding().setDisabled(false);
                getPayScaleBinding().setDisabled(true);
                getLetterReferenceNumberBinding().setDisabled(false);
                getLetterReferenceDateBinding().setDisabled(false);
                getProbationPeroidBinding().setDisabled(false);
                getDesigCodeBinding().setDisabled(true);
                getDesigNameBinding().setDisabled(true);
                getLevelCodeBinding().setDisabled(true);
                getLevelDescBinding().setDisabled(true);
                
                getUnitCodeDetailBinding().setDisabled(true);
                getEntryDateDetailBinding().setDisabled(true);
                getEntryNumberDetailBinding().setDisabled(true);
                getHeadDescriptionDetailBinding().setDisabled(false);
                getSalaryAmountDetailBinding().setDisabled(false);
                getHeaderEditBinding().setDisabled(true);
                getDetailcreateBinding().setDisabled(false);
                getDetaildeleteBinding().setDisabled(false);
                
                getDetailcreateBinding().setDisabled(false);
                getDetaildeleteBinding().setDisabled(false);
            } else if (mode.equals("V")) {
        
                getDetailcreateBinding().setDisabled(true);
                getDetaildeleteBinding().setDisabled(true);
                
            }
            
        }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setDesigCodeBinding(RichInputText desigCodeBinding) {
        this.desigCodeBinding = desigCodeBinding;
    }

    public RichInputText getDesigCodeBinding() {
        return desigCodeBinding;
    }

    public void setDesigNameBinding(RichInputText desigNameBinding) {
        this.desigNameBinding = desigNameBinding;
    }

    public RichInputText getDesigNameBinding() {
        return desigNameBinding;
    }

    public void setLevelCodeBinding(RichInputText levelCodeBinding) {
        this.levelCodeBinding = levelCodeBinding;
    }

    public RichInputText getLevelCodeBinding() {
        return levelCodeBinding;
    }

    public void setLevelDescBinding(RichInputText levelDescBinding) {
        this.levelDescBinding = levelDescBinding;
    }

    public RichInputText getLevelDescBinding() {
        return levelDescBinding;
    }

    public void setUnitNameBinding(RichInputText unitNameBinding) {
        this.unitNameBinding = unitNameBinding;
    }

    public RichInputText getUnitNameBinding() {
        return unitNameBinding;
    }

    public void payscaleauto(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce.getNewValue()!=null)
        {   
            String unitCd=(String) unitCodeBinding.getValue();
            System.out.println("Unit Value is-----.."+unitCd);
            String ccdd=(String) categoryCodeBinding.getValue();
            System.out.println("Unit Value is-----.."+ccdd);
            String lvlcd=(String) levelCodeBinding.getValue();
            System.out.println("Unit Value is-----.."+lvlcd);
            OperationBinding op=ADFUtils.findOperation("payscalegenerationforappointmentletter");
            op.getParamsMap().put("UnitCd", unitCd);
            op.getParamsMap().put("LevelCode", lvlcd);
            op.getParamsMap().put("CategoryCd", ccdd);
            op.execute();
            
        }
    }
}
