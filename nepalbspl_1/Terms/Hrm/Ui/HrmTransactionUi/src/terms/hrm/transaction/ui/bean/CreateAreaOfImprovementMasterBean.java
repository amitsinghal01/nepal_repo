package terms.hrm.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.RichDialog;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichPanelLabelAndMessage;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class CreateAreaOfImprovementMasterBean {
    private RichTable createAreaOfImprovementMasterTableBinding;
    private RichButton detailCreateBinding;
    private RichButton detailDeleteBinding;
    private RichDialog deletePopupDialogDL;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText improvementAreaBinding;
    private RichInputComboboxListOfValues unitCodeDetailBinding;
    private RichInputText improvementMasterAreaDetailBinding;
    private RichPanelLabelAndMessage bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private String message="C";
    private RichInputComboboxListOfValues requiredTrainingSetBinding;

    public CreateAreaOfImprovementMasterBean() {
    }

    public void setCreateAreaOfImprovementMasterTableBinding(RichTable createAreaOfImprovementMasterTableBinding) {
        this.createAreaOfImprovementMasterTableBinding = createAreaOfImprovementMasterTableBinding;
    }

    public RichTable getCreateAreaOfImprovementMasterTableBinding() {
        return createAreaOfImprovementMasterTableBinding;
    }

    public void setDetailCreateBinding(RichButton detailCreateBinding) {
        this.detailCreateBinding = detailCreateBinding;
    }

    public RichButton getDetailCreateBinding() {
        return detailCreateBinding;
    }

    public void setDetailDeleteBinding(RichButton detailDeleteBinding) {
        this.detailDeleteBinding = detailDeleteBinding;
    }

    public RichButton getDetailDeleteBinding() {
        return detailDeleteBinding;
    }

    public void setDeletePopupDialogDL(RichDialog deletePopupDialogDL) {
        this.deletePopupDialogDL = deletePopupDialogDL;
    }

    public RichDialog getDeletePopupDialogDL() {
        return deletePopupDialogDL;
    }

    public void deletePopUpDialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                            {
                            ADFUtils.findOperation("Delete").execute();
                                OperationBinding op=  ADFUtils.findOperation("Commit");
                                Object rst = op.execute();
                            System.out.println("Record Delete Successfully");
                           
                                if(op.getErrors().isEmpty())
                                {
                                FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                                Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                                FacesContext fc = FacesContext.getCurrentInstance();  
                                fc.addMessage(null, Message);
                                }
                                else
                                {
                                FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                                FacesContext fc = FacesContext.getCurrentInstance();
                                fc.addMessage(null, Message);
                                 }
                        }
                        AdfFacesContext.getCurrentInstance().addPartialTarget(createAreaOfImprovementMasterTableBinding);
        
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setImprovementAreaBinding(RichInputText improvementAreaBinding) {
        this.improvementAreaBinding = improvementAreaBinding;
    }

    public RichInputText getImprovementAreaBinding() {
        return improvementAreaBinding;
    }

    public void setUnitCodeDetailBinding(RichInputComboboxListOfValues unitCodeDetailBinding) {
        this.unitCodeDetailBinding = unitCodeDetailBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeDetailBinding() {
        return unitCodeDetailBinding;
    }

    public void setImprovementMasterAreaDetailBinding(RichInputText improvementMasterAreaDetailBinding) {
        this.improvementMasterAreaDetailBinding = improvementMasterAreaDetailBinding;
    }

    public RichInputText getImprovementMasterAreaDetailBinding() {
        return improvementMasterAreaDetailBinding;
    }

    public void setBindingOutputText(RichPanelLabelAndMessage bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichPanelLabelAndMessage getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }
    
    
    private void cevmodecheck(){
           if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
               cevModeDisableComponent("V");
           }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
               cevModeDisableComponent("C");
           }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
               cevModeDisableComponent("E");
           }
       }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                try {
                    Method method1 =
                        component.getClass().getMethod("setDisabled", boolean.class);
                    if (method1 != null) {
                        method1.invoke(component, valueComponent);
                    }
                } catch (NoSuchMethodException e) {
                    try {
                        Method method =
                            component.getClass().getMethod("setReadOnly", boolean.class);
                        if (method != null) {
                            method.invoke(component, valueComponent);
                        }
                    } catch (Exception e1) {
                        // e.printStackTrace();//silently eat this exception.
                    }


                } catch (Exception e) {
                    // e.printStackTrace();//silently eat this exception.
                }
                List<UIComponent> childComponents = component.getChildren();
                for (UIComponent comp : childComponents) {
                    makeComponentHierarchyReadOnly(comp, valueComponent);
                }
            }
    
    public void cevModeDisableComponent(String mode) {
   
            if (mode.equals("E")) {
                getUnitCodeBinding().setDisabled(true);
                getImprovementAreaBinding().setDisabled(false);
                getUnitCodeDetailBinding().setDisabled(true);
                getImprovementMasterAreaDetailBinding().setDisabled(true);
                getRequiredTrainingSetBinding().setDisabled(false);
                   getHeaderEditBinding().setDisabled(true);
                message="E";
                    
            } else if (mode.equals("C")) {
                getUnitCodeBinding().setDisabled(true);
                getImprovementAreaBinding().setDisabled(false);
                getUnitCodeDetailBinding().setDisabled(true);
                getImprovementMasterAreaDetailBinding().setDisabled(true);
                getRequiredTrainingSetBinding().setDisabled(false);
                getHeaderEditBinding().setDisabled(true);
            } else if (mode.equals("V")) {
               
            }
            
        }

    public void editButtonAL(ActionEvent actionEvent) {
      cevmodecheck();
    }

    public void saveButtonAL(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedBy("AreaOfImprovementMasterHeaderVO1Iterator","LastUpdatedBy");
        OperationBinding opr = ADFUtils.findOperation("Commit");
        Object obj = opr.execute();
        
        System.out.println("outside error block");

         //   if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
         
        //       if(leaveCodeHeaderBinding.getValue() !=null){
        if (message.equalsIgnoreCase("C")){
                System.out.println("S####");
                FacesMessage Message = new FacesMessage("Record Save Successfully");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);

            } else if (message.equalsIgnoreCase("E")) {
                FacesMessage Message = new FacesMessage("Record Update Successfully");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            }
    }

    public void setRequiredTrainingSetBinding(RichInputComboboxListOfValues requiredTrainingSetBinding) {
        this.requiredTrainingSetBinding = requiredTrainingSetBinding;
    }

    public RichInputComboboxListOfValues getRequiredTrainingSetBinding() {
        return requiredTrainingSetBinding;
    }
}
