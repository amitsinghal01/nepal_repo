package terms.hrm.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichPanelLabelAndMessage;
import oracle.adf.view.rich.component.rich.nav.RichButton;

import oracle.binding.OperationBinding;

public class CreateConfirmationLetterBean {
    private RichButton headerEditBinding;
    private RichPanelHeader getMyPageRootComponent;  
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText entryNumberBinding;
    private RichInputDate entryDateBinding;
    private RichInputComboboxListOfValues appointmentReferenceLetterNumberBinding;
    private RichInputDate appointmentLetterDateBinding;
    private RichInputText interviewEntryNumberBinding;
    private RichInputDate confirmationDateBinding;
    private RichInputText referenceLetterNumberBinding;
    private RichInputDate referenceLetterDateBinding;
    private RichInputText allowancePerMonthBinding;
    private String message="C";
    private RichPanelLabelAndMessage bindingOutputText;

    public CreateConfirmationLetterBean() {
    }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void saveButtonAL(ActionEvent actionEvent) {
                ADFUtils.setLastUpdatedBy("CreateConfirmationLetterVO1Iterator","LastUpdatedBy");
        ADFUtils.findOperation("generateConfirmationEntryNumber").execute();
                OperationBinding opr = ADFUtils.findOperation("Commit");
                Object obj = opr.execute();

                System.out.println("outside error block");
                 System.out.println("Level Code value is"+entryNumberBinding.getValue());

                 //   if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {

                //       if(leaveCodeHeaderBinding.getValue() !=null){
                if (message.equalsIgnoreCase("C")){
                        System.out.println("S####");
                        FacesMessage Message = new FacesMessage("Record Save Successfully. New Category Code is "+entryNumberBinding.getValue());
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);
                        FacesContext fc = FacesContext.getCurrentInstance();
                        fc.addMessage(null, Message);

                    } else if (message.equalsIgnoreCase("E")) {
                        FacesMessage Message = new FacesMessage("Record Update Successfully");
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);
                        FacesContext fc = FacesContext.getCurrentInstance();
                        fc.addMessage(null, Message);
                    }
            }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setEntryNumberBinding(RichInputText entryNumberBinding) {
        this.entryNumberBinding = entryNumberBinding;
    }

    public RichInputText getEntryNumberBinding() {
        return entryNumberBinding;
    }
    private void cevmodecheck(){
           if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
               cevModeDisableComponent("V");
           }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
               cevModeDisableComponent("C");
           }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
               cevModeDisableComponent("E");
           }
       }
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                try {
                    Method method1 =
                        component.getClass().getMethod("setDisabled", boolean.class);
                    if (method1 != null) {
                        method1.invoke(component, valueComponent);
                    }
                } catch (NoSuchMethodException e) {
                    try {
                        Method method =
                            component.getClass().getMethod("setReadOnly", boolean.class);
                        if (method != null) {
                            method.invoke(component, valueComponent);
                        }
                    } catch (Exception e1) {
                        // e.printStackTrace();//silently eat this exception.
                    }


                } catch (Exception e) {
                    // e.printStackTrace();//silently eat this exception.
                }
                List<UIComponent> childComponents = component.getChildren();
                for (UIComponent comp : childComponents) {
                    makeComponentHierarchyReadOnly(comp, valueComponent);
                }
            }
    public void cevModeDisableComponent(String mode) {
    if (mode.equals("E")) {
        getUnitCodeBinding().setDisabled(true);
        getEntryNumberBinding().setDisabled(true);
        getEntryDateBinding().setDisabled(true);
        getAppointmentLetterDateBinding().setDisabled(true);
        getInterviewEntryNumberBinding().setDisabled(true);
        message="E";
        
    } else if (mode.equals("C")) {
        getUnitCodeBinding().setDisabled(true);
        getEntryNumberBinding().setDisabled(true);
        getEntryDateBinding().setDisabled(true);
        getAppointmentLetterDateBinding().setDisabled(true);
        getInterviewEntryNumberBinding().setDisabled(true);
        getHeaderEditBinding().setDisabled(true);
        
    } else if (mode.equals("V")) {
        

}
    }

    public void setEntryDateBinding(RichInputDate entryDateBinding) {
        this.entryDateBinding = entryDateBinding;
    }

    public RichInputDate getEntryDateBinding() {
        return entryDateBinding;
    }

    public void setAppointmentReferenceLetterNumberBinding(RichInputComboboxListOfValues appointmentReferenceLetterNumberBinding) {
        this.appointmentReferenceLetterNumberBinding = appointmentReferenceLetterNumberBinding;
    }

    public RichInputComboboxListOfValues getAppointmentReferenceLetterNumberBinding() {
        return appointmentReferenceLetterNumberBinding;
    }

    public void setAppointmentLetterDateBinding(RichInputDate appointmentLetterDateBinding) {
        this.appointmentLetterDateBinding = appointmentLetterDateBinding;
    }

    public RichInputDate getAppointmentLetterDateBinding() {
        return appointmentLetterDateBinding;
    }

    public void setInterviewEntryNumberBinding(RichInputText interviewEntryNumberBinding) {
        this.interviewEntryNumberBinding = interviewEntryNumberBinding;
    }

    public RichInputText getInterviewEntryNumberBinding() {
        return interviewEntryNumberBinding;
    }

    public void setConfirmationDateBinding(RichInputDate confirmationDateBinding) {
        this.confirmationDateBinding = confirmationDateBinding;
    }

    public RichInputDate getConfirmationDateBinding() {
        return confirmationDateBinding;
    }

    public void setReferenceLetterNumberBinding(RichInputText referenceLetterNumberBinding) {
        this.referenceLetterNumberBinding = referenceLetterNumberBinding;
    }

    public RichInputText getReferenceLetterNumberBinding() {
        return referenceLetterNumberBinding;
    }

    public void setReferenceLetterDateBinding(RichInputDate referenceLetterDateBinding) {
        this.referenceLetterDateBinding = referenceLetterDateBinding;
    }

    public RichInputDate getReferenceLetterDateBinding() {
        return referenceLetterDateBinding;
    }

    public void setAllowancePerMonthBinding(RichInputText allowancePerMonthBinding) {
        this.allowancePerMonthBinding = allowancePerMonthBinding;
    }

    public RichInputText getAllowancePerMonthBinding() {
        return allowancePerMonthBinding;
    }

    public void setBindingOutputText(RichPanelLabelAndMessage bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichPanelLabelAndMessage getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }
}
