package terms.hrm.transaction.ui.bean;

import java.io.BufferedReader;
import java.io.IOException;

import java.io.InputStream;
import java.io.InputStreamReader;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import java.util.Map;
import java.util.StringTokenizer;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectBooleanCheckbox;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;

import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichPanelLabelAndMessage;

import oracle.adf.view.rich.component.rich.nav.RichButton;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.adf.view.rich.event.DialogEvent;

import oracle.adf.view.rich.util.ResetUtils;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;
import oracle.jbo.domain.Timestamp;

import oracle.jbo.uicli.binding.JUCtrlHierBinding;

import org.apache.myfaces.trinidad.event.AttributeChangeEvent;
import org.apache.myfaces.trinidad.model.CollectionModel;
import org.apache.myfaces.trinidad.model.UploadedFile;

import terms.hrm.transaction.model.view.EmployeeMonthlyEarningDetailVORowImpl;

public class CreateEmployeeMonthlyEarningBean {
    private RichInputComboboxListOfValues bindUnit;
    private RichInputText bindYear;
    private RichSelectOneChoice bindMonth;
    private RichInputComboboxListOfValues bindSalCode;
    private RichInputText bindSubCode;
    private RichInputText bindSubCode1;
    private RichInputText entryNumberBinding;
    private String message = "C";
    //private RichSelectOneChoice bindFreeze;
    private RichInputComboboxListOfValues bindApprovedBy;
    private RichPanelLabelAndMessage bindingOutputText;
    private RichButton headerEditBinding;
    private RichButton detailcreateBinding;
    private RichButton detaildeleteBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichSelectBooleanCheckbox synchronizeTrans1Binding;
    private RichSelectBooleanCheckbox synchronizeTrans2Binding;
    private RichButton populateButtonBinding;
    private RichInputText binddtlAmount;
    private RichInputComboboxListOfValues employeeNumberDetailBinding;
    private RichInputText employeeNameDetailBinding;
    private RichTable detailTableBinding;
    private RichInputComboboxListOfValues empNoBinding;
    BigDecimal Amount=new BigDecimal(0);
    private RichInputDate approvedDateBinding;
    private RichInputText bindFreeze;
    private UploadedFile file;
    private RichPopup filePopupBinding;
    private ArrayList<String> list=new ArrayList<String>();

    public CreateEmployeeMonthlyEarningBean() {
    }

    public void setBindUnit(RichInputComboboxListOfValues bindUnit) {
        this.bindUnit = bindUnit;
    }

    public RichInputComboboxListOfValues getBindUnit() {
        return bindUnit;
    }

    public void setBindYear(RichInputText bindYear) {
        this.bindYear = bindYear;
    }

    public RichInputText getBindYear() {
        return bindYear;
    }

    public void populateEmpMonEarnAL(ActionEvent actionEvent) {
        OperationBinding op=ADFUtils.findOperation("filterValueForEmployeeMonthlyEarning");
        Object obj=op.execute();
        System.out.println("ob.getResult()"+op.getResult());
        
        if(op.getResult()!=null && op.getResult().equals("Y"))
        {
        
        System.out.println("In Action Listener");
        System.out.println("Unit Code" + bindUnit.getValue());
        System.out.println("Unit Code 2 " + bindUnit.getValue());
        System.out.println("Month" + bindMonth.getValue());
        System.out.println("Year" + bindYear.getValue());

        System.out.println("bindSalCode Code" + bindSalCode.getValue().toString());
        System.out.println("bindSubCode Code 2 " + bindSubCode.getValue().toString());
        System.out.println("bindSubCode1" + bindSubCode1.getValue().toString());
        System.out.println("synchronizeTrans2Binding" + synchronizeTrans2Binding.getValue().toString());


        if (bindUnit.getValue() != null || bindMonth.getValue() != null || bindYear.getValue() != null) {

            System.out.println("In if Action Listener");
            String Unit = bindUnit.getValue() == null ? "" : bindUnit.getValue().toString();
            String Year = bindYear.getValue() == null ? "" : bindYear.getValue().toString();
            String Month = bindMonth.getValue() == null ? "" : bindMonth.getValue().toString();
            String SalCode = bindSalCode.getValue() == null ? "" : bindSalCode.getValue().toString();
            String SubCode = bindSubCode.getValue() == null ? "" : bindSubCode.getValue().toString();
            String SubCode1 = bindSubCode1.getValue() == null ? "" : bindSubCode1.getValue().toString();
            String freeze = bindFreeze.getValue()==null ? "" : bindFreeze.getValue().toString();

            String synchronizeTrans2 =
                synchronizeTrans2Binding.getValue() == null ? "" : (synchronizeTrans2Binding.getValue().equals(true) ? "Y" : "N");
            System.out.println("synchronizeTrans2Binding 2 --->" + synchronizeTrans2);
            // String EntryNo = bindEntryNo.getValue()==null ? "" : bindEntryNo.getValue().toString();
            System.out.println("############ Employee Monthly Deduction Data ----> " + Unit + " " + Year + " " + Month +
                               " " + SalCode + " " + synchronizeTrans2);
            //Passing  Data into HrmTrancationAMImpl file
            OperationBinding ob = ADFUtils.findOperation("populateEmployeeMonthlyEarning");
            ob.getParamsMap().put("Unit", Unit);
            ob.getParamsMap().put("Year", Year);
            ob.getParamsMap().put("Month", Month);
            ob.getParamsMap().put("SlCode", SalCode);
            ob.getParamsMap().put("synchronizeTrans2", synchronizeTrans2);
            ob.getParamsMap().put("EntryBy", 232);
            /*ob.getParamsMap().put("SubCode", SubCode);
            ob.getParamsMap().put("SubCode1", SubCode1);
            ob.getParamsMap().put("Freeze", Freeze);
            ob.getParamsMap().put("FinYear", FinYear);
            ob.getParamsMap().put("SalaryDays", SalaryDays);  */
            //    ob.getParamsMap().put("EntryNo", EntryNo);
            ob.execute();
            
//            detailcreateBinding.setDisabled(true);
        }
        else {
            ADFUtils.showMessage("Month and Year should be selected.", 0);
        }
        }
        else {
            detailcreateBinding.setDisabled(false);
        }
        bindMonth.setDisabled(true);
        bindYear.setDisabled(true);
        bindSalCode.setDisabled(true);
        populateButtonBinding.setDisabled(true);
    }

    public void setBindMonth(RichSelectOneChoice bindMonth) {
        this.bindMonth = bindMonth;
    }

    public RichSelectOneChoice getBindMonth() {
        return bindMonth;
    }

    public void setBindSalCode(RichInputComboboxListOfValues bindSalCode) {
        this.bindSalCode = bindSalCode;
    }

    public RichInputComboboxListOfValues getBindSalCode() {
        return bindSalCode;
    }

    public void setBindSubCode(RichInputText bindSubCode) {
        this.bindSubCode = bindSubCode;
    }

    public RichInputText getBindSubCode() {
        return bindSubCode;
    }

    public void setBindSubCode1(RichInputText bindSubCode1) {
        this.bindSubCode1 = bindSubCode1;
    }

    public RichInputText getBindSubCode1() {
        return bindSubCode1;
    }


    public void saveButtonAL(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedBy("EmployeeMonthlyEarningHeaderVO1Iterator","LastUpdatedBy");
        System.out.println("Rows in Detail Table  1--->>" + getDetailTableBinding().getEstimatedRowCount());


        ADFUtils.findOperation("generateEntryNumberEmployeeMonthlyEarning").execute();
        OperationBinding opr = ADFUtils.findOperation("Commit");
        Object obj = opr.execute();
        System.out.println("Rows in Detail Table  2--->>" + getDetailTableBinding().getEstimatedRowCount());
        System.out.println("outside error block");
        System.out.println("Entry Number is" + entryNumberBinding.getValue());

        //   if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {

        //       if(leaveCodeHeaderBinding.getValue() !=null){
        if (message.equalsIgnoreCase("C")) {
            System.out.println("S####");
            FacesMessage Message =
                new FacesMessage("Record Save Successfully. New Entry Number is " + entryNumberBinding.getValue());
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }

        else if (message.equalsIgnoreCase("E")) {
            FacesMessage Message = new FacesMessage("Record Update Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
        }
    }

    public void setEntryNumberBinding(RichInputText entryNumberBinding) {
        this.entryNumberBinding = entryNumberBinding;
    }

    public RichInputText getEntryNumberBinding() {
        return entryNumberBinding;
    }

    public void AmountVCL(ValueChangeEvent vce) {
//        System.out.println("In Amount VCE");
//        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
//        if (vce.getNewValue() != null) {
//            Amount=(BigDecimal) vce.getNewValue();
//            System.out.println("Amount Value in bean" + vce.getNewValue());
//            OperationBinding ob = ADFUtils.findOperation("getAmountValueForEmpMonEarning");
//            ob.getParamsMap().put("amount", vce.getNewValue());
//            ob.execute();
//            System.out.println("In Bean AmountVCL 2");
//        }
    }

    public void ApprovedByVCL(ValueChangeEvent vce) {
        System.out.println("ValueChangeEvent" + vce);
        System.out.println("Unit Cd" + bindUnit.getValue().toString());
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        System.out.println("In Bean");

        if (vce != null) {
            System.out.println("In if Bean");
            vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
            OperationBinding op = (OperationBinding) ADFUtils.findOperation("checkAuthorityForTourProgram");
            op.getParamsMap().put("unitCd", bindUnit.getValue().toString());
            System.out.println("Unit Cd" + bindUnit.getValue().toString());
            // op.getParamsMap().put("empCode", headOdDepartmentBinding.getValue();
            op.getParamsMap().put("authLevel", "AP");
            op.getParamsMap().put("formName", "FINBP");
            // op.getParamsMap().put("Usernm", ADFUtils.resolveExpression("#{pageFlowScope.userName}"));
            // op.getParamsMap().put("Usernm", ADFUtils.resolveExpression("#{pageFlowScope.userId}"));
            op.getParamsMap().put("Usernm", "Rajat");
            op.execute();
            String res = (String) op.getResult();

            if (res.equalsIgnoreCase("NE")) {
                System.out.println("In NE case");
                FacesMessage Message = new FacesMessage("Sorry,You Have No Authority For Approval the Tour Programme.");
                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            } else {
                FacesMessage Message = new FacesMessage("You Approved The Tour Programme");
                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
                System.out.println("In N case");
                
                String freezeVal = "Y";
                
                
            }
        }
    }

//    public void setBindFreeze(RichSelectOneChoice bindFreeze) {
//        this.bindFreeze = bindFreeze;
//    }

//    public RichSelectOneChoice getBindFreeze() {
//        return bindFreeze;
//    }

    public void setBindApprovedBy(RichInputComboboxListOfValues bindApprovedBy) {
        this.bindApprovedBy = bindApprovedBy;
    }

    public RichInputComboboxListOfValues getBindApprovedBy() {
        return bindApprovedBy;
    }

    public void setBindingOutputText(RichPanelLabelAndMessage bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichPanelLabelAndMessage getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setDetailcreateBinding(RichButton detailcreateBinding) {
        this.detailcreateBinding = detailcreateBinding;
    }

    public RichButton getDetailcreateBinding() {
        return detailcreateBinding;
    }

    public void setDetaildeleteBinding(RichButton detaildeleteBinding) {
        this.detaildeleteBinding = detaildeleteBinding;
    }

    public RichButton getDetaildeleteBinding() {
        return detaildeleteBinding;
    }

    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }

    public void cevModeDisableComponent(String mode) {
        if (mode.equals("E")) {
            getBindApprovedBy().setDisabled(false);
            //getBindFreeze().setDisabled(false);
            getBindMonth().setDisabled(false);
            getBindSalCode().setDisabled(false);
            getBindSubCode().setDisabled(false);
            getBindSubCode1().setDisabled(false);
            getBindUnit().setDisabled(true);
            getBindYear().setDisabled(false);
            getEntryNumberBinding().setDisabled(true);
            getApprovedDateBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            getEmpNoBinding().setDisabled(true);
            getPopulateButtonBinding().setDisabled(true); 
            getDetailcreateBinding().setDisabled(true);
            getDetaildeleteBinding().setDisabled(false);


        } else if (mode.equals("C")) {
            getBindApprovedBy().setDisabled(true);
            //getBindFreeze().setDisabled(true);
            getBindMonth().setDisabled(false);
            getBindSalCode().setDisabled(false);
            getBindSubCode().setDisabled(true);
            getBindSubCode1().setDisabled(true);
            getBindUnit().setDisabled(true);
            getBindYear().setDisabled(false);
            getEntryNumberBinding().setDisabled(true);
            getApprovedDateBinding().setDisabled(true);
//            getPopulateButtonBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(false);
//            getEmpNoBinding().setDisabled(true);

//            getDetailcreateBinding().setDisabled(true);
            detailcreateBinding.setDisabled(true);
            getDetaildeleteBinding().setDisabled(false);
        } else if (mode.equals("V")) {
            getDetailcreateBinding().setDisabled(true);
            getDetaildeleteBinding().setDisabled(true);
        }

    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void editButtonAL(ActionEvent actionEvent) {
        if(bindApprovedBy.getValue()!=null)
        {
          OperationBinding op =(OperationBinding)ADFUtils.findOperation("freezeAuthority");
          op.getParamsMap().put("formName", "F_EARN.FMX");
          op.getParamsMap().put("freeze", bindFreeze.getValue());
          op.execute();
          if(op.getResult()!=null && op.getResult().equals("Y"))
          {
          ADFUtils.showMessage("Freezed Data cannot be edited further.", 2);  
          ADFUtils.setEL("#{pageFlowScope.mode}", "V");
          }else
          {
            cevmodecheck();
        }
        }else
        {
          cevmodecheck();
        }
    }

    public void setSynchronizeTrans1Binding(RichSelectBooleanCheckbox synchronizeTrans1Binding) {
        this.synchronizeTrans1Binding = synchronizeTrans1Binding;
    }

    public RichSelectBooleanCheckbox getSynchronizeTrans1Binding() {
        return synchronizeTrans1Binding;
    }

    public void setSynchronizeTrans2Binding(RichSelectBooleanCheckbox synchronizeTrans2Binding) {
        this.synchronizeTrans2Binding = synchronizeTrans2Binding;
    }

    public RichSelectBooleanCheckbox getSynchronizeTrans2Binding() {
        return synchronizeTrans2Binding;
    }

    public void yearVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());

        OperationBinding ob = ADFUtils.findOperation("filterValueForEmpMonthlyEarning");
        Object obj = ob.execute();
        System.out.println("ob.getResult()" + ob.getResult());
        if (ob.getResult().equals("Y")) {
            getPopulateButtonBinding().setDisabled(false);
            getDetailcreateBinding().setDisabled(true);
            System.out.println("In bean Y");
            AdfFacesContext.getCurrentInstance().addPartialTarget(populateButtonBinding);
            AdfFacesContext.getCurrentInstance().addPartialTarget(detailcreateBinding);

        } else {
            getDetailcreateBinding().setDisabled(false);
            getPopulateButtonBinding().setDisabled(true);
            System.out.println("In bean N");
            AdfFacesContext.getCurrentInstance().addPartialTarget(populateButtonBinding);
            AdfFacesContext.getCurrentInstance().addPartialTarget(detailcreateBinding);
        }
    }

    public void setPopulateButtonBinding(RichButton populateButtonBinding) {
        this.populateButtonBinding = populateButtonBinding;
    }

    public RichButton getPopulateButtonBinding() {
        return populateButtonBinding;
    }


    public void setBinddtlAmount(RichInputText binddtlAmount) {
        this.binddtlAmount = binddtlAmount;
    }

    public RichInputText getBinddtlAmount() {
        return binddtlAmount;
    }

    public void setEmployeeNumberDetailBinding(RichInputComboboxListOfValues employeeNumberDetailBinding) {
        this.employeeNumberDetailBinding = employeeNumberDetailBinding;
    }

    public RichInputComboboxListOfValues getEmployeeNumberDetailBinding() {
        return employeeNumberDetailBinding;
    }

    public void setEmployeeNameDetailBinding(RichInputText employeeNameDetailBinding) {
        this.employeeNameDetailBinding = employeeNameDetailBinding;
    }

    public RichInputText getEmployeeNameDetailBinding() {
        return employeeNameDetailBinding;
    }

    public void setDetailTableBinding(RichTable detailTableBinding) {
        this.detailTableBinding = detailTableBinding;
    }

    public RichTable getDetailTableBinding() {
        return detailTableBinding;
    }

    public void setEmpNoBinding(RichInputComboboxListOfValues empNoBinding) {
        this.empNoBinding = empNoBinding;
    }

    public RichInputComboboxListOfValues getEmpNoBinding() {
        return empNoBinding;
    }

    public void approvedByVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce.getNewValue() != null) {
            OperationBinding op = (OperationBinding) ADFUtils.findOperation("checkApprovalAuthority");
            op.getParamsMap().put("unitCd", getBindUnit().getValue().toString());
            op.getParamsMap().put("empCode", vce.getNewValue());
            op.getParamsMap().put("authLevel", "AP");
            op.getParamsMap().put("formName", "EME");
            op.execute();
            if (op.getResult() != null && op.getResult().equals("N")) {
                Row r = (Row) ADFUtils.evaluateEL("#{bindings.EmployeeMonthlyEarningHeaderVO1Iterator.currentRow}");
                r.setAttribute("ApprovedBy", null);
                r.setAttribute("ApprovedDate", null);
//                bindFreeze.setValue("N");
                FacesMessage Message =
                    new FacesMessage("Sorry You Are Not Authorized To Approve this Employee Monthly Earning.");
                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            }
            else {
                Timestamp apDate = new Timestamp(System.currentTimeMillis());
                System.out.println("INSIDE HEAD BINDING ##### ");
                
                java.sql.Timestamp datetime = new java.sql.Timestamp(System.currentTimeMillis());
                oracle.jbo.domain.Date daTime = new oracle.jbo.domain.Date(datetime);
                approvedDateBinding.setValue(daTime);
//                bindFreeze.setValue("Y");
                System.out.println("INSIDE ELSE #### " + apDate);
            }
        }
    }

    public void createDetailButtonBinding(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert").execute();
        populateButtonBinding.setDisabled(true);
        empNoBinding.setDisabled(false);
    }

    public void ChangeAmountFirstValueVCL(ValueChangeEvent vce) {
        System.out.println("INSIDE ChangeAmountFirstValueVCL ##### ");
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce.getNewValue()!=null)
        {
        System.out.println("In Bean AmountVCL 2"+Amount+"Check Box==>"+vce.getNewValue());
        OperationBinding ob=ADFUtils.findOperation("getAmtValForEmployeeMonthlyEarning");
                ob.getParamsMap().put("amount", Amount);
                ob.getParamsMap().put("check", vce.getNewValue());
                ob.execute();
        }
    }

    public void setApprovedDateBinding(RichInputDate approvedDateBinding) {
        this.approvedDateBinding = approvedDateBinding;
    }

    public RichInputDate getApprovedDateBinding() {
        return approvedDateBinding;
    }

    public void setBindFreeze(RichInputText bindFreeze) {
        this.bindFreeze = bindFreeze;
    }

    public RichInputText getBindFreeze() {
        return bindFreeze;
    }

    public void salaryCodeVCL(ValueChangeEvent vce) {
        System.out.println("INSIDE salaryCodeVCL #### ");
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        
        if(vce.getNewValue() != null) {
            System.out.println("INSIDE VCE #### ");
            
            OperationBinding op=ADFUtils.findOperation("filterValueForEmployeeMonthlyEarning");
            Object obj=op.execute();
            System.out.println("ob.getResult()"+op.getResult());
            
            if(op.getResult()!=null && op.getResult().equals("Y"))
            {
               System.out.println("INSIDE IF #### ");
               empNoBinding.setDisabled(true);
               populateButtonBinding.setDisabled(false);
               detailcreateBinding.setDisabled(true);
            }
            else {
                System.out.println("INSIDE ELSE #### ");
                empNoBinding.setDisabled(false);
                populateButtonBinding.setDisabled(true);
                detailcreateBinding.setDisabled(false);
            }
        }
    }

    public void uploadFileDL(DialogEvent dialogEvent) {
//        if(dialogEvent.getOutcome().name().equals("ok"))
//                    {
//                   System.out.println("OK Clicked");
////                            parseFile();
//        }
    }

    public void fileuploadVCL(ValueChangeEvent vce) {
        System.out.println("in file upload vce"+vce.getNewValue()+" new:"+vce.getOldValue());
       
        UploadedFile file= (UploadedFile)vce.getNewValue();
//        setFile(file);
        System.out.println(" in file upload vce Content Type:"+file.getContentType()+" file name:"+file.getFilename()+" Size:"
                           +file.getLength()+" file:"+file);
        System.out.println(file.getFilename().substring(file.getFilename().lastIndexOf('.') + 1));
        
//        
//        System.out.println(" in file upload vce getFile Content Type:"+getFile().getContentType()+" file name:"+getFile().getFilename()+" Size:"
//                           +getFile().getLength()+" file:"+getFile());
        

        try {
        if (file.getFilename().substring(file.getFilename().lastIndexOf('.') + 1).equalsIgnoreCase("csv")) {
            System.out.println("CSV File Format catched");
                parseFile(file.getInputStream());
            ResetUtils.reset(vce.getComponent());
        }
        else {
        FacesMessage msg = new FacesMessage("File format not supported.-- Upload CSV file");
        msg.setSeverity(FacesMessage.SEVERITY_WARN);
        FacesContext.getCurrentInstance().addMessage(null, msg);
        ResetUtils.reset(vce.getComponent());
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(detailTableBinding);
        displayErrors();
        } catch (IOException e) {
        // TODO
        }
        
    }
    
    public void cleardata(){
        DCIteratorBinding dci = ADFUtils.findIterator("EmployeeMonthlyEarningDetailVO1Iterator");
        Row[] r=dci.getAllRowsInRange();
        System.out.println("ITERATOR RANGE:"+r.length);
        if(r.length>0){
            RowSetIterator rsi1 = dci.getViewObject().createRowSetIterator(null);
            while(rsi1.hasNext()){
                Row r1= rsi1.next();
                r1.remove();
            }
            AdfFacesContext.getCurrentInstance().addPartialTarget(detailTableBinding);
            rsi1.closeRowSetIterator();
        }
    }
        
    private void parseFile(InputStream file) {
        list.clear();
        getFilePopupBinding().hide();
        BufferedReader reader =
                new BufferedReader(new InputStreamReader(file));
            String strLine = "";
            StringTokenizer st = null;
            int lineNumber = 0, tokenNumber = 0,flag=0;
            EmployeeMonthlyEarningDetailVORowImpl rw=null;
            
    /*        CollectionModel _tableModel = (CollectionModel)impTab.getValue();
            //the ADF object that implements the CollectionModel is JUCtrlHierBinding. It
            //is wrapped by the CollectionModel API
            JUCtrlHierBinding _adfTableBinding =
                (JUCtrlHierBinding)_tableModel.getWrappedData();
            //Acess the ADF iterator binding that is used with ADF table binding
            DCIteratorBinding it = _adfTableBinding.getDCIteratorBinding();
*/
             cleardata();
            DCIteratorBinding dci = ADFUtils.findIterator("EmployeeMonthlyEarningDetailVO1Iterator");
            RowSetIterator rsi = dci.getViewObject().createRowSetIterator(null);
            try {
                
                while ((strLine = reader.readLine()) != null) {
                    lineNumber++;
                    if (lineNumber > 1) {
                        Row last = rsi.last();
                        int i = rsi.getRangeIndexOf(last);
                        rw = (EmployeeMonthlyEarningDetailVORowImpl) rsi.createRow();
                        rw.setNewRowState(Row.STATUS_INITIALIZED);
                        rsi.insertRowAtRangeIndex(i + 1, rw);
                        rsi.setCurrentRow(rw);
                        flag=1;
                    }

                    st = new StringTokenizer(strLine, ",");
                    while (st.hasMoreTokens()) {
                        tokenNumber++;

                        String theToken = st.nextToken();
                        System.out.println("Line # " + lineNumber + ", Token # " +
                                           tokenNumber + ", Token : " + theToken+" flag:"+flag+" tokenNumber:"+tokenNumber);
                        if (lineNumber > 1 ) {
                            if(!validatedata(theToken) && flag==1){
                                System.out.println(" !validatedata(theToken): "+theToken);
                                list.add(theToken);
                                rw.remove();
                                break;
                            }
                            
                            switch (tokenNumber) {
                            case 1:
                                System.out.println("validate emp:"+theToken);
                                rw.setAttribute("EmpNo", theToken);                             
                                rw.setAttribute("UnitCd", bindUnit.getValue());
                                rw.setAttribute("Year", bindYear.getValue());
                                rw.setAttribute("Month", bindMonth.getValue());
                                rw.setAttribute("SalCode", bindSalCode.getValue());
                                flag=0;
                            break;
                            case 2:
                                rw.setAttribute("Amount", theToken);
                                flag=0;
                            break;
                            }
                        }
                    }
                    //reset token number
                    tokenNumber = 0;
                }
               rsi.closeRowSetIterator();
            } catch (IOException e) {
                e.printStackTrace();
                ADFUtils.showMessage("Content error in uploaded file", 0);
                rsi.closeRowSetIterator();
            } catch (Exception e) {
                e.printStackTrace();
                ADFUtils.showMessage("Data error in uploaded file", 0);
                rsi.closeRowSetIterator();
            }
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public UploadedFile getFile() {
        return file;
    }
    
    public void displayErrors(){
        for(String obj:list)  {
            System.out.println(obj);  
             ADFUtils.showMessage("Invalid Employee Number:"+obj, 0);
         } 
    }
    
    public boolean validatedata(String emp){
        OperationBinding ob = ADFUtils.findOperation("validatedataForEmpMonthlyEarning");
        ob.getParamsMap().put("empcd", emp);
        ob.execute();
        System.out.println("ob.getResult() in bean:"+ob.getResult());
        if(ob.getResult()!= null){  
            boolean result=Boolean.parseBoolean(ob.getResult().toString());
            System.out.println("result:"+result);   
            if(result){
                return true;
            }else{
                return false;
            }
        }else{
            return false;   
        }
    }

    public void setFilePopupBinding(RichPopup filePopupBinding) {
        this.filePopupBinding = filePopupBinding;
    }

    public RichPopup getFilePopupBinding() {
        return filePopupBinding;
    }

    public void setList(ArrayList<String> list) {
        this.list = list;
    }

    public ArrayList<String> getList() {
        return list;
    }
    
}
