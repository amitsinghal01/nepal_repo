package terms.hrm.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichPanelLabelAndMessage;
import oracle.adf.view.rich.component.rich.nav.RichButton;

import oracle.binding.OperationBinding;

public class CreateExtensionOfProbationPeriodBean {
    private RichButton headerEditBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText entryNumberBinding;
    private RichInputDate entryDateBinding;
    private RichInputComboboxListOfValues appointmentLetterNumberBinding;
    private RichInputComboboxListOfValues employeeCodeBinding;
    private RichInputText letterReferenceNumberBinding;
    private RichInputDate letterReferenceDateBinding;
    private RichInputText extensionProbationPeriodBinding;
    private RichInputDate extensionProbationFromBinding;
    private RichInputDate extensionProbationToDateBinding;
    private RichPanelLabelAndMessage bindingOutputText;
    private String message="C";
    private RichInputText designationBinding;

    public CreateExtensionOfProbationPeriodBean() {
    }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void saveButtonAL(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedBy("ExtensionOfProbationPeriodVO1Iterator","LastUpdatedBy");
        ADFUtils.findOperation("generateEntryNumberExtensionOfProbationPeriod").execute();
        OperationBinding opr = ADFUtils.findOperation("Commit");
        Object obj = opr.execute();
        
        System.out.println("outside error block");
         System.out.println("Entry Number value is"+entryNumberBinding.getValue());

         //   if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
         
        //       if(leaveCodeHeaderBinding.getValue() !=null){
        if (message.equalsIgnoreCase("C")){
                System.out.println("S####");
                FacesMessage Message = new FacesMessage("Record Save Successfully. New Entry Number is "+entryNumberBinding.getValue());
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);

            } else if (message.equalsIgnoreCase("E")) {
                FacesMessage Message = new FacesMessage("Record Update Successfully");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            }
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setEntryNumberBinding(RichInputText entryNumberBinding) {
        this.entryNumberBinding = entryNumberBinding;
    }

    public RichInputText getEntryNumberBinding() {
        return entryNumberBinding;
    }

    public void setEntryDateBinding(RichInputDate entryDateBinding) {
        this.entryDateBinding = entryDateBinding;
    }

    public RichInputDate getEntryDateBinding() {
        return entryDateBinding;
    }

    public void setAppointmentLetterNumberBinding(RichInputComboboxListOfValues appointmentLetterNumberBinding) {
        this.appointmentLetterNumberBinding = appointmentLetterNumberBinding;
    }

    public RichInputComboboxListOfValues getAppointmentLetterNumberBinding() {
        return appointmentLetterNumberBinding;
    }

    public void setEmployeeCodeBinding(RichInputComboboxListOfValues employeeCodeBinding) {
        this.employeeCodeBinding = employeeCodeBinding;
    }

    public RichInputComboboxListOfValues getEmployeeCodeBinding() {
        return employeeCodeBinding;
    }

    public void setLetterReferenceNumberBinding(RichInputText letterReferenceNumberBinding) {
        this.letterReferenceNumberBinding = letterReferenceNumberBinding;
    }

    public RichInputText getLetterReferenceNumberBinding() {
        return letterReferenceNumberBinding;
    }

    public void setLetterReferenceDateBinding(RichInputDate letterReferenceDateBinding) {
        this.letterReferenceDateBinding = letterReferenceDateBinding;
    }

    public RichInputDate getLetterReferenceDateBinding() {
        return letterReferenceDateBinding;
    }

    public void setExtensionProbationPeriodBinding(RichInputText extensionProbationPeriodBinding) {
        this.extensionProbationPeriodBinding = extensionProbationPeriodBinding;
    }

    public RichInputText getExtensionProbationPeriodBinding() {
        return extensionProbationPeriodBinding;
    }

    public void setExtensionProbationFromBinding(RichInputDate extensionProbationFromBinding) {
        this.extensionProbationFromBinding = extensionProbationFromBinding;
    }

    public RichInputDate getExtensionProbationFromBinding() {
        return extensionProbationFromBinding;
    }

    public void setExtensionProbationToDateBinding(RichInputDate extensionProbationToDateBinding) {
        this.extensionProbationToDateBinding = extensionProbationToDateBinding;
    }

    public RichInputDate getExtensionProbationToDateBinding() {
        return extensionProbationToDateBinding;
    }

    public void setBindingOutputText(RichPanelLabelAndMessage bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichPanelLabelAndMessage getBindingOutputText() {
          cevmodecheck();
        return bindingOutputText;
    }
    
    private void cevmodecheck(){
          if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
              makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
              cevModeDisableComponent("V");
          }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
              makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
              cevModeDisableComponent("C");
          }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
              makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
              cevModeDisableComponent("E");
          }
      }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
               try {
                   Method method1 =
                       component.getClass().getMethod("setDisabled", boolean.class);
                   if (method1 != null) {
                       method1.invoke(component, valueComponent);
                   }
               } catch (NoSuchMethodException e) {
                   try {
                       Method method =
                           component.getClass().getMethod("setReadOnly", boolean.class);
                       if (method != null) {
                           method.invoke(component, valueComponent);
                       }
                   } catch (Exception e1) {
                       // e.printStackTrace();//silently eat this exception.
                   }


               } catch (Exception e) {
                   // e.printStackTrace();//silently eat this exception.
               }
               List<UIComponent> childComponents = component.getChildren();
               for (UIComponent comp : childComponents) {
                   makeComponentHierarchyReadOnly(comp, valueComponent);
               }
           }
    
    public void cevModeDisableComponent(String mode) {
        if (mode.equals("E")) {
                getUnitCodeBinding().setDisabled(true);
                getEntryNumberBinding().setDisabled(true);
                getEntryDateBinding().setDisabled(true);
                getAppointmentLetterNumberBinding().setDisabled(true);
                getEmployeeCodeBinding().setDisabled(true);
                getLetterReferenceNumberBinding().setDisabled(false);
                getLetterReferenceDateBinding().setDisabled(false);
                getExtensionProbationPeriodBinding().setDisabled(false);
                getExtensionProbationFromBinding().setDisabled(false);
                getExtensionProbationToDateBinding().setDisabled(false);
                getDesignationBinding().setDisabled(true);
                getHeaderEditBinding().setDisabled(true);
                message="E";
            } else if (mode.equals("C")) {
                getUnitCodeBinding().setDisabled(true);
                getEntryNumberBinding().setDisabled(true);
                getEntryDateBinding().setDisabled(true);
                getAppointmentLetterNumberBinding().setDisabled(false);
                getEmployeeCodeBinding().setDisabled(false);
                getLetterReferenceNumberBinding().setDisabled(false);
                getLetterReferenceDateBinding().setDisabled(false);
                getExtensionProbationPeriodBinding().setDisabled(false);
                getExtensionProbationFromBinding().setDisabled(false);
                getDesignationBinding().setDisabled(true);
                getExtensionProbationToDateBinding().setDisabled(false);
            } else if (mode.equals("V")) {
                
            }
            
        }

    public void setDesignationBinding(RichInputText designationBinding) {
        this.designationBinding = designationBinding;
    }

    public RichInputText getDesignationBinding() {
        return designationBinding;
    }
}
