package terms.hrm.transaction.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.math.BigInteger;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;

import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichPanelLabelAndMessage;

import oracle.adf.view.rich.component.rich.nav.RichButton;

import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

public class CreateFullAndFinalBean {
    private String message = "C";
    private RichButton headerEditBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichOutputText outputTextBinding;
    private RichInputComboboxListOfValues unitCdBinding;
    private RichInputComboboxListOfValues empCodeBinding;
    private RichInputDate fullAndFinalDateBinding;
    private RichInputText departmentBinding;
    private RichInputText designationBinding;
    private RichInputDate lastWorkingDateBinding;
    private RichInputDate joiningDateBinding;
    private RichInputDate relievingDateBinding;
    private RichInputDate resignationDateBinding;
    private RichInputText leavesElBinding;
    private RichInputText leavesSlBinding;
    private RichInputText leavesClBinding;
    private RichInputText salaryDaysBinding;
    private RichInputText netAmountBinding;
    private RichInputText canteenDedBinding;
    private RichInputText gratuityAmtBinding;
    private RichInputText otherAmtBinding;
    private RichInputText noticePerAmtBinding;
    private RichInputText bonusAmtBinding;
    private RichInputText srNoEarningBinding;
    private RichInputText headTypeEarningBinding;
    private RichInputText srNoDedBinding;
    private RichInputText headTypeDedBinding;
    private RichInputText srNoReimbBinding;
    private RichInputText headCodeReimbBinding;
    private RichInputText empNameBinding;
    private RichInputText fatherNameBinding;
    private RichButton earningCreateButtonBinding;
    private RichButton reimbCreateButtonBinding;
    private RichButton dednCreateButtonBinding;
    private RichButton populateBinding;


    public CreateFullAndFinalBean() {
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setOutputTextBinding(RichOutputText outputTextBinding) {
        this.outputTextBinding = outputTextBinding;
    }

    public RichOutputText getOutputTextBinding() {
        cevmodecheck();
        System.out.println("Flow   " + ADFUtils.resolveExpression("#{pageFlowScope.mode}"));
        return outputTextBinding;
    }

    public void setUnitCdBinding(RichInputComboboxListOfValues unitCdBinding) {
        this.unitCdBinding = unitCdBinding;
    }

    public RichInputComboboxListOfValues getUnitCdBinding() {
        return unitCdBinding;
    }

    public void setEmpCodeBinding(RichInputComboboxListOfValues empCodeBinding) {
        this.empCodeBinding = empCodeBinding;
    }

    public RichInputComboboxListOfValues getEmpCodeBinding() {
        return empCodeBinding;
    }

    public void setFullAndFinalDateBinding(RichInputDate fullAndFinalDateBinding) {
        this.fullAndFinalDateBinding = fullAndFinalDateBinding;
    }

    public RichInputDate getFullAndFinalDateBinding() {
        return fullAndFinalDateBinding;
    }

    public void setDepartmentBinding(RichInputText departmentBinding) {
        this.departmentBinding = departmentBinding;
    }

    public RichInputText getDepartmentBinding() {
        return departmentBinding;
    }

    public void setDesignationBinding(RichInputText designationBinding) {
        this.designationBinding = designationBinding;
    }

    public RichInputText getDesignationBinding() {
        return designationBinding;
    }

    public void setLastWorkingDateBinding(RichInputDate lastWorkingDateBinding) {
        this.lastWorkingDateBinding = lastWorkingDateBinding;
    }

    public RichInputDate getLastWorkingDateBinding() {
        return lastWorkingDateBinding;
    }

    public void setJoiningDateBinding(RichInputDate joiningDateBinding) {
        this.joiningDateBinding = joiningDateBinding;
    }

    public RichInputDate getJoiningDateBinding() {
        return joiningDateBinding;
    }

    public void setRelievingDateBinding(RichInputDate relievingDateBinding) {
        this.relievingDateBinding = relievingDateBinding;
    }

    public RichInputDate getRelievingDateBinding() {
        return relievingDateBinding;
    }

    public void setResignationDateBinding(RichInputDate resignationDateBinding) {
        this.resignationDateBinding = resignationDateBinding;
    }

    public RichInputDate getResignationDateBinding() {
        return resignationDateBinding;
    }

    public void setLeavesElBinding(RichInputText leavesElBinding) {
        this.leavesElBinding = leavesElBinding;
    }

    public RichInputText getLeavesElBinding() {
        return leavesElBinding;
    }

    public void setLeavesSlBinding(RichInputText leavesSlBinding) {
        this.leavesSlBinding = leavesSlBinding;
    }

    public RichInputText getLeavesSlBinding() {
        return leavesSlBinding;
    }

    public void setLeavesClBinding(RichInputText leavesClBinding) {
        this.leavesClBinding = leavesClBinding;
    }

    public RichInputText getLeavesClBinding() {
        return leavesClBinding;
    }

    public void setSalaryDaysBinding(RichInputText salaryDaysBinding) {
        this.salaryDaysBinding = salaryDaysBinding;
    }

    public RichInputText getSalaryDaysBinding() {
        return salaryDaysBinding;
    }


    public void saveButtonAL(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedBy("FullAndFinalVO1Iterator","LastUpdatedBy");
        OperationBinding opr = ADFUtils.findOperation("Commit");
        Object obj = opr.execute();


        if (message.equalsIgnoreCase("C")) {
            System.out.println("S####");
            FacesMessage Message = new FacesMessage("Record Saved Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        } else if (message.equalsIgnoreCase("E")) {
            FacesMessage Message = new FacesMessage("Record Updated Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

            ADFUtils.setEL("#{pageFlowScope.mode}", "V");
            cevmodecheck();
            AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);

        }
    }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void employeeCodeVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());

        System.out.println("Before call method.");
        OperationBinding op = ADFUtils.findOperation("getFullAndFinalHeaderValues");
        op.execute();

        System.out.println("After call method.");
        OperationBinding op1 = ADFUtils.findOperation("getLeavesValue");
        op1.execute();

        OperationBinding op3 = ADFUtils.findOperation("getLeavesCL");
        op3.execute();

        OperationBinding op4 = ADFUtils.findOperation("getLeavesSL");
        op4.execute();


        OperationBinding op2 = ADFUtils.findOperation("getSalaryDays");
        op2.execute();

    }

    public void populateButtonAL(ActionEvent actionEvent) {
        OperationBinding opp = ADFUtils.findOperation("CallProcedureForFnfEarning");
        opp.execute();

        OperationBinding oppp = ADFUtils.findOperation("CallProcedureForFnfDed");
        oppp.execute();

        OperationBinding opppp = ADFUtils.findOperation("CallProcedureForFnfReimb");
        opppp.execute();

        System.out.println("Before call method");
        OperationBinding op = ADFUtils.findOperation("populateFullAndFinalEarningDetailBlock");
        op.execute();
        System.out.println("After call method");
        OperationBinding op1 = ADFUtils.findOperation("populateFullAndFinalDeductionDetailBlock");
        op1.execute();

        OperationBinding op2 = ADFUtils.findOperation("populateFullAndFinalReimbursmentDetailBlock");
        op2.execute();
        System.out.println("before function call");
        OperationBinding op4 = ADFUtils.findOperation("gratuttyamt");
        op4.execute();
        System.out.println("after function call");
        populateBinding.setDisabled(true);
        //        OperationBinding op3 = ADFUtils.findOperation("totalSum");
        //        op3.execute();
        totalSum();
    }

    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }

    public void cevModeDisableComponent(String mode) {
        if (mode.equals("E")) {
            getUnitCdBinding().setDisabled(true);
            getEmpCodeBinding().setDisabled(true);
            getDepartmentBinding().setDisabled(true);
            getFullAndFinalDateBinding().setDisabled(true);
            getDesignationBinding().setDisabled(true);
            getLastWorkingDateBinding().setDisabled(false);
            getRelievingDateBinding().setDisabled(false);
            getResignationDateBinding().setDisabled(true);
            getLeavesClBinding().setDisabled(false);
            getLeavesElBinding().setDisabled(false);
            getLeavesSlBinding().setDisabled(false);
            getSalaryDaysBinding().setDisabled(false);
            getEmpNameBinding().setDisabled(true);
            getFatherNameBinding().setDisabled(true);
            getJoiningDateBinding().setDisabled(true);

            getSrNoEarningBinding().setDisabled(true);
            getHeadTypeEarningBinding().setDisabled(true);

            getSrNoDedBinding().setDisabled(true);
            getHeadTypeDedBinding().setDisabled(true);

            getSrNoReimbBinding().setDisabled(true);
            //            getHeadCodeReimbBinding().setDisabled(true);

            getNetAmountBinding().setDisabled(true);

            getEarningCreateButtonBinding().setDisabled(false);
            getReimbCreateButtonBinding().setDisabled(false);
            getDednCreateButtonBinding().setDisabled(false);


            message = "E";
        } else if (mode.equals("C")) {
            getUnitCdBinding().setDisabled(true);
            getFullAndFinalDateBinding().setDisabled(true);
            getDepartmentBinding().setDisabled(true);
            getFullAndFinalDateBinding().setDisabled(true);
            getDesignationBinding().setDisabled(true);
            getLastWorkingDateBinding().setDisabled(false);
            getRelievingDateBinding().setDisabled(false);
            getResignationDateBinding().setDisabled(true);
            getLeavesClBinding().setDisabled(false);
            getLeavesElBinding().setDisabled(false);
            getLeavesSlBinding().setDisabled(false);
            getSalaryDaysBinding().setDisabled(false);
            getEmpNameBinding().setDisabled(true);
            getFatherNameBinding().setDisabled(true);
            getJoiningDateBinding().setDisabled(true);

            getSrNoEarningBinding().setDisabled(true);
            getHeadTypeEarningBinding().setDisabled(true);

            getSrNoDedBinding().setDisabled(true);
            getHeadTypeDedBinding().setDisabled(true);

            getSrNoReimbBinding().setDisabled(true);
            //            getHeadCodeReimbBinding().setDisabled(true);

            getNetAmountBinding().setDisabled(true);

            //            getEarningCreateButtonBinding().setDisabled(true);
            //            getReimbCreateButtonBinding().setDisabled(true);
            //            getDednCreateButtonBinding().setDisabled(true);


        } else if (mode.equals("V")) {
            getEarningCreateButtonBinding().setDisabled(true);
            getReimbCreateButtonBinding().setDisabled(true);
            getDednCreateButtonBinding().setDisabled(true);


        }

    }

    public void earningAmtPayVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce.getNewValue() != null) {
            totalSum();
        }
    }

    public void deductionAmtPayVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());

        if (vce.getNewValue() != null) {
            totalSum();
        }
    }

    public void reimburseAmtPayVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());

        if (vce.getNewValue() != null) {
            totalSum();
        }
    }

    public void totalSum() {
        System.out.println("sumAmtPayTrans   ---->>>>" +
                           ADFUtils.resolveExpression("#{bindings.sumAmtPayTrans.inputValue}"));
        BigDecimal amt =
            (BigDecimal) ADFUtils.resolveExpression("#{bindings.sumAmtPayTrans.inputValue}") == null ?
            new BigDecimal(0) : (BigDecimal) ADFUtils.resolveExpression("#{bindings.sumAmtPayTrans.inputValue}");
        
        System.out.println(amt+" amt");
        BigDecimal amt1 =
            (BigDecimal) ADFUtils.resolveExpression("#{bindings.amtPaySumTrans.inputValue}") == null ?
            new BigDecimal(0) : (BigDecimal) ADFUtils.resolveExpression("#{bindings.amtPaySumTrans.inputValue}");
        BigDecimal amt2 =
            (BigDecimal) ADFUtils.resolveExpression("#{bindings.amtPaySumTrans1.inputValue}") == null ?
            new BigDecimal(0) : (BigDecimal) ADFUtils.resolveExpression("#{bindings.amtPaySumTrans1.inputValue}");

        System.out.println("sumAmtPayTrans   ---->>>>" + amt + " " + amt1 + " " + amt2);


        BigDecimal EarnAmt = amt == null ? new BigDecimal(0) : amt;
        BigDecimal DednAmt = amt1 == null ? new BigDecimal(0) : amt1;
        BigDecimal ReimAmt = amt2 == null ? new BigDecimal(0) : amt2;

        BigDecimal grAmt =
            (BigDecimal) gratuityAmtBinding.getValue() == null ? new BigDecimal(0) :
            (BigDecimal) gratuityAmtBinding.getValue();
        // BigDecimal cantDed=(BigDecimal)canteenDedBinding.getValue()==null ? new BigDecimal(0) : (BigDecimal)canteenDedBinding.getValue();
        BigDecimal otherAmt =
            (BigDecimal) otherAmtBinding.getValue() == null ? new BigDecimal(0) :
            (BigDecimal) otherAmtBinding.getValue();
        BigDecimal NoticePerAmt =
            (BigDecimal) noticePerAmtBinding.getValue() == null ? new BigDecimal(0) :
            (BigDecimal) noticePerAmtBinding.getValue();
        BigDecimal bonusAmt =
            (BigDecimal) bonusAmtBinding.getValue() == null ? new BigDecimal(0) :
            (BigDecimal) bonusAmtBinding.getValue();

        System.out.println(otherAmt + "   otherAmt");
        BigDecimal total =
            ((((((EarnAmt.subtract(DednAmt)).add(ReimAmt)).add(grAmt)).add(bonusAmt)).subtract(otherAmt)).subtract(NoticePerAmt));
        System.out.println("Earn " + EarnAmt);
        System.out.println("Total=====>" + total.setScale(2, BigDecimal.ROUND_HALF_UP));
        Row row = (Row) ADFUtils.evaluateEL("#{bindings.FullAndFinalEarningVO1Iterator.currentRow}");
        if (row != null)
            row.setAttribute("NetAmount", total.setScale(2, BigDecimal.ROUND_HALF_UP));
        //        netAmountBinding.setValue(total);

    }

    public void setNetAmountBinding(RichInputText netAmountBinding) {
        this.netAmountBinding = netAmountBinding;
    }

    public RichInputText getNetAmountBinding() {
        return netAmountBinding;
    }

    public void setCanteenDedBinding(RichInputText canteenDedBinding) {
        this.canteenDedBinding = canteenDedBinding;
    }

    public RichInputText getCanteenDedBinding() {
        return canteenDedBinding;
    }

    public void setGratuityAmtBinding(RichInputText gratuityAmtBinding) {
        this.gratuityAmtBinding = gratuityAmtBinding;
    }

    public RichInputText getGratuityAmtBinding() {
        return gratuityAmtBinding;
    }

    public void setOtherAmtBinding(RichInputText otherAmtBinding) {
        this.otherAmtBinding = otherAmtBinding;
    }

    public RichInputText getOtherAmtBinding() {
        return otherAmtBinding;
    }

    public void setNoticePerAmtBinding(RichInputText noticePerAmtBinding) {
        this.noticePerAmtBinding = noticePerAmtBinding;
    }

    public RichInputText getNoticePerAmtBinding() {
        return noticePerAmtBinding;
    }

    public void setBonusAmtBinding(RichInputText bonusAmtBinding) {
        this.bonusAmtBinding = bonusAmtBinding;
    }

    public RichInputText getBonusAmtBinding() {
        return bonusAmtBinding;
    }

    public void setSrNoEarningBinding(RichInputText srNoEarningBinding) {
        this.srNoEarningBinding = srNoEarningBinding;
    }

    public RichInputText getSrNoEarningBinding() {
        return srNoEarningBinding;
    }

    public void setHeadTypeEarningBinding(RichInputText headTypeEarningBinding) {
        this.headTypeEarningBinding = headTypeEarningBinding;
    }

    public RichInputText getHeadTypeEarningBinding() {
        return headTypeEarningBinding;
    }

    public void setSrNoDedBinding(RichInputText srNoDedBinding) {
        this.srNoDedBinding = srNoDedBinding;
    }

    public RichInputText getSrNoDedBinding() {
        return srNoDedBinding;
    }

    public void setHeadTypeDedBinding(RichInputText headTypeDedBinding) {
        this.headTypeDedBinding = headTypeDedBinding;
    }

    public RichInputText getHeadTypeDedBinding() {
        return headTypeDedBinding;
    }

    public void setSrNoReimbBinding(RichInputText srNoReimbBinding) {
        this.srNoReimbBinding = srNoReimbBinding;
    }

    public RichInputText getSrNoReimbBinding() {
        return srNoReimbBinding;
    }

    public void setHeadCodeReimbBinding(RichInputText headCodeReimbBinding) {
        this.headCodeReimbBinding = headCodeReimbBinding;
    }

    public RichInputText getHeadCodeReimbBinding() {
        return headCodeReimbBinding;
    }

    public void setEmpNameBinding(RichInputText empNameBinding) {
        this.empNameBinding = empNameBinding;
    }

    public RichInputText getEmpNameBinding() {
        return empNameBinding;
    }

    public void setFatherNameBinding(RichInputText fatherNameBinding) {
        this.fatherNameBinding = fatherNameBinding;
    }

    public RichInputText getFatherNameBinding() {
        return fatherNameBinding;
    }

    public void setEarningCreateButtonBinding(RichButton earningCreateButtonBinding) {
        this.earningCreateButtonBinding = earningCreateButtonBinding;
    }

    public RichButton getEarningCreateButtonBinding() {
        return earningCreateButtonBinding;
    }

    public void setReimbCreateButtonBinding(RichButton reimbCreateButtonBinding) {
        this.reimbCreateButtonBinding = reimbCreateButtonBinding;
    }

    public RichButton getReimbCreateButtonBinding() {
        return reimbCreateButtonBinding;
    }

    public void setDednCreateButtonBinding(RichButton dednCreateButtonBinding) {
        this.dednCreateButtonBinding = dednCreateButtonBinding;
    }

    public RichButton getDednCreateButtonBinding() {
        return dednCreateButtonBinding;
    }

    public void grAmountVCl(ValueChangeEvent valueChangeEvent) {
        if (valueChangeEvent.getNewValue() != null) {
            totalSum();
        }
    }

    public void otherAmtVCL(ValueChangeEvent valueChangeEvent) {
        if (valueChangeEvent.getNewValue() != null) {
            totalSum();
        }
    }

    public void noticeAmtVCL(ValueChangeEvent valueChangeEvent) {
        if (valueChangeEvent.getNewValue() != null) {
            totalSum();
        }
    }

    public void bonusAmtVCL(ValueChangeEvent valueChangeEvent) {
        if (valueChangeEvent.getNewValue() != null) {
            totalSum();
        }
    }

    public void earningCreatAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert1").execute();
        Row row = (Row) ADFUtils.evaluateEL("#{bindings.FullAndFinalEarningVO1.currentRow}");
        row.setAttribute("SlNo", ADFUtils.evaluateEL("#{bindings.FullAndFinalEarningVO1.estimatedRowCount}"));
    }

    public void dednCreateAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert2").execute();
        Row row = (Row) ADFUtils.evaluateEL("#{bindings.FullAndFinalReimbursmentVO1.currentRow}");
        row.setAttribute("SlNo", ADFUtils.evaluateEL("#{bindings.FullAndFinalReimbursmentVO1.estimatedRowCount}"));

    }

    public void dedn1CreateAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert").execute();
        Row row = (Row) ADFUtils.evaluateEL("#{bindings.FullAndFinalDeductionVO1.currentRow}");
        row.setAttribute("SlNo", ADFUtils.evaluateEL("#{bindings.FullAndFinalDeductionVO1.estimatedRowCount}"));

    }

    public void setPopulateBinding(RichButton populateBinding) {
        this.populateBinding = populateBinding;
    }

    public RichButton getPopulateBinding() {
        return populateBinding;
    }
}
