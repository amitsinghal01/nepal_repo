package terms.hrm.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.binding.OperationBinding;

public class CreateIncomeTaxDeclarationBean {
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputComboboxListOfValues employeeBinding;
    private RichInputText mobileNoBinding;
    private RichInputText transactionIdBinding;
    private RichOutputText bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichShowDetailItem deductionU80cTabBinding;
    private RichShowDetailItem deductionU80GTabBinding;
    private RichShowDetailItem interestOnHomeLoanRendTabBinding;
    private RichShowDetailItem otherEarningTabBinding;
    private RichInputDate entryDateBinding;
    private RichInputText lifeInsurenceSalaryBinding;

    public CreateIncomeTaxDeclarationBean() {
    }

    public void saveButtonAL(ActionEvent actionEvent) 
    
    {
        ADFUtils.setLastUpdatedBy("IncomeTaxDeclarationVO1Iterator","LastUpdatedBy");
        
        if(unitCodeBinding.getValue()!=null && employeeBinding.getValue()!=null)
        {
            OperationBinding opr = ADFUtils.findOperation("generateNumberForIncomeTaxDeclaration");
            opr.execute();
            System.out.println("Result is============>"+opr.getResult());
            if (opr.getResult() != null && opr.getResult().equals("Y")) {
                ADFUtils.findOperation("Commit").execute();
                ADFUtils.showMessage("Record saved successfully,New Transaction id is " +
                                     transactionIdBinding.getValue(), 2);
            } else if (opr.getResult() != null && opr.getResult().equals("N")) {
                ADFUtils.findOperation("Commit").execute();
                ADFUtils.showMessage("Record Updated successfully.", 2);
            }
        }
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setEmployeeBinding(RichInputComboboxListOfValues employeeBinding) {
        this.employeeBinding = employeeBinding;
    }

    public RichInputComboboxListOfValues getEmployeeBinding() {
        return employeeBinding;
    }

    public void setMobileNoBinding(RichInputText mobileNoBinding) {
        this.mobileNoBinding = mobileNoBinding;
    }

    public RichInputText getMobileNoBinding() {
        return mobileNoBinding;
    }

    public void setTransactionIdBinding(RichInputText transactionIdBinding) {
        this.transactionIdBinding = transactionIdBinding;
    }

    public RichInputText getTransactionIdBinding() {
        return transactionIdBinding;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }
    private void cevmodecheck(){
           if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
               cevModeDisableComponent("V");
           }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
               cevModeDisableComponent("C");
           }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
               cevModeDisableComponent("E");
           }
       }
    
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                try {
                    Method method1 =
                        component.getClass().getMethod("setDisabled", boolean.class);
                    if (method1 != null) {
                        method1.invoke(component, valueComponent);
                    }
                } catch (NoSuchMethodException e) {
                    try {
                        Method method =
                            component.getClass().getMethod("setReadOnly", boolean.class);
                        if (method != null) {
                            method.invoke(component, valueComponent);
                        }
                    } catch (Exception e1) {
                        // e.printStackTrace();//silently eat this exception.
                    }


                } catch (Exception e) {
                    // e.printStackTrace();//silently eat this exception.
                }
                List<UIComponent> childComponents = component.getChildren();
                for (UIComponent comp : childComponents) {
                    makeComponentHierarchyReadOnly(comp, valueComponent);
                }
            }
    public void cevModeDisableComponent(String mode) {
        if (mode.equals("E"))
        {
        getUnitCodeBinding().setDisabled(true);
        getTransactionIdBinding().setDisabled(true);
        getEmployeeBinding().setDisabled(true);
        getEntryDateBinding().setDisabled(true);
        getLifeInsurenceSalaryBinding().setDisabled(true);
        } else if (mode.equals("C")) 
        {
        getLifeInsurenceSalaryBinding().setDisabled(true);
        if(ADFUtils.evaluateEL("#{pageFlowScope.userName}")!=null)
        {
        String CodeName = (String)ADFUtils.evaluateEL("#{pageFlowScope.userName}");
        if(CodeName.equalsIgnoreCase("Admin"))
        getEmployeeBinding().setDisabled(false);
        else
        getEmployeeBinding().setDisabled(true);
        }
        getUnitCodeBinding().setDisabled(true);
        getTransactionIdBinding().setDisabled(true);
        } else if (mode.equals("V")) 
        {
        deductionU80cTabBinding.setDisabled(false);
        deductionU80GTabBinding.setDisabled(false);
        interestOnHomeLoanRendTabBinding.setDisabled(false);
        otherEarningTabBinding.setDisabled(false);
        }
        
    }

    public void editButtonAL(ActionEvent actionEvent) 
    {
        cevmodecheck();        
    }

    public void setDeductionU80cTabBinding(RichShowDetailItem deductionU80cTabBinding) {
        this.deductionU80cTabBinding = deductionU80cTabBinding;
    }

    public RichShowDetailItem getDeductionU80cTabBinding() {
        return deductionU80cTabBinding;
    }

    public void setDeductionU80GTabBinding(RichShowDetailItem deductionU80GTabBinding) {
        this.deductionU80GTabBinding = deductionU80GTabBinding;
    }

    public RichShowDetailItem getDeductionU80GTabBinding() {
        return deductionU80GTabBinding;
    }

    public void setInterestOnHomeLoanRendTabBinding(RichShowDetailItem interestOnHomeLoanRendTabBinding) {
        this.interestOnHomeLoanRendTabBinding = interestOnHomeLoanRendTabBinding;
    }

    public RichShowDetailItem getInterestOnHomeLoanRendTabBinding() {
        return interestOnHomeLoanRendTabBinding;
    }

    public void setOtherEarningTabBinding(RichShowDetailItem otherEarningTabBinding) {
        this.otherEarningTabBinding = otherEarningTabBinding;
    }

    public RichShowDetailItem getOtherEarningTabBinding() {
        return otherEarningTabBinding;
    }

    public void setEntryDateBinding(RichInputDate entryDateBinding) {
        this.entryDateBinding = entryDateBinding;
    }

    public RichInputDate getEntryDateBinding() {
        return entryDateBinding;
    }

    public String cancelButtonAction() {
        //#{pageFlowScope.pageMode}
        System.out.println("Page Mode Value="+ADFUtils.evaluateEL("#{pageFlowScope.pageMode}"));
        if(ADFUtils.evaluateEL("#{pageFlowScope.pageMode}")!=null)
        {
        String pageModeValue =(String)ADFUtils.evaluateEL("#{pageFlowScope.pageMode}");
        if(pageModeValue.equalsIgnoreCase("ITD"))
        {
        System.out.println("When Value Is "+pageModeValue);
        ADFUtils.findOperation("Rollback").execute();
        }
        return "saveAndClose"; 
        }
        return null;
    }

    public void setLifeInsurenceSalaryBinding(RichInputText lifeInsurenceSalaryBinding) {
        this.lifeInsurenceSalaryBinding = lifeInsurenceSalaryBinding;
    }

    public RichInputText getLifeInsurenceSalaryBinding() {
        return lifeInsurenceSalaryBinding;
    }
}
