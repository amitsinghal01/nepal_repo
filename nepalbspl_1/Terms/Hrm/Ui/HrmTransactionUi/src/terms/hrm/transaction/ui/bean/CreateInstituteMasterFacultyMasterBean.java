package terms.hrm.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class CreateInstituteMasterFacultyMasterBean {
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText unitNameBinding;
    private RichInputText instituteNameBinding;
    private RichSelectOneChoice placeBinding;
    private RichInputText cityBinding;
    private RichInputText phoneBinding;
    private RichInputText firstAddressBiding;
    private RichInputText secondaddressBiding;
    private RichInputText instituteIdBinding;
    private RichTable createInstituteMasterFacultyMasterTableBinding;
    private RichInputComboboxListOfValues unitCodeDetailBinding;
    private RichInputText unitNameDetailBinding;
    private RichInputText facultyIdBinding;
    private RichInputText facultyNameBinding;
    //private RichInputText instituteNameDetailBinding;
    private RichInputText firstAddressDetailBinding;
    private RichInputText secondAddressDetailBinding;
    private RichInputText cityDetailBinding;
    private RichInputText phoneDetailBnding;
    private RichOutputText bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichButton detailcreateBinding;
    private RichButton detaildeleteBinding;
    private String message="C";

    public CreateInstituteMasterFacultyMasterBean() {
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void popUpDialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                                    {
        ADFUtils.findOperation("Delete").execute();
        OperationBinding op= ADFUtils.findOperation("Commit");
        Object rst = op.execute();
        System.out.println("Record Delete Successfully");

        if(op.getErrors().isEmpty())
         {
        FacesMessage Message = new 
        FacesMessage("Record Deleted Successfully");
        Message.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext fc =  FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        }
        else
        {
        FacesMessage Message = new 
        FacesMessage("Record cannot be deleted. Child record found.");
        Message.setSeverity(FacesMessage.SEVERITY_ERROR);
        FacesContext fc =   FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        }
                     }
        AdfFacesContext.getCurrentInstance().addPartialTarget(createInstituteMasterFacultyMasterTableBinding);
        }

    public void setUnitNameBinding(RichInputText unitNameBinding) {
        this.unitNameBinding = unitNameBinding;
    }

    public RichInputText getUnitNameBinding() {
        return unitNameBinding;
    }

    public void setInstituteNameBinding(RichInputText instituteNameBinding) {
        this.instituteNameBinding = instituteNameBinding;
    }

    public RichInputText getInstituteNameBinding() {
        return instituteNameBinding;
    }

    public void setPlaceBinding(RichSelectOneChoice placeBinding) {
        this.placeBinding = placeBinding;
    }

    public RichSelectOneChoice getPlaceBinding() {
        return placeBinding;
    }

    public void setCityBinding(RichInputText cityBinding) {
        this.cityBinding = cityBinding;
    }

    public RichInputText getCityBinding() {
        return cityBinding;
    }

    public void setPhoneBinding(RichInputText phoneBinding) {
        this.phoneBinding = phoneBinding;
    }

    public RichInputText getPhoneBinding() {
        return phoneBinding;
    }

    public void setFirstAddressBiding(RichInputText firstAddressBiding) {
        this.firstAddressBiding = firstAddressBiding;
    }

    public RichInputText getFirstAddressBiding() {
        return firstAddressBiding;
    }

    public void setSecondaddressBiding(RichInputText secondaddressBiding) {
        this.secondaddressBiding = secondaddressBiding;
    }

    public RichInputText getSecondaddressBiding() {
        return secondaddressBiding;
    }

    public void setInstituteIdBinding(RichInputText instituteIdBinding) {
        this.instituteIdBinding = instituteIdBinding;
    }

    public RichInputText getInstituteIdBinding() {
        return instituteIdBinding;
    }

    public void setCreateInstituteMasterFacultyMasterTableBinding(RichTable createInstituteMasterFacultyMasterTableBinding) {
        this.createInstituteMasterFacultyMasterTableBinding = createInstituteMasterFacultyMasterTableBinding;
    }

    public RichTable getCreateInstituteMasterFacultyMasterTableBinding() {
        return createInstituteMasterFacultyMasterTableBinding;
    }

    public void setUnitCodeDetailBinding(RichInputComboboxListOfValues unitCodeDetailBinding) {
        this.unitCodeDetailBinding = unitCodeDetailBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeDetailBinding() {
        return unitCodeDetailBinding;
    }

    public void setUnitNameDetailBinding(RichInputText unitNameDetailBinding) {
        this.unitNameDetailBinding = unitNameDetailBinding;
    }

    public RichInputText getUnitNameDetailBinding() {
        return unitNameDetailBinding;
    }

    public void setFacultyIdBinding(RichInputText facultyIdBinding) {
        this.facultyIdBinding = facultyIdBinding;
    }

    public RichInputText getFacultyIdBinding() {
        return facultyIdBinding;
    }

    public void setFacultyNameBinding(RichInputText facultyNameBinding) {
        this.facultyNameBinding = facultyNameBinding;
    }

    public RichInputText getFacultyNameBinding() {
        return facultyNameBinding;
    }

//    public void setInstituteNameDetailBinding(RichInputText instituteNameDetailBinding) {
//        this.instituteNameDetailBinding = instituteNameDetailBinding;
//    }
//
//    public RichInputText getInstituteNameDetailBinding() {
//        return instituteNameDetailBinding;
//    }

    public void setFirstAddressDetailBinding(RichInputText firstAddressDetailBinding) {
        this.firstAddressDetailBinding = firstAddressDetailBinding;
    }

    public RichInputText getFirstAddressDetailBinding() {
        return firstAddressDetailBinding;
    }

    public void setSecondAddressDetailBinding(RichInputText secondAddressDetailBinding) {
        this.secondAddressDetailBinding = secondAddressDetailBinding;
    }

    public RichInputText getSecondAddressDetailBinding() {
        return secondAddressDetailBinding;
    }

    public void setCityDetailBinding(RichInputText cityDetailBinding) {
        this.cityDetailBinding = cityDetailBinding;
    }

    public RichInputText getCityDetailBinding() {
        return cityDetailBinding;
    }

    public void setPhoneDetailBnding(RichInputText phoneDetailBnding) {
        this.phoneDetailBnding = phoneDetailBnding;
    }

    public RichInputText getPhoneDetailBnding() {
        return phoneDetailBnding;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setDetailcreateBinding(RichButton detailcreateBinding) {
        this.detailcreateBinding = detailcreateBinding;
    }

    public RichButton getDetailcreateBinding() {
        return detailcreateBinding;
    }

    public void setDetaildeleteBinding(RichButton detaildeleteBinding) {
        this.detaildeleteBinding = detaildeleteBinding;
    }

    public RichButton getDetaildeleteBinding() {
        return detaildeleteBinding;
    }
    private void cevmodecheck(){
            if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
                cevModeDisableComponent("V");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("C");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("E");
            }
        }
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                try {
                    Method method1 =
                        component.getClass().getMethod("setDisabled", boolean.class);
                    if (method1 != null) {
                        method1.invoke(component, valueComponent);
                    }
                } catch (NoSuchMethodException e) {
                    try {
                        Method method =
                            component.getClass().getMethod("setReadOnly", boolean.class);
                        if (method != null) {
                            method.invoke(component, valueComponent);
                        }
                    } catch (Exception e1) {
                        // e.printStackTrace();//silently eat this exception.
                    }


                } catch (Exception e) {
                    // e.printStackTrace();//silently eat this exception.
                }
                List<UIComponent> childComponents = component.getChildren();
                for (UIComponent comp : childComponents) {
                    makeComponentHierarchyReadOnly(comp, valueComponent);
                }
            }
    //Set Fields and Button disable.
        public void cevModeDisableComponent(String mode) {
        //            FacesContext fctx = FacesContext.getCurrentInstance();
        //            ELContext elctx = fctx.getELContext();
        //            Application jsfApp = fctx.getApplication();
        //            //create a ValueExpression that points to the ADF binding layer
        //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
        //            //
        //            ValueExpression valueExpr = exprFactory.createValueExpression(
        //                                         elctx,
        //                                         "#{pageFlowScope.mode=='E'}",
        //                                          Object.class
        //                                         );
        //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
            if (mode.equals("E")) {
                
                    getHeaderEditBinding().setDisabled(true);
                    getDetailcreateBinding().setDisabled(false);
                    getDetaildeleteBinding().setDisabled(false);
                    getUnitCodeBinding().setDisabled(true);
                    getUnitCodeDetailBinding().setDisabled(true);
                    getInstituteNameBinding().setDisabled(true);
            } else if (mode.equals("C")) {
               
                getDetailcreateBinding().setDisabled(false);
                getDetaildeleteBinding().setDisabled(false);
                getUnitCodeBinding().setDisabled(true);
                getUnitCodeDetailBinding().setDisabled(true);
            } else if (mode.equals("V")) {
                getDetailcreateBinding().setDisabled(true);
                getDetaildeleteBinding().setDisabled(true);
            }
            
        }


    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void saveButtonAL(ActionEvent actionEvent) {
        // Add event code here...
        ADFUtils.setLastUpdatedBy("InstituteMasterFacultyMasterHeaderVO1Iterator","LastUpdatedBy");
        OperationBinding opr = ADFUtils.findOperation("Commit");
        Object obj = opr.execute();
        if (message.equalsIgnoreCase("C")){
                System.out.println("S####");
                FacesMessage Message = new FacesMessage("Record Save Successfully.");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);

            } else if (message.equalsIgnoreCase("E")) {
                FacesMessage Message = new FacesMessage("Record Update Successfully");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            }
    }
}
