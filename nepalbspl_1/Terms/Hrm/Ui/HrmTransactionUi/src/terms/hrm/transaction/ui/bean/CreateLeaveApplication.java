package terms.hrm.transaction.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.time.LocalDate;

import java.time.temporal.ChronoUnit;

import java.util.Date;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import javax.faces.validator.ValidatorException;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectBooleanCheckbox;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.ReturnPopupEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

public class CreateLeaveApplication {
    private RichInputComboboxListOfValues empCodeBinding;
    private RichInputText deptHeadBinding;
    private RichSelectOneChoice dayTypeBinding;
    private RichSelectOneChoice firstSecondHalfBinding;
    private RichTable createLeaveApplicationDetailTableBinding;
    private RichInputDate fromDateBinding;
    private RichInputDate toDateBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputComboboxListOfValues leaveTypeBinding;
    private RichInputText noofDaysBinding;
    private RichInputText appNoBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichOutputText editTransBinding;
    private RichButton editButtonAL;
    private RichInputDate appDateBinding;
    private RichSelectOneChoice leaveGrantedBinding;
    private RichInputComboboxListOfValues verByBinding;
    private RichInputText verRemarksBinding;
    private RichInputComboboxListOfValues aprByBinding;
    private RichInputText aprRemarksBinding;
    private RichInputDate cancelFrmBinding;
    private RichInputDate cancelToBinding;
    private RichInputText cancelDaysBinding;
    private RichSelectBooleanCheckbox chkVerByBinding;
    private RichSelectBooleanCheckbox chkAprByBinding;
    private RichInputText leaveTypeDetailBinding;
    private RichInputText opBalDetailBinding;
    private RichInputText currLeaveDetailBinding;
    private RichInputText balDetailBinding;
    private RichInputText availLeaveDetailBinding;
    private RichInputText avildDetailBinding;
    private RichInputText mbalDetailBinding;
    private RichInputText reasonBinding;
    private RichInputDate chkBinding;
    private RichInputComboboxListOfValues appltCdBinding;
    private RichInputText userNameTrans;
    String Status="P";
    public CreateLeaveApplication() {
    }

    public void applicantNameReturnPopup(ReturnPopupEvent returnPopupEvent) {
        OperationBinding op = ADFUtils.findOperation("parameterCheckLeaveApp");
        op.execute();
        if (op.getResult().equals("N")) {
            ADFUtils.showMessage("Please check value at parameter no 801.", 0);
            /*FacesMessage message = new FacesMessage("Please check value at parameter no 801.");
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(empCodeBinding.getClientId(), message);*/
        }
    }

    public void setEmpCodeBinding(RichInputComboboxListOfValues empCodeBinding) {
        this.empCodeBinding = empCodeBinding;
    }

    public RichInputComboboxListOfValues getEmpCodeBinding() {
        return empCodeBinding;
    }

    public void callDepMethod() {
        OperationBinding opr = ADFUtils.findOperation("depHeadLeaveApplication");
        opr.execute();
        System.out.println("dept head:" + opr.getResult());
        deptHeadBinding.setValue(opr.getResult());
        AdfFacesContext.getCurrentInstance().addPartialTarget(deptHeadBinding);

    }

    public void applicantNameVCE(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        callDepMethod();
    }

    public void applicantNameVCE1(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        callDepMethod();
    }

    public void setDeptHeadBinding(RichInputText deptHeadBinding) {
        this.deptHeadBinding = deptHeadBinding;
    }

    public RichInputText getDeptHeadBinding() {
        return deptHeadBinding;
    }

    public void firstSecondHalfValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (object != null) {
            String str_obj = object.toString();
            System.out.println("Object:" + object + "  daytype:" + dayTypeBinding.getValue() + "Str Obj Val" + str_obj);
            if (object != null && dayTypeBinding.getValue() != null) {
                if (dayTypeBinding.getValue().toString().equals("F")) {
                    if (!(str_obj.equals("3"))) {
                        throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                      "Please select correct day type.", null));
                    }
                } else if (dayTypeBinding.getValue().toString().equals("H")) {
                    if (!(str_obj.equals("1") || str_obj.equals("2"))) {
                        throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                      "Please select correct day type.", null));
                    }
                } else if (dayTypeBinding.getValue().toString().equals("Q")) {
                    if (!(str_obj.equals("4") || str_obj.equals("5") || str_obj.equals("6") || str_obj.equals("7"))) {
                        throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                      "Please select correct day type.", null));
                    }
                }
            }
        }
    }

    public void setDayTypeBinding(RichSelectOneChoice dayTypeBinding) {
        this.dayTypeBinding = dayTypeBinding;
    }

    public RichSelectOneChoice getDayTypeBinding() {
        return dayTypeBinding;
    }

    public void dayTypeVCL(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        Row row = (Row) ADFUtils.evaluateEL("#{bindings.LeaveApplicationHeaderVO1Iterator.currentRow}");
        System.out.println("in vcl" + valueChangeEvent.getNewValue());
        if (valueChangeEvent.getNewValue() != null) {

            if (valueChangeEvent.getNewValue().equals("H")) {
                row.setAttribute("LeaveTypeDet", 1);
            } else if (valueChangeEvent.getNewValue().equals("F")) {
                row.setAttribute("LeaveTypeDet", 3);
            } else if (valueChangeEvent.getNewValue().equals("Q")) {
                row.setAttribute("LeaveTypeDet", 4);
            }

            AdfFacesContext.getCurrentInstance().addPartialTarget(firstSecondHalfBinding);
        }

    }

    public void setFirstSecondHalfBinding(RichSelectOneChoice firstSecondHalfBinding) {
        this.firstSecondHalfBinding = firstSecondHalfBinding;
    }

    public RichSelectOneChoice getFirstSecondHalfBinding() {
        return firstSecondHalfBinding;
    }


    public void setCreateLeaveApplicationDetailTableBinding(RichTable createLeaveApplicationDetailTableBinding) {
        this.createLeaveApplicationDetailTableBinding = createLeaveApplicationDetailTableBinding;
    }

    public RichTable getCreateLeaveApplicationDetailTableBinding() {
        return createLeaveApplicationDetailTableBinding;
    }

    public void fromDateVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        OperationBinding ob = ADFUtils.findOperation("PopulateLeaveBalance");
        ob.execute();
        //        if(vce.getNewValue()!=null && toDateBinding.getValue()!=null)
        //        {
        //        System.out.println("from DATE"+vce.getNewValue());
        //        LocalDate d1 = LocalDate.parse(vce.getNewValue().toString());
        //        LocalDate d2 = LocalDate.parse(toDateBinding.getValue().toString());
        //        int days = (int) ChronoUnit.DAYS.between(d1, d2);
        //        oracle.jbo.domain.Number num = new oracle.jbo.domain.Number(days);
        //
        //            if(dayTypeBinding.getValue()!= null){
        //                String daytype=dayTypeBinding.getValue().toString();
        //                if (daytype.equals("H")) {
        //                   num= num.add(0.5);
        //                } else if (daytype.equals("F")) {
        //                    num= num.add(1);
        //                } else if (daytype.equals("Q")) {
        //                    num= num.add(0.25);
        //                }
        //            }
        //
        //            System.out.println(" no of days"+num);
        //                Row row =(Row) ADFUtils.evaluateEL("#{bindings.LeaveApplicationHeaderVO1Iterator.currentRow}");
        //                row.setAttribute("LeaveDays", num);
        //        }
    }

    public void toDateVCL(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        System.out.println("values"+valueChangeEvent.getNewValue());
        //edit mode function call
        String mode =(String)ADFUtils.resolveExpression("#{pageFlowScope.mode}");
                       if (mode !=null && mode.equals("E")) {
                       OperationBinding Op = ADFUtils.findOperation("leaveEndDateCheck1");
                       Op.getParamsMap().put("todate", valueChangeEvent.getNewValue());
                       Object check1 = Op.execute();
                       System.out.println("Result is===> " + Op.getResult());
                       if (Op.getResult()!=null && !(((oracle.jbo.domain.Number) Op.getResult()).compareTo(new oracle.jbo.domain.Number(0)) ==
                             0)) {
                           
                           Status="N";
                                }
                       }   
        oracle.jbo.domain.Date d=(oracle.jbo.domain.Date)valueChangeEvent.getNewValue();
        oracle.jbo.domain.Date d13=(oracle.jbo.domain.Date)valueChangeEvent.getOldValue();
        System.out.println("Value of new VCL d: "+d);
        System.out.println("Value of old value d13: "+d13);
        //String mode=(String)ADFUtils.resolveExpression("#{pageFlowScope.mode}");
        if((mode!=null && mode.equals("E") && d!=d13)||(mode!=null && mode.equals("C"))){
            OperationBinding op3=ADFUtils.findOperation("leaveEndDateCheck3");
            Object check4=op3.execute();
            System.out.println("Result is: "+op3.getResult());
            if(op3.getResult()!=null && !(op3.getResult().equals("N"))){
                    FacesMessage message = new FacesMessage("ERROR, PLEASE CHECK");
                    message.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext context = FacesContext.getCurrentInstance();
                    context.addMessage(empCodeBinding.getClientId(), message);
                }
            }
        if (valueChangeEvent.getNewValue() != null && fromDateBinding.getValue() != null) {
            System.out.println("TO DATE" + valueChangeEvent.getNewValue());
            LocalDate d1 = LocalDate.parse(valueChangeEvent.getNewValue().toString());
            LocalDate d2 = LocalDate.parse(fromDateBinding.getValue().toString());
            int days = (int) ChronoUnit.DAYS.between(d2, d1);
            oracle.jbo.domain.Number num = new oracle.jbo.domain.Number(days);

            if (dayTypeBinding.getValue() != null) {
                String daytype = dayTypeBinding.getValue().toString();
                if (daytype.equals("H")) {
                    num = num.add(0.5);
                } else if (daytype.equals("F")) {
                    num = num.add(1);
                } else if (daytype.equals("Q")) {
                    num = num.add(0.25);
                }
            }
            System.out.println(" no of days" + num);
            Row row = (Row) ADFUtils.evaluateEL("#{bindings.LeaveApplicationHeaderVO1Iterator.currentRow}");
            row.setAttribute("LeaveDays", num);
        }
    }


    public void setFromDateBinding(RichInputDate fromDateBinding) {
        this.fromDateBinding = fromDateBinding;
    }

    public RichInputDate getFromDateBinding() {
        return fromDateBinding;
    }

    public void setToDateBinding(RichInputDate toDateBinding) {
        this.toDateBinding = toDateBinding;
    }

    public RichInputDate getToDateBinding() {
        return toDateBinding;
    }

    public void saveAL(ActionEvent actionEvent) {
        if(Status.equals("P")){
        ADFUtils.setLastUpdatedBy("LeaveApplicationHeaderVO1Iterator","LastUpdatedBy");
        System.out.println("----------------------Inside AL---------------");

        OperationBinding Opp = ADFUtils.findOperation("generateLeaveApplicationNumber");
        Object obj = Opp.execute();
        System.out.println("Result is===> " + Opp.getResult());
        if (Opp.getResult() != null && !Opp.getResult().equals("N")) {
            ADFUtils.findOperation("Commit").execute();
            ADFUtils.showMessage("Record Save Successfully.Leave Application Number is " + appNoBinding.getValue(), 2);
            ADFUtils.setEL("#{pageFlowScope.mode}", "V");
            System.out.println("after setting"+ADFUtils.resolveExpression("#{pageFlowScope.mode}"));
            cevmodecheck();
            AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
        } else {
            ADFUtils.findOperation("Commit").execute();
            ADFUtils.showMessage(" Record Updated Successfully", 2);
            ADFUtils.setEL("#{pageFlowScope.mode}", "V");
            System.out.println("after setting"+ADFUtils.resolveExpression("#{pageFlowScope.mode}"));
            cevmodecheck();
            AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
        }
        }
        else{
                    FacesMessage Message = new FacesMessage("Leave application already applied for this period.");
                    Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);
                }
    }

    public void toDateValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (object != null) {
            if (leaveTypeBinding.getValue() != null) {
                if (dayTypeBinding.getValue() != null) {
                    if (firstSecondHalfBinding.getValue() != null) {
                        if (fromDateBinding.getValue() != null) {
                            LocalDate d1 = LocalDate.parse(object.toString());
                            LocalDate d2 = LocalDate.parse(fromDateBinding.getValue().toString());
                            int days = (int) ChronoUnit.DAYS.between(d2, d1);
                            oracle.jbo.domain.Number num = new oracle.jbo.domain.Number(days);

                            if (dayTypeBinding.getValue() != null) {
                                String daytype = dayTypeBinding.getValue().toString();
                                if (daytype.equals("H")) {
                                    num = num.add(0.5);
                                } else if (daytype.equals("F")) {
                                    num = num.add(1);
                                } else if (daytype.equals("Q")) {
                                    num = num.add(0.25);
                                }
                            }
                            System.out.println(" no of days" + num);
                            Row row = (Row) ADFUtils.evaluateEL("#{bindings.LeaveApplicationHeaderVO1Iterator.currentRow}");
                            row.setAttribute("LeaveDays", num);
                            
                            oracle.jbo.domain.Date fromdate = (oracle.jbo.domain.Date) fromDateBinding.getValue();
                            oracle.jbo.domain.Date todate = (oracle.jbo.domain.Date) object;
                            if (dayTypeBinding.getValue().toString().equals("Q")) {
                                if (!(fromdate.compareTo(todate) == 0)) {
                                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                                  "Please select correct To Date.",
                                                                                  null));
                                }
                            } else if (dayTypeBinding.getValue().toString().equals("H")) {
                                if (!(fromdate.compareTo(todate) == 0)) {
                                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                                  "Please select correct To Date.",
                                                                                  null));
                                }
                            }
                            //create mode function call  
                              String mode =(String)ADFUtils.resolveExpression("#{pageFlowScope.mode}");
                              System.out.println("MODE"+mode);
                              if (mode !=null && mode.equals("C")) {
                              OperationBinding Op = ADFUtils.findOperation("leaveEndDateCheck1");
                              Op.getParamsMap().put("todate", object);
                              Object check1 = Op.execute();
                              System.out.println("Result is===> " + Op.getResult());
                              if (Op.getResult()!=null && !(((oracle.jbo.domain.Number) Op.getResult()).compareTo(new oracle.jbo.domain.Number(0)) ==
                                    0)) {
                                  
                                  throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                  "Leave application already applied for this period.",
                                                                                                                  null));
                              }   
                              }

                            System.out.println("++++2++++");
                            OperationBinding Op1 = ADFUtils.findOperation("leaveEndDateCheck2");
                            Object check2 = Op1.execute();
                            System.out.println("Result is===> " + Op1.getResult());
                            if (!(Op1.getResult().equals("N"))) {
                                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                              Op1.getResult().toString(), null));
                            }

                            System.out.println("++++3++++");
                            OperationBinding Op2 = ADFUtils.findOperation("checkLeaveRunningBalance");
                            Object check3 = Op2.execute();
                            System.out.println("Result is===> " + Op2.getResult());
                            if ((Op2.getResult().equals("N"))) {
                                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                              "Leave Days exceeding the Balance Leave(Monthly)",
                                                                              null));
                            }
                            
                            System.out.println("++++4++++");
                            String mode1=(String)ADFUtils.resolveExpression("#{pageFlowScope.mode}");
                            if(mode1!=null && mode.equals("C")){
                            OperationBinding Op3 = ADFUtils.findOperation("leaveEndDateCheck3");
                            Object check4 = Op3.execute();
                            System.out.println("Result is===> " + Op3.getResult());
                            if ((Op3.getResult().equals("N"))) {
                                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                              Op3.getResult().toString(),
                                                                              null));
                            }}
                            

                        } else {
                            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                          "Please select From Date.", null));
                        }


                    } else {
                        throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                      "Please select First and Second Half.", null));
                    }


                } else {
                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                  "Please select Day type.", null));
                }
            } else {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Please select Leave type.",
                                                              null));
            }
        }

    }

    public void fromDateValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (object != null) {
            if (unitCodeBinding.getValue() != null) {
                if (empCodeBinding.getValue() != null) {

                } else {
                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                  "Please select Applicant Name.", null));
                }
            } else {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Please select Unit.",
                                                              null));
            }
        }

    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setLeaveTypeBinding(RichInputComboboxListOfValues leaveTypeBinding) {
        this.leaveTypeBinding = leaveTypeBinding;
    }

    public RichInputComboboxListOfValues getLeaveTypeBinding() {
        return leaveTypeBinding;
    }

    public void setNoofDaysBinding(RichInputText noofDaysBinding) {
        this.noofDaysBinding = noofDaysBinding;
    }

    public RichInputText getNoofDaysBinding() {
        return noofDaysBinding;
    }

    public void setAppNoBinding(RichInputText appNoBinding) {
        this.appNoBinding = appNoBinding;
    }

    public RichInputText getAppNoBinding() {
        return appNoBinding;
    }

    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }

    public void cevModeDisableComponent(String mode) {
        System.out.println("username:"+getUserNameTrans().getValue());
        if (mode.equals("E")) {
            getAppNoBinding().setDisabled(true);
            getAppDateBinding().setDisabled(true);
            getDeptHeadBinding().setDisabled(true);
            getLeaveGrantedBinding().setDisabled(true);
            getNoofDaysBinding().setDisabled(true);
            getVerByBinding().setDisabled(true);
            getVerRemarksBinding().setDisabled(true);
            getAprByBinding().setDisabled(true);
            getAprRemarksBinding().setDisabled(true);
            getCancelFrmBinding().setDisabled(true);
            getCancelToBinding().setDisabled(true);
            getCancelDaysBinding().setDisabled(true);
            getChkAprByBinding().setDisabled(true);
            getChkVerByBinding().setDisabled(true);
            getLeaveTypeDetailBinding().setDisabled(true);
            getOpBalDetailBinding().setDisabled(true);
            getCurrLeaveDetailBinding().setDisabled(true);
            getAvailLeaveDetailBinding().setDisabled(true);
            getBalDetailBinding().setDisabled(true);
            getAvildDetailBinding().setDisabled(true);
            getMbalDetailBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
            getEmpCodeBinding().setDisabled(true);
            //getReasonBinding().setDisabled(true);
            getChkBinding().setDisabled(true);
            getAppltCdBinding().setDisabled(true);
            getLeaveTypeBinding().setDisabled(false);
            getDayTypeBinding().setDisabled(false);
            getFirstSecondHalfBinding().setDisabled(false);
            getFromDateBinding().setDisabled(false);
            getToDateBinding().setDisabled(false);
            getReasonBinding().setDisabled(false);
            if(getUserNameTrans().getValue() != null && getUserNameTrans().getValue().toString().equalsIgnoreCase("Admin")){
                getEmpCodeBinding().setRendered(true);
                getAppltCdBinding().setRendered(false);
            }else{
                getEmpCodeBinding().setRendered(false);
                getAppltCdBinding().setRendered(true);
            }
            
        }
        if (mode.equals("C")) {
            getAppNoBinding().setDisabled(true);
            getAppDateBinding().setDisabled(true);
            getDeptHeadBinding().setDisabled(true);
            getLeaveGrantedBinding().setDisabled(true);
            getNoofDaysBinding().setDisabled(true);
            getVerByBinding().setDisabled(true);
            getVerRemarksBinding().setDisabled(true);
            getAprByBinding().setDisabled(true);
            getAprRemarksBinding().setDisabled(true);
            getCancelFrmBinding().setDisabled(true);
            getCancelToBinding().setDisabled(true);
            getCancelDaysBinding().setDisabled(true);
            getChkAprByBinding().setDisabled(true);
            getChkVerByBinding().setDisabled(true);
            getLeaveTypeDetailBinding().setDisabled(true);
            getOpBalDetailBinding().setDisabled(true);
            getCurrLeaveDetailBinding().setDisabled(true);
            getAvailLeaveDetailBinding().setDisabled(true);
            getBalDetailBinding().setDisabled(true);
            getAvildDetailBinding().setDisabled(true);
            getMbalDetailBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
            if(getUserNameTrans().getValue() != null && getUserNameTrans().getValue().toString().equalsIgnoreCase("Admin")){
                getEmpCodeBinding().setRendered(true);
                getAppltCdBinding().setRendered(false);
            }else{
                getEmpCodeBinding().setRendered(false);
                getAppltCdBinding().setRendered(true);
            }
        } 
        else if (mode.equals("V")) {
            if(getUserNameTrans().getValue() != null && getUserNameTrans().getValue().toString().equalsIgnoreCase("Admin")){
                getEmpCodeBinding().setRendered(true);
                getAppltCdBinding().setRendered(false);
            }else{
                getEmpCodeBinding().setRendered(false);
                getAppltCdBinding().setRendered(true);
            } 
        }

    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setEditTransBinding(RichOutputText editTransBinding) {
        this.editTransBinding = editTransBinding;
    }

    public RichOutputText getEditTransBinding() {
       cevmodecheck();
        return editTransBinding;
    }

    public void setEditButtonAL(RichButton editButtonAL) {
        this.editButtonAL = editButtonAL;
    }

    public RichButton getEditButtonAL() {
        return editButtonAL;
    }

    public void EditButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setAppDateBinding(RichInputDate appDateBinding) {
        this.appDateBinding = appDateBinding;
    }

    public RichInputDate getAppDateBinding() {
        return appDateBinding;
    }

    public void setLeaveGrantedBinding(RichSelectOneChoice leaveGrantedBinding) {
        this.leaveGrantedBinding = leaveGrantedBinding;
    }

    public RichSelectOneChoice getLeaveGrantedBinding() {
        return leaveGrantedBinding;
    }

    public void setVerByBinding(RichInputComboboxListOfValues verByBinding) {
        this.verByBinding = verByBinding;
    }

    public RichInputComboboxListOfValues getVerByBinding() {
        return verByBinding;
    }

    public void setVerRemarksBinding(RichInputText verRemarksBinding) {
        this.verRemarksBinding = verRemarksBinding;
    }

    public RichInputText getVerRemarksBinding() {
        return verRemarksBinding;
    }

    public void setAprByBinding(RichInputComboboxListOfValues aprByBinding) {
        this.aprByBinding = aprByBinding;
    }

    public RichInputComboboxListOfValues getAprByBinding() {
        return aprByBinding;
    }

    public void setAprRemarksBinding(RichInputText aprRemarksBinding) {
        this.aprRemarksBinding = aprRemarksBinding;
    }

    public RichInputText getAprRemarksBinding() {
        return aprRemarksBinding;
    }

    public void setCancelFrmBinding(RichInputDate cancelFrmBinding) {
        this.cancelFrmBinding = cancelFrmBinding;
    }

    public RichInputDate getCancelFrmBinding() {
        return cancelFrmBinding;
    }

    public void setCancelToBinding(RichInputDate cancelToBinding) {
        this.cancelToBinding = cancelToBinding;
    }

    public RichInputDate getCancelToBinding() {
        return cancelToBinding;
    }

    public void setCancelDaysBinding(RichInputText cancelDaysBinding) {
        this.cancelDaysBinding = cancelDaysBinding;
    }

    public RichInputText getCancelDaysBinding() {
        return cancelDaysBinding;
    }

    public void setChkVerByBinding(RichSelectBooleanCheckbox chkVerByBinding) {
        this.chkVerByBinding = chkVerByBinding;
    }

    public RichSelectBooleanCheckbox getChkVerByBinding() {
        return chkVerByBinding;
    }

    public void setChkAprByBinding(RichSelectBooleanCheckbox chkAprByBinding) {
        this.chkAprByBinding = chkAprByBinding;
    }

    public RichSelectBooleanCheckbox getChkAprByBinding() {
        return chkAprByBinding;
    }

    public void setLeaveTypeDetailBinding(RichInputText leaveTypeDetailBinding) {
        this.leaveTypeDetailBinding = leaveTypeDetailBinding;
    }

    public RichInputText getLeaveTypeDetailBinding() {
        return leaveTypeDetailBinding;
    }

    public void setOpBalDetailBinding(RichInputText opBalDetailBinding) {
        this.opBalDetailBinding = opBalDetailBinding;
    }

    public RichInputText getOpBalDetailBinding() {
        return opBalDetailBinding;
    }

    public void setCurrLeaveDetailBinding(RichInputText currLeaveDetailBinding) {
        this.currLeaveDetailBinding = currLeaveDetailBinding;
    }

    public RichInputText getCurrLeaveDetailBinding() {
        return currLeaveDetailBinding;
    }

    public void setBalDetailBinding(RichInputText balDetailBinding) {
        this.balDetailBinding = balDetailBinding;
    }

    public RichInputText getBalDetailBinding() {
        return balDetailBinding;
    }

    public void setAvailLeaveDetailBinding(RichInputText availLeaveDetailBinding) {
        this.availLeaveDetailBinding = availLeaveDetailBinding;
    }

    public RichInputText getAvailLeaveDetailBinding() {
        return availLeaveDetailBinding;
    }

    public void setAvildDetailBinding(RichInputText avildDetailBinding) {
        this.avildDetailBinding = avildDetailBinding;
    }

    public RichInputText getAvildDetailBinding() {
        return avildDetailBinding;
    }

    public void setMbalDetailBinding(RichInputText mbalDetailBinding) {
        this.mbalDetailBinding = mbalDetailBinding;
    }

    public RichInputText getMbalDetailBinding() {
        return mbalDetailBinding;
    }

    public void editButtonAL(ActionEvent actionEvent) {
        if(getLeaveGrantedBinding().getValue().equals("A") || getLeaveGrantedBinding().getValue().equals("C")){
            ADFUtils.showMessage("Approved or Cancelled Leave Application cannot be edited", 2);
            }else{
        cevmodecheck();
            }
        }

    public void setReasonBinding(RichInputText reasonBinding) {
        this.reasonBinding = reasonBinding;
    }

    public RichInputText getReasonBinding() {
        return reasonBinding;
    }

    public void setChkBinding(RichInputDate chkBinding) {
        this.chkBinding = chkBinding;
    }

    public RichInputDate getChkBinding() {
        return chkBinding;
    }

    public void setAppltCdBinding(RichInputComboboxListOfValues appltCdBinding) {
        this.appltCdBinding = appltCdBinding;
    }

    public RichInputComboboxListOfValues getAppltCdBinding() {
        return appltCdBinding;
    }

    public void setUserNameTrans(RichInputText userNameTrans) {
        this.userNameTrans = userNameTrans;
    }

    public RichInputText getUserNameTrans() {
        return userNameTrans;
    }
}
