package terms.hrm.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import javax.faces.validator.ValidatorException;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;
import oracle.jbo.domain.Timestamp;

public class CreateMissPunchRequisitionBean {
    
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputComboboxListOfValues employeeCodeBinding;
    private RichInputText cardNumberBinding;
    private RichInputDate dateBinding;
    private RichInputText shiftBinding;
    private RichInputDate timeInOutBinding;
    private RichInputDate approvedDateBinding;
    private RichOutputText bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichInputComboboxListOfValues approvedByBinding;
    private String message="C";
    private RichButton headerEditBinding;
    private RichInputComboboxListOfValues createdByBinding;
    private RichInputDate creationDateBinding;
    private RichInputText deptname;
    private RichInputText departmentCodeBinding;
    private RichInputText empNameBinding;
    private RichPopup popupBinding;


    public CreateMissPunchRequisitionBean() {
    }

    public void saveButtonAL(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedBy("MissPunchRequisitionVO1Iterator","LastUpdatedBy");
         //   if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
         
        //       if(leaveCodeHeaderBinding.getValue() !=null){
        if (message.equalsIgnoreCase("C")){
                OperationBinding opr = ADFUtils.findOperation("Commit");
                Object obj = opr.execute();
                
                System.out.println("outside error block");

                System.out.println("S####");
//                FacesMessage Message = new FacesMessage("Record Saved Successfully");
//                Message.setSeverity(FacesMessage.SEVERITY_INFO);
//                FacesContext fc = FacesContext.getCurrentInstance();
//                fc.addMessage(null, Message);
                ADFUtils.showPopup(popupBinding);
                
//                ADFUtils.setEL("#{pageFlowScope.mode}", "V");
//                cevmodecheck();
//                AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
                
                        String EmpCd=(String) getEmployeeCodeBinding().getValue();
                        String CardNo=(String) getCardNumberBinding().getValue();
                        String DeptCd=(String) getDepartmentCodeBinding().getValue();
                        String DeptNm=(String) getDeptname().getValue();
                        String shift=(String) shiftBinding.getValue();
                        Timestamp missDate=(Timestamp)dateBinding.getValue();
                System.out.println("EmpCd-->>"+"  "+EmpCd + "  " + "CardNo-->>"+"  "+CardNo + "  " + "DeptCd-->>"+"  "+DeptCd + "  " + "DeptNm-->>"+"  "+DeptNm
                                   + "  " + "shift-->>"+"  "+shift);
                ADFUtils.findOperation("CreateInsert").execute();
                DCIteratorBinding dci=ADFUtils.findIterator("MissPunchRequisitionVO1Iterator");
                dci.getCurrentRow().setAttribute("EmpCd", EmpCd);
                dci.getCurrentRow().setAttribute("MissDate", missDate);
                dci.getCurrentRow().setAttribute("Shift", shift);
               // dci.getCurrentRow().setAttribute("arg0", arg1);
                //dci.getCurrentRow().setAttribute("arg0", arg1);
//                getEmployeeCodeBinding().setValue(EmpCd);
//                                getEmpNameBinding().setValue(EmpNm);
//                                getCardNumberBinding().setValue(CardNo);
//                                getDepartmentCodeBinding().setValue(DeptCd);
//                                getDeptname().setValue(DeptNm);   

            } else if (message.equalsIgnoreCase("E")) {
                OperationBinding opr = ADFUtils.findOperation("Commit");
                Object obj = opr.execute();
                
                System.out.println("outside error block");

                FacesMessage Message = new FacesMessage("Record Updated Successfully");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
                ADFUtils.setEL("#{pageFlowScope.mode}", "V");
                cevmodecheck();
                AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
            }
        
//        
//        DCIteratorBinding dci1 = ADFUtils.findIterator("MissPunchRequisitionVO1Iterator");
//                RowSetIterator rsi1=dci1.getViewObject().createRowSetIterator(null);
//                rsi1.getEstimatedRangePageCount();
//                
//        rsi1.createRow();
////        rsi1.getCurrentRow().getAttribute("UnitCd");
////        System.out.println("rsi1.getCurrentRow().getAttribute(\"UnitCd\")-->"+rsi1.getCurrentRow().getAttribute("UnitCd"));
//        String unitcd=(String) getUnitCodeBinding().getValue();
//        String EmpCd=(String) getEmployeeCodeBinding().getValue();
//        String CardNo=(String) getCardNumberBinding().getValue();
//        String DeptCd=(String) getDepartmentCodeBinding().getValue();
//        String DeptNm=(String) getDeptname().getValue();
//        String EmpNm=(String) getEmpNameBinding().getValue();
//        String CreateBy=(String) getCreatedByBinding().getValue();
        
//        System.out.println("unitcd-->"+unitcd + "  " + "EmpCd-->>"+"  "+EmpCd + "  " + "CardNo-->>"+"  "+CardNo + "  " + "DeptCd-->>"+"  "+DeptCd);
//        String unitc=(String) getUnitCodeBinding().getValue();
                    
      //  rsi1.getCurrentRow().setAttribute("EmpCd", null);
//        if(unitcd!=null)
//        {
//            System.out.println("In if bean");
//               // rsi1.createRow();
//                ADFUtils.findOperation("CreateInsert").execute();
////                getEmployeeCodeBinding().setValue(EmpCd);
////                getEmpNameBinding().setValue(EmpNm);
////                getCardNumberBinding().setValue(CardNo);
////                getDepartmentCodeBinding().setValue(DeptCd);
////                getDeptname().setValue(DeptNm);              
//                System.out.println("In if bean 2");        
//            }
        
        
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setEmployeeCodeBinding(RichInputComboboxListOfValues employeeCodeBinding) {
        this.employeeCodeBinding = employeeCodeBinding;
    }

    public RichInputComboboxListOfValues getEmployeeCodeBinding() {
        return employeeCodeBinding;
    }

    public void setCardNumberBinding(RichInputText cardNumberBinding) {
        this.cardNumberBinding = cardNumberBinding;
    }

    public RichInputText getCardNumberBinding() {
        return cardNumberBinding;
    }

    public void setDateBinding(RichInputDate dateBinding) {
        this.dateBinding = dateBinding;
    }

    public RichInputDate getDateBinding() {
        return dateBinding;
    }

    public void setShiftBinding(RichInputText shiftBinding) {
        this.shiftBinding = shiftBinding;
    }

    public RichInputText getShiftBinding() {
        return shiftBinding;
    }

    public void setTimeInOutBinding(RichInputDate timeInOutBinding) {
        this.timeInOutBinding = timeInOutBinding;
    }

    public RichInputDate getTimeInOutBinding() {
        return timeInOutBinding;
    }

    public void setApprovedDateBinding(RichInputDate approvedDateBinding) {
        this.approvedDateBinding = approvedDateBinding;
    }

    public RichInputDate getApprovedDateBinding() {
        return approvedDateBinding;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }
    
    private void cevmodecheck(){
            if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
                cevModeDisableComponent("V");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("C");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("E");
            }
        }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                try {
                    Method method1 =
                        component.getClass().getMethod("setDisabled", boolean.class);
                    if (method1 != null) {
                        method1.invoke(component, valueComponent);
                    }
                } catch (NoSuchMethodException e) {
                    try {
                        Method method =
                            component.getClass().getMethod("setReadOnly", boolean.class);
                        if (method != null) {
                            method.invoke(component, valueComponent);
                        }
                    } catch (Exception e1) {
                        // e.printStackTrace();//silently eat this exception.
                    }


                } catch (Exception e) {
                    // e.printStackTrace();//silently eat this exception.
                }
                List<UIComponent> childComponents = component.getChildren();
                for (UIComponent comp : childComponents) {
                    makeComponentHierarchyReadOnly(comp, valueComponent);
                }
            }
    
    public void cevModeDisableComponent(String mode) {
    if (mode.equals("E")) {
                    getUnitCodeBinding().setDisabled(true);
                    getEmployeeCodeBinding().setDisabled(true);
                    getCardNumberBinding().setDisabled(true);
                    getDateBinding().setDisabled(false);
                    getShiftBinding().setDisabled(true);
                    getTimeInOutBinding().setDisabled(false);
                    getCreatedByBinding().setDisabled(true);
                    getCreationDateBinding().setDisabled(true);
                    getApprovedByBinding().setDisabled(false);
                    getApprovedDateBinding().setDisabled(true);
                    getHeaderEditBinding().setDisabled(true);
                getDeptname().setDisabled(true);
                    message="E";
            } else if (mode.equals("C")) {
                getUnitCodeBinding().setDisabled(true);
                getEmployeeCodeBinding().setDisabled(false);
                getCardNumberBinding().setDisabled(true);
                getDateBinding().setDisabled(false);
                getShiftBinding().setDisabled(true);
                getTimeInOutBinding().setDisabled(false);
                getCreatedByBinding().setDisabled(true);
           getCreationDateBinding().setDisabled(true);
                getApprovedByBinding().setDisabled(true);
                getApprovedDateBinding().setDisabled(true);
                getDeptname().setDisabled(true);
                
                
            } else if (mode.equals("V")) {
                
            }
            
        }

    public void setApprovedByBinding(RichInputComboboxListOfValues approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputComboboxListOfValues getApprovedByBinding() {
        return approvedByBinding;
    }

    public void editButtonAL(ActionEvent actionEvent) {
        
        if(getApprovedByBinding().getValue()!=null){
               ADFUtils.showMessage("Application For Miss Punch can't be edit after approved.", 2);
               ADFUtils.setEL("#{pageFlowScope.mode}", "V");
              }
              else{
                  cevmodecheck();
              }
        
       // cevmodecheck();
        // Add event code here...
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setCreatedByBinding(RichInputComboboxListOfValues createdByBinding) {
        this.createdByBinding = createdByBinding;
    }

    public RichInputComboboxListOfValues getCreatedByBinding() {
        return createdByBinding;
    }

    public void setCreationDateBinding(RichInputDate creationDateBinding) {
        this.creationDateBinding = creationDateBinding;
    }

    public RichInputDate getCreationDateBinding() {
        return creationDateBinding;
    }

    public void approvedByVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
                 if(vce!=null)
                 {
                  vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
                  OperationBinding op = (OperationBinding) ADFUtils.findOperation("checkAuthorityForFormAuthority");
                  op.getParamsMap().put("unitCd", unitCodeBinding.getValue().toString());
                  op.getParamsMap().put("empCode", approvedByBinding.getValue());
                  op.getParamsMap().put("authLevel", "AP");
                  op.getParamsMap().put("formName", "MISS_PUNCH");
                     op.execute();
                 }
    }

//    public void missDateVCL(ValueChangeEvent vce) {
//        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
//               if(vce!=null){
//                       OperationBinding op = ADFUtils.findOperation("missDateForShift");
//                       op.getParamsMap().put("unitCd", unitCodeBinding.getValue());
//                       op.getParamsMap().put("empCode", employeeCodeBinding.getValue());
//                       op.getParamsMap().put("misDt", dateBinding.getValue());
//                       op.execute();
//                        System.out.println("Result is==>"+op.getResult());
//                       if(op.getResult()!=null)
//                       {
//                            shiftBinding.setValue(op.getResult().toString());
//                            AdfFacesContext.getCurrentInstance().addPartialTarget(shiftBinding);
//
//                        }
//                       else
//                       {
//                        ADFUtils.showMessage("Shift Code Not found for this Date.", 2);    
//                        }
//                       
//                   
//                   }
//    }

    public void setDeptname(RichInputText deptname) {
        this.deptname = deptname;
    }

    public RichInputText getDeptname() {
        return deptname;
    }

    public void missPunchDateVCL(ValueChangeEvent valueChangeEvent) {
        oracle.jbo.domain.Timestamp d=  (oracle.jbo.domain.Timestamp)valueChangeEvent.getNewValue();
                     System.out.println(d);
            
                     OperationBinding op = ADFUtils.findOperation("getShiftCodeForMissPunch");
                     op.getParamsMap().put("date", d);
                   Object rst = op.execute();
    }

    public void setDepartmentCodeBinding(RichInputText departmentCodeBinding) {
        this.departmentCodeBinding = departmentCodeBinding;
    }

    public RichInputText getDepartmentCodeBinding() {
        return departmentCodeBinding;
    }

    public void setEmpNameBinding(RichInputText empNameBinding) {
        this.empNameBinding = empNameBinding;
    }

    public RichInputText getEmpNameBinding() {
        return empNameBinding;
    }

    public void saveAndCloseButtonAL(ActionEvent actionEvent) {
        //       if(leaveCodeHeaderBinding.getValue() !=null){
        if (message.equalsIgnoreCase("C")){
                OperationBinding opr = ADFUtils.findOperation("Commit");
                Object obj = opr.execute();
                
                System.out.println("outside error block");

                System.out.println("S####");
                FacesMessage Message = new FacesMessage("Record Saved Successfully");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
                ADFUtils.setEL("#{pageFlowScope.mode}", "V");
                cevmodecheck();
                AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
                

            } else if (message.equalsIgnoreCase("E")) {
                OperationBinding opr = ADFUtils.findOperation("Commit");
                Object obj = opr.execute();
                
                System.out.println("outside error block");

                FacesMessage Message = new FacesMessage("Record Updated Successfully");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
                ADFUtils.setEL("#{pageFlowScope.mode}", "V");
                cevmodecheck();
                AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
            }
    }

    public void inOutValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
//        Row r = (Row) ADFUtils.evaluateEL("#{bindings.MissPunchRequisitionVO1Iterator.currentRow}");
//        oracle.jbo.domain.Timestamp dr = (oracle.jbo.domain.Timestamp) r.getAttribute("InOutTime");
//        oracle.jbo.domain.Timestamp de = (oracle.jbo.domain.Timestamp) r.getAttribute("MissDate");
//        System.out.println("InOutTime "+dr+ "  "+"MissDate "+de);
//        if (dr != null && de != null) {
//            long ndays = ((dr.getTime() - de.getTime()) / 86400000) + 1;
//        
//            int k = (int) ndays;
//            System.out.println("Number of Days==>" + ndays);
//            if (k != 1) {
//                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Select Same as Miss Date.",
//                                                              null));
//            }
//        }

    }

    public void inOutVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        Row r = (Row) ADFUtils.evaluateEL("#{bindings.MissPunchRequisitionVO1Iterator.currentRow}");
        oracle.jbo.domain.Timestamp dr = (oracle.jbo.domain.Timestamp) r.getAttribute("InOutTime");
        oracle.jbo.domain.Timestamp de = (oracle.jbo.domain.Timestamp) r.getAttribute("MissDate");
        
        System.out.println("InOutTime "+dr+ "  "+"MissDate "+de);
        if (dr != null && de != null) {
            long ndays = ((dr.getTime() - de.getTime()) / 86400000) + 1;
        
            int k = (int) ndays;
            System.out.println("Number of Days==>" + ndays);
            if (k != 1) {
                FacesMessage Message = new FacesMessage("Select Date same as Miss Date.");
                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
                getTimeInOutBinding().setValue(null);
              
            }
        }
    }

    public void setPopupBinding(RichPopup popupBinding) {
        this.popupBinding = popupBinding;
    }

    public RichPopup getPopupBinding() {
        return popupBinding;
    }
}
