package terms.hrm.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;

import oracle.adf.view.rich.component.rich.input.RichInputText;

import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;

import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class CreateMonthlyPfProcessBean {
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichSelectOneChoice monthBinding;
    private RichInputText yearBinding;
    private RichInputText ac10Binding;
    private RichInputText ac21Binding;
    private RichInputText ac22Binding;
    private RichOutputText bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichButton processBtnBinding;
    private RichInputComboboxListOfValues freezeBinding;
    private RichPopup refreshpopup;

    public CreateMonthlyPfProcessBean() {
    }

    public void processBtnAL(ActionEvent actionEvent) {
        System.out.println("----------------------Inside processBtnAL---------------");

        OperationBinding ob1 = ADFUtils.findOperation("checkfreezemonthlyPfProcess");
        ob1.getParamsMap().put("mde", ADFUtils.resolveExpression("#{pageFlowScope.mode}"));
        ob1.execute();
        System.out.println("ob.getResult() in bean:" + ob1.getResult());

        if (ob1.getResult() != null) {
            String result1 = ob1.getResult().toString().substring(0, 1);
            String msgg1 = ob1.getResult().toString().substring(ob1.getResult().toString().indexOf("-") + 1);
            System.out.println("result after substring:" + result1);
            System.out.println("msg:" + msgg1);
            if (result1.equals("Y")) {
                //                        FacesMessage msg=new FacesMessage(msgg1);
                //                        msg.setSeverity(FacesMessage.SEVERITY_INFO);
                //                        FacesContext fc=FacesContext.getCurrentInstance();
                //                        fc.addMessage(null, msg);


                OperationBinding ob = ADFUtils.findOperation("monthlyPfProcess");
                ob.execute();
                System.out.println("ob.getResult() in bean:" + ob.getResult());

                if (ob.getResult() != null) {
                    String result = ob.getResult().toString().substring(0, 1);
                    String msgg = ob.getResult().toString().substring(ob.getResult().toString().indexOf("-") + 1);
                    System.out.println("result after substring:" + result);
                    System.out.println("msg:" + msgg);
                    if (result.equals("Y")) {

                    } else {
                        FacesMessage msg = new FacesMessage(msgg);
                        msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                        FacesContext fc = FacesContext.getCurrentInstance();
                        fc.addMessage(null, msg);
                    }
                }

            } else if (result1.equals("R")) {

                RichPopup.PopupHints pop = new RichPopup.PopupHints();
                refreshpopup.show(pop);
            } else {
                FacesMessage msg = new FacesMessage(msgg1);
                msg.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, msg);
            }
        }
    }

    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }


    public void cevModeDisableComponent(String mode) {
        if (mode.equals("E")) {
            getUnitCodeBinding().setDisabled(true);
            getMonthBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            getYearBinding().setDisabled(true);
            getAc10Binding().setDisabled(true);
            getAc21Binding().setDisabled(true);
            getAc22Binding().setDisabled(true);
            getFreezeBinding().setDisabled(false);
            getProcessBtnBinding().setDisabled(false);
        } else if (mode.equals("C")) {
            getUnitCodeBinding().setDisabled(true);
            getMonthBinding().setDisabled(false);
            getYearBinding().setDisabled(false);
            getAc10Binding().setDisabled(true);
            getAc21Binding().setDisabled(true);
            getAc22Binding().setDisabled(true);
            getFreezeBinding().setDisabled(true);
            getProcessBtnBinding().setDisabled(false);
        } else if (mode.equals("V")) {
            getProcessBtnBinding().setDisabled(true);
        }

    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setMonthBinding(RichSelectOneChoice monthBinding) {
        this.monthBinding = monthBinding;
    }

    public RichSelectOneChoice getMonthBinding() {
        return monthBinding;
    }

    public void setYearBinding(RichInputText yearBinding) {
        this.yearBinding = yearBinding;
    }

    public RichInputText getYearBinding() {
        return yearBinding;
    }

    public void setAc10Binding(RichInputText ac10Binding) {
        this.ac10Binding = ac10Binding;
    }

    public RichInputText getAc10Binding() {
        return ac10Binding;
    }

    public void setAc21Binding(RichInputText ac21Binding) {
        this.ac21Binding = ac21Binding;
    }

    public RichInputText getAc21Binding() {
        return ac21Binding;
    }

    public void setAc22Binding(RichInputText ac22Binding) {
        this.ac22Binding = ac22Binding;
    }

    public RichInputText getAc22Binding() {
        return ac22Binding;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void editButtonAL(ActionEvent actionEvent) {
        if (freezeBinding.getValue() != null) {
            FacesMessage msg = new FacesMessage("Data is already freezed so cannot modify.");
            msg.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, msg);
        } else {
            cevmodecheck();
        }

    }

    public void setProcessBtnBinding(RichButton processBtnBinding) {
        this.processBtnBinding = processBtnBinding;
    }

    public RichButton getProcessBtnBinding() {
        return processBtnBinding;
    }

    public void setFreezeBinding(RichInputComboboxListOfValues freezeBinding) {
        this.freezeBinding = freezeBinding;
    }

    public RichInputComboboxListOfValues getFreezeBinding() {
        return freezeBinding;
    }

    public void saveAL(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedBy("MonthlyPfProcessHeaderVO1Iterator","LastUpdatedBy");
        if (freezeBinding.isDisabled()) {
            ADFUtils.findOperation("Commit").execute();
            FacesMessage Message = new FacesMessage("Record Saved Successfully.");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
            ADFUtils.setEL("#{pageFlowScope.mode}", "V");
            cevmodecheck();
            AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
        } else {
            ADFUtils.findOperation("Commit").execute();
            FacesMessage Message = new FacesMessage("Record Update Successfully.");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
            ADFUtils.setEL("#{pageFlowScope.mode}", "V");
            cevmodecheck();
            AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
        }
    }

    public void refreshDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            OperationBinding ob = ADFUtils.findOperation("cleanmonthlypfData");
            ob.execute();
            System.out.println("ob.getResult() in bean:" + ob.getResult());

            if (ob.getResult() != null) {
                String result = ob.getResult().toString().substring(0, 1);
                String msgg = ob.getResult().toString().substring(ob.getResult().toString().indexOf("-") + 1);
                System.out.println("result after substring:" + result);
                System.out.println("msg:" + msgg);
                if (result.equals("Y")) {

                } else {
                    FacesMessage msg = new FacesMessage(msgg);
                    msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, msg);
                }
            }
            //            OperationBinding ob = ADFUtils.findOperation("monthlyPfProcess");
            //            ob.execute();
            //            System.out.println("ob.getResult() in bean:"+ob.getResult());
            //
            //            if(ob.getResult()!= null){
            //                String result=ob.getResult().toString().substring(0,1);
            //                String msgg=ob.getResult().toString().substring(ob.getResult().toString().indexOf("-")+1);
            //                System.out.println("result after substring:"+result);
            //                System.out.println("msg:"+msgg);
            //                if(result.equals("Y")){
            //
            //                }else{
            //                    FacesMessage msg=new FacesMessage(msgg);
            //                    msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            //                    FacesContext fc=FacesContext.getCurrentInstance();
            //                    fc.addMessage(null, msg);
            //                }
            //            }
        }

    }

    public void setRefreshpopup(RichPopup refreshpopup) {
        this.refreshpopup = refreshpopup;
    }

    public RichPopup getRefreshpopup() {
        return refreshpopup;
    }
}
