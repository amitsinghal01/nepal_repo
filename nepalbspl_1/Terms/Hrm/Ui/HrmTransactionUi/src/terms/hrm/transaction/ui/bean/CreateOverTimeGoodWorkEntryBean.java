package terms.hrm.transaction.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.math.RoundingMode;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;

import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;

import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichPanelLabelAndMessage;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;
import oracle.jbo.server.ViewObjectImpl;

public class CreateOverTimeGoodWorkEntryBean {
    private RichInputComboboxListOfValues unitCodeDetailBinding;
    private RichInputComboboxListOfValues employeeNumberBinding;
    private RichInputText actualHoursBinding;
    private RichInputText inPlaceHourBinding;
    private RichInputText totalHoursBinding;
    private RichInputText amountBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichInputText totalHourdBinding;
    private RichInputText monthBinding;
    private RichSelectOneChoice dualMonthBinding;
    private RichInputText dualYearBinding;
    private RichInputText yearBinding;
    private RichButton headerEditBinding;
    private String message = "C";
    private RichPanelLabelAndMessage bindingOutputText;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputComboboxListOfValues unitCodeBinding1;
    private RichSelectOneChoice flag1Binding;
    private RichInputText month1Binding;
    private RichInputText yearheaderBinding;
    private RichInputComboboxListOfValues approvedByBinding;
    private RichInputDate approvedDateBinding;
    private RichSelectOneChoice flagBinding;
    private RichInputText getgdOtPlagBinding;
    private RichSelectOneChoice flagHeaderBinding;
    private RichInputText otStatusBinding;
    private RichInputComboboxListOfValues apprvByBinding;
    private RichButton createButtonBinding;
    private RichButton deleteButtonBinding;


    public CreateOverTimeGoodWorkEntryBean() {

    }

    public void saveButtonAL(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedBy("OverTimeGoodWorkEntryVO1Iterator","LastUpdatedBy");

        String appBy=(String)approvedByBinding.getValue();
        DCIteratorBinding dci = ADFUtils.findIterator("OverTimeGoodWorkEntryVO1Iterator");
        RowSetIterator rsi=dci.getViewObject().createRowSetIterator(null); 
        while(rsi.hasNext()) {
        System.out.println("Inside save Button AL");
        Row r = rsi.next();
        System.out.println("Approve by: "+appBy);
         r.setAttribute("ApprovedBy", appBy);
        }
        rsi.closeRowSetIterator();
        System.out.println("Approve By set!");
            
            

        if (message.equalsIgnoreCase("C")) {
            System.out.println("S####");
            FacesMessage Message = new FacesMessage("Record Saved Successfully.");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        } else if (message.equalsIgnoreCase("E")) {
            FacesMessage Message = new FacesMessage("Record Updated Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
            
            ADFUtils.setEL("#{pageFlowScope.mode}", "V");
            cevmodecheck();
            AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);

        }
    }

    public void EditButtonAL(ActionEvent actionEvent) {
        if(getApprovedByBinding().getValue()!=null){
            ADFUtils.showMessage("Approved Over Time Good Work Entry can not be edited.", 2);
            }
        else{
        cevmodecheck();
        }
    }

    public void CreateButtonAL(ActionEvent actionEvent) {

        System.out.println("In CreateButtonAL");
        getUnitCodeBinding().setDisabled(true);
        getDualMonthBinding().setDisabled(true);
        getFlag1Binding().setDisabled(true);
        getDualYearBinding().setDisabled(true);

       //ADFUtils.findOperation("CreateInsert").execute();
        
//        String uc=(String)unitCodeBinding.getValue();
//        String m=(String)dualMonthBinding.getValue();
//        BigDecimal y=(BigDecimal)dualYearBinding.getValue();
//        String f=(String)flag1Binding.getValue();
//        System.out.println("Values from dual: "+uc+" "+m+" "+y+" "+f);

//        DCIteratorBinding dci = ADFUtils.findIterator("OverTimeGoodWorkEntryVO1Iterator");
//        RowSetIterator rsi = dci.getViewObject().createRowSetIterator(null);
//        String Unit = (String) getUnitCodeDetailBinding().getValue();
//        System.out.println("Unit Value" + Unit);
//        if (Unit == null) {
//            System.out.println("Unit Value 1--> " + Unit);
//            while (rsi.hasNext()) {
//                System.out.println("Unit Value 2--> " + Unit);
//                System.out.println("In While Loop");
//                Row r = rsi.next();
//               // r.remove();
//            }
//            rsi.closeRowSetIterator();
//        }
       // ADFUtils.findOperation("CreateInsert").execute();
       OperationBinding ob=(OperationBinding) ADFUtils.findOperation("getValuesOnDetailTable");
       String res=(String)ob.execute();
       String result=(String)ob.getResult();
       System.out.println("Result returned: "+result);
       if(result.equals("Y"))
       {
           System.out.println("Back to bean:::value returned as Y");
               ADFUtils.showMessage("Approved Over Time Good Work Entry cannot be created", 2);
           }
       else
       {
           System.out.println("Value returned null!");
           }
        System.out.println("Back to create button AL");
    }

    public void setUnitCodeDetailBinding(RichInputComboboxListOfValues unitCodeDetailBinding) {
        this.unitCodeDetailBinding = unitCodeDetailBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeDetailBinding() {
        return unitCodeDetailBinding;
    }

    public void setEmployeeNumberBinding(RichInputComboboxListOfValues employeeNumberBinding) {
        this.employeeNumberBinding = employeeNumberBinding;
    }

    public RichInputComboboxListOfValues getEmployeeNumberBinding() {
        return employeeNumberBinding;
    }

    public void setActualHoursBinding(RichInputText actualHoursBinding) {
        this.actualHoursBinding = actualHoursBinding;
    }

    public RichInputText getActualHoursBinding() {
        return actualHoursBinding;
    }

    public void setInPlaceHourBinding(RichInputText inPlaceHourBinding) {
        this.inPlaceHourBinding = inPlaceHourBinding;
    }

    public RichInputText getInPlaceHourBinding() {
        return inPlaceHourBinding;
    }

    public void setTotalHoursBinding(RichInputText totalHoursBinding) {
        this.totalHoursBinding = totalHoursBinding;
    }

    public RichInputText getTotalHoursBinding() {
        return totalHoursBinding;
    }

    public void setAmountBinding(RichInputText amountBinding) {
        this.amountBinding = amountBinding;
    }

    public RichInputText getAmountBinding() {
        return amountBinding;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setTotalHourdBinding(RichInputText totalHourdBinding) {
        this.totalHourdBinding = totalHourdBinding;
    }

    public RichInputText getTotalHourdBinding() {
        return totalHourdBinding;
    }

    public void actualHoursBinding(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
    }

    public void actualhoursVCL(ValueChangeEvent vce) {
        BigDecimal ActHrs = (BigDecimal) actualHoursBinding.getValue();
        BigDecimal InpHrs = (BigDecimal) inPlaceHourBinding.getValue();

        System.out.println("ActHrs--->>> " + ActHrs);
        System.out.println("InpHrs--->>> " + InpHrs);

        BigDecimal n = new BigDecimal(0);

        System.out.println("n--->>> " + n);

        BigDecimal AtHrs = (BigDecimal) (ActHrs != null ? ActHrs : n);
        BigDecimal IpHrs = (BigDecimal) (InpHrs != null ? InpHrs : n);

        System.out.println("AtHrs--->>> " + AtHrs);
        System.out.println("IpHrs--->>> " + IpHrs);

        BigDecimal TotHrs = new BigDecimal(0);

        TotHrs = AtHrs.add(IpHrs);
        System.out.println("TotHrs--->>> " + TotHrs);
        getTotalHourdBinding().setValue(TotHrs);

        System.out.println("getTotalHourdBinding Value--->> " + getTotalHourdBinding().getValue());

        BigDecimal TotalHrs = (BigDecimal) totalHourdBinding.getValue();
        System.out.println("TotalHrs----> " + TotalHrs);
        String mnth2 = (String) monthBinding.getValue();
        System.out.println("dualMonthBinding----> " + mnth2);

        BigDecimal yr2 = (BigDecimal) yearBinding.getValue();
        System.out.println("dualYearBinding----> " + yr2);
        
        //function call for amt
                      String ot=(String)otStatusBinding.getValue();
                      OperationBinding opr1 = ADFUtils.findOperation("AmCalculatationFunction");
                      opr1.getParamsMap().put("Employee", employeeNumberBinding.getValue());
                      opr1.getParamsMap().put("UnitCd", unitCodeBinding.getValue());
                      opr1.getParamsMap().put("tohhr", totalHourdBinding.getValue());
                      opr1.getParamsMap().put("otstatus", otStatusBinding.getValue());
                      opr1.getParamsMap().put("year", yearBinding.getValue());
                      opr1.getParamsMap().put("month", monthBinding.getValue());
                      opr1.execute();
                      BigDecimal Amt = (BigDecimal) opr1.getResult();
                      System.out.println("Amt  in bean----> " + Amt);
        //                DCIteratorBinding dci = ADFUtils.findIterator("OverTimeGoodWorkEntryVO1Iterator");
        //                RowSetIterator rsi = dci.getViewObject().createRowSetIterator(null);
                      amountBinding.setValue(Amt);

//        BigDecimal paidDays = new BigDecimal(0);
//        BigDecimal n1 = new BigDecimal(4);
//        BigDecimal n2 = new BigDecimal(100);
//        BigDecimal n3 = new BigDecimal(400);
//
//
//        if (mnth2 != null || mnth2 == "") {
//            if (mnth2.equalsIgnoreCase("JAN") || mnth2.equalsIgnoreCase("MAR") || mnth2.equalsIgnoreCase("MAY") ||
//                mnth2.equalsIgnoreCase("JUL") || mnth2.equalsIgnoreCase("AUG") || mnth2.equalsIgnoreCase("OCT") ||
//                mnth2.equalsIgnoreCase("DEC")) {
//                paidDays = new BigDecimal(31);
//                System.out.println("paidDays--->  " + paidDays);
//            }
//
//            else if (mnth2.equalsIgnoreCase("FEB")) {
//                if (yr2.remainder(n1) == n || (yr2.remainder(n2) == n && yr2.remainder(n3) == n)) {
//                    paidDays = new BigDecimal(29);
//                    System.out.println("paidDays--->  " + paidDays);
//                } else {
//                    paidDays = new BigDecimal(28);
//                    System.out.println("paidDays--->  " + paidDays);
//                }
//            } else {
//                paidDays = new BigDecimal(30);
//                System.out.println("paidDays--->  " + paidDays);
//            }
//        }
//        
//        String Emp ="";
//        String Unit="";


//        DCIteratorBinding dci = ADFUtils.findIterator("OverTimeGoodWorkEntryVO1Iterator");
//               RowSetIterator rsi=dci.getViewObject().createRowSetIterator(null);
//               rsi.getEstimatedRangePageCount();
//               System.out.println("Total Rows"+  rsi.getEstimatedRangePageCount());
//        
//                   while(rsi.hasNext()) {
//                       System.out.println("In While Loop");
//                       Row r = rsi.next();
//                    Emp =(String) r.getAttribute("EmpNo");
//                       System.out.println("Employee From Detail Table--> "+Emp);
//                    Unit =(String) r.getAttribute("UnitCd");
//                       System.out.println("Unit Code Detail Table--> "+Unit);
//                   }
//                   rsi.closeRowSetIterator();
        

//        if (AtHrs != null && IpHrs != null && TotalHrs != null) {
//            System.out.println("In Total Hours ACL");
//            
//            System.out.println("Employee From Detail Table 2 --> "+employeeNumberBinding.getValue());
//            System.out.println("Unit Code Detail Table 2 --> "+unitCodeBinding.getValue());
//            
//            OperationBinding opr = ADFUtils.findOperation("CalculateAmountValueForOverTime");
//            opr.getParamsMap().put("Employee", employeeNumberBinding.getValue());
//            opr.getParamsMap().put("UnitCd", unitCodeBinding.getValue());
//            opr.execute();
//            
//            BigDecimal Amt = (BigDecimal) opr.getResult();
//            System.out.println("Amt ----> " + Amt);
//
//            BigDecimal paidHrs = (BigDecimal) totalHourdBinding.getValue();
//            System.out.println("paidHrs----> " + paidHrs);
//
//            BigDecimal n4 = new BigDecimal(8);
//            System.out.println("n4--->  " + n4);
//            BigDecimal Amount1 = (Amt.divide(paidDays, 3, RoundingMode.HALF_UP));
//            System.out.println("Amount1----> " + Amount1);
//            BigDecimal Amount2 = (Amount1.divide(n4, 3, RoundingMode.HALF_UP));
//            System.out.println("Amount2----> " + Amount2);
//
//            BigDecimal n5 = new BigDecimal(2);
//            BigDecimal n6 = new BigDecimal(1);
//            BigDecimal Amount3;
//            if(flag1Binding.getValue().equals("GD")){
//                System.out.println("GOOD WORK!!!");
//           Amount3 =(Amount2.multiply(paidHrs).multiply(n5)).setScale(0, RoundingMode.HALF_UP);
//            }
//            else{
//                System.out.println("OVER TIME!!!");
//                     Amount3 =(Amount2.multiply(paidHrs).multiply(n6)).setScale(0, RoundingMode.HALF_UP);
//                }
//
//            System.out.println("Amount3----> " + Amount3);
//
//
//            amountBinding.setValue(Amount3);
//
//            System.out.println("In Total Hours ACL After call VCL");
//
//
//        }
    }

    public void totalHoursVCL(ValueChangeEvent valueChangeEvent) {
        //       System.out.println("In Total Hours ACL");
        //       OperationBinding opr=ADFUtils.findOperation("CalculateAmountValueForOverTime");
        //       opr.execute();
        //        System.out.println("In Total Hours ACL After call VCL");
        //
        //        DCIteratorBinding dci =ADFUtils.findIterator("ContractOrderDetailVO1Iterator");
        //                RowSetIterator rsi=dci.getViewObject().createRowSetIterator(null);
        //                    while(rsi.hasNext()) {
        //                        Row r = rsi.next();
        //                        String Amt=(String) r.getAttribute("Lumsum");
        //                        System.out.println("Amt ITERATOR"+Amt);
        //                    }
        //                    rsi.closeRowSetIterator();
    }

    public void setMonthBinding(RichInputText monthBinding) {
        this.monthBinding = monthBinding;
    }

    public RichInputText getMonthBinding() {
        return monthBinding;
    }

    public void setDualMonthBinding(RichSelectOneChoice dualMonthBinding) {
        this.dualMonthBinding = dualMonthBinding;
    }

    public RichSelectOneChoice getDualMonthBinding() {
        return dualMonthBinding;
    }

    public void setDualYearBinding(RichInputText dualYearBinding) {
        this.dualYearBinding = dualYearBinding;
    }

    public RichInputText getDualYearBinding() {
        return dualYearBinding;
    }

    public void setYearBinding(RichInputText yearBinding) {
        this.yearBinding = yearBinding;
    }

    public RichInputText getYearBinding() {
        return yearBinding;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }

    public void cevModeDisableComponent(String mode) {
        if (mode.equals("E")) {
            message = "E";
            getUnitCodeDetailBinding().setDisabled(true);
            getMonthBinding().setDisabled(true);
            
            getMonth1Binding().setDisabled(true);
            getYearheaderBinding().setDisabled(true);
            getUnitCodeBinding1().setDisabled(true);
            getFlagHeaderBinding().setDisabled(true);
            
            getEmployeeNumberBinding().setDisabled(true);
            getTotalHourdBinding().setDisabled(true);
            getAmountBinding().setDisabled(true);
            getYearBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
            getDualMonthBinding().setDisabled(true);
            getDualYearBinding().setDisabled(true);
            getFlag1Binding().setDisabled(true);
            getApprovedDateBinding().setDisabled(false);
            getFlagBinding().setDisabled(false);
            getGetgdOtPlagBinding().setDisabled(false);
        } else if (mode.equals("C")) {
            getUnitCodeDetailBinding().setDisabled(true);
            getMonthBinding().setDisabled(false);
            getYearBinding().setDisabled(false);
            getEmployeeNumberBinding().setDisabled(false);
            getTotalHourdBinding().setDisabled(true);
            getAmountBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
            getUnitCodeBinding1().setDisabled(true);
            getFlagBinding().setDisabled(false);
            getGetgdOtPlagBinding().setDisabled(false);
            getFlagHeaderBinding().setDisabled(false);
        } else if (mode.equals("V")) {
            


        }
    }

    public void setBindingOutputText(RichPanelLabelAndMessage bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichPanelLabelAndMessage getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setUnitCodeBinding1(RichInputComboboxListOfValues unitCodeBinding1) {
        this.unitCodeBinding1 = unitCodeBinding1;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding1() {
        return unitCodeBinding1;
    }

    public void setFlag1Binding(RichSelectOneChoice flag1Binding) {
        this.flag1Binding = flag1Binding;
    }

    public RichSelectOneChoice getFlag1Binding() {
        return flag1Binding;
    }

    public void setMonth1Binding(RichInputText month1Binding) {
        this.month1Binding = month1Binding;
    }

    public RichInputText getMonth1Binding() {
        return month1Binding;
    }

    public void setYearheaderBinding(RichInputText yearheaderBinding) {
        this.yearheaderBinding = yearheaderBinding;
    }

    public RichInputText getYearheaderBinding() {
        return yearheaderBinding;
    }

    public void approvedByVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
             if (vce.getNewValue() != null) {
                 OperationBinding op = (OperationBinding) ADFUtils.findOperation("checkApprovalAuthority");
                 op.getParamsMap().put("unitCd", getUnitCodeBinding().getValue().toString());
                 op.getParamsMap().put("empCode", vce.getNewValue());
                 op.getParamsMap().put("authLevel", "AP");
                 op.getParamsMap().put("formName", "OTA");
                 
                 op.execute();
                 if (op.getResult() != null && op.getResult().equals("N")) {
                     Row r = (Row) ADFUtils.evaluateEL("#{bindings.OverTimeGoodWorkEntryDualVO1Iterator.currentRow}");
                     r.setAttribute("ApprovedBy", null);
                     FacesMessage Message =
                         new FacesMessage("Sorry You Are Not Authorized To Approve this Over Time.");
                     Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                     FacesContext fc = FacesContext.getCurrentInstance();
                     fc.addMessage(null, Message);
                 } 

             }
    }

    public void setApprovedByBinding(RichInputComboboxListOfValues approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputComboboxListOfValues getApprovedByBinding() {
        return approvedByBinding;
    }

    public void approvedByOverTimeVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
              
                                if(vce!=null)
                                {
                                 vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
                                 OperationBinding op = (OperationBinding) ADFUtils.findOperation("approvedByForOverTimeGoodWork");
                                 op.getParamsMap().put("unitCd", unitCodeBinding.getValue().toString());
                                 op.getParamsMap().put("EmpCd",(String) vce.getNewValue());
                                 op.getParamsMap().put("authLevel", "AP");
                                 op.getParamsMap().put("formName", "OTA");
                                    op.execute();
                                }
    }

    public void setApprovedDateBinding(RichInputDate approvedDateBinding) {
        this.approvedDateBinding = approvedDateBinding;
    }

    public RichInputDate getApprovedDateBinding() {
        return approvedDateBinding;
    }

    public void setFlagBinding(RichSelectOneChoice flagBinding) {
        this.flagBinding = flagBinding;
    }

    public RichSelectOneChoice getFlagBinding() {
        return flagBinding;
    }

    public void setGetgdOtPlagBinding(RichInputText getgdOtPlagBinding) {
        this.getgdOtPlagBinding = getgdOtPlagBinding;
    }

    public RichInputText getGetgdOtPlagBinding() {
        return getgdOtPlagBinding;
    }

    public void setFlagHeaderBinding(RichSelectOneChoice flagHeaderBinding) {
        this.flagHeaderBinding = flagHeaderBinding;
    }

    public RichSelectOneChoice getFlagHeaderBinding() {
        return flagHeaderBinding;
    }

//    public void EmpNoVCL(ValueChangeEvent valueChangeEvent) {
//      String flg=(String) getGetgdOtPlagBinding().getValue();
//      if(flg=="GD")
//      {
//          OperationBinding op=(OperationBinding)ADFUtils.findOperation("getEmployeesForGoodTime");
//          op.execute();
//          }
//    }

    public void setOtStatusBinding(RichInputText otStatusBinding) {
        this.otStatusBinding = otStatusBinding;
    }

    public RichInputText getOtStatusBinding() {
        return otStatusBinding;
    }

    public void setApprvByBinding(RichInputComboboxListOfValues apprvByBinding) {
        this.apprvByBinding = apprvByBinding;
    }

    public RichInputComboboxListOfValues getApprvByBinding() {
        return apprvByBinding;
    }

    public void setCreateButtonBinding(RichButton createButtonBinding) {
        this.createButtonBinding = createButtonBinding;
    }

    public RichButton getCreateButtonBinding() {
        return createButtonBinding;
    }

    public void setDeleteButtonBinding(RichButton deleteButtonBinding) {
        this.deleteButtonBinding = deleteButtonBinding;
    }

    public RichButton getDeleteButtonBinding() {
        return deleteButtonBinding;
    }

    public void deleteButtonAL(ActionEvent actionEvent) {
        if(getApprovedByBinding().getValue()!=null){
                       ADFUtils.showMessage("Approved Over Time Good Work Entry can not be deleted.", 2);
                   }
    }
}
