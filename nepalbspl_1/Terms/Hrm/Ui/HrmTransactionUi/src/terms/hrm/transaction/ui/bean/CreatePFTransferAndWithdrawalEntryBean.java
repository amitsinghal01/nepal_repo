package terms.hrm.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichPanelLabelAndMessage;
import oracle.adf.view.rich.component.rich.nav.RichButton;

import oracle.binding.OperationBinding;

public class CreatePFTransferAndWithdrawalEntryBean {
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText entryNumberBinding;
    private RichInputDate entryDateBinding;
    private RichInputComboboxListOfValues employeeNumberBinding;
    private RichInputDate dateOfJoiningBinding;
    private RichInputText letterReferenceNumberBinding;
    private RichInputDate letterReferenceDateBinding;
    private RichInputDate dateOfLeavingBinding;
    private RichInputText permanentAddressBinding;
    private RichInputText permanentAddress2Binding;
    private RichInputText newAddressBinding;
    private RichInputText newAddress2Binding;
    private RichPanelLabelAndMessage bindingOutputText;
    private String message="C";
    private RichSelectOneChoice pfTypeBinding;
    private RichInputText pfNumberBinding;

    public CreatePFTransferAndWithdrawalEntryBean() {
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void saveButtonAL(ActionEvent actionEvent) {
        
        ADFUtils.setLastUpdatedBy("PFTransferAndWithdrawalEntryVO1Iterator","LastUpdatedBy");
        
        ADFUtils.findOperation("generateEntryNumberPFTransferAndWithdrawalEntry").execute();
        OperationBinding opr = ADFUtils.findOperation("Commit");
        Object obj = opr.execute();
        
        System.out.println("outside error block");
         System.out.println("Level Code value is"+entryNumberBinding.getValue());

         //   if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
         
        //       if(leaveCodeHeaderBinding.getValue() !=null){
        if (message.equalsIgnoreCase("C")){
                System.out.println("S####");
                FacesMessage Message = new FacesMessage("Record Save Successfully. New Entry Number is "+entryNumberBinding.getValue());
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);

            } else if (message.equalsIgnoreCase("E")) {
                FacesMessage Message = new FacesMessage("Record Update Successfully");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            }
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setEntryNumberBinding(RichInputText entryNumberBinding) {
        this.entryNumberBinding = entryNumberBinding;
    }

    public RichInputText getEntryNumberBinding() {
        return entryNumberBinding;
    }

    public void setEntryDateBinding(RichInputDate entryDateBinding) {
        this.entryDateBinding = entryDateBinding;
    }

    public RichInputDate getEntryDateBinding() {
        return entryDateBinding;
    }

    public void setEmployeeNumberBinding(RichInputComboboxListOfValues employeeNumberBinding) {
        this.employeeNumberBinding = employeeNumberBinding;
    }

    public RichInputComboboxListOfValues getEmployeeNumberBinding() {
        return employeeNumberBinding;
    }

    public void setDateOfJoiningBinding(RichInputDate dateOfJoiningBinding) {
        this.dateOfJoiningBinding = dateOfJoiningBinding;
    }

    public RichInputDate getDateOfJoiningBinding() {
        return dateOfJoiningBinding;
    }

    public void setLetterReferenceNumberBinding(RichInputText letterReferenceNumberBinding) {
        this.letterReferenceNumberBinding = letterReferenceNumberBinding;
    }

    public RichInputText getLetterReferenceNumberBinding() {
        return letterReferenceNumberBinding;
    }

    public void setLetterReferenceDateBinding(RichInputDate letterReferenceDateBinding) {
        this.letterReferenceDateBinding = letterReferenceDateBinding;
    }

    public RichInputDate getLetterReferenceDateBinding() {
        return letterReferenceDateBinding;
    }

    public void setDateOfLeavingBinding(RichInputDate dateOfLeavingBinding) {
        this.dateOfLeavingBinding = dateOfLeavingBinding;
    }

    public RichInputDate getDateOfLeavingBinding() {
        return dateOfLeavingBinding;
    }

    public void setPermanentAddressBinding(RichInputText permanentAddressBinding) {
        this.permanentAddressBinding = permanentAddressBinding;
    }

    public RichInputText getPermanentAddressBinding() {
        return permanentAddressBinding;
    }

    public void setPermanentAddress2Binding(RichInputText permanentAddress2Binding) {
        this.permanentAddress2Binding = permanentAddress2Binding;
    }

    public RichInputText getPermanentAddress2Binding() {
        return permanentAddress2Binding;
    }

    public void setNewAddressBinding(RichInputText newAddressBinding) {
        this.newAddressBinding = newAddressBinding;
    }

    public RichInputText getNewAddressBinding() {
        return newAddressBinding;
    }

    public void setNewAddress2Binding(RichInputText newAddress2Binding) {
        this.newAddress2Binding = newAddress2Binding;
    }

    public RichInputText getNewAddress2Binding() {
        return newAddress2Binding;
    }

    public void setBindingOutputText(RichPanelLabelAndMessage bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichPanelLabelAndMessage getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }
    
    private void cevmodecheck(){
            if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
                cevModeDisableComponent("V");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("C");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("E");
            }
        }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
               try {
                   Method method1 =
                       component.getClass().getMethod("setDisabled", boolean.class);
                   if (method1 != null) {
                       method1.invoke(component, valueComponent);
                   }
               } catch (NoSuchMethodException e) {
                   try {
                       Method method =
                           component.getClass().getMethod("setReadOnly", boolean.class);
                       if (method != null) {
                           method.invoke(component, valueComponent);
                       }
                   } catch (Exception e1) {
                       // e.printStackTrace();//silently eat this exception.
                   }


               } catch (Exception e) {
                   // e.printStackTrace();//silently eat this exception.
               }
               List<UIComponent> childComponents = component.getChildren();
               for (UIComponent comp : childComponents) {
                   makeComponentHierarchyReadOnly(comp, valueComponent);
               }
           }

    public void cevModeDisableComponent(String mode) {
    if (mode.equals("E")) {
                getUnitCodeBinding().setDisabled(true);
                getEntryDateBinding().setDisabled(true);
                getEntryNumberBinding().setDisabled(true);
                getEmployeeNumberBinding().setDisabled(true);
                getDateOfJoiningBinding().setDisabled(true);
                getDateOfLeavingBinding().setDisabled(true);
                getLetterReferenceDateBinding().setDisabled(false);
                getLetterReferenceNumberBinding().setDisabled(false);
                getNewAddress2Binding().setDisabled(false);
                getNewAddressBinding().setDisabled(false);
                getPermanentAddress2Binding().setDisabled(true);
                getPermanentAddressBinding().setDisabled(true);
                getPfTypeBinding().setDisabled(false);
                getPfNumberBinding().setDisabled(true);
                getHeaderEditBinding().setDisabled(true);
                message="E";
            } else if (mode.equals("C")) {

                getUnitCodeBinding().setDisabled(true);
                getEntryDateBinding().setDisabled(true);
                getEntryNumberBinding().setDisabled(true);
                getEmployeeNumberBinding().setDisabled(false);
                getDateOfJoiningBinding().setDisabled(true);
                getDateOfLeavingBinding().setDisabled(true);
                getLetterReferenceDateBinding().setDisabled(false);
                getLetterReferenceNumberBinding().setDisabled(false);
                getNewAddress2Binding().setDisabled(false);
                getNewAddressBinding().setDisabled(false);
                getPermanentAddress2Binding().setDisabled(true);
                getPermanentAddressBinding().setDisabled(true);
                getPfTypeBinding().setDisabled(false);
                getPfNumberBinding().setDisabled(true);
                
            } else if (mode.equals("V")) {
            }
            
        }

    public void setPfTypeBinding(RichSelectOneChoice pfTypeBinding) {
        this.pfTypeBinding = pfTypeBinding;
    }

    public RichSelectOneChoice getPfTypeBinding() {
        return pfTypeBinding;
    }

    public void setPfNumberBinding(RichInputText pfNumberBinding) {
        this.pfNumberBinding = pfNumberBinding;
    }

    public RichInputText getPfNumberBinding() {
        return pfNumberBinding;
    }
}
