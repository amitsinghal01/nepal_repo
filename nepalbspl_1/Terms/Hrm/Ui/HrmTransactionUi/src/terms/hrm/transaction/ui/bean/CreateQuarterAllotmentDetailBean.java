package terms.hrm.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.validator.ValidatorException;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichPanelLabelAndMessage;

import oracle.binding.OperationBinding;

public class CreateQuarterAllotmentDetailBean {
    private RichInputText rentBinding;
    private RichInputComboboxListOfValues unitBinding;
    private RichInputComboboxListOfValues empNoBinding;
    private RichInputComboboxListOfValues colonyCdBinding;
    private RichInputComboboxListOfValues quarterCodrBinding;
    private RichInputComboboxListOfValues quarterCodeBinding;
    private RichInputText quarterNoBinding;
    private RichInputDate quarterAllotmentDateBinding;
    private RichInputDate quarterAWithdrawalDateBinding;
    private RichPanelLabelAndMessage bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private String message="C";
    

    public CreateQuarterAllotmentDetailBean() {
    }   
    private void cevmodecheck(){
           if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
               cevModeDisableComponent("V");
           }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
               cevModeDisableComponent("C");
           }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
               cevModeDisableComponent("E");
           }
       } 
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                try {
                    Method method1 =
                        component.getClass().getMethod("setDisabled", boolean.class);
                    if (method1 != null) {
                        method1.invoke(component, valueComponent);
                    }
                } catch (NoSuchMethodException e) {
                    try {
                        Method method =
                            component.getClass().getMethod("setReadOnly", boolean.class);
                        if (method != null) {
                            method.invoke(component, valueComponent);
                        }
                    } catch (Exception e1) {
                        // e.printStackTrace();//silently eat this exception.
                    }


                } catch (Exception e) {
                    // e.printStackTrace();//silently eat this exception.
                }
                List<UIComponent> childComponents = component.getChildren();
                for (UIComponent comp : childComponents) {
                    makeComponentHierarchyReadOnly(comp, valueComponent);
                }
            }
    
    
    //Set Fields and Button disable.
        public void cevModeDisableComponent(String mode) {
        //            FacesContext fctx = FacesContext.getCurrentInstance();
        //            ELContext elctx = fctx.getELContext();
        //            Application jsfApp = fctx.getApplication();
        //            //create a ValueExpression that points to the ADF binding layer
        //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
        //            //
        //            ValueExpression valueExpr = exprFactory.createValueExpression(
        //                                         elctx,
        //                                         "#{pageFlowScope.mode=='E'}",
        //                                          Object.class
        //                                         );
        //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
            if (mode.equals("E")) {
               getUnitBinding().setDisabled(true);
               getRentBinding().setDisabled(true);
               getQuarterNoBinding().setDisabled(true);
               getUnitBinding().setDisabled(true);
               empNoBinding.setDisabled(true);
                    message="E";
            } else if (mode.equals("C")) {
                getRentBinding().setDisabled(true);
                getQuarterNoBinding().setDisabled(true);
                getUnitBinding().setDisabled(true);
  
            } else if (mode.equals("V")) {
                getUnitBinding().setDisabled(true);
            }
            
        }
    
    public void setRentBinding(RichInputText rentBinding) {
        this.rentBinding = rentBinding;
    }

    public RichInputText getRentBinding() {
        return rentBinding;
    }

    public void setUnitBinding(RichInputComboboxListOfValues unitBinding) {
        this.unitBinding = unitBinding;
    }

    public RichInputComboboxListOfValues getUnitBinding() {
        return unitBinding;
    }

    public void setEmpNoBinding(RichInputComboboxListOfValues empNoBinding) {
        this.empNoBinding = empNoBinding;
    }

    public RichInputComboboxListOfValues getEmpNoBinding() {
        return empNoBinding;
    }

    public void setColonyCdBinding(RichInputComboboxListOfValues colonyCdBinding) {
        this.colonyCdBinding = colonyCdBinding;
    }

    public RichInputComboboxListOfValues getColonyCdBinding() {
        return colonyCdBinding;
    }

    public void setQuarterCodrBinding(RichInputComboboxListOfValues quarterCodrBinding) {
        this.quarterCodrBinding = quarterCodrBinding;
    }

    public RichInputComboboxListOfValues getQuarterCodrBinding() {
        return quarterCodrBinding;
    }

    public void setQuarterCodeBinding(RichInputComboboxListOfValues quarterCodeBinding) {
        this.quarterCodeBinding = quarterCodeBinding;
    }

    public RichInputComboboxListOfValues getQuarterCodeBinding() {
        return quarterCodeBinding;
    }

    public void setQuarterNoBinding(RichInputText quarterNoBinding) {
        this.quarterNoBinding = quarterNoBinding;
    }

    public RichInputText getQuarterNoBinding() {
        return quarterNoBinding;
    }

    public void setQuarterAllotmentDateBinding(RichInputDate quarterAllotmentDateBinding) {
        this.quarterAllotmentDateBinding = quarterAllotmentDateBinding;
    }

    public RichInputDate getQuarterAllotmentDateBinding() {
        return quarterAllotmentDateBinding;
    }

    public void setQuarterAWithdrawalDateBinding(RichInputDate quarterAWithdrawalDateBinding) {
        this.quarterAWithdrawalDateBinding = quarterAWithdrawalDateBinding;
    }

    public RichInputDate getQuarterAWithdrawalDateBinding() {
        return quarterAWithdrawalDateBinding;
    }

    public void setBindingOutputText(RichPanelLabelAndMessage bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichPanelLabelAndMessage getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void saveButtonAL(ActionEvent actionEvent) {
       
        ADFUtils.setLastUpdatedBy("QuarterAllotmentDetailVO1Iterator","LastUpdatedBy");
        System.out.println("--------Commit-------");
                if (message.equalsIgnoreCase("C")) {
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message = new FacesMessage("Record Saved Successfully.");   
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                    FacesContext fc = FacesContext.getCurrentInstance();   
                    fc.addMessage(null, Message);
                }                     
                if (message.equalsIgnoreCase("E"))  {
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message = new FacesMessage("Record Update Successfully.");   
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                    FacesContext fc = FacesContext.getCurrentInstance();   
                    fc.addMessage(null, Message);
                }
            
    }

//    public void DateValidatorForQuarterAllotment(FacesContext facesContext, UIComponent uIComponent, Object object) {
//            
//        if(object!=null && quarterAWithdrawalDateBinding.getValue()!=null){            
//          oracle.jbo.domain.Date d=(oracle.jbo.domain.Date)object;
//          oracle.jbo.domain.Date d1=(oracle.jbo.domain.Date)quarterAWithdrawalDateBinding.getValue();          
//          if(d.compareTo(d1)==-1){
//              System.out.println("Inside Validator");
////              ADFUtils.showMessage(" ",1);
//           throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,"Quarter Date shouldn't be less",null));
//          }              
//          }
//          
//        }

  
}
