package terms.hrm.transaction.ui.bean;

import java.math.BigDecimal;
import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import javax.faces.validator.ValidatorException;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.binding.OperationBinding;

public class CreateSalaryIncrementArrearBean {
    private RichPanelHeader getMyPageRootComponent;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputComboboxListOfValues employeeCodeBinding;
    private RichInputText designationCdBinding;
    private RichInputText catcdBinding;
    private RichInputText lvlcdBinding;
    private RichInputDate effectiveDateBinding;
    private RichInputText unitNameBinding;
    private RichInputText EMPNAMEBINDING;
    private RichInputText olddesnamebinding;
    private RichInputText oldcatnamebinding;
    private RichInputText oldlvlnamebinding;
    private RichInputText yearBinding;
    private RichInputDate incDateBinding;
    private RichInputText newCategoryBinding;
    private RichInputText newLevelCodeBinding;
    private RichSelectOneChoice monthBinding;
    private RichInputText newdesnamebinding;
    private RichInputText newcatnamebinding;
    private RichInputText newlvlnamebinding;
    private RichButton populatebtnBinding;
    private String message="C";
    private RichInputText newDesignationBinding;
    private RichInputText incArreridBinding;
    private RichInputText bindingOutputText;

    public CreateSalaryIncrementArrearBean() {
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setEmployeeCodeBinding(RichInputComboboxListOfValues employeeCodeBinding) {
        this.employeeCodeBinding = employeeCodeBinding;
    }

    public RichInputComboboxListOfValues getEmployeeCodeBinding() {
        return employeeCodeBinding;
    }

    public void setDesignationCdBinding(RichInputText designationCdBinding) {
        this.designationCdBinding = designationCdBinding;
    }

    public RichInputText getDesignationCdBinding() {
        return designationCdBinding;
    }

    public void setCatcdBinding(RichInputText catcdBinding) {
        this.catcdBinding = catcdBinding;
    }

    public RichInputText getCatcdBinding() {
        return catcdBinding;
    }

    public void setLvlcdBinding(RichInputText lvlcdBinding) {
        this.lvlcdBinding = lvlcdBinding;
    }

    public RichInputText getLvlcdBinding() {
        return lvlcdBinding;
    }

    public void setEffectiveDateBinding(RichInputDate effectiveDateBinding) {
        this.effectiveDateBinding = effectiveDateBinding;
    }

    public RichInputDate getEffectiveDateBinding() {
        return effectiveDateBinding;
    }

    public void setUnitNameBinding(RichInputText unitNameBinding) {
        this.unitNameBinding = unitNameBinding;
    }

    public RichInputText getUnitNameBinding() {
        return unitNameBinding;
    }

    public void setEMPNAMEBINDING(RichInputText EMPNAMEBINDING) {
        this.EMPNAMEBINDING = EMPNAMEBINDING;
    }

    public RichInputText getEMPNAMEBINDING() {
        return EMPNAMEBINDING;
    }

    public void setOlddesnamebinding(RichInputText olddesnamebinding) {
        this.olddesnamebinding = olddesnamebinding;
    }

    public RichInputText getOlddesnamebinding() {
        return olddesnamebinding;
    }

    public void setOldcatnamebinding(RichInputText oldcatnamebinding) {
        this.oldcatnamebinding = oldcatnamebinding;
    }

    public RichInputText getOldcatnamebinding() {
        return oldcatnamebinding;
    }

    public void setOldlvlnamebinding(RichInputText oldlvlnamebinding) {
        this.oldlvlnamebinding = oldlvlnamebinding;
    }

    public RichInputText getOldlvlnamebinding() {
        return oldlvlnamebinding;
    }

    public void setYearBinding(RichInputText yearBinding) {
        this.yearBinding = yearBinding;
    }

    public RichInputText getYearBinding() {
        return yearBinding;
    }

    public void setIncDateBinding(RichInputDate incDateBinding) {
        this.incDateBinding = incDateBinding;
    }

    public RichInputDate getIncDateBinding() {
        return incDateBinding;
    }

    public void setNewCategoryBinding(RichInputText newCategoryBinding) {
        this.newCategoryBinding = newCategoryBinding;
    }

    public RichInputText getNewCategoryBinding() {
        return newCategoryBinding;
    }

    public void setNewLevelCodeBinding(RichInputText newLevelCodeBinding) {
        this.newLevelCodeBinding = newLevelCodeBinding;
    }

    public RichInputText getNewLevelCodeBinding() {
        return newLevelCodeBinding;
    }

    public void setMonthBinding(RichSelectOneChoice monthBinding) {
        this.monthBinding = monthBinding;
    }

    public RichSelectOneChoice getMonthBinding() {
        return monthBinding;
    }

    public void setNewdesnamebinding(RichInputText newdesnamebinding) {
        this.newdesnamebinding = newdesnamebinding;
    }

    public RichInputText getNewdesnamebinding() {
        return newdesnamebinding;
    }

    public void setNewcatnamebinding(RichInputText newcatnamebinding) {
        this.newcatnamebinding = newcatnamebinding;
    }

    public RichInputText getNewcatnamebinding() {
        return newcatnamebinding;
    }

    public void setNewlvlnamebinding(RichInputText newlvlnamebinding) {
        this.newlvlnamebinding = newlvlnamebinding;
    }

    public RichInputText getNewlvlnamebinding() {
        return newlvlnamebinding;
    }

    public void setPopulatebtnBinding(RichButton populatebtnBinding) {
        this.populatebtnBinding = populatebtnBinding;
    }

    public RichButton getPopulatebtnBinding() {
        return populatebtnBinding;
    }



    public void yearVCL(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        monthBinding.setValue(null);
        monthBinding.resetValue();
    }

    public void monthValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        // Add event code here...
        if(object!=null){
                    if(yearBinding.getValue()!=null){
                    String month = (String)object;
                    BigDecimal YEAR = (BigDecimal)yearBinding.getValue();
                    OperationBinding op = ADFUtils.findOperation("dateChECK");
                    op.getParamsMap().put("YEAR", YEAR);
                    op.getParamsMap().put("month", month);
                    op.execute();
                    System.out.println("result in bean"+op.getResult());
                    oracle.jbo.domain.Date d=(oracle.jbo.domain.Date)effectiveDateBinding.getValue();
                    System.out.println("effective date"+d);
                    String effDateStr=d.toString();
                    System.out.println("effdate"+effDateStr.substring(0, 10));
                    oracle.jbo.domain.Date effDate= new oracle.jbo.domain.Date(effDateStr.substring(0, 10));
                    if( effDate.compareTo(op.getResult())== 1){
                        throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Year and Month should be greater than or equal to Effective Date", null));
                    }
                }
                    else{
                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_INFO, "Select Year First", null));
                        }
                        
                }

    }

    public void setNewDesignationBinding(RichInputText newDesignationBinding) {
        this.newDesignationBinding = newDesignationBinding;
    }

    public RichInputText getNewDesignationBinding() {
        return newDesignationBinding;
    }
    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }

    //Set Fields and Button disable.
        public void cevModeDisableComponent(String mode) {
               if (mode.equals("E")) {
                   designationCdBinding.setDisabled(true);
                   lvlcdBinding.setDisabled(true);
                   catcdBinding.setDisabled(true);
                   incDateBinding.setDisabled(true);
                   unitCodeBinding.setDisabled(true);
                   newLevelCodeBinding.setDisabled(true);
                   newCategoryBinding.setDisabled(true);
                   message="E";
                   newDesignationBinding.setDisabled(true);
                   unitNameBinding.setDisabled(true);
                   olddesnamebinding.setDisabled(true);
                   oldcatnamebinding.setDisabled(true);
                   oldlvlnamebinding.setDisabled(true);
                   newdesnamebinding.setDisabled(true);
                   newcatnamebinding.setDisabled(true);
                   newlvlnamebinding.setDisabled(true);
                   EMPNAMEBINDING.setDisabled(true);

               } else if (mode.equals("C")) {
                   designationCdBinding.setDisabled(true);
                   lvlcdBinding.setDisabled(true);
                   catcdBinding.setDisabled(true);
                   incDateBinding.setDisabled(true);
                   unitCodeBinding.setDisabled(true);
                   newLevelCodeBinding.setDisabled(true);
                   newCategoryBinding.setDisabled(true);
                   newDesignationBinding.setDisabled(true);
                   unitNameBinding.setDisabled(true);
                   olddesnamebinding.setDisabled(true);
                   oldcatnamebinding.setDisabled(true);
                   oldlvlnamebinding.setDisabled(true);
                   newdesnamebinding.setDisabled(true);
                   newcatnamebinding.setDisabled(true);
                   newlvlnamebinding.setDisabled(true);
                   EMPNAMEBINDING.setDisabled(true);

               } else if (mode.equals("V")) {
    //                   incamtbinding.setDisabled(true);
               }
               
           }

    public void setIncArreridBinding(RichInputText incArreridBinding) {
        this.incArreridBinding = incArreridBinding;
    }

    public RichInputText getIncArreridBinding() {
        return incArreridBinding;
    }

    public void setBindingOutputText(RichInputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichInputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void saveButtonAL(ActionEvent actionEvent) {
        // Add event code here...
        System.out.println("--------Commit-------");
                if (message.equalsIgnoreCase("C")) {
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message = new FacesMessage("Record Saved Successfully.");   
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                    FacesContext fc = FacesContext.getCurrentInstance();   
                    fc.addMessage(null, Message);
                    //procedure call
                    String unitCd = (String) unitCodeBinding.getValue();
                    String empCd = (String) employeeCodeBinding.getValue();
                    String month = (String) monthBinding.getValue();
                    String newLevelCd = (String) newLevelCodeBinding.getValue();
                    String oldLevelCd = (String) lvlcdBinding.getValue();
                    BigDecimal year = (BigDecimal) yearBinding.getValue();
                    System.out.println("year--->  " + year);
                    oracle.jbo.domain.Date effectiveD = (oracle.jbo.domain.Date) effectiveDateBinding.getValue();
                    System.out.println("effectiveDt  " + effectiveD);
                    String effDateStr=effectiveD.toString();
                    System.out.println("effdate"+effDateStr.substring(0, 10));
                    oracle.jbo.domain.Date effectiveDt= new oracle.jbo.domain.Date(effDateStr.substring(0, 10));
                    System.out.println("effectiveDt after " + effectiveDt);
                    oracle.jbo.domain.Number incid = (oracle.jbo.domain.Number) incArreridBinding.getValue();
                    System.out.println("incid  " + incid);
                    OperationBinding op = ADFUtils.findOperation("savadataaftercommitinsalary");
                    op.getParamsMap().put("unitCd", unitCd);
                    op.getParamsMap().put("empCd", empCd);
                    op.getParamsMap().put("month", month);
                    op.getParamsMap().put("newLevelCd", newLevelCd);
                    op.getParamsMap().put("oldLevelCd", oldLevelCd);
                    op.getParamsMap().put("incid", incid);
                    op.getParamsMap().put("year", year);
                    op.getParamsMap().put("effectiveDt", effectiveDt);
                    op.execute();
                    ADFUtils.findOperation("Commit").execute();
                    System.out.println("return in bean");
                    
                }                     
                if (message.equalsIgnoreCase("E"))  {
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message = new FacesMessage("Record Update Successfully.");   
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                    FacesContext fc = FacesContext.getCurrentInstance();   
                    fc.addMessage(null, Message);
                }
    }

    public void populateButtonAL(ActionEvent actionEvent) {
        // Add event code here...
        System.out.println("Inside Pop Button AL");

         String unitCd = (String) unitCodeBinding.getValue();
         String empCd = (String) employeeCodeBinding.getValue();
         String month = (String) monthBinding.getValue();
         String newLevelCd = (String) newLevelCodeBinding.getValue();
         String newCatery = (String) newCategoryBinding.getValue();
         String newDesignation = (String) newDesignationBinding.getValue();
         BigDecimal year= (BigDecimal) yearBinding.getValue();
         System.out.println("year--->  "+year);
         oracle.jbo.domain.Date effectiveDt=(oracle.jbo.domain.Date) effectiveDateBinding.getValue();
         System.out.println("effectiveDt  "+effectiveDt);
        OperationBinding op=ADFUtils.findOperation("populateSalaryIncrementArrearData");
        op.getParamsMap().put("unitCd", unitCd);
         op.getParamsMap().put("empCd", empCd);
         op.getParamsMap().put("month", month);
         op.getParamsMap().put("newLevelCd", newLevelCd);
         op.getParamsMap().put("newCatery", newCatery);
         op.getParamsMap().put("newDesignation", newDesignation);
         op.getParamsMap().put("year", year);
         op.getParamsMap().put("effectiveDt", effectiveDt);
        op.execute();
         employeeCodeBinding.setDisabled(true);
         populatebtnBinding.setDisabled(true);
    }
}
