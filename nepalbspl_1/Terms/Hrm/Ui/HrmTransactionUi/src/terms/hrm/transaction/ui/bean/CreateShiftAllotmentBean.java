package terms.hrm.transaction.ui.bean;

import java.lang.reflect.Method;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import javax.faces.validator.ValidatorException;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;

import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;
import oracle.jbo.domain.Timestamp;
import oracle.jbo.server.ViewObjectImpl;

public class CreateShiftAllotmentBean {
    private RichButton headerEditBinding;
    private String message = "C";
    private RichPanelHeader getMyPageRootComponent;
    private RichInputComboboxListOfValues unitCodeShiftAllotmentHeaderBinding;
    private RichSelectOneChoice monthBinding;
    private RichInputText yearBinding;
    private RichOutputText bindingOutputText;
    private RichInputText joinDateBinding;
    private RichInputDate shiftStartDateBinding;
    private RichButton detailCreateBinding;
    private RichInputComboboxListOfValues unitCodeDetailBinding;
    private RichSelectOneChoice monthDetailBinding;
    private RichInputText yearDetailBinding;
    private RichInputComboboxListOfValues empCodeDetailBinding;
    private RichInputText empNameDetail;
    private RichInputText departmentBinding;
    private RichInputText locationDetailBinding;
    private RichInputText levelDetailBinding;
    private RichInputText desigDetailBinding;
    private RichInputText deptDualBinding;
    private RichInputText locationDualBinding;
    private RichInputText levelDualBinding;
    private RichInputText desigDualBinding;
    private RichInputText unitNameDualBinding;
    private RichInputText empNameDualBinding;
    private RichInputText deptDescDualBinding;
    private RichInputText desigDescDualBinding;
    private RichInputText deptDescBinding;
    private RichInputText desigDescBinding;
    private RichInputText deptDescrptionBinding;
    private RichInputText shiftIdBinding;

    public CreateShiftAllotmentBean() {
    }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void saveButtonAL(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedBy("ShiftAllotmentHeaderVO1Iterator","LastUpdatedBy");
        OperationBinding opr = ADFUtils.findOperation("Commit");
        Object obj = opr.execute();

        if (message.equalsIgnoreCase("C")) {
            System.out.println("S####");
            FacesMessage Message = new FacesMessage("Record Save Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

            ADFUtils.setEL("#{pageFlowScope.mode}", "V");
            cevmodecheck();
            AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);

        } else if (message.equalsIgnoreCase("E")) {
            FacesMessage Message = new FacesMessage("Record Update Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

            ADFUtils.setEL("#{pageFlowScope.mode}", "V");
            cevmodecheck();
            AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);

        }
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setUnitCodeShiftAllotmentHeaderBinding(RichInputComboboxListOfValues unitCodeShiftAllotmentHeaderBinding) {
        this.unitCodeShiftAllotmentHeaderBinding = unitCodeShiftAllotmentHeaderBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeShiftAllotmentHeaderBinding() {
        return unitCodeShiftAllotmentHeaderBinding;
    }

    public void setMonthBinding(RichSelectOneChoice monthBinding) {
        this.monthBinding = monthBinding;
    }

    public RichSelectOneChoice getMonthBinding() {
        return monthBinding;
    }

    public void setYearBinding(RichInputText yearBinding) {
        this.yearBinding = yearBinding;
    }

    public RichInputText getYearBinding() {
        return yearBinding;
    }

    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }

    public void cevModeDisableComponent(String mode) {
        if (mode.equals("E")) {

            getUnitCodeShiftAllotmentHeaderBinding().setDisabled(true);
            getDetailCreateBinding().setDisabled(false);


            getUnitCodeDetailBinding().setDisabled(true);
            getMonthDetailBinding().setDisabled(true);
            getYearDetailBinding().setDisabled(true);
            getEmpCodeDetailBinding().setDisabled(true);
            getDeptDescrptionBinding().setDisabled(true);
            getDesigDescBinding().setDisabled(true);

            getDepartmentBinding().setDisabled(true);
            getLocationDetailBinding().setDisabled(true);
            getLevelDetailBinding().setDisabled(true);
            getDesigDetailBinding().setDisabled(true);

            message = "E";


        } else if (mode.equals("C")) {
            getUnitCodeShiftAllotmentHeaderBinding().setDisabled(true);
            getDeptDualBinding().setDisabled(true);
            getLocationDualBinding().setDisabled(true);
            getLevelDualBinding().setDisabled(true);
            getDesigDualBinding().setDisabled(true);
            getUnitNameDualBinding().setDisabled(true);
            getEmpNameDualBinding().setDisabled(true);
            getDeptDescDualBinding().setDisabled(true);
            getDesigDescDualBinding().setDisabled(true);


        } else if (mode.equals("V")) {
            getUnitCodeShiftAllotmentHeaderBinding().setDisabled(true);
            getUnitCodeDetailBinding().setDisabled(true);

            getDetailCreateBinding().setDisabled(true);


        }

    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void CreateButtonAL(ActionEvent actionEvent) {

        System.out.println("In CreateButtonAL");


        System.out.println("OutPut Value=====>" + bindingOutputText.getValue());
        if (bindingOutputText.getValue() != null && bindingOutputText.getValue().equals(1)) {

            String unit = (String) unitCodeDetailBinding.getValue();
            String month = (String) monthDetailBinding.getValue();
            oracle.jbo.domain.Number year = (oracle.jbo.domain.Number) yearDetailBinding.getValue();
            String empCode = (String) empCodeDetailBinding.getValue();
            String empName = (String) empNameDetail.getValue();


          
            ADFUtils.findOperation("CreateInsert1").execute();
            unitCodeDetailBinding.setValue(unit);
            monthDetailBinding.setValue(month);
            yearDetailBinding.setValue(year);
            empCodeDetailBinding.setValue(empCode);

            // empNameDetail.setValue(empName);
            //            departmentBinding.setValue(dept);
            //            locationDetailBinding.setValue(loc);
            //            levelDetailBinding.setValue(lvl);
            //            desigDetailBinding.setValue(desig);


        } else {
            ADFUtils.findOperation("getValuesFromShiftAllotmentDualToShiftAllotmentHeaderTable").execute();
        }
//        System.out.println("Month " + monthDetailBinding.getValue() + " Year " + yearDetailBinding.getValue());
//        String first = "01" + "/" + monthDetailBinding.getValue() + "/" + yearDetailBinding.getValue();
//        System.out.println("first "+first);
//      //  String sDate1="31/12/1998";
//        try{
//        Date date1=new SimpleDateFormat("dd/MM/yyyy").parse(first);
//        }
//        catch(Exception e){
//            e.printStackTrace();
//            }
    }

    public void setJoinDateBinding(RichInputText joinDateBinding) {
        this.joinDateBinding = joinDateBinding;
    }

    public RichInputText getJoinDateBinding() {
        return joinDateBinding;
    }

    public void setShiftStartDateBinding(RichInputDate shiftStartDateBinding) {
        this.shiftStartDateBinding = shiftStartDateBinding;
    }

    public RichInputDate getShiftStartDateBinding() {
        return shiftStartDateBinding;
    }

    public void shiftStartDateValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (object != null) {
            oracle.jbo.domain.Timestamp joinDate = (oracle.jbo.domain.Timestamp) joinDateBinding.getValue();
            oracle.jbo.domain.Timestamp shiftStartDate = (oracle.jbo.domain.Timestamp) object;
            System.out.println("joinDate: " + joinDate + " shiftStartDate: " + shiftStartDate);
            if (shiftStartDate.compareTo(joinDate) == -1) {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                              "Shift Start date can not be less then employee Joining Date.",
                                                              null));
            }
            String empCode = (String) getEmpCodeDetailBinding().getValue();
            Long id = (Long) getShiftIdBinding().getValue();
            //            oracle.jbo.domain.Timestamp newDate = (oracle.jbo.domain.Timestamp) vce.getNewValue();
            //        System.out.println("newStartDate---->" + newDate);
            OperationBinding op = ADFUtils.findOperation("shiftDateValidation");
            op.getParamsMap().put("empCode", empCode);
            op.getParamsMap().put("newStarDate", shiftStartDate);
            op.getParamsMap().put("shiftId", id);
            Object rst = op.execute();
            if (op.getResult() != null && op.getResult().equals("Y")) {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                              "Shift for this date is already alloted.", null));
            }

        }
        
        OperationBinding op = ADFUtils.findOperation("dateToCharForShiftAllotment");
        op.getParamsMap().put("StarDate", (oracle.jbo.domain.Timestamp) object);
        Object rst = op.execute();
        System.out.println(op.getResult()+" Result "+getMonthBinding().getValue());
        if (op.getResult() != null && op.getResult().equals(getMonthBinding().getValue())) {
            
        }
        else{
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                          "select current month.", null));
        }

    }

    public void setDetailCreateBinding(RichButton detailCreateBinding) {
        this.detailCreateBinding = detailCreateBinding;
    }

    public RichButton getDetailCreateBinding() {
        return detailCreateBinding;
    }

    public void setUnitCodeDetailBinding(RichInputComboboxListOfValues unitCodeDetailBinding) {
        this.unitCodeDetailBinding = unitCodeDetailBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeDetailBinding() {
        return unitCodeDetailBinding;
    }

    public void setMonthDetailBinding(RichSelectOneChoice monthDetailBinding) {
        this.monthDetailBinding = monthDetailBinding;
    }

    public RichSelectOneChoice getMonthDetailBinding() {
        return monthDetailBinding;
    }

    public void setYearDetailBinding(RichInputText yearDetailBinding) {
        this.yearDetailBinding = yearDetailBinding;
    }

    public RichInputText getYearDetailBinding() {
        return yearDetailBinding;
    }

    public void setEmpCodeDetailBinding(RichInputComboboxListOfValues empCodeDetailBinding) {
        this.empCodeDetailBinding = empCodeDetailBinding;
    }

    public RichInputComboboxListOfValues getEmpCodeDetailBinding() {
        return empCodeDetailBinding;
    }

    public void setEmpNameDetail(RichInputText empNameDetail) {
        this.empNameDetail = empNameDetail;
    }

    public RichInputText getEmpNameDetail() {
        return empNameDetail;
    }

    public void setDepartmentBinding(RichInputText departmentBinding) {
        this.departmentBinding = departmentBinding;
    }

    public RichInputText getDepartmentBinding() {
        return departmentBinding;
    }

    public void setLocationDetailBinding(RichInputText locationDetailBinding) {
        this.locationDetailBinding = locationDetailBinding;
    }

    public RichInputText getLocationDetailBinding() {
        return locationDetailBinding;
    }

    public void setLevelDetailBinding(RichInputText levelDetailBinding) {
        this.levelDetailBinding = levelDetailBinding;
    }

    public RichInputText getLevelDetailBinding() {
        return levelDetailBinding;
    }

    public void setDesigDetailBinding(RichInputText desigDetailBinding) {
        this.desigDetailBinding = desigDetailBinding;
    }

    public RichInputText getDesigDetailBinding() {
        return desigDetailBinding;
    }

    public void setDeptDualBinding(RichInputText deptDualBinding) {
        this.deptDualBinding = deptDualBinding;
    }

    public RichInputText getDeptDualBinding() {
        return deptDualBinding;
    }

    public void setLocationDualBinding(RichInputText locationDualBinding) {
        this.locationDualBinding = locationDualBinding;
    }

    public RichInputText getLocationDualBinding() {
        return locationDualBinding;
    }

    public void setLevelDualBinding(RichInputText levelDualBinding) {
        this.levelDualBinding = levelDualBinding;
    }

    public RichInputText getLevelDualBinding() {
        return levelDualBinding;
    }

    public void setDesigDualBinding(RichInputText desigDualBinding) {
        this.desigDualBinding = desigDualBinding;
    }

    public RichInputText getDesigDualBinding() {
        return desigDualBinding;
    }

    public void setUnitNameDualBinding(RichInputText unitNameDualBinding) {
        this.unitNameDualBinding = unitNameDualBinding;
    }

    public RichInputText getUnitNameDualBinding() {
        return unitNameDualBinding;
    }

    public void setEmpNameDualBinding(RichInputText empNameDualBinding) {
        this.empNameDualBinding = empNameDualBinding;
    }

    public RichInputText getEmpNameDualBinding() {
        return empNameDualBinding;
    }

    public void setDeptDescDualBinding(RichInputText deptDescDualBinding) {
        this.deptDescDualBinding = deptDescDualBinding;
    }

    public RichInputText getDeptDescDualBinding() {
        return deptDescDualBinding;
    }

    public void setDesigDescDualBinding(RichInputText desigDescDualBinding) {
        this.desigDescDualBinding = desigDescDualBinding;
    }

    public RichInputText getDesigDescDualBinding() {
        return desigDescDualBinding;
    }

    public void setDeptDescBinding(RichInputText deptDescBinding) {
        this.deptDescBinding = deptDescBinding;
    }

    public RichInputText getDeptDescBinding() {
        return deptDescBinding;
    }

    public void setDesigDescBinding(RichInputText desigDescBinding) {
        this.desigDescBinding = desigDescBinding;
    }

    public RichInputText getDesigDescBinding() {
        return desigDescBinding;
    }

    public void setDeptDescrptionBinding(RichInputText deptDescrptionBinding) {
        this.deptDescrptionBinding = deptDescrptionBinding;
    }

    public RichInputText getDeptDescrptionBinding() {
        return deptDescrptionBinding;
    }

    public void shiftStartDateVCL(ValueChangeEvent vce) {

    }

    public void endDateVCL(ValueChangeEvent valueChangeEvent) {

    }

    public void setShiftIdBinding(RichInputText shiftIdBinding) {
        this.shiftIdBinding = shiftIdBinding;
    }

    public RichInputText getShiftIdBinding() {
        return shiftIdBinding;
    }

    public void endDateValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {

        String empCode = (String) getEmpCodeDetailBinding().getValue();
        oracle.jbo.domain.Timestamp shiftEndDate = (oracle.jbo.domain.Timestamp) object;
        oracle.jbo.domain.Timestamp newShiftStartDate = (oracle.jbo.domain.Timestamp) shiftStartDateBinding.getValue();
        Long id = (Long) getShiftIdBinding().getValue();
        System.out.println("endDate In Bean---->" + shiftEndDate + "  shiftStartDate  " + "  ----  " +
                           newShiftStartDate);

        OperationBinding op = ADFUtils.findOperation("shiftEndDateValidation");
        op.getParamsMap().put("empCode", empCode);
        op.getParamsMap().put("endDate", shiftEndDate);
        op.getParamsMap().put("newStarDate", newShiftStartDate);
        op.getParamsMap().put("shiftId", id);
        Object rst = op.execute();

        if (op.getResult() != null && op.getResult().equals("Y")) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                          "Shift for this date is already alloted.", null));
        }
        
        OperationBinding op1 = ADFUtils.findOperation("dateToCharForEndDateShiftAllotment");
        op1.getParamsMap().put("EndDate", (oracle.jbo.domain.Timestamp) object);
        Object rst1 = op1.execute();
        System.out.println(op1.getResult()+" Result in end date "+getMonthBinding().getValue());
        if (op1.getResult() != null && op1.getResult().equals(getMonthBinding().getValue())) {
            
        }
        else{
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                          "select current month.", null));
        }

        

    }
    
    
}
