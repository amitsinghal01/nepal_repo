package terms.hrm.transaction.ui.bean;

import java.io.BufferedReader;
import java.io.IOException;

import java.io.InputStream;
import java.io.InputStreamReader;

import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.util.ResetUtils;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;

import oracle.jbo.ViewCriteria;
import oracle.jbo.ViewObject;

import oracle.jbo.server.ViewObjectImpl;

import org.apache.myfaces.trinidad.model.UploadedFile;

import terms.hrm.transaction.model.applicationModule.HrmTransactionAMImpl;
import terms.hrm.transaction.model.view.EmployeeMonthlyEarningDetailVORowImpl;
import terms.hrm.transaction.model.view.ShiftAllotmentDetailVORowImpl;
import terms.hrm.transaction.model.view.ShiftTempDateAllotmentVORowImpl;

public class CreateShiftAllotmentImportBean {
    private RichTable importTableBinding;
    private UploadedFile file;
    private RichPopup filePopupBinding;
    private ArrayList<String> list = new ArrayList<String>();
    private RichOutputText bindingOutputText;
    private String message = "C";
    private RichPanelHeader getMyPageRootComponent;

    public CreateShiftAllotmentImportBean() {
    }

    public void fileUploadVCL(ValueChangeEvent vce) {
        System.out.println("in file upload vce" + vce.getNewValue() + " new:" + vce.getOldValue());

        UploadedFile file = (UploadedFile) vce.getNewValue();
        //        setFile(file);
        System.out.println(" in file upload vce Content Type:" + file.getContentType() + " file name:" +
                           file.getFilename() + " Size:" + file.getLength() + " file:" + file);


        try {
            if (file.getFilename().substring(file.getFilename().lastIndexOf('.') + 1).equalsIgnoreCase("csv")) {
                System.out.println("CSV File Format catched");
                parseFile(file.getInputStream());
                ResetUtils.reset(vce.getComponent());
            } else {
                FacesMessage msg = new FacesMessage("File format not supported.-- Upload CSV file");
                msg.setSeverity(FacesMessage.SEVERITY_WARN);
                FacesContext.getCurrentInstance().addMessage(null, msg);
                ResetUtils.reset(vce.getComponent());
            }
            AdfFacesContext.getCurrentInstance().addPartialTarget(importTableBinding);
            displayErrors();
        } catch (IOException e) {
            // TODO
        }
    }

    public void cleardata() {
        DCIteratorBinding dci = ADFUtils.findIterator("ShiftTempDateAllotmentVO1Iterator");
        Row[] r = dci.getAllRowsInRange();
        System.out.println("ITERATOR RANGE:" + r.length);
        if (r.length > 0) {
            RowSetIterator rsi1 = dci.getViewObject().createRowSetIterator(null);
            while (rsi1.hasNext()) {
                Row r1 = rsi1.next();
                r1.remove();
            }
            AdfFacesContext.getCurrentInstance().addPartialTarget(importTableBinding);
            rsi1.closeRowSetIterator();
        }
    }

    public void clearPrevData(String unitname) {
        System.out.println("in clearprevdata unit:" + unitname);
        HrmTransactionAMImpl am =
            (HrmTransactionAMImpl) ADFUtils.getApplicationModuleForDataControl("HrmTransactionAMDataControl");
        ViewObjectImpl vo = am.getShiftTempDateAllotmentVO2();
        System.out.println("Row count before filter " + vo.getEstimatedRowCount());
        ViewCriteria hVc = vo.getViewCriteria("ShiftTempDateAllotmentVOCriteriaForUnitFilter");
        vo.applyViewCriteria(hVc, false);
        vo.setNamedWhereClauseParam("bindMode", "V");
        vo.setNamedWhereClauseParam("bindUnitCode", unitname);
        vo.executeQuery();
        System.out.println("Row count after filter " + vo.getEstimatedRowCount());
        RowSetIterator row=vo.createRowSetIterator(null);
        while(row.hasNext()){
            Row r=row.next();
            r.remove();
        }
        row.closeRowSetIterator();
        vo.setNamedWhereClauseParam("bindMode", null);
        


//        DCIteratorBinding dci = ADFUtils.findIterator("ShiftTempDateAllotmentVO1Iterator");
//        Row[] r = dci.getAllRowsInRange();
//        System.out.println("ITERATOR RANGE:" + r.length);
//        if (r.length > 0) {
//            RowSetIterator rsi1 = dci.getViewObject().createRowSetIterator(null);
//            while (rsi1.hasNext()) {
//                Row r1 = rsi1.next();
//                r1.remove();
//            }
//            rsi1.closeRowSetIterator();
//        }
    }

    private void parseFile(InputStream file) {
        boolean flagonetime = true;
        list.clear();
        //        getFilePopupBinding().hide();
        BufferedReader reader = new BufferedReader(new InputStreamReader(file));
        String strLine = "";
        StringTokenizer st = null;
        int lineNumber = 0, tokenNumber = 0, flag = 0;
        ShiftTempDateAllotmentVORowImpl rw = null;


        cleardata();
        DCIteratorBinding dci = ADFUtils.findIterator("ShiftTempDateAllotmentVO1Iterator");
        RowSetIterator rsi = dci.getViewObject().createRowSetIterator(null);
        try {

            while ((strLine = reader.readLine()) != null) {
                lineNumber++;
                if (lineNumber > 1) {
                    Row last = rsi.last();
                    int i = rsi.getRangeIndexOf(last);
                    rw = (ShiftTempDateAllotmentVORowImpl) rsi.createRow();
                    rw.setNewRowState(Row.STATUS_INITIALIZED);
                    rsi.insertRowAtRangeIndex(i + 1, rw);
                    rsi.setCurrentRow(rw);
                    flag = 1;
                }

                st = new StringTokenizer(strLine, ",");
                while (st.hasMoreTokens()) {
                    tokenNumber++;

                    String theToken = st.nextToken();
                    System.out.println("Line # " + lineNumber + ", Token # " + tokenNumber + ", Token : " + theToken +
                                       " flag:" + flag + " tokenNumber:" + tokenNumber);
                    if (lineNumber > 1) {
                        if (!validatedata(theToken) && flag == 1) {
                            System.out.println(" !validatedata(theToken): " + theToken);
                            list.add(theToken);
                            rw.remove();
                            break;
                        }

                        switch (tokenNumber) {
                        case 1:
                            System.out.println("validate emp:" + theToken);

                            rw.setAttribute("EmpCode", theToken);
                            flag = 0;
                            break;

                        case 2:
                            rw.setAttribute("EmpName", theToken);
                            flag = 0;
                            break;

                        case 3:
                            rw.setAttribute("UnitCd", theToken);
                            flag = 0;
//                            if (flagonetime) {
                                clearPrevData(theToken);
//                                flagonetime = false;
//                            }
                            break;

                        case 4:
                            rw.setAttribute("UnitName", theToken);
                            flag = 0;
                            break;

                        case 5:
                            rw.setAttribute("ShiftMonth", theToken);
                            flag = 0;
                            break;
                        case 6:
                            rw.setAttribute("ShiftYear", theToken);
                            flag = 0;
                            break;
                        case 7:
                            rw.setAttribute("Day01", theToken);
                            flag = 0;
                            break;
                        case 8:
                            rw.setAttribute("Day02", theToken);
                            flag = 0;
                            break;
                        case 9:
                            rw.setAttribute("Day03", theToken);
                            flag = 0;
                            break;
                        case 10:
                            rw.setAttribute("Day04", theToken);
                            flag = 0;
                            break;
                        case 11:
                            rw.setAttribute("Day05", theToken);
                            flag = 0;
                            break;
                        case 12:
                            rw.setAttribute("Day06", theToken);
                            flag = 0;
                            break;
                        case 13:
                            rw.setAttribute("Day07", theToken);
                            flag = 0;
                            break;
                        case 14:
                            rw.setAttribute("Day08", theToken);
                            flag = 0;
                            break;
                        case 15:
                            rw.setAttribute("Day09", theToken);
                            flag = 0;
                            break;
                        case 16:
                            rw.setAttribute("Day10", theToken);
                            flag = 0;
                            break;
                        case 17:
                            rw.setAttribute("Day11", theToken);
                            flag = 0;
                            break;
                        case 18:
                            rw.setAttribute("Day12", theToken);
                            flag = 0;
                            break;
                        case 19:
                            rw.setAttribute("Day13", theToken);
                            flag = 0;
                            break;
                        case 20:
                            rw.setAttribute("Day14", theToken);
                            flag = 0;
                            break;
                        case 21:
                            rw.setAttribute("Day15", theToken);
                            flag = 0;
                            break;
                        case 22:
                            rw.setAttribute("Day16", theToken);
                            flag = 0;
                            break;
                        case 23:
                            rw.setAttribute("Day17", theToken);
                            flag = 0;
                            break;
                        case 24:
                            rw.setAttribute("Day18", theToken);
                            flag = 0;
                            break;
                        case 25:
                            rw.setAttribute("Day19", theToken);
                            flag = 0;
                            break;
                        case 26:
                            rw.setAttribute("Day20", theToken);
                            flag = 0;
                            break;
                        case 27:
                            rw.setAttribute("Day21", theToken);
                            flag = 0;
                            break;
                        case 28:
                            rw.setAttribute("Day22", theToken);
                            flag = 0;
                            break;
                        case 29:
                            rw.setAttribute("Day23", theToken);
                            flag = 0;
                            break;
                        case 30:
                            rw.setAttribute("Day24", theToken);
                            flag = 0;
                            break;
                        case 31:
                            rw.setAttribute("Day25", theToken);
                            flag = 0;
                            break;
                        case 32:
                            rw.setAttribute("Day26", theToken);
                            flag = 0;
                            break;
                        case 33:
                            rw.setAttribute("Day27", theToken);
                            flag = 0;
                            break;
                        case 34:
                            rw.setAttribute("Day28", theToken);
                            flag = 0;
                            break;
                        case 35:
                            rw.setAttribute("Day29", theToken);
                            flag = 0;
                            break;
                        case 36:
                            rw.setAttribute("Day30", theToken);
                            flag = 0;
                            break;
                        case 37:
                            rw.setAttribute("Day31", theToken);
                            flag = 0;
                            break;

                        }
                    }
                }
                //reset token number
                tokenNumber = 0;
            }
            rsi.closeRowSetIterator();
        } catch (IOException e) {
            e.printStackTrace();
            ADFUtils.showMessage("Content error in uploaded file", 0);
            rsi.closeRowSetIterator();
        } catch (Exception e) {
            e.printStackTrace();
            ADFUtils.showMessage("Data error in uploaded file", 0);
            rsi.closeRowSetIterator();
        }
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public UploadedFile getFile() {
        return file;
    }

    public void displayErrors() {
        for (String obj : list) {
            System.out.println(obj);
            ADFUtils.showMessage("Invalid Employee Number:" + obj, 0);
        }
    }

    public boolean validatedata(String emp) {
        OperationBinding ob = ADFUtils.findOperation("validatedataForShiftAllot");
        ob.getParamsMap().put("empcd", emp);
        ob.execute();
        System.out.println("ob.getResult() in bean:" + ob.getResult());
        if (ob.getResult() != null) {
            boolean result = Boolean.parseBoolean(ob.getResult().toString());
            System.out.println("result:" + result);
            if (result) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public void setFilePopupBinding(RichPopup filePopupBinding) {
        this.filePopupBinding = filePopupBinding;
    }

    public RichPopup getFilePopupBinding() {
        return filePopupBinding;
    }

    public void setList(ArrayList<String> list) {
        this.list = list;
    }

    public ArrayList<String> getList() {
        return list;
    }

    public void setImportTableBinding(RichTable importTableBinding) {
        this.importTableBinding = importTableBinding;
    }

    public RichTable getImportTableBinding() {
        return importTableBinding;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        return bindingOutputText;
    }

    public void saveButtonAL(ActionEvent actionEvent) {
        OperationBinding opr = ADFUtils.findOperation("Commit");
        Object obj = opr.execute();
        if (message.equalsIgnoreCase("C")) {
            System.out.println("S####");
            FacesMessage Message = new FacesMessage("Record Saved Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
            //cevmodecheck();
            HrmTransactionAMImpl am =
                (HrmTransactionAMImpl) ADFUtils.getApplicationModuleForDataControl("HrmTransactionAMDataControl");
            ViewObjectImpl vo = am.getShiftTempDateAllotmentVO1();
            vo.executeQuery();
            AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
        }
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }
}
