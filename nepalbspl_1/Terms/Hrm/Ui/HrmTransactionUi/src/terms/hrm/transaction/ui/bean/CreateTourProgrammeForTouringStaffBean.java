package terms.hrm.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichPanelLabelAndMessage;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichPanelCollection;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class CreateTourProgrammeForTouringStaffBean {
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichButton detailCreateBinding;
    private RichButton detailDeleteBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText entryNumberBinding;
    private RichInputDate entryDateBinding;
    private RichInputComboboxListOfValues employeeCodeBinding;
    private RichTable createTourProgrammeForTouringStaffTableBinding;
    private RichInputComboboxListOfValues unitCodeDetailBinding;
    private RichInputText entryNumberDetailBinding;
    private RichInputDate entryDateDetailBinding;
    private RichInputText employeeCodeDetailBinding;
    private RichInputDate departureDateBinding;
    private RichInputDate departureTimeBinding;
    private RichInputText departureTownBinding;
    private RichInputDate arrivalDateBinding;
    private RichInputDate arrivalTimeBinding;
    private RichInputText arrivalTownBinding;
    private RichInputText postalAddressBinding;
    private RichPanelLabelAndMessage bindingOutputText;
    private String message="C";
    private RichInputText fareAmountBinding;
    private RichInputText dailyAllowanceBinding;
    private RichInputText miscellaneousAmountBinding;
    private RichInputComboboxListOfValues headOdDepartmentBinding;
    private RichInputComboboxListOfValues generalManagerBinding;
    private RichInputDate approvalDateBinding;
    private RichInputDate generalManagerDateBinding;
    private RichInputDate approvalDateHdBinding;
    private RichInputDate headOfDepartmentDateBinding;
    private RichInputDate generalManagerDateBinding1;
    private RichInputDate headDateDetailBinding;
    private RichInputDate generalDateDetailBinding;

    public CreateTourProgrammeForTouringStaffBean() {
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void saveButtonAL(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedBy("TourProgrammeForTouringStaffHeaderVO1Iterator","LastUpdatedBy");
        ADFUtils.findOperation("generateEntryNumberTourProgramme").execute();
        OperationBinding opr = ADFUtils.findOperation("Commit");
        Object obj = opr.execute();
        
        System.out.println("outside error block");
         System.out.println("Level Code value is"+entryNumberBinding.getValue());

         //   if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
         
        //       if(leaveCodeHeaderBinding.getValue() !=null){
        if (message.equalsIgnoreCase("C")){
                System.out.println("S####");
                FacesMessage Message = new FacesMessage("Record Save Successfully. New Category Code is "+entryNumberBinding.getValue());
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);

            } else if (message.equalsIgnoreCase("E")) {
                FacesMessage Message = new FacesMessage("Record Update Successfully");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            }
    }

    public void setDetailCreateBinding(RichButton detailCreateBinding) {
        this.detailCreateBinding = detailCreateBinding;
    }

    public RichButton getDetailCreateBinding() {
        return detailCreateBinding;
    }

    public void setDetailDeleteBinding(RichButton detailDeleteBinding) {
        this.detailDeleteBinding = detailDeleteBinding;
    }

    public RichButton getDetailDeleteBinding() {
        return detailDeleteBinding;
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                            {
                            ADFUtils.findOperation("Delete").execute();
                                OperationBinding op=  ADFUtils.findOperation("Commit");
                                Object rst = op.execute();
                            System.out.println("Record Delete Successfully");
                           
                                if(op.getErrors().isEmpty())
                                {
                                FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                                Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                                FacesContext fc = FacesContext.getCurrentInstance();  
                                fc.addMessage(null, Message);
                                }
                                else
                                {
                                FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                                FacesContext fc = FacesContext.getCurrentInstance();
                                fc.addMessage(null, Message);
                                 }
                        }
                        AdfFacesContext.getCurrentInstance().addPartialTarget(createTourProgrammeForTouringStaffTableBinding);
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setEntryNumberBinding(RichInputText entryNumberBinding) {
        this.entryNumberBinding = entryNumberBinding;
    }

    public RichInputText getEntryNumberBinding() {
        return entryNumberBinding;
    }

    public void setEntryDateBinding(RichInputDate entryDateBinding) {
        this.entryDateBinding = entryDateBinding;
    }

    public RichInputDate getEntryDateBinding() {
        return entryDateBinding;
    }

    public void setEmployeeCodeBinding(RichInputComboboxListOfValues employeeCodeBinding) {
        this.employeeCodeBinding = employeeCodeBinding;
    }

    public RichInputComboboxListOfValues getEmployeeCodeBinding() {
        return employeeCodeBinding;
    }

    public void setCreateTourProgrammeForTouringStaffTableBinding(RichTable createTourProgrammeForTouringStaffTableBinding) {
        this.createTourProgrammeForTouringStaffTableBinding = createTourProgrammeForTouringStaffTableBinding;
    }

    public RichTable getCreateTourProgrammeForTouringStaffTableBinding() {
        return createTourProgrammeForTouringStaffTableBinding;
    }

    public void setUnitCodeDetailBinding(RichInputComboboxListOfValues unitCodeDetailBinding) {
        this.unitCodeDetailBinding = unitCodeDetailBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeDetailBinding() {
        return unitCodeDetailBinding;
    }

    public void setEntryNumberDetailBinding(RichInputText entryNumberDetailBinding) {
        this.entryNumberDetailBinding = entryNumberDetailBinding;
    }

    public RichInputText getEntryNumberDetailBinding() {
        return entryNumberDetailBinding;
    }

    public void setEntryDateDetailBinding(RichInputDate entryDateDetailBinding) {
        this.entryDateDetailBinding = entryDateDetailBinding;
    }

    public RichInputDate getEntryDateDetailBinding() {
        return entryDateDetailBinding;
    }

    public void setEmployeeCodeDetailBinding(RichInputText employeeCodeDetailBinding) {
        this.employeeCodeDetailBinding = employeeCodeDetailBinding;
    }

    public RichInputText getEmployeeCodeDetailBinding() {
        return employeeCodeDetailBinding;
    }

    public void setDepartureDateBinding(RichInputDate departureDateBinding) {
        this.departureDateBinding = departureDateBinding;
    }

    public RichInputDate getDepartureDateBinding() {
        return departureDateBinding;
    }

    public void setDepartureTimeBinding(RichInputDate departureTimeBinding) {
        this.departureTimeBinding = departureTimeBinding;
    }

    public RichInputDate getDepartureTimeBinding() {
        return departureTimeBinding;
    }

    public void setDepartureTownBinding(RichInputText departureTownBinding) {
        this.departureTownBinding = departureTownBinding;
    }

    public RichInputText getDepartureTownBinding() {
        return departureTownBinding;
    }

    public void setArrivalDateBinding(RichInputDate arrivalDateBinding) {
        this.arrivalDateBinding = arrivalDateBinding;
    }

    public RichInputDate getArrivalDateBinding() {
        return arrivalDateBinding;
    }

    public void setArrivalTimeBinding(RichInputDate arrivalTimeBinding) {
        this.arrivalTimeBinding = arrivalTimeBinding;
    }

    public RichInputDate getArrivalTimeBinding() {
        return arrivalTimeBinding;
    }

    public void setArrivalTownBinding(RichInputText arrivalTownBinding) {
        this.arrivalTownBinding = arrivalTownBinding;
    }

    public RichInputText getArrivalTownBinding() {
        return arrivalTownBinding;
    }

    public void setPostalAddressBinding(RichInputText postalAddressBinding) {
        this.postalAddressBinding = postalAddressBinding;
    }

    public RichInputText getPostalAddressBinding() {
        return postalAddressBinding;
    }

    public void setBindingOutputText(RichPanelLabelAndMessage bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichPanelLabelAndMessage getBindingOutputText() {
       cevmodecheck();
        return bindingOutputText;
    }
    
    private void cevmodecheck(){
            if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
                cevModeDisableComponent("V");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("C");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("E");
            }
        }
    
     public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                try {
                    Method method1 =
                        component.getClass().getMethod("setDisabled", boolean.class);
                    if (method1 != null) {
                        method1.invoke(component, valueComponent);
                    }
                } catch (NoSuchMethodException e) {
                    try {
                        Method method =
                            component.getClass().getMethod("setReadOnly", boolean.class);
                        if (method != null) {
                            method.invoke(component, valueComponent);
                        }
                    } catch (Exception e1) {
                        // e.printStackTrace();//silently eat this exception.
                    }


                } catch (Exception e) {
                    // e.printStackTrace();//silently eat this exception.
                }
                List<UIComponent> childComponents = component.getChildren();
                for (UIComponent comp : childComponents) {
                    makeComponentHierarchyReadOnly(comp, valueComponent);
                }
            }
    public void cevModeDisableComponent(String mode) {
    if (mode.equals("E")) {
            getUnitCodeBinding().setDisabled(true);
            getEntryNumberBinding().setDisabled(true);
            getEntryDateBinding().setDisabled(true);
            getEmployeeCodeBinding().setDisabled(false);
            getUnitCodeDetailBinding().setDisabled(true);
            getEntryNumberDetailBinding().setDisabled(true);
            getEntryDateDetailBinding().setDisabled(true);
            getEmployeeCodeDetailBinding().setDisabled(true);
            getDepartureDateBinding().setDisabled(false);
            getDepartureTimeBinding().setDisabled(false);
            getDepartureTownBinding().setDisabled(false);
            getArrivalDateBinding().setDisabled(false);
            getArrivalTimeBinding().setDisabled(false);
            getArrivalTownBinding().setDisabled(false);
            getPostalAddressBinding().setDisabled(false);
            getFareAmountBinding().setDisabled(false);
            getMiscellaneousAmountBinding().setDisabled(false);
            //getDailyAllowanceBinding().setDisabled(false);
            getHeaderEditBinding().setDisabled(true);
            getHeadOfDepartmentDateBinding().setDisabled(true);
            getGeneralManagerDateBinding1().setDisabled(true);
//            getApprovalDateBinding().setDisabled(true);
//            getGeneralManagerDateBinding().setDisabled(true);
            message="E";
                   } 
    
    else if (mode.equals("C")) {
               getUnitCodeBinding().setDisabled(true);
               getEntryNumberBinding().setDisabled(true);
               getEntryDateBinding().setDisabled(true);
               getEmployeeCodeBinding().setDisabled(false);
               getUnitCodeDetailBinding().setDisabled(true);
               getEntryNumberDetailBinding().setDisabled(true);
               getEntryDateDetailBinding().setDisabled(true);
               getEmployeeCodeDetailBinding().setDisabled(true);
               getDepartureDateBinding().setDisabled(false);
               getDepartureTimeBinding().setDisabled(false);
               getDepartureTownBinding().setDisabled(false);
               getArrivalDateBinding().setDisabled(false);
               getArrivalTimeBinding().setDisabled(false);
               getArrivalTownBinding().setDisabled(false);
               getPostalAddressBinding().setDisabled(false);
               getFareAmountBinding().setDisabled(false);
               getMiscellaneousAmountBinding().setDisabled(false);
//               getHeadOfDepartmentDateBinding().setDisabled(true);
//               getGeneralManagerDateBinding1().setDisabled(true);
               //getDailyAllowanceBinding().setDisabled(false);
//               getApprovalDateHdBinding().setDisabled(true);
//               getGeneralManagerDateBinding().setDisabled(true);
           } else if (mode.equals("V")) {
              
           }
           
       }

    public void createButtonAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert").execute();
    }

    public void setFareAmountBinding(RichInputText fareAmountBinding) {
        this.fareAmountBinding = fareAmountBinding;
    }

    public RichInputText getFareAmountBinding() {
        return fareAmountBinding;
    }

    public void setDailyAllowanceBinding(RichInputText dailyAllowanceBinding) {
        this.dailyAllowanceBinding = dailyAllowanceBinding;
    }

    public RichInputText getDailyAllowanceBinding() {
        return dailyAllowanceBinding;
    }

    public void setMiscellaneousAmountBinding(RichInputText miscellaneousAmountBinding) {
        this.miscellaneousAmountBinding = miscellaneousAmountBinding;
    }

    public RichInputText getMiscellaneousAmountBinding() {
        return miscellaneousAmountBinding;
    }

//    public void headOfDepartmentVCL(ValueChangeEvent vce) {
//        System.out.println("ValueChangeEvent" + vce);
//        System.out.println("Unit Cd" + unitCodeBinding.getValue().toString());
//        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
//        System.out.println("In Bean");
//
//
//        if (vce != null) {
//            System.out.println("In if Bean");
//            vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
//            OperationBinding op = (OperationBinding) ADFUtils.findOperation("checkAuthorityForTourProgram");
//            op.getParamsMap().put("unitCd", unitCodeBinding.getValue().toString());
//            System.out.println("Unit Cd" + unitCodeBinding.getValue().toString());
//            // op.getParamsMap().put("empCode", headOdDepartmentBinding.getValue();
//            op.getParamsMap().put("authLevel", "AP");
//            op.getParamsMap().put("formName", "TOUR_PR");
//           op.getParamsMap().put("Usernm", ADFUtils.resolveExpression("#{pageFlowScope.userName}"));
//           //op.getParamsMap().put("Usernm","SWE161");
//            op.execute();
//            String res = (String) op.getResult();
//
//            if (res.equalsIgnoreCase("NE")) {
//                System.out.println("In NE case");
//                FacesMessage Message = new FacesMessage("Sorry,You Have No Authority For Approval the Tour Programme.");
//                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
//                FacesContext fc = FacesContext.getCurrentInstance();
//                fc.addMessage(null, Message);
//            } else {
//                FacesMessage Message = new FacesMessage("You Approved The Tour Programme");
//                Message.setSeverity(FacesMessage.SEVERITY_INFO);
//                FacesContext fc = FacesContext.getCurrentInstance();
//                fc.addMessage(null, Message);
//                System.out.println("In N case");
//                
//                //               if(getHeadOdDepartmentBinding().getValue() != null) {
//                                   System.out.println("INSIDE HEAD BINDING ##### ");
//                                   
//                                   java.sql.Timestamp datetime = new java.sql.Timestamp(System.currentTimeMillis());
//                                   oracle.jbo.domain.Date daTime = new oracle.jbo.domain.Date(datetime);
//                                   headOfDepartmentDateBinding.setValue(daTime);
//                //               }
//            }
//        }        
//    }

    public void setHeadOdDepartmentBinding(RichInputComboboxListOfValues headOdDepartmentBinding) {
        this.headOdDepartmentBinding = headOdDepartmentBinding;
    }

    public RichInputComboboxListOfValues getHeadOdDepartmentBinding() {
        return headOdDepartmentBinding;
    }

    public void setGeneralManagerBinding(RichInputComboboxListOfValues generalManagerBinding) {
        this.generalManagerBinding = generalManagerBinding;
    }

    public RichInputComboboxListOfValues getGeneralManagerBinding() {
        return generalManagerBinding;
    }

    public void generalManagerVCL(ValueChangeEvent vce) {
        System.out.println("ValueChangeEvent" + vce);
        System.out.println("Unit Cd" + unitCodeBinding.getValue().toString());
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        System.out.println("In Bean");


        if (vce != null) {
            System.out.println("In if Bean");
            vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
            OperationBinding op = (OperationBinding) ADFUtils.findOperation("checkAuthorityForTourProgramForGM");
            op.getParamsMap().put("unitCd", unitCodeBinding.getValue().toString());
            System.out.println("Unit Cd" + unitCodeBinding.getValue().toString());
            // op.getParamsMap().put("empCode", headOdDepartmentBinding.getValue();
            op.getParamsMap().put("authLevel", "AP");
            op.getParamsMap().put("formName", "TOUR_PR");
           // op.getParamsMap().put("Usernm", ADFUtils.resolveExpression("#{pageFlowScope.userName}"));
           op.getParamsMap().put("Usernm","Rajat");
            op.execute();
            String res = (String) op.getResult();

            if (res.equalsIgnoreCase("NE")) {
                System.out.println("In NE case");
                FacesMessage Message = new FacesMessage("Sorry,You Have No Authority For Approval the Tour Programme.");
                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            } else {
//                FacesMessage Message = new FacesMessage("You Approved The Tour Programme");
//                Message.setSeverity(FacesMessage.SEVERITY_INFO);
//                FacesContext fc = FacesContext.getCurrentInstance();
//                fc.addMessage(null, Message);
                System.out.println("In N case");
                
                System.out.println("INSIDE HEAD BINDING ##### ");
                
                java.sql.Timestamp datetime = new java.sql.Timestamp(System.currentTimeMillis());
                oracle.jbo.domain.Date daTime = new oracle.jbo.domain.Date(datetime);
                generalManagerDateBinding1.setValue(daTime);
              }
            }
        }

    public void setGeneralManagerDateBinding1(RichInputDate generalManagerDateBinding1) {
        this.generalManagerDateBinding1 = generalManagerDateBinding1;
    }

    public RichInputDate getGeneralManagerDateBinding1() {
        return generalManagerDateBinding1;
    }

    public void setApprovalDateBinding(RichInputDate approvalDateBinding) {
        this.approvalDateBinding = approvalDateBinding;
    }

    public RichInputDate getApprovalDateBinding() {
        return approvalDateBinding;
    }

    public void setGeneralManagerDateBinding(RichInputDate generalManagerDateBinding) {
        this.generalManagerDateBinding = generalManagerDateBinding;
    }

    public RichInputDate getGeneralManagerDateBinding() {
        return generalManagerDateBinding;
    }

    public void setApprovalDateHdBinding(RichInputDate approvalDateHdBinding) {
        this.approvalDateHdBinding = approvalDateHdBinding;
    }

    public RichInputDate getApprovalDateHdBinding() {
        return approvalDateHdBinding;
    }

    public void setHeadOfDepartmentDateBinding(RichInputDate headOfDepartmentDateBinding) {
        this.headOfDepartmentDateBinding = headOfDepartmentDateBinding;
    }

    public RichInputDate getHeadOfDepartmentDateBinding() {
        return headOfDepartmentDateBinding;
    }

    public void setHeadDateDetailBinding(RichInputDate headDateDetailBinding) {
        this.headDateDetailBinding = headDateDetailBinding;
    }

    public RichInputDate getHeadDateDetailBinding() {
        return headDateDetailBinding;
    }

    public void setGeneralDateDetailBinding(RichInputDate generalDateDetailBinding) {
        this.generalDateDetailBinding = generalDateDetailBinding;
    }

    public RichInputDate getGeneralDateDetailBinding() {
        return generalDateDetailBinding;
    }

    public void employeeCodeVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce != null) {
        OperationBinding op = (OperationBinding) ADFUtils.findOperation("hodlovforTourProgramme");
        op.getParamsMap().put("unitCd", unitCodeBinding.getValue().toString());
        op.getParamsMap().put("empCode",vce.getNewValue());
        op.execute();
                           
    }
}
    }
