package terms.hrm.transaction.ui.bean;


import javax.faces.application.FacesMessage;
import javax.faces.application.ViewHandler;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;

import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;
import oracle.jbo.domain.Timestamp;

import terms.hrm.transaction.model.view.DailyAttendanceVORowImpl;

public class DailyAttendanceBean {
    private String editAction="V";
    private RichInputComboboxListOfValues synchronizeAttendTypebinding;
    private RichTable dailyAttendanceTableBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputDate attendDateBinding;
    private RichInputDate headerAttendDateBinding;

    public DailyAttendanceBean() {
    }

//    public void populateButtonAL(ActionEvent actionEvent) {
//               ADFUtils.findOperation("PopulateDailyAttendance").execute();
//       
//    }

    public void synchronizeAttendanceTypeVCL(ValueChangeEvent vce) {
//             refreshPage();
//        String AtendTpe=(String)vce.getNewValue();
//        DCIteratorBinding dci=ADFUtils.findIterator("DailyAttendanceVO1Iterator");
//        RowSetIterator rsi=dci.getViewObject().createRowSetIterator(null); 
//        rsi.getCurrentRow();                                                                                
//             dci.getEstimatedRowCount();
//
//        while(rsi.hasNext())
//        {
//                System.out.println("In while Loop");  
//            Row r=rsi.next();
//            r.setAttribute("AttendType", AtendTpe);
//            }
//             rsi.closeRowSetIterator();        
 //            System.out.println("Hello from Attendance Type VCL");
         }
        

    public void setSynchronizeAttendTypebinding(RichInputComboboxListOfValues synchronizeAttendTypebinding) {
        this.synchronizeAttendTypebinding = synchronizeAttendTypebinding;
    }

    public RichInputComboboxListOfValues getSynchronizeAttendTypebinding() {
        return synchronizeAttendTypebinding;
    }
    
    protected void refreshPage() 
    {
    FacesContext fctx = FacesContext.getCurrentInstance();
    String refreshpage = fctx.getViewRoot().getViewId();
    ViewHandler ViewH = fctx.getApplication().getViewHandler();
    UIViewRoot UIV = ViewH.createView(fctx, refreshpage);
    UIV.setViewId(refreshpage);
    fctx.setViewRoot(UIV);
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
        {
        OperationBinding op = null;
        ADFUtils.findOperation("Delete").execute();
        op = (OperationBinding) ADFUtils.findOperation("Commit");
        if (getEditAction().equals("V")) {
        Object rst = op.execute();
        }
        System.out.println("Record Delete Successfully");
        if(op.getErrors().isEmpty()){
        FacesMessage Message = new FacesMessage("Record Deleted Successfully");
        Message.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        } else if (!op.getErrors().isEmpty())
        {
        OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
        Object rstr = opr.execute();
        FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
        Message.setSeverity(FacesMessage.SEVERITY_ERROR);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(dailyAttendanceTableBinding);
    }

    public void saveButtonAL(ActionEvent actionEvent) {
      ADFUtils.findOperation("Commit").execute();
      ADFUtils.showMessage("Record Updated Successfully.", 2);
      ADFUtils.findOperation("clearDailyAttendance").execute();
    }

    public void setDailyAttendanceTableBinding(RichTable dailyAttendanceTableBinding) {
        this.dailyAttendanceTableBinding = dailyAttendanceTableBinding;
    }

    public RichTable getDailyAttendanceTableBinding() {
        return dailyAttendanceTableBinding;
    }

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public void shiftCodeVCL(ValueChangeEvent vce) {
        
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
//        
//       Date AttenDate = (Date) getAttendDateBinding().getValue();     
//         Timestamp dt=new Timestamp(AttenDate);
//          System.out.println("AttenDate 2 ---->>"+dt);
        
//        
//        String AttenDate1=(String)getHeaderAttendDateBinding().getValue();     
//          System.out.println("AttenDate 1 ---->>"+AttenDate1);
//          
//          Timestamp dt=Timestamp.toTimestamp(AttenDate1);
//        System.out.println("AttenDate 3 ---->>"+dt);
//        
//        Timestamp AttenDate1 = (Timestamp) getHeaderAttendDateBinding().getValue();     
//          System.out.println("AttenDate 1---->>"+AttenDate1);
        
       // Date AttenDate=(Date) getAttendDateBinding().getValue();
      //  String AttenDate=(String) getAttendDateBinding().getValue();

//       String AttenDate=(String)getAttendDateBinding().getValue();     
//        System.out.println("AttenDate 2---->>"+AttenDate);
//        
//        Timestamp dt2=Timestamp.toTimestamp(AttenDate);
//        System.out.println("AttenDate 3 ---->>"+dt2);
// 
        OperationBinding op=ADFUtils.findOperation("shiftChangeInDailyAttendance");       
//        op.getParamsMap().put("Unit", Unit);
//        op.getParamsMap().put("AttenDate", AttenDate);      
        op.execute();
       
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setAttendDateBinding(RichInputDate attendDateBinding) {
        this.attendDateBinding = attendDateBinding;
    }

    public RichInputDate getAttendDateBinding() {
        return attendDateBinding;
    }

    public void setHeaderAttendDateBinding(RichInputDate headerAttendDateBinding) {
        this.headerAttendDateBinding = headerAttendDateBinding;
    }

    public RichInputDate getHeaderAttendDateBinding() {
        return headerAttendDateBinding;
    }

    public void inTimeVCL(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
    }

    public void attendDateVCL(ValueChangeEvent vce) {
//        String unit=(String)unitCodeBinding.getValue();
//        System.out.println("unit---->"+unit);
////        Timestamp AttendDate=(Timestamp)attendDateBinding.getValue();
////        System.out.println("AttendDate---->"+AttendDate);
//        System.out.println("Before call method");
//        ADFUtils.findOperation("attendDateValidationDailyAttendance").execute();
//        System.out.println("After call method");
        
    }
    public void cancelDataAL(ActionEvent actionEvent) {
       
        ADFUtils.findOperation("Rollback").execute();
//        refreshPage();
        ADFUtils.findOperation("clearDailyAttendance").execute();
    }
}
