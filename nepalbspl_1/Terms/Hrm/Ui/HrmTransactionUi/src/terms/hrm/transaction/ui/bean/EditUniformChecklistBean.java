package terms.hrm.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

public class EditUniformChecklistBean {
    private RichOutputText outputTextBinding;
    private RichPanelHeader getMyPageRootComponent;
    private String editAction="V";
    private RichInputText entrynoBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputDate entrydatebinding;
    private RichInputComboboxListOfValues finyearbinding;
    private RichInputText employeeNoBinding;
    private RichInputText desigCdBinding;
    private RichInputText deptcdbinding;
    private RichInputDate confirmBinding;

    public EditUniformChecklistBean() {
    }

    public void setOutputTextBinding(RichOutputText outputTextBinding) {
        this.outputTextBinding = outputTextBinding;
    }

    public RichOutputText getOutputTextBinding() {
        cevmodecheck();
        return outputTextBinding;
    }
 
    private void cevmodecheck(){
           if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
               cevModeDisableComponent("V");
           }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
               cevModeDisableComponent("C");
           }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
               cevModeDisableComponent("E");
           }
       }
    
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                try {
                    Method method1 =
                        component.getClass().getMethod("setDisabled", boolean.class);
                    if (method1 != null) {
                        method1.invoke(component, valueComponent);
                    }
                } catch (NoSuchMethodException e) {
                    try {
                        Method method =
                            component.getClass().getMethod("setReadOnly", boolean.class);
                        if (method != null) {
                            method.invoke(component, valueComponent);
                        }
                    } catch (Exception e1) {
                        // e.printStackTrace();//silently eat this exception.
                    }


                } catch (Exception e) {
                    // e.printStackTrace();//silently eat this exception.
                }
                List<UIComponent> childComponents = component.getChildren();
                for (UIComponent comp : childComponents) {
                    makeComponentHierarchyReadOnly(comp, valueComponent);
                }
            }


    public void cevModeDisableComponent(String mode) {
    //            FacesContext fctx = FacesContext.getCurrentInstance();
    //            ELContext elctx = fctx.getELContext();
    //            Application jsfApp = fctx.getApplication();
    //            //create a ValueExpression that points to the ADF binding layer
    //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
    //            //
    //            ValueExpression valueExpr = exprFactory.createValueExpression(
    //                                         elctx,
    //                                         "#{pageFlowScope.mode=='E'}",
    //                                          Object.class
    //                                         );
    //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
        if (mode.equals("E")) {
            getEntrynoBinding().setDisabled(true);
            getEntrydatebinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
            getFinyearbinding().setDisabled(true);
            getEmployeeNoBinding().setDisabled(true);
            getDeptcdbinding().setDisabled(true);
            getDesigCdBinding().setDisabled(true);
            getConfirmBinding().setDisabled(true);
    
             
        } else if (mode.equals("C")) {
           
        } else if (mode.equals("V")) {
           
        }
        
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void EditAL1(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void SaveAL(ActionEvent actionEvent) {
        ADFUtils.showMessage("Record Updated Succesfully", 2);
    }

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public void setEntrynoBinding(RichInputText entrynoBinding) {
        this.entrynoBinding = entrynoBinding;
    }

    public RichInputText getEntrynoBinding() {
        return entrynoBinding;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setEntrydatebinding(RichInputDate entrydatebinding) {
        this.entrydatebinding = entrydatebinding;
    }

    public RichInputDate getEntrydatebinding() {
        return entrydatebinding;
    }

    public void setFinyearbinding(RichInputComboboxListOfValues finyearbinding) {
        this.finyearbinding = finyearbinding;
    }

    public RichInputComboboxListOfValues getFinyearbinding() {
        return finyearbinding;
    }

    public void setEmployeeNoBinding(RichInputText employeeNoBinding) {
        this.employeeNoBinding = employeeNoBinding;
    }

    public RichInputText getEmployeeNoBinding() {
        return employeeNoBinding;
    }

    public void setDesigCdBinding(RichInputText desigCdBinding) {
        this.desigCdBinding = desigCdBinding;
    }

    public RichInputText getDesigCdBinding() {
        return desigCdBinding;
    }

    public void setDeptcdbinding(RichInputText deptcdbinding) {
        this.deptcdbinding = deptcdbinding;
    }

    public RichInputText getDeptcdbinding() {
        return deptcdbinding;
    }

    public void setConfirmBinding(RichInputDate confirmBinding) {
        this.confirmBinding = confirmBinding;
    }

    public RichInputDate getConfirmBinding() {
        return confirmBinding;
    }
}
