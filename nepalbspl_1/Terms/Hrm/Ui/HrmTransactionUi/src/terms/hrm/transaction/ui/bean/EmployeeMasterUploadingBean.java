package terms.hrm.transaction.ui.bean;

import java.io.BufferedReader;
import java.io.IOException;

import java.io.InputStream;

import java.io.InputStreamReader;

import java.util.ArrayList;
//import java.util.Date;

import java.util.StringTokenizer;

import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.util.ResetUtils;

import oracle.binding.OperationBinding;
import oracle.jbo.domain.Date;

import oracle.jbo.Row;

import oracle.jbo.RowSetIterator;

import org.apache.myfaces.trinidad.model.UploadedFile;

import terms.hrm.transaction.model.view.EmployeeMasterUploadingVORowImpl;



public class EmployeeMasterUploadingBean {
    private RichTable employeeMasterUploadTableBind;
    private String message = "C";
    private ArrayList<String> list = new ArrayList<String>();
    private RichPanelHeader myRootComponent;

    public EmployeeMasterUploadingBean() {
    }

    public void FileUpload(ValueChangeEvent vce) {
        System.out.println("in file upload vce" + vce.getNewValue() + " new:" + vce.getOldValue());

        UploadedFile file = (UploadedFile) vce.getNewValue();
        //        setFile(file);
        System.out.println(" in file upload vce Content Type:" + file.getContentType() + " file name:" +
                           file.getFilename() + " Size:" + file.getLength() + " file:" + file);
        System.out.println(file.getFilename().substring(file.getFilename().lastIndexOf('.') + 1));

        //
        //        System.out.println(" in file upload vce getFile Content Type:"+getFile().getContentType()+" file name:"+getFile().getFilename()+" Size:"
        //                           +getFile().getLength()+" file:"+getFile());


        try {
            if (file.getFilename().substring(file.getFilename().lastIndexOf('.') + 1).equalsIgnoreCase("csv")) {
                System.out.println("CSV File Format catched");
                parseFile(file.getInputStream());
                ResetUtils.reset(vce.getComponent());
            } else {
                FacesMessage msg = new FacesMessage("File format not supported.-- Upload CSV file");
                msg.setSeverity(FacesMessage.SEVERITY_WARN);
                FacesContext.getCurrentInstance().addMessage(null, msg);
                ResetUtils.reset(vce.getComponent());
            }
            AdfFacesContext.getCurrentInstance().addPartialTarget(employeeMasterUploadTableBind);
            displayErrors();
        } catch (IOException e) {
            // TODO
        }
    }

    public void setEmployeeMasterUploadTableBind(RichTable employeeMasterUploadTableBind) {
        this.employeeMasterUploadTableBind = employeeMasterUploadTableBind;
    }

    public RichTable getEmployeeMasterUploadTableBind() {
        return employeeMasterUploadTableBind;
    }

    private void parseFile(InputStream file) {
        list.clear();
        // getPopUpBinding().hide();
        BufferedReader reader = new BufferedReader(new InputStreamReader(file));
        String strLine = "";
        StringTokenizer st = null;
        int lineNumber = 0, tokenNumber = 0, flag = 0;
        EmployeeMasterUploadingVORowImpl rw = null;

        cleardata();
        DCIteratorBinding dci = ADFUtils.findIterator("EmployeeMasterUploadingVO1Iterator");
        RowSetIterator rsi = dci.getViewObject().createRowSetIterator(null);
        try {

            while ((strLine = reader.readLine()) != null) {
                lineNumber++;
                if (lineNumber > 1) {
                    Row last = rsi.last();
                    int i = rsi.getRangeIndexOf(last);
                    rw = (EmployeeMasterUploadingVORowImpl) rsi.createRow();
                    rw.setNewRowState(Row.STATUS_INITIALIZED);
                    rsi.insertRowAtRangeIndex(i + 1, rw);
                    rsi.setCurrentRow(rw);
                    flag = 1;
                }
                //handling null values
                Pattern p=Pattern.compile("<br />");
                strLine=strLine.replaceAll("<br />", "");
              strLine=strLine.replaceAll(",,", ",--,");
                System.out.println("PRINTING::::");
                System.out.println(strLine);
                

                st = new StringTokenizer(strLine, ",");
                while (st.hasMoreTokens()) {
                    tokenNumber++;

                    String theToken = st.nextToken();
                    System.out.println("Line # " + lineNumber + ", Token # " + tokenNumber + ", Token : " + theToken +
                                       " flag:" + flag + " tokenNumber:" + tokenNumber);
                    if (lineNumber > 1) {
                        //       if (!validatedata(theToken) && flag == 1) {
                        //                            System.out.println(" !validatedata(theToken): " + theToken);
                        //                            list.add(theToken);
                        //                            rw.remove();
                        //                            break;
                        //                        }

                        switch (tokenNumber) {
                        case 1:
                            System.out.println("Inside case 1 " + tokenNumber);
                            rw.setAttribute("EmpNumber", theToken);
                            flag = 0;
                            break;
                        case 2:
                            System.out.println("Inside case 2");
                            rw.setAttribute("NationCode", theToken);
                            flag = 0;
                            break;
                        case 3:
                            Date d = ADFUtils.convertStringToJboDate(theToken);
                            System.out.println("Join Date: " + theToken);
                            System.out.println("Join Date JBO: " + d);
                            rw.setAttribute("JoinDate", d);
                            flag = 0;
                            break;

                            //                            System.out.println("Inside case 3");
                            //                                rw.setAttribute("JoinDate", theToken);
                            //                                flag = 0;
                            //                                break;
                        case 4:
                            System.out.println("Inside case 4");
                            rw.setAttribute("EmpLastName", theToken);
                            flag = 0;
                            break;
                        case 5:
                            System.out.println("Inside case 5");
                            rw.setAttribute("EmpFirstName", theToken);
                            flag = 0;
                            break;
                        case 6:
                            Date dob = ADFUtils.convertStringToJboDate(theToken);
                            System.out.println("DOB Date: " + theToken);
                            System.out.println("DOB Date JBO: " + dob);
                            rw.setAttribute("DateOfBirth", dob);
                            //                            System.out.println("Inside case 6");
                            //                                rw.setAttribute("DateOfBirth", theToken);
                            flag = 0;
                            break;
                        case 7:
                            System.out.println("Inside case 7");
                            rw.setAttribute("Sex", theToken);
                            flag = 0;
                            break;
                        case 8:
                            System.out.println("Inside case 8");
                            rw.setAttribute("FatherName", theToken);
                            flag = 0;
                            break;
//                        case 9:
//                            System.out.println("Inside case 9");
//                            rw.setAttribute("PresentAdd1", theToken);
//                            flag = 0;
//                            break;
                        case 9:
                            System.out.println("Inside case 9");
                            rw.setAttribute("PresentAdd1", theToken.replace(',', ' '));
                            flag = 0;
                            break;
                        case 10:
                            System.out.println("Inside case 10");
                            rw.setAttribute("PresentAdd2", theToken);
                            flag = 0;
                            break;
                        case 11:
                            System.out.println("Inside case 11");
                            rw.setAttribute("PresentCity", theToken);
                            flag = 0;
                            break;
                        case 12:
                            System.out.println("Inside case 12");
                            rw.setAttribute("PresentPhone", theToken);
                            flag = 0;
                            break;
                        case 13:
                            System.out.println("Inside case 13");
                            rw.setAttribute("PermanentAdd1", theToken);
                            flag = 0;
                            break;
                        case 14:
                            System.out.println("Inside case 14");
                            rw.setAttribute("PermanentAdd2", theToken);
                            flag = 0;
                            break;
                        case 15:
                            System.out.println("Inside case 15");
                            rw.setAttribute("PermanentCity", theToken);
                            flag = 0;
                            break;
                        case 16:
                            System.out.println("Inside case 16");
                            rw.setAttribute("PermanentPhone", theToken);
                            flag = 0;
                            break;
                        case 17:
                            System.out.println("Inside case 17");
                            rw.setAttribute("MaritalStatus", theToken);
                            flag = 0;
                            break;
                        case 18:
                            System.out.println("Inside case 18");
                            rw.setAttribute("BloodGroup", theToken);
                            flag = 0;
                            break;
                        case 19:
                            Date dom = ADFUtils.convertStringToJboDate(theToken);
                            System.out.println("DOM Date: " + theToken);
                            System.out.println("DOM Date JBO: " + dom);
                            rw.setAttribute("DateOfMarriage", dom);
                            //                            System.out.println("Inside case 19");
                            //                                rw.setAttribute("DateOfMarriage", theToken);
                            flag = 0;
                            break;
                        case 20:
                            System.out.println("Inside case 20");
                            rw.setAttribute("SpouseName", theToken);
                            flag = 0;
                            break;
                        case 21:
                            Date cd = ADFUtils.convertStringToJboDate(theToken);
                            System.out.println("DOM Date: " + theToken);
                            System.out.println("DOM Date JBO: " + cd);
                            rw.setAttribute("ConfirmDate", cd);
                            //                            System.out.println("Inside case 21");
                            //                               rw.setAttribute("ConfirmDate", theToken);
                            flag = 0;
                            break;
                        case 22:
                            System.out.println("Inside case 22");
                            rw.setAttribute("NoticePeriodDays", theToken);
                            flag = 0;
                            break;
                        case 23:
                            System.out.println("Inside case 23");
                            rw.setAttribute("CompanyEmail", theToken);
                            flag = 0;
                            break;
                        case 24:
                            System.out.println("Inside case 24");
                            rw.setAttribute("PerEmailid", theToken);
                            flag = 0;
                            break;
                        case 25:
                            System.out.println("Inside case 25");
                            rw.setAttribute("Unit", theToken);
                            flag = 0;
                            break;
                        case 26:
                            System.out.println("Inside case 26");
                            rw.setAttribute("PermanentState", theToken);
                            flag = 0;
                            break;
                        case 27:
                            System.out.println("Inside case 27");
                            rw.setAttribute("PresentState", theToken);
                            flag = 0;
                            break;
                        case 28:
                            System.out.println("Inside case 28");
                            rw.setAttribute("Religion", theToken);
                            flag = 0;
                            break;
                        case 29:
                            System.out.println("Inside case 29");
                            rw.setAttribute("EsiStatus", theToken);
                            flag = 0;
                            break;
                        case 30:
                            System.out.println("Inside case 30");
                            rw.setAttribute("BonusYesNo", theToken);
                            flag = 0;
                            break;
                        case 31:
                            System.out.println("Inside case 31");
                            rw.setAttribute("PermanentMob", theToken);
                            flag = 0;
                            break;
                        case 32:
                            System.out.println("Inside case 32");
                            rw.setAttribute("PresentMob", theToken);
                            flag = 0;
                            break;
                        case 33:
                            System.out.println("Inside case 33");
                            rw.setAttribute("CardNo", theToken);
                            flag = 0;
                            break;
                        case 34:
                            System.out.println("Inside case 34");
                            rw.setAttribute("PfStatus", theToken);
                            flag = 0;
                            break;
                        case 35:
                            System.out.println("Inside case 35");
                            rw.setAttribute("MotherName", theToken);
                            flag = 0;
                            break;
                        case 36:
                            System.out.println("Inside case 36");
                            rw.setAttribute("OtherName", theToken);
                            flag = 0;
                            break;
                        case 37:
                            System.out.println("Inside case 37");
                            rw.setAttribute("RCFlag", theToken);
                            flag = 0;
                            break;
                        case 38:
                            Date reg = ADFUtils.convertStringToJboDate(theToken);
                            System.out.println("Reg Date: " + theToken);
                            System.out.println("Reg Date JBO: " + reg);
                            rw.setAttribute("ResigDate", reg);
                            //                            System.out.println("Inside case 38");
                            //                              rw.setAttribute("ResigDate", theToken);
                            flag = 0;
                            break;
                        case 39:
                            System.out.println("Inside case 39");
                            rw.setAttribute("Shift", theToken);
                            flag = 0;
                            break;
                        case 40:
                            System.out.println("Inside case 40");
                            rw.setAttribute("Fpf", theToken);
                            flag = 0;
                            break;
                        case 41:
                            System.out.println("Inside case 41");
                            rw.setAttribute("UanNo", theToken);
                            flag = 0;
                            break;
                        case 42:
                            System.out.println("Inside case 42");
                            rw.setAttribute("AdhaarNo", theToken);
                            flag = 0;
                            break;
                        case 43:
                            Date rert = ADFUtils.convertStringToJboDate(theToken);
                            System.out.println("RetirementDate: " + theToken);
                            System.out.println("RetirementDate JBO: " + rert);
                            rw.setAttribute("RetirementDate", rert);
                            //                            System.out.println("Inside case 43");
                            //                                rw.setAttribute("RetirementDate", theToken);
                            flag = 0;
                            break;
                        case 44:
                            System.out.println("Inside case 44");
                            rw.setAttribute("AttType", theToken);
                            flag = 0;
                            break;
                        case 45:
                            System.out.println("Inside case 45");
                            rw.setAttribute("DesigCode", theToken);
                            flag = 0;
                            break;
                        case 46:
                            System.out.println("Inside case 46");
                            rw.setAttribute("DesigLvlMastLvlMastLevel", theToken);
                            flag = 0;
                            break;
                        case 47:
                            System.out.println("Inside case 47");
                            rw.setAttribute("AreaMastCode", theToken);
                            flag = 0;
                            break;
                        case 48:
                            System.out.println("Inside case 48");
                            rw.setAttribute("AreaMastDepartmentCode", theToken);
                            flag = 0;
                            break;
                        case 49:
                            Date pos = ADFUtils.convertStringToJboDate(theToken);
                            System.out.println("Pos Date: " + theToken);
                            System.out.println("PostTillDate Date JBO: " + pos);
                            rw.setAttribute("PostTillDate", pos);
                            //                            System.out.println("Inside case 49");
                            //                                rw.setAttribute("PostTillDate", theToken);
                            flag = 0;
                            break;
                        case 50:
                            System.out.println("Inside case 50");
                            rw.setAttribute("ReportingTo", theToken);
                            flag = 0;
                            break;
                        case 51:
                            System.out.println("Inside case 51");
                            rw.setAttribute("Status", theToken);
                            flag = 0;
                            break;
                        case 52:
                            System.out.println("Inside case 52");
                            rw.setAttribute("ModeOfPayment", theToken);
                            flag = 0;
                            break;
                        case 53:
                            System.out.println("Inside case 53");
                            rw.setAttribute("Category", theToken);
                            flag = 0;
                            break;
                        case 54:
                            System.out.println("Inside case 54");
                            rw.setAttribute("BankmastCode", theToken);
                            flag = 0;
                            break;
                        case 55:
                            System.out.println("Inside case 55");
                            rw.setAttribute("PfNumber", theToken);
                            flag = 0;
                            break;
                        case 56:
                            Date pf = ADFUtils.convertStringToJboDate(theToken);
                            System.out.println("PfDate: " + theToken);
                            System.out.println("PfDate JBO: " + pf);
                            rw.setAttribute("PfDate", pf);
                            //                            System.out.println("Inside case 56");
                            //                                rw.setAttribute("PfDate", theToken);
                            flag = 0;
                            break;
                        case 57:
                            System.out.println("Inside case 57");
                            rw.setAttribute("EsiNumber", theToken);
                            flag = 0;
                            break;
                        case 58:
                            Date esi = ADFUtils.convertStringToJboDate(theToken);
                            System.out.println("EsiDate: " + theToken);
                            System.out.println("EsiDate JBO: " + esi);
                            rw.setAttribute("EsiDate", esi);
                            //                            System.out.println("Inside case 58");
                            //                               rw.setAttribute("EsiDate", theToken);
                            flag = 0;
                            break;
                        case 59:
                            System.out.println("Inside case 59");
                            rw.setAttribute("BankAccNumber", theToken);
                            flag = 0;
                            break;
                        case 60:
                            System.out.println("Inside case 60");
                            rw.setAttribute("PanNo", theToken);
                            flag = 0;
                            break;
                        case 61:
                            System.out.println("Inside case 61");
                            rw.setAttribute("OtStatus", theToken);
                            flag = 0;
                            break;
                        case 62:
                            System.out.println("Inside case 62");
                            rw.setAttribute("SkillCode", theToken);
                            flag = 0;
                            break;
                        case 63:
                            System.out.println("Inside case 63");
                            rw.setAttribute("SectionCode", theToken);
                            flag = 0;
                            break;
                        }
                    }
                }
                //reset token number
                tokenNumber = 0;
            }
            rsi.closeRowSetIterator();
        } catch (IOException e) {
            e.printStackTrace();
            ADFUtils.showMessage("Content error in uploaded file", 0);
            rsi.closeRowSetIterator();
        } catch (Exception e) {
            e.printStackTrace();
            ADFUtils.showMessage("Data error in uploaded file", 0);
            rsi.closeRowSetIterator();
        }
    }

    public void displayErrors() {
        for (String obj : list) {
            System.out.println(obj);
            ADFUtils.showMessage("Invalid Employee Number:" + obj, 0);
        }
    }

    public void cleardata() {
        DCIteratorBinding dci = ADFUtils.findIterator("EmployeeMasterUploadingVO1Iterator");
        Row[] r = dci.getAllRowsInRange();
        System.out.println("ITERATOR RANGE:" + r.length);
        if (r.length > 0) {
            RowSetIterator rsi1 = dci.getViewObject().createRowSetIterator(null);
            while (rsi1.hasNext()) {
                Row r1 = rsi1.next();
                r1.remove();
            }
            AdfFacesContext.getCurrentInstance().addPartialTarget(employeeMasterUploadTableBind);
            rsi1.closeRowSetIterator();
        }
    }

 
    public void saveAL(ActionEvent actionEvent) {
        System.out.println("Inside Save AL");
//        oracle.binding.OperationBinding op3= ADFUtils.findOperation("clearIBE");
//        op3.execute();
        OperationBinding opr = ADFUtils.findOperation("Commit");
        Object obj = opr.execute();
        if (message.equalsIgnoreCase("C")) {
            System.out.println("S####");
            FacesMessage Message = new FacesMessage("Record Uploaded & Saved Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
        }
        System.out.println("Calling clear!");
        oracle.binding.OperationBinding op3= ADFUtils.findOperation("clearIBE");
           op3.execute();
           System.out.println("Clear Done!");
       
    }

    public void setMyRootComponent(RichPanelHeader myRootComponent) {
        this.myRootComponent = myRootComponent;
    }

    public RichPanelHeader getMyRootComponent() {
        return myRootComponent;
    }
}
