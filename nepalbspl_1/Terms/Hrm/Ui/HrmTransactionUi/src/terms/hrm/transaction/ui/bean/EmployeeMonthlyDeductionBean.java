package terms.hrm.transaction.ui.bean;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectBooleanCheckbox;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichPanelLabelAndMessage;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputFormatted;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.util.ResetUtils;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;
import oracle.jbo.domain.Timestamp;

import org.apache.myfaces.trinidad.model.UploadedFile;

import terms.hrm.transaction.model.view.EmployeeMonthlyDetailVORowImpl;
import terms.hrm.transaction.model.view.EmployeeMonthlyEarningDetailVORowImpl;

public class EmployeeMonthlyDeductionBean {
    private RichInputComboboxListOfValues bindUnit;
    private RichInputText bindEntryNo;
    private RichSelectOneChoice bindMonth;
    private RichInputText bindYear;
    private RichSelectOneChoice bindFreeze;
    private RichInputText bindSubCode;
    private RichInputText bindSubCode1;
    private RichInputText bindSalaryDays;
    private RichInputText bindFinYear;
    private RichInputText amountBinding;
    private RichInputText empNameBinding;
    private RichInputText unitNameBinding;
    private RichOutputText bindDid;
    private RichTable employeeMonthlyDetailTableBinding;
    private RichInputComboboxListOfValues bindSalCode;
    private RichInputText descriptionBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichButton detailcreateBinding;
    private RichButton detaildeleteBinding;
    private RichOutputFormatted bindingOutputText;
    private String Message = "C";
    private RichInputComboboxListOfValues empCodeBinding;
    private RichButton populateButtonBinding;
    private RichInputText approvedByBinding;
    private RichInputText approvedDateBinding;
    private RichSelectBooleanCheckbox synchronizeTrans2Binding;
    private RichInputComboboxListOfValues approvedBinding;
    BigDecimal Amount = new BigDecimal(0);
    private RichInputText freezeBinding;
    private ArrayList<String> list = new ArrayList<String>();
    private UploadedFile file;
    private RichPopup filePopupBindingDed;
    private RichPopup popUpBinding;

    public EmployeeMonthlyDeductionBean() {
    }

    public void populateEmpMonDedAL(ActionEvent actionEvent) {
        OperationBinding op = ADFUtils.findOperation("filterValueForEmpMonthlyDedc");
        Object obj = op.execute();
        System.out.println("ob.getResult()" + op.getResult());
        if (op.getResult() != null && op.getResult().equals("Y")) {
            System.out.println("In Action Listener");
            System.out.println("Unit Code" + bindUnit.getValue());
            System.out.println("Unit Code 2 " + bindUnit.getValue());
            System.out.println("Month" + bindMonth.getValue());
            System.out.println("Year" + bindYear.getValue());
            if (bindUnit.getValue() != null && bindMonth.getValue() != null && bindYear.getValue() != null) {
                System.out.println("In if Action Listener");
                String Unit = bindUnit.getValue() == null ? "" : bindUnit.getValue().toString();
                String Year = bindYear.getValue() == null ? "" : bindYear.getValue().toString();
                String Month = bindMonth.getValue() == null ? "" : bindMonth.getValue().toString();
                String SalCode = bindSalCode.getValue() == null ? "" : bindSalCode.getValue().toString();
                //                                    String SubCode = bindSubCode.getValue()==null ? "" : bindSubCode.getValue().toString();
                //                                    String SubCode1 = bindSubCode1.getValue()==null ? "" : bindSubCode1.getValue().toString();
                //                                    String Freeze = bindFreeze.getValue()==null ? "" : bindFreeze.getValue().toString();
                //                                    String FinYear = bindFinYear.getValue()==null ? "" : bindFinYear.getValue().toString();
                //                                    String SalaryDays = bindSalaryDays.getValue()==null ? "" : bindSalaryDays.getValue().toString();
                String synchronizeTrans2 =
                    synchronizeTrans2Binding.getValue() == null ? "" : synchronizeTrans2Binding.getValue().toString();
                // String EntryNo = bindEntryNo.getValue()==null ? "" : bindEntryNo.getValue().toString();
                System.out.println("############ Employee Monthly Deduction Data ----> " + Unit + " " + Year + " " +
                                   Month + " " + SalCode + " " + synchronizeTrans2);
                //Passing  Data into HrmTrancationAMImpl file
                OperationBinding ob = ADFUtils.findOperation("populateEmpMonDedAM2");
                ob.getParamsMap().put("Unit", Unit);
                ob.getParamsMap().put("Year", Year);
                ob.getParamsMap().put("Month", Month);
                ob.getParamsMap().put("SlCode", SalCode);
                ob.getParamsMap().put("synchronizeTrans2", synchronizeTrans2.equals(true) ? "Y" : "N");
                //ob.getParamsMap().put("EntryBy", 232);
                //                                        ob.getParamsMap().put("SubCode", SubCode);
                //                                        ob.getParamsMap().put("SubCode1", SubCode1);
                //                                        ob.getParamsMap().put("Freeze", Freeze);
                //                                        ob.getParamsMap().put("FinYear", FinYear);
                //                                        ob.getParamsMap().put("SalaryDays", SalaryDays);
                //    ob.getParamsMap().put("EntryNo", EntryNo);
                ob.execute();
            } else {
                ADFUtils.showMessage("Month and Year should be selected.", 2);
            }
        } else {
            detailcreateBinding.setDisabled(false);
        }
        bindSalCode.setDisabled(true);

    }

    public void setBindUnit(RichInputComboboxListOfValues bindUnit) {
        this.bindUnit = bindUnit;
    }

    public RichInputComboboxListOfValues getBindUnit() {
        return bindUnit;
    }

    public void setBindEntryNo(RichInputText bindEntryNo) {
        this.bindEntryNo = bindEntryNo;
    }

    public RichInputText getBindEntryNo() {
        return bindEntryNo;
    }

    public void setBindMonth(RichSelectOneChoice bindMonth) {
        this.bindMonth = bindMonth;
    }

    public RichSelectOneChoice getBindMonth() {
        return bindMonth;
    }

    public void setBindYear(RichInputText bindYear) {
        this.bindYear = bindYear;
    }

    public RichInputText getBindYear() {
        return bindYear;
    }

    public void setBindFreeze(RichSelectOneChoice bindFreeze) {
        this.bindFreeze = bindFreeze;
    }

    public RichSelectOneChoice getBindFreeze() {
        return bindFreeze;
    }

    public void setBindSubCode(RichInputText bindSubCode) {
        this.bindSubCode = bindSubCode;
    }

    public RichInputText getBindSubCode() {
        return bindSubCode;
    }

    public void setBindSubCode1(RichInputText bindSubCode1) {
        this.bindSubCode1 = bindSubCode1;
    }

    public RichInputText getBindSubCode1() {
        return bindSubCode1;
    }

    public void setBindSalaryDays(RichInputText bindSalaryDays) {
        this.bindSalaryDays = bindSalaryDays;
    }

    public RichInputText getBindSalaryDays() {
        return bindSalaryDays;
    }

    public void setBindFinYear(RichInputText bindFinYear) {
        this.bindFinYear = bindFinYear;
    }

    public RichInputText getBindFinYear() {
        return bindFinYear;
    }

    public void setAmountBinding(RichInputText amountBinding) {
        this.amountBinding = amountBinding;
    }

    public RichInputText getAmountBinding() {
        return amountBinding;
    }

    public void setEmpNameBinding(RichInputText empNameBinding) {
        this.empNameBinding = empNameBinding;
    }

    public RichInputText getEmpNameBinding() {
        return empNameBinding;
    }

    public void setUnitNameBinding(RichInputText unitNameBinding) {
        this.unitNameBinding = unitNameBinding;
    }

    public RichInputText getUnitNameBinding() {
        return unitNameBinding;
    }

    public void setBindDid(RichOutputText bindDid) {
        this.bindDid = bindDid;
    }

    public RichOutputText getBindDid() {
        return bindDid;
    }

    public void setEmployeeMonthlyDetailTableBinding(RichTable employeeMonthlyDetailTableBinding) {
        this.employeeMonthlyDetailTableBinding = employeeMonthlyDetailTableBinding;
    }

    public RichTable getEmployeeMonthlyDetailTableBinding() {
        return employeeMonthlyDetailTableBinding;
    }

    public void setBindSalCode(RichInputComboboxListOfValues bindSalCode) {
        this.bindSalCode = bindSalCode;
    }

    public RichInputComboboxListOfValues getBindSalCode() {
        return bindSalCode;
    }

    public void setDescriptionBinding(RichInputText descriptionBinding) {
        this.descriptionBinding = descriptionBinding;
    }

    public RichInputText getDescriptionBinding() {
        return descriptionBinding;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setDetailcreateBinding(RichButton detailcreateBinding) {
        this.detailcreateBinding = detailcreateBinding;
    }

    public RichButton getDetailcreateBinding() {
        return detailcreateBinding;
    }

    public void setDetaildeleteBinding(RichButton detaildeleteBinding) {
        this.detaildeleteBinding = detaildeleteBinding;
    }

    public RichButton getDetaildeleteBinding() {
        return detaildeleteBinding;
    }

    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }
        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }
    //Set Fields and Button disable.
    public void cevModeDisableComponent(String mode) {
        //            FacesContext fctx = FacesContext.getCurrentInstance();
        //            ELContext elctx = fctx.getELContext();
        //            Application jsfApp = fctx.getApplication();
        //            //create a ValueExpression that points to the ADF binding layer
        //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
        //            //
        //            ValueExpression valueExpr = exprFactory.createValueExpression(
        //                                         elctx,
        //                                         "#{pageFlowScope.mode=='E'}",
        //                                          Object.class
        //                                         );
        //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
        if (mode.equals("E")) {
            Message = "E";
            getHeaderEditBinding().setDisabled(true);
            getDetailcreateBinding().setDisabled(true);
            getDetaildeleteBinding().setDisabled(true);
            getBindSubCode().setDisabled(true);
            getBindSubCode1().setDisabled(true);
            getBindFreeze().setDisabled(true);
            getBindUnit().setDisabled(true);
            getEmpCodeBinding().setDisabled(true);
            //getAmountBinding().setDisabled(true);
            getEmpNameBinding().setDisabled(true);
            getUnitNameBinding().setDisabled(true);
            getDescriptionBinding().setDisabled(true);
            getUnitNameBinding().setDisabled(true);
            getDescriptionBinding().setDisabled(true);
            getEmpNameBinding().setDisabled(true);
            getBindEntryNo().setDisabled(true);
            getPopulateButtonBinding().setDisabled(true);
            bindUnit.setDisabled(true);
            approvedDateBinding.setDisabled(true);
            bindMonth.setDisabled(true);
            bindYear.setDisabled(true);
            bindSalCode.setDisabled(true);
        } else if (mode.equals("C")) {

            getHeaderEditBinding().setDisabled(true);
            // getDetaildeleteBinding().setDisabled(false);
            getBindEntryNo().setDisabled(true);

            getBindSubCode().setDisabled(true);
            getBindSubCode1().setDisabled(true);
            getBindFreeze().setDisabled(true);
            // getBindUnit().setDisabled(true);
            getEmpNameBinding().setDisabled(true);
            getUnitNameBinding().setDisabled(true);
            getDescriptionBinding().setDisabled(true);


            //getUnitNameBinding().setDisabled(true);
            getDescriptionBinding().setDisabled(true);
            getEmpNameBinding().setDisabled(true);
            //                getPopulateButtonBinding().setDisabled(true);
            //                getDetailcreateBinding().setDisabled(true);
            bindUnit.setDisabled(true);
            approvedBinding.setDisabled(true);
            approvedDateBinding.setDisabled(true);
            detailcreateBinding.setDisabled(true);

        } else if (mode.equals("V")) {
            getDetailcreateBinding().setDisabled(true);
            getDetaildeleteBinding().setDisabled(true);
            //                getAmountBinding().setDisabled(true);
            //                getEmpCodeBinding().setDisabled(true);
        }

    }

    public void editButtonAL(ActionEvent actionEvent) {
        if (approvedBinding.getValue() != null) {
            OperationBinding op = (OperationBinding) ADFUtils.findOperation("freezeAuthority");
            op.getParamsMap().put("formName", "F_DEDC.FMX");
            op.getParamsMap().put("freeze", freezeBinding.getValue());
            op.execute();
            if (op.getResult() != null && op.getResult().equals("Y")) {
                ADFUtils.showMessage("Freezed Data cannot be edited further.", 2);
                ADFUtils.setEL("#{pageFlowScope.mode}", "V");
            } else {
                cevmodecheck();
            }
        } else {
            cevmodecheck();
        }
    }

    public void setBindingOutputText(RichOutputFormatted bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputFormatted getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }

        AdfFacesContext.getCurrentInstance().addPartialTarget(employeeMonthlyDetailTableBinding);
    }

    public void saveButtonAL(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedBy("EmployeeMonthlyHeaderVO1Iterator","LastUpdatedBy");
        System.out.println("Mode Message" + Message);

        if (Message.equalsIgnoreCase("C") && bindEntryNo.getValue() == null) {
            OperationBinding op = ADFUtils.findOperation("getEntryNumber");
            Object rst = op.execute();

            System.out.println("Mode Message 1" + Message);
            ADFUtils.findOperation("Commit").execute();
            System.out.println("Mode Message 2" + Message);
            System.out.println("rst " + rst);
            ADFUtils.showMessage("Record Saved Successfully. New Entry Number is " + rst + ".", 2);
            System.out.println("rst 2 -->>> " + rst);

            //                    ADFUtils.findOperation("Commit").execute();
            //                    ADFUtils.showMessage("Record Saved Successfully. New Entry Number is "+bindEntryNo.getValue()+".", 2);
        } else {
            System.out.println("Mode Message 2" + Message);
            ADFUtils.findOperation("Commit").execute();
            ADFUtils.showMessage("Record Updated Successfully.", 2);
        }
        //        if(Message.equalsIgnoreCase("C"))
        //               {
        //            if(bindEntryNo.getValue()==null){
        //                   OperationBinding op = ADFUtils.findOperation("getEntryNumber");
        //                   Object rst = op.execute();
        //
        //                   System.out.println("Mode Message 1"+Message);
        //                   ADFUtils.findOperation("Commit").execute();
        //                   ADFUtils.showMessage("Record Saved Successfully. New Entry Number is "+rst+".", 2);
        //            }
        //            else if (bindEntryNo.getValue()!=null)
        //            {
        //                    ADFUtils.findOperation("Commit").execute();
        //                    ADFUtils.showMessage("Record Saved Successfully. New Entry Number is "+bindEntryNo.getValue()+".", 2);
        //                }
        //               }
        //               else if(Message.equalsIgnoreCase("E"))
        //               {
        //                   System.out.println("Mode Message 2"+Message);
        //                   ADFUtils.findOperation("Commit").execute();
        //                   ADFUtils.showMessage("Record Updated Successfully.", 2);
        //               }


        //        FacesMessage Message = new FacesMessage("Record Save And Update Successfully");
        //               Message.setSeverity(FacesMessage.SEVERITY_INFO);
        //               FacesContext fc = FacesContext.getCurrentInstance();
        //               fc.addMessage(null, Message);
        //        ADFUtils.findOperation("getEntryNumber").execute();
        //        ADFUtils.findOperation("Commit").execute();
    }

    public void AmountVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce.getNewValue() != null) {
            Amount = (BigDecimal) vce.getNewValue();

            //        System.out.println("Amount Value in bean"+vce.getNewValue());
            //        OperationBinding ob=ADFUtils.findOperation("getAmountValue");
            //        ob.getParamsMap().put("amount", vce.getNewValue());
            //        ob.execute();
            //        System.out.println("In Bean AmountVCL 2");
        }
    }

    public void setEmpCodeBinding(RichInputComboboxListOfValues empCodeBinding) {
        this.empCodeBinding = empCodeBinding;
    }

    public RichInputComboboxListOfValues getEmpCodeBinding() {
        return empCodeBinding;
    }

    public void yearVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        //        OperationBinding op = ADFUtils.findOperation("filterValueForEmpMonthlyDedc");
        //        Object rst = op.execute();
        OperationBinding ob = ADFUtils.findOperation("filterValueForEmpMonthlyDedc");
        Object obj = ob.execute();
        System.out.println("ob.getResult()" + ob.getResult());

    }

    public void setPopulateButtonBinding(RichButton populateButtonBinding) {
        this.populateButtonBinding = populateButtonBinding;
    }

    public RichButton getPopulateButtonBinding() {
        return populateButtonBinding;
    }

    public void setApprovedByBinding(RichInputText approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputText getApprovedByBinding() {
        return approvedByBinding;
    }

    public void setApprovedDateBinding(RichInputText approvedDateBinding) {
        this.approvedDateBinding = approvedDateBinding;
    }

    public RichInputText getApprovedDateBinding() {
        return approvedDateBinding;
    }

    public void approvedByVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce.getNewValue() != null) {
            vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
            OperationBinding op = (OperationBinding) ADFUtils.findOperation("checkApprovalAuthority");
            op.getParamsMap().put("unitCd", getBindUnit().getValue().toString());
            op.getParamsMap().put("empCode", vce.getNewValue());
            op.getParamsMap().put("authLevel", "AP");
            op.getParamsMap().put("formName", "EMD");
            op.execute();
            if (op.getResult() != null && op.getResult().equals("N")) {
                Row r = (Row) ADFUtils.evaluateEL("#{bindings.EmployeeMonthlyHeaderVO1Iterator.currentRow}");
                r.setAttribute("ApprovedBy", null);
                r.setAttribute("ApprovedDate", null);
                bindFreeze.setValue("N");
                FacesMessage Message =
                    new FacesMessage("Sorry You Are Not Authorized To Approve this Employee Monthly Deduction.");
                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            } else {
                Timestamp dt = new Timestamp(System.currentTimeMillis());
                approvedDateBinding.setValue(dt);
                bindFreeze.setValue("Y");
            }

        }
    }

    public void setSynchronizeTrans2Binding(RichSelectBooleanCheckbox synchronizeTrans2Binding) {
        this.synchronizeTrans2Binding = synchronizeTrans2Binding;
    }

    public RichSelectBooleanCheckbox getSynchronizeTrans2Binding() {
        return synchronizeTrans2Binding;
    }

    public void setApprovedBinding(RichInputComboboxListOfValues approvedBinding) {
        this.approvedBinding = approvedBinding;
    }

    public RichInputComboboxListOfValues getApprovedBinding() {
        return approvedBinding;
    }

    public void updateAmtDtlsVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce.getNewValue() != null) {
            System.out.println("In Bean AmountVCL 2" + Amount + "Check Box==>" + vce.getNewValue());
            OperationBinding ob = ADFUtils.findOperation("getAmountValue");
            ob.getParamsMap().put("amount", Amount);
            ob.getParamsMap().put("check", vce.getNewValue());
            ob.execute();
        }
    }

    public void dtlCreateButtonAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert").execute();
        Row r = (Row) ADFUtils.resolveExpression("#{bindings.EmployeeMonthlyDetailVO1Iterator.currentRow}");
        r.setAttribute("Month", bindMonth.getValue());
        r.setAttribute("Year", bindYear.getValue());
        r.setAttribute("SalCode", bindSalCode.getValue());
        r.setAttribute("UnitCd", bindUnit.getValue());
    }

    public void setFreezeBinding(RichInputText freezeBinding) {
        this.freezeBinding = freezeBinding;
    }

    public RichInputText getFreezeBinding() {
        return freezeBinding;
    }

    public void fileuploadVCL(ValueChangeEvent vce) {
        System.out.println("in file upload vce" + vce.getNewValue() + " new:" + vce.getOldValue());

        UploadedFile file = (UploadedFile) vce.getNewValue();
        //        setFile(file);
        System.out.println(" in file upload vce Content Type:" + file.getContentType() + " file name:" +
                           file.getFilename() + " Size:" + file.getLength() + " file:" + file);
        System.out.println(file.getFilename().substring(file.getFilename().lastIndexOf('.') + 1));

        //
        //        System.out.println(" in file upload vce getFile Content Type:"+getFile().getContentType()+" file name:"+getFile().getFilename()+" Size:"
        //                           +getFile().getLength()+" file:"+getFile());


        try {
            if (file.getFilename().substring(file.getFilename().lastIndexOf('.') + 1).equalsIgnoreCase("csv")) {
                System.out.println("CSV File Format catched");
                parseFile(file.getInputStream());
                ResetUtils.reset(vce.getComponent());
            } else {
                FacesMessage msg = new FacesMessage("File format not supported.-- Upload CSV file");
                msg.setSeverity(FacesMessage.SEVERITY_WARN);
                FacesContext.getCurrentInstance().addMessage(null, msg);
                ResetUtils.reset(vce.getComponent());
            }
            AdfFacesContext.getCurrentInstance().addPartialTarget(employeeMonthlyDetailTableBinding);
            displayErrors();
        } catch (IOException e) {
            // TODO
        }

    }

    public void cleardata() {
        DCIteratorBinding dci = ADFUtils.findIterator("EmployeeMonthlyDetailVO1Iterator");
        Row[] r = dci.getAllRowsInRange();
        System.out.println("ITERATOR RANGE:" + r.length);
        if (r.length > 0) {
            RowSetIterator rsi1 = dci.getViewObject().createRowSetIterator(null);
            while (rsi1.hasNext()) {
                Row r1 = rsi1.next();
                r1.remove();
            }
            AdfFacesContext.getCurrentInstance().addPartialTarget(employeeMonthlyDetailTableBinding);
            rsi1.closeRowSetIterator();
        }
    }

    private void parseFile(InputStream file) {
        list.clear();
       // getPopUpBinding().hide();
        BufferedReader reader = new BufferedReader(new InputStreamReader(file));
        String strLine = "";
        StringTokenizer st = null;
        int lineNumber = 0, tokenNumber = 0, flag = 0;
        EmployeeMonthlyDetailVORowImpl rw = null;

        cleardata();
        DCIteratorBinding dci = ADFUtils.findIterator("EmployeeMonthlyDetailVO1Iterator");
        RowSetIterator rsi = dci.getViewObject().createRowSetIterator(null);
        try {

            while ((strLine = reader.readLine()) != null) {
                lineNumber++;
                if (lineNumber > 1) {
                    Row last = rsi.last();
                    int i = rsi.getRangeIndexOf(last);
                    rw = (EmployeeMonthlyDetailVORowImpl) rsi.createRow();
                    rw.setNewRowState(Row.STATUS_INITIALIZED);
                    rsi.insertRowAtRangeIndex(i + 1, rw);
                    rsi.setCurrentRow(rw);
                    flag = 1;
                }

                st = new StringTokenizer(strLine, ",");
                while (st.hasMoreTokens()) {
                    tokenNumber++;

                    String theToken = st.nextToken();
                    System.out.println("Line # " + lineNumber + ", Token # " + tokenNumber + ", Token : " + theToken +
                                       " flag:" + flag + " tokenNumber:" + tokenNumber);
                    if (lineNumber > 1) {
                        if (!validatedata(theToken) && flag == 1) {
                            System.out.println(" !validatedata(theToken): " + theToken);
                            list.add(theToken);
                            rw.remove();
                            break;
                        }

                        switch (tokenNumber) {
                        case 1:
                            System.out.println("validate emp:" + theToken);
                            rw.setAttribute("EmpNo", theToken);
                            rw.setAttribute("UnitCd", bindUnit.getValue());
                            rw.setAttribute("Year", bindYear.getValue());
                            rw.setAttribute("Month", bindMonth.getValue());
                            rw.setAttribute("SalCode", bindSalCode.getValue());
                            flag = 0;
                            break;
                        case 2:
                            rw.setAttribute("Amount", theToken);
                            flag = 0;
                            break;
                        }
                    }
                }
                //reset token number
                tokenNumber = 0;
            }
            rsi.closeRowSetIterator();
        } catch (IOException e) {
            e.printStackTrace();
            ADFUtils.showMessage("Content error in uploaded file", 0);
            rsi.closeRowSetIterator();
        } catch (Exception e) {
            e.printStackTrace();
            ADFUtils.showMessage("Data error in uploaded file", 0);
            rsi.closeRowSetIterator();
        }
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public UploadedFile getFile() {
        return file;
    }

    public void displayErrors() {
        for (String obj : list) {
            System.out.println(obj);
            ADFUtils.showMessage("Invalid Employee Number:" + obj, 0);
        }
    }

    public boolean validatedata(String emp) {
        OperationBinding ob = ADFUtils.findOperation("validatedataForEmpMonthlyDeduction");
        ob.getParamsMap().put("empcd", emp);
        ob.execute();
        System.out.println("ob.getResult() in bean:" + ob.getResult());
        if (ob.getResult() != null) {
            boolean result = Boolean.parseBoolean(ob.getResult().toString());
            System.out.println("result:" + result);
            if (result) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

//    public void setFilePopupBinding(RichPopup filePopupBinding) {
//        this.filePopupBindingDed = filePopupBindingDed;
//    }
//
//    public RichPopup getFilePopupBinding() {
//        return filePopupBindingDed;
//    }

    public void setList(ArrayList<String> list) {
        this.list = list;
    }

    public ArrayList<String> getList() {
        return list;
    }
//
//    public void setFilePopupBindingDed(RichPopup filePopupBindingDed) {
//        this.filePopupBindingDed = filePopupBindingDed;
//    }
//
//    public RichPopup getFilePopupBindingDed() {
//        return filePopupBindingDed;
//    }
    public void setPopUpBinding(RichPopup popUpBinding) {
        this.popUpBinding = popUpBinding;
    }

    public RichPopup getPopUpBinding() {
        return popUpBinding;
    }
}

