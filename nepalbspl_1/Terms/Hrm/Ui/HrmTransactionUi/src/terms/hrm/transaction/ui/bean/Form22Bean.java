package terms.hrm.transaction.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;

import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;

import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.binding.OperationBinding;

import oracle.jbo.domain.Timestamp;
import oracle.jbo.format.DefaultDateFormatter;
import oracle.jbo.format.FormatErrorException;

public class Form22Bean {
    private RichInputComboboxListOfValues getbindUnitCd;
    private RichPanelHeader getMyPageRootComponent;
    private RichInputText entryNoBinding;
    private RichInputDate getFromDtBinding;
    private RichInputDate todtbinding;
    private RichInputDate todatenewbind;
    private RichInputText nameofoccupierbinding;
    private RichInputDate getEntrydtbinding;
    private RichInputText getmencontractor;
    private RichInputText adoifrmaleoccupierbinding;
    private RichInputText adoifrmalecontractorbinding;
    private RichInputText adoimalecontractorbinding;
    private RichInputText adoimaleoccupierbinding;
    private RichOutputText outputTextBinding;
    private RichInputText childmalelabourbinding;
    private RichInputText childfemaleoccupierbinding;
    private RichInputText childmalecontractorbinding;
    private RichInputText childfemalecontractorbinding;
    private RichInputText womenoccupierbinding;
    private RichInputText menoccupier;
    private RichInputText womencontractor;


    public Form22Bean() {
    }

    public void getOccupierNameBean(ValueChangeEvent vce) {

    }

    public void setGetbindUnitCd(RichInputComboboxListOfValues getbindUnitCd) {
        this.getbindUnitCd = getbindUnitCd;
    }

    public RichInputComboboxListOfValues getGetbindUnitCd() {
        return getbindUnitCd;
    }

    public void EntryDateVCE(ValueChangeEvent vce) {
        System.out.println("Occupier Name");
        if(vce.getNewValue()!=null){
        OperationBinding binding = ADFUtils.findOperation("Form21");
        binding.getParamsMap().put("PASS_UNIT",getbindUnitCd.getValue());

        binding.execute();
        }
    }

    public void SaveAL(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedBy("FormNo22VO1Iterator","LastUpdatedBy");
        System.out.println("Committt");
       OperationBinding op=ADFUtils.findOperation("getEntryNo1");
        op.execute();
        if(entryNoBinding.getValue()!=null){
            ADFUtils.findOperation("Commit").execute();
            ADFUtils.showMessage("Record has been Saved Succesfully"+ ADFUtils.findOperation("getEntryNo1").execute(), 2);
        }
        else{
            ADFUtils.findOperation("Commit").execute();
            ADFUtils.showMessage("Record has been Updated Succesfully", 2);
        
    }
    }
    public void EditAL(ActionEvent actionEvent) {
        cevmodecheck();
    }
    private void cevmodecheck(){
           if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
               cevModeDisableComponent("V");
           }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
               cevModeDisableComponent("C");
           }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
               cevModeDisableComponent("E");
           }
       }
    
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                try {
                    Method method1 =
                        component.getClass().getMethod("setDisabled", boolean.class);
                    if (method1 != null) {
                        method1.invoke(component, valueComponent);
                    }
                } catch (NoSuchMethodException e) {
                    try {
                        Method method =
                            component.getClass().getMethod("setReadOnly", boolean.class);
                        if (method != null) {
                            method.invoke(component, valueComponent);
                        }
                    } catch (Exception e1) {
                        // e.printStackTrace();//silently eat this exception.
                    }


                } catch (Exception e) {
                    // e.printStackTrace();//silently eat this exception.
                }
                List<UIComponent> childComponents = component.getChildren();
                for (UIComponent comp : childComponents) {
                    makeComponentHierarchyReadOnly(comp, valueComponent);
                }
            }
    public void cevModeDisableComponent(String mode) {
    //            FacesContext fctx = FacesContext.getCurrentInstance();
    //            ELContext elctx = fctx.getELContext();
    //            Application jsfApp = fctx.getApplication();
    //            //create a ValueExpression that points to the ADF binding layer
    //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
    //            //
    //            ValueExpression valueExpr = exprFactory.createValueExpression(
    //                                         elctx,
    //                                         "#{pageFlowScope.mode=='E'}",
    //                                          Object.class
    //                                         );
    //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
        if (mode.equals("E")) {
         getNameofoccupierbinding().setDisabled(true);
            getEntryNoBinding().setDisabled(true);
    getEntrydtbinding.setDisabled(true);
    getmencontractor.setDisabled(true);
    getAdoifrmalecontractorbinding().setDisabled(true);
    getAdoifrmaleoccupierbinding().setDisabled(true );
    getAdoimalecontractorbinding().setDisabled(true);
    getAdoimaleoccupierbinding().setDisabled(true);
            getChildfemalecontractorbinding().setDisabled(true);
            getChildfemaleoccupierbinding().setDisabled(true);
            getChildmalecontractorbinding().setDisabled(true);
            getChildmalelabourbinding().setDisabled(true);
            getbindUnitCd.setDisabled(true);
            getWomencontractor().setDisabled(true);
            getWomenoccupierbinding().setDisabled(true);
            getMenoccupier().setDisabled(true);
             
        } else if (mode.equals("C")) {
            getEntryNoBinding().setDisabled(true);
            getAdoifrmalecontractorbinding().setDisabled(true);
            getAdoifrmaleoccupierbinding().setDisabled(true );
            getAdoimalecontractorbinding().setDisabled(true);
            getAdoimaleoccupierbinding().setDisabled(true);
            getChildfemalecontractorbinding().setDisabled(true);
            getChildfemaleoccupierbinding().setDisabled(true);
            getChildmalecontractorbinding().setDisabled(true);
            getChildmalelabourbinding().setDisabled(true);
            getbindUnitCd.setDisabled(true);
            getWomencontractor().setDisabled(true);
            getWomenoccupierbinding().setDisabled(true);
            getMenoccupier().setDisabled(true);
           
        } else if (mode.equals("V")) {
            getNameofoccupierbinding().setDisabled(true);
               getEntryNoBinding().setDisabled(true);
            getEntrydtbinding.setDisabled(true);
            getmencontractor.setDisabled(true);
          
                
        }
        
    }


    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setEntryNoBinding(RichInputText entryNoBinding) {
        this.entryNoBinding = entryNoBinding;
    }

    public RichInputText getEntryNoBinding() {
        return entryNoBinding;
    }

    public void setGetFromDtBinding(RichInputDate getFromDtBinding) {
        this.getFromDtBinding = getFromDtBinding;
    }

    public RichInputDate getGetFromDtBinding() {
        return getFromDtBinding;
    }

    public void FromDateVce(ValueChangeEvent vce) {
        if(vce!=null){
            
            int monthstoadd=6;
            String date1=(String)vce.getNewValue();
//            String date2=(String)todtbinding.getValue();
            LocalDate ldc=LocalDate.parse(date1);
            LocalDate toDt=ldc.now().plusMonths(monthstoadd);
            System.out.println("New Dayssss"+toDt);
            SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
            Calendar cal=Calendar.getInstance();
            try{
            cal.setTime(sdf.parse(date1));
            }catch(ParseException e)
            {
                e.printStackTrace();
            }
            cal.add(Calendar.MONTH,monthstoadd);
            String newDate=sdf.format(cal.getTime());
        System.out.println("New Date=======>"+newDate);
              //  Date PaymentDate=new Date(cal.getTime().getTime());
                //System.out.println("TimesStamp########==> "+PaymentDate);
              //  todtbinding.setValue(PaymentDate);   
//                 String time_jbo = (String)getFromDtBinding.getValue();
//                 System.out.println("time_jbo"+time_jbo);
//                  String time_str =time_jbo.toString();
//                  System.out.println("time_str"+time_str);
//                  time_str=time_str.substring(0, 10);
//                System.out.println("time_str"+time_str);
////                  Date dtt=new Date(time_str);
//                 System.out.println("date"+dtt);
//                  DateTimeFormatter pattern1 = DateTimeFormatter.ofPattern("dd/MM/yyyy");
//            // String date =null;
            //
           // String date=null;
//            try {
//                    DefaultDateFormatter dff = new DefaultDateFormatter();
//                    date = dff.format("dd/MM/yyyy", dtt);
//                
//                    System.out.println("valuee of date isss=" + date);
//                } catch (FormatErrorException fee) {
//                    // TODO: Add catch code
//                    fee.printStackTrace();
//                }
                
//                    try {
//                        SimpleDateFormat dateF = new SimpleDateFormat("dd/MM/yyyy");
//                    java.util.Date dt = dateF.parse(date);
//                     System.out.println("*****************DATE IS*********"+dt);
//        //                
//                    } catch (ParseException pe) {
//                        pe.printStackTrace();
//                    }
//        //
//                Calendar cal=Calendar.getInstance();
//             
//                
//                            String newDate=sdf.format(cal.getTime());
//                java.util.Date jDate;
//            try {
//                jDate = (java.util.Date) sdf.parse(newDate);
//                java.util.Date util_date = new java.util.Date(date);
//                    System.out.println("date"+util_date);
//                java.sql.Date sDate = new java.sql.Date(jDate.getTime());
//                oracle.jbo.domain.Date oDate=new oracle.jbo.domain.Date(sDate);
//            } catch (ParseException e) {
//            }
//            cal.add(Calendar.MONTH,monthstoadd);
//                    
//                        System.out.println("valuee of date isss=" + date);
//                
//                    
//           
//           // Calendar cal=Calendar.getInstance();
//            cal.setTimeInMillis(util_date.getTime());
//           
//            Timestamp todt=new Timestamp(cal.getTime().getTime());
//            
//            getTodtbinding().setValue(todt);
//      
           
        }
    }

    public void setTodtbinding(RichInputDate todtbinding) {
        this.todtbinding = todtbinding;
    }

    public RichInputDate getTodtbinding() {
        return todtbinding;
    }

   public void frmdtvce(ValueChangeEvent vce) {
//    int monthstoadd=6;
//    Date Frmdt=(Date)vce.getNewValue();
//    Date date1=(Date)Frmdt;
//    Date Todt=(Date)todtbinding.getValue();
//    Date date2=(Date)Todt;
//    Calendar cal= Calendar.getInstance();
//    Date todt=new Date(cal.getTime().getTime());
//    getTodtbinding().setValue(todt);
//    
        
        
        
        
        //------------------
        int monthstoadd=6;
        String date1=(String)vce.getNewValue();
        //            String date2=(String)todtbinding.getValue();
        LocalDate ldc=LocalDate.parse(date1);
        LocalDate toDt=ldc.now().plusMonths(monthstoadd);
        System.out.println("New Dayssss"+toDt);
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal=Calendar.getInstance();
        try{
        cal.setTime(sdf.parse(date1));
        }catch(ParseException e)
        {
            e.printStackTrace();
        }
        cal.add(Calendar.MONTH,monthstoadd);
        String newDate=sdf.format(cal.getTime());
        System.out.println("New Date=======>"+newDate);
       getTodatenewbind().setValue(newDate);
//    }
}

    public void setTodatenewbind(RichInputDate todatenewbind) {
        this.todatenewbind = todatenewbind;
    }

    public RichInputDate getTodatenewbind() {
        return todatenewbind;
    }

    public void setNameofoccupierbinding(RichInputText nameofoccupierbinding) {
        this.nameofoccupierbinding = nameofoccupierbinding;
    }

    public RichInputText getNameofoccupierbinding() {
        return nameofoccupierbinding;
    }

    public void setGetEntrydtbinding(RichInputDate getEntrydtbinding) {
        this.getEntrydtbinding = getEntrydtbinding;
    }

    public RichInputDate getGetEntrydtbinding() {
        return getEntrydtbinding;
    }

    public void setGetmencontractor(RichInputText getmencontractor) {
        this.getmencontractor = getmencontractor;
    }

    public RichInputText getGetmencontractor() {
        return getmencontractor;
    }

    public void setAdoifrmaleoccupierbinding(RichInputText adoifrmaleoccupierbinding) {
        this.adoifrmaleoccupierbinding = adoifrmaleoccupierbinding;
    }

    public RichInputText getAdoifrmaleoccupierbinding() {
        return adoifrmaleoccupierbinding;
    }

    public void setAdoifrmalecontractorbinding(RichInputText adoifrmalecontractorbinding) {
        this.adoifrmalecontractorbinding = adoifrmalecontractorbinding;
    }

    public RichInputText getAdoifrmalecontractorbinding() {
        return adoifrmalecontractorbinding;
    }

    public void setAdoimalecontractorbinding(RichInputText adoimalecontractorbinding) {
        this.adoimalecontractorbinding = adoimalecontractorbinding;
    }

    public RichInputText getAdoimalecontractorbinding() {
        return adoimalecontractorbinding;
    }

    public void setAdoimaleoccupierbinding(RichInputText adoimaleoccupierbinding) {
        this.adoimaleoccupierbinding = adoimaleoccupierbinding;
    }

    public RichInputText getAdoimaleoccupierbinding() {
        return adoimaleoccupierbinding;
    }

    public void setOutputTextBinding(RichOutputText outputTextBinding) {
        this.outputTextBinding = outputTextBinding;
    }

    public RichOutputText getOutputTextBinding() {
        cevmodecheck();
        return outputTextBinding;
    }

    public void setChildmalelabourbinding(RichInputText childmalelabourbinding) {
        this.childmalelabourbinding = childmalelabourbinding;
    }

    public RichInputText getChildmalelabourbinding() {
        return childmalelabourbinding;
    }

    public void setChildfemaleoccupierbinding(RichInputText childfemaleoccupierbinding) {
        this.childfemaleoccupierbinding = childfemaleoccupierbinding;
    }

    public RichInputText getChildfemaleoccupierbinding() {
        return childfemaleoccupierbinding;
    }

    public void setChildmalecontractorbinding(RichInputText childmalecontractorbinding) {
        this.childmalecontractorbinding = childmalecontractorbinding;
    }

    public RichInputText getChildmalecontractorbinding() {
        return childmalecontractorbinding;
    }

    public void setChildfemalecontractorbinding(RichInputText childfemalecontractorbinding) {
        this.childfemalecontractorbinding = childfemalecontractorbinding;
    }

    public RichInputText getChildfemalecontractorbinding() {
        return childfemalecontractorbinding;
    }

    public void setWomenoccupierbinding(RichInputText womenoccupierbinding) {
        this.womenoccupierbinding = womenoccupierbinding;
    }

    public RichInputText getWomenoccupierbinding() {
        return womenoccupierbinding;
    }

    public void setMenoccupier(RichInputText menoccupier) {
        this.menoccupier = menoccupier;
    }

    public RichInputText getMenoccupier() {
        return menoccupier;
    }

    public void setWomencontractor(RichInputText womencontractor) {
        this.womencontractor = womencontractor;
    }

    public RichInputText getWomencontractor() {
        return womencontractor;
    }
}
