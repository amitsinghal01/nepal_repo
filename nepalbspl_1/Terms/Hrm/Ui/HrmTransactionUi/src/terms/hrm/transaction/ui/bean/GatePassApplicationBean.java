package terms.hrm.transaction.ui.bean;

import java.lang.reflect.Method;


import java.math.BigDecimal;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import javax.faces.validator.ValidatorException;

import oracle.adf.controller.activity.ActivityLogicException;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;

import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;

import oracle.adf.view.rich.component.rich.layout.RichPanelLabelAndMessage;

import oracle.adf.view.rich.component.rich.nav.RichButton;

import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.binding.OperationBinding;

import oracle.jbo.domain.Timestamp;

import oracle.jbo.server.ViewObjectImpl;

import oracle.net.aso.e;

public class GatePassApplicationBean {
    private RichInputComboboxListOfValues unitCdBinding;
    private RichInputDate gatepassDateBinding;
    private RichInputDate outTimeBinding;
    private RichInputText shiftCdBinding;
    private RichInputText gtpassHourBinding;
    private RichInputComboboxListOfValues empnoBinding;
    private RichInputText gtOutReadBinding;
    private RichInputText gtInReadBinding;
    private RichInputText remarksBinding;
    private RichInputText gpOdNoBinding;
    private RichInputDate startTimeBinding;
    private RichInputText placeOfVisitBinding;
    private RichInputDate endTimeBinding;
    private RichInputDate inTimeBinding;
    private RichInputText reasonOutReasonCdBinding;
    private RichPanelHeader getMyPageRootComponent;
    private String message="C";
    private RichButton headerEditBinding;
    private RichOutputText bindingOutputText;
    private RichSelectOneChoice gatePassTakenOnBindin;
    private RichSelectOneChoice purposeBinding;
    private RichSelectOneChoice gpOdStatusBinding;
    private RichInputComboboxListOfValues approvedByBinding;
    private RichInputDate approvedDateBinding;
    private RichInputComboboxListOfValues shiftCodeBinding;
    private RichInputText outReadingBinding;

    public GatePassApplicationBean() {
    }
    public void setUnitCdBinding(RichInputComboboxListOfValues unitCdBinding) {
        this.unitCdBinding = unitCdBinding;
    }
    public RichInputComboboxListOfValues getUnitCdBinding() {
        return unitCdBinding;
    }
    public void setGatepassDateBinding(RichInputDate gatepassDateBinding) {
        this.gatepassDateBinding = gatepassDateBinding;
    }
    public RichInputDate getGatepassDateBinding() {
        return gatepassDateBinding;
    }
    public void setOutTimeBinding(RichInputDate outTimeBinding) {
        this.outTimeBinding = outTimeBinding;
    }
    public RichInputDate getOutTimeBinding() {
        return outTimeBinding;
    }
    public void setShiftCdBinding(RichInputText shiftCdBinding) {
        this.shiftCdBinding = shiftCdBinding;
    }
    public RichInputText getShiftCdBinding() {
        return shiftCdBinding;
    }
    public void setGtpassHourBinding(RichInputText gtpassHourBinding) {
        this.gtpassHourBinding = gtpassHourBinding;
    }
    public RichInputText getGtpassHourBinding() {
        return gtpassHourBinding;
    }

    public void setEmpnoBinding(RichInputComboboxListOfValues empnoBinding) {
        this.empnoBinding = empnoBinding;
    }
    public RichInputComboboxListOfValues getEmpnoBinding() {
        return empnoBinding;
    }
    public void setGtOutReadBinding(RichInputText gtOutReadBinding) {
        this.gtOutReadBinding = gtOutReadBinding;
    }
    public RichInputText getGtOutReadBinding() {
        return gtOutReadBinding;
    }
    public void setGtInReadBinding(RichInputText gtInReadBinding) {
        this.gtInReadBinding = gtInReadBinding;
    }
    public RichInputText getGtInReadBinding() {
        return gtInReadBinding;
    }
    public void setRemarksBinding(RichInputText remarksBinding) {
        this.remarksBinding = remarksBinding;
    }
    public RichInputText getRemarksBinding() {
        return remarksBinding;
    }
    public void setGpOdNoBinding(RichInputText gpOdNoBinding) {
        this.gpOdNoBinding = gpOdNoBinding;
    }
    public RichInputText getGpOdNoBinding() {
        return gpOdNoBinding;
    }
    public void setStartTimeBinding(RichInputDate startTimeBinding) {
        this.startTimeBinding = startTimeBinding;
    }
    public RichInputDate getStartTimeBinding() {
        return startTimeBinding;        
    }
    public void setPlaceOfVisitBinding(RichInputText placeOfVisitBinding) {
        this.placeOfVisitBinding = placeOfVisitBinding;
    }
    public RichInputText getPlaceOfVisitBinding() {
        return placeOfVisitBinding;
    }

    public void setEndTimeBinding(RichInputDate endTimeBinding) {
        this.endTimeBinding = endTimeBinding;
    }
    public RichInputDate getEndTimeBinding() {
        return endTimeBinding;
    }
    public void setInTimeBinding(RichInputDate inTimeBinding) {
        this.inTimeBinding = inTimeBinding;
    }

    public RichInputDate getInTimeBinding() {
        return inTimeBinding;
    }
    public void setReasonOutReasonCdBinding(RichInputText reasonOutReasonCdBinding) {
        this.reasonOutReasonCdBinding = reasonOutReasonCdBinding;
    }
    public RichInputText getReasonOutReasonCdBinding() {
        return reasonOutReasonCdBinding;
    }
    public void saveButtonAL(ActionEvent actionEvent) {
            ADFUtils.setLastUpdatedBy("GatePassApplicationVO1Iterator","LastUpdatedBy");
        System.out.println("In SaveAl");
        OperationBinding op= ADFUtils.findOperation("generategtpassno");
        op.execute(); 
        System.out.println("In SaveAl After Call method");
        if (message.equalsIgnoreCase("C")){
            OperationBinding op2= ADFUtils.findOperation("Commit"); 
            System.out.println("In SaveAl create");
            op2.execute();
        FacesMessage Message = new FacesMessage("Record Save Successfully!");
        Message.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext fc=FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        }
        else{
                OperationBinding op3= ADFUtils.findOperation("Commit"); 
                System.out.println("In SaveAl edit");
                op3.execute();
            FacesMessage Message = new FacesMessage("Record Updated Successfully!");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc=FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
            }
        }
    private void cevmodecheck(){
               if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
                   makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
                   cevModeDisableComponent("V");
               }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
                   makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                   cevModeDisableComponent("C");
               }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
                   makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                   cevModeDisableComponent("E");
               }
           } 
        public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                    try {
                        Method method1 =
                            component.getClass().getMethod("setDisabled", boolean.class);
                        if (method1 != null) {
                            method1.invoke(component, valueComponent);
                        }
                    } catch (NoSuchMethodException e) {
                        try {
                            Method method =
                                component.getClass().getMethod("setReadOnly", boolean.class);
                            if (method != null) {
                                method.invoke(component, valueComponent);
                            }
                        } catch (Exception e1) {
                            // e.printStackTrace();//silently eat this exception.
                        }


                        } catch (Exception e) {
                        // e.printStackTrace();//silently eat this exception.
                    }
                    List<UIComponent> childComponents = component.getChildren();
                    for (UIComponent comp : childComponents) {
                        makeComponentHierarchyReadOnly(comp, valueComponent);
                    }
                }
                //Set Fields and Button disable.
            public void cevModeDisableComponent(String mode) {
                if (mode.equals("E")) {
                   getUnitCdBinding().setDisabled(true);
                   getGpOdNoBinding().setDisabled(true);
                   getApprovedDateBinding().setDisabled(true);
                   getApprovedByBinding().setDisabled(true);
                   getShiftCodeBinding().setDisabled(true);
                   getStartTimeBinding().setDisabled(true);
                   getEndTimeBinding().setDisabled(true);
                   getEmpnoBinding().setDisabled(true);
                   

                        message="E";
                } else if (mode.equals("C")) {
                    getUnitCdBinding().setDisabled(true);
                    getApprovedByBinding().setDisabled(true);
                    getGpOdNoBinding().setDisabled(true);
                    getApprovedDateBinding().setDisabled(true);
                    getShiftCodeBinding().setDisabled(true);
                    getStartTimeBinding().setDisabled(true);
                    getEndTimeBinding().setDisabled(true);                
                                    
      
                } else if (mode.equals("V")) {  
                    
                }
            }
    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }
    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }
    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }
    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }
    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }
    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }
    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGatePassTakenOnBindin(RichSelectOneChoice gatePassTakenOnBindin) {
        this.gatePassTakenOnBindin = gatePassTakenOnBindin;
    }

    public RichSelectOneChoice getGatePassTakenOnBindin() {
        return gatePassTakenOnBindin;
    }

    public void setPurposeBinding(RichSelectOneChoice purposeBinding) {
        this.purposeBinding = purposeBinding;
    }

    public RichSelectOneChoice getPurposeBinding() {
        return purposeBinding;
    }
    public void setGpOdStatusBinding(RichSelectOneChoice gpOdStatusBinding) {
        this.gpOdStatusBinding = gpOdStatusBinding;
    }

    public RichSelectOneChoice getGpOdStatusBinding() {
        return gpOdStatusBinding;
    }

    public void outTimeVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce.getNewValue()!=null)
        {
        System.out.println("Out time is 1----->> "+vce.getNewValue());
        String unitCd=(String) getUnitCdBinding().getValue();
        System.out.println("Unit Value is-----.."+unitCd);
        String empCode=(String) getEmpnoBinding().getValue();
        System.out.println("Emp Value is-----.."+empCode);
        Timestamp outtime= (Timestamp)vce.getNewValue();
        System.out.println("Out time is 2 ----->> "+outtime); 
        outtime.getTime();
        System.out.println("Out time is 2 ----->> "+outtime);
        OperationBinding op=ADFUtils.findOperation("getshiftValue");
        op.getParamsMap().put("unitCd", unitCd);
        op.getParamsMap().put("empCode", empCode);
        op.getParamsMap().put("outtime", outtime);
        op.execute();
        
        oracle.jbo.domain.Timestamp intime=(oracle.jbo.domain.Timestamp) getInTimeBinding().getValue();
        System.out.println("intime-----.."+intime);
        oracle.jbo.domain.Timestamp outtime1=(oracle.jbo.domain.Timestamp) getOutTimeBinding().getValue();
        System.out.println("outtime-----.."+outtime1);
        
        if(intime != null && outtime1 !=null){
            System.out.println("In if Condition");
        OperationBinding op1 = (OperationBinding) ADFUtils.findOperation("setgtpasshour");
        //        op.getParamsMap().put("intime",intime);
        //        op.getParamsMap().put("outtime",outtime );
        op1.execute(); 
        }
        }
    }

    public void approvedByVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        System.out.println("In Bean");
        //String userName = (String) ADFContext.getCurrent().getPageFlowScope().get("userName");
        String userName = "E-09";
                                if(vce!=null)
                                {
                                    System.out.println("In Bean in if");
                                 vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
                                 OperationBinding op = (OperationBinding) ADFUtils.findOperation("approvedByForGatePassApplication");
                                 op.getParamsMap().put("unitCd", unitCdBinding.getValue().toString());
                                 op.getParamsMap().put("empCode", userName);
                                 op.getParamsMap().put("authLevel", "AP");
                                 op.getParamsMap().put("formName", "GPA");
                                 op.execute();
                                 
                                    OperationBinding op2 = (OperationBinding) ADFUtils.findOperation("approvedByDateValue");
                                    op2.execute();
                                }                               
    }
    public void inTimeVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        vce.getNewValue();
        System.out.println("in timeeeeee>>>>>----->> "+vce.getNewValue());
        oracle.jbo.domain.Timestamp intime=(oracle.jbo.domain.Timestamp) getInTimeBinding().getValue();
        System.out.println("intime-----.."+intime);
        oracle.jbo.domain.Timestamp outtime=(oracle.jbo.domain.Timestamp) getOutTimeBinding().getValue();
        System.out.println("outtime-----.."+outtime);
        
        if(intime != null && outtime !=null){
            System.out.println("In if Condition");
        OperationBinding op = (OperationBinding) ADFUtils.findOperation("setgtpasshour");
//        op.getParamsMap().put("intime",intime);
//        op.getParamsMap().put("outtime",outtime );
        op.execute(); 
        }
    }

    public void setApprovedByBinding(RichInputComboboxListOfValues approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputComboboxListOfValues getApprovedByBinding() {
        return approvedByBinding;
    }

    public void setApprovedDateBinding(RichInputDate approvedDateBinding) {
        this.approvedDateBinding = approvedDateBinding;
    }

    public RichInputDate getApprovedDateBinding() {
        return approvedDateBinding;
    }

    public void setShiftCodeBinding(RichInputComboboxListOfValues shiftCodeBinding) {
        this.shiftCodeBinding = shiftCodeBinding;
    }

    public RichInputComboboxListOfValues getShiftCodeBinding() {
        return shiftCodeBinding;
    }

    public void empShiftCdVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce.getNewValue()!=null)
        {
            ADFUtils.findOperation("shiftCodeEmpAssigned").execute();
        }
    }

    public void inTimeValidator(FacesContext facesContext, UIComponent uIComponent, Object inDate) {
//        if(inDate!=null && outTimeBinding.getValue()!=null)
//        {
//        oracle.jbo.domain.Timestamp outDate=(oracle.jbo.domain.Timestamp) outTimeBinding.getValue();
//        oracle.jbo.domain.Timestamp inDateT= (oracle.jbo.domain.Timestamp)inDate;
//        if(inDateT.compareTo(outDate)==-1 || inDateT.compareTo(outDate)==0)
//        {
//            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,"In Date & Time must be greater than Out Date & Time.",null));
//        }
//        }
    }

    public void inReadingValidator(FacesContext facesContext, UIComponent uIComponent, Object inReading) {
        if(inReading!=null && outReadingBinding.getValue()!=null)
        {
            BigDecimal inRead=new BigDecimal(inReading.toString());
            BigDecimal outRead=new BigDecimal(outReadingBinding.getValue().toString());
            if(inRead.compareTo(outRead)==-1)
            {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,"Gate In Reading must be greater than Gate Out Reading.",null));
            }
        }
    }

    public void setOutReadingBinding(RichInputText outReadingBinding) {
        this.outReadingBinding = outReadingBinding;
    }

    public RichInputText getOutReadingBinding() {
        return outReadingBinding;
    }

    public void gateOutValidator(FacesContext facesContext, UIComponent uIComponent, Object outReading) {
        if(outReading!=null && gtInReadBinding.getValue()!=null)
        {
            BigDecimal inRead=new BigDecimal(gtInReadBinding.getValue().toString());
            BigDecimal outRead=new BigDecimal(outReading.toString());
            if(outRead.compareTo(inRead)==1)
            {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,"Gate Out Reading must be less than Gate In Reading.",null));
            }
        }

    }
}
