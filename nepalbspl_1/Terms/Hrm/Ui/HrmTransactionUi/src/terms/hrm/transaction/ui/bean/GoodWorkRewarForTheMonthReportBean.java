package terms.hrm.transaction.ui.bean;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;

import java.sql.Connection;

import java.sql.SQLException;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.faces.context.FacesContext;

import javax.naming.Context;
import javax.naming.InitialContext;

import javax.servlet.http.HttpServletResponse;

import javax.sql.DataSource;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

import oracle.adf.model.BindingContext;

import oracle.adf.model.binding.DCIteratorBinding;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

public class GoodWorkRewarForTheMonthReportBean {
    private String pMode="";
       String file_name="";
       
    public GoodWorkRewarForTheMonthReportBean() {
    }
    
    public BindingContainer getBindings()
            {
              return BindingContext.getCurrent().getCurrentBindingsEntry();
            }

    public void downloadjrxml(FacesContext facesContext, OutputStream outputStream) {
        Connection conn=null;
        try {
            
            System.out.println(" pmode:"+pMode);
            
            
            Map n = new HashMap();
            if(pMode != null){
              if(pMode.equalsIgnoreCase("A")){
                 file_name ="r_gwr_for_month.jasper";
                DCIteratorBinding paySlip = (DCIteratorBinding) getBindings().get("GoodWorkRewarForMonthReportVVO1Iterator");
                n.put("p_unit", paySlip.getCurrentRow().getAttribute("UnitCode").toString());
                n.put("p_month", paySlip.getCurrentRow().getAttribute("Month").toString());
                n.put("p_year", paySlip.getCurrentRow().getAttribute("Year").toString());
                
                System.out.println(paySlip.getCurrentRow().getAttribute("UnitCode").toString());
                System.out.println(paySlip.getCurrentRow().getAttribute("Month").toString());
                System.out.println(paySlip.getCurrentRow().getAttribute("Year").toString());
                
            }
        //              else if(pMode.equalsIgnoreCase("B")){
        //                 file_name ="r_salary_slip_staff.jasper";
        //                DCIteratorBinding paySlip = (DCIteratorBinding) getBindings().get("SalarySlipStaffReportVVO1Iterator");
        //                n.put("p_unit", paySlip.getCurrentRow().getAttribute("UnitCode").toString());
        //                n.put("p_month", paySlip.getCurrentRow().getAttribute("Month").toString());
        //                n.put("p_year", paySlip.getCurrentRow().getAttribute("Year").toString());
        //            }
            }
            
            OperationBinding binding = ADFUtils.findOperation("fileNameForPrint");
            binding.getParamsMap().put("fileId","ERP0000000120");
            binding.execute();
            System.out.println("Binding Result :"+binding.getResult());
            if(binding.getResult() != null){
                String result = binding.getResult().toString();
                                  int last_index = result.lastIndexOf("/");
                if(last_index==-1)
                {
                    last_index=result.lastIndexOf("\\");
                }
                                                      String path = result.substring(0,last_index+1)+file_name;
                                                      System.out.println("FILE PATH IS===>"+path);
                                  InputStream input = new FileInputStream(path);
            //    InputStream input = new FileInputStream("//home//beta3//Jasper//Jour_prn1.jrxml");

                conn = getConnection( );
                
                JasperReport design = (JasperReport) JRLoader.loadObject(input);
                System.out.println("Path : " + input + " -------" + design+" param:"+n);
                
                @SuppressWarnings("unchecked")
                net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                byte pdf[] = JasperExportManager.exportReportToPdf(print);
                HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                response.getOutputStream().write(pdf);
                response.getOutputStream().flush();
                response.getOutputStream().close();
                facesContext.responseComplete();
                }else
                System.out.println("File Name/File Path not found.");
                } catch (FileNotFoundException fnfe) {
                // TODO: Add catch code
                fnfe.printStackTrace();
                try {
                    System.out.println("in finally connection closed");
                            conn.close( );
                            conn = null;
                    
                } catch( SQLException e ) {
                    e.printStackTrace( );
                }
                } catch (Exception e) {
                // TODO: Add catch code
                e.printStackTrace();
                try {
                    System.out.println("in finally connection closed");
                            conn.close( );
                            conn = null;
                    
                } catch( SQLException e1 ) {
                    e1.printStackTrace( );
                }
                }finally {
                        try {
                                System.out.println("in finally connection closed");
                                        conn.close( );
                                        conn = null;
                                
                        } catch( SQLException e ) {
                                e.printStackTrace( );
                        }
                }
    }
    
    public void setPMode(String pMode) {
        this.pMode = pMode;
    }

    public String getPMode() {
        return pMode;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    public String getFile_name() {
        return file_name;
    }
    
    public Connection getConnection() throws Exception{
            Context ctx = null;
            Connection conn = null;
            ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/ApplicationDBDS");
            conn = ds.getConnection();
            return conn;
        }
}
