package terms.hrm.transaction.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;

public class IncomeTaxComputationBean {
    private RichInputText outputTextBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichInputText transactionIdBinding;
    private RichInputComboboxListOfValues empCdBinding;
    private RichInputText sexBinding;
    private RichInputText designationBinding;
    private RichInputText departmentBinding;
    private RichInputText finYearBinding;
    private RichInputDate fromDtBinding;
    private RichInputDate toDtBinding;
    private RichInputComboboxListOfValues unitCdBinding;
    private RichInputText panNoBinding;
    private RichTable itHraTableBinding;
    private RichPopup hraPopUpBinding;
    private RichButton itHraButtonAL;
    private RichInputText empCdItHraBinding;
    private RichButton showAllSalaryHeadButtonBinding;
    private RichButton extraCalculationButtonBinding;
    private RichButton otherEarningButtonBinding;
    private RichButton itHraButtonBinding;
    private RichButton incomeFromHousePropertyButtonBinding;
    private RichButton gdeductionButtonBinding;
    private RichButton gdDeductionButtonBinding;
    private RichInputText grossSalaryBinding;
    private RichInputText hraBinding;
    private RichInputText hraReceivedBinding;
    private RichInputText profTaxBinding;
    private RichInputText underIncomeBinding;
    private RichInputText incomeFromHouseBinding;
    private RichInputText standardDeductionBinding;
    private RichInputText grossTotalBinding;
    private RichInputText infraBondDetailBinding;
    private RichInputText totalTaxIncomeBinding;
    private RichInputText totalIncomeBinding;
    private RichButton computeTaxButtonBinding;
    private RichInputText taxOnTotalIncomeBinding;
    private RichInputText rebateBinding;
    private RichInputText totTaxRebateBinding;
    private RichInputText eduCessBinding;
    private RichInputText heCessBinding;
    private RichInputText totTaxPayableBinding;
    private RichInputText taxAdjAmtBinding;
    private RichInputText taxPaidBinding;
    private RichInputText taxBalanceBinding;

    public IncomeTaxComputationBean() {
    }

    public void setOutputTextBinding(RichInputText outputTextBinding) {
        this.outputTextBinding = outputTextBinding;
    }

    public RichInputText getOutputTextBinding() {
        cevmodecheck();
        return outputTextBinding;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }
    
    private void cevmodecheck(){
            if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
                cevModeDisableComponent("V");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("C");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("E");
            }
        }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
               try {
                   Method method1 =
                       component.getClass().getMethod("setDisabled", boolean.class);
                   if (method1 != null) {
                       method1.invoke(component, valueComponent);
                   }
               } catch (NoSuchMethodException e) {
                   try {
                       Method method =
                           component.getClass().getMethod("setReadOnly", boolean.class);
                       if (method != null) {
                           method.invoke(component, valueComponent);
                       }
                   } catch (Exception e1) {
                       // e.printStackTrace();//silently eat this exception.
                   }


               } catch (Exception e) {
                   // e.printStackTrace();//silently eat this exception.
               }
               List<UIComponent> childComponents = component.getChildren();
               for (UIComponent comp : childComponents) {
                   makeComponentHierarchyReadOnly(comp, valueComponent);
               }
           }
    
    //Set Fields and Button disable.
        public void cevModeDisableComponent(String mode) {
        //            FacesContext fctx = FacesContext.getCurrentInstance();
        //            ELContext elctx = fctx.getELContext();
        //            Application jsfApp = fctx.getApplication();
        //            //create a ValueExpression that points to the ADF binding layer
        //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
        //            //
        //            ValueExpression valueExpr = exprFactory.createValueExpression(
        //                                         elctx,
        //                                         "#{pageFlowScope.mode=='E'}",
        //                                          Object.class
        //                                         );
        //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
            if (mode.equals("E")) {
                transactionIdBinding.setDisabled(true);
                unitCdBinding.setDisabled(true);
                empCdBinding.setDisabled(true);
                sexBinding.setDisabled(true);
                designationBinding.setDisabled(true);
                departmentBinding.setDisabled(true);
                finYearBinding.setDisabled(true);
                fromDtBinding.setDisabled(true);
                toDtBinding.setDisabled(true);
                panNoBinding.setDisabled(true);
                grossSalaryBinding.setDisabled(true);
                hraBinding.setDisabled(true);
                hraReceivedBinding.setDisabled(true);
                profTaxBinding.setDisabled(true);
                underIncomeBinding.setDisabled(true);
                incomeFromHouseBinding.setDisabled(true);
                standardDeductionBinding.setDisabled(true);
                grossTotalBinding.setDisabled(true);
                infraBondDetailBinding.setDisabled(true);
                totalTaxIncomeBinding.setDisabled(true);
                totalIncomeBinding.setDisabled(true);
                taxOnTotalIncomeBinding.setDisabled(true);
                rebateBinding.setDisabled(true);
                totTaxRebateBinding.setDisabled(true);
                eduCessBinding.setDisabled(true);
                heCessBinding.setDisabled(true);
                totTaxPayableBinding.setDisabled(true);
                taxAdjAmtBinding.setDisabled(true);
                taxPaidBinding.setDisabled(true);
                taxBalanceBinding.setDisabled(true);

            } else if (mode.equals("C")) {

            } else if (mode.equals("V")) {
                showAllSalaryHeadButtonBinding.setDisabled(false);
                itHraButtonBinding.setDisabled(false);
                extraCalculationButtonBinding.setDisabled(false);
                otherEarningButtonBinding.setDisabled(false);
                incomeFromHousePropertyButtonBinding.setDisabled(false);
                gdeductionButtonBinding.setDisabled(false);
                gdDeductionButtonBinding.setDisabled(false);
                computeTaxButtonBinding.setDisabled(false);

 
            }
            
        }


    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setTransactionIdBinding(RichInputText transactionIdBinding) {
        this.transactionIdBinding = transactionIdBinding;
    }

    public RichInputText getTransactionIdBinding() {
        return transactionIdBinding;
    }

    public void setEmpCdBinding(RichInputComboboxListOfValues empCdBinding) {
        this.empCdBinding = empCdBinding;
    }

    public RichInputComboboxListOfValues getEmpCdBinding() {
        return empCdBinding;
    }

    public void setSexBinding(RichInputText sexBinding) {
        this.sexBinding = sexBinding;
    }

    public RichInputText getSexBinding() {
        return sexBinding;
    }

    public void setDesignationBinding(RichInputText designationBinding) {
        this.designationBinding = designationBinding;
    }

    public RichInputText getDesignationBinding() {
        return designationBinding;
    }

    public void setDepartmentBinding(RichInputText departmentBinding) {
        this.departmentBinding = departmentBinding;
    }

    public RichInputText getDepartmentBinding() {
        return departmentBinding;
    }

    public void setFinYearBinding(RichInputText finYearBinding) {
        this.finYearBinding = finYearBinding;
    }

    public RichInputText getFinYearBinding() {
        return finYearBinding;
    }

    public void setFromDtBinding(RichInputDate fromDtBinding) {
        this.fromDtBinding = fromDtBinding;
    }

    public RichInputDate getFromDtBinding() {
        return fromDtBinding;
    }

    public void setToDtBinding(RichInputDate toDtBinding) {
        this.toDtBinding = toDtBinding;
    }

    public RichInputDate getToDtBinding() {
        return toDtBinding;
    }

    public void setUnitCdBinding(RichInputComboboxListOfValues unitCdBinding) {
        this.unitCdBinding = unitCdBinding;
    }

    public RichInputComboboxListOfValues getUnitCdBinding() {
        return unitCdBinding;
    }

    public void setPanNoBinding(RichInputText panNoBinding) {
        this.panNoBinding = panNoBinding;
    }

    public RichInputText getPanNoBinding() {
        return panNoBinding;
    }

    public void deleteItHraDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().equals("Y"))
        {
            if(dialogEvent.getOutcome().name().equals("ok"))
                        {
                        ADFUtils.findOperation("Delete").execute();
                        FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                        FacesContext fc = FacesContext.getCurrentInstance();  
                        fc.addMessage(null, Message);  
                       
                        }
                    AdfFacesContext.getCurrentInstance().addPartialTarget(itHraTableBinding);

        }
    }

    public void setItHraTableBinding(RichTable itHraTableBinding) {
        this.itHraTableBinding = itHraTableBinding;
    }

    public RichTable getItHraTableBinding() {
        return itHraTableBinding;
    }

    public void cityVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce.getNewValue()!=null)
        {
            System.out.println("City Value"+vce.getNewValue());
            ADFUtils.findOperation("parameterValueFromCity").execute();
        }
    }

    public void rentPaidVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce.getNewValue()!=null)
        {
            System.out.println("Rent Paid"+vce.getNewValue());
            ADFUtils.findOperation("parameterValueFromRentPaid").execute();
        }
    }

    public void setHraPopUpBinding(RichPopup hraPopUpBinding) {
        this.hraPopUpBinding = hraPopUpBinding;
    }

    public RichPopup getHraPopUpBinding() {
        return hraPopUpBinding;
    }

    public void itHraButtonAL(ActionEvent actionEvent) {
//        RichPopup.PopupHints hints=new RichPopup.PopupHints();
//        this.hraPopUpBinding.show(hints);
//      //  ADFUtils.findOperation("CreateInsert").execute();
//        empCdItHraBinding.setValue(empCdBinding.getValue());
    }

    public void setEmpCdItHraBinding(RichInputText empCdItHraBinding) {
        this.empCdItHraBinding = empCdItHraBinding;
    }

    public RichInputText getEmpCdItHraBinding() {
        return empCdItHraBinding;
    }

    public void setShowAllSalaryHeadButtonBinding(RichButton showAllSalaryHeadButtonBinding) {
        this.showAllSalaryHeadButtonBinding = showAllSalaryHeadButtonBinding;
    }

    public RichButton getShowAllSalaryHeadButtonBinding() {
        return showAllSalaryHeadButtonBinding;
    }

    public void setExtraCalculationButtonBinding(RichButton extraCalculationButtonBinding) {
        this.extraCalculationButtonBinding = extraCalculationButtonBinding;
    }

    public RichButton getExtraCalculationButtonBinding() {
        return extraCalculationButtonBinding;
    }

    public void setOtherEarningButtonBinding(RichButton otherEarningButtonBinding) {
        this.otherEarningButtonBinding = otherEarningButtonBinding;
    }

    public RichButton getOtherEarningButtonBinding() {
        return otherEarningButtonBinding;
    }

    public void setItHraButtonBinding(RichButton itHraButtonBinding) {
        this.itHraButtonBinding = itHraButtonBinding;
    }

    public RichButton getItHraButtonBinding() {
        return itHraButtonBinding;
    }

    public void setIncomeFromHousePropertyButtonBinding(RichButton incomeFromHousePropertyButtonBinding) {
        this.incomeFromHousePropertyButtonBinding = incomeFromHousePropertyButtonBinding;
    }

    public RichButton getIncomeFromHousePropertyButtonBinding() {
        return incomeFromHousePropertyButtonBinding;
    }

    public void setGdeductionButtonBinding(RichButton gdeductionButtonBinding) {
        this.gdeductionButtonBinding = gdeductionButtonBinding;
    }

    public RichButton getGdeductionButtonBinding() {
        return gdeductionButtonBinding;
    }

    public void setGdDeductionButtonBinding(RichButton gdDeductionButtonBinding) {
        this.gdDeductionButtonBinding = gdDeductionButtonBinding;
    }

    public RichButton getGdDeductionButtonBinding() {
        return gdDeductionButtonBinding;
    }

    public void setGrossSalaryBinding(RichInputText grossSalaryBinding) {
        this.grossSalaryBinding = grossSalaryBinding;
    }

    public RichInputText getGrossSalaryBinding() {
        return grossSalaryBinding;
    }

    public void setHraBinding(RichInputText hraBinding) {
        this.hraBinding = hraBinding;
    }

    public RichInputText getHraBinding() {
        return hraBinding;
    }

    public void setHraReceivedBinding(RichInputText hraReceivedBinding) {
        this.hraReceivedBinding = hraReceivedBinding;
    }

    public RichInputText getHraReceivedBinding() {
        return hraReceivedBinding;
    }

    public void setProfTaxBinding(RichInputText profTaxBinding) {
        this.profTaxBinding = profTaxBinding;
    }

    public RichInputText getProfTaxBinding() {
        return profTaxBinding;
    }

    public void setUnderIncomeBinding(RichInputText underIncomeBinding) {
        this.underIncomeBinding = underIncomeBinding;
    }

    public RichInputText getUnderIncomeBinding() {
        return underIncomeBinding;
    }

    public void setIncomeFromHouseBinding(RichInputText incomeFromHouseBinding) {
        this.incomeFromHouseBinding = incomeFromHouseBinding;
    }

    public RichInputText getIncomeFromHouseBinding() {
        return incomeFromHouseBinding;
    }

    public void setStandardDeductionBinding(RichInputText standardDeductionBinding) {
        this.standardDeductionBinding = standardDeductionBinding;
    }

    public RichInputText getStandardDeductionBinding() {
        return standardDeductionBinding;
    }

    public void setGrossTotalBinding(RichInputText grossTotalBinding) {
        this.grossTotalBinding = grossTotalBinding;
    }

    public RichInputText getGrossTotalBinding() {
        return grossTotalBinding;
    }

    public void setInfraBondDetailBinding(RichInputText infraBondDetailBinding) {
        this.infraBondDetailBinding = infraBondDetailBinding;
    }

    public RichInputText getInfraBondDetailBinding() {
        return infraBondDetailBinding;
    }

    public void setTotalTaxIncomeBinding(RichInputText totalTaxIncomeBinding) {
        this.totalTaxIncomeBinding = totalTaxIncomeBinding;
    }

    public RichInputText getTotalTaxIncomeBinding() {
        return totalTaxIncomeBinding;
    }

    public void setTotalIncomeBinding(RichInputText totalIncomeBinding) {
        this.totalIncomeBinding = totalIncomeBinding;
    }

    public RichInputText getTotalIncomeBinding() {
        return totalIncomeBinding;
    }

    public void setComputeTaxButtonBinding(RichButton computeTaxButtonBinding) {
        this.computeTaxButtonBinding = computeTaxButtonBinding;
    }

    public RichButton getComputeTaxButtonBinding() {
        return computeTaxButtonBinding;
    }

    public void setTaxOnTotalIncomeBinding(RichInputText taxOnTotalIncomeBinding) {
        this.taxOnTotalIncomeBinding = taxOnTotalIncomeBinding;
    }

    public RichInputText getTaxOnTotalIncomeBinding() {
        return taxOnTotalIncomeBinding;
    }

    public void setRebateBinding(RichInputText rebateBinding) {
        this.rebateBinding = rebateBinding;
    }

    public RichInputText getRebateBinding() {
        return rebateBinding;
    }

    public void setTotTaxRebateBinding(RichInputText totTaxRebateBinding) {
        this.totTaxRebateBinding = totTaxRebateBinding;
    }

    public RichInputText getTotTaxRebateBinding() {
        return totTaxRebateBinding;
    }

    public void setEduCessBinding(RichInputText eduCessBinding) {
        this.eduCessBinding = eduCessBinding;
    }

    public RichInputText getEduCessBinding() {
        return eduCessBinding;
    }

    public void setHeCessBinding(RichInputText heCessBinding) {
        this.heCessBinding = heCessBinding;
    }

    public RichInputText getHeCessBinding() {
        return heCessBinding;
    }

    public void setTotTaxPayableBinding(RichInputText totTaxPayableBinding) {
        this.totTaxPayableBinding = totTaxPayableBinding;
    }

    public RichInputText getTotTaxPayableBinding() {
        return totTaxPayableBinding;
    }

    public void setTaxAdjAmtBinding(RichInputText taxAdjAmtBinding) {
        this.taxAdjAmtBinding = taxAdjAmtBinding;
    }

    public RichInputText getTaxAdjAmtBinding() {
        return taxAdjAmtBinding;
    }

    public void setTaxPaidBinding(RichInputText taxPaidBinding) {
        this.taxPaidBinding = taxPaidBinding;
    }

    public RichInputText getTaxPaidBinding() {
        return taxPaidBinding;
    }

    public void setTaxBalanceBinding(RichInputText taxBalanceBinding) {
        this.taxBalanceBinding = taxBalanceBinding;
    }

    public RichInputText getTaxBalanceBinding() {
        return taxBalanceBinding;
    }

    public void saveAL(ActionEvent actionEvent) {
        FacesMessage Message = new FacesMessage("Record Updated Successfully.");
        Message.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
    }

    public void exemptCheckBoxVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        System.out.println("Exempt Value======>"+vce.getNewValue());
        if(vce.getNewValue()!=vce.getOldValue() && vce.getOldValue().equals("Y"))
        {
            BigDecimal RentPaid=new BigDecimal(0);
            DCIteratorBinding dci = ADFUtils.findIterator("IncomeTaxItHraExemptDtlVO1Iterator");
            RowSetIterator rsi=dci.getViewObject().createRowSetIterator(null);
                while(rsi.hasNext()) {
                    Row r = rsi.next();
                    BigDecimal monPaid=(BigDecimal)r.getAttribute("RentPaid");
                    if(monPaid!=null)
                    {
                        DCIteratorBinding dci1 = ADFUtils.findIterator("IncomeTaxItHraExemptDtlVO1Iterator");
                        RowSetIterator rsi1=dci1.getViewObject().createRowSetIterator(null);
                        while(rsi1.hasNext()) 
                        {
                            Row r2=rsi1.next();
                            r2.setAttribute("RentPaid", monPaid);
                        }
                        rsi1.closeRowSetIterator();
                        rsi.closeRowSetIterator();
                    }
                }
                rsi.closeRowSetIterator();
        }
    }
}
