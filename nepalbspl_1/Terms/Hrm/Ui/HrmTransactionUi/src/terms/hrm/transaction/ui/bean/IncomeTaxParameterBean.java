package terms.hrm.transaction.ui.bean;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class IncomeTaxParameterBean {
    private RichTable incomeTaxParameteTableBinding;
    private String editAction="V";

    public IncomeTaxParameterBean() {
    }

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public void setIncomeTaxParameteTableBinding(RichTable incomeTaxParameteTableBinding) {
        this.incomeTaxParameteTableBinding = incomeTaxParameteTableBinding;
    }

    public RichTable getIncomeTaxParameteTableBinding() {
        return incomeTaxParameteTableBinding;
    }

    public void popUpDialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                                    {
        ADFUtils.findOperation("Delete").execute();
        OperationBinding op= ADFUtils.findOperation("Commit");
        Object rst = op.execute();
        System.out.println("Record Delete Successfully");

        if(op.getErrors().isEmpty())
         {
        FacesMessage Message = new 
        FacesMessage("Record Deleted Successfully");
        Message.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext fc =  FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        }
        else
        {
        FacesMessage Message = new 
        FacesMessage("Record cannot be deleted. Child record found.");
        Message.setSeverity(FacesMessage.SEVERITY_ERROR);
        FacesContext fc =   FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        }
                     }
        AdfFacesContext.getCurrentInstance().addPartialTarget(incomeTaxParameteTableBinding);
        
    }
}
