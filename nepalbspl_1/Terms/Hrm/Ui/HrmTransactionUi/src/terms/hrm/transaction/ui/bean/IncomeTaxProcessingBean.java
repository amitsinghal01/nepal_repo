package terms.hrm.transaction.ui.bean;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.binding.OperationBinding;

public class IncomeTaxProcessingBean {
    public IncomeTaxProcessingBean() {
    }

    public void TaxProcessingBtnAL(ActionEvent actionEvent) {
        OperationBinding ob = ADFUtils.findOperation("incomeTaxProcess");
        ob.execute();
        System.out.println("ob.getResult() in bean:"+ob.getResult());
        
        if(ob.getResult()!= null){
            String result=ob.getResult().toString().substring(0,1);
            String msgg=ob.getResult().toString().substring(ob.getResult().toString().indexOf("-")+1);
            System.out.println("result after substring:"+result);
            System.out.println("msg:"+msgg);
            if(result.equals("Y")){
                FacesMessage msg=new FacesMessage(msgg);
                msg.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc=FacesContext.getCurrentInstance();
                fc.addMessage(null, msg);
            }else{
                FacesMessage msg=new FacesMessage(msgg);
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc=FacesContext.getCurrentInstance();
                fc.addMessage(null, msg);
            }
        }
    }
}
