package terms.hrm.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

public class InterviewDetailEntryBean {
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputComboboxListOfValues bioDataEntryBinding;
    private RichInputText candidatesLastNameBinding;
    private RichInputComboboxListOfValues levelCodeBinding;
    private RichInputComboboxListOfValues designationCodeBinding;
    private RichSelectOneChoice selectedBinding;
    private RichSelectOneChoice callForFinalInterviewBinding;
    private RichInputText settledSalaryBinding;
    private RichSelectOneChoice offerLetterGivenBinding;
    private RichSelectOneChoice acceptedBinding;
    private RichInputText unitNameBinding;
    private RichInputText candidatesFirstNameBinding;
    private RichInputDate interviewDateBinding;
    private RichInputText levelDescriptionBinding;
    private RichInputText designationNameBinding;
    private RichInputDate finalInterviewDateBinding;
    private RichInputDate expectedDateofJoiningBinding;
    private RichInputDate offerDateBinding;
    private RichInputText remarkBinding;
    private RichOutputText bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichSelectOneChoice interviewConductBinding;

    public InterviewDetailEntryBean() {
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setBioDataEntryBinding(RichInputComboboxListOfValues bioDataEntryBinding) {
        this.bioDataEntryBinding = bioDataEntryBinding;
    }

    public RichInputComboboxListOfValues getBioDataEntryBinding() {
        return bioDataEntryBinding;
    }

    public void setCandidatesLastNameBinding(RichInputText candidatesLastNameBinding) {
        this.candidatesLastNameBinding = candidatesLastNameBinding;
    }

    public RichInputText getCandidatesLastNameBinding() {
        return candidatesLastNameBinding;
    }

    public void setLevelCodeBinding(RichInputComboboxListOfValues levelCodeBinding) {
        this.levelCodeBinding = levelCodeBinding;
    }

    public RichInputComboboxListOfValues getLevelCodeBinding() {
        return levelCodeBinding;
    }

    public void setDesignationCodeBinding(RichInputComboboxListOfValues designationCodeBinding) {
        this.designationCodeBinding = designationCodeBinding;
    }

    public RichInputComboboxListOfValues getDesignationCodeBinding() {
        return designationCodeBinding;
    }

    public void setSelectedBinding(RichSelectOneChoice selectedBinding) {
        this.selectedBinding = selectedBinding;
    }

    public RichSelectOneChoice getSelectedBinding() {
        return selectedBinding;
    }

    public void setCallForFinalInterviewBinding(RichSelectOneChoice callForFinalInterviewBinding) {
        this.callForFinalInterviewBinding = callForFinalInterviewBinding;
    }

    public RichSelectOneChoice getCallForFinalInterviewBinding() {
        return callForFinalInterviewBinding;
    }

    public void setSettledSalaryBinding(RichInputText settledSalaryBinding) {
        this.settledSalaryBinding = settledSalaryBinding;
    }

    public RichInputText getSettledSalaryBinding() {
        return settledSalaryBinding;
    }

    public void setOfferLetterGivenBinding(RichSelectOneChoice offerLetterGivenBinding) {
        this.offerLetterGivenBinding = offerLetterGivenBinding;
    }

    public RichSelectOneChoice getOfferLetterGivenBinding() {
        return offerLetterGivenBinding;
    }

    public void setAcceptedBinding(RichSelectOneChoice acceptedBinding) {
        this.acceptedBinding = acceptedBinding;
    }

    public RichSelectOneChoice getAcceptedBinding() {
        return acceptedBinding;
    }


    public void setUnitNameBinding(RichInputText unitNameBinding) {
        this.unitNameBinding = unitNameBinding;
    }

    public RichInputText getUnitNameBinding() {
        return unitNameBinding;
    }

    public void setCandidatesFirstNameBinding(RichInputText candidatesFirstNameBinding) {
        this.candidatesFirstNameBinding = candidatesFirstNameBinding;
    }

    public RichInputText getCandidatesFirstNameBinding() {
        return candidatesFirstNameBinding;
    }

    public void setInterviewDateBinding(RichInputDate interviewDateBinding) {
        this.interviewDateBinding = interviewDateBinding;
    }

    public RichInputDate getInterviewDateBinding() {
        return interviewDateBinding;
    }

    public void setLevelDescriptionBinding(RichInputText levelDescriptionBinding) {
        this.levelDescriptionBinding = levelDescriptionBinding;
    }

    public RichInputText getLevelDescriptionBinding() {
        return levelDescriptionBinding;
    }

    public void setDesignationNameBinding(RichInputText designationNameBinding) {
        this.designationNameBinding = designationNameBinding;
    }

    public RichInputText getDesignationNameBinding() {
        return designationNameBinding;
    }

    public void setFinalInterviewDateBinding(RichInputDate finalInterviewDateBinding) {
        this.finalInterviewDateBinding = finalInterviewDateBinding;
    }

    public RichInputDate getFinalInterviewDateBinding() {
        return finalInterviewDateBinding;
    }

    public void setExpectedDateofJoiningBinding(RichInputDate expectedDateofJoiningBinding) {
        this.expectedDateofJoiningBinding = expectedDateofJoiningBinding;
    }

    public RichInputDate getExpectedDateofJoiningBinding() {
        return expectedDateofJoiningBinding;
    }

    public void setOfferDateBinding(RichInputDate offerDateBinding) {
        this.offerDateBinding = offerDateBinding;
    }

    public RichInputDate getOfferDateBinding() {
        return offerDateBinding;
    }

    public void setRemarkBinding(RichInputText remarkBinding) {
        this.remarkBinding = remarkBinding;
    }

    public RichInputText getRemarkBinding() {
        return remarkBinding;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }

    //Set Fields and Button disable.
    public void cevModeDisableComponent(String mode) {
        //            FacesContext fctx = FacesContext.getCurrentInstance();
        //            ELContext elctx = fctx.getELContext();
        //            Application jsfApp = fctx.getApplication();
        //            //create a ValueExpression that points to the ADF binding layer
        //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
        //            //
        //            ValueExpression valueExpr = exprFactory.createValueExpression(
        //                                         elctx,
        //                                         "#{pageFlowScope.mode=='E'}",
        //                                          Object.class
        //                                         );
        //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
        if (mode.equals("E")) {
            getHeaderEditBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
            getUnitNameBinding().setDisabled(true);
            getBioDataEntryBinding().setDisabled(false);
            getCandidatesFirstNameBinding().setDisabled(true);
            getCandidatesLastNameBinding().setDisabled(true);
            getLevelCodeBinding().setDisabled(false);
            getLevelDescriptionBinding().setDisabled(true);
            getDesignationCodeBinding().setDisabled(false);
            getDesignationNameBinding().setDisabled(true);
            getSelectedBinding().setDisabled(false);
            getCallForFinalInterviewBinding().setDisabled(false);
            getFinalInterviewDateBinding().setDisabled(false);
            getSettledSalaryBinding().setDisabled(false);
            getExpectedDateofJoiningBinding().setDisabled(false);
            getOfferLetterGivenBinding().setDisabled(false);
            getOfferDateBinding().setDisabled(false);
            getAcceptedBinding().setDisabled(false);
            getRemarkBinding().setDisabled(false);

        } else if (mode.equals("C")) {
            //                getHeaderEditBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
            getUnitNameBinding().setDisabled(true);
            //                getBioDataEntryBinding().setDisabled(false);
            //                getCandidatesFirstNameBinding().setDisabled(true);
            //                getCandidatesLastNameBinding().setDisabled(true);
            //                getLevelCodeBinding().setDisabled(true);
            //                getLevelDescriptionBinding().setDisabled(true);
            getDesignationCodeBinding().setDisabled(true);
            //                getDesignationNameBinding().setDisabled(true);
            getSelectedBinding().setDisabled(true);
            //                getCallForFinalInterviewBinding().setDisabled(false);
            //                getFinalInterviewDateBinding().setDisabled(false);
            //                getSettledSalaryBinding().setDisabled(false);
            //                getExpectedDateofJoiningBinding().setDisabled(false);
            //                getOfferLetterGivenBinding().setDisabled(false);
            //                getOfferDateBinding().setDisabled(false);
            //                getAcceptedBinding().setDisabled(false);
            getRemarkBinding().setDisabled(false);
        } else if (mode.equals("V")) {

        }

    }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setInterviewConductBinding(RichSelectOneChoice interviewConductBinding) {
        this.interviewConductBinding = interviewConductBinding;
    }

    public RichSelectOneChoice getInterviewConductBinding() {
        return interviewConductBinding;
    }

    public void saveButtonAL(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedBy("InterviewDetailEntryVO1Iterator","LastUpdatedBy");
        System.out.println("In SaveAl");
        if (!levelCodeBinding.isDisabled()) {
            OperationBinding op1 = ADFUtils.findOperation("Commit");
            System.out.println("In SaveAl create");
            op1.execute();
            FacesMessage Message = new FacesMessage("Record Save Successfully!");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
            
            ADFUtils.setEL("#{pageFlowScope.mode}", "V");
            cevmodecheck();
            AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);

        } else {
            if (levelCodeBinding.isDisabled()) {
                OperationBinding op2 = ADFUtils.findOperation("Commit");
                System.out.println("In SaveAl edit");
                op2.execute();
                FacesMessage Message = new FacesMessage("Record Updated Successfully!");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
                
                ADFUtils.setEL("#{pageFlowScope.mode}", "V");
                cevmodecheck();
                AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);

            }
        }
    }

    public void interviewAttendVCL(ValueChangeEvent valueChangeEvent) {
        if (valueChangeEvent != null) {
            String flag = (String) valueChangeEvent.getNewValue();
            String interviewAttendFlag = (String) getInterviewConductBinding().getValue();
            System.out.println("interviewAttendFlag " + interviewAttendFlag);
            if (flag.equalsIgnoreCase("N")) {
                getSelectedBinding().setDisabled(true);
                System.out.println("inside if");

            } else {
                getSelectedBinding().setDisabled(false);
            }
        }

    }

    public void selectedVCL(ValueChangeEvent valueChangeEvent) {
        if (valueChangeEvent != null) {
            String flag = (String) getSelectedBinding().getValue();
            if (flag.equalsIgnoreCase("Y")) {
                getDesignationCodeBinding().setDisabled(false);
            }
            else
            getDesignationCodeBinding().setDisabled(true);

        }
    }
}
