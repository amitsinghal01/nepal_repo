package terms.hrm.transaction.ui.bean;

import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

public class ItDeclarationApprovalBean {
    private RichTable populateDataTableNBinding;

    public ItDeclarationApprovalBean() {
    }

    public void populateDataAL(ActionEvent actionEvent) 
    {
    ADFUtils.findOperation("dataPopulateInItDeclarationApprovalForm").execute();
    }

    public void saveButtonAL(ActionEvent actionEvent) 
    {
    OperationBinding opr =ADFUtils.findOperation("removeRowBeforeSaveItDeclarationApproval");
    opr.execute();
    System.out.println("==========Opr Result========="+opr.getResult());
    if(opr.getResult()!=null && opr.getResult().toString().startsWith("Y"))
    {
    System.out.println("Sub String Value"+opr.getResult().toString().substring(1));
    ADFUtils.showMessage(opr.getResult().toString().substring(1)+" Record Saved Successfully.", 2);
    ADFUtils.findOperation("Commit").execute();
    AdfFacesContext.getCurrentInstance().addPartialTarget(populateDataTableNBinding);
    }
    else if(opr.getResult()!=null && opr.getResult().toString().equalsIgnoreCase("N"))
    {
        ADFUtils.showMessage("No Record Selected For Save Data.", 2);
    }
    else if(opr.getResult()!=null && opr.getResult().toString().equalsIgnoreCase("P"))
    {
        ADFUtils.showMessage("Problem in save record,Please check.",0);
    }
    }

    public void setPopulateDataTableNBinding(RichTable populateDataTableNBinding) {
        this.populateDataTableNBinding = populateDataTableNBinding;
    }

    public RichTable getPopulateDataTableNBinding() {
        return populateDataTableNBinding;
    }
}
