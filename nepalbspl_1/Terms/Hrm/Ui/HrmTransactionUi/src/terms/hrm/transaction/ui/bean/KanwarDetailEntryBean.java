package terms.hrm.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;

import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.binding.OperationBinding;

public class KanwarDetailEntryBean {
   
    private RichInputText entryNumberBinding;
    private RichInputText placeFromBinding;
    private RichInputDate kanwarDateBinding;
    private RichInputDate entryDateBinding;
    private RichInputText totalKilometersBinding;
    private RichOutputText bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText unitNameBinding;
    private RichInputText employeeNameBinding;
    private RichInputText vehicleNumberBinding;
    private RichInputText partnerNameBinding;
    private RichInputComboboxListOfValues empcodebinding;
    private RichInputText shiftcdbinding;
    private RichInputText bsdisbind;
    private RichInputText busCdBinding;

    public KanwarDetailEntryBean() {
    }

    public void KanwarDetailEntrySaveAL(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedBy("KanwarDetailEntryVO1Iterator","LastUpdatedBy");
        OperationBinding op = ADFUtils.findOperation("getKanwarDetailEntryEntryNumber");
        Object rst = op.execute();
        
        System.out.println("Result is===> "+op.getResult());
        
        
        System.out.println("--------Commit-------");
            System.out.println("value after getting result--->?"+rst);
            
        
            if (rst.toString() != null && rst.toString() != "" && rst.toString()!="N") {
                if (op.getErrors().isEmpty()) {
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message = new FacesMessage("Record Saved Successfully. New Entry No. is "+rst+".");   
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                    FacesContext fc = FacesContext.getCurrentInstance();   
                    fc.addMessage(null, Message);  
        
                }
            }
            
            if (rst.toString().equalsIgnoreCase("N")) {
                if (op.getErrors().isEmpty()) {
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message = new FacesMessage("Record Update Successfully.");   
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                    FacesContext fc = FacesContext.getCurrentInstance();   
                    fc.addMessage(null, Message);  
            
                }

            
            }
    }

   

    public void setEntryNumberBinding(RichInputText entryNumberBinding) {
        this.entryNumberBinding = entryNumberBinding;
    }

    public RichInputText getEntryNumberBinding() {
        return entryNumberBinding;
    }

    public void setPlaceFromBinding(RichInputText placeFromBinding) {
        this.placeFromBinding = placeFromBinding;
    }

    public RichInputText getPlaceFromBinding() {
        return placeFromBinding;
    }

    public void setKanwarDateBinding(RichInputDate kanwarDateBinding) {
        this.kanwarDateBinding = kanwarDateBinding;
    }

    public RichInputDate getKanwarDateBinding() {
        return kanwarDateBinding;
    }


    public void setEntryDateBinding(RichInputDate entryDateBinding) {
        this.entryDateBinding = entryDateBinding;
    }

    public RichInputDate getEntryDateBinding() {
        return entryDateBinding;
    }

    public void setTotalKilometersBinding(RichInputText totalKilometersBinding) {
        this.totalKilometersBinding = totalKilometersBinding;
    }

    public RichInputText getTotalKilometersBinding() {
        return totalKilometersBinding;
    }


    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }
    
    
    private void cevmodecheck(){
           if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
               cevModeDisableComponent("V");
           }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
               cevModeDisableComponent("C");
           }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
               cevModeDisableComponent("E");
           }
       }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
               try {
                   Method method1 =
                       component.getClass().getMethod("setDisabled", boolean.class);
                   if (method1 != null) {
                       method1.invoke(component, valueComponent);
                   }
               } catch (NoSuchMethodException e) {
                   try {
                       Method method =
                           component.getClass().getMethod("setReadOnly", boolean.class);
                       if (method != null) {
                           method.invoke(component, valueComponent);
                       }
                   } catch (Exception e1) {
                       // e.printStackTrace();//silently eat this exception.
                   }


               } catch (Exception e) {
                   // e.printStackTrace();//silently eat this exception.
               }
               List<UIComponent> childComponents = component.getChildren();
               for (UIComponent comp : childComponents) {
                   makeComponentHierarchyReadOnly(comp, valueComponent);
               }
           }

    //Set Fields and Button disable.
        public void cevModeDisableComponent(String mode) {
        //            FacesContext fctx = FacesContext.getCurrentInstance();
        //            ELContext elctx = fctx.getELContext();
        //            Application jsfApp = fctx.getApplication();
        //            //create a ValueExpression that points to the ADF binding layer
        //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
        //            //
        //            ValueExpression valueExpr = exprFactory.createValueExpression(
        //                                         elctx,
        //                                         "#{pageFlowScope.mode=='E'}",
        //                                          Object.class
        //                                         );
        //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
            if (mode.equals("E")) {
                    getHeaderEditBinding().setDisabled(true);
                    getUnitCodeBinding().setDisabled(true);
                    getUnitNameBinding().setDisabled(true);
                    getEntryNumberBinding().setDisabled(true);
                    getEntryDateBinding().setDisabled(true);
//                    getEmployeeNameBinding().setDisabled(true);
                   getShiftcdbinding().setDisabled(true);
//                    getVehicleNumberBinding().setDisabled(false);
                    getPlaceFromBinding().setDisabled(true);
                  getTotalKilometersBinding().setDisabled(true);
                  getBsdisbind().setDisabled(true);
//                    getKanwarDateBinding().setDisabled(false);
                    getPartnerNameBinding().setDisabled(true);
                    getBusCdBinding().setDisabled(true);
                } else if (mode.equals("C")) {
                getHeaderEditBinding().setDisabled(true);
                getUnitCodeBinding().setDisabled(true);
                getUnitNameBinding().setDisabled(true);
                getEntryNumberBinding().setDisabled(true);
               getEntryDateBinding().setDisabled(true);
//                getEmployeeNameBinding().setDisabled(true);
               getShiftcdbinding().setDisabled(true);
//                getVehicleNumberBinding().setDisabled(false);
                getPlaceFromBinding().setDisabled(true);
            getTotalKilometersBinding().setDisabled(true);
                getBsdisbind().setDisabled(true);
//                getKanwarDateBinding().setDisabled(false);
                getPartnerNameBinding().setDisabled(true);
                getBusCdBinding().setDisabled(true);
                
            } else if (mode.equals("V")) {
                
            }
            
        }


    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
        // Add event code here...
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setUnitNameBinding(RichInputText unitNameBinding) {
        this.unitNameBinding = unitNameBinding;
    }

    public RichInputText getUnitNameBinding() {
        return unitNameBinding;
    }

    public void setEmployeeNameBinding(RichInputText employeeNameBinding) {
        this.employeeNameBinding = employeeNameBinding;
    }

    public RichInputText getEmployeeNameBinding() {
        return employeeNameBinding;
    }

    public void setVehicleNumberBinding(RichInputText vehicleNumberBinding) {
        this.vehicleNumberBinding = vehicleNumberBinding;
    }

    public RichInputText getVehicleNumberBinding() {
        return vehicleNumberBinding;
    }

    public void setPartnerNameBinding(RichInputText partnerNameBinding) {
        this.partnerNameBinding = partnerNameBinding;
    }

    public RichInputText getPartnerNameBinding() {
        return partnerNameBinding;
    }

    public void empCodeVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        vce.getNewValue();
        String unitCd=(String) getUnitCodeBinding().getValue();
        System.out.println("Unit Value is-----.."+unitCd);
        String empCode=(String) getEmpcodebinding().getValue();
        System.out.println("Emp Value is-----.."+empCode);
        oracle.jbo.domain.Date kdate=(oracle.jbo.domain.Date) getKanwarDateBinding().getValue();
        System.out.println("kdate Value is-----.."+kdate);
        OperationBinding op=ADFUtils.findOperation("getshiftValueforkanwer");
        op.getParamsMap().put("empCode", empCode);
        op.getParamsMap().put("kdate", kdate);
        op.getParamsMap().put("unitCd", unitCd);
        op.execute();
        OperationBinding op1=ADFUtils.findOperation("getBusCodeAndDestinationForKanwerEntry");
        op1.getParamsMap().put("empCode", empCode);
        op1.getParamsMap().put("kdate", kdate);
        op1.getParamsMap().put("unitCd", unitCd);
        op1.execute();
        
    }

    public void setEmpcodebinding(RichInputComboboxListOfValues empcodebinding) {
        this.empcodebinding = empcodebinding;
    }

    public RichInputComboboxListOfValues getEmpcodebinding() {
        return empcodebinding;
    }

    public void setShiftcdbinding(RichInputText shiftcdbinding) {
        this.shiftcdbinding = shiftcdbinding;
    }

    public RichInputText getShiftcdbinding() {
        return shiftcdbinding;
    }

    public void setBsdisbind(RichInputText bsdisbind) {
        this.bsdisbind = bsdisbind;
    }

    public RichInputText getBsdisbind() {
        return bsdisbind;
    }

    public void setBusCdBinding(RichInputText busCdBinding) {
        this.busCdBinding = busCdBinding;
    }

    public RichInputText getBusCdBinding() {
        return busCdBinding;
    }
}
