package terms.hrm.transaction.ui.bean;

import javax.faces.application.FacesMessage;

import javax.faces.context.FacesContext;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.adfinternal.view.faces.context.AdfFacesContextImpl;

import oracle.binding.OperationBinding;

public class LeaveApprovalAuthorityBean {
    private RichTable leaveApprovalAuthorityTableBinding;
    private String editAction = "V";

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public LeaveApprovalAuthorityBean() {
    }

    public void setLeaveApprovalAuthorityTableBinding(RichTable leaveApprovalAuthorityTableBinding) {
        this.leaveApprovalAuthorityTableBinding = leaveApprovalAuthorityTableBinding;
    }

    public RichTable getLeaveApprovalAuthorityTableBinding() {
        return leaveApprovalAuthorityTableBinding;
    }

    public void popUpDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            OperationBinding op = null;
            ADFUtils.findOperation("Delete").execute();
            op = (OperationBinding) ADFUtils.findOperation("Commit");
            if (getEditAction().equals("V")) {
                Object rst = op.execute();
            }
            System.out.println("Record Delete Successfully");
            if (op.getErrors().isEmpty()) {
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            } else if (!op.getErrors().isEmpty()) {
                OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
                Object rstr = opr.execute();
                FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            }
        }
        AdfFacesContextImpl.getCurrentInstance().addPartialTarget(leaveApprovalAuthorityTableBinding);
    }
}
