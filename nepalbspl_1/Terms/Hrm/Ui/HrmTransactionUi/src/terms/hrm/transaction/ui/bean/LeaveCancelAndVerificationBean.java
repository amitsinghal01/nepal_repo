package terms.hrm.transaction.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;



import java.util.Date;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectBooleanCheckbox;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;
import oracle.jbo.ViewObject;
import oracle.jbo.domain.Timestamp;
import oracle.jbo.server.ViewObjectImpl;

public class LeaveCancelAndVerificationBean {
    private RichTable leaveCancelAndVerificationTableBinding;
    private RichSelectOneChoice leaveGrantedBinding;
    private RichInputText chkVerByBinding;
    private RichInputText verByBinding;
    private RichInputText leaveGrantedPreBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputComboboxListOfValues employeeNumberBinding;
    private RichInputDate fromDateBinding;
    private RichInputDate endDateBinding;
    private RichInputText leaveTypeBinding;
    private RichInputDate cancelFromDateBinding;
    private RichInputDate cancelToDateBinding;
    private RichInputText cancelDaysBinding;
    private RichInputText approvalCancelHeadBinding;
    private RichInputText remarksBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichInputText employeeNameBinding;
    private RichInputText approvalCancelPersonNameBinding;
    private RichSelectBooleanCheckbox accountCheckBinding;
    private RichInputText unitNameBinding;
    private RichColumn bindingOutputText;
    
    private String editAction="V";
    private RichInputText authNameBinding;

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }


    public LeaveCancelAndVerificationBean() {
    }

    public void populateAL(ActionEvent actionEvent) {
        OperationBinding ob=ADFUtils.findOperation("PopulateLeaveCancelAndApproval");
                 ob.execute();
//                 if(ob.getResult()!=null && 
//        ob.getResult().toString().equalsIgnoreCase("V"))
//                 {
//                     ADFUtils.showMessage("Data populated Succesfully.", 2);
//                     }
        //AdfFacesContext.getCurrentInstance().addPartialTarget(leaveCancelAndVerificationTableBinding);
             }



    public void setLeaveCancelAndVerificationTableBinding(RichTable leaveCancelAndVerificationTableBinding) {
        this.leaveCancelAndVerificationTableBinding = leaveCancelAndVerificationTableBinding;
    }

    public RichTable getLeaveCancelAndVerificationTableBinding() {
        return leaveCancelAndVerificationTableBinding;
    }

    public void leaveGrantedVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
            System.out.println("In VCE");
        OperationBinding ob=ADFUtils.findOperation("LeaveCancelAndApprovalEntry");
        Object rst=ob.execute();
            System.out.println("Result of Function(1): "+ob.getResult());     
            System.out.println("Result after Function execute: "+rst);         
            System.out.println("Leave Granted: "+getLeaveGrantedBinding().getValue());
            System.out.println("Chk Ver By: "+getChkVerByBinding().getValue());
            System.out.println("Ver By: "+getVerByBinding().getValue()); 
        String LeaveGrntPre=(String)leaveGrantedPreBinding.getValue();
            System.out.println("leaveGranted Pre Binding: "+LeaveGrntPre); 
            System.out.println("leaveGranted Post Binding: "+leaveGrantedBinding.getValue());     
        if (rst.toString() != null && rst.toString() != "") { 
                System.out.println("Result of Function(2):  "+ob.getResult());
            String result=(String)ob.getResult();
           // if (ob.getErrors().isEmpty()) {     
            if(rst.toString().equalsIgnoreCase("E-You are not authorized..N")) {
                System.out.println("Error Not Authorized(1): "+ob.getResult());         
         //   if (ob.getErrors().isEmpty()) {
                System.out.println("Error Not Authorized(2): "+ob.getResult()); 
            ADFUtils.showMessage("You are not authorized.", 2);                            
                if(LeaveGrntPre.equalsIgnoreCase("P")) {
                        leaveGrantedBinding.setValue(1);
                }               
                if(LeaveGrntPre.equalsIgnoreCase("A")) {
                        leaveGrantedBinding.setValue(2);
                }               
                if(LeaveGrntPre.equalsIgnoreCase("C")) {
                        leaveGrantedBinding.setValue(3);
                }               
                if(LeaveGrntPre.equalsIgnoreCase("H")) {
                        leaveGrantedBinding.setValue(4);
                }                
                if(LeaveGrntPre.equalsIgnoreCase("R")) {
                        leaveGrantedBinding.setValue(5);
                }                
                if(LeaveGrntPre.equalsIgnoreCase("N")) {
                        leaveGrantedBinding.setValue(6);
                }
                    System.out.println("Leave Granted New value(1): "+vce.getNewValue());
                employeeNumberBinding.setValue("");
                employeeNameBinding.setValue("");
                approvalCancelPersonNameBinding.setValue("");
                accountCheckBinding.setValid(false);
        }
        else if(rst.toString().equalsIgnoreCase("E-Please Recommended the Leave Status First.")) {
                        System.out.println("For Check Recommended the Leave Status First"); 
                    ADFUtils.showMessage("Please Recommended the Leave Status First.", 2);                                        
                    if(LeaveGrntPre.equalsIgnoreCase("P")) {
                            leaveGrantedBinding.setValue(1);
                    }                    
                    if(LeaveGrntPre.equalsIgnoreCase("A")) {
                            leaveGrantedBinding.setValue(2);
                    }                   
                    if(LeaveGrntPre.equalsIgnoreCase("C")) {
                            leaveGrantedBinding.setValue(3);
                    }                    
                    if(LeaveGrntPre.equalsIgnoreCase("H")) {
                            leaveGrantedBinding.setValue(4);
                    }                   
                    if(LeaveGrntPre.equalsIgnoreCase("R")) {
                            leaveGrantedBinding.setValue(5);
                    }                    
                    if(LeaveGrntPre.equalsIgnoreCase("N")) {
                            leaveGrantedBinding.setValue(6);
                    }
                    System.out.println("Leave Granted New value(2): "+vce.getNewValue());
        }
            else if(rst.toString().equalsIgnoreCase(result)) {
                    System.out.println("Result is 5: "+ob.getResult());                   
                    DCIteratorBinding dci = ADFUtils.findIterator("LeaveCancelAndVerificationVO1Iterator");
                    DCIteratorBinding dci1 = ADFUtils.findIterator("LeaveCancelAndVerificationViewVO1Iterator");    
                    RowSetIterator rsi=dci1.getViewObject().createRowSetIterator(null);
                    while(rsi.hasNext()) {
                        System.out.println("Inside RSI 1 ");
                      Row r2 = rsi.next();      
                      String appr = (String) r2.getAttribute("AppAuth");
                      String appnm = (String) r2.getAttribute("AppAuthName");   
                        approvalCancelPersonNameBinding.setValue(appr);
                        authNameBinding.setValue(appnm);
                        Integer no = 1;
                        Boolean bo = true;
                        accountCheckBinding.setValue(bo);
                        System.out.println("Check Value 1 #### " + accountCheckBinding.getValue());
                    }
                    rsi.closeRowSetIterator();
        }
                else if(rst.toString().equalsIgnoreCase(result)) {
                        System.out.println("Result is 6:  "+ob.getResult());
                    DCIteratorBinding dci1 = ADFUtils.findIterator("LeaveCancelAndVerificationViewVO1Iterator"); 
                    RowSetIterator rsi=dci1.getViewObject().createRowSetIterator(null);
                    while(rsi.hasNext()){
                        System.out.println("Inside RSI 2 ");
                      Row r2 = rsi.next();
                        
                        String appr = (String) r2.getAttribute("AppAuth");
                        String appnm = (String) r2.getAttribute("AppAuthName");
                        
                        approvalCancelPersonNameBinding.setValue(appr);
                        authNameBinding.setValue(appnm);
                        Integer no = 1;
                        Boolean bo = true;
                        accountCheckBinding.setValue(bo);
                        System.out.println("Check Value 2 #### " + accountCheckBinding.getValue());
                    }
                    rsi.closeRowSetIterator();
//                        approvalCancelPersonNameBinding.setValue(appr);
//                        accountCheckBinding.setValue(1);
        }            
            else {
                    System.out.println("Result is 7: "+ob.getResult());
                DCIteratorBinding dci1 = ADFUtils.findIterator("LeaveCancelAndVerificationViewVO1Iterator");                   
                RowSetIterator rsi=dci1.getViewObject().createRowSetIterator(null);
                    while(rsi.hasNext()) {
                        System.out.println("Inside RSI 3 ");
                      Row r2 = rsi.next();          
                      String appr = (String) r2.getAttribute("AppAuth");
                      String appnm = (String) r2.getAttribute("AppAuthName");  
                        approvalCancelPersonNameBinding.setValue(appr);
                        authNameBinding.setValue(appnm);
                        Integer no = 1;
                        Boolean bo = true;
                        accountCheckBinding.setValue(bo);
                            System.out.println("Check Value 3 #### " + accountCheckBinding.getValue());
                    }
                    rsi.closeRowSetIterator();
        }       
                System.out.println("leaveGrantedBinding.getValue()"+leaveGrantedBinding.getValue());
            if(leaveGrantedBinding.getValue().equals("N")) {
                    Timestamp FrDt =  (Timestamp) fromDateBinding.getValue();
                        System.out.println("FrDt: "+FrDt);
                    Timestamp ToDt = (Timestamp) endDateBinding.getValue();
                        System.out.println("ToDt: "+ToDt);
                    cancelFromDateBinding.setValue("FrDt");
                    cancelToDateBinding.setValue("ToDt");
                    getCancelFromDateBinding().setDisabled(true);
                    getCancelToDateBinding().setDisabled(true);
                    getCancelDaysBinding().setDisabled(true);
            }         
           // else if(leaveGrantedBinding.getValue().equals("C"))
//            {
//                  getCancelFromDateBinding().setDisabled(false);
//                  getCancelToDateBinding().setDisabled(false);
//                  getCancelDaysBinding().setDisabled(true);
//              }
//              
//              else
//              {
//                      getCancelFromDateBinding().setDisabled(true);
//                      getCancelToDateBinding().setDisabled(true);
//                      getCancelDaysBinding().setDisabled(true);
//                  }
            
        }
            System.out.println("getLeaveGrantedBinding 1: "+getLeaveGrantedBinding().getValue());
            System.out.println("getChkVerByBinding  1: "+getChkVerByBinding().getValue());
            System.out.println("getVerByBinding 1: "+getVerByBinding().getValue());         
    }

    public void saveButtonAL(ActionEvent actionEvent) {
        
        Integer N=0;
        
        DCIteratorBinding dci = ADFUtils.findIterator("LeaveCancelAndVerificationVO1Iterator");
        RowSetIterator rsi=dci.getViewObject().createRowSetIterator(null);
            while(rsi.hasNext()) {
     
                System.out.println("Leave Cancel And Verification ");
                Row r = rsi.next();
                String UnitCd=(String) r.getAttribute("UnitCd");
                String EmpNumber=(String) r.getAttribute("EmpNumber");
                BigDecimal CancelDays=(BigDecimal) r.getAttribute("CancelDays");
                Timestamp FromDate=(Timestamp) r.getAttribute("FromDate");
                Timestamp EndDate=(Timestamp) r.getAttribute("EndDate");
                String LeaveDescType=(String) r.getAttribute("LeaveDescType");
                String LeaveGranted=(String) r.getAttribute("LeaveGranted");
                String Remarks=(String) r.getAttribute("Remarks");
                Timestamp CancelFrdt=(Timestamp) r.getAttribute("CancelFrdt");
                Timestamp CancelTodt=(Timestamp) r.getAttribute("CancelTodt");
                String ApprCancelHead=(String) r.getAttribute("ApprCancelHead");
                oracle.jbo.domain.Number AccCheck=(oracle.jbo.domain.Number) r.getAttribute("AccCheck");
                
                System.out.println("UnitCd "+UnitCd + " EmpNumber "+EmpNumber +" CancelDays "+ CancelDays + 
                                   " FromDate "+FromDate+ " EndDate "+EndDate +  " LeaveDescType "+ LeaveDescType+
                                   " LeaveGranted "+LeaveGranted+ " ApprCancelHead "+ApprCancelHead+ " Remarks "+ Remarks+
                                   " CancelFrdt "+CancelFrdt + " CancelTodt "+CancelTodt + "AccCheck"+AccCheck);
                
                
                
                // Validations Function call
//                if( FromDate  != null && EndDate  != null &&  LeaveDescType  != null &&  EmpNumber != null )
//                {
                System.out.println("Before Call Function For All Validations");                          
                OperationBinding op1=(OperationBinding)ADFUtils.findOperation("CallFunctionForAllValidationsForLeaveCancelAndVerification");
                System.out.println("After Call  Function For All Validations");
                
                op1.getParamsMap().put("UnitCd", UnitCd);
                op1.getParamsMap().put("EmpNumber", EmpNumber);
                op1.getParamsMap().put("LeaveDescType", LeaveDescType);
                op1.getParamsMap().put("FromDate", FromDate);
                op1.getParamsMap().put("EndDate", EndDate);
                op1.getParamsMap().put("CancelFrdt", CancelFrdt);
                op1.getParamsMap().put("CancelTodt", CancelTodt);               
                op1.getParamsMap().put("LeaveGranted", LeaveGranted);
                op1.getParamsMap().put("ApprCancelHead",ApprCancelHead);
                Object rst=op1.execute();
                
                if (op1.getResult() != null) {
                    System.out.println("Result is 2 ===> "+op1.getResult());
          
                    if (op1.getErrors().isEmpty()) {
                        System.out.println("Result is 4 ===> "+op1.getResult());                 
                        String res = (String) op1.getResult();
                        String result = res.substring(2);
                        FacesMessage Message = new FacesMessage("+result+");   
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                        FacesContext fc = FacesContext.getCurrentInstance();   
                        fc.addMessage(null, Message);  
                    }                 
                }  
                
                
                
                // Commit Data in tables by Procedure
                if (ApprCancelHead != null && AccCheck.equals(1)){
                System.out.println("Before Call Procedure");                          
                OperationBinding op=(OperationBinding)ADFUtils.findOperation("CallProcedureForLeaveCancelAndVerification");
                System.out.println("After Call Procedure");
                
                
                op.getParamsMap().put("UnitCd", UnitCd);
                op.getParamsMap().put("EmpNumber", EmpNumber);
                op.getParamsMap().put("LeaveDescType", LeaveDescType);
                op.getParamsMap().put("FromDate", FromDate);
                op.getParamsMap().put("EndDate", EndDate);
                op.getParamsMap().put("CancelFrdt", CancelFrdt);
                op.getParamsMap().put("CancelTodt", CancelTodt);
                op.getParamsMap().put("CancelDays", CancelDays);
                op.getParamsMap().put("LeaveGranted", LeaveGranted);
                op.getParamsMap().put("ApprCancelHead",ApprCancelHead);
                op.getParamsMap().put("Remarks", Remarks);
                op.execute();
                
                System.out.println("UnitCd 2---> "+UnitCd + " EmpNumber "+EmpNumber +" CancelDays "+ CancelDays + 
                                   " FromDate "+FromDate+ " EndDate "+EndDate +  " LeaveDescType "+ LeaveDescType+
                                   " LeaveGranted "+LeaveGranted+ " ApprCancelHead "+ApprCancelHead+ " Remarks "+ Remarks+
                                   " CancelFrdt "+CancelFrdt + " CancelTodt "+CancelTodt);
        
                }
                System.out.println("Value of N is----> "+N);
               N=N+1;
                System.out.println("Value of N after Updated is----> "+N);
            }
        rsi.closeRowSetIterator(); 
        
        FacesMessage Message = new FacesMessage("Records Updated.");   
        Message.setSeverity(FacesMessage.SEVERITY_INFO);   
        FacesContext fc = FacesContext.getCurrentInstance();   
        fc.addMessage(null, Message);
        
        
        DCIteratorBinding dci2 = ADFUtils.findIterator("LeaveCancelAndVerificationVO1Iterator");
        RowSetIterator rsi2=dci2.getViewObject().createRowSetIterator(null);
            while(rsi2.hasNext()) {
                    System.out.println("Leave Cancel And Verification To Remove the rows after commit");
                Row r = rsi2.next();
                    System.out.println("OK 1");
                r.remove();
                    System.out.println("OK 2");
            }
            rsi2.closeRowSetIterator();
                System.out.println("Close n OK");
    }
                 
    //                    
    //                    if(rst.toString().equalsIgnoreCase("E-Please Enter From Date..")) {
    //                        System.out.println("Result is 3 ===> "+op1.getResult());
    //                    
    //                    if (op1.getErrors().isEmpty()) {
    //                        System.out.println("Result is 4 ===> "+op1.getResult()); 
    //                        ADFUtils.showMessage("Please Enter From Date.", 2);
    //                        
    //                        String LeaveGrntPre=(String)leaveGrantedPreBinding.getValue();
    //                        System.out.println("leaveGrantedPreBinding Value 2 --."+LeaveGrntPre); 
    //                        System.out.println("leaveGranted Binding Value --."+leaveGrantedBinding.getValue());
    //                        
    //                    }
    //                    }
                        
                        
    //                   else if(rst.toString().equalsIgnoreCase("E-Please Enter To Date..")) {
    //                        System.out.println("Result is 3 ===> "+op1.getResult());
    //                    
    //                    if (op1.getErrors().isEmpty()) {
    //                        System.out.println("Result is 4 ===> "+op1.getResult()); 
    //                        ADFUtils.showMessage("Please Enter To Date.", 2);           
    //                        
    //                    }
    //                    }
    //                    
    //                    else if(rst.toString().equalsIgnoreCase("E-Please Enter Leave Type..")) {
    //                         System.out.println("Result is 3 ===> "+op1.getResult());
    //                     
    //                     if (op1.getErrors().isEmpty()) {
    //                         System.out.println("Result is 4 ===> "+op1.getResult()); 
    //                         ADFUtils.showMessage("Please Enter Leave Type.", 2);           
    //                         
    //                     }
    //                     }
    //                    
    //                    else if(rst.toString().equalsIgnoreCase("E-Please Enter Employee..")) {
    //                         System.out.println("Result is 3 ===> "+op1.getResult());
    //                     
    //                     if (op1.getErrors().isEmpty()) {
    //                         System.out.println("Result is 4 ===> "+op1.getResult()); 
    //                         ADFUtils.showMessage("Please Enter Employee.", 2);           
    //                         
    //                     }
    //                     }
    //                    
    //                    else if(rst.toString().equalsIgnoreCase("E-Please check cancel date, Cancel Date can not be Null")) {
    //                         System.out.println("Result is 3 ===> "+op1.getResult());
    //                     
    //                     if (op1.getErrors().isEmpty()) {
    //                         System.out.println("Result is 4 ===> "+op1.getResult()); 
    //                         ADFUtils.showMessage("Please check cancel date, Cancel Date can not be Null.", 2);           
    //                         
    //                     }
    //                     }
    //                    
    //                    else if(rst.toString().equalsIgnoreCase("E-Check cancel date, Cancel Date should be Nul")) {
    //                         System.out.println("Result is 3 ===> "+op1.getResult());
    //                     
    //                     if (op1.getErrors().isEmpty()) {
    //                         System.out.println("Result is 4 ===> "+op1.getResult()); 
    //                         ADFUtils.showMessage("Check cancel date, Cancel Date should be Null.", 2);           
    //                         
    //                     }
    //                     }
    //                    
    //                    else if(rst.toString().equalsIgnoreCase("E-Applied date and Cancel Date should be same...")) {
    //                         System.out.println("Result is 3 ===> "+op1.getResult());
    //                     
    //                     if (op1.getErrors().isEmpty()) {
    //                         System.out.println("Result is 4 ===> "+op1.getResult()); 
    //                         ADFUtils.showMessage("Applied date and Cancel Date should be same.", 2);           
    //                         
    //                     }
    //                     }
    //                    
    //                    else if(rst.toString().equalsIgnoreCase("E-Applied date and Cancel Date should not be same.., So change Status as Reject.")) {
    //                         System.out.println("Result is 3 ===> "+op1.getResult());
    //                     
    //                     if (op1.getErrors().isEmpty()) {
    //                         System.out.println("Result is 4 ===> "+op1.getResult()); 
    //                         ADFUtils.showMessage("Applied date and Cancel Date should not be same. So change Status as Reject.", 2);           
    //                         
    //                     }
    //                     }
    //                    
    //                    else if(rst.toString().equalsIgnoreCase("E-Leave Status must be change from applied to others.")) {
    //                         System.out.println("Result is 3 ===> "+op1.getResult());
    //                     
    //                     if (op1.getErrors().isEmpty()) {
    //                         System.out.println("Result is 4 ===> "+op1.getResult()); 
    //                         ADFUtils.showMessage("Leave Status must be change from applied to others.", 2);           
    //                         
    //                     }
    //                     } 
    
//                
//                if ( FromDate == null){
//                    System.out.println("FromDate "+FromDate);
//                        ADFUtils.showMessage("Please Enter From Date.", 2);
//                    }
//                
//                
//                if ( EndDate == null){
//                        System.out.println("EndDate "+EndDate);
//                        ADFUtils.showMessage("Please Enter To Date.", 2);
//                    }
//                
//                
//                if ( LeaveDescType == null){
//                        System.out.println("LeaveDescType "+LeaveDescType);
//                        ADFUtils.showMessage("Please Enter Leave Type.", 2);
//                    }
//                
//                
//                if ( EmpNumber == null){
//                        System.out.println("EmpNumber "+EmpNumber);
//                        ADFUtils.showMessage("Please Enter Employee.", 2);
//                    }
                
                               
                    
//                    if ( LeaveGranted.equalsIgnoreCase("C") &&  (FromDate == null || EndDate == null)){
//                            System.out.println("LeaveGranted "+LeaveGranted);
//                            ADFUtils.showMessage("Please check cancel date, Cancel Date can not be Null.", 2);
//                        }
//                    
//                    if ( (LeaveGranted.equalsIgnoreCase("A") || LeaveGranted.equalsIgnoreCase("P") || LeaveGranted.equalsIgnoreCase("H") ||
//                         LeaveGranted.equalsIgnoreCase("N")) && (FromDate != null || EndDate != null)){
//                            System.out.println("LeaveGranted "+LeaveGranted);
//                            ADFUtils.showMessage("Check cancel date, Cancel Date should be Null.", 2);
//                        } 
//                
//                
//                if ( (LeaveGranted.equalsIgnoreCase("R")  && FromDate != CancelFrdt && EndDate != CancelTodt)){
//                        System.out.println("LeaveGranted "+LeaveGranted);
//                        ADFUtils.showMessage("Applied date and Cancel Date should be same", 2);
//                    } 
//                        
//                        
//                if ( (LeaveGranted.equalsIgnoreCase("C")  && FromDate == CancelFrdt && EndDate == CancelTodt)){
//                        System.out.println("LeaveGranted "+LeaveGranted);
//                        ADFUtils.showMessage("Applied date and Cancel Date should not be same. So change Status as Reject.", 2);
//                    } 
//                        
//                if ( LeaveGranted.equalsIgnoreCase("P")  && ApprCancelHead != null){
//                        System.out.println("LeaveGranted "+LeaveGranted);
//                        ADFUtils.showMessage("Leave Status must be change from applied to others.", 2);
//                    }    

    

    public void setLeaveGrantedBinding(RichSelectOneChoice leaveGrantedBinding) {
        this.leaveGrantedBinding = leaveGrantedBinding;
    }

    public RichSelectOneChoice getLeaveGrantedBinding() {
        return leaveGrantedBinding;
    }

    public void setChkVerByBinding(RichInputText chkVerByBinding) {
        this.chkVerByBinding = chkVerByBinding;
    }

    public RichInputText getChkVerByBinding() {
        return chkVerByBinding;
    }

    public void setVerByBinding(RichInputText verByBinding) {
        this.verByBinding = verByBinding;
    }

    public RichInputText getVerByBinding() {
        return verByBinding;
    }

    public void setLeaveGrantedPreBinding(RichInputText leaveGrantedPreBinding) {
        this.leaveGrantedPreBinding = leaveGrantedPreBinding;
    }

    public RichInputText getLeaveGrantedPreBinding() {
        return leaveGrantedPreBinding;
    }


    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setEmployeeNumberBinding(RichInputComboboxListOfValues employeeNumberBinding) {
        this.employeeNumberBinding = employeeNumberBinding;
    }

    public RichInputComboboxListOfValues getEmployeeNumberBinding() {
        return employeeNumberBinding;
    }

    public void setFromDateBinding(RichInputDate fromDateBinding) {
        this.fromDateBinding = fromDateBinding;
    }

    public RichInputDate getFromDateBinding() {
        return fromDateBinding;
    }

    public void setEndDateBinding(RichInputDate endDateBinding) {
        this.endDateBinding = endDateBinding;
    }

    public RichInputDate getEndDateBinding() {
        return endDateBinding;
    }

    public void setLeaveTypeBinding(RichInputText leaveTypeBinding) {
        this.leaveTypeBinding = leaveTypeBinding;
    }

    public RichInputText getLeaveTypeBinding() {
        return leaveTypeBinding;
    }

    public void setCancelFromDateBinding(RichInputDate cancelFromDateBinding) {
        this.cancelFromDateBinding = cancelFromDateBinding;
    }

    public RichInputDate getCancelFromDateBinding() {
        return cancelFromDateBinding;
    }

    public void setCancelToDateBinding(RichInputDate cancelToDateBinding) {
        this.cancelToDateBinding = cancelToDateBinding;
    }

    public RichInputDate getCancelToDateBinding() {
        return cancelToDateBinding;
    }

    public void setCancelDaysBinding(RichInputText cancelDaysBinding) {
        this.cancelDaysBinding = cancelDaysBinding;
    }

    public RichInputText getCancelDaysBinding() {
        return cancelDaysBinding;
    }

    public void setApprovalCancelHeadBinding(RichInputText approvalCancelHeadBinding) {
        this.approvalCancelHeadBinding = approvalCancelHeadBinding;
    }

    public RichInputText getApprovalCancelHeadBinding() {
        return approvalCancelHeadBinding;
    }

    public void setRemarksBinding(RichInputText remarksBinding) {
        this.remarksBinding = remarksBinding;
    }

    public RichInputText getRemarksBinding() {
        return remarksBinding;
    }

    public void cancelFromDaysVCL(ValueChangeEvent vce) {     
        System.out.println("In cancelFromDaysVCL");
     
                DCIteratorBinding dci =ADFUtils.findIterator("LeaveCancelAndVerificationVVO1Iterator");
              RowSetIterator rsi=dci.getViewObject().createRowSetIterator(null);
              while(rsi.hasNext()) {
                System.out.println("In cancelFromDaysVCL 2 ");
                Row r = rsi.next();
                  
                Timestamp cancelFrDt2 =  (Timestamp) cancelFromDateBinding.getValue();
                System.out.println("cancelFrDt2--> "+cancelFrDt2);
                 Timestamp cancelToDt2 = (Timestamp) cancelToDateBinding.getValue();
                 System.out.println("cancelToDt2--> "+cancelToDt2);
                  
                if(cancelFrDt2 != null && cancelToDt2 != null){
                Long CancelDays = (((cancelToDt2.getTime()) - (cancelFrDt2.getTime()))/86400000)+1;
                Number  CanDays = (Number)(CancelDays);
                System.out.println("CanDays--> "+CanDays);
                cancelDaysBinding.setValue(CanDays);
                }      
            }
            rsi.closeRowSetIterator(); 
        
        System.out.println("In cancelFromDaysVCL 2 ");

    }
    
    private void cevmodecheck(){
           if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
               cevModeDisableComponent("V");
           }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
               cevModeDisableComponent("C");
           }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
               cevModeDisableComponent("E");
           }
       }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
               try {
                   Method method1 =component.getClass().getMethod("setDisabled", boolean.class);
                   if (method1 != null) {
                       method1.invoke(component, valueComponent);
                   }
               } catch (NoSuchMethodException e) {
                   try {
                       Method method =
                           component.getClass().getMethod("setReadOnly", boolean.class);
                       if (method != null) {
                           method.invoke(component, valueComponent);
                       }
                   } catch (Exception e1) {
                      
                   }
               } catch (Exception e) {                 
              }
               List<UIComponent> childComponents = component.getChildren();
               for (UIComponent comp : childComponents) {
                   makeComponentHierarchyReadOnly(comp, valueComponent);
               }
           }
    
    //Set Fields and Button disable.
        public void cevModeDisableComponent(String mode) {
            if (mode.equals("E")) {
                getUnitCodeBinding().setDisabled(true);
                getUnitNameBinding().setDisabled(true);
                getEmployeeNumberBinding().setDisabled(true);
                getEmployeeNameBinding().setDisabled(true);
                getFromDateBinding().setDisabled(true);
                getEndDateBinding().setDisabled(true);
                getLeaveTypeBinding().setDisabled(true);
                getApprovalCancelHeadBinding().setDisabled(true);
                getCancelDaysBinding().setDisabled(true);
                getAuthNameBinding().setDisabled(true);
             
            } else if (mode.equals("C")) {
                getUnitCodeBinding().setDisabled(true);
                getUnitNameBinding().setDisabled(true);
                getEmployeeNumberBinding().setDisabled(true);
                getEmployeeNameBinding().setDisabled(true);
                getFromDateBinding().setDisabled(true);
                getEndDateBinding().setDisabled(true);
                getLeaveTypeBinding().setDisabled(true);
                getApprovalCancelHeadBinding().setDisabled(true);
                getCancelDaysBinding().setDisabled(true);
                getAuthNameBinding().setDisabled(true);
            }
          else if (mode.equals("V")) {  
            }
            
        }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setEmployeeNameBinding(RichInputText employeeNameBinding) {
        this.employeeNameBinding = employeeNameBinding;
    }

    public RichInputText getEmployeeNameBinding() {
        return employeeNameBinding;
    }


    public void setApprovalCancelPersonNameBinding(RichInputText approvalCancelPersonNameBinding) {
        this.approvalCancelPersonNameBinding = approvalCancelPersonNameBinding;
    }

    public RichInputText getApprovalCancelPersonNameBinding() {
        return approvalCancelPersonNameBinding;
    }

    public void setAccountCheckBinding(RichSelectBooleanCheckbox accountCheckBinding) {
        this.accountCheckBinding = accountCheckBinding;
    }

    public RichSelectBooleanCheckbox getAccountCheckBinding() {
        return accountCheckBinding;
    }

    public void setUnitNameBinding(RichInputText unitNameBinding) {
        this.unitNameBinding = unitNameBinding;
    }

    public RichInputText getUnitNameBinding() {
        return unitNameBinding;
    }

    public void setBindingOutputText(RichColumn bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichColumn getBindingOutputText() {
        return bindingOutputText;
    }

    public void popUpDialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
        {
        OperationBinding op = null;
        ADFUtils.findOperation("Delete").execute();
        op = (OperationBinding) ADFUtils.findOperation("Commit");
        if (getEditAction().equals("V")) {
        Object rst = op.execute();
        }
        System.out.println("Record Delete Successfully");
        if(op.getErrors().isEmpty()){
        FacesMessage Message = new FacesMessage("Record Deleted Successfully");
        Message.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        } else if (!op.getErrors().isEmpty())
        {
        OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
        Object rstr = opr.execute();
        FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
        Message.setSeverity(FacesMessage.SEVERITY_ERROR);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(leaveCancelAndVerificationTableBinding);
    }
    
    public void resetView()
    {
        System.out.println("Reset Viewwwwww");
       ADFUtils.findOperation("resetViewLeaveCancel").execute();
    }

    public void setAuthNameBinding(RichInputText authNameBinding) {
        this.authNameBinding = authNameBinding;
    }

    public RichInputText getAuthNameBinding() {
        return authNameBinding;
    }
}
