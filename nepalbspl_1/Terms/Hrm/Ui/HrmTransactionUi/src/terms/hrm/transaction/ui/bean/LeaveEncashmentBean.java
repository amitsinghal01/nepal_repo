package terms.hrm.transaction.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;


import javax.faces.validator.ValidatorException;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichPanelLabelAndMessage;

import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.binding.OperationBinding;

public class LeaveEncashmentBean {
   
    private String message="C";
    private RichInputText approvedByBinding;
    private RichInputDate approvedDateBinding;
    private RichInputText categoryCodeBinding;
    private RichInputText clAmountBinding;
    private RichInputText leaveClBinding;
    private RichInputText departmentCodeBinding;
    private RichInputText designationCodeBinding;
    private RichInputText elAmountBinding;
    private RichInputComboboxListOfValues unitCdBinding;
    private RichInputComboboxListOfValues empNoBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichInputText lvlcd;
    private RichInputText lvldes;
    private RichInputText totpl;
    private RichInputText leavepl;
    private RichInputText plamountbinding;
    private RichInputText slamountbinding;
    private RichInputText leaveEncashmentBinding;
    private RichOutputText bindingOutputtext;
    private RichInputText totalCLBinding;
    private RichInputText totalELBinding;
    private RichInputText totalSLBinding;
    private RichInputText leaveSlBinding;
    private RichInputText leaveElBinding;
    private RichInputText coaBinding;
    private RichInputDate leaveEncashDateBinding;

    public LeaveEncashmentBean() {
    }
    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }
    private void cevmodecheck(){
               if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
                   makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
                   cevModeDisableComponent("V");
               }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
                   makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                   cevModeDisableComponent("C");
               }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
                   makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                   cevModeDisableComponent("E");
               }
           } 
        public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                    try {
                        Method method1 =
                            component.getClass().getMethod("setDisabled", boolean.class);
                        if (method1 != null) {
                            method1.invoke(component, valueComponent);
                        }
                    } catch (NoSuchMethodException e) {
                        try {
                            Method method =
                                component.getClass().getMethod("setReadOnly", boolean.class);
                            if (method != null) {
                                method.invoke(component, valueComponent);
                            }
                        } catch (Exception e1) {
                            // e.printStackTrace();//silently eat this exception.
                        }


                        } catch (Exception e) {
                        // e.printStackTrace();//silently eat this exception.
                    }
                    List<UIComponent> childComponents = component.getChildren();
                    for (UIComponent comp : childComponents) {
                        makeComponentHierarchyReadOnly(comp, valueComponent);
                    }
                }
                //Set Fields and Button disable.
            public void cevModeDisableComponent(String mode) {
                if (mode.equals("E")) {
                getUnitCdBinding().setDisabled(true);
                getEmpNoBinding().setDisabled(true);
                getDesignationCodeBinding().setDisabled(true);
                getDepartmentCodeBinding().setDisabled(true);
                getLvlcd().setDisabled(true);
                getTotpl().setDisabled(true);
                getTotalCLBinding().setDisabled(true);
                getTotalELBinding().setDisabled(true);
                getTotalSLBinding().setDisabled(true);
                getLeavepl().setDisabled(true);
                getPlamountbinding().setDisabled(true);
                getClAmountBinding().setDisabled(true);
                getElAmountBinding().setDisabled(true);
                getSlamountbinding().setDisabled(true);
                getLeaveEncashmentBinding().setDisabled(true);
                getCoaBinding().setDisabled(true);
      
                } else if (mode.equals("C")) {
                    getUnitCdBinding().setDisabled(true);
                    getDesignationCodeBinding().setDisabled(true);
                    getDepartmentCodeBinding().setDisabled(true);
                    getLvlcd().setDisabled(true);
                    getTotpl().setDisabled(true);
                    getTotalCLBinding().setDisabled(true);
                    getTotalELBinding().setDisabled(true);
                    getTotalSLBinding().setDisabled(true);
                    //getLeavepl().setDisabled(true);
                    getPlamountbinding().setDisabled(true);
                    getClAmountBinding().setDisabled(true);
                    getElAmountBinding().setDisabled(true);
                    getSlamountbinding().setDisabled(true);
                    getLeaveEncashmentBinding().setDisabled(true);
                    getCoaBinding().setDisabled(true);
                } else if (mode.equals("V")) {  
                    
                }
            }

   
    public void saveButtonAL(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedBy("LeaveEncashmentVO1Iterator","LastUpdatedBy");
        System.out.println("In SaveAl After Call method");
        
        if(leaveEncashDateBinding.getValue()!=null){
        if (message.equalsIgnoreCase("C")){
            OperationBinding op2= ADFUtils.findOperation("Commit"); 
            System.out.println("In SaveAl create");
            op2.execute();
        FacesMessage Message = new FacesMessage("Record Save Successfully!");
        Message.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext fc=FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        }
       
            else
        {
                OperationBinding op3= ADFUtils.findOperation("Commit"); 
                System.out.println("In SaveAl edit");
                op3.execute();
            FacesMessage Message = new FacesMessage("Record Updated Successfully!");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc=FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
            }
        } 
        else{
            ADFUtils.showMessage("Leave Encashment Date is required.", 0);
            }
        
    }
    public void setApprovedByBinding(RichInputText approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputText getApprovedByBinding() {
        return approvedByBinding;
    }

    public void setApprovedDateBinding(RichInputDate approvedDateBinding) {
        this.approvedDateBinding = approvedDateBinding;
    }

    public RichInputDate getApprovedDateBinding() {
        return approvedDateBinding;
    }

    public void setCategoryCodeBinding(RichInputText categoryCodeBinding) {
        this.categoryCodeBinding = categoryCodeBinding;
    }

    public RichInputText getCategoryCodeBinding() {
        return categoryCodeBinding;
    }

    public void setClAmountBinding(RichInputText clAmountBinding) {
        this.clAmountBinding = clAmountBinding;
    }

    public RichInputText getClAmountBinding() {
        return clAmountBinding;
    }

    public void setLeaveClBinding(RichInputText leaveClBinding) {
        this.leaveClBinding = leaveClBinding;
    }

    public RichInputText getLeaveClBinding() {
        return leaveClBinding;
    }

    public void setDepartmentCodeBinding(RichInputText departmentCodeBinding) {
        this.departmentCodeBinding = departmentCodeBinding;
    }

    public RichInputText getDepartmentCodeBinding() {
        return departmentCodeBinding;
    }

    public void setDesignationCodeBinding(RichInputText designationCodeBinding) {
        this.designationCodeBinding = designationCodeBinding;
    }
    public RichInputText getDesignationCodeBinding() {
        return designationCodeBinding;
    }

    public void setElAmountBinding(RichInputText elAmountBinding) {
        this.elAmountBinding = elAmountBinding;
    }

    public RichInputText getElAmountBinding() {
        return elAmountBinding;
    }

    public void setUnitCdBinding(RichInputComboboxListOfValues unitCdBinding) {
        this.unitCdBinding = unitCdBinding;
    }

    public RichInputComboboxListOfValues getUnitCdBinding() {
        return unitCdBinding;
    }

    public void setEmpNoBinding(RichInputComboboxListOfValues empNoBinding) {
        this.empNoBinding = empNoBinding;
    }

    public RichInputComboboxListOfValues getEmpNoBinding() {
        return empNoBinding;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void empCodeVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        vce.getNewValue();
        String unitCd=(String) getUnitCdBinding().getValue();
        System.out.println("Unit Value is-----.."+unitCd);
        String empcd=(String) vce.getNewValue();
        System.out.println("Emp Value is-----.."+empcd);
        OperationBinding op=ADFUtils.findOperation("empdetailvalueforleaveencash");
        op.getParamsMap().put("unitCd", unitCd);
        op.getParamsMap().put("empcd", empcd);
        op.execute();    
        
        //FOR PROCEDURE OFFICER
        System.out.println("In levelDescriptionVCL");
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        vce.getNewValue();
        System.out.println("In levelDescriptionVCL 2");
        String unit=(String) getUnitCdBinding().getValue();
        System.out.println(unit);
        String lvcd=(String) getLvlcd().getValue();
        System.out.println(lvcd);
        String lvdes=(String) getLvldes().getValue();
        System.out.println(lvdes);
        String epcd=(String) getEmpNoBinding().getValue();
        System.out.println(epcd);
        if (unit != null && lvcd != null && lvdes !=null && epcd !=null) {
            if(lvdes.equalsIgnoreCase("Officer")){
                       System.out.println("Before Call Procedure");
                       OperationBinding op1=(OperationBinding)ADFUtils.findOperation("CallProcedureForLeaveEncashforofficer");
                       System.out.println("After Call Procedure");            
                       op1.getParamsMap().put("lvcd", lvcd);
                       op1.getParamsMap().put("lvdes", lvdes);
                       op1.getParamsMap().put("epcd", epcd);
                       //op1.getParamsMap().put("unit", unit);
                       op1.execute();
                        }
            System.out.println("After Call Procedure1");
            //FOR PROCEDURE STAFF
//            String lvcd2=(String) getLvlcd().getValue();
//            String lvdes2=(String) getLvldes().getValue();
//            String epcd2=(String) getEmpNoBinding().getValue();
//            System.out.println(lvcd2);
//            System.out.println(lvdes2);
//            System.out.println(epcd2);
          if(lvdes.equalsIgnoreCase("Staff")){
                  OperationBinding op2=(OperationBinding)ADFUtils.findOperation("CallProcedureForLeaveEncashforstaff");                      
                  op2.getParamsMap().put("lvcd", lvcd);
                  op2.getParamsMap().put("lvdes", lvdes);
                  op2.getParamsMap().put("epcd", epcd);                      
                  op2.execute();    
                   }
          System.out.println("After Call Procedure2");   
           }
    }
    public void levelDescriptionVCL(ValueChangeEvent vce) { 
    }

    public void setLvlcd(RichInputText lvlcd) {
        this.lvlcd = lvlcd;
    }

    public RichInputText getLvlcd() {
        return lvlcd;
    }

    public void setLvldes(RichInputText lvldes) {
        this.lvldes = lvldes;
    }

    public RichInputText getLvldes() {
        return lvldes;
    }

    public void setTotpl(RichInputText totpl) {
        this.totpl = totpl;
    }

    public RichInputText getTotpl() {
        return totpl;
    }

    public void setLeavepl(RichInputText leavepl) {
        this.leavepl = leavepl;
    }

    public RichInputText getLeavepl() {
        return leavepl;
    }

    public void eLAmountVCL(ValueChangeEvent valueChangeEvent) {
        System.out.println("in vcl change");
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        valueChangeEvent.getNewValue();
        BigDecimal elam =(BigDecimal) (elAmountBinding.getValue() != null ? elAmountBinding.getValue() : new BigDecimal(0));
        BigDecimal plam=(BigDecimal) (plamountbinding.getValue() !=null ? plamountbinding.getValue() : new BigDecimal(0));
        BigDecimal clam=(BigDecimal) (clAmountBinding.getValue() !=null ? clAmountBinding.getValue() : new BigDecimal(0));
        BigDecimal slam=(BigDecimal) (slamountbinding.getValue() !=null ? slamountbinding.getValue() : new BigDecimal(0));
        BigDecimal summ;
        summ=elam.add(plam.add(clam).add(slam));
        System.out.println(summ);
        leaveEncashmentBinding.setValue(summ);
    }

    public void setPlamountbinding(RichInputText plamountbinding) {
        this.plamountbinding = plamountbinding;
    }

    public RichInputText getPlamountbinding() {
        return plamountbinding;
    }

    public void setSlamountbinding(RichInputText slamountbinding) {
        this.slamountbinding = slamountbinding;
    }

    public RichInputText getSlamountbinding() {
        return slamountbinding;
    }

    public void setLeaveEncashmentBinding(RichInputText leaveEncashmentBinding) {
        this.leaveEncashmentBinding = leaveEncashmentBinding;
    }

    public RichInputText getLeaveEncashmentBinding() {
        return leaveEncashmentBinding;
    }

    public void setBindingOutputtext(RichOutputText bindingOutputtext) {
        this.bindingOutputtext = bindingOutputtext;
    }

    public RichOutputText getBindingOutputtext() {
        cevmodecheck();
        return bindingOutputtext;
    }

    public void eLLeaveValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if(object!=null){
        System.out.println("In leaveValidator: "+object);
        System.out.println("LeaveEL: "+leaveElBinding.getValue()+"LeavePL: "+leavepl.getValue()+"LeaveCl: "+leaveClBinding.getValue()+"LeaveSL: "+leaveSlBinding.getValue());
        OperationBinding op= ADFUtils.findOperation("allvalidationonleaveencashment");
        op.getParamsMap().put("type", "EL");
        op.getParamsMap().put("leave", object);
        Object rst=op.execute();
        System.out.println("Result: "+rst);
        if(rst!=null){
            String result=(String)rst;
            result=result.substring(0,1);
            System.out.println("Result is after substring: "+result);
            if(result.equalsIgnoreCase("E")){
              throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR ,rst.toString().substring(2),null));
            }
        }
        }
    }
    public void pLLeaveValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if(object!=null){
        System.out.println("In leaveValidator: "+object);
        OperationBinding op= ADFUtils.findOperation("allvalidationonleaveencashment");
        op.getParamsMap().put("type", "PL");
        op.getParamsMap().put("leave", object);
        Object rst=op.execute();
        System.out.println("Result: "+rst);
        if(rst!=null){
            String result=(String)rst;
            result=result.substring(0,1);
            System.out.println("Result is after substring: "+result);
            if(result.equalsIgnoreCase("E")){
              throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR ,rst.toString().substring(2),null));
            }
        }
        }
    }
    public void cLLeaveValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if(object!=null){
        System.out.println("In leaveValidator: "+object);
        OperationBinding op= ADFUtils.findOperation("allvalidationonleaveencashment");
        op.getParamsMap().put("type", "CL");
        op.getParamsMap().put("leave", object);
        Object rst=op.execute();
        System.out.println("Result: "+rst);
        if(rst!=null){
            String result=(String)rst;
            result=result.substring(0,1);
            System.out.println("Result is after substring: "+result);
            if(result.equalsIgnoreCase("E")){
              throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR ,rst.toString().substring(2),null));
            }
        }
        }
    }
    public void sLLeaveValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if(object!=null){
        System.out.println("In leaveValidator: "+object);
        OperationBinding op= ADFUtils.findOperation("allvalidationonleaveencashment");
        op.getParamsMap().put("type", "SL");
        op.getParamsMap().put("leave", object);
        Object rst=op.execute();
        System.out.println("Result: "+rst);
        if(rst!=null){
            String result=(String)rst;
            result=result.substring(0,1);
            System.out.println("Result is after substring: "+result);
            if(result.equalsIgnoreCase("E")){
              throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR ,rst.toString().substring(2),null));
            }
            }
        }
    }

    public void setTotalCLBinding(RichInputText totalCLBinding) {
        this.totalCLBinding = totalCLBinding;
    }

    public RichInputText getTotalCLBinding() {
        return totalCLBinding;
    }

    public void setTotalELBinding(RichInputText totalELBinding) {
        this.totalELBinding = totalELBinding;
    }

    public RichInputText getTotalELBinding() {
        return totalELBinding;
    }

    public void setTotalSLBinding(RichInputText totalSLBinding) {
        this.totalSLBinding = totalSLBinding;
    }

    public RichInputText getTotalSLBinding() {
        return totalSLBinding;
    }

    public void setLeaveSlBinding(RichInputText leaveSlBinding) {
        this.leaveSlBinding = leaveSlBinding;
    }

    public RichInputText getLeaveSlBinding() {
        return leaveSlBinding;
    }

    public void setLeaveElBinding(RichInputText leaveElBinding) {
        this.leaveElBinding = leaveElBinding;
    }

    public RichInputText getLeaveElBinding() {
        return leaveElBinding;
    }

    public void setCoaBinding(RichInputText coaBinding) {
        this.coaBinding = coaBinding;
    }

    public RichInputText getCoaBinding() {
        return coaBinding;
    }

    public void setLeaveEncashDateBinding(RichInputDate leaveEncashDateBinding) {
        this.leaveEncashDateBinding = leaveEncashDateBinding;
    }

    public RichInputDate getLeaveEncashDateBinding() {
        return leaveEncashDateBinding;
    }
}
