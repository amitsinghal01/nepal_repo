package terms.hrm.transaction.ui.bean;

import com.tangosol.coherence.component.net.management.gateway.Local;

import java.math.BigDecimal;

import java.sql.Date;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.time.LocalDate;

import java.time.format.DateTimeFormatter;

import java.time.format.DateTimeParseException;

import java.util.Calendar;

//import java.util.Date;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;

//import oracle.jbo.domain.Date;

public class LeaveForwardingBean {
    private RichInputText startYearBinding;
    private RichInputDate startDateBinding;
    private RichInputDate calculateTransBinding;
    private RichInputText calTransBinding;
    private RichInputDate dateTransBinding;

    public LeaveForwardingBean() {
    }

    public void startYearVCL(ValueChangeEvent vce) {
        System.out.println("INSIDE startYearVCL #### ");
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        
        String year1 = "1";
        String year = (String) startYearBinding.getValue();
        System.out.println("Value of Year Entered: "+year);
        
        String calculate = (String) calTransBinding.getValue();
        System.out.println("Value of calculate: "+calculate);
        
        String sub = calculate.substring(0, 4);
        System.out.println("Value of sub: "+sub);
        
        BigDecimal calc = new BigDecimal(sub);
        BigDecimal calc1 = new BigDecimal(1);
        BigDecimal add1 = calc.add(calc1);
        System.out.println("Value of add1: "+add1);

        oracle.jbo.domain.Date dateTrans = (oracle.jbo.domain.Date)dateTransBinding.getValue();
        java.util.Date d=new java.util.Date();
        Calendar c = Calendar.getInstance();
        c.setTime(d);
        int yearZ = c.get(Calendar.YEAR);
        //String years = String.valueOf(yearZ+1);
        String years = String.valueOf(yearZ);
        
        System.out.println("Value of yearZ: "+yearZ);
        System.out.println("Value of years: "+years);
//        oracle.jbo.domain.Date dateTrans1 = (oracle.jbo.domain.Date)dateTrans.subs;
//        String year1 = "1";
//        String year2 = year.;
        
        System.out.println("YEAR #### " + yearZ);
        Calendar cal = Calendar.getInstance();
        System.out.println("Calendar Value #### " + cal + "Calculate " + calculate + "Sub " + sub + "Add1" + add1 + "years" + yearZ);
//        cal.add(Calendar.YEAR,1);
//        System.out.println(cal.add(Calendar.YEAR,1));                
//        
        if(vce != null) {
            try {
                
                String string = "01/" + "JAN/" + year;
               // String string = "01/" + "JAN/" + years;
                DateFormat df=new SimpleDateFormat("dd/MMM/yyyy");
                java.util.Date d1 = df.parse(string);
                java.sql.Date sqlDate= new java.sql.Date(d1.getTime());
                oracle.jbo.domain.Date dt=new oracle.jbo.domain.Date(sqlDate);
//                DateTimeFormatter format=DateTimeFormatter.ofPattern("dd/MM/yyyy");
//                try {
//                    DateFormat formatter;
//                    java.util.Date date;
//                    
//                    formatter = new SimpleDateFormat("dd/MM/yyyy");
//                    date = formatter.parse(string);
//                    
////                    LocalDate dt=getDateFromString(string, format);
//                    System.out.println("START DATE #### " + string);
//                }
//                catch(IllegalArgumentException e)
//                {
//                System.out.println("Exception e"+ e);
//                }
//                catch(DateTimeParseException es)
//                {
//                System.out.println("Exception es"+ es);
//                }
//                DateFormat formatter = new SimpleDateFormat("dd/MMM/yyyy");
//                Date date=new Date(strdate);
//                Date date = (Date)formatter.parse(str_date);
//                java.util.Date utilDate = new java.util.Date();
//                java.sql.Date sqlDate = new java.sql.Date(date.getTime());
                    
//                Date dt1 = new SimpleDateFormat("dd/MMM/yyyy").parse(str_date);
//                oracle.jbo.domain.Date jboDate = new oracle.jbo.domain.Date(sqlDate);
                System.out.println("dt " + dt);
                
                startDateBinding.setValue(dt);
            }
            catch(Exception e) {
                e.printStackTrace();
            }
        }
    }
    public LocalDate getDateFromString(String string,DateTimeFormatter format)
    {
        LocalDate dt=LocalDate.parse(string, format);
        return dt;
    }

    public void setStartYearBinding(RichInputText startYearBinding) {
        this.startYearBinding = startYearBinding;
    }

    public RichInputText getStartYearBinding() {
        return startYearBinding;
    }

    public void setStartDateBinding(RichInputDate startDateBinding) {
        this.startDateBinding = startDateBinding;
    }

    public RichInputDate getStartDateBinding() {
        return startDateBinding;
    }

    public void generateLeaveForwardPopulateAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("generateLeaveForwardForLeaveForwarding").execute();
    }

    public void setCalculateTransBinding(RichInputDate calculateTransBinding) {
        this.calculateTransBinding = calculateTransBinding;
    }

    public RichInputDate getCalculateTransBinding() {
        return calculateTransBinding;
    }

    public void setCalTransBinding(RichInputText calTransBinding) {
        this.calTransBinding = calTransBinding;
    }

    public RichInputText getCalTransBinding() {
        return calTransBinding;
    }

    public void setDateTransBinding(RichInputDate dateTransBinding) {
        this.dateTransBinding = dateTransBinding;
    }

    public RichInputDate getDateTransBinding() {
        return dateTransBinding;
    }
}
