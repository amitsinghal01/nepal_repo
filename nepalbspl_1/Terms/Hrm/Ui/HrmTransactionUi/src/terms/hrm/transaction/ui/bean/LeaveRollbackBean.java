package terms.hrm.transaction.ui.bean;

import java.util.Date;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectBooleanCheckbox;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;

import oracle.binding.OperationBinding;

import oracle.jbo.JboException;
import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;

public class LeaveRollbackBean {
    private RichSelectOneChoice leaveGrantedBinding;
    private RichInputDate fromDateBinding;
    private RichInputDate endDateBinding;
    private RichInputDate cancelFromDtBinding;
    private RichInputDate cancelToDtBinding;
    private RichInputText approvalPersonCdBinding;
    private RichInputText approvalPersonNameBinding;
    private RichSelectBooleanCheckbox approvalCheckBinding;
    private RichInputComboboxListOfValues unitCodeBinding;

    public LeaveRollbackBean() {
    }

    public void populateLeaveRollbackData(ActionEvent actionEvent) {
        String val=(String)(ADFUtils.evaluateEL("#{pageFlowScope.empCode}")!=null?ADFUtils.evaluateEL("#{pageFlowScope.empCode}"):"SWE161");
        OperationBinding op=ADFUtils.findOperation("leaveRollbackPopulateData");
        op.getParamsMap().put("appAuth", val);
        op.execute();
    }

    public void setLeaveGrantedBinding(RichSelectOneChoice leaveGrantedBinding) {
        this.leaveGrantedBinding = leaveGrantedBinding;
    }

    public RichSelectOneChoice getLeaveGrantedBinding() {
        return leaveGrantedBinding;
    }

    public void setFromDateBinding(RichInputDate fromDateBinding) {
        this.fromDateBinding = fromDateBinding;
    }

    public RichInputDate getFromDateBinding() {
        return fromDateBinding;
    }

    public void setEndDateBinding(RichInputDate endDateBinding) {
        this.endDateBinding = endDateBinding;
    }

    public RichInputDate getEndDateBinding() {
        return endDateBinding;
    }

    public void saveLeaveRollbackDataAL(ActionEvent actionEvent) {
        OperationBinding op=ADFUtils.findOperation("leaveRollbackSaveAction");
                op.execute();
                System.out.println("Result after function call: "+op.getResult());
                if(op.getResult()!=null){
                if(op.getResult().equals("NR")){
                    ADFUtils.showMessage("Please Select the Approval check box for Leave Rollback!",0);
                    }
                else if(op.getResult().equals("Y")){
                    ADFUtils.showMessage("Selected Leave Rollbacked Successfully!",2);
                    }
                else {
                    ADFUtils.showMessage(op.getResult().toString(),0);
                    }
                }
    }

    public void rollbackLeaveDataAL(ActionEvent actionEvent) {
        DCIteratorBinding dci = ADFUtils.findIterator("LeaveRollbackDtlVO1Iterator");
        RowSetIterator rsi=dci.getViewObject().createRowSetIterator(null);
            while(rsi.hasNext()) {
                Row r = rsi.next();
               r.remove();
            }
            rsi.closeRowSetIterator();
    }

    public void setCancelFromDtBinding(RichInputDate cancelFromDtBinding) {
        this.cancelFromDtBinding = cancelFromDtBinding;
    }

    public RichInputDate getCancelFromDtBinding() {
        return cancelFromDtBinding;
    }

    public void setCancelToDtBinding(RichInputDate cancelToDtBinding) {
        this.cancelToDtBinding = cancelToDtBinding;
    }

    public RichInputDate getCancelToDtBinding() {
        return cancelToDtBinding;
    }

    public void setApprovalPersonCdBinding(RichInputText approvalPersonCdBinding) {
        this.approvalPersonCdBinding = approvalPersonCdBinding;
    }

    public RichInputText getApprovalPersonCdBinding() {
        return approvalPersonCdBinding;
    }

    public void setApprovalPersonNameBinding(RichInputText approvalPersonNameBinding) {
        this.approvalPersonNameBinding = approvalPersonNameBinding;
    }

    public RichInputText getApprovalPersonNameBinding() {
        return approvalPersonNameBinding;
    }

    public void setApprovalCheckBinding(RichSelectBooleanCheckbox approvalCheckBinding) {
        this.approvalCheckBinding = approvalCheckBinding;
    }

    public RichSelectBooleanCheckbox getApprovalCheckBinding() {
        return approvalCheckBinding;
    }

    public void approvalCheckVCL(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
                if(valueChangeEvent!=null){
                    String val=(String)(ADFUtils.evaluateEL("#{pageFlowScope.empCode}")!=null?ADFUtils.evaluateEL("#{pageFlowScope.empCode}"):"SWE161");
                System.out.println("Unit Code: "+unitCodeBinding.getValue());
                    OperationBinding op = ADFUtils.findOperation("checkApprovalAuthority");
                    op.getParamsMap().put("formName", "LRB");     
                    op.getParamsMap().put("unitCd", unitCodeBinding.getValue());
                    op.getParamsMap().put("authLevel", "CN");
                    //op.getParamsMap().put("emp_code", ADFUtils.evaluateEL("#{pageFlowScope.empCode}"));
                    op.getParamsMap().put("empCode", "SWE161");
                    op.execute();
                    System.out.println("Result after Approval Check: "+op.getResult());
                    if(op.getResult()!=null && op.getResult().equals("Y")){
                        approvalPersonCdBinding.setValue(val);   
                        }
                    else{
                        ADFUtils.showMessage("You don't have Authority to Cancel this Leave.", 0);
                        approvalPersonCdBinding.setValue(null);
                        approvalCheckBinding.setValue(null);
                        }
                }
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }
}
