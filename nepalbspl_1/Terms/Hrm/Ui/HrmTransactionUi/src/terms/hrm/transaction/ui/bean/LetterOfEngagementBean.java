package terms.hrm.transaction.ui.bean;



import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import javax.sql.DataSource;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.naming.InitialContext;
import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;

import java.sql.Connection;

import java.sql.SQLException;

import java.util.Hashtable;

import javax.naming.Context;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;

import net.sf.jasperreports.engine.util.JRLoader;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.binding.OperationBinding;

import oracle.adf.model.BindingContext;

import oracle.binding.BindingContainer;

public class LetterOfEngagementBean {
    private RichTable letterOfEngagementTableBinding;
    private String editAction="V";

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public LetterOfEngagementBean() {
    }

    public void setLetterOfEngagementTableBinding(RichTable letterOfEngagementTableBinding) {
        this.letterOfEngagementTableBinding = letterOfEngagementTableBinding;
        
    }

    public RichTable getLetterOfEngagementTableBinding() {
        return letterOfEngagementTableBinding;
    }

    public void popUpDialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
        {
        OperationBinding op = null;
        ADFUtils.findOperation("Delete").execute();
        op = (OperationBinding) ADFUtils.findOperation("Commit");
        if (getEditAction().equals("V")) {
        Object rst = op.execute();
        }
        System.out.println("Record Delete Successfully");
        if(op.getErrors().isEmpty()){
        FacesMessage Message = new FacesMessage("Record Deleted Successfully");
        Message.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        } else if (!op.getErrors().isEmpty())
        {
        OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
        Object rstr = opr.execute();
        FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
        Message.setSeverity(FacesMessage.SEVERITY_ERROR);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(letterOfEngagementTableBinding);
    }

    public void saveButtonEntryCodeAL(ActionEvent actionEvent) {         
        OperationBinding op =
            (OperationBinding) ADFUtils.findOperation("generateEntryNumberLetterEngagement");
        op.execute();
        System.out.println("Result Save"+op.getResult());
        if(op.getResult()!=null && op.getResult().toString().equalsIgnoreCase("N"))
        {
           if (op.getErrors().isEmpty())
           {
               ADFUtils.findOperation("Commit").execute();
               FacesMessage Message = new FacesMessage("Record Update Successfully.");
               Message.setSeverity(FacesMessage.SEVERITY_INFO);
               FacesContext fc = FacesContext.getCurrentInstance();
               fc.addMessage(null, Message);
           }
        }
        else if (op.getResult() != null && op.getResult().toString() != "")
        {
          if (op.getErrors().isEmpty())
          {
                System.out.println("CREATE MODE");
                ADFUtils.findOperation("Commit").execute();
                FacesMessage Message = new FacesMessage("Record Save Successfully.Letter Of Engagement Number is "+op.getResult());
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
          }
        }
    }
    
    public BindingContainer getBindings()
      {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
      }
    
    public void downloadJrxml(FacesContext facesContext, OutputStream outputStream) {
        Connection conn=null;
        try {
            String file_name ="r_engage_letter.jasper";
            OperationBinding binding = ADFUtils.findOperation("fileNameForPrint");
            binding.getParamsMap().put("fileId","HRM0000000203");
            binding.execute();
            System.out.println("Binding Result :"+binding.getResult());
            if(binding.getResult() != null){ 
               
                DCIteratorBinding pvIter = (DCIteratorBinding) getBindings().get("LetterOfEngagementVO1Iterator");  
                String result = binding.getResult().toString();
                int last_index = result.lastIndexOf("/");
                if(last_index==-1)
                {
                    last_index=result.lastIndexOf("\\");
                }
                    String path = result.substring(0,last_index+1)+file_name;
                    System.out.println("FILE PATH IS===>"+path);
                InputStream input = new FileInputStream(path);
                Map n = new HashMap();
                n.put("p_unit",  pvIter.getCurrentRow().getAttribute("UnitCd"));
//                n.put("p_month", pvIter.getCurrentRow().getAttribute("Month"));
//                n.put("p_year", pvIter.getCurrentRow().getAttribute("Year"));
                n.put("p_entry_no", pvIter.getCurrentRow().getAttribute("EntryNo"));
//                n.put("p_lvl", pvIter.getCurrentRow().getAttribute("DesigLvlMastLevel"));
//                n.put("p_desig", pvIter.getCurrentRow().getAttribute("Month"));
//                n.put("p_cont", pvIter.getCurrentRow().getAttribute("Year"));
                conn = getConnection( );
                
                JasperReport design = (JasperReport) JRLoader.loadObject(input);
                System.out.println("Path : " + input + " -------" + design+" param:"+n);
                
                @SuppressWarnings("unchecked")
                net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                byte pdf[] = JasperExportManager.exportReportToPdf(print);
                HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                response.getOutputStream().write(pdf);
                response.getOutputStream().flush();
                response.getOutputStream().close();
                facesContext.responseComplete();
                }else
                System.out.println("File Name/File Path not found.");
                } catch (FileNotFoundException fnfe) {
                // TODO: Add catch code
                fnfe.printStackTrace();
                try {
                    System.out.println("in finally connection closed");
                            conn.close( );
                            conn = null;
                    
                } catch( SQLException e ) {
                    e.printStackTrace( );
                }
                } catch (Exception e) {
                // TODO: Add catch code
                e.printStackTrace();
                try {
                    System.out.println("in finally connection closed");
                            conn.close( );
                            conn = null;
                    
                } catch( SQLException e1 ) {
                    e1.printStackTrace( );
                }
                }finally {
                        try {
                                System.out.println("in finally connection closed");
                                        conn.close( );
                                        conn = null;
                                
                        } catch( SQLException e ) {
                                e.printStackTrace( );
                        }
                }

    }
    
    public Connection getConnection() throws Exception{
            Context ctx = null;
            Connection conn = null;
            ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/ApplicationDBDS");
            conn = ds.getConnection();
            return conn;
        }
    
}
