package terms.hrm.transaction.ui.bean;

import java.io.OutputStream;
import javax.naming.InitialContext;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;

import java.sql.Connection;

import java.sql.SQLException;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;

import javax.faces.application.Application;
import javax.faces.context.FacesContext;

import javax.naming.InitialContext;

import javax.servlet.http.HttpServletResponse;

import javax.sql.DataSource;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;
import javax.faces.context.FacesContext;

import javax.naming.Context;

public class ListofAbsenteeismfortheperiodReportBean {
    public ListofAbsenteeismfortheperiodReportBean() {
    }
    public BindingContainer getBindings()
      {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
      }
    public void downloadJrxml(FacesContext facesContext, OutputStream outputStream) {
        Connection conn=null;
        try {
                    String file_name ="r_absentism_regist_for_period.jasper";
                    OperationBinding binding = ADFUtils.findOperation("fileNameForPrint");
                    binding.getParamsMap().put("fileId","ERP0000001935");
                    binding.execute();
                    System.out.println("Binding Result :"+binding.getResult());
                    if(binding.getResult() != null){ 
                       
                        DCIteratorBinding pvIter = (DCIteratorBinding) getBindings().get("ListofAbsenteeismfortheperiodReportVVO1Iterator");  
                        String result = binding.getResult().toString();
                        int last_index = result.lastIndexOf("/");
                        if(last_index==-1)
                        {
                            last_index=result.lastIndexOf("\\");
                        }
                            String path = result.substring(0,last_index+1)+file_name;
                            System.out.println("FILE PATH IS===>"+path);
                        InputStream input = new FileInputStream(path);
            //                 String Cust =(String)pvIter.getCurrentRow().getAttribute("CustCode");
                       System.out.println(pvIter.getCurrentRow().getAttribute("empcd"));
                        System.out.println(pvIter.getCurrentRow().getAttribute("Month"));
                        System.out.println(pvIter.getCurrentRow().getAttribute("Year"));
                        System.out.println(pvIter.getCurrentRow().getAttribute("Unitcd"));
                       
                        Map n = new HashMap();
                        n.put("p_emp", pvIter.getCurrentRow().getAttribute("empcd"));
                        n.put("p_month", pvIter.getCurrentRow().getAttribute("Month"));
                        n.put("p_year", pvIter.getCurrentRow().getAttribute("Year"));
                        n.put("p_unit",  pvIter.getCurrentRow().getAttribute("Unitcd"));
                        conn = getConnection( );
                        
                        JasperReport design = (JasperReport) JRLoader.loadObject(input);
                        System.out.println("Path : " + input + " -------" + design+" param:"+n);
                        
                        @SuppressWarnings("unchecked")
                        net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                        byte pdf[] = JasperExportManager.exportReportToPdf(print);
                        HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                        response.getOutputStream().write(pdf);
                        response.getOutputStream().flush();
                        response.getOutputStream().close();
                        facesContext.responseComplete();
                        }else
                        System.out.println("File Name/File Path not found.");
                        } catch (FileNotFoundException fnfe) {
                        // TODO: Add catch code
                        fnfe.printStackTrace();
                        try {
                            System.out.println("in finally connection closed");
                                    conn.close( );
                                    conn = null;
                            
                        } catch( SQLException e ) {
                            e.printStackTrace( );
                        }
                        } catch (Exception e) {
                        // TODO: Add catch code
                        e.printStackTrace();
                        try {
                            System.out.println("in finally connection closed");
                                    conn.close( );
                                    conn = null;
                            
                        } catch( SQLException e1 ) {
                            e1.printStackTrace( );
                        }
                        }finally {
                                try {
                                        System.out.println("in finally connection closed");
                                                conn.close( );
                                                conn = null;
                                        
                                } catch( SQLException e ) {
                                        e.printStackTrace( );
                                }
                        }
    }

    public Connection getConnection() throws Exception{
            Context ctx = null;
            Connection conn = null;
            ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/ApplicationDBDS");
            conn = ds.getConnection();
            return conn;
        }
}
