package terms.hrm.transaction.ui.bean;

import com.tangosol.internal.sleepycat.je.utilint.Timestamp;

import java.lang.reflect.Method;

import java.text.SimpleDateFormat;

import java.time.LocalDateTime;

import java.time.Month;

//import java.util.Date;
import oracle.jbo.domain.Date;
import java.util.List;

import java.util.SimpleTimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import javax.faces.validator.ValidatorException;

import oracle.adf.share.ADFContext;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;

import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;

import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;

import oracle.adf.view.rich.component.rich.nav.RichButton;

import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

public class LoanBean {
    private RichInputComboboxListOfValues bindUnitCode;
    private RichInputText bindAmountOfEachInstallment;
    private RichInputText bindRestAmountToBePaid;
    private RichInputText bindTotalNumberOfInstallmentsPaid;
    private RichInputText bindTotalAmountPaid;
    private RichInputText bindRestInstallmentToBePaid;
    private RichInputText bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichInputText bindUnitName;
    private RichSelectOneChoice bindStatus;
    private RichSelectOneChoice bindFreezedBy;
    private RichInputDate bindLastInstallmentPaidMonthYear;
    private RichInputDate bindLoanDate;
    private RichInputDate bindDeductionStartMonthYear;
    private RichInputText bindLoanNumber;
    private RichInputComboboxListOfValues bindEmployeeNumber;
    private RichInputText bindLoanTotalAmount;
    private RichInputText bindTotalNumberOfInstallments;
    private RichInputText bindLoanMasterId;
    private RichInputText bindEmployeeName;
    private RichButton detailCreateBinding;
    private RichButton detailDeleteBinding;

    public LoanBean() {
    }

    public void LoanSaveAl(ActionEvent actionEvent) {
        
        ADFUtils.setLastUpdatedBy("LoanPrePaymentVO1Iterator","LastUpdatedBy");
        OperationBinding op = ADFUtils.findOperation("getLoanNo");
        Object rst = op.execute();
        
        System.out.println("Result is===> "+op.getResult());
        
        
        System.out.println("--------Commit-------");
            System.out.println("value aftr getting result--->?"+rst);
            
        
            if (rst.toString() != null && rst.toString() != "" && rst.toString()!="N") {
                if (op.getErrors().isEmpty()) {
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message = new FacesMessage("Record Saved Successfully. New loan No. is "+rst+".");   
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                    FacesContext fc = FacesContext.getCurrentInstance();   
                    fc.addMessage(null, Message);  
        
                }
            }
            
            if (rst.toString().equalsIgnoreCase("N")) {
                if (op.getErrors().isEmpty()) {
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message = new FacesMessage("Record Update Successfully.");   
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                    FacesContext fc = FacesContext.getCurrentInstance();   
                    fc.addMessage(null, Message);  
            
                }

            
            }
    }


    private void cevmodecheck(){
         if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
             makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
             cevModeDisableComponent("V");
         }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
             makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
             cevModeDisableComponent("C");
         }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
             makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
             cevModeDisableComponent("E");
         }
     }

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
              try {
                  Method method1 =
                      component.getClass().getMethod("setDisabled", boolean.class);
                  if (method1 != null) {
                      method1.invoke(component, valueComponent);
                  }
              } catch (NoSuchMethodException e) {
                  try {
                      Method method =
                          component.getClass().getMethod("setReadOnly", boolean.class);
                      if (method != null) {
                          method.invoke(component, valueComponent);
                      }
                  } catch (Exception e1) {
                      // e.printStackTrace();//silently eat this exception.
                  }


              } catch (Exception e) {
                  // e.printStackTrace();//silently eat this exception.
              }
              List<UIComponent> childComponents = component.getChildren();
              for (UIComponent comp : childComponents) {
                  makeComponentHierarchyReadOnly(comp, valueComponent);
              }
          }
    
    
    //Set Fields and Button disable.
        public void cevModeDisableComponent(String mode) {
        //            FacesContext fctx = FacesContext.getCurrentInstance();
        //            ELContext elctx = fctx.getELContext();
        //            Application jsfApp = fctx.getApplication();
        //            //create a ValueExpression that points to the ADF binding layer
        //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
        //            //
        //            ValueExpression valueExpr = exprFactory.createValueExpression(
        //                                         elctx,
        //                                         "#{pageFlowScope.mode=='E'}",
        //                                          Object.class
        //                                         );
        //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
            if (mode.equals("E")) {
                getBindUnitCode().setDisabled(true);
                getBindUnitName().setDisabled(true);
                getBindAmountOfEachInstallment().setDisabled(true);
                getBindRestAmountToBePaid().setDisabled(true);
                getBindTotalNumberOfInstallmentsPaid().setDisabled(true);
                getBindTotalAmountPaid().setDisabled(true);
                getBindRestInstallmentToBePaid().setDisabled(true);
                getBindStatus().setDisabled(false);
                getBindFreezedBy().setDisabled(false);
                getBindLastInstallmentPaidMonthYear().setDisabled(true);
                getBindLoanNumber().setDisabled(true);
                getBindLoanDate().setDisabled(true);
                getBindDeductionStartMonthYear().setDisabled(true);
                getBindEmployeeNumber().setDisabled(true);
            } else if (mode.equals("C")) {
                 getBindUnitCode().setDisabled(true);
                  getBindUnitName().setDisabled(true);
                  getBindAmountOfEachInstallment().setDisabled(true);
                  getBindRestAmountToBePaid().setDisabled(true);
                  getBindTotalNumberOfInstallmentsPaid().setDisabled(true);
                  getBindTotalAmountPaid().setDisabled(true);
                  getBindRestInstallmentToBePaid().setDisabled(true);
                  getBindStatus().setDisabled(true);
                  getBindFreezedBy().setDisabled(true);
                  getBindLastInstallmentPaidMonthYear().setDisabled(true);
                getBindLoanNumber().setDisabled(true);
            } else if (mode.equals("V")) {
               
            }
            
        }

    public void setBindUnitCode(RichInputComboboxListOfValues bindUnitCode) {
        this.bindUnitCode = bindUnitCode;
    }

    public RichInputComboboxListOfValues getBindUnitCode() {
        return bindUnitCode;
    }

    public void setBindAmountOfEachInstallment(RichInputText bindAmountOfEachInstallment) {
        this.bindAmountOfEachInstallment = bindAmountOfEachInstallment;
    }

    public RichInputText getBindAmountOfEachInstallment() {
        return bindAmountOfEachInstallment;
    }

    public void setBindRestAmountToBePaid(RichInputText bindRestAmountToBePaid) {
        this.bindRestAmountToBePaid = bindRestAmountToBePaid;
    }

    public RichInputText getBindRestAmountToBePaid() {
        return bindRestAmountToBePaid;
    }

    public void setBindTotalNumberOfInstallmentsPaid(RichInputText bindTotalNumberOfInstallmentsPaid) {
        this.bindTotalNumberOfInstallmentsPaid = bindTotalNumberOfInstallmentsPaid;
    }

    public RichInputText getBindTotalNumberOfInstallmentsPaid() {
        return bindTotalNumberOfInstallmentsPaid;
    }

    public void setBindTotalAmountPaid(RichInputText bindTotalAmountPaid) {
        this.bindTotalAmountPaid = bindTotalAmountPaid;
    }

    public RichInputText getBindTotalAmountPaid() {
        return bindTotalAmountPaid;
    }

    public void setBindRestInstallmentToBePaid(RichInputText bindRestInstallmentToBePaid) {
        this.bindRestInstallmentToBePaid = bindRestInstallmentToBePaid;
    }

    public RichInputText getBindRestInstallmentToBePaid() {
        return bindRestInstallmentToBePaid;
    }

    public void setBindingOutputText(RichInputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichInputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setBindUnitName(RichInputText bindUnitName) {
        this.bindUnitName = bindUnitName;
    }

    public RichInputText getBindUnitName() {
        return bindUnitName;
    }

    public void setBindStatus(RichSelectOneChoice bindStatus) {
        this.bindStatus = bindStatus;
    }

    public RichSelectOneChoice getBindStatus() {
        return bindStatus;
    }

    public void setBindFreezedBy(RichSelectOneChoice bindFreezedBy) {
        this.bindFreezedBy = bindFreezedBy;
    }

    public RichSelectOneChoice getBindFreezedBy() {
        return bindFreezedBy;
    }

    public void setBindLastInstallmentPaidMonthYear(RichInputDate bindLastInstallmentPaidMonthYear) {
        this.bindLastInstallmentPaidMonthYear = bindLastInstallmentPaidMonthYear;
    }

    public RichInputDate getBindLastInstallmentPaidMonthYear() {
        return bindLastInstallmentPaidMonthYear;
    }

    public void setBindLoanDate(RichInputDate bindLoanDate) {
        this.bindLoanDate = bindLoanDate;
    }

    public RichInputDate getBindLoanDate() {
        return bindLoanDate;
    }

    public void setBindDeductionStartMonthYear(RichInputDate bindDeductionStartMonthYear) {
        this.bindDeductionStartMonthYear = bindDeductionStartMonthYear;
    }

    public RichInputDate getBindDeductionStartMonthYear() {
        return bindDeductionStartMonthYear;
    }

//    public void deductionStartMonthYear(ValueChangeEvent vce) {
        
        
//        Timestamp t= (Timestamp)bindLoanDate.getValue();
//        System.out.println("First Loan Month"+t);
//         String dt1=(String)bindLoanDate.getValue();
//        System.out.println("Loan Current Date+dt1"+dt1);
//        String[] out1=dt1.split("/");
//        System.out.println("Loan Date = "+out1[1]);
//        System.out.println("Loan Month = "+out1[2]);
//        System.out.println("Loan Year = "+out1[0]);
//        Integer date1= Integer.valueOf(out1[0]);
//        Integer month1= Integer.valueOf(out1[1]);
//        Integer year1= Integer.valueOf(out1[2]);
//        System.out.println("Integer Loan Date"+ date1);
//        System.out.println("Integer Loan Month"+ month1);
//        System.out.println("Integer Loan Year"+ year1);
        
        
//        LocalDateTime now= LocalDateTime.now();
//        int year1 = now.getYear();
//        int month1 = now.getMonthValue();
//        System.out.println(" Current Year and Month"+year1 + "And" + month1);
//        
//        String dt2=(String)vce.getNewValue();
//        String[] out2=dt2.split("/");
//        System.out.println(" Deduction Month = "+out2[0]);
//        System.out.println("Deduction Year = "+out2[1]);
//        Integer month2= Integer.valueOf(out2[0]);
//        Integer year2= Integer.valueOf(out2[1]);
//        System.out.println("Integer Deduction Month"+ month2);
//        System.out.println("Integer Deduction Month"+ year2);
//        
//        if(month2>=month1 && year2>=year1)
//        {
//            System.out.println("Month and year of Deduction are greater than or equal to Loan Date.");
//            }
//        else
//        {
//                System.out.println("Month and year are not greater and equal");
//               FacesMessage Message = new FacesMessage("Deduction start Month and year should be greater than or equal to Loan Date.");   
//                Message.setSeverity(FacesMessage.SEVERITY_ERROR);   
//                FacesContext fc = FacesContext.getCurrentInstance();   
//                fc.addMessage(null, Message);  
//           throw new ValidatorException(Message);
//               
//            }
             
//    }

    public void loanDateVCL(ValueChangeEvent vce1) {
//        String dt2=(String)vce1.getNewValue();
//        System.out.println(" Loan  Current Date = "+dt2);
    }




    public void deductionStartMonthYearValidator(FacesContext facesContext, UIComponent uIComponent, Object ob) {
        if(ob !=null){
        Date now= (Date)bindLoanDate.getValue();
        String now1 = now.toString();
        String year1 = now1.substring(0,4);
        String month1 = now1.substring(5,7);
        System.out.println("now1"+now1);
        
        System.out.println("year1"+year1);
        
        System.out.println("month1"+month1);
        int years = Integer.parseInt(year1);
        int months = Integer.parseInt(month1);
        System.out.println(" Current Year and Month"+year1 + "And" + month1);
        
        String dt2=(String)ob.toString();
        String[] out2=dt2.split("-");
        System.out.println(" Deduction Month = "+out2[0]);
        System.out.println("Deduction Year = "+out2[1]);
        Integer month2= Integer.valueOf(out2[1]);
        Integer year2= Integer.valueOf(out2[0]);
        System.out.println("Integer Deduction Month"+ month2);
        System.out.println("Integer Deduction Month"+ year2);
        
        if(month2>=months && year2>=years)
        {
            System.out.println("Month and year of Deduction are greater than  Loan Date.");
            }
        else
        {
                System.out.println("Month and year are not greater and equal");
//               FacesMessage Message = new FacesMessage("Deduction start Month and year should be greater than or equal to Loan Date.");   
//                Message.setSeverity(FacesMessage.SEVERITY_ERROR);   
//                FacesContext fc = FacesContext.getCurrentInstance();   
//                fc.addMessage(null, Message);  
           throw new ValidatorException(new FacesMessage("Deduction start Month and year should be greater than Loan Date."));
               
            }

    }}

    public void setBindLoanNumber(RichInputText bindLoanNumber) {
        this.bindLoanNumber = bindLoanNumber;
    }

    public RichInputText getBindLoanNumber() {
        return bindLoanNumber;
    }
    
    public void popUpDialogDL(DialogEvent dialogEvent) {
        // Add event code here...
    }

    public void setBindEmployeeNumber(RichInputComboboxListOfValues bindEmployeeNumber) {
        this.bindEmployeeNumber = bindEmployeeNumber;
    }

    public RichInputComboboxListOfValues getBindEmployeeNumber() {
        return bindEmployeeNumber;
    }

    public void setBindLoanTotalAmount(RichInputText bindLoanTotalAmount) {
        this.bindLoanTotalAmount = bindLoanTotalAmount;
    }

    public RichInputText getBindLoanTotalAmount() {
        return bindLoanTotalAmount;
    }

    public void setBindTotalNumberOfInstallments(RichInputText bindTotalNumberOfInstallments) {
        this.bindTotalNumberOfInstallments = bindTotalNumberOfInstallments;
    }

    public RichInputText getBindTotalNumberOfInstallments() {
        return bindTotalNumberOfInstallments;
    }

    public void setBindLoanMasterId(RichInputText bindLoanMasterId) {
        this.bindLoanMasterId = bindLoanMasterId;
    }

    public RichInputText getBindLoanMasterId() {
        return bindLoanMasterId;
    }

    public void setBindEmployeeName(RichInputText bindEmployeeName) {
        this.bindEmployeeName = bindEmployeeName;
    }

    public RichInputText getBindEmployeeName() {
        return bindEmployeeName;
    }

    public void setDetailCreateBinding(RichButton detailCreateBinding) {
        this.detailCreateBinding = detailCreateBinding;
    }

    public RichButton getDetailCreateBinding() {
        return detailCreateBinding;
    }

    public void setDetailDeleteBinding(RichButton detailDeleteBinding) {
        this.detailDeleteBinding = detailDeleteBinding;
    }

    public RichButton getDetailDeleteBinding() {
        return detailDeleteBinding;
    }


    public void employeeNoProcessCheck(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        
        if(vce.getNewValue() != null){
         String empCode = (String) vce.getNewValue();
        
        OperationBinding op=ADFUtils.findOperation("LoanProcessPrnding");  
        op.getParamsMap().put("empCode", empCode);
        op.execute(); 
        if(op.getResult()!=null && op.getResult().equals("F"))
        {
            Row row=(Row)ADFUtils.evaluateEL("#{bindings.LoanVO1Iterator.currentRow}");
            row.setAttribute("EmpNo",null);
            ADFUtils.showMessage("Employee Already Having Loan in Process, Please Paid Previous Loan Amount First.", 0);
        }
    }}

    public void statuschangeVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
                               System.out.println("In Bean");
                               //String empCode = (String) ADFContext.getCurrent().getPageFlowScope().get("empCode");
                               String empcd = (String) ADFUtils.resolveExpression("#{pageFlowScope.empCode}");
                               //String empcd = "SWE161";
                               String userName = (String)vce.getNewValue();
                                                       if(vce!=null)
                                                       {
                                                           System.out.println("In Bean in if");
                       vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
                                                        OperationBinding op = (OperationBinding) ADFUtils.findOperation("statusForLoanEntry");
                                                        op.getParamsMap().put("unitCd", bindUnitCode.getValue().toString());
                                                        op.getParamsMap().put("empCode", empcd);
                                                       op.getParamsMap().put("authLevel", "CN");
                                                        op.getParamsMap().put("formName", "LON");
                                                        op.execute();  
    }}

    public void FreezebyVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
                              System.out.println("In Bean");
                              //String userName = (String) ADFContext.getCurrent().getPageFlowScope().get("userName");
                              String empcd = (String) ADFUtils.resolveExpression("#{pageFlowScope.empCode}");
                              String userName = (String)vce.getNewValue();
                                                      if(vce!=null)
                                                      {
                                                          System.out.println("In Bean in if");
                      vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
                                                       OperationBinding op = (OperationBinding) ADFUtils.findOperation("FreezeForLoanEntry");
                                                       op.getParamsMap().put("unitCd", bindUnitCode.getValue().toString());
                                                       op.getParamsMap().put("empCode",empcd);
                                                      op.getParamsMap().put("authLevel", "AP");
                                                       op.getParamsMap().put("formName", "LON");
                                                       op.execute();  
           }
    }
}
