package terms.hrm.transaction.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;
import javax.faces.validator.ValidatorException;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;

import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;

import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichPanelLabelAndMessage;

import oracle.adf.view.rich.component.rich.nav.RichButton;

import oracle.binding.OperationBinding;

public class LoanPrePaymentBean {
    private RichInputComboboxListOfValues bindUnitCode;
    private RichInputText bindEntryNumber;
    private RichInputText bindUnitName;
    private RichInputText bindTillDateBalance;
    private RichInputText bindEmployee;
    private RichInputDate bindPaymentDate;
    private RichPanelLabelAndMessage bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichInputComboboxListOfValues bindLoanNo;
    private String message = "C";
    private RichInputComboboxListOfValues bindEmployeeNo;
    private RichSelectOneChoice bindApproved;

    public LoanPrePaymentBean() {
    }

    public void LoanPrePaymentSaveAll(ActionEvent actionEvent) {
        // Add event code here...
        ADFUtils.setLastUpdatedBy("LoanVO1Iterator","LastUpdatedBy");
        OperationBinding op = ADFUtils.findOperation("getEntryNo");
              Object rst = op.execute();
              System.out.println("--------Commit-------");
                  System.out.println("value aftr getting result--->?"+rst);    
                   if (!bindEmployeeNo.isDisabled()) {
                     // if (op.getErrors().isEmpty() &&  message.equalsIgnoreCase("C")) {
                          ADFUtils.findOperation("Commit").execute();
                          FacesMessage Message = new FacesMessage("Record Saved Successfully. New loan No. is "+rst+".");  
                          Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                          FacesContext fc = FacesContext.getCurrentInstance();  
                          fc.addMessage(null, Message); 
                     // }
                  }
                 else{
                          ADFUtils.findOperation("Commit").execute();
                          FacesMessage Message = new FacesMessage("Record Update Successfully.");  
                          Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                          FacesContext fc = FacesContext.getCurrentInstance();  
                          fc.addMessage(null, Message);
                  }
    }


    private void cevmodecheck(){
           if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
               cevModeDisableComponent("V");
           }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
               cevModeDisableComponent("C");
           }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
               cevModeDisableComponent("E");
           }
       }
    
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                try {
                    Method method1 =
                        component.getClass().getMethod("setDisabled", boolean.class);
                    if (method1 != null) {
                        method1.invoke(component, valueComponent);
                    }
                } catch (NoSuchMethodException e) {
                    try {
                        Method method =
                            component.getClass().getMethod("setReadOnly", boolean.class);
                        if (method != null) {
                            method.invoke(component, valueComponent);
                        }
                    } catch (Exception e1) {
                        // e.printStackTrace();//silently eat this exception.
                    }


                } catch (Exception e) {
                    // e.printStackTrace();//silently eat this exception.
                }
                List<UIComponent> childComponents = component.getChildren();
                for (UIComponent comp : childComponents) {
                    makeComponentHierarchyReadOnly(comp, valueComponent);
                }
            }
    
    //Set Fields and Button disable.
        public void cevModeDisableComponent(String mode) {
        //            FacesContext fctx = FacesContext.getCurrentInstance();
        //            ELContext elctx = fctx.getELContext();
        //            Application jsfApp = fctx.getApplication();
        //            //create a ValueExpression that points to the ADF binding layer
        //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
        //            //
        //            ValueExpression valueExpr = exprFactory.createValueExpression(
        //                                         elctx,
        //                                         "#{pageFlowScope.mode=='E'}",
        //                                          Object.class
        //                                         );
        //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
            if (mode.equals("E")) {
                getBindUnitCode().setDisabled(true);
                getBindUnitName().setDisabled(true);
                getBindEntryNumber().setDisabled(true);
               getBindEmployeeNo().setDisabled(true);
                getBindTillDateBalance().setDisabled(true);
                getBindPaymentDate().setDisabled(true);
                getBindLoanNo().setDisabled(true);
                
                 
            } else if (mode.equals("C")) {
                getBindUnitCode().setDisabled(true);
                getBindUnitName().setDisabled(true);
                getBindEntryNumber().setDisabled(true);
//                getBindEmployee().setDisabled(true);
                getBindTillDateBalance().setDisabled(true);
                getBindPaymentDate().setDisabled(true);
                getBindLoanNo().setDisabled(true);
                getBindApproved().setDisabled(true);
            } else if (mode.equals("V")) {
               
            }
            
        }


    
    
    public void setBindUnitCode(RichInputComboboxListOfValues bindUnitCode) {
        this.bindUnitCode = bindUnitCode;
    }

    public RichInputComboboxListOfValues getBindUnitCode() {
        return bindUnitCode;
    }

    public void setBindEntryNumber(RichInputText bindEntryNumber) {
        this.bindEntryNumber = bindEntryNumber;
    }

    public RichInputText getBindEntryNumber() {
        return bindEntryNumber;
    }

    public void setBindUnitName(RichInputText bindUnitName) {
        this.bindUnitName = bindUnitName;
    }

    public RichInputText getBindUnitName() {
        return bindUnitName;
    }

    public void setBindTillDateBalance(RichInputText bindTillDateBalance) {
        this.bindTillDateBalance = bindTillDateBalance;
    }

    public RichInputText getBindTillDateBalance() {
        return bindTillDateBalance;
    }

    public void setBindEmployee(RichInputText bindEmployee) {
        this.bindEmployee = bindEmployee;
    }

    public RichInputText getBindEmployee() {
        return bindEmployee;
    }

    public void setBindPaymentDate(RichInputDate bindPaymentDate) {
        this.bindPaymentDate = bindPaymentDate;
    }

    public RichInputDate getBindPaymentDate() {
        return bindPaymentDate;
    }

    public void setBindingOutputText(RichPanelLabelAndMessage bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichPanelLabelAndMessage getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void amountValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if(object!=null){
            BigDecimal tillBalAmt=(BigDecimal)(bindTillDateBalance.getValue()!=null?bindTillDateBalance.getValue():new BigDecimal(0));
            BigDecimal amount=(BigDecimal)object;
            if(amount.compareTo(tillBalAmt) == 1){
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Loan balance for this employee is Rs. "+tillBalAmt,null)); 
            }
        }

    }

    public void empNoBinding(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        vce.getNewValue();
        
        if(vce.getNewValue()!= null) {
            getBindLoanNo().setDisabled(false);
        }
       
    }

    public void setBindLoanNo(RichInputComboboxListOfValues bindLoanNo) {
        this.bindLoanNo = bindLoanNo;
    }

    public RichInputComboboxListOfValues getBindLoanNo() {
        return bindLoanNo;
    }

    public void setBindEmployeeNo(RichInputComboboxListOfValues bindEmployeeNo) {
        this.bindEmployeeNo = bindEmployeeNo;
    }

    public RichInputComboboxListOfValues getBindEmployeeNo() {
        return bindEmployeeNo;
    }

    public void setBindApproved(RichSelectOneChoice bindApproved) {
        this.bindApproved = bindApproved;
    }

    public RichSelectOneChoice getBindApproved() {
        return bindApproved;
    }
}
