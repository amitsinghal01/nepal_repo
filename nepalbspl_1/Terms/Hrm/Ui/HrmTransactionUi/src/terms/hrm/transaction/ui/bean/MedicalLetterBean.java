package terms.hrm.transaction.ui.bean;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;

import java.sql.Connection;

import java.sql.SQLException;

import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;
import javax.faces.validator.ValidatorException;

import javax.naming.Context;
import javax.naming.InitialContext;

import javax.servlet.http.HttpServletResponse;

import javax.sql.DataSource;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

public class MedicalLetterBean {
    private RichTable tableBinding;
    private String editAction="V";
    private RichInputDate reportsubmitDateBinding;
    private RichInputComboboxListOfValues interviewDateBinding;
    private RichSelectOneChoice medicalStatusBinding;
    private RichInputDate medicalDateBinding;


    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public MedicalLetterBean() {
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            OperationBinding op = null;
            ADFUtils.findOperation("Delete").execute();
            op = (OperationBinding) ADFUtils.findOperation("Commit");
            if (getEditAction().equals("V")) {
                Object rst = op.execute();
            }
            System.out.println("Record Delete Successfully");
            if (op.getErrors().isEmpty()) {
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            } else if (!op.getErrors().isEmpty()) {
                OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
                Object rstr = opr.execute();
                FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(tableBinding);

    }

    public void setTableBinding(RichTable tableBinding) {
        this.tableBinding = tableBinding;
    }

    public RichTable getTableBinding() {
        return tableBinding;
    }

    public void saveAL(ActionEvent actionEvent) {
        OperationBinding op = (OperationBinding) ADFUtils.findOperation("generateMedicalEntryNo");
                op.execute();
                System.out.println("Result Save"+op.getResult());
                if(op.getResult()!=null && op.getResult().toString().equalsIgnoreCase("N"))
                {
                   if (op.getErrors().isEmpty())
                   {
                       ADFUtils.findOperation("Commit").execute();
                       FacesMessage Message = new FacesMessage("Record Update Successfully.");
                       Message.setSeverity(FacesMessage.SEVERITY_INFO);
                       FacesContext fc = FacesContext.getCurrentInstance();
                       fc.addMessage(null, Message);
                   }
                }
                else if (op.getResult() != null && op.getResult().toString() != "")
                {
                  if (op.getErrors().isEmpty())
                  {
                        System.out.println("CREATE MODE");
                        ADFUtils.findOperation("Commit").execute();
                        FacesMessage Message = new FacesMessage("Record Save Successfully.Medical Entry Number is "+op.getResult());
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);
                        FacesContext fc = FacesContext.getCurrentInstance();
                        fc.addMessage(null, Message);
                  }
                }
    }

  

    public void setReportsubmitDateBinding(RichInputDate reportsubmitDateBinding) {
        this.reportsubmitDateBinding = reportsubmitDateBinding;
    }

    public RichInputDate getReportsubmitDateBinding() {
        return reportsubmitDateBinding;
    }

    public void setInterviewDateBinding(RichInputComboboxListOfValues interviewDateBinding) {
        this.interviewDateBinding = interviewDateBinding;
    }

    public RichInputComboboxListOfValues getInterviewDateBinding() {
        return interviewDateBinding;
    }
    
    public BindingContainer getBindings()
      {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
      }
    
    public void downloadJrxml(FacesContext facesContext, OutputStream outputStream) {
        Connection conn=null;
            try {
                OperationBinding binding = ADFUtils.findOperation("fileNameForPrint");
                binding.getParamsMap().put("fileId","HRM0000000197");
                binding.execute();
                System.out.println("Binding Result :"+binding.getResult());
                if(binding.getResult() != null){
                    InputStream input = new FileInputStream(binding.getResult().toString());                    
                    DCIteratorBinding pvIter = (DCIteratorBinding) getBindings().get("MedicalLetterVO1Iterator");
                    String unitCode = pvIter.getCurrentRow().getAttribute("UnitCd").toString();
                    String entryNo = pvIter.getCurrentRow().getAttribute("IntvEntryNo").toString();
    //        //            oracle.jbo.domain.Date vouDate =(oracle.jbo.domain.Date)pvIter.getCurrentRow().getAttribute("VouDate");
    //
    //                    System.out.println("Machine Code: "+machCode+" ||UNIT CODE: "+unitCode);
                    System.out.println("UnitCd====>"+unitCode+" Entry No===>"+entryNo);
                    Map n = new HashMap();
                    n.put("p_unit", unitCode);
                    n.put("p_entry_no", entryNo);
                    conn = getConnection( );
                    
                    JasperReport design = (JasperReport) JRLoader.loadObject(input);
                    System.out.println("Path : " + input + " -------" + design+" param:"+n);
                    
                    @SuppressWarnings("unchecked")
                    net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                    byte pdf[] = JasperExportManager.exportReportToPdf(print);
                    HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                    response.getOutputStream().write(pdf);
                    response.getOutputStream().flush();
                    response.getOutputStream().close();
                    facesContext.responseComplete();
                    }else
                    System.out.println("File Name/File Path not found.");
                    } catch (FileNotFoundException fnfe) {
                    // TODO: Add catch code
                    fnfe.printStackTrace();
                    try {
                        System.out.println("in finally connection closed");
                                conn.close( );
                                conn = null;
                        
                    } catch( SQLException e ) {
                        e.printStackTrace( );
                    }
                    } catch (Exception e) {
                    // TODO: Add catch code
                    e.printStackTrace();
                    try {
                        System.out.println("in finally connection closed");
                                conn.close( );
                                conn = null;
                        
                    } catch( SQLException e1 ) {
                        e1.printStackTrace( );
                    }
                    }finally {
                            try {
                                    System.out.println("in finally connection closed");
                                            conn.close( );
                                            conn = null;
                                    
                            } catch( SQLException e ) {
                                    e.printStackTrace( );
                            }
                    }
    }

    public void medicalStatusVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        vce.getNewValue();
        Integer ms=(Integer)medicalStatusBinding.getValue();
        System.out.println("Result: "+ms);
        if(ms.equals(1)){
            medicalDateBinding.setDisabled(false);
            oracle.jbo.domain.Date d=new  oracle.jbo.domain.Date();
            medicalDateBinding.setValue(d.getCurrentDate());
            }
        else{
            medicalDateBinding.setDisabled(true);
            medicalDateBinding.setValue(null);
                 
        }
    }

    public void setMedicalStatusBinding(RichSelectOneChoice medicalStatusBinding) {
        this.medicalStatusBinding = medicalStatusBinding;
    }

    public RichSelectOneChoice getMedicalStatusBinding() {
        return medicalStatusBinding;
    }

    public void setMedicalDateBinding(RichInputDate medicalDateBinding) {
        this.medicalDateBinding = medicalDateBinding;
    }

    public RichInputDate getMedicalDateBinding() {
        return medicalDateBinding;
    }
    
    public Connection getConnection() throws Exception{
            Context ctx = null;
            Connection conn = null;
            ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/ApplicationDBDS");
            conn = ds.getConnection();
            return conn;
        }
}
