package terms.hrm.transaction.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.List;

import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.application.ViewHandler;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;

import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;

import oracle.adf.view.rich.component.rich.input.RichSelectBooleanCheckbox;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichPanelLabelAndMessage;

import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;
import oracle.jbo.domain.Date;

public class MonthlyBasketBillEntryBean {
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText unitNameBinding;
    private RichInputComboboxListOfValues employeeCodeBinding;
    private RichInputText employeeNameBinding;
    private RichInputText departmentCodeBinding;
    private RichInputText departmentNameBinding;
    private RichInputText monthBinding;
    private RichInputText yearBinding;
    private RichInputDate entryDateBinding;
    private RichInputText enteredByBinding;
    private RichInputText financialYearBinding;
    private RichInputDate approveDateBinding;
    private RichInputDate checkDateBinding;
    private RichInputDate verifyDateBinding;
    private RichInputText nbtEmpBinding;
    private RichInputText documentNumberBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichSelectBooleanCheckbox verifyStatusBinding;
    private RichSelectBooleanCheckbox statusBinding;
    private RichSelectBooleanCheckbox checkStatusBinding;
    private RichTable monthlyBasketBillEntryDetailTableBinding;
    
    private String editAction="V";

    private RichSelectBooleanCheckbox approveStatusBinding;
    private RichInputText basketAmountbinding;
    private RichInputText medicalAmountBinding;
    private RichInputText basketBalanceBinding;
    private RichInputText basketPaidBinding;
    private RichInputText medicalPaidBinding;
    private RichInputText medicalBalanceBinding;
    private RichInputComboboxListOfValues checkedByBinding;
    private RichInputComboboxListOfValues verifiedByBinding;
    private RichInputComboboxListOfValues approvedByBinding;
    private RichInputText enteredByNameBinding;
    private RichInputText unitCodeDetailBinding;
    private RichInputText unitNameDetailBinding;
    private RichInputText monthDetailBinding;
    private RichInputText yearDetailBinding;
    private RichInputText employeeCodeDetailBinding;
    private RichInputText employeeNameDetailBinding;
    private RichInputText departmentCodeDetailBinding;
    private RichInputText departmentNameDetailBinding;
    private RichInputDate entryDateDetailBinding;
    private RichInputText childNoDetailBinding;
    private RichInputText amtOfBillsDetailBinding;
    private RichInputText finYearDetailBinding;
    private RichInputText amtClaimedDetailBinding;
    private RichInputText curPaidDetailBinding;
    private RichInputText lastPaidDetailBinding;
    private RichInputText penAmtDetailBinding;
    private RichInputText noOfBillsDetailBinding;
    private RichOutputText bindingOutputText;
    private RichOutputText headCodeBinding;

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public MonthlyBasketBillEntryBean() {
    }

    private String message="C" , sts="T";
    public void saveButtonAL(ActionEvent actionEvent) {
        
        if(sts.equalsIgnoreCase("F"))
        {
            System.out.println("Status In Bean Save AL F");
                FacesMessage Message = new FacesMessage("Sum total of Amount Claimed should not be greater than Basket Balance.");
                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            
            }
        else {
        
            System.out.println("Status In Bean Save AL T");
                   OperationBinding opr = ADFUtils.findOperation("Commit");
                   Object obj = opr.execute();

                   System.out.println("outside error block");
                    //System.out.println("Level Code value is"+in.getValue());

                    //   if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {

                   //       if(leaveCodeHeaderBinding.getValue() !=null){
                   if (message.equalsIgnoreCase("C")){
                           System.out.println("S####");
                           FacesMessage Message = new FacesMessage("Record Save Successfully.");
                           Message.setSeverity(FacesMessage.SEVERITY_INFO);
                           FacesContext fc = FacesContext.getCurrentInstance();
                           fc.addMessage(null, Message);

                       } else if (message.equalsIgnoreCase("E")) {
                           FacesMessage Message = new FacesMessage("Record Update Successfully");
                           Message.setSeverity(FacesMessage.SEVERITY_INFO);
                           FacesContext fc = FacesContext.getCurrentInstance();
                           fc.addMessage(null, Message);
                       }
        }
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setUnitNameBinding(RichInputText unitNameBinding) {
        this.unitNameBinding = unitNameBinding;
    }

    public RichInputText getUnitNameBinding() {
        return unitNameBinding;
    }

    public void setEmployeeCodeBinding(RichInputComboboxListOfValues employeeCodeBinding) {
        this.employeeCodeBinding = employeeCodeBinding;
    }

    public RichInputComboboxListOfValues getEmployeeCodeBinding() {
        return employeeCodeBinding;
    }

    public void setEmployeeNameBinding(RichInputText employeeNameBinding) {
        this.employeeNameBinding = employeeNameBinding;
    }

    public RichInputText getEmployeeNameBinding() {
        return employeeNameBinding;
    }

    public void setDepartmentCodeBinding(RichInputText departmentCodeBinding) {
        this.departmentCodeBinding = departmentCodeBinding;
    }

    public RichInputText getDepartmentCodeBinding() {
        return departmentCodeBinding;
    }

    public void setDepartmentNameBinding(RichInputText departmentNameBinding) {
        this.departmentNameBinding = departmentNameBinding;
    }

    public RichInputText getDepartmentNameBinding() {
        return departmentNameBinding;
    }

    public void setMonthBinding(RichInputText monthBinding) {
        this.monthBinding = monthBinding;
    }

    public RichInputText getMonthBinding() {
        return monthBinding;
    }

    public void setYearBinding(RichInputText yearBinding) {
        this.yearBinding = yearBinding;
    }

    public RichInputText getYearBinding() {
        return yearBinding;
    }

    public void setEntryDateBinding(RichInputDate entryDateBinding) {
        this.entryDateBinding = entryDateBinding;
    }

    public RichInputDate getEntryDateBinding() {
        return entryDateBinding;
    }

    public void setEnteredByBinding(RichInputText enteredByBinding) {
        this.enteredByBinding = enteredByBinding;
    }

    public RichInputText getEnteredByBinding() {
        return enteredByBinding;
    }

    public void setFinancialYearBinding(RichInputText financialYearBinding) {
        this.financialYearBinding = financialYearBinding;
    }

    public RichInputText getFinancialYearBinding() {
        return financialYearBinding;
    }

    public void setApproveDateBinding(RichInputDate approveDateBinding) {
        this.approveDateBinding = approveDateBinding;
    }

    public RichInputDate getApproveDateBinding() {
        return approveDateBinding;
    }

    public void setCheckDateBinding(RichInputDate checkDateBinding) {
        this.checkDateBinding = checkDateBinding;
    }

    public RichInputDate getCheckDateBinding() {
        return checkDateBinding;
    }

    public void setVerifyDateBinding(RichInputDate verifyDateBinding) {
        this.verifyDateBinding = verifyDateBinding;
    }

    public RichInputDate getVerifyDateBinding() {
        return verifyDateBinding;
    }

    public void setNbtEmpBinding(RichInputText nbtEmpBinding) {
        this.nbtEmpBinding = nbtEmpBinding;
    }

    public RichInputText getNbtEmpBinding() {
        return nbtEmpBinding;
    }

    public void setDocumentNumberBinding(RichInputText documentNumberBinding) {
        this.documentNumberBinding = documentNumberBinding;
    }

    public RichInputText getDocumentNumberBinding() {
        return documentNumberBinding;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }
    
    private void cevmodecheck(){
            if (ADFUtils.resolveExpression("#{pageFlowScope.md}").equals("V")){
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
                cevModeDisableComponent("V");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.md}").equals("C")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("C");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.md}").equals("E")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("E");
            }
        }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                try {
                    Method method1 =
                        component.getClass().getMethod("setDisabled", boolean.class);
                    if (method1 != null) {
                        method1.invoke(component, valueComponent);
                    }
                } catch (NoSuchMethodException e) {
                    try {
                        Method method =
                            component.getClass().getMethod("setReadOnly", boolean.class);
                        if (method != null) {
                            method.invoke(component, valueComponent);
                        }
                    } catch (Exception e1) {
                        // e.printStackTrace();//silently eat this exception.
                    }


                } catch (Exception e) {
                    // e.printStackTrace();//silently eat this exception.
                }
                List<UIComponent> childComponents = component.getChildren();
    for (UIComponent comp : childComponents) {
                   makeComponentHierarchyReadOnly(comp, valueComponent);
               }
           }
    
    public void cevModeDisableComponent(String md) {
   
            if (md.equals("E")) {
                getUnitCodeBinding().setDisabled(true);
                getUnitNameBinding().setDisabled(true);
                getEmployeeCodeBinding().setDisabled(false);
                //getEmployeeNameBinding().setDisabled(true);
                getDepartmentCodeBinding().setDisabled(true);
                getDepartmentNameBinding().setDisabled(true);
                getMonthBinding().setDisabled(false);
                getYearBinding().setDisabled(false);
                getEntryDateBinding().setDisabled(false);
                getEnteredByBinding().setDisabled(true);
                getFinancialYearBinding().setDisabled(true);
                getCheckedByBinding().setDisabled(false);
                getStatusBinding().setDisabled(false);
                getVerifiedByBinding().setDisabled(true);
                getApprovedByBinding().setDisabled(true);
                getApproveDateBinding().setDisabled(true);
                getApproveStatusBinding().setDisabled(true);
                getCheckDateBinding().setDisabled(false);
                getCheckStatusBinding().setDisabled(false);
                getVerifyDateBinding().setDisabled(true);
                getVerifyStatusBinding().setDisabled(true);
                getNbtEmpBinding().setDisabled(false);
                getDocumentNumberBinding().setDisabled(false);
                
                getMedicalAmountBinding().setDisabled(true);
                getBasketBalanceBinding().setDisabled(true);
                getBasketPaidBinding().setDisabled(true);
                getMedicalPaidBinding().setDisabled(true);
                getMedicalBalanceBinding().setDisabled(true);
                getBasketAmountbinding().setDisabled(true);
                getCheckedByBinding().setDisabled(true);
                getCheckDateBinding().setDisabled(true);
                getCheckStatusBinding().setDisabled(true);
                getHeaderEditBinding().setDisabled(true);
           
            } else if (md.equals("C")) {
                getUnitCodeBinding().setDisabled(true);
                getUnitNameBinding().setDisabled(true);
                getEmployeeCodeBinding().setDisabled(false);
                //getEmployeeNameBinding().setDisabled(true);
                getDepartmentCodeBinding().setDisabled(true);
                getDepartmentNameBinding().setDisabled(true);
                getMonthBinding().setDisabled(false);
                getYearBinding().setDisabled(false);
                getEntryDateBinding().setDisabled(false);
                getEnteredByBinding().setDisabled(true);
                getFinancialYearBinding().setDisabled(true);
                getCheckedByBinding().setDisabled(false);
                
                getVerifiedByBinding().setDisabled(true);
                getApprovedByBinding().setDisabled(true);
                getApproveDateBinding().setDisabled(true);
                getApproveStatusBinding().setDisabled(true);
                getCheckDateBinding().setDisabled(false);
                getCheckStatusBinding().setDisabled(false);
                getVerifyDateBinding().setDisabled(true);
                getVerifyStatusBinding().setDisabled(true);
                getNbtEmpBinding().setDisabled(false);
                getDocumentNumberBinding().setDisabled(false);
                
                getMedicalAmountBinding().setDisabled(true);
                getBasketBalanceBinding().setDisabled(true);
                getBasketPaidBinding().setDisabled(true);
                getMedicalPaidBinding().setDisabled(true);
                getMedicalBalanceBinding().setDisabled(true);
                getBasketAmountbinding().setDisabled(true);
                getCheckedByBinding().setDisabled(true);
                getCheckDateBinding().setDisabled(true);
                getCheckStatusBinding().setDisabled(true);
                getHeaderEditBinding().setDisabled(true);
        
            } else if (md.equals("V")) {
                getUnitCodeBinding().setDisabled(true);
            }
            
        }

    public void editButtonAL(ActionEvent actionEvent) {
        if(approvedByBinding.getValue() == null)
        {
        cevmodecheck();
        }
        else {
            ADFUtils.showMessage("Approved Entry Can not be Edited", 0);
        }
    }

    public void setVerifyStatusBinding(RichSelectBooleanCheckbox verifyStatusBinding) {
        this.verifyStatusBinding = verifyStatusBinding;
    }

    public RichSelectBooleanCheckbox getVerifyStatusBinding() {
        return verifyStatusBinding;
    }

    public void setStatusBinding(RichSelectBooleanCheckbox statusBinding) {
        this.statusBinding = statusBinding;
    }

    public RichSelectBooleanCheckbox getStatusBinding() {
        return statusBinding;
    }

    public void setCheckStatusBinding(RichSelectBooleanCheckbox checkStatusBinding) {
        this.checkStatusBinding = checkStatusBinding;
    }

    public RichSelectBooleanCheckbox getCheckStatusBinding() {
        return checkStatusBinding;
    }

    public void popupDialogDetailDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
        {
        OperationBinding op = null;
        ADFUtils.findOperation("Delete").execute();
        op = (OperationBinding) ADFUtils.findOperation("Commit");
        if (getEditAction().equals("V")) {
        Object rst = op.execute();
        }
        System.out.println("Record Delete Successfully");
        if(op.getErrors().isEmpty()){
        FacesMessage Message = new FacesMessage("Record Deleted Successfully");
        Message.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        } else if (!op.getErrors().isEmpty())
        {
        OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
        Object rstr = opr.execute();
        FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
        Message.setSeverity(FacesMessage.SEVERITY_ERROR);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(monthlyBasketBillEntryDetailTableBinding);
    }

    public void setMonthlyBasketBillEntryDetailTableBinding(RichTable monthlyBasketBillEntryDetailTableBinding) {
        this.monthlyBasketBillEntryDetailTableBinding = monthlyBasketBillEntryDetailTableBinding;
    }

    public RichTable getMonthlyBasketBillEntryDetailTableBinding() {
        return monthlyBasketBillEntryDetailTableBinding;
    }



    public void setApproveStatusBinding(RichSelectBooleanCheckbox approveStatusBinding) {
        this.approveStatusBinding = approveStatusBinding;
    }

    public RichSelectBooleanCheckbox getApproveStatusBinding() {
        return approveStatusBinding;
    }

    public void employeeCodeVCL(ValueChangeEvent vce) {
       vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        System.out.println("Before Method");
        OperationBinding op= ADFUtils.findOperation("getValuesForMonthlyBasketBillEntry");
        op.execute();

        op.getResult();
        System.out.println("op.getResult"+op.getResult());

        String mon=(String)op.getResult();
        System.out.println("mon"+mon);
 
        int year=Calendar.getInstance().get(Calendar.YEAR);
        BigDecimal y=new BigDecimal(year);
        System.out.println("Year: "+y);
        
        monthBinding.setValue(mon.toUpperCase());
        yearBinding.setValue(y);
        
        OperationBinding op1=ADFUtils.findOperation("getBasketValuesForMonthlyBasketBillEntry");
        op1.execute();

        System.out.println("After Method");
    }
    
    

    public void setBasketAmountbinding(RichInputText basketAmountbinding) {
        this.basketAmountbinding = basketAmountbinding;
    }

    public RichInputText getBasketAmountbinding() {
        return basketAmountbinding;
    }

    public void setMedicalAmountBinding(RichInputText medicalAmountBinding) {
        this.medicalAmountBinding = medicalAmountBinding;
    }

    public RichInputText getMedicalAmountBinding() {
        return medicalAmountBinding;
    }

    public void setBasketBalanceBinding(RichInputText basketBalanceBinding) {
        this.basketBalanceBinding = basketBalanceBinding;
    }

    public RichInputText getBasketBalanceBinding() {
        return basketBalanceBinding;
    }

    public void setBasketPaidBinding(RichInputText basketPaidBinding) {
        this.basketPaidBinding = basketPaidBinding;
    }

    public RichInputText getBasketPaidBinding() {
        return basketPaidBinding;
    }

    public void setMedicalPaidBinding(RichInputText medicalPaidBinding) {
        this.medicalPaidBinding = medicalPaidBinding;
    }

    public RichInputText getMedicalPaidBinding() {
        return medicalPaidBinding;
    }

    public void setMedicalBalanceBinding(RichInputText medicalBalanceBinding) {
        this.medicalBalanceBinding = medicalBalanceBinding;
    }

    public RichInputText getMedicalBalanceBinding() {
        return medicalBalanceBinding;
    }

    public void statusVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
       // String s=(String)statusBinding.getValue();
       Boolean s=(Boolean)vce.getNewValue();
        if(s.equals("Y"))
        {
                getStatusBinding().setDisabled(true);            
            }
        else
        {
                getStatusBinding().setDisabled(false);
            }
    }

    public void checkStatusVCL(ValueChangeEvent vce) {
        System.out.println("In Bean Check Status VCL");
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        Boolean cs=(Boolean)vce.getNewValue();
        if(cs.equals("Y"))
        {
           String chkby=(String)checkedByBinding.getValue();
           System.out.println("Checked By value: "+chkby);
            if(chkby != null) {
                OperationBinding op = (OperationBinding) ADFUtils.findOperation("checkAuthorityForMonthlyBasketBillEntryCheckedBy");
                op.getParamsMap().put("unitCd", unitCodeBinding.getValue().toString());
              //  op.getParamsMap().put("empCode", checkedByBinding.getValue());
                op.getParamsMap().put("authLevel", "CH");
                op.getParamsMap().put("formName", "BBE");
                op.execute();
            }
            
            }
        System.out.println("Out Bean Check Status VCL");
    }

    public void setCheckedByBinding(RichInputComboboxListOfValues checkedByBinding) {
        this.checkedByBinding = checkedByBinding;
    }

    public RichInputComboboxListOfValues getCheckedByBinding() {
        return checkedByBinding;
    }

    public void setVerifiedByBinding(RichInputComboboxListOfValues verifiedByBinding) {
        this.verifiedByBinding = verifiedByBinding;
    }

    public RichInputComboboxListOfValues getVerifiedByBinding() {
        return verifiedByBinding;
    }

    public void setApprovedByBinding(RichInputComboboxListOfValues approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputComboboxListOfValues getApprovedByBinding() {
        return approvedByBinding;
    }

    public void approveStatusVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        Boolean as=(Boolean)vce.getNewValue();
        if(as.equals("Y"))
        {
           String appby=(String)approvedByBinding.getValue();
           System.out.println("Approved By value: "+appby);
            if(appby != null) {
                OperationBinding op = (OperationBinding) ADFUtils.findOperation("checkAuthorityForMonthlyBasketBillEntryApprovedBy");
                op.getParamsMap().put("unitCd", unitCodeBinding.getValue().toString());
                op.getParamsMap().put("authLevel", "CH");
                op.getParamsMap().put("formName", "BBE");
                op.execute();
            }
            
            }
       
    }

    public void verifyStatusVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        Boolean vs=(Boolean)vce.getNewValue();
        if(vs.equals("Y"))
        {
           String verby=(String)verifiedByBinding.getValue();
           System.out.println("Approved By value: "+verby);
            if(verby != null) {
                OperationBinding op = (OperationBinding) ADFUtils.findOperation("checkAuthorityForMonthlyBasketBillEntryVerifiedBy");
                op.getParamsMap().put("unitCd", unitCodeBinding.getValue().toString());
                op.getParamsMap().put("authLevel", "CH");
                op.getParamsMap().put("formName", "BBE");
                op.execute();
            }
            
            }
        
    }

    public void setEnteredByNameBinding(RichInputText enteredByNameBinding) {
        this.enteredByNameBinding = enteredByNameBinding;
    }

    public RichInputText getEnteredByNameBinding() {
        return enteredByNameBinding;
    }
    public void createButtonDetailAL(ActionEvent actionEvent) {
        System.out.println("Inside Create Button AL");
        
        System.out.println("Calling Head Code Populate From Create Button AL");

        DCIteratorBinding dci=ADFUtils.findIterator("MonthlyBasketBillEntryVO1Iterator");
        RowSetIterator rsi=dci.getViewObject().createRowSetIterator(null);
        while(rsi.hasNext())
        {
        System.out.println("Inside while loop");
        Row r=rsi.next();
        String uc=(String)r.getAttribute("UnitCd");
        String ec=(String)r.getAttribute("EmpCode");
        String m=(String)r.getAttribute("Month");
        Number y=(Number)r.getAttribute("Year");
        String fy=(String)r.getAttribute("FinYear");
        String eby=(String)r.getAttribute("EnteredBy");
        Date ed=(Date)r.getAttribute("EntryDate");
        
        System.out.println("Values from Monthly Basket Bill Entry Iterator------->>>>");
        System.out.println("Unit Code: "+uc);
        System.out.println("Emp Code: "+ec);
        System.out.println("Month: "+m);
        System.out.println("Year: "+y);
        System.out.println("Fin Year: "+fy);
        System.out.println("Entered By: "+eby);
        System.out.println("Entry Date: "+ed);
        OperationBinding ob1=ADFUtils.findOperation("getMonthlyBasketBillEntryHeadPop");
        ob1.getParamsMap().put("UnitCd", uc);
        ob1.getParamsMap().put("EmpCd", ec);
        ob1.getParamsMap().put("Month", m);
        ob1.getParamsMap().put("FinYr", fy);
        ob1.getParamsMap().put("EntryDt", ed);
        ob1.getParamsMap().put("EnteredBy", eby);
        ob1.getParamsMap().put("Year", y);
        ob1.execute();
     
        System.out.println("Back to bean in Create Button AL");
        }
        rsi.closeRowSetIterator();
        
        OperationBinding ob=ADFUtils.findOperation("populateValuesInMonthlyBasketBillEntryDetail");
        ob.execute();
        System.out.println("Exiting from Create Button AL");
    }

    public void entryDateVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        System.out.println("Inside entry date VCL");
        DCIteratorBinding dci=ADFUtils.findIterator("MonthlyBasketBillEntryVO1Iterator");
        RowSetIterator rsi=dci.getViewObject().createRowSetIterator(null);
        
        if(vce.getNewValue() != null)
        {
            System.out.println("INSIDE IF ###");
        while(rsi.hasNext())
        {
            System.out.println("Inside while loop");
            Row r=rsi.next();
            String uc=(String)r.getAttribute("UnitCd");
            String ec=(String)r.getAttribute("EmpCode");
            String m=(String)r.getAttribute("Month");
            Number y=(Number)r.getAttribute("Year");
            String fy=(String)r.getAttribute("FinYear");
            String eby=(String)r.getAttribute("EnteredBy");
            Date ed=(Date)r.getAttribute("EntryDate");
            
            System.out.println("Values from Monthly Basket Bill Entry Iterator::::");
            System.out.println("Unit Code: "+uc);
            System.out.println("Emp Code: "+ec);
            System.out.println("Month: "+m);
            System.out.println("Year: "+y);
            System.out.println("Fin Year: "+fy);
            System.out.println("Entered By: "+eby);
            System.out.println("Entry Date: "+ed);
            
            System.out.println("Before Function Call entrydate vcl");
            OperationBinding op=ADFUtils.findOperation("getValueForEntryDateMonthlyBasketBillEntry");        
            op.getParamsMap().put("UnitCd", uc);
            op.getParamsMap().put("EmpCd", ec);
            op.getParamsMap().put("Month", m);
            op.getParamsMap().put("FinYr", fy);
            op.getParamsMap().put("EntryDt", ed);
            op.getParamsMap().put("EnteredBy", eby);
            op.getParamsMap().put("Year", y);
           
            Object rst=op.execute();
    //           String res=(String)op.getResult();
    //            System.out.println("Result error  ---> "+res);
            
            System.out.println("After Function Call");

             
        }
        rsi.closeRowSetIterator();
        }
        rsi.closeRowSetIterator();
    }

    public void amtOfBillsVCL(ValueChangeEvent vce) {
        System.out.println("Inside Amount Of Bills VCL");
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        String unitCd=(String)unitCodeDetailBinding.getValue();
        String empCd=(String)employeeCodeDetailBinding.getValue();
        String headCd = (String) headCodeBinding.getValue();
        oracle.jbo.domain.Number aob=(oracle.jbo.domain.Number)amtOfBillsDetailBinding.getValue();
        oracle.jbo.domain.Number nob=(oracle.jbo.domain.Number)noOfBillsDetailBinding.getValue();
        oracle.jbo.domain.Number mbal=(oracle.jbo.domain.Number)medicalBalanceBinding.getValue();
        String finyr=(String)finYearDetailBinding.getValue();
        Date entryDt=(Date)entryDateDetailBinding.getValue();
        String enteredBy=(String)enteredByBinding.getValue();
        
        System.out.println("Values Fetched From UI: ");
        System.out.println("Unit Code: "+unitCd);
        System.out.println("Employee Code: "+empCd);
        System.out.println("Head Code: "+headCd);
        System.out.println("Amount Of Bills: "+aob);
        System.out.println("Number Of Bills: "+nob);
        System.out.println("Medical Balance: "+mbal);
        System.out.println("Financial Year: "+finyr);
        System.out.println("Entry Date: "+entryDt);
        System.out.println("Entered By: "+enteredBy);
        
        System.out.println("Before Function Call amtofbills vcl-----");
        OperationBinding ob=ADFUtils.findOperation("validateAmtOfBillsForMonthlyBasketBillEntry");
        ob.getParamsMap().put("UnitCd",unitCd);
        ob.getParamsMap().put("EmpCd",empCd);
        ob.getParamsMap().put("HeadCd",headCd);
        ob.getParamsMap().put("AmtOfBills",aob);
        ob.getParamsMap().put("NoOfBills",nob);
        ob.getParamsMap().put("MedBal",mbal);
        ob.getParamsMap().put("FinYr",finyr);
        ob.getParamsMap().put("EntryDt",entryDt);
        ob.getParamsMap().put("EnteredBy",enteredBy);
        ob.execute();
        
        System.out.println("Back to Amount Of Bills In Bean");
        
    }
    
    public void amtClaimedVCL(ValueChangeEvent vce) {
            vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
            System.out.println("Inside Amount Claimed VCL");
            System.out.println("Before Function Call amtclaimed vcl-----");
            OperationBinding ob=ADFUtils.findOperation("validateAmtClaimedForMonthlyBasketBillEntry");
            ob.execute();
            ob.getResult();
            System.out.println("Result "+ob.getResult());
            
             sts=(String)ob.getResult();
        }

    public void setUnitCodeDetailBinding(RichInputText unitCodeDetailBinding) {
        this.unitCodeDetailBinding = unitCodeDetailBinding;
    }

    public RichInputText getUnitCodeDetailBinding() {
        return unitCodeDetailBinding;
    }

    public void setUnitNameDetailBinding(RichInputText unitNameDetailBinding) {
        this.unitNameDetailBinding = unitNameDetailBinding;
    }

    public RichInputText getUnitNameDetailBinding() {
        return unitNameDetailBinding;
    }

    public void setMonthDetailBinding(RichInputText monthDetailBinding) {
        this.monthDetailBinding = monthDetailBinding;
    }

    public RichInputText getMonthDetailBinding() {
        return monthDetailBinding;
    }

    public void setYearDetailBinding(RichInputText yearDetailBinding) {
        this.yearDetailBinding = yearDetailBinding;
    }

    public RichInputText getYearDetailBinding() {
        return yearDetailBinding;
    }

    public void setEmployeeCodeDetailBinding(RichInputText employeeCodeDetailBinding) {
        this.employeeCodeDetailBinding = employeeCodeDetailBinding;
    }

    public RichInputText getEmployeeCodeDetailBinding() {
        return employeeCodeDetailBinding;
    }

    public void setEmployeeNameDetailBinding(RichInputText employeeNameDetailBinding) {
        this.employeeNameDetailBinding = employeeNameDetailBinding;
    }

    public RichInputText getEmployeeNameDetailBinding() {
        return employeeNameDetailBinding;
    }

    public void setDepartmentCodeDetailBinding(RichInputText departmentCodeDetailBinding) {
        this.departmentCodeDetailBinding = departmentCodeDetailBinding;
    }

    public RichInputText getDepartmentCodeDetailBinding() {
        return departmentCodeDetailBinding;
    }

    public void setDepartmentNameDetailBinding(RichInputText departmentNameDetailBinding) {
        this.departmentNameDetailBinding = departmentNameDetailBinding;
    }

    public RichInputText getDepartmentNameDetailBinding() {
        return departmentNameDetailBinding;
    }

    public void setEntryDateDetailBinding(RichInputDate entryDateDetailBinding) {
        this.entryDateDetailBinding = entryDateDetailBinding;
    }

    public RichInputDate getEntryDateDetailBinding() {
        return entryDateDetailBinding;
    }

   

    public void setChildNoDetailBinding(RichInputText childNoDetailBinding) {
        this.childNoDetailBinding = childNoDetailBinding;
    }

    public RichInputText getChildNoDetailBinding() {
        return childNoDetailBinding;
    }

    public void setAmtOfBillsDetailBinding(RichInputText amtOfBillsDetailBinding) {
        this.amtOfBillsDetailBinding = amtOfBillsDetailBinding;
    }

    public RichInputText getAmtOfBillsDetailBinding() {
        return amtOfBillsDetailBinding;
    }

    public void setFinYearDetailBinding(RichInputText finYearDetailBinding) {
        this.finYearDetailBinding = finYearDetailBinding;
    }

    public RichInputText getFinYearDetailBinding() {
        return finYearDetailBinding;
    }

    public void setAmtClaimedDetailBinding(RichInputText amtClaimedDetailBinding) {
        this.amtClaimedDetailBinding = amtClaimedDetailBinding;
    }

    public RichInputText getAmtClaimedDetailBinding() {
        return amtClaimedDetailBinding;
    }

    public void setCurPaidDetailBinding(RichInputText curPaidDetailBinding) {
        this.curPaidDetailBinding = curPaidDetailBinding;
    }

    public RichInputText getCurPaidDetailBinding() {
        return curPaidDetailBinding;
    }

    public void setLastPaidDetailBinding(RichInputText lastPaidDetailBinding) {
        this.lastPaidDetailBinding = lastPaidDetailBinding;
    }

    public RichInputText getLastPaidDetailBinding() {
        return lastPaidDetailBinding;
    }

    public void setPenAmtDetailBinding(RichInputText penAmtDetailBinding) {
        this.penAmtDetailBinding = penAmtDetailBinding;
    }

    public RichInputText getPenAmtDetailBinding() {
        return penAmtDetailBinding;
    }

    public void setNoOfBillsDetailBinding(RichInputText noOfBillsDetailBinding) {
        this.noOfBillsDetailBinding = noOfBillsDetailBinding;
    }

    public RichInputText getNoOfBillsDetailBinding() {
        return noOfBillsDetailBinding;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setHeadCodeBinding(RichOutputText headCodeBinding) {
        this.headCodeBinding = headCodeBinding;
    }

    public RichOutputText getHeadCodeBinding() {
        return headCodeBinding;
    }


    public String saveAndCloseAction() {
                
                if(sts.equalsIgnoreCase("F"))
                {
                    System.out.println("Status In Bean Save AL F");
                        FacesMessage Message = new FacesMessage("Sum total of Amount Claimed should not be greater than Basket Balance.");
                        Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                        FacesContext fc = FacesContext.getCurrentInstance();
                        fc.addMessage(null, Message);
                    
                    }
                else {
                
                    System.out.println("Status In Bean Save AL T");
        //                   OperationBinding opr = ADFUtils.findOperation("Commit");
        //                   Object obj = opr.execute();
        
                           System.out.println("outside error block");
                            //System.out.println("Level Code value is"+in.getValue());
        
                            //   if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
        
                           //       if(leaveCodeHeaderBinding.getValue() !=null){
                           if (message.equalsIgnoreCase("C")){
                               
                                   System.out.println("SC####");
                                   OperationBinding opr = ADFUtils.findOperation("Commit");
                                   Object obj = opr.execute();
                                   
                                   FacesMessage Message = new FacesMessage("Record Save Successfully.");
                                   Message.setSeverity(FacesMessage.SEVERITY_INFO);
                                   FacesContext fc = FacesContext.getCurrentInstance();
                                   fc.addMessage(null, Message);
                                   return "Save And Close";
                              
        
                               } else if (message.equalsIgnoreCase("E")) {
                                   System.out.println("SE####");
                                   OperationBinding opr = ADFUtils.findOperation("Commit");
                                   Object obj = opr.execute();
                                   FacesMessage Message = new FacesMessage("Record Update Successfully");
                                   Message.setSeverity(FacesMessage.SEVERITY_INFO);
                                   FacesContext fc = FacesContext.getCurrentInstance();
                                   fc.addMessage(null, Message);
                                   return "Save And Close";
                               
                               }
                   
                }
     
       return null;
    }
}
