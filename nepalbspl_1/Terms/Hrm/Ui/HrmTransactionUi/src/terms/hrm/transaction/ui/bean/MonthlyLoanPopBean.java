package terms.hrm.transaction.ui.bean;

import com.sun.org.apache.bcel.internal.classfile.Code;

import java.lang.reflect.Method;

import java.time.Year;

import java.util.List;
import java.util.Set;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import javax.faces.validator.ValidatorException;

import oracle.adf.share.ADFContext;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputText;

import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;

import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichPanelLabelAndMessage;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class MonthlyLoanPopBean {


    //private RichInputText bindLoanPid;
    private RichInputComboboxListOfValues bindHeadCode;
    private RichInputComboboxListOfValues bindUnit;
    private RichSelectOneChoice bindMonth;
    private RichInputText bindYear;
    private RichTable monthlyLoanPopulationTableBinding;
    private RichInputText bindUnitName;
    private RichInputText bindHeadDescription;
    private RichInputText bindSlNo;
    private RichInputText bindLoanNumber;
    private RichInputText bindEmployeeNumber;
    private RichInputText bindEmployeeName;
    private RichInputText bindLoanAmount;
    private RichInputText bindLoanPaid;
    private RichInputText bindLoanBalance;
    private RichInputText bindLoanInstallment;
    private RichPanelLabelAndMessage bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichButton detailcreateBinding;
    private RichButton detaildeleteBinding;
    private String Message="C";
    private RichInputComboboxListOfValues approvedByBinding;

    public MonthlyLoanPopBean() {
    }


       public void monthlyLoanPopulation(ActionEvent actionEvent) {
            if(bindUnit.getValue()!=null || bindMonth.getValue()!=null || bindYear.getValue()!=null || bindHeadCode.getValue()!=null){
                    //String LoanProcessID = bindLoanPid.getValue()==null ? "" : bindLoanPid.getValue().toString();
                    String Unit = bindUnit.getValue()==null ? "" : bindUnit.getValue().toString();
                    String Month = bindMonth.getValue()==null ? "" : bindMonth.getValue().toString();
                    String Year = bindYear.getValue()==null ? "" : bindYear.getValue().toString();
                    String HeadCode= bindHeadCode.getValue()==null ? "" : bindHeadCode.getValue().toString();

                    System.out.println("@@@@@@@@@@@Bean data," + Unit+ "," + Month+" "+Year+ "," + HeadCode);
                    OperationBinding ob = ADFUtils.findOperation("populateMonthlyLoan"); 
                            ob.getParamsMap().put("Unit", Unit);
                            ob.getParamsMap().put("Month", Month);
                            ob.getParamsMap().put("Year", Year);
                            ob.getParamsMap().put("HeadCode", HeadCode);
                          //  ob.getParamsMap().put("LoanProcessID", LoanProcessID);
                            ob.execute();
                  }
       }

    public void setBindHeadCode(RichInputComboboxListOfValues bindHeadCode) {
        this.bindHeadCode = bindHeadCode;
    }

    public RichInputComboboxListOfValues getBindHeadCode() {
        return bindHeadCode;
    }

    public void setBindUnit(RichInputComboboxListOfValues bindUnit) {
        this.bindUnit = bindUnit;
    }

    public RichInputComboboxListOfValues getBindUnit() {
        return bindUnit;
    }

    public void setBindMonth(RichSelectOneChoice bindMonth) {
        this.bindMonth = bindMonth;
    }

    public RichSelectOneChoice getBindMonth() {
        return bindMonth;
    }

    public void setBindYear(RichInputText bindYear) {
        this.bindYear = bindYear;
    }

    public RichInputText getBindYear() {
        return bindYear;
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent)
    {
        if(dialogEvent.getOutcome().name().equals("ok"))
                    {
                    ADFUtils.findOperation("Delete").execute();
                    FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                    FacesContext fc = FacesContext.getCurrentInstance();  
                    fc.addMessage(null, Message);  
                   
                    }
                AdfFacesContext.getCurrentInstance().addPartialTarget(monthlyLoanPopulationTableBinding);

    }

    private void cevmodecheck(){
          if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
              makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
              cevModeDisableComponent("V");
          }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
              makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
              cevModeDisableComponent("C");
          }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
              makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
              cevModeDisableComponent("E");
          }
      }
    
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                try {
                    Method method1 =
                        component.getClass().getMethod("setDisabled", boolean.class);
                    if (method1 != null) {
                        method1.invoke(component, valueComponent);
                    }
                } catch (NoSuchMethodException e) {
                    try {
                        Method method =
                            component.getClass().getMethod("setReadOnly", boolean.class);
                        if (method != null) {
                            method.invoke(component, valueComponent);
                        }
                    } catch (Exception e1) {
                        // e.printStackTrace();//silently eat this exception.
                    }


                } catch (Exception e) {
                    // e.printStackTrace();//silently eat this exception.
                }
                List<UIComponent> childComponents = component.getChildren();
                for (UIComponent comp : childComponents) {
                    makeComponentHierarchyReadOnly(comp, valueComponent);
                }
            }
    
        public void cevModeDisableComponent(String mode) {

            if (mode.equals("E")) {
                Message="E";
                getBindUnit().setDisabled(true);
                getBindUnitName().setDisabled(true);
                getBindHeadDescription().setDisabled(true);
                getBindSlNo().setDisabled(true);
                getBindLoanNumber().setDisabled(true);
                getBindEmployeeNumber().setDisabled(true);
                getBindEmployeeName().setDisabled(true);
                getBindLoanAmount().setDisabled(true);
                getBindLoanPaid().setDisabled(true);
                getBindLoanBalance().setDisabled(true);
                //getBindLoanInstallment().setDisabled(true);
                getHeaderEditBinding().setDisabled(true);
                getBindMonth().setDisabled(true);
                getBindYear().setDisabled(true);
                    
            } else if (mode.equals("C")) {
                getBindUnit().setDisabled(true);
                getBindUnitName().setDisabled(true);
                getBindHeadDescription().setDisabled(true);
                getBindSlNo().setDisabled(true);
                getBindLoanNumber().setDisabled(true);
                getBindEmployeeNumber().setDisabled(true);
                getBindEmployeeName().setDisabled(true);
                getBindLoanAmount().setDisabled(true);
                getBindLoanPaid().setDisabled(true);
                getBindLoanBalance().setDisabled(true);
                getBindLoanInstallment().setDisabled(false);
                getHeaderEditBinding().setDisabled(true);
                getApprovedByBinding().setDisabled(true);
            } else if (mode.equals("V")) {
                getDetailcreateBinding().setDisabled(true);
                getDetaildeleteBinding().setDisabled(true);
              
            }
            
        }

    public void setMonthlyLoanPopulationTableBinding(RichTable monthlyLoanPopulationTableBinding) {
        this.monthlyLoanPopulationTableBinding = monthlyLoanPopulationTableBinding;
    }

    public RichTable getMonthlyLoanPopulationTableBinding() {
        return monthlyLoanPopulationTableBinding;
    }

    public void setBindUnitName(RichInputText bindUnitName) {
        this.bindUnitName = bindUnitName;
    }

    public RichInputText getBindUnitName() {
        return bindUnitName;
    }

    public void setBindHeadDescription(RichInputText bindHeadDescription) {
        this.bindHeadDescription = bindHeadDescription;
    }

    public RichInputText getBindHeadDescription() {
        return bindHeadDescription;
    }

    public void setBindSlNo(RichInputText bindSlNo) {
        this.bindSlNo = bindSlNo;
    }

    public RichInputText getBindSlNo() {
        return bindSlNo;
    }

    public void setBindLoanNumber(RichInputText bindLoanNumber) {
        this.bindLoanNumber = bindLoanNumber;
    }

    public RichInputText getBindLoanNumber() {
        return bindLoanNumber;
    }

    public void setBindEmployeeNumber(RichInputText bindEmployeeNumber) {
        this.bindEmployeeNumber = bindEmployeeNumber;
    }

    public RichInputText getBindEmployeeNumber() {
        return bindEmployeeNumber;
    }

    public void setBindEmployeeName(RichInputText bindEmployeeName) {
        this.bindEmployeeName = bindEmployeeName;
    }

    public RichInputText getBindEmployeeName() {
        return bindEmployeeName;
    }

    public void setBindLoanAmount(RichInputText bindLoanAmount) {
        this.bindLoanAmount = bindLoanAmount;
    }

    public RichInputText getBindLoanAmount() {
        return bindLoanAmount;
    }

    public void setBindLoanPaid(RichInputText bindLoanPaid) {
        this.bindLoanPaid = bindLoanPaid;
    }

    public RichInputText getBindLoanPaid() {
        return bindLoanPaid;
    }

    public void setBindLoanBalance(RichInputText bindLoanBalance) {
        this.bindLoanBalance = bindLoanBalance;
    }

    public RichInputText getBindLoanBalance() {
        return bindLoanBalance;
    }

    public void setBindLoanInstallment(RichInputText bindLoanInstallment) {
        this.bindLoanInstallment = bindLoanInstallment;
    }

    public RichInputText getBindLoanInstallment() {
        return bindLoanInstallment;
    }

    public void setBindingOutputText(RichPanelLabelAndMessage bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichPanelLabelAndMessage getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setDetailcreateBinding(RichButton detailcreateBinding) {
        this.detailcreateBinding = detailcreateBinding;
    }

    public RichButton getDetailcreateBinding() {
        return detailcreateBinding;
    }

    public void setDetaildeleteBinding(RichButton detaildeleteBinding) {
        this.detaildeleteBinding = detaildeleteBinding;
    }

    public RichButton getDetaildeleteBinding() {
        return detaildeleteBinding;
    }

    public void editButtonAL(ActionEvent actionEvent) {
        if(approvedByBinding.getValue() != null){
            ADFUtils.showMessage("Approved Record can't be edited", 0);
            ADFUtils.setEL("#{pageFlowScope.mode}","V");
        }
        cevmodecheck();
    }

    public void saveButtonAL(ActionEvent actionEvent) {
       System.out.println("Save Button Click==>"+Message);

        if(Message.equalsIgnoreCase("E"))
               {
        System.out.println("Save Button Click Before Save & Close.");
        ADFUtils.findOperation("Commit").execute();
        System.out.println("Save Button Click After Save & Close.1");
        FacesMessage Message = new FacesMessage("Record Updated Successfully.");   
        Message.setSeverity(FacesMessage.SEVERITY_INFO);   
        FacesContext fc = FacesContext.getCurrentInstance();   
        fc.addMessage(null, Message); 
        System.out.println("Save Button Click After Save & Close.2");
        }
        else if(Message.equalsIgnoreCase("C"))
        {   
        System.out.println("Save Button Click Before Save.");
        ADFUtils.findOperation("Commit").execute();
        System.out.println("Save Button Click After Save.1");
        FacesMessage Message = new FacesMessage("Record Saved Successfully.");
        Message.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        System.out.println("Save Button Click After Save.2");
        }
    }

    public void approvedByVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
      
                        if(vce!=null) {
                         vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
                         OperationBinding op = (OperationBinding) ADFUtils.findOperation("approvedByForMonthlyLoanPopulation");
                         op.getParamsMap().put("unitCd", bindUnit.getValue().toString());
                         op.getParamsMap().put("EpC",(String) vce.getNewValue());
                         op.getParamsMap().put("authLevel", "AP");
                         op.getParamsMap().put("formName", "LCH");
                            op.execute();
                        }
    }

    public void setApprovedByBinding(RichInputComboboxListOfValues approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputComboboxListOfValues getApprovedByBinding() {
        return approvedByBinding;
    }


    public void LessThanValdiator(FacesContext facesContext, UIComponent uIComponent, Object oj) {
        if(oj!=null && bindLoanBalance.getValue()!=null){
          java.math.BigDecimal  ob = (java.math.BigDecimal)bindLoanBalance.getValue(); //loan balance
                java.math.BigDecimal ozz = (java.math.BigDecimal)oj;  //loan instalment
            if(ob.compareTo(ozz)==-1){
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                              "Loan Installment should be less than or equal to Loan Balance",
                                                              null));
                }
            }
    }
}
