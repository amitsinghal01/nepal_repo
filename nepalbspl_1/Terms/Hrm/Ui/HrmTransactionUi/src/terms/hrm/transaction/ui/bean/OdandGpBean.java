package terms.hrm.transaction.ui.bean;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import javax.faces.validator.ValidatorException;

import oracle.adf.model.OperationBinding;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;

import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;

import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;

import oracle.adf.view.rich.component.rich.input.RichSelectBooleanCheckbox;

import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;

import oracle.adfinternal.view.faces.context.AdfFacesContextImpl;

import oracle.jbo.Row;
import oracle.jbo.ViewObject;
import oracle.jbo.domain.Timestamp;

public class OdandGpBean {
    private RichInputComboboxListOfValues unitbinding;
    private RichInputText approvedBy;
    private RichInputDate approvedDateBinding;
    private RichSelectBooleanCheckbox checkBoxBinding;
    private RichSelectOneChoice typeBinding;
    private RichInputDate fromDateBinding;
    private RichInputDate toDateBinding;


    public OdandGpBean() {
    }

    public void ProcessAL(ActionEvent actionEvent) {
        OperationBinding op = (OperationBinding) ADFUtils.findOperation("ProcessGPOD");
        op.execute();
    }

    public void ApprovedbyVCE(ValueChangeEvent vce) {
        System.out.println(vce);
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        
                                 if(vce.getNewValue().equals(true))
                                 {
                                  String empCode=(String)(ADFUtils.evaluateEL("#{pageFlowScope.empCode}") == null ? "SWE161":ADFUtils.evaluateEL("#{pageFlowScope.empCode}"));
                                  vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
                                  oracle.binding.OperationBinding op = ADFUtils.findOperation("checkApprovalAuthority");
                                  op.getParamsMap().put("unitCd", getUnitbinding().getValue().toString());
                                  //op.getParamsMap().put("empCode", ADFUtils.evaluateEL("#{pageFlowScope.empCode}"));
                                  op.getParamsMap().put("empCode", empCode);
                                  op.getParamsMap().put("authLevel", "AP");
                                  if(typeBinding.getValue().equals("GP")){
                                  op.getParamsMap().put("formName", "GPA");
                                  }
                                  else{
                                  op.getParamsMap().put("formName", "ODA");
                                  }
                                  op.execute();
                                  if(op.getResult()!=null && op.getResult().equals("N"))
                                  {
                                      Row r = (Row) ADFUtils.evaluateEL("#{bindings.GatePassVO1Iterator.currentRow}");
                                      r.setAttribute("ApprovedBy", null);
                                      r.setAttribute("ApprovedDate", null);
                                      r.setAttribute("TransCheck", null);
                                      FacesMessage Message = new FacesMessage("Sorry You Are Not Authorized To Approve this Record");
                                      Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                                      FacesContext fc = FacesContext.getCurrentInstance();
                                      fc.addMessage(null, Message);
                                  }
                                  else
                                  {
                                      Timestamp dt=new Timestamp(System.currentTimeMillis());
                                      approvedDateBinding.setValue(dt);
                                      approvedBy.setValue(empCode);
                                     
                                  }
                                  
                                 }
        else{
                                    checkBoxBinding.setValue(false);
                                    approvedDateBinding.setValue(null);
                                    approvedBy.setValue(null);  
            AdfFacesContextImpl.getCurrentInstance().addPartialTarget(approvedBy);
            AdfFacesContextImpl.getCurrentInstance().addPartialTarget(approvedDateBinding);
            
        }
    }

    public void setUnitbinding(RichInputComboboxListOfValues unitbinding) {
        this.unitbinding = unitbinding;
    }

    public RichInputComboboxListOfValues getUnitbinding() {
        return unitbinding;
    }

    public void setApprovedBy(RichInputText approvedBy) {
        this.approvedBy = approvedBy;
    }

    public RichInputText getApprovedBy() {
        return approvedBy;
    }

    public void saveAL(ActionEvent actionEvent) {
        DCIteratorBinding dciter = (DCIteratorBinding) ADFUtils.findIterator("GatePassVO1Iterator");

               ViewObject voh = dciter.getViewObject();
               String Trans =(String) (voh.getCurrentRow().getAttribute("TransCheck") != null ?
                            voh.getCurrentRow().getAttribute("TransCheck") :"N");
               System.out.println("Trans is===="+Trans);
              if(Trans.equalsIgnoreCase("Y")||Trans.equalsIgnoreCase("true"))
                {
                  System.out.println("committt");
              ADFUtils.findOperation("Commit").execute();
                FacesMessage Message = new FacesMessage("Record Save Successfully!");
                  Message.setSeverity(FacesMessage.SEVERITY_INFO);
                  FacesContext fc=FacesContext.getCurrentInstance();
                  fc.addMessage(null, Message);
              voh.setNamedWhereClauseParam("bindMode", "V");
           voh.executeQuery();
              }
                else
                {
                    ADFUtils.showMessage("TransCheck must be check to save the particular row", 2);
                    }
    }


    public void setApprovedDateBinding(RichInputDate approvedDateBinding) {
        this.approvedDateBinding = approvedDateBinding;
    }

    public RichInputDate getApprovedDateBinding() {
        return approvedDateBinding;
    }

    public void setCheckBoxBinding(RichSelectBooleanCheckbox checkBoxBinding) {
        this.checkBoxBinding = checkBoxBinding;
    }

    public RichSelectBooleanCheckbox getCheckBoxBinding() {
        return checkBoxBinding;
    }

    public void setTypeBinding(RichSelectOneChoice typeBinding) {
        this.typeBinding = typeBinding;
    }

    public RichSelectOneChoice getTypeBinding() {
        return typeBinding;
    }

    public void DateValidatorForODandGP(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if(object!=null && fromDateBinding.getValue()!=null){
                 oracle.jbo.domain.Date d=(oracle.jbo.domain.Date)object;
                 oracle.jbo.domain.Date d1=(oracle.jbo.domain.Date)fromDateBinding.getValue();          
                 if(d.compareTo(d1)==-1){
                  throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,"Date cannot be null",null));
                  
                 }
                 
               }

    }

    public void setFromDateBinding(RichInputDate fromDateBinding) {
        this.fromDateBinding = fromDateBinding;
    }

    public RichInputDate getFromDateBinding() {
        return fromDateBinding;
    }

    public void setToDateBinding(RichInputDate toDateBinding) {
        this.toDateBinding = toDateBinding;
    }

    public RichInputDate getToDateBinding() {
        return toDateBinding;
    }

//    public void DateValidationODandGP(FacesContext facesContext, UIComponent uIComponent, Object object) {
//        if (object != null && fromDateBinding.getValue()!=null) {
//                   oracle.jbo.domain.Date fromDate = (oracle.jbo.domain.Date) fromDateBinding.getValue();
//                   oracle.jbo.domain.Date toDate = (oracle.jbo.domain.Date) object;
//                   if (toDate.compareTo(fromDate) == -1) {
//                       throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
//                                                                     "To date can not be less then From date.",
//                                                                     null));
//                   }
//
//               }
//
//    }
}
