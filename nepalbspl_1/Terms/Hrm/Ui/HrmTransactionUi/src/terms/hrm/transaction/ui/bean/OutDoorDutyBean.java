package terms.hrm.transaction.ui.bean;

import java.lang.reflect.Method;


import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UISelectItem;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;

import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.input.RichSelectOneRadio;

import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichPanelLabelAndMessage;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.adf.view.rich.event.ReturnPopupDataEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.domain.Date;

public class OutDoorDutyBean {
    private RichInputText unitNameBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputComboboxListOfValues employeeNumberBinding;
    private RichInputText employeeNameBinding;
    private RichInputText odNumberBinding;
    private RichSelectOneRadio odStartFromBinding;
    private RichSelectOneRadio odTypeBinding;
    private RichInputDate odDateBinging;
    private RichInputDate odEndDateBinding;
    private RichInputDate startTimeBinding;
    private RichInputDate endTimeBinding;
    private RichInputText shiftCodeBinding;
 
    private RichInputText placeOfVisitBinding;
    private RichInputText remarksBinding;
  
    private RichInputText approvedByNameBinding;
    private RichInputText createdByBinding;
    private RichInputDate createdDateBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichInputComboboxListOfValues approvedByBinding;
    private String message = "C";
    private RichPanelLabelAndMessage bindingOutputText;
    private RichSelectOneChoice odStatusBinding;
    private RichSelectOneChoice purposeBinding;
    private RichInputDate odEDateTransBinding;
    private RichInputDate odSDateTransBinding;
    private RichInputDate approvedByDateBinding;


    public OutDoorDutyBean() {
    }

    public void saveButtonAL(ActionEvent actionEvent) {
                    ADFUtils.setLastUpdatedBy("OutDoorDutyVO1Iterator","LastUpdatedBy");
                       System.out.println("In SaveAl");
                               OperationBinding op= ADFUtils.findOperation("generateOdNumber");
                               op.execute();
                               System.out.println("In SaveAl After Call method");
                               if (message.equalsIgnoreCase("C")){
                                   OperationBinding op2= ADFUtils.findOperation("Commit");
                                   System.out.println("In SaveAl create");
                                   op2.execute();
                               FacesMessage Message = new FacesMessage("Record Save Successfully!");
                               Message.setSeverity(FacesMessage.SEVERITY_INFO);
                               FacesContext fc=FacesContext.getCurrentInstance();
                               fc.addMessage(null, Message);
                               }
                               else{
                                       OperationBinding op3= ADFUtils.findOperation("Commit");
                                       System.out.println("In SaveAl edit");
                                       op3.execute();
                                   FacesMessage Message = new FacesMessage("Record Updated Successfully!");
                                   Message.setSeverity(FacesMessage.SEVERITY_INFO);
                                   FacesContext fc=FacesContext.getCurrentInstance();
                                   fc.addMessage(null, Message);
                                   }
                               } 
//        OperationBinding op = ADFUtils.findOperation("generateOdNumber");
//               Object rst = op.execute();
//               
//               System.out.println("Result is===> "+op.getResult());
//               
//               
//               System.out.println("--------Commit-------");
//                   System.out.println("value aftr getting result--->?"+rst);
//                   
//               
//                   if (rst.toString() != null && rst.toString() != "" && rst.toString()!="N") {
//                       if (op.getErrors().isEmpty()) {
//                           ADFUtils.findOperation("Commit").execute();
//                           FacesMessage Message = new FacesMessage("Record Saved Successfully. New Entry No. is "+rst+".");   
//                           Message.setSeverity(FacesMessage.SEVERITY_INFO);   
//                           FacesContext fc = FacesContext.getCurrentInstance();   
//                           fc.addMessage(null, Message);  
//               
//                       }
//                   }
//                   
//                   if (rst.toString().equalsIgnoreCase("N")) {
//                       if (op.getErrors().isEmpty()) {
//                           ADFUtils.findOperation("Commit").execute();
//                           FacesMessage Message = new FacesMessage("Record Update Successfully.");   
//                           Message.setSeverity(FacesMessage.SEVERITY_INFO);   
//                           FacesContext fc = FacesContext.getCurrentInstance();   
//                           fc.addMessage(null, Message);  
//                   
//                       }

                   

    public void setUnitNameBinding(RichInputText unitNameBinding) {
        this.unitNameBinding = unitNameBinding;
    }

    public RichInputText getUnitNameBinding() {
        return unitNameBinding;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setEmployeeNumberBinding(RichInputComboboxListOfValues employeeNumberBinding) {
        this.employeeNumberBinding = employeeNumberBinding;
    }

    public RichInputComboboxListOfValues getEmployeeNumberBinding() {
        return employeeNumberBinding;
    }

    public void setEmployeeNameBinding(RichInputText employeeNameBinding) {
        this.employeeNameBinding = employeeNameBinding;
    }

    public RichInputText getEmployeeNameBinding() {
        return employeeNameBinding;
    }

    public void setOdNumberBinding(RichInputText odNumberBinding) {
        this.odNumberBinding = odNumberBinding;
    }

    public RichInputText getOdNumberBinding() {
        return odNumberBinding;
    }

    public void setOdStartFromBinding(RichSelectOneRadio odStartFromBinding) {
        
        this.odStartFromBinding = odStartFromBinding;
        
    }

    public RichSelectOneRadio getOdStartFromBinding() {
    
        return odStartFromBinding;
    }

    public void setOdTypeBinding(RichSelectOneRadio odTypeBinding) {
        this.odTypeBinding = odTypeBinding;
    }

    public RichSelectOneRadio getOdTypeBinding() {
        return odTypeBinding;
    }

    public void setOdDateBinging(RichInputDate odDateBinging) {
        this.odDateBinging = odDateBinging;
    }

    public RichInputDate getOdDateBinging() {
        return odDateBinging;
    }

    public void setOdEndDateBinding(RichInputDate odEndDateBinding) {
        this.odEndDateBinding = odEndDateBinding;
    }

    public RichInputDate getOdEndDateBinding() {
        return odEndDateBinding;
    }

    public void setStartTimeBinding(RichInputDate startTimeBinding) {
        this.startTimeBinding = startTimeBinding;
    }

    public RichInputDate getStartTimeBinding() {
        return startTimeBinding;
    }

    public void setEndTimeBinding(RichInputDate endTimeBinding) {
        this.endTimeBinding = endTimeBinding;
    }

    public RichInputDate getEndTimeBinding() {
        return endTimeBinding;
    }

    public void setShiftCodeBinding(RichInputText shiftCodeBinding) {
        this.shiftCodeBinding = shiftCodeBinding;
    }

    public RichInputText getShiftCodeBinding() {
        return shiftCodeBinding;
    }


    public void setPlaceOfVisitBinding(RichInputText placeOfVisitBinding) {
        this.placeOfVisitBinding = placeOfVisitBinding;
    }

    public RichInputText getPlaceOfVisitBinding() {
        return placeOfVisitBinding;
    }

    public void setRemarksBinding(RichInputText remarksBinding) {
        this.remarksBinding = remarksBinding;
    }

    public RichInputText getRemarksBinding() {
        return remarksBinding;
    }

 
    public void setApprovedByBinding(RichInputComboboxListOfValues approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputComboboxListOfValues getApprovedByBinding() {
        return approvedByBinding;
    }

    public void setApprovedByNameBinding(RichInputText approvedByNameBinding) {
        this.approvedByNameBinding = approvedByNameBinding;
    }

    public RichInputText getApprovedByNameBinding() {
        return approvedByNameBinding;
    }

    public void setCreatedByBinding(RichInputText createdByBinding) {
        this.createdByBinding = createdByBinding;
    }

    public RichInputText getCreatedByBinding() {
        return createdByBinding;
    }

    public void setCreatedDateBinding(RichInputDate createdDateBinding) {
        this.createdDateBinding = createdDateBinding;
    }

    public RichInputDate getCreatedDateBinding() {
        return createdDateBinding;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }
    
    private void cevmodecheck(){
            if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
                cevModeDisableComponent("V");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("C");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("E");
            }
        }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
               try {
                   Method method1 =
                       component.getClass().getMethod("setDisabled", boolean.class);
                   if (method1 != null) {
                       method1.invoke(component, valueComponent);
                   }
               } catch (NoSuchMethodException e) {
                   try {
                       Method method =
                           component.getClass().getMethod("setReadOnly", boolean.class);
                       if (method != null) {
                           method.invoke(component, valueComponent);
                       }
                   } catch (Exception e1) {
                       // e.printStackTrace();//silently eat this exception.
                   }


               } catch (Exception e) {
                   // e.printStackTrace();//silently eat this exception.
               }
               List<UIComponent> childComponents = component.getChildren();
               for (UIComponent comp : childComponents) {
                   makeComponentHierarchyReadOnly(comp, valueComponent);
               }
           }
    
    public void cevModeDisableComponent(String mode) {
    if (mode.equals("E")) {
                  getUnitCodeBinding().setDisabled(true);
                  getUnitNameBinding().setDisabled(true);
                  getEmployeeNumberBinding().setDisabled(true);
                  getEmployeeNameBinding().setDisabled(true);
                  getOdNumberBinding().setDisabled(true);
                  getApprovedByNameBinding().setDisabled(true);
                  getCreatedByBinding().setDisabled(true);
               getApprovedByBinding().setDisabled(true);
                   getCreatedDateBinding().setDisabled(true);
               getShiftCodeBinding().setDisabled(true);
               getStartTimeBinding().setDisabled(true);
               getEndTimeBinding().setDisabled(true);
               getApprovedByDateBinding().setDisabled(true);
               
           } else if (mode.equals("C")) {
               getUnitCodeBinding().setDisabled(true);
               getUnitNameBinding().setDisabled(true);
               getEmployeeNumberBinding().setDisabled(false);
               getEmployeeNameBinding().setDisabled(true);
               getOdNumberBinding().setDisabled(true);
               getApprovedByBinding().setDisabled(true);
               getApprovedByNameBinding().setDisabled(true);
               getShiftCodeBinding().setDisabled(true);
               getStartTimeBinding().setDisabled(true);
               getEndTimeBinding().setDisabled(true);
               getCreatedByBinding().setDisabled(true);
                getCreatedDateBinding().setDisabled(true);
               getApprovedByDateBinding().setDisabled(true);
           } else if (mode.equals("V")) {
              
           }
           
       }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

   

    public void odsdateVCL(ValueChangeEvent vce) {
        
       vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
       vce.getNewValue();
        System.out.println("Out time is 1----->> "+vce.getNewValue());
               String unitCd=(String) getUnitCodeBinding().getValue();
               System.out.println("Unit Value is-----.."+unitCd);
               String empCode=(String) getEmployeeNumberBinding().getValue();
               System.out.println("Emp Value is-----.."+empCode);
               Date startdate= (Date)vce.getNewValue();
               System.out.println("Out time is 2 ----->> "+startdate);
        String sttt=(String) getOdStartFromBinding().getValue();
        String hellld=(String) getOdTypeBinding().getValue();
        System.out.println("sttt is-----.."+sttt + hellld );
        
        
    
               OperationBinding op=ADFUtils.findOperation("getshiftCode");
               op.getParamsMap().put("unitCd", unitCd);
               op.getParamsMap().put("empCode", empCode);
               op.getParamsMap().put("startdate", startdate);
               op.execute(); 
               
        OperationBinding op1=ADFUtils.findOperation("GetOutTime");
        op1.getParamsMap().put("unitCd", unitCd);
        op1.getParamsMap().put("empCode", empCode);
        op1.getParamsMap().put("startdate", startdate);
        op1.execute();
        
               
        
    }

  

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }


    public void approvedByVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
                System.out.println("In Bean");
                //String userName = (String) ADFContext.getCurrent().getPageFlowScope().get("userName");
                String userName = (String)vce.getNewValue();
                                        if(vce!=null)
                                        {
                                            System.out.println("In Bean in if");
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
                                         OperationBinding op = (OperationBinding) ADFUtils.findOperation("approvedByForOutDoorDutyApplication");
                                         op.getParamsMap().put("unitCd", unitCodeBinding.getValue().toString());
                                         op.getParamsMap().put("empCode", userName);
                                        op.getParamsMap().put("authLevel", "AP");
                                         op.getParamsMap().put("formName", "ODA");
                                         op.execute();

                                            OperationBinding op2 = (OperationBinding) ADFUtils.findOperation("approvedByDate");
                                            op2.execute();
                                        } 
    }

    public void setBindingOutputText(RichPanelLabelAndMessage bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichPanelLabelAndMessage getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setOdStatusBinding(RichSelectOneChoice odStatusBinding) {
        this.odStatusBinding = odStatusBinding;
    }

    public RichSelectOneChoice getOdStatusBinding() {
        return odStatusBinding;
    }

    public void setPurposeBinding(RichSelectOneChoice purposeBinding) {
        this.purposeBinding = purposeBinding;
    }

    public RichSelectOneChoice getPurposeBinding() {
        return purposeBinding;
    }

    public void odtypeVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        System.out.println("bean Type");
        String odtyp = (String) getOdTypeBinding().getValue();
        System.out.println(odtyp);
        OperationBinding op = (OperationBinding) ADFUtils.findOperation("OpTypeForOutDoorDuty");
        op.getParamsMap().put("odtyp", odtyp);
        op.execute();
        
    }


    public void setOdEDateTransBinding(RichInputDate odEDateTransBinding) {
        this.odEDateTransBinding = odEDateTransBinding;
    }

    public RichInputDate getOdEDateTransBinding() {
        return odEDateTransBinding;
    }

    public void setOdSDateTransBinding(RichInputDate odSDateTransBinding) {
        this.odSDateTransBinding = odSDateTransBinding;
    }

    public RichInputDate getOdSDateTransBinding() {
        return odSDateTransBinding;
    }

    public void odEDateVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        vce.getNewValue();
        String unitCd=(String) getUnitCodeBinding().getValue();
        System.out.println("Unit Value is-----.."+unitCd);
        String empCode=(String) getEmployeeNumberBinding().getValue();
        System.out.println("Emp Value is-----.."+empCode);
         
                Date enddate= (Date)vce.getNewValue();
                System.out.println("enddate ----->> "+enddate);
        
        Date startdate= (Date) getOdDateBinging().getValue();
                
         OperationBinding op1=ADFUtils.findOperation("GetOutTime");
        op1.getParamsMap().put("unitCd", unitCd);
        op1.getParamsMap().put("empCode", empCode);
        op1.getParamsMap().put("startdate", startdate);
         op1.execute();
         
    }

    public void setApprovedByDateBinding(RichInputDate approvedByDateBinding) {
        this.approvedByDateBinding = approvedByDateBinding;
    }

    public RichInputDate getApprovedByDateBinding() {
        return approvedByDateBinding;
    }
}


