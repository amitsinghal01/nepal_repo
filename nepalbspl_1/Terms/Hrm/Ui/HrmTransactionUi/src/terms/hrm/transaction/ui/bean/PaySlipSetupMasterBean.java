package terms.hrm.transaction.ui.bean;

import oracle.adf.view.rich.component.rich.data.RichTable;

public class PaySlipSetupMasterBean {
    private RichTable paySlipSetupMasterTableBinding;
    private String editAction="V";

    public PaySlipSetupMasterBean() {
    }

    public void setPaySlipSetupMasterTableBinding(RichTable paySlipSetupMasterTableBinding) {
        this.paySlipSetupMasterTableBinding = paySlipSetupMasterTableBinding;
    }

    public RichTable getPaySlipSetupMasterTableBinding() {
        return paySlipSetupMasterTableBinding;
    }

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }
}
