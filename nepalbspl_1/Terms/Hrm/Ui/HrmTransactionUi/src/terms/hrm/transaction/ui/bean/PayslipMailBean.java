package terms.hrm.transaction.ui.bean;

import java.sql.Connection;

import java.sql.SQLException;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;

import javax.mail.internet.MimeMultipart;

import javax.naming.Context;
import javax.naming.InitialContext;

import javax.sql.DataSource;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

import net.sf.jasperreports.engine.JasperReport;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;

import oracle.adf.view.rich.component.rich.nav.RichButton;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;


public class PayslipMailBean {
    private RichInputComboboxListOfValues bindUnitCode;
    private RichInputComboboxListOfValues bindDeptCd;
    private RichInputComboboxListOfValues bindLevelCd;
    private RichSelectOneChoice bindMonth;
    private RichInputText bindYear;
    private RichButton sendButton;

    public PayslipMailBean() {
    }
    
    Properties emailProperties;
    Session mailSession;
    MimeMessage emailMessage;
    String toWhom;
    String messagae;
    String subject;
    String path=payslipPathName();
    String pathforjasper=payslipJasperFileName();
    String file_name="";

    public void PDFPopulate(ActionEvent actionEvent) {
        System.out.println("Inside PDF Populate in bean!");
        System.out.println(bindMonth.getValue().toString() + ' ' + bindYear.getValue().toString());
         System.out.println(bindUnitCode.getValue().toString());
         System.out.println(bindDeptCd.getValue()==null?'%':bindDeptCd.getValue().toString());
         System.out.println(bindLevelCd.getValue()==null?'%':bindLevelCd.getValue().toString());
         
        OperationBinding op=ADFUtils.findOperation("payslippopulate");
                op.getParamsMap().put("unitCd", bindUnitCode.getValue().toString());
                op.getParamsMap().put("deptCd", bindDeptCd.getValue()==null?'%':bindDeptCd.getValue().toString());
                op.getParamsMap().put("levelCd", bindLevelCd.getValue()==null?'%':bindLevelCd.getValue().toString());
                op.getParamsMap().put("Yr", bindYear.getValue().toString());
               op.getParamsMap().put("Mon", bindMonth.getValue().toString());
                op.execute();
                
        OperationBinding op1=ADFUtils.findOperation("DisableSendMail");
               Object result = op1.execute();
               System.out.println("Y "+ result);
               if(result.equals("Y")) {
               getSendButton().setDisabled(false);
               }
    }
    
    public String payslipPathName(){
           String path="";
           OperationBinding binding = ADFUtils.findOperation("PayslipPathName");
           binding.execute();
           System.out.println("Binding Result :"+binding.getResult());
           if(binding.getResult() != null){
                path = binding.getResult().toString();
                                
           }else{
               ADFUtils.showMessage("Invalid Path Name for Payslip", 0);
           }
           
           return path;
       }
    
    public String payslipJasperFileName(){
           String pathforjasper="";
           OperationBinding binding = ADFUtils.findOperation("payslipJasperFileName");
           binding.execute();
           System.out.println("Binding Result1:"+binding.getResult());
           if(binding.getResult() != null){
                pathforjasper = binding.getResult().toString();
                System.out.println("path"+pathforjasper);
                                
           }else{
               ADFUtils.showMessage("Invalid Path Name for Jasper", 0);
           }
           
           return pathforjasper;
       }
    
    public void checkbox() throws Exception
        {
                Connection conn=null;
                file_name ="r_salary_slip_officer.jrxml";
                String path2="pathforjasper+r_salary_slip_officer.jrxml";
                System.out.println("pathhhhh");
            System.out.println(pathforjasper+file_name);
                 
                 try{
                 JasperReport jasp = JasperCompileManager.compileReport(pathforjasper+file_name);
                 
                 //JasperReport jasp  = (JasperReport) JRLoader.loadObjectFromFile("/home/adfbeta03/Downloads/r_salary_slip_officer.jasper");
                 
                 
                // Connection conn=null;
                // JRDataSource jrd = new JREmptyDataSource();
    //            DCIteratorBinding pvIter = (DCIteratorBinding) getBindings().get("PaySlipPopulateVO1Iterator");
    //             String e = (String)pvIter.getCurrentRow().getAttribute("EmperndedEmpNo");
    //             System.out.println("Emp" + e);
                     
                     
                             DCIteratorBinding pvIter = ADFUtils.findIterator("PaySlipPopulateVO1Iterator");
            RowSetIterator rsi=pvIter.getViewObject().createRowSetIterator(null);
           
    //                 String emp = (String)pvIter.getCurrentRow().getAttribute("EmperndedEmpNo");
    //               String email = (String)pvIter.getCurrentRow().getAttribute("Email");
                   
            while(rsi.hasNext())
                {

                    Row r = rsi.next();
                 
                     String e = (String)r.getAttribute("EmperndedEmpNo");
                 
                 if(r.getAttribute("Multiselect") == "True" && r.getAttribute("Email") !=null)
                 {
                 Map m = new HashMap();
                 m.put("p_emp_cd", e);
                 m.put("p_unit", bindUnitCode.getValue().toString());
                 m.put("p_month", bindMonth.getValue().toString());
                 m.put("p_year", bindYear.getValue().toString());
                 m.put("p_level", "%");
                 
                 
                 System.out.println("m" + m);
                 conn = getConnection( );
                 
                //  System.out.println(conn);
                 
                 JasperPrint jsprint = JasperFillManager.fillReport(jasp,m,conn);
                 
                //  System.out.println(jsprint);
                   //  String str = "/home/adfbeta03/pdffile/"+e+bindMonth.getValue().toString()+bindYear.getValue().toString()+".pdf";
                 String str = String.format("%s%s_%s_%s.pdf",path,e.replace('/', '_'),bindMonth.getValue().toString(),bindYear.getValue().toString());
                 
                 JasperExportManager.exportReportToPdfFile(jsprint,str);
                 }
    //                 else {
    //                      
    //                     ADFUtils.showMessage("Email should not be Null if checked", 0);
    //                 }
                 }
                     rsi.closeRowSetIterator();
                 }
                 catch(JRException e) {
                     
                     e.printStackTrace();
                 }
                 finally{
                     try{
                             conn.close( );
                         }
                         catch(SQLException e) {
                             
                             e.printStackTrace();
                         }
                     
                     }
            }
    
    public Connection getConnection() throws Exception{
                Context ctx = null;
                Connection conn = null;
                ctx = new InitialContext();
                DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/ApplicationDBDS");
                conn = ds.getConnection();
                return conn;
            }

    public void SendMailAL(ActionEvent actionEvent) throws Exception,AddressException, MessagingException{
        checkbox();
                setMailServerProperties();
               createEmailMessage();
               // sendEmail();
               
               OperationBinding op=ADFUtils.findOperation("InsertPayslipDetailData");
               op.getParamsMap().put("unitCd", bindUnitCode.getValue().toString());
               op.getParamsMap().put("Mon", bindMonth.getValue().toString());
               op.execute();
               
                OperationBinding binding = ADFUtils.findOperation("RemovePopulateData");
                binding.execute();
               
                OperationBinding op2= ADFUtils.findOperation("Commit");
                System.out.println("In SaveAl create");
                op2.execute();
                
        bindDeptCd.setValue("");
               bindLevelCd.setValue("");
               bindMonth.setValue("");
               bindYear.setValue("");
    }
    
    public void setMailServerProperties() {
            
            OperationBinding op1 = ADFUtils.findOperation("InfoFunction");
            Object result = op1.execute();
            
            System.out.println("Result" + result);
            
            Map<String,String> mn = (Map<String,String>)result;
            
            System.out.println("Hello" + mn);
            
            String emailPort = mn.get("Port");
            
            String emailauth = mn.get("Auth");
                       
             String emailtls = mn.get("Starttls");
                        
             String emailhost =  mn.get("Host");  
             
    //  String emailPort = "587"; //gmail's smtp port
           emailProperties = System.getProperties();
           emailProperties.put("mail.smtp.port", emailPort);
           emailProperties.put("mail.smtp.auth", emailauth); //"true"
           emailProperties.put("mail.smtp.starttls.enable", emailtls); //"true"
           emailProperties.put("mail.smtp.host", emailhost); //"smtp.gmail.com"

       }
    
    public void createEmailMessage() throws AddressException, MessagingException {
           
            OperationBinding op1 = ADFUtils.findOperation("InfoFunction");
            Object result = op1.execute();
            
            System.out.println("Result" + result);
            
            Map<String,String> mn = (Map<String,String>)result;
            
            System.out.println("Hello" + mn);
            
            String mailSubject = mn.get("Subject");
            
            String mailSignature = mn.get("Signature");
            
            String mailBody = mn.get("Body");;
           
           
            DCIteratorBinding pvIter = ADFUtils.findIterator("PaySlipPopulateVO1Iterator");
    RowSetIterator rsi=pvIter.getViewObject().createRowSetIterator(null);
         
    //                 String emp = (String)pvIter.getCurrentRow().getAttribute("EmperndedEmpNo");
    //               String email = (String)pvIter.getCurrentRow().getAttribute("Email");
                 
          while(rsi.hasNext())
              {

                  Row r = rsi.next();
              
              String emp = (String)r.getAttribute("EmperndedEmpNo");
                  String email = (String)r.getAttribute("Email");
              
               if(r.getAttribute("Multiselect") == "True" && r.getAttribute("Email") !=null)
               {
              System.out.println("emp1" + emp);
              System.out.println("email1" + email);
              System.out.println("Check" + r.getAttribute("Multiselect"));
    //      System.out.println("emp1" + emp);
    //         System.out.println("email1" + email);
          
            toWhom =  email;             //"gaurav.tyagi@mawaimail.com";
            subject = String.format("%s %s-%s",mailSubject,bindMonth.getValue().toString(),bindYear.getValue().toString());
            messagae = String.format("Dear %s,",r.getAttribute("EmpFirstName"));
            String[] toEmails = { toWhom };
            String emailSubject = subject;
            String emailBody = String.format("%s\n\n%s %s-%s.\n\n\nRegards,\n%s",messagae,mailBody,bindMonth.getValue().toString(),bindYear.getValue().toString(),mailSignature);
            System.out.println("to---->" + toEmails + "subject is-->" + emailSubject + "body is--->" + emailBody);
            mailSession = Session.getDefaultInstance(emailProperties, null);
            emailMessage = new MimeMessage(mailSession);

            for (int i = 0; i < toEmails.length; i++) {
                emailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmails[i]));
            }

            emailMessage.setSubject(emailSubject);

            //1) create MimeBodyPart object and set your message content
            BodyPart messageBodyPart1 = new MimeBodyPart();
                   messageBodyPart1.setText(emailBody);

                   //2) create new MimeBodyPart object and set DataHandler object to this object
                   MimeBodyPart messageBodyPart2 = new MimeBodyPart();

                     
                   String filename = String.format("%s/%s_%s_%s.pdf",path,emp.replace('/', '_'),bindMonth.getValue().toString(),bindYear.getValue().toString());
                   String file = String.format("%s_%s_%s.pdf",emp.replace('/', '_'),bindMonth.getValue().toString(),bindYear.getValue().toString());
                   System.out.println("Exact path--->" + filename);
                 javax.activation.DataSource  source = new FileDataSource(filename);
                   messageBodyPart2.setDataHandler(new DataHandler(source));
                   messageBodyPart2.setFileName(file);


                   //5) create Multipart object and add MimeBodyPart objects to this object
                   Multipart multipart = new MimeMultipart();
                   multipart.addBodyPart(messageBodyPart1);
    multipart.addBodyPart(messageBodyPart2);

            //6) set the multiplart object to the message object
            emailMessage.setContent(multipart);
                sendEmail();
                
                 }
    //            else {
    //                 
    //                ADFUtils.showMessage("Email should not be Null if checked", 0);
    //            }
                 
            // emailMessage.setContent(emailBody, "text/html");//for a html email
            //emailMessage.setText(emailBody);// for a text email
            }
            rsi.closeRowSetIterator();    
        }
    
    public void sendEmail() {

           OperationBinding op1 = ADFUtils.findOperation("InfoFunction");
           Object result = op1.execute();
           
           System.out.println("Result" + result);
           
           Map<String,String> mn = (Map<String,String>)result;
           
           System.out.println("Hello" + mn);
           
           String mailhost = mn.get("Host");
           
           String mailUsername = mn.get("Username");
           
           String emailPassword = mn.get("Password");;


           String emailHost = mailhost;//"smtp.gmail.com";
           String fromUser = mailUsername;  // "gauravtyagi961@gmail.com"; //just the id alone without @gmail.com
           String fromUserEmailPassword = emailPassword;  //"gauravtyagi11";
                   Transport transport = null;
                   try {
                       transport = mailSession.getTransport("smtp");
                   } catch (NoSuchProviderException e) {
                       System.out.println("No such Provider Exception");
                   }
                   try {
                       transport.connect(emailHost, fromUser, fromUserEmailPassword);
                       transport.sendMessage(emailMessage, emailMessage.getAllRecipients());
                       transport.close();
                       StringBuilder msgEmail = new StringBuilder("<html><body><b>Email Sent</b></body></html>");
                       FacesMessage msg = new FacesMessage(msgEmail.toString());
                       msg.setSeverity(FacesMessage.SEVERITY_INFO);
                       FacesContext.getCurrentInstance().addMessage("No Network Error !", msg);
                       System.out.println("Email sent successfully.");
                   } catch (MessagingException e) {
                       System.out.println("somthing went wrong-->Messaging Exception" + e);
    StringBuilder msgEmail = new StringBuilder("<html><body><b>Something went wrong !</b>");
               msgEmail.append("<p><b>please provide correct parameters</b></p>");
               msgEmail.append("</body></html>");
               FacesMessage msg = new FacesMessage(msgEmail.toString());
               msg.setSeverity(FacesMessage.SEVERITY_WARN);
               FacesContext.getCurrentInstance().addMessage("No Network Error !", msg);

           }

       }
    
    public void selelctAllAL(ActionEvent actionEvent) {
           OperationBinding binding = ADFUtils.findOperation("selectAndDeselectMethod");
                      binding.getParamsMap().put("mode_type", "S");
                      binding.getParamsMap().put("grant_type", "A");
                      binding.execute();        
       }

       public void deselectAllAL(ActionEvent actionEvent) {
           OperationBinding binding = ADFUtils.findOperation("selectAndDeselectMethod");
                     binding.getParamsMap().put("mode_type", "D");
                     binding.getParamsMap().put("grant_type", "A");
                     binding.execute();
       }
       
       

    public void setBindUnitCode(RichInputComboboxListOfValues bindUnitCode) {
        this.bindUnitCode = bindUnitCode;
    }

    public RichInputComboboxListOfValues getBindUnitCode() {
        return bindUnitCode;
    }

    public void setBindDeptCd(RichInputComboboxListOfValues bindDeptCd) {
        this.bindDeptCd = bindDeptCd;
    }

    public RichInputComboboxListOfValues getBindDeptCd() {
        return bindDeptCd;
    }

    public void setBindLevelCd(RichInputComboboxListOfValues bindLevelCd) {
        this.bindLevelCd = bindLevelCd;
    }

    public RichInputComboboxListOfValues getBindLevelCd() {
        return bindLevelCd;
    }

    public void setBindMonth(RichSelectOneChoice bindMonth) {
        this.bindMonth = bindMonth;
    }

    public RichSelectOneChoice getBindMonth() {
        return bindMonth;
    }

    public void setBindYear(RichInputText bindYear) {
        this.bindYear = bindYear;
    }

    public RichInputText getBindYear() {
        return bindYear;
    }

    public void setSendButton(RichButton sendButton) {
        this.sendButton = sendButton;
    }

    public RichButton getSendButton() {
        return sendButton;
    }
}
