package terms.hrm.transaction.ui.bean;

import java.io.BufferedWriter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;

import java.io.InputStream;
import java.io.OutputStream;

import java.nio.file.Files;

import java.nio.file.Paths;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.model.binding.DCIteratorBinding;

import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;

public class PfEcrDataBean {
    private String editAction="V";
    private RichSelectOneChoice monBinding;
    private RichInputText yearBinding;
    private RichSelectOneChoice ecrBinding;

    public PfEcrDataBean() {
       
    }
    
    /**
         * Use Streams when you are dealing with raw data
         * @param data
         */
        private static void writeUsingOutputStream(String data) {
            OutputStream os = null;
            try {
                os = new FileOutputStream(new File("/home/adfbeta4/os.csv"));
                os.write(data.getBytes(), 0, data.length());
            } catch (IOException e) {
                e.printStackTrace();
            }finally{
                try {
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        
        /**
         * Use Files class from Java 1.7 to write files, internally uses OutputStream
         * @param data
         */
        private static void writeUsingFiles(String data) {
            try {
                Files.write(Paths.get("/home/adfbeta4/files.csv"), data.getBytes());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        /**
         * Use BufferedWriter when number of write operations are more
         * It uses internal buffer to reduce real IO operations and saves time
         * @param data
         * @param noOfLines
         */
        private static void writeUsingBufferedWriter(String data, int noOfLines) {
            File file = new File("/home/adfbeta4/BufferedWriter.csv");
            FileWriter fr = null;
            BufferedWriter br = null;
            String dataWithNewLine=data+System.getProperty("line.separator");
            try{
                fr = new FileWriter(file);
                br = new BufferedWriter(fr);
                for(int i = noOfLines; i>0; i--){
                    br.write(dataWithNewLine);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }finally{
                try {
                    br.close();
                    fr.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        /**
         * Use FileWriter when number of write operations are less
         * @param data
         */
        private static void writeUsingFileWriter(String data) {
            File file = new File("/home/adfbeta4/FileWriter.csv");
            FileWriter fr = null;
            try {
                fr = new FileWriter(file);
                fr.write(data);
            } catch (IOException e) {
                e.printStackTrace();
            }finally{
                //close resources
                try {
                    fr.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    public void populateDetailProcess(ActionEvent actionEvent) {
        
        if(monBinding.getValue()== null){
            ADFUtils.showMessage("Month must have some value.", 0);
        }else if(yearBinding.getValue()==null){
            ADFUtils.showMessage("Year must have some value.", 0);
        }else if(ecrBinding.getValue()==null){
            ADFUtils.showMessage("ECR For must have some value.", 0);
        }else{
        OperationBinding Pop = (OperationBinding) ADFUtils.findOperation("populatePfECRData");
        Object PopObj = Pop.execute();

        if (Pop.getResult() != null && Pop.getResult().toString().equalsIgnoreCase("S")) {
//            FacesMessage Message = new FacesMessage("Record Populated successfully.");
//            Message.setSeverity(FacesMessage.SEVERITY_INFO);
//            FacesContext fc = FacesContext.getCurrentInstance();
//            fc.addMessage(null, Message);
            
        }

        else if(Pop.getResult() != null && Pop.getResult().toString().equalsIgnoreCase("V"))
        {
            FacesMessage fm=new FacesMessage("No Data Found");
            fm.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc=FacesContext.getCurrentInstance();
            fc.addMessage(null, fm);
            }
        }
    }

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }
    public void isEnableEdit(ActionEvent actionEvent) {
    // Add event code here...
    this.editAction=getEditAction();
    }

    public void pfEcrCsvFileDownload(FacesContext facesContext, OutputStream os) {
                oracle.binding.OperationBinding Opp=ADFUtils.findOperation("writePfEcrToFileCsv");
                Object Obj=Opp.execute();
                System.out.println("Result is writePfEcrToFileCsv===>"+Opp.getResult());
                if(Opp.getResult()!=null)
                {
                String a_file = (String) Opp.getResult();
                
                System.out.println("path in bean :"+a_file);
                File file = new File(a_file);
                
                InputStream is = null;
                
         
                try {
                    is = new FileInputStream(file);
                    
                    
                    int read = 0;
                    byte[] bytes = new byte[1024];
                    
                    while ((read = is.read(bytes)) != -1) {
                                            os.write(bytes, 0, read);
                    }
                    
                    
                } catch (IOException e) {
                    e.printStackTrace();
                }
                finally {
                    if (is != null) {
                                try {
                                      is.close();
                                 } catch (IOException e) {
                                    e.printStackTrace();
                               }
                     }
                     if (os != null) {
                            try {
                                  os.flush();
                             } catch (IOException e) {
                                    e.printStackTrace();
                             }
                           }
                        }
                }
         
         
            }

    public void pfEcrArrearFileDownload(FacesContext facesContext, OutputStream os) {
        oracle.binding.OperationBinding Opp=ADFUtils.findOperation("writePfEcrArrearToFileCsv");
        Object Obj=Opp.execute();
        System.out.println("Result is writePfEcrArrearToFileCsv===>"+Opp.getResult());
        if(Opp.getResult()!=null)
        {
        String a_file = (String) Opp.getResult();
        
        System.out.println("path in bean :"+a_file);
                
                File file = new File(a_file);
                
                InputStream is = null;
                
         
                try {
                    is = new FileInputStream(file);
                    
                    
                    int read = 0;
                    byte[] bytes = new byte[1024];
                    
                    while ((read = is.read(bytes)) != -1) {
                                            os.write(bytes, 0, read);
                    }
                    
                    
                } catch (IOException e) {
                    e.printStackTrace();
                }
                finally {
                    if (is != null) {
                                try {
                                      is.close();
                                 } catch (IOException e) {
                                    e.printStackTrace();
                               }
                     }
                     if (os != null) {
                            try {
                                  os.flush();
                             } catch (IOException e) {
                                    e.printStackTrace();
                             }
                           }
                        }
        }

    }

    public void cancelAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("Rollback").execute();
        DCIteratorBinding dci = ADFUtils.findIterator("PfEcrDataDetailVO1Iterator");
        RowSetIterator rsi=dci.getViewObject().createRowSetIterator(null);
            while(rsi.hasNext()) {
                Row r = rsi.next();
               r.remove();
            }
            rsi.closeRowSetIterator();
    }

    public void setMonBinding(RichSelectOneChoice monBinding) {
        this.monBinding = monBinding;
    }

    public RichSelectOneChoice getMonBinding() {
        return monBinding;
    }

    public void setYearBinding(RichInputText yearBinding) {
        this.yearBinding = yearBinding;
    }

    public RichInputText getYearBinding() {
        return yearBinding;
    }

    public void setEcrBinding(RichSelectOneChoice ecrBinding) {
        this.ecrBinding = ecrBinding;
    }

    public RichSelectOneChoice getEcrBinding() {
        return ecrBinding;
    }
}
