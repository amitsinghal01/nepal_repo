package terms.hrm.transaction.ui.bean;

import java.util.HashMap;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import oracle.security.xml.ws.secconv.wssx.bindings.Identifier;

public class RawDataCorrectionBean {
    private RichTable rawDataCorrectionTableBinding;
    private String editAction="V";
    private RichInputComboboxListOfValues cardNoBinding;
    private RichInputText employeeNameBinding;
    private RichInputDate officePunchBinding;
    private RichInputText idNoBinding;

    public RawDataCorrectionBean() {
    }

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public void setRawDataCorrectionTableBinding(RichTable rawDataCorrectionTableBinding) {
        this.rawDataCorrectionTableBinding = rawDataCorrectionTableBinding;
    }

    public RichTable getRawDataCorrectionTableBinding() {
        return rawDataCorrectionTableBinding;
    }

    public void popUpDialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                                    {
        ADFUtils.findOperation("Delete").execute();
        OperationBinding op= ADFUtils.findOperation("Commit");
        Object rst = op.execute();
        System.out.println("Record Delete Successfully");

        if(op.getErrors().isEmpty())
         {
        FacesMessage Message = new 
        FacesMessage("Record Deleted Successfully");
        Message.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext fc =  FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        }
        else
        {
        FacesMessage Message = new 
        FacesMessage("Record cannot be deleted. Child record found.");
        Message.setSeverity(FacesMessage.SEVERITY_ERROR);
        FacesContext fc =   FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        }
            }
        AdfFacesContext.getCurrentInstance().addPartialTarget(rawDataCorrectionTableBinding);
        }

    public void setCardNoBinding(RichInputComboboxListOfValues cardNoBinding) {
        this.cardNoBinding = cardNoBinding;
    }

    public RichInputComboboxListOfValues getCardNoBinding() {
        return cardNoBinding;
    }

    public void setEmployeeNameBinding(RichInputText employeeNameBinding) {
        this.employeeNameBinding = employeeNameBinding;
    }

    public RichInputText getEmployeeNameBinding() {
        return employeeNameBinding;
    }

    public void setOfficePunchBinding(RichInputDate officePunchBinding) {
        this.officePunchBinding = officePunchBinding;
    }

    public RichInputDate getOfficePunchBinding() {
        return officePunchBinding;
    }

    public void setIdNoBinding(RichInputText idNoBinding) {
        this.idNoBinding = idNoBinding;
    }

    public RichInputText getIdNoBinding() {
        return idNoBinding;
    }

    public void saveButtonAL(ActionEvent actionEvent) {
        System.out.println("Inside Bean in save button AL");
        System.out.println("Before Method Call");
        OperationBinding ob=ADFUtils.findOperation("getValuesForRawDataCorrection");
        ob.execute();
        ADFUtils.findOperation("Commit").execute();
        System.out.println("Back to bean, after method returns!");
    }


}
