package terms.hrm.transaction.ui.bean;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;

import java.sql.Connection;

import java.sql.SQLException;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.faces.context.FacesContext;

import javax.naming.Context;
import javax.naming.InitialContext;

import javax.servlet.http.HttpServletResponse;

import javax.sql.DataSource;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

import oracle.adf.model.BindingContext;

import oracle.adf.model.binding.DCIteratorBinding;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;

import oracle.adf.view.rich.component.rich.input.RichInputText;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

public class RegularityChartReportBean {
    private String pMode="";
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText unitNameBinding;
    private RichInputComboboxListOfValues empNumberBinding;
    private RichInputText empNameBinding;

    private RichInputText yearBinding;
    private RichInputComboboxListOfValues monthBinding;

    public void setPMode(String pMode) {
        this.pMode = pMode;
    }

    public String getPMode() {
        return pMode;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    public String getFile_name() {
        return file_name;
    }
    String file_name="";
    public RegularityChartReportBean() {
    }
    public BindingContainer getBindings()
            {
              return BindingContext.getCurrent().getCurrentBindingsEntry();
            }

    public void downloadJrxml(FacesContext facesContext, OutputStream outputStream) {
        Connection conn=null;
        try {
            
            System.out.println(" pmode:"+pMode);
            
            
            Map n = new HashMap();
            System.out.println("Before Procedure Call");
            String UnitCode=(String)getUnitCodeBinding().getValue();
            String Month=(String)getMonthBinding().getValue();
            String Year=(String)getYearBinding().getValue();
            String EmpNo=(String)getEmpNumberBinding().getValue();
            System.out.println("Values from UI: "+UnitCode+" "+Month+" "+Year+" "+EmpNo);
            OperationBinding op=ADFUtils.findOperation("CallProcedureForRegularityChartReport");
            op.getParamsMap().put("Month", Month);
            op.getParamsMap().put("Year", Year);
            op.getParamsMap().put("UnitCode", UnitCode);
            op.getParamsMap().put("DeptCd", "%");
            op.getParamsMap().put("LevelCd", "%");
            op.getParamsMap().put("CatCd", "%");
            if(EmpNo==null)
            op.getParamsMap().put("EmpNo", '%');
            else
                op.getParamsMap().put("EmpNo", EmpNo);
            op.execute();
            System.out.println("Back to bean after procedure");
//            if(pMode != null){
//              if(pMode.equalsIgnoreCase("A")){
                 file_name ="r_regul_chart.jasper";
                DCIteratorBinding paySlip = (DCIteratorBinding) getBindings().get("RegularityChartReportVVO1Iterator");
                n.put("p_unit", paySlip.getCurrentRow().getAttribute("UnitCode").toString());
            n.put("p_emp_no", paySlip.getCurrentRow().getAttribute("EmpNo")==null?'%':paySlip.getCurrentRow().getAttribute("EmpNo").toString());
            n.put("p_mon", paySlip.getCurrentRow().getAttribute("Month").toString());
            n.put("p_year", paySlip.getCurrentRow().getAttribute("Year").toString());
//                  System.out.println("UnitCode Valu"+(String)paySlip.getCurrentRow().getAttribute("UnitCode"));
//                System.out.println("Emp Valu"+(String)paySlip.getCurrentRow().getAttribute("EmpNo"));
//                String EmpVal=(String) paySlip.getCurrentRow().getAttribute("EmpNo");
//                if(paySlip.getCurrentRow().getAttribute("EmpNo").equals(EmpVal))
//                {
//                        n.put("p_emp_no", '%');
//                    }
//                else{
//                n.put("p_emp_no", paySlip.getCurrentRow().getAttribute("EmpNo").toString());
//                }
             
//                n.put("p_from_dt", paySlip.getCurrentRow().getAttribute("FromDt"));
//                n.put("p_to_dt", paySlip.getCurrentRow().getAttribute("ToDt"));
   //           }
//            else if(pMode.equalsIgnoreCase("B")){
//                 file_name ="r_salary_slip_staff.jasper";
//                DCIteratorBinding paySlip = (DCIteratorBinding) getBindings().get("SalarySlipStaffReportVVO1Iterator");
//                n.put("p_unit", paySlip.getCurrentRow().getAttribute("UnitCode").toString());
//                n.put("p_month", paySlip.getCurrentRow().getAttribute("Month").toString());
//                n.put("p_year", paySlip.getCurrentRow().getAttribute("Year").toString());
//            }
 //           }
            
            OperationBinding binding = ADFUtils.findOperation("fileNameForPrint");
            binding.getParamsMap().put("fileId","ERP0000000120");
            binding.execute();
            System.out.println("Binding Result :"+binding.getResult());
            if(binding.getResult() != null){
                String result = binding.getResult().toString();
                                  int last_index = result.lastIndexOf("/");
                if(last_index==-1)
                {
                    last_index=result.lastIndexOf("\\");
                }
                                                      String path = result.substring(0,last_index+1)+file_name;
                                                      System.out.println("FILE PATH IS===>"+path);
                                  InputStream input = new FileInputStream(path);
            //    InputStream input = new FileInputStream("//home//beta3//Jasper//Jour_prn1.jrxml");
            conn = getConnection( );
            
            JasperReport design = (JasperReport) JRLoader.loadObject(input);
            System.out.println("Path : " + input + " -------" + design+" param:"+n);
            
            @SuppressWarnings("unchecked")
            net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
            byte pdf[] = JasperExportManager.exportReportToPdf(print);
            HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
            response.getOutputStream().write(pdf);
            response.getOutputStream().flush();
            response.getOutputStream().close();
            facesContext.responseComplete();
            }else
            System.out.println("File Name/File Path not found.");
            } catch (FileNotFoundException fnfe) {
            // TODO: Add catch code
            fnfe.printStackTrace();
            try {
                System.out.println("in finally connection closed");
                        conn.close( );
                        conn = null;
                
            } catch( SQLException e ) {
                e.printStackTrace( );
            }
            } catch (Exception e) {
            // TODO: Add catch code
            e.printStackTrace();
            try {
                System.out.println("in finally connection closed");
                        conn.close( );
                        conn = null;
                
            } catch( SQLException e1 ) {
                e1.printStackTrace( );
            }
            }finally {
                    try {
                            System.out.println("in finally connection closed");
                                    conn.close( );
                                    conn = null;
                            
                    } catch( SQLException e ) {
                            e.printStackTrace( );
                    }
            }

    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setUnitNameBinding(RichInputText unitNameBinding) {
        this.unitNameBinding = unitNameBinding;
    }

    public RichInputText getUnitNameBinding() {
        return unitNameBinding;
    }

    public void setEmpNumberBinding(RichInputComboboxListOfValues empNumberBinding) {
        this.empNumberBinding = empNumberBinding;
    }

    public RichInputComboboxListOfValues getEmpNumberBinding() {
        return empNumberBinding;
    }

    public void setEmpNameBinding(RichInputText empNameBinding) {
        this.empNameBinding = empNameBinding;
    }

    public RichInputText getEmpNameBinding() {
        return empNameBinding;
    }



    public void setYearBinding(RichInputText yearBinding) {
        this.yearBinding = yearBinding;
    }

    public RichInputText getYearBinding() {
        return yearBinding;
    }

    public void setMonthBinding(RichInputComboboxListOfValues monthBinding) {
        this.monthBinding = monthBinding;
    }

    public RichInputComboboxListOfValues getMonthBinding() {
        return monthBinding;
    }
    
    public Connection getConnection() throws Exception{
            Context ctx = null;
            Connection conn = null;
            ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/ApplicationDBDS");
            conn = ds.getConnection();
            return conn;
        }
}
