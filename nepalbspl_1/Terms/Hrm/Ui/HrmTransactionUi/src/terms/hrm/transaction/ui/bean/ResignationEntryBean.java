package terms.hrm.transaction.ui.bean;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;

import java.sql.Connection;

import java.sql.SQLException;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.faces.application.FacesMessage;

import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import javax.naming.Context;
import javax.naming.InitialContext;

import javax.servlet.http.HttpServletResponse;

import javax.sql.DataSource;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

public class ResignationEntryBean {
    private RichTable resignationEntryTableBinding;
    private String editAction="V";
    private RichInputDate resignationDateBinding;
    private RichInputDate relievingDateBinding;

    public ResignationEntryBean() {
    }

    public void setResignationEntryTableBinding(RichTable resignationEntryTableBinding) {
        this.resignationEntryTableBinding = resignationEntryTableBinding;
    }

    public RichTable getResignationEntryTableBinding() {
        return resignationEntryTableBinding;
    }

    public void popUpDialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
        {
        OperationBinding op = null;
        ADFUtils.findOperation("Delete").execute();
        op = (OperationBinding) ADFUtils.findOperation("Commit");
        if (getEditAction().equals("V")) {
        Object rst = op.execute();
        }
        System.out.println("Record Delete Successfully");
        if(op.getErrors().isEmpty()){
        FacesMessage Message = new FacesMessage("Record Deleted Successfully");
        Message.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        } else if (!op.getErrors().isEmpty())
        {
        OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
        Object rstr = opr.execute();
        FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
        Message.setSeverity(FacesMessage.SEVERITY_ERROR);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(resignationEntryTableBinding);
    }

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public void createButtonAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert").execute();
        ADFUtils.findOperation("generateEntryNumber").execute();
    }

    public void resignationDateVCl(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        vce.getNewValue();
        oracle.jbo.domain.Date rd=(oracle.jbo.domain.Date)resignationDateBinding.getValue();
        System.out.println("vaalue"+rd);
        relievingDateBinding.setValue("");  
    }

    public void setResignationDateBinding(RichInputDate resignationDateBinding) {
        this.resignationDateBinding = resignationDateBinding;
    }

    public RichInputDate getResignationDateBinding() {
        return resignationDateBinding;
    }

    public void setRelievingDateBinding(RichInputDate relievingDateBinding) {
        this.relievingDateBinding = relievingDateBinding;
    }

    public RichInputDate getRelievingDateBinding() {
        return relievingDateBinding;
    }
    
    
    public BindingContainer getBindings()
      {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
      }
    
    public void downloadJrxml(FacesContext facesContext, OutputStream outputStream) {
        Connection conn=null;
            try {

                OperationBinding binding = ADFUtils.findOperation("fileNameForPrint");
                binding.getParamsMap().put("fileId","HRM0000000178");
                binding.execute();
                System.out.println("Binding Result :"+binding.getResult());
                if(binding.getResult() != null){
                    InputStream input = new FileInputStream(binding.getResult().toString());
                    
                    DCIteratorBinding pvIter = (DCIteratorBinding) getBindings().get("ResignationEntryVO1Iterator");
                    String unitCode = pvIter.getCurrentRow().getAttribute("UnitCd").toString();
                    String entryNo = pvIter.getCurrentRow().getAttribute("EntryNo").toString();
    //        //            oracle.jbo.domain.Date vouDate =(oracle.jbo.domain.Date)pvIter.getCurrentRow().getAttribute("VouDate");
    //
    //                    System.out.println("Machine Code: "+machCode+" ||UNIT CODE: "+unitCode);
                    Map n = new HashMap();
                    n.put("p_unit", unitCode);
                    n.put("p_resign_entry_no", entryNo);
                    conn = getConnection( );
                    
                    JasperReport design = (JasperReport) JRLoader.loadObject(input);
                    System.out.println("Path : " + input + " -------" + design+" param:"+n);
                    
                    @SuppressWarnings("unchecked")
                    net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                    byte pdf[] = JasperExportManager.exportReportToPdf(print);
                    HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                    response.getOutputStream().write(pdf);
                    response.getOutputStream().flush();
                    response.getOutputStream().close();
                    facesContext.responseComplete();
                    }else
                    System.out.println("File Name/File Path not found.");
                    } catch (FileNotFoundException fnfe) {
                    // TODO: Add catch code
                    fnfe.printStackTrace();
                    try {
                        System.out.println("in finally connection closed");
                                conn.close( );
                                conn = null;
                        
                    } catch( SQLException e ) {
                        e.printStackTrace( );
                    }
                    } catch (Exception e) {
                    // TODO: Add catch code
                    e.printStackTrace();
                    try {
                        System.out.println("in finally connection closed");
                                conn.close( );
                                conn = null;
                        
                    } catch( SQLException e1 ) {
                        e1.printStackTrace( );
                    }
                    }finally {
                            try {
                                    System.out.println("in finally connection closed");
                                            conn.close( );
                                            conn = null;
                                    
                            } catch( SQLException e ) {
                                    e.printStackTrace( );
                            }
                    }
        }  
    
    public Connection getConnection() throws Exception{
            Context ctx = null;
            Connection conn = null;
            ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/ApplicationDBDS");
            conn = ds.getConnection();
            return conn;
        }
}
