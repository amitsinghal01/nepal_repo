package terms.hrm.transaction.ui.bean;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;

import oracle.binding.OperationBinding;

public class SalaryProcessBean {
    private RichInputDate monthYearBinding;

    public SalaryProcessBean() {
    }

    public void monthVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce.getNewValue()!=null){
//            StringBuilder str= new StringBuilder(vce.getNewValue().toString());
//            str.insert(2, '/');
//            monthYearBinding.setValue(str);
            System.out.println("Date in:"+vce.getNewValue());
        }
    }

    public void SalaryProcessAL(ActionEvent actionEvent) {
        OperationBinding ob = ADFUtils.findOperation("salaryProcess");
        ob.execute();
        System.out.println("ob.getResult() in bean:"+ob.getResult());
        if(ob.getResult()!= null){  
            String result=ob.getResult().toString().substring(0,1);
//            String msgg=ob.getResult().toString().substring(ob.getResult().toString().indexOf("-")+1);
//            String msg1=ob.getResult().toString();
//            System.out.println("result after substring:"+result);
//            System.out.println("msg:"+msgg);
            if(result.equalsIgnoreCase("E")){
                ADFUtils.showMessage("Salary not processed successfully. Error is "+ob.getResult().toString(), 0);
            }else {
                ADFUtils.showMessage("Salary processed successfully.", 2);
            }
        }
    }

    public void setMonthYearBinding(RichInputDate monthYearBinding) {
        this.monthYearBinding = monthYearBinding;
    }

    public RichInputDate getMonthYearBinding() {
        return monthYearBinding;
    }
}
