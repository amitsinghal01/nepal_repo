package terms.hrm.transaction.ui.bean;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;

import java.sql.Connection;

import java.sql.SQLException;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.naming.Context;
import javax.naming.InitialContext;

import javax.servlet.http.HttpServletResponse;

import javax.sql.DataSource;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCIteratorBinding;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;

import oracle.adf.view.rich.component.rich.input.RichInputText;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;

import org.apache.myfaces.trinidad.event.DisclosureEvent;

public class SalarySlipOfficersAndStaffReportBean {
    private String pMode="";
       String file_name="";
    private RichInputComboboxListOfValues unitCodeOffBinding;
    private RichInputComboboxListOfValues empNumOffBinding;
    private RichInputComboboxListOfValues monthOffBinding;
    private RichInputText yearOffBinding;
    private RichInputComboboxListOfValues unitCodeStfBinding;
    private RichInputComboboxListOfValues empNumStfBinding;
    private RichInputComboboxListOfValues mnthStfBinding;
    private RichInputText yearStfBinding;
    private RichInputText levelCdOffBinding;
    private RichInputText lvlCdStfBinding;


    public void setPMode(String pMode) {
        this.pMode = pMode;
    }

    public String getPMode() {
        return pMode;
    }

    public SalarySlipOfficersAndStaffReportBean() {
    }

    public BindingContainer getBindings()
            {
              return BindingContext.getCurrent().getCurrentBindingsEntry();
            }

        public void downloadJrxml(FacesContext facesContext, OutputStream outputStream) {
            Connection conn=null;
            try {
                
                System.out.println(" pmode:"+pMode);
                
                
                Map n = new HashMap();
                if(pMode != null){
                  if(pMode.equalsIgnoreCase("A")){
                      
                    System.out.println("Before Procedure Call For Officer");
                    String UnitCode=(String)getUnitCodeOffBinding().getValue();
                    String Month=(String)getMonthOffBinding().getValue();
                    String Year=(String)getYearOffBinding().getValue();
                    String EmpNo=(String)getEmpNumOffBinding().getValue();
                    System.out.println("Level Code Val--->"+(String)getLevelCdOffBinding().getValue());
                    String LevelCd=(String)getLevelCdOffBinding().getValue();
                    System.out.println("Level Value-->"+LevelCd);
                    System.out.println("Values from UI: "+UnitCode+" "+Month+" "+Year+" "+EmpNo+" "+LevelCd);
                    OperationBinding op=ADFUtils.findOperation("CallProcedureForOfficerSalaryReport");
                    op.getParamsMap().put("Month", Month);
                    op.getParamsMap().put("Year", Year);
                    op.getParamsMap().put("UnitCode", UnitCode);
                    op.getParamsMap().put("DeptCd", "%");
                    op.getParamsMap().put("LevelCd", LevelCd);
                    op.getParamsMap().put("CatCd", "%");
                    if(EmpNo==null)
                    op.getParamsMap().put("EmpNo", '%');
                    else
                        op.getParamsMap().put("EmpNo", EmpNo);
                    op.execute();
                    System.out.println("Back to bean after officer procedure");
                    
                     file_name ="r_salary_slip_officer.jasper";
                    DCIteratorBinding paySlip = (DCIteratorBinding) getBindings().get("SalarySlipOfficersReportVVO1Iterator");
                    n.put("p_unit", paySlip.getCurrentRow().getAttribute("UnitCode").toString());
                    n.put("p_month", paySlip.getCurrentRow().getAttribute("Month").toString());
                    n.put("p_year", paySlip.getCurrentRow().getAttribute("Year").toString());
                    
                }else if(pMode.equalsIgnoreCase("B")){
                      
                    System.out.println("Before Procedure Call For Staff");
                    String UnitCode=(String)getUnitCodeStfBinding().getValue();
                    String Month=(String)getMnthStfBinding().getValue();
                    String Year=(String)getYearStfBinding().getValue();
                    String EmpNo=(String)getEmpNumStfBinding().getValue();
                    String LevelCd=(String)getLvlCdStfBinding().getValue();
                    System.out.println("Values from UI: "+UnitCode+" "+Month+" "+Year+" "+EmpNo+" "+LevelCd);
                    OperationBinding op=ADFUtils.findOperation("CallProcedureForStaffSalaryReport");
                    op.getParamsMap().put("Month", Month);
                    op.getParamsMap().put("Year", Year);
                    op.getParamsMap().put("UnitCode", UnitCode);
                    op.getParamsMap().put("DeptCd", "%");
                    op.getParamsMap().put("LevelCd", LevelCd);
                    op.getParamsMap().put("CatCd", "%");
                    if(EmpNo==null)
                    op.getParamsMap().put("EmpNo", '%');
                    else
                        op.getParamsMap().put("EmpNo", EmpNo);
                    op.execute();
                    System.out.println("Back to bean after staff procedure");
                      
                     file_name ="r_salary_slip_staff.jasper";
                    DCIteratorBinding paySlip = (DCIteratorBinding) getBindings().get("SalarySlipStaffReportVVO1Iterator");
                    n.put("p_unit", paySlip.getCurrentRow().getAttribute("UnitCode").toString());
                    n.put("p_month", paySlip.getCurrentRow().getAttribute("Month").toString());
                    n.put("p_year", paySlip.getCurrentRow().getAttribute("Year").toString());
                }
                }
                
                OperationBinding binding = ADFUtils.findOperation("fileNameForPrint");
                binding.getParamsMap().put("fileId","ERP0000000120");
                binding.execute();
                System.out.println("Binding Result :"+binding.getResult());
                if(binding.getResult() != null){
                    String result = binding.getResult().toString();
                                      int last_index = result.lastIndexOf("/");
                    if(last_index==-1)
                    {
                        last_index=result.lastIndexOf("\\");
                    }
                                                          String path = result.substring(0,last_index+1)+file_name;
                                                          System.out.println("FILE PATH IS===>"+path);
                                      InputStream input = new FileInputStream(path);
                //    InputStream input = new FileInputStream("//home//beta3//Jasper//Jour_prn1.jrxml");

                    conn = getConnection( );
                    
                    JasperReport design = (JasperReport) JRLoader.loadObject(input);
                    System.out.println("Path : " + input + " -------" + design+" param:"+n);
                    
                    @SuppressWarnings("unchecked")
                    net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                    byte pdf[] = JasperExportManager.exportReportToPdf(print);
                    HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                    response.getOutputStream().write(pdf);
                    response.getOutputStream().flush();
                    response.getOutputStream().close();
                    facesContext.responseComplete();
                    }else
                    System.out.println("File Name/File Path not found.");
                    } catch (FileNotFoundException fnfe) {
                    // TODO: Add catch code
                    fnfe.printStackTrace();
                    try {
                        System.out.println("in finally connection closed");
                                conn.close( );
                                conn = null;
                        
                    } catch( SQLException e ) {
                        e.printStackTrace( );
                    }
                    } catch (Exception e) {
                    // TODO: Add catch code
                    e.printStackTrace();
                    try {
                        System.out.println("in finally connection closed");
                                conn.close( );
                                conn = null;
                        
                    } catch( SQLException e1 ) {
                        e1.printStackTrace( );
                    }
                    }finally {
                            try {
                                    System.out.println("in finally connection closed");
                                            conn.close( );
                                            conn = null;
                                    
                            } catch( SQLException e ) {
                                    e.printStackTrace( );
                            }
                    }
        }

    public void callCreateInsert(DisclosureEvent disclosureEvent) {
ADFUtils.findOperation("CreateInsert").execute();
    }

    public void setUnitCodeOffBinding(RichInputComboboxListOfValues unitCodeOffBinding) {
        this.unitCodeOffBinding = unitCodeOffBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeOffBinding() {
        return unitCodeOffBinding;
    }

    public void setEmpNumOffBinding(RichInputComboboxListOfValues empNumOffBinding) {
        this.empNumOffBinding = empNumOffBinding;
    }

    public RichInputComboboxListOfValues getEmpNumOffBinding() {
        return empNumOffBinding;
    }

    public void setMonthOffBinding(RichInputComboboxListOfValues monthOffBinding) {
        this.monthOffBinding = monthOffBinding;
    }

    public RichInputComboboxListOfValues getMonthOffBinding() {
        return monthOffBinding;
    }

    public void setYearOffBinding(RichInputText yearOffBinding) {
        this.yearOffBinding = yearOffBinding;
    }

    public RichInputText getYearOffBinding() {
        return yearOffBinding;
    }


    public void setUnitCodeStfBinding(RichInputComboboxListOfValues unitCodeStfBinding) {
        this.unitCodeStfBinding = unitCodeStfBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeStfBinding() {
        return unitCodeStfBinding;
    }

    public void setEmpNumStfBinding(RichInputComboboxListOfValues empNumStfBinding) {
        this.empNumStfBinding = empNumStfBinding;
    }

    public RichInputComboboxListOfValues getEmpNumStfBinding() {
        return empNumStfBinding;
    }

    public void setMnthStfBinding(RichInputComboboxListOfValues mnthStfBinding) {
        this.mnthStfBinding = mnthStfBinding;
    }

    public RichInputComboboxListOfValues getMnthStfBinding() {
        return mnthStfBinding;
    }

    public void setYearStfBinding(RichInputText yearStfBinding) {
        this.yearStfBinding = yearStfBinding;
    }

    public RichInputText getYearStfBinding() {
        return yearStfBinding;
    }


    public void setLevelCdOffBinding(RichInputText levelCdOffBinding) {
        this.levelCdOffBinding = levelCdOffBinding;
    }

    public RichInputText getLevelCdOffBinding() {
        return levelCdOffBinding;
    }

    public void setLvlCdStfBinding(RichInputText lvlCdStfBinding) {
        this.lvlCdStfBinding = lvlCdStfBinding;
    }

    public RichInputText getLvlCdStfBinding() {
        return lvlCdStfBinding;
    }
    
    public Connection getConnection() throws Exception{
            Context ctx = null;
            Connection conn = null;
            ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/ApplicationDBDS");
            conn = ds.getConnection();
            return conn;
        }
}
