package terms.hrm.transaction.ui.bean;

import com.tangosol.dev.assembler.Pop;

import javax.el.ELContext;
import javax.el.ExpressionFactory;

import javax.el.ValueExpression;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.OperationBinding;
import oracle.adf.model.binding.DCDataControl;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;
import oracle.jbo.server.ViewObjectImpl;

import terms.hrm.transaction.model.applicationModule.HrmTransactionAMImpl;
import terms.hrm.transaction.model.view.SalaryVoucherPostingDetailVORowImpl;
import terms.hrm.transaction.model.view.SalaryVoucherPostinhHeaderVORowImpl;

public class SalaryVoucherPostingBean {
    private RichInputComboboxListOfValues unitCdBinding;
    private RichInputComboboxListOfValues approveByCodeBinding;

    public SalaryVoucherPostingBean() {
    }

    public void SaveAL(ActionEvent actionEvent) {
//          BindingContext bco=BindingContext.getCurrent();
//          DCDataControl dc=bco.findDataControl("HrmTransactionAMDataControl");
//                 HrmTransactionAMImpl am = (HrmTransactionAMImpl) dc.getDataProvider();
//       ViewObjectImpl vo = (ViewObjectImpl) am.getSalaryVoucherPostinhHeaderVO1().getCurrentRow();
        
        String approveBy = (String) getApproveByCodeBinding().getValue();
        System.out.println("Approvr By Code is---->>"+approveBy);
        
        
      DCIteratorBinding Dciter=ADFUtils.findIterator("SalaryVoucherPostingDetailVO1Iterator");
        
//          SalaryVoucherPostinhHeaderVORowImpl voupost =(SalaryVoucherPostinhHeaderVORowImpl)vo.getCurrentRow();
             
        SalaryVoucherPostingDetailVORowImpl row=(SalaryVoucherPostingDetailVORowImpl)Dciter.getCurrentRow();
    System.out.println("TotalDeb"+row.getTotalDebit()+"Total Credit"+row.getTotalCredit());
    
    
  if(row.getTotalDebit()!=null && row.getTotalCredit()!=null && row.getTotalDebit().compareTo(row.getTotalCredit())==0 && approveBy!=null)
    
  {
         
          OperationBinding Opr = (OperationBinding) ADFUtils.findOperation("generateSalaryVouPosting");
                   Object Obj=Opr.execute();
                   System.out.println("******Result is:"+Opr.getResult());
          if(Opr.getResult()!=null && Opr.getResult().toString().startsWith("Y"))
                       {
                       ADFUtils.findOperation("Commit").execute();
                       System.out.println("voucherif"+Opr.getResult().toString().substring(2));
                       ADFUtils.showMessage("Record Saved Successfully.New Number is "+Opr.getResult().toString().substring(2), 2);
                       }
          else
          {
                  ADFUtils.showMessage("Record can not saved,error is "+Opr.getResult(), 2);
                   
              }
//          ADFUtils.findOperation("Commit").execute();
//                         FacesMessage Message = new FacesMessage("Record Saved Successfully.");
//                         Message.setSeverity(FacesMessage.SEVERITY_INFO);
//                         FacesContext fc = FacesContext.getCurrentInstance();
//                         fc.addMessage(null, Message);
      }
  
  
  
          if(row.getTotalDebit()!=null && row.getTotalCredit()!=null && row.getTotalDebit().compareTo(row.getTotalCredit())==0 && approveBy==null)
            
          {
//                 
//                  OperationBinding Opr = (OperationBinding) ADFUtils.findOperation("generateSalaryVouPosting");
//                           Object Obj=Opr.execute();
//                           System.out.println("******Result is:"+Opr.getResult());
//                  if(Opr.getResult()!=null && Opr.getResult().toString().startsWith("Y"))
//                               {
                               ADFUtils.findOperation("Commit").execute();
                               System.out.println("voucherif 2");
                               ADFUtils.showMessage("Record Saved Successfully.",2);
//                               }
//                  else
//                  {
//                          ADFUtils.showMessage("Record can not saved,error is "+Opr.getResult(), 2);
//                           
//                      }
              }
  
  
  
  else
  {
      ADFUtils.showMessage("Total Debit Amount is not equal to Total Credit Amount. ", 0);
      
      }}

    public void PopulateAL(ActionEvent actionEvent) {
        OperationBinding Pop = (OperationBinding) ADFUtils.findOperation("PopulateSalaryVoucher");
        Pop.execute();

        if (Pop.getResult() != null && Pop.getResult().toString().equalsIgnoreCase("S")) {
            FacesMessage Message = new FacesMessage("Record Populated successfully.");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
            
        }

        else if(Pop.getResult() != null && Pop.getResult().toString().equalsIgnoreCase("V"))
        {
            FacesMessage fm=new FacesMessage("No Data Found");
            fm.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc=FacesContext.getCurrentInstance();
            fc.addMessage(null, fm);
            }
    }

    public void cancelAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("Rollback").execute();
        DCIteratorBinding Dciter=ADFUtils.findIterator("SalaryVoucherPostingDetailVO1Iterator");
//         SalaryVoucherPostingDetailVORowImpl row=(SalaryVoucherPostingDetailVORowImpl)Dciter.getCurrentRow();
      ViewObjectImpl vod = (ViewObjectImpl) Dciter.getViewObject();
        vod.setNamedWhereClauseParam("bindMode", "C");
        vod.executeQuery();
    }

    public void approveByVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        System.out.println("In VCL");
        System.out.println("UnitCD-->"+getUnitCdBinding().getValue().toString());
        System.out.println("empCode-->"+vce.getNewValue());
                     if (vce.getNewValue() != null) {
                         System.out.println("In VCL if");
                         OperationBinding op = (OperationBinding) ADFUtils.findOperation("checkApprovalAuthority");
                         op.getParamsMap().put("unitCd", getUnitCdBinding().getValue().toString());
                         op.getParamsMap().put("empCode", vce.getNewValue());
                         op.getParamsMap().put("authLevel", "AP");
                         op.getParamsMap().put("formName", "SVP");
                         op.execute();


            java.sql.Timestamp dateTime = new java.sql.Timestamp(System.currentTimeMillis());
            System.out.println("Syatem Date--->" + dateTime);

            oracle.jbo.domain.Timestamp datm = new oracle.jbo.domain.Timestamp(dateTime);
            System.out.println("Sastem Date Timestamp-->" + datm);

            System.out.println("In if");
            DCIteratorBinding Dciter = ADFUtils.findIterator("SalaryVoucherPostingDetailVO1Iterator");
            ViewObjectImpl vod = (ViewObjectImpl) Dciter.getViewObject();

            RowSetIterator rsi = vod.createRowSetIterator(null);
            while (rsi.hasNext()) {
                System.out.println("In while");
                Row r1 = rsi.next();
                if (r1.getAttribute("ApprovedDate") == null) {
                    System.out.println("In while if");
                    vod.getCurrentRow().setAttribute("ApprovedDate", datm);
                }
            }
            rsi.closeRowSetIterator();


            if (op.getResult() != null && op.getResult().equals("N")) {
                             Row r = (Row) ADFUtils.evaluateEL("#{bindings.SalaryVoucherPostingHeaderVVO1Iterator.currentRow}");
                             r.setAttribute("ApproveBy", null);
                             r.setAttribute("ApproveDate", null);
                             FacesMessage Message =
                                 new FacesMessage("Sorry You Are Not Authorized To Approve this.");
                             Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                             FacesContext fc = FacesContext.getCurrentInstance();
                             fc.addMessage(null, Message);
                         } 
                         if (op.getResult() != null && op.getResult().equals("Y")) {
//                             Row r = (Row) ADFUtils.evaluateEL("#{bindings.SalaryVoucherPostingHeaderVVO1Iterator.currentRow}");
//                             r.setAttribute("ApproveBy", null);
                                          
                             FacesMessage Message = new FacesMessage("Approval is successful.");
                             Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                             FacesContext fc = FacesContext.getCurrentInstance();
                             fc.addMessage(null, Message);
                         } 

                     }
    }

    public void setUnitCdBinding(RichInputComboboxListOfValues unitCdBinding) {
        this.unitCdBinding = unitCdBinding;
    }

    public RichInputComboboxListOfValues getUnitCdBinding() {
        return unitCdBinding;
    }

    public void setApproveByCodeBinding(RichInputComboboxListOfValues approveByCodeBinding) {
        this.approveByCodeBinding = approveByCodeBinding;
    }

    public RichInputComboboxListOfValues getApproveByCodeBinding() {
        return approveByCodeBinding;
    }
}
