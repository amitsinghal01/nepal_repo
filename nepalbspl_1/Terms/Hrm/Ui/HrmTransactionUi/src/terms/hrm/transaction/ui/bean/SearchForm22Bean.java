package terms.hrm.transaction.ui.bean;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

public class SearchForm22Bean {
    private RichTable form22tableBinding;

    public SearchForm22Bean() {
    }

    public void deletepopupDL(DialogEvent dialogEvent) {
        {
            if(dialogEvent.getOutcome().name().equals("ok"))
                        {
                        ADFUtils.findOperation("Delete").execute();
                        FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                        FacesContext fc = FacesContext.getCurrentInstance();  
                        fc.addMessage(null, Message);  
                       
                        }
                    AdfFacesContext.getCurrentInstance().addPartialTarget(form22tableBinding);

                    }
        }

    public void setForm22tableBinding(RichTable form22tableBinding) {
        this.form22tableBinding = form22tableBinding;
    }

    public RichTable getForm22tableBinding() {
        return form22tableBinding;
    }
}
