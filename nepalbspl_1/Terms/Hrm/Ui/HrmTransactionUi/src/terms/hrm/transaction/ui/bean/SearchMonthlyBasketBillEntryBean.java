package terms.hrm.transaction.ui.bean;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;

import java.sql.Connection;

import java.sql.SQLException;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.faces.application.FacesMessage;

import javax.faces.context.FacesContext;

import javax.naming.Context;
import javax.naming.InitialContext;

import javax.servlet.http.HttpServletResponse;

import javax.sql.DataSource;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

public class SearchMonthlyBasketBillEntryBean {
    private RichTable monthlyBasketBillEntryTableBinding;
    private String editAction="V";
    private String pMode="";
       String file_name="";

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public SearchMonthlyBasketBillEntryBean() {
    }

    public void setMonthlyBasketBillEntryTableBinding(RichTable monthlyBasketBillEntryTableBinding) {
        this.monthlyBasketBillEntryTableBinding = monthlyBasketBillEntryTableBinding;
    }

    public RichTable getMonthlyBasketBillEntryTableBinding() {
        return monthlyBasketBillEntryTableBinding;
    }
    
    public BindingContainer getBindings()
            {
              return BindingContext.getCurrent().getCurrentBindingsEntry();
            }

    public void popUpDialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
        {
        OperationBinding op = null;
        ADFUtils.findOperation("Delete").execute();
        op = (OperationBinding) ADFUtils.findOperation("Commit");
        if (getEditAction().equals("V")) {
        Object rst = op.execute();
        }
        System.out.println("Record Delete Successfully");
        if(op.getErrors().isEmpty()){
        FacesMessage Message = new FacesMessage("Record Deleted Successfully");
        Message.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        } else if (!op.getErrors().isEmpty())
        {
        OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
        Object rstr = opr.execute();
        FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
        Message.setSeverity(FacesMessage.SEVERITY_ERROR);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(monthlyBasketBillEntryTableBinding);
    }

    public void downloadjrxml(FacesContext facesContext, OutputStream outputStream) {
        Connection conn=null;
        try {
            
            System.out.println(" pmode:"+pMode);
            
            
            Map n = new HashMap();
            if(pMode != null){
              if(pMode.equalsIgnoreCase("A")){
                 file_name ="r_month_basket.jasper";
                DCIteratorBinding paySlip = (DCIteratorBinding) getBindings().get("SearchMonthlyBasketBillEntryVO1Iterator");
//                System.out.println("Unit Code-->"+paySlip.getCurrentRow().getAttribute("UnitCd").toString());
//                System.out.println("EmpCd Code-->"+paySlip.getCurrentRow().getAttribute("EmpCd").toString());
//                System.out.println("ShiftDate-->"+paySlip.getCurrentRow().getAttribute("ShiftDate"));
                n.put("p_unit", paySlip.getCurrentRow().getAttribute("UnitCd").toString());
                n.put("p_emp_no", paySlip.getCurrentRow().getAttribute("EmpCode").toString());
                n.put("p_month", paySlip.getCurrentRow().getAttribute("Month"));
                n.put("p_year", paySlip.getCurrentRow().getAttribute("Year"));
                
            }
        //              else if(pMode.equalsIgnoreCase("B")){
        //                 file_name ="r_salary_slip_staff.jasper";
        //                DCIteratorBinding paySlip = (DCIteratorBinding) getBindings().get("SalarySlipStaffReportVVO1Iterator");
        //                n.put("p_unit", paySlip.getCurrentRow().getAttribute("UnitCode").toString());
        //                n.put("p_month", paySlip.getCurrentRow().getAttribute("Month").toString());
        //                n.put("p_year", paySlip.getCurrentRow().getAttribute("Year").toString());
        //            }
            }
            
            OperationBinding binding = ADFUtils.findOperation("fileNameForPrint");
            binding.getParamsMap().put("fileId","ERP0000000120");
            binding.execute();
            System.out.println("Binding Result :"+binding.getResult());
            if(binding.getResult() != null){
                String result = binding.getResult().toString();
                                  int last_index = result.lastIndexOf("/");
                if(last_index==-1)
                {
                    last_index=result.lastIndexOf("\\");
                }
                                                      String path = result.substring(0,last_index+1)+file_name;
                                                      System.out.println("FILE PATH IS===>"+path);
                                  InputStream input = new FileInputStream(path);
            //    InputStream input = new FileInputStream("//home//beta3//Jasper//Jour_prn1.jrxml");

                conn = getConnection( );
                
                JasperReport design = (JasperReport) JRLoader.loadObject(input);
                System.out.println("Path : " + input + " -------" + design+" param:"+n);
                
                @SuppressWarnings("unchecked")
                net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                byte pdf[] = JasperExportManager.exportReportToPdf(print);
                HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                response.getOutputStream().write(pdf);
                response.getOutputStream().flush();
                response.getOutputStream().close();
                facesContext.responseComplete();
                }else
                System.out.println("File Name/File Path not found.");
                } catch (FileNotFoundException fnfe) {
                // TODO: Add catch code
                fnfe.printStackTrace();
                try {
                    System.out.println("in finally connection closed");
                            conn.close( );
                            conn = null;
                    
                } catch( SQLException e ) {
                    e.printStackTrace( );
                }
                } catch (Exception e) {
                // TODO: Add catch code
                e.printStackTrace();
                try {
                    System.out.println("in finally connection closed");
                            conn.close( );
                            conn = null;
                    
                } catch( SQLException e1 ) {
                    e1.printStackTrace( );
                }
                }finally {
                        try {
                                System.out.println("in finally connection closed");
                                        conn.close( );
                                        conn = null;
                                
                        } catch( SQLException e ) {
                                e.printStackTrace( );
                        }
                }

    }

    public void setPMode(String pMode) {
        this.pMode = pMode;
    }

    public String getPMode() {
        return pMode;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    public String getFile_name() {
        return file_name;
    }
    
    public Connection getConnection() throws Exception{
            Context ctx = null;
            Connection conn = null;
            ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/ApplicationDBDS");
            conn = ds.getConnection();
            return conn;
        }
}
