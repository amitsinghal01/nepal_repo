package terms.hrm.transaction.ui.bean;

import javax.faces.application.FacesMessage;

import javax.faces.context.FacesContext;

import oracle.adf.model.OperationBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

public class SearchMonthlyEsiBean {
    private RichTable searchTableBinding;

    public SearchMonthlyEsiBean() {
    }

    public void deletepopupDialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                    {
                        ADFUtils.findOperation("Delete").execute();
                        oracle.binding.OperationBinding op=  ADFUtils.findOperation("Commit");
                        Object rst = op.execute();
                        if(op.getErrors().isEmpty())
                        {
                        FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                        FacesContext fc = FacesContext.getCurrentInstance();  
                        fc.addMessage(null, Message);
                        }
                        else
                        {
                        FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                        Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                        FacesContext fc = FacesContext.getCurrentInstance();
                        fc.addMessage(null, Message);
                         }
                }
        AdfFacesContext.getCurrentInstance().addPartialTarget(searchTableBinding);
    }

    public void setSearchTableBinding(RichTable searchTableBinding) {
        this.searchTableBinding = searchTableBinding;
    }

    public RichTable getSearchTableBinding() {
        return searchTableBinding;
    }
}
