package terms.hrm.transaction.ui.bean;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;

import java.sql.Connection;

import java.sql.SQLException;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.faces.application.FacesMessage;

import javax.faces.context.FacesContext;

import javax.naming.Context;
import javax.naming.InitialContext;

import javax.servlet.http.HttpServletResponse;

import javax.sql.DataSource;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

public class SearchOutDoorDutyBean {
    private String pMode="";
       String file_name="";
    private RichTable searchOutDoorDutyTableBinding;

    public SearchOutDoorDutyBean() {

    }

    public void setSearchOutDoorDutyTableBinding(RichTable searchOutDoorDutyTableBinding) {
        this.searchOutDoorDutyTableBinding = searchOutDoorDutyTableBinding;
    }

    public RichTable getSearchOutDoorDutyTableBinding() {
        return searchOutDoorDutyTableBinding;
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
    if(dialogEvent.getOutcome().name().equals("ok"))
                               {
    ADFUtils.findOperation("Delete").execute();
                                   OperationBinding op= ADFUtils.findOperation("Commit");
                                   Object rst = op.execute();
                               System.out.println("Record Delete Successfully");

                                   if(op.getErrors().isEmpty())
                                   {
                                   FacesMessage Message = new FacesMessage("Record Deleted Successfully");
    Message.setSeverity(FacesMessage.SEVERITY_INFO);
                                   FacesContext fc = FacesContext.getCurrentInstance();
                                   fc.addMessage(null, Message);
                                   }
                                   else
                                   {
                                   FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
    Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                                   FacesContext fc = FacesContext.getCurrentInstance();
                                   fc.addMessage(null, Message);
                                    }
                           }
    AdfFacesContext.getCurrentInstance().addPartialTarget(searchOutDoorDutyTableBinding);
       }
    
    public BindingContainer getBindings()
            {
              return BindingContext.getCurrent().getCurrentBindingsEntry();
            }

    
    public void downloadJrxml(FacesContext facesContext, OutputStream outputStream) {
        Connection conn=null;
        try {
            
            System.out.println(" pmode:"+pMode);
            
            
            Map n = new HashMap();
            if(pMode != null){
              if(pMode.equalsIgnoreCase("A")){
                 file_name ="r_out_door.jasper";
                DCIteratorBinding paySlip = (DCIteratorBinding) getBindings().get("SearchOutDoorDutyVO1Iterator");
                n.put("p_unit", paySlip.getCurrentRow().getAttribute("UnitCd").toString());
                n.put("p_gp_od_no", paySlip.getCurrentRow().getAttribute("GpOdNo").toString());
                //n.put("p_year", paySlip.getCurrentRow().getAttribute("Year").toString());
                
            }
              else if(pMode.equalsIgnoreCase("B")){
                 file_name ="r_out_station_for_short_distance.jasper";
                DCIteratorBinding paySlip = (DCIteratorBinding) getBindings().get("SearchOutDoorDutyVO1Iterator");
                n.put("p_unit", paySlip.getCurrentRow().getAttribute("UnitCd").toString());
                n.put("p_gp_od_no", paySlip.getCurrentRow().getAttribute("GpOdNo").toString());
               // n.put("p_year", paySlip.getCurrentRow().getAttribute("Year").toString());
            }
            }
            
            OperationBinding binding = ADFUtils.findOperation("fileNameForPrint");
            binding.getParamsMap().put("fileId","ERP0000000120");
            binding.execute();
            System.out.println("Binding Result :"+binding.getResult());
            if(binding.getResult() != null){
                String result = binding.getResult().toString();
                                  int last_index = result.lastIndexOf("/");
                if(last_index==-1)
                {
                    last_index=result.lastIndexOf("\\");
                }
                                                      String path = result.substring(0,last_index+1)+file_name;
                                                      System.out.println("FILE PATH IS===>"+path);
                                  InputStream input = new FileInputStream(path);
            //    InputStream input = new FileInputStream("//home//beta3//Jasper//Jour_prn1.jrxml");
                conn = getConnection( );
                
                JasperReport design = (JasperReport) JRLoader.loadObject(input);
                System.out.println("Path : " + input + " -------" + design+" param:"+n);
                
                @SuppressWarnings("unchecked")
                net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                byte pdf[] = JasperExportManager.exportReportToPdf(print);
                HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                response.getOutputStream().write(pdf);
                response.getOutputStream().flush();
                response.getOutputStream().close();
                facesContext.responseComplete();
                }else
                System.out.println("File Name/File Path not found.");
                } catch (FileNotFoundException fnfe) {
                // TODO: Add catch code
                fnfe.printStackTrace();
                try {
                    System.out.println("in finally connection closed");
                            conn.close( );
                            conn = null;
                    
                } catch( SQLException e ) {
                    e.printStackTrace( );
                }
                } catch (Exception e) {
                // TODO: Add catch code
                e.printStackTrace();
                try {
                    System.out.println("in finally connection closed");
                            conn.close( );
                            conn = null;
                    
                } catch( SQLException e1 ) {
                    e1.printStackTrace( );
                }
                }finally {
                        try {
                                System.out.println("in finally connection closed");
                                        conn.close( );
                                        conn = null;
                                
                        } catch( SQLException e ) {
                                e.printStackTrace( );
                        }
                }
    }

    public void setPMode(String pMode) {
        this.pMode = pMode;
    }

    public String getPMode() {
        return pMode;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    public String getFile_name() {
        return file_name;
    }
    
    public Connection getConnection() throws Exception{
            Context ctx = null;
            Connection conn = null;
            ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/ApplicationDBDS");
            conn = ds.getConnection();
            return conn;
        }
}
