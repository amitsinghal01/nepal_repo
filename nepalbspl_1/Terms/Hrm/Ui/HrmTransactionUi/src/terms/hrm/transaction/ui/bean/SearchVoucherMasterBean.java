package terms.hrm.transaction.ui.bean;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

public class SearchVoucherMasterBean {
    private RichTable searchVoucherMasterBinding;

    public SearchVoucherMasterBean() {
    }

    public void SearchDeleteDialogDL(DialogEvent dialogEvent) {
        {
            if(dialogEvent.getOutcome().name().equals("ok"))
                        {
                        ADFUtils.findOperation("Delete").execute();
                        FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                        FacesContext fc = FacesContext.getCurrentInstance();  
                        fc.addMessage(null, Message);  
                       
                        }
                    AdfFacesContext.getCurrentInstance().addPartialTarget(searchVoucherMasterBinding);

                    }
        }

    public void setSearchVoucherMasterBinding(RichTable searchVoucherMasterBinding) {
        this.searchVoucherMasterBinding = searchVoucherMasterBinding;
    }

    public RichTable getSearchVoucherMasterBinding() {
        return searchVoucherMasterBinding;
    }
}
