package terms.hrm.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;

import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;

import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;
import oracle.jbo.server.ViewObjectImpl;

public class TrainingFeedbackHeaderBean {
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText trainingFeedbackCodeBinding;
    private RichInputDate startDateBinding;
    private RichInputComboboxListOfValues trainingTypeBinding;
    private RichInputText trainingPlanCodeBinding;
    private RichInputDate endDateBinding;
    private RichInputText facultyBinding;
    private RichOutputText bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichButton detailcreateBinding;
    private RichButton detaildeleteBinding;
    private String Message ="C";
    private RichInputText trainingCodeBinding;

    public TrainingFeedbackHeaderBean() {
    }

    public void TrainingFeedbackSaveAL(ActionEvent actionEvent) {
       // OperationBinding op = ADFUtils.findOperation("generateTrainingFeedbackCode");
            //   Object rst = op.execute();
               
            //   System.out.println("Result is===> "+op.getResult());
     ADFUtils.setLastUpdatedBy("TrainingFeedbackHeaderVO1Iterator","LastUpdatedBy");
        String code=(String)trainingFeedbackCodeBinding.getValue();
        System.out.println("Genrated code is---->> "+code);
               
               
               System.out.println("--------Commit-------");
                //   System.out.println("value aftr getting result--->?"+rst);

                   if (Message.equalsIgnoreCase("C")) {
                       
                           ADFUtils.findOperation("Commit").execute();
                           FacesMessage Message = new FacesMessage("Record Saved Successfully. New Entry No. is "+code+" ");   
                           Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                           FacesContext fc = FacesContext.getCurrentInstance();   
                           fc.addMessage(null, Message);  

                   }
                   
                   if (Message.equalsIgnoreCase("E")) {
                       
                           ADFUtils.findOperation("Commit").execute();
                           FacesMessage Message = new FacesMessage("Record Update Successfully.");   
                           Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                           FacesContext fc = FacesContext.getCurrentInstance();   
                           fc.addMessage(null, Message);  
                   }
    }
    
    private void cevmodecheck(){
            if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
                cevModeDisableComponent("V");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("C");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("E");
            }
        }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
               try {
                   Method method1 =
                       component.getClass().getMethod("setDisabled", boolean.class);
                   if (method1 != null) {
                       method1.invoke(component, valueComponent);
                   }
               } catch (NoSuchMethodException e) {
                   try {
                       Method method =
                           component.getClass().getMethod("setReadOnly", boolean.class);
                       if (method != null) {
                           method.invoke(component, valueComponent);
                       }
                   } catch (Exception e1) {
                       // e.printStackTrace();//silently eat this exception.
                   }


               } catch (Exception e) {
                   // e.printStackTrace();//silently eat this exception.
               }
               List<UIComponent> childComponents = component.getChildren();
               for (UIComponent comp : childComponents) {
                   makeComponentHierarchyReadOnly(comp, valueComponent);
               }
           }
    
    public void cevModeDisableComponent(String mode) {
    
            if (mode.equals("E")) {
                Message ="E";
                
                getUnitCodeBinding().setDisabled(true);
                getTrainingFeedbackCodeBinding().setDisabled(true);             
                getTrainingCodeBinding().setDisabled(true);             
                getStartDateBinding().setDisabled(true);             
                getEndDateBinding().setDisabled(true);            
                getFacultyBinding().setDisabled(true);
                getTrainingPlanCodeBinding().setDisabled(true);
                    
            } else if (mode.equals("C")) {
                
                getUnitCodeBinding().setDisabled(true);
                getTrainingFeedbackCodeBinding().setDisabled(true);             
                getTrainingCodeBinding().setDisabled(true);             
                getStartDateBinding().setDisabled(true);             
                getEndDateBinding().setDisabled(true);            
                getFacultyBinding().setDisabled(true);
                getTrainingPlanCodeBinding().setDisabled(true);
              
                
            } else if (mode.equals("V")) {
               
            }
    }


    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setTrainingFeedbackCodeBinding(RichInputText trainingFeedbackCodeBinding) {
        this.trainingFeedbackCodeBinding = trainingFeedbackCodeBinding;
    }

    public RichInputText getTrainingFeedbackCodeBinding() {
        return trainingFeedbackCodeBinding;
    }

    public void setStartDateBinding(RichInputDate startDateBinding) {
        this.startDateBinding = startDateBinding;
    }

    public RichInputDate getStartDateBinding() {
        return startDateBinding;
    }

    public void setTrainingTypeBinding(RichInputComboboxListOfValues trainingTypeBinding) {
        this.trainingTypeBinding = trainingTypeBinding;
    }


    public void setTrainingPlanCodeBinding(RichInputText trainingPlanCodeBinding) {
        this.trainingPlanCodeBinding = trainingPlanCodeBinding;
    }

    public RichInputText getTrainingPlanCodeBinding() {
        return trainingPlanCodeBinding;
    }



    public void setEndDateBinding(RichInputDate endDateBinding) {
        this.endDateBinding = endDateBinding;
    }

    public RichInputDate getEndDateBinding() {
        return endDateBinding;
    }

    public void setFacultyBinding(RichInputText facultyBinding) {
        this.facultyBinding = facultyBinding;
    }

    public RichInputText getFacultyBinding() {
        return facultyBinding;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setDetailcreateBinding(RichButton detailcreateBinding) {
        this.detailcreateBinding = detailcreateBinding;
    }

    public RichButton getDetailcreateBinding() {
        return detailcreateBinding;
    }

    public void setDetaildeleteBinding(RichButton detaildeleteBinding) {
        this.detaildeleteBinding = detaildeleteBinding;
    }

    public RichButton getDetaildeleteBinding() {
        return detaildeleteBinding;
    }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void trainingTitleVCL(ValueChangeEvent valueChangeEvent) {
        
        OperationBinding op1 = ADFUtils.findOperation("generateTrainingFeedbackCode");
               Object rst1 = op1.execute();
        
        OperationBinding op = ADFUtils.findOperation("populateDataFromViewToDeailTableTrainingFB");
               Object rst = op.execute();
        
//
//        DCIteratorBinding dci = ADFUtils.findIterator("ContractOrderDetailVO1Iterator");
//        RowSetIterator rsi=dci.getViewObject().createRowSetIterator(null);
//            while(rsi.hasNext()) {
//                System.out.println("LUMSUMMMMM");
//                Row r = rsi.next();
//                String lumsum=(String) r.getAttribute("Lumsum");
//                System.out.println("LUMSUM ITERATOR"+lumsum);
//              
//            }
//            rsi.closeRowSetIterator();



        
    }

    public void createButtonAL(ActionEvent actionEvent) {
        //ADFUtils.findOperation("CreateInsert").execute();
        
//        OperationBinding op = ADFUtils.findOperation("populateDataFromViewToDeailTableTrainingFB");
//               Object rst = op.execute();
    }

    public void setTrainingCodeBinding(RichInputText trainingCodeBinding) {
        this.trainingCodeBinding = trainingCodeBinding;
    }

    public RichInputText getTrainingCodeBinding() {
        return trainingCodeBinding;
    }
}
