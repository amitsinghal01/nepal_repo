package terms.hrm.transaction.ui.bean;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import javax.naming.Context;
import javax.naming.InitialContext;

import javax.servlet.http.HttpServletResponse;

import javax.sql.DataSource;

//import net.sf.jasperreports.engine.JasperExportManager;
//import net.sf.jasperreports.engine.JasperFillManager;
//import net.sf.jasperreports.engine.JasperReport;

//import net.sf.jasperreports.engine.util.JRLoader;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;
import oracle.jbo.server.ViewObjectImpl;

//import net.sf.jasperreports.engine.JasperCompileManager;
//
//import net.sf.jasperreports.engine.JasperExportManager;
//import net.sf.jasperreports.engine.JasperFillManager;
//import net.sf.jasperreports.engine.JasperReport;
//import net.sf.jasperreports.engine.util.JRLoader;
//import net.sf.jasperreports.engine.xml.JRXmlLoader;

public class TrainingHistoryDetailBean {
    private RichSelectOneChoice searchForBinding;
    private RichInputComboboxListOfValues selectByBinding;
    private RichInputText labelChangeBinding;

    public TrainingHistoryDetailBean() {
    }

    public void goPopulateAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("setGoDataForTrainingHistoryDetailProc").execute();
        //        ADFUtils.findOperation("goDataPopulateForTrainingHistoryDetail").execute();
        oracle.adf.model.OperationBinding op1 =
            (oracle.adf.model.OperationBinding) ADFUtils.findOperation("CreateFilterCP");
        op1.getParamsMap().put("mode", "V");
        op1.execute();
        //        ADFUtils.findOperation("setGoDataForTrainingHistoryDetailProc").execute();
    }

    public void changeLabelForTrainingHistoryDetail1() {
        System.out.println("INSIDE changeLabelForTrainingHistoryDetail");
        DCIteratorBinding itr = ADFUtils.findIterator("TrainingHistoryHeaderDummyVO1Iterator");
        if (itr != null) {
            //            String searchFor = (String)hd.getCurrentRow().getAttribute("SearchForTrans");
            System.out.println("SEARCH FOR VALUE IS #### ");

            String ob = (String) searchForBinding.getValue();
            System.out.println("INSIDE VALUE ##" + ob);
            String val = "";
            String value = "TT";
            String empName = "EN";
            String val1 = "Search By";
            String value1 = "Select Training Title";
            String value2 = "Select Employee Name";

            try {
                System.out.println("INSIDE TRY" + ob.toString().equalsIgnoreCase(value));
                if (ob.toString().equalsIgnoreCase(val)) {
                    System.out.println("");

                    labelChangeBinding.setValue(val1);
                    System.out.println("INSIDE TRY IF1 #### " + val1);
                }

                else if (ob.toString().equalsIgnoreCase(value)) {
                    System.out.println("inside value ####");
                    labelChangeBinding.setValue(value1);
                    System.out.println("INSIDE TRY IF #### " + value1);
                } else if (ob.toString().equalsIgnoreCase(empName)) {
                    labelChangeBinding.setValue(value2);
                    System.out.println("INSIDE ELSE IF #### " + value2);
                }
            }

            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void searchForVCL(ValueChangeEvent vce) {
        System.out.println("INSIDE vce #### ");
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());

        if (vce != null) {
            System.out.println("INSIDE VCE #### ");
            //            setSearchForBinding(searchForBinding);
            changeLabelForTrainingHistoryDetail1();
        }
    }

    public void setSearchForBinding(RichSelectOneChoice searchForBinding) {
        this.searchForBinding = searchForBinding;
    }

    public RichSelectOneChoice getSearchForBinding() {
        return searchForBinding;
    }

    public void setSelectByBinding(RichInputComboboxListOfValues selectByBinding) {
        this.selectByBinding = selectByBinding;
    }

    public RichInputComboboxListOfValues getSelectByBinding() {
        return selectByBinding;
    }

    private void setSelectByBinding(String value1) {

        value1 = "Select Training Title";
    }

    public void setLabelChangeBinding(RichInputText labelChangeBinding) {
        this.labelChangeBinding = labelChangeBinding;
    }

    public RichInputText getLabelChangeBinding() {
        return labelChangeBinding;
    }

    public void trainingTitleButtonAL(ActionEvent actionEvent) {
        // Add event code here...
    }

    public BindingContainer getBindings() {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
    }

    public void downloadJrxml(FacesContext facesContext, OutputStream outputStream) {
        Connection conn = null;
        try {

            OperationBinding binding = ADFUtils.findOperation("fileNameForPrint");
            binding.getParamsMap().put("fileId", "ERP0000001855");
            binding.execute();
            System.out.println("Binding Result :" + binding.getResult());
            System.out.println("Unit Code ######## ");

            if (binding.getResult() != null) {
                InputStream input = new FileInputStream(binding.getResult().toString());


                DCIteratorBinding ppIter =
                    (DCIteratorBinding) getBindings().get("TrainingHistoryDetailDummyVO1Iterator");

                Map n = new HashMap();
                //                        n.put("P_TPLAN_CD", planCd);
                //                        n.put("P_EMP_NUMBER", empNumber);
                //                        n.put("P_UNIT", unitCode);

                /*      conn = getConnection();

                JasperReport design = (JasperReport) JRLoader.loadObject(input);
                System.out.println("Path : " + input + " -------" + design + " param:" + n);

                @SuppressWarnings("unchecked")
                net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                byte pdf[] = JasperExportManager.exportReportToPdf(print);
                HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                response.getOutputStream().write(pdf);
                response.getOutputStream().flush();
                response.getOutputStream().close();
                facesContext.responseComplete(); */
            } else
                System.out.println("File Name/File Path not found.");
        } catch (FileNotFoundException fnfe) {
            // TODO: Add catch code
            fnfe.printStackTrace();
            try {
                System.out.println("in finally connection closed");
                conn.close();
                conn = null;

            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            // TODO: Add catch code
            e.printStackTrace();
            try {
                System.out.println("in finally connection closed");
                conn.close();
                conn = null;

            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        } finally {
            try {
                System.out.println("in finally connection closed");
                conn.close();
                conn = null;

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public Connection getConnection() throws Exception {
        Context ctx = null;
        Connection conn = null;
        ctx = new InitialContext();
        DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/ApplicationDBDS");
        conn = ds.getConnection();
        return conn;
    }
}
