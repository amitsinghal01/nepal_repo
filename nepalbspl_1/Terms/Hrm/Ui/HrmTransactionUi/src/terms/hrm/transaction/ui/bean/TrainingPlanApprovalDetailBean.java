package terms.hrm.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichPanelLabelAndMessage;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class TrainingPlanApprovalDetailBean {
    private RichTable trgPlanQuestionTableBinding;
    private RichTable trgPlanEmpTableBinding;
    private RichInputText trainingCodeBinding;
    private RichPanelLabelAndMessage bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton detailcreateBinding;
    private RichButton detaildeleteBinding;
    private RichButton detailcreate1Binding;
    private RichButton detaildelete1Binding;
    private RichInputText empdetailBinding;
    private RichInputText designationdetailBinding;
    private RichInputText designamedetailBinding;
    private RichInputText deptcodedetailBinding;
    private RichInputText deptnamedetailBinding;
    private RichInputText questypedetailBinding;

    public TrainingPlanApprovalDetailBean() {
    }

    public void deleteDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
                    ADFUtils.findOperation("Delete").execute();
                    System.out.println("Record Delete Successfully");

                    FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);

                }
                AdfFacesContext.getCurrentInstance().addPartialTarget(trgPlanEmpTableBinding);
    }

    public void deleteDialogDL1(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
                    ADFUtils.findOperation("Delete1").execute();
                    System.out.println("Record Delete Successfully");

                    FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);

                }
                AdfFacesContext.getCurrentInstance().addPartialTarget(trgPlanQuestionTableBinding);
    }

    public void setTrgPlanQuestionTableBinding(RichTable trgPlanQuestionTableBinding) {
        this.trgPlanQuestionTableBinding = trgPlanQuestionTableBinding;
    }

    public RichTable getTrgPlanQuestionTableBinding() {
        return trgPlanQuestionTableBinding;
    }

    public void setTrgPlanEmpTableBinding(RichTable trgPlanEmpTableBinding) {
        this.trgPlanEmpTableBinding = trgPlanEmpTableBinding;
    }

    public RichTable getTrgPlanEmpTableBinding() {
        return trgPlanEmpTableBinding;
    }

    public void saveAL(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedBy("TrainingPlanHeaderVO1Iterator","LastUpdatedBy");
        System.out.println("----------------------Inside AL---------------");

        OperationBinding Opp = ADFUtils.findOperation("generateTrainingCodeNumber");
        Object obj = Opp.execute();
        System.out.println("Result is===> " + Opp.getResult());
        if (Opp.getResult() != null && !Opp.getResult().equals("N")) {
            ADFUtils.findOperation("Commit").execute();
            ADFUtils.showMessage("Record Save Successfully.Leave Application Number is " + trainingCodeBinding.getValue(), 2);
            ADFUtils.setEL("#{pageFlowScope.mode}", "V");
            System.out.println("after setting"+ADFUtils.resolveExpression("#{pageFlowScope.mode}"));
            cevmodecheck();
            AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
        } else {
            ADFUtils.findOperation("Commit").execute();
            ADFUtils.showMessage(" Record Updated Successfully", 2);
            ADFUtils.setEL("#{pageFlowScope.mode}", "V");
            System.out.println("after setting"+ADFUtils.resolveExpression("#{pageFlowScope.mode}"));
            cevmodecheck();
            AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
        }
    }

    public void setTrainingCodeBinding(RichInputText trainingCodeBinding) {
        this.trainingCodeBinding = trainingCodeBinding;
    }

    public RichInputText getTrainingCodeBinding() {
        return trainingCodeBinding;
    }

    public void setBindingOutputText(RichPanelLabelAndMessage bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichPanelLabelAndMessage getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setDetailcreateBinding(RichButton detailcreateBinding) {
        this.detailcreateBinding = detailcreateBinding;
    }

    public RichButton getDetailcreateBinding() {
        return detailcreateBinding;
    }

    public void setDetaildeleteBinding(RichButton detaildeleteBinding) {
        this.detaildeleteBinding = detaildeleteBinding;
    }

    public RichButton getDetaildeleteBinding() {
        return detaildeleteBinding;
    }

    public void setDetailcreate1Binding(RichButton detailcreate1Binding) {
        this.detailcreate1Binding = detailcreate1Binding;
    }

    public RichButton getDetailcreate1Binding() {
        return detailcreate1Binding;
    }

    public void setDetaildelete1Binding(RichButton detaildelete1Binding) {
        this.detaildelete1Binding = detaildelete1Binding;
    }

    public RichButton getDetaildelete1Binding() {
        return detaildelete1Binding;
    }
    
    private void cevmodecheck(){
           if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
               cevModeDisableComponent("V");
           }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
               cevModeDisableComponent("C");
           }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
               cevModeDisableComponent("E");
           }
       }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                try {
                    Method method1 =
                        component.getClass().getMethod("setDisabled", boolean.class);
                    if (method1 != null) {
                        method1.invoke(component, valueComponent);
                    }
                } catch (NoSuchMethodException e) {
                    try {
                        Method method =
                            component.getClass().getMethod("setReadOnly", boolean.class);
                        if (method != null) {
                            method.invoke(component, valueComponent);
                        }
                    } catch (Exception e1) {
                    }


                } catch (Exception e) {
                }
                List<UIComponent> childComponents = component.getChildren();
                for (UIComponent comp : childComponents) {
                    makeComponentHierarchyReadOnly(comp, valueComponent);
                }
            }
    
        public void cevModeDisableComponent(String mode) {
       
            if (mode.equals("E")) {

                getTrainingCodeBinding().setDisabled(true);
                getEmpdetailBinding().setDisabled(true);
                getDesignamedetailBinding().setDisabled(true);
                getDesignationdetailBinding().setDisabled(true);
                getDeptcodedetailBinding().setDisabled(true);
                getDeptnamedetailBinding().setDisabled(true);
                getQuestypedetailBinding().setDisabled(true);
                
            } else if (mode.equals("C")) {
                
                getTrainingCodeBinding().setDisabled(true);
                getEmpdetailBinding().setDisabled(true);
                getDesignamedetailBinding().setDisabled(true);
                getDesignationdetailBinding().setDisabled(true);
                getDeptcodedetailBinding().setDisabled(true);
                getDeptnamedetailBinding().setDisabled(true);
                getQuestypedetailBinding().setDisabled(true);
                
            } else if (mode.equals("V")) {

            }
            
        }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setEmpdetailBinding(RichInputText empdetailBinding) {
        this.empdetailBinding = empdetailBinding;
    }

    public RichInputText getEmpdetailBinding() {
        return empdetailBinding;
    }

    public void setDesignationdetailBinding(RichInputText designationdetailBinding) {
        this.designationdetailBinding = designationdetailBinding;
    }

    public RichInputText getDesignationdetailBinding() {
        return designationdetailBinding;
    }

    public void setDesignamedetailBinding(RichInputText designamedetailBinding) {
        this.designamedetailBinding = designamedetailBinding;
    }

    public RichInputText getDesignamedetailBinding() {
        return designamedetailBinding;
    }

    public void setDeptcodedetailBinding(RichInputText deptcodedetailBinding) {
        this.deptcodedetailBinding = deptcodedetailBinding;
    }

    public RichInputText getDeptcodedetailBinding() {
        return deptcodedetailBinding;
    }

    public void setDeptnamedetailBinding(RichInputText deptnamedetailBinding) {
        this.deptnamedetailBinding = deptnamedetailBinding;
    }

    public RichInputText getDeptnamedetailBinding() {
        return deptnamedetailBinding;
    }

    public void setQuestypedetailBinding(RichInputText questypedetailBinding) {
        this.questypedetailBinding = questypedetailBinding;
    }

    public RichInputText getQuestypedetailBinding() {
        return questypedetailBinding;
    }
}
