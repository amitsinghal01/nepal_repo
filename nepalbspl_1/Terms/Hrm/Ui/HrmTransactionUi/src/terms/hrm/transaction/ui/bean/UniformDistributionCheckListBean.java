package terms.hrm.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;

import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.binding.OperationBinding;

public class UniformDistributionCheckListBean {
    private RichPanelHeader getMyPageRootComponent;
    private RichOutputText outputBinding;
    private RichOutputText outputbindingnew;
    private RichInputText entryNoBinding;
    private String editAction="V";
    private RichInputDate entrydteBinding;
    private RichInputComboboxListOfValues unitCdBinding;
    private RichInputText designationCodeBinding;
    private RichInputText departmentCodeBinding;
    private RichInputDate confimationBinding;


    public UniformDistributionCheckListBean() {
    }

    public void SaveAL(ActionEvent actionEvent) {
//       OperationBinding op= ADFUtils.findOperation("getEntryNoforUnifmDistNo");
//       op.execute();
        ADFUtils.setLastUpdatedBy("UniformDistCheckListDualVO4Iterator","LastUpdatedBy");
       if(entryNoBinding.getValue()==null){
           OperationBinding op= ADFUtils.findOperation("getEntryNoforUnifmDistNo");
           op.execute();
           ADFUtils.findOperation("Commit").execute();
           ADFUtils.showMessage("Record saved Succsfully.", 2);
           
       }
    }

    public void PopulateBean(ActionEvent actionEvent) {
       
        
    }

    public void EditAL(ActionEvent actionEvent) {
        cevmodecheck();
    }
    private void cevmodecheck(){
           if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
               cevModeDisableComponent("V");
           }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
               cevModeDisableComponent("C");
           }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
               cevModeDisableComponent("E");
           }
       }
    
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                try {
                    Method method1 =
                        component.getClass().getMethod("setDisabled", boolean.class);
                    if (method1 != null) {
                        method1.invoke(component, valueComponent);
                    }
                } catch (NoSuchMethodException e) {
                    try {
                        Method method =
                            component.getClass().getMethod("setReadOnly", boolean.class);
                        if (method != null) {
                            method.invoke(component, valueComponent);
                        }
                    } catch (Exception e1) {
                        // e.printStackTrace();//silently eat this exception.
                    }


                } catch (Exception e) {
                    // e.printStackTrace();//silently eat this exception.
                }
                List<UIComponent> childComponents = component.getChildren();
                for (UIComponent comp : childComponents) {
                    makeComponentHierarchyReadOnly(comp, valueComponent);
                }
            }


    public void cevModeDisableComponent(String mode) {
    //            FacesContext fctx = FacesContext.getCurrentInstance();
    //            ELContext elctx = fctx.getELContext();
    //            Application jsfApp = fctx.getApplication();
    //            //create a ValueExpression that points to the ADF binding layer
    //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
    //            //
    //            ValueExpression valueExpr = exprFactory.createValueExpression(
    //                                         elctx,
    //                                         "#{pageFlowScope.mode=='E'}",
    //                                          Object.class
    //                                         );
    //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
        if (mode.equals("E")) {
    
             
        } else if (mode.equals("C")) {
            getEntryNoBinding().setDisabled(true);
            getEntrydteBinding().setDisabled(true);
          
          
        } else if (mode.equals("V")) {
           
        }
        
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setOutputBinding(RichOutputText outputBinding) {
        this.outputBinding = outputBinding;
    }

    public RichOutputText getOutputBinding() {
        
        return outputBinding;
    }

    public void PopulateBean1(ActionEvent actionEvent) {
        OperationBinding op=ADFUtils.findOperation("getUniformDistCheckList");
                op.getParamsMap().put("PASS_UNIT", unitCdBinding.getValue());
                op.execute();
                ADFUtils.findOperation("getDataALl").execute();
    }


    public void setEntryNoBinding(RichInputText entryNoBinding) {
        this.entryNoBinding = entryNoBinding;
    }

    public RichInputText getEntryNoBinding() {
        return entryNoBinding;
    }

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public void setEntrydteBinding(RichInputDate entrydteBinding) {
        this.entrydteBinding = entrydteBinding;
    }

    public RichInputDate getEntrydteBinding() {
        return entrydteBinding;
    }

    public void setUnitCdBinding(RichInputComboboxListOfValues unitCdBinding) {
        this.unitCdBinding = unitCdBinding;
    }

    public RichInputComboboxListOfValues getUnitCdBinding() {
        return unitCdBinding;
    }

    public void setDesignationCodeBinding(RichInputText designationCodeBinding) {
        this.designationCodeBinding = designationCodeBinding;
    }

    public RichInputText getDesignationCodeBinding() {
        return designationCodeBinding;
    }

    public void setDepartmentCodeBinding(RichInputText departmentCodeBinding) {
        this.departmentCodeBinding = departmentCodeBinding;
    }

    public RichInputText getDepartmentCodeBinding() {
        return departmentCodeBinding;
    }

    public void setConfimationBinding(RichInputDate confimationBinding) {
        this.confimationBinding = confimationBinding;
    }

    public RichInputDate getConfimationBinding() {
        return confimationBinding;
    }
}
