package terms.hrm.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

public class VoucherMasterBean {
    private RichTable voucherMasterDtlTableBinding;
    private RichInputComboboxListOfValues acCodeBinding;
    private RichPanelHeader getMyPageRootComponent;

    private RichOutputText bindingOutputText;
    private RichInputComboboxListOfValues bindUnitcode;

    public VoucherMasterBean() {
    }

    public void deletepopupDL(DialogEvent dialogEvent) {
        {
            if(dialogEvent.getOutcome().name().equals("ok"))
                        {
                        ADFUtils.findOperation("Delete").execute();
                        FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                        FacesContext fc = FacesContext.getCurrentInstance();  
                        fc.addMessage(null, Message);  
                       
                        }
                    AdfFacesContext.getCurrentInstance().addPartialTarget(voucherMasterDtlTableBinding);

                    }
    }

    public void setVoucherMasterDtlTableBinding(RichTable voucherMasterDtlTableBinding) {
        this.voucherMasterDtlTableBinding = voucherMasterDtlTableBinding;
    }

    public RichTable getVoucherMasterDtlTableBinding() {
        return voucherMasterDtlTableBinding;
    }

    public void saveAL(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedBy("VoucherMasterHeaderVO1Iterator","LastUpdatedBy");
        if(acCodeBinding.getValue()!=null){
            FacesMessage Message = new FacesMessage("Record Saved Successfully.");  
            Message.setSeverity(FacesMessage.SEVERITY_INFO);  
            FacesContext fc = FacesContext.getCurrentInstance();  
            fc.addMessage(null, Message); 
        }
        else {
            FacesMessage Message = new FacesMessage("Record Updated Successfully.");  
            Message.setSeverity(FacesMessage.SEVERITY_INFO);  
            FacesContext fc = FacesContext.getCurrentInstance();  
            fc.addMessage(null, Message); 
        }
    }

    public void setAcCodeBinding(RichInputComboboxListOfValues acCodeBinding) {
        this.acCodeBinding = acCodeBinding;
    }

    public RichInputComboboxListOfValues getAcCodeBinding() {
        return acCodeBinding;
    }

    private void cevmodecheck(){
           if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
               cevModeDisableComponent("V");
           }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
               cevModeDisableComponent("C");
           }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
               cevModeDisableComponent("E");
           }
       }
    
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                try {
                    Method method1 =
                        component.getClass().getMethod("setDisabled", boolean.class);
                    if (method1 != null) {
                        method1.invoke(component, valueComponent);
                    }
                } catch (NoSuchMethodException e) {
                    try {
                        Method method =
                            component.getClass().getMethod("setReadOnly", boolean.class);
                        if (method != null) {
                            method.invoke(component, valueComponent);
                        }
                    } catch (Exception e1) {
                        // e.printStackTrace();//silently eat this exception.
                    }


                } catch (Exception e) {
                    // e.printStackTrace();//silently eat this exception.
                }
                List<UIComponent> childComponents = component.getChildren();
                for (UIComponent comp : childComponents) {
                    makeComponentHierarchyReadOnly(comp, valueComponent);
                }
            }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }
    public void cevModeDisableComponent(String mode) {
    //            FacesContext fctx = FacesContext.getCurrentInstance();
    //            ELContext elctx = fctx.getELContext();
    //            Application jsfApp = fctx.getApplication();
    //            //create a ValueExpression that points to the ADF binding layer
    //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
    //            //
    //            ValueExpression valueExpr = exprFactory.createValueExpression(
    //                                         elctx,
    //                                         "#{pageFlowScope.mode=='E'}",
    //                                          Object.class
    //                                         );
    //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
        if (mode.equals("E")) {
            getBindUnitcode().setDisabled(true);
             
        } else if (mode.equals("C")) {
            getBindUnitcode().setDisabled(true);
           
        } else if (mode.equals("V")) {
           
        }
        
    }




    public void EditAL(ActionEvent actionEvent) {
       cevmodecheck();
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setBindUnitcode(RichInputComboboxListOfValues bindUnitcode) {
        this.bindUnitcode = bindUnitcode;
    }

    public RichInputComboboxListOfValues getBindUnitcode() {
        return bindUnitcode;
    }
}
