package terms.hrm.transaction.ui.bean;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.application.FacesMessage;

import javax.faces.context.FacesContext;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.event.QueryEvent;

import oracle.adf.view.rich.model.AttributeCriterion;
import oracle.adf.view.rich.model.AttributeDescriptor;
import oracle.adf.view.rich.model.ConjunctionCriterion;
import oracle.adf.view.rich.model.Criterion;
import oracle.adf.view.rich.model.QueryDescriptor;

import oracle.binding.BindingContainer;

import oracle.jbo.server.ViewObjectImpl;

public class monthlyPFProcessBean {
    public monthlyPFProcessBean() {
    }
    
    public void customeQueryListener(QueryEvent queryEvent) {
        Integer month = null;
        Integer year = null;

        QueryDescriptor qd = queryEvent.getDescriptor();

        ConjunctionCriterion conCrit = qd.getConjunctionCriterion();
        //access the list of search fields
        List<Criterion> criterionList = conCrit.getCriterionList();
        //iterate over the attributes to find FromDate and ToDate
        for (Criterion criterion : criterionList) {
            AttributeDescriptor attrDescriptor = ((AttributeCriterion) criterion).getAttribute();

            if (attrDescriptor.getName().equalsIgnoreCase("Mon")) {
                month = (Integer) ((AttributeCriterion) criterion).getValues().get(0);
            } else {
                if (attrDescriptor.getName().equalsIgnoreCase("Year")) {
                    year = (Integer) ((AttributeCriterion) criterion).getValues().get(0);
                }
            }
        }
        if ((year != null && year<0)) {
            FacesMessage msg = new FacesMessage("Year Value can not be negative.");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        } else {
            
            ADFUtils.invokeEL("#{bindings.SearchMonthlyPfProcessHeaderVOCriteriaQuery.processQuery}",
                              new Class[] { QueryEvent.class }, new Object[] { queryEvent });
        }
    }
}
