package terms.mkt.transaction.model.view;

import java.math.BigDecimal;

import oracle.jbo.RowIterator;
import oracle.jbo.RowSet;
import oracle.jbo.domain.Date;
import oracle.jbo.domain.Number;
import oracle.jbo.domain.Timestamp;
import oracle.jbo.server.AttributeDefImpl;
import oracle.jbo.server.EntityImpl;
import oracle.jbo.server.ViewRowImpl;

import terms.eng.setup.model.entity.VendorMasterEOImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Tue Oct 09 15:01:02 IST 2018
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class DailyDespatchHeaderVORowImpl extends ViewRowImpl {


    public static final int ENTITY_DAILYDESPATCHHEADEREO = 0;
    public static final int ENTITY_UNITEO = 1;
    public static final int ENTITY_VENDORMASTEREO = 2;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        CreatedBy {
            public Object get(DailyDespatchHeaderVORowImpl obj) {
                return obj.getCreatedBy();
            }

            public void put(DailyDespatchHeaderVORowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        CreationDate {
            public Object get(DailyDespatchHeaderVORowImpl obj) {
                return obj.getCreationDate();
            }

            public void put(DailyDespatchHeaderVORowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        EntryDate {
            public Object get(DailyDespatchHeaderVORowImpl obj) {
                return obj.getEntryDate();
            }

            public void put(DailyDespatchHeaderVORowImpl obj, Object value) {
                obj.setEntryDate((Date) value);
            }
        }
        ,
        EntryId {
            public Object get(DailyDespatchHeaderVORowImpl obj) {
                return obj.getEntryId();
            }

            public void put(DailyDespatchHeaderVORowImpl obj, Object value) {
                obj.setEntryId((Number) value);
            }
        }
        ,
        EntryNo {
            public Object get(DailyDespatchHeaderVORowImpl obj) {
                return obj.getEntryNo();
            }

            public void put(DailyDespatchHeaderVORowImpl obj, Object value) {
                obj.setEntryNo((String) value);
            }
        }
        ,
        LastUpdateDate {
            public Object get(DailyDespatchHeaderVORowImpl obj) {
                return obj.getLastUpdateDate();
            }

            public void put(DailyDespatchHeaderVORowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        LastUpdatedBy {
            public Object get(DailyDespatchHeaderVORowImpl obj) {
                return obj.getLastUpdatedBy();
            }

            public void put(DailyDespatchHeaderVORowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        ObjectVersionNumber {
            public Object get(DailyDespatchHeaderVORowImpl obj) {
                return obj.getObjectVersionNumber();
            }

            public void put(DailyDespatchHeaderVORowImpl obj, Object value) {
                obj.setObjectVersionNumber((Number) value);
            }
        }
        ,
        PartyCd {
            public Object get(DailyDespatchHeaderVORowImpl obj) {
                return obj.getPartyCd();
            }

            public void put(DailyDespatchHeaderVORowImpl obj, Object value) {
                obj.setPartyCd((String) value);
            }
        }
        ,
        EditTrans {
            public Object get(DailyDespatchHeaderVORowImpl obj) {
                return obj.getEditTrans();
            }

            public void put(DailyDespatchHeaderVORowImpl obj, Object value) {
                obj.setEditTrans((Integer) value);
            }
        }
        ,
        UnitCd {
            public Object get(DailyDespatchHeaderVORowImpl obj) {
                return obj.getUnitCd();
            }

            public void put(DailyDespatchHeaderVORowImpl obj, Object value) {
                obj.setUnitCd((String) value);
            }
        }
        ,
        Name {
            public Object get(DailyDespatchHeaderVORowImpl obj) {
                return obj.getName();
            }

            public void put(DailyDespatchHeaderVORowImpl obj, Object value) {
                obj.setName((String) value);
            }
        }
        ,
        Code {
            public Object get(DailyDespatchHeaderVORowImpl obj) {
                return obj.getCode();
            }

            public void put(DailyDespatchHeaderVORowImpl obj, Object value) {
                obj.setCode((String) value);
            }
        }
        ,
        ObjectVersionNumber1 {
            public Object get(DailyDespatchHeaderVORowImpl obj) {
                return obj.getObjectVersionNumber1();
            }

            public void put(DailyDespatchHeaderVORowImpl obj, Object value) {
                obj.setObjectVersionNumber1((Integer) value);
            }
        }
        ,
        Name1 {
            public Object get(DailyDespatchHeaderVORowImpl obj) {
                return obj.getName1();
            }

            public void put(DailyDespatchHeaderVORowImpl obj, Object value) {
                obj.setName1((String) value);
            }
        }
        ,
        ObjectVersionNumber2 {
            public Object get(DailyDespatchHeaderVORowImpl obj) {
                return obj.getObjectVersionNumber2();
            }

            public void put(DailyDespatchHeaderVORowImpl obj, Object value) {
                obj.setObjectVersionNumber2((Integer) value);
            }
        }
        ,
        VendorId {
            public Object get(DailyDespatchHeaderVORowImpl obj) {
                return obj.getVendorId();
            }

            public void put(DailyDespatchHeaderVORowImpl obj, Object value) {
                obj.setVendorId((Long) value);
            }
        }
        ,
        AmdNo {
            public Object get(DailyDespatchHeaderVORowImpl obj) {
                return obj.getAmdNo();
            }

            public void put(DailyDespatchHeaderVORowImpl obj, Object value) {
                obj.setAmdNo((Number) value);
            }
        }
        ,
        AmdDate {
            public Object get(DailyDespatchHeaderVORowImpl obj) {
                return obj.getAmdDate();
            }

            public void put(DailyDespatchHeaderVORowImpl obj, Object value) {
                obj.setAmdDate((Date) value);
            }
        }
        ,
        Note {
            public Object get(DailyDespatchHeaderVORowImpl obj) {
                return obj.getNote();
            }

            public void put(DailyDespatchHeaderVORowImpl obj, Object value) {
                obj.setNote((String) value);
            }
        }
        ,
        DailyDespatchDetailVO {
            public Object get(DailyDespatchHeaderVORowImpl obj) {
                return obj.getDailyDespatchDetailVO();
            }

            public void put(DailyDespatchHeaderVORowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        UnitVO1 {
            public Object get(DailyDespatchHeaderVORowImpl obj) {
                return obj.getUnitVO1();
            }

            public void put(DailyDespatchHeaderVORowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        CustomerVVO1 {
            public Object get(DailyDespatchHeaderVORowImpl obj) {
                return obj.getCustomerVVO1();
            }

            public void put(DailyDespatchHeaderVORowImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ;
        static AttributesEnum[] vals = null;
        ;
        private static final int firstIndex = 0;

        public abstract Object get(DailyDespatchHeaderVORowImpl object);

        public abstract void put(DailyDespatchHeaderVORowImpl object, Object value);

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int CREATEDBY = AttributesEnum.CreatedBy.index();
    public static final int CREATIONDATE = AttributesEnum.CreationDate.index();
    public static final int ENTRYDATE = AttributesEnum.EntryDate.index();
    public static final int ENTRYID = AttributesEnum.EntryId.index();
    public static final int ENTRYNO = AttributesEnum.EntryNo.index();
    public static final int LASTUPDATEDATE = AttributesEnum.LastUpdateDate.index();
    public static final int LASTUPDATEDBY = AttributesEnum.LastUpdatedBy.index();
    public static final int OBJECTVERSIONNUMBER = AttributesEnum.ObjectVersionNumber.index();
    public static final int PARTYCD = AttributesEnum.PartyCd.index();
    public static final int EDITTRANS = AttributesEnum.EditTrans.index();
    public static final int UNITCD = AttributesEnum.UnitCd.index();
    public static final int NAME = AttributesEnum.Name.index();
    public static final int CODE = AttributesEnum.Code.index();
    public static final int OBJECTVERSIONNUMBER1 = AttributesEnum.ObjectVersionNumber1.index();
    public static final int NAME1 = AttributesEnum.Name1.index();
    public static final int OBJECTVERSIONNUMBER2 = AttributesEnum.ObjectVersionNumber2.index();
    public static final int VENDORID = AttributesEnum.VendorId.index();
    public static final int AMDNO = AttributesEnum.AmdNo.index();
    public static final int AMDDATE = AttributesEnum.AmdDate.index();
    public static final int NOTE = AttributesEnum.Note.index();
    public static final int DAILYDESPATCHDETAILVO = AttributesEnum.DailyDespatchDetailVO.index();
    public static final int UNITVO1 = AttributesEnum.UnitVO1.index();
    public static final int CUSTOMERVVO1 = AttributesEnum.CustomerVVO1.index();

    /**
     * This is the default constructor (do not remove).
     */
    public DailyDespatchHeaderVORowImpl() {
    }

    /**
     * Gets DailyDespatchHeaderEO entity object.
     * @return the DailyDespatchHeaderEO
     */
    public EntityImpl getDailyDespatchHeaderEO() {
        return (EntityImpl) getEntity(ENTITY_DAILYDESPATCHHEADEREO);
    }

    /**
     * Gets UnitEO entity object.
     * @return the UnitEO
     */
    public EntityImpl getUnitEO() {
        return (EntityImpl) getEntity(ENTITY_UNITEO);
    }

    /**
     * Gets VendorMasterEO entity object.
     * @return the VendorMasterEO
     */
    public VendorMasterEOImpl getVendorMasterEO() {
        return (VendorMasterEOImpl) getEntity(ENTITY_VENDORMASTEREO);
    }

    /**
     * Gets the attribute value for CREATED_BY using the alias name CreatedBy.
     * @return the CREATED_BY
     */
    public String getCreatedBy() {
        return (String) getAttributeInternal(CREATEDBY);
    }

    /**
     * Gets the attribute value for CREATION_DATE using the alias name CreationDate.
     * @return the CREATION_DATE
     */
    public Timestamp getCreationDate() {
        return (Timestamp) getAttributeInternal(CREATIONDATE);
    }

    /**
     * Gets the attribute value for ENTRY_DATE using the alias name EntryDate.
     * @return the ENTRY_DATE
     */
    public Date getEntryDate() {
        return (Date) getAttributeInternal(ENTRYDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for ENTRY_DATE using the alias name EntryDate.
     * @param value value to set the ENTRY_DATE
     */
    public void setEntryDate(Date value) {
        setAttributeInternal(ENTRYDATE, value);
    }

    /**
     * Gets the attribute value for ENTRY_ID using the alias name EntryId.
     * @return the ENTRY_ID
     */
    public Number getEntryId() {
        if(getAttributeInternal(ENTRYID)==null){
                BigDecimal id = oracle.jbo.server.uniqueid.UniqueIdHelper.getNextId();
                Number value=new Number(id.intValue());
                setAttributeInternal(ENTRYID, value);  
            }
        return (Number) getAttributeInternal(ENTRYID);
    }

    /**
     * Sets <code>value</code> as attribute value for ENTRY_ID using the alias name EntryId.
     * @param value value to set the ENTRY_ID
     */
    public void setEntryId(Number value) {
        setAttributeInternal(ENTRYID, value);
    }

    /**
     * Gets the attribute value for ENTRY_NO using the alias name EntryNo.
     * @return the ENTRY_NO
     */
    public String getEntryNo() {
        return (String) getAttributeInternal(ENTRYNO);
    }

    /**
     * Sets <code>value</code> as attribute value for ENTRY_NO using the alias name EntryNo.
     * @param value value to set the ENTRY_NO
     */
    public void setEntryNo(String value) {
        setAttributeInternal(ENTRYNO, value);
    }

    /**
     * Gets the attribute value for LAST_UPDATE_DATE using the alias name LastUpdateDate.
     * @return the LAST_UPDATE_DATE
     */
    public Timestamp getLastUpdateDate() {
        return (Timestamp) getAttributeInternal(LASTUPDATEDATE);
    }

    /**
     * Gets the attribute value for LAST_UPDATED_BY using the alias name LastUpdatedBy.
     * @return the LAST_UPDATED_BY
     */
    public String getLastUpdatedBy() {
        return (String) getAttributeInternal(LASTUPDATEDBY);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Number getObjectVersionNumber() {
        return (Number) getAttributeInternal(OBJECTVERSIONNUMBER);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber(Number value) {
        setAttributeInternal(OBJECTVERSIONNUMBER, value);
    }

    /**
     * Gets the attribute value for PARTY_CD using the alias name PartyCd.
     * @return the PARTY_CD
     */
    public String getPartyCd() {
        return (String) getAttributeInternal(PARTYCD);
    }

    /**
     * Sets <code>value</code> as attribute value for PARTY_CD using the alias name PartyCd.
     * @param value value to set the PARTY_CD
     */
    public void setPartyCd(String value) {
        setAttributeInternal(PARTYCD, value);
    }

    /**
     * Gets the attribute value for the calculated attribute EditTrans.
     * @return the EditTrans
     */
    public Integer getEditTrans() {
        byte entityState = this.getEntity(0).getEntityState();
               return new Integer(entityState);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute EditTrans.
     * @param value value to set the  EditTrans
     */
    public void setEditTrans(Integer value) {
        setAttributeInternal(EDITTRANS, value);
    }

    /**
     * Gets the attribute value for UNIT_CD using the alias name UnitCd.
     * @return the UNIT_CD
     */
    public String getUnitCd() {
        return (String) getAttributeInternal(UNITCD);
    }

    /**
     * Sets <code>value</code> as attribute value for UNIT_CD using the alias name UnitCd.
     * @param value value to set the UNIT_CD
     */
    public void setUnitCd(String value) {
        setAttributeInternal(UNITCD, value);
    }

    /**
     * Gets the attribute value for NAME using the alias name Name.
     * @return the NAME
     */
    public String getName() {
        return (String) getAttributeInternal(NAME);
    }

    /**
     * Sets <code>value</code> as attribute value for NAME using the alias name Name.
     * @param value value to set the NAME
     */
    public void setName(String value) {
        setAttributeInternal(NAME, value);
    }

    /**
     * Gets the attribute value for CODE using the alias name Code.
     * @return the CODE
     */
    public String getCode() {
        return (String) getAttributeInternal(CODE);
    }

    /**
     * Sets <code>value</code> as attribute value for CODE using the alias name Code.
     * @param value value to set the CODE
     */
    public void setCode(String value) {
        setAttributeInternal(CODE, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber1.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber1() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER1);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber1.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber1(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER1, value);
    }

    /**
     * Gets the attribute value for NAME using the alias name Name1.
     * @return the NAME
     */
    public String getName1() {
        return (String) getAttributeInternal(NAME1);
    }

    /**
     * Sets <code>value</code> as attribute value for NAME using the alias name Name1.
     * @param value value to set the NAME
     */
    public void setName1(String value) {
        setAttributeInternal(NAME1, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber2.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber2() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER2);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber2.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber2(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER2, value);
    }

    /**
     * Gets the attribute value for VENDOR_ID using the alias name VendorId.
     * @return the VENDOR_ID
     */
    public Long getVendorId() {
        return (Long) getAttributeInternal(VENDORID);
    }

    /**
     * Sets <code>value</code> as attribute value for VENDOR_ID using the alias name VendorId.
     * @param value value to set the VENDOR_ID
     */
    public void setVendorId(Long value) {
        setAttributeInternal(VENDORID, value);
    }

    /**
     * Gets the attribute value for AMD_NO using the alias name AmdNo.
     * @return the AMD_NO
     */
    public Number getAmdNo() {
        return (Number) getAttributeInternal(AMDNO);
    }

    /**
     * Sets <code>value</code> as attribute value for AMD_NO using the alias name AmdNo.
     * @param value value to set the AMD_NO
     */
    public void setAmdNo(Number value) {
        setAttributeInternal(AMDNO, value);
    }

    /**
     * Gets the attribute value for AMD_DATE using the alias name AmdDate.
     * @return the AMD_DATE
     */
    public Date getAmdDate() {
        return (Date) getAttributeInternal(AMDDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for AMD_DATE using the alias name AmdDate.
     * @param value value to set the AMD_DATE
     */
    public void setAmdDate(Date value) {
        setAttributeInternal(AMDDATE, value);
    }

    /**
     * Gets the attribute value for NOTE using the alias name Note.
     * @return the NOTE
     */
    public String getNote() {
        return (String) getAttributeInternal(NOTE);
    }

    /**
     * Sets <code>value</code> as attribute value for NOTE using the alias name Note.
     * @param value value to set the NOTE
     */
    public void setNote(String value) {
        setAttributeInternal(NOTE, value);
    }

    /**
     * Gets the associated <code>RowIterator</code> using master-detail link DailyDespatchDetailVO.
     */
    public RowIterator getDailyDespatchDetailVO() {
        return (RowIterator) getAttributeInternal(DAILYDESPATCHDETAILVO);
    }

    /**
     * Gets the view accessor <code>RowSet</code> UnitVO1.
     */
    public RowSet getUnitVO1() {
        return (RowSet) getAttributeInternal(UNITVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> CustomerVVO1.
     */
    public RowSet getCustomerVVO1() {
        return (RowSet) getAttributeInternal(CUSTOMERVVO1);
    }

    /**
     * getAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param attrDef the attribute

     * @return the attribute value
     * @throws Exception
     */
    protected Object getAttrInvokeAccessor(int index, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            return AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].get(this);
        }
        return super.getAttrInvokeAccessor(index, attrDef);
    }

    /**
     * setAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param value the value to assign to the attribute
     * @param attrDef the attribute

     * @throws Exception
     */
    protected void setAttrInvokeAccessor(int index, Object value, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].put(this, value);
            return;
        }
        super.setAttrInvokeAccessor(index, value, attrDef);
    }
}

