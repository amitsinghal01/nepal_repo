package terms.mkt.setup.ui.bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.validator.ValidatorException;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputFile;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

import org.apache.myfaces.trinidad.model.UploadedFile;

import terms.mkt.setup.model.view.CustomerMasterVORowImpl;
import terms.mkt.setup.model.view.CustomerRegdAddressVORowImpl;

public class CustomerMasterBean {
    private RichInputFile uploadFileBind;
    private RichOutputText filenNmPath;
    private RichInputText partyCodeBinding;
    private RichInputText unitNameBinding;
    private RichOutputText editTransBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichButton detailGeneralCreateBinding;
    private RichButton detailGeneralDeleteBinding;
    private RichButton detailContactCreateBinding;
    private RichButton detailContactDeleteBinding;
    private RichButton detailReferenceDeleteBinding;
    private RichButton detailReferenceCreateBinding;
    private RichButton detailAttachmentsCreateBinding;
    private RichButton detailAttachmentsDeleteBinding;
    private RichInputComboboxListOfValues unitCode;
    private RichInputText headerUnitNameBinding;
    private RichSelectOneChoice partyTypeBinding;
    private RichInputText currencyDescriptionBinding;
    private RichInputComboboxListOfValues detailUnitCodeBinding;
    private RichInputText addressCityCodeBinding;
    private RichInputText addressStateBinding;
    private RichInputText addressCountryBinding;
    private RichInputText creditDaysBinding;
    private RichShowDetailItem generalTabBinding;
    private RichShowDetailItem commercialTabBinding;
    private RichShowDetailItem addressTabBinding;
    private RichShowDetailItem contactTabBinding;
    private RichShowDetailItem referenceTabBinding;
    private RichShowDetailItem otherTabBinding;
    private RichShowDetailItem attachementTabBinding;
    private RichTable customerUnitTableBinding;
    private RichTable customerContactTableBinding;
    private RichTable customerItemTableBinding;
    private RichTable customerSuppTableBinding;
    private RichTable deleteCustomerCertifDialogDL;
    private RichTable docAttachTableBinding;
    private RichInputText partyNameBinding;
    private RichButton genDeleteButton;
    private RichButton contCreatebutton;
    private RichButton contDeleteButton;
    private RichButton refCreateButton;
    private RichButton refDeleteButton;
    private RichButton attaCreateButton;
    private RichButton attaDeleteButton;
    private RichButton genCreateButtonBinding;
    private RichInputDate registeredDateBinding;
    private RichInputComboboxListOfValues cityBinding;
    private RichInputComboboxListOfValues appByBinding;
    private RichInputText gtRegBinding;
    private RichInputText stategstbinding;
    private RichInputText pinCodeBinding;
    private RichInputText panNoBinding;
    private RichInputText transitPeriodBinding;
    private RichInputText contactPersonBinding;
    private RichSelectOneChoice custTypeBinding;
    private RichSelectOneChoice regFlagBinding;
    private RichSelectOneChoice vendFlag;
    private RichInputText partyNameTransBinding;


    public CustomerMasterBean() {
    }


    public void addCustomerUnitAL(ActionEvent actionEvent) {

        ADFUtils.findOperation("CreateInsert").execute();
        ADFUtils.findOperation("setCustomerCode").execute();
    }

    public void addCustomerContactAL(ActionEvent actionEvent) {

        ADFUtils.findOperation("CreateInsert1").execute();
        ADFUtils.findOperation("setCustomerCode").execute();
    }

    public void addCustomerSupplierAL(ActionEvent actionEvent) {

        ADFUtils.findOperation("CreateInsert2").execute();
        ADFUtils.findOperation("setCustomerCode").execute();

    }

    public void addCustomerItemAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert5").execute();
        ADFUtils.findOperation("setCustomerCode").execute();
    }

    //    public void addCustomerCertifiAL(ActionEvent actionEvent) {
    //
    //        ADFUtils.findOperation("CreateInsert3").execute();
    //        ADFUtils.findOperation("setCustomerCode").execute();
    //    }


    public void setUploadFileBind(RichInputFile uploadFileBind) {
        this.uploadFileBind = uploadFileBind;
    }

    public RichInputFile getUploadFileBind() {
        return uploadFileBind;
    }

    InputStream inputStream = null;
    InputStream SaveInputStream = null;
    private UploadedFile PhotoFile = null;
    private String AbsoluteFilePath = "";
    String imagePath = "";
    Boolean check = false;


    public void uploadFileVCE(ValueChangeEvent vce) {
        if (vce.getNewValue() != null) {
            setPhotoFile((UploadedFile) vce.getNewValue());
            UploadedFile myfile = (UploadedFile) vce.getNewValue();
            String path = null;

            if (myfile == null) {

            } else {
                File dir = new File("/tmp/pht");
                File savedir = new File("/home/beta12/pics");
                if (!dir.exists()) {
                    try {
                        dir.mkdir();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (!savedir.exists()) {
                    try {
                        savedir.mkdir();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                // All uploaded files will be stored in below path
                path = "/home/beta12/pics/" + myfile.getFilename();
                //                Imagepath=path;
                File f = new File(path);
                if (!f.exists()) {
                    try {
                        f.createNewFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                inputStream = null;
                try {

                    inputStream = myfile.getInputStream();
                    SaveInputStream = myfile.getInputStream();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                check = true;
                //                System.out.println("check in bean" + check);
                Imagepath = uploadFile(PhotoFile, "/tmp/pht/" + PhotoFile.getFilename());

            }
        }
    }


    private String uploadFile(UploadedFile file, String path) {

        UploadedFile myfile = file;

        try {

            FileOutputStream out = new FileOutputStream(path);
            byte[] buffer = new byte[8192];
            int bytesRead = 0;
            while ((bytesRead = inputStream.read(buffer, 0, 8192)) != -1) {
                out.write(buffer, 0, bytesRead);
            }
            out.flush();
            out.close();
        } catch (Exception ex) {
            // handle exception
            ex.printStackTrace();
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
            }
        }


        //Returns the path where file is stored
        return path;
    }


    private String SaveuploadFile(UploadedFile file, String path) {

        UploadedFile myfile = file;

        try {

            FileOutputStream out = new FileOutputStream(path);
            byte[] buffer = new byte[8192];
            int bytesRead = 0;
            while ((bytesRead = SaveInputStream.read(buffer, 0, 8192)) != -1) {
                out.write(buffer, 0, bytesRead);
            }
            out.flush();
            out.close();
        } catch (Exception ex) {
            // handle exception
            ex.printStackTrace();
        } finally {
            try {
                SaveInputStream.close();
            } catch (IOException e) {
            }
        }
        return path;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getImagePath() {
        return imagePath;
    }

    String Imagepath = "";

    public void saveVendormasterAL(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedBy("CustomerMasterVO1Iterator","LastUpdatedBy");
        if (requiredAttributes().equals("Y")) {
            DCIteratorBinding Dcite = ADFUtils.findIterator("CustomerMasterVO1Iterator");
            DCIteratorBinding Dcite1 = ADFUtils.findIterator("CustomerRegdAddressVO1Iterator");
            CustomerMasterVORowImpl row = (CustomerMasterVORowImpl) Dcite.getCurrentRow();
            CustomerRegdAddressVORowImpl row1 = (CustomerRegdAddressVORowImpl) Dcite1.getCurrentRow();
       //     System.out.println("gtRegBinding.getValue()+" + row.getGstRegNo() + "stategstbinding.getValue()" +
         //                      row1.getStateGstCode());
            String CustType = (String) (custTypeBinding.getValue() != null ? custTypeBinding.getValue() : "OE");
            String RegFLag = (String) (regFlagBinding.getValue() != null ? regFlagBinding.getValue() : "R");
            String venFlag = (String) (vendFlag.getValue() != null ? vendFlag.getValue() : "I");
            System.out.println("CustType----->" + CustType + "regFlag" + RegFLag + "venflag" + venFlag);
            if (CustType.equalsIgnoreCase("OE") && (RegFLag.equalsIgnoreCase("R") && venFlag.equalsIgnoreCase("I"))) {
               // if (row.getGstRegNo() != null && !row.getGstRegNo().equals(".") && row1.getCity() != null) {
                  //  System.out.println("iNSIDEgtRegBinding.getValue()+" + row.getGstRegNo() +
                   //                    "stategstbinding.getValue()" + row1.getStateGstCode());
                    if (row1.getStateGstCode() != null && row1.getCity() != null) {
                        //if (row.getGstRegNo().substring(0, 2).equals(row1.getStateGstCode().substring(0, 2))) {
                            System.out.println("In save method");
                            OperationBinding op = ADFUtils.findOperation("genarateCustomerCode");

                            op.execute();

                            System.out.println("Get Result is====> " + op.getResult());


                            if (op.getResult() != null) {
                                try {

                                    check = false;
                                    String path;
                                    if (getPhotoFile() != null) {
                                        path = "/home/beta12/pics/" + PhotoFile.getFilename();
                                        Imagepath = SaveuploadFile(PhotoFile, path);
                                        //        System.out.println("path " + Imagepath);
                                        OperationBinding ob = ADFUtils.findOperation("AddImagePath");
                                        ob.getParamsMap().put("ImagePath", Imagepath);
                                        ob.execute();
                                    }


                                    File directory = new File("/tmp/pht");
                                    //get all the files from a directory
                                    File[] fList = directory.listFiles();
                                    if (fList != null) {
                                        for (File file : fList) {
                                            //Delete all previously uploaded files
                                            // if (!"NoImage.png".equalsIgnoreCase(file.getName())) {
                                            file.delete();
                                            //}


                                        }
                                    }
                                } catch (Exception ee) {
                                    System.out.println("Error in Path and Directory");
                                }
                                //  System.out.println("##Before Commit operation##");
                                ADFUtils.findOperation("Commit").execute();


                                if (op.getErrors().isEmpty() || op.getErrors() == null) {
                                    if (op.getResult().equals("N")) {
                                        FacesMessage Message = new FacesMessage("Record Updated Successfully");
                                        Message.setSeverity(FacesMessage.SEVERITY_INFO);
                                        FacesContext fc = FacesContext.getCurrentInstance();
                                        fc.addMessage(null, Message);

                                    } else {
                                        FacesMessage Message =
                                            new FacesMessage("Record Saved Successfully.New Customer Code is " +
                                                             getPartyCodeBinding().getValue());
                                        Message.setSeverity(FacesMessage.SEVERITY_INFO);
                                        FacesContext fc = FacesContext.getCurrentInstance();
                                        fc.addMessage(null, Message);
                                    }
                                }

                            }
                       // } else {
                       //     ADFUtils.showMessage("First Two Characters Of GST Regd. No. Is Equals First Two Characters Of State GST Code",
                        //                         0);
                       // }

                    } else {
                        ADFUtils.showMessage("Either City or State Gst Code is not found", 0);
                    }
//                } else {
//                  //  ADFUtils.showMessage("GST Reg No in General tab is required.", 0);
//                }

            } else {
                System.out.println("CustEXP");
                OperationBinding op = ADFUtils.findOperation("genarateCustomerCode");

                op.execute();

                System.out.println("Get Result is====> " + op.getResult());


                if (op.getResult() != null) {
                    try {

                        check = false;
                        String path;
                        if (getPhotoFile() != null) {
                            path = "/home/beta12/pics/" + PhotoFile.getFilename();
                            Imagepath = SaveuploadFile(PhotoFile, path);
                            //        System.out.println("path " + Imagepath);
                            OperationBinding ob = ADFUtils.findOperation("AddImagePath");
                            ob.getParamsMap().put("ImagePath", Imagepath);
                            ob.execute();
                        }


                        File directory = new File("/tmp/pht");
                        //get all the files from a directory
                        File[] fList = directory.listFiles();
                        if (fList != null) {
                            for (File file : fList) {
                                //Delete all previously uploaded files
                                // if (!"NoImage.png".equalsIgnoreCase(file.getName())) {
                                file.delete();
                                //}


                            }
                        }
                    } catch (Exception ee) {
                        System.out.println("Error in Path and Directory");
                    }
                    //  System.out.println("##Before Commit operation##");
                    ADFUtils.findOperation("Commit").execute();


                    if (op.getErrors().isEmpty() || op.getErrors() == null) {
                        if (op.getResult().equals("N")) {
                            FacesMessage Message = new FacesMessage("Record Updated Successfully");
                            Message.setSeverity(FacesMessage.SEVERITY_INFO);
                            FacesContext fc = FacesContext.getCurrentInstance();
                            fc.addMessage(null, Message);

                        } else {
                            FacesMessage Message =
                                new FacesMessage("Record Saved Successfully.New Customer Code is " +
                                                 getPartyCodeBinding().getValue());
                            Message.setSeverity(FacesMessage.SEVERITY_INFO);
                            FacesContext fc = FacesContext.getCurrentInstance();
                            fc.addMessage(null, Message);
                        }
                    }

                }
            }
        }
    }

    public void setCheck(Boolean check) {
        this.check = check;
    }

    public Boolean getCheck() {
        return check;
    }

    public void setPhotoFile(UploadedFile PhotoFile) {
        this.PhotoFile = PhotoFile;
    }

    public UploadedFile getPhotoFile() {
        return PhotoFile;
    }


    public void downloadDocment(FacesContext facesContext, OutputStream outputStream) {
        if (filenNmPath.getValue() != null) {
            //Read file from particular path, path bind is binding of table field that contains path
            File filed = new File(filenNmPath.getValue().toString());
            FileInputStream fis;
            byte[] b;
            try {
                fis = new FileInputStream(filed);

                int n;
                while ((n = fis.available()) > 0) {
                    b = new byte[n];
                    int result = fis.read(b);
                    outputStream.write(b, 0, b.length);
                    if (result == -1)
                        break;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    public void setFilenNmPath(RichOutputText filenNmPath) {
        this.filenNmPath = filenNmPath;
    }

    public RichOutputText getFilenNmPath() {
        return filenNmPath;
    }

    public void pageCreateInseart() {
        ADFUtils.findOperation("CreateInsert7").execute();
        ADFUtils.findOperation("CreateInsert5").execute();
        ADFUtils.findOperation("CreateInsert6").execute();
    }

    //    public String resolvEl(String data)
    //    {
    //                      FacesContext fc = FacesContext.getCurrentInstance();
    //                      Application app = fc.getApplication();
    //                      ExpressionFactory elFactory = app.getExpressionFactory();
    //                      ELContext elContext = fc.getELContext();
    //                      ValueExpression valueExp = elFactory.createValueExpression(elContext, data, Object.class);
    //                      String Message=valueExp.getValue(elContext).toString();
    //                      return Message;
    //    }

    public void setPartyCodeBinding(RichInputText partyCodeBinding) {
        this.partyCodeBinding = partyCodeBinding;
    }

    public RichInputText getPartyCodeBinding() {
        return partyCodeBinding;
    }


    public void setUnitNameBinding(RichInputText unitNameBinding) {
        this.unitNameBinding = unitNameBinding;
    }

    public RichInputText getUnitNameBinding() {
        return unitNameBinding;
    }

    public void setEditTransBinding(RichOutputText editTransBinding) {
        this.editTransBinding = editTransBinding;


    }

    public RichOutputText getEditTransBinding() {
        cevmodecheck();
        return editTransBinding;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setDetailGeneralCreateBinding(RichButton detailGeneralCreateBinding) {
        this.detailGeneralCreateBinding = detailGeneralCreateBinding;
    }

    public RichButton getDetailGeneralCreateBinding() {
        return detailGeneralCreateBinding;
    }

    public void setDetailGeneralDeleteBinding(RichButton detailGeneralDeleteBinding) {
        this.detailGeneralDeleteBinding = detailGeneralDeleteBinding;
    }

    public RichButton getDetailGeneralDeleteBinding() {
        return detailGeneralDeleteBinding;
    }

    public void setDetailContactCreateBinding(RichButton detailContactCreateBinding) {
        this.detailContactCreateBinding = detailContactCreateBinding;
    }

    public RichButton getDetailContactCreateBinding() {
        return detailContactCreateBinding;
    }

    public void setDetailContactDeleteBinding(RichButton detailContactDeleteBinding) {
        this.detailContactDeleteBinding = detailContactDeleteBinding;
    }

    public RichButton getDetailContactDeleteBinding() {
        return detailContactDeleteBinding;
    }

    public void setDetailReferenceDeleteBinding(RichButton detailReferenceDeleteBinding) {
        this.detailReferenceDeleteBinding = detailReferenceDeleteBinding;
    }

    public RichButton getDetailReferenceDeleteBinding() {
        return detailReferenceDeleteBinding;
    }

    public void setDetailReferenceCreateBinding(RichButton detailReferenceCreateBinding) {
        this.detailReferenceCreateBinding = detailReferenceCreateBinding;
    }

    public RichButton getDetailReferenceCreateBinding() {
        return detailReferenceCreateBinding;
    }

    public void setDetailAttachmentsCreateBinding(RichButton detailAttachmentsCreateBinding) {
        this.detailAttachmentsCreateBinding = detailAttachmentsCreateBinding;
    }

    public RichButton getDetailAttachmentsCreateBinding() {
        return detailAttachmentsCreateBinding;
    }

    public void setDetailAttachmentsDeleteBinding(RichButton detailAttachmentsDeleteBinding) {
        this.detailAttachmentsDeleteBinding = detailAttachmentsDeleteBinding;
    }

    public RichButton getDetailAttachmentsDeleteBinding() {
        return detailAttachmentsDeleteBinding;
    }

    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }


    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }

    //Set Fields and Button disable.
    public void cevModeDisableComponent(String mode) {
        //            FacesContext fctx = FacesContext.getCurrentInstance();
        //            ELContext elctx = fctx.getELContext();
        //            Application jsfApp = fctx.getApplication();
        //            //create a ValueExpression that points to the ADF binding layer
        //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
        //            //
        //            ValueExpression valueExpr = exprFactory.createValueExpression(
        //                                         elctx,
        //                                         "#{pageFlowScope.mode=='E'}",
        //                                          Object.class
        //                                         );
        //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
        if (mode.equals("E")) {
            getStategstbinding().setDisabled(true);
            getPartyCodeBinding().setDisabled(true);
            getPartyNameBinding().setDisabled(false);
            getPartyTypeBinding().setDisabled(true);
            getHeaderUnitNameBinding().setDisabled(true);
            //  getUnitCode().setDisabled(true);
            getCurrencyDescriptionBinding().setDisabled(true);
            //            getDetailUnitCodeBinding().setDisabled(true);
            getUnitNameBinding().setDisabled(true);
            getAddressCountryBinding().setDisabled(true);
            getAddressStateBinding().setDisabled(true);
            getAddressCityCodeBinding().setDisabled(true);
            getCreditDaysBinding().setDisabled(true);
            getGenCreateButtonBinding().setDisabled(false);
            getGenDeleteButton().setDisabled(false);
            getRegisteredDateBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);

            getRefCreateButton().setDisabled(false);
            //       getRefDeleteButton().setDisabled(false);
            getContCreatebutton().setDisabled(false);
            //       getContDeleteButton().setDisabled(false);
            getAttaCreateButton().setDisabled(false);
            //        getAttaDeleteButton().setDisabled(false);


        } else if (mode.equals("C")) {
            getStategstbinding().setDisabled(true);

            getAppByBinding().setDisabled(true);
            getPartyCodeBinding().setDisabled(true);
            getPartyTypeBinding().setDisabled(true);
            getHeaderUnitNameBinding().setDisabled(true);
            getUnitCode().setDisabled(true);
            getCurrencyDescriptionBinding().setDisabled(true);
            getUnitNameBinding().setDisabled(true);
            getGenCreateButtonBinding().setDisabled(false);
            getAddressCountryBinding().setDisabled(true);
            getAddressStateBinding().setDisabled(true);
            getAddressCityCodeBinding().setDisabled(true);
            getCreditDaysBinding().setDisabled(true);
            getPinCodeBinding().setDisabled(true);
            //    getGenDeleteButton().setDisabled(false);

        } else if (mode.equals("V")) {

            getPinCodeBinding().setDisabled(true);
            getGenDeleteButton().setDisabled(true);
            getGenCreateButtonBinding().setDisabled(true);
            getRefCreateButton().setDisabled(true);
            //   getRefDeleteButton().setDisabled(true);
            getContCreatebutton().setDisabled(true);
            //  getContDeleteButton().setDisabled(true);
            getAttaCreateButton().setDisabled(true);
            //  getAttaDeleteButton().setDisabled(true);

            getGeneralTabBinding().setDisabled(false);
            getCommercialTabBinding().setDisabled(false);
            getAddressTabBinding().setDisabled(false);
            getContactTabBinding().setDisabled(false);
            getReferenceTabBinding().setDisabled(false);
            getOtherTabBinding().setDisabled(false);
            getAttachementTabBinding().setDisabled(false);


        }

    }


    public void setUnitCode(RichInputComboboxListOfValues unitCode) {
        this.unitCode = unitCode;
    }

    public RichInputComboboxListOfValues getUnitCode() {
        return unitCode;
    }

    public void setHeaderUnitNameBinding(RichInputText headerUnitNameBinding) {
        this.headerUnitNameBinding = headerUnitNameBinding;
    }

    public RichInputText getHeaderUnitNameBinding() {
        return headerUnitNameBinding;
    }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setPartyTypeBinding(RichSelectOneChoice partyTypeBinding) {
        this.partyTypeBinding = partyTypeBinding;
    }

    public RichSelectOneChoice getPartyTypeBinding() {
        return partyTypeBinding;
    }

    public void setCurrencyDescriptionBinding(RichInputText currencyDescriptionBinding) {
        this.currencyDescriptionBinding = currencyDescriptionBinding;
    }

    public RichInputText getCurrencyDescriptionBinding() {
        return currencyDescriptionBinding;
    }

    public void setDetailUnitCodeBinding(RichInputComboboxListOfValues detailUnitCodeBinding) {
        this.detailUnitCodeBinding = detailUnitCodeBinding;
    }

    public RichInputComboboxListOfValues getDetailUnitCodeBinding() {
        return detailUnitCodeBinding;
    }

    public void setAddressCityCodeBinding(RichInputText addressCityCodeBinding) {
        this.addressCityCodeBinding = addressCityCodeBinding;
    }

    public RichInputText getAddressCityCodeBinding() {
        return addressCityCodeBinding;
    }

    public void setAddressStateBinding(RichInputText addressStateBinding) {
        this.addressStateBinding = addressStateBinding;
    }

    public RichInputText getAddressStateBinding() {
        return addressStateBinding;
    }

    public void setAddressCountryBinding(RichInputText addressCountryBinding) {
        this.addressCountryBinding = addressCountryBinding;
    }

    public RichInputText getAddressCountryBinding() {
        return addressCountryBinding;
    }

    public void setCreditDaysBinding(RichInputText creditDaysBinding) {
        this.creditDaysBinding = creditDaysBinding;
    }

    public RichInputText getCreditDaysBinding() {
        return creditDaysBinding;
    }

    public void setGeneralTabBinding(RichShowDetailItem generalTabBinding) {
        this.generalTabBinding = generalTabBinding;
    }

    public RichShowDetailItem getGeneralTabBinding() {
        return generalTabBinding;
    }

    public void setCommercialTabBinding(RichShowDetailItem commercialTabBinding) {
        this.commercialTabBinding = commercialTabBinding;
    }

    public RichShowDetailItem getCommercialTabBinding() {
        return commercialTabBinding;
    }

    public void setAddressTabBinding(RichShowDetailItem addressTabBinding) {
        this.addressTabBinding = addressTabBinding;
    }

    public RichShowDetailItem getAddressTabBinding() {
        return addressTabBinding;
    }

    public void setContactTabBinding(RichShowDetailItem contactTabBinding) {
        this.contactTabBinding = contactTabBinding;
    }

    public RichShowDetailItem getContactTabBinding() {
        return contactTabBinding;
    }

    public void setReferenceTabBinding(RichShowDetailItem referenceTabBinding) {
        this.referenceTabBinding = referenceTabBinding;
    }

    public RichShowDetailItem getReferenceTabBinding() {
        return referenceTabBinding;
    }

    public void setOtherTabBinding(RichShowDetailItem otherTabBinding) {
        this.otherTabBinding = otherTabBinding;
    }

    public RichShowDetailItem getOtherTabBinding() {
        return otherTabBinding;
    }

    public void setAttachementTabBinding(RichShowDetailItem attachementTabBinding) {
        this.attachementTabBinding = attachementTabBinding;
    }

    public RichShowDetailItem getAttachementTabBinding() {
        return attachementTabBinding;
    }


    public void deleteCustomerUnitDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }

        AdfFacesContext.getCurrentInstance().addPartialTarget(customerUnitTableBinding);
    }

    public void setCustomerUnitTableBinding(RichTable customerUnitTableBinding) {
        this.customerUnitTableBinding = customerUnitTableBinding;
    }

    public RichTable getCustomerUnitTableBinding() {
        return customerUnitTableBinding;
    }

    public void deleteCustomerContactDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete1").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }

        AdfFacesContext.getCurrentInstance().addPartialTarget(customerContactTableBinding);
    }

    public void setCustomerContactTableBinding(RichTable customerContactTableBinding) {
        this.customerContactTableBinding = customerContactTableBinding;
    }

    public RichTable getCustomerContactTableBinding() {
        return customerContactTableBinding;
    }

    public void deleteCustomerVendorDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete5").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(customerItemTableBinding);
    }

    public void setCustomerItemTableBinding(RichTable customerItemTableBinding) {
        this.customerItemTableBinding = customerItemTableBinding;
    }

    public RichTable getCustomerItemTableBinding() {
        return customerItemTableBinding;
    }

    public void deleteCustomerSuppDetailDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete2").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }

        AdfFacesContext.getCurrentInstance().addPartialTarget(customerSuppTableBinding);
    }

    public void setCustomerSuppTableBinding(RichTable customerSuppTableBinding) {
        this.customerSuppTableBinding = customerSuppTableBinding;
    }

    public RichTable getCustomerSuppTableBinding() {
        return customerSuppTableBinding;
    }

    public void deleteCustomerCertifDialogDL(DialogEvent dialogEvent) {

        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete3").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(deleteCustomerCertifDialogDL);
    }

    public void setDeleteCustomerCertifDialogDL(RichTable deleteCustomerCertifDialogDL) {
        this.deleteCustomerCertifDialogDL = deleteCustomerCertifDialogDL;
    }

    public RichTable getDeleteCustomerCertifDialogDL() {
        return deleteCustomerCertifDialogDL;
    }

    public void deleteCustomerDocAttachDialogDL(DialogEvent dialogEvent) {

        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete4").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(docAttachTableBinding);
    }

    public void setDocAttachTableBinding(RichTable docAttachTableBinding) {
        this.docAttachTableBinding = docAttachTableBinding;
    }

    public RichTable getDocAttachTableBinding() {
        return docAttachTableBinding;
    }

    public void setPartyNameBinding(RichInputText partyNameBinding) {
        this.partyNameBinding = partyNameBinding;
    }

    public RichInputText getPartyNameBinding() {
        return partyNameBinding;
    }

    public void setGenDeleteButton(RichButton genDeleteButton) {
        this.genDeleteButton = genDeleteButton;
    }

    public RichButton getGenDeleteButton() {
        return genDeleteButton;
    }

    public void setContCreatebutton(RichButton contCreatebutton) {
        this.contCreatebutton = contCreatebutton;
    }

    public RichButton getContCreatebutton() {
        return contCreatebutton;
    }

    public void setContDeleteButton(RichButton contDeleteButton) {
        this.contDeleteButton = contDeleteButton;
    }

    public RichButton getContDeleteButton() {
        return contDeleteButton;
    }

    public void setRefCreateButton(RichButton refCreateButton) {
        this.refCreateButton = refCreateButton;
    }

    public RichButton getRefCreateButton() {
        return refCreateButton;
    }

    public void setRefDeleteButton(RichButton refDeleteButton) {
        this.refDeleteButton = refDeleteButton;
    }

    public RichButton getRefDeleteButton() {
        return refDeleteButton;
    }

    public void setAttaCreateButton(RichButton attaCreateButton) {
        this.attaCreateButton = attaCreateButton;
    }

    public RichButton getAttaCreateButton() {
        return attaCreateButton;
    }

    public void setAttaDeleteButton(RichButton attaDeleteButton) {
        this.attaDeleteButton = attaDeleteButton;
    }

    public RichButton getAttaDeleteButton() {
        return attaDeleteButton;
    }

    public void setGenCreateButtonBinding(RichButton genCreateButtonBinding) {
        this.genCreateButtonBinding = genCreateButtonBinding;
    }

    public RichButton getGenCreateButtonBinding() {
        return genCreateButtonBinding;
    }

    public void setRegisteredDateBinding(RichInputDate registeredDateBinding) {
        this.registeredDateBinding = registeredDateBinding;
    }

    public RichInputDate getRegisteredDateBinding() {
        return registeredDateBinding;
    }

    public void setCityBinding(RichInputComboboxListOfValues cityBinding) {
        this.cityBinding = cityBinding;
    }

    public RichInputComboboxListOfValues getCityBinding() {
        return cityBinding;
    }

    public void setAppByBinding(RichInputComboboxListOfValues appByBinding) {
        this.appByBinding = appByBinding;
    }

    public RichInputComboboxListOfValues getAppByBinding() {
        return appByBinding;
    }

    public void gstStateValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        String CustType = (String) (custTypeBinding.getValue() != null ? custTypeBinding.getValue() : "OE");
        String RegFLag = (String) (regFlagBinding.getValue() != null ? regFlagBinding.getValue() : "R");
        String venFlag = (String) (vendFlag.getValue() != null ? vendFlag.getValue() : "I");
        System.out.println("CustType----->" + CustType + "regFlag" + RegFLag + "venflag" + venFlag);
        if (object != null && gtRegBinding.getValue() != null && stategstbinding.getValue() != null) {
            if (CustType.equalsIgnoreCase("OE") && (RegFLag.equalsIgnoreCase("R") && venFlag.equalsIgnoreCase("I"))) {
                if (!stategstbinding.getValue().toString().substring(0,
                                                                     2).equals(gtRegBinding.getValue().toString().substring(0,
                                                                                                                            2)) &&
                    stategstbinding.getValue().toString().trim().length() >= 2) {
                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                  "First Two Characters Of GST Regd. No. Is Equals First Two Characters Of State GST Code.",
                                                                  null));
                }
            }
        }
    }

    public void setGtRegBinding(RichInputText gtRegBinding) {
        this.gtRegBinding = gtRegBinding;
    }

    public RichInputText getGtRegBinding() {
        return gtRegBinding;
    }

    public void setStategstbinding(RichInputText stategstbinding) {
        this.stategstbinding = stategstbinding;
    }

    public RichInputText getStategstbinding() {
        return stategstbinding;
    }

    public void gststateValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        String CustType = (String) (custTypeBinding.getValue() != null ? custTypeBinding.getValue() : "OE");
        String RegFLag = (String) (regFlagBinding.getValue() != null ? regFlagBinding.getValue() : "R");
        String venFlag = (String) (vendFlag.getValue() != null ? vendFlag.getValue() : "I");
        System.out.println("CustType----->" + CustType + "regFlag" + RegFLag + "venflag" + venFlag);
        if (object != null && gtRegBinding.getValue() != null && stategstbinding.getValue() != null) {
            if (CustType.equalsIgnoreCase("OE") && (RegFLag.equalsIgnoreCase("R") && venFlag.equalsIgnoreCase("I"))) {
                if (object != null && object.toString().trim().length() >= 2 && stategstbinding.getValue() != null) {
                    if (!object.toString().substring(0,
                                                     2).equals(stategstbinding.getValue().toString().substring(0, 2))) {
                        throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                      "First Two Characters Of GST Regd. No. Is Equals First Two Characters Of State GST Code.",
                                                                      null));
                    }
                }
            }
        }
    }

    public void setPinCodeBinding(RichInputText pinCodeBinding) {
        this.pinCodeBinding = pinCodeBinding;
    }

    public RichInputText getPinCodeBinding() {
        return pinCodeBinding;
    }

    public String SaveAndCloseAL() {
        if (requiredAttributes().equals("Y")) {
            DCIteratorBinding Dcite = ADFUtils.findIterator("CustomerMasterVO1Iterator");
            DCIteratorBinding Dcite1 = ADFUtils.findIterator("CustomerRegdAddressVO1Iterator");
            CustomerMasterVORowImpl row = (CustomerMasterVORowImpl) Dcite.getCurrentRow();
            CustomerRegdAddressVORowImpl row1 = (CustomerRegdAddressVORowImpl) Dcite1.getCurrentRow();
            //System.out.println("gtRegBinding.getValue()+" + row.getGstRegNo() + "stategstbinding.getValue()" +
                    //           row1.getStateGstCode());
            String CustType = (String) (custTypeBinding.getValue() != null ? custTypeBinding.getValue() : "OE");
            String RegFLag = (String) (regFlagBinding.getValue() != null ? regFlagBinding.getValue() : "R");
            String venFlag = (String) (vendFlag.getValue() != null ? vendFlag.getValue() : "I");
            System.out.println("CustType----->" + CustType + "regFlag" + RegFLag + "venflag" + venFlag);
            if (RegFLag.equalsIgnoreCase("R") && venFlag.equalsIgnoreCase("I") && CustType.equalsIgnoreCase("OE")) {
                //            if(CustType.equalsIgnoreCase("OE"))
                //            {
              //  if (row.getGstRegNo() != null && row1.getCity() != null) {
//                    System.out.println("iNSIDEgtRegBinding.getValue()+" + row.getGstRegNo() +
//                                       "stategstbinding.getValue()" + row1.getStateGstCode());
                    if (row1.getStateGstCode() != null && row1.getCity() != null) {
                       // if (row.getGstRegNo().substring(0, 2).equals(row1.getStateGstCode().substring(0, 2))) {
                            System.out.println("In save method");
                            OperationBinding op = ADFUtils.findOperation("genarateCustomerCode");

                            op.execute();

                            System.out.println("Get Result is====> " + op.getResult());


                            if (op.getResult() != null) {
                                try {

                                    check = false;
                                    String path;
                                    if (getPhotoFile() != null) {
                                        path = "/home/beta12/pics/" + PhotoFile.getFilename();
                                        Imagepath = SaveuploadFile(PhotoFile, path);
                                        //        System.out.println("path " + Imagepath);
                                        OperationBinding ob = ADFUtils.findOperation("AddImagePath");
                                        ob.getParamsMap().put("ImagePath", Imagepath);
                                        ob.execute();
                                    }


                                    File directory = new File("/tmp/pht");
                                    //get all the files from a directory
                                    File[] fList = directory.listFiles();
                                    if (fList != null) {
                                        for (File file : fList) {
                                            //Delete all previously uploaded files
                                            // if (!"NoImage.png".equalsIgnoreCase(file.getName())) {
                                            file.delete();
                                            //}








                                        }
                                    }
                                } catch (Exception ee) {
                                    System.out.println("Error in Path and Directory");
                                }
                                //  System.out.println("##Before Commit operation##");
                                ADFUtils.findOperation("Commit").execute();


                                if (op.getErrors().isEmpty() || op.getErrors() == null) {
                                    if (op.getResult().equals("N")) {
                                        FacesMessage Message = new FacesMessage("Record Updated Successfully");
                                        Message.setSeverity(FacesMessage.SEVERITY_INFO);
                                        FacesContext fc = FacesContext.getCurrentInstance();
                                        fc.addMessage(null, Message);

                                    }

                                    else {
                                        FacesMessage Message =
                                            new FacesMessage("Record Saved Successfully.New Customer Code is " +
                                                             getPartyCodeBinding().getValue());
                                        Message.setSeverity(FacesMessage.SEVERITY_INFO);
                                        FacesContext fc = FacesContext.getCurrentInstance();
                                        fc.addMessage(null, Message);

                                    }
                                    return "SaveAndClose";

                                }

                           }
//                        } else {
//                            ADFUtils.showMessage("First Two Characters Of GST Regd. No. Is Equals First Two Characters Of State GST Code",
//                                                 0);
//                            return null;
//                        }
                    } else {
                        ADFUtils.showMessage("Either City or State Gst Code is not found", 0);
                    }
//                } else {
//                    ADFUtils.showMessage("GST Reg No in General tab is required.", 0);
//                }
            }

            else {
                //         else if(CustType.equalsIgnoreCase("EXP"))
                //            {
                System.out.println("In save method");
                OperationBinding op = ADFUtils.findOperation("genarateCustomerCode");

                op.execute();

                System.out.println("Get Result is====> " + op.getResult());


                if (op.getResult() != null) {
                    try {

                        check = false;
                        String path;
                        if (getPhotoFile() != null) {
                            path = "/home/beta12/pics/" + PhotoFile.getFilename();
                            Imagepath = SaveuploadFile(PhotoFile, path);
                            //        System.out.println("path " + Imagepath);
                            OperationBinding ob = ADFUtils.findOperation("AddImagePath");
                            ob.getParamsMap().put("ImagePath", Imagepath);
                            ob.execute();
                        }
                        File directory = new File("/tmp/pht");
                        //get all the files from a directory
                        File[] fList = directory.listFiles();
                        if (fList != null) {
                            for (File file : fList) {
                                //Delete all previously uploaded files
                                // if (!"NoImage.png".equalsIgnoreCase(file.getName())) {
                                file.delete();
                                //}



                            }
                        }
                    } catch (Exception ee) {
                        System.out.println("Error in Path and Directory");
                    }
                    //  System.out.println("##Before Commit operation##");
                    ADFUtils.findOperation("Commit").execute();


                    if (op.getErrors().isEmpty() || op.getErrors() == null) {
                        if (op.getResult().equals("N")) {
                            FacesMessage Message = new FacesMessage("Record Updated Successfully");
                            Message.setSeverity(FacesMessage.SEVERITY_INFO);
                            FacesContext fc = FacesContext.getCurrentInstance();
                            fc.addMessage(null, Message);

                        }

                        else {
                            FacesMessage Message =
                                new FacesMessage("Record Saved Successfully.New Customer Code is " +
                                                 getPartyCodeBinding().getValue());
                            Message.setSeverity(FacesMessage.SEVERITY_INFO);
                            FacesContext fc = FacesContext.getCurrentInstance();
                            fc.addMessage(null, Message);

                        }
                        return "SaveAndClose";

                    }

                }
            }
        }
        return null;
    }

    public void approvedByVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        OperationBinding ob = ADFUtils.findOperation("checkApprovalStatus");
        ob.getParamsMap().put("formNm", "CM");
        ob.getParamsMap().put("authoLim", "AP");
        ob.getParamsMap().put("empcd", vce.getNewValue().toString());
        ob.execute();
        System.out.println(" Result after Method Call: " + vce.getNewValue().toString());
        if ((ob.getResult() != null && ob.getResult().equals("Y"))) {
            Row row = (Row) ADFUtils.evaluateEL("#{bindings.CustomerMasterVO1Iterator.currentRow}");
            row.setAttribute("ApprovedBy", null);
            ADFUtils.showMessage("You don't have approval authority.", 0);
        }
    }

    public String requiredAttributes() {
        DCIteratorBinding dci = ADFUtils.findIterator("CustomerUnitVO1Iterator");
        System.out.println("Party Name: " + partyNameBinding.getValue() + " Registered Date: " +
                           registeredDateBinding.getValue() + " Pan No: " + panNoBinding.getValue() + " Unit Code: " +
                           dci.getCurrentRow().getAttribute("UnitCode") + " Transit Period: " +
                           dci.getCurrentRow().getAttribute("TransitPeriod") + " Contact Person: " +
                           contactPersonBinding.getValue() + " City Binding: " + cityBinding.getValue());
        if (partyNameBinding.getValue() == null) {
            ADFUtils.showMessage("Party Name is required.", 0);
            return "N";
        } else if (registeredDateBinding.getValue() == null) {
            ADFUtils.showMessage("Reg. Date is required.", 0);
            return "N";
        } else if (panNoBinding.getValue() == null &&
                   ADFUtils.resolveExpression("#{bindings.RegFlag.inputValue}").equals("R") &&
                   ADFUtils.resolveExpression("#{bindings.CustType.inputValue}").equals("OE")) {
            ADFUtils.showMessage("PAN No is required in General tab.", 0);
            return "N";
        } 
//        else if (gtRegBinding.getValue() == null &&
//                   ADFUtils.resolveExpression("#{bindings.RegFlag.inputValue}").equals("R") &&
//                   ADFUtils.resolveExpression("#{bindings.CustType.inputValue}").equals("OE")) {
//            //ADFUtils.showMessage("Gst Reg No is required in General tab.", 0);
        //            return "N";
        //        }
    else if (dci.getCurrentRow().getAttribute("UnitCode") == null) {
            ADFUtils.showMessage("Unit Code is required in General tab.", 0);
            return "N";
        } else if (dci.getCurrentRow().getAttribute("TransitPeriod") == null) {
            ADFUtils.showMessage("Transit Period is required in General tab.", 0);
            return "N";
        } else if (contactPersonBinding.getValue() == null) {
            ADFUtils.showMessage("Contact Person is required in Address tab.", 0);
            return "N";
        } else if (cityBinding.getValue() == null) {
            ADFUtils.showMessage("City is required in Address tab.", 0);
            return "N";
        }
        if (partyNameBinding.getValue() != null && registeredDateBinding.getValue() != null &&
            dci.getCurrentRow().getAttribute("UnitCode") != null &&
            dci.getCurrentRow().getAttribute("TransitPeriod") != null && contactPersonBinding.getValue() != null &&
            cityBinding.getValue() != null) {
            return "Y";
        }
        return "N";
    }

    public void setPanNoBinding(RichInputText panNoBinding) {
        this.panNoBinding = panNoBinding;
    }

    public RichInputText getPanNoBinding() {
        return panNoBinding;
    }

    public void setTransitPeriodBinding(RichInputText transitPeriodBinding) {
        this.transitPeriodBinding = transitPeriodBinding;
    }

    public RichInputText getTransitPeriodBinding() {
        return transitPeriodBinding;
    }

    public void setContactPersonBinding(RichInputText contactPersonBinding) {
        this.contactPersonBinding = contactPersonBinding;
    }

    public RichInputText getContactPersonBinding() {
        return contactPersonBinding;
    }

    public void setCustTypeBinding(RichSelectOneChoice custTypeBinding) {
        this.custTypeBinding = custTypeBinding;
    }

    public RichSelectOneChoice getCustTypeBinding() {
        return custTypeBinding;
    }

    public void setRegFlagBinding(RichSelectOneChoice regFlagBinding) {
        this.regFlagBinding = regFlagBinding;
    }

    public RichSelectOneChoice getRegFlagBinding() {
        return regFlagBinding;
    }

    public void setVendFlag(RichSelectOneChoice vendFlag) {
        this.vendFlag = vendFlag;
    }

    public RichSelectOneChoice getVendFlag() {
        return vendFlag;
    }

    public void glCodeVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        String gl_code=(String)vce.getOldValue();
        System.out.println(" the old Gl Code is----"+gl_code+"---New value is--"+vce.getNewValue());
        if(vce.getOldValue()!=null && ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E"))
        {
            System.out.println("=----- In the Edit Mode------");

            OperationBinding ob = ADFUtils.findOperation("glCodeFilterInCustomer");
            ob.getParamsMap().put("glcode", vce.getOldValue());
            ob.execute();
            Row row=(Row)ADFUtils.evaluateEL("#{bindings.CustomerUnitVO1Iterator.currentRow}");
            System.out.println(" and the gl code is =====>>"+row.getAttribute("GlCd"));
                if((ob.getResult() !=null && ob.getResult().equals("Y")))
                {
                    
                    row.setAttribute("GlCd", null);
                    ADFUtils.showMessage("You can not change already mapped Gl Code.",0);
                    
                    if(row.getAttribute("GlCd")==null){
                        row.setAttribute("GlCd", gl_code);
                    }
                }
                else{
                    //System.out.println("hello------");
                }
                
        }
    }

    public void partyNameValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        String name = (String) object;
        String trnasName = (String) partyNameTransBinding.getValue();
        System.out.println("name"+name+" trnasName "+trnasName);
        //        System.out.println("Character "+name.substring(0,1)+" "+trnasName.substring(0, 1));
        if(trnasName!=null && name!=null){
        if(name.substring(0,1).equalsIgnoreCase(trnasName.substring(0, 1))){
            System.out.println("Equals");
            cevmodecheck();
            }
        else{
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                              "First Charcater should be same.",
                                                              null));
            }
        }

    }

    public void setPartyNameTransBinding(RichInputText partyNameTransBinding) {
        this.partyNameTransBinding = partyNameTransBinding;
    }

    public RichInputText getPartyNameTransBinding() {
        return partyNameTransBinding;
    }
}
