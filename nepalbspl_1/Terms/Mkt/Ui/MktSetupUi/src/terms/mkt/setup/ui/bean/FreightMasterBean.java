package terms.mkt.setup.ui.bean;

import javax.faces.application.FacesMessage;

import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class FreightMasterBean {
    private RichTable freightTableBinding;
    private String editAction="V";
    private RichInputText amendNoBinding;

    public FreightMasterBean() {
    }

    public void deleteRecordDailogListener(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
        {
        OperationBinding op = null;
        ADFUtils.findOperation("Delete").execute();
        op = (OperationBinding) ADFUtils.findOperation("Commit");
        if (getEditAction().equals("V")) {
        Object rst = op.execute();
        }
        System.out.println("Record Delete Successfully");
        if(op.getErrors().isEmpty()){
        FacesMessage Message = new FacesMessage("Record Deleted Successfully");
        Message.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        } else if (!op.getErrors().isEmpty())
        {
        OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
        Object rstr = opr.execute();
        FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
        Message.setSeverity(FacesMessage.SEVERITY_ERROR);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(freightTableBinding);
    }

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public void setFreightTableBinding(RichTable freightTableBinding) {
        this.freightTableBinding = freightTableBinding;
    }

    public RichTable getFreightTableBinding() {
        return freightTableBinding;
    }

    public void AmndFreightAL(ActionEvent actionEvent) {
        System.out.println("amendNoBinding.getValue()"+amendNoBinding.getValue());
       OperationBinding op=ADFUtils.findOperation("AmendFreight");
//       op.getParamsMap().put("AmendNumber", amendNoBinding.getValue());
       op.execute();
        AdfFacesContext.getCurrentInstance().addPartialTarget(freightTableBinding);
    }

    public void setAmendNoBinding(RichInputText amendNoBinding) {
        this.amendNoBinding = amendNoBinding;
    }

    public RichInputText getAmendNoBinding() {
        return amendNoBinding;
    }
}
