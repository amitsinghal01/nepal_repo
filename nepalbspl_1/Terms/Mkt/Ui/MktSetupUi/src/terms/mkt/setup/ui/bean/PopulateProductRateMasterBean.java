package terms.mkt.setup.ui.bean;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.lang.reflect.Method;

import java.util.ArrayList;
import java.util.List;

import java.util.StringTokenizer;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;

import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.adf.view.rich.util.ResetUtils;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;
import oracle.jbo.domain.Date;

import oracle.sql.DATE;

import org.apache.myfaces.trinidad.model.UploadedFile;

import terms.mkt.setup.model.view.ProductRateMasterVORowImpl;

public class PopulateProductRateMasterBean {
    private RichInputText assRateBinding;
    private RichButton headerEditBinding;
    private RichInputText bindingOutputText;
    private RichPanelHeader myPageRootComponent;
    private RichInputComboboxListOfValues productCodeBinding;
    private RichInputComboboxListOfValues approvedByBinding;
    private RichInputComboboxListOfValues verifiedByBinding;
    private RichInputText previousRateBinding;
    private RichInputText previousMrpBinding;
    private RichTable tableBinding;
    private RichInputDate frDateBinding;
    private UploadedFile file;
    private ArrayList<String> list = new ArrayList<String>();
//    boolean var=false;

    public PopulateProductRateMasterBean() {
    }

    public void populateOnClick(ActionEvent actionEvent) 
    {
        OperationBinding opp=ADFUtils.findOperation("populateProductRate");
        AdfFacesContext.getCurrentInstance().addPartialTarget(tableBinding); 
        Object rst =opp.execute();
        System.out.println("opp.getResult() in bean:"+opp.getResult());
        if(opp.getResult()!= null){  
            String result=opp.getResult().toString().substring(0,1);
                    String msgg=opp.getResult().toString().substring(opp.getResult().toString().indexOf("-")+1);
//            var=true;
//                    String msg1=opp.getResult().toString();
//                    System.out.println("result after substring:"+result);
//                    System.out.println("msg:"+msgg+" msg1:"+msg1);
            if(result.equalsIgnoreCase("E")){
                ADFUtils.showMessage(msgg, 0);
//                var=false;
            }
        }
    }
    
    public void onPageCreate()
    {
       
       OperationBinding opp1=ADFUtils.findOperation("CreateFilter");
       opp1.execute();

         OperationBinding opp=ADFUtils.findOperation("CreateInsertPopulate");
        Object obj=opp.execute(); 
      

   }

//    public void saveAL(ActionEvent actionEvent)
//    {
//        System.out.println("Before");
//        ADFUtils.findOperation("calculateAmountRateMaster").execute();
//        System.out.println("End");
//
//
//    }

    public void currentMRPVCL(ValueChangeEvent valueChangeEvent) 
    {
         System.out.println("Before VCL");
        OperationBinding ob = ADFUtils.findOperation("calculateAmountRateMaster");
        ob.getParamsMap().put("currentMrp", valueChangeEvent.getNewValue());
            ob.execute();
        AdfFacesContext.getCurrentInstance().addPartialTarget(tableBinding);

    }

    public void saveButtonAL(ActionEvent actionEvent) 
    {
//        if(var ){
//       ADFUtils.setLastUpdatedBy("ProductRateMasterVO1Iterator","LastUpdatedBy");
//       OperationBinding op= ADFUtils.findOperation("insertPopulate");
//       Object obj=op.execute();
//       System.out.println("OBJECT VALUE"+obj);
//       if(obj.equals("N"))
//       {
//           FacesMessage Message = new FacesMessage("Current Rate and Current MRP is required to save Product.");   
//           Message.setSeverity(FacesMessage.SEVERITY_INFO);   
//           FacesContext fc = FacesContext.getCurrentInstance();   
//           fc.addMessage(null, Message); 
//       }
//       else
//       {
        Integer i=(Integer)bindingOutputText.getValue();
        System.out.println("EDIT TRANS VALUE ====> "+i);
//        ADFUtils.setLastUpdatedBy("ProductRateMasterVO1Iterator","LastUpdatedBy");
        if(i.equals(0) )      {
            
        ADFUtils.findOperation("Commit").execute();
        FacesMessage Message = new FacesMessage("Record Save Successfully");   
        Message.setSeverity(FacesMessage.SEVERITY_INFO);   
        FacesContext fc = FacesContext.getCurrentInstance();   
        fc.addMessage(null, Message);      
         
        }
        else
       
          { 
             
         ADFUtils.findOperation("Commit").execute();
         FacesMessage Message = new FacesMessage("Record Update Successfully");   
         Message.setSeverity(FacesMessage.SEVERITY_INFO);   
         FacesContext fc = FacesContext.getCurrentInstance();   
         fc.addMessage(null, Message);      
         }
//        }else{
//            ADFUtils.showMessage("Please solve error before saving", 2);
//        }
//       }
            
    }  
        public String resolvEl(String data)
        {
                   FacesContext fc = FacesContext.getCurrentInstance();
                   Application app = fc.getApplication();
                   ExpressionFactory elFactory = app.getExpressionFactory();
                   ELContext elContext = fc.getELContext();
                   ValueExpression valueExp = elFactory.createValueExpression(elContext, data, Object.class);
                   String Message=valueExp.getValue(elContext).toString();
                   return Message;
        }


    public void setAssRateBinding(RichInputText assRateBinding) {
        this.assRateBinding = assRateBinding;
    }

    public RichInputText getAssRateBinding() {
        return assRateBinding;
    }
    
//    public void saveandCloseButtonAL(ActionEvent actionEvent) 
//    {
//            
//        ADFUtils.findOperation("insertPopulate").execute();
//        String param=resolvEl("#{pageFlowScope.mode=='E'}"); 
//        System.out.println("Mode is ====> "+param);
//        if(param.equalsIgnoreCase("true"))
//        {
//        ADFUtils.findOperation("Commit").execute();
//        FacesMessage Message = new FacesMessage("Record Update Successfully");   
//        Message.setSeverity(FacesMessage.SEVERITY_INFO);   
//        FacesContext fc = FacesContext.getCurrentInstance();   
//        fc.addMessage(null, Message);      
//         
//        }
//        else
//        {
//         
//         ADFUtils.findOperation("Commit").execute();
//         FacesMessage Message = new FacesMessage("Record Save Successfully");   
//         Message.setSeverity(FacesMessage.SEVERITY_INFO);   
//         FacesContext fc = FacesContext.getCurrentInstance();   
//         fc.addMessage(null, Message);      
//        }
//    }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setBindingOutputText(RichInputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichInputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }
    public void setMyPageRootComponent(RichPanelHeader myPageRootComponent) {
        this.myPageRootComponent = myPageRootComponent;
    }

    public RichPanelHeader getMyPageRootComponent() {
        return myPageRootComponent;
    }
    private void cevmodecheck(){
         if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
             makeComponentHierarchyReadOnly(this.getMyPageRootComponent(), true);
             cevModeDisableComponent("V");
         }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
             makeComponentHierarchyReadOnly(this.getMyPageRootComponent(), false);
             cevModeDisableComponent("C");
         }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
             makeComponentHierarchyReadOnly(this.getMyPageRootComponent(), false);
             cevModeDisableComponent("E");
         }
     }
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
            try {
                Method method1 =
                    component.getClass().getMethod("setDisabled", boolean.class);
                if (method1 != null) {
                    method1.invoke(component, valueComponent);
                }
            } catch (NoSuchMethodException e) {
                try {
                    Method method =
                        component.getClass().getMethod("setReadOnly", boolean.class);
                    if (method != null) {
                        method.invoke(component, valueComponent);
                    }
                } catch (Exception e1) {
                    // e.printStackTrace();//silently eat this exception.
                }


            } catch (Exception e) {
                // e.printStackTrace();//silently eat this exception.
            }
            List<UIComponent> childComponents = component.getChildren();
            for (UIComponent comp : childComponents) {
                makeComponentHierarchyReadOnly(comp, valueComponent);
            }
        }

 
    public void cevModeDisableComponent(String mode) {
        if (mode.equals("E")) {
                      getAssRateBinding().setDisabled(true);
                       getHeaderEditBinding().setDisabled(true);
                       getProductCodeBinding().setDisabled(true);
                       getPreviousMrpBinding().setDisabled(true);
                       getPreviousRateBinding().setDisabled(true);
                       getFrDateBinding().setDisabled(true);
                     
               } else if (mode.equals("C")) {
                   getAssRateBinding().setDisabled(true);
                   getHeaderEditBinding().setDisabled(true);
                   getProductCodeBinding().setDisabled(true);
                   getPreviousMrpBinding().setDisabled(true);
                   getPreviousRateBinding().setDisabled(true);
                   getApprovedByBinding().setDisabled(true);
                   getVerifiedByBinding().setDisabled(true);
                   getFrDateBinding().setDisabled(true);
                   
               } else if (mode.equals("V")) {
                   
               }
    }

    public void setProductCodeBinding(RichInputComboboxListOfValues productCodeBinding) {
        this.productCodeBinding = productCodeBinding;
    }

    public RichInputComboboxListOfValues getProductCodeBinding() {
        return productCodeBinding;
    }

    public void setApprovedByBinding(RichInputComboboxListOfValues approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputComboboxListOfValues getApprovedByBinding() {
        return approvedByBinding;
    }

    public void setVerifiedByBinding(RichInputComboboxListOfValues verifiedByBinding) {
        this.verifiedByBinding = verifiedByBinding;
    }

    public RichInputComboboxListOfValues getVerifiedByBinding() {
        return verifiedByBinding;
    }

    public void setPreviousRateBinding(RichInputText previousRateBinding) {
        this.previousRateBinding = previousRateBinding;
    }

    public RichInputText getPreviousRateBinding() {
        return previousRateBinding;
    }

    public void setPreviousMrpBinding(RichInputText previousMrpBinding) {
        this.previousMrpBinding = previousMrpBinding;
    }

    public RichInputText getPreviousMrpBinding() {
        return previousMrpBinding;
    }

    public void setTableBinding(RichTable tableBinding) {
        this.tableBinding = tableBinding;
    }

    public RichTable getTableBinding() {
        return tableBinding;
    }

    public void setFrDateBinding(RichInputDate frDateBinding) {
        this.frDateBinding = frDateBinding;
    }

    public RichInputDate getFrDateBinding() {
        return frDateBinding;
    }

    public void fileUploadVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
            System.out.println("in file upload vce" + vce.getNewValue() + " new:" + vce.getOldValue());
        UploadedFile file = (UploadedFile) vce.getNewValue();
            System.out.println(" in file upload vce Content Type:" + file.getContentType() + " file name:" +
                               file.getFilename() + " Size:" + file.getLength() + " file:" + file);
            System.out.println(file.getFilename().substring(file.getFilename().lastIndexOf('.') + 1));
        try {
                if (file.getFilename().substring(file.getFilename().lastIndexOf('.') + 1).equalsIgnoreCase("csv")) {
                        System.out.println("CSV File Format catched");
                    parseFile(file.getInputStream());
                        ResetUtils.reset(vce.getComponent());
//                    OperationBinding opp=ADFUtils.findOperation("uploadPreviousRate");
//                    AdfFacesContext.getCurrentInstance().addPartialTarget(tableBinding); 
//                        opp.execute();
//                            System.out.println("opp.getResult() in bean:"+opp.getResult());
                } else {
                    FacesMessage msg = new FacesMessage("File format not supported.-- Upload CSV file");
                    msg.setSeverity(FacesMessage.SEVERITY_WARN);
                    FacesContext.getCurrentInstance().addMessage(null, msg);
                    ResetUtils.reset(vce.getComponent());
                }
                AdfFacesContext.getCurrentInstance().addPartialTarget(tableBinding);                    
            } catch (IOException e) {
                // TODO
            }
    }
    
    private void parseFile(InputStream file) {
        list.clear();
        BufferedReader reader = new BufferedReader(new InputStreamReader(file));
        String strLine = "";
        StringTokenizer st = null;
        int lineNumber = 0, tokenNumber = 0, flag = 0;
        ProductRateMasterVORowImpl rw = null;
        cleardata();
        DCIteratorBinding dci = ADFUtils.findIterator("ProductRateMasterVO1Iterator");
        RowSetIterator rsi = dci.getViewObject().createRowSetIterator(null);
        try {
            while ((strLine = reader.readLine()) != null) {
                lineNumber++;
                if (lineNumber > 1) {
                    Row last = rsi.last();
                    int i = rsi.getRangeIndexOf(last);
                    rw = (ProductRateMasterVORowImpl) rsi.createRow();
                    rw.setNewRowState(Row.STATUS_INITIALIZED);
                    rsi.insertRowAtRangeIndex(i + 1, rw);
                    rsi.setCurrentRow(rw);
                    flag = 1;
                }
                st = new StringTokenizer(strLine, ",");
                while (st.hasMoreTokens()) {
                    tokenNumber++;
                    String theToken = st.nextToken();
                    System.out.println("Line # " + lineNumber + ", Token # " + tokenNumber + ", Token : " + theToken +
                                       " flag:" + flag + " tokenNumber:" + tokenNumber);
                    if (lineNumber > 1) {
                    switch (tokenNumber) {

                            case 1:
                                    System.out.println("Customer Type: " + theToken);
                                rw.setAttribute("CustType", theToken);
                                flag = 0;
                                break;

                            case 2:
                                    System.out.println("Product Code: " + theToken);
                                rw.setAttribute("ProdCd", theToken);
                                flag = 0;
                                break;

                            case 3:
                                Date d =  ADFUtils.convertStringToJboDate(theToken);
                                    System.out.println("From Date: " + theToken);
                                    System.out.println("From Date JBO: " + d);
                                rw.setAttribute("FrDt", d);
                                flag = 0;
                                break;

                            case 4:
                                Date e =  ADFUtils.convertStringToJboDate(theToken);
                                    System.out.println("To Date: " + theToken);
                                    System.out.println("To Date JBO: " + e);
                                rw.setAttribute("ToDt", e);
                                flag = 0;
                                break;   
                    
                            case 5:
                                    System.out.println("Current MRP: " + theToken);
                                rw.setAttribute("CurrMrp", theToken);
                                flag = 0;
                        
                                if(rw.getAttribute("CurrMrp")!=null){
                                    OperationBinding ob = ADFUtils.findOperation("getAssRate");
                                    ob.getParamsMap().put("currentMrp", rw.getAttribute("CurrMrp"));
                                    ob.execute();
                                    rw.setAttribute("AssRate", ob.getResult());
                                    AdfFacesContext.getCurrentInstance().addPartialTarget(tableBinding);
                                }
                                break;
                    
                            case 6:
                                    System.out.println("Current Rate: " + theToken);
                                rw.setAttribute("CurrRate", theToken);
                                flag = 0;
                                break;
                        }
                    }
                }
                //reset token number
                tokenNumber = 0;
            }
            rsi.closeRowSetIterator();
        } catch (IOException e) {
            e.printStackTrace();
            ADFUtils.showMessage("Content error in uploaded file", 0);
            rsi.closeRowSetIterator();
        } catch (Exception e) {
            e.printStackTrace();
            ADFUtils.showMessage("Data error in uploaded file", 0);
            rsi.closeRowSetIterator();
        }
    }
    
    public void cleardata() {
           DCIteratorBinding dci = ADFUtils.findIterator("ProductRateMasterVO1Iterator");
           Row[] r = dci.getAllRowsInRange();
           System.out.println("ITERATOR RANGE: " + r.length);
           if (r.length > 0) {
               RowSetIterator rsi1 = dci.getViewObject().createRowSetIterator(null);
               while (rsi1.hasNext()) {
                   Row r1 = rsi1.next();
                   r1.remove();
               }
               AdfFacesContext.getCurrentInstance().addPartialTarget(tableBinding);
               rsi1.closeRowSetIterator();
           }
       }
    
    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public UploadedFile getFile() {
        return file;
    }
    
    public void setList(ArrayList<String> list) {
        this.list = list;
    }

    public ArrayList<String> getList() {
        return list;
    }
    
}