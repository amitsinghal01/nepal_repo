package terms.mkt.setup.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

public class ProductCustomerMappingBean {
    private RichInputText prodCdDescBinding;
    private RichInputText modelDescriptionBinding;
    private RichInputText cityCodeBinding;
    private RichInputText plantCodeBinding;
    private RichOutputText bindingOutputText;
    private RichButton editBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton saveBinding;
    private RichButton saveAndCloseBinding;

    public ProductCustomerMappingBean() {
    }

    public void saveButtonAL(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedBy("ProductCustomerMappingVO1Iterator","LastUpdatedBy");
           Integer i=(Integer)getBindingOutputText().getValue();
           System.out.println("EDIT TRANS"+i);
        if(i.equals(0))
        {
           FacesMessage Message = new FacesMessage("Record Save Successfully");   
           Message.setSeverity(FacesMessage.SEVERITY_INFO);   
           FacesContext fc = FacesContext.getCurrentInstance();   
           fc.addMessage(null, Message);      
            
        }
        else 
        {
            FacesMessage Message = new FacesMessage("Record Updated Successfully");   
            Message.setSeverity(FacesMessage.SEVERITY_INFO);   
            FacesContext fc = FacesContext.getCurrentInstance();   
            fc.addMessage(null, Message);      
        }
    }
    
    public String resolvEl(String data)
    {
                      FacesContext fc = FacesContext.getCurrentInstance();
                      Application app = fc.getApplication();
                      ExpressionFactory elFactory = app.getExpressionFactory();
                      ELContext elContext = fc.getELContext();
                      ValueExpression valueExp = elFactory.createValueExpression(elContext, data, Object.class);
                      String Message=valueExp.getValue(elContext).toString();
                      return Message;
    }


    public void setProdCdDescBinding(RichInputText prodCdDescBinding) {
        this.prodCdDescBinding = prodCdDescBinding;
    }

    public RichInputText getProdCdDescBinding() {
        return prodCdDescBinding;
    }

    public void setModelDescriptionBinding(RichInputText modelDescriptionBinding) {
        this.modelDescriptionBinding = modelDescriptionBinding;
    }

    public RichInputText getModelDescriptionBinding() {
        return modelDescriptionBinding;
    }

    public void setCityCodeBinding(RichInputText cityCodeBinding) {
        this.cityCodeBinding = cityCodeBinding;
    }

    public RichInputText getCityCodeBinding() {
        return cityCodeBinding;
    }

    public void setPlantCodeBinding(RichInputText plantCodeBinding) {
        this.plantCodeBinding = plantCodeBinding;
    }

    public RichInputText getPlantCodeBinding() {
        return plantCodeBinding;
    }
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
            try {
                Method method1 =
                    component.getClass().getMethod("setDisabled", boolean.class);
                if (method1 != null) {
                    method1.invoke(component, valueComponent);
                }
            } catch (NoSuchMethodException e) {
                try {
                    Method method =
                        component.getClass().getMethod("setReadOnly", boolean.class);
                    if (method != null) {
                        method.invoke(component, valueComponent);
                    }
                } catch (Exception e1) {
                    // e.printStackTrace();//silently eat this exception.
                }


            } catch (Exception e) {
                // e.printStackTrace();//silently eat this exception.
            }
            List<UIComponent> childComponents = component.getChildren();
            for (UIComponent comp : childComponents) {
                makeComponentHierarchyReadOnly(comp, valueComponent);
            }
        }
    public void cevModeDisableComponent(String mode) {
    if (mode.equals("E")) {
            getProdCdDescBinding().setDisabled(true);
            getModelDescriptionBinding().setDisabled(true);
            getEditBinding().setDisabled(true);
            getCityCodeBinding().setDisabled(true);
            getPlantCodeBinding().setDisabled(true);
            getSaveBinding().setDisabled(false);
            getSaveAndCloseBinding().setDisabled(false);
    } else if (mode.equals("C")) {
        getProdCdDescBinding().setDisabled(true);
        getModelDescriptionBinding().setDisabled(true);
        getCityCodeBinding().setDisabled(true);
        getPlantCodeBinding().setDisabled(true);
        getSaveBinding().setDisabled(false);
        getSaveAndCloseBinding().setDisabled(false);
    } else if (mode.equals("V")) {
        
       getSaveBinding().setDisabled(true);
       getSaveAndCloseBinding().setDisabled(true);
    }
    }
    public void EditButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }
    private void cevmodecheck(){
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setEditBinding(RichButton editBinding) {
        this.editBinding = editBinding;
    }

    public RichButton getEditBinding() {
        return editBinding;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setSaveBinding(RichButton saveBinding) {
        this.saveBinding = saveBinding;
    }

    public RichButton getSaveBinding() {
        return saveBinding;
    }

    public void setSaveAndCloseBinding(RichButton saveAndCloseBinding) {
        this.saveAndCloseBinding = saveAndCloseBinding;
    }

    public RichButton getSaveAndCloseBinding() {
        return saveAndCloseBinding;
    }
}
