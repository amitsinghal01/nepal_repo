package terms.mkt.setup.ui.bean;

import javax.faces.application.FacesMessage;

import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class SearchProductRateMasterBean {
    private RichTable productRateMasterTableBinding;

    public SearchProductRateMasterBean() {
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {

        if (dialogEvent.getOutcome().name().equals("ok")) {
            OperationBinding op = null;
            ADFUtils.findOperation("Delete").execute();
            op = (OperationBinding) ADFUtils.findOperation("Commit");
            Object rst = op.execute();
            System.out.println("Record Delete Successfully");
            if (op.getErrors().isEmpty()) {
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            } else if (!op.getErrors().isEmpty()) {
                OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
                Object rstr = opr.execute();
                FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            }
        }

        AdfFacesContext.getCurrentInstance().addPartialTarget(productRateMasterTableBinding);

    }

    public void setProductRateMasterTableBinding(RichTable productRateMasterTableBinding) {
        this.productRateMasterTableBinding = productRateMasterTableBinding;
    }

    public RichTable getProductRateMasterTableBinding() {
        return productRateMasterTableBinding;
    }

    public String editAllRecords() {
        System.out.println("beforee calling");
        OperationBinding binding = (OperationBinding) ADFUtils.findOperation("viewAllProductRate");
        binding.getParamsMap().put("view_mode", "V");
        binding.execute();
        System.out.println("after calling");
        return "goProductRate";
    }

    public String viewProcDtlLAC() {
        OperationBinding binding = (OperationBinding) ADFUtils.findOperation("viewAllProductRate");
        binding.getParamsMap().put("view_mode", "L");
        binding.execute();
        return "goProductRate";
    }

    public void selelctAllAL(ActionEvent actionEvent) {
        OperationBinding binding = ADFUtils.findOperation("selectAndDeselectMethod");
                   binding.getParamsMap().put("mode_type", "S");
                   binding.getParamsMap().put("grant_type", "A");
                   binding.execute();
                  
    }

    public void deselectAllAL(ActionEvent actionEvent) {
        OperationBinding binding = ADFUtils.findOperation("selectAndDeselectMethod");
                  binding.getParamsMap().put("mode_type", "D");
                  binding.getParamsMap().put("grant_type", "A");
                  binding.execute();
    }
}
