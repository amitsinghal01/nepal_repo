package terms.mkt.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichPanelLabelAndMessage;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

public class CreateDailyDespatchPlanBean {
    private RichPanelLabelAndMessage bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichButton detailcreateBinding;
    private RichInputDate entryDateBinding;
    private RichInputComboboxListOfValues partyCodeBinding;
    private RichInputText entryNoBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichTable tableCreateDailyDespatchBinding;
    private RichInputComboboxListOfValues partyCode2Binding;
    private RichInputDate dtlEntryDate;
    private RichInputText partyNameBinding;
    private RichInputDate saleNoteDateBinding;
    private RichInputText packingBinding;
    private RichInputText bookQuantityBinding;
    private RichInputText balanceQtyBinding;
    private RichInputText despatchQtyBinding;
    private RichInputText creditNoDateBinding;
    private RichInputText destinationBinding;
    private RichInputText lcNoBinding;
    private RichInputText amdNoBinding;
    private RichInputDate amdDateBinding;
    private RichPopup editPopupBinding;

    public CreateDailyDespatchPlanBean() {
    }

    public void setBindingOutputText(RichPanelLabelAndMessage bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichPanelLabelAndMessage getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void EditButtonAL(ActionEvent actionEvent) {
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        ADFUtils.setEL("#{pageFlowScope.mode}", "V");
        editPopupBinding.show(hints);
    }

    public void setDetailcreateBinding(RichButton detailcreateBinding) {
        this.detailcreateBinding = detailcreateBinding;
    }

    public RichButton getDetailcreateBinding() {
        return detailcreateBinding;
    }
    private void cevmodecheck(){
          if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
              makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
              cevModeDisableComponent("V");
          }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
              makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
              cevModeDisableComponent("C");
          }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
              makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
              cevModeDisableComponent("E");
          }
      }
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
              try {
                  Method method1 =
                      component.getClass().getMethod("setDisabled", boolean.class);
                  if (method1 != null) {
                      method1.invoke(component, valueComponent);
                  }
              } catch (NoSuchMethodException e) {
                  try {
                      Method method =
                          component.getClass().getMethod("setReadOnly", boolean.class);
                      if (method != null) {
                          method.invoke(component, valueComponent);
                      }
                  } catch (Exception e1) {
                      // e.printStackTrace();//silently eat this exception.
                  }


              } catch (Exception e) {
                  // e.printStackTrace();//silently eat this exception.
              }
              List<UIComponent> childComponents = component.getChildren();
              for (UIComponent comp : childComponents) {
                  makeComponentHierarchyReadOnly(comp, valueComponent);
              }
          }

      public void cevModeDisableComponent(String mode) {
       if (mode.equals("E")) {
              getUnitCodeBinding().setDisabled(true);
              getEntryNoBinding().setDisabled(true);
              getEntryDateBinding().setDisabled(true);
              getPartyCodeBinding().setDisabled(true);
                  getHeaderEditBinding().setDisabled(true);
                  getDetailcreateBinding().setDisabled(false);
                  getPartyCode2Binding().setDisabled(false);
                  getPartyNameBinding().setDisabled(true);
                  getSaleNoteDateBinding().setDisabled(true);
              getBookQuantityBinding().setDisabled(true);
                  getPackingBinding().setDisabled(true);
             // getDespatchQtyBinding().setDisabled(true);
              getBalanceQtyBinding().setDisabled(true);
              getCreditNoDateBinding().setDisabled(true);
              getDestinationBinding().setDisabled(true);
              getLcNoBinding().setDisabled(true);
              getAmdNoBinding().setDisabled(true);
              getAmdDateBinding().setDisabled(true);
          } else if (mode.equals("C")) {
             getUnitCodeBinding().setDisabled(true);
              getEntryNoBinding().setDisabled(true);
              getDetailcreateBinding().setDisabled(false);
              getPartyNameBinding().setDisabled(true);
              getSaleNoteDateBinding().setDisabled(true);
              getBookQuantityBinding().setDisabled(true);
              getPackingBinding().setDisabled(true);
            //  getDespatchQtyBinding().setDisabled(true);
              getBalanceQtyBinding().setDisabled(true);
              getCreditNoDateBinding().setDisabled(true);
              getDestinationBinding().setDisabled(true);
              getLcNoBinding().setDisabled(true);
              getAmdNoBinding().setDisabled(true);
              getAmdDateBinding().setDisabled(true);
          } else if (mode.equals("V")) {
              getDetailcreateBinding().setDisabled(true);
          }
          
      }

    public void DeletePopupDialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                   {
               
                   ADFUtils.findOperation("Delete").execute();
                   FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                   Message.setSeverity(FacesMessage.SEVERITY_INFO);
                   FacesContext fc = FacesContext.getCurrentInstance();
                   fc.addMessage(null, Message);
       
               }
               AdfFacesContext.getCurrentInstance().addPartialTarget(tableCreateDailyDespatchBinding);   
    }

   

    public void SaveAL(ActionEvent actionEvent) {
        oracle.binding.OperationBinding op = ADFUtils.findOperation("generateDailyDespatchno");
        Object rst = op.execute();

        System.out.println("--------Commit-------");
        System.out.println("value aftr getting result--->?" + rst);


        if (rst.toString() != null && rst.toString() != "") {
            if (op.getErrors().isEmpty()) {
                ADFUtils.findOperation("Commit").execute();
                FacesMessage Message = new FacesMessage("Record Saved Successfully.Entry No is " + rst + ".");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);

            }
        }

        if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ") {
            if (op.getErrors().isEmpty()) {
                ADFUtils.findOperation("Commit").execute();
                FacesMessage Message = new FacesMessage("Record Updated Successfully.");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
 }
 }
}

    public void createAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert").execute();
//        Date EntryHeader = (Date) entryDateBinding.getValue();
//        System.out.println("entry Date==="+EntryHeader);
//        dtlEntryDate.setValue(EntryHeader);
        
        getEntryDateBinding().setDisabled(true);
        getPartyCodeBinding().setDisabled(true);
        
    }

    public void setEntryDateBinding(RichInputDate entryDateBinding) {
        this.entryDateBinding = entryDateBinding;
    }

    public RichInputDate getEntryDateBinding() {
        return entryDateBinding;
    }

    public void setPartyCodeBinding(RichInputComboboxListOfValues partyCodeBinding) {
        this.partyCodeBinding = partyCodeBinding;
    }

    public RichInputComboboxListOfValues getPartyCodeBinding() {
        return partyCodeBinding;
    }

    public void setEntryNoBinding(RichInputText entryNoBinding) {
        this.entryNoBinding = entryNoBinding;
    }

    public RichInputText getEntryNoBinding() {
        return entryNoBinding;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setTableCreateDailyDespatchBinding(RichTable tableCreateDailyDespatchBinding) {
        this.tableCreateDailyDespatchBinding = tableCreateDailyDespatchBinding;
    }

    public RichTable getTableCreateDailyDespatchBinding() {
        return tableCreateDailyDespatchBinding;
    }

    public void setPartyCode2Binding(RichInputComboboxListOfValues partyCode2Binding) {
        this.partyCode2Binding = partyCode2Binding;
    }

    public RichInputComboboxListOfValues getPartyCode2Binding() {
        return partyCode2Binding;
    }

    public void setDtlEntryDate(RichInputDate dtlEntryDate) {
        this.dtlEntryDate = dtlEntryDate;
    }

    public RichInputDate getDtlEntryDate() {
        return dtlEntryDate;
    }

    public void setPartyNameBinding(RichInputText partyNameBinding) {
        this.partyNameBinding = partyNameBinding;
    }

    public RichInputText getPartyNameBinding() {
        return partyNameBinding;
    }

    public void setSaleNoteDateBinding(RichInputDate saleNoteDateBinding) {
        this.saleNoteDateBinding = saleNoteDateBinding;
    }

    public RichInputDate getSaleNoteDateBinding() {
        return saleNoteDateBinding;
    }

    public void setPackingBinding(RichInputText packingBinding) {
        this.packingBinding = packingBinding;
    }

    public RichInputText getPackingBinding() {
        return packingBinding;
    }

    public void setBookQuantityBinding(RichInputText bookQuantityBinding) {
        this.bookQuantityBinding = bookQuantityBinding;
    }

    public RichInputText getBookQuantityBinding() {
        return bookQuantityBinding;
    }

    public void setBalanceQtyBinding(RichInputText balanceQtyBinding) {
        this.balanceQtyBinding = balanceQtyBinding;
    }

    public RichInputText getBalanceQtyBinding() {
        return balanceQtyBinding;
    }

    public void setDespatchQtyBinding(RichInputText despatchQtyBinding) {
        this.despatchQtyBinding = despatchQtyBinding;
    }

    public RichInputText getDespatchQtyBinding() {
        return despatchQtyBinding;
    }

    public void setCreditNoDateBinding(RichInputText creditNoDateBinding) {
        this.creditNoDateBinding = creditNoDateBinding;
    }

    public RichInputText getCreditNoDateBinding() {
        return creditNoDateBinding;
    }

    public void setDestinationBinding(RichInputText destinationBinding) {
        this.destinationBinding = destinationBinding;
    }

    public RichInputText getDestinationBinding() {
        return destinationBinding;
    }

    public void setLcNoBinding(RichInputText lcNoBinding) {
        this.lcNoBinding = lcNoBinding;
    }

    public RichInputText getLcNoBinding() {
        return lcNoBinding;
    }

    public void setAmdNoBinding(RichInputText amdNoBinding) {
        this.amdNoBinding = amdNoBinding;
    }

    public RichInputText getAmdNoBinding() {
        return amdNoBinding;
    }

    public void setAmdDateBinding(RichInputDate amdDateBinding) {
        this.amdDateBinding = amdDateBinding;
    }

    public RichInputDate getAmdDateBinding() {
        return amdDateBinding;
    }

    public void amdConfirmDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
        {
            
           oracle.binding.OperationBinding op1=ADFUtils.findOperation("checkMaxAmendmentDailyDespach");
            Object ob=op1.execute();
            if(op1.getResult().toString()!=null && op1.getResult().toString().equalsIgnoreCase("N")){
                
                ADFUtils.showMessage("You Can Update Latest Amendment Only.", 2);
                ADFUtils.setEL("#{pageFlowScope.mode}", "V");
            }
            else{
           
           System.out.println("AmdNo==="+ADFUtils.resolveExpression("#{pageFlowScope.AmdNo}"));
            System.out.println("EntryId==="+ADFUtils.resolveExpression("#{pageFlowScope.EntryId}"));
            System.out.println("EntryNo==="+ADFUtils.resolveExpression("#{pageFlowScope.EntryNo}"));
        oracle.binding.OperationBinding op = ADFUtils.findOperation("getAmdDailyDespatch");
        op.getParamsMap().put("AmdNo", ADFUtils.resolveExpression("#{pageFlowScope.AmdNo}"));
        op.getParamsMap().put("EntryId", ADFUtils.resolveExpression("#{pageFlowScope.EntryId}"));
        op.getParamsMap().put("EntryNo", ADFUtils.resolveExpression("#{pageFlowScope.EntryNo}"));
        op.execute();
        ADFUtils.setEL("#{pageFlowScope.mode}", "E");
        cevmodecheck();
        AdfFacesContext.getCurrentInstance().addPartialTarget(tableCreateDailyDespatchBinding);
        AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
        }
        }
    }

    public void setEditPopupBinding(RichPopup editPopupBinding) {
        this.editPopupBinding = editPopupBinding;
    }

    public RichPopup getEditPopupBinding() {
        return editPopupBinding;
    }
}
