package terms.mkt.transaction.ui.bean;

import java.awt.Component;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.text.ParseException;

import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;

import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import javax.faces.validator.ValidatorException;

import javax.persistence.criteria.Order;

import javax.xml.ws.Dispatch;

import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichDialog;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectBooleanCheckbox;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.nav.RichCommandLink;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.adf.view.rich.util.ResetUtils;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

import oracle.jbo.RowSetIterator;

import org.apache.myfaces.trinidad.model.UploadedFile;

import terms.mkt.transaction.model.view.DespatchAdviceDetailVORowImpl;
import terms.mkt.transaction.model.view.DespatchAdviceHeaderVORowImpl;

public class DespatchAdviceBean {
    private RichTable dispatchAdviceTableBinding;
    private RichSelectOneChoice bindDoctype;
    private RichInputText bindDocNo;
    private RichInputText bindOrderQty;
    private RichInputText bindDispatchQty;
    private RichInputText orderQuantBinding;
    private RichInputText dispQuanBinding;
    private RichInputText dispatchQuan;
    private RichInputText dispatchh;
    private RichInputText bindRate;
    private RichInputText bindTransMul;
    private RichInputText bindStockQty;
    private RichInputComboboxListOfValues bindAckNo;
    private RichInputComboboxListOfValues bindUnit;
    private RichInputComboboxListOfValues bindCustomer;
    private RichInputText cgstAmtBinding;
    private RichInputText sgstRateBinding;
    private RichInputText sgstAmtBinding;
    private RichInputText igstRateBinding;
    private RichInputText igstAmtBinding;
    private RichInputText typeBinding;
    private RichInputText amendmentBinding;
    private RichInputText adviceNoBinding;
    private RichInputDate entryDateBinding;
    private RichInputDate aviceDateBinding;
    private RichInputText cityBinding;
    private RichInputText plantBinding;
    private RichInputComboboxListOfValues consigneeBinding;
    private RichInputText consigneeAddressBinding;
    private RichInputText stockTypeBinding;
    private RichInputText stockTypeDescription;
    private RichInputComboboxListOfValues authoriseByBinding;
    private RichButton headerEditBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichOutputText bindingOutputTextt;
    private RichButton detailcreateBinding;
    private RichButton detaildeleteBinding;
    private RichInputText cgstRateBinding;
    private RichInputText grossAmtBinding;
    private RichInputText sgstHeaderBinding;
    private RichInputText cgstHeaderBinding;
    private RichInputText igstHeaderBinding;
    private RichInputText netAmountBinding;
    private RichSelectOneChoice statusHeaderBinding;
    private RichInputComboboxListOfValues orderTypeBinding;
    private RichInputComboboxListOfValues prodCodeBinding;
    private RichInputText hsnNoBinding;
    private RichInputText bookQty;
    private RichColumn parameterFlagBind;
    private RichInputText parameterFlagBinding;
    private RichInputText bindTotalQty;
    private RichShowDetailItem headerTabBinding;
    private RichShowDetailItem itemTabBinding;
    private RichShowDetailItem discountDetailTab;
    private RichShowDetailItem otherDetail;
    private RichShowDetailItem bankDetailTab;
    private RichInputDate cancelDate;
    private RichPopup statusPopupBinding;
    private RichPopup editpopupbinding;
    private RichInputText amdNoBinding;
    private RichInputDate amdDateBinding;
    private RichInputText consiNameBind;
    private RichInputComboboxListOfValues portDischargeBindng;
    private RichInputComboboxListOfValues recPreCarriage;
    private RichInputComboboxListOfValues preCarriage;
    private RichInputComboboxListOfValues portLoading;
    private RichInputComboboxListOfValues placeDelivery;
    private RichInputText amendRemaBinding;
    private RichInputText inscompname;
    private RichInputDate validfrombinding;
    private RichInputText amtBinding;
    private RichButton accountInsBinding;
    private RichButton plantInsBinding;
    private RichInputText insAccBinding;
    private RichInputText insPlantBinding;
    private RichInputComboboxListOfValues checkByBinding;
    private RichInputText stockreq;
    private RichInputText uomBinding;
    private RichInputText totalAmtBinding;
    private RichInputComboboxListOfValues preparedByBinding;
    private RichInputDate preparedDate;
    private RichInputText planthoBinding;
    private RichInputText pathBinding;
    private RichInputText docSrNoBinding;
    private RichInputText docRefUnitCodeBinding;
    private RichInputText refDocNoBinding;
    private RichInputText refDocTypeBinding;
    private RichInputText docFileNameBinding;
    private RichInputDate refDocDateBinding;
    private RichTable docRefDetailBinding;
    private RichShowDetailItem refDocTabBinding;
    private RichCommandLink downloadLinkBinding;
    private RichInputDate validdate;
    private RichSelectOneChoice bindIncoterm;
    private RichInputText bindPayTerm;
    private RichSelectOneChoice bindModeShip;
    private RichInputComboboxListOfValues bindPayTermLov;
    private RichInputText amountBinding;
    private RichInputText detProdCodeBinding;
    private RichPopup daBatchDetTableBinding;
    private RichInputText pendingQtyBinding;
    private RichInputText despQtyBinding;
    private RichInputText despTotalQtyBinding;
    private RichButton pndingSaleOrderbtnBinding;
    private RichInputText soAmdNoBinding;
    private RichButton socreateBtnBinding;
    private RichButton sodeleteBtnBinding;
    private RichButton populateBatchBtnBinding;
    private RichInputText dtlBatchNoBinding;
    private RichInputText dtlMfgDateBinding;
    private RichInputDate dtlExpDateBinding;
    private RichInputText dtlPendingQtyBinding;
    private RichInputText dtlBatchQtyBinding;
    private RichButton btDtlCreateBtnBinding;
    private RichButton btDtlDeleteBtnBinding;
    private RichInputText totalBtcQtyBinding;
    private RichButton populateBatchDtlBtnBinding;
    private RichInputText soFreeQtyBinding;
    private RichInputText soBookQtyBinding;
    private RichInputText soStockQtyBinding;
    private RichInputText dadtlBatchQtyBinding;
    private RichInputText daDtlMfgDtBinding;
    private RichInputText daDtlExpDtBinding;
    private RichPopup pendSODetailPopupBinding;
    private String desStatus="VV";
    private RichButton allBatchPopulateBtnBinding;
    private RichButton allPopulateBatchDtlBtnBinding;
    private RichInputText newTotalBatchQtyTransBinding;
    private RichInputComboboxListOfValues soDtlPoNoBinding;
    private RichInputText dtlBatchProductCodeBinding;
    private RichSelectBooleanCheckbox selectCheckBoxBinding;
    private RichInputText transSoManualFlagBinding;
    private RichButton addManualDaDetailBinding;
    private RichTable daBatchDetailTableBinding;
    private RichInputText daBatchChStatusBinding;
    private RichInputText custBalanceBinding;
    private RichInputText dadatenepalinding;
    private RichInputText batchProdNameBinding;
    private RichInputText daDtlGrossWtBinding;
    private RichInputText sodtlUomBinding;
    private RichInputText packQtyBinding;
    private RichInputText packUomBinding;
    private RichInputText netWeightBinding;
    private RichInputText soDtlProdDescBinding;
    private RichInputText soDtlPackQtyBinding;
    private RichInputText soDtlPacckUomBinding;

    public DespatchAdviceBean() {
    }

    private static ADFLogger logger = ADFLogger.createADFLogger(DespatchAdviceBean.class); 
    
    public void DeleteDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete").execute();
            //System.out.println("Record Delete Successfully");
            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(dispatchAdviceTableBinding);
    }

    public void setDispatchAdviceTableBinding(RichTable dispatchAdviceTableBinding) {
        this.dispatchAdviceTableBinding = dispatchAdviceTableBinding;
    }

    public RichTable getDispatchAdviceTableBinding() {
        return dispatchAdviceTableBinding;
    }

    public void setBindDoctype(RichSelectOneChoice bindDoctype) {
        this.bindDoctype = bindDoctype;
    }

    public RichSelectOneChoice getBindDoctype() {
        return bindDoctype;
    }

    public void setBindDocNo(RichInputText bindDocNo) {
        this.bindDocNo = bindDocNo;
    }

    public RichInputText getBindDocNo() {
        return bindDocNo;
    }

    public void setDocNoVCL(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        String DocType = (String) bindDoctype.getValue();
        if (DocType.equalsIgnoreCase("D")) {
            bindDocNo.setValue(".");
        } else
            bindDocNo.setValue(null);
        AdfFacesContext.getCurrentInstance().addPartialTarget(dispatchAdviceTableBinding);
    }


    /*   public void validateDispatchQty(FacesContext facesContext, UIComponent uIComponent, Object object) {

        if(object !=null && getOrderQuantBinding()!= null && getDispatchQuan()!=null) {
    oracle.jbo.domain.Number DisQuan = (oracle.jbo.domain.Number) dispatchQuan.getValue();
     System.out.println("Dis num"+DisQuan);
     oracle.jbo.domain.Number OrderQuan = (oracle.jbo.domain.Number) orderQuantBinding.getValue();
     System.out.println("order num"+OrderQuan);


          Integer DispQty = DisQuan.intValue();
          Integer OrderQty = OrderQuan.intValue();

            if(OrderQty.compareTo(DispQty)==-1)
      {
          throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Dispatch Qty must be greater than Order Qty ", null));
      }
      }
    } */


    public void setOrderQuantBinding(RichInputText orderQuantBinding) {
        this.orderQuantBinding = orderQuantBinding;
    }

    public RichInputText getOrderQuantBinding() {
        return orderQuantBinding;
    }


    public void validateDispatchQty(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (object != null) {
            if (getOrderQuantBinding() != null && getDispatchh() != null) {
                BigDecimal DispatchQty = (BigDecimal) object;

                logger.info("dispatchhghgh" + DispatchQty);
                BigDecimal OrderQty =
                    (BigDecimal) ((BigDecimal) orderQuantBinding.getValue() == null ? new BigDecimal(0) :
                                  orderQuantBinding.getValue());
                BigDecimal stock =
                    (BigDecimal) (bindStockQty.getValue() == null ? new BigDecimal(0) : bindStockQty.getValue());
                BigDecimal Book = (BigDecimal) (bookQty.getValue() == null ? new BigDecimal(0) : bookQty.getValue());
                if (OrderQty != null) {

                    if (DispatchQty.compareTo(OrderQty) == 1) {
                        throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                      "Order Qty must be greater than Dispatch Qty ",
                                                                      null));

                    }

                    //                    AdfFacesContext.getCurrentInstance().addPartialTarget(dispatchAdviceTableBinding);
                    //                    OperationBinding op = (OperationBinding) ADFUtils.findOperation("getTransSummm").execute();
                    //                  UIInput input = (UIInput) facesContext.getViewRoot().findComponent("c30:it52");
                    ////                    UIInput input = (UIInput) uIComponent.getAttributes().get("c30:it52");
                    //                    System.out.println("input"+input);
                    //                     Object inputValue = input.getSubmittedValue();
                    //                     System.out.println("objecttt"+inputValue);
                    //                     String Value=inputValue.toString();
                    //                     Integer i=Integer.getInteger(Value);
                    //                     System.out.println("input value---"+i);
                    //
                    //                   oracle.jbo.domain.Number TotDispatch=new oracle.jbo.domain.Number(0);
                    //                    DCIteratorBinding Dcite=ADFUtils.findIterator("DespatchAdviceDetailVO1Iterator");
                    //                    DespatchAdviceDetailVORowImpl row=(DespatchAdviceDetailVORowImpl) Dcite.getCurrentRow();
                    //                    if((Long)ADFUtils.evaluateEL("#{bindings.DespatchAdviceDetailVO1Iterator.estimatedRowCount}")>=1 && row !=null )
                    //                                                  {
                    //                        System.out.println("row.getQty"+row.getQty());
                    //                            System.out.println("row.gettotQty"+row.gettotalQty());
                    //                              if(row.gettotalQty()!=null)
                    //                              {
                    //                    TotDispatch=row.gettotalQty();
                    //                              }
                    //                                                  }
                    //                   Integer TotDispatch1 = Integer.getInteger(facesContext.getExternalContext().getRequestParameterMap().get("arg0"));
                   BigDecimal TotDispatch =(BigDecimal) bindTotalQty.getValue() != null ?(BigDecimal) bindTotalQty.getValue() : new BigDecimal(0);

                    Integer TotDispatchQty = TotDispatch.intValue();
                    //System.out.println("TotDispatchhhhinintttt" + TotDispatchQty);

                    //stock related validation
                    //System.out.println("StockReqValidator===" + stockreq.getValue());
                    String StockReq = (String) (stockreq.getValue() != null ? stockreq.getValue() : "N");
                    //System.out.println("stockzgh" + StockReq);
                    if (StockReq.equalsIgnoreCase("Y")) {
                        if (stock.compareTo(Book) == 1 || stock.compareTo(Book) == 0) {
                            BigDecimal OrderValid = stock.subtract(Book);
                           // System.out.println("OrderValid" + OrderValid);
                            String parameter = (String) parameterFlagBinding.getValue();
                            //System.out.println("Parameter===" + parameter);
                            if (parameter.equalsIgnoreCase("Y") && DispatchQty.compareTo(OrderValid) == 1) {
                              //  System.out.println("intovalidator ifffff");
                                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                              "Dispatch Qty must be less than (Stock-Book) Qty ",
                                                                              null));
                            }
                        } else {
                            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                          "Stock Qty must be greater than book Qty.",
                                                                          null));
                        }
                    }
                }
            }

            //                AdfFacesContext.getCurrentInstance().addPartialTarget(dispatchAdviceTableBinding);
        }


    }

    public void setDispatchh(RichInputText dispatchh) {
        this.dispatchh = dispatchh;
    }

    public RichInputText getDispatchh() {
        return dispatchh;
    }

    public void GrossamtCalc(ValueChangeEvent valueChangeEvent) {
        //     valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        //        ADFUtils.findOperation("setGrossAmt");
    }

    public void setBindRate(RichInputText bindRate) {
        this.bindRate = bindRate;
    }

    public RichInputText getBindRate() {
        return bindRate;
    }

    public void setBindTransMul(RichInputText bindTransMul) {
        this.bindTransMul = bindTransMul;
    }

    public RichInputText getBindTransMul() {
        return bindTransMul;
    }

    public void setTransmultVCL(ValueChangeEvent valueChangeEvent) {
//        System.out.println("bindCustomer.getValue()" + bindCustomer.getValue());
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());

        String StockReq = (String) (stockreq.getValue() != null ? stockreq.getValue() : "N");

        if (StockReq.equalsIgnoreCase("Y")) {
            OperationBinding opz = (OperationBinding) ADFUtils.findOperation("getTransSummm").execute();
        }
        /*  DCIteratorBinding Dcite=ADFUtils.findIterator("DespatchAdviceDetailVO1Iterator");
            DespatchAdviceDetailVORowImpl row=(DespatchAdviceDetailVORowImpl) Dcite.getCurrentRow();
            if(row.getTransStockQty()!=null && row.getTransBookQty()!=null)
            {
                BigDecimal Sub=row.getTransStockQty().subtract(row.getTransBookQty());
                System.out.println("Subbbbbbb"+Sub);
                oracle.jbo.domain.Number TotalQty=(oracle.jbo.domain.Number)row.gettotalQty();
                System.out.println("TotalQtyyyy"+TotalQty);
                if(TotalQty.compareTo(Sub)==1)
                {
                    System.out.println("IntooooooIfffffffValidator");
                        FacesMessage Message = new FacesMessage("Sum of Qty must be less than (Stock-Book) qty.");
                        Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                        FacesContext fc = FacesContext.getCurrentInstance();
                       fc.addMessage(null, Message);
//                        fc.addMessage(dispQuanBinding.getClientId(),Message);

                    }
                } */
        if (bindRate.getValue() != null && dispatchh.getValue() != null) {
            /*  oracle.jbo.domain.Number rate = (oracle.jbo.domain.Number) bindRate.getValue();
            oracle.jbo.domain.Number qty = (oracle.jbo.domain.Number) dispatchh.getValue();
            oracle.jbo.domain.Number oq=rate.multiply(qty);
           bindTransMul.setValue(oq); */
            OperationBinding op = (OperationBinding) ADFUtils.findOperation("setGrossAmt");
            op.execute();
            String Order = (String) (orderTypeBinding.getValue() != null ? orderTypeBinding.getValue() : "R");
            if (!Order.equalsIgnoreCase("E")) {
                valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
                ADFUtils.findOperation("RefreshGstAmountDespatch").execute();
            }
            ADFUtils.findOperation("setTotalCgst").execute();
            ADFUtils.findOperation("setTotalSgst").execute();
            ADFUtils.findOperation("setTotalIgst").execute();
            ADFUtils.findOperation("setNetAmt").execute();
        }
        //       AdfFacesContext.getCurrentInstance().addPartialTarget(dispatchAdviceTableBinding);
    }

    public void setgrossvce(ValueChangeEvent valueChangeEvent) {
        /* valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
OperationBinding op = (OperationBinding) ADFUtils.findOperation("setGrossAmt");
    op.execute(); */
    }

    public void setBindStockQty(RichInputText bindStockQty) {
        this.bindStockQty = bindStockQty;
    }

    public RichInputText getBindStockQty() {
        return bindStockQty;
    }

    public void StockQtyValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (object != null) {
            if (getBindStockQty() != null && getDispatchh() != null) {
                BigDecimal DispatchQty = (BigDecimal) dispatchh.getValue();
                BigDecimal StockQty = (BigDecimal) object;
                if (StockQty != null)  {
               if (DispatchQty.compareTo(StockQty) == 1) {
                        throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                      "Stock Qty must be greater than Dispatch Qty ",
                                                                      null));
                    }
                }
            }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(dispatchAdviceTableBinding);
    }

    public void LovFetchVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce != null && vce.getNewValue().toString().trim().length() > 0) {
            OperationBinding opr = (OperationBinding) ADFUtils.findOperation("LovFetch");
            opr.execute();
            OperationBinding op6 = (OperationBinding) ADFUtils.findOperation("AckAmdTopend");
            op6.execute();
        }
        //       AdfFacesContext.getCurrentInstance().addPartialTarget(dispatchAdviceTableBinding);
        //        if (vce != null && vce.getNewValue().toString().trim().length() > 0) {
        //        //    vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        //            OperationBinding op6 = (OperationBinding) ADFUtils.findOperation("AckAmdTopend");
        //            op6.execute();
        //        }
    }

    public void setBindAckNo(RichInputComboboxListOfValues bindAckNo) {
        this.bindAckNo = bindAckNo;
    }

    public RichInputComboboxListOfValues getBindAckNo() {
        return bindAckNo;
    }

    public void refreshAmtVCL(ValueChangeEvent valueChangeEvent) {
        /*  OperationBinding op6 = (OperationBinding) ADFUtils.findOperation("AckAmdTopend");
        op6.execute(); */
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());

        /* if(prodCodeBinding.getValue()!=null)
        {
            String ProdCode=prodCodeBinding.getValue().toString();
            OperationBinding opp=ADFUtils.findOperation("gethsnnoDispatch");
            opp.getParamsMap().put("ProdCode", ProdCode);

            opp.execute();

            }
            if(hsnNoBinding.getValue()!=null)
            {
                String HsnNo =(String) hsnNoBinding.getValue();
              OperationBinding op = ADFUtils.findOperation("GstCodefromProdcodeDispatch");

                System.out.println("--------Hsnno------"+HsnNo);
                op.getParamsMap().put("HsnNo", HsnNo);
                op.execute();
        } */
        logger.info("bindCustomer.getValue()" + bindCustomer.getValue());
        String Unit = "";
        String vencd = "";
        String Order = (String) (orderTypeBinding.getValue() != null ? orderTypeBinding.getValue() : "R");
        if (!Order.equalsIgnoreCase("E")) {
            valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
            ADFUtils.findOperation("RefreshGstAmountDespatch").execute();
        }
        ADFUtils.findOperation("setTotalCgst").execute();
        ADFUtils.findOperation("setTotalSgst").execute();
        ADFUtils.findOperation("setTotalIgst").execute();
        ADFUtils.findOperation("setNetAmt").execute();
        ADFUtils.findOperation("packcodetokey").execute();

    }

    public void setBindUnit(RichInputComboboxListOfValues bindUnit) {
        this.bindUnit = bindUnit;
    }

    public RichInputComboboxListOfValues getBindUnit() {
        return bindUnit;
    }

    public void setBindCustomer(RichInputComboboxListOfValues bindCustomer) {
        this.bindCustomer = bindCustomer;
    }

    public RichInputComboboxListOfValues getBindCustomer() {
        return bindCustomer;
    }

    public void createLovSetAL(ActionEvent actionEvent) {

        ADFUtils.findOperation("CreateInsert").execute();
        getConsigneeBinding().setDisabled(true);
        getBindCustomer().setDisabled(true);
        getOrderTypeBinding().setDisabled(true);
        getBindDoctype().setDisabled(true);


    }


    public void setCgstAmtBinding(RichInputText cgstAmtBinding) {
        this.cgstAmtBinding = cgstAmtBinding;
    }

    public RichInputText getCgstAmtBinding() {
        return cgstAmtBinding;
    }

    public void setSgstRateBinding(RichInputText sgstRateBinding) {
        this.sgstRateBinding = sgstRateBinding;
    }

    public RichInputText getSgstRateBinding() {
        return sgstRateBinding;
    }

    public void setSgstAmtBinding(RichInputText sgstAmtBinding) {
        this.sgstAmtBinding = sgstAmtBinding;
    }

    public RichInputText getSgstAmtBinding() {
        return sgstAmtBinding;
    }

    public void setIgstRateBinding(RichInputText igstRateBinding) {
        this.igstRateBinding = igstRateBinding;
    }

    public RichInputText getIgstRateBinding() {
        return igstRateBinding;
    }

    public void setIgstAmtBinding(RichInputText igstAmtBinding) {
        this.igstAmtBinding = igstAmtBinding;
    }

    public RichInputText getIgstAmtBinding() {
        return igstAmtBinding;
    }

    public void setTypeBinding(RichInputText typeBinding) {
        this.typeBinding = typeBinding;
    }

    public RichInputText getTypeBinding() {
        return typeBinding;
    }

    public void setAmendmentBinding(RichInputText amendmentBinding) {
        this.amendmentBinding = amendmentBinding;
    }

    public RichInputText getAmendmentBinding() {
        return amendmentBinding;
    }


    public void setAdviceNoBinding(RichInputText adviceNoBinding) {
        this.adviceNoBinding = adviceNoBinding;
    }

    public RichInputText getAdviceNoBinding() {
        return adviceNoBinding;
    }

    public void setEntryDateBinding(RichInputDate entryDateBinding) {
        this.entryDateBinding = entryDateBinding;
    }

    public RichInputDate getEntryDateBinding() {
        return entryDateBinding;
    }

    public void setAviceDateBinding(RichInputDate aviceDateBinding) {
        this.aviceDateBinding = aviceDateBinding;
    }

    public RichInputDate getAviceDateBinding() {
        try {
              System.out.println("set getAviceDateBinding"+ADFUtils.getTodayDate());
              String date=ADFUtils.convertAdToBs(ADFUtils.getTodayDate().toString());
              System.out.println("setter getvaluedate"+date);
              dadatenepalinding.setValue(date.toString());
    //                    DCIteratorBinding dci = ADFUtils.findIterator("SaleOrderHeaderVO1Iterator");
    //                    dci.getCurrentRow().setAttribute("AckDateNepal",date);
          } 
        catch (ParseException e) {
            
          }
        return aviceDateBinding;
    }

    public void setCityBinding(RichInputText cityBinding) {
        this.cityBinding = cityBinding;
    }

    public RichInputText getCityBinding() {
        return cityBinding;
    }

    public void setPlantBinding(RichInputText plantBinding) {
        this.plantBinding = plantBinding;
    }

    public RichInputText getPlantBinding() {
        return plantBinding;
    }

    public void setConsigneeBinding(RichInputComboboxListOfValues consigneeBinding) {
        this.consigneeBinding = consigneeBinding;
    }

    public RichInputComboboxListOfValues getConsigneeBinding() {
        return consigneeBinding;
    }

    public void setConsigneeAddressBinding(RichInputText consigneeAddressBinding) {
        this.consigneeAddressBinding = consigneeAddressBinding;
    }

    public RichInputText getConsigneeAddressBinding() {
        return consigneeAddressBinding;
    }

    public void setStockTypeBinding(RichInputText stockTypeBinding) {
        this.stockTypeBinding = stockTypeBinding;
    }

    public RichInputText getStockTypeBinding() {
        return stockTypeBinding;
    }

    public void setStockTypeDescription(RichInputText stockTypeDescription) {
        this.stockTypeDescription = stockTypeDescription;
    }

    public RichInputText getStockTypeDescription() {
        return stockTypeDescription;
    }

    public void setAuthoriseByBinding(RichInputComboboxListOfValues authoriseByBinding) {
        this.authoriseByBinding = authoriseByBinding;
    }

    public RichInputComboboxListOfValues getAuthoriseByBinding() {
        return authoriseByBinding;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }
    //    private UIComponent getUIComponent(String id){
    //        FacesContext facesCtx = FacesContext.getCurrentInstance();
    //        return findComponent(facesCtx.getViewRoot(), id);
    //    }


    public RichOutputText getBindingOutputText() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
        } else {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
        }
        return bindingOutputTextt;
    }


    public void EditButtonActionAL(ActionEvent actionEvent) {
        DCIteratorBinding binding = ADFUtils.findIterator("DespatchAdviceHeaderVO1Iterator");
        if (binding.getEstimatedRowCount() > 0 && binding.getCurrentRow() != null) {

            DespatchAdviceHeaderVORowImpl row = (DespatchAdviceHeaderVORowImpl) binding.getCurrentRow();
            System.out.println("Status====" + row.getStatus() + statusHeaderBinding.getValue());
            String Status = (String) (row.getStatus() != null ? row.getStatus() : "A");
            /*             if (row.getAuthorisedBy() != null && Status.equalsIgnoreCase("A")) {

                System.out.println("Approved By Not Null");
                RichPopup.PopupHints hints = new RichPopup.PopupHints();
                getEditpopupbinding().show(hints);
                getOrderTypeBinding().setDisabled(true);
                //ADFUtils.setEL("#{pageFlowScope.mode}", "E");
                //ADFUtils.showMessage("This PO is freezed.You can only update the status.", 2);
                //cevmodecheck(); */
            if (row.getAuthorisedBy() != null) {
                ADFUtils.showMessage("Approved Record can't be modified.", 2);
                ADFUtils.setEL("#{pageFlowScope.mode}", "V");
            } else {
                cevmodecheck();
                //                if(verifiedByBinding.getValue()==null ){
                //
                //                    System.out.println("Verified by isssss when null>>>>"+verifiedByBinding.getValue());
                //
                //                    authoriseByBinding.setDisabled(true);
                //                }
                //                else{
                //                    verifiedByBinding.setDisabled(true);
                //
                //                }
                if (checkByBinding.getValue() == null) {

                    System.out.println("Checked by isssss when null>>>>" + checkByBinding.getValue());
                    authoriseByBinding.setDisabled(true);

                } else {

                    checkByBinding.setDisabled(true);
                }
            }
            if (Status.equalsIgnoreCase("C")) {
                String mode = "V";
                System.out.println("mode beforesetting" + ADFUtils.resolveExpression("#{pageFlowScope.mode}"));
                ADFUtils.showMessage("Sale Note Can not modified once cancelled", 2);

                ADFUtils.setEL("#{pageFlowScope.mode}", mode);
                cevmodecheck();
                System.out.println("mode after" + ADFUtils.resolveExpression("#{pageFlowScope.mode}"));
            }
            System.out.println("aprobed" + row.getAuthorisedBy());
            /*                if (row.getAuthorisedBy() != null && row.getAuthorisedBy() != "" &&
                           row.getAuthorisedBy().toString().trim().length() > 0 && Status.equalsIgnoreCase("A")) {
                 System.out.println("Appr by"+row.getAuthorisedBy() );
            RichPopup.PopupHints hints = new RichPopup.PopupHints();
            editpopupbinding.show(hints);
        }
    else
    {

    cevmodecheck();
    } */
        }
    }


    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setBindingOutputTextt(RichOutputText bindingOutputTextt) {
        this.bindingOutputTextt = bindingOutputTextt;
    }

    public RichOutputText getBindingOutputTextt() {
        cevmodecheck();
        return bindingOutputTextt;
    }

    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }

    public void cevModeDisableComponent(String mode) {
        //            FacesContext fctx = FacesContext.getCurrentInstance();
        //            ELContext elctx = fctx.getELContext();
        //            Application jsfApp = fctx.getApplication();
        //            //create a ValueExpression that points to the ADF binding layer
        //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
        //            //
        //            ValueExpression valueExpr = exprFactory.createValueExpression(
        //                                         elctx,
        //                                         "#{pageFlowScope.mode=='E'}",
        //                                          Object.class
        //                                         );
        //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
        if (mode.equals("E")) {
            getAmountBinding().setDisabled(true);
            getBindIncoterm().setDisabled(true);


            getBindModeShip().setDisabled(true);
            getDadatenepalinding().setDisabled(true);
            getCustBalanceBinding().setDisabled(true);
            getBindPayTermLov().setDisabled(true);
            getPreparedByBinding().setDisabled(true);
            getPreparedDate().setDisabled(true);
            getBindUnit().setDisabled(true);
            getBookQty().setDisabled(true);
            getEntryDateBinding().setDisabled(true);
            getCityBinding().setDisabled(true);
            getPlantBinding().setDisabled(true);
            getAdviceNoBinding().setDisabled(true);
            getConsigneeAddressBinding().setDisabled(true);
            getConsiNameBind().setDisabled(true);
            getStockTypeBinding().setDisabled(true);
            getStockTypeDescription().setDisabled(true);
            getAmendmentBinding().setDisabled(true);
            getTypeBinding().setDisabled(true);
            getOrderQuantBinding().setDisabled(true);
            getCgstAmtBinding().setDisabled(true);
            getCgstRateBinding().setDisabled(true);
            getSgstAmtBinding().setDisabled(true);
            getSgstRateBinding().setDisabled(true);
            getIgstAmtBinding().setDisabled(true);
            getIgstRateBinding().setDisabled(true);
            getGrossAmtBinding().setDisabled(true);
            getCgstHeaderBinding().setDisabled(true);
            getSgstHeaderBinding().setDisabled(true);
            getIgstHeaderBinding().setDisabled(true);
            getNetAmountBinding().setDisabled(true);
            getAdviceNoBinding().setDisabled(true);
            getAmtBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            getDetailcreateBinding().setDisabled(true);
            getDetaildeleteBinding().setDisabled(false);
            getCancelDate().setDisabled(true);
            getAviceDateBinding().setDisabled(true);
            getBindCustomer().setDisabled(true);
            getOrderTypeBinding().setDisabled(true);
            getBindDoctype().setDisabled(true);
            getBindStockQty().setDisabled(true);
            getAmdNoBinding().setDisabled(true);
            getAmdDateBinding().setDisabled(true);
            getPlantInsBinding().setDisabled(false);
            getAccountInsBinding().setDisabled(false);
            getUomBinding().setDisabled(true);
            getTotalAmtBinding().setDisabled(true);
            getPlanthoBinding().setDisabled(true);
            if (orderTypeBinding.getValue().toString().equalsIgnoreCase("R")) {
                System.out.println("Disabledddd" + orderTypeBinding.getValue());
                getPortDischargeBindng().setDisabled(true);
                getPortLoading().setDisabled(true);
                getPreCarriage().setDisabled(true);
                getPlaceDelivery().setDisabled(true);
                getRecPreCarriage().setDisabled(true);
            } else {
                System.out.println("DisableddddNot" + orderTypeBinding.getValue());
                getPortDischargeBindng().setDisabled(false);
                getPortLoading().setDisabled(false);
                getPreCarriage().setDisabled(false);
                getPlaceDelivery().setDisabled(false);
                getRecPreCarriage().setDisabled(false);

            }

            getDocSrNoBinding().setDisabled(true);
            getDocRefUnitCodeBinding().setDisabled(true);
            getRefDocNoBinding().setDisabled(true);
            getRefDocTypeBinding().setDisabled(true);
            getDocFileNameBinding().setDisabled(true);
            getRefDocDateBinding().setDisabled(true);
            getDespTotalQtyBinding().setDisabled(true);
            getSoAmdNoBinding().setDisabled(true);
            getDetProdCodeBinding().setDisabled(true);
            getPendingQtyBinding().setDisabled(true);
            getDtlBatchNoBinding().setDisabled(true);
            getDtlExpDateBinding().setDisabled(true);
            getDtlMfgDateBinding().setDisabled(true);
            getDtlPendingQtyBinding().setDisabled(true);
            getTotalBtcQtyBinding().setDisabled(true);
            getSoFreeQtyBinding().setDisabled(true);
            getSoStockQtyBinding().setDisabled(true);
            getSoBookQtyBinding().setDisabled(true);
            getDadtlBatchQtyBinding().setDisabled(true);
            getDaDtlMfgDtBinding().setDisabled(true);
            getDaDtlExpDtBinding().setDisabled(true);
            getSocreateBtnBinding().setDisabled(false);
            getSodeleteBtnBinding().setDisabled(false);
            getPopulateBatchDtlBtnBinding().setDisabled(false);
            getBtDtlCreateBtnBinding().setDisabled(false);
            getBtDtlDeleteBtnBinding().setDisabled(false);
            getSoDtlPoNoBinding().setDisabled(false);
            getNewTotalBatchQtyTransBinding().setDisabled(true);
            getAllBatchPopulateBtnBinding().setDisabled(true);
            getAllPopulateBatchDtlBtnBinding().setDisabled(true);
            getBatchProdNameBinding().setDisabled(true);
            getDaDtlGrossWtBinding().setDisabled(true);
            getSodtlUomBinding().setDisabled(true);
            
            getSoDtlPacckUomBinding().setDisabled(true);
            getSoDtlPackQtyBinding().setDisabled(true);
            getSoDtlProdDescBinding().setDisabled(true);
            
            getPackQtyBinding().setDisabled(true);
            getPackUomBinding().setDisabled(true);
            getNetWeightBinding().setDisabled(true);
            
        } else if (mode.equals("C")) {
            getDadatenepalinding().setDisabled(true);
            getAmountBinding().setDisabled(true);
            getBindIncoterm().setDisabled(true);
            getBindModeShip().setDisabled(true);
            getBindPayTermLov().setDisabled(true);
            getPreparedByBinding().setDisabled(true);
            getPreparedDate().setDisabled(true);
            getCheckByBinding().setDisabled(true);
            //            getVerifiedByBinding().setDisabled(true);
            //            getBindDoctype().setDisabled(true);
            getCustBalanceBinding().setDisabled(true);
            getAmendRemaBinding().setDisabled(true);
            getAuthoriseByBinding().setDisabled(true);
            getBindUnit().setDisabled(true);
            getEntryDateBinding().setDisabled(true);
            getBookQty().setDisabled(true);
            getCityBinding().setDisabled(true);
            getPlantBinding().setDisabled(true);
            getAdviceNoBinding().setDisabled(true);
            getAmtBinding().setDisabled(true);
            getConsigneeAddressBinding().setDisabled(true);
            getConsiNameBind().setDisabled(true);
            getStockTypeBinding().setDisabled(true);
            getStockTypeDescription().setDisabled(true);
            getAmendmentBinding().setDisabled(true);
            getTypeBinding().setDisabled(true);
            getOrderQuantBinding().setDisabled(true);
            getCgstAmtBinding().setDisabled(true);
            getCgstRateBinding().setDisabled(true);
            getSgstAmtBinding().setDisabled(true);
            getSgstRateBinding().setDisabled(true);
            getIgstAmtBinding().setDisabled(true);
            getIgstRateBinding().setDisabled(true);
            getGrossAmtBinding().setDisabled(true);
            getCgstHeaderBinding().setDisabled(true);
            getSgstHeaderBinding().setDisabled(true);
            getIgstHeaderBinding().setDisabled(true);
            getNetAmountBinding().setDisabled(true);
            getStatusHeaderBinding().setDisabled(true);
            getDetailcreateBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            //getDetaildeleteBinding().setDisabled(false);
            getBindStockQty().setDisabled(true);
            getAviceDateBinding().setDisabled(true);
            getAmdNoBinding().setDisabled(true);
            getAmdDateBinding().setDisabled(true);
            getCancelDate().setDisabled(true);
            getPlantInsBinding().setDisabled(false);
            getAccountInsBinding().setDisabled(false);
            getUomBinding().setDisabled(true);
            getPlanthoBinding().setDisabled(true);
            getTotalAmtBinding().setDisabled(true);

            getDocSrNoBinding().setDisabled(true);
            getDocRefUnitCodeBinding().setDisabled(true);
            getRefDocNoBinding().setDisabled(true);
            getRefDocTypeBinding().setDisabled(true);
            getDocFileNameBinding().setDisabled(true);
            getRefDocDateBinding().setDisabled(true);
            getDespTotalQtyBinding().setDisabled(true);
            getSoAmdNoBinding().setDisabled(true);
            getDetProdCodeBinding().setDisabled(true);
            getPendingQtyBinding().setDisabled(true);
            getDtlBatchNoBinding().setDisabled(true);
            getDtlExpDateBinding().setDisabled(true);
            getDtlMfgDateBinding().setDisabled(true);
            getDtlPendingQtyBinding().setDisabled(true);
            getTotalBtcQtyBinding().setDisabled(true);
            getSoFreeQtyBinding().setDisabled(true);
            getSoStockQtyBinding().setDisabled(true);
            getSoBookQtyBinding().setDisabled(true);
            getDadtlBatchQtyBinding().setDisabled(true);
            getDaDtlMfgDtBinding().setDisabled(true);
            getDaDtlExpDtBinding().setDisabled(true);
            getSocreateBtnBinding().setDisabled(true);
            getSodeleteBtnBinding().setDisabled(true);
            getSoDtlPoNoBinding().setDisabled(true);
            getNewTotalBatchQtyTransBinding().setDisabled(true);
            getAllBatchPopulateBtnBinding().setDisabled(false);
            getAllPopulateBatchDtlBtnBinding().setDisabled(false);
            getBatchProdNameBinding().setDisabled(true);
            getDaDtlGrossWtBinding().setDisabled(true);
            
            getSoDtlPacckUomBinding().setDisabled(true);
            getSoDtlPackQtyBinding().setDisabled(true);
            getSoDtlProdDescBinding().setDisabled(true);
            
            getSodtlUomBinding().setDisabled(true);
            getPackQtyBinding().setDisabled(true);
            getPackUomBinding().setDisabled(true);
            getNetWeightBinding().setDisabled(true);
            
        } else if (mode.equals("V")) {
            getPopulateBatchDtlBtnBinding().setDisabled(true);
            getTotalBtcQtyBinding().setDisabled(true);
            getBtDtlCreateBtnBinding().setDisabled(true);
            getBtDtlDeleteBtnBinding().setDisabled(true);
            getSocreateBtnBinding().setDisabled(true);
            getSodeleteBtnBinding().setDisabled(true);
            getPopulateBatchBtnBinding().setDisabled(false);
            getPndingSaleOrderbtnBinding().setDisabled(false);
            getPlantInsBinding().setDisabled(true);
            getAccountInsBinding().setDisabled(true);
            getDetailcreateBinding().setDisabled(true);
            getDetaildeleteBinding().setDisabled(true);
            getHeaderTabBinding().setDisabled(false);
            getItemTabBinding().setDisabled(false);
            getDiscountDetailTab().setDisabled(false);
            getOtherDetail().setDisabled(false);
            getBankDetailTab().setDisabled(false);
            getRefDocTabBinding().setDisabled(false);
            downloadLinkBinding.setDisabled(false);
            getSelectCheckBoxBinding().setDisabled(true);
            getAllBatchPopulateBtnBinding().setDisabled(true);
            getAllPopulateBatchDtlBtnBinding().setDisabled(true);

            getAddManualDaDetailBinding().setVisible(false);
        
        }
    }

    public void setDetailcreateBinding(RichButton detailcreateBinding) {
        this.detailcreateBinding = detailcreateBinding;
    }

    public RichButton getDetailcreateBinding() {
        return detailcreateBinding;
    }

    public void setDetaildeleteBinding(RichButton detaildeleteBinding) {
        this.detaildeleteBinding = detaildeleteBinding;
    }

    public RichButton getDetaildeleteBinding() {
        return detaildeleteBinding;
    }

    public void setCgstRateBinding(RichInputText cgstRateBinding) {
        this.cgstRateBinding = cgstRateBinding;
    }

    public RichInputText getCgstRateBinding() {
        return cgstRateBinding;
    }

    public void setGrossAmtBinding(RichInputText grossAmtBinding) {
        this.grossAmtBinding = grossAmtBinding;
    }

    public RichInputText getGrossAmtBinding() {
        return grossAmtBinding;
    }

    public void setSgstHeaderBinding(RichInputText sgstHeaderBinding) {
        this.sgstHeaderBinding = sgstHeaderBinding;
    }

    public RichInputText getSgstHeaderBinding() {
        return sgstHeaderBinding;
    }

    public void setCgstHeaderBinding(RichInputText cgstHeaderBinding) {
        this.cgstHeaderBinding = cgstHeaderBinding;
    }

    public RichInputText getCgstHeaderBinding() {
        return cgstHeaderBinding;
    }

    public void setIgstHeaderBinding(RichInputText igstHeaderBinding) {
        this.igstHeaderBinding = igstHeaderBinding;
    }

    public RichInputText getIgstHeaderBinding() {
        return igstHeaderBinding;
    }

    public void setNetAmountBinding(RichInputText netAmountBinding) {
        this.netAmountBinding = netAmountBinding;
    }

    public RichInputText getNetAmountBinding() {
        return netAmountBinding;
    }

    public void setStatusHeaderBinding(RichSelectOneChoice statusHeaderBinding) {
        this.statusHeaderBinding = statusHeaderBinding;
    }

    public RichSelectOneChoice getStatusHeaderBinding() {
        return statusHeaderBinding;
    }

    public void setOrderTypeBinding(RichInputComboboxListOfValues orderTypeBinding) {
        this.orderTypeBinding = orderTypeBinding;
    }

    public RichInputComboboxListOfValues getOrderTypeBinding() {
        return orderTypeBinding;
    }

    public void setProdCodeBinding(RichInputComboboxListOfValues prodCodeBinding) {
        this.prodCodeBinding = prodCodeBinding;
    }

    public RichInputComboboxListOfValues getProdCodeBinding() {
        return prodCodeBinding;
    }

    public void setHsnNoBinding(RichInputText hsnNoBinding) {
        this.hsnNoBinding = hsnNoBinding;
    }

    public RichInputText getHsnNoBinding() {
        return hsnNoBinding;
    }

    public void CheckAuthorityVCE(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (valueChangeEvent != null) {

            OperationBinding opr = (OperationBinding) ADFUtils.findOperation("checkApprovalFunctionStatusForSaleNote");
            opr.getParamsMap().put("formNm", "FINDA");
            opr.getParamsMap().put("empcd", valueChangeEvent.getNewValue());
            opr.getParamsMap().put("authoLim", "AP");
            opr.getParamsMap().put("UnitCd", bindUnit.getValue());
            opr.getParamsMap().put("Amount", null);
            opr.getParamsMap().put("ProdType", null);
            opr.execute();
            System.out.println("Result is===============>" + opr.getResult());
            if ((opr.getResult() != null && !opr.getResult().equals("Y"))) {
                Row row = (Row) ADFUtils.evaluateEL("#{bindings.DespatchAdviceHeaderVO1Iterator.currentRow}");
                row.setAttribute("AuthorisedBy", null);
                ADFUtils.showMessage(opr.getResult().toString(), 0);
            }

            ///ProdType

            //    valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
            //   OperationBinding op=ADFUtils.findOperation("CheckAuthority");
            //                 op.getParamsMap().put("unitCd",bindUnit.getValue());
            //                 op.getParamsMap().put("EmpCd", authoriseByBinding.getValue());
            //                 op.execute();

        }
    }

    public void setBookQty(RichInputText bookQty) {
        this.bookQty = bookQty;
    }

    public RichInputText getBookQty() {
        return bookQty;
    }

    public void setParameterFlagBind(RichColumn parameterFlagBind) {
        this.parameterFlagBind = parameterFlagBind;
    }

    public RichColumn getParameterFlagBind() {
        return parameterFlagBind;
    }

    public void setParameterFlagBinding(RichInputText parameterFlagBinding) {
        this.parameterFlagBinding = parameterFlagBinding;
    }

    public RichInputText getParameterFlagBinding() {
        return parameterFlagBinding;
    }

    public void setConsignee(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce.getNewValue() != null) 
        {
            System.out.println("Customerif not " + bindCustomer.getValue());
            String Customer = (String) bindCustomer.getValue();
            if (Customer != null) {
                System.out.println("Customer is===>" + Customer);
                consigneeBinding.setValue(Customer);
            }
            AdfFacesContext.getCurrentInstance().addPartialTarget(pendSODetailPopupBinding);
        }
    }

    public void setBindTotalQty(RichInputText bindTotalQty) {
        this.bindTotalQty = bindTotalQty;
    }

    public RichInputText getBindTotalQty() {
        return bindTotalQty;
    }

    public String SaveAndCloseACtion() 
    {
        if((Long)ADFUtils.evaluateEL("#{bindings.DespatchAdviceDetailVO1Iterator.estimatedRowCount}")>0)
        {
            OperationBinding op = ADFUtils.findOperation("generateDano");
            op.execute();
            System.out.println("Result is------> "+op.getResult());
            if(op.getResult() != null && op.getResult().toString().equalsIgnoreCase("E"))
            {
                    ADFUtils.findOperation("Commit").execute();
                    ADFUtils.showMessage(" Record Updated Successfully", 2);
    //                ADFUtils.setEL("#{pageFlowScope.mode}", "V");
    //                cevmodecheck();
    //                AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
                    return "Save And Close";
            }
            else if (op.getResult() != null && op.getResult().toString() != ""  && op.getResult().equals("S"))
             {
                    logger.info("+++++ in the save modein save and close");
                    ADFUtils.findOperation("Commit").execute();
                    ADFUtils.showMessage("Record Saved Successfully.Advise No.is "+ADFUtils.evaluateEL("#{bindings.DaNo.inputValue}"), 2);  
    //                ADFUtils.setEL("#{pageFlowScope.mode}", "V");
    //                cevmodecheck();
    //                AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
                    return "Save And Close";
            }
            else if(op.getResult() != null && op.getResult().toString().equalsIgnoreCase("N"))
            {
                 ADFUtils.showMessage("Dispatch Advise could not be generated.Try Again !!", 0);
                 return null;
            }
        }else{
            ADFUtils.showMessage("Enter Details in Dispatch Advise Detail.", 0);
            return null;
        }
        return null;
    }


    public void setHeaderTabBinding(RichShowDetailItem headerTabBinding) {
        this.headerTabBinding = headerTabBinding;
    }

    public RichShowDetailItem getHeaderTabBinding() {
        return headerTabBinding;
    }

    public void setItemTabBinding(RichShowDetailItem itemTabBinding) {
        this.itemTabBinding = itemTabBinding;
    }

    public RichShowDetailItem getItemTabBinding() {
        return itemTabBinding;
    }

    public void setDiscountDetailTab(RichShowDetailItem discountDetailTab) {
        this.discountDetailTab = discountDetailTab;
    }

    public RichShowDetailItem getDiscountDetailTab() {
        return discountDetailTab;
    }

    public void setOtherDetail(RichShowDetailItem otherDetail) {
        this.otherDetail = otherDetail;
    }

    public RichShowDetailItem getOtherDetail() {
        return otherDetail;
    }

    public void setBankDetailTab(RichShowDetailItem bankDetailTab) {
        this.bankDetailTab = bankDetailTab;
    }

    public RichShowDetailItem getBankDetailTab() {
        return bankDetailTab;
    }

    public void StatusVCE(ValueChangeEvent vce) {
        if (vce != null) {
            vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
            String StatusHeader = (String) vce.getNewValue();
            System.out.println("StatusHeader" + StatusHeader);
            OperationBinding op = ADFUtils.findOperation("SetStatusheadertoDtlDespatchAdvice");
            op.getParamsMap().put("Stutus", StatusHeader);
            op.execute();
     //       statusDtlBinding.setDisabled(true);

            System.out.println("Statusssss" + StatusHeader);
            oracle.jbo.domain.Date dt = (oracle.jbo.domain.Date) new oracle.jbo.domain.Date();
            dt = (oracle.jbo.domain.Date) dt.getCurrentDate();
            System.out.println("Cureeentdate==========>" + dt);
            if (StatusHeader.equalsIgnoreCase("C")) {

                RichPopup.PopupHints hints = new RichPopup.PopupHints();
                statusPopupBinding.show(hints);
                cancelDate.setValue(dt);

            }

        }
    }

    public void PortDisableVCE(ValueChangeEvent vce) {
        if (vce.getNewValue() != null) {
            vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
            System.out.println("vce" + vce.getNewValue());
            if (vce.getNewValue().toString().equalsIgnoreCase("R")) {
                getPortDischargeBindng().setDisabled(true);
                getPortLoading().setDisabled(true);
                getPreCarriage().setDisabled(true);
                getPlaceDelivery().setDisabled(true);
                getRecPreCarriage().setDisabled(true);
            } else {
                getPortDischargeBindng().setDisabled(false);
                getPortLoading().setDisabled(false);
                getPreCarriage().setDisabled(false);
                getPlaceDelivery().setDisabled(false);
                getRecPreCarriage().setDisabled(false);

            }
        }

    }

    public void setCancelDate(RichInputDate cancelDate) {
        this.cancelDate = cancelDate;
    }

    public RichInputDate getCancelDate() {
        return cancelDate;
    }

    public void setStatusPopupBinding(RichPopup statusPopupBinding) {
        this.statusPopupBinding = statusPopupBinding;
    }

    public RichPopup getStatusPopupBinding() {
        return statusPopupBinding;
    }

    public void amdConfirmDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {

            OperationBinding op1 = ADFUtils.findOperation("checkMaxAmendmentDespach");
            Object ob = op1.execute();
            if (op1.getResult().toString() != null && op1.getResult().toString().equalsIgnoreCase("N")) {

                ADFUtils.showMessage("You Can Update Latest Amendment Only.", 2);
                ADFUtils.setEL("#{pageFlowScope.mode}", "V");
            } else {

                System.out.println("Da===" + ADFUtils.resolveExpression("#{pageFlowScope.DaNo}"));
                System.out.println("Daidd===" + ADFUtils.resolveExpression("#{pageFlowScope.DaId}"));
                System.out.println("AmdNo===" + ADFUtils.resolveExpression("#{pageFlowScope.AmdNo}"));
                OperationBinding op = ADFUtils.findOperation("getAmdDespatchDetails");
                op.getParamsMap().put("AmdNo", ADFUtils.resolveExpression("#{pageFlowScope.AmdNo}"));
                op.getParamsMap().put("DaId", ADFUtils.resolveExpression("#{pageFlowScope.DaId}"));
                op.getParamsMap().put("DaNo", ADFUtils.resolveExpression("#{pageFlowScope.DaNo}"));
                op.execute();
                System.out.println("orderTypeBinding.getValue()" + orderTypeBinding.getValue());

                cevmodecheck();
                AdfFacesContext.getCurrentInstance().addPartialTarget(dispatchAdviceTableBinding);
            }
        } else
            getEditpopupbinding().hide();
        ADFUtils.setEL("#{pageFlowScope.mode}", "V");
    }

    public void setCheckByBinding(RichInputComboboxListOfValues checkByBinding) {
        this.checkByBinding = checkByBinding;
    }

    public RichInputComboboxListOfValues getCheckByBinding() {
        return checkByBinding;
    }


    public void setAccountInsBinding(RichButton accountInsBinding) {
        this.accountInsBinding = accountInsBinding;
    }

    public RichButton getAccountInsBinding() {
        return accountInsBinding;
    }

    public void setPlantInsBinding(RichButton plantInsBinding) {
        this.plantInsBinding = plantInsBinding;
    }

    public RichButton getPlantInsBinding() {
        return plantInsBinding;
    }

    public void setInsAccBinding(RichInputText insAccBinding) {
        this.insAccBinding = insAccBinding;
    }

    public RichInputText getInsAccBinding() {
        return insAccBinding;
    }

    public void setInsPlantBinding(RichInputText insPlantBinding) {
        this.insPlantBinding = insPlantBinding;
    }

    public RichInputText getInsPlantBinding() {
        return insPlantBinding;
    }

    public void setConsiNameBind(RichInputText consiNameBind) {
        this.consiNameBind = consiNameBind;
    }

    public RichInputText getConsiNameBind() {
        return consiNameBind;
    }

    public void setEditpopupbinding(RichPopup editpopupbinding) {
        this.editpopupbinding = editpopupbinding;
    }

    public RichPopup getEditpopupbinding() {
        return editpopupbinding;
    }

    public void setAmtBinding(RichInputText amtBinding) {
        this.amtBinding = amtBinding;
    }

    public RichInputText getAmtBinding() {
        return amtBinding;
    }

    public void setAmdNoBinding(RichInputText amdNoBinding) {
        this.amdNoBinding = amdNoBinding;
    }

    public RichInputText getAmdNoBinding() {
        return amdNoBinding;
    }

    public void setAmdDateBinding(RichInputDate amdDateBinding) {
        this.amdDateBinding = amdDateBinding;
    }

    public RichInputDate getAmdDateBinding() {
        return amdDateBinding;
    }

    public void setAmendRemaBinding(RichInputText amendRemaBinding) {
        this.amendRemaBinding = amendRemaBinding;
    }

    public RichInputText getAmendRemaBinding() {
        return amendRemaBinding;
    }

    public void setPreCarriage(RichInputComboboxListOfValues preCarriage) {
        this.preCarriage = preCarriage;
    }

    public RichInputComboboxListOfValues getPreCarriage() {
        return preCarriage;
    }

    public void setPortDischargeBindng(RichInputComboboxListOfValues portDischargeBindng) {
        this.portDischargeBindng = portDischargeBindng;
    }

    public RichInputComboboxListOfValues getPortDischargeBindng() {
        return portDischargeBindng;
    }

    public void setRecPreCarriage(RichInputComboboxListOfValues recPreCarriage) {
        this.recPreCarriage = recPreCarriage;
    }

    public RichInputComboboxListOfValues getRecPreCarriage() {
        return recPreCarriage;
    }


    public void setPortLoading(RichInputComboboxListOfValues portLoading) {
        this.portLoading = portLoading;
    }

    public RichInputComboboxListOfValues getPortLoading() {
        return portLoading;
    }

    public void setPlaceDelivery(RichInputComboboxListOfValues placeDelivery) {
        this.placeDelivery = placeDelivery;
    }

    public RichInputComboboxListOfValues getPlaceDelivery() {
        return placeDelivery;
    }

    public void setInscompname(RichInputText inscompname) {
        this.inscompname = inscompname;
    }

    public RichInputText getInscompname() {
        return inscompname;
    }

    public void setValidfrombinding(RichInputDate validfrombinding) {
        this.validfrombinding = validfrombinding;
    }

    public RichInputDate getValidfrombinding() {
        return validfrombinding;
    }


    public void setStockreq(RichInputText stockreq) {
        this.stockreq = stockreq;
    }

    public RichInputText getStockreq() {
        return stockreq;
    }

    public void setUomBinding(RichInputText uomBinding) {
        this.uomBinding = uomBinding;
    }

    public RichInputText getUomBinding() {
        return uomBinding;
    }

    public void setTotalAmtBinding(RichInputText totalAmtBinding) {
        this.totalAmtBinding = totalAmtBinding;
    }

    public RichInputText getTotalAmtBinding() {
        return totalAmtBinding;
    }

    public void prodcodeVCL(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());

        if (prodCodeBinding.getValue() != null) {
            String ProdCode = prodCodeBinding.getValue().toString();
            OperationBinding opp = ADFUtils.findOperation("gethsnnoDispatch");
            opp.getParamsMap().put("ProdCode", ProdCode);

            opp.execute();

        }
        if (hsnNoBinding.getValue() != null) {
            String HsnNo = (String) prodCodeBinding.getValue();
            OperationBinding op = ADFUtils.findOperation("GstCodefromProdcodeDispatch");

            System.out.println("--------prodCodeBinding------" + HsnNo);
            op.getParamsMap().put("HsnNo", HsnNo);
            op.execute();
        }
    }

    public void setPreparedByBinding(RichInputComboboxListOfValues preparedByBinding) {
        this.preparedByBinding = preparedByBinding;
    }

    public RichInputComboboxListOfValues getPreparedByBinding() {
        return preparedByBinding;
    }

    public void setPreparedDate(RichInputDate preparedDate) {
        this.preparedDate = preparedDate;
    }

    public RichInputDate getPreparedDate() {
        return preparedDate;
    }

    public void checkedByVCE(ValueChangeEvent valueChangeEvent) {
        System.out.println("checkbybean");
        if (valueChangeEvent != null) {
            valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
            OperationBinding op = ADFUtils.findOperation("CheckByAuthority");
            op.getParamsMap().put("unitCd", bindUnit.getValue());
            op.getParamsMap().put("EmpCd", checkByBinding.getValue());
            op.execute();

        }
    }

    public void SaveButtonAL(ActionEvent actionEvent) {
        /*         System.out.println("INTOSAVE");
        System.out.println("Object in bean ===" + validdate.getValue());
        OperationBinding op1 = (OperationBinding) ADFUtils.findOperation("validuptoDespatch");
        op1.getParamsMap().put("ValidDes", validdate.getValue());
        op1.execute();
        System.out.println("Result" + op1.getResult());
        if (op1.getResult() != null && op1.getResult().toString().equalsIgnoreCase("Y")) {
            ADFUtils.showMessage("Validity of Checklist is expired.So You Can not Make Sale Note with this Validity.",
                                 0);
                        throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Validity of Checklist is expired.So You Can not Make Sale Note with this Validity.", null));
                        FacesMessage Message = new FacesMessage("Validity of Checklist is expired.So You Can not Make Sale Note with this Validity.");
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);
                        FacesContext fc = FacesContext.getCurrentInstance();
                        fc.addMessage(null, Message);
                        ADFUtils.setEL("#{pageFlowScope.mode}","V");
                        cevmodecheck();
            AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);

        }

        else { */
          //  DCIteratorBinding Dcite = ADFUtils.findIterator("DespatchAdviceDetailVO1Iterator");
            //            DespatchAdviceDetailVORowImpl row=(DespatchAdviceDetailVORowImpl) Dcite.getCurrentRow();
            //            if(row.getTransStockQty()!=null && row.getTransBookQty()!=null)
            //            {
            //                BigDecimal Sub=row.getTransStockQty().subtract(row.getTransBookQty());
            //                System.out.println("Subbbbbbb"+Sub);
            //                oracle.jbo.domain.Number TotalQty=(oracle.jbo.domain.Number)row.gettotalQty();
            //                System.out.println("TotalQtyyyy"+TotalQty);
            //                if(TotalQty.compareTo(Sub)==-1 || TotalQty.compareTo(Sub)==0)
            //                {
        if((Long)ADFUtils.evaluateEL("#{bindings.DespatchAdviceDetailVO1Iterator.estimatedRowCount}")>0)
        {
            OperationBinding op = ADFUtils.findOperation("generateDano");
            op.execute();
            logger.info("Result is------> "+op.getResult());
            if(op.getResult() != null && op.getResult().toString().equalsIgnoreCase("E"))
            {
                    ADFUtils.findOperation("Commit").execute();
                    ADFUtils.showMessage(" Record Updated Successfully", 2);
                    ADFUtils.setEL("#{pageFlowScope.mode}", "V");
                    cevmodecheck();
                    AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
            }
            else if (op.getResult() != null && op.getResult().toString() != ""  && op.getResult().equals("S"))
             {
                    logger.info("+++++ in the save mode+++++++++++");
                    ADFUtils.findOperation("Commit").execute();
                    ADFUtils.showMessage("Record Saved Successfully.Advise No.is "+ADFUtils.evaluateEL("#{bindings.DaNo.inputValue}"), 2);  
                    ADFUtils.setEL("#{pageFlowScope.mode}", "V");
                    cevmodecheck();
                    AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
            }
            else if(op.getResult() != null && op.getResult().toString().equalsIgnoreCase("N"))
            {
                 ADFUtils.showMessage("Dispatch Advise could not be generated.Try Again !!", 0);
            }
        }else{
                ADFUtils.showMessage("Enter Details in Dispatch Advise Detail.", 0);
            }
    }

    public void setPlanthoBinding(RichInputText planthoBinding) {
        this.planthoBinding = planthoBinding;
    }

    public RichInputText getPlanthoBinding() {
        return planthoBinding;
    }

    public void setPathBinding(RichInputText pathBinding) {
        this.pathBinding = pathBinding;
    }

    public RichInputText getPathBinding() {
        return pathBinding;
    }

    public void downloadAction(FacesContext facesContext, OutputStream outputStream) throws Exception {

        File filed = new File(pathBinding.getValue().toString());
        FileInputStream fis;
        byte[] b;
        try {
            fis = new FileInputStream(filed);

            int n;
            while ((n = fis.available()) > 0) {
                b = new byte[n];
                int result = fis.read(b);
                outputStream.write(b, 0, b.length);
                if (result == -1)
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        outputStream.flush();

    }


    public void uploadAction(ValueChangeEvent vce) {
        // Add event code here...
        System.out.println("New Value In VCE Of Upload File" + vce.getNewValue());
        if (vce.getNewValue() != null) {
            //Get File Object from VC Event
            UploadedFile fileVal = (UploadedFile) vce.getNewValue();
            //Upload File to path- Return actual server path
            String path = ADFUtils.uploadFile(fileVal);
            logger.info(fileVal.getContentType());
            logger.info("Content Type==>>" + fileVal.getContentType() + "Path==>>" + path + "File name==>>" +
                               fileVal.getFilename());
            //Method to insert data in table to keep track of uploaded files
            OperationBinding ob = ADFUtils.findOperation("setFileDataDespatchAdv");
            ob.getParamsMap().put("name", fileVal.getFilename());
            ob.getParamsMap().put("path", path);
            ob.getParamsMap().put("contTyp", fileVal.getContentType());
            ob.getParamsMap().put("SrNo",
                                  ADFUtils.evaluateEL("#{bindings.DocAttachRefDtlVO3Iterator.estimatedRowCount+1}"));
            ob.execute();
            // Reset inputFile component after upload
            ResetUtils.reset(vce.getComponent());
        }
    }

    public void setDocSrNoBinding(RichInputText docSrNoBinding) {
        this.docSrNoBinding = docSrNoBinding;
    }

    public RichInputText getDocSrNoBinding() {
        return docSrNoBinding;
    }

    public void setDocRefUnitCodeBinding(RichInputText docRefUnitCodeBinding) {
        this.docRefUnitCodeBinding = docRefUnitCodeBinding;
    }

    public RichInputText getDocRefUnitCodeBinding() {
        return docRefUnitCodeBinding;
    }

    public void setRefDocNoBinding(RichInputText refDocNoBinding) {
        this.refDocNoBinding = refDocNoBinding;
    }

    public RichInputText getRefDocNoBinding() {
        return refDocNoBinding;
    }

    public void setRefDocTypeBinding(RichInputText refDocTypeBinding) {
        this.refDocTypeBinding = refDocTypeBinding;
    }

    public RichInputText getRefDocTypeBinding() {
        return refDocTypeBinding;
    }

    public void setDocFileNameBinding(RichInputText docFileNameBinding) {
        this.docFileNameBinding = docFileNameBinding;
    }

    public RichInputText getDocFileNameBinding() {
        return docFileNameBinding;
    }

    public void setRefDocDateBinding(RichInputDate refDocDateBinding) {
        this.refDocDateBinding = refDocDateBinding;
    }

    public RichInputDate getRefDocDateBinding() {
        return refDocDateBinding;
    }

    public void refDocDelPopupDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete1").execute();
            ADFUtils.showMessage("Record Deleted Successfully", 2);

        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(docRefDetailBinding);
    }


    public void setDocRefDetailBinding(RichTable docRefDetailBinding) {
        this.docRefDetailBinding = docRefDetailBinding;
    }

    public RichTable getDocRefDetailBinding() {
        return docRefDetailBinding;
    }

    public void setRefDocTabBinding(RichShowDetailItem refDocTabBinding) {
        this.refDocTabBinding = refDocTabBinding;
    }

    public RichShowDetailItem getRefDocTabBinding() {
        return refDocTabBinding;
    }

    public void setDownloadLinkBinding(RichCommandLink downloadLinkBinding) {
        this.downloadLinkBinding = downloadLinkBinding;
    }

    public RichCommandLink getDownloadLinkBinding() {
        return downloadLinkBinding;
    }

    public void validDateValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        System.out.println("Object===" + object);
        if (object != null) {
            System.out.println("Sale Note date===" + aviceDateBinding.getValue());
            oracle.jbo.domain.Date Saledate = (oracle.jbo.domain.Date) aviceDateBinding.getValue();
            oracle.jbo.domain.Date Validdate = (oracle.jbo.domain.Date) object;
            //               System.out.println("Sale Note date==="+Saledate +"Validity=="+Validdate+"validation==="+Validdate.compareTo(Saledate));
            if (Validdate.compareTo(Saledate) == -1) {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                              "Valid Upto must be greater ot equal to Sale Note Date.",
                                                              null));

            }
        }

    }

    public void setValiddate(RichInputDate validdate) {
        this.validdate = validdate;
    }

    public RichInputDate getValiddate() {
        return validdate;
    }

    public void setBindIncoterm(RichSelectOneChoice bindIncoterm) {
        this.bindIncoterm = bindIncoterm;
    }

    public RichSelectOneChoice getBindIncoterm() {
        return bindIncoterm;
    }

    public void setBindPayTerm(RichInputText bindPayTerm) {
        this.bindPayTerm = bindPayTerm;
    }

    public RichInputText getBindPayTerm() {
        return bindPayTerm;
    }

    public void setBindModeShip(RichSelectOneChoice bindModeShip) {
        this.bindModeShip = bindModeShip;
    }

    public RichSelectOneChoice getBindModeShip() {
        return bindModeShip;
    }

    public void setBindPayTermLov(RichInputComboboxListOfValues bindPayTermLov) {
        this.bindPayTermLov = bindPayTermLov;
    }

    public RichInputComboboxListOfValues getBindPayTermLov() {
        return bindPayTermLov;
    }

    public void setAmountBinding(RichInputText amountBinding) {
        this.amountBinding = amountBinding;
    }

    public RichInputText getAmountBinding() {
        return amountBinding;
    }

    public void populateDetailsAL(ActionEvent actionEvent) 
    {
        if(!(ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")))
        {
            OperationBinding opp = ADFUtils.findOperation("getPopulateRowWiseBatchDetails");
            Object obb = opp.execute();
            System.out.println("populateDetailsAL,Return obb---->"+opp.getResult());
            if (opp.getErrors() != null && opp.getResult().equals("Success")) 
            {
                ADFUtils.showPopup(daBatchDetTableBinding);
                AdfFacesContext.getCurrentInstance().addPartialTarget(despTotalQtyBinding);
                AdfFacesContext.getCurrentInstance().addPartialTarget(transSoManualFlagBinding);
                ADFUtils.findOperation("Execute").execute();
                System.out.println("Manual flag is----->>"+transSoManualFlagBinding.getValue());
                if(transSoManualFlagBinding.getValue()!=null)
                {
                    if("M".equalsIgnoreCase(transSoManualFlagBinding.getValue().toString()))
                    {
                        getPopulateBatchDtlBtnBinding().setVisible(false);
                        getAddManualDaDetailBinding().setVisible(true);
                    }else{
                        getPopulateBatchDtlBtnBinding().setVisible(true);
                        getAddManualDaDetailBinding().setVisible(false);
                    }
                    AdfFacesContext.getCurrentInstance().addPartialTarget(populateBatchDtlBtnBinding);
                    AdfFacesContext.getCurrentInstance().addPartialTarget(addManualDaDetailBinding);
                }
            }else
            {
                ADFUtils.showMessage("This product is not selected.", 0);
            }
        }else{
            ADFUtils.showPopup(daBatchDetTableBinding);
        }
        /*if (detProdCodeBinding.getValue() != null) {
        OperationBinding op = ADFUtils.findOperation("batchDataPopulateWithQty");
        op.getParamsMap().put("productCode", detProdCodeBinding.getValue());
        op.getParamsMap().put("despQty", despQtyBinding.getValue());
        op.execute(); 
        ADFUtils.showPopup(daBatchDetTableBinding);
        AdfFacesContext.getCurrentInstance().addPartialTarget(despTotalQtyBinding);
        ADFUtils.findOperation("Execute").execute();
        } else {
        ADFUtils.showMessage("Please select Sale Order No.", 0);
        } */
    }

    public void setDetProdCodeBinding(RichInputText detProdCodeBinding) {
        this.detProdCodeBinding = detProdCodeBinding;
    }

    public RichInputText getDetProdCodeBinding() {
        return detProdCodeBinding;
    }

    public void setDaBatchDetTableBinding(RichPopup daBatchDetTableBinding) {
        this.daBatchDetTableBinding = daBatchDetTableBinding;
    }

    public RichPopup getDaBatchDetTableBinding() {
        return daBatchDetTableBinding;
    }

    public void setPendingQtyBinding(RichInputText pendingQtyBinding) {
        this.pendingQtyBinding = pendingQtyBinding;
    }

    public RichInputText getPendingQtyBinding() {
        return pendingQtyBinding;
    }

    public void setDespQtyBinding(RichInputText despQtyBinding) {
        this.despQtyBinding = despQtyBinding;
    }

    public RichInputText getDespQtyBinding() {
        return despQtyBinding;
    }


  public void totalBatchQtyValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if(object !=null && getPendingQtyBinding()!= null) 
        {
            BigDecimal pendQty = (BigDecimal) dtlPendingQtyBinding.getValue();
            BigDecimal BQty = (BigDecimal)object;
            if(pendQty.compareTo(BQty)<0)
            {
              throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Quantity must be less than Pending Qty.", null));
            }else if(BQty.compareTo(new BigDecimal(0)) < 0){
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Quantity must be greater or equal to zero.", null));
            }
        }
    }
    public void setDespTotalQtyBinding(RichInputText despTotalQtyBinding) {
        this.despTotalQtyBinding = despTotalQtyBinding;
    }

    public RichInputText getDespTotalQtyBinding() {
        return despTotalQtyBinding;
    }

    public void setPndingSaleOrderbtnBinding(RichButton pndingSaleOrderbtnBinding) {
        this.pndingSaleOrderbtnBinding = pndingSaleOrderbtnBinding;
    }

    public RichButton getPndingSaleOrderbtnBinding() {
        return pndingSaleOrderbtnBinding;
    }

    public void setSoAmdNoBinding(RichInputText soAmdNoBinding) {
        this.soAmdNoBinding = soAmdNoBinding;
    }

    public RichInputText getSoAmdNoBinding() {
        return soAmdNoBinding;
    }

    public void setSocreateBtnBinding(RichButton socreateBtnBinding) {
        this.socreateBtnBinding = socreateBtnBinding;
    }

    public RichButton getSocreateBtnBinding() {
        return socreateBtnBinding;
    }

    public void setSodeleteBtnBinding(RichButton sodeleteBtnBinding) {
        this.sodeleteBtnBinding = sodeleteBtnBinding;
    }

    public RichButton getSodeleteBtnBinding() {
        return sodeleteBtnBinding;
    }

    public void setPopulateBatchBtnBinding(RichButton populateBatchBtnBinding) {
        this.populateBatchBtnBinding = populateBatchBtnBinding;
    }

    public RichButton getPopulateBatchBtnBinding() {
        return populateBatchBtnBinding;
    }

    public void setDtlBatchNoBinding(RichInputText dtlBatchNoBinding) {
        this.dtlBatchNoBinding = dtlBatchNoBinding;
    }

    public RichInputText getDtlBatchNoBinding() {
        return dtlBatchNoBinding;
    }

    public void setDtlMfgDateBinding(RichInputText dtlMfgDateBinding) {
        this.dtlMfgDateBinding = dtlMfgDateBinding;
    }

    public RichInputText getDtlMfgDateBinding() {
        return dtlMfgDateBinding;
    }

    public void setDtlExpDateBinding(RichInputDate dtlExpDateBinding) {
        this.dtlExpDateBinding = dtlExpDateBinding;
    }

    public RichInputDate getDtlExpDateBinding() {
        return dtlExpDateBinding;
    }

    public void setDtlPendingQtyBinding(RichInputText dtlPendingQtyBinding) {
        this.dtlPendingQtyBinding = dtlPendingQtyBinding;
    }

    public RichInputText getDtlPendingQtyBinding() {
        return dtlPendingQtyBinding;
    }

    public void setDtlBatchQtyBinding(RichInputText dtlBatchQtyBinding) {
        this.dtlBatchQtyBinding = dtlBatchQtyBinding;
    }

    public RichInputText getDtlBatchQtyBinding() {
        return dtlBatchQtyBinding;
    }

    public void setBtDtlCreateBtnBinding(RichButton btDtlCreateBtnBinding) {
        this.btDtlCreateBtnBinding = btDtlCreateBtnBinding;
    }

    public RichButton getBtDtlCreateBtnBinding() {
        return btDtlCreateBtnBinding;
    }

    public void setBtDtlDeleteBtnBinding(RichButton btDtlDeleteBtnBinding) {
        this.btDtlDeleteBtnBinding = btDtlDeleteBtnBinding;
    }

    public RichButton getBtDtlDeleteBtnBinding() {
        return btDtlDeleteBtnBinding;
    }

    public void setTotalBtcQtyBinding(RichInputText totalBtcQtyBinding) {
        this.totalBtcQtyBinding = totalBtcQtyBinding;
    }

    public RichInputText getTotalBtcQtyBinding() {
        return totalBtcQtyBinding;
    }

    public void batchDtlPopPopulateAL(ActionEvent actionEvent) 
    {
        BigDecimal despTotQty = (BigDecimal) despTotalQtyBinding.getValue()!=null?(BigDecimal) despTotalQtyBinding.getValue():new BigDecimal(0);
        BigDecimal totBtcQty = (BigDecimal)totalBtcQtyBinding.getValue()!=null?(BigDecimal)totalBtcQtyBinding.getValue():new BigDecimal(0);

        if(despTotQty.compareTo(totBtcQty)==0)
        {
            System.out.println("Yes Execute Method-----");
            OperationBinding opp = ADFUtils.findOperation("updateBatchQtyInDaDetail");
            Object obb = opp.execute();
            System.out.println("Return true---->"+opp.getResult());
            if (opp.getErrors() != null && opp.getResult().equals("NM"))
            {
                getDaBatchDetTableBinding().cancel();
                getPendSODetailPopupBinding().cancel();
            }
            else{
                System.out.println("Error Error------");
            }

//            getPopulateBatchDtlBtnBinding().setDisabled(true);
//            AdfFacesContext.getCurrentInstance().addPartialTarget(populateBatchDtlBtnBinding);
        }
        else{
            FacesMessage message = new FacesMessage("Despatch Qty and Quantity must be equal.");
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(dtlBatchQtyBinding.getClientId(), message);
        }
    }

    public void btcDtlPopCancelAL(ActionEvent actionEvent) {
            getDaBatchDetTableBinding().cancel();
    }

    public void setPopulateBatchDtlBtnBinding(RichButton populateBatchDtlBtnBinding) {
        this.populateBatchDtlBtnBinding = populateBatchDtlBtnBinding;
    }

    public RichButton getPopulateBatchDtlBtnBinding() {
        return populateBatchDtlBtnBinding;
    }

    public void setSoFreeQtyBinding(RichInputText soFreeQtyBinding) {
        this.soFreeQtyBinding = soFreeQtyBinding;
    }

    public RichInputText getSoFreeQtyBinding() {
        return soFreeQtyBinding;
    }

    public void setSoBookQtyBinding(RichInputText soBookQtyBinding) {
        this.soBookQtyBinding = soBookQtyBinding;
    }

    public RichInputText getSoBookQtyBinding() {
        return soBookQtyBinding;
    }

    public void setSoStockQtyBinding(RichInputText soStockQtyBinding) {
        this.soStockQtyBinding = soStockQtyBinding;
    }

    public RichInputText getSoStockQtyBinding() {
        return soStockQtyBinding;
    }

    public void setDadtlBatchQtyBinding(RichInputText dadtlBatchQtyBinding) {
        this.dadtlBatchQtyBinding = dadtlBatchQtyBinding;
    }

    public RichInputText getDadtlBatchQtyBinding() {
        return dadtlBatchQtyBinding;
    }

    public void setDaDtlMfgDtBinding(RichInputText daDtlMfgDtBinding) {
        this.daDtlMfgDtBinding = daDtlMfgDtBinding;
    }

    public RichInputText getDaDtlMfgDtBinding() {
        return daDtlMfgDtBinding;
    }

    public void setDaDtlExpDtBinding(RichInputText daDtlExpDtBinding) {
        this.daDtlExpDtBinding = daDtlExpDtBinding;
    }

    public RichInputText getDaDtlExpDtBinding() {
        return daDtlExpDtBinding;
    }

    public void pendingSaleOrderAL(ActionEvent actionEvent) 
    {
        if(ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C"))
        {
            OperationBinding opp = ADFUtils.findOperation("populateSODetailOnDA");
            Object obb = opp.execute();
            System.out.println("Return true---->"+opp.getResult());
            if (opp.getErrors() != null && opp.getResult().equals("SS")) {
                ADFUtils.showPopup(pendSODetailPopupBinding);
            }else{
                ADFUtils.showMessage("There is no Sale Order for this Customer", 1);
            }
        }
//        else if(ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E"))
//        {
//            ADFUtils.showPopup(pendSODetailPopupBinding);
//        }
        else{
            ADFUtils.showPopup(pendSODetailPopupBinding);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(pendSODetailPopupBinding);
    }

    public void setPendSODetailPopupBinding(RichPopup pendSODetailPopupBinding) {
        this.pendSODetailPopupBinding = pendSODetailPopupBinding;
    }

    public RichPopup getPendSODetailPopupBinding() {
        return pendSODetailPopupBinding;
    }

    public void selectCheckValueVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        logger.info("The New Value of Check Box----->>"+vce.getNewValue());
        OperationBinding op = ADFUtils.findOperation("setBooleanCheckboxOnDA");
        op.getParamsMap().put("checkVal", vce.getNewValue());
        op.execute();
        AdfFacesContext.getCurrentInstance().addPartialTarget(transSoManualFlagBinding);
    }

    public void allBatchPopulateAL(ActionEvent actionEvent)
    {
        OperationBinding opp = ADFUtils.findOperation("getPopulateAllBatchWiseDetails");
        Object obb = opp.execute();
        System.out.println("Return baba---->"+opp.getResult());
        
        if (opp.getErrors() != null && opp.getResult().equals("Success")) {
            System.out.println("Set Status fied----");
            getAllBatchPopulateBtnBinding().setDisabled(true);
            AdfFacesContext.getCurrentInstance().addPartialTarget(allBatchPopulateBtnBinding);
        }else if (opp.getErrors() != null && opp.getResult().equals("fail")) {
            ADFUtils.showMessage("Please Select any Check Box.", 0);
        }
    }

    public void populateDADetailBtnAL(ActionEvent actionEvent) 
    {
//        if((Long)ADFUtils.evaluateEL("#{bindings.DaBatchDtlVO1Iterator.estimatedRowCount}")>0)
//        {
            OperationBinding os = ADFUtils.findOperation("checkDABatchDespAvailQty");
            Object obb = os.execute();
            System.out.println("Res Value---->"+os.getResult());
            boolean flag=Boolean.parseBoolean(os.getResult().toString());
            System.out.println("Flag in bean------>>"+flag);
            if (os.getErrors() != null && os.getResult().equals("SS")) 
            {
                OperationBinding op1 = ADFUtils.findOperation("daBatchDtlPopulateIntoDaDetails");             
                op1.execute();
                System.out.println("daBatchDtlPopulateIntoDaDetails====Status============>"+op1.getResult());

                if (op1.getResult().equals("N")) 
                {
                    ADFUtils.showMessage("There is no data in DA Batch Detail.Please Check.", 0);
                }
                else{
                    //System.out.println("WOW");
                    getAllPopulateBatchDtlBtnBinding().setDisabled(true);
                    AdfFacesContext.getCurrentInstance().addPartialTarget(allPopulateBatchDtlBtnBinding);
                    getPendSODetailPopupBinding().cancel();
                }
            }
            else{
                    ADFUtils.showMessage("Despatch Qty and Batch Quantity must be equal.", 0);
            }
//        }
//    else{
//            ADFUtils.showMessage("Please Populate All Batch.", 0);
//        }
    }

    public void popuDADetailCancelAL(ActionEvent actionEvent) {

            getPendSODetailPopupBinding().cancel();
        }

    public void setDesStatus(String desStatus) {
        this.desStatus = desStatus;
    }

    public String getDesStatus() {
        return desStatus;
    }

    public void setAllBatchPopulateBtnBinding(RichButton allBatchPopulateBtnBinding) {
        this.allBatchPopulateBtnBinding = allBatchPopulateBtnBinding;
    }

    public RichButton getAllBatchPopulateBtnBinding() {
        return allBatchPopulateBtnBinding;
    }

    public void setAllPopulateBatchDtlBtnBinding(RichButton allPopulateBatchDtlBtnBinding) {
        this.allPopulateBatchDtlBtnBinding = allPopulateBatchDtlBtnBinding;
    }

    public RichButton getAllPopulateBatchDtlBtnBinding() {
        return allPopulateBatchDtlBtnBinding;
    }

    public void setNewTotalBatchQtyTransBinding(RichInputText newTotalBatchQtyTransBinding) {
        this.newTotalBatchQtyTransBinding = newTotalBatchQtyTransBinding;
    }

    public RichInputText getNewTotalBatchQtyTransBinding() {
        return newTotalBatchQtyTransBinding;
    }

    public void setSoDtlPoNoBinding(RichInputComboboxListOfValues soDtlPoNoBinding) {
        this.soDtlPoNoBinding = soDtlPoNoBinding;
    }

    public RichInputComboboxListOfValues getSoDtlPoNoBinding() {
        return soDtlPoNoBinding;
    }

    public void setDtlBatchProductCodeBinding(RichInputText dtlBatchProductCodeBinding) {
        this.dtlBatchProductCodeBinding = dtlBatchProductCodeBinding;
    }

    public RichInputText getDtlBatchProductCodeBinding() {
        return dtlBatchProductCodeBinding;
    }

    public void daBatchQtyVCE(ValueChangeEvent vce) 
    {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        logger.info("The New Value of DA Batch Quantity---->>"+vce.getNewValue());
        getPopulateBatchDtlBtnBinding().setDisabled(false);
        AdfFacesContext.getCurrentInstance().addPartialTarget(totalBtcQtyBinding);
        AdfFacesContext.getCurrentInstance().addPartialTarget(populateBatchDtlBtnBinding);
    }

    public void setSelectCheckBoxBinding(RichSelectBooleanCheckbox selectCheckBoxBinding) {
        this.selectCheckBoxBinding = selectCheckBoxBinding;
    }

    public RichSelectBooleanCheckbox getSelectCheckBoxBinding() {
        return selectCheckBoxBinding;
    }

    public void selectSoPonoVCE(ValueChangeEvent vce) 
    {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce!=null && detProdCodeBinding.getValue()!=null)
        {
            transSoManualFlagBinding.setValue("M");
            System.out.println(" The Selected Product No is---->>"+detProdCodeBinding.getValue());
            OperationBinding op = ADFUtils.findOperation("updateBookQtyForSelectedSo");
            op.getParamsMap().put("ProductCd", detProdCodeBinding.getValue());
            op.execute();
                
            if(transSoManualFlagBinding.getValue()!=null)
            {
                if("M".equalsIgnoreCase(transSoManualFlagBinding.getValue().toString()))
                {
                    getPopulateBatchDtlBtnBinding().setVisible(false);
                    getAddManualDaDetailBinding().setVisible(true);
                    System.out.println("mmmmmpppp");
                }else{
                    getPopulateBatchDtlBtnBinding().setVisible(true);
                    getAddManualDaDetailBinding().setVisible(false);
                    System.out.println("nnnpppp");
                }
            }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(populateBatchDtlBtnBinding);
        AdfFacesContext.getCurrentInstance().addPartialTarget(addManualDaDetailBinding);
    }

    public void setTransSoManualFlagBinding(RichInputText transSoManualFlagBinding) {
        this.transSoManualFlagBinding = transSoManualFlagBinding;
    }

    public RichInputText getTransSoManualFlagBinding() {
        return transSoManualFlagBinding;
    }

    public void setAddManualDaDetailBinding(RichButton addManualDaDetailBinding) {
        this.addManualDaDetailBinding = addManualDaDetailBinding;
    }

    public RichButton getAddManualDaDetailBinding() {
        return addManualDaDetailBinding;
    }

    public void addManualDaDetailAL(ActionEvent actionEvent) 
    {
        OperationBinding op1 = ADFUtils.findOperation("daBatchDtlPopulateIntoDaDetails");             
        op1.execute();
        System.out.println("addManualDaDetailAL==>"+op1.getResult());
        getDaBatchDetTableBinding().cancel();
        getAddManualDaDetailBinding().setDisabled(true);
        AdfFacesContext.getCurrentInstance().addPartialTarget(addManualDaDetailBinding);
        if (op1.getResult().equals("N")) 
        {
            ADFUtils.showMessage("There is no data in DA Batch Detail.Please Check.", 0);
        }
    }

    public void soDespatchQtyVCE(ValueChangeEvent vce) 
    {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce!=null)
        {
            BigDecimal despQty = (BigDecimal)vce.getNewValue();//despQtyBinding
            BigDecimal penQty = (BigDecimal) pendingQtyBinding.getValue();
            BigDecimal freeQty = (BigDecimal) soFreeQtyBinding.getValue();
            System.out.println("Dispatch Quantity is-->>"+despQty+" Binding Value is--->>"+despQtyBinding.getValue());
            if(despQty !=null && getPendingQtyBinding()!= null) 
            {
                if(penQty.compareTo(despQty)<0)
                {
                    FacesMessage message = new FacesMessage("Dispatch Qty must be less than or equal Pending Qty.");
                    message.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext context = FacesContext.getCurrentInstance();
                    context.addMessage(despQtyBinding.getClientId(), message);
                }
                else if(despQty.compareTo(new BigDecimal(0)) < 0){
                    FacesMessage message = new FacesMessage("Dispatch Qty must be greater than or equal to zero.");
                    message.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext context = FacesContext.getCurrentInstance();
                    context.addMessage(despQtyBinding.getClientId(), message);
                }
                else if(freeQty.compareTo(despQty)<0)
                {
                    FacesMessage message = new FacesMessage("Dispatch Qty must be less than or equal to Free Qty.");
                    message.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext context = FacesContext.getCurrentInstance();
                    context.addMessage(despQtyBinding.getClientId(), message);
                }
            }
        }
    }

    public void createBatchDetailAL(ActionEvent actionEvent) 
    {
        OperationBinding opp = ADFUtils.findOperation("insertRowDABatch");
        Object obb = opp.execute();
        System.out.println("Insert DA Batch Add Row---->>"+opp.getResult());
        if (opp.getErrors() != null && opp.getResult().equals("T")) 
        {
            System.out.println("Yesy Yesss");
        }else
        {
            ADFUtils.showMessage("New Row can't be Inserted.Error!!", 0);
        }
    }

    public void daBatchProductCodeVCE(ValueChangeEvent vce) 
    {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce!=null)
        {
            OperationBinding opr=ADFUtils.findOperation("checkBatchProductDuplicateDA");
            opr.getParamsMap().put("batchNo",dtlBatchNoBinding.getValue());
            Object obj=opr.execute();
            if(opr.getResult().equals("N"))
            {
                ADFUtils.showMessage("Batch No.Already Exist.",0);
            }  
        }
    }

    public void daBatchDetailDeleteDialogDL(DialogEvent dialogEvent) 
    {
        if (dialogEvent.getOutcome().name().equals("ok")) 
        {
            ADFUtils.findOperation("deleteScrapOnBatchDetail").execute();
            ADFUtils.findOperation("Delete3").execute();
            System.out.println("Record Delete Successfully");
            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(daBatchDetailTableBinding);
    }

    public void setDaBatchDetailTableBinding(RichTable daBatchDetailTableBinding) {
        this.daBatchDetailTableBinding = daBatchDetailTableBinding;
    }

    public RichTable getDaBatchDetailTableBinding() {
        return daBatchDetailTableBinding;
    }

    public void setDaBatchChStatusBinding(RichInputText daBatchChStatusBinding) {
        this.daBatchChStatusBinding = daBatchChStatusBinding;
    }

    public RichInputText getDaBatchChStatusBinding() {
        return daBatchChStatusBinding;
    }

    public void setCustBalanceBinding(RichInputText custBalanceBinding) {
        this.custBalanceBinding = custBalanceBinding;
    }

    public RichInputText getCustBalanceBinding() {
        return custBalanceBinding;
    }
    
    public void setDadatenepalinding(RichInputText dadatenepalinding) {
        this.dadatenepalinding = dadatenepalinding;
    }

    public RichInputText getDadatenepalinding() {
        return dadatenepalinding;
    }

    public void setBatchProdNameBinding(RichInputText batchProdNameBinding) {
        this.batchProdNameBinding = batchProdNameBinding;
    }

    public RichInputText getBatchProdNameBinding() {
        return batchProdNameBinding;
    }

    public void setDaDtlGrossWtBinding(RichInputText daDtlGrossWtBinding) {
        this.daDtlGrossWtBinding = daDtlGrossWtBinding;
    }

    public RichInputText getDaDtlGrossWtBinding() {
        return daDtlGrossWtBinding;
    }

    public void setSodtlUomBinding(RichInputText sodtlUomBinding) {
        this.sodtlUomBinding = sodtlUomBinding;
    }

    public RichInputText getSodtlUomBinding() {
        return sodtlUomBinding;
    }

    public void setPackQtyBinding(RichInputText packQtyBinding) {
        this.packQtyBinding = packQtyBinding;
    }

    public RichInputText getPackQtyBinding() {
        return packQtyBinding;
    }

    public void setPackUomBinding(RichInputText packUomBinding) {
        this.packUomBinding = packUomBinding;
    }

    public RichInputText getPackUomBinding() {
        return packUomBinding;
    }

    public void setNetWeightBinding(RichInputText netWeightBinding) {
        this.netWeightBinding = netWeightBinding;
    }

    public RichInputText getNetWeightBinding() {
        return netWeightBinding;
    }

    public void setSoDtlProdDescBinding(RichInputText soDtlProdDescBinding) {
        this.soDtlProdDescBinding = soDtlProdDescBinding;
    }

    public RichInputText getSoDtlProdDescBinding() {
        return soDtlProdDescBinding;
    }

    public void setSoDtlPackQtyBinding(RichInputText soDtlPackQtyBinding) {
        this.soDtlPackQtyBinding = soDtlPackQtyBinding;
    }

    public RichInputText getSoDtlPackQtyBinding() {
        return soDtlPackQtyBinding;
    }

    public void setSoDtlPacckUomBinding(RichInputText soDtlPacckUomBinding) {
        this.soDtlPacckUomBinding = soDtlPacckUomBinding;
    }

    public RichInputText getSoDtlPacckUomBinding() {
        return soDtlPacckUomBinding;
    }
}

