package terms.mkt.transaction.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;
import javax.faces.validator.ValidatorException;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichDialog;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;

import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.adf.view.rich.event.PopupFetchEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.JboWarning;
import oracle.jbo.Row;
import oracle.jbo.domain.Timestamp;

import terms.mkt.transaction.model.view.DespatchScheduleHeaderVORowImpl;


public class DespatchScheduleBean {
   private RichButton populateBinding;
    private RichSelectOneChoice monthBInd;
    private RichPopup bindPopup;
    private RichInputDate entryDateBind;
    private RichInputText yearBind;
    private RichInputText bindCity;
    private RichInputDate appByBind;
    private RichInputText bindDt4;
    private RichInputText bindDt5;
    private RichInputText bindDt6;
    private RichInputText bindDt7;
    private RichInputText bindDt8;
    private RichInputText bindDt9;
    private RichInputText bindDt10;
    private RichInputText bindDt11;
    private RichInputText bindDt12;
    private RichInputText bindDt13;
    private RichInputText bindDt14;
    private RichInputText bindDt15;
    private RichInputText bindDt16;
    private RichInputText bindDt17;
    private RichInputText bindDt18;
    private RichInputText bindDt19;
    private RichInputText bindDt20;
    private RichInputText bindDt21;
    private RichInputText bindDt22;
    private RichInputText bindDt23;
    private RichInputText bindDt24;
    private RichInputText bindDt25;
    private RichInputText bindDt26;
    private RichInputText bindDt27;
    private RichInputText bindDt28;
    private RichInputText bindDt29;
    private RichInputText bindDt30;
    private RichInputText bindDt31;
    private RichInputText bindDt1;
    private RichInputText bindDt2;
    private RichInputText bindDt3;
    private RichInputText bindPoQty;
    private RichInputComboboxListOfValues bindCustomer;
    private RichInputText bindProdCode;
    private RichButton headerEditBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichOutputText bindingOutputText;
    private RichInputText scheduleNoBinding;
    private RichInputComboboxListOfValues unitBinding;
    private RichInputComboboxListOfValues appByBinding;
    private RichInputComboboxListOfValues enterByBinding;
    private RichInputText custProdNoBinding;
    private RichInputText poNoBinding;
    private RichInputText poAmdNoBinding;
    private RichInputText transitPeriodBinding;
    private RichInputText procCodeBinding;
    private RichInputText saleOrderQuantity;
    private RichPopup confirmationPopUpBind;
   
    private RichInputText amdNoBinding;
    private RichInputText srNoBinding;
    private RichInputText remarksBinding;
    private RichInputDate amdDateBinding;
    private RichColumn poQtyBinding;
    private RichSelectOneChoice typeBinding;
    private RichTable detailTableBinding;
    private RichInputText dayTransBinding;
    private RichInputText monthTransBind;


    public DespatchScheduleBean() {
    }

    public void saveScheduleNoAL(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedBy("DespatchScheduleHeaderVO1Iterator","LastUpdatedBy");
        
//        OperationBinding op =  ADFUtils.findOperation("getPoNo");
//        op.getParamsMap().put("City", getBindCity().getValue());
//        op.execute();
//        
//        ADFUtils.findOperation("getPoAmendAckNo").execute();
        
                       
       //  OperationBinding op = (OperationBinding) ADFUtils.findOperation("getSubSrSequence").execute();
        ADFUtils.findOperation("getDespatchScheduleRefresh").execute();
       // ADFUtils.findOperation("compareWithFinYear").execute();

        OperationBinding op1 = ADFUtils.findOperation("getScheduleNo");
        op1.getParamsMap().put("City", getBindCity().getValue());
        Object rst = op1.execute();
        System.out.println("--------Commit-------");
        System.out.println("value aftr getting result--->?"+rst);
        if (rst!= null && rst!= "") 
        {
            System.out.println("MessgaeBlock");
                               if (op1.getErrors().isEmpty()) {
                                   ADFUtils.findOperation("Commit").execute();
                                   FacesMessage Message = new FacesMessage("Record Saved Successfully. New Schedule No is "+rst+".");
                                   Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                                   FacesContext fc = FacesContext.getCurrentInstance(); 
                                   fc.addMessage(null, Message);  
                                   
                               }
            }
                           
                           if (rst == null || rst.toString() == "" || rst.toString() == " ") {
                               if (op1.getErrors().isEmpty()) {
                                   ADFUtils.findOperation("Commit").execute();
                                   FacesMessage Message = new FacesMessage("Record Update Successfully.");   
                                   Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                                   FacesContext fc = FacesContext.getCurrentInstance();   
                                   fc.addMessage(null, Message);  
                           
                               }

                            } 
                           
                           
           
 

    }

    public void setPopulateBinding(RichButton populateBinding) {
        this.populateBinding = populateBinding;
    }

    public RichButton getPopulateBinding() {
        return populateBinding;
    }

    

    public void setMonthBInd(RichSelectOneChoice monthBInd) {
        this.monthBInd = monthBInd;
    }

    public RichSelectOneChoice getMonthBInd() {
        return monthBInd;
    }

    public void populateScheduleAL(ActionEvent actionEvent) {
        
        
    }


    public void yesNoPopulateDataDL(DialogEvent dialogEvent)
    {
        
        if(dialogEvent.getOutcome().name().equals("yes"))
        {
            System.out.println("POPUP DATA");
            OperationBinding op1 = ADFUtils.findOperation("populateSchedule");
            
            
            Object obj=op1.execute();
            System.out.println("**********Result is*******:"+op1.getResult());
            if(op1.getResult().equals("N") && op1.getResult()!=null)
            {
            FacesMessage Message = new FacesMessage("No PO/Customer Product Mapping Is Available For This Customer");  
            Message.setSeverity(FacesMessage.SEVERITY_INFO);  
            FacesContext fc = FacesContext.getCurrentInstance();  
            fc.addMessage(null, Message);  
            }
            else if(op1.getResult().equals("Y") && op1.getResult()!=null)
            {
//            if(dialogEvent.getOutcome().name().equals("yes"))
//                        {
                            //OperationBinding op1=ADFUtils.findOperation("populateSchedule");
                            //Object obj1=op1.execute();
                            
                            getBindCustomer().setDisabled(true);
                            getYearBind().setDisabled(true);
                            getMonthBInd().setDisabled(true);
                            getTypeBinding().setDisabled(true);
                            
                            OperationBinding op =  ADFUtils.findOperation("getPoNo");
                            op.getParamsMap().put("City", getBindCity().getValue());
                            op.execute();
                disableDateMonthwise();
            
                            ADFUtils.findOperation("getPoAmendAckNo").execute();
                            OperationBinding op2 = (OperationBinding) ADFUtils.findOperation("getSubSrSequence").execute();
                            System.out.println("Result is ===> "+op1.getResult());
                            System.out.println("YES POPUP DATA");
                      //  }
            
            }
        }
//        
//        if(dialogEvent.getOutcome().name().equals("no"))
//                   {
//                       System.out.println("NO POPUP DATA");
//
//                       FacesMessage Message = new FacesMessage("No data available for this record.");  
//                       Message.setSeverity(FacesMessage.SEVERITY_INFO);  
//                       FacesContext fc = FacesContext.getCurrentInstance();  
//                       fc.addMessage(null, Message);  
//                  
//                   } 
        AdfFacesContext.getCurrentInstance().addPartialTarget(detailTableBinding);  
    }

    public void populateAmendAL(ActionEvent actionEvent) {
        System.out.println("in populate button al");
                            
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C") ||  (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E") ) ){
            RichPopup.PopupHints hints = new RichPopup.PopupHints();
                                     getBindPopup().show(hints);
        }

        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("A") ) {
        System.out.println("in if of populate");
        FacesMessage Message = new FacesMessage("Press the Modify Button or Add Button to populate");
        Message.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
   }
        disabledFieldsAll();
        disabledFieldAccordingToDate(); 
    }  

    

    public void setBindPopup(RichPopup bindPopup) {
        this.bindPopup = bindPopup;
    }

    public RichPopup getBindPopup() {
        return bindPopup;
    }

    public void populateAmendAL(PopupFetchEvent popupFetchEvent) {
        // Add event code here...
    }

    public void setEntryDateBind(RichInputDate entryDateBind) {
        this.entryDateBind = entryDateBind;
    }

    public RichInputDate getEntryDateBind() {
        return entryDateBind;
    }

    public void setYearBind(RichInputText yearBind) {
        this.yearBind = yearBind;
    }

    public RichInputText getYearBind() {
        return yearBind;
    }

    public void monthValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
      
        if(object!= null && getMonthBInd()!= null)
        {
       // System.out.println("month in bean====>>"+object);
        System.out.println("year in bean===>" + yearBind.getValue());
        //String month=getMonthBInd().toString();
        
BigDecimal monthVal=null;

if(object.equals("JAN"))
{
monthVal=new BigDecimal(01);
}
else if(object.equals("FEB"))
        {
        monthVal=new BigDecimal(02);
        }
        
else if(object.equals("MAR"))
        {
        monthVal=new BigDecimal(03);
        }
        
else if(object.equals("APR"))
        {
        monthVal=new BigDecimal(04);
        }
        
else if(object.equals("MAY"))
        {
        monthVal=new BigDecimal(05);
        }
        
else if(object.equals("JUN"))
        {
        monthVal=new BigDecimal(06);
        }
        
else if(object.equals("JUL"))
        {
        monthVal=new BigDecimal(07);
        }
        
else if(object.equals("AUG"))
        {
        monthVal=new BigDecimal(8);
        }
        
else if(object.equals("SEP"))
        {
        monthVal=new BigDecimal(9);
        }
        
else  if(object.equals("OCT"))
        {
        monthVal=new BigDecimal(10);
        }
        
else if(object.equals("NOV"))
        {
        monthVal=new BigDecimal(11);
        }


else if(object.equals("DEC"))
        {
        monthVal=new BigDecimal(12);
        }

else
{
 System.out.println("Not date found");   
 }

        
        System.out.println("month in bean====."  +object);
        Timestamp Edate = (Timestamp) getEntryDateBind().getValue();
        System.out.println("entry date====>"+Edate);
        String monthfromEdate  =Edate.toString().substring(5,7 );
        //String yearfromEdate  =Edate.toString().substring(0,4 );
        
        
        if(monthfromEdate.equals("08"))
        {
            
             monthfromEdate="8"; 
             System.out.println("october ===>" +monthfromEdate );
             
         }
        
        
        if(monthfromEdate.equals("09"))
        {
            
             monthfromEdate="9";   
         }
        
        
        //System.out.println("month from entry date====>>" +monthfromEdate + "year from entry date=====>>"+ yearfromEdate);
        BigDecimal monEdate =new BigDecimal(monthfromEdate.toString());
        //BigDecimal mon =new BigDecimal(getMonthBInd().toString());
        
     System.out.println("monthEdate in bigdecimal====>"  +monEdate  );
        
        if(monthVal.compareTo(monEdate) == -1)  {
            
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Back month schedule cannot be created.", null));
            
        }
        
 }
}

    public void yearValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        System.out.println("year enter****************************");
        if(object!= null && getYearBind()!=null) {
            
            System.out.println("year enter in if ********************************");
            System.out.println("year in validator  ====>" +object);
          
            
            
          //  String year = (String) getYearBind().getValue();
           // System.out.println("year in 2 valid===>" + year);
            
            Timestamp Edate = (Timestamp) getEntryDateBind().getValue();
            System.out.println("entry date====>"+Edate);
            String yearfromEdate  =Edate.toString().substring(0,4 );
            BigDecimal yrEdate =new BigDecimal(yearfromEdate.toString());
            BigDecimal yr=new BigDecimal(object.toString());
            System.out.println("year in bigdecimal"  +yr);
            
            if(yr.compareTo(yrEdate) == -1) {
                
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Back month schedule cannot be created.", null));
                
            }
            
            Integer yrinfin = (Integer) object;
            
            OperationBinding ob=ADFUtils.findOperation("compareWithFinYear");
            ob.getParamsMap().put("Year", yrinfin);
            ob.execute();
            System.out.println("with year comparison fin year**** " +ob.getResult());
            if(yrinfin.compareTo((Integer) ob.getResult()) == 1) {
                
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Financial year is not defined.", null));
                
            }
            
            
            
        

    }
}

    public void setBindCity(RichInputText bindCity) {
        this.bindCity = bindCity;
    }

    public RichInputText getBindCity() {
        return bindCity;
    }


    public void aooByValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if(object != null){
            System.out.println("app by date in bean  "  +object);
        }

    }

    public void setAppByBind(RichInputDate appByBind) {
        this.appByBind = appByBind;
    }

    public RichInputDate getAppByBind() {
        return appByBind;
    }

    public void setBindDt4(RichInputText bindDt4) {
        this.bindDt4 = bindDt4;
    }

    public RichInputText getBindDt4() {
        return bindDt4;
    }

    public void setBindDt5(RichInputText bindDt5) {
        this.bindDt5 = bindDt5;
    }

    public RichInputText getBindDt5() {
        return bindDt5;
    }

    public void setBindDt6(RichInputText bindDt6) {
        this.bindDt6 = bindDt6;
    }

    public RichInputText getBindDt6() {
        return bindDt6;
    }

    public void setBindDt7(RichInputText bindDt7) {
        this.bindDt7 = bindDt7;
    }

    public RichInputText getBindDt7() {
        return bindDt7;
    }

    public void setBindDt8(RichInputText bindDt8) {
        this.bindDt8 = bindDt8;
    }

    public RichInputText getBindDt8() {
        return bindDt8;
    }

    public void setBindDt9(RichInputText bindDt9) {
        this.bindDt9 = bindDt9;
    }

    public RichInputText getBindDt9() {
        return bindDt9;
    }

    public void setBindDt10(RichInputText bindDt10) {
        this.bindDt10 = bindDt10;
    }

    public RichInputText getBindDt10() {
        return bindDt10;
    }

    public void setBindDt11(RichInputText bindDt11) {
        this.bindDt11 = bindDt11;
    }

    public RichInputText getBindDt11() {
        return bindDt11;
    }

    public void setBindDt12(RichInputText bindDt12) {
        this.bindDt12 = bindDt12;
    }

    public RichInputText getBindDt12() {
        return bindDt12;
    }

    public void setBindDt13(RichInputText bindDt13) {
        this.bindDt13 = bindDt13;
    }

    public RichInputText getBindDt13() {
        return bindDt13;
    }

    public void setBindDt14(RichInputText bindDt14) {
        this.bindDt14 = bindDt14;
    }

    public RichInputText getBindDt14() {
        return bindDt14;
    }

    public void setBindDt15(RichInputText bindDt15) {
        this.bindDt15 = bindDt15;
    }

    public RichInputText getBindDt15() {
        return bindDt15;
    }

    public void setBindDt16(RichInputText bindDt16) {
        this.bindDt16 = bindDt16;
    }

    public RichInputText getBindDt16() {
        return bindDt16;
    }

    public void setBindDt17(RichInputText bindDt17) {
        this.bindDt17 = bindDt17;
    }

    public RichInputText getBindDt17() {
        return bindDt17;
    }

    public void setBindDt18(RichInputText bindDt18) {
        this.bindDt18 = bindDt18;
    }

    public RichInputText getBindDt18() {
        return bindDt18;
    }

    public void setBindDt19(RichInputText bindDt19) {
        this.bindDt19 = bindDt19;
    }

    public RichInputText getBindDt19() {
        return bindDt19;
    }

    public void setBindDt20(RichInputText bindDt20) {
        this.bindDt20 = bindDt20;
    }

    public RichInputText getBindDt20() {
        return bindDt20;
    }

    public void setBindDt21(RichInputText bindDt21) {
        this.bindDt21 = bindDt21;
    }

    public RichInputText getBindDt21() {
        return bindDt21;
    }

    public void setBindDt22(RichInputText bindDt22) {
        this.bindDt22 = bindDt22;
    }

    public RichInputText getBindDt22() {
        return bindDt22;
    }

    public void setBindDt23(RichInputText bindDt23) {
        this.bindDt23 = bindDt23;
    }

    public RichInputText getBindDt23() {
        return bindDt23;
    }

    public void setBindDt24(RichInputText bindDt24) {
        this.bindDt24 = bindDt24;
    }

    public RichInputText getBindDt24() {
        return bindDt24;
    }

    public void setBindDt25(RichInputText bindDt25) {
        this.bindDt25 = bindDt25;
    }

    public RichInputText getBindDt25() {
        return bindDt25;
    }

    public void setBindDt26(RichInputText bindDt26) {
        this.bindDt26 = bindDt26;
    }

    public RichInputText getBindDt26() {
        return bindDt26;
    }

    public void setBindDt27(RichInputText bindDt27) {
        this.bindDt27 = bindDt27;
    }

    public RichInputText getBindDt27() {
        return bindDt27;
    }

    public void setBindDt28(RichInputText bindDt28) {
        this.bindDt28 = bindDt28;
    }

    public RichInputText getBindDt28() {
        return bindDt28;
    }

    public void setBindDt29(RichInputText bindDt29) {
        this.bindDt29 = bindDt29;
    }

    public RichInputText getBindDt29() {
        return bindDt29;
    }

    public void setBindDt30(RichInputText bindDt30) {
        this.bindDt30 = bindDt30;
    }

    public RichInputText getBindDt30() {
        return bindDt30;
    }

    public void setBindDt31(RichInputText bindDt31) {
        this.bindDt31 = bindDt31;
    }

    public RichInputText getBindDt31() {
        return bindDt31;
    }
    public void setBindDt1(RichInputText bindDt1) {
        this.bindDt1 = bindDt1;
    }
    

    public RichInputText getBindDt1() {
        return bindDt1;
    }

    public void setBindDt2(RichInputText bindDt2) {
        this.bindDt2 = bindDt2;
    }

    public RichInputText getBindDt2() {
        return bindDt2;
    }

    public void setBindDt3(RichInputText bindDt3) {
        this.bindDt3 = bindDt3;
    }

    public RichInputText getBindDt3() {
        return bindDt3;
    }

    public void dateVCE(ValueChangeEvent vce) {
        
//        System.out.println("bfore vce enter");
//        try{
//       if(vce!= null && getBindProdCode().getValue() != null ){
//           
//           System.out.println("after"+ vce.getNewValue());
//           
//           
//                
//                vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
//               
//                BigDecimal sum=new BigDecimal(0);
//               // BigDecimal updatedPr=new BigDecimal(0);
//                Object dt1=getBindDt1().getValue()==null?'0':getBindDt1().getValue();
//                Object dt2=getBindDt2().getValue()==null?'0':getBindDt2().getValue();
//                Object dt3=getBindDt3().getValue()==null?'0':getBindDt3().getValue();
//                Object dt4=getBindDt4().getValue()==null?'0':getBindDt4().getValue();
//                Object dt5=getBindDt5().getValue()==null?'0':getBindDt5().getValue();
//                Object dt6=getBindDt6().getValue()==null?'0':getBindDt6().getValue();
//                Object dt7=getBindDt7().getValue()==null?'0':getBindDt7().getValue();
//                Object dt8=getBindDt8().getValue()==null?'0':getBindDt8().getValue();
//                Object dt9=getBindDt9().getValue()==null?'0':getBindDt9().getValue();
//                Object dt10=getBindDt10().getValue()==null?'0':getBindDt10().getValue();
//                Object dt11=getBindDt11().getValue()==null?'0':getBindDt11().getValue();
//                Object dt12=getBindDt12().getValue()==null?'0':getBindDt12().getValue();
//                Object dt13=getBindDt13().getValue()==null?'0':getBindDt13().getValue();
//                Object dt14=getBindDt14().getValue()==null?'0':getBindDt14().getValue();
//                Object dt15=getBindDt15().getValue()==null?'0':getBindDt15().getValue();
//                Object dt16=getBindDt16().getValue()==null?'0':getBindDt16().getValue();
//                Object dt17=getBindDt17().getValue()==null?'0':getBindDt17().getValue();
//                Object dt18=getBindDt18().getValue()==null?'0':getBindDt18().getValue();
//                Object dt19=getBindDt19().getValue()==null?'0':getBindDt19().getValue();
//                Object dt20=getBindDt20().getValue()==null?'0':getBindDt20().getValue();
//                Object dt21=getBindDt21().getValue()==null?'0':getBindDt21().getValue();
//                Object dt22=getBindDt22().getValue()==null?'0':getBindDt22().getValue();
//                Object dt23=getBindDt23().getValue()==null?'0':getBindDt23().getValue();
//                Object dt24=getBindDt24().getValue()==null?'0':getBindDt24().getValue();
//                Object dt25=getBindDt25().getValue()==null?'0':getBindDt25().getValue();
//                Object dt26=getBindDt26().getValue()==null?'0':getBindDt26().getValue();
//                Object dt27=getBindDt27().getValue()==null?'0':getBindDt27().getValue();
//                Object dt28=getBindDt28().getValue()==null?'0':getBindDt28().getValue();
//                Object dt29=getBindDt29().getValue()==null?'0':getBindDt29().getValue();
//                Object dt30=getBindDt30().getValue()==null?'0':getBindDt30().getValue();
//                Object dt31=getBindDt31().getValue()==null?'0':getBindDt31().getValue();
//                
//                        
//        BigDecimal dte1=new BigDecimal(dt1.toString());
//        BigDecimal dte2=new BigDecimal(dt2.toString());
//        BigDecimal dte3=new BigDecimal(dt3.toString());
//        BigDecimal dte4=new BigDecimal(dt4.toString());
//        BigDecimal dte5=new BigDecimal(dt5.toString());
//        BigDecimal dte6=new BigDecimal(dt6.toString());
//        BigDecimal dte7=new BigDecimal(dt7.toString());
//        BigDecimal dte8=new BigDecimal(dt8.toString());
//        BigDecimal dte9=new BigDecimal(dt9.toString());
//        BigDecimal dte10=new BigDecimal(dt10.toString());
//        BigDecimal dte11=new BigDecimal(dt11.toString());
//        BigDecimal dte12=new BigDecimal(dt12.toString());
//        BigDecimal dte13=new BigDecimal(dt13.toString());
//        BigDecimal dte14=new BigDecimal(dt14.toString());
//        BigDecimal dte15=new BigDecimal(dt15.toString());
//        BigDecimal dte16=new BigDecimal(dt16.toString());
//        BigDecimal dte17=new BigDecimal(dt17.toString());
//        BigDecimal dte18=new BigDecimal(dt18.toString());
//        BigDecimal dte19=new BigDecimal(dt19.toString());
//        BigDecimal dte20=new BigDecimal(dt20.toString());
//        BigDecimal dte21=new BigDecimal(dt21.toString());
//        BigDecimal dte22=new BigDecimal(dt22.toString());
//        BigDecimal dte23=new BigDecimal(dt23.toString());
//        BigDecimal dte24=new BigDecimal(dt24.toString());
//        BigDecimal dte25=new BigDecimal(dt25.toString());
//        BigDecimal dte26=new BigDecimal(dt26.toString());
//        BigDecimal dte27=new BigDecimal(dt27.toString());
//        BigDecimal dte28=new BigDecimal(dt28.toString());
//        BigDecimal dte29=new BigDecimal(dt29.toString());
//        BigDecimal dte30=new BigDecimal(dt30.toString());
//        BigDecimal dte31=new BigDecimal(dt31.toString());
//                        
//
//                sum=dte1.add(dte2.add(dte3.add(dte4.add(dte5.add(dte6.add(dte7.add(dte8.add(dte9.add(dte10.add(dte11.add(dte12.add(dte13.add(dte14.
//               add(dte15.add(dte16.add(dte17.add(dte18.add(dte19.add(dte20.add(dte21.add(dte22.add(dte23.add(dte24.add(dte25.add(dte26.add(dte27.
//               add(dte28.add(dte29.add(dte30.add(dte31))))))))))))))))))))))))))))));
//                System.out.println("sum" +sum);
//                
////              updatedPr=d1.subtract(sum);
////                System.out.println("updatedPr" + updatedPr);
//               bindPoQty.setValue(sum);
//                 AdfFacesContext.getCurrentInstance().addPartialTarget(bindPoQty);     
//}
//        }
//
//            catch(Exception e)    {
//            
//                FacesMessage Message = new FacesMessage("No data available for this record.");  
//                Message.setSeverity(FacesMessage.SEVERITY_INFO);  
//                FacesContext fc = FacesContext.getCurrentInstance();  
//                fc.addMessage(null, Message);  
//                
//                
//                
//            }
//        
        }


    


    public void setBindPoQty(RichInputText bindPoQty) {
        this.bindPoQty = bindPoQty;
    }

    public RichInputText getBindPoQty() {
        return bindPoQty;
    }

    public void setBindCustomer(RichInputComboboxListOfValues bindCustomer) {
        this.bindCustomer = bindCustomer;
    }

    public RichInputComboboxListOfValues getBindCustomer() {
        return bindCustomer;
    }

    public void setBindProdCode(RichInputText bindProdCode) {
        this.bindProdCode = bindProdCode;
    }

    public RichInputText getBindProdCode() {
        return bindProdCode;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }


    public void EditButtonAL(ActionEvent actionEvent) {
        

        Date dte=new Date();
       // Date now = new Date();
       BigDecimal monthVal=null;
        Calendar calendar = Calendar.getInstance();
        Integer yr=(Integer)calendar.get(Calendar.YEAR);
        
        Integer days = (Integer)(calendar.get(Calendar.MONTH)+1);
        BigDecimal inInt=new BigDecimal(days);
        System.out.println("MONTH IN EDIT MODE IS=====>>>>"+days);
       // calendar.set(Calendar.MONTH,days);
       // calendar.set(Calendar.YEAR,yr);
       // Date dte=calendar.getTime();
        SimpleDateFormat  format=new SimpleDateFormat("yyyy/MM");
        try {
             dte = format.parse(yr + "/" + inInt);
        } catch (ParseException e) {
            
        }
        System.out.println("Current date from yearand month is===>>+"+dte);
        Object prevmonth=monthBInd.getValue();
        Object prevYear=yearBind.getValue();
        
        if(prevmonth.equals("JAN"))
        {
        monthVal=new BigDecimal(01);
        }
        else if(prevmonth.equals("FEB"))
                {
                monthVal=new BigDecimal(02);
                }
                
        else if(prevmonth.equals("MAR"))
                {
                monthVal=new BigDecimal(03);
                }
                
        else if(prevmonth.equals("APR"))
                {
                monthVal=new BigDecimal(04);
                }
                
        else if(prevmonth.equals("MAY"))
                {
                monthVal=new BigDecimal(05);
                }
                
        else if(prevmonth.equals("JUN"))
                {
                monthVal=new BigDecimal(06);
                }
                
        else if(prevmonth.equals("JUL"))
                {
                monthVal=new BigDecimal(07);
                }
                
        else if(prevmonth.equals("AUG"))
                {
                monthVal=new BigDecimal(8);
                }
                
        else if(prevmonth.equals("SEP"))
                {
                monthVal=new BigDecimal(9);
                }
                
        else  if(prevmonth.equals("OCT"))
                {
                monthVal=new BigDecimal(10);
                }
                
        else if(prevmonth.equals("NOV"))
                {
                monthVal=new BigDecimal(11);
                }


        else if(prevmonth.equals("DEC"))
                {
                monthVal=new BigDecimal(12);
                }

        System.out.println("previous month is===>>>"+monthVal);
       
       
        if(!appByBinding.getValue().toString().isEmpty())   {
            
            
            DCIteratorBinding binding = ADFUtils.findIterator("DespatchScheduleHeaderVO1Iterator");
            if (binding.getEstimatedRowCount() > 0 && binding.getCurrentRow() != null){
              DespatchScheduleHeaderVORowImpl row = (DespatchScheduleHeaderVORowImpl) binding.getCurrentRow();
                if (row.getAuthBy() != null && row.getAuthBy() != "" &&
                    row.getAuthBy().toString().trim().length() > 0) {
                    System.out.println("inside amendmen method==");
                    RichPopup.PopupHints hints = new RichPopup.PopupHints();
                    getConfirmationPopUpBind().show(hints);
                } else{
                    cevmodecheck();
                    disableDateMonthwise();
                    
                }
            }
            
        }
        else{
                
        if((inInt.compareTo(monthVal)==0 && appByBinding.getValue().toString().isEmpty()) && (yr.equals(prevYear))) {
            System.out.println("inside month compare method");
           
            cevmodecheck();
            disableDateMonthwise();
            
        }
        else{
           ADFUtils.showMessage("Back Month schedule cannot be modified", 2);
            ADFUtils.setEL("#{pageFlowScope.mode}", "V");
        }
        }/*  */
//        DCIteratorBinding binding = ADFUtils.findIterator("DespatchScheduleHeaderVO1Iterator");
//        if (binding.getEstimatedRowCount() > 0 && binding.getCurrentRow() != null){
//          DespatchScheduleHeaderVORowImpl row = (DespatchScheduleHeaderVORowImpl) binding.getCurrentRow();
//            if (row.getAuthBy() != null && row.getAuthBy() != "" &&
//                row.getAuthBy().toString().trim().length() > 0) {
//                System.out.println("inside amendmen method==");
//                RichPopup.PopupHints hints = new RichPopup.PopupHints();
//                getConfirmationPopUpBind().show(hints);
//            } else{
//                cevmodecheck();
//                
//            }
//        }
  
        disabledFieldsAll();
        disabledFieldAccordingToDate();
        disableDateMonthwise();

    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
            try {
                Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
                if (method1 != null) {
                    method1.invoke(component, valueComponent);
                }
            } catch (NoSuchMethodException e) {
                try {
                    Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                    if (method != null) {
                        method.invoke(component, valueComponent);
                    }
                } catch (Exception e1) {
                    // e.printStackTrace();//silently eat this exception.
                }


            } catch (Exception e) {
                // e.printStackTrace();//silently eat this exception.
            }
            List<UIComponent> childComponents = component.getChildren();
            for (UIComponent comp : childComponents) {
                makeComponentHierarchyReadOnly(comp, valueComponent);
            }
        }
    
    private void cevmodecheck() {
        System.out.println("ADFUtils.resolveExpression(\"#{pageFlowScope.mode}\")"+ADFUtils.resolveExpression("#{pageFlowScope.mode}"));
            if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
                cevModeDisableComponent("V");
            } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("C");
            } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("E");
            }
            
        }
    public void cevModeDisableComponent(String mode) {
        
        if(mode.equals("C")){
//            getPopulateBinding().setDisabled(false);
            getBindProdCode().setDisabled(true);
            getProcCodeBinding().setDisabled(true);
            getCustProdNoBinding().setDisabled(true);
            getSaleOrderQuantity().setDisabled(true);
            getScheduleNoBinding().setDisabled(true);
            getPoAmdNoBinding().setDisabled(true);
            getPoNoBinding().setDisabled(true);
            getAmdNoBinding().setDisabled(true);
            getSrNoBinding().setDisabled(true);
            getTransitPeriodBinding().setDisabled(true);
           getEntryDateBind().setDisabled(true);
            getAppByBinding().setDisabled(true);
            getAppByBind().setDisabled(true);
            getAmdDateBinding().setDisabled(true);
          getUnitBinding().setDisabled(true);
          //getEnterByBinding().setDisabled(true);
            getBindPoQty().setDisabled(true);
          
        }
        if(mode.equals("E")){
            System.out.println("Edit Mode");
//            getPopulateBinding().setDisabled(true);
            getBindProdCode().setDisabled(true);
            getProcCodeBinding().setDisabled(true);
            getCustProdNoBinding().setDisabled(true);
            getSaleOrderQuantity().setDisabled(true);
            getScheduleNoBinding().setDisabled(true);
            getPoAmdNoBinding().setDisabled(true);
            getPoNoBinding().setDisabled(true);
            getAmdNoBinding().setDisabled(true);
            getSrNoBinding().setDisabled(true);
            getTransitPeriodBinding().setDisabled(true);
          getEntryDateBind().setDisabled(true);
            getAppByBind().setDisabled(true);
            getRemarksBinding().setDisabled(true);
            getUnitBinding().setDisabled(true);
            //getMonthBInd().setDisabled(true);
            //getYearBind().setDisabled(true);
            getAmdDateBinding().setDisabled(true);
            getBindPoQty().setDisabled(true);
           
            
        }
        if(mode.equals("V")){
            System.out.println("View Mode");
//           getPopulateBinding().setDisabled(true);
            
        }
        }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setScheduleNoBinding(RichInputText scheduleNoBinding) {
        this.scheduleNoBinding = scheduleNoBinding;
    }

    public RichInputText getScheduleNoBinding() {
        return scheduleNoBinding;
    }

    public void setUnitBinding(RichInputComboboxListOfValues unitBinding) {
        this.unitBinding = unitBinding;
    }

    public RichInputComboboxListOfValues getUnitBinding() {
        return unitBinding;
    }

    public void setAppByBinding(RichInputComboboxListOfValues appByBinding) {
        this.appByBinding = appByBinding;
    }

    public RichInputComboboxListOfValues getAppByBinding() {
        return appByBinding;
    }

    public void setEnterByBinding(RichInputComboboxListOfValues enterByBinding) {
        this.enterByBinding = enterByBinding;
    }

    public RichInputComboboxListOfValues getEnterByBinding() {
        return enterByBinding;
    }

    public void setCustProdNoBinding(RichInputText custProdNoBinding) {
        this.custProdNoBinding = custProdNoBinding;
    }

    public RichInputText getCustProdNoBinding() {
        return custProdNoBinding;
    }

    public void setPoNoBinding(RichInputText poNoBinding) {
        this.poNoBinding = poNoBinding;
    }

    public RichInputText getPoNoBinding() {
        return poNoBinding;
    }

    public void setPoAmdNoBinding(RichInputText poAmdNoBinding) {
        this.poAmdNoBinding = poAmdNoBinding;
    }

    public RichInputText getPoAmdNoBinding() {
        return poAmdNoBinding;
    }

    public void setTransitPeriodBinding(RichInputText transitPeriodBinding) {
        this.transitPeriodBinding = transitPeriodBinding;
    }

    public RichInputText getTransitPeriodBinding() {
        return transitPeriodBinding;
    }

    public void setProcCodeBinding(RichInputText procCodeBinding) {
        this.procCodeBinding = procCodeBinding;
    }

    public RichInputText getProcCodeBinding() {
        return procCodeBinding;
    }

    public void setSaleOrderQuantity(RichInputText saleOrderQuantity) {
        this.saleOrderQuantity = saleOrderQuantity;
    }

    public RichInputText getSaleOrderQuantity() {
        return saleOrderQuantity;
    }
    public void amendConfirmDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            OperationBinding op = ADFUtils.findOperation("getAmdDespatchScheduleDetails");
            op.getParamsMap().put("DocNo", ADFUtils.resolveExpression("#{pageFlowScope.DocNo}"));
            op.getParamsMap().put("SoId", ADFUtils.resolveExpression("#{pageFlowScope.SoId}"));
            op.getParamsMap().put("AmdNo", ADFUtils.resolveExpression("#{pageFlowScope.AmdNo}"));
            op.execute();
          cevmodecheck();
        } else
            getConfirmationPopUpBind().hide();
    }

    public void setConfirmationPopUpBind(RichPopup confirmationPopUpBind) {
        this.confirmationPopUpBind = confirmationPopUpBind;
    }

    public RichPopup getConfirmationPopUpBind() {
        return confirmationPopUpBind;
    }


    

    public void setAmdNoBinding(RichInputText amdNoBinding) {
        this.amdNoBinding = amdNoBinding;
    }

    public RichInputText getAmdNoBinding() {
        return amdNoBinding;
    }

    public void setSrNoBinding(RichInputText srNoBinding) {
        this.srNoBinding = srNoBinding;
    }

    public RichInputText getSrNoBinding() {
        return srNoBinding;
    }


    public void setRemarksBinding(RichInputText remarksBinding) {
        this.remarksBinding = remarksBinding;
    }

    public RichInputText getRemarksBinding() {
        return remarksBinding;
    }

    public void setAmdDateBinding(RichInputDate amdDateBinding) {
        this.amdDateBinding = amdDateBinding;
    }

    public RichInputDate getAmdDateBinding() {
        return amdDateBinding;
    }

    public void setPoQtyBinding(RichColumn poQtyBinding) {
        this.poQtyBinding = poQtyBinding;
    }

    public RichColumn getPoQtyBinding() {
        return poQtyBinding;
    }
    
    public void disableDateMonthwise() {
                
                int flag=0;
                System.out.println("aaaaaaaaaaaaaaabbbbbbbbbbbbbbbbbb"+monthBInd.getValue());
                if(monthBInd.getValue()!= null){
                    
                    
                    
                    
                    if(monthBInd.getValue().equals("JAN")||monthBInd.getValue().equals("MAR")
                       ||monthBInd.getValue().equals("MAY")||monthBInd.getValue().equals("JUL")
                       ||monthBInd.getValue().equals("AUG")||monthBInd.getValue().equals("OCT")||monthBInd.getValue().equals("DEC")) {
                        System.out.println("inside first if====== ");
                        bindDt31.setDisabled(false);
                       
                        
                    }
                    else if(monthBInd.getValue().equals("APR")||monthBInd.getValue().equals("JUN")
                            ||monthBInd.getValue().equals("SEP")||monthBInd.getValue().equals("NOV")){
                                System.out.println("inside second if====== ");

                                bindDt31.setDisabled(true);
                                
                            }
                    else if(monthBInd.getValue().equals("FEB")){
                        
                        System.out.println("inside third if====== ");

                        int yr=(Integer)yearBind.getValue();
                        if(yr%4==0 && yr%100 !=0){
                            
                            flag=1;
                            
                        }
                        else {
                            
                            flag=0;
                        }
                       
                        if(flag==1) {
                            
                            bindDt29.setDisabled(false);
                            bindDt30.setDisabled(true);
                            bindDt31.setDisabled(true);
                        }
                        else {
                            bindDt29.setDisabled(true);
                            bindDt30.setDisabled(true);
                            bindDt31.setDisabled(true);
                            
                            
                        }
                    }
                    
                    
                    
                }
                
            }


    public void setTypeBinding(RichSelectOneChoice typeBinding) {
        this.typeBinding = typeBinding;
    }

    public RichSelectOneChoice getTypeBinding() {
        return typeBinding;
    }

    public void setDetailTableBinding(RichTable detailTableBinding) {
        this.detailTableBinding = detailTableBinding;
    }

    public RichTable getDetailTableBinding() {
        return detailTableBinding;
    }
    
    
    public void disabledFieldsAll()
    {
        getBindDt1().setDisabled(true);
        getBindDt2().setDisabled(true);
        getBindDt3().setDisabled(true);
        getBindDt4().setDisabled(true);
        getBindDt5().setDisabled(true);
        getBindDt6().setDisabled(true);
        getBindDt7().setDisabled(true);
        getBindDt8().setDisabled(true);
        getBindDt9().setDisabled(true);
        getBindDt10().setDisabled(true);
        getBindDt11().setDisabled(true);
        getBindDt12().setDisabled(true);
        getBindDt13().setDisabled(true);
        getBindDt14().setDisabled(true);
        getBindDt15().setDisabled(true);
        getBindDt16().setDisabled(true);
        getBindDt17().setDisabled(true);
        getBindDt18().setDisabled(true);
        getBindDt19().setDisabled(true);
        getBindDt20().setDisabled(true);
        getBindDt21().setDisabled(true);
        getBindDt22().setDisabled(true);
        getBindDt23().setDisabled(true);
        getBindDt24().setDisabled(true);
        getBindDt25().setDisabled(true);
        getBindDt26().setDisabled(true);
        getBindDt27().setDisabled(true);
        getBindDt28().setDisabled(true);
        getBindDt29().setDisabled(true);
        getBindDt30().setDisabled(true);
        getBindDt31().setDisabled(true);
        
    }
    
    public void disabledFieldAccordingToDate()
    {
//        oracle.jbo.domain.Timestamp d1=(oracle.jbo.domain.Timestamp)getEntryDateBind().getValue();
//        
//        Date now = new Date(d1.getTime());
//        Calendar cal=Calendar.getInstance();
//        cal.get
//        System.out.println("*******Value is *******===> "+d1);
//        Integer d=(Integer)cal.get(Calendar.DATE);
        Integer d=(Integer)dayTransBinding.getValue();
        String month=(String)monthBInd.getValue();
        String monthTrans=(String)monthTransBind.getValue();
       System.out.println("Month : "+month+" Month Trans: "+monthTrans);
        System.out.println("******************Value is D in Edit Mode ===>  "+d);
        if(!d.equals(null)&& month.equalsIgnoreCase(monthTrans))
        {
            if (d <= 1) {
                getBindDt1().setDisabled(false);
            }
            if (d <= 2) {
                getBindDt2().setDisabled(false);
            }
            if (d <= 3) {
                getBindDt3().setDisabled(false);
            }
            if (d <= 4) {
                getBindDt4().setDisabled(false);
            }
            if (d <= 5) {
                getBindDt5().setDisabled(false);
            }
            if (d <= 6) {
                getBindDt6().setDisabled(false);
            }
            if (d <= 7) {
                getBindDt7().setDisabled(false);
            }
            if (d <= 8) {
                getBindDt8().setDisabled(false);
            }
            if (d <= 9) {
                getBindDt9().setDisabled(false);
            }
            if (d <= 10) {
                getBindDt10().setDisabled(false);
            }
            if (d <= 11) {
                getBindDt11().setDisabled(false);
            }
            if (d <= 12) {
                getBindDt12().setDisabled(false);
            }
            if (d <= 13) {
                getBindDt13().setDisabled(false);
            }
            if (d <= 14) {
                getBindDt14().setDisabled(false);
            }
            if (d <= 15) {
                getBindDt15().setDisabled(false);
            }
            if (d <= 16) {
                getBindDt16().setDisabled(false);
            }
            if (d <= 17) {
                getBindDt17().setDisabled(false);
            }
            if (d <= 18) {
                getBindDt18().setDisabled(false);
            }
            if (d <= 19) {
                getBindDt19().setDisabled(false);
            }
            if (d <=20) {
                getBindDt20().setDisabled(false);
            }
            if (d <= 21) {
                getBindDt21().setDisabled(false);
            }
            if (d <= 22) {
                getBindDt22().setDisabled(false);
            }
            if (d <= 23) {
                getBindDt23().setDisabled(false);
            }
            if (d <= 24) {
                getBindDt24().setDisabled(false);
            }
            if (d <= 25) {
                getBindDt25().setDisabled(false);
            }
            if (d <= 26) {
                getBindDt26().setDisabled(false);
            }
            if (d <= 27) {
                getBindDt27().setDisabled(false);
            }
            if (d <= 28) {
                getBindDt28().setDisabled(false);
            }
            if (d <= 29) {
                getBindDt29().setDisabled(false);
            }
            if (d <= 30) {
                getBindDt30().setDisabled(false);
            }
            if (d <= 31) {
                getBindDt31().setDisabled(false);
            }        
        }    
        else{
                getBindDt1().setDisabled(false);
                getBindDt2().setDisabled(false);
                getBindDt3().setDisabled(false);
                getBindDt4().setDisabled(false);
                getBindDt5().setDisabled(false);
                getBindDt6().setDisabled(false);
                getBindDt7().setDisabled(false);
                getBindDt8().setDisabled(false);
                getBindDt9().setDisabled(false);
                getBindDt10().setDisabled(false);
                getBindDt11().setDisabled(false);
                getBindDt12().setDisabled(false);
                getBindDt13().setDisabled(false);
                getBindDt14().setDisabled(false);
                getBindDt15().setDisabled(false);
                getBindDt16().setDisabled(false);
                getBindDt17().setDisabled(false);
                getBindDt18().setDisabled(false);
                getBindDt19().setDisabled(false);
                getBindDt20().setDisabled(false);
                getBindDt21().setDisabled(false);
                getBindDt22().setDisabled(false);
                getBindDt23().setDisabled(false);
                getBindDt24().setDisabled(false);
                getBindDt25().setDisabled(false);
                getBindDt26().setDisabled(false);
                getBindDt27().setDisabled(false);
                getBindDt28().setDisabled(false);
                getBindDt29().setDisabled(false);
                getBindDt30().setDisabled(false);
                getBindDt31().setDisabled(false);
            }
    }


    public void setDayTransBinding(RichInputText dayTransBinding) {
        this.dayTransBinding = dayTransBinding;
    }

    public RichInputText getDayTransBinding() {
        return dayTransBinding;
    }

    public void ApprovedVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());            
                     OperationBinding ob = ADFUtils.findOperation("checkApprovalStatus");
                    ob.getParamsMap().put("formNm", "DS");
                    ob.getParamsMap().put("authoLim", "AP");
                    ob.getParamsMap().put("empcd", vce.getNewValue().toString());
                    ob.execute();
                    System.out.println(" EMP CD"+vce.getNewValue().toString());
                    if((ob.getResult() !=null && ob.getResult().equals("Y")))
                    {
                        Row row=(Row)ADFUtils.evaluateEL("#{bindings.DespatchScheduleHeaderVO1Iterator.currentRow}");
                        row.setAttribute("AuthBy", null);
                        ADFUtils.showMessage("You have no authority to approve this Dispatch Schedule.",0);
        }
    }

    public void setMonthTransBind(RichInputText monthTransBind) {
        this.monthTransBind = monthTransBind;
    }

    public RichInputText getMonthTransBind() {
        return monthTransBind;
    }
}
