package terms.mkt.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class EnquiryBean {
    private RichTable enquiryTableBinding;
    private RichButton deleteFollowupBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputDate enquiryDateBinding;
    private RichInputText enquiryNoBinding;
    private RichInputText plantBinding;
    private RichInputComboboxListOfValues cityBinding;
    private RichOutputText bindingoutputtext;
    private RichButton detailCreateBinding;
    private RichButton headerEditBinding;
    private RichPanelHeader getMyPageRootBinding;
    private RichInputText salesRepBinding;
    private RichInputText productDescriptionBinding;
    private RichShowDetailItem tabLabel1Binding;
    private RichShowDetailItem tabLabel2Binding;

    public EnquiryBean() {
    }

     public void getEnquiryNoAL(ActionEvent actionEvent) {
       
       
       
       System.out.println("Bean####");
            OperationBinding op = ADFUtils.findOperation("getEnquiryNumber");
                   Object rst = op.execute();
                 
                   
                   
                   
                   System.out.println("--------Commit-------");
                       System.out.println("value aftr getting result--->?"+rst);
                       
                   
                       if (rst != null && rst.toString() != "") {
                           if (op.getErrors().isEmpty()) {
                               ADFUtils.findOperation("Commit").execute();
                               FacesMessage Message = new FacesMessage("Record Saved Successfully. New Enquiry No is "+rst+".");   
                               Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                               FacesContext fc = FacesContext.getCurrentInstance();   
                               fc.addMessage(null, Message);  
                   
                           }
                       }
                       
                       if (rst == null || rst.toString() == "" || rst.toString() == " ") {
                           if (op.getErrors().isEmpty()) {
                               ADFUtils.findOperation("Commit").execute();
                               FacesMessage Message = new FacesMessage("Record Update Successfully.");   
                               Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                               FacesContext fc = FacesContext.getCurrentInstance();   
                               fc.addMessage(null, Message);  
                       
                           }

                       
                       }
                   }


    public void deletePopupDialogAL(DialogEvent dialogEvent) 
    {
            if(dialogEvent.getOutcome().name().equals("ok"))
                        {
                        ADFUtils.findOperation("Delete").execute();
                        System.out.println("Record Delete Successfully");
                       
                            FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                            Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                            FacesContext fc = FacesContext.getCurrentInstance();  
                            fc.addMessage(null, Message);  
                       
                        }
                AdfFacesContext.getCurrentInstance().addPartialTarget(enquiryTableBinding);
    }

    public void setEnquiryTableBinding(RichTable enquiryTableBinding) {
        this.enquiryTableBinding = enquiryTableBinding;
    }

    public RichTable getEnquiryTableBinding() {
        return enquiryTableBinding;
    }

    /*     public void saveButtonAL(ActionEvent actionEvent)
    {
        System.out.println("Bean####");
             OperationBinding op = ADFUtils.findOperation("getEnquiryNumber");
                    Object rst = op.execute();




                    System.out.println("--------Commit-------");
                        System.out.println("value aftr getting result--->?"+rst);
    } */
    public void deleteFollowuppopup(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                    {
                    ADFUtils.findOperation("Delete2").execute();
                    System.out.println("Record Delete Successfully");
                   
                        FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                        FacesContext fc = FacesContext.getCurrentInstance();  
                        fc.addMessage(null, Message);  
                   
                    }

                AdfFacesContext.getCurrentInstance().addPartialTarget(deleteFollowupBinding);
        
    }    
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
            try {
                Method method1 =
                    component.getClass().getMethod("setDisabled", boolean.class);
                if (method1 != null) {
                    method1.invoke(component, valueComponent);
                }
            } catch (NoSuchMethodException e) {
                try {
                    Method method =
                        component.getClass().getMethod("setReadOnly", boolean.class);
                    if (method != null) {
                        method.invoke(component, valueComponent);
                    }
                } catch (Exception e1) {
                    // e.printStackTrace();//silently eat this exception.
                }


            } catch (Exception e) {
                // e.printStackTrace();//silently eat this exception.
            }
            List<UIComponent> childComponents = component.getChildren();
            for (UIComponent comp : childComponents) {
                makeComponentHierarchyReadOnly(comp, valueComponent);
            }
        }
        
        
                        private void cevmodecheck(){
                            if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
                                makeComponentHierarchyReadOnly(this.getGetMyPageRootBinding(), true);
                                cevModeDisableComponent("V");
                            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
                                makeComponentHierarchyReadOnly(this.getGetMyPageRootBinding(), false);
                                cevModeDisableComponent("C");
                            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
                                makeComponentHierarchyReadOnly(this.getGetMyPageRootBinding(), false);
                                cevModeDisableComponent("E");
                            }
                        }
        
    public void cevModeDisableComponent(String mode) {
        if (mode.equals("E")) {
                getSalesRepBinding().setDisabled(true);
                getUnitCodeBinding().setDisabled(true);
                getEnquiryDateBinding().setDisabled(true);
                getEnquiryNoBinding().setDisabled(true);
                getCityBinding().setDisabled(true);
                getPlantBinding().setDisabled(true);
                getDetailCreateBinding().setDisabled(false);
                getHeaderEditBinding().setDisabled(true);
                getProductDescriptionBinding().setDisabled(true);
        } else if (mode.equals("C")) {
            getSalesRepBinding().setDisabled(true);
            getEnquiryNoBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
            getDetailCreateBinding().setDisabled(false);
            getProductDescriptionBinding().setDisabled(true);
        } else if (mode.equals("V")) {
            getDetailCreateBinding().setDisabled(true);
            getTabLabel1Binding().setDisabled(false);
            getTabLabel2Binding().setDisabled(false);
        }
    }    
    

    public void setDeleteFollowupBinding(RichButton deleteFollowupBinding) {
        this.deleteFollowupBinding = deleteFollowupBinding;
    }

    public RichButton getDeleteFollowupBinding() {
        return deleteFollowupBinding;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setEnquiryDateBinding(RichInputDate enquiryDateBinding) {
        this.enquiryDateBinding = enquiryDateBinding;
    }

    public RichInputDate getEnquiryDateBinding() {
        return enquiryDateBinding;
    }

    public void setEnquiryNoBinding(RichInputText enquiryNoBinding) {
        this.enquiryNoBinding = enquiryNoBinding;
    }

    public RichInputText getEnquiryNoBinding() {
        return enquiryNoBinding;
    }

    public void setPlantBinding(RichInputText plantBinding) {
        this.plantBinding = plantBinding;
    }

    public RichInputText getPlantBinding() {
        return plantBinding;
    }

    public void setCityBinding(RichInputComboboxListOfValues cityBinding) {
        this.cityBinding = cityBinding;
    }

    public RichInputComboboxListOfValues getCityBinding() {
        return cityBinding;
    }

    public void setBindingoutputtext(RichOutputText bindingoutputtext) {
        cevmodecheck();
        this.bindingoutputtext = bindingoutputtext;
    }

    public RichOutputText getBindingoutputtext() {
        return bindingoutputtext;
    }

    public void setDetailCreateBinding(RichButton detailCreateBinding) {
        this.detailCreateBinding = detailCreateBinding;
    }

    public RichButton getDetailCreateBinding() {
        return detailCreateBinding;
    }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setGetMyPageRootBinding(RichPanelHeader getMyPageRootBinding) {
        this.getMyPageRootBinding = getMyPageRootBinding;
    }

    public RichPanelHeader getGetMyPageRootBinding() {
        return getMyPageRootBinding;
    }

    public void setSalesRepBinding(RichInputText salesRepBinding) {
        this.salesRepBinding = salesRepBinding;
    }

    public RichInputText getSalesRepBinding() {
        return salesRepBinding;
    }

    public void setProductDescriptionBinding(RichInputText productDescriptionBinding) {
        this.productDescriptionBinding = productDescriptionBinding;
    }

    public RichInputText getProductDescriptionBinding() {
        return productDescriptionBinding;
    }

    public void setTabLabel1Binding(RichShowDetailItem tabLabel1Binding) {
        this.tabLabel1Binding = tabLabel1Binding;
    }

    public RichShowDetailItem getTabLabel1Binding() {
        return tabLabel1Binding;
    }

    public void setTabLabel2Binding(RichShowDetailItem tabLabel2Binding) {
        this.tabLabel2Binding = tabLabel2Binding;
    }

    public RichShowDetailItem getTabLabel2Binding() {
        return tabLabel2Binding;
    }
}
