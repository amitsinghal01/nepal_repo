package terms.mkt.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class ExportShipmentPlanBean {
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText unitNameBinding;
    private RichInputText entryNumberBinding;
    private RichInputDate entryDateBinding;
    private RichInputText partyCodeBinding;
    private RichInputText partyNameBinding;
    private RichInputText poNumberBinding;
    private RichInputDate poDateBinding;
    private RichInputText quantityBinding;
    private RichInputText numberOfPackagesBinding;
    private RichInputDate dispatchDateBinding;
    private RichInputText remarkBinding;
    private RichOutputText bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichTable createDetailTableBinding;
    private RichInputComboboxListOfValues partyCode1Binding;
    private RichInputComboboxListOfValues productBinding;
    private RichPopup editPopupBinding;
    private RichInputText amdNoBinding;
    private RichInputDate amdDateBinding;

    public ExportShipmentPlanBean() {
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setUnitNameBinding(RichInputText unitNameBinding) {
        this.unitNameBinding = unitNameBinding;
    }

    public RichInputText getUnitNameBinding() {
        return unitNameBinding;
    }

    public void setEntryNumberBinding(RichInputText entryNumberBinding) {
        this.entryNumberBinding = entryNumberBinding;
    }

    public RichInputText getEntryNumberBinding() {
        return entryNumberBinding;
    }

    public void setEntryDateBinding(RichInputDate entryDateBinding) {
        this.entryDateBinding = entryDateBinding;
    }

    public RichInputDate getEntryDateBinding() {
        return entryDateBinding;
    }

    public void setPartyCodeBinding(RichInputText partyCodeBinding) {
        this.partyCodeBinding = partyCodeBinding;
    }

    public RichInputText getPartyCodeBinding() {
        return partyCodeBinding;
    }

    public void setPartyNameBinding(RichInputText partyNameBinding) {
        this.partyNameBinding = partyNameBinding;
    }

    public RichInputText getPartyNameBinding() {
        return partyNameBinding;
    }

    public void setPoNumberBinding(RichInputText poNumberBinding) {
        this.poNumberBinding = poNumberBinding;
    }

    public RichInputText getPoNumberBinding() {
        return poNumberBinding;
    }

    public void setPoDateBinding(RichInputDate poDateBinding) {
        this.poDateBinding = poDateBinding;
    }

    public RichInputDate getPoDateBinding() {
        return poDateBinding;
    }


    public void setQuantityBinding(RichInputText quantityBinding) {
        this.quantityBinding = quantityBinding;
    }

    public RichInputText getQuantityBinding() {
        return quantityBinding;
    }

    public void setNumberOfPackagesBinding(RichInputText numberOfPackagesBinding) {
        this.numberOfPackagesBinding = numberOfPackagesBinding;
    }

    public RichInputText getNumberOfPackagesBinding() {
        return numberOfPackagesBinding;
    }

    public void setDispatchDateBinding(RichInputDate dispatchDateBinding) {
        this.dispatchDateBinding = dispatchDateBinding;
    }

    public RichInputDate getDispatchDateBinding() {
        return dispatchDateBinding;
    }

    public void setRemarkBinding(RichInputText remarkBinding) {
        this.remarkBinding = remarkBinding;
    }

    public RichInputText getRemarkBinding() {
        return remarkBinding;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        cevmodecheck();
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

  
    private void cevmodecheck(){
          if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
              makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
              cevModeDisableComponent("V");
          }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
              makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
              cevModeDisableComponent("C");
          }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
              makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
              cevModeDisableComponent("E");
          }
      }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                try {
                    Method method1 =
                        component.getClass().getMethod("setDisabled", boolean.class);
                    if (method1 != null) {
                        method1.invoke(component, valueComponent);
                    }
                } catch (NoSuchMethodException e) {
                    try {
                        Method method =
                            component.getClass().getMethod("setReadOnly", boolean.class);
                        if (method != null) {
                            method.invoke(component, valueComponent);
                        }
                    } catch (Exception e1) {
                        // e.printStackTrace();//silently eat this exception.
                    }


                } catch (Exception e) {
                    // e.printStackTrace();//silently eat this exception.
                }
                List<UIComponent> childComponents = component.getChildren();
                for (UIComponent comp : childComponents) {
                    makeComponentHierarchyReadOnly(comp, valueComponent);
                }
            }
    
    //Set Fields and Button disable.
        public void cevModeDisableComponent(String mode) {
        //            FacesContext fctx = FacesContext.getCurrentInstance();
        //            ELContext elctx = fctx.getELContext();
        //            Application jsfApp = fctx.getApplication();
        //            //create a ValueExpression that points to the ADF binding layer
        //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
        //            //
        //            ValueExpression valueExpr = exprFactory.createValueExpression(
        //                                         elctx,
        //                                         "#{pageFlowScope.mode=='E'}",
        //                                          Object.class
        //                                         );
        //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
            if (mode.equals("E")) {
                getUnitCodeBinding().setDisabled(true);
                getUnitNameBinding().setDisabled(true);
                getEntryNumberBinding().setDisabled(true);
                getEntryDateBinding().setDisabled(true);
                getPoDateBinding().setDisabled(true);
               // getPartyCodeBinding().setDisabled(true);
                getPartyNameBinding().setDisabled(true);
                getHeaderEditBinding().setDisabled(true);
                //getPartyCode1Binding().setDisabled(true);
                //getProductBinding().setDisabled(true);
                getAmdNoBinding().setDisabled(true);
                getAmdDateBinding().setDisabled(true);
            } else if (mode.equals("C")) {
               getUnitCodeBinding().setDisabled(true);
                getUnitNameBinding().setDisabled(true);
                getEntryNumberBinding().setDisabled(true);
                getPoDateBinding().setDisabled(true);
              //  getPartyCodeBinding().setDisabled(true);
                getPartyNameBinding().setDisabled(true);
                getAmdNoBinding().setDisabled(true);
                getAmdDateBinding().setDisabled(true);
            } else if (mode.equals("V")) {
            }
            
        }

    public void editButtonAL(ActionEvent actionEvent) {
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        ADFUtils.setEL("#{pageFlowScope.mode}", "V");
        editPopupBinding.show(hints);
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok"))
        {
            ADFUtils.findOperation("Delete").execute();
            OperationBinding op=  ADFUtils.findOperation("Commit");
            Object rst = op.execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }

        AdfFacesContext.getCurrentInstance().addPartialTarget(createDetailTableBinding);
    }

    public void saveButtonAL(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedBy("ExportShipmentPlanHeaderVO1Iterator","LastUpdatedBy");
        oracle.binding.OperationBinding op = ADFUtils.findOperation("generateEntryNumberForShipmentPlan");
        Object rst = op.execute();

        System.out.println("--------Commit-------");
        System.out.println("Value aftr getting result--->?" + rst);


        if (rst.toString() != null && rst.toString() != "") {
            if (op.getErrors().isEmpty()) {
                ADFUtils.findOperation("Commit").execute();
                FacesMessage Message = new FacesMessage("Record Saved Successfully. New Entry Number is " + rst + ".");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            }
        }
        if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ") {
            if (op.getErrors().isEmpty()) {
                ADFUtils.findOperation("Commit").execute();
                FacesMessage Message = new FacesMessage("Record Updated Successfully.");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            }
        }
    }

    public void setCreateDetailTableBinding(RichTable createDetailTableBinding) {
        this.createDetailTableBinding = createDetailTableBinding;
    }

    public RichTable getCreateDetailTableBinding() {
        return createDetailTableBinding;
    }

    public void setPartyCode1Binding(RichInputComboboxListOfValues partyCode1Binding) {
        this.partyCode1Binding = partyCode1Binding;
    }

    public RichInputComboboxListOfValues getPartyCode1Binding() {
        return partyCode1Binding;
    }

    public void setProductBinding(RichInputComboboxListOfValues productBinding) {
        this.productBinding = productBinding;
    }

    public RichInputComboboxListOfValues getProductBinding() {
        return productBinding;
    }

    public void setEditPopupBinding(RichPopup editPopupBinding) {
        this.editPopupBinding = editPopupBinding;
    }

    public RichPopup getEditPopupBinding() {
        return editPopupBinding;
    }

    public void amdConfirmDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
             {
                 
                oracle.binding.OperationBinding op1=ADFUtils.findOperation("checkMaxAmendmentExportShipment");
                 Object ob=op1.execute();
                 if(op1.getResult().toString()!=null && op1.getResult().toString().equalsIgnoreCase("N")){
                     
                     ADFUtils.showMessage("You Can Update Latest Amendment Only.", 2);
                     ADFUtils.setEL("#{pageFlowScope.mode}", "V");
                 }
                 else{
                
                System.out.println("AmdNo==="+ADFUtils.resolveExpression("#{pageFlowScope.AmdNo}"));
                 System.out.println("EntryId==="+ADFUtils.resolveExpression("#{pageFlowScope.EntryId}"));
                 System.out.println("EntryNo==="+ADFUtils.resolveExpression("#{pageFlowScope.EntryNo}"));
             oracle.binding.OperationBinding op = ADFUtils.findOperation("getAmdExportShipment");
             op.getParamsMap().put("AmdNo", ADFUtils.resolveExpression("#{pageFlowScope.AmdNo}"));
             op.getParamsMap().put("EntryId", ADFUtils.resolveExpression("#{pageFlowScope.EntryId}"));
             op.getParamsMap().put("EntryNo", ADFUtils.resolveExpression("#{pageFlowScope.EntryNo}"));
             op.execute();
             ADFUtils.setEL("#{pageFlowScope.mode}", "E");
             cevmodecheck();
             AdfFacesContext.getCurrentInstance().addPartialTarget(createDetailTableBinding);
             AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
             }
             }
    }

    public void setAmdNoBinding(RichInputText amdNoBinding) {
        this.amdNoBinding = amdNoBinding;
    }

    public RichInputText getAmdNoBinding() {
        return amdNoBinding;
    }

    public void setAmdDateBinding(RichInputDate amdDateBinding) {
        this.amdDateBinding = amdDateBinding;
    }

    public RichInputDate getAmdDateBinding() {
        return amdDateBinding;
    }
}
