package terms.mkt.transaction.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import javax.faces.validator.ValidatorException;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;

public class FreightEntryBean {
    private RichTable detailTableBinding;
    private RichPanelHeader getMyPageRootBinding;
    private RichOutputText outputTextBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputDate entryDateBinding;
    private RichInputText entryNoBinding;
    private RichButton editButtonBinding;
    private RichInputComboboxListOfValues preparedByBinding;
    private RichSelectOneChoice typeBinding;
    private RichInputText documentNoBinding;
    private RichInputDate docDateBinding;
    private RichInputText qtyBinding;
    private RichInputText productCodeBinding;
    int i=0;
    private RichInputComboboxListOfValues checkedByBinding;
    private RichInputComboboxListOfValues receivedByBinding;
    private RichInputComboboxListOfValues authorizedByBinding;
    private RichInputDate checkedDateBinding;
    private RichInputDate receivedDateBinding;
    private RichInputDate authorisedDateBinding;

    public FreightEntryBean() {
    }

    public void saveAL(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedBy("FreightEntryHeaderVO1Iterator","LastUpdatedBy");
        
        if ((Long) ADFUtils.evaluateEL("#{bindings.FreightEntryDetailVO1Iterator.estimatedRowCount}")>0)
        {              
            OperationBinding op = ADFUtils.findOperation("generateFreightEntryNo");
            op.execute();
            System.out.println("Result is===> "+op.getResult());
            if(op.getResult() != null && op.getResult().toString().equalsIgnoreCase("E"))
            {
                ADFUtils.findOperation("Commit").execute();
                ADFUtils.showMessage(" Record Updated Successfully", 2);
                
            }
            else if (op.getResult() != null && op.getResult().toString() != ""  && op.getResult().equals("S"))
            {
                System.out.println("+++++ in the save mode+++++++++++");
                ADFUtils.findOperation("Commit").execute();
                ADFUtils.showMessage("Record Saved Successfully.Freight Entry No.is "+ADFUtils.evaluateEL("#{bindings.EntryNo.inputValue}"), 2);
                
            }
            else if(op.getResult() != null && op.getResult().toString().equalsIgnoreCase("N"))
            {
                ADFUtils.showMessage("Entry No. could not be generated. Try Again !!", 0);
            }
        }
        else{
            ADFUtils.showMessage("Please Enter any record in the Detail table.", 0);
        }
    }

    public void deleteDetailDialogDL(DialogEvent dialogEvent) 
    {
        if(dialogEvent.getOutcome().name().equals("ok"))
        {
            ADFUtils.findOperation("Delete").execute();
 //           ADFUtils.findOperation("Commit").execute();    //Unable on Search page.
            System.out.println("Record Delete Successfully");
        
           FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
           Message.setSeverity(FacesMessage.SEVERITY_INFO);  
           FacesContext fc = FacesContext.getCurrentInstance();  
           fc.addMessage(null, Message);  
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(detailTableBinding);
    }

    public void createDtlAL(ActionEvent actionEvent) 
    {
        
        ADFUtils.findOperation("CreateInsert").execute();
        ADFUtils.findOperation("setFreightEntryNo").execute();
        if ((Long) ADFUtils.evaluateEL("#{bindings.FreightEntryDetailVO1Iterator.estimatedRowCount}")>0
         && ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")){
            
            System.out.println("--------=========llll-----");
            getTypeBinding().setDisabled(true);
        }
        else{
            getTypeBinding().setDisabled(false);
        }
    }
    
    
    
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
            try {
                Method method1 =
                    component.getClass().getMethod("setDisabled", boolean.class);
                if (method1 != null) {
                    method1.invoke(component, valueComponent);
                }
            } catch (NoSuchMethodException e) {
                try {
                    Method method =
                        component.getClass().getMethod("setReadOnly", boolean.class);
                    if (method != null) {
                        method.invoke(component, valueComponent);
                    }
                } catch (Exception e1) {
                    // e.printStackTrace();//silently eat this exception.
                }


            } catch (Exception e) {
                // e.printStackTrace();//silently eat this exception.
            }
            List<UIComponent> childComponents = component.getChildren();
            for (UIComponent comp : childComponents) {
                makeComponentHierarchyReadOnly(comp, valueComponent);
            }
        }
        
        
    private void cevmodecheck()
    {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
                makeComponentHierarchyReadOnly(this.getGetMyPageRootBinding(), true);
                cevModeDisableComponent("V");
         }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootBinding(), false);
                cevModeDisableComponent("C");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootBinding(), false);
                cevModeDisableComponent("E");
        }
    }
        
    public void cevModeDisableComponent(String mode) {
        if (mode.equals("E")) {
                
                getDocumentNoBinding().setDisabled(true);
                getDocDateBinding().setDisabled(true);
                getProductCodeBinding().setDisabled(true);
               // getQtyBinding().setDisabled(true);
                
                
                getPreparedByBinding().setDisabled(true);
                getTypeBinding().setDisabled(true);
                getEntryDateBinding().setDisabled(true);
                getEntryNoBinding().setDisabled(true);
                getUnitCodeBinding().setDisabled(true);
                checkedDateBinding.setDisabled(true);
                receivedDateBinding.setDisabled(true);
                authorisedDateBinding.setDisabled(true);
                
        } else if (mode.equals("C")) {
            
            getDocumentNoBinding().setDisabled(true);
            getDocDateBinding().setDisabled(true);
            getProductCodeBinding().setDisabled(true);
         //   getQtyBinding().setDisabled(true);
            
            getPreparedByBinding().setDisabled(true);
            //getEntryDateBinding().setDisabled(true);
            getEntryNoBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
            checkedByBinding.setDisabled(true);
            checkedDateBinding.setDisabled(true);
            receivedByBinding.setDisabled(true);
            receivedDateBinding.setDisabled(true);
            authorizedByBinding.setDisabled(true);
            authorisedDateBinding.setDisabled(true);
        } else if (mode.equals("V")) {
           
        }
    }
    public void setDetailTableBinding(RichTable detailTableBinding) {
        this.detailTableBinding = detailTableBinding;
    }

    public RichTable getDetailTableBinding() {
        return detailTableBinding;
    }

    public void setGetMyPageRootBinding(RichPanelHeader getMyPageRootBinding) {
        this.getMyPageRootBinding = getMyPageRootBinding;
    }

    public RichPanelHeader getGetMyPageRootBinding() {
        return getMyPageRootBinding;
    }

    public void editAL(ActionEvent actionEvent) {
        if(checkedByBinding.getValue()!=null && receivedByBinding.getValue()!=null && authorizedByBinding.getValue()!=null)
        {
            ADFUtils.showMessage("Authorised Freight Entry cannot be edited further.", 0);
        }
        else{
        cevmodecheck();
        if(checkedByBinding.getValue()==null)
        {
            receivedByBinding.setDisabled(true);
            authorizedByBinding.setDisabled(true);
        }else if(receivedByBinding.getValue()==null)
        {
            checkedByBinding.setDisabled(true);
            authorizedByBinding.setDisabled(true);
        }else if(authorizedByBinding.getValue()==null)
        {
            checkedByBinding.setDisabled(true);
            receivedByBinding.setDisabled(true);
        }
        }
    }

    public void setOutputTextBinding(RichOutputText outputTextBinding) {
        cevmodecheck();
        this.outputTextBinding = outputTextBinding;
    }

    public RichOutputText getOutputTextBinding() {
        return outputTextBinding;
    }
    
    public String saveandCloseAL() {
        ADFUtils.setLastUpdatedBy("FreightEntryHeaderVO1Iterator","LastUpdatedBy");
        if ((Long) ADFUtils.evaluateEL("#{bindings.FreightEntryDetailVO1Iterator.estimatedRowCount}")>0)
        {              
            OperationBinding op = ADFUtils.findOperation("generateFreightEntryNo");
            op.execute();
            System.out.println("Result is===> "+op.getResult());
            if(op.getResult() != null && op.getResult().toString().equalsIgnoreCase("E"))
            {
                ADFUtils.findOperation("Commit").execute();
                ADFUtils.showMessage(" Record Updated Successfully", 2);
                return "saveAndClose";
            }
            else if (op.getResult() != null && op.getResult().toString() != ""  && op.getResult().equals("S"))
            {
                System.out.println("+++++ in the save mode+++++++++++");
                ADFUtils.findOperation("Commit").execute();
                ADFUtils.showMessage("Record Saved Successfully.Freight Entry No.is "+ADFUtils.evaluateEL("#{bindings.EntryNo.inputValue}"), 2);
                return "saveAndClose";
            }
            else if(op.getResult() != null && op.getResult().toString().equalsIgnoreCase("N"))
            {
                ADFUtils.showMessage("Entry No. could not be generated. Try Again !!", 0);
                return null;
            }
        }
        else{
            ADFUtils.showMessage("Please Enter any record in the Detail table.", 0);
            return null;
        }
        return null;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setEntryDateBinding(RichInputDate entryDateBinding) {
        this.entryDateBinding = entryDateBinding;
    }

    public RichInputDate getEntryDateBinding() {
        return entryDateBinding;
    }

    public void setEntryNoBinding(RichInputText entryNoBinding) {
        this.entryNoBinding = entryNoBinding;
    }

    public RichInputText getEntryNoBinding() {
        return entryNoBinding;
    }

    public void setEditButtonBinding(RichButton editButtonBinding) {
        this.editButtonBinding = editButtonBinding;
    }

    public RichButton getEditButtonBinding() {
        return editButtonBinding;
    }

    public void setPreparedByBinding(RichInputComboboxListOfValues preparedByBinding) {
        this.preparedByBinding = preparedByBinding;
    }

    public RichInputComboboxListOfValues getPreparedByBinding() {
        return preparedByBinding;
    }

    public void setTypeBinding(RichSelectOneChoice typeBinding) {
        this.typeBinding = typeBinding;
    }

    public RichSelectOneChoice getTypeBinding() {
        return typeBinding;
    }

    public void setDocumentNoBinding(RichInputText documentNoBinding) {
        this.documentNoBinding = documentNoBinding;
    }

    public RichInputText getDocumentNoBinding() {
        return documentNoBinding;
    }

    public void setDocDateBinding(RichInputDate docDateBinding) {
        this.docDateBinding = docDateBinding;
    }

    public RichInputDate getDocDateBinding() {
        return docDateBinding;
    }

    public void setQtyBinding(RichInputText qtyBinding) {
        this.qtyBinding = qtyBinding;
    }

    public RichInputText getQtyBinding() {
        return qtyBinding;
    }

    public void setProductCodeBinding(RichInputText productCodeBinding) {
        this.productCodeBinding = productCodeBinding;
    }

    public RichInputText getProductCodeBinding() {
        return productCodeBinding;
    }
    public void prartyCdVCL(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance()); 
        if(valueChangeEvent.getNewValue()!=null && productCodeBinding.getValue()!=null){
        int j=0;
        String partyCd=(String)valueChangeEvent.getNewValue();
        String productCd=(String)productCodeBinding.getValue();
        DCIteratorBinding dci=ADFUtils.findIterator("FreightEntryDetailVO1Iterator");
        RowSetIterator rsi=dci.getRowSetIterator();
        while(rsi.hasNext()){
            Row r=rsi.next();
            System.out.println("Party Code: "+r.getAttribute("PartyCd")+" Product Code: "+r.getAttribute("Product"));
            if(r.getAttribute("PartyCd")!=null && r.getAttribute("Product")!=null){
                if(partyCd.equalsIgnoreCase(r.getAttribute("PartyCd").toString()) && productCd.equalsIgnoreCase(r.getAttribute("Product").toString())){
                    System.out.println("Inside if condition!");
                    j=j+1;
                    }
                }
            
            }
        i=j;
            rsi.closeRowSetIterator();
        }
    }

    public void partyCdValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        System.out.println("Value of i: "+i);
        if(i>=1){
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                              "Party Code and Product Code combination alredy exists!", null));
            }

    }

    public void setCheckedByBinding(RichInputComboboxListOfValues checkedByBinding) {
        this.checkedByBinding = checkedByBinding;
    }

    public RichInputComboboxListOfValues getCheckedByBinding() {
        return checkedByBinding;
    }

    public void setReceivedByBinding(RichInputComboboxListOfValues receivedByBinding) {
        this.receivedByBinding = receivedByBinding;
    }

    public RichInputComboboxListOfValues getReceivedByBinding() {
        return receivedByBinding;
    }

    public void setAuthorizedByBinding(RichInputComboboxListOfValues authorizedByBinding) {
        this.authorizedByBinding = authorizedByBinding;
    }

    public RichInputComboboxListOfValues getAuthorizedByBinding() {
        return authorizedByBinding;
    }

    public void setCheckedDateBinding(RichInputDate checkedDateBinding) {
        this.checkedDateBinding = checkedDateBinding;
    }

    public RichInputDate getCheckedDateBinding() {
        return checkedDateBinding;
    }

    public void setReceivedDateBinding(RichInputDate receivedDateBinding) {
        this.receivedDateBinding = receivedDateBinding;
    }

    public RichInputDate getReceivedDateBinding() {
        return receivedDateBinding;
    }

    public void setAuthorisedDateBinding(RichInputDate authorisedDateBinding) {
        this.authorisedDateBinding = authorisedDateBinding;
    }

    public RichInputDate getAuthorisedDateBinding() {
        return authorisedDateBinding;
    }

    public void checkedByVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        OperationBinding ob = ADFUtils.findOperation("checkApprovalStatus");
        ob.getParamsMap().put("formNm", "FR_EN");
        ob.getParamsMap().put("authoLim", "CH");
        ob.getParamsMap().put("empcd", vce.getNewValue().toString());
        ob.execute();
        System.out.println(" EMP CD"+vce.getNewValue().toString());
        if((ob.getResult() !=null && ob.getResult().equals("Y")))
        {
           Row row=(Row)ADFUtils.evaluateEL("#{bindings.FreightEntryHeaderVO1Iterator.currentRow}");
           row.setAttribute("CheckedBy", null);
           ADFUtils.showMessage("You have no authority to check this Freight Entry.",0);
        }
    }

    public void receivedByVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        OperationBinding ob = ADFUtils.findOperation("checkApprovalStatus");
        ob.getParamsMap().put("formNm", "FR_EN");
        ob.getParamsMap().put("authoLim", "VR");
        ob.getParamsMap().put("empcd", vce.getNewValue().toString());
        ob.execute();
        System.out.println(" EMP CD"+vce.getNewValue().toString());
        if((ob.getResult() !=null && ob.getResult().equals("Y")))
        {
            Row row=(Row)ADFUtils.evaluateEL("#{bindings.FreightEntryHeaderVO1Iterator.currentRow}");
            row.setAttribute("ReceivedBy", null);
            ADFUtils.showMessage("You have no authority to receive this Freight Entry.",0);
        }
    }

    public void authorisedByVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        OperationBinding ob = ADFUtils.findOperation("checkApprovalFunctionStatus");
        ob.getParamsMap().put("formNm", "FR_EN");
        ob.getParamsMap().put("authoLim", "AP");
        ob.getParamsMap().put("UnitCd", unitCodeBinding.getValue());
        ob.getParamsMap().put("Amount", new BigDecimal(0));
        ob.getParamsMap().put("empcd", vce.getNewValue().toString());
        ob.execute();
        System.out.println(" EMP CD"+vce.getNewValue().toString());
        if((ob.getResult() !=null && !ob.getResult().equals("Y")))
        {
            Row row=(Row)ADFUtils.evaluateEL("#{bindings.FreightEntryHeaderVO1Iterator.currentRow}");
            row.setAttribute("AuthorizedBy", null);
            row.setAttribute("AuthorizedDate", null);
            ADFUtils.showMessage("You have no authority to authorise this Freight Entry.",0);
        }
    }
}
