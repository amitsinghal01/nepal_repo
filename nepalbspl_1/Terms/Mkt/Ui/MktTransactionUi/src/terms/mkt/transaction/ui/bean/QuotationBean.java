package terms.mkt.transaction.ui.bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import java.io.OutputStream;

import java.lang.reflect.Method;

import java.util.List;

import javax.el.ELContext;
import javax.el.ExpressionFactory;

import javax.el.ValueExpression;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputFile;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import org.apache.myfaces.trinidad.model.UploadedFile;

public class QuotationBean {
    private RichOutputText filenNmPath;
    private RichInputFile uploadFileBind;
    private RichInputText lovSwitchTransBinding;
    private RichInputText enquiryTransBinding;
    private RichTable dtlTableBind;
    private RichInputText quotationNoBinding;
    private RichInputDate quotationDateBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText customerNameBinding;
    private RichInputText plantBinding;
    private RichInputText gstPersentageBinding;
    private RichInputText gstDescriptionBinding;
    private RichInputText unitDescriptionBinding;
    private RichButton detailDeleteBinding;
    private RichButton detailCreateBinding;
    private RichButton headerEditBinding;
    private RichOutputText bindingOutputText;
    private RichPanelHeader getMyPageRootBinding;
    private RichTable followupTableBinding;
    private RichTable docAttachTableBinding;
    private RichInputComboboxListOfValues approvedByBinding;
    private RichButton attachmentButtonBinding;
    private RichButton followUpDetailBinding;
    private RichInputText prodDescriptionBinding;

    public QuotationBean() {
    }

//    public void docfileDialogListener(DialogEvent de) {
//        if (de.getOutcome() == DialogEvent.Outcome.ok) {
//
//           // docfilePopup.hide();
//        }
//    }
    
    public void setFilenNmPath(RichOutputText filenNmPath) {
        this.filenNmPath = filenNmPath;
    }

    public RichOutputText getFilenNmPath() {
        return filenNmPath;
    }

    public void setUploadFileBind(RichInputFile uploadFileBind) {
        this.uploadFileBind = uploadFileBind;
    }

    public RichInputFile getUploadFileBind() {
        return uploadFileBind;
    }
    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getImagePath() {
        return imagePath;
    }

    String Imagepath = "";

    InputStream inputStream = null;
    InputStream SaveInputStream = null;
    private UploadedFile PhotoFile = null;
    private String AbsoluteFilePath = "";
    String imagePath = "";
    Boolean check = false;
    
    public void setPhotoFile(UploadedFile PhotoFile) {
        this.PhotoFile = PhotoFile;
    }

    public UploadedFile getPhotoFile() {
        return PhotoFile;
    }


    public void uploadFileVCE(ValueChangeEvent vce) {
        if (vce.getNewValue() != null) {
            setPhotoFile((UploadedFile) vce.getNewValue());
            UploadedFile myfile = (UploadedFile) vce.getNewValue();
            String path = null;

            if (myfile == null) {

            } else {
                File dir = new File("/tmp/pht");
                File savedir = new File("/home/beta9/Pictures");
                if (!dir.exists()) {
                    try {
                        dir.mkdir();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (!savedir.exists()) {
                    try {
                        savedir.mkdir();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                // All uploaded files will be stored in below path
                path = "/home/beta9/Pictures" + myfile.getFilename();
                //                Imagepath=path;
                File f = new File(path);
                if (!f.exists()) {
                    try {
                        f.createNewFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                inputStream = null;
                try {

                    inputStream = myfile.getInputStream();
                    SaveInputStream = myfile.getInputStream();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                check = true;
                //                System.out.println("check in bean" + check);
                Imagepath = uploadFile(PhotoFile, "/tmp/pht/" + PhotoFile.getFilename());

            }
        }
    }
    private String SaveuploadFile(UploadedFile file, String path) {

        UploadedFile myfile = file;

        try {

            FileOutputStream out = new FileOutputStream(path);
            byte[] buffer = new byte[8192];
            int bytesRead = 0;
            while ((bytesRead = SaveInputStream.read(buffer, 0, 8192)) != -1) {
                out.write(buffer, 0, bytesRead);
            }
            out.flush();
            out.close();
        } catch (Exception ex) {
            // handle exception
            ex.printStackTrace();
        } finally {
            try {
                SaveInputStream.close();
            } catch (IOException e) {
            }
        }
        return path;
    }
    private String uploadFile(UploadedFile file, String path) {

        UploadedFile myfile = file;

        try {

            FileOutputStream out = new FileOutputStream(path);
            byte[] buffer = new byte[8192];
            int bytesRead = 0;
            while ((bytesRead = inputStream.read(buffer, 0, 8192)) != -1) {
                out.write(buffer, 0, bytesRead);
            }
            out.flush();
            out.close();
        } catch (Exception ex) {
            // handle exception
            ex.printStackTrace();
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
            }
        }

        //Returns the path where file is stored
        return path;
    }   
 
    public void downloadDocment(FacesContext facesContext, OutputStream outputStream) {
        //Read file from particular path, path bind is binding of table field that contains path
        File filed = new File(filenNmPath.getValue().toString());
        System.out.println("----------The filed name is ----------" + filed);
        FileInputStream fis;
        byte[] b;
        try {
            fis = new FileInputStream(filed);

            int n;
            while ((n = fis.available()) > 0) {
                b = new byte[n];
                int result = fis.read(b);
                outputStream.write(b, 0, b.length);
                if (result == -1)
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public void createDocFileAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert2").execute();
                
        ADFUtils.findOperation("getSequenceDocNo").execute();
        
    }
    public void saveQuotationNoAL(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedBy("QuotationHeaderVO1Iterator","LastUpdatedBy");        
        check = false;
        String path;
        if (getPhotoFile() != null) {
            path = "/home/beta9/Pictures" + PhotoFile.getFilename();
            Imagepath = SaveuploadFile(PhotoFile, path);
            //        System.out.println("path " + Imagepath);
            OperationBinding ob = ADFUtils.findOperation("AddImagePathQuotation");
            ob.getParamsMap().put("ImagePath", Imagepath);
            ob.execute();
        }

        File directory = new File("/tmp/pht");
        //get all the files from a directory
        File[] fList = directory.listFiles();
        if (fList != null) {
            for (File file : fList) {
                //Delete all previously uploaded files
                // if (!"NoImage.png".equalsIgnoreCase(file.getName())) {
                file.delete();
                //}
            }
        }

        OperationBinding op = ADFUtils.findOperation("generateQuotationNo");
        op.execute();
        ADFUtils.findOperation("setQuotationNumber").execute();

        System.out.println("--------Value Aftr getting result--- " + op.getResult());
        if (op.getResult() != null && op.getResult().toString().equalsIgnoreCase("N")) {
            if (op.getErrors().isEmpty()) {
                ADFUtils.findOperation("Commit").execute();
                FacesMessage Message = new FacesMessage("Record Update Successfully.");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);

            }

        } else if (op.getResult() != null && op.getResult().toString() != "") {
            if (op.getErrors().isEmpty()) {
                System.out.println("--------in result set-------");

                ADFUtils.findOperation("Commit").execute();
                FacesMessage Message =
                    new FacesMessage("Record Saved Successfully.Quotation Number is " + op.getResult() + ".");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            }
        }
    }

    public String saveandCloseAL() {
        check = false;
        String path;
        if (getPhotoFile() != null) {
            path = "/home/beta9/Pictures" + PhotoFile.getFilename();
            Imagepath = SaveuploadFile(PhotoFile, path);
            //        System.out.println("path " + Imagepath);
            OperationBinding ob = ADFUtils.findOperation("AddImagePathQuotation");
            ob.getParamsMap().put("ImagePath", Imagepath);
            ob.execute();
        }

        File directory = new File("/tmp/pht");
        //get all the files from a directory
        File[] fList = directory.listFiles();
        if (fList != null) {
            for (File file : fList) {
                //Delete all previously uploaded files
                // if (!"NoImage.png".equalsIgnoreCase(file.getName())) {
                file.delete();
                //}
            }
        }

        OperationBinding op = ADFUtils.findOperation("generateQuotationNo");
        op.execute();
        ADFUtils.findOperation("setQuotationNumber").execute();

        System.out.println("--------Value Aftr getting result--- " + op.getResult());
        if (op.getResult() != null && op.getResult().toString().equalsIgnoreCase("N")) {
            if (op.getErrors().isEmpty()) {
                ADFUtils.findOperation("Commit").execute();
                FacesMessage Message = new FacesMessage("Record Update Successfully.");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
                return "save and cancel";

            }


        } else if (op.getResult() != null && op.getResult().toString() != "") {
            if (op.getErrors().isEmpty()) {
                System.out.println("--------in result set-------");

                ADFUtils.findOperation("Commit").execute();
                FacesMessage Message =
                    new FacesMessage("Record Saved Successfully.Quotation Number is " + op.getResult() + ".");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
                return "save and cancel";
            }
        }
        return null;
    }

    public void attachemtAL(ActionEvent actionEvent) {
       
        ADFUtils.findOperation("refreshPage").execute();
    }

    public void setLovSwitchTransBinding(RichInputText lovSwitchTransBinding) {
        this.lovSwitchTransBinding = lovSwitchTransBinding;
    }

    public RichInputText getLovSwitchTransBinding() {
        return lovSwitchTransBinding;
    }

    public void setEnquiryTransBinding(RichInputText enquiryTransBinding) {
        this.enquiryTransBinding = enquiryTransBinding;
    }

    public RichInputText getEnquiryTransBinding() {
        return enquiryTransBinding;
    }

    public void createDetailAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert").execute();
        ADFUtils.findOperation("setQuotationNumber").execute();
        setLovSwitchTransBinding(enquiryTransBinding);
        AdfFacesContext.getCurrentInstance().addPartialTarget(dtlTableBind);
    }

    public void editDialogListener(DialogEvent dialogEvent) {
       
                if(dialogEvent.getOutcome().name().equals("ok"))
                {
                ADFUtils.findOperation("Delete").execute();
                System.out.println("Record Delete Successfully");
                
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                FacesContext fc = FacesContext.getCurrentInstance();  
                fc.addMessage(null, Message);  
                
                }
                AdfFacesContext.getCurrentInstance().addPartialTarget(dtlTableBind);
            } 

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
            try {
                Method method1 =
                    component.getClass().getMethod("setDisabled", boolean.class);
                if (method1 != null) {
                    method1.invoke(component, valueComponent);
                }
            } catch (NoSuchMethodException e) {
                try {
                    Method method =
                        component.getClass().getMethod("setReadOnly", boolean.class);
                    if (method != null) {
                        method.invoke(component, valueComponent);
                    }
                } catch (Exception e1) {
                    // e.printStackTrace();//silently eat this exception.
                }


            } catch (Exception e) {
                // e.printStackTrace();//silently eat this exception.
            }
            List<UIComponent> childComponents = component.getChildren();
            for (UIComponent comp : childComponents) {
                makeComponentHierarchyReadOnly(comp, valueComponent);
            }
        }
        
        
                        private void cevmodecheck(){
                            if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
                                makeComponentHierarchyReadOnly(this.getGetMyPageRootBinding(), true);
                                cevModeDisableComponent("V");
                            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
                                makeComponentHierarchyReadOnly(this.getGetMyPageRootBinding(), false);
                                cevModeDisableComponent("C");
                            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
                                makeComponentHierarchyReadOnly(this.getGetMyPageRootBinding(), false);
                                cevModeDisableComponent("E");
                            }
                        }
        
    public void cevModeDisableComponent(String mode) {
        if (mode.equals("E")) {
                getQuotationNoBinding().setDisabled(true);
                getQuotationDateBinding().setDisabled(true);
                getUnitCodeBinding().setDisabled(true);
            getCustomerNameBinding().setDisabled(true);
            getPlantBinding().setDisabled(true);
            getGstPersentageBinding().setDisabled(true);
            getGstDescriptionBinding().setDisabled(true);
                getDetailCreateBinding().setDisabled(false);
                getDetailDeleteBinding().setDisabled(false);
                getHeaderEditBinding().setDisabled(true);
                getProdDescriptionBinding().setDisabled(true);
        } else if (mode.equals("C")) {
            getQuotationNoBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
            getPlantBinding().setDisabled(true);
       //   getVendorcodeBinding().setDisabled(true);
            getDetailCreateBinding().setDisabled(false);
            getApprovedByBinding().setDisabled(true);
            getProdDescriptionBinding().setDisabled(true);
        } else if (mode.equals("V")) {
            getDetailCreateBinding().setDisabled(true);
            getDetailDeleteBinding().setDisabled(true);
            getAttachmentButtonBinding().setDisabled(false);
            getFollowUpDetailBinding().setDisabled(false);
        }
    }
 

    public void setDtlTableBind(RichTable dtlTableBind) {
        this.dtlTableBind = dtlTableBind;
    }

    public RichTable getDtlTableBind() {
        return dtlTableBind;
    }

    public void setQuotationNoBinding(RichInputText quotationNoBinding) {
        this.quotationNoBinding = quotationNoBinding;
    }

    public RichInputText getQuotationNoBinding() {
        return quotationNoBinding;
    }

    public void setQuotationDateBinding(RichInputDate quotationDateBinding) {
        this.quotationDateBinding = quotationDateBinding;
    }

    public RichInputDate getQuotationDateBinding() {
        return quotationDateBinding;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setCustomerNameBinding(RichInputText customerNameBinding) {
        this.customerNameBinding = customerNameBinding;
    }

    public RichInputText getCustomerNameBinding() {
        return customerNameBinding;
    }

    public void setPlantBinding(RichInputText plantBinding) {
        this.plantBinding = plantBinding;
    }

    public RichInputText getPlantBinding() {
        return plantBinding;
    }

    public void setGstPersentageBinding(RichInputText gstPersentageBinding) {
        this.gstPersentageBinding = gstPersentageBinding;
    }

    public RichInputText getGstPersentageBinding() {
        return gstPersentageBinding;
    }

    public void setGstDescriptionBinding(RichInputText gstDescriptionBinding) {
        this.gstDescriptionBinding = gstDescriptionBinding;
    }

    public RichInputText getGstDescriptionBinding() {
        return gstDescriptionBinding;
    }

    public void setUnitDescriptionBinding(RichInputText unitDescriptionBinding) {
        this.unitDescriptionBinding = unitDescriptionBinding;
    }

    public RichInputText getUnitDescriptionBinding() {
        return unitDescriptionBinding;
    }

    public void setDetailDeleteBinding(RichButton detailDeleteBinding) {
        this.detailDeleteBinding = detailDeleteBinding;
    }

    public RichButton getDetailDeleteBinding() {
        return detailDeleteBinding;
    }

    public void setDetailCreateBinding(RichButton detailCreateBinding) {
        this.detailCreateBinding = detailCreateBinding;
    }

    public RichButton getDetailCreateBinding() {
        return detailCreateBinding;
    }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        cevmodecheck();
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        return bindingOutputText;
    }

    public void setGetMyPageRootBinding(RichPanelHeader getMyPageRootBinding) {
        this.getMyPageRootBinding = getMyPageRootBinding;
    }

    public RichPanelHeader getGetMyPageRootBinding() {
        return getMyPageRootBinding;
    }

    public void deleteFollowUpDialogListener(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                    {
                    ADFUtils.findOperation("Delete2").execute();
                    System.out.println("Record Delete Successfully");
                   
                        FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                        FacesContext fc = FacesContext.getCurrentInstance();  
                        fc.addMessage(null, Message);  
                   
                    }

                AdfFacesContext.getCurrentInstance().addPartialTarget(followupTableBinding);
    }

    public void setFollowupTableBinding(RichTable followupTableBinding) {
        this.followupTableBinding = followupTableBinding;
    }

    public RichTable getFollowupTableBinding() {
        return followupTableBinding;
    }

    public void setDocAttachTableBinding(RichTable docAttachTableBinding) {
        this.docAttachTableBinding = docAttachTableBinding;
    }

    public RichTable getDocAttachTableBinding() {
        return docAttachTableBinding;
    }

    public void fileDocumentDeleteDialogListener(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                    {
                    ADFUtils.findOperation("Delete1").execute();
                    System.out.println("Record Delete Successfully");
                   
                        FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                        FacesContext fc = FacesContext.getCurrentInstance();  
                        fc.addMessage(null, Message);  
                   
                    }
                AdfFacesContext.getCurrentInstance().addPartialTarget(docAttachTableBinding);
    }

    public void setApprovedByBinding(RichInputComboboxListOfValues approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputComboboxListOfValues getApprovedByBinding() {
        return approvedByBinding;
    }

    public void setAttachmentButtonBinding(RichButton attachmentButtonBinding) {
        this.attachmentButtonBinding = attachmentButtonBinding;
    }

    public RichButton getAttachmentButtonBinding() {
        return attachmentButtonBinding;
    }

    public void setFollowUpDetailBinding(RichButton followUpDetailBinding) {
        this.followUpDetailBinding = followUpDetailBinding;
    }

    public RichButton getFollowUpDetailBinding() {
        return followUpDetailBinding;
    }

    public void setProdDescriptionBinding(RichInputText prodDescriptionBinding) {
        this.prodDescriptionBinding = prodDescriptionBinding;
    }

    public RichInputText getProdDescriptionBinding() {
        return prodDescriptionBinding;
    }
}
