package terms.mkt.transaction.ui.bean;

import javax.faces.event.ActionEvent;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichPopup;

import oracle.jbo.ViewObject;

import terms.mkt.transaction.model.view.SaleChangeVORowImpl;
import terms.mkt.transaction.model.view.SaleOrderHeaderVORowImpl;

public class SaleChangeBean {
    private String editAction="V";
    private RichPopup invoicePopupBinding;

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }
    public SaleChangeBean() {
    }

    public void SaveAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("UpdateSaleInfo").execute();
        ADFUtils.findOperation("Commit").execute();
    }

    public void showpopupAL(ActionEvent actionEvent) {
    DCIteratorBinding binding = ADFUtils.findIterator("SaleChangeVO1Iterator");
           if (binding.getEstimatedRowCount() > 0 && binding.getCurrentRow() != null) {
               
              SaleChangeVORowImpl row = (SaleChangeVORowImpl) binding.getCurrentRow();
    System.out.println("Appr by"+row.getAckNumber()+"SaleNote"+row.getDaNo());
        DCIteratorBinding binding1 = ADFUtils.findIterator("InvoiceDetailToSaleNoteVVO1Iterator");
               if (binding.getEstimatedRowCount() > 0 && binding.getCurrentRow() != null) {
                     ViewObject voh = binding1.getViewObject();
                     voh.setNamedWhereClauseParam("Salenote", row.getDaNo());
        voh.setNamedWhereClauseParam("saleorder", row.getAckNumber());
        voh.executeQuery();
        System.out.println("rowsin popup=="+voh.getEstimatedRowCount());
        RichPopup.PopupHints pop=new RichPopup.PopupHints();
        invoicePopupBinding.show(pop);
    }
}
    }

    public void setInvoicePopupBinding(RichPopup invoicePopupBinding) {
        this.invoicePopupBinding = invoicePopupBinding;
    }

    public RichPopup getInvoicePopupBinding() {
        return invoicePopupBinding;
    }
}
