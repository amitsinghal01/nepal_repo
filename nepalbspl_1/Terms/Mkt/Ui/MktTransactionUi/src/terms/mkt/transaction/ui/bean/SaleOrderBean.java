package terms.mkt.transaction.ui.bean;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;

import java.sql.Connection;

import java.sql.SQLException;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.faces.application.FacesMessage;

import javax.faces.context.FacesContext;

import javax.naming.Context;
import javax.naming.InitialContext;

import javax.servlet.http.HttpServletResponse;

import javax.sql.DataSource;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

public class SaleOrderBean {
    private RichTable saleOrderTableBinding;
    private RichOutputText ackNumberBinding;

    private RichInputText approvedByBind;
    private String saleMode ="";



    public SaleOrderBean() {
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                   {
                   ADFUtils.findOperation("Delete").execute();
                   ADFUtils.findOperation("Commit").execute();    //Unable on Search page.
                   System.out.println("Record Delete Successfully");
                  
                       FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                       Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                       FacesContext fc = FacesContext.getCurrentInstance();  
                       fc.addMessage(null, Message);  
                  
                   }

               AdfFacesContext.getCurrentInstance().addPartialTarget(saleOrderTableBinding);
    

    }

    public void setSaleOrderTableBinding(RichTable saleOrderTableBinding) {
        this.saleOrderTableBinding = saleOrderTableBinding;
    }

    public RichTable getSaleOrderTableBinding() {
        return saleOrderTableBinding;
    }

    public void setAckNumberBinding(RichOutputText ackNumberBinding) {
        this.ackNumberBinding = ackNumberBinding;
    }

    public RichOutputText getAckNumberBinding() {
        return ackNumberBinding;
    }
    public String checkApprovedByAC() {
        
        OperationBinding binding = ADFUtils.findOperation("amdApprovedNumber");
        binding.getParamsMap().put("ApprovedBy", approvedByBind.getValue());
        binding.getParamsMap().put("AckNumber", ackNumberBinding.getValue());     
        
        binding.execute();
    
        if(binding.getResult()!=null && binding.getResult().toString().equalsIgnoreCase("0"))

        {
            
            return "goToNext";
            
        }else{
                FacesMessage Message =
                    new FacesMessage("Yor are unable to generate new document because "+ackNumberBinding.getValue()+ " Amendment Number "+binding.getResult()+" is not Approved.");
                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
        
            }
        return null;
    }

  
    public void setApprovedByBind(RichInputText approvedByBind) {
        this.approvedByBind = approvedByBind;
    }

    public RichInputText getApprovedByBind() {
        return approvedByBind;
    }
    
    
    public void downloadJrxml(FacesContext facesContext, OutputStream outputStream) {
        Connection conn=null;
            try {

                String file_name ="";
                System.out.println("saleMode===================>"+saleMode);
                
                if(saleMode.equals("PO"))
                {
                file_name="r_po_so.jasper";
                }
                else if(saleMode.equals("CK"))
                {
                    file_name="r_ck_so.jasper";
                }
                
                System.out.println("After Set Value File Name is===>"+file_name);
                

                OperationBinding binding = ADFUtils.findOperation("fileNameForPrint");
                binding.getParamsMap().put("fileId","ERP0000000548");
                binding.execute();
                //*************End Find File name***********//
                System.out.println("Binding Result :"+binding.getResult());
                if(binding.getResult() != null){
                    String result = binding.getResult().toString();
                    int last_index = result.lastIndexOf("/");
                    String path = result.substring(0,last_index+1)+file_name;
                    System.out.println("FILE PATH IS===>"+path);
                    InputStream input = new FileInputStream(path); 
                    DCIteratorBinding soIter = (DCIteratorBinding) getBindings().get("SearchSaleOrderVO1Iterator");
                    String so_no = soIter.getCurrentRow().getAttribute("AckNumber").toString();
                    String unitCode = soIter.getCurrentRow().getAttribute("UnitCode").toString();
                    System.out.println("SO Number :- "+so_no+" "+unitCode);
                    Map n = new HashMap();
                    n.put("p_no", so_no);
                    n.put("p_unit", unitCode);
                    conn = getConnection( );
                    
                    JasperReport design = (JasperReport) JRLoader.loadObject(input);
                    System.out.println("Path : " + input + " -------" + design+" param:"+n);
                    
                    @SuppressWarnings("unchecked")
                    net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                    byte pdf[] = JasperExportManager.exportReportToPdf(print);
                    HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                    response.getOutputStream().write(pdf);
                    response.getOutputStream().flush();
                    response.getOutputStream().close();
                    facesContext.responseComplete();
                    }else
                    System.out.println("File Name/File Path not found.");
                    } catch (FileNotFoundException fnfe) {
                    // TODO: Add catch code
                    fnfe.printStackTrace();
                    try {
                        System.out.println("in finally connection closed");
                                conn.close( );
                                conn = null;
                        
                    } catch( SQLException e ) {
                        e.printStackTrace( );
                    }
                    } catch (Exception e) {
                    // TODO: Add catch code
                    e.printStackTrace();
                    try {
                        System.out.println("in finally connection closed");
                                conn.close( );
                                conn = null;
                        
                    } catch( SQLException e1 ) {
                        e1.printStackTrace( );
                    }
                    }finally {
                            try {
                                    System.out.println("in finally connection closed");
                                            conn.close( );
                                            conn = null;
                                    
                            } catch( SQLException e ) {
                                    e.printStackTrace( );
                            }
                    }
        }

    public Connection getConnection() throws Exception{
            Context ctx = null;
            Connection conn = null;
            ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/ApplicationDBDS");
            conn = ds.getConnection();
            return conn;
        }

    public BindingContainer getBindings()
          {
            return BindingContext.getCurrent().getCurrentBindingsEntry();
          }

    public void setSaleMode(String saleMode) {
        this.saleMode = saleMode;
    }

    public String getSaleMode() {
        return saleMode;
    }
}
