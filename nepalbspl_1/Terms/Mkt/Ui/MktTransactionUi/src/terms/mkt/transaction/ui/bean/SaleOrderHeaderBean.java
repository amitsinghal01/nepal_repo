package terms.mkt.transaction.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.text.ParseException;

import java.util.Calendar;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;
import javax.faces.validator.ValidatorException;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;

import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;

import oracle.adf.view.rich.component.rich.input.RichSelectBooleanCheckbox;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;

import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichPanelLabelAndMessage;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.JboWarning;
import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;
import oracle.jbo.domain.Date;
import oracle.jbo.domain.Timestamp;
import oracle.jbo.server.DBTransaction;

import terms.mkt.transaction.model.view.DespatchAdviceDetailVORowImpl;
import terms.mkt.transaction.model.view.SaleOrderDetailVORowImpl;
import terms.mkt.transaction.model.view.SaleOrderHeaderVORowImpl;


public class SaleOrderHeaderBean {
    private RichInputComboboxListOfValues copypoBinding;
    private RichInputText ackIdBinding;
    private RichInputText poNumberBinding;
    private RichInputText ackNumberBinding;
    private RichInputComboboxListOfValues approvedByBinding;
    private RichInputText hsnNoBinding;
    private RichInputComboboxListOfValues bindProdCode;
    private RichInputText bindQuantity;
    private RichInputDate bindDeliveryDate;
    private RichInputComboboxListOfValues bindUnitCd;
    private RichInputComboboxListOfValues bindCustCode;
    private RichColumn bindAckIdDtl;
    private RichSelectOneChoice bindCurrency;
    private RichInputText bindCurrencyRate;
    private RichInputText entryDateBinding;
    private RichInputText plantBinding;
    private RichInputComboboxListOfValues cityBinding;

    private RichOutputText outputtextBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichButton detailcreateBinding;
    private RichButton detaildeleteBinding;
    private RichInputText processSeqBinding;
    private RichInputText gstDescBinding;
    private RichInputText cgstBinding;
    private RichInputText cgstRateBinding;
    private RichInputText igstBinding;
    private RichInputText igstRateBinding;
    private RichInputText sgstBinding;
    private RichInputText sgstRateBinding;
    private RichPopup editPopupBinding;
    private RichInputText amendAm;
    private RichInputText custAmendNoBinding;
    private RichInputDate poDateBinding;
    private RichInputComboboxListOfValues quotationBinding;
    private RichPopup termsnConditionBinding;
    private RichInputText creditperiodBinding;
    private RichInputText destinationBinding;
    private RichSelectOneChoice shipModeBinding;
    private RichTable detailSaleOrderTableBinding;
    private RichButton termsButtonBinding;
    private RichPopup pendPopupBindingp7;
    private RichInputComboboxListOfValues consigneeCd;
    private RichSelectOneChoice poStatusBinding;
    private RichSelectBooleanCheckbox poCanBinding;
    private RichInputComboboxListOfValues transporterBinding;
    private RichSelectOneChoice openFixBinding;

    private RichInputText insurComp;
    private RichInputDate bindingValF;
    String Flag = "C";
    private RichInputDate bindingValidUpto;
    private RichSelectOneChoice cpclbinding;
    private RichSelectOneChoice soTypeBinding;
    private RichInputText amtBinding;
    private RichSelectOneChoice addTypeBinding;
    private RichInputText addDiscBinding;
    private RichSelectOneChoice tradeTypeVCE;
    private RichSelectOneChoice serType;
    private RichInputText tradeDisc;
    private RichInputText serVDisc;
    private RichInputText plantHoBinding;
    private RichInputDate amdDateBinding;
    private RichInputText netAmtBinding;
    private RichInputText totalamtbind;
    private RichInputComboboxListOfValues destinationBinding1;
    private RichSelectOneChoice marketType;
    private RichColumn quantityColumnBinding;
    private RichPanelLabelAndMessage destinationCreateBinding;
    private RichInputText destinationFieldBind;
    private RichInputText discountPriceBinding;
    private RichInputText employeeNamebinding;
    private RichInputComboboxListOfValues uomBinding;
    private RichInputText ackdatenepalinding;
    private RichInputText custnepaldateinding;
    private RichInputText validdatenepalinding;

    public SaleOrderHeaderBean() {
    }


    public void saveAckNoAL(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedBy("SaleOrderHeaderVO1Iterator","LastUpdatedBy");
        DCIteratorBinding Dcite = ADFUtils.findIterator("SaleOrderDetailVO1Iterator");
        SaleOrderDetailVORowImpl row = (SaleOrderDetailVORowImpl) Dcite.getCurrentRow();
        System.out.println("openFix=====" + row.getOpenFix());
        if ((row.getOpenFix().toString().equalsIgnoreCase("F") && row.getQuantity() != null) ||
            (row.getOpenFix().toString().equalsIgnoreCase("O") && row.getQuantity() == null)) {
            OperationBinding op = ADFUtils.findOperation("getAckNumber");
            Object rst = op.execute();
            System.out.println("--------Commit-------");
            System.out.println("value aftr getting result--->?" + rst);
            System.out.println("Ackkk" + ackNumberBinding.getValue());
            System.out.println("AmdNoooo" + amendAm.getValue());
            
//            OperationBinding op1=ADFUtils.findOperation("generateRateForSaleOrdre");            
//            op1.getParamsMap().put("AckDate",ackNumberBinding.getValue());
//            op1.execute();

            if (rst.toString() != null && rst.toString() != "") {
                if (op.getErrors().isEmpty()) {
                    ADFUtils.findOperation("apprvdvalue").execute();
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message =
                        new FacesMessage("Record Saved Successfully.New Sale Order No is " + rst + ".");
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);
                    ADFUtils.setEL("#{pageFlowScope.mode}", "V");
                    cevmodecheck();
                    AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);

                }
            }

            if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ") {
                if (op.getErrors().isEmpty()) {
                    ADFUtils.findOperation("apprvdvalue").execute();
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message = new FacesMessage("Record Update Successfully.");
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);
                    if (Flag.equalsIgnoreCase("V")) {
                        System.out.println("Flag==" + Flag);
                        try {
                            OperationBinding opp = (OperationBinding) ADFUtils.findOperation("validUptoSet");
                            opp.getParamsMap().put("AmendAckNumber",
                                                   ADFUtils.resolveExpression("#{pageFlowScope.AmendAckNumber}"));
                            opp.getParamsMap().put("AckId", ADFUtils.resolveExpression("#{pageFlowScope.AckId}"));
                            opp.getParamsMap().put("AckNumber",
                                                   ADFUtils.resolveExpression("#{pageFlowScope.AckNumber}"));
                            opp.execute();
                        } catch (Exception e) {
                            ADFUtils.showMessage("Quantity is required in case of fixed Sale Order.", 0);

                        }
                    }

                    ADFUtils.findOperation("Commit").execute();
                }
                ADFUtils.setEL("#{pageFlowScope.mode}", "V");
                cevmodecheck();
                AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);

            }
        } else {
            ADFUtils.showMessage("Quantity is required in case of fixed Sale Order", 0);

        }
    }

    public void CopyPOAL(ActionEvent actionEvent) {
        if (copypoBinding.getValue() != null) {
            Long reqnobinding = (Long) copypoBinding.getValue();


            OperationBinding op1 = ADFUtils.findOperation("populateCopyPOHeader");
            op1.getParamsMap().put("acknumber", reqnobinding);
            op1.execute();
            DCIteratorBinding Dcite = ADFUtils.findIterator("SaleOrderHeaderVO1Iterator");
            if (Dcite.getEstimatedRowCount() > 0 && Dcite.getCurrentRow() != null) {
                SaleOrderHeaderVORowImpl row = (SaleOrderHeaderVORowImpl) Dcite.getCurrentRow();
//
//                if (row.getPaymentTerm() != null) {
//                    OperationBinding op = ADFUtils.findOperation("populateCopyPODetail");
//
//                    System.out.println("--------Sale Order in Bean------" + reqnobinding);
//                    op.getParamsMap().put("acknumber", reqnobinding);
//                    op.execute();
//                    getCpclbinding().setDisabled(true);
//                    getSoTypeBinding().setDisabled(true);
//                    getPoNumberBinding().setDisabled(true);
//                    getBindingValidUpto().setDisabled(true);
//                    getCustAmendNoBinding().setDisabled(true);
//                    getPoDateBinding().setDisabled(true);
//                    getQuotationBinding().setDisabled(true);
//                    getBindCustCode().setDisabled(true);
//                    AdfFacesContext.getCurrentInstance().addPartialTarget(detailSaleOrderTableBinding);
//                } else {
//
//                    //                           ADFUtils.showMessage("Destination is required in Terms and Condition ", 0);
//                    //                        ADFUtils.showMessage("Transporter Code is required in Terms and Condition ", 0);
//                    ADFUtils.showMessage("Payment Term is required in Terms and Condition ", 0);
//
//                }
            }
        }
    }

    public void setCopypoBinding(RichInputComboboxListOfValues copypoBinding) {
        this.copypoBinding = copypoBinding;
    }

    public RichInputComboboxListOfValues getCopypoBinding() {
        return copypoBinding;
    }

    public void setAckIdBinding(RichInputText ackIdBinding) {
        this.ackIdBinding = ackIdBinding;
    }

    public RichInputText getAckIdBinding() {
        return ackIdBinding;
    }


    //    public void setVubinding(RichInputText vubinding) {
    //        this.vubinding = vubinding;
    //    }
    //
    //    public RichInputText getVubinding() {
    //        return vubinding;
    //    }
    public void setPoNumberBinding(RichInputText poNumberBinding) {
        this.poNumberBinding = poNumberBinding;
    }

    public RichInputText getPoNumberBinding() {
        return poNumberBinding;
    }

    public void setAckNumberBinding(RichInputText ackNumberBinding) {
        this.ackNumberBinding = ackNumberBinding;
    }

    public RichInputText getAckNumberBinding() {
        return ackNumberBinding;
    }

    public void altkeyValidation(FacesContext facesContext, UIComponent uIComponent, Object object) {

        if (object != null) {
            String AckNumber = getAckNumberBinding().toString();
            System.out.println("bean ack" + AckNumber);
            String CustPoNumber = getCopypoBinding().toString();
            System.out.println("bean po" + CustPoNumber);
            OperationBinding ob = ADFUtils.findOperation("PoNumberValidator");
            ob.getParamsMap().put("ponum", CustPoNumber);
            ob.getParamsMap().put("acknum", AckNumber);
            ob.execute();

            if (ob.getResult() != null) {
                String flag = ob.getResult().toString();

                if ("Y".equalsIgnoreCase(flag)) {
                    throw new ValidatorException(new FacesMessage("Duplicate Record",
                                                                  "This PO Number exists in Database"), null);
                }
            }
        }
    }

    public void setApprovedByBinding(RichInputComboboxListOfValues approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputComboboxListOfValues getApprovedByBinding() {
        return approvedByBinding;
    }

    public void productCodeValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        System.out.println("#################################################################################################### in product validator");
        if (object != null) {

            if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C") &&
                openFixBinding.getValue().toString().equalsIgnoreCase("O")) {
                String prod = (String) bindProdCode.getValue();
                System.out.println("bean ack" + prod);

                OperationBinding ob = ADFUtils.findOperation("productCodeCheckValueMtlPara");
                ob.getParamsMap().put("prod", prod);

                ob.execute();

                if (ob.getResult() != null) {
                    String flag = ob.getResult().toString();

                    if ("A".equalsIgnoreCase(flag)) {
                        System.out.println("@@@@@@");
                        throw new ValidatorException(new FacesMessage("Single Valid PO Can Exist For A Product For This Customer"),
                                                     null);
                    }

                }
            }
        }
    }


    public void setHsnNoBinding(RichInputText hsnNoBinding) {
        this.hsnNoBinding = hsnNoBinding;
    }

    public RichInputText getHsnNoBinding() {
        return hsnNoBinding;
    }

    public void GstFromProdCodeVCL(ValueChangeEvent valueChangeEvent) {  
            valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
            //    AdfFacesContext.getCurrentInstance().addPartialTarget(detailSaleOrderTableBinding);
            if (valueChangeEvent.getNewValue() != null) {
                    if (bindProdCode.getValue() != null) {
                                   String ProdCode = valueChangeEvent.getNewValue().toString();
                                   OperationBinding opp = ADFUtils.findOperation("gethsnno");
                                   opp.getParamsMap().put("ProdCode", ProdCode); 
                                   opp.execute();
                                   OperationBinding op1=ADFUtils.findOperation("generateRateForSaleOrdre");
                                   op1.getParamsMap().put("AckDate",entryDateBinding.getValue());
                                   op1.getParamsMap().put("ProdCode", bindProdCode.getValue());   
                                   op1.execute();
                                    
                                   
                               }

            if (hsnNoBinding.getValue() != null) {
                String HsnNo = (String) hsnNoBinding.getValue();
                OperationBinding op = ADFUtils.findOperation("GstCodefromProdcode");

                System.out.println("--------Hsnno------" + HsnNo);
                op.getParamsMap().put("HsnNo", HsnNo);
                op.execute();
            }


            //            OperationBinding ob1 = ADFUtils.findOperation("MinMaxMasterMsg");
            //            ob1.execute();

            //        if(bindProdCode.getValue()!=null)
            //        {
            String Unit = (String) bindUnitCd.getValue();

            System.out.println("sdsfsggsghrhrjjtjklthlelfgkelfrwretrY^*^&%TREREY^%YGRGFS**********************************************" +
                               ackIdBinding.getValue());
            Long ackid = (Long) ackIdBinding.getValue();
            System.out.println("bean ack" + Unit);
            System.out.println("******************************************************before popup");
            RichPopup.PopupHints hints = new RichPopup.PopupHints();
            pendPopupBindingp7.show(hints);
            System.out.println("()()()()()()()()()()()()()()()()()()()()()()()()()()after popup");


            /*  OperationBinding ob1 = ADFUtils.findOperation("Lpr_Stock_status");
     ob1.getParamsMap().put("Unit",Unit );
     ob1.getParamsMap().put("ProdCode",ProdCode );
     ob1.getParamsMap().put("ackid",ackid );
     ob1.execute();  */
            
        }

        String Unit = bindUnitCd.getValue().toString();
        System.out.println("bean Unit" + Unit);
        String vencd = bindCustCode.getValue().toString();
        System.out.println("bean Vencd" + vencd);
        String Market = (String) (marketType.getValue() != null ? marketType.getValue() : "R");
        if (!Market.equalsIgnoreCase("E")) {
            OperationBinding ob = ADFUtils.findOperation("RefreshAmount");
            ob.getParamsMap().put("Unit", Unit);
            ob.getParamsMap().put("vencd", vencd);
            ob.execute();
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(detailSaleOrderTableBinding);

    }


    public void setBindProdCode(RichInputComboboxListOfValues bindProdCode) {
        this.bindProdCode = bindProdCode;
    }

    public RichInputComboboxListOfValues getBindProdCode() {
        return bindProdCode;
    }


    public void setBindQuantity(RichInputText bindQuantity) {
        this.bindQuantity = bindQuantity;
    }

    public RichInputText getBindQuantity() {
        return bindQuantity;
    }

    public void QuantityOpenFixVCL(ValueChangeEvent valueChangeEvent) {
        if (valueChangeEvent != null) {

            //        if(bindQuantity.getValue()!=null)
            //        { BigDecimal t= new BigDecimal(0);
            //          String F="F";
            //          String O="O";
            //            BigDecimal Quan=(BigDecimal)bindQuantity.getValue();
            //            System.out.println("QUANTTTT"+Quan);
            //            if(Quan.compareTo(t) == 1)
            //            {
            //               bindOpenFix.setValue("F");
            //
            //
            //                }
            //            else
            //            {
            //               bindOpenFix.setValue("O");
            //                }
            //    }
            valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
            String Unit = bindUnitCd.getValue().toString();
            System.out.println("bean Unit" + Unit);
            String vencd = bindCustCode.getValue().toString();
            System.out.println("bean Vencd" + vencd);
            String Market = (String) (marketType.getValue() != null ? marketType.getValue() : "R");
            if (!Market.equalsIgnoreCase("E")) {
                OperationBinding ob = ADFUtils.findOperation("RefreshAmount");
                ob.getParamsMap().put("Unit", Unit);
                ob.getParamsMap().put("vencd", vencd);
                ob.execute();
            }
        }
    }

    public void setBindDeliveryDate(RichInputDate bindDeliveryDate) {
        this.bindDeliveryDate = bindDeliveryDate;
    }

    public RichInputDate getBindDeliveryDate() {
        return bindDeliveryDate;
    }

    public void setBindUnitCd(RichInputComboboxListOfValues bindUnitCd) {
        this.bindUnitCd = bindUnitCd;
    }

    public RichInputComboboxListOfValues getBindUnitCd() {
        return bindUnitCd;
    }

    public void setBindCustCode(RichInputComboboxListOfValues bindCustCode) {
        this.bindCustCode = bindCustCode;
    }

    public RichInputComboboxListOfValues getBindCustCode() {
        return bindCustCode;
    }

    public void RefreshAmtVCL(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        String Unit = bindUnitCd.getValue().toString();
        System.out.println("bean Unit" + Unit);
        String vencd = bindCustCode.getValue().toString();
        System.out.println("bean Vencd" + vencd);
        String Market = (String) (marketType.getValue() != null ? marketType.getValue() : "R");
        if (!Market.equalsIgnoreCase("E")) {
            OperationBinding ob = ADFUtils.findOperation("RefreshAmount");
            ob.getParamsMap().put("Unit", Unit);
            ob.getParamsMap().put("vencd", vencd);
            ob.execute();
        }
    }

    public void ValidateQuantityfromof(FacesContext facesContext, UIComponent uIComponent, Object object) {
        System.out.println("openFixBind.getValue()" + openFixBinding.getValue());
        String n = null;
        if (object != null) {
            BigDecimal t = new BigDecimal(0);
            String F = "F";
            String O = "O";
            BigDecimal Quan = (BigDecimal) object;
            System.out.println("QUANTTTT" + Quan);
            String of = (String) openFixBinding.getValue();
            //          if(of.equals("n"))
            //          {
            //                  throw new ValidatorException(new FacesMessage("Specify whether it's an Open or Fixed P.O."),null);
            //              }
            System.out.println("OFFFF" + of);

            if (of.equalsIgnoreCase("O")) {
                throw new ValidatorException(new FacesMessage("For an Open P.O. Quantity need not be specified"), null);
            }
        }
    }


    public void setBindAckIdDtl(RichColumn bindAckIdDtl) {
        this.bindAckIdDtl = bindAckIdDtl;
    }

    public RichColumn getBindAckIdDtl() {
        return bindAckIdDtl;
    }

    public String saveAndCloseAckNoAL() {
        ADFUtils.setLastUpdatedBy("SaleOrderHeaderVO1Iterator","LastUpdatedBy");    
        DCIteratorBinding Dcite = ADFUtils.findIterator("SaleOrderDetailVO1Iterator");
        SaleOrderDetailVORowImpl row = (SaleOrderDetailVORowImpl) Dcite.getCurrentRow();
        System.out.println("openFix=====" + row.getOpenFix());
        if ((row.getOpenFix().toString().equalsIgnoreCase("F") && row.getQuantity() != null) ||
            (row.getOpenFix().toString().equalsIgnoreCase("O") && row.getQuantity() == null)) {

            OperationBinding op = ADFUtils.findOperation("getAckNumber");

            Object rst = op.execute();

            //        ADFUtils.findOperation("getSaleOrderRefresh").execute();
            //

            System.out.println("--------Commit-------");
            System.out.println("value aftr getting result--->?" + rst);


            if (rst.toString() != null && rst.toString() != "") {
                if (op.getErrors().isEmpty()) {
                    ADFUtils.findOperation("apprvdvalue").execute();
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message =
                        new FacesMessage("Record Saved Successfully.New Sale Order No is " + rst + ".");
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);
                    return "SAve And Close";
                }
            }

            if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ") {
                if (op.getErrors().isEmpty()) {
                    ADFUtils.findOperation("apprvdvalue").execute();
                    ADFUtils.findOperation("postChanges").execute();
                    FacesMessage Message = new FacesMessage("Record Update Successfully.");
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);
                    if (Flag.equalsIgnoreCase("V")) {
                        System.out.println("Flag==" + Flag);
                        try {
                            OperationBinding opp = (OperationBinding) ADFUtils.findOperation("validUptoSet");
                            opp.getParamsMap().put("AmendAckNumber",
                                                   ADFUtils.resolveExpression("#{pageFlowScope.AmendAckNumber}"));
                            opp.getParamsMap().put("AckId", ADFUtils.resolveExpression("#{pageFlowScope.AckId}"));
                            opp.getParamsMap().put("AckNumber",
                                                   ADFUtils.resolveExpression("#{pageFlowScope.AckNumber}"));
                            opp.execute();

                        } catch (Exception e) {
                            ADFUtils.showMessage("Quantity is required in case of fixed Sale Order.", 0);
                            return null;
                        }
                    }

                    ADFUtils.findOperation("Commit").execute();
                    return "SAve And Close";
                }


            }
        } else {
            ADFUtils.showMessage("Quantity is required in case of fixed Sale Order.", 0);
            return null;
        }
        return "SAve And Close";
    }

    public void setCurrencyRate(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        System.out.println("curr" + bindCurrency.getValue());
        if (valueChangeEvent != null && bindCurrency.getValue() != null) {
            String Currency = (String) bindCurrency.getValue();
            System.out.println("Currency" + Currency);
            //            BigDecimal currRate = (BigDecimal) bindCurrencyRate.getValue();
            if (Currency.equals("INR")) {

                getBindCurrencyRate().setValue("1");
                System.out.println("rate" + bindCurrencyRate.getValue());
            } else
                bindCurrencyRate.setValue(null);
        }

    }

    public void setBindCurrency(RichSelectOneChoice bindCurrency) {
        this.bindCurrency = bindCurrency;
    }

    public RichSelectOneChoice getBindCurrency() {
        return bindCurrency;
    }

    public void setBindCurrencyRate(RichInputText bindCurrencyRate) {
        this.bindCurrencyRate = bindCurrencyRate;
    }

    public RichInputText getBindCurrencyRate() {
        return bindCurrencyRate;
    }

    public void CreateAL(ActionEvent actionEvent) {

        if (destinationFieldBind.getValue()!= null && destinationFieldBind.getValue()!="" ) {
            System.out.println("Value of D"+destinationFieldBind.getValue());
            DCIteratorBinding Dcite = ADFUtils.findIterator("SaleOrderHeaderVO1Iterator");
            if (Dcite.getEstimatedRowCount() > 0 && Dcite.getCurrentRow() != null) {
                SaleOrderHeaderVORowImpl row = (SaleOrderHeaderVORowImpl) Dcite.getCurrentRow();

               // if (row.getPaymentTerm() != null) {
                    System.out.println("not null condition");
                    if ((Long) ADFUtils.evaluateEL("#{bindings.SaleOrderDetailVO1Iterator.estimatedRowCount}") >= 1) {
                        OperationBinding op = (OperationBinding) ADFUtils.findOperation("CreateAtFirst");
                        op.execute();
                    } else {
                        ADFUtils.findOperation("CreateInsert").execute();
                    }
                    getSoTypeBinding().setDisabled(true);
                    getPoNumberBinding().setDisabled(true);
                    getBindingValidUpto().setDisabled(true);
                    getCustAmendNoBinding().setDisabled(true);
                    getPoDateBinding().setDisabled(true);
                    getQuotationBinding().setDisabled(true);
                    getBindCustCode().setDisabled(true);
                    getCpclbinding().setDisabled(true);
//                } else {
//
//                    //                           ADFUtils.showMessage("Destination is required in Terms and Condition ", 0);
//                    //                        ADFUtils.showMessage("Transporter Code is required in Terms and Condition ", 0);
//                    ADFUtils.showMessage("Payment Term is required in Terms and Condition ", 0);
//
//                }

            }
            oracle.jbo.domain.Date Valid = (Date) bindingValidUpto.getValue();
            System.out.println("validupto=====" + Valid);
            bindDeliveryDate.setValue(Valid);
        } else {
            ADFUtils.showMessage("Destinaiton is required in terms and condition Tab", 0);
        }
    }

    public void setEntryDateBinding(RichInputText entryDateBinding) {
        this.entryDateBinding = entryDateBinding;
    }

    public RichInputText getEntryDateBinding() {
        try {
              System.out.println("set getBindSrvDate"+ADFUtils.getTodayDate());
              String date=ADFUtils.convertAdToBs(ADFUtils.getTodayDate().toString());
              System.out.println("setter getvaluedate"+date);
           //   srvdatenepalbinding.setValue(date.toString());
                    DCIteratorBinding dci = ADFUtils.findIterator("SaleOrderHeaderVO1Iterator");
                    dci.getCurrentRow().setAttribute("AckDateNepal",date);
          } 
        catch (ParseException e) {
            
          }
        return entryDateBinding;
    }

    public void setPlantBinding(RichInputText plantBinding) {
        this.plantBinding = plantBinding;
    }

    public RichInputText getPlantBinding() {
        return plantBinding;
    }

    public void setCityBinding(RichInputComboboxListOfValues cityBinding) {
        this.cityBinding = cityBinding;
    }

    public RichInputComboboxListOfValues getCityBinding() {
        return cityBinding;
    }


    public void setOutputtextBinding(RichOutputText outputtextBinding) {
        this.outputtextBinding = outputtextBinding;
    }

    public RichOutputText getOutputtextBinding() {
        cevmodecheck();
        return outputtextBinding;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }


    public void setDetailcreateBinding(RichButton detailcreateBinding) {
        this.detailcreateBinding = detailcreateBinding;
    }

    public RichButton getDetailcreateBinding() {
        return detailcreateBinding;
    }

    public void setDetaildeleteBinding(RichButton detaildeleteBinding) {
        this.detaildeleteBinding = detaildeleteBinding;
    }

    public RichButton getDetaildeleteBinding() {
        return detaildeleteBinding;
    }

    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }


    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }


    //Set Fields and Button disable.
    public void cevModeDisableComponent(String mode) {
        //            FacesContext fctx = FacesContext.getCurrentInstance();
        //            ELContext elctx = fctx.getELContext();
        //            Application jsfApp = fctx.getApplication();
        //            //create a ValueExpression that points to the ADF binding layer
        //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
        //            //
        //            ValueExpression valueExpr = exprFactory.createValueExpression(
        //                                         elctx,
        //                                         "#{pageFlowScope.mode=='E'}",
        //                                          Object.class
        //                                         );
        //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
        if (mode.equals("E")) {
            getUomBinding().setDisabled(true);
            getAmdDateBinding().setDisabled(true);
            getPlantHoBinding().setDisabled(true);
            //          getBindingValidUpto().setDisabled(true);
            getAmendAm().setDisabled(true);
            getCpclbinding().setDisabled(true);
            getAmtBinding().setDisabled(true);
            getBindUnitCd().setDisabled(true);
            getAckNumberBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            getEntryDateBinding().setDisabled(true);
            getCityBinding().setDisabled(true);
            getPlantBinding().setDisabled(true);
            getProcessSeqBinding().setDisabled(true);
            getGstDescBinding().setDisabled(true);
            getCgstBinding().setDisabled(true);
            getCgstRateBinding().setDisabled(true);
            getIgstBinding().setDisabled(true);
            getIgstRateBinding().setDisabled(true);
//            getSgstBinding().setDisabled(true);
//            getSgstRateBinding().setDisabled(true);
            getNetAmtBinding().setDisabled(true);
            getTotalamtbind().setDisabled(true);
            //  getDetailcreateBinding().setDisabled(false);
            //getDetaildeleteBinding().setDisabled(false);
            getAckdatenepalinding().setDisabled(true);
            getValiddatenepalinding().setDisabled(true);
            getCustnepaldateinding().setDisabled(true);
        } else if (mode.equals("C")) {
            getUomBinding().setDisabled(true);
            getAmdDateBinding().setDisabled(true);
            getCpclbinding().setDisabled(true);
            getPlantHoBinding().setDisabled(true);
            getAmtBinding().setDisabled(true);
            getPoCanBinding().setDisabled(true);
            getAmendAm().setDisabled(true);
            getBindUnitCd().setDisabled(true);
            getAckNumberBinding().setDisabled(true);
            getPoStatusBinding().setDisabled(true);
            getEntryDateBinding().setDisabled(true);
            getApprovedByBinding().setDisabled(true);
            getCityBinding().setDisabled(true);
            getPlantBinding().setDisabled(true);
            getProcessSeqBinding().setDisabled(true);
            getGstDescBinding().setDisabled(true);
            getCgstBinding().setDisabled(true);
            getCgstRateBinding().setDisabled(true);
            getIgstBinding().setDisabled(true);
            getIgstRateBinding().setDisabled(true);
//            getSgstBinding().setDisabled(true);
//            getSgstRateBinding().setDisabled(true);
            getNetAmtBinding().setDisabled(true);
            getTotalamtbind().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            getAckdatenepalinding().setDisabled(true);
            //  getDetailcreateBinding().setDisabled(false);
            // getDetaildeleteBinding().setDisabled(false);
            getValiddatenepalinding().setDisabled(true);
            getCustnepaldateinding().setDisabled(true);
        } else if (mode.equals("V")) {
            getUomBinding().setDisabled(true);
            getCpclbinding().setDisabled(true);
            getAmtBinding().setDisabled(true);
            getAckNumberBinding().setDisabled(true);
            getEntryDateBinding().setDisabled(true);
            
            getCityBinding().setDisabled(true);
            getPlantBinding().setDisabled(true);
            getProcessSeqBinding().setDisabled(true);
            getGstDescBinding().setDisabled(true);
            getCgstBinding().setDisabled(true);
            getCgstRateBinding().setDisabled(true);
            getIgstBinding().setDisabled(true);
            getIgstRateBinding().setDisabled(true);
//            getSgstBinding().setDisabled(true);
//            getSgstRateBinding().setDisabled(true);
            //   getDetailcreateBinding().setDisabled(true);
            // getDetaildeleteBinding().setDisabled(true);
        }

    }

    public void EditButtonAL(ActionEvent actionEvent) {
        System.out.println("fghgfffg");
        DCIteratorBinding binding = ADFUtils.findIterator("SaleOrderHeaderVO1Iterator");
        if (binding.getEstimatedRowCount() > 0 && binding.getCurrentRow() != null) {

            SaleOrderHeaderVORowImpl row = (SaleOrderHeaderVORowImpl) binding.getCurrentRow();
            if (poStatusBinding.getValue().toString().equalsIgnoreCase("C")) {
                String mode = "V";
                System.out.println("mode beforesetting" + ADFUtils.resolveExpression("#{pageFlowScope.mode}"));
                ADFUtils.showMessage("Check List Can not modified once cancelled", 2);

                ADFUtils.setEL("#{pageFlowScope.mode}", mode);
                cevmodecheck();
                System.out.println("mode after" + ADFUtils.resolveExpression("#{pageFlowScope.mode}"));
            }
            System.out.println("aprobed" + row.getApprovedBy());
            if (row.getApprovedBy() != null && row.getApprovedBy() != "" &&
                row.getApprovedBy().toString().trim().length() > 0 && poStatusBinding.getValue().equals("A")) {
                System.out.println("Appr by" + row.getApprovedBy());
                RichPopup.PopupHints pop = new RichPopup.PopupHints();
                editPopupBinding.show(pop);

            }


            else {

                cevmodecheck();
            }
        }
    }

    public void setProcessSeqBinding(RichInputText processSeqBinding) {
        this.processSeqBinding = processSeqBinding;
    }

    public RichInputText getProcessSeqBinding() {
        return processSeqBinding;
    }

    public void setGstDescBinding(RichInputText gstDescBinding) {
        this.gstDescBinding = gstDescBinding;
    }

    public RichInputText getGstDescBinding() {
        return gstDescBinding;
    }

    public void setCgstBinding(RichInputText cgstBinding) {
        this.cgstBinding = cgstBinding;
    }

    public RichInputText getCgstBinding() {
        return cgstBinding;
    }

    public void setCgstRateBinding(RichInputText cgstRateBinding) {
        this.cgstRateBinding = cgstRateBinding;
    }

    public RichInputText getCgstRateBinding() {
        return cgstRateBinding;
    }

    public void setIgstBinding(RichInputText igstBinding) {
        this.igstBinding = igstBinding;
    }

    public RichInputText getIgstBinding() {
        return igstBinding;
    }

    public void setIgstRateBinding(RichInputText igstRateBinding) {
        this.igstRateBinding = igstRateBinding;
    }

    public RichInputText getIgstRateBinding() {
        return igstRateBinding;
    }

    public void setSgstBinding(RichInputText sgstBinding) {
        this.sgstBinding = sgstBinding;
    }

    public RichInputText getSgstBinding() {
        return sgstBinding;
    }

    public void setSgstRateBinding(RichInputText sgstRateBinding) {
        this.sgstRateBinding = sgstRateBinding;
    }

    public RichInputText getSgstRateBinding() {
        return sgstRateBinding;
    }

    public void amdConfirmDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            
            OperationBinding operBind=ADFUtils.findOperation("purchaseOrderPaymentTerms");
                    Object obj=operBind.execute();
                    System.out.println("Result is ====>  "+operBind.getResult());
                    if(operBind.getResult()!=null && operBind.getResult().equals("N"))
                    {
                        FacesMessage Message = new FacesMessage("Payment Term Must Be Defined 100%.");  
                        Message.setSeverity(FacesMessage.SEVERITY_ERROR);  
                        FacesContext fc = FacesContext.getCurrentInstance();  
                        fc.addMessage(null, Message); 
                        
                    }
                    
            
            OperationBinding op1 = ADFUtils.findOperation("checkMaxAmendment");
            Object ob = op1.execute();
            if (op1.getResult().toString() != null && op1.getResult().toString().equalsIgnoreCase("N")) {

                ADFUtils.showMessage("You Can Update Latest Amendment Only.", 2);
                ADFUtils.setEL("#{pageFlowScope.mode}", "V");
            } else {
                Flag = "V";

                OperationBinding op = ADFUtils.findOperation("getAmdSaleOrderDetails");
                op.getParamsMap().put("AmendAckNumber", ADFUtils.resolveExpression("#{pageFlowScope.AmendAckNumber}"));
                op.getParamsMap().put("AckId", ADFUtils.resolveExpression("#{pageFlowScope.AckId}"));
                op.getParamsMap().put("AckNumber", ADFUtils.resolveExpression("#{pageFlowScope.AckNumber}"));
                Object ob1 = op.execute();
                System.out.println("cust po no result is==>" + op.getResult().toString());
                poNumberBinding.setValue(op.getResult().toString());

                cevmodecheck();
                getOpenFixBinding().setDisabled(true);


                AdfFacesContext.getCurrentInstance().addPartialTarget(detailSaleOrderTableBinding);
            }
        } else
            getEditPopupBinding().hide();

    }

    public void setEditPopupBinding(RichPopup editPopupBinding) {
        this.editPopupBinding = editPopupBinding;
    }

    public RichPopup getEditPopupBinding() {
        return editPopupBinding;
    }

    public void setAmendAm(RichInputText amendAm) {
        this.amendAm = amendAm;
    }

    public RichInputText getAmendAm() {
        return amendAm;
    }

    public void setCustAmendNoBinding(RichInputText custAmendNoBinding) {
        this.custAmendNoBinding = custAmendNoBinding;
    }

    public RichInputText getCustAmendNoBinding() {
        return custAmendNoBinding;
    }

    public void setPoDateBinding(RichInputDate poDateBinding) {
        this.poDateBinding = poDateBinding;
    }

    public RichInputDate getPoDateBinding() {
        try {
          //              System.out.println("set getdate"+ADFUtils.getTodayDate());
                        String date=ADFUtils.convertAdToBs(ADFUtils.getTodayDate().toString());
                        System.out.println("setter getvaluedate"+date);
             //   srvdatenepalbinding.setValue(date.toString());
                      DCIteratorBinding dci = ADFUtils.findIterator("SaleOrderHeaderVO1Iterator");
                      dci.getCurrentRow().setAttribute("CustPoDateNepal",date);
                    }
                  catch (ParseException e) {
          
                    }
        return poDateBinding;
    }

    public void setQuotationBinding(RichInputComboboxListOfValues quotationBinding) {
        this.quotationBinding = quotationBinding;
    }

    public RichInputComboboxListOfValues getQuotationBinding() {
        return quotationBinding;
    }

    public void setTermsnConditionBinding(RichPopup termsnConditionBinding) {
        this.termsnConditionBinding = termsnConditionBinding;
    }

    public RichPopup getTermsnConditionBinding() {
        return termsnConditionBinding;
    }

    public void setCreditperiodBinding(RichInputText creditperiodBinding) {
        this.creditperiodBinding = creditperiodBinding;
    }

    public RichInputText getCreditperiodBinding() {
        return creditperiodBinding;
    }

    public void setDestinationBinding(RichInputText destinationBinding) {
        this.destinationBinding = destinationBinding;
    }

    public RichInputText getDestinationBinding() {
        return destinationBinding;
    }

    public void setShipModeBinding(RichSelectOneChoice shipModeBinding) {
        this.shipModeBinding = shipModeBinding;
    }

    public RichSelectOneChoice getShipModeBinding() {
        return shipModeBinding;
    }

    public void DeletePopupDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete").execute();
            System.out.println("Record Delete Successfully");
            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(detailSaleOrderTableBinding);
    }

    public void setDetailSaleOrderTableBinding(RichTable detailSaleOrderTableBinding) {
        this.detailSaleOrderTableBinding = detailSaleOrderTableBinding;
    }

    public RichTable getDetailSaleOrderTableBinding() {
        return detailSaleOrderTableBinding;
    }

    public void setTermsButtonBinding(RichButton termsButtonBinding) {
        this.termsButtonBinding = termsButtonBinding;
    }

    public RichButton getTermsButtonBinding() {
        return termsButtonBinding;
    }

    public void setPendPopupBindingp7(RichPopup pendPopupBindingp7) {
        this.pendPopupBindingp7 = pendPopupBindingp7;
    }

    public RichPopup getPendPopupBindingp7() {
        return pendPopupBindingp7;
    }

    public void Refreshafterprocess(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce != null) {
            AdfFacesContext.getCurrentInstance().addPartialTarget(detailSaleOrderTableBinding);
        }


    }

    public void QtyRequired(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (object != null) {
            String OF = (String) object;
            System.out.println("Objecttttt" + object);
            if (OF.equalsIgnoreCase("F")) {
                //                System.out.println("iffff when of ========F");
                //                throw new ValidatorException(new FacesMessage("For Open S.O Quantity need not to be specify."),null);
                //
                //            }
                //        else
                //        {
                System.out.println("else when of ========F");
                throw new ValidatorException(new FacesMessage("For Fixed S.O Quantity need is required."), null);


            }

        }

    }

    public void VIDATEQTYOF(FacesContext facesContext, UIComponent uIComponent, Object object) {
        BigDecimal Quant1 = (BigDecimal) object;
        BigDecimal Quant = object == null ? new BigDecimal(0) : (BigDecimal) object;
        System.out.println("objQty" + Quant);
        System.out.println("Quant1111" + Quant1);
        //     if(Quant.equals(null))
        //     {
        //             bindOpenFix.setValue(0);
        //         }
        if (Quant.compareTo(new BigDecimal(0)) == 0) {
            openFixBinding.setValue("O");
            //        throw new ValidatorException(new FacesMessage("Quantity Cannot be zeroo"),null);
        } else if (Quant.compareTo(new BigDecimal(0)) == 1) {
            openFixBinding.setValue("F");
        }


        //   else if(Quant1.compareTo(new BigDecimal(0))==0)
        //   {
        //      System.out.println("elseeeee");
        //       bindOpenFix.setValue(0);
        //    }
    }


    public void OpenSOValidate(FacesContext facesContext, UIComponent uIComponent, Object object) {

        if (object != null) {
            BigDecimal Quant = (BigDecimal) object;
            if (Quant.compareTo(new BigDecimal(0)) == 1) {
                System.out.println("iffff when of =====O" + Quant);
                throw new ValidatorException(new FacesMessage("For Open S.O Quantity need not to be specify."), null);
            }
        }
    }


    public void setconsignee(ValueChangeEvent vce) {
        if (vce.getNewValue() != null) {
            vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
            System.out.println("Customerif not " + bindCustCode.getValue());
            String Customer = (String) bindCustCode.getValue();
            if (Customer != null) {
                System.out.println("Customer is===>" + Customer);

                System.out.println("Consignee Code is====>" + consigneeCd.getValue());
                consigneeCd.setValue(Customer);

            }
        }
    }

    public void setConsigneeCd(RichInputComboboxListOfValues consigneeCd) {
        this.consigneeCd = consigneeCd;
    }

    public RichInputComboboxListOfValues getConsigneeCd() {
        return consigneeCd;
    }

    public void approveDaUTHvce(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        System.out.println("Cpcl===" + cpclbinding.getValue());
        String Cpcl = (String) cpclbinding.getValue();
        BigDecimal amount = new BigDecimal(0);
        DCIteratorBinding dci = ADFUtils.findIterator("SaleOrderDetailVO1Iterator");
        RowSetIterator rsi = dci.getViewObject().createRowSetIterator(null);
        while (rsi.hasNext()) {
            System.out.println("Value in amt====>>");
            Row r = rsi.next();
            BigDecimal sum = (BigDecimal) r.getAttribute("AmountTrans");
            if (sum != null) {
                amount = amount.add(sum);

            }

        }
        rsi.closeRowSetIterator();
        System.out.println("Amountttttttttttttt==>>" + amount);
//        if (Cpcl.equalsIgnoreCase("CL")) {
            OperationBinding ob = ADFUtils.findOperation("checkApprovalFunctionStatus");
            ob.getParamsMap().put("formNm", "SO");
            ob.getParamsMap().put("authoLim", "AP");
            ob.getParamsMap().put("empcd", vce.getNewValue().toString());
            ob.getParamsMap().put("UnitCd", bindUnitCd.getValue());
            ob.getParamsMap().put("Amount", amount);
            ob.execute();

            System.out.println(" EMP CD" + vce.getNewValue().toString() + netAmtBinding.getValue());
            if ((ob.getResult() != null && !ob.getResult().equals("Y"))) {
                Row row = (Row) ADFUtils.evaluateEL("#{bindings.SaleOrderHeaderVO1Iterator.currentRow}");
                row.setAttribute("ApprovedBy", null);
                ADFUtils.showMessage(ob.getResult().toString(), 0);
            }
//        } else {
//            Row row = (Row) ADFUtils.evaluateEL("#{bindings.SaleOrderHeaderVO1Iterator.currentRow}");
//            row.setAttribute("ApprovedBy", null);
//            ADFUtils.showMessage("Approval allowed in Check List order type only.", 0);
//        }
    }

    public void setPoStatusBinding(RichSelectOneChoice poStatusBinding) {
        this.poStatusBinding = poStatusBinding;
    }

    public RichSelectOneChoice getPoStatusBinding() {
        return poStatusBinding;
    }

    public void ChangeStatusVCE(ValueChangeEvent vce) {
        if (vce != null) {
            vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
            String StatusHeader = (String) poStatusBinding.getValue();
            System.out.println("StatusHeader" + StatusHeader);
            OperationBinding op = ADFUtils.findOperation("SetStatusheadertoDtl");
            op.getParamsMap().put("Stutus", StatusHeader);
            op.execute();
            AdfFacesContext.getCurrentInstance().addPartialTarget(detailSaleOrderTableBinding);
        }
    }

    public void setPoCanBinding(RichSelectBooleanCheckbox poCanBinding) {
        this.poCanBinding = poCanBinding;
    }

    public RichSelectBooleanCheckbox getPoCanBinding() {
        return poCanBinding;
    }

    public void setTransporterBinding(RichInputComboboxListOfValues transporterBinding) {
        this.transporterBinding = transporterBinding;
    }

    public RichInputComboboxListOfValues getTransporterBinding() {
        return transporterBinding;
    }

    public void setOpenFixBinding(RichSelectOneChoice openFixBinding) {
        this.openFixBinding = openFixBinding;
    }

    public RichSelectOneChoice getOpenFixBinding() {
        return openFixBinding;
    }


    public void setInsurComp(RichInputText insurComp) {
        this.insurComp = insurComp;
    }

    public RichInputText getInsurComp() {
        return insurComp;
    }

    public void setBindingValF(RichInputDate bindingValF) {
        this.bindingValF = bindingValF;
    }

    public RichInputDate getBindingValF() {
        return bindingValF;
    }

    public void currVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce != null) {
            System.out.println("inside vce of urrency code");
            OperationBinding op = ADFUtils.findOperation("CurrCheckFromDailyCurre");
            op.getParamsMap().put("CurCd", vce.getNewValue());
            Object ob = op.execute();
            System.out.println("result iss==>>" + op.getResult());

            if (op.getResult() != null && op.getResult().toString().equalsIgnoreCase("N")) {
                System.out.println("inside if when result is ===>>N");

                FacesMessage Message =
                    new FacesMessage("Daily Currency is  not updated for this SO Date in Currency Master");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            }
        }
    }

    public void Validuptovalidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (object != null) {
            System.out.println("object====" + object);
            oracle.jbo.domain.Date valid = (oracle.jbo.domain.Date) object;
            System.out.println("validUto" + valid);
            Calendar cal = Calendar.getInstance();
            cal.getTime();
            System.out.println("calll" + cal);
            java.sql.Timestamp datet = new java.sql.Timestamp(System.currentTimeMillis());
            System.out.println("datetdatetdatetdatet" + datet);
            String date_string = datet.toString();

            System.out.println("date_string 0-10    " + date_string.substring(0, 10));
            System.out.println("date_string 1-10    " + date_string.substring(1, 10));


            oracle.jbo.domain.Date datetime = new oracle.jbo.domain.Date((date_string.substring(0, 10)));

            System.out.println("JBO DATE datetime" + datetime);
            System.out.println("valid.compareTo(datetime)" + valid.compareTo(datetime));
            if (valid.compareTo(datetime) == -1) {
                throw new ValidatorException(new FacesMessage("Valid Upto must be greater or equals to current date"),
                                             null);
            }
        }

    }

    public void OrderCheckListVCL(ValueChangeEvent vce) {
        if (vce.getNewValue() != null) {
            vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
            String CLp = (String) vce.getNewValue();
            System.out.println("vce new value====." + CLp);
            if (CLp.equalsIgnoreCase("CL")) {
                System.out.println("mandatoryyyy");
                getBindingValidUpto().setRequired(true);
            } else {
                System.out.println(":validuptooo");
                getBindingValidUpto().setRequired(false);
            }
        }
    }

    public void setBindingValidUpto(RichInputDate bindingValidUpto) {
        this.bindingValidUpto = bindingValidUpto;
    }

    public RichInputDate getBindingValidUpto() {
        return bindingValidUpto;
    }

    public void delDateValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {

        if (object != null && bindingValidUpto.getValue() != null) {
            Date delDate = (Date) object;
            Date valupto = (Date) bindingValidUpto.getValue();
            System.out.println("delDate====" + delDate);
            System.out.println("valupto====" + valupto);
            if (delDate.compareTo(valupto) == 1) {
                throw new ValidatorException(new FacesMessage("Delivery Date must be less or equals to valid upto date"),
                                             null);

            }


        }

    }

    public void setCpclbinding(RichSelectOneChoice cpclbinding) {
        this.cpclbinding = cpclbinding;
    }

    public RichSelectOneChoice getCpclbinding() {
        return cpclbinding;
    }

    public void setSoTypeBinding(RichSelectOneChoice soTypeBinding) {
        this.soTypeBinding = soTypeBinding;
    }

    public RichSelectOneChoice getSoTypeBinding() {
        return soTypeBinding;
    }

    public void setAmtBinding(RichInputText amtBinding) {
        this.amtBinding = amtBinding;
    }

    public RichInputText getAmtBinding() {
        return amtBinding;
    }

    public void setAddTypeBinding(RichSelectOneChoice addTypeBinding) {
        this.addTypeBinding = addTypeBinding;
    }

    public RichSelectOneChoice getAddTypeBinding() {
        return addTypeBinding;
    }

    public void setAddDiscBinding(RichInputText addDiscBinding) {
        this.addDiscBinding = addDiscBinding;
    }

    public RichInputText getAddDiscBinding() {
        return addDiscBinding;
    }

    public void setReqAddTypeVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        System.out.println("AddType---+" + vce.getNewValue());
        String AddType = (String) vce.getNewValue();
        if (AddType.equalsIgnoreCase("A") || AddType.equalsIgnoreCase("P")) {
            System.out.println("AddDisc mandatory");
            getAddDiscBinding().setRequired(true);
            getAddDiscBinding().setDisabled(false);
        } else {
            addDiscBinding.setValue(null);
            System.out.println("elseeee");
            getAddDiscBinding().setDisabled(true);
            getAddDiscBinding().setRequired(false);
        }
    }

    public void SetReqTradeVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        System.out.println("Trade---+" + vce.getNewValue());
        String TradeType = (String) vce.getNewValue();
        if (TradeType.equalsIgnoreCase("A") || TradeType.equalsIgnoreCase("P")) {
            System.out.println("TradeType mandatory");
            getTradeDisc().setRequired(true);
            getTradeDisc().setDisabled(false);
        } else {
            tradeDisc.setValue(null);
            System.out.println("elseeee");
            getTradeDisc().setDisabled(true);
            System.out.println("elseeee");
            getTradeDisc().setRequired(false);
        }
    }

    public void SetReqServiceVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        System.out.println("Serv---+" + vce.getNewValue());
        String Serv = (String) vce.getNewValue();
        if (Serv.equalsIgnoreCase("A") || Serv.equalsIgnoreCase("P")) {
            System.out.println("Serv mandatory");
            getSerVDisc().setDisabled(false);
            getSerVDisc().setRequired(true);

        } else {
            serVDisc.setValue(null);
            System.out.println("elseeee");
            getSerVDisc().setDisabled(true);
            getSerVDisc().setRequired(false);
        }
    }

    public void setTradeTypeVCE(RichSelectOneChoice tradeTypeVCE) {
        this.tradeTypeVCE = tradeTypeVCE;
    }

    public RichSelectOneChoice getTradeTypeVCE() {
        return tradeTypeVCE;
    }

    public void setSerType(RichSelectOneChoice serType) {
        this.serType = serType;
    }

    public RichSelectOneChoice getSerType() {
        return serType;
    }

    public void setTradeDisc(RichInputText tradeDisc) {
        this.tradeDisc = tradeDisc;
    }

    public RichInputText getTradeDisc() {
        return tradeDisc;
    }

    public void setSerVDisc(RichInputText serVDisc) {
        this.serVDisc = serVDisc;
    }

    public RichInputText getSerVDisc() {
        return serVDisc;
    }

    public void setPlantHoBinding(RichInputText plantHoBinding) {
        this.plantHoBinding = plantHoBinding;
    }

    public RichInputText getPlantHoBinding() {
        return plantHoBinding;
    }

    public void setAmdDateBinding(RichInputDate amdDateBinding) {
        this.amdDateBinding = amdDateBinding;
    }

    public RichInputDate getAmdDateBinding() {
        return amdDateBinding;
    }

    public void setNetAmtBinding(RichInputText netAmtBinding) {
        this.netAmtBinding = netAmtBinding;
    }

    public RichInputText getNetAmtBinding() {
        return netAmtBinding;
    }

    public void setTotalamtbind(RichInputText totalamtbind) {
        this.totalamtbind = totalamtbind;
    }

    public RichInputText getTotalamtbind() {
        return totalamtbind;
    }

    public void setDestinationBinding1(RichInputComboboxListOfValues destinationBinding1) {
        this.destinationBinding1 = destinationBinding1;
    }

    public RichInputComboboxListOfValues getDestinationBinding1() {
        return destinationBinding1;
    }

    public void SetDelivery(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        //        oracle.jbo.domain.Date Valid = (Date) bindingValidUpto.getValue();
        //        System.out.println("validupto====="+Valid);
        //
            try {
        //              System.out.println("set getdate"+ADFUtils.getTodayDate());
                      String date=ADFUtils.convertAdToBs(valueChangeEvent.getNewValue().toString());
                      System.out.println("setter getvaluedate"+date);
           //   srvdatenepalbinding.setValue(date.toString());
                    DCIteratorBinding dci = ADFUtils.findIterator("SaleOrderHeaderVO1Iterator");
                    dci.getCurrentRow().setAttribute("ValidUptoNepal",date);
                  }
                catch (ParseException e) {
        
                  }
    }

    public void AltKeyVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce.getNewValue() != null) {
            String AckNumber = (String) bindCustCode.getValue();
            System.out.println("bean ack" + AckNumber);
            String CustPoNumber = (String) poNumberBinding.getValue();
            System.out.println("bean po" + CustPoNumber);
            OperationBinding ob = ADFUtils.findOperation("PoNumberValidator");
            ob.getParamsMap().put("ponum", CustPoNumber);
            ob.getParamsMap().put("acknum", AckNumber);
            ob.execute();

            if (ob.getResult() != null) {
                String flag = ob.getResult().toString();

                if ("Y".equalsIgnoreCase(flag)) {
                    ADFUtils.showMessage("Duplicate Record,This PO Number exists in Database", 2);
                }
            }
        }
    }

    public void setMarketType(RichSelectOneChoice marketType) {
        this.marketType = marketType;
    }

    public RichSelectOneChoice getMarketType() {
        return marketType;
    }

    public void CustomerVCE(ValueChangeEvent vce) {
        if (vce.getNewValue() != null) {
            vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
            System.out.println("Customerif not " + bindCustCode.getValue());
            OperationBinding op = (OperationBinding) ADFUtils.findOperation("setmarketType");
            op.getParamsMap().put("Cust", bindCustCode.getValue());
            Object Result = op.execute();
            System.out.println("ob.getrE" + op.getResult());
            if (op.getResult().toString().equalsIgnoreCase("Y")) {
                marketType.setValue("E");
            } else {
                marketType.setValue("R");
            }
        }
    }

    public void setQuantityColumnBinding(RichColumn quantityColumnBinding) {
        this.quantityColumnBinding = quantityColumnBinding;
    }

    public RichColumn getQuantityColumnBinding() {
        return quantityColumnBinding;
    }

    public void setDestinationCreateBinding(RichPanelLabelAndMessage destinationCreateBinding) {
        this.destinationCreateBinding = destinationCreateBinding;
    }

    public RichPanelLabelAndMessage getDestinationCreateBinding() {
        return destinationCreateBinding;
    }

    public void setDestinationFieldBind(RichInputText destinationFieldBind) {
        this.destinationFieldBind = destinationFieldBind;
    }

    public RichInputText getDestinationFieldBind() {
        return destinationFieldBind;
    }
    

    public void setDiscountPriceBinding(RichInputText discountPriceBinding) {
        this.discountPriceBinding = discountPriceBinding;
    }

    public RichInputText getDiscountPriceBinding() {
        return discountPriceBinding;
    }

    public void percentagePayTermVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
         
           System.out.println("Object Value====>  "+vce.toString());
           System.out.println("Start Call purchaseOrderPaymentTerms method");
           OperationBinding operBind=ADFUtils.findOperation("purchaseOrderPaymentTerms");
           Object obj=operBind.execute();
           System.out.println("Result is ====>  "+operBind.getResult());
           if(operBind.getResult()!=null && operBind.getResult().equals("N"))
           { 
                          FacesMessage Message = new FacesMessage("Payment Term Must Be Defined 100%.");  
                           Message.setSeverity(FacesMessage.SEVERITY_ERROR);  
                           FacesContext fc = FacesContext.getCurrentInstance();  
                           fc.addMessage(null, Message);    
           }
            System.out.println("End Calling purchaseOrderPaymentTerms");
        //        }
    }

    public void setEmployeeNamebinding(RichInputText employeeNamebinding) {
        this.employeeNamebinding = employeeNamebinding;
    }

    public RichInputText getEmployeeNamebinding() {
        return employeeNamebinding;
    }

    public void setUomBinding(RichInputComboboxListOfValues uomBinding) {
        this.uomBinding = uomBinding;
    }

    public RichInputComboboxListOfValues getUomBinding() {
        return uomBinding;
    }

    public void setAckdatenepalinding(RichInputText ackdatenepalinding) {
        this.ackdatenepalinding = ackdatenepalinding;
    }

    public RichInputText getAckdatenepalinding() {
        return ackdatenepalinding;
    }

    public void setCustnepaldateinding(RichInputText custnepaldateinding) {
        this.custnepaldateinding = custnepaldateinding;
    }

    public RichInputText getCustnepaldateinding() {
        return custnepaldateinding;
    }

    public void setValiddatenepalinding(RichInputText validdatenepalinding) {
        this.validdatenepalinding = validdatenepalinding;
    }

    public RichInputText getValiddatenepalinding() {
        return validdatenepalinding;
    }

    public void custdateVCL(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
                try {
        //              System.out.println("set getdate"+ADFUtils.getTodayDate());
                      String date=ADFUtils.convertAdToBs(valueChangeEvent.getNewValue().toString());
        //              System.out.println("setter getvaluedate"+date);
           //   srvdatenepalbinding.setValue(date.toString());
                    DCIteratorBinding dci = ADFUtils.findIterator("SaleOrderHeaderVO1Iterator");
                    dci.getCurrentRow().setAttribute("CustPoDateNepal",date);
                  }
                catch (ParseException e) {
        
                  }
    }
}
