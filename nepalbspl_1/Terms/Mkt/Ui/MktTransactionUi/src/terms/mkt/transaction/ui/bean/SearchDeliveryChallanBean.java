package terms.mkt.transaction.ui.bean;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

public class SearchDeliveryChallanBean {
    private RichTable deliveryChallanSearchTableBinding;

    public SearchDeliveryChallanBean() {
    }
    public void deletePopupDialogDL(DialogEvent dialogEvent)
    {
        if(dialogEvent.getOutcome().name().equals("ok"))
                    {
                    ADFUtils.findOperation("Delete").execute();
                    ADFUtils.findOperation("Commit").execute();    //Unable on Search page.
                    System.out.println("Record Delete Successfully");
                   
                        FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                        FacesContext fc = FacesContext.getCurrentInstance();  
                        fc.addMessage(null, Message);  
                   
                    }

                AdfFacesContext.getCurrentInstance().addPartialTarget(deliveryChallanSearchTableBinding);
    
    
    
    }
    public void setDeliveryChallanSearchTableBinding(RichTable deliveryChallanSearchTableBinding) {
        this.deliveryChallanSearchTableBinding = deliveryChallanSearchTableBinding;
    }

    public RichTable getDeliveryChallanSearchTableBinding() {
        return deliveryChallanSearchTableBinding;
    }
}
