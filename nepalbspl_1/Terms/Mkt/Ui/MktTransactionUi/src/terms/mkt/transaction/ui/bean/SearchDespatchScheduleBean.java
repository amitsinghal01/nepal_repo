package terms.mkt.transaction.ui.bean;

import javax.faces.application.FacesMessage;

import javax.faces.context.FacesContext;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class SearchDespatchScheduleBean {
    private RichTable despatchScheduleTableBinding;
    private RichOutputText bindDocNo;
    private RichInputText enterbyBinding;
    private RichInputText remarksBinding;
    private RichInputDate authDateBind;
    private RichInputText yearBind;

    public SearchDespatchScheduleBean() {
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
       
        if(dialogEvent.getOutcome().name().equals("ok"))
                    {
                    ADFUtils.findOperation("Delete").execute();
                   ADFUtils.findOperation("Commit").execute();    //Unable on Search page.
                    System.out.println("Record Delete Successfully");
                   
                        FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                        FacesContext fc = FacesContext.getCurrentInstance();  
                        fc.addMessage(null, Message);  
                   
                    }

                AdfFacesContext.getCurrentInstance().addPartialTarget(despatchScheduleTableBinding);
       
    }

    public void setDespatchScheduleTableBinding(RichTable despatchScheduleTableBinding) {
        this.despatchScheduleTableBinding = despatchScheduleTableBinding;
    }

    public RichTable getDespatchScheduleTableBinding() {
        return despatchScheduleTableBinding;
    }

    public String checkAuthByAC() {
        System.out.println("ENTERBY in bean"+enterbyBinding.getValue());
        System.out.println("rEMARKS in bean========>>>"+remarksBinding.getValue());
        System.out.println("authdate in bean========>>>"+authDateBind.getValue());
        //System.out.println("year in bean========>>>"+yearBind.getValue());

        OperationBinding binding = ADFUtils.findOperation("amdApprovedNumber");
        binding.getParamsMap().put("DocNo", bindDocNo.getValue());  
        binding.getParamsMap().put("EnteredBy", enterbyBinding.getValue());   
        binding.getParamsMap().put("Remarks", remarksBinding.getValue());   
        binding.getParamsMap().put("AuthDate", authDateBind.getValue());
        //binding.getParamsMap().put("Year", yearBind.getValue());

        binding.execute();
    
        if(binding.getResult()!=null && binding.getResult().toString().equalsIgnoreCase("0"))

        {
            
            return "despatchDetails";
            
        }else{
                FacesMessage Message =
                    new FacesMessage("Yor are unable to generate new document because "+bindDocNo.getValue()+ " Amendment Number "+binding.getResult()+" is not Approved.");
                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
        
            }
        return null;
    }

    public void setBindDocNo(RichOutputText bindDocNo) {
        this.bindDocNo = bindDocNo;
    }

    public RichOutputText getBindDocNo() {
        return bindDocNo;
    }

    public void setEnterbyBinding(RichInputText enterbyBinding) {
        this.enterbyBinding = enterbyBinding;
    }

    public RichInputText getEnterbyBinding() {
        return enterbyBinding;
    }

    public void setRemarksBinding(RichInputText remarksBinding) {
        this.remarksBinding = remarksBinding;
    }

    public RichInputText getRemarksBinding() {
        return remarksBinding;
    }

    public void setAuthDateBind(RichInputDate authDateBind) {
        this.authDateBind = authDateBind;
    }

    public RichInputDate getAuthDateBind() {
        return authDateBind;
    }

    public void setYearBind(RichInputText yearBind) {
        this.yearBind = yearBind;
    }

    public RichInputText getYearBind() {
        return yearBind;
    }
}
