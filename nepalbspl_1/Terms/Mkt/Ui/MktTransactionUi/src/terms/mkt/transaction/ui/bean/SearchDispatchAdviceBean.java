package terms.mkt.transaction.ui.bean;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;

import java.math.BigDecimal;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import javax.naming.Context;
import javax.naming.InitialContext;

import javax.servlet.http.HttpServletResponse;

import javax.sql.DataSource;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import oracle.adf.model.BindingContext;
import oracle.adf.model.OperationBinding;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichDialog;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.BindingContainer;

public class SearchDispatchAdviceBean {
    private String NoteMode ="";
    private RichDialog deletePopupDialogDL;
    private RichTable searchDispatchAdviceTableBinding;
    private RichSelectOneChoice salenoteparam;

    public SearchDispatchAdviceBean() {
    }

    public void setDeletePopupDialogDL(RichDialog deletePopupDialogDL) {
        this.deletePopupDialogDL = deletePopupDialogDL;
    }

    public RichDialog getDeletePopupDialogDL() {
        return deletePopupDialogDL;
    }

    public void DeletePopupDialogDL(DialogEvent dialogEvent)   {
            if(dialogEvent.getOutcome().name().equals("ok"))
                {
                OperationBinding op = null;
                ADFUtils.findOperation("Delete").execute();
                op = (OperationBinding) ADFUtils.findOperation("Commit");
                Object rst = op.execute();
                System.out.println("Record Delete Successfully");
                    if(op.getErrors().isEmpty()){
                        FacesMessage Message = new FacesMessage("Record Deleted Successfully");
Message.setSeverity(FacesMessage.SEVERITY_INFO);
                        FacesContext fc = FacesContext.getCurrentInstance();
                        fc.addMessage(null, Message);
                    } else if (!op.getErrors().isEmpty())
                    {
                       OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
                       Object rstr = opr.execute();
                       FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                       FacesContext fc = FacesContext.getCurrentInstance();
                       fc.addMessage(null, Message);
                     }
                }

                        AdfFacesContext.getCurrentInstance().addPartialTarget(searchDispatchAdviceTableBinding);
            
    }

    public void setSearchDispatchAdviceTableBinding(RichTable searchDispatchAdviceTableBinding) {
        this.searchDispatchAdviceTableBinding = searchDispatchAdviceTableBinding;
    }

    public RichTable getSearchDispatchAdviceTableBinding() {
        return searchDispatchAdviceTableBinding;
    }
    
    
    public BindingContainer getBindings()
          {
            return BindingContext.getCurrent().getCurrentBindingsEntry();
          }
    
    public void downloadJrxml(FacesContext facesContext, OutputStream outputStream) {
        Connection conn=null;
            try {
                String file_name ="";
                System.out.println("NoteMode===================>"+NoteMode);
                
                if(NoteMode.equals("SN"))
                {
                file_name="sale_note.jasper";
                }
                else if(NoteMode.equals("Amd"))
                {
                    file_name="sale_note_amd.jasper";
                }
                else if(NoteMode.equals("Annex"))
                {
                    file_name="sale_note_annex.jasper";
                }
            
                oracle.binding.OperationBinding binding =ADFUtils.findOperation("fileNameForPrint");
                binding.getParamsMap().put("fileId","ERP0000001655");
                binding.execute();
                //*************End Find File name***********//
                System.out.println("Binding Result :"+binding.getResult().toString());
                if(binding.getResult() != null){
                    String result = binding.getResult().toString();
                    int last_index = result.lastIndexOf("/");
                    String path = result.substring(0,last_index+1)+file_name;
                    System.out.println("FILE PATH IS===>"+path);
                    InputStream input = new FileInputStream(path); 
                    DCIteratorBinding poIter = (DCIteratorBinding) getBindings().get("SearchDespatchAdviceHeaderVO1Iterator");
                    String daNum = poIter.getCurrentRow().getAttribute("DaNo").toString();
                    String unitCode = poIter.getCurrentRow().getAttribute("UnitCd").toString();
                    oracle.jbo.domain.Number AmdNo =
                    (oracle.jbo.domain.Number) poIter.getCurrentRow().getAttribute("DaAmdNo");
                System.out.println("Da Number :- "+daNum+"amd=="+AmdNo+" "+unitCode+"parameteeer"+poIter.getCurrentRow().getAttribute("InvParamTrans")+"bindinnnng"+salenoteparam.getValue());
                    Map n = new HashMap();
                    if(NoteMode.equals("SN"))
                    {
                    n.put("p_da_no", daNum);
                    n.put("p_unit", unitCode);
                    n.put("p_sp",salenoteparam.getValue());
                    }
                    if(NoteMode.equals("Amd"))
                    {
                        String LV_SID=oracle.jbo.server.uniqueid.UniqueIdHelper.getNextId().toString();
                        System.out.println("SID======>"+LV_SID);
                        oracle.adf.model.OperationBinding binding1 = (oracle.adf.model.OperationBinding) ADFUtils.findOperation("SaleAmendProcedure");
                        binding1.getParamsMap().put("PO",daNum);
                        binding1.getParamsMap().put("AMD",AmdNo);
                        binding1.getParamsMap().put("LVSID",LV_SID);
                        binding1.execute();
                        n.put("p_po_no",daNum);
                        n.put("p_amd", AmdNo);
                        n.put("p_sp",salenoteparam.getValue());
       }
                    if(NoteMode.equals("Annex"))
                    {
                    n.put("p_ap",salenoteparam.getValue());
                    }
                    conn = getConnection( );
                    
                    JasperReport design = (JasperReport) JRLoader.loadObject(input);
                    System.out.println("Path : " + input + " -------" + design+" param:"+n);
                    
                    @SuppressWarnings("unchecked")
                    net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                    byte pdf[] = JasperExportManager.exportReportToPdf(print);
                    HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                    response.getOutputStream().write(pdf);
                    response.getOutputStream().flush();
                    response.getOutputStream().close();
                    facesContext.responseComplete();
                    }else
                    System.out.println("File Name/File Path not found.");
                    } catch (FileNotFoundException fnfe) {
                    // TODO: Add catch code
                    fnfe.printStackTrace();
                    try {
                        System.out.println("in finally connection closed");
                                conn.close( );
                                conn = null;
                        
                    } catch( SQLException e ) {
                        e.printStackTrace( );
                    }
                    } catch (Exception e) {
                    // TODO: Add catch code
                    e.printStackTrace();
                    try {
                        System.out.println("in finally connection closed");
                                conn.close( );
                                conn = null;
                        
                    } catch( SQLException e1 ) {
                        e1.printStackTrace( );
                    }
                    }finally {
                            try {
                                    System.out.println("in finally connection closed");
                                            conn.close( );
                                            conn = null;
                                    
                            } catch( SQLException e ) {
                                    e.printStackTrace( );
                            }
                    }
                    }
    public Connection getConnection() throws Exception {
        Context ctx = null;
        Connection conn = null;
        ctx = new InitialContext();
        DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/ApplicationDBDS");
        conn = ds.getConnection();
        return conn;
    }


    public void setNoteMode(String NoteMode) {
        this.NoteMode = NoteMode;
    }

    public String getNoteMode() {
        return NoteMode;
    }

    public void setSalenoteparam(RichSelectOneChoice salenoteparam) {
        this.salenoteparam = salenoteparam;
    }

    public RichSelectOneChoice getSalenoteparam() {
        return salenoteparam;
    }
}
