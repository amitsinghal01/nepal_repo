package terms.mkt.transaction.ui.bean;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.event.PopupCanceledEvent;
import oracle.adf.view.rich.event.PopupFetchEvent;

import oracle.binding.OperationBinding;

public class SearchQuotationBean {
    private RichTable masterTable;

    public SearchQuotationBean() {
    }

    public void setMasterTable(RichTable masterTable) {
        this.masterTable = masterTable;
    }

    public RichTable getMasterTable() {
        return masterTable;
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        {
                   if(dialogEvent.getOutcome().name().equals("ok"))
                       {
                       OperationBinding op = null;
                       ADFUtils.findOperation("Delete").execute();
                       op = (OperationBinding) ADFUtils.findOperation("Commit");
                       Object rst = op.execute();
                       System.out.println("Record Delete Successfully");
                           if(op.getErrors().isEmpty()){
                               FacesMessage Message = new FacesMessage("Record Deleted Successfully");
        Message.setSeverity(FacesMessage.SEVERITY_INFO);
                               FacesContext fc = FacesContext.getCurrentInstance();
                               fc.addMessage(null, Message);
                           } else if (!op.getErrors().isEmpty())
                           {
                              OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
                              Object rstr = opr.execute();
                              FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
        Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                              FacesContext fc = FacesContext.getCurrentInstance();
                              fc.addMessage(null, Message);
                            }
                       }

        AdfFacesContext.getCurrentInstance().addPartialTarget(masterTable);

        }
        

    }
}
