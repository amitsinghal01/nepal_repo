package terms.mkt.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

public class VehicleEntryBean {
    private RichTable bindDetailTable;
    private RichPanelHeader getMyPageRootComponent;
    private RichOutputText getBindingOutputText;
    private RichInputText entryNoBinding;
    private RichButton editButtonBinding;
    private RichInputText entryNoDetailBinding;
    private RichInputComboboxListOfValues unitCodeBinding;

    public VehicleEntryBean() {
    }

    public void deleteRecordDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete").execute();
            System.out.println("Record Delete Successfully");
            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
        }

        AdfFacesContext.getCurrentInstance().addPartialTarget(bindDetailTable);
    }

    public void setBindDetailTable(RichTable bindDetailTable) {
        this.bindDetailTable = bindDetailTable;
    }

    public RichTable getBindDetailTable() {
        return bindDetailTable;
    }
    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }

    public void cevModeDisableComponent(String mode) {
        if (mode.equals("E")) {
            getEntryNoBinding().setDisabled(true);
            getEntryNoDetailBinding().setDisabled(true);
            getEditButtonBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
        }
        if (mode.equals("C")) {
            getEntryNoBinding().setDisabled(true);
            getEntryNoDetailBinding().setDisabled(true);
            getEditButtonBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
        }
        if (mode.equals("V")) {


        }

    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setGetBindingOutputText(RichOutputText getBindingOutputText) {
        this.getBindingOutputText = getBindingOutputText;
    }

    public RichOutputText getGetBindingOutputText() {
        cevmodecheck();
        return getBindingOutputText;
    }

    public void EditButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void saveAL(ActionEvent actionEvent) {
        OperationBinding op=(OperationBinding)ADFUtils.findOperation("generateEntryNoVES");
                Object obj= op.execute();
                System.out.println("result after function calling: "+op.getResult());
                if(op.getResult()!=null ){
                    if(op.getResult().equals("Y")){
                    ADFUtils.findOperation("Commit").execute();
                    ADFUtils.showMessage("Entry No Generated is "+entryNoBinding.getValue(), 2);
                    }
                    else{
                    ADFUtils.findOperation("Commit").execute();
                    ADFUtils.showMessage("Record Updated Successfully.", 2);
                    }
//                    ADFUtils.findOperation("CreateInsert1").execute();
                    }
    }

    public void setEntryNoBinding(RichInputText entryNoBinding) {
        this.entryNoBinding = entryNoBinding;
    }

    public RichInputText getEntryNoBinding() {
        return entryNoBinding;
    }

    public String saveAndCloseAL() {
        OperationBinding op=(OperationBinding)ADFUtils.findOperation("generateEntryNoVES");
                Object obj= op.execute();
                System.out.println("result after function calling: "+op.getResult());
                if(op.getResult()!=null ){
                    if(op.getResult().equals("Y")){
                    ADFUtils.findOperation("Commit").execute();
                    ADFUtils.showMessage("Entry No Generated is "+entryNoBinding.getValue(), 2);
                    }
                    else{
                    ADFUtils.findOperation("Commit").execute();
                    ADFUtils.showMessage("Record Updated Successfully.", 2);
                    }
                 return "SaveAndClose";
                    }
        return null;
    }

    public void setEditButtonBinding(RichButton editButtonBinding) {
        this.editButtonBinding = editButtonBinding;
    }

    public RichButton getEditButtonBinding() {
        return editButtonBinding;
    }

    public void setEntryNoDetailBinding(RichInputText entryNoDetailBinding) {
        this.entryNoDetailBinding = entryNoDetailBinding;
    }

    public RichInputText getEntryNoDetailBinding() {
        return entryNoDetailBinding;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }
}
