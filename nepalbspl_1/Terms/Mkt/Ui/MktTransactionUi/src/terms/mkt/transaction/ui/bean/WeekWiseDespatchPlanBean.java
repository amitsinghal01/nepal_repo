package terms.mkt.transaction.ui.bean;

import java.lang.reflect.Method;

import java.sql.Timestamp;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import java.util.Locale;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

public class WeekWiseDespatchPlanBean {
    private RichOutputText bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichButton detailcreateBinding;
    private RichInputText entryNoBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputDate entryDateBinding;
    private RichTable detailTableBinding;
    private RichInputText totalValueBinding;
    private RichInputText amendmentNumberBinding;
    private RichInputComboboxListOfValues productCodeBinding;
    private RichInputText week1Binding;
    private RichInputText week2Binding;
    private RichInputText week3Binding;
    private RichInputText week4Binding;
    private RichInputText scheduleForSecondMonthBinding;
    private RichInputText remarksBinding;
    private RichInputText getScheduleForFirstMonthBinding;
    private RichPopup confirmationPopupBinding;
    private RichInputDate amendmentDateBinding;
    private RichInputText yearBinding;
    private RichSelectOneChoice routeBinding;
    private RichSelectOneChoice monthBinding;
    private RichInputText despatchUptoDtlBinding;
    private RichInputText entryIdHDBinding;
    private RichInputComboboxListOfValues uomHdBinding;

    public WeekWiseDespatchPlanBean() {
    }

    public void editButtonAL(ActionEvent actionEvent) {
        System.out.println("In Action Listener");
        Integer amd=(Integer)getAmendmentNumberBinding().getValue();
        System.out.println("Amd Value"+getAmendmentNumberBinding().getValue());
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        getConfirmationPopupBinding().show(hints);
        if(amd != 0)
        {
            
            amdEnableDisable();          
        }
        else
        {           
            cevmodecheck();
        }
        
//        if(amd !=0)
//        {
//            amdEnableDisable();
//           
//        }
//        else
//        {
//            RichPopup.PopupHints hints = new RichPopup.PopupHints();
//            getConfirmationPopupBinding().show(hints);
//            cevmodecheck();
//        }
                    try {
                enabledValues();
            } catch (Exception e) {
                // TODO: Add catch code
                e.printStackTrace();
            }
                    
    }

    public void saveEntryNoAL(ActionEvent actionEvent) {

        System.out.println("Bean####");
        OperationBinding op = ADFUtils.findOperation("getEntryNoWeekDispatch");
        Object rst = op.execute();
        System.out.println("--------Commit-------");
        System.out.println("value aftr getting result--->?" + rst);


        if (rst != null && rst.toString() != "") {
            if (op.getErrors().isEmpty()) {
                System.out.println("ENTRY ID SAVE #### " + entryIdHDBinding.getValue());
                ADFUtils.findOperation("weekWiseDetailIdAndInvQty").execute();
                ADFUtils.findOperation("Commit").execute();
//                ADFUtils.findOperation("weekWiseDetailIdAndInvQty").execute();
                FacesMessage Message = new FacesMessage("Record Saved Successfully. New Entry No is " + rst + ".");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);

            }
        }

        if (rst == null || rst.toString() == "" || rst.toString() == " ") {
            if (op.getErrors().isEmpty()) {
                ADFUtils.findOperation("Commit").execute();
                FacesMessage Message = new FacesMessage("Record Update Successfully.");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);

            }


        }
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete2").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }
        else {
            entryDateBinding.setDisabled(true);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(detailTableBinding);
            
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setDetailcreateBinding(RichButton detailcreateBinding) {
        this.detailcreateBinding = detailcreateBinding;
    }

    public RichButton getDetailcreateBinding() {
        return detailcreateBinding;
    }



    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }


    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }

    //Set Fields and Button disable.
    public void cevModeDisableComponent(String mode) {

        if (mode.equals("E")) {
//            getMonthBinding().setDisabled(true);
//            getYearBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            getDetailcreateBinding().setDisabled(false);
            getUnitCodeBinding().setDisabled(true);
            getEntryNoBinding().setDisabled(true);
            getEntryDateBinding().setDisabled(true);
            getTotalValueBinding().setDisabled(true);
            getAmendmentNumberBinding().setDisabled(true);
            getAmendmentDateBinding().setDisabled(true);
            getDespatchUptoDtlBinding().setDisabled(true);
            getUomHdBinding().setDisabled(true);
            try {
            enabledValues();
            } catch (Exception e) {
            // TODO: Add catch code
            e.printStackTrace();
            }
            
        } else if (mode.equals("C")) {
//            getMonthBinding().setDisabled(true);
//            getYearBinding().setDisabled(true);
            getDetailcreateBinding().setDisabled(false);
            getUnitCodeBinding().setDisabled(true);
            getEntryNoBinding().setDisabled(true);
            getTotalValueBinding().setDisabled(true);
            getAmendmentNumberBinding().setDisabled(true);
            getAmendmentDateBinding().setDisabled(true);
            getDespatchUptoDtlBinding().setDisabled(true);
            getUomHdBinding().setDisabled(true);
        } else if (mode.equals("V")) {
//            getDetailcreateBinding().setDisabled(true);
        }

    }

    public void setEntryNoBinding(RichInputText entryNoBinding) {
        this.entryNoBinding = entryNoBinding;
    }

    public RichInputText getEntryNoBinding() {
        return entryNoBinding;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setEntryDateBinding(RichInputDate entryDateBinding) {
        this.entryDateBinding = entryDateBinding;
    }

    public RichInputDate getEntryDateBinding() {
        return entryDateBinding;
    }

    public void setDetailTableBinding(RichTable detailTableBinding) {
        this.detailTableBinding = detailTableBinding;
    }

    public RichTable getDetailTableBinding() {
        return detailTableBinding;
    }

    public void setTotalValueBinding(RichInputText totalValueBinding) {
        this.totalValueBinding = totalValueBinding;
    }

    public RichInputText getTotalValueBinding() {
        return totalValueBinding;
    }

    public void amendmentWeeklyDespatchDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
        {
                oracle.binding.OperationBinding op1=ADFUtils.findOperation("checkMaxAmendmentWeeklyDespachPlan");
                           Object ob=op1.execute();
                           if(op1.getResult().toString()!=null && op1.getResult().toString().equalsIgnoreCase("N")){
                               
                               ADFUtils.showMessage("You Can Update Latest Amendment Only.", 2);
                               ADFUtils.setEL("#{pageFlowScope.mode}", "V");
            
                           }
                else{
            
            System.out.println("Click On OK");
            OperationBinding op=ADFUtils.findOperation("getAmendmentForWeeklyDespatch");
            op.getParamsMap().put("entryid",ADFUtils.resolveExpression("#{pageFlowScope.EntryId}"));
            op.getParamsMap().put("entryNo",ADFUtils.resolveExpression("#{pageFlowScope.EntryNo}"));
            op.getParamsMap().put("amdNo",ADFUtils.resolveExpression("#{pageFlowScope.AmdNo}"));             
            op.execute();
            amdEnableDisable();
            try {
            enabledValues();
            } catch (Exception e) {
            // TODO: Add catch code
            e.printStackTrace();
            }
            ADFUtils.findOperation("getDespatchUptoForWeekWisePlan").execute();                   
            AdfFacesContext.getCurrentInstance().addPartialTarget(detailTableBinding);
                }
            }
        else{
                getConfirmationPopupBinding().hide();
                }
        
    }
    
    public void amdEnableDisable()
    {
      getWeek1Binding().setDisabled(false);
      getWeek2Binding().setDisabled(false);
      getWeek3Binding().setDisabled(false);
      getWeek4Binding().setDisabled(false);
      getRemarksBinding().setDisabled(false);
      getRouteBinding().setDisabled(false);
      getScheduleForSecondMonthBinding().setDisabled(false);
      getGetScheduleForFirstMonthBinding().setDisabled(false);
      getProductCodeBinding().setDisabled(false);
      getAmendmentNumberBinding().setDisabled(true);
      getAmendmentDateBinding().setDisabled(true);
        try {
        enabledValues();
        } catch (Exception e) {
        // TODO: Add catch code
        e.printStackTrace();
        }
    }

    public void setAmendmentNumberBinding(RichInputText amendmentNumberBinding) {
        this.amendmentNumberBinding = amendmentNumberBinding;
    }

    public RichInputText getAmendmentNumberBinding() {
        return amendmentNumberBinding;
    }

    public void setProductCodeBinding(RichInputComboboxListOfValues productCodeBinding) {
        this.productCodeBinding = productCodeBinding;
    }

    public RichInputComboboxListOfValues getProductCodeBinding() {
        return productCodeBinding;
    }

    public void setWeek1Binding(RichInputText week1Binding) {
        this.week1Binding = week1Binding;
    }

    public RichInputText getWeek1Binding() {
        return week1Binding;
    }

    public void setWeek2Binding(RichInputText week2Binding) {
        this.week2Binding = week2Binding;
    }

    public RichInputText getWeek2Binding() {
        return week2Binding;
    }

    public void setWeek3Binding(RichInputText week3Binding) {
        this.week3Binding = week3Binding;
    }

    public RichInputText getWeek3Binding() {
        return week3Binding;
    }

    public void setWeek4Binding(RichInputText week4Binding) {
        this.week4Binding = week4Binding;
    }

    public RichInputText getWeek4Binding() {
        return week4Binding;
    }


    public void setScheduleForSecondMonthBinding(RichInputText scheduleForSecondMonthBinding) {
        this.scheduleForSecondMonthBinding = scheduleForSecondMonthBinding;
    }

    public RichInputText getScheduleForSecondMonthBinding() {
        return scheduleForSecondMonthBinding;
    }

    public void setRemarksBinding(RichInputText remarksBinding) {
        this.remarksBinding = remarksBinding;
    }

    public RichInputText getRemarksBinding() {
        return remarksBinding;
    }

    public void setGetScheduleForFirstMonthBinding(RichInputText getScheduleForFirstMonthBinding) {
        this.getScheduleForFirstMonthBinding = getScheduleForFirstMonthBinding;
    }

    public RichInputText getGetScheduleForFirstMonthBinding() {
        return getScheduleForFirstMonthBinding;
    }

    public void setConfirmationPopupBinding(RichPopup confirmationPopupBinding) {
        this.confirmationPopupBinding = confirmationPopupBinding;
    }

    public RichPopup getConfirmationPopupBinding() {
        return confirmationPopupBinding;
    }

    public void setAmendmentDateBinding(RichInputDate amendmentDateBinding) {
        this.amendmentDateBinding = amendmentDateBinding;
    }

    public RichInputDate getAmendmentDateBinding() {
        return amendmentDateBinding;
    }

    public void setYearBinding(RichInputText yearBinding) {
        this.yearBinding = yearBinding;
    }

    public RichInputText getYearBinding() {
        return yearBinding;
    }

    public void setRouteBinding(RichSelectOneChoice routeBinding) {
        this.routeBinding = routeBinding;
    }

    public RichSelectOneChoice getRouteBinding() {
        return routeBinding;
    }
    
    public void enabledValues()
        { 
            try
                    {       
                    if (yearBinding.getValue() != null && monthBinding.getValue() != null) {
                    String str_date = "01-" +monthBinding.getValue()+ "-" +yearBinding.getValue();
                    System.out.println("STR DATE==>" + str_date);
                    DateFormat date_format = new SimpleDateFormat("dd-MMM-yyyy");
                    java.util.Date date = date_format.parse(str_date);
                    System.out.println("After convert date is====>"+date);
                    System.out.println("GET ENTR Y DATE"+entryDateBinding.getValue());               
                    
                    oracle.jbo.domain.Date entryDate =(oracle.jbo.domain.Date)entryDateBinding.getValue();
                    String str_entry = entryDate.toString();
                    
                    str_entry=str_entry.substring(0, 10);
                    Calendar cal = GregorianCalendar.getInstance();
                    cal.setTime(date);
                    DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    

                    //            String currentMonthAsSting = df.format(cal.getTime());
                    
                    //For First Next Month
                    cal.set(Calendar.MONTH, cal.get(Calendar.MONTH) + 0);
                    String next_month = df.format(cal.getTime());
                    System.out.println("Current Month " + next_month);
                    

                        next_month = next_month.substring(0,7);
                        str_entry =str_entry.substring(0,7);
                        System.out.println("NEXT MONTH DATE===>"+next_month);
                        System.out.println("ENTRY DATE DATE===>"+str_entry);
                        
                        if(!next_month.equals(str_entry))
                        {
                            getWeek1Binding().setDisabled(false);
                            getWeek2Binding().setDisabled(false);
                            getWeek3Binding().setDisabled(false);
                            getWeek4Binding().setDisabled(false);
                        }
                        else
                        {
                        try {
                            oracle.jbo.domain.Date entryDatetry =(oracle.jbo.domain.Date)entryDateBinding.getValue();
                            String str_entry1 = entryDatetry.toString();
                            
                            
                            SimpleDateFormat df1=new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                            System.out.println("df1 ####" + df1);
                            df1.parse(str_entry1, new ParsePosition(0));
                            
                            java.util.Date date1 =df1.parse(str_entry1);
                            System.out.println("date1 ####" + date1);

                            Timestamp entryDt1 = new Timestamp(date1.getTime());
                            System.out.println("entryDt1 ####" + entryDt1);

                            
                            
                           // Timestamp entryDt1 = new Timestamp(System.currentTimeMillis());
                            java.util.Date date_curr = new java.util.Date();
                            date_curr.setTime(entryDt1.getTime());

                            String str_date1 = new SimpleDateFormat("dd-MMM-yyyy").format(date_curr);

                            //                       String str_date = getAttributeInternal(ENTRYDT);
                            System.out.println("STR DATE==>" + str_date1);
                            //               String format = "dd-mm-yyyy";

                            DateFormat new_df = new SimpleDateFormat("dd-MMM-yyyy");
                            java.util.Date date_new = new_df.parse(str_date1);

                            Calendar cal_new = Calendar.getInstance();
                            cal_new.setTime(date_new);

                            cal_new.set(Calendar.MONTH, cal_new.get(Calendar.MONTH));
                            String current_month1 = new_df.format(cal_new.getTime());
                            System.out.println("CURRENT MONTH #####" + current_month1);

                            int week_no = cal_new.get(Calendar.DAY_OF_MONTH);
                            System.out.println("week_no " + week_no);
                            if(week_no==1 || week_no==2 || week_no==3 || week_no==4 || week_no==5 || week_no==6 || week_no==7)
                            {
                                getWeek1Binding().setDisabled(false);
                                getWeek2Binding().setDisabled(false);
                                getWeek3Binding().setDisabled(false);
                                getWeek4Binding().setDisabled(false);
                            }
                            else if(week_no==8 || week_no==9 || week_no==10 || week_no==11 || week_no==12 || week_no==13 || week_no==14)
                            {
                                getWeek1Binding().setDisabled(true);
                                getWeek2Binding().setDisabled(false);
                                getWeek3Binding().setDisabled(false);
                                getWeek4Binding().setDisabled(false);
                            }
                            else if(week_no==15 || week_no==16 || week_no==17 || week_no==18 || week_no==19 || week_no==20 || week_no==21)
                            {
                                getWeek1Binding().setDisabled(true);
                                getWeek2Binding().setDisabled(true);
                                getWeek3Binding().setDisabled(false);
                                getWeek4Binding().setDisabled(false);
                            }
                            else if(week_no>=22)
                            {
                                getWeek1Binding().setDisabled(true);
                                getWeek2Binding().setDisabled(true);
                                getWeek3Binding().setDisabled(true);
                                getWeek4Binding().setDisabled(false);
                            
                            }
                            
                            
                            System.out.println("CURRENT WEEK #### " + cal_new.get(Calendar.WEEK_OF_MONTH));
                        } catch (ParseException pe) {
                        // TODO: Add catch code
                        pe.printStackTrace();
                        }
                        
                        
                        }

                    
        //                oracle.jbo.domain.Date popDate =new oracle.jbo.domain.Date(next_month);
        //                oracle.jbo.domain.Date entry_date =new oracle.jbo.domain.Date(str_entry);
                    }
                    
                    }
                    catch(ParseException pe)
                    {
                    // TODO: Add catch code
                    pe.printStackTrace();
                    }
            }
    
    public void setMonthBinding(RichSelectOneChoice monthBinding) {
        this.monthBinding = monthBinding;
    }

    public RichSelectOneChoice getMonthBinding() {
        return monthBinding;
    }

    public void createInsertAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert").execute();
        enabledValues();
    }

    public void productVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        ADFUtils.findOperation("getDespatchUptoForWeekWisePlan").execute();
    }

    public void routeVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        ADFUtils.findOperation("getDespatchUptoForWeekWisePlan").execute();
    }

    public void entryDateVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        ADFUtils.findOperation("getDespatchUptoForWeekWisePlan").execute();
        enabledValues();
        ADFUtils.findOperation("weekWiseDetailIdAndInvQty").execute();
        AdfFacesContext.getCurrentInstance().addPartialTarget(detailTableBinding);
//        enableDisableEntryDate();
//     //   ADFUtils.findOperation("weekWiseDetailIdAndInvQty").execute();
//        System.out.println("ENTRY ID HEADER #### " + entryIdHDBinding.getValue());
////        ADFUtils.findOperation("weekWiseDetailIdAndInvQty").execute();
    }

    public void setDespatchUptoDtlBinding(RichInputText despatchUptoDtlBinding) {
        this.despatchUptoDtlBinding = despatchUptoDtlBinding;
    }

    public RichInputText getDespatchUptoDtlBinding() {
        return despatchUptoDtlBinding;
    }

    public void setEntryIdHDBinding(RichInputText entryIdHDBinding) {
        this.entryIdHDBinding = entryIdHDBinding;
    }

    public RichInputText getEntryIdHDBinding() {
        return entryIdHDBinding;
    }

    public void setUomHdBinding(RichInputComboboxListOfValues uomHdBinding) {
        this.uomHdBinding = uomHdBinding;
    }

    public RichInputComboboxListOfValues getUomHdBinding() {
        return uomHdBinding;
    }
    

}
