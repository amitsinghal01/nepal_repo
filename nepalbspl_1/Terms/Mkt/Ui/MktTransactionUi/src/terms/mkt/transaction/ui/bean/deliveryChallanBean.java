package terms.mkt.transaction.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.sql.Timestamp;

import java.util.Calendar;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import javax.faces.validator.ValidatorException;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;

import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;

import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import terms.mkt.transaction.ui.bean.ADFUtils;

public class deliveryChallanBean {
    private RichInputDate rettargetDate;
    private RichInputDate retTargetDateBinding;
    private RichInputComboboxListOfValues getPartyCdBinding;
    private RichSelectOneChoice getExitTypeBinding;
    private RichTable gatePassChallanDetailTableBinding;
    private RichOutputText bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichButton detailcreateBinding;
    private RichButton detaildeleteBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText challanNumberBinding;
    private RichInputDate challanDateBinding;
    private RichSelectOneChoice challanTypeBinding;
    private RichSelectOneChoice exitTypeBinding;
    private RichSelectOneChoice statusBinding;
    private RichInputText gateExitNumberBinding;
    private RichInputDate gateExitDateBinding;
    private RichInputComboboxListOfValues respDepartmentBinding;
    private RichInputText departmentDescriptionBinding;
    private RichInputComboboxListOfValues respEmployeeBinding;
    private RichInputComboboxListOfValues partyCodeBinding;
    private RichSelectOneChoice deleiveryModeBinding;
    private RichInputComboboxListOfValues preparedByBinding;
    private RichInputText forProcessRemarksBinding;
    private RichInputText documentTypeBinding;
    private RichInputDate documentDateBinding;
    private RichInputDate e57F3DateBinding;
    private RichInputText uomBinding;

    private RichInputText partyNameBinding;
    private RichInputText transporterNameBinding;
    private RichInputText preparedByBNameBinding;
    private RichInputText respEmployeeNameBinding;
    private RichTable gatePassTableBinding;
    private RichInputText unitNameBinding;
    private RichInputText itemDescBinding;
    private RichInputText rateBinding;
    private RichInputText qtyBinding;
    private RichInputText convQtyBinding;
    private RichInputComboboxListOfValues docNoBinding;
    private RichInputComboboxListOfValues itemCdBinding;
    private RichInputText hsnNumberBinding;
    private RichInputText sgstBinding;
    private RichInputText cgstBinding;
    private RichInputText igstBinding;
    private RichInputText igstAmtBinding;
    private RichInputText cgstAmtBinding;
    private RichInputText sgstAmtBinding;
    private RichInputText amountBinding;

    public deliveryChallanBean() {
    }


    public void saveChallanNoAL(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedBy("DeliverChallanHeaderVO1Iterator","LastUpdatedBy");
        OperationBinding op1 = ADFUtils.findOperation("validateQty");
        op1.execute();
        System.out.println("OP VALUE==>" + op1.getResult());
        if (op1.getResult().equals("Y")) {
            FacesMessage Message = new FacesMessage("Only for Balance Qty Challan Qty can be made.");
            Message.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
        } else {
            ADFUtils.findOperation("setProcessCodeValueGPC").execute();
            OperationBinding op = ADFUtils.findOperation("generateChallanNo");
            Object rst = op.execute();
            //ADFUtils.findOperation("goForUnitRate").execute();
            System.out.println("OperationBinding op------> " + op);
            System.out.println("Object rst-----> " + rst);
            System.out.println("--------Commit-------");
            System.out.println("value aftr getting result--->?" + rst);

            System.out.println("Result is==> " + op.getResult());
            if (op.getResult() != null && op.getResult() != "") {
                if (op.getErrors().isEmpty()) {
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message =
                        new FacesMessage("Record Saved Successfully. New Challan No is " + rst + ".");
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);
                }
            }


            if (op.getResult() == null || op.getResult() == "" || op.getResult() == " ") {
                if (op.getErrors().isEmpty()) {
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message = new FacesMessage("Record Update Successfully.");
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);

                }


            }
        }
    }


    public void setChallanNoAL(ActionEvent actionEvent) { //genrateSeqNoGPC
        ADFUtils.findOperation("CreateInsert1").execute();
        ADFUtils.findOperation("setChallanNo").execute();
        //   ADFUtils.findOperation("genrateSeqNoGPC").execute();

        // Add event code here...
    }

    public void gateExitTypeVCL(ValueChangeEvent VCE) {
        if (VCE.getNewValue() != null && VCE.getNewValue().equals("R")) {

            //java.sql.Timestamp ts=(java.sql.Timestamp) challanDateBinding.getValue();

            // java.sql.Timestamp ts=new java.sql.Timestamp(System.currentTimeMillis());
            java.sql.Timestamp ts = new java.sql.Timestamp(System.currentTimeMillis());
            System.out.println("ts" + ts);
            Calendar cal = Calendar.getInstance();
            cal.setTime(ts);
            cal.add(Calendar.DAY_OF_WEEK, 7);
            ts.setTime(cal.getTime().getTime()); // or
            ts = new Timestamp(cal.getTime().getTime());
            System.out.println("Value of Timestamp is=> " + ts);
            retTargetDateBinding.setValue(ts);
        }
    }


    public void setRetTargetDateBinding(RichInputDate retTargetDateBinding) {
        this.retTargetDateBinding = retTargetDateBinding;
    }

    public RichInputDate getRetTargetDateBinding() {
        return retTargetDateBinding;
    }


    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete").execute();
            //ADFUtils.findOperation("Commit").execute();    //Unable on Search page.
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }

        AdfFacesContext.getCurrentInstance().addPartialTarget(gatePassChallanDetailTableBinding);


    }

    public void setGatePassChallanDetailTableBinding(RichTable gatePassChallanDetailTableBinding) {
        this.gatePassChallanDetailTableBinding = gatePassChallanDetailTableBinding;
    }

    public RichTable getGatePassChallanDetailTableBinding() {
        return gatePassChallanDetailTableBinding;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setDetailcreateBinding(RichButton detailcreateBinding) {
        this.detailcreateBinding = detailcreateBinding;
    }

    public RichButton getDetailcreateBinding() {
        return detailcreateBinding;
    }

    public void setDetaildeleteBinding(RichButton detaildeleteBinding) {
        this.detaildeleteBinding = detaildeleteBinding;
    }

    public RichButton getDetaildeleteBinding() {
        return detaildeleteBinding;
    }

    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }


    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }


    //Set Fields and Button disable.
    public void cevModeDisableComponent(String mode) {

        if (mode.equals("E")) {
            getHeaderEditBinding().setDisabled(true);
            getDetailcreateBinding().setDisabled(false);
            //getDetaildeleteBinding().setDisabled(false);
            // getUnitCodeBinding().setDisabled(true);
            getChallanNumberBinding().setDisabled(true);
            getChallanDateBinding().setDisabled(true);
            getChallanTypeBinding().setDisabled(true);
            getExitTypeBinding().setDisabled(true);
            getStatusBinding().setDisabled(true);
            getGateExitNumberBinding().setDisabled(true);
            getGateExitDateBinding().setDisabled(true);
            getRetTargetDateBinding().setDisabled(true);
            getRespDepartmentBinding().setDisabled(true);
            getDepartmentDescriptionBinding().setDisabled(true);
            getRespEmployeeBinding().setDisabled(true);
            getRespEmployeeNameBinding().setDisabled(true);
            getPartyNameBinding().setDisabled(true);
            getPreparedByBNameBinding().setDisabled(true);
            getTransporterNameBinding().setDisabled(true);
            getPreparedByBinding().setDisabled(true);
            getDocumentTypeBinding().setDisabled(true);
            getDocumentDateBinding().setDisabled(true);
            getE57F3DateBinding().setDisabled(true);
            getUomBinding().setDisabled(true);
            getUnitNameBinding().setDisabled(true);
            getItemDescBinding().setDisabled(true);
            getRateBinding().setDisabled(true);
            getConvQtyBinding().setDisabled(true);
            getHsnNumberBinding().setDisabled(true);
            getCgstBinding().setDisabled(true);
            getSgstBinding().setDisabled(true);
            getIgstBinding().setDisabled(true);
            getCgstAmtBinding().setDisabled(true);
            getIgstAmtBinding().setDisabled(true);
            getSgstAmtBinding().setDisabled(true);

        } else if (mode.equals("C")) {
            getHeaderEditBinding().setDisabled(true);
            getDetailcreateBinding().setDisabled(false);
            // getDetaildeleteBinding().setDisabled(false);
            getUnitCodeBinding().setDisabled(true);
            getPreparedByBinding().setDisabled(true);
            getUnitNameBinding().setDisabled(true);
            getDocumentTypeBinding().setDisabled(true);
            getDocumentDateBinding().setDisabled(true);
            getChallanNumberBinding().setDisabled(true);
            getChallanDateBinding().setDisabled(true);
            getStatusBinding().setDisabled(true);
            getGateExitDateBinding().setDisabled(true);
            getGateExitNumberBinding().setDisabled(true);
            getPartyNameBinding().setDisabled(true);
            getDepartmentDescriptionBinding().setDisabled(true);
            getRespEmployeeNameBinding().setDisabled(true);
            getPreparedByBNameBinding().setDisabled(true);
            getTransporterNameBinding().setDisabled(true);
            getItemDescBinding().setDisabled(true);
            getUomBinding().setDisabled(true);
            getE57F3DateBinding().setDisabled(true);
            getRateBinding().setDisabled(true);
            getConvQtyBinding().setDisabled(true);
            getHsnNumberBinding().setDisabled(true);
            getCgstBinding().setDisabled(true);
            getSgstBinding().setDisabled(true);
            getIgstBinding().setDisabled(true);
            getCgstAmtBinding().setDisabled(true);
            getIgstAmtBinding().setDisabled(true);
            getSgstAmtBinding().setDisabled(true);

        } else if (mode.equals("V")) {
            getDetailcreateBinding().setDisabled(true);
            //   getDetaildeleteBinding().setDisabled(true);
        }

    }


    public void editButtonAL(ActionEvent actionEvent) {
        System.out.println("Exit Date" + statusBinding.getValue());
        if (statusBinding.getValue().toString().equals("E")) {
            ADFUtils.showMessage("Exited Gate Pass Challlan cannot be edited further.", 2);
            ADFUtils.setEL("#{pageFlowScope.mode}", "V");
        } else {
            cevmodecheck();
        }
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setChallanNumberBinding(RichInputText challanNumberBinding) {
        this.challanNumberBinding = challanNumberBinding;
    }

    public RichInputText getChallanNumberBinding() {
        return challanNumberBinding;
    }

    public void setChallanDateBinding(RichInputDate challanDateBinding) {
        this.challanDateBinding = challanDateBinding;
    }

    public RichInputDate getChallanDateBinding() {
        return challanDateBinding;
    }

    public void setChallanTypeBinding(RichSelectOneChoice challanTypeBinding) {
        this.challanTypeBinding = challanTypeBinding;
    }

    public RichSelectOneChoice getChallanTypeBinding() {
        return challanTypeBinding;
    }

    public void setExitTypeBinding(RichSelectOneChoice exitTypeBinding) {
        this.exitTypeBinding = exitTypeBinding;
    }

    public RichSelectOneChoice getExitTypeBinding() {
        return exitTypeBinding;
    }

    public void setStatusBinding(RichSelectOneChoice statusBinding) {
        this.statusBinding = statusBinding;
    }

    public RichSelectOneChoice getStatusBinding() {
        return statusBinding;
    }

    public void setGateExitNumberBinding(RichInputText gateExitNumberBinding) {
        this.gateExitNumberBinding = gateExitNumberBinding;
    }

    public RichInputText getGateExitNumberBinding() {
        return gateExitNumberBinding;
    }

    public void setGateExitDateBinding(RichInputDate gateExitDateBinding) {
        this.gateExitDateBinding = gateExitDateBinding;
    }

    public RichInputDate getGateExitDateBinding() {
        return gateExitDateBinding;
    }

    public void setRespDepartmentBinding(RichInputComboboxListOfValues respDepartmentBinding) {
        this.respDepartmentBinding = respDepartmentBinding;
    }

    public RichInputComboboxListOfValues getRespDepartmentBinding() {
        return respDepartmentBinding;
    }

    public void setDepartmentDescriptionBinding(RichInputText departmentDescriptionBinding) {
        this.departmentDescriptionBinding = departmentDescriptionBinding;
    }

    public RichInputText getDepartmentDescriptionBinding() {
        return departmentDescriptionBinding;
    }

    public void setRespEmployeeBinding(RichInputComboboxListOfValues respEmployeeBinding) {
        this.respEmployeeBinding = respEmployeeBinding;
    }

    public RichInputComboboxListOfValues getRespEmployeeBinding() {
        return respEmployeeBinding;
    }

    public void setPartyCodeBinding(RichInputComboboxListOfValues partyCodeBinding) {
        this.partyCodeBinding = partyCodeBinding;
    }

    public RichInputComboboxListOfValues getPartyCodeBinding() {
        return partyCodeBinding;
    }

    public void setDeleiveryModeBinding(RichSelectOneChoice deleiveryModeBinding) {
        this.deleiveryModeBinding = deleiveryModeBinding;
    }

    public RichSelectOneChoice getDeleiveryModeBinding() {
        return deleiveryModeBinding;
    }

    public void setPreparedByBinding(RichInputComboboxListOfValues preparedByBinding) {
        this.preparedByBinding = preparedByBinding;
    }

    public RichInputComboboxListOfValues getPreparedByBinding() {
        return preparedByBinding;
    }

    public void setForProcessRemarksBinding(RichInputText forProcessRemarksBinding) {
        this.forProcessRemarksBinding = forProcessRemarksBinding;
    }

    public RichInputText getForProcessRemarksBinding() {
        return forProcessRemarksBinding;
    }

    public void setDocumentTypeBinding(RichInputText documentTypeBinding) {
        this.documentTypeBinding = documentTypeBinding;
    }

    public RichInputText getDocumentTypeBinding() {
        return documentTypeBinding;
    }

    public void setDocumentDateBinding(RichInputDate documentDateBinding) {
        this.documentDateBinding = documentDateBinding;
    }

    public RichInputDate getDocumentDateBinding() {
        return documentDateBinding;
    }

    public void setE57F3DateBinding(RichInputDate e57F3DateBinding) {
        this.e57F3DateBinding = e57F3DateBinding;
    }

    public RichInputDate getE57F3DateBinding() {
        return e57F3DateBinding;
    }

    public void setUomBinding(RichInputText uomBinding) {
        this.uomBinding = uomBinding;
    }

    public RichInputText getUomBinding() {
        return uomBinding;
    }


    public void setPartyNameBinding(RichInputText partyNameBinding) {
        this.partyNameBinding = partyNameBinding;
    }

    public RichInputText getPartyNameBinding() {
        return partyNameBinding;
    }

    public void setTransporterNameBinding(RichInputText transporterNameBinding) {
        this.transporterNameBinding = transporterNameBinding;
    }

    public RichInputText getTransporterNameBinding() {
        return transporterNameBinding;
    }

    public void setPreparedByBNameBinding(RichInputText preparedByBNameBinding) {
        this.preparedByBNameBinding = preparedByBNameBinding;
    }

    public RichInputText getPreparedByBNameBinding() {
        return preparedByBNameBinding;
    }

    public void setRespEmployeeNameBinding(RichInputText respEmployeeNameBinding) {
        this.respEmployeeNameBinding = respEmployeeNameBinding;
    }

    public RichInputText getRespEmployeeNameBinding() {
        return respEmployeeNameBinding;
    }

    public void setGatePassTableBinding(RichTable gatePassTableBinding) {
        this.gatePassTableBinding = gatePassTableBinding;
    }

    public RichTable getGatePassTableBinding() {
        return gatePassTableBinding;
    }

    public void deletepopupdialog(DialogEvent dialogEvent) {
        {
            if (dialogEvent.getOutcome().name().equals("ok")) {
                ADFUtils.findOperation("Delete").execute();
                //ADFUtils.findOperation("Commit").execute();    //Unable on Search page.
                System.out.println("Record Delete Successfully");

                FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);

            }

            AdfFacesContext.getCurrentInstance().addPartialTarget(gatePassTableBinding);

        }
    }

    public void detailCreateAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert").execute();
        getChallanTypeBinding().setDisabled(true);
        getPartyCodeBinding().setDisabled(true);
        getForProcessRemarksBinding().setDisabled(true);
    }

    public void setUnitNameBinding(RichInputText unitNameBinding) {
        this.unitNameBinding = unitNameBinding;
    }

    public RichInputText getUnitNameBinding() {
        return unitNameBinding;
    }

    public void setItemDescBinding(RichInputText itemDescBinding) {
        this.itemDescBinding = itemDescBinding;
    }

    public RichInputText getItemDescBinding() {
        return itemDescBinding;
    }

    public void clearSelectedRowDocNo(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce != null) {

            if (!vce.getNewValue().equals(vce.getOldValue())) {


                ADFUtils.findOperation("clearSelectedRowGatePassChallan").execute();


            }
        }
    }

    public void setRateBinding(RichInputText rateBinding) {
        this.rateBinding = rateBinding;
    }

    public RichInputText getRateBinding() {
        return rateBinding;
    }

    public void QuantityVCL(ValueChangeEvent vcl) {
        vcl.getComponent().processUpdates(FacesContext.getCurrentInstance());
        System.out.println("RATE" + rateBinding.getValue());
        if (vcl.getNewValue() != null && rateBinding.getValue() != null) {
            BigDecimal rate = (BigDecimal) rateBinding.getValue();
            BigDecimal qty = (BigDecimal) vcl.getNewValue();
            BigDecimal amt = qty.multiply(rate);
            BigDecimal sgst = (BigDecimal) sgstBinding.getValue();
            BigDecimal cgst = (BigDecimal) cgstBinding.getValue();
            BigDecimal igst = (BigDecimal) igstBinding.getValue();
            System.out.println("AMT");
            BigDecimal per = new BigDecimal(100);
            BigDecimal sgstAmt = new BigDecimal(0);
            BigDecimal cgstAmt = new BigDecimal(0);
            BigDecimal igstAmt = new BigDecimal(0);
            if (sgst != null && cgst != null) {
                sgstAmt = (amt.multiply(sgst)).divide(per);
                cgstAmt = (amt.multiply(cgst)).divide(per);
                System.out.println("CGST AMT" + cgstAmt + " SGST AMT" + sgstAmt);
                sgstAmtBinding.setValue(sgstAmt);
                cgstAmtBinding.setValue(cgstAmt);
            } else if (igst != null) {
                igstAmt = (amt.multiply(igst)).divide(per);
                System.out.println("IGST AMT" + igstAmt);
                igstAmtBinding.setValue(igstAmt);
            }
        }
        if (vcl != null && docNoBinding.getValue() != null) {
            BigDecimal oldQty = (BigDecimal) vcl.getOldValue();
            BigDecimal newQty = (BigDecimal) vcl.getNewValue();
            if (newQty.compareTo(oldQty) == 1) {
                FacesMessage msg =
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Only for Balance Qty Challan Qty can be made.",
                                     null);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, msg);
            }
        }

    }

    public void setQtyBinding(RichInputText qtyBinding) {
        this.qtyBinding = qtyBinding;
    }

    public RichInputText getQtyBinding() {
        return qtyBinding;
    }

    public void setConvQtyBinding(RichInputText convQtyBinding) {
        this.convQtyBinding = convQtyBinding;
    }

    public RichInputText getConvQtyBinding() {
        return convQtyBinding;
    }

    public void setDocNoBinding(RichInputComboboxListOfValues docNoBinding) {
        this.docNoBinding = docNoBinding;
    }

    public RichInputComboboxListOfValues getDocNoBinding() {
        return docNoBinding;
    }

    public void amountValidator(FacesContext facesContext, UIComponent uIComponent, Object ob) {
        BigDecimal val = new BigDecimal(0);
        BigDecimal amt = (BigDecimal) ob;
        if (amt != null) {
            if (amt.compareTo(val) == 0 || amt.compareTo(val) == -1) {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                              "Amount must be greater than 0.", null));
            }
        }
    }

    public void setHsnNumberBinding(RichInputText hsnNumberBinding) {
        this.hsnNumberBinding = hsnNumberBinding;
    }

    public RichInputText getHsnNumberBinding() {
        return hsnNumberBinding;
    }

    public void setSgstBinding(RichInputText sgstBinding) {
        this.sgstBinding = sgstBinding;
    }

    public RichInputText getSgstBinding() {
        return sgstBinding;
    }

    public void setCgstBinding(RichInputText cgstBinding) {
        this.cgstBinding = cgstBinding;
    }

    public RichInputText getCgstBinding() {
        return cgstBinding;
    }

    public void setIgstBinding(RichInputText igstBinding) {
        this.igstBinding = igstBinding;
    }

    public RichInputText getIgstBinding() {
        return igstBinding;
    }

    public void AmountVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        System.out.println("NEW VALUE" + vce.getNewValue());
        if (vce.getNewValue() != null) {
            BigDecimal amt = (BigDecimal) vce.getNewValue();
            BigDecimal sgst = (BigDecimal) sgstBinding.getValue();
            BigDecimal cgst = (BigDecimal) cgstBinding.getValue();
            BigDecimal igst = (BigDecimal) igstBinding.getValue();
            BigDecimal per = new BigDecimal(100);
            BigDecimal sgstAmt = new BigDecimal(0);
            BigDecimal cgstAmt = new BigDecimal(0);
            BigDecimal igstAmt = new BigDecimal(0);
            if (sgst != null && cgst != null) {
                sgstAmt = (amt.multiply(sgst)).divide(per);
                cgstAmt = (amt.multiply(cgst)).divide(per);
                System.out.println("CGST AMT" + cgstAmt + " SGST AMT" + sgstAmt);
                sgstAmtBinding.setValue(sgstAmt);
                cgstAmtBinding.setValue(cgstAmt);
            } else if (igst != null) {
                igstAmt = (amt.multiply(igst)).divide(per);
                System.out.println("IGST AMT" + igstAmt);
                igstAmtBinding.setValue(igstAmt);
            }
        }
    }

    public void setIgstAmtBinding(RichInputText igstAmtBinding) {
        this.igstAmtBinding = igstAmtBinding;
    }

    public RichInputText getIgstAmtBinding() {
        return igstAmtBinding;
    }

    public void setCgstAmtBinding(RichInputText cgstAmtBinding) {
        this.cgstAmtBinding = cgstAmtBinding;
    }

    public RichInputText getCgstAmtBinding() {
        return cgstAmtBinding;
    }

    public void setSgstAmtBinding(RichInputText sgstAmtBinding) {
        this.sgstAmtBinding = sgstAmtBinding;
    }

    public RichInputText getSgstAmtBinding() {
        return sgstAmtBinding;
    }

    public void gstCodeVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce.getNewValue() != vce.getOldValue()) {
            BigDecimal amt = (BigDecimal) amountBinding.getValue();
            BigDecimal sgst = (BigDecimal) sgstBinding.getValue();
            BigDecimal cgst = (BigDecimal) cgstBinding.getValue();
            BigDecimal igst = (BigDecimal) igstBinding.getValue();
            BigDecimal per = new BigDecimal(100);
            BigDecimal sgstAmt = new BigDecimal(0);
            BigDecimal cgstAmt = new BigDecimal(0);
            BigDecimal igstAmt = new BigDecimal(0);
            if (sgst != null && cgst != null) {
                sgstAmt = (amt.multiply(sgst)).divide(per);
                cgstAmt = (amt.multiply(cgst)).divide(per);
                System.out.println("CGST AMT" + cgstAmt + " SGST AMT" + sgstAmt);
                sgstAmtBinding.setValue(sgstAmt);
                cgstAmtBinding.setValue(cgstAmt);
            } else if (igst != null) {
                igstAmt = (amt.multiply(igst)).divide(per);
                System.out.println("IGST AMT" + igstAmt);
                igstAmtBinding.setValue(igstAmt);
            }
        }
    }

    public void setAmountBinding(RichInputText amountBinding) {
        this.amountBinding = amountBinding;
    }

    public RichInputText getAmountBinding() {
        return amountBinding;
    }

    public void challanTypeVCL(ValueChangeEvent vce) {
        if (vce != null) {

            vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
            if (challanTypeBinding.getValue().toString().equalsIgnoreCase("AS")) {

                getDocNoBinding().setDisabled(true);
            }
        }
    }
}
