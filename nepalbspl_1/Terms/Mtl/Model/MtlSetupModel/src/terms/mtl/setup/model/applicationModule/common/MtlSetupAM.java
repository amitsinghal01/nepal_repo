package terms.mtl.setup.model.applicationModule.common;

import java.math.BigDecimal;

import java.text.ParseException;

import oracle.jbo.ApplicationModule;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Sat Apr 22 10:30:10 IST 2017
// ---------------------------------------------------------------------
public interface MtlSetupAM extends ApplicationModule {


    void getEntryNo(String EntryNo);

    void Create(String modeId);

    void getItemID(String itemID, String modeId);

    void getLoctionToUnit();


    void CompareWithFinYear(String month, String year);

    void SavingOpeningId();
    void docNumberLotIssue();

    String qtyCheck(oracle.jbo.domain.Number rqty);
    void docNumberLotOpening();

    void viewAllMasterQuantities(String view_mode);

    String selectAndDeselectMethod(String mode_type, String grant_type);

    void masterQuantityModeValues();
    void generateServiceJobCodeSeq();

    void getAmendmentPJDetails();

    void serialNoCreatePaymentTerm(BigDecimal SrNo);
    void serialNoInsert(BigDecimal SrNo);

    String AnnexureLimit();

    void getManfCode();

    String getDepartCode();

    void getDeptCodeDetails(String deptCode);

    Integer generateSeqNo();
}

