package terms.mtl.setup.model.view;

import oracle.jbo.RowSet;
import oracle.jbo.domain.Number;
import oracle.jbo.server.EntityImpl;
import oracle.jbo.server.ViewRowImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Sat Jun 01 14:50:35 IST 2019
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class MtlParameterVORowImpl extends ViewRowImpl {

    public static final int ENTITY_MTLPARAMETEREO = 0;
    public static final int ENTITY_UNITEO = 1;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        KeyNo,
        ParameterName,
        ParameterValue,
        Uom,
        Module,
        UnitCd,
        EditTrans,
        Name,
        Code,
        ObjectVersionNumber,
        UnitVO1,
        UomVO1;
        static AttributesEnum[] vals = null;
        ;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int KEYNO = AttributesEnum.KeyNo.index();
    public static final int PARAMETERNAME = AttributesEnum.ParameterName.index();
    public static final int PARAMETERVALUE = AttributesEnum.ParameterValue.index();
    public static final int UOM = AttributesEnum.Uom.index();
    public static final int MODULE = AttributesEnum.Module.index();
    public static final int UNITCD = AttributesEnum.UnitCd.index();
    public static final int EDITTRANS = AttributesEnum.EditTrans.index();
    public static final int NAME = AttributesEnum.Name.index();
    public static final int CODE = AttributesEnum.Code.index();
    public static final int OBJECTVERSIONNUMBER = AttributesEnum.ObjectVersionNumber.index();
    public static final int UNITVO1 = AttributesEnum.UnitVO1.index();
    public static final int UOMVO1 = AttributesEnum.UomVO1.index();

    /**
     * This is the default constructor (do not remove).
     */
    public MtlParameterVORowImpl() {
    }

    /**
     * Gets MtlParameterEO entity object.
     * @return the MtlParameterEO
     */
    public EntityImpl getMtlParameterEO() {
        return (EntityImpl) getEntity(ENTITY_MTLPARAMETEREO);
    }

    /**
     * Gets UnitEO entity object.
     * @return the UnitEO
     */
    public EntityImpl getUnitEO() {
        return (EntityImpl) getEntity(ENTITY_UNITEO);
    }

    /**
     * Gets the attribute value for KEY_NO using the alias name KeyNo.
     * @return the KEY_NO
     */
    public Number getKeyNo() {
        return (Number) getAttributeInternal(KEYNO);
    }

    /**
     * Sets <code>value</code> as attribute value for KEY_NO using the alias name KeyNo.
     * @param value value to set the KEY_NO
     */
    public void setKeyNo(Number value) {
        setAttributeInternal(KEYNO, value);
    }

    /**
     * Gets the attribute value for PARAMETER_NAME using the alias name ParameterName.
     * @return the PARAMETER_NAME
     */
    public String getParameterName() {
        return (String) getAttributeInternal(PARAMETERNAME);
    }

    /**
     * Sets <code>value</code> as attribute value for PARAMETER_NAME using the alias name ParameterName.
     * @param value value to set the PARAMETER_NAME
     */
    public void setParameterName(String value) {
        setAttributeInternal(PARAMETERNAME, value);
    }

    /**
     * Gets the attribute value for PARAMETER_VALUE using the alias name ParameterValue.
     * @return the PARAMETER_VALUE
     */
    public String getParameterValue() {
        return (String) getAttributeInternal(PARAMETERVALUE);
    }

    /**
     * Sets <code>value</code> as attribute value for PARAMETER_VALUE using the alias name ParameterValue.
     * @param value value to set the PARAMETER_VALUE
     */
    public void setParameterValue(String value) {
        setAttributeInternal(PARAMETERVALUE, value);
    }

    /**
     * Gets the attribute value for UOM using the alias name Uom.
     * @return the UOM
     */
    public String getUom() {
        return (String) getAttributeInternal(UOM);
    }

    /**
     * Sets <code>value</code> as attribute value for UOM using the alias name Uom.
     * @param value value to set the UOM
     */
    public void setUom(String value) {
        setAttributeInternal(UOM, value);
    }

    /**
     * Gets the attribute value for MODULE using the alias name Module.
     * @return the MODULE
     */
    public String getModule() {
        return (String) getAttributeInternal(MODULE);
    }

    /**
     * Sets <code>value</code> as attribute value for MODULE using the alias name Module.
     * @param value value to set the MODULE
     */
    public void setModule(String value) {
        setAttributeInternal(MODULE, value);
    }

    /**
     * Gets the attribute value for UNIT_CD using the alias name UnitCd.
     * @return the UNIT_CD
     */
    public String getUnitCd() {
        return (String) getAttributeInternal(UNITCD);
    }

    /**
     * Sets <code>value</code> as attribute value for UNIT_CD using the alias name UnitCd.
     * @param value value to set the UNIT_CD
     */
    public void setUnitCd(String value) {
        setAttributeInternal(UNITCD, value);
    }

    /**
     * Gets the attribute value for the calculated attribute EditTrans.
     * @return the EditTrans
     */
    public Integer getEditTrans() {
        byte entityState = this.getEntity(0).getEntityState();
        return new Integer(entityState);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute EditTrans.
     * @param value value to set the  EditTrans
     */
    public void setEditTrans(Integer value) {
        setAttributeInternal(EDITTRANS, value);
    }

    /**
     * Gets the attribute value for NAME using the alias name Name.
     * @return the NAME
     */
    public String getName() {
        return (String) getAttributeInternal(NAME);
    }

    /**
     * Sets <code>value</code> as attribute value for NAME using the alias name Name.
     * @param value value to set the NAME
     */
    public void setName(String value) {
        setAttributeInternal(NAME, value);
    }

    /**
     * Gets the attribute value for CODE using the alias name Code.
     * @return the CODE
     */
    public String getCode() {
        return (String) getAttributeInternal(CODE);
    }

    /**
     * Sets <code>value</code> as attribute value for CODE using the alias name Code.
     * @param value value to set the CODE
     */
    public void setCode(String value) {
        setAttributeInternal(CODE, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER, value);
    }

    /**
     * Gets the view accessor <code>RowSet</code> UnitVO1.
     */
    public RowSet getUnitVO1() {
        return (RowSet) getAttributeInternal(UNITVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> UomVO1.
     */
    public RowSet getUomVO1() {
        return (RowSet) getAttributeInternal(UOMVO1);
    }
}

