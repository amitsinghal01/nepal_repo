package terms.mtl.transaction.model.view;

import java.math.BigDecimal;

import java.sql.Timestamp;

import oracle.jbo.server.EntityImpl;
import oracle.jbo.server.ViewRowImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Fri Jan 18 15:37:47 IST 2019
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class ContractOrderTermDtlVORowImpl extends ViewRowImpl {
    public static final int ENTITY_CONTRACTORDERTERMDTLEO = 0;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        TermsDescription,
        CreatedBy,
        CreationDate,
        LastUpdatedBy,
        LastUpdateDate,
        ObjectVersionNumber,
        JoId,
        JoLineId,
        SrNo;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }
    public static final int TERMSDESCRIPTION = AttributesEnum.TermsDescription.index();
    public static final int CREATEDBY = AttributesEnum.CreatedBy.index();
    public static final int CREATIONDATE = AttributesEnum.CreationDate.index();
    public static final int LASTUPDATEDBY = AttributesEnum.LastUpdatedBy.index();
    public static final int LASTUPDATEDATE = AttributesEnum.LastUpdateDate.index();
    public static final int OBJECTVERSIONNUMBER = AttributesEnum.ObjectVersionNumber.index();
    public static final int JOID = AttributesEnum.JoId.index();
    public static final int JOLINEID = AttributesEnum.JoLineId.index();
    public static final int SRNO = AttributesEnum.SrNo.index();

    /**
     * This is the default constructor (do not remove).
     */
    public ContractOrderTermDtlVORowImpl() {
    }

    /**
     * Gets ContractOrderTermDtlEO entity object.
     * @return the ContractOrderTermDtlEO
     */
    public EntityImpl getContractOrderTermDtlEO() {
        return (EntityImpl) getEntity(ENTITY_CONTRACTORDERTERMDTLEO);
    }

    /**
     * Gets the attribute value for TERMS_DESCRIPTION using the alias name TermsDescription.
     * @return the TERMS_DESCRIPTION
     */
    public String getTermsDescription() {
        return (String) getAttributeInternal(TERMSDESCRIPTION);
    }

    /**
     * Sets <code>value</code> as attribute value for TERMS_DESCRIPTION using the alias name TermsDescription.
     * @param value value to set the TERMS_DESCRIPTION
     */
    public void setTermsDescription(String value) {
        setAttributeInternal(TERMSDESCRIPTION, value);
    }

    /**
     * Gets the attribute value for CREATED_BY using the alias name CreatedBy.
     * @return the CREATED_BY
     */
    public String getCreatedBy() {
        return (String) getAttributeInternal(CREATEDBY);
    }

    /**
     * Gets the attribute value for CREATION_DATE using the alias name CreationDate.
     * @return the CREATION_DATE
     */
    public Timestamp getCreationDate() {
        return (Timestamp) getAttributeInternal(CREATIONDATE);
    }

    /**
     * Gets the attribute value for LAST_UPDATED_BY using the alias name LastUpdatedBy.
     * @return the LAST_UPDATED_BY
     */
    public String getLastUpdatedBy() {
        return (String) getAttributeInternal(LASTUPDATEDBY);
    }

    /**
     * Gets the attribute value for LAST_UPDATE_DATE using the alias name LastUpdateDate.
     * @return the LAST_UPDATE_DATE
     */
    public Timestamp getLastUpdateDate() {
        return (Timestamp) getAttributeInternal(LASTUPDATEDATE);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER, value);
    }

    /**
     * Gets the attribute value for JO_ID using the alias name JoId.
     * @return the JO_ID
     */
    public Long getJoId() {
        return (Long) getAttributeInternal(JOID);
    }

    /**
     * Sets <code>value</code> as attribute value for JO_ID using the alias name JoId.
     * @param value value to set the JO_ID
     */
    public void setJoId(Long value) {
        setAttributeInternal(JOID, value);
    }

    /**
     * Gets the attribute value for JO_LINE_ID using the alias name JoLineId.
     * @return the JO_LINE_ID
     */
    public Long getJoLineId() {
        return (Long) getAttributeInternal(JOLINEID);
    }

    /**
     * Sets <code>value</code> as attribute value for JO_LINE_ID using the alias name JoLineId.
     * @param value value to set the JO_LINE_ID
     */
    public void setJoLineId(Long value) {
        setAttributeInternal(JOLINEID, value);
    }

    /**
     * Gets the attribute value for SR_NO using the alias name SrNo.
     * @return the SR_NO
     */
    public BigDecimal getSrNo() {
        return (BigDecimal) getAttributeInternal(SRNO);
    }

    /**
     * Sets <code>value</code> as attribute value for SR_NO using the alias name SrNo.
     * @param value value to set the SR_NO
     */
    public void setSrNo(BigDecimal value) {
        setAttributeInternal(SRNO, value);
    }
}

