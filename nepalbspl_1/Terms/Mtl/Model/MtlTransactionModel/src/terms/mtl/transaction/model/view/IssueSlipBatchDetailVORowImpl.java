package terms.mtl.transaction.model.view;

import java.math.BigDecimal;

import java.sql.Timestamp;

import oracle.jbo.RowSet;
import oracle.jbo.server.EntityImpl;
import oracle.jbo.server.ViewRowImpl;

import terms.mtl.transaction.model.applicationModule.MtlTransactionAMImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Thu Mar 05 13:03:42 IST 2020
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class IssueSlipBatchDetailVORowImpl extends ViewRowImpl {


    public static final int ENTITY_ISSUESLIPBATCHDETAILEO = 0;
    public static final int ENTITY_ISSUESLIPBATCHHEADEREO = 1;
    public static final int ENTITY_PROCESSEO = 2;
    public static final int ENTITY_ITEMSTOCKEO = 3;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        AuthFlag,
        CcgpSt,
        ConsTo,
        ConsType,
        CreatedBy,
        CreatedDate,
        CreationDate,
        Diam,
        DocAmdNo,
        DocDt,
        DocNo,
        FnsItemCd,
        FnsPrSeqNo,
        FnsPrSeqRevno,
        FnsProcCd,
        FnsRevNo,
        Height,
        Hght,
        InvHeadType,
        InvIdentifier,
        IssHslipSlipNo,
        IssRate,
        ItemCd,
        JcBarcode,
        LastUpdatedBy,
        LastUpdatedDate,
        Length,
        Lnth,
        LotNo,
        ObjectVersionNumber,
        ProcCd,
        ProcSeq,
        ProcSeqRevno,
        Qty,
        RackCd,
        RefItemCd,
        RefProcCd,
        RefProcSeq,
        RefProcSeqRevno,
        RefRevno,
        Remarks,
        RetnQty,
        RevNo,
        SlipId,
        SlipLineId,
        StockQty,
        StrQty,
        StrUom,
        Uom,
        Weight,
        Width,
        IssFor,
        IssTo,
        IssType,
        UnitCd,
        SlipId1,
        LongDescrip,
        ObjectVersionNumber1,
        ProcId,
        ShortDescrip,
        ItemCd1,
        ItemDesc,
        IssueSlipDocumentNumberVVO1,
        IssueSlipBatchLotNoLovVVO1,
        IssueSlipItemCodeVVO1;
        static AttributesEnum[] vals = null; ;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int AUTHFLAG = AttributesEnum.AuthFlag.index();
    public static final int CCGPST = AttributesEnum.CcgpSt.index();
    public static final int CONSTO = AttributesEnum.ConsTo.index();
    public static final int CONSTYPE = AttributesEnum.ConsType.index();
    public static final int CREATEDBY = AttributesEnum.CreatedBy.index();
    public static final int CREATEDDATE = AttributesEnum.CreatedDate.index();
    public static final int CREATIONDATE = AttributesEnum.CreationDate.index();
    public static final int DIAM = AttributesEnum.Diam.index();
    public static final int DOCAMDNO = AttributesEnum.DocAmdNo.index();
    public static final int DOCDT = AttributesEnum.DocDt.index();
    public static final int DOCNO = AttributesEnum.DocNo.index();
    public static final int FNSITEMCD = AttributesEnum.FnsItemCd.index();
    public static final int FNSPRSEQNO = AttributesEnum.FnsPrSeqNo.index();
    public static final int FNSPRSEQREVNO = AttributesEnum.FnsPrSeqRevno.index();
    public static final int FNSPROCCD = AttributesEnum.FnsProcCd.index();
    public static final int FNSREVNO = AttributesEnum.FnsRevNo.index();
    public static final int HEIGHT = AttributesEnum.Height.index();
    public static final int HGHT = AttributesEnum.Hght.index();
    public static final int INVHEADTYPE = AttributesEnum.InvHeadType.index();
    public static final int INVIDENTIFIER = AttributesEnum.InvIdentifier.index();
    public static final int ISSHSLIPSLIPNO = AttributesEnum.IssHslipSlipNo.index();
    public static final int ISSRATE = AttributesEnum.IssRate.index();
    public static final int ITEMCD = AttributesEnum.ItemCd.index();
    public static final int JCBARCODE = AttributesEnum.JcBarcode.index();
    public static final int LASTUPDATEDBY = AttributesEnum.LastUpdatedBy.index();
    public static final int LASTUPDATEDDATE = AttributesEnum.LastUpdatedDate.index();
    public static final int LENGTH = AttributesEnum.Length.index();
    public static final int LNTH = AttributesEnum.Lnth.index();
    public static final int LOTNO = AttributesEnum.LotNo.index();
    public static final int OBJECTVERSIONNUMBER = AttributesEnum.ObjectVersionNumber.index();
    public static final int PROCCD = AttributesEnum.ProcCd.index();
    public static final int PROCSEQ = AttributesEnum.ProcSeq.index();
    public static final int PROCSEQREVNO = AttributesEnum.ProcSeqRevno.index();
    public static final int QTY = AttributesEnum.Qty.index();
    public static final int RACKCD = AttributesEnum.RackCd.index();
    public static final int REFITEMCD = AttributesEnum.RefItemCd.index();
    public static final int REFPROCCD = AttributesEnum.RefProcCd.index();
    public static final int REFPROCSEQ = AttributesEnum.RefProcSeq.index();
    public static final int REFPROCSEQREVNO = AttributesEnum.RefProcSeqRevno.index();
    public static final int REFREVNO = AttributesEnum.RefRevno.index();
    public static final int REMARKS = AttributesEnum.Remarks.index();
    public static final int RETNQTY = AttributesEnum.RetnQty.index();
    public static final int REVNO = AttributesEnum.RevNo.index();
    public static final int SLIPID = AttributesEnum.SlipId.index();
    public static final int SLIPLINEID = AttributesEnum.SlipLineId.index();
    public static final int STOCKQTY = AttributesEnum.StockQty.index();
    public static final int STRQTY = AttributesEnum.StrQty.index();
    public static final int STRUOM = AttributesEnum.StrUom.index();
    public static final int UOM = AttributesEnum.Uom.index();
    public static final int WEIGHT = AttributesEnum.Weight.index();
    public static final int WIDTH = AttributesEnum.Width.index();
    public static final int ISSFOR = AttributesEnum.IssFor.index();
    public static final int ISSTO = AttributesEnum.IssTo.index();
    public static final int ISSTYPE = AttributesEnum.IssType.index();
    public static final int UNITCD = AttributesEnum.UnitCd.index();
    public static final int SLIPID1 = AttributesEnum.SlipId1.index();
    public static final int LONGDESCRIP = AttributesEnum.LongDescrip.index();
    public static final int OBJECTVERSIONNUMBER1 = AttributesEnum.ObjectVersionNumber1.index();
    public static final int PROCID = AttributesEnum.ProcId.index();
    public static final int SHORTDESCRIP = AttributesEnum.ShortDescrip.index();
    public static final int ITEMCD1 = AttributesEnum.ItemCd1.index();
    public static final int ITEMDESC = AttributesEnum.ItemDesc.index();
    public static final int ISSUESLIPDOCUMENTNUMBERVVO1 = AttributesEnum.IssueSlipDocumentNumberVVO1.index();
    public static final int ISSUESLIPBATCHLOTNOLOVVVO1 = AttributesEnum.IssueSlipBatchLotNoLovVVO1.index();
    public static final int ISSUESLIPITEMCODEVVO1 = AttributesEnum.IssueSlipItemCodeVVO1.index();

    /**
     * This is the default constructor (do not remove).
     */
    public IssueSlipBatchDetailVORowImpl() {
    }

    /**
     * Gets IssueSlipBatchDetailEO entity object.
     * @return the IssueSlipBatchDetailEO
     */
    public EntityImpl getIssueSlipBatchDetailEO() {
        return (EntityImpl) getEntity(ENTITY_ISSUESLIPBATCHDETAILEO);
    }

    /**
     * Gets IssueSlipBatchHeaderEO entity object.
     * @return the IssueSlipBatchHeaderEO
     */
    public EntityImpl getIssueSlipBatchHeaderEO() {
        return (EntityImpl) getEntity(ENTITY_ISSUESLIPBATCHHEADEREO);
    }

    /**
     * Gets ProcessEO entity object.
     * @return the ProcessEO
     */
    public EntityImpl getProcessEO() {
        return (EntityImpl) getEntity(ENTITY_PROCESSEO);
    }


    /**
     * Gets ItemStockEO entity object.
     * @return the ItemStockEO
     */
    public EntityImpl getItemStockEO() {
        return (EntityImpl) getEntity(ENTITY_ITEMSTOCKEO);
    }

    /**
     * Gets the attribute value for AUTH_FLAG using the alias name AuthFlag.
     * @return the AUTH_FLAG
     */
    public String getAuthFlag() {
        return (String) getAttributeInternal(AUTHFLAG);
    }

    /**
     * Sets <code>value</code> as attribute value for AUTH_FLAG using the alias name AuthFlag.
     * @param value value to set the AUTH_FLAG
     */
    public void setAuthFlag(String value) {
        setAttributeInternal(AUTHFLAG, value);
    }

    /**
     * Gets the attribute value for CCGP_ST using the alias name CcgpSt.
     * @return the CCGP_ST
     */
    public String getCcgpSt() {
        return (String) getAttributeInternal(CCGPST);
    }

    /**
     * Sets <code>value</code> as attribute value for CCGP_ST using the alias name CcgpSt.
     * @param value value to set the CCGP_ST
     */
    public void setCcgpSt(String value) {
        setAttributeInternal(CCGPST, value);
    }

    /**
     * Gets the attribute value for CONS_TO using the alias name ConsTo.
     * @return the CONS_TO
     */
    public String getConsTo() {
        return (String) getAttributeInternal(CONSTO);
    }

    /**
     * Sets <code>value</code> as attribute value for CONS_TO using the alias name ConsTo.
     * @param value value to set the CONS_TO
     */
    public void setConsTo(String value) {
        setAttributeInternal(CONSTO, value);
    }

    /**
     * Gets the attribute value for CONS_TYPE using the alias name ConsType.
     * @return the CONS_TYPE
     */
    public String getConsType() {
        return (String) getAttributeInternal(CONSTYPE);
    }

    /**
     * Sets <code>value</code> as attribute value for CONS_TYPE using the alias name ConsType.
     * @param value value to set the CONS_TYPE
     */
    public void setConsType(String value) {
        setAttributeInternal(CONSTYPE, value);
    }

    /**
     * Gets the attribute value for CREATED_BY using the alias name CreatedBy.
     * @return the CREATED_BY
     */
    public String getCreatedBy() {
        return (String) getAttributeInternal(CREATEDBY);
    }

    /**
     * Sets <code>value</code> as attribute value for CREATED_BY using the alias name CreatedBy.
     * @param value value to set the CREATED_BY
     */
    public void setCreatedBy(String value) {
        setAttributeInternal(CREATEDBY, value);
    }

    /**
     * Gets the attribute value for CREATED_DATE using the alias name CreatedDate.
     * @return the CREATED_DATE
     */
    public Timestamp getCreatedDate() {
        return (Timestamp) getAttributeInternal(CREATEDDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for CREATED_DATE using the alias name CreatedDate.
     * @param value value to set the CREATED_DATE
     */
    public void setCreatedDate(Timestamp value) {
        setAttributeInternal(CREATEDDATE, value);
    }

    /**
     * Gets the attribute value for CREATION_DATE using the alias name CreationDate.
     * @return the CREATION_DATE
     */
    public Timestamp getCreationDate() {
        return (Timestamp) getAttributeInternal(CREATIONDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for CREATION_DATE using the alias name CreationDate.
     * @param value value to set the CREATION_DATE
     */
    public void setCreationDate(Timestamp value) {
        setAttributeInternal(CREATIONDATE, value);
    }

    /**
     * Gets the attribute value for DIAM using the alias name Diam.
     * @return the DIAM
     */
    public BigDecimal getDiam() {
        return (BigDecimal) getAttributeInternal(DIAM);
    }

    /**
     * Sets <code>value</code> as attribute value for DIAM using the alias name Diam.
     * @param value value to set the DIAM
     */
    public void setDiam(BigDecimal value) {
        setAttributeInternal(DIAM, value);
    }

    /**
     * Gets the attribute value for DOC_AMD_NO using the alias name DocAmdNo.
     * @return the DOC_AMD_NO
     */
    public BigDecimal getDocAmdNo() {
        return (BigDecimal) getAttributeInternal(DOCAMDNO);
    }

    /**
     * Sets <code>value</code> as attribute value for DOC_AMD_NO using the alias name DocAmdNo.
     * @param value value to set the DOC_AMD_NO
     */
    public void setDocAmdNo(BigDecimal value) {
        setAttributeInternal(DOCAMDNO, value);
    }

    /**
     * Gets the attribute value for DOC_DT using the alias name DocDt.
     * @return the DOC_DT
     */
    public Timestamp getDocDt() {
        return (Timestamp) getAttributeInternal(DOCDT);
    }

    /**
     * Sets <code>value</code> as attribute value for DOC_DT using the alias name DocDt.
     * @param value value to set the DOC_DT
     */
    public void setDocDt(Timestamp value) {
        setAttributeInternal(DOCDT, value);
    }

    /**
     * Gets the attribute value for DOC_NO using the alias name DocNo.
     * @return the DOC_NO
     */
    public String getDocNo() {
        return (String) getAttributeInternal(DOCNO);
    }

    /**
     * Sets <code>value</code> as attribute value for DOC_NO using the alias name DocNo.
     * @param value value to set the DOC_NO
     */
    public void setDocNo(String value) {
        setAttributeInternal(DOCNO, value);
    }

    /**
     * Gets the attribute value for FNS_ITEM_CD using the alias name FnsItemCd.
     * @return the FNS_ITEM_CD
     */
    public String getFnsItemCd() {
        return (String) getAttributeInternal(FNSITEMCD);
    }

    /**
     * Sets <code>value</code> as attribute value for FNS_ITEM_CD using the alias name FnsItemCd.
     * @param value value to set the FNS_ITEM_CD
     */
    public void setFnsItemCd(String value) {
        setAttributeInternal(FNSITEMCD, value);
    }

    /**
     * Gets the attribute value for FNS_PR_SEQ_NO using the alias name FnsPrSeqNo.
     * @return the FNS_PR_SEQ_NO
     */
    public BigDecimal getFnsPrSeqNo() {
        return (BigDecimal) getAttributeInternal(FNSPRSEQNO);
    }

    /**
     * Sets <code>value</code> as attribute value for FNS_PR_SEQ_NO using the alias name FnsPrSeqNo.
     * @param value value to set the FNS_PR_SEQ_NO
     */
    public void setFnsPrSeqNo(BigDecimal value) {
        setAttributeInternal(FNSPRSEQNO, value);
    }

    /**
     * Gets the attribute value for FNS_PR_SEQ_REVNO using the alias name FnsPrSeqRevno.
     * @return the FNS_PR_SEQ_REVNO
     */
    public BigDecimal getFnsPrSeqRevno() {
        return (BigDecimal) getAttributeInternal(FNSPRSEQREVNO);
    }

    /**
     * Sets <code>value</code> as attribute value for FNS_PR_SEQ_REVNO using the alias name FnsPrSeqRevno.
     * @param value value to set the FNS_PR_SEQ_REVNO
     */
    public void setFnsPrSeqRevno(BigDecimal value) {
        setAttributeInternal(FNSPRSEQREVNO, value);
    }

    /**
     * Gets the attribute value for FNS_PROC_CD using the alias name FnsProcCd.
     * @return the FNS_PROC_CD
     */
    public String getFnsProcCd() {
        return (String) getAttributeInternal(FNSPROCCD);
    }

    /**
     * Sets <code>value</code> as attribute value for FNS_PROC_CD using the alias name FnsProcCd.
     * @param value value to set the FNS_PROC_CD
     */
    public void setFnsProcCd(String value) {
        setAttributeInternal(FNSPROCCD, value);
    }

    /**
     * Gets the attribute value for FNS_REV_NO using the alias name FnsRevNo.
     * @return the FNS_REV_NO
     */
    public BigDecimal getFnsRevNo() {
        return (BigDecimal) getAttributeInternal(FNSREVNO);
    }

    /**
     * Sets <code>value</code> as attribute value for FNS_REV_NO using the alias name FnsRevNo.
     * @param value value to set the FNS_REV_NO
     */
    public void setFnsRevNo(BigDecimal value) {
        setAttributeInternal(FNSREVNO, value);
    }

    /**
     * Gets the attribute value for HEIGHT using the alias name Height.
     * @return the HEIGHT
     */
    public BigDecimal getHeight() {
        return (BigDecimal) getAttributeInternal(HEIGHT);
    }

    /**
     * Sets <code>value</code> as attribute value for HEIGHT using the alias name Height.
     * @param value value to set the HEIGHT
     */
    public void setHeight(BigDecimal value) {
        setAttributeInternal(HEIGHT, value);
    }

    /**
     * Gets the attribute value for HGHT using the alias name Hght.
     * @return the HGHT
     */
    public BigDecimal getHght() {
        return (BigDecimal) getAttributeInternal(HGHT);
    }

    /**
     * Sets <code>value</code> as attribute value for HGHT using the alias name Hght.
     * @param value value to set the HGHT
     */
    public void setHght(BigDecimal value) {
        setAttributeInternal(HGHT, value);
    }

    /**
     * Gets the attribute value for INV_HEAD_TYPE using the alias name InvHeadType.
     * @return the INV_HEAD_TYPE
     */
    public String getInvHeadType() {
        return (String) getAttributeInternal(INVHEADTYPE);
    }

    /**
     * Sets <code>value</code> as attribute value for INV_HEAD_TYPE using the alias name InvHeadType.
     * @param value value to set the INV_HEAD_TYPE
     */
    public void setInvHeadType(String value) {
        setAttributeInternal(INVHEADTYPE, value);
    }

    /**
     * Gets the attribute value for INV_IDENTIFIER using the alias name InvIdentifier.
     * @return the INV_IDENTIFIER
     */
    public String getInvIdentifier() {
        return (String) getAttributeInternal(INVIDENTIFIER);
    }

    /**
     * Sets <code>value</code> as attribute value for INV_IDENTIFIER using the alias name InvIdentifier.
     * @param value value to set the INV_IDENTIFIER
     */
    public void setInvIdentifier(String value) {
        setAttributeInternal(INVIDENTIFIER, value);
    }

    /**
     * Gets the attribute value for ISS_HSLIP_SLIP_NO using the alias name IssHslipSlipNo.
     * @return the ISS_HSLIP_SLIP_NO
     */
    public String getIssHslipSlipNo() {
        return (String) getAttributeInternal(ISSHSLIPSLIPNO);
    }

    /**
     * Sets <code>value</code> as attribute value for ISS_HSLIP_SLIP_NO using the alias name IssHslipSlipNo.
     * @param value value to set the ISS_HSLIP_SLIP_NO
     */
    public void setIssHslipSlipNo(String value) {
        setAttributeInternal(ISSHSLIPSLIPNO, value);
    }

    /**
     * Gets the attribute value for ISS_RATE using the alias name IssRate.
     * @return the ISS_RATE
     */
    public BigDecimal getIssRate() {
        return (BigDecimal) getAttributeInternal(ISSRATE);
    }

    /**
     * Sets <code>value</code> as attribute value for ISS_RATE using the alias name IssRate.
     * @param value value to set the ISS_RATE
     */
    public void setIssRate(BigDecimal value) {
        setAttributeInternal(ISSRATE, value);
    }

    /**
     * Gets the attribute value for ITEM_CD using the alias name ItemCd.
     * @return the ITEM_CD
     */
    public String getItemCd() {
        return (String) getAttributeInternal(ITEMCD);
    }

    /**
     * Sets <code>value</code> as attribute value for ITEM_CD using the alias name ItemCd.
     * @param value value to set the ITEM_CD
     */
    public void setItemCd(String value) {
        setAttributeInternal(ITEMCD, value);
    }

    /**
     * Gets the attribute value for JC_BARCODE using the alias name JcBarcode.
     * @return the JC_BARCODE
     */
    public String getJcBarcode() {
        return (String) getAttributeInternal(JCBARCODE);
    }

    /**
     * Sets <code>value</code> as attribute value for JC_BARCODE using the alias name JcBarcode.
     * @param value value to set the JC_BARCODE
     */
    public void setJcBarcode(String value) {
        setAttributeInternal(JCBARCODE, value);
    }

    /**
     * Gets the attribute value for LAST_UPDATED_BY using the alias name LastUpdatedBy.
     * @return the LAST_UPDATED_BY
     */
    public String getLastUpdatedBy() {
        return (String) getAttributeInternal(LASTUPDATEDBY);
    }

    /**
     * Sets <code>value</code> as attribute value for LAST_UPDATED_BY using the alias name LastUpdatedBy.
     * @param value value to set the LAST_UPDATED_BY
     */
    public void setLastUpdatedBy(String value) {
        setAttributeInternal(LASTUPDATEDBY, value);
    }

    /**
     * Gets the attribute value for LAST_UPDATED_DATE using the alias name LastUpdatedDate.
     * @return the LAST_UPDATED_DATE
     */
    public Timestamp getLastUpdatedDate() {
        return (Timestamp) getAttributeInternal(LASTUPDATEDDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for LAST_UPDATED_DATE using the alias name LastUpdatedDate.
     * @param value value to set the LAST_UPDATED_DATE
     */
    public void setLastUpdatedDate(Timestamp value) {
        setAttributeInternal(LASTUPDATEDDATE, value);
    }

    /**
     * Gets the attribute value for LENGTH using the alias name Length.
     * @return the LENGTH
     */
    public BigDecimal getLength() {
        return (BigDecimal) getAttributeInternal(LENGTH);
    }

    /**
     * Sets <code>value</code> as attribute value for LENGTH using the alias name Length.
     * @param value value to set the LENGTH
     */
    public void setLength(BigDecimal value) {
        setAttributeInternal(LENGTH, value);
    }

    /**
     * Gets the attribute value for LNTH using the alias name Lnth.
     * @return the LNTH
     */
    public BigDecimal getLnth() {
        return (BigDecimal) getAttributeInternal(LNTH);
    }

    /**
     * Sets <code>value</code> as attribute value for LNTH using the alias name Lnth.
     * @param value value to set the LNTH
     */
    public void setLnth(BigDecimal value) {
        setAttributeInternal(LNTH, value);
    }

    /**
     * Gets the attribute value for LOT_NO using the alias name LotNo.
     * @return the LOT_NO
     */
    public String getLotNo() {
        return (String) getAttributeInternal(LOTNO);
    }

    /**
     * Sets <code>value</code> as attribute value for LOT_NO using the alias name LotNo.
     * @param value value to set the LOT_NO
     */
    public void setLotNo(String value) {
        setAttributeInternal(LOTNO, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @return the OBJECT_VERSION_NUMBER
     */
    public BigDecimal getObjectVersionNumber() {
        return (BigDecimal) getAttributeInternal(OBJECTVERSIONNUMBER);
    }

    /**
     * Gets the attribute value for PROC_CD using the alias name ProcCd.
     * @return the PROC_CD
     */
    public String getProcCd() {
        return (String) getAttributeInternal(PROCCD);
    }

    /**
     * Sets <code>value</code> as attribute value for PROC_CD using the alias name ProcCd.
     * @param value value to set the PROC_CD
     */
    public void setProcCd(String value) {
//        MtlTransactionAMImpl am = (MtlTransactionAMImpl) this.getApplicationModule();
//        setAttributeInternal(PROCCD, am.getIssueSlipItemCodeVVO1().getCurrentRow().getAttribute("ProductCode"));
//        System.out.println("Product Code " + am.getIssueSlipItemCodeVVO1().getCurrentRow().getAttribute("ProductCode"));
        setAttributeInternal(PROCCD, value);
    }

    /**
     * Gets the attribute value for PROC_SEQ using the alias name ProcSeq.
     * @return the PROC_SEQ
     */
    public BigDecimal getProcSeq() {
        return (BigDecimal) getAttributeInternal(PROCSEQ);
    }

    /**
     * Sets <code>value</code> as attribute value for PROC_SEQ using the alias name ProcSeq.
     * @param value value to set the PROC_SEQ
     */
    public void setProcSeq(BigDecimal value) {
        setAttributeInternal(PROCSEQ, value);
    }

    /**
     * Gets the attribute value for PROC_SEQ_REVNO using the alias name ProcSeqRevno.
     * @return the PROC_SEQ_REVNO
     */
    public BigDecimal getProcSeqRevno() {
        return (BigDecimal) getAttributeInternal(PROCSEQREVNO);
    }

    /**
     * Sets <code>value</code> as attribute value for PROC_SEQ_REVNO using the alias name ProcSeqRevno.
     * @param value value to set the PROC_SEQ_REVNO
     */
    public void setProcSeqRevno(BigDecimal value) {
        setAttributeInternal(PROCSEQREVNO, value);
    }

    /**
     * Gets the attribute value for QTY using the alias name Qty.
     * @return the QTY
     */
    public BigDecimal getQty() {
        return (BigDecimal) getAttributeInternal(QTY);
    }

    /**
     * Sets <code>value</code> as attribute value for QTY using the alias name Qty.
     * @param value value to set the QTY
     */
    public void setQty(BigDecimal value) {
        setAttributeInternal(QTY, value);
    }

    /**
     * Gets the attribute value for RACK_CD using the alias name RackCd.
     * @return the RACK_CD
     */
    public String getRackCd() {
        return (String) getAttributeInternal(RACKCD);
    }

    /**
     * Sets <code>value</code> as attribute value for RACK_CD using the alias name RackCd.
     * @param value value to set the RACK_CD
     */
    public void setRackCd(String value) {
        setAttributeInternal(RACKCD, value);
    }

    /**
     * Gets the attribute value for REF_ITEM_CD using the alias name RefItemCd.
     * @return the REF_ITEM_CD
     */
    public String getRefItemCd() {
        return (String) getAttributeInternal(REFITEMCD);
    }

    /**
     * Sets <code>value</code> as attribute value for REF_ITEM_CD using the alias name RefItemCd.
     * @param value value to set the REF_ITEM_CD
     */
    public void setRefItemCd(String value) {
        setAttributeInternal(REFITEMCD, value);
    }

    /**
     * Gets the attribute value for REF_PROC_CD using the alias name RefProcCd.
     * @return the REF_PROC_CD
     */
    public String getRefProcCd() {
        return (String) getAttributeInternal(REFPROCCD);
    }

    /**
     * Sets <code>value</code> as attribute value for REF_PROC_CD using the alias name RefProcCd.
     * @param value value to set the REF_PROC_CD
     */
    public void setRefProcCd(String value) {
        setAttributeInternal(REFPROCCD, value);
    }

    /**
     * Gets the attribute value for REF_PROC_SEQ using the alias name RefProcSeq.
     * @return the REF_PROC_SEQ
     */
    public BigDecimal getRefProcSeq() {
        return (BigDecimal) getAttributeInternal(REFPROCSEQ);
    }

    /**
     * Sets <code>value</code> as attribute value for REF_PROC_SEQ using the alias name RefProcSeq.
     * @param value value to set the REF_PROC_SEQ
     */
    public void setRefProcSeq(BigDecimal value) {
        setAttributeInternal(REFPROCSEQ, value);
    }

    /**
     * Gets the attribute value for REF_PROC_SEQ_REVNO using the alias name RefProcSeqRevno.
     * @return the REF_PROC_SEQ_REVNO
     */
    public BigDecimal getRefProcSeqRevno() {
        return (BigDecimal) getAttributeInternal(REFPROCSEQREVNO);
    }

    /**
     * Sets <code>value</code> as attribute value for REF_PROC_SEQ_REVNO using the alias name RefProcSeqRevno.
     * @param value value to set the REF_PROC_SEQ_REVNO
     */
    public void setRefProcSeqRevno(BigDecimal value) {
        setAttributeInternal(REFPROCSEQREVNO, value);
    }

    /**
     * Gets the attribute value for REF_REVNO using the alias name RefRevno.
     * @return the REF_REVNO
     */
    public BigDecimal getRefRevno() {
        return (BigDecimal) getAttributeInternal(REFREVNO);
    }

    /**
     * Sets <code>value</code> as attribute value for REF_REVNO using the alias name RefRevno.
     * @param value value to set the REF_REVNO
     */
    public void setRefRevno(BigDecimal value) {
        setAttributeInternal(REFREVNO, value);
    }

    /**
     * Gets the attribute value for REMARKS using the alias name Remarks.
     * @return the REMARKS
     */
    public String getRemarks() {
        return (String) getAttributeInternal(REMARKS);
    }

    /**
     * Sets <code>value</code> as attribute value for REMARKS using the alias name Remarks.
     * @param value value to set the REMARKS
     */
    public void setRemarks(String value) {
        setAttributeInternal(REMARKS, value);
    }

    /**
     * Gets the attribute value for RETN_QTY using the alias name RetnQty.
     * @return the RETN_QTY
     */
    public BigDecimal getRetnQty() {
        return (BigDecimal) getAttributeInternal(RETNQTY);
    }

    /**
     * Sets <code>value</code> as attribute value for RETN_QTY using the alias name RetnQty.
     * @param value value to set the RETN_QTY
     */
    public void setRetnQty(BigDecimal value) {
        setAttributeInternal(RETNQTY, value);
    }

    /**
     * Gets the attribute value for REV_NO using the alias name RevNo.
     * @return the REV_NO
     */
    public BigDecimal getRevNo() {
        return (BigDecimal) getAttributeInternal(REVNO);
    }

    /**
     * Sets <code>value</code> as attribute value for REV_NO using the alias name RevNo.
     * @param value value to set the REV_NO
     */
    public void setRevNo(BigDecimal value) {
        setAttributeInternal(REVNO, value);
    }

    /**
     * Gets the attribute value for SLIP_ID using the alias name SlipId.
     * @return the SLIP_ID
     */
    public BigDecimal getSlipId() {
        return (BigDecimal) getAttributeInternal(SLIPID);
    }

    /**
     * Sets <code>value</code> as attribute value for SLIP_ID using the alias name SlipId.
     * @param value value to set the SLIP_ID
     */
    public void setSlipId(BigDecimal value) {
        setAttributeInternal(SLIPID, value);
    }

    /**
     * Gets the attribute value for SLIP_LINE_ID using the alias name SlipLineId.
     * @return the SLIP_LINE_ID
     */
    public BigDecimal getSlipLineId() {
        return (BigDecimal) getAttributeInternal(SLIPLINEID);
    }

    /**
     * Sets <code>value</code> as attribute value for SLIP_LINE_ID using the alias name SlipLineId.
     * @param value value to set the SLIP_LINE_ID
     */
    public void setSlipLineId(BigDecimal value) {
        setAttributeInternal(SLIPLINEID, value);
    }

    /**
     * Gets the attribute value for STOCK_QTY using the alias name StockQty.
     * @return the STOCK_QTY
     */
    public BigDecimal getStockQty() {
        return (BigDecimal) getAttributeInternal(STOCKQTY);
    }

    /**
     * Sets <code>value</code> as attribute value for STOCK_QTY using the alias name StockQty.
     * @param value value to set the STOCK_QTY
     */
    public void setStockQty(BigDecimal value) {
        setAttributeInternal(STOCKQTY, value);
    }

    /**
     * Gets the attribute value for STR_QTY using the alias name StrQty.
     * @return the STR_QTY
     */
    public BigDecimal getStrQty() {
        return (BigDecimal) getAttributeInternal(STRQTY);
    }

    /**
     * Sets <code>value</code> as attribute value for STR_QTY using the alias name StrQty.
     * @param value value to set the STR_QTY
     */
    public void setStrQty(BigDecimal value) {
        setAttributeInternal(STRQTY, value);
    }

    /**
     * Gets the attribute value for STR_UOM using the alias name StrUom.
     * @return the STR_UOM
     */
    public String getStrUom() {
        return (String) getAttributeInternal(STRUOM);
    }

    /**
     * Sets <code>value</code> as attribute value for STR_UOM using the alias name StrUom.
     * @param value value to set the STR_UOM
     */
    public void setStrUom(String value) {
        setAttributeInternal(STRUOM, value);
    }

    /**
     * Gets the attribute value for UOM using the alias name Uom.
     * @return the UOM
     */
    public String getUom() {
        return (String) getAttributeInternal(UOM);
    }

    /**
     * Sets <code>value</code> as attribute value for UOM using the alias name Uom.
     * @param value value to set the UOM
     */
    public void setUom(String value) {
        setAttributeInternal(UOM, value);
    }

    /**
     * Gets the attribute value for WEIGHT using the alias name Weight.
     * @return the WEIGHT
     */
    public BigDecimal getWeight() {
        return (BigDecimal) getAttributeInternal(WEIGHT);
    }

    /**
     * Sets <code>value</code> as attribute value for WEIGHT using the alias name Weight.
     * @param value value to set the WEIGHT
     */
    public void setWeight(BigDecimal value) {
        setAttributeInternal(WEIGHT, value);
    }

    /**
     * Gets the attribute value for WIDTH using the alias name Width.
     * @return the WIDTH
     */
    public BigDecimal getWidth() {
        return (BigDecimal) getAttributeInternal(WIDTH);
    }

    /**
     * Sets <code>value</code> as attribute value for WIDTH using the alias name Width.
     * @param value value to set the WIDTH
     */
    public void setWidth(BigDecimal value) {
        setAttributeInternal(WIDTH, value);
    }

    /**
     * Gets the attribute value for ISS_FOR using the alias name IssFor.
     * @return the ISS_FOR
     */
    public String getIssFor() {
        return (String) getAttributeInternal(ISSFOR);
    }

    /**
     * Sets <code>value</code> as attribute value for ISS_FOR using the alias name IssFor.
     * @param value value to set the ISS_FOR
     */
    public void setIssFor(String value) {
        setAttributeInternal(ISSFOR, value);
    }

    /**
     * Gets the attribute value for ISS_TO using the alias name IssTo.
     * @return the ISS_TO
     */
    public String getIssTo() {
        return (String) getAttributeInternal(ISSTO);
    }

    /**
     * Sets <code>value</code> as attribute value for ISS_TO using the alias name IssTo.
     * @param value value to set the ISS_TO
     */
    public void setIssTo(String value) {
        setAttributeInternal(ISSTO, value);
    }

    /**
     * Gets the attribute value for ISS_TYPE using the alias name IssType.
     * @return the ISS_TYPE
     */
    public String getIssType() {
        return (String) getAttributeInternal(ISSTYPE);
    }

    /**
     * Sets <code>value</code> as attribute value for ISS_TYPE using the alias name IssType.
     * @param value value to set the ISS_TYPE
     */
    public void setIssType(String value) {
        setAttributeInternal(ISSTYPE, value);
    }

    /**
     * Gets the attribute value for UNIT_CD using the alias name UnitCd.
     * @return the UNIT_CD
     */
    public String getUnitCd() {
        return (String) getAttributeInternal(UNITCD);
    }

    /**
     * Sets <code>value</code> as attribute value for UNIT_CD using the alias name UnitCd.
     * @param value value to set the UNIT_CD
     */
    public void setUnitCd(String value) {
        setAttributeInternal(UNITCD, value);
    }

    /**
     * Gets the attribute value for SLIP_ID using the alias name SlipId1.
     * @return the SLIP_ID
     */
    public Long getSlipId1() {
        return (Long) getAttributeInternal(SLIPID1);
    }

    /**
     * Sets <code>value</code> as attribute value for SLIP_ID using the alias name SlipId1.
     * @param value value to set the SLIP_ID
     */
    public void setSlipId1(Long value) {
        setAttributeInternal(SLIPID1, value);
    }

    /**
     * Gets the attribute value for LONG_DESCRIP using the alias name LongDescrip.
     * @return the LONG_DESCRIP
     */
    public String getLongDescrip() {
        return (String) getAttributeInternal(LONGDESCRIP);
    }

    /**
     * Sets <code>value</code> as attribute value for LONG_DESCRIP using the alias name LongDescrip.
     * @param value value to set the LONG_DESCRIP
     */
    public void setLongDescrip(String value) {
        setAttributeInternal(LONGDESCRIP, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber1.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber1() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER1);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber1.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber1(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER1, value);
    }

    /**
     * Gets the attribute value for PROC_ID using the alias name ProcId.
     * @return the PROC_ID
     */
    public Long getProcId() {
        return (Long) getAttributeInternal(PROCID);
    }

    /**
     * Sets <code>value</code> as attribute value for PROC_ID using the alias name ProcId.
     * @param value value to set the PROC_ID
     */
    public void setProcId(Long value) {
        setAttributeInternal(PROCID, value);
    }

    /**
     * Gets the attribute value for SHORT_DESCRIP using the alias name ShortDescrip.
     * @return the SHORT_DESCRIP
     */
    public String getShortDescrip() {
        return (String) getAttributeInternal(SHORTDESCRIP);
    }

    /**
     * Sets <code>value</code> as attribute value for SHORT_DESCRIP using the alias name ShortDescrip.
     * @param value value to set the SHORT_DESCRIP
     */
    public void setShortDescrip(String value) {
        setAttributeInternal(SHORTDESCRIP, value);
    }


    /**
     * Gets the attribute value for ITEM_CD using the alias name ItemCd1.
     * @return the ITEM_CD
     */
    public String getItemCd1() {
        return (String) getAttributeInternal(ITEMCD1);
    }

    /**
     * Sets <code>value</code> as attribute value for ITEM_CD using the alias name ItemCd1.
     * @param value value to set the ITEM_CD
     */
    public void setItemCd1(String value) {
        setAttributeInternal(ITEMCD1, value);
    }

    /**
     * Gets the attribute value for ITEM_DESC using the alias name ItemDesc.
     * @return the ITEM_DESC
     */
    public String getItemDesc() {
        return (String) getAttributeInternal(ITEMDESC);
    }

    /**
     * Sets <code>value</code> as attribute value for ITEM_DESC using the alias name ItemDesc.
     * @param value value to set the ITEM_DESC
     */
    public void setItemDesc(String value) {
        setAttributeInternal(ITEMDESC, value);
    }


    /**
     * Gets the view accessor <code>RowSet</code> IssueSlipDocumentNumberVVO1.
     */
    public RowSet getIssueSlipDocumentNumberVVO1() {
        return (RowSet) getAttributeInternal(ISSUESLIPDOCUMENTNUMBERVVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> IssueSlipBatchLotNoLovVVO1.
     */
    public RowSet getIssueSlipBatchLotNoLovVVO1() {
        return (RowSet) getAttributeInternal(ISSUESLIPBATCHLOTNOLOVVVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> IssueSlipItemCodeVVO1.
     */
    public RowSet getIssueSlipItemCodeVVO1() {
        return (RowSet) getAttributeInternal(ISSUESLIPITEMCODEVVO1);
    }
}

