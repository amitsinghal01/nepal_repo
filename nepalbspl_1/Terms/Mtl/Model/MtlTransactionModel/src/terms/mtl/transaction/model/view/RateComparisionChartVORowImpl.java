package terms.mtl.transaction.model.view;

import oracle.jbo.domain.Timestamp;

import oracle.jbo.RowIterator;
import oracle.jbo.RowSet;
import oracle.jbo.server.EntityImpl;
import oracle.jbo.server.ViewRowImpl;

import terms.hrm.setup.model.entity.EmployeeMasterEOImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Tue Aug 28 12:52:37 IST 2018
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class RateComparisionChartVORowImpl extends ViewRowImpl {
    public static final int ENTITY_RATECOMPARISIONCHARTEO = 0;
    public static final int ENTITY_UNITEO = 1;
    public static final int ENTITY_EMPLOYEEMASTEREO = 2;
    public static final int ENTITY_EMPLOYEEMASTEREO1 = 3;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        CompNo,
        CompDt,
        Quot1,
        Quot2,
        Quot3,
        Quot4,
        Quot5,
        SystemName,
        ApproveBy,
        UnitCd,
        CreatedBy,
        CreationDate,
        LastUpdatedBy,
        LastUpdateDate,
        ObjectVersionNumber,
        CompId,
        Name,
        Code,
        ObjectVersionNumber1,
        EditTrans,
        EmpFirstName,
        EmpNumber,
        ObjectVersionNumber2,
        EmpLastName,
        ModifiedDate,
        ModifiedBy,
        EmpFirstName1,
        EmpLastName1,
        EmpNumber1,
        ObjectVersionNumber3,
        Quot11,
        Quot21,
        Quot31,
        Quot41,
        Quot51,
        QuotTypTrans,
        QuotType,
        QuotCheck1,
        QuotCheck2,
        QuotCheck3,
        QuotCheck4,
        QuotCheck5,
        RateComparisonDtlVO,
        RateCompareVO,
        UnitVO1,
        ItemCodeRateComparisionChartVVO1,
        QuotationNumberRateComparisionVVO1,
        QuotationNumberRateComparisionVVO2,
        QuotationNumberRateComparisionVVO3,
        QuotationNumberRateComparisionVVO4,
        QuotationNumberRateComparisionVVO5,
        ApprovedByEmpViewVVO1,
        ModifiedByEmpViewVVO1,
        ContractRateQuotationNoVVO1,
        ContractRateQuotationNoVVO2,
        ContractRateQuotationNoVVO3,
        ContractRateQuotationNoVVO4,
        ContractRateQuotationNoVVO5;
        static AttributesEnum[] vals = null;
        ;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int COMPNO = AttributesEnum.CompNo.index();
    public static final int COMPDT = AttributesEnum.CompDt.index();
    public static final int QUOT1 = AttributesEnum.Quot1.index();
    public static final int QUOT2 = AttributesEnum.Quot2.index();
    public static final int QUOT3 = AttributesEnum.Quot3.index();
    public static final int QUOT4 = AttributesEnum.Quot4.index();
    public static final int QUOT5 = AttributesEnum.Quot5.index();
    public static final int SYSTEMNAME = AttributesEnum.SystemName.index();
    public static final int APPROVEBY = AttributesEnum.ApproveBy.index();
    public static final int UNITCD = AttributesEnum.UnitCd.index();
    public static final int CREATEDBY = AttributesEnum.CreatedBy.index();
    public static final int CREATIONDATE = AttributesEnum.CreationDate.index();
    public static final int LASTUPDATEDBY = AttributesEnum.LastUpdatedBy.index();
    public static final int LASTUPDATEDATE = AttributesEnum.LastUpdateDate.index();
    public static final int OBJECTVERSIONNUMBER = AttributesEnum.ObjectVersionNumber.index();
    public static final int COMPID = AttributesEnum.CompId.index();
    public static final int NAME = AttributesEnum.Name.index();
    public static final int CODE = AttributesEnum.Code.index();
    public static final int OBJECTVERSIONNUMBER1 = AttributesEnum.ObjectVersionNumber1.index();
    public static final int EDITTRANS = AttributesEnum.EditTrans.index();
    public static final int EMPFIRSTNAME = AttributesEnum.EmpFirstName.index();
    public static final int EMPNUMBER = AttributesEnum.EmpNumber.index();
    public static final int OBJECTVERSIONNUMBER2 = AttributesEnum.ObjectVersionNumber2.index();
    public static final int EMPLASTNAME = AttributesEnum.EmpLastName.index();
    public static final int MODIFIEDDATE = AttributesEnum.ModifiedDate.index();
    public static final int MODIFIEDBY = AttributesEnum.ModifiedBy.index();
    public static final int EMPFIRSTNAME1 = AttributesEnum.EmpFirstName1.index();
    public static final int EMPLASTNAME1 = AttributesEnum.EmpLastName1.index();
    public static final int EMPNUMBER1 = AttributesEnum.EmpNumber1.index();
    public static final int OBJECTVERSIONNUMBER3 = AttributesEnum.ObjectVersionNumber3.index();
    public static final int QUOT11 = AttributesEnum.Quot11.index();
    public static final int QUOT21 = AttributesEnum.Quot21.index();
    public static final int QUOT31 = AttributesEnum.Quot31.index();
    public static final int QUOT41 = AttributesEnum.Quot41.index();
    public static final int QUOT51 = AttributesEnum.Quot51.index();
    public static final int QUOTTYPTRANS = AttributesEnum.QuotTypTrans.index();
    public static final int QUOTTYPE = AttributesEnum.QuotType.index();
    public static final int QUOTCHECK1 = AttributesEnum.QuotCheck1.index();
    public static final int QUOTCHECK2 = AttributesEnum.QuotCheck2.index();
    public static final int QUOTCHECK3 = AttributesEnum.QuotCheck3.index();
    public static final int QUOTCHECK4 = AttributesEnum.QuotCheck4.index();
    public static final int QUOTCHECK5 = AttributesEnum.QuotCheck5.index();
    public static final int RATECOMPARISONDTLVO = AttributesEnum.RateComparisonDtlVO.index();
    public static final int RATECOMPAREVO = AttributesEnum.RateCompareVO.index();
    public static final int UNITVO1 = AttributesEnum.UnitVO1.index();
    public static final int ITEMCODERATECOMPARISIONCHARTVVO1 = AttributesEnum.ItemCodeRateComparisionChartVVO1.index();
    public static final int QUOTATIONNUMBERRATECOMPARISIONVVO1 =
        AttributesEnum.QuotationNumberRateComparisionVVO1.index();
    public static final int QUOTATIONNUMBERRATECOMPARISIONVVO2 =
        AttributesEnum.QuotationNumberRateComparisionVVO2.index();
    public static final int QUOTATIONNUMBERRATECOMPARISIONVVO3 =
        AttributesEnum.QuotationNumberRateComparisionVVO3.index();
    public static final int QUOTATIONNUMBERRATECOMPARISIONVVO4 =
        AttributesEnum.QuotationNumberRateComparisionVVO4.index();
    public static final int QUOTATIONNUMBERRATECOMPARISIONVVO5 =
        AttributesEnum.QuotationNumberRateComparisionVVO5.index();
    public static final int APPROVEDBYEMPVIEWVVO1 = AttributesEnum.ApprovedByEmpViewVVO1.index();
    public static final int MODIFIEDBYEMPVIEWVVO1 = AttributesEnum.ModifiedByEmpViewVVO1.index();
    public static final int CONTRACTRATEQUOTATIONNOVVO1 = AttributesEnum.ContractRateQuotationNoVVO1.index();
    public static final int CONTRACTRATEQUOTATIONNOVVO2 = AttributesEnum.ContractRateQuotationNoVVO2.index();
    public static final int CONTRACTRATEQUOTATIONNOVVO3 = AttributesEnum.ContractRateQuotationNoVVO3.index();
    public static final int CONTRACTRATEQUOTATIONNOVVO4 = AttributesEnum.ContractRateQuotationNoVVO4.index();
    public static final int CONTRACTRATEQUOTATIONNOVVO5 = AttributesEnum.ContractRateQuotationNoVVO5.index();

    /**
     * This is the default constructor (do not remove).
     */
    public RateComparisionChartVORowImpl() {
    }

    /**
     * Gets RateComparisionChartEO entity object.
     * @return the RateComparisionChartEO
     */
    public EntityImpl getRateComparisionChartEO() {
        return (EntityImpl) getEntity(ENTITY_RATECOMPARISIONCHARTEO);
    }

    /**
     * Gets UnitEO entity object.
     * @return the UnitEO
     */
    public EntityImpl getUnitEO() {
        return (EntityImpl) getEntity(ENTITY_UNITEO);
    }

    /**
     * Gets EmployeeMasterEO entity object.
     * @return the EmployeeMasterEO
     */
    public EmployeeMasterEOImpl getEmployeeMasterEO() {
        return (EmployeeMasterEOImpl) getEntity(ENTITY_EMPLOYEEMASTEREO);
    }

    /**
     * Gets EmployeeMasterEO1 entity object.
     * @return the EmployeeMasterEO1
     */
    public EmployeeMasterEOImpl getEmployeeMasterEO1() {
        return (EmployeeMasterEOImpl) getEntity(ENTITY_EMPLOYEEMASTEREO1);
    }

    /**
     * Gets the attribute value for COMP_NO using the alias name CompNo.
     * @return the COMP_NO
     */
    public String getCompNo() {
        return (String) getAttributeInternal(COMPNO);
    }

    /**
     * Sets <code>value</code> as attribute value for COMP_NO using the alias name CompNo.
     * @param value value to set the COMP_NO
     */
    public void setCompNo(String value) {
        setAttributeInternal(COMPNO, value);
    }

    /**
     * Gets the attribute value for COMP_DT using the alias name CompDt.
     * @return the COMP_DT
     */
    public Timestamp getCompDt() {
        return (Timestamp) getAttributeInternal(COMPDT);
    }

    /**
     * Sets <code>value</code> as attribute value for COMP_DT using the alias name CompDt.
     * @param value value to set the COMP_DT
     */
    public void setCompDt(Timestamp value) {
        setAttributeInternal(COMPDT, value);
    }

    /**
     * Gets the attribute value for QUOT1 using the alias name Quot1.
     * @return the QUOT1
     */
    public String getQuot1() {
        return (String) getAttributeInternal(QUOT1);
    }

    /**
     * Sets <code>value</code> as attribute value for QUOT1 using the alias name Quot1.
     * @param value value to set the QUOT1
     */
    public void setQuot1(String value) {
        setAttributeInternal(QUOT1, value);
    }

    /**
     * Gets the attribute value for QUOT2 using the alias name Quot2.
     * @return the QUOT2
     */
    public String getQuot2() {
        return (String) getAttributeInternal(QUOT2);
    }

    /**
     * Sets <code>value</code> as attribute value for QUOT2 using the alias name Quot2.
     * @param value value to set the QUOT2
     */
    public void setQuot2(String value) {
        setAttributeInternal(QUOT2, value);
    }

    /**
     * Gets the attribute value for QUOT3 using the alias name Quot3.
     * @return the QUOT3
     */
    public String getQuot3() {
        return (String) getAttributeInternal(QUOT3);
    }

    /**
     * Sets <code>value</code> as attribute value for QUOT3 using the alias name Quot3.
     * @param value value to set the QUOT3
     */
    public void setQuot3(String value) {
        setAttributeInternal(QUOT3, value);
    }

    /**
     * Gets the attribute value for QUOT4 using the alias name Quot4.
     * @return the QUOT4
     */
    public String getQuot4() {
        return (String) getAttributeInternal(QUOT4);
    }

    /**
     * Sets <code>value</code> as attribute value for QUOT4 using the alias name Quot4.
     * @param value value to set the QUOT4
     */
    public void setQuot4(String value) {
        setAttributeInternal(QUOT4, value);
    }

    /**
     * Gets the attribute value for QUOT5 using the alias name Quot5.
     * @return the QUOT5
     */
    public String getQuot5() {
        return (String) getAttributeInternal(QUOT5);
    }

    /**
     * Sets <code>value</code> as attribute value for QUOT5 using the alias name Quot5.
     * @param value value to set the QUOT5
     */
    public void setQuot5(String value) {
        setAttributeInternal(QUOT5, value);
    }

    /**
     * Gets the attribute value for SYSTEM_NAME using the alias name SystemName.
     * @return the SYSTEM_NAME
     */
    public String getSystemName() {
        return (String) getAttributeInternal(SYSTEMNAME);
    }

    /**
     * Sets <code>value</code> as attribute value for SYSTEM_NAME using the alias name SystemName.
     * @param value value to set the SYSTEM_NAME
     */
    public void setSystemName(String value) {
        setAttributeInternal(SYSTEMNAME, value);
    }

    /**
     * Gets the attribute value for APPROVE_BY using the alias name ApproveBy.
     * @return the APPROVE_BY
     */
    public String getApproveBy() {
        return (String) getAttributeInternal(APPROVEBY);
    }

    /**
     * Sets <code>value</code> as attribute value for APPROVE_BY using the alias name ApproveBy.
     * @param value value to set the APPROVE_BY
     */
    public void setApproveBy(String value) {
        setAttributeInternal(APPROVEBY, value);
    }

    /**
     * Gets the attribute value for UNIT_CD using the alias name UnitCd.
     * @return the UNIT_CD
     */
    public String getUnitCd() {
        return (String) getAttributeInternal(UNITCD);
    }

    /**
     * Sets <code>value</code> as attribute value for UNIT_CD using the alias name UnitCd.
     * @param value value to set the UNIT_CD
     */
    public void setUnitCd(String value) {
        setAttributeInternal(UNITCD, value);
    }

    /**
     * Gets the attribute value for CREATED_BY using the alias name CreatedBy.
     * @return the CREATED_BY
     */
    public String getCreatedBy() {
        return (String) getAttributeInternal(CREATEDBY);
    }

    /**
     * Sets <code>value</code> as attribute value for CREATED_BY using the alias name CreatedBy.
     * @param value value to set the CREATED_BY
     */
    public void setCreatedBy(String value) {
        setAttributeInternal(CREATEDBY, value);
    }

    /**
     * Gets the attribute value for CREATION_DATE using the alias name CreationDate.
     * @return the CREATION_DATE
     */
    public Timestamp getCreationDate() {
        return (Timestamp) getAttributeInternal(CREATIONDATE);
    }

    /**
     * Gets the attribute value for LAST_UPDATED_BY using the alias name LastUpdatedBy.
     * @return the LAST_UPDATED_BY
     */
    public String getLastUpdatedBy() {
        return (String) getAttributeInternal(LASTUPDATEDBY);
    }

    /**
     * Sets <code>value</code> as attribute value for LAST_UPDATED_BY using the alias name LastUpdatedBy.
     * @param value value to set the LAST_UPDATED_BY
     */
    public void setLastUpdatedBy(String value) {
        setAttributeInternal(LASTUPDATEDBY, value);
    }

    /**
     * Gets the attribute value for LAST_UPDATE_DATE using the alias name LastUpdateDate.
     * @return the LAST_UPDATE_DATE
     */
    public Timestamp getLastUpdateDate() {
        return (Timestamp) getAttributeInternal(LASTUPDATEDATE);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER, value);
    }

    /**
     * Gets the attribute value for COMP_ID using the alias name CompId.
     * @return the COMP_ID
     */
    public Long getCompId() {
        return (Long) getAttributeInternal(COMPID);
    }

    /**
     * Sets <code>value</code> as attribute value for COMP_ID using the alias name CompId.
     * @param value value to set the COMP_ID
     */
    public void setCompId(Long value) {
        setAttributeInternal(COMPID, value);
    }

    /**
     * Gets the attribute value for NAME using the alias name Name.
     * @return the NAME
     */
    public String getName() {
        return (String) getAttributeInternal(NAME);
    }

    /**
     * Sets <code>value</code> as attribute value for NAME using the alias name Name.
     * @param value value to set the NAME
     */
    public void setName(String value) {
        setAttributeInternal(NAME, value);
    }

    /**
     * Gets the attribute value for CODE using the alias name Code.
     * @return the CODE
     */
    public String getCode() {
        return (String) getAttributeInternal(CODE);
    }

    /**
     * Sets <code>value</code> as attribute value for CODE using the alias name Code.
     * @param value value to set the CODE
     */
    public void setCode(String value) {
        setAttributeInternal(CODE, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber1.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber1() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER1);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber1.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber1(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER1, value);
    }

    /**
     * Gets the attribute value for the calculated attribute EditTrans.
     * @return the EditTrans
     */
    public Integer getEditTrans() {
        byte entityState = this.getEntity(0).getEntityState();
        return new Integer(entityState);
        //return (String) getAttributeInternal(EDITTRANS);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute EditTrans.
     * @param value value to set the  EditTrans
     */
    public void setEditTrans(Integer value) {
        setAttributeInternal(EDITTRANS, value);
    }

    /**
     * Gets the attribute value for EMP_FIRST_NAME using the alias name EmpFirstName.
     * @return the EMP_FIRST_NAME
     */
    public String getEmpFirstName() {
        if(getApproveBy()!=null)
        {
            return (String) getAttributeInternal(EMPFIRSTNAME)+" "+getAttributeInternal(EMPLASTNAME);
        }
        return (String) getAttributeInternal(EMPFIRSTNAME);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_FIRST_NAME using the alias name EmpFirstName.
     * @param value value to set the EMP_FIRST_NAME
     */
    public void setEmpFirstName(String value) {
        setAttributeInternal(EMPFIRSTNAME, value);
    }

    /**
     * Gets the attribute value for EMP_NUMBER using the alias name EmpNumber.
     * @return the EMP_NUMBER
     */
    public String getEmpNumber() {
        return (String) getAttributeInternal(EMPNUMBER);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_NUMBER using the alias name EmpNumber.
     * @param value value to set the EMP_NUMBER
     */
    public void setEmpNumber(String value) {
        setAttributeInternal(EMPNUMBER, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber2.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber2() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER2);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber2.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber2(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER2, value);
    }

    /**
     * Gets the attribute value for EMP_LAST_NAME using the alias name EmpLastName.
     * @return the EMP_LAST_NAME
     */
    public String getEmpLastName() {
        return (String) getAttributeInternal(EMPLASTNAME);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_LAST_NAME using the alias name EmpLastName.
     * @param value value to set the EMP_LAST_NAME
     */
    public void setEmpLastName(String value) {
        setAttributeInternal(EMPLASTNAME, value);
    }

    /**
     * Gets the attribute value for MODIFIED_DATE using the alias name ModifiedDate.
     * @return the MODIFIED_DATE
     */
    public Timestamp getModifiedDate() {
        return (Timestamp) getAttributeInternal(MODIFIEDDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for MODIFIED_DATE using the alias name ModifiedDate.
     * @param value value to set the MODIFIED_DATE
     */
    public void setModifiedDate(Timestamp value) {
        setAttributeInternal(MODIFIEDDATE, value);
    }

    /**
     * Gets the attribute value for MODIFIED_BY using the alias name ModifiedBy.
     * @return the MODIFIED_BY
     */
    public String getModifiedBy() {
        return (String) getAttributeInternal(MODIFIEDBY);
    }

    /**
     * Sets <code>value</code> as attribute value for MODIFIED_BY using the alias name ModifiedBy.
     * @param value value to set the MODIFIED_BY
     */
    public void setModifiedBy(String value) {
        setAttributeInternal(MODIFIEDBY, value);
    }

    /**
     * Gets the attribute value for EMP_FIRST_NAME using the alias name EmpFirstName1.
     * @return the EMP_FIRST_NAME
     */
    public String getEmpFirstName1() {
        if(getModifiedBy()!=null)
        {
            setAttributeInternal(MODIFIEDDATE, new oracle.jbo.domain.Timestamp(System.currentTimeMillis()));
            return (String) getAttributeInternal(EMPFIRSTNAME1)+" "+getAttributeInternal(EMPLASTNAME1);     
        }
        return (String) getAttributeInternal(EMPFIRSTNAME1);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_FIRST_NAME using the alias name EmpFirstName1.
     * @param value value to set the EMP_FIRST_NAME
     */
    public void setEmpFirstName1(String value) {
        setAttributeInternal(EMPFIRSTNAME1, value);
    }

    /**
     * Gets the attribute value for EMP_LAST_NAME using the alias name EmpLastName1.
     * @return the EMP_LAST_NAME
     */
    public String getEmpLastName1() {
        return (String) getAttributeInternal(EMPLASTNAME1);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_LAST_NAME using the alias name EmpLastName1.
     * @param value value to set the EMP_LAST_NAME
     */
    public void setEmpLastName1(String value) {
        setAttributeInternal(EMPLASTNAME1, value);
    }

    /**
     * Gets the attribute value for EMP_NUMBER using the alias name EmpNumber1.
     * @return the EMP_NUMBER
     */
    public String getEmpNumber1() {
        return (String) getAttributeInternal(EMPNUMBER1);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_NUMBER using the alias name EmpNumber1.
     * @param value value to set the EMP_NUMBER
     */
    public void setEmpNumber1(String value) {
        setAttributeInternal(EMPNUMBER1, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber3.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber3() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER3);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber3.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber3(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER3, value);
    }

    /**
     * Gets the attribute value for QUOT1 using the alias name Quot11.
     * @return the QUOT1
     */
    public String getQuot11() {
        return (String) getAttributeInternal(QUOT11);
    }

    /**
     * Sets <code>value</code> as attribute value for QUOT1 using the alias name Quot11.
     * @param value value to set the QUOT1
     */
    public void setQuot11(String value) {
        setAttributeInternal(QUOT11, value);
    }

    /**
     * Gets the attribute value for QUOT2 using the alias name Quot21.
     * @return the QUOT2
     */
    public String getQuot21() {
        return (String) getAttributeInternal(QUOT21);
    }

    /**
     * Sets <code>value</code> as attribute value for QUOT2 using the alias name Quot21.
     * @param value value to set the QUOT2
     */
    public void setQuot21(String value) {
        setAttributeInternal(QUOT21, value);
    }

    /**
     * Gets the attribute value for QUOT3 using the alias name Quot31.
     * @return the QUOT3
     */
    public String getQuot31() {
        return (String) getAttributeInternal(QUOT31);
    }

    /**
     * Sets <code>value</code> as attribute value for QUOT3 using the alias name Quot31.
     * @param value value to set the QUOT3
     */
    public void setQuot31(String value) {
        setAttributeInternal(QUOT31, value);
    }

    /**
     * Gets the attribute value for QUOT4 using the alias name Quot41.
     * @return the QUOT4
     */
    public String getQuot41() {
        return (String) getAttributeInternal(QUOT41);
    }

    /**
     * Sets <code>value</code> as attribute value for QUOT4 using the alias name Quot41.
     * @param value value to set the QUOT4
     */
    public void setQuot41(String value) {
        setAttributeInternal(QUOT41, value);
    }

    /**
     * Gets the attribute value for QUOT5 using the alias name Quot51.
     * @return the QUOT5
     */
    public String getQuot51() {
        return (String) getAttributeInternal(QUOT51);
    }

    /**
     * Sets <code>value</code> as attribute value for QUOT5 using the alias name Quot51.
     * @param value value to set the QUOT5
     */
    public void setQuot51(String value) {
        setAttributeInternal(QUOT51, value);
    }

    /**
     * Gets the attribute value for the calculated attribute QuotTypTrans.
     * @return the QuotTypTrans
     */
    public String getQuotTypTrans() {
        return (String) getAttributeInternal(QUOTTYPTRANS);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute QuotTypTrans.
     * @param value value to set the  QuotTypTrans
     */
    public void setQuotTypTrans(String value) {
        setAttributeInternal(QUOTTYPTRANS, value);
    }

    /**
     * Gets the attribute value for QUOT_TYPE using the alias name QuotType.
     * @return the QUOT_TYPE
     */
    public String getQuotType() {
        return (String) getAttributeInternal(QUOTTYPE);
    }

    /**
     * Sets <code>value</code> as attribute value for QUOT_TYPE using the alias name QuotType.
     * @param value value to set the QUOT_TYPE
     */
    public void setQuotType(String value) {
        setAttributeInternal(QUOTTYPE, value);
    }

    /**
     * Gets the attribute value for QUOT_CHECK1 using the alias name QuotCheck1.
     * @return the QUOT_CHECK1
     */
    public String getQuotCheck1() {
        return (String) getAttributeInternal(QUOTCHECK1);
    }

    /**
     * Sets <code>value</code> as attribute value for QUOT_CHECK1 using the alias name QuotCheck1.
     * @param value value to set the QUOT_CHECK1
     */
    public void setQuotCheck1(String value) {
        setAttributeInternal(QUOTCHECK1, value);
    }

    /**
     * Gets the attribute value for QUOT_CHECK2 using the alias name QuotCheck2.
     * @return the QUOT_CHECK2
     */
    public String getQuotCheck2() {
        return (String) getAttributeInternal(QUOTCHECK2);
    }

    /**
     * Sets <code>value</code> as attribute value for QUOT_CHECK2 using the alias name QuotCheck2.
     * @param value value to set the QUOT_CHECK2
     */
    public void setQuotCheck2(String value) {
        setAttributeInternal(QUOTCHECK2, value);
    }

    /**
     * Gets the attribute value for QUOT_CHECK3 using the alias name QuotCheck3.
     * @return the QUOT_CHECK3
     */
    public String getQuotCheck3() {
        return (String) getAttributeInternal(QUOTCHECK3);
    }

    /**
     * Sets <code>value</code> as attribute value for QUOT_CHECK3 using the alias name QuotCheck3.
     * @param value value to set the QUOT_CHECK3
     */
    public void setQuotCheck3(String value) {
        setAttributeInternal(QUOTCHECK3, value);
    }

    /**
     * Gets the attribute value for QUOT_CHECK4 using the alias name QuotCheck4.
     * @return the QUOT_CHECK4
     */
    public String getQuotCheck4() {
        return (String) getAttributeInternal(QUOTCHECK4);
    }

    /**
     * Sets <code>value</code> as attribute value for QUOT_CHECK4 using the alias name QuotCheck4.
     * @param value value to set the QUOT_CHECK4
     */
    public void setQuotCheck4(String value) {
        setAttributeInternal(QUOTCHECK4, value);
    }

    /**
     * Gets the attribute value for QUOT_CHECK5 using the alias name QuotCheck5.
     * @return the QUOT_CHECK5
     */
    public String getQuotCheck5() {
        return (String) getAttributeInternal(QUOTCHECK5);
    }

    /**
     * Sets <code>value</code> as attribute value for QUOT_CHECK5 using the alias name QuotCheck5.
     * @param value value to set the QUOT_CHECK5
     */
    public void setQuotCheck5(String value) {
        setAttributeInternal(QUOTCHECK5, value);
    }

    /**
     * Gets the associated <code>RowIterator</code> using master-detail link RateComparisonDtlVO.
     */
    public RowIterator getRateComparisonDtlVO() {
        return (RowIterator) getAttributeInternal(RATECOMPARISONDTLVO);
    }

    /**
     * Gets the associated <code>RowIterator</code> using master-detail link RateCompareVO.
     */
    public RowIterator getRateCompareVO() {
        return (RowIterator) getAttributeInternal(RATECOMPAREVO);
    }

    /**
     * Gets the view accessor <code>RowSet</code> UnitVO1.
     */
    public RowSet getUnitVO1() {
        return (RowSet) getAttributeInternal(UNITVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> ItemCodeRateComparisionChartVVO1.
     */
    public RowSet getItemCodeRateComparisionChartVVO1() {
        return (RowSet) getAttributeInternal(ITEMCODERATECOMPARISIONCHARTVVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> QuotationNumberRateComparisionVVO1.
     */
    public RowSet getQuotationNumberRateComparisionVVO1() {
        return (RowSet) getAttributeInternal(QUOTATIONNUMBERRATECOMPARISIONVVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> QuotationNumberRateComparisionVVO2.
     */
    public RowSet getQuotationNumberRateComparisionVVO2() {
        return (RowSet) getAttributeInternal(QUOTATIONNUMBERRATECOMPARISIONVVO2);
    }

    /**
     * Gets the view accessor <code>RowSet</code> QuotationNumberRateComparisionVVO3.
     */
    public RowSet getQuotationNumberRateComparisionVVO3() {
        return (RowSet) getAttributeInternal(QUOTATIONNUMBERRATECOMPARISIONVVO3);
    }

    /**
     * Gets the view accessor <code>RowSet</code> QuotationNumberRateComparisionVVO4.
     */
    public RowSet getQuotationNumberRateComparisionVVO4() {
        return (RowSet) getAttributeInternal(QUOTATIONNUMBERRATECOMPARISIONVVO4);
    }

    /**
     * Gets the view accessor <code>RowSet</code> QuotationNumberRateComparisionVVO5.
     */
    public RowSet getQuotationNumberRateComparisionVVO5() {
        return (RowSet) getAttributeInternal(QUOTATIONNUMBERRATECOMPARISIONVVO5);
    }

    /**
     * Gets the view accessor <code>RowSet</code> ApprovedByEmpViewVVO1.
     */
    public RowSet getApprovedByEmpViewVVO1() {
        return (RowSet) getAttributeInternal(APPROVEDBYEMPVIEWVVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> ModifiedByEmpViewVVO1.
     */
    public RowSet getModifiedByEmpViewVVO1() {
        return (RowSet) getAttributeInternal(MODIFIEDBYEMPVIEWVVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> ContractRateQuotationNoVVO1.
     */
    public RowSet getContractRateQuotationNoVVO1() {
        return (RowSet) getAttributeInternal(CONTRACTRATEQUOTATIONNOVVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> ContractRateQuotationNoVVO2.
     */
    public RowSet getContractRateQuotationNoVVO2() {
        return (RowSet) getAttributeInternal(CONTRACTRATEQUOTATIONNOVVO2);
    }

    /**
     * Gets the view accessor <code>RowSet</code> ContractRateQuotationNoVVO3.
     */
    public RowSet getContractRateQuotationNoVVO3() {
        return (RowSet) getAttributeInternal(CONTRACTRATEQUOTATIONNOVVO3);
    }

    /**
     * Gets the view accessor <code>RowSet</code> ContractRateQuotationNoVVO4.
     */
    public RowSet getContractRateQuotationNoVVO4() {
        return (RowSet) getAttributeInternal(CONTRACTRATEQUOTATIONNOVVO4);
    }

    /**
     * Gets the view accessor <code>RowSet</code> ContractRateQuotationNoVVO5.
     */
    public RowSet getContractRateQuotationNoVVO5() {
        return (RowSet) getAttributeInternal(CONTRACTRATEQUOTATIONNOVVO5);
    }
}

