package terms.mtl.transaction.model.view;

import oracle.jbo.RowSet;
import oracle.jbo.domain.Timestamp;
import oracle.jbo.server.EntityImpl;
import oracle.jbo.server.ViewRowImpl;

import terms.hrm.setup.model.entity.EmployeeMasterEOImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Tue Oct 17 17:22:12 IST 2017
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class SearchInterStoreTransferVORowImpl extends ViewRowImpl {


    public static final int ENTITY_INTERSTORETRANSFERHEADEREO = 0;
    public static final int ENTITY_UNITEO = 1;
    public static final int ENTITY_EMPLOYEEMASTEREO = 2;
    public static final int ENTITY_LOCATIONEO = 3;
    public static final int ENTITY_LOCATIONEO1 = 4;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        CreatedBy,
        CreationDate,
        LastUpdateDate,
        LastUpdatedBy,
        ObjectVersionNumber,
        TrnfBy,
        TrnfDt,
        TrnfFrom,
        TrnfId,
        TrnfNo,
        TrnfPurpose,
        TrnfTo,
        UnitCd,
        Name,
        Code,
        ObjectVersionNumber1,
        EmpFirstName,
        EmpNumber,
        ObjectVersionNumber4,
        EmpLastName,
        TrnfFrom1,
        TrnfTo1,
        TrnfNo1,
        LongDesc,
        LocatCode,
        ObjectVersionNumber2,
        LongDesc1,
        LocatCode1,
        ObjectVersionNumber3,
        ShortDesc,
        ShortDesc1,
        UnitVO1,
        TrnByEmpViewVVO1,
        TrnNoSearchInterStoreTransferVO1,
        SearchInterStoreTransferVO1,
        LocationVO1,
        LocationVO2;
        static AttributesEnum[] vals = null;
        ;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int CREATEDBY = AttributesEnum.CreatedBy.index();
    public static final int CREATIONDATE = AttributesEnum.CreationDate.index();
    public static final int LASTUPDATEDATE = AttributesEnum.LastUpdateDate.index();
    public static final int LASTUPDATEDBY = AttributesEnum.LastUpdatedBy.index();
    public static final int OBJECTVERSIONNUMBER = AttributesEnum.ObjectVersionNumber.index();
    public static final int TRNFBY = AttributesEnum.TrnfBy.index();
    public static final int TRNFDT = AttributesEnum.TrnfDt.index();
    public static final int TRNFFROM = AttributesEnum.TrnfFrom.index();
    public static final int TRNFID = AttributesEnum.TrnfId.index();
    public static final int TRNFNO = AttributesEnum.TrnfNo.index();
    public static final int TRNFPURPOSE = AttributesEnum.TrnfPurpose.index();
    public static final int TRNFTO = AttributesEnum.TrnfTo.index();
    public static final int UNITCD = AttributesEnum.UnitCd.index();
    public static final int NAME = AttributesEnum.Name.index();
    public static final int CODE = AttributesEnum.Code.index();
    public static final int OBJECTVERSIONNUMBER1 = AttributesEnum.ObjectVersionNumber1.index();
    public static final int EMPFIRSTNAME = AttributesEnum.EmpFirstName.index();
    public static final int EMPNUMBER = AttributesEnum.EmpNumber.index();
    public static final int OBJECTVERSIONNUMBER4 = AttributesEnum.ObjectVersionNumber4.index();
    public static final int EMPLASTNAME = AttributesEnum.EmpLastName.index();
    public static final int TRNFFROM1 = AttributesEnum.TrnfFrom1.index();
    public static final int TRNFTO1 = AttributesEnum.TrnfTo1.index();
    public static final int TRNFNO1 = AttributesEnum.TrnfNo1.index();
    public static final int LONGDESC = AttributesEnum.LongDesc.index();
    public static final int LOCATCODE = AttributesEnum.LocatCode.index();
    public static final int OBJECTVERSIONNUMBER2 = AttributesEnum.ObjectVersionNumber2.index();
    public static final int LONGDESC1 = AttributesEnum.LongDesc1.index();
    public static final int LOCATCODE1 = AttributesEnum.LocatCode1.index();
    public static final int OBJECTVERSIONNUMBER3 = AttributesEnum.ObjectVersionNumber3.index();
    public static final int SHORTDESC = AttributesEnum.ShortDesc.index();
    public static final int SHORTDESC1 = AttributesEnum.ShortDesc1.index();
    public static final int UNITVO1 = AttributesEnum.UnitVO1.index();
    public static final int TRNBYEMPVIEWVVO1 = AttributesEnum.TrnByEmpViewVVO1.index();
    public static final int TRNNOSEARCHINTERSTORETRANSFERVO1 = AttributesEnum.TrnNoSearchInterStoreTransferVO1.index();
    public static final int SEARCHINTERSTORETRANSFERVO1 = AttributesEnum.SearchInterStoreTransferVO1.index();
    public static final int LOCATIONVO1 = AttributesEnum.LocationVO1.index();
    public static final int LOCATIONVO2 = AttributesEnum.LocationVO2.index();

    /**
     * This is the default constructor (do not remove).
     */
    public SearchInterStoreTransferVORowImpl() {
    }

    /**
     * Gets InterStoreTransferHeaderEO entity object.
     * @return the InterStoreTransferHeaderEO
     */
    public EntityImpl getInterStoreTransferHeaderEO() {
        return (EntityImpl) getEntity(ENTITY_INTERSTORETRANSFERHEADEREO);
    }

    /**
     * Gets UnitEO entity object.
     * @return the UnitEO
     */
    public EntityImpl getUnitEO() {
        return (EntityImpl) getEntity(ENTITY_UNITEO);
    }


    /**
     * Gets EmployeeMasterEO entity object.
     * @return the EmployeeMasterEO
     */
    public EmployeeMasterEOImpl getEmployeeMasterEO() {
        return (EmployeeMasterEOImpl) getEntity(ENTITY_EMPLOYEEMASTEREO);
    }

    /**
     * Gets LocationEO entity object.
     * @return the LocationEO
     */
    public EntityImpl getLocationEO() {
        return (EntityImpl) getEntity(ENTITY_LOCATIONEO);
    }

    /**
     * Gets LocationEO1 entity object.
     * @return the LocationEO1
     */
    public EntityImpl getLocationEO1() {
        return (EntityImpl) getEntity(ENTITY_LOCATIONEO1);
    }

    /**
     * Gets the attribute value for CREATED_BY using the alias name CreatedBy.
     * @return the CREATED_BY
     */
    public String getCreatedBy() {
        return (String) getAttributeInternal(CREATEDBY);
    }

    /**
     * Gets the attribute value for CREATION_DATE using the alias name CreationDate.
     * @return the CREATION_DATE
     */
    public Timestamp getCreationDate() {
        return (Timestamp) getAttributeInternal(CREATIONDATE);
    }

    /**
     * Gets the attribute value for LAST_UPDATE_DATE using the alias name LastUpdateDate.
     * @return the LAST_UPDATE_DATE
     */
    public Timestamp getLastUpdateDate() {
        return (Timestamp) getAttributeInternal(LASTUPDATEDATE);
    }

    /**
     * Gets the attribute value for LAST_UPDATED_BY using the alias name LastUpdatedBy.
     * @return the LAST_UPDATED_BY
     */
    public String getLastUpdatedBy() {
        return (String) getAttributeInternal(LASTUPDATEDBY);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER, value);
    }

    /**
     * Gets the attribute value for TRNF_BY using the alias name TrnfBy.
     * @return the TRNF_BY
     */
    public String getTrnfBy() {
        return (String) getAttributeInternal(TRNFBY);
    }

    /**
     * Sets <code>value</code> as attribute value for TRNF_BY using the alias name TrnfBy.
     * @param value value to set the TRNF_BY
     */
    public void setTrnfBy(String value) {
        setAttributeInternal(TRNFBY, value);
    }

    /**
     * Gets the attribute value for TRNF_DT using the alias name TrnfDt.
     * @return the TRNF_DT
     */
    public Timestamp getTrnfDt() {
        return (Timestamp) getAttributeInternal(TRNFDT);
    }

    /**
     * Sets <code>value</code> as attribute value for TRNF_DT using the alias name TrnfDt.
     * @param value value to set the TRNF_DT
     */
    public void setTrnfDt(Timestamp value) {
        setAttributeInternal(TRNFDT, value);
    }

    /**
     * Gets the attribute value for TRNF_FROM using the alias name TrnfFrom.
     * @return the TRNF_FROM
     */
    public String getTrnfFrom() {
        return (String) getAttributeInternal(TRNFFROM);
    }

    /**
     * Sets <code>value</code> as attribute value for TRNF_FROM using the alias name TrnfFrom.
     * @param value value to set the TRNF_FROM
     */
    public void setTrnfFrom(String value) {
        setAttributeInternal(TRNFFROM, value);
    }

    /**
     * Gets the attribute value for TRNF_ID using the alias name TrnfId.
     * @return the TRNF_ID
     */
    public Long getTrnfId() {
        return (Long) getAttributeInternal(TRNFID);
    }

    /**
     * Sets <code>value</code> as attribute value for TRNF_ID using the alias name TrnfId.
     * @param value value to set the TRNF_ID
     */
    public void setTrnfId(Long value) {
        setAttributeInternal(TRNFID, value);
    }

    /**
     * Gets the attribute value for TRNF_NO using the alias name TrnfNo.
     * @return the TRNF_NO
     */
    public String getTrnfNo() {
        return (String) getAttributeInternal(TRNFNO);
    }

    /**
     * Sets <code>value</code> as attribute value for TRNF_NO using the alias name TrnfNo.
     * @param value value to set the TRNF_NO
     */
    public void setTrnfNo(String value) {
        setAttributeInternal(TRNFNO, value);
    }

    /**
     * Gets the attribute value for TRNF_PURPOSE using the alias name TrnfPurpose.
     * @return the TRNF_PURPOSE
     */
    public String getTrnfPurpose() {
        return (String) getAttributeInternal(TRNFPURPOSE);
    }

    /**
     * Sets <code>value</code> as attribute value for TRNF_PURPOSE using the alias name TrnfPurpose.
     * @param value value to set the TRNF_PURPOSE
     */
    public void setTrnfPurpose(String value) {
        setAttributeInternal(TRNFPURPOSE, value);
    }

    /**
     * Gets the attribute value for TRNF_TO using the alias name TrnfTo.
     * @return the TRNF_TO
     */
    public String getTrnfTo() {
        return (String) getAttributeInternal(TRNFTO);
    }

    /**
     * Sets <code>value</code> as attribute value for TRNF_TO using the alias name TrnfTo.
     * @param value value to set the TRNF_TO
     */
    public void setTrnfTo(String value) {
        setAttributeInternal(TRNFTO, value);
    }

    /**
     * Gets the attribute value for UNIT_CD using the alias name UnitCd.
     * @return the UNIT_CD
     */
    public String getUnitCd() {
        return (String) getAttributeInternal(UNITCD);
    }

    /**
     * Sets <code>value</code> as attribute value for UNIT_CD using the alias name UnitCd.
     * @param value value to set the UNIT_CD
     */
    public void setUnitCd(String value) {
        setAttributeInternal(UNITCD, value);
    }

    /**
     * Gets the attribute value for NAME using the alias name Name.
     * @return the NAME
     */
    public String getName() {
        return (String) getAttributeInternal(NAME);
    }

    /**
     * Sets <code>value</code> as attribute value for NAME using the alias name Name.
     * @param value value to set the NAME
     */
    public void setName(String value) {
        setAttributeInternal(NAME, value);
    }

    /**
     * Gets the attribute value for CODE using the alias name Code.
     * @return the CODE
     */
    public String getCode() {
        return (String) getAttributeInternal(CODE);
    }

    /**
     * Sets <code>value</code> as attribute value for CODE using the alias name Code.
     * @param value value to set the CODE
     */
    public void setCode(String value) {
        setAttributeInternal(CODE, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber1.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber1() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER1);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber1.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber1(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER1, value);
    }


    /**
     * Gets the attribute value for EMP_FIRST_NAME using the alias name EmpFirstName.
     * @return the EMP_FIRST_NAME
     */
    public String getEmpFirstName() {
        if((getAttributeInternal(EMPFIRSTNAME) !=null)
        &&(getAttributeInternal(EMPLASTNAME)!=null)){
                 return (String) getAttributeInternal(EMPFIRSTNAME)+" "+(String)
        getAttributeInternal(EMPLASTNAME);
                 } else {
                     return (String) getAttributeInternal(EMPFIRSTNAME);
                 } 
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_FIRST_NAME using the alias name EmpFirstName.
     * @param value value to set the EMP_FIRST_NAME
     */
    public void setEmpFirstName(String value) {
        setAttributeInternal(EMPFIRSTNAME, value);
    }

    /**
     * Gets the attribute value for EMP_NUMBER using the alias name EmpNumber.
     * @return the EMP_NUMBER
     */
    public String getEmpNumber() {
        return (String) getAttributeInternal(EMPNUMBER);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_NUMBER using the alias name EmpNumber.
     * @param value value to set the EMP_NUMBER
     */
    public void setEmpNumber(String value) {
        setAttributeInternal(EMPNUMBER, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber4.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber4() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER4);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber4.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber4(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER4, value);
    }

    /**
     * Gets the attribute value for EMP_LAST_NAME using the alias name EmpLastName.
     * @return the EMP_LAST_NAME
     */
    public String getEmpLastName() {
        return (String) getAttributeInternal(EMPLASTNAME);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_LAST_NAME using the alias name EmpLastName.
     * @param value value to set the EMP_LAST_NAME
     */
    public void setEmpLastName(String value) {
        setAttributeInternal(EMPLASTNAME, value);
    }

    /**
     * Gets the attribute value for TRNF_FROM using the alias name TrnfFrom1.
     * @return the TRNF_FROM
     */
    public String getTrnfFrom1() {
        return (String) getAttributeInternal(TRNFFROM1);
    }

    /**
     * Sets <code>value</code> as attribute value for TRNF_FROM using the alias name TrnfFrom1.
     * @param value value to set the TRNF_FROM
     */
    public void setTrnfFrom1(String value) {
        setAttributeInternal(TRNFFROM1, value);
    }

    /**
     * Gets the attribute value for TRNF_TO using the alias name TrnfTo1.
     * @return the TRNF_TO
     */
    public String getTrnfTo1() {
        return (String) getAttributeInternal(TRNFTO1);
    }

    /**
     * Sets <code>value</code> as attribute value for TRNF_TO using the alias name TrnfTo1.
     * @param value value to set the TRNF_TO
     */
    public void setTrnfTo1(String value) {
        setAttributeInternal(TRNFTO1, value);
    }

    /**
     * Gets the attribute value for TRNF_NO using the alias name TrnfNo1.
     * @return the TRNF_NO
     */
    public String getTrnfNo1() {
        return (String) getAttributeInternal(TRNFNO1);
    }

    /**
     * Sets <code>value</code> as attribute value for TRNF_NO using the alias name TrnfNo1.
     * @param value value to set the TRNF_NO
     */
    public void setTrnfNo1(String value) {
        setAttributeInternal(TRNFNO1, value);
    }

    /**
     * Gets the attribute value for LONG_DESC using the alias name LongDesc.
     * @return the LONG_DESC
     */
    public String getLongDesc() {
        return (String) getAttributeInternal(LONGDESC);
    }

    /**
     * Sets <code>value</code> as attribute value for LONG_DESC using the alias name LongDesc.
     * @param value value to set the LONG_DESC
     */
    public void setLongDesc(String value) {
        setAttributeInternal(LONGDESC, value);
    }

    /**
     * Gets the attribute value for LOCAT_CODE using the alias name LocatCode.
     * @return the LOCAT_CODE
     */
    public String getLocatCode() {
        return (String) getAttributeInternal(LOCATCODE);
    }

    /**
     * Sets <code>value</code> as attribute value for LOCAT_CODE using the alias name LocatCode.
     * @param value value to set the LOCAT_CODE
     */
    public void setLocatCode(String value) {
        setAttributeInternal(LOCATCODE, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber2.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber2() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER2);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber2.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber2(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER2, value);
    }

    /**
     * Gets the attribute value for LONG_DESC using the alias name LongDesc1.
     * @return the LONG_DESC
     */
    public String getLongDesc1() {
        return (String) getAttributeInternal(LONGDESC1);
    }

    /**
     * Sets <code>value</code> as attribute value for LONG_DESC using the alias name LongDesc1.
     * @param value value to set the LONG_DESC
     */
    public void setLongDesc1(String value) {
        setAttributeInternal(LONGDESC1, value);
    }

    /**
     * Gets the attribute value for LOCAT_CODE using the alias name LocatCode1.
     * @return the LOCAT_CODE
     */
    public String getLocatCode1() {
        return (String) getAttributeInternal(LOCATCODE1);
    }

    /**
     * Sets <code>value</code> as attribute value for LOCAT_CODE using the alias name LocatCode1.
     * @param value value to set the LOCAT_CODE
     */
    public void setLocatCode1(String value) {
        setAttributeInternal(LOCATCODE1, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber3.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber3() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER3);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber3.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber3(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER3, value);
    }

    /**
     * Gets the attribute value for SHORT_DESC using the alias name ShortDesc.
     * @return the SHORT_DESC
     */
    public String getShortDesc() {
        return (String) getAttributeInternal(SHORTDESC);
    }

    /**
     * Sets <code>value</code> as attribute value for SHORT_DESC using the alias name ShortDesc.
     * @param value value to set the SHORT_DESC
     */
    public void setShortDesc(String value) {
        setAttributeInternal(SHORTDESC, value);
    }

    /**
     * Gets the attribute value for SHORT_DESC using the alias name ShortDesc1.
     * @return the SHORT_DESC
     */
    public String getShortDesc1() {
        return (String) getAttributeInternal(SHORTDESC1);
    }

    /**
     * Sets <code>value</code> as attribute value for SHORT_DESC using the alias name ShortDesc1.
     * @param value value to set the SHORT_DESC
     */
    public void setShortDesc1(String value) {
        setAttributeInternal(SHORTDESC1, value);
    }

    /**
     * Gets the view accessor <code>RowSet</code> UnitVO1.
     */
    public RowSet getUnitVO1() {
        return (RowSet) getAttributeInternal(UNITVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> TrnByEmpViewVVO1.
     */
    public RowSet getTrnByEmpViewVVO1() {
        return (RowSet) getAttributeInternal(TRNBYEMPVIEWVVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> TrnNoSearchInterStoreTransferVO1.
     */
    public RowSet getTrnNoSearchInterStoreTransferVO1() {
        return (RowSet) getAttributeInternal(TRNNOSEARCHINTERSTORETRANSFERVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> SearchInterStoreTransferVO1.
     */
    public RowSet getSearchInterStoreTransferVO1() {
        return (RowSet) getAttributeInternal(SEARCHINTERSTORETRANSFERVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> LocationVO1.
     */
    public RowSet getLocationVO1() {
        return (RowSet) getAttributeInternal(LOCATIONVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> LocationVO2.
     */
    public RowSet getLocationVO2() {
        return (RowSet) getAttributeInternal(LOCATIONVO2);
    }
}

