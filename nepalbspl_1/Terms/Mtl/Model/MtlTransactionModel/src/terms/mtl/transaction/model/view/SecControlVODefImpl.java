package terms.mtl.transaction.model.view;

import oracle.jbo.server.ViewDefImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Mon Jun 19 18:17:52 IST 2017
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class SecControlVODefImpl extends ViewDefImpl {
    public SecControlVODefImpl(String name) {
        super(name);
    }

    /**
     * This is the default constructor (do not remove).
     */
    public SecControlVODefImpl() {
    }
}

