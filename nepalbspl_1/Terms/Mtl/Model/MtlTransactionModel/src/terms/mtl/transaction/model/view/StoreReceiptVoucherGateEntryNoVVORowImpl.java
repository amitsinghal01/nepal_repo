package terms.mtl.transaction.model.view;

import java.math.BigDecimal;

import java.sql.Timestamp;

import oracle.jbo.domain.Date;
import oracle.jbo.server.ViewRowImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Thu Oct 25 15:50:28 IST 2018
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class StoreReceiptVoucherGateEntryNoVVORowImpl extends ViewRowImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        ManfCode,
        Status,
        GateEno,
        GateEdt,
        PoHeadPoNo,
        Name,
        PoDt,
        VenCd,
        ChallanType,
        NoCases,
        DocNo,
        DocDt,
        PoHeadAmdNo,
        DocketNo,
        DocketDt,
        Value,
        LraNo,
        GateEtime,
        CityCode,
        PlantCode,
        BillNo,
        BillDt,
        PoVen,
        PurHindIndNo,
        ApprovedBy;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int MANFCODE = AttributesEnum.ManfCode.index();
    public static final int STATUS = AttributesEnum.Status.index();
    public static final int GATEENO = AttributesEnum.GateEno.index();
    public static final int GATEEDT = AttributesEnum.GateEdt.index();
    public static final int POHEADPONO = AttributesEnum.PoHeadPoNo.index();
    public static final int NAME = AttributesEnum.Name.index();
    public static final int PODT = AttributesEnum.PoDt.index();
    public static final int VENCD = AttributesEnum.VenCd.index();
    public static final int CHALLANTYPE = AttributesEnum.ChallanType.index();
    public static final int NOCASES = AttributesEnum.NoCases.index();
    public static final int DOCNO = AttributesEnum.DocNo.index();
    public static final int DOCDT = AttributesEnum.DocDt.index();
    public static final int POHEADAMDNO = AttributesEnum.PoHeadAmdNo.index();
    public static final int DOCKETNO = AttributesEnum.DocketNo.index();
    public static final int DOCKETDT = AttributesEnum.DocketDt.index();
    public static final int VALUE = AttributesEnum.Value.index();
    public static final int LRANO = AttributesEnum.LraNo.index();
    public static final int GATEETIME = AttributesEnum.GateEtime.index();
    public static final int CITYCODE = AttributesEnum.CityCode.index();
    public static final int PLANTCODE = AttributesEnum.PlantCode.index();
    public static final int BILLNO = AttributesEnum.BillNo.index();
    public static final int BILLDT = AttributesEnum.BillDt.index();
    public static final int POVEN = AttributesEnum.PoVen.index();
    public static final int PURHINDINDNO = AttributesEnum.PurHindIndNo.index();
    public static final int APPROVEDBY = AttributesEnum.ApprovedBy.index();

    /**
     * This is the default constructor (do not remove).
     */
    public StoreReceiptVoucherGateEntryNoVVORowImpl() {
    }
}

