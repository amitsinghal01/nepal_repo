package terms.mtl.setup.ui.bean;

import javax.faces.application.FacesMessage;

import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.JboException;

import oracle.net.aso.n;

public class AnnexureMasterBean {
    private RichTable tableBinding;
    private String editAction="V";
    private RichInputText srNoBinding;

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public AnnexureMasterBean() {
    }

    public void popUpDialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
        {
        OperationBinding op = null;
        ADFUtils.findOperation("Delete").execute();
        op = (OperationBinding) ADFUtils.findOperation("Commit");
        if (getEditAction().equals("V")) {
        Object rst = op.execute();
        }
        System.out.println("Record Delete Successfully");
        if(op.getErrors().isEmpty()){
        FacesMessage Message = new FacesMessage("Record Deleted Successfully");
        Message.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        } else if (!op.getErrors().isEmpty())
        {
        OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
        Object rstr = opr.execute();
        FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
        Message.setSeverity(FacesMessage.SEVERITY_ERROR);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(tableBinding);
    }

    public void setTableBinding(RichTable tableBinding) {
        this.tableBinding = tableBinding;
    }

    public RichTable getTableBinding() {
        return tableBinding;
    }

    public void createButtonAL(ActionEvent actionEvent) {
       ADFUtils.findOperation("CreateInsert").execute();
       OperationBinding ob= ADFUtils.findOperation("serialNoInsert");
       ob.getParamsMap().put("SrNo", ADFUtils.evaluateEL("#{bindings.AnnexureMasterVO1Iterator.estimatedRowCount}"));
       ob.execute();
    }


    public void cancelButtonAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("Rollback").execute();
                AdfFacesContext.getCurrentInstance().addPartialTarget(tableBinding);


    }

    public void setSrNoBinding(RichInputText srNoBinding) {
        this.srNoBinding = srNoBinding;
    }

    public RichInputText getSrNoBinding() {
        return srNoBinding;
    }

    public void SaveAL(ActionEvent actionEvent) {
        OperationBinding op=(OperationBinding)ADFUtils.findOperation("AnnexureLimit");
        Object ob=op.execute();
        System.out.println("Result is===>>"+op.getResult());
        if(op.getResult()!=null && op.getResult().toString().equalsIgnoreCase("N")){
            System.out.println("enter in ifffffffffffffffff");
            ADFUtils.findOperation("Commit").execute();
        }
        else{
            System.out.println("enter in else");
            ADFUtils.showMessage("This type already saved many times.Data caanot be saved.", 0);
        }
    
    }

    public void annDetailDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok")){
            AdfFacesContext.getCurrentInstance().addPartialTarget(tableBinding);
        }
    }
}
