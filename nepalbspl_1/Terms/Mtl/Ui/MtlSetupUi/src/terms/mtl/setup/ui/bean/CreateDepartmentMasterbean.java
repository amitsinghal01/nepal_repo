package terms.mtl.setup.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.binding.OperationBinding;

public class CreateDepartmentMasterbean {
//    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText departmentCodeBinding;
    private RichOutputText bindingOutputText;
    private RichButton editBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton saveALBinding;
    private RichButton saveAndCloseALBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputDate operationFormBinding;

    public CreateDepartmentMasterbean() {
    }

    public void saveAL(ActionEvent actionEvent) {
       String empcd=(String)ADFUtils.resolveExpression("#{pageFlowScope.empCode}") ; 
       System.out.println("empcd"+empcd);
        ADFUtils.setColumnValue("DepartmentMasterVO1Iterator", "ModifiedBy", empcd);
            System.out.println("Inside i1 Save");
           Integer i1=0;
              i1=(Integer)bindingOutputText.getValue();
                  System.out.println("EDIT TRANS VALUE"+i1);
                  System.out.println("Inside i1 save");
              if(i1.equals(0)){
                  ADFUtils.findOperation("getDepartCode").execute();
                  ADFUtils.findOperation("Commit").execute();
                  FacesMessage Message = new FacesMessage("Record Save Successfully. Department Code is: "
                                                          +departmentCodeBinding.getValue()+".");   
                  Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                  FacesContext fc = FacesContext.getCurrentInstance();   
                      fc.addMessage(null, Message);      
              }else{
                  ADFUtils.findOperation("Commit").execute();
                  FacesMessage Message = new FacesMessage("Record Updated Successfully");   
                  Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                  FacesContext fc = FacesContext.getCurrentInstance();   
                      fc.addMessage(null, Message);      
              }
        
//
//               String param=resolvEl("#{pageFlowScope.mode=='E'}"); 
//               System.out.println("Save Mode is ====> "+param);
//       if(param.equalsIgnoreCase("true"))
//               
//       {
//           ADFUtils.findOperation("Commit").execute();
//           FacesMessage Message = new FacesMessage("Record Updated Successfully.");
//           Message.setSeverity(FacesMessage.SEVERITY_INFO);
//           FacesContext fc = FacesContext.getCurrentInstance();
//           fc.addMessage(null, Message);
//       } 
//       else
//       {
//
//
//           ADFUtils.findOperation("Commit").execute();
//           FacesMessage Message = new FacesMessage("Record Saved Successfully");
//           Message.setSeverity(FacesMessage.SEVERITY_INFO);
//           FacesContext fc = FacesContext.getCurrentInstance();
//           fc.addMessage(null, Message);
//       }

//       if(ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E"))
//       {
//           ADFUtils.findOperation("Commit").execute();
//           FacesMessage Message = new FacesMessage("Record Updated Successfully.");
//           Message.setSeverity(FacesMessage.SEVERITY_INFO);
//           FacesContext fc = FacesContext.getCurrentInstance();
//           fc.addMessage(null, Message);
//           
//       }
//       else
//       {
//           ADFUtils.findOperation("Commit").execute();
//           FacesMessage Message=new FacesMessage("Record Saved Successfully.");
//           Message.setSeverity(FacesMessage.SEVERITY_INFO);
//           FacesContext fc=FacesContext.getCurrentInstance();
//           fc.addMessage(null, Message);
//           
//       }

       
//        
//        String param=resolvEl("#{pageFlowScope.mode=='E'}"); 
//        System.out.println("Save Mode is ====> "+param);
//        if(param.equalsIgnoreCase("true"))
//        {
//           FacesMessage Message = new FacesMessage("Record Updated Successfully");   
//           Message.setSeverity(FacesMessage.SEVERITY_INFO);   
//           FacesContext fc = FacesContext.getCurrentInstance();   
//           fc.addMessage(null, Message);      
//            
//        }
//        else 
//        {
//            FacesMessage Message = new FacesMessage("Record Saved Successfully");   
//            Message.setSeverity(FacesMessage.SEVERITY_INFO);   
//            FacesContext fc = FacesContext.getCurrentInstance();   
//            fc.addMessage(null, Message);      
//        }
//        
   }
    public String resolvEl(String data)
    {
                      FacesContext fc = FacesContext.getCurrentInstance();
                      Application app = fc.getApplication();
                      ExpressionFactory elFactory = app.getExpressionFactory();
                      ELContext elContext = fc.getELContext();
                      ValueExpression valueExp = elFactory.createValueExpression(elContext, data, Object.class);
                      String Message=valueExp.getValue(elContext).toString();
                      return Message;
    }

  

  

    public void setDepartmentCodeBinding(RichInputText departmentCodeBinding) {
        this.departmentCodeBinding = departmentCodeBinding;
    }

    public RichInputText getDepartmentCodeBinding() {
        return departmentCodeBinding;
    }
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
            try {
                Method method1 =
                    component.getClass().getMethod("setDisabled", boolean.class);
                if (method1 != null) {
                    method1.invoke(component, valueComponent);
                }
            } catch (NoSuchMethodException e) {
                try {
                    Method method =
                        component.getClass().getMethod("setReadOnly", boolean.class);
                    if (method != null) {
                        method.invoke(component, valueComponent);
                    }
                } catch (Exception e1) {
                    // e.printStackTrace();//silently eat this exception.
                }


            } catch (Exception e) {
                // e.printStackTrace();//silently eat this exception.
            }
            List<UIComponent> childComponents = component.getChildren();
            for (UIComponent comp : childComponents) {
                makeComponentHierarchyReadOnly(comp, valueComponent);
            }
        }
    public void cevModeDisableComponent(String mode) {
    if (mode.equals("E")) {
       getUnitCodeBinding().setDisabled(true);
            getDepartmentCodeBinding().setDisabled(true);
            getEditBinding().setDisabled(true);
           getSaveALBinding().setDisabled(false);
           getSaveAndCloseALBinding().setDisabled(false);
        getOperationFormBinding().setDisabled(true);
           
    } else if (mode.equals("C")) {
   getUnitCodeBinding().setDisabled(true);
            getDepartmentCodeBinding().setDisabled(true);
        getSaveALBinding().setDisabled(false);
        getSaveAndCloseALBinding().setDisabled(false);
        getOperationFormBinding().setDisabled(true);
    } else if (mode.equals("V")) {
        getSaveALBinding().setDisabled(true);
       getSaveAndCloseALBinding().setDisabled(true);
       
    }
    }

    public void EditButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }
    
    private void cevmodecheck(){
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }
    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText()
    {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setEditBinding(RichButton editBinding) {
        this.editBinding = editBinding;
    }

    public RichButton getEditBinding() {
        return editBinding;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setSaveALBinding(RichButton saveALBinding) {
        this.saveALBinding = saveALBinding;
    }

    public RichButton getSaveALBinding() {
        return saveALBinding;
    }

    public void setSaveAndCloseALBinding(RichButton saveAndCloseALBinding) {
        this.saveAndCloseALBinding = saveAndCloseALBinding;
    }

    public RichButton getSaveAndCloseALBinding() {
        return saveAndCloseALBinding;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setOperationFormBinding(RichInputDate operationFormBinding) {
        this.operationFormBinding = operationFormBinding;
    }

    public RichInputDate getOperationFormBinding() {
        return operationFormBinding;
    }
}
