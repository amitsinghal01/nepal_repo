package terms.mtl.setup.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.validator.ValidatorException;

import oracle.adf.model.OperationBinding;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;

public class CreateProjectJobDetailBean {
    private RichButton headerEditBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichOutputText bindingOutputText;
    private RichInputDate entryDtBinding;
    private RichInputComboboxListOfValues approvedByBinding;
    private RichButton saveAndCloseBinding;
    private RichButton saveBinding;
    private RichInputText amdNumberBinding;
    private RichInputDate amdDateBinding;
    private RichInputText amdAmountBinding;
    private RichInputText unitNameBinding;
    private RichInputText prepareByNameBinding;
    private RichInputText approvedByNameBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputComboboxListOfValues preparedByBinding;
    private RichInputText projectjobNoBinding;
    private RichSelectOneChoice statusBinding;
    private RichButton amendButtonBinding;
    private boolean checkbutton = true;
    private RichInputText referenceNoBinding;
    private RichInputDate referenceDateBinding;
    private RichInputText projectDetailBinding;
    private RichInputText budgetApprovalBinding;
    private RichInputComboboxListOfValues costCentreBinding;
    private RichInputDate validFromBinding;
    private RichInputDate validToBinding;

    public CreateProjectJobDetailBean() {
    }
    
    public void saveAL(ActionEvent actionEvent)
    {
        String empcd=(String)ADFUtils.resolveExpression("#{pageFlowScope.empCode}") ; 
        System.out.println("empcd"+empcd);
         ADFUtils.setColumnValue("ProjectJobDetailVO1Iterator", "LastUpdatedBy", empcd);
        Integer i=0;
        i=(Integer)bindingOutputText.getValue();
                System.out.println("-------EDIT TRANS VALUE-----> "+i);
    if(i.equals(0))
    {
        ADFUtils.findOperation("Commit").execute();
        FacesMessage Message = new FacesMessage("Record Saved Successfully.");
        Message.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        System.out.println("-------We are in Create Mode. ------");
    }
   else
    {
        ADFUtils.findOperation("Commit").execute();
        FacesMessage Message=new FacesMessage("Record Updated Successfully.");
        Message.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext fc=FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        System.out.println("-------We are in Edit Mode. ------");
    }

    }

    public void EditButtonAL(ActionEvent actionEvent) 
    {
        System.out.println("Value is=>"+statusBinding.getValue());        
        if(statusBinding.getValue().equals("C"))
        {
            ADFUtils.showMessage("Closed Document Can Not Be Changed.", 2);
        }
        else if(approvedByBinding.getValue()!=null)
        {
            ADFUtils.showMessage("Approved Document can not be changed,Only Status can be Changed/Amend.", 2);
            getAmendButtonBinding().setDisabled(false);
            getStatusBinding().setDisabled(false);
            getAmdAmountBinding().setDisabled(false);
            getSaveBinding().setDisabled(false);
            getSaveAndCloseBinding().setDisabled(false);
            costCentreBinding.setDisabled(false);
            headerEditBinding.setDisabled(true);
        }
        else
        {
            cevmodecheck();
        }
        
}
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
            try {
                Method method1 =
                    component.getClass().getMethod("setDisabled", boolean.class);
                if (method1 != null) {
                    method1.invoke(component, valueComponent);
                }
            } catch (NoSuchMethodException e) {
                try {
                    Method method =
                        component.getClass().getMethod("setReadOnly", boolean.class);
                    if (method != null) {
                        method.invoke(component, valueComponent);
                    }
                } catch (Exception e1) {
                    // e.printStackTrace();//silently eat this exception.
                }


            } catch (Exception e) {
                // e.printStackTrace();//silently eat this exception.
            }
            List<UIComponent> childComponents = component.getChildren();
            for (UIComponent comp : childComponents) {
                makeComponentHierarchyReadOnly(comp, valueComponent);
            }
        }
    private void cevmodecheck(){
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }
    public void cevModeDisableComponent(String mode) {

        if (mode.equals("E")) {

            getPreparedByBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);   
            getEntryDtBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            getSaveAndCloseBinding().setDisabled(false);
            getAmdNumberBinding().setDisabled(true);
            getAmdDateBinding().setDisabled(true);
            getAmdAmountBinding().setDisabled(true);
            getUnitNameBinding().setDisabled(true);
            getPrepareByNameBinding().setDisabled(true);
            getApprovedByNameBinding().setDisabled(true);
            getSaveBinding().setDisabled(false);
            getProjectjobNoBinding().setDisabled(true);
            getAmendButtonBinding().setDisabled(true);
        } else if (mode.equals("C")) {
            getAmendButtonBinding().setDisabled(true);
            getPreparedByBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
            getEntryDtBinding().setDisabled(true);
            getApprovedByBinding().setDisabled(true);
            getAmdNumberBinding().setDisabled(true);
            getAmdDateBinding().setDisabled(true);
            getAmdAmountBinding().setDisabled(true);
            getPrepareByNameBinding().setDisabled(true);
            getApprovedByNameBinding().setDisabled(true);
            getSaveAndCloseBinding().setDisabled(false);
            getSaveBinding().setDisabled(false);
            getAmdNumberBinding().setDisabled(true);
            getAmdDateBinding().setDisabled(true);
            getAmdAmountBinding().setDisabled(true);
            getUnitNameBinding().setDisabled(true);
            statusBinding.setDisabled(true);
        } else if (mode.equals("V")) {
//            getDetailcreateBinding().setDisabled(true);
//            getDetaildeleteBinding().setDisabled(true);
            getSaveAndCloseBinding().setDisabled(true);
            getSaveBinding().setDisabled(true);
           
        }
    }
    
    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setEntryDtBinding(RichInputDate entryDtBinding) {
        this.entryDtBinding = entryDtBinding;
    }

    public RichInputDate getEntryDtBinding() {
        return entryDtBinding;
    }

    public void setApprovedByBinding(RichInputComboboxListOfValues approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputComboboxListOfValues getApprovedByBinding() {
        return approvedByBinding;
    }

    public void setSaveAndCloseBinding(RichButton saveAndCloseBinding) {
        this.saveAndCloseBinding = saveAndCloseBinding;
    }

    public RichButton getSaveAndCloseBinding() {
        return saveAndCloseBinding;
    }

    public void setSaveBinding(RichButton saveBinding) {
        this.saveBinding = saveBinding;
    }

    public RichButton getSaveBinding() {
        return saveBinding;
    }

    public void setAmdNumberBinding(RichInputText amdNumberBinding) {
        this.amdNumberBinding = amdNumberBinding;
    }

    public RichInputText getAmdNumberBinding() {
        return amdNumberBinding;
    }

    public void setAmdDateBinding(RichInputDate amdDateBinding) {
        this.amdDateBinding = amdDateBinding;
    }

    public RichInputDate getAmdDateBinding() {
        return amdDateBinding;
    }

    public void setAmdAmountBinding(RichInputText amdAmountBinding) {
        this.amdAmountBinding = amdAmountBinding;
    }

    public RichInputText getAmdAmountBinding() {
        return amdAmountBinding;
    }

    public void setUnitNameBinding(RichInputText unitNameBinding) {
        this.unitNameBinding = unitNameBinding;
    }

    public RichInputText getUnitNameBinding() {
        return unitNameBinding;
    }

    public void setPrepareByNameBinding(RichInputText prepareByNameBinding) {
        this.prepareByNameBinding = prepareByNameBinding;
    }

    public RichInputText getPrepareByNameBinding() {
        return prepareByNameBinding;
    }

    public void setApprovedByNameBinding(RichInputText approvedByNameBinding) {
        this.approvedByNameBinding = approvedByNameBinding;
    }

    public RichInputText getApprovedByNameBinding() {
        return approvedByNameBinding;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setPreparedByBinding(RichInputComboboxListOfValues preparedByBinding) {
        this.preparedByBinding = preparedByBinding;
    }

    public RichInputComboboxListOfValues getPreparedByBinding() {
        return preparedByBinding;
    }

    public void setProjectjobNoBinding(RichInputText projectjobNoBinding) {
        this.projectjobNoBinding = projectjobNoBinding;
    }

    public RichInputText getProjectjobNoBinding() {
        return projectjobNoBinding;
    }

    public void amendmentAL(ActionEvent actionEvent) {
        if(approvedByBinding.getValue()!=null && checkbutton == true )
        {
            // && statusBinding.getValue().equals("V") 
        System.out.println("------Approved By in BEan------>>"+approvedByBinding.getValue());
        ADFUtils.findOperation("getAmendmentPJDetails").execute();
        checkbutton = false;
        }
        if(approvedByBinding.getValue()==null)
        {
            FacesMessage Message = new FacesMessage("Only Approved Document can be Amend.");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
        }
        
    }

    public void setStatusBinding(RichSelectOneChoice statusBinding) {
        this.statusBinding = statusBinding;
    }

    public RichSelectOneChoice getStatusBinding() {
        return statusBinding;
    }

    public void setAmendButtonBinding(RichButton amendButtonBinding) {
        this.amendButtonBinding = amendButtonBinding;
    }

    public RichButton getAmendButtonBinding() {
        return amendButtonBinding;
    }

    public void setReferenceNoBinding(RichInputText referenceNoBinding) {
        this.referenceNoBinding = referenceNoBinding;
    }

    public RichInputText getReferenceNoBinding() {
        return referenceNoBinding;
    }

    public void setReferenceDateBinding(RichInputDate referenceDateBinding) {
        this.referenceDateBinding = referenceDateBinding;
    }

    public RichInputDate getReferenceDateBinding() {
        return referenceDateBinding;
    }

    public void setProjectDetailBinding(RichInputText projectDetailBinding) {
        this.projectDetailBinding = projectDetailBinding;
    }

    public RichInputText getProjectDetailBinding() {
        return projectDetailBinding;
    }

    public void setBudgetApprovalBinding(RichInputText budgetApprovalBinding) {
        this.budgetApprovalBinding = budgetApprovalBinding;
    }

    public RichInputText getBudgetApprovalBinding() {
        return budgetApprovalBinding;
    }

    public void setCostCentreBinding(RichInputComboboxListOfValues costCentreBinding) {
        this.costCentreBinding = costCentreBinding;
    }

    public RichInputComboboxListOfValues getCostCentreBinding() {
        return costCentreBinding;
    }

    public void setValidFromBinding(RichInputDate validFromBinding) {
        this.validFromBinding = validFromBinding;
    }

    public RichInputDate getValidFromBinding() {
        return validFromBinding;
    }

    public void setValidToBinding(RichInputDate validToBinding) {
        this.validToBinding = validToBinding;
    }

    public RichInputDate getValidToBinding() {
        return validToBinding;
    }

    public void validFromValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if(object!=null && validToBinding.getValue()!=null)
        {
//        oracle.jbo.domain.Timestamp from=(oracle.jbo.domain.Timestamp)object;
        String str_from = object.toString();
        System.out.println("FROMMMM"+str_from);
        String str_to = validToBinding.getValue().toString();
        System.out.println("TOOOO"+str_to);
        oracle.jbo.domain.Date  dt_frm = new oracle.jbo.domain.Date(str_from.substring(0, 10));
        oracle.jbo.domain.Date  dt_to = new oracle.jbo.domain.Date(str_to.substring(0, 10));            
        if(dt_frm.compareTo(dt_to)==1)
        {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Valid From cannot be greater than Valid To.", null));
        }
        }
    }
}
