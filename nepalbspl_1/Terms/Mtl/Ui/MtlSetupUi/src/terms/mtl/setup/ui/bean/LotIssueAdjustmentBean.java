package terms.mtl.setup.ui.bean;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import javax.faces.validator.ValidatorException;

import oracle.adf.model.OperationBinding;
import oracle.adf.view.rich.component.rich.output.RichPanelCollection;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

public class LotIssueAdjustmentBean {
    private RichPanelCollection bindingLotIssueAdjustmenttable;
    private String editAction="V";

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public LotIssueAdjustmentBean() {
    }

    public void DeletePopup(DialogEvent dialogEvent) {
        
            if(dialogEvent.getOutcome().name().equals("ok"))
            {
            OperationBinding op = null;
            ADFUtils.findOperation("Delete").execute();
            op = (OperationBinding) ADFUtils.findOperation("Commit");
            if (getEditAction().equals("V")) {
            Object rst = op.execute();
            }
            System.out.println("Record Delete Successfully");
            if(op.getErrors().isEmpty()){
            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
            } else if (!op.getErrors().isEmpty())
            {
            OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
            Object rstr = opr.execute();
            FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
            Message.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
            }
            }
            AdfFacesContext.getCurrentInstance().addPartialTarget(bindingLotIssueAdjustmenttable);
        
        }


    public void setBindingLotIssueAdjustmenttable(RichPanelCollection bindingLotIssueAdjustmenttable) {
        this.bindingLotIssueAdjustmenttable = bindingLotIssueAdjustmenttable;
    }

    public RichPanelCollection getBindingLotIssueAdjustmenttable() {
        return bindingLotIssueAdjustmenttable;
    }

    /*  public void unitVCL(ValueChangeEvent vce) 
    {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        ADFUtils.findOperation("docNumberLotIssue").execute();
    } */

 
           
       

    public void quantityValid(FacesContext facesContext, UIComponent uIComponent, Object object) {
        //OperationBinding opp= ADFUtils.findOperation("qtyCheck");
      
                        oracle.binding.OperationBinding ob=ADFUtils.findOperation("qtyCheck");
                        ob.getParamsMap().put("rqty", object);
                        Object rst =ob.execute();
                        
                        if(rst!=null && rst.equals("N"))
                        {
                            
                                   throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "For the selected challant type - Type should be returnable", null));                         
                             }
    }

    public void SaveLotIssue(ActionEvent actionEvent) {
     actionEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        ADFUtils.findOperation("docNumberLotIssue").execute();
        ADFUtils.findOperation("Commit").execute();
    }
}


