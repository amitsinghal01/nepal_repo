package terms.mtl.setup.ui.bean;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

public class LotOpeningClosingBean {
    private RichTable lotOpeningClosingTableBinding;
    private String editAction="V";


    public LotOpeningClosingBean()
    {
    }

    public void deleteDialogListener(DialogEvent dialogEvent) {
 
        if(dialogEvent.getOutcome().name().equals("ok"))
        {
        OperationBinding op = null;
        ADFUtils.findOperation("Delete").execute();
        op = (OperationBinding) ADFUtils.findOperation("Commit");
        if (getEditAction().equals("V")) {
        Object rst = op.execute();
        }
        System.out.println("Record Delete Successfully");
        if(op.getErrors().isEmpty()){
        FacesMessage Message = new FacesMessage("Record Deleted Successfully");
        Message.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        } else if (!op.getErrors().isEmpty())
        {
        OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
        Object rstr = opr.execute();
        FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
        Message.setSeverity(FacesMessage.SEVERITY_ERROR);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        }
        }
  
        AdfFacesContext.getCurrentInstance().addPartialTarget(lotOpeningClosingTableBinding);
       
    }
    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public void setLotOpeningClosingTableBinding(RichTable lotOpeningClosingTableBinding) {
        this.lotOpeningClosingTableBinding = lotOpeningClosingTableBinding;
    }

    public RichTable getLotOpeningClosingTableBinding() {
        return lotOpeningClosingTableBinding;
    }

//    public void unitCodeDocNumVCE(ValueChangeEvent vce)
//    {
//        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
//        if(vce!=null)
//        {
//        ADFUtils.findOperation("docNumberLotOpening").execute();
//        }
//    }

    public void entryGenerate(ActionEvent actionEvent) {
        ADFUtils.findOperation("docNumberLotOpening").execute();
        ADFUtils.findOperation("Commit").execute();
    }
}
