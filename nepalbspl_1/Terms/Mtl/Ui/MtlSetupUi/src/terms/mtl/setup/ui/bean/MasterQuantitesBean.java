package terms.mtl.setup.ui.bean;

import java.util.ArrayList;

import javax.faces.application.FacesMessage;

import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;

public class MasterQuantitesBean {
    private RichTable masterQuantitiesTableBinding;

    public MasterQuantitesBean() {
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent)
            {
                if(dialogEvent.getOutcome().name().equals("ok"))
                    {
                    OperationBinding op = null;
                    ADFUtils.findOperation("Delete").execute();
                    op = (OperationBinding) ADFUtils.findOperation("Commit");
                    Object rst = op.execute();
                    System.out.println("Record Delete Successfully");
                        if(op.getErrors().isEmpty()){
                            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
    Message.setSeverity(FacesMessage.SEVERITY_INFO);
                            FacesContext fc = FacesContext.getCurrentInstance();
                            fc.addMessage(null, Message);
                        } else if (!op.getErrors().isEmpty())
                        {
                           OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
                           Object rstr = opr.execute();
                           FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
    Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                           FacesContext fc = FacesContext.getCurrentInstance();
                           fc.addMessage(null, Message);
                         }
                    }

    AdfFacesContext.getCurrentInstance().addPartialTarget(masterQuantitiesTableBinding);

            } 

    public void setMasterQuantitiesTableBinding(RichTable masterQuantitiesTableBinding) {
        this.masterQuantitiesTableBinding = masterQuantitiesTableBinding;
    }

    public RichTable getMasterQuantitiesTableBinding() {
        return masterQuantitiesTableBinding;
    }

    public String viewMasterRecords() {
        oracle.binding.OperationBinding binding = (oracle.binding.OperationBinding) ADFUtils.findOperation("viewAllMasterQuantities");
        binding.getParamsMap().put("view_mode", "V");
        binding.execute();
        return "goMasterQuantities";
    }

    public String editAllMasterQuantities() {
        oracle.binding.OperationBinding binding = (oracle.binding.OperationBinding) ADFUtils.findOperation("viewAllMasterQuantities");
        binding.getParamsMap().put("view_mode", "L");
        binding.execute();
        return "goMasterQuantities";
    }

    public void selectAllAL(ActionEvent actionEvent) {
        oracle.binding.OperationBinding binding = ADFUtils.findOperation("selectAndDeselectMethod");
                   binding.getParamsMap().put("mode_type", "S");
                   binding.getParamsMap().put("grant_type", "A");
                   binding.execute();
    }

    public void deSelectAllAL(ActionEvent actionEvent) {
        oracle.binding.OperationBinding binding = ADFUtils.findOperation("selectAndDeselectMethod");
                   binding.getParamsMap().put("mode_type", "D");
                   binding.getParamsMap().put("grant_type", "A");
                   binding.execute();
    }
}


