package terms.mtl.setup.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;
import javax.faces.validator.ValidatorException;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputText;

import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.binding.OperationBinding;

public class MasterQuantitiesPopulateBean {
    private RichInputText unitBind;
    private RichInputText itemTypeBind;
    private RichOutputText bindingOutputText;
    private RichButton headerEditBinding;
    private RichPanelHeader myPageRootComponent;
    private RichInputText minLevelBinding;
    private RichInputText maxLevelBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputComboboxListOfValues itemTypeBinding;
    private RichInputText itemCodeBinding;
    private RichButton populateButtonBinding;
    private RichInputText itemDescBinding;
    private RichInputText uomBinding;
    private RichInputText itemCdBinding;
    private RichInputText rolBinding;
    private RichInputText itemTypeDescBinding;

    public MasterQuantitiesPopulateBean() {
    }

    public void setUnitBind(RichInputText unitBind) {
        this.unitBind = unitBind;
    }

    public RichInputText getUnitBind() {
        return unitBind;
    }

    public void setItemTypeBind(RichInputText itemTypeBind) {
        this.itemTypeBind = itemTypeBind;
    }

    public RichInputText getItemTypeBind() {
        return itemTypeBind;
    }

    public void populateMasterQuantitiesData(ActionEvent actionEvent) {
        if(unitCodeBinding.getValue()!=null || itemTypeBinding.getValue()!=null){
                                         String Unit = unitCodeBinding.getValue()==null ? "" : unitCodeBinding.getValue().toString();
                                         String ItemType = itemTypeBinding.getValue()==null ? "" : itemTypeBinding.getValue().toString();

                                         OperationBinding ob = ADFUtils.findOperation("populateQuantityData");
                                                 ob.getParamsMap().put("Unit", Unit);
                                                 ob.getParamsMap().put("ItemType", ItemType);
                                                      ob.execute();
                                                      if(ob.getResult().equals("N"))
                                                      {
                                                          ADFUtils.showMessage("Record not found for Item Type-"+ItemType, 2);

                                                      }
                                                      
                                         //AdfFacesContext.getCurrentInstance().addPartialTarget(populateTableBind);
                                         }
    }

    public void saveButtonAL(ActionEvent actionEvent)
    {
//        String empcd=(String)ADFUtils.resolveExpression("#{pageFlowScope.empCode}") ; 
//        System.out.println("empcd"+empcd);
//         ADFUtils.setColumnValue("ProjectJobDetailVO1Iterator", "LastUpdatedBy", empcd);
         
        Integer i=0;
        i=(Integer)bindingOutputText.getValue();
        if (i.equals(0)) 
        {
        ADFUtils.findOperation("Commit").execute();
        FacesMessage Message = new FacesMessage("Record Saved Successfully");  
        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
        FacesContext fc = FacesContext.getCurrentInstance();  
        fc.addMessage(null, Message); 
        }
    else
    {
        ADFUtils.findOperation("Commit").execute();
        FacesMessage Message = new FacesMessage("Record Update Successfully");  
        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
        FacesContext fc = FacesContext.getCurrentInstance();  
        fc.addMessage(null, Message); 
            
    }
    }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    private void cevmodecheck(){
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
            makeComponentHierarchyReadOnly(this.getMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
              try {
                  Method method1 =
                      component.getClass().getMethod("setDisabled", boolean.class);
                  if (method1 != null) {
                      method1.invoke(component, valueComponent);
                  }
              } catch (NoSuchMethodException e) {
                  try {
                      Method method =
                          component.getClass().getMethod("setReadOnly", boolean.class);
                      if (method != null) {
                          method.invoke(component, valueComponent);
                      }
                  } catch (Exception e1) {
                      // e.printStackTrace();//silently eat this exception.
                  }


              } catch (Exception e) {
                  // e.printStackTrace();//silently eat this exception.
              }
              List<UIComponent> childComponents = component.getChildren();
              for (UIComponent comp : childComponents) {
                  makeComponentHierarchyReadOnly(comp, valueComponent);
              }
          }
    
    public void cevModeDisableComponent(String mode) {
        if (mode.equals("E")) {
                       getUnitCodeBinding().setDisabled(true);
                       getHeaderEditBinding().setDisabled(true);
                       getPopulateButtonBinding().setDisabled(true);
//                       getItemDescBinding().setDisabled(true);
//                       getUomBinding().setDisabled(true);
                       getItemCdBinding().setDisabled(true);
                   getItemTypeBinding().setDisabled(true);
                       getItemTypeDescBinding().setDisabled(true);
                       
               } else if (mode.equals("C")) {
                        getUnitCodeBinding().setDisabled(true);
                        getHeaderEditBinding().setDisabled(true);
//                        getItemDescBinding().setDisabled(true);
//                        getUomBinding().setDisabled(true);
                        getItemTypeDescBinding().setDisabled(true);

                   getItemCdBinding().setDisabled(true);

               } else if (mode.equals("V")) {
                   
               }      
    }

    public void setMyPageRootComponent(RichPanelHeader myPageRootComponent) {
        this.myPageRootComponent = myPageRootComponent;
    }

    public RichPanelHeader getMyPageRootComponent() {
        return myPageRootComponent;
    }

    public void setMinLevelBinding(RichInputText minLevelBinding) {
        this.minLevelBinding = minLevelBinding;
    }

    public RichInputText getMinLevelBinding() {
        return minLevelBinding;
    }

    public void setMaxLevelBinding(RichInputText maxLevelBinding) {
        this.maxLevelBinding = maxLevelBinding;
    }

    public RichInputText getMaxLevelBinding() {
        return maxLevelBinding;
    }

    public void maxValidator(FacesContext facesContext, UIComponent uIComponent, Object object)
    {
        if(object!=null && minLevelBinding.getValue()!=null)
        {
            System.out.println("Max Value=> "+object+"   Min Value=>"+minLevelBinding.getValue());
            
            oracle.jbo.domain.Number  a=(oracle.jbo.domain.Number)object;
            oracle.jbo.domain.Number  b=(oracle.jbo.domain.Number)minLevelBinding.getValue();
            
            if(a.compareTo(b)==-1)
            {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Max Level Must Be Equal or Greater Than Min Level", null));
            }
            
        }
        // Add event code here...

    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setItemTypeBinding(RichInputComboboxListOfValues itemTypeBinding) {
        this.itemTypeBinding = itemTypeBinding;
    }

    public RichInputComboboxListOfValues getItemTypeBinding() {
        return itemTypeBinding;
    }

    public void setItemCodeBinding(RichInputText itemCodeBinding) {
        this.itemCodeBinding = itemCodeBinding;
    }

    public RichInputText getItemCodeBinding() {
        return itemCodeBinding;
    }

    public void setPopulateButtonBinding(RichButton populateButtonBinding) {
        this.populateButtonBinding = populateButtonBinding;
    }

    public RichButton getPopulateButtonBinding() {
        return populateButtonBinding;
    }

    public void setItemDescBinding(RichInputText itemDescBinding) {
        this.itemDescBinding = itemDescBinding;
    }

    public RichInputText getItemDescBinding() {
        return itemDescBinding;
    }

    public void setUomBinding(RichInputText uomBinding) {
        this.uomBinding = uomBinding;
    }

    public RichInputText getUomBinding() {
        return uomBinding;
    }

    public void setItemCdBinding(RichInputText itemCdBinding) {
        this.itemCdBinding = itemCdBinding;
    }

    public RichInputText getItemCdBinding() {
        return itemCdBinding;
    }

    public void setRolBinding(RichInputText rolBinding) {
        this.rolBinding = rolBinding;
    }

    public RichInputText getRolBinding() {
        return rolBinding;
    }

    public void RolValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        oracle.jbo.domain.Number rol=(oracle.jbo.domain.Number)object;
        oracle.jbo.domain.Number min=(oracle.jbo.domain.Number) minLevelBinding.getValue();
        oracle.jbo.domain.Number max=(oracle.jbo.domain.Number) maxLevelBinding.getValue();
        System.out.println("ROL"+rol+" MIN"+min+" MAX"+max);
        if(rol!=null && (rol.compareTo(min)==0 || rol.compareTo(min)==-1 || rol.compareTo(max)==1 || rol.compareTo(max)==0))
        {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "ROL should be between Min and Max level.", null));
         
        }

    }

    public void setItemTypeDescBinding(RichInputText itemTypeDescBinding) {
        this.itemTypeDescBinding = itemTypeDescBinding;
    }

    public RichInputText getItemTypeDescBinding() {
        return itemTypeDescBinding;
    }
}
