package terms.mtl.setup.ui.bean;

import javax.faces.application.FacesMessage;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import javax.faces.validator.ValidatorException;

import oracle.adf.model.OperationBinding;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

public class OpeningBalanceBean {
    private RichTable deleteOpeningBalanceTableBinding;
    private RichInputText finYearBinding;
    private RichInputText yearBinding;
    private RichInputText monBinding;
    private RichInputComboboxListOfValues bindItemCode;

    public OpeningBalanceBean() {
    }
    private String editAction="V";
   
        public void deletePopupDialogDL(DialogEvent dialogEvent) {
               if(dialogEvent.getOutcome().name().equals("ok"))
                           {
                           OperationBinding op = null;
                           ADFUtils.findOperation("Delete").execute();
                           op = (OperationBinding) ADFUtils.findOperation("Commit");
                           if (getEditAction().equals("V")) {
                           Object rst = op.execute();
                           }
                           System.out.println("Record Delete Successfully");
                           if(op.getErrors().isEmpty()){
                           FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                           Message.setSeverity(FacesMessage.SEVERITY_INFO);
                           FacesContext fc = FacesContext.getCurrentInstance();
                           fc.addMessage(null, Message);
                           } else if (!op.getErrors().isEmpty())
                           {
                           OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
                           Object rstr = opr.execute();
                           FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                           Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                           FacesContext fc = FacesContext.getCurrentInstance();
                           fc.addMessage(null, Message);
                           }
                           }
                       AdfFacesContext.getCurrentInstance().addPartialTarget(deleteOpeningBalanceTableBinding);
           }

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public void setDeleteOpeningBalanceTableBinding(RichTable deleteOpeningBalanceTableBinding) {
        this.deleteOpeningBalanceTableBinding = deleteOpeningBalanceTableBinding;
    }

    public RichTable getDeleteOpeningBalanceTableBinding() {
        return deleteOpeningBalanceTableBinding;
    }

    public void setFinYearBinding(RichInputText finYearBinding) {
        this.finYearBinding = finYearBinding;
    }

    public RichInputText getFinYearBinding() {
        return finYearBinding;
    }

    public void monthVCE(ValueChangeEvent vce) 
    {
    vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
    
    //Year Value
    if(monBinding.getValue() != null && yearBinding.getValue() != null) {
        OperationBinding op = (OperationBinding) ADFUtils.findOperation("CompareWithFinYear");    
        op.getParamsMap().put("month",monBinding.getValue());
        op.getParamsMap().put("year",yearBinding.getValue());
        Object ovbj=op.execute();
        if(finYearBinding.getValue()==null && yearBinding.getValue()!=null)
        {
System.out.println("into loop");
                FacesMessage message = new FacesMessage("Financial year is not defined");
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(yearBinding.getClientId(), message);
               
            } 
    }
    
//    System.out.println("get Result YEAR VALUE===> "+op.getResult()); 
//    System.out.println("month"+getMonBinding().getValue());

//    if(monBinding.getValue()!=null && yearBinding.getValue()!=null)
//    { 
//        System.out.println("month"+monBinding.getValue());
//        Integer mon=(Integer)monBinding.getValue();
//        System.out.println("monthafter"+monBinding.getValue());
//     Integer Three=3;
//        String year=String.valueOf(yearBinding.getValue());
//        if (mon.compareTo(Three)==1 )
//            {
//             
//                year = year.substring(2);
//                Integer YearNum = Integer.parseInt(year) + 1;
//                finYearBinding.setValue(year + "-" + YearNum);
//            }
//       else{
//               year = year.substring(2);
//               Integer YearNum = Integer.parseInt(year)-1;
//               finYearBinding.setValue(YearNum + "-" + year);     }
//      
//    }


}

    public void setYearBinding(RichInputText yearBinding) {
        this.yearBinding = yearBinding;
    }

    public RichInputText getYearBinding() {
        return yearBinding;
    }

    public void setMonBinding(RichInputText monBinding) {
        this.monBinding = monBinding;
    }

    public RichInputText getMonBinding() {
        return monBinding;
    }

    public void LocToUnitVCE(ValueChangeEvent valueChangeEvent) {
        
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(valueChangeEvent!=null)
        {
              ADFUtils.findOperation("getLoctionToUnit").execute();
//            OperationBinding op=(OperationBinding)ADFUtils.findOperation("getLoctionToUnit");
//            Object rst=op.execute();
            }
    }

    /*  public void ValidateFinYear(FacesContext facesContext, UIComponent uIComponent, Object object) {
       if (object!=null)
        {System.out.println("fnyear"+finYearBinding.getValue());
        if(yearBinding.getValue()!= null  && finYearBinding.getValue()== null)
        {System.out.println("into error loop");
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Financial year is not defined.", null));
            }

    } */

    public void SaveOpeningIdAL(ActionEvent actionEvent) {
         DCIteratorBinding binding = ADFUtils.findIterator("OpeningBalanceVO1Iterator");
        if(binding.getCurrentRow().getAttribute("FinYear")==null)
        {
            ADFUtils.showMessage("Financial year is not defined.", 0);
            }
        else 
        {
//        OperationBinding op1 = (OperationBinding) ADFUtils.findOperation("SavingOpeningId");
//       op1.execute();
      ADFUtils.findOperation("Commit").execute();
    }
    }

  
    public void setBindItemCode(RichInputComboboxListOfValues bindItemCode) {
        this.bindItemCode = bindItemCode;
    }

    public RichInputComboboxListOfValues getBindItemCode() {
        return bindItemCode;
    }
}
