package terms.mtl.setup.ui.bean;

import java.math.BigDecimal;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;

import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;

import terms.mtl.setup.model.view.WeekGroupMasterVORowImpl;

public class WeekGroupBinding {
    private RichTable tableBinding;
    private String editAction="V";
    
    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public WeekGroupBinding() {
    }

    public void setTableBinding(RichTable tableBinding) {
        this.tableBinding = tableBinding;
    }

    public RichTable getTableBinding() {
        return tableBinding;
    }

    public void createButtonAL(ActionEvent actionEvent) {
                    System.out.println("In Button action");
        DCIteratorBinding dci = ADFUtils.findIterator("WeekGroupMasterVO1Iterator");
        if (dci != null) {
            Integer seq_no = fetchMaxLineNumber(dci);
                    System.out.println("Sequence Number after fetchMaxLineNumber:" + seq_no);
            RowSetIterator rsi = dci.getRowSetIterator();
            if (rsi != null) {
                Row last = rsi.last();
                int i = rsi.getRangeIndexOf(last);
                WeekGroupMasterVORowImpl newRow = (WeekGroupMasterVORowImpl) rsi.createRow();
                newRow.setNewRowState(Row.STATUS_INITIALIZED);
                rsi.insertRowAtRangeIndex(i + 1, newRow);
                rsi.setCurrentRow(newRow);
                newRow.setGroupCd("0"+seq_no.toString());
            }
            rsi.closeRowSetIterator();
        }
    }

    private Integer fetchMaxLineNumber(DCIteratorBinding itr) {
                    System.out.println("In fetchmaxline number");
        Integer max = new Integer(0);
                    System.out.println("Starting Max value:" + max);
        WeekGroupMasterVORowImpl currRow = (WeekGroupMasterVORowImpl) itr.getCurrentRow();
        if (currRow != null && currRow.getGroupCd() != null)
            max = Integer.parseInt(currRow.getGroupCd());
                    System.out.println("Max Value after setting currentseqno:" + max);
        RowSetIterator rsi = itr.getRowSetIterator();
        if (rsi != null) {
            Row[] allRowsInRange = rsi.getAllRowsInRange();
            for (Row rw : allRowsInRange) {
                if (rw != null && rw.getAttribute("GroupCd") != null &&
                    max.compareTo(Integer.parseInt(rw.getAttribute("GroupCd").toString())) < 0)
                            System.out.println("rw.getAttribute(\"GroupCd\")" + rw.getAttribute("GroupCd"));
                max = Integer.parseInt(rw.getAttribute("GroupCd").toString());
            }
        }
        rsi.closeRowSetIterator();
        max = max + new Integer(1);
                    System.out.println("max VAlue:" + max);
                    System.out.println("out fetchmaxline number");
            return max;

    }

    public void deleteDialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok")){
               oracle.adf.model.OperationBinding op = null;
                   ADFUtils.findOperation("Delete").execute();
                   op = (oracle.adf.model.OperationBinding) ADFUtils.findOperation("Commit");
           if (getEditAction().equals("V")) {
                    Object rst = op.execute();
           }
                           System.out.println("Record Delete Successfully");
           if(op.getErrors().isEmpty()){
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                   Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);
           } else if (!op.getErrors().isEmpty()){
                oracle.adf.model.OperationBinding opr = (oracle.adf.model.OperationBinding) ADFUtils.findOperation("Rollback");
                     Object rstr = opr.execute();
                   FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                   Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                   FacesContext fc = FacesContext.getCurrentInstance();
                        fc.addMessage(null, Message);
             }
        }
                AdfFacesContext.getCurrentInstance().addPartialTarget(tableBinding);
    }
}
