package terms.mtl.transaction.ui.bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.application.ViewHandler;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import javax.faces.validator.ValidatorException;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectBooleanCheckbox;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.nav.RichCommandLink;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.adf.view.rich.util.ResetUtils;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;

import org.apache.myfaces.trinidad.model.UploadedFile;

public class AdvanceSanctionBean {
    private RichTable createAdvanceTableBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText addressBinding;
    private RichInputText currencyBinding;
    private RichInputComboboxListOfValues checkedByBinding;
    private RichInputText checkedByNameBinding;
    private RichInputDate checkedDateBinding;
    private RichInputText unitNameBinding;
    private RichInputText partyNameBinding;
    private RichInputText amdnoBinding;
    private RichInputText currencyDescriptionBinding;
    private RichInputComboboxListOfValues preparedByBinding;
    private RichInputComboboxListOfValues verifiedByBinding;
    private RichInputText verifiedByNameBinding;
    private RichInputText entryNoBinding;
    private RichInputDate poDateBinding;
    private RichInputText paidAdvanceBinding;
    private RichInputText advanceRecomBinding;
    private RichInputText preparedByNameBinding;
    private RichInputComboboxListOfValues approvedbyBinding;
    private RichInputText approvedByNameBinding;
    private RichInputDate approveddateBinding;
    private RichInputText poAmountBinding;
    private RichInputText outStandingBinding;
    private RichInputText totalBinding;
    private RichInputText payToPartyBinding;
    private RichSelectBooleanCheckbox cancelledBinding;
    private RichInputText amountBinding;
    private RichInputText rateBinding;
    private RichInputText itemDescBinding;
    private RichOutputText bindingOutputText;
    private RichButton headerEditBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton detailcreateBinding;
    private RichButton detaildeleteBinding;
    private RichInputDate verifiedDateBinding;
    private RichInputDate entryDateBinding;
    private RichInputText locationNameBinding;
    private RichInputText advanceRecoBinding;
    private RichShowDetailItem advanceTab1Binding;
    private RichShowDetailItem advanceTab2Binding;
    private RichPopup popupSaveBinding;
    private RichPopup popup2Binding;
    private RichShowDetailItem docTabBinding;
    private RichInputText seqNoBinding;
    private RichInputText unitBinding;
    private RichInputText docNoBinding;
    private RichInputText docTypeBinding;
    private RichInputText fileNameBinding;
    private RichInputDate docDateBinding;
    private RichInputText pathBinding;
    private RichInputText contTypeBinding;
    private RichTable docTableBinding;
    private RichCommandLink linkBinding;

    public AdvanceSanctionBean() {
    }


    public void saveGenerateAdvSanNo(ActionEvent actionEvent) {
        String empcd=(String)ADFUtils.resolveExpression("#{pageFlowScope.empCode}") ; 
        System.out.println("empcd"+empcd);
        ADFUtils.setColumnValue("AdvanceSanctionHeaderVO1Iterator", "LastUpdatedBy", empcd);
        
        System.out.println("no of row sin detail==>?>>"+ADFUtils.evaluateEL("#{bindings.AdvanceSanctionDetailVO1Iterator.estimatedRowCount}"));
        if((Long)ADFUtils.evaluateEL("#{bindings.AdvanceSanctionDetailVO1Iterator.estimatedRowCount}")==0){
           
            RichPopup.PopupHints popup=new RichPopup.PopupHints();
            getPopup2Binding().show(popup);
            
        }
        
        else{
        OperationBinding op = ADFUtils.findOperation("generateAdvanceSanctionNo");
        Object rst = op.execute();
        System.out.println("--------Commit-------");
        System.out.println("value aftr getting result--->?" + rst);

        if (rst.toString() != null && rst.toString() != "") {
            if (op.getErrors().isEmpty()) {
                ADFUtils.findOperation("Commit").execute();
                FacesMessage Message = new FacesMessage("Record Saved Successfully.New Entry Number " + rst + ".");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);

            }
        }

        if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ") {
            if (op.getErrors().isEmpty()) {
                ADFUtils.findOperation("Commit").execute();
                FacesMessage Message = new FacesMessage("Record Update Successfully.");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);

            }


        }
        }
    }


    public void setSanNumber(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert").execute();
        ADFUtils.findOperation("setAdvanceSanctionNo").execute();

    }

    public void poNumberVCL(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        ADFUtils.findOperation("goForPaidAdvOutStd").execute();
    }

    public void itemCodeVCL(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        ADFUtils.findOperation("goAdvSancQtyRate").execute();
    }


    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete").execute();
            OperationBinding op = ADFUtils.findOperation("Commit");
            Object rst = op.execute();
            System.out.println("Record Delete Successfully");

            if (op.getErrors().isEmpty()) {
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            }
        }

        AdfFacesContext.getCurrentInstance().addPartialTarget(createAdvanceTableBinding);

    }

    public void setCreateAdvanceTableBinding(RichTable createAdvanceTableBinding) {
        this.createAdvanceTableBinding = createAdvanceTableBinding;
    }

    public RichTable getCreateAdvanceTableBinding() {
        return createAdvanceTableBinding;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setAddressBinding(RichInputText addressBinding) {
        this.addressBinding = addressBinding;
    }

    public RichInputText getAddressBinding() {
        return addressBinding;
    }

    public void setCurrencyBinding(RichInputText currencyBinding) {
        this.currencyBinding = currencyBinding;
    }

    public RichInputText getCurrencyBinding() {
        return currencyBinding;
    }

    public void setCheckedByBinding(RichInputComboboxListOfValues checkedByBinding) {
        this.checkedByBinding = checkedByBinding;
    }

    public RichInputComboboxListOfValues getCheckedByBinding() {
        return checkedByBinding;
    }

    public void setCheckedByNameBinding(RichInputText checkedByNameBinding) {
        this.checkedByNameBinding = checkedByNameBinding;
    }

    public RichInputText getCheckedByNameBinding() {
        return checkedByNameBinding;
    }

    public void setCheckedDateBinding(RichInputDate checkedDateBinding) {
        this.checkedDateBinding = checkedDateBinding;
    }

    public RichInputDate getCheckedDateBinding() {
        return checkedDateBinding;
    }

    public void setUnitNameBinding(RichInputText unitNameBinding) {
        this.unitNameBinding = unitNameBinding;
    }

    public RichInputText getUnitNameBinding() {
        return unitNameBinding;
    }

    public void setPartyNameBinding(RichInputText partyNameBinding) {
        this.partyNameBinding = partyNameBinding;
    }

    public RichInputText getPartyNameBinding() {
        return partyNameBinding;
    }

    public void setAmdnoBinding(RichInputText amdnoBinding) {
        this.amdnoBinding = amdnoBinding;
    }

    public RichInputText getAmdnoBinding() {
        return amdnoBinding;
    }

    public void setCurrencyDescriptionBinding(RichInputText currencyDescriptionBinding) {
        this.currencyDescriptionBinding = currencyDescriptionBinding;
    }

    public RichInputText getCurrencyDescriptionBinding() {
        return currencyDescriptionBinding;
    }

    public void setPreparedByBinding(RichInputComboboxListOfValues preparedByBinding) {
        this.preparedByBinding = preparedByBinding;
    }

    public RichInputComboboxListOfValues getPreparedByBinding() {
        return preparedByBinding;
    }

    public void setVerifiedByBinding(RichInputComboboxListOfValues verifiedByBinding) {
        this.verifiedByBinding = verifiedByBinding;
    }

    public RichInputComboboxListOfValues getVerifiedByBinding() {
        return verifiedByBinding;
    }

    public void setVerifiedByNameBinding(RichInputText verifiedByNameBinding) {
        this.verifiedByNameBinding = verifiedByNameBinding;
    }

    public RichInputText getVerifiedByNameBinding() {
        return verifiedByNameBinding;
    }

    public void setEntryNoBinding(RichInputText entryNoBinding) {
        this.entryNoBinding = entryNoBinding;
    }

    public RichInputText getEntryNoBinding() {
        return entryNoBinding;
    }

    public void setPoDateBinding(RichInputDate poDateBinding) {
        this.poDateBinding = poDateBinding;
    }

    public RichInputDate getPoDateBinding() {
        return poDateBinding;
    }

    public void setPaidAdvanceBinding(RichInputText paidAdvanceBinding) {
        this.paidAdvanceBinding = paidAdvanceBinding;
    }

    public RichInputText getPaidAdvanceBinding() {
        return paidAdvanceBinding;
    }

    public void setAdvanceRecomBinding(RichInputText advanceRecomBinding) {
        this.advanceRecomBinding = advanceRecomBinding;
    }

    public RichInputText getAdvanceRecomBinding() {
        return advanceRecomBinding;
    }

    public void setPreparedByNameBinding(RichInputText preparedByNameBinding) {
        this.preparedByNameBinding = preparedByNameBinding;
    }

    public RichInputText getPreparedByNameBinding() {
        return preparedByNameBinding;
    }

    public void setApprovedbyBinding(RichInputComboboxListOfValues approvedbyBinding) {
        this.approvedbyBinding = approvedbyBinding;
    }

    public RichInputComboboxListOfValues getApprovedbyBinding() {
        return approvedbyBinding;
    }

    public void setApprovedByNameBinding(RichInputText approvedByNameBinding) {
        this.approvedByNameBinding = approvedByNameBinding;
    }

    public RichInputText getApprovedByNameBinding() {
        return approvedByNameBinding;
    }

    public void setApproveddateBinding(RichInputDate approveddateBinding) {
        this.approveddateBinding = approveddateBinding;
    }

    public RichInputDate getApproveddateBinding() {
        return approveddateBinding;
    }

    public void setPoAmountBinding(RichInputText poAmountBinding) {
        this.poAmountBinding = poAmountBinding;
    }

    public RichInputText getPoAmountBinding() {
        return poAmountBinding;
    }

    public void setOutStandingBinding(RichInputText outStandingBinding) {
        this.outStandingBinding = outStandingBinding;
    }

    public RichInputText getOutStandingBinding() {
        return outStandingBinding;
    }

    public void setTotalBinding(RichInputText totalBinding) {
        this.totalBinding = totalBinding;
    }

    public RichInputText getTotalBinding() {
        return totalBinding;
    }

    public void setPayToPartyBinding(RichInputText payToPartyBinding) {
        this.payToPartyBinding = payToPartyBinding;
    }

    public RichInputText getPayToPartyBinding() {
        return payToPartyBinding;
    }

    public void setCancelledBinding(RichSelectBooleanCheckbox cancelledBinding) {
        this.cancelledBinding = cancelledBinding;
    }

    public RichSelectBooleanCheckbox getCancelledBinding() {
        return cancelledBinding;
    }

    public void setAmountBinding(RichInputText amountBinding) {
        this.amountBinding = amountBinding;
    }

    public RichInputText getAmountBinding() {
        return amountBinding;
    }

    public void setRateBinding(RichInputText rateBinding) {
        this.rateBinding = rateBinding;
    }

    public RichInputText getRateBinding() {
        return rateBinding;
    }

    public void setItemDescBinding(RichInputText itemDescBinding) {
        this.itemDescBinding = itemDescBinding;
    }

    public RichInputText getItemDescBinding() {
        return itemDescBinding;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {

        cevmodecheck();
        return bindingOutputText;
    }

    public void EditButtonAL(ActionEvent actionEvent) {
        if (approvedbyBinding.getValue() != null) {
            ADFUtils.showMessage("You can not update approved Sanction", 2);
        } else {
            cevmodecheck();
        }

    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }

    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }

    public void cevModeDisableComponent(String mode) {
        //            FacesContext fctx = FacesContext.getCurrentInstance();
        //            ELContext elctx = fctx.getELContext();
        //            Application jsfApp = fctx.getApplication();
        //            //create a ValueExpression that points to the ADF binding layer
        //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
        //            //
        //            ValueExpression valueExpr = exprFactory.createValueExpression(
        //                                         elctx,
        //                                         "#{pageFlowScope.mode=='E'}",
        //                                          Object.class
        //                                         );
        //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
        if (mode.equals("E")) {
            seqNoBinding.setDisabled(true);
            docNoBinding.setDisabled(true);
            docDateBinding.setDisabled(true);
            fileNameBinding.setDisabled(true);
            unitBinding.setDisabled(true);
            docTypeBinding.setDisabled(true);
            getEntryDateBinding().setDisabled(true);
            getPartyNameBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
            getUnitNameBinding().setDisabled(true);
            getAmdnoBinding().setDisabled(true);
            getCurrencyDescriptionBinding().setDisabled(true);
            getLocationNameBinding().setDisabled(true);
            getPoAmountBinding().setDisabled(true);
            getOutStandingBinding().setDisabled(true);
            getTotalBinding().setDisabled(true);
            getPayToPartyBinding().setDisabled(true);
            getItemDescBinding().setDisabled(true);
            getRateBinding().setDisabled(true);
            getAmountBinding().setDisabled(true);
            getPoDateBinding().setDisabled(true);
            getPaidAdvanceBinding().setDisabled(true);
            getEntryNoBinding().setDisabled(true);
            getAdvanceRecomBinding().setDisabled(true);

            getPreparedByNameBinding().setDisabled(true);
            getApprovedByNameBinding().setDisabled(true);
            getApproveddateBinding().setDisabled(true);
            getPreparedByBinding().setDisabled(true);

            getAddressBinding().setDisabled(true);
            getCurrencyBinding().setDisabled(true);
            getVerifiedByNameBinding().setDisabled(true);
            getVerifiedDateBinding().setDisabled(true);

            getCheckedByNameBinding().setDisabled(true);
            getCheckedDateBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            getDetailcreateBinding().setDisabled(false);
            getDetaildeleteBinding().setDisabled(false);

            if (checkedByBinding.getValue() != null) {
                getCheckedByBinding().setDisabled(true);
            }
            if (verifiedByBinding.getValue() != null) {
                getVerifiedByBinding().setDisabled(true);
            }
            if (approvedbyBinding.getValue() != null) {
                getApprovedbyBinding().setDisabled(true);
            }


        } else if (mode.equals("C")) {
            seqNoBinding.setDisabled(true);
            docNoBinding.setDisabled(true);
            docDateBinding.setDisabled(true);
            fileNameBinding.setDisabled(true);
            unitBinding.setDisabled(true);
            docTypeBinding.setDisabled(true);
            getPreparedByBinding().setDisabled(true);
            getPartyNameBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
            getUnitNameBinding().setDisabled(true);

            getPreparedByNameBinding().setDisabled(true);
            getAmdnoBinding().setDisabled(true);
            getCurrencyDescriptionBinding().setDisabled(true);
            getLocationNameBinding().setDisabled(true);
            getItemDescBinding().setDisabled(true);
            getRateBinding().setDisabled(true);
            getAmountBinding().setDisabled(true);
            getEntryDateBinding().setDisabled(true);
            getLocationNameBinding().setDisabled(true);
            getPoAmountBinding().setDisabled(true);
            getOutStandingBinding().setDisabled(true);
            getTotalBinding().setDisabled(true);
            getPayToPartyBinding().setDisabled(true);
            getCancelledBinding().setDisabled(true);
            getPoDateBinding().setDisabled(true);
            getPaidAdvanceBinding().setDisabled(true);
            getEntryNoBinding().setDisabled(true);
            getAdvanceRecomBinding().setDisabled(true);
            getPreparedByNameBinding().setDisabled(true);
            getApprovedByNameBinding().setDisabled(true);
            getApproveddateBinding().setDisabled(true);
            getApprovedbyBinding().setDisabled(true);
            getCheckedByBinding().setDisabled(true);
            getVerifiedByBinding().setDisabled(true);
            getVerifiedByNameBinding().setDisabled(true);
            getVerifiedDateBinding().setDisabled(true);
            getCheckedByNameBinding().setDisabled(true);
            getCheckedDateBinding().setDisabled(true);
            getAddressBinding().setDisabled(true);
            getCurrencyBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            getDetailcreateBinding().setDisabled(false);
            //            getDetaildeleteBinding().setDisabled(false);
        } else if (mode.equals("V")) {
            getDetailcreateBinding().setDisabled(true);
            getDetaildeleteBinding().setDisabled(true);
            getAdvanceTab1Binding().setDisabled(false);
            getAdvanceTab2Binding().setDisabled(false);
            docTabBinding.setDisabled(false);
            linkBinding.setDisabled(false);
        }
    }

    public void setDetailcreateBinding(RichButton detailcreateBinding) {
        this.detailcreateBinding = detailcreateBinding;
    }

    public RichButton getDetailcreateBinding() {
        return detailcreateBinding;
    }

    public void setDetaildeleteBinding(RichButton detaildeleteBinding) {
        this.detaildeleteBinding = detaildeleteBinding;
    }

    public RichButton getDetaildeleteBinding() {
        return detaildeleteBinding;
    }

    public void setVerifiedDateBinding(RichInputDate verifiedDateBinding) {
        this.verifiedDateBinding = verifiedDateBinding;
    }

    public RichInputDate getVerifiedDateBinding() {
        return verifiedDateBinding;
    }

    public void setEntryDateBinding(RichInputDate entryDateBinding) {
        this.entryDateBinding = entryDateBinding;
    }

    public RichInputDate getEntryDateBinding() {
        return entryDateBinding;
    }

    public void setLocationNameBinding(RichInputText locationNameBinding) {
        this.locationNameBinding = locationNameBinding;
    }

    public RichInputText getLocationNameBinding() {
        return locationNameBinding;
    }

    public void advanceRecoValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (object != null) {
            BigDecimal advncreco = new BigDecimal(object.toString());
            if (outStandingBinding.getValue() != null && poAmountBinding.getValue() != null) {
                BigDecimal outstand = new BigDecimal(getOutStandingBinding().getValue().toString());
                BigDecimal poamt = new BigDecimal(getPoAmountBinding().getValue().toString());
                if ((advncreco.compareTo(outstand) == 1) && (poamt.compareTo(new BigDecimal(0)) == 1)) {
                    System.out.println("in condition");
                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                  "Advance Reco Amount must be less than or equal to PO. Amount!!",
                                                                  null));
                    //              FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error",
                    //                  "Advance Reco Amount must be less than or equal to PO. Amount!!");
                    //              throw new ValidatorException(msg);
                }
            }
        }

    }

    public void setAdvanceRecoBinding(RichInputText advanceRecoBinding) {
        this.advanceRecoBinding = advanceRecoBinding;
    }

    public RichInputText getAdvanceRecoBinding() {
        return advanceRecoBinding;
    }

    public void setAdvanceTab1Binding(RichShowDetailItem advanceTab1Binding) {
        this.advanceTab1Binding = advanceTab1Binding;
    }

    public RichShowDetailItem getAdvanceTab1Binding() {
        return advanceTab1Binding;
    }

    public void setAdvanceTab2Binding(RichShowDetailItem advanceTab2Binding) {
        this.advanceTab2Binding = advanceTab2Binding;
    }

    public RichShowDetailItem getAdvanceTab2Binding() {
        return advanceTab2Binding;
    }

    public void setPopupSaveBinding(RichPopup popupSaveBinding) {
        this.popupSaveBinding = popupSaveBinding;
    }

    public RichPopup getPopupSaveBinding() {
        return popupSaveBinding;
    }

    public void popupDialogDL(DialogEvent dialogEvent) {
        System.out.println("inside dialog listener ");
        if(dialogEvent.getOutcome().name().equals("ok")){
            
            System.out.println("inside ok outcome ===");
            OperationBinding op = ADFUtils.findOperation("generateAdvanceSanctionNo");
            Object rst = op.execute();
            System.out.println("--------Commit-------");
            System.out.println("value aftr getting result--->?" + rst);

            if (rst.toString() != null && rst.toString() != "") {
                if (op.getErrors().isEmpty()) {
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message = new FacesMessage("Record Saved Successfully.New Entry Number " + rst + ".");
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);

                }
            }

            if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ") {
                if (op.getErrors().isEmpty()) {
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message = new FacesMessage("Record Update Successfully.");
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);

                }


            } 
            
        }
        
//        if(dialogEvent.getOutcome().equals("cancel")){
//            
//            
//        }
    }
    
    public String SaveAndClose(){
        String empcd=(String)ADFUtils.resolveExpression("#{pageFlowScope.empCode}") ; 
        System.out.println("empcd"+empcd);
        ADFUtils.setColumnValue("AdvanceSanctionHeaderVO1Iterator", "LastUpdatedBy", empcd);
        System.out.println("no of row sin detail==>?>>"+ADFUtils.evaluateEL("#{bindings.AdvanceSanctionDetailVO1Iterator.estimatedRowCount}"));
        if((Long)ADFUtils.evaluateEL("#{bindings.AdvanceSanctionDetailVO1Iterator.estimatedRowCount}")==0){
           
            RichPopup.PopupHints popup=new RichPopup.PopupHints();
            getPopupSaveBinding().show(popup);
            
        }
        
        else{
        OperationBinding op = ADFUtils.findOperation("generateAdvanceSanctionNo");
        Object rst = op.execute();
        System.out.println("--------Commit-------");
        System.out.println("value aftr getting result--->?" + rst);

        if (rst.toString() != null && rst.toString() != "") {
            if (op.getErrors().isEmpty()) {
                ADFUtils.findOperation("Commit").execute();
                FacesMessage Message = new FacesMessage("Record Saved Successfully.New Entry Number " + rst + ".");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
                return "save and close";

            }
        }

        if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ") {
            if (op.getErrors().isEmpty()) {
                ADFUtils.findOperation("Commit").execute();
                FacesMessage Message = new FacesMessage("Record Update Successfully.");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
                return "save and close";
            }


        }
        }
        
        
        
        return null;
    }

    public void okButtonAL(ActionEvent actionEvent) {
       
        System.out.println("inside ok outcome ===");
        OperationBinding op = ADFUtils.findOperation("generateAdvanceSanctionNo");
        Object rst = op.execute();
        System.out.println("--------Commit-------");
        System.out.println("value aftr getting result--->?" + rst);

        if (rst.toString() != null && rst.toString() != "") {
            if (op.getErrors().isEmpty()) {
                ADFUtils.findOperation("Commit").execute();
                FacesMessage Message = new FacesMessage("Record Saved Successfully.New Entry Number " + rst + ".");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);

            }
        }

        if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ") {
            if (op.getErrors().isEmpty()) {
                ADFUtils.findOperation("Commit").execute();
                FacesMessage Message = new FacesMessage("Record Update Successfully.");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);

            }


        }
    }

    public void cancelAL(ActionEvent actionEvent) {
       
        RichPopup.PopupHints popup=new RichPopup.PopupHints();
        popupSaveBinding.hide();
    }

    public void saveAndCloseAL(ActionEvent actionEvent) {
        // Add event code here...
    }

    public void popupDialogsaveDL(DialogEvent dialogEvent) {
        
        if(dialogEvent.getOutcome().name().equals("ok")){
            
            System.out.println("inside ok outcome ===");
            OperationBinding op = ADFUtils.findOperation("generateAdvanceSanctionNo");
            Object rst = op.execute();
            System.out.println("--------Commit-------");
            System.out.println("value aftr getting result--->?" + rst);

            if (rst.toString() != null && rst.toString() != "") {
                if (op.getErrors().isEmpty()) {
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message = new FacesMessage("Record Saved Successfully.New Entry Number " + rst + ".");
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);

                }
            }

            if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ") {
                if (op.getErrors().isEmpty()) {
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message = new FacesMessage("Record Update Successfully.");
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);

                }


            } 
            
        }
        
       
    }

    public void setPopup2Binding(RichPopup popup2Binding) {
        this.popup2Binding = popup2Binding;
    }

    public RichPopup getPopup2Binding() {
        return popup2Binding;
    }

    public void recordDeteteDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
                   ADFUtils.findOperation("Delete").execute();
                   System.out.println("Record Delete Successfully");
                   FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                   Message.setSeverity(FacesMessage.SEVERITY_INFO);
                   FacesContext fc = FacesContext.getCurrentInstance();
                   fc.addMessage(null, Message);
               }
    }

    public void approvedByVCE(ValueChangeEvent vce) 
    {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce.getNewValue()!=null)
        {
            BigDecimal amount=new BigDecimal(0);
            DCIteratorBinding dci = ADFUtils.findIterator("AdvanceSanctionDetailVO1Iterator");
            
            RowSetIterator rsi=dci.getViewObject().createRowSetIterator(null);
            while(rsi.hasNext()) {
            System.out.println("Amount");
            Row r = rsi.next();
            BigDecimal sum=(BigDecimal) r.getAttribute("Amount");
                if(sum!=null){
                    amount=amount.add(sum);
                }
            }
            rsi.closeRowSetIterator();
            
            System.out.println("Amountttttttttttttt==>>"+advanceRecoBinding.getValue());
            
            OperationBinding ob = ADFUtils.findOperation("checkApprovalFunctionStatus");
            ob.getParamsMap().put("formNm", "ADVS");
            ob.getParamsMap().put("authoLim", "AP");
            ob.getParamsMap().put("UnitCd", unitCodeBinding.getValue());
            ob.getParamsMap().put("Amount", advanceRecoBinding.getValue());
            ob.getParamsMap().put("empcd", vce.getNewValue().toString());
            ob.execute();
            
            System.out.println(" EMP CD"+vce.getNewValue().toString());
            if((ob.getResult() !=null && !ob.getResult().equals("Y")))
            {
                Row row=(Row)ADFUtils.evaluateEL("#{bindings.AdvanceSanctionHeaderVO1Iterator.currentRow}");
                row.setAttribute("ApprovedBy", null);
                ADFUtils.showMessage(ob.getResult().toString(), 0);
            }
        }
    }

    public void setDocTabBinding(RichShowDetailItem docTabBinding) {
        this.docTabBinding = docTabBinding;
    }

    public RichShowDetailItem getDocTabBinding() {
        return docTabBinding;
    }

    public void setSeqNoBinding(RichInputText seqNoBinding) {
        this.seqNoBinding = seqNoBinding;
    }

    public RichInputText getSeqNoBinding() {
        return seqNoBinding;
    }

    public void setUnitBinding(RichInputText unitBinding) {
        this.unitBinding = unitBinding;
    }

    public RichInputText getUnitBinding() {
        return unitBinding;
    }

    public void setDocNoBinding(RichInputText docNoBinding) {
        this.docNoBinding = docNoBinding;
    }

    public RichInputText getDocNoBinding() {
        return docNoBinding;
    }

    public void setDocTypeBinding(RichInputText docTypeBinding) {
        this.docTypeBinding = docTypeBinding;
    }

    public RichInputText getDocTypeBinding() {
        return docTypeBinding;
    }

    public void setFileNameBinding(RichInputText fileNameBinding) {
        this.fileNameBinding = fileNameBinding;
    }

    public RichInputText getFileNameBinding() {
        return fileNameBinding;
    }

    public void setDocDateBinding(RichInputDate docDateBinding) {
        this.docDateBinding = docDateBinding;
    }

    public RichInputDate getDocDateBinding() {
        return docDateBinding;
    }

    public void uploadAction(ValueChangeEvent vce) {
        if (vce.getNewValue() != null) {
                    //Get File Object from VC Event
                    UploadedFile fileVal = (UploadedFile) vce.getNewValue();
                    //Upload File to path- Return actual server path
                    String path = ADFUtils.uploadFile(fileVal);
                      System.out.println(fileVal.getContentType());
                      System.out.println("Content Type==>>"+fileVal.getContentType()+"Path==>>"+path+"File name==>>"+fileVal.getFilename());
                    //Method to insert data in table to keep track of uploaded files
                    OperationBinding ob = ADFUtils.findOperation("setFileDataAdvSanc");
                    ob.getParamsMap().put("name", fileVal.getFilename());
                    ob.getParamsMap().put("path", path);
                    ob.getParamsMap().put("contTyp", fileVal.getContentType());
                    ob.getParamsMap().put("SrNo", ADFUtils.evaluateEL("#{bindings.DocAttachRefDtlVO11Iterator.estimatedRowCount+1}"));
                    ob.execute();
                    // Reset inputFile component after upload
                    ResetUtils.reset(vce.getComponent());
                }
    }

    public void setPathBinding(RichInputText pathBinding) {
        this.pathBinding = pathBinding;
    }

    public RichInputText getPathBinding() {
        return pathBinding;
    }

    public void setContTypeBinding(RichInputText contTypeBinding) {
        this.contTypeBinding = contTypeBinding;
    }

    public RichInputText getContTypeBinding() {
        return contTypeBinding;
    }

    public void deletePopupAttachDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete1").execute();
            OperationBinding op = ADFUtils.findOperation("Commit");
            Object rst = op.execute();
            System.out.println("Record Delete Successfully");

            if (op.getErrors().isEmpty()) {
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            }
        }

        AdfFacesContext.getCurrentInstance().addPartialTarget(docTableBinding);

    }

    public void setDocTableBinding(RichTable docTableBinding) {
        this.docTableBinding = docTableBinding;
    }

    public RichTable getDocTableBinding() {
        return docTableBinding;
    }

    public void downloadAction(FacesContext facesContext, OutputStream outputStream)throws  Exception{
        File filed = new File(pathBinding.getValue().toString());
                               FileInputStream fis;
                               byte[] b;
                               try {
                                   fis = new FileInputStream(filed);

                                   int n;
                                   while ((n = fis.available()) > 0) {
                                       b = new byte[n];
                                       int result = fis.read(b);
                                       outputStream.write(b, 0, b.length);
                                       if (result == -1)
                                           break;
                                   }
                               } catch (Exception e) {
                                   e.printStackTrace();
                               }
                               outputStream.flush();

    }

    public void setLinkBinding(RichCommandLink linkBinding) {
        this.linkBinding = linkBinding;
    }

    public RichCommandLink getLinkBinding() {
        return linkBinding;
    }

    public void checkedByVCL(ValueChangeEvent vce) {
        System.out.println("INSIDE checkedByVCL ##### ");
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce.getNewValue()!=null)
        {
            BigDecimal amount=new BigDecimal(0);
            DCIteratorBinding dci = ADFUtils.findIterator("AdvanceSanctionDetailVO1Iterator");
            
            RowSetIterator rsi=dci.getViewObject().createRowSetIterator(null);
            while(rsi.hasNext()) {
            System.out.println("Amount");
            Row r = rsi.next();
            BigDecimal sum=(BigDecimal) r.getAttribute("Amount");
                if(sum!=null){
                    amount=amount.add(sum);
                }
            }
            rsi.closeRowSetIterator();
            
            System.out.println("Amountttttttttttttt==>>"+advanceRecoBinding.getValue());
            
            OperationBinding ob = ADFUtils.findOperation("checkApprovalFunctionStatus");
            ob.getParamsMap().put("formNm", "ADVS");
            ob.getParamsMap().put("authoLim", "CH");
            ob.getParamsMap().put("UnitCd", unitCodeBinding.getValue());
            ob.getParamsMap().put("Amount", advanceRecoBinding.getValue());
            ob.getParamsMap().put("empcd", vce.getNewValue().toString());
            ob.execute();
            
            System.out.println(" EMP CD"+vce.getNewValue().toString());
            if((ob.getResult() !=null && !ob.getResult().equals("Y")))
            {
                Row row=(Row)ADFUtils.evaluateEL("#{bindings.AdvanceSanctionHeaderVO1Iterator.currentRow}");
                row.setAttribute("CheckedBy", null);
                ADFUtils.showMessage(ob.getResult().toString(), 0);
            }
        }
    }

    public void verifiedByVCL(ValueChangeEvent vce) {
        System.out.println("INSIDE verifiedByVCL ##### ");
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce.getNewValue()!=null)
        {
            BigDecimal amount=new BigDecimal(0);
            DCIteratorBinding dci = ADFUtils.findIterator("AdvanceSanctionDetailVO1Iterator");
            
            RowSetIterator rsi=dci.getViewObject().createRowSetIterator(null);
            while(rsi.hasNext()) {
            System.out.println("Amount");
            Row r = rsi.next();
            BigDecimal sum=(BigDecimal) r.getAttribute("Amount");
                if(sum!=null){
                    amount=amount.add(sum);
                }
            }
            rsi.closeRowSetIterator();
            
            System.out.println("Amountttttttttttttt==>>"+advanceRecoBinding.getValue());
            
            OperationBinding ob = ADFUtils.findOperation("checkApprovalFunctionStatus");
            ob.getParamsMap().put("formNm", "ADVS");
            ob.getParamsMap().put("authoLim", "VR");
            ob.getParamsMap().put("UnitCd", unitCodeBinding.getValue());
            ob.getParamsMap().put("Amount", advanceRecoBinding.getValue());
            ob.getParamsMap().put("empcd", vce.getNewValue().toString());
            ob.execute();
            
            System.out.println(" EMP CD"+vce.getNewValue().toString());
            if((ob.getResult() !=null && !ob.getResult().equals("Y")))
            {
                Row row=(Row)ADFUtils.evaluateEL("#{bindings.AdvanceSanctionHeaderVO1Iterator.currentRow}");
                row.setAttribute("VerifiedBy", null);
                ADFUtils.showMessage(ob.getResult().toString(), 0);
            }
        }
    }
}
