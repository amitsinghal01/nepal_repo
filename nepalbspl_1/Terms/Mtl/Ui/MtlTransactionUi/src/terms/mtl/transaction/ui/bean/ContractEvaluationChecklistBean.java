package terms.mtl.transaction.ui.bean;

import javax.faces.application.FacesMessage;

import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

public class ContractEvaluationChecklistBean {
    private RichTable contractEvaluationChecklistTableBinding;
    private String editAction="V";

    public ContractEvaluationChecklistBean() {
    }

    public void popUpDialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
        {
        OperationBinding op = null;
        ADFUtils.findOperation("Delete").execute();
        op = (OperationBinding) ADFUtils.findOperation("Commit");
        if (getEditAction().equals("V")) {
        Object rst = op.execute();
        }
        System.out.println("Record Delete Successfully");
        if(op.getErrors().isEmpty()){
        FacesMessage Message = new FacesMessage("Record Deleted Successfully");
        Message.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        } else if (!op.getErrors().isEmpty())
        {
        OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
        Object rstr = opr.execute();
        FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
        Message.setSeverity(FacesMessage.SEVERITY_ERROR);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(contractEvaluationChecklistTableBinding);
    }

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public void setContractEvaluationChecklistTableBinding(RichTable contractEvaluationChecklistTableBinding) {
        this.contractEvaluationChecklistTableBinding = contractEvaluationChecklistTableBinding;
    }

    public RichTable getContractEvaluationChecklistTableBinding() {
        return contractEvaluationChecklistTableBinding;
    }

    public void createButtonAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert").execute();
        Row row=(Row)ADFUtils.evaluateEL("#{bindings.ContractEvaluationChecklistVO1Iterator.currentRow}");
        row.setAttribute("SrNo", ADFUtils.evaluateEL("#{bindings.ContractEvaluationChecklistVO1Iterator.estimatedRowCount}"));

    }
}
