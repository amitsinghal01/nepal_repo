package terms.mtl.transaction.ui.bean;

import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.event.DialogEvent;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import oracle.adfinternal.view.faces.context.AdfFacesContextImpl;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

public class ContractEvaluationParaBean {
    private RichTable contractEvaluationParaTableBinding;
    private String editAction="V";

    public ContractEvaluationParaBean() {
    }

    public void setContractEvaluationParaTableBinding(RichTable contractEvaluationParaTableBinding) {
        this.contractEvaluationParaTableBinding = contractEvaluationParaTableBinding;
    }

    public RichTable getContractEvaluationParaTableBinding() {
        return contractEvaluationParaTableBinding;
    }

    public void popUpDialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
        {
        OperationBinding op = null;
        ADFUtils.findOperation("Delete").execute();
        op = (OperationBinding) ADFUtils.findOperation("Commit");
        if (getEditAction().equals("V")) {
        Object rst = op.execute();
        }
        System.out.println("Record Delete Successfully");
        if(op.getErrors().isEmpty()){
        FacesMessage Message = new FacesMessage("Record Deleted Successfully");
        Message.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        } else if (!op.getErrors().isEmpty())
        {
        OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
        Object rstr = opr.execute();
        FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
        Message.setSeverity(FacesMessage.SEVERITY_ERROR);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        }
        }
        AdfFacesContextImpl.getCurrentInstance().addPartialTarget(contractEvaluationParaTableBinding);
    }

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public void createButtonAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert").execute();
        Row row=(Row)ADFUtils.evaluateEL("#{bindings.ContractEvaluationParaVO1Iterator.currentRow}");
        row.setAttribute("SNo", ADFUtils.evaluateEL("#{bindings.ContractEvaluationParaVO1Iterator.estimatedRowCount}"));
    }
}
