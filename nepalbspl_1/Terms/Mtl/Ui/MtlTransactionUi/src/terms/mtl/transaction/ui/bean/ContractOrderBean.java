package terms.mtl.transaction.ui.bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.sql.Timestamp;

import java.text.ParseException;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.application.ViewHandler;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.validator.ValidatorException;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputFile;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectBooleanCheckbox;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.nav.RichCommandLink;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.adf.view.rich.util.ResetUtils;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

import oracle.jbo.RowSetIterator;

import org.apache.myfaces.trinidad.model.UploadedFile;

import terms.mtl.transaction.model.view.ContractOrderDetailVORowImpl;
import terms.mtl.transaction.model.view.ContractOrderHeaderVORowImpl;

public class ContractOrderBean {
    private RichPopup annexurepopup;
    private RichInputFile uploadFileBind;
    private RichOutputText filenNmPath;
    private RichPopup docfilepopup;
    private boolean checkannex = true;
    private RichTable jodetailtable;
    private RichInputComboboxListOfValues reqnobind;
    private RichInputText workqtyBinding;
    private RichInputComboboxListOfValues contractorBinding;
    private RichTable docTableBinding;
    private RichOutputText bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton detailcreateBinding;
    private RichButton detaildeleteBinding;
    private RichInputText joNoBinding;
    private RichInputDate joDateBinding;
    private RichInputText joAmdNoBinding;
    private RichInputText panNoBinding;
    private RichInputText gstPerBinding;
    private RichInputDate checkedDateBinding;
    private RichInputDate verifiedDateBinding;
    private RichInputDate approedDateBinding;
    private RichInputComboboxListOfValues checkedByBinding;
    private RichInputComboboxListOfValues verifiedByBinding;
    private RichInputComboboxListOfValues approvedByBinding;
    private RichInputDate joAmdDateBinding;
    private RichInputText jobDetailBinding;
    private RichPopup confirmationPopUpBind;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText ratebinding;
    private RichInputComboboxListOfValues preparedByBinding;
    private RichButton pupulateButtonBinding;
    private RichSelectOneChoice openCloseBinding;
    private RichShowDetailItem contractTab1Binding;
    private RichShowDetailItem contractTab2Binding;
    private RichInputText sgstBinding;
    private RichInputText cgstBinding;
    private RichInputText igstBinding;
    private RichSelectOneChoice contractTypeBinding;
    private RichSelectOneChoice statusBinding;
    private RichInputText requisitionAmdNoBinding;
    private RichSelectOneChoice crTypeBinding;
    private RichSelectOneChoice orderTypeBinding;
    private RichInputComboboxListOfValues projectJobNoBinding;
    private RichSelectOneChoice materialBinding;
    private RichInputComboboxListOfValues locationBinding;
    private RichInputComboboxListOfValues gstCdBinding;
    private RichInputText otherBinding;
    private RichInputText sacCodeBinding;
    private RichInputText paymentTermBinding;
    private RichInputText remarksBinding;
    private RichInputComboboxListOfValues productCdBinding;
    private RichInputComboboxListOfValues jobCodeBinding;
    private RichInputComboboxListOfValues sacCdBinding;
    private RichInputComboboxListOfValues uomBinding;
    private RichInputText discountBinding;
    private RichInputDate validFromBinding;
    private RichInputText approvalAmountBinding;
    private RichInputText sgstAmountBinding;
    private RichInputText cgstAmountBinding;
    private RichInputText igstAmountBinding;
    private RichInputDate validToBinding;
    private RichInputComboboxListOfValues gstCd1Binding;
    private RichInputText gstPer1Binding;
    private RichInputText valueBinding;
    private RichInputText amtBinding;
    private RichSelectBooleanCheckbox lumpSumBinding;
    private RichInputComboboxListOfValues quotationNoBinding;
    private RichButton copuQuotButtonBinding;
    private RichInputText amtTransBinding;
    private RichTable termsTableBinding;
    private RichButton termsConditionButtonBinding;
    private RichInputText gstPerTransBinding;
    private RichInputText justificationBinding;
    String status="F",status1="F";
    private RichInputComboboxListOfValues transContractBinding;
    private RichTable multiPayTermTableBinding;
    private RichButton paymentTermButtonBinding;
    private RichInputText remrksBinding;
    private RichInputText amountBinding;
    private RichShowDetailItem refDocBindingTab;
    private RichInputText pathBinding;
    private RichInputText contentTypeBinding;
    private RichInputDate docDateBinding;
    private RichInputText seqNoBinding;
    private RichInputText unitCdBinding;
    private RichInputText docNoBinding;
    private RichInputText docTypeBinding;
    private RichInputText docFileNmBinding;
    private RichTable docRefTableBinding;
    String ReqNo="";BigDecimal ReqAmdNo=new BigDecimal(0);
    private RichInputComboboxListOfValues copyJoNoBinding;
    private RichInputText copyJoAmdNoBinding;
    private RichInputText copyJoIdBinding;
    private RichButton copyOrderButtonBinding;
    private RichInputText amountHdBinding;
    private RichCommandLink downloadLinkBinding;
    private RichInputText contractdateNepalBinding;
    private RichInputText joamendmentDateNepalBinding;

    public ContractOrderBean() {
    }

    public void setAnnexurepopup(RichPopup annexurepopup) {
        this.annexurepopup = annexurepopup;
    }

    public RichPopup getAnnexurepopup() {
        return annexurepopup;
    }

    public void AnnexureDialogListener(DialogEvent de) {
        if (de.getOutcome() == DialogEvent.Outcome.ok) {

            annexurepopup.hide();
        }
    }
    /*       public void AnnexureAL(ActionEvent actionEvent) {

      if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C") && checkannex ==true) {

                ADFUtils.findOperation("CreateInsert1").execute();
                ADFUtils.findOperation("setJoNumber").execute();
                checkannex = false;
            }
        } */

    public void docFileDialogListener(DialogEvent de) {
        if (de.getOutcome() == DialogEvent.Outcome.ok) {

            docfilepopup.hide();
        }
    }

    public void setUploadFileBind(RichInputFile uploadFileBind) {
        this.uploadFileBind = uploadFileBind;
    }

    public RichInputFile getUploadFileBind() {
        return uploadFileBind;
    }

    public void setFilenNmPath(RichOutputText filenNmPath) {
        this.filenNmPath = filenNmPath;
    }

    public RichOutputText getFilenNmPath() {
        return filenNmPath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getImagePath() {
        return imagePath;
    }

    String Imagepath = "";

    InputStream inputStream = null;
    InputStream SaveInputStream = null;
    private UploadedFile PhotoFile = null;
    private String AbsoluteFilePath = "";
    String imagePath = "";
    Boolean check = false;

    public void setPhotoFile(UploadedFile PhotoFile) {
        this.PhotoFile = PhotoFile;
    }

    public UploadedFile getPhotoFile() {
        return PhotoFile;
    }


    public void uploadFileVCE(ValueChangeEvent vce) {
        if (vce.getNewValue() != null) {
            setPhotoFile((UploadedFile) vce.getNewValue());
            UploadedFile myfile = (UploadedFile) vce.getNewValue();
            String path = null;

            if (myfile == null) {

            } else {
                File dir = new File("/tmp/pht");
                File savedir = new File("/home/beta9/Pictures");
                if (!dir.exists()) {
                    try {
                        dir.mkdir();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (!savedir.exists()) {
                    try {
                        savedir.mkdir();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                // All uploaded files will be stored in below path
                path = "/home/beta9/Pictures" + myfile.getFilename();
                //                Imagepath=path;
                File f = new File(path);
                if (!f.exists()) {
                    try {
                        f.createNewFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                inputStream = null;
                try {

                    inputStream = myfile.getInputStream();
                    SaveInputStream = myfile.getInputStream();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                check = true;
                //                System.out.println("check in bean" + check);
                Imagepath = uploadFile(PhotoFile, "/tmp/pht/" + PhotoFile.getFilename());

            }
        }
    }

    private String SaveuploadFile(UploadedFile file, String path) {

        UploadedFile myfile = file;

        try {

            FileOutputStream out = new FileOutputStream(path);
            byte[] buffer = new byte[8192];
            int bytesRead = 0;
            while ((bytesRead = SaveInputStream.read(buffer, 0, 8192)) != -1) {
                out.write(buffer, 0, bytesRead);
            }
            out.flush();
            out.close();
        } catch (Exception ex) {
            // handle exception
            ex.printStackTrace();
        } finally {
            try {
                SaveInputStream.close();
            } catch (IOException e) {
            }
        }
        return path;
    }

    private String uploadFile(UploadedFile file, String path) {

        UploadedFile myfile = file;

        try {

            FileOutputStream out = new FileOutputStream(path);
            byte[] buffer = new byte[8192];
            int bytesRead = 0;
            while ((bytesRead = inputStream.read(buffer, 0, 8192)) != -1) {
                out.write(buffer, 0, bytesRead);
            }
            out.flush();
            out.close();
        } catch (Exception ex) {
            // handle exception
            ex.printStackTrace();
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
            }
        }


        //Returns the path where file is stored
        return path;
    }

    public void downloadDocment(FacesContext facesContext, OutputStream outputStream) {
        //Read file from particular path, path bind is binding of table field that contains path
        File filed = new File(filenNmPath.getValue().toString());
        System.out.println("----------The filed name is ----------" + filed);
        FileInputStream fis;
        byte[] b;
        try {
            fis = new FileInputStream(filed);

            int n;
            while ((n = fis.available()) > 0) {
                b = new byte[n];
                int result = fis.read(b);
                outputStream.write(b, 0, b.length);
                if (result == -1)
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void setDocfilepopup(RichPopup docfilepopup) {
        this.docfilepopup = docfilepopup;
    }

    public RichPopup getDocfilepopup() {
        return docfilepopup;
    }

    public void saveContractOrderAL(ActionEvent actionEvent) {


    }

    public void createnewDocRowAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert2").execute();


        ADFUtils.findOperation("getSequenceDocNo").execute();


    }

    public void addJoDetailsAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("copyDataInContractOrderDtl").execute();
        ADFUtils.findOperation("setJoNumber").execute();
        String GstCd=(String)gstCdBinding.getValue();
        BigDecimal gstPer=(BigDecimal)gstPerBinding.getValue();
        gstCd1Binding.setValue(GstCd);
        gstPer1Binding.setValue(gstPer);
        gstPerTransBinding.setValue(gstPer);
        ADFUtils.findOperation("setGstInDetail").execute();
        AdfFacesContext.getCurrentInstance().addPartialTarget(jodetailtable);
//        getContractorBinding().setDisabled(true);
        getOpenCloseBinding().setDisabled(true);
        quotationNoBinding.setDisabled(true);
        copuQuotButtonBinding.setDisabled(true);
        
    }

    public void onPageLoad() {


        ADFUtils.findOperation("ContractHeaderCreateInsert").execute();
        ADFUtils.findOperation("AnnexureCreateInsert").execute();


    }

    public void setJodetailtable(RichTable jodetailtable) {
        this.jodetailtable = jodetailtable;
    }

    public RichTable getJodetailtable() {
        return jodetailtable;
    }

    protected void refreshPage() {
        FacesContext fctx = FacesContext.getCurrentInstance();
        String page = fctx.getViewRoot().getViewId();
        ViewHandler ViewH = fctx.getApplication().getViewHandler();
        UIViewRoot UIV = ViewH.createView(fctx, page);
        UIV.setViewId(page);
        fctx.setViewRoot(UIV);
    }


    public void saveContractOrderSaveAL(ActionEvent actionEvent) {
        String empcd=(String)ADFUtils.resolveExpression("#{pageFlowScope.empCode}") ; 
        System.out.println("empcd"+empcd);
        ADFUtils.setLastUpdatedBy("ContractOrderHeaderVO1Iterator", "LastUpdatedBy");
        
        if(contractorBinding.getValue()==null) {
            contractorBinding.setDisabled(false);
            ADFUtils.showMessage("Contractor cannot be null in Contract Order.", 0);
        }else if(status.equals("T"))
        {
            ADFUtils.showMessage("Work Quantity cannot be greater than Requisition Quantity. ", 0);
        }
        else if(status1.equals("T"))
        {
            ADFUtils.showMessage("Work Quantity cannot be greater than Quotation Quantity. ", 0);
        }
        else
        {
//        boolean result = checkallField();
//        if (result) {
//            check = false;
//            String path;
//            if (getPhotoFile() != null) {
//                path = "/home/beta9/Pictures" + PhotoFile.getFilename();
//                Imagepath = SaveuploadFile(PhotoFile, path);
//                //        System.out.println("path " + Imagepath);
//                OperationBinding ob = ADFUtils.findOperation("AddImagePathContract");
//                ob.getParamsMap().put("ImagePath", Imagepath);
//                ob.execute();
//            }
//
//            File directory = new File("/tmp/pht");
//            //get all the files from a directory
//            File[] fList = directory.listFiles();
//            if (fList != null) {
//                for (File file : fList) {
//                    //Delete all previously uploaded files
//                    // if (!"NoImage.png".equalsIgnoreCase(file.getName())) {
//                    file.delete();
//                    //}
//
//
//
//
//
//
//                }
//            }

            OperationBinding op = ADFUtils.findOperation("generateContractOrderNo");
            op.execute();
            ADFUtils.findOperation("setJoNumber").execute();
            ADFUtils.findOperation("getContractOrderRefresh").execute();
            System.out.println("--------Value Aftr getting result--- " + op.getResult());
            if (op.getResult() != null && op.getResult().toString().equalsIgnoreCase("N")) {
                if (op.getErrors().isEmpty()) {
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message = new FacesMessage("Record Update Successfully.");
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);

                }

            } else if (op.getResult() != null && op.getResult().toString() != "") {
                if (op.getErrors().isEmpty()) {
                    System.out.println("--------in result set-------");

                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message =
                        new FacesMessage("Record Saved Successfully.New Contract Order Number is " + op.getResult() +
                                         ".");
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);
                }
            }
        }
    }

    public void cancelRefreshAL(ActionEvent actionEvent) {
        //refreshPage();
    }

    public void joopclVCL(ValueChangeEvent valueChangeEvent) {
        AdfFacesContext.getCurrentInstance().addPartialTarget(jodetailtable);

    }


    public void populateDetailAL(ActionEvent actionEvent) {
        System.out.println("OPEN/CLOSE"+openCloseBinding.getValue());
        if(openCloseBinding.getValue().equals("O"))
        {
            ADFUtils.showMessage("Only for Close order Requisition is allowed.", 2);
        }   
        else{
        if (reqnobind.getValue() != null) {
            openCloseBinding.setDisabled(true);
            contractTypeBinding.setDisabled(true);
            String reqnobinding = reqnobind.getValue().toString();
            Integer reqAmdNo=0;
            OperationBinding opc = ADFUtils.findOperation("removePopdetail");
            opc.execute();

            OperationBinding op = ADFUtils.findOperation("populateDetailCO");
            System.out.println("--------Requisition Number in Bean------ Populate" + reqnobinding);
            op.getParamsMap().put("reqnumber", reqnobinding);
            op.getParamsMap().put("reqAmdNo", reqAmdNo);
            op.execute();

            OperationBinding oph = ADFUtils.findOperation("populateDetailToHeader");
            oph.getParamsMap().put("reqnumber", reqnobinding);
            op.getParamsMap().put("reqAmdNo", reqAmdNo);
            oph.execute();
            String GstCd=(String)gstCdBinding.getValue();
            BigDecimal gstPer=(BigDecimal)gstPerBinding.getValue();
            gstCd1Binding.setValue(GstCd);
            gstPer1Binding.setValue(gstPer);
            ADFUtils.findOperation("setGstInDetail").execute();
            AdfFacesContext.getCurrentInstance().addPartialTarget(jodetailtable);
//              if(validFromBinding.getValue()==null)
//                          {
//                          Calendar cal=Calendar.getInstance();
//                          Date dt=cal.getTime();
//                          int i=30;
////                          Timestamp RefDate=(Timestamp)dt;
//                          cal.setTimeInMillis(dt.getTime());
//                          cal.add(Calendar.DAY_OF_MONTH,i);
//                          Timestamp vaidTo=new Timestamp(cal.getTime().getTime());
//                          validFromBinding.setValue(dt);
//                          validToBinding.setValue(vaidTo);  
//                          }
//                          else if(validToBinding.getValue()==null)
//                          {
//                              Calendar cal=Calendar.getInstance();
//                              Date dt=cal.getTime();
//                              int i=30;
////                              Timestamp RefDate=(Timestamp)validFromBinding.getValue();
//                              cal.setTimeInMillis(dt.getTime());
//                              cal.add(Calendar.DAY_OF_MONTH,i);
//                              Timestamp vaidTo=new Timestamp(cal.getTime().getTime());
//                              validToBinding.setValue(vaidTo);  
//                          }
          }
         }
//        productCdBinding.setDisabled(true);
        contractorBinding.setDisabled(true);
        gstCdBinding.setDisabled(true);
//        jobCodeBinding.setDisabled(true);
//        jobDetailBinding.setDisabled(false);
//        sacCdBinding.setDisabled(true);
//        uomBinding.setDisabled(true);
//        workqtyBinding.setDisabled(true);
//        ratebinding.setDisabled(true);
//        validFromBinding.setDisabled(true);
//        validToBinding.setDisabled(true);

    }

    public void setReqnobind(RichInputComboboxListOfValues reqnobind) {
        this.reqnobind = reqnobind;
    }

    public RichInputComboboxListOfValues getReqnobind() {
        return reqnobind;
    }

    public void setWorkqtyBinding(RichInputText workqtyBinding) {
        this.workqtyBinding = workqtyBinding;
    }

    public RichInputText getWorkqtyBinding() {
        return workqtyBinding;
    }

    public void workQtyValidationCheck(FacesContext facesContext, UIComponent uIComponent, Object object) {
            if(object != null)
            {
            BigDecimal a=new BigDecimal(0);
            BigDecimal wrkqty = (BigDecimal) object;
            System.out.println("wrkqty qty is===>"+wrkqty);
            if(wrkqty.compareTo(a)==-1 || wrkqty.compareTo(a)==0)
            {
              System.out.println("iside compare"+wrkqty);
              throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Work Quantity must be greater than 0.", null));
            }
        }

    }


    public boolean checkallField() {
        DCIteratorBinding binding = ADFUtils.findIterator("ContractOrderHeaderVO1Iterator");
        if (binding.getEstimatedRowCount() > 0 && binding.getCurrentRow() != null) {
            ContractOrderHeaderVORowImpl row = (ContractOrderHeaderVORowImpl) binding.getCurrentRow();
//            if (row.getVenCd() == null) {
//                String Client = contractorBinding.getClientId();
//                FacesMessage Message = new FacesMessage("Contractor is required. ");
//                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
//                FacesContext fc = FacesContext.getCurrentInstance();
//                fc.addMessage(Client, Message);
//                return false;
//            }

        }

        return true;
    }

    public void setContractorBinding(RichInputComboboxListOfValues contractorBinding) {
        this.contractorBinding = contractorBinding;
    }

    public RichInputComboboxListOfValues getContractorBinding() {
        return contractorBinding;
    }

    public String saveAndCloseAC() {
        String empcd=(String)ADFUtils.resolveExpression("#{pageFlowScope.empCode}") ; 
        System.out.println("empcd"+empcd);
        ADFUtils.setColumnValue("ContractOrderHeaderVO1Iterator", "LastUpdatedBy", empcd);
        if(contractorBinding.getValue()==null) {
            contractorBinding.setDisabled(false);
            ADFUtils.showMessage("Contractor cannot be null in Contract Order.", 0);
        }else
        {
        if(status.equals("T"))
        {
            ADFUtils.showMessage("Work Quantity cannot be greater than Requisition Quantity. ", 0);
        }
        else if(status1.equals("T"))
        {
            ADFUtils.showMessage("Work Quantity cannot be greater than Quotation Quantity. ", 0);
        }
        else
        {
        boolean result = checkallField();
        if (result) {
            check = false;
            String path;
            if (getPhotoFile() != null) {
                path = "/home/beta9/Pictures" + PhotoFile.getFilename();
                Imagepath = SaveuploadFile(PhotoFile, path);
                //        System.out.println("path " + Imagepath);
                OperationBinding ob = ADFUtils.findOperation("AddImagePathContract");
                ob.getParamsMap().put("ImagePath", Imagepath);
                ob.execute();
            }

            File directory = new File("/tmp/pht");
            //get all the files from a directory
            File[] fList = directory.listFiles();
            if (fList != null) {
                for (File file : fList) {
                    //Delete all previously uploaded files
                    // if (!"NoImage.png".equalsIgnoreCase(file.getName())) {
                    file.delete();
                    //}






                }
            }

            OperationBinding op = ADFUtils.findOperation("generateContractOrderNo");
            op.execute();
            ADFUtils.findOperation("setJoNumber").execute();
           ADFUtils.findOperation("getContractOrderRefresh").execute();

            System.out.println("--------Value Aftr getting result--- " + op.getResult());
            if (op.getResult() != null && op.getResult().toString().equalsIgnoreCase("N")) {
                if (op.getErrors().isEmpty()) {
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message = new FacesMessage("Record Update Successfully.");
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);
                    return "save and cancel";

                }

            } else if (op.getResult() != null && op.getResult().toString() != "") {
                if (op.getErrors().isEmpty()) {
                    System.out.println("--------in result set-------");

                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message =
                        new FacesMessage("Record Saved Successfully.New Contract Order Number is " + op.getResult() +
                                         ".");
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);
                    return "save and cancel";
                }
            }
        }
        }
        }
        return null;
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(jodetailtable);
    }

    public void deletePopupDialogDocDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete1").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(docTableBinding);
    }

    public void setDocTableBinding(RichTable docTableBinding) {
        this.docTableBinding = docTableBinding;
    }

    public RichTable getDocTableBinding() {
        return docTableBinding;
    }

    public void EditButtonAL(ActionEvent actionEvent) {
        DCIteratorBinding binding = ADFUtils.findIterator("ContractOrderHeaderVO1Iterator");
        if (binding.getEstimatedRowCount() > 0 && binding.getCurrentRow() != null) {
            ContractOrderHeaderVORowImpl row = (ContractOrderHeaderVORowImpl) binding.getCurrentRow();
            Integer amd=(Integer)getJoAmdNoBinding().getValue();
            System.out.println("AMD1"+amd);
            if (row.getApprovedBy() != null && row.getApprovedBy() != "" &&
                row.getApprovedBy().toString().trim().length() > 0) {
                RichPopup.PopupHints hints = new RichPopup.PopupHints();
                getConfirmationPopUpBind().show(hints);
                System.out.println("AMD2"+amd);
                
                
                try {    
                         System.out.println("set getEntryDateBinding"+ADFUtils.getTodayDate());
                         String date=ADFUtils.convertAdToBs(ADFUtils.getTodayDate().toString());
                         System.out.println("setter getvaluedate"+date);
                       //  joamendmentDateNepalBinding.setValue(date.toString());
                            DCIteratorBinding dci = ADFUtils.findIterator("ContractOrderHeaderVO1Iterator");
                            dci.getCurrentRow().setAttribute("JoAmdDateNepal",date);
                     } catch (ParseException e) {
                     }

            }
            else if(amd==0)
            {
                cevmodecheck();
                System.out.println("AMD="+amd);

            }
            else if(amd>0)
            {
                    amdEnableDisable();
                System.out.println("AMD>"+amd);

            }
            lumsumCaseEnable();
//            if(verifiedByBinding.getValue()==null ){
//                
//                System.out.println("Verified by isssss when null>>>>"+verifiedByBinding.getValue());
//            
//                approvedByBinding.setDisabled(true);
//            }
//            if(checkedByBinding.getValue()==null){ 
//                
//                System.out.println("Checked by isssss when null>>>>"+checkedByBinding.getValue());
//                verifiedByBinding.setDisabled(true);
//                
//            }
        if(checkedByBinding.getValue()==null)
        {
            verifiedByBinding.setDisabled(true);
            approvedByBinding.setDisabled(true);
        }
        else if(verifiedByBinding.getValue()==null)
        {
            checkedByBinding.setDisabled(true);
            approvedByBinding.setDisabled(true);
        }
        else if(approvedByBinding.getValue()==null)
        {
            checkedByBinding.setDisabled(true);
            verifiedByBinding.setDisabled(true);
        }
        else if(approvedByBinding.getValue()!=null)
        {
            checkedByBinding.setDisabled(true);
            verifiedByBinding.setDisabled(true);
            verifiedByBinding.setDisabled(true);
        }
        }
    }


    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }

    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }

    public void cevModeDisableComponent(String mode) {
        if (mode.equals("E")) {
            getContractTab2Binding().setDisabled(false);
            getContractorBinding().setDisabled(true);
            getOpenCloseBinding().setDisabled(true);
            getPupulateButtonBinding().setDisabled(true);
            getPreparedByBinding().setDisabled(true);
            getJobDetailBinding().setDisabled(false);
            getJoAmdDateBinding().setDisabled(true);
            getCheckedDateBinding().setDisabled(true);
            getVerifiedDateBinding().setDisabled(true);
            getApproedDateBinding().setDisabled(true);
            getJoNoBinding().setDisabled(true);
            getJoAmdNoBinding().setDisabled(true);
            getPanNoBinding().setDisabled(true);
            getJoDateBinding().setDisabled(true);
            getGstPerBinding().setDisabled(true);
//            getGstDescBinding().setDisabled(true);
            getDetailcreateBinding().setDisabled(false);
            getDetaildeleteBinding().setDisabled(false);
            getUnitCodeBinding().setDisabled(true);
            getIgstBinding().setDisabled(true);
            getSgstBinding().setDisabled(true);
            getCgstBinding().setDisabled(true);
            getPreparedByBinding().setDisabled(true);
            getRequisitionAmdNoBinding().setDisabled(true);
            getApprovalAmountBinding().setDisabled(true);
            getCgstAmountBinding().setDisabled(true);
            getSgstAmountBinding().setDisabled(true);
            getIgstAmountBinding().setDisabled(true);
            getGstPer1Binding().setDisabled(true);
            quotationNoBinding.setDisabled(true);
            copuQuotButtonBinding.setDisabled(true);
            reqnobind.setDisabled(true);
            unitCdBinding.setDisabled(true);
            seqNoBinding.setDisabled(true);
            docDateBinding.setDisabled(true);
            docFileNmBinding.setDisabled(true);
            docTypeBinding.setDisabled(true);
            docNoBinding.setDisabled(true);
            copyJoAmdNoBinding.setDisabled(true);
            copyJoNoBinding.setDisabled(true);
            copyOrderButtonBinding.setDisabled(true);
            copyJoAmdNoBinding.setDisabled(true);
            copyJoNoBinding.setDisabled(true);
            copyOrderButtonBinding.setDisabled(true);
            contractorBinding.setDisabled(true);
            getJoamendmentDateNepalBinding().setDisabled(true);
            getContractdateNepalBinding().setDisabled(true);
            amountHdBinding.setDisabled(true);


        } else if (mode.equals("C")) {
            getIgstBinding().setDisabled(true);
            getSgstBinding().setDisabled(true);
            getCgstBinding().setDisabled(true);
            getPreparedByBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
            //getJobDetailBinding().setDisabled(true);
            getJoAmdDateBinding().setDisabled(true);
            getApprovedByBinding().setDisabled(true);
            getCheckedByBinding().setDisabled(true);
            getVerifiedByBinding().setDisabled(true);
            getCheckedDateBinding().setDisabled(true);
            getVerifiedDateBinding().setDisabled(true);
            getApproedDateBinding().setDisabled(true);
            getJoNoBinding().setDisabled(true);
            getJoDateBinding().setDisabled(true);
            getGstPerBinding().setDisabled(true);
            getPanNoBinding().setDisabled(true);
            getJoAmdNoBinding().setDisabled(true);
//            getDetailcreateBinding().setDisabled(false);
            getPreparedByBinding().setDisabled(true);
            getStatusBinding().setDisabled(true);
            getRequisitionAmdNoBinding().setDisabled(true);
            getApprovalAmountBinding().setDisabled(true);
            //getDetaildeleteBinding().setDisabled(false);
            getCgstAmountBinding().setDisabled(true);
            getSgstAmountBinding().setDisabled(true);
            getIgstAmountBinding().setDisabled(true);
            getGstPer1Binding().setDisabled(true);
            quotationNoBinding.setDisabled(true);
            copuQuotButtonBinding.setDisabled(true);
            unitCdBinding.setDisabled(true);
            seqNoBinding.setDisabled(true);
            docDateBinding.setDisabled(true);
            docFileNmBinding.setDisabled(true);
            docTypeBinding.setDisabled(true);
            docNoBinding.setDisabled(true);
            copyJoAmdNoBinding.setDisabled(true);
            copyJoNoBinding.setDisabled(true);
            copyOrderButtonBinding.setDisabled(true);
            contractorBinding.setDisabled(true);
            amountHdBinding.setDisabled(true);
            getContractTab2Binding().setDisabled(false);
            getJoamendmentDateNepalBinding().setDisabled(true);
            getContractdateNepalBinding().setDisabled(true);

        } else if (mode.equals("V")) {
            getDetailcreateBinding().setDisabled(true);
            getDetaildeleteBinding().setDisabled(true);
            getContractTab1Binding().setDisabled(false);
            getContractTab2Binding().setDisabled(false);
            copuQuotButtonBinding.setDisabled(true);
            termsConditionButtonBinding.setDisabled(false);
            paymentTermButtonBinding.setDisabled(false);
            refDocBindingTab.setDisabled(false);
            copyOrderButtonBinding.setDisabled(true);
            downloadLinkBinding.setDisabled(false);
                
        }
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setDetailcreateBinding(RichButton detailcreateBinding) {
        this.detailcreateBinding = detailcreateBinding;
    }

    public RichButton getDetailcreateBinding() {
        return detailcreateBinding;
    }

    public void setDetaildeleteBinding(RichButton detaildeleteBinding) {
        this.detaildeleteBinding = detaildeleteBinding;
    }

    public RichButton getDetaildeleteBinding() {
        return detaildeleteBinding;
    }

    public void setJoNoBinding(RichInputText joNoBinding) {
        this.joNoBinding = joNoBinding;
    }

    public RichInputText getJoNoBinding() {
        return joNoBinding;
    }

    public void setJoDateBinding(RichInputDate joDateBinding) {
        this.joDateBinding = joDateBinding;
    }

    public RichInputDate getJoDateBinding() {
        try {
            System.out.println("set getJoDateBinding"+ADFUtils.getTodayDate());
            String date=ADFUtils.convertAdToBs(ADFUtils.getTodayDate().toString());
            System.out.println("setter getvaluedate"+date);
 //           contractdateNepalBinding.setValue(date);
                    DCIteratorBinding dci = ADFUtils.findIterator("ContractOrderHeaderVO1Iterator");
                    dci.getCurrentRow().setAttribute("JoDateNepal",date);
        } catch (ParseException e) {
        }
        return joDateBinding;
    }

    public void setJoAmdNoBinding(RichInputText joAmdNoBinding) {
        this.joAmdNoBinding = joAmdNoBinding;
    }

    public RichInputText getJoAmdNoBinding() {
        return joAmdNoBinding;
    }

    public void setPanNoBinding(RichInputText panNoBinding) {
        this.panNoBinding = panNoBinding;
    }

    public RichInputText getPanNoBinding() {
        return panNoBinding;
    }

    public void setGstPerBinding(RichInputText gstPerBinding) {
        this.gstPerBinding = gstPerBinding;
    }

    public RichInputText getGstPerBinding() {
        return gstPerBinding;
    }

    public void setCheckedDateBinding(RichInputDate checkedDateBinding) {
        this.checkedDateBinding = checkedDateBinding;
    }

    public RichInputDate getCheckedDateBinding() {
        return checkedDateBinding;
    }

    public void setVerifiedDateBinding(RichInputDate verifiedDateBinding) {
        this.verifiedDateBinding = verifiedDateBinding;
    }

    public RichInputDate getVerifiedDateBinding() {
        return verifiedDateBinding;
    }

    public void setApproedDateBinding(RichInputDate approedDateBinding) {
        this.approedDateBinding = approedDateBinding;
    }

    public RichInputDate getApproedDateBinding() {
        return approedDateBinding;
    }

    public void setCheckedByBinding(RichInputComboboxListOfValues checkedByBinding) {
        this.checkedByBinding = checkedByBinding;
    }

    public RichInputComboboxListOfValues getCheckedByBinding() {
        return checkedByBinding;
    }

    public void setVerifiedByBinding(RichInputComboboxListOfValues verifiedByBinding) {
        this.verifiedByBinding = verifiedByBinding;
    }

    public RichInputComboboxListOfValues getVerifiedByBinding() {
        return verifiedByBinding;
    }

    public void setApprovedByBinding(RichInputComboboxListOfValues approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputComboboxListOfValues getApprovedByBinding() {
        return approvedByBinding;
    }

    public void setJoAmdDateBinding(RichInputDate joAmdDateBinding) {
        this.joAmdDateBinding = joAmdDateBinding;
    }

    public RichInputDate getJoAmdDateBinding() {

       

        return joAmdDateBinding;
    }
    public void setJobDetailBinding(RichInputText jobDetailBinding) {
        this.jobDetailBinding = jobDetailBinding;
    }

    public RichInputText getJobDetailBinding() {
        return jobDetailBinding;
    }

    public void setConfirmationPopUpBind(RichPopup confirmationPopUpBind) {
        this.confirmationPopUpBind = confirmationPopUpBind;
    }

    public RichPopup getConfirmationPopUpBind() {
        return confirmationPopUpBind;
    }

    public void amendCnfrmtnDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
//            String ReqNo=(String)reqnobind.getValue();
//            BigDecimal ReqAmdNo=(BigDecimal)requisitionAmdNoBinding.getValue();
//            System.out.println("Before Amd Value=====>"+ReqNo+" AmdNo"+ReqAmdNo);
            System.out.println("Contract No in Bean"+getJoNoBinding().getValue()+" Amend No in Bean"+getJoAmdNoBinding().getValue());
            OperationBinding op1=ADFUtils.findOperation("checkMaxAmend");
            op1.getParamsMap().put("ContractNo", getJoNoBinding().getValue());
            op1.getParamsMap().put("AmendNo", getJoAmdNoBinding().getValue());
            op1.execute();
            if(op1.getResult().toString().equalsIgnoreCase("E"))
            {
                ADFUtils.showMessage("You can only Amend the latest approved Contract Order!!", 2);
                ADFUtils.setEL("#{pageFlowScope.mode}", "V");
            }
            else
            {
            OperationBinding op = ADFUtils.findOperation("getAmdJOPODetails");
            op.getParamsMap().put("JoNo", ADFUtils.resolveExpression("#{pageFlowScope.JoNo}"));
            op.getParamsMap().put("JoId", ADFUtils.resolveExpression("#{pageFlowScope.JoId}"));
            op.getParamsMap().put("AmdNo", ADFUtils.resolveExpression("#{pageFlowScope.AmdNo}"));
            op.execute();   
                if(reqnobind.getValue()==null)
                {
                    reqnobind.setValue(ReqNo);
                    requisitionAmdNoBinding.setValue(ReqAmdNo);
                }
//            cevmodecheck();
            amdEnableDisable();
            getDetailcreateBinding().setDisabled(false);
            getDetaildeleteBinding().setDisabled(false);

              if(checkedByBinding.getValue()==null)
              {
                  verifiedByBinding.setDisabled(true);
                  approvedByBinding.setDisabled(true);
              }
              else if(verifiedByBinding.getValue()==null)
              {
                  checkedByBinding.setDisabled(true);
                  approvedByBinding.setDisabled(true);
              }
              else if(approvedByBinding.getValue()==null)
              {
                  checkedByBinding.setDisabled(true);
                  verifiedByBinding.setDisabled(true);
              }
              else if(approvedByBinding.getValue()!=null)
              {
                  checkedByBinding.setDisabled(true);
                  verifiedByBinding.setDisabled(true);
                  verifiedByBinding.setDisabled(true);
              }
            }
            lumsumCaseEnable();
        } else
            getConfirmationPopUpBind().hide();
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setRatebinding(RichInputText ratebinding) {
        this.ratebinding = ratebinding;
    }

    public RichInputText getRatebinding() {
        return ratebinding;
    }

    public void setPreparedByBinding(RichInputComboboxListOfValues preparedByBinding) {
        this.preparedByBinding = preparedByBinding;
    }

    public RichInputComboboxListOfValues getPreparedByBinding() {
        return preparedByBinding;
    }

    public void setPupulateButtonBinding(RichButton pupulateButtonBinding) {
        this.pupulateButtonBinding = pupulateButtonBinding;
    }

    public RichButton getPupulateButtonBinding() {
        return pupulateButtonBinding;
    }

    public void setOpenCloseBinding(RichSelectOneChoice openCloseBinding) {
        this.openCloseBinding = openCloseBinding;
    }

    public RichSelectOneChoice getOpenCloseBinding() {
        return openCloseBinding;
    }

//    public void verifyByVCL(ValueChangeEvent valueChangeEvent) {
//        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
//        System.out.println("Verified by isssss=s==s>>>>"+verifiedByBinding.getValue());
//        if(valueChangeEvent!=null){
//            
//            if(verifiedByBinding.getValue()!=null){
//                
//                System.out.println("Verified by isssss when not null>>>>"+verifiedByBinding.getValue());
//  
//                approvedByBinding.setDisabled(false);
//            }
//            else {
//                
//                System.out.println("Verified by isssss when not null>>>>"+verifiedByBinding.getValue());
//                approvedByBinding.setDisabled(false);
//            }
//        }
//    }

//    public void checkByVCL(ValueChangeEvent valueChangeEvent) {
//        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
//        System.out.println("Verified by isssss=s==s>>>>"+verifiedByBinding.getValue());
//        if(valueChangeEvent!=null){
//            
//            if(checkedByBinding.getValue()!=null){
//                
//                System.out.println("Verified by isssss when not null>>>>"+verifiedByBinding.getValue());
//        
//                verifiedByBinding.setDisabled(false);
//            }
//            else {
//                
//                System.out.println("Verified by isssss when not null>>>>"+verifiedByBinding.getValue());
//                verifiedByBinding.setDisabled(false);
//            }
//        }
//    }

    public void setContractTab1Binding(RichShowDetailItem contractTab1Binding) {
        this.contractTab1Binding = contractTab1Binding;
    }

    public RichShowDetailItem getContractTab1Binding() {
        return contractTab1Binding;
    }

    public void setContractTab2Binding(RichShowDetailItem contractTab2Binding) {
        this.contractTab2Binding = contractTab2Binding;
    }

    public RichShowDetailItem getContractTab2Binding() {
        return contractTab2Binding;
    }

    public void itemVCE(ValueChangeEvent valueChangeEvent) {
        if(valueChangeEvent!=null){
            
            
//            ADFUtils.findOperation("setGstInDetail").execute();
////            ADFUtils.findOperation("clearRowOnItemVceConOrder").execute();
//            AdfFacesContext.getCurrentInstance().addPartialTarget(jodetailtable);

        }
    }

    public void setSgstBinding(RichInputText sgstBinding) {
        this.sgstBinding = sgstBinding;
    }

    public RichInputText getSgstBinding() {
        return sgstBinding;
    }

    public void setCgstBinding(RichInputText cgstBinding) {
        this.cgstBinding = cgstBinding;
    }

    public RichInputText getCgstBinding() {
        return cgstBinding;
    }

    public void setIgstBinding(RichInputText igstBinding) {
        this.igstBinding = igstBinding;
    }

    public RichInputText getIgstBinding() {
        return igstBinding;
    }

    public void discountVCE(ValueChangeEvent valueChangeEvent) {
        AdfFacesContext.getCurrentInstance().addPartialTarget(jodetailtable);
    }

    public void contractTypeVCE(ValueChangeEvent valueChangeEvent) {
       // AdfFacesContext.getCurrentInstance().addPartialTarget(getContractTypeBinding());
        if(valueChangeEvent!=null){
            
            if(contractTypeBinding.getValue().toString().equalsIgnoreCase("DR")) {
               
                reqnobind.setDisabled(true);
               pupulateButtonBinding.setDisabled(true);
               copuQuotButtonBinding.setDisabled(false);
               quotationNoBinding.setDisabled(false);
               justificationBinding.isShowRequired();
                justificationBinding.isRequired();
               copyOrderButtonBinding.setDisabled(false);
               copyJoNoBinding.setDisabled(false);
               contractorBinding.setDisabled(false);
            }
            
            else{
              
                reqnobind.setDisabled(false);
                reqnobind.isShowRequired();
                reqnobind.isRequired();
                pupulateButtonBinding.setDisabled(false);
                copuQuotButtonBinding.setDisabled(true);
                quotationNoBinding.setDisabled(true);
                copyOrderButtonBinding.setDisabled(true);
                copyJoNoBinding.setDisabled(true);
                contractorBinding.setDisabled(true);
            }
            
            
        }
    }

    public void setContractTypeBinding(RichSelectOneChoice contractTypeBinding) {
        this.contractTypeBinding = contractTypeBinding;
    }

    public RichSelectOneChoice getContractTypeBinding() {
        return contractTypeBinding;
    }

    public void setStatusBinding(RichSelectOneChoice statusBinding) {
        this.statusBinding = statusBinding;
    }

    public RichSelectOneChoice getStatusBinding() {
        return statusBinding;
    }

    public void setRequisitionAmdNoBinding(RichInputText requisitionAmdNoBinding) {
        this.requisitionAmdNoBinding = requisitionAmdNoBinding;
    }

    public RichInputText getRequisitionAmdNoBinding() {
        return requisitionAmdNoBinding;
    }
    
//////////////Fields enabled/Disabled as per Amendment//////////////////
    public void amdEnableDisable()
    {
        getContractorBinding().setDisabled(true);
        getOpenCloseBinding().setDisabled(true);
        getPupulateButtonBinding().setDisabled(true);
        getPreparedByBinding().setDisabled(true);
        getJobDetailBinding().setDisabled(false);
        getJoAmdDateBinding().setDisabled(true);
        getCheckedDateBinding().setDisabled(true);
        getVerifiedDateBinding().setDisabled(true);
        getApproedDateBinding().setDisabled(true);
        getJoNoBinding().setDisabled(true);
        getJoAmdNoBinding().setDisabled(true);
        getPanNoBinding().setDisabled(true);
        getJoDateBinding().setDisabled(true);
        getGstPerBinding().setDisabled(true);
//        getGstDescBinding().setDisabled(true);
        getUnitCodeBinding().setDisabled(true);
        getIgstBinding().setDisabled(true);
        getSgstBinding().setDisabled(true);
        getCgstBinding().setDisabled(true);
        getPreparedByBinding().setDisabled(true);
        getRequisitionAmdNoBinding().setDisabled(true);
        getReqnobind().setDisabled(true);
        getContractTypeBinding().setDisabled(true);
        getStatusBinding().setDisabled(true);
        getCrTypeBinding().setDisabled(true);
        getOrderTypeBinding().setDisabled(true);
        getProjectJobNoBinding().setDisabled(true);
        getMaterialBinding().setDisabled(true);
        getLocationBinding().setDisabled(true);
        getGstCdBinding().setDisabled(false);
        getOtherBinding().setDisabled(true);
        getSacCodeBinding().setDisabled(true);
        getPaymentTermBinding().setDisabled(true);
        getRemarksBinding().setDisabled(true);
        getProductCdBinding().setDisabled(true);
        getJobCodeBinding().setDisabled(true);
        getSacCdBinding().setDisabled(true);
        getUomBinding().setDisabled(true);
        getDiscountBinding().setDisabled(true);
        getSgstBinding().setDisabled(true);
        getIgstBinding().setDisabled(true);
        getCgstBinding().setDisabled(true);
        getValidFromBinding().setDisabled(false);
        getRemrksBinding().setDisabled(true);
//        getWorkqtyBinding().setDisabled(false);
//        getRatebinding().setDisabled(false);
        getValidToBinding().setDisabled(false);
        getCheckedByBinding().setDisabled(false);
        getVerifiedByBinding().setDisabled(false);
        getApprovedByBinding().setDisabled(false);
        getApprovalAmountBinding().setDisabled(true);
        getCgstAmountBinding().setDisabled(true);
        getSgstAmountBinding().setDisabled(true);
        getIgstAmountBinding().setDisabled(true);
        gstCd1Binding.setDisabled(false);
        getGstPer1Binding().setDisabled(true);
//        valueBinding.setDisabled(false);
    }

    public void setCrTypeBinding(RichSelectOneChoice crTypeBinding) {
        this.crTypeBinding = crTypeBinding;
    }

    public RichSelectOneChoice getCrTypeBinding() {
        return crTypeBinding;
    }

    public void setOrderTypeBinding(RichSelectOneChoice orderTypeBinding) {
        this.orderTypeBinding = orderTypeBinding;
    }

    public RichSelectOneChoice getOrderTypeBinding() {
        return orderTypeBinding;
    }

    public void setProjectJobNoBinding(RichInputComboboxListOfValues projectJobNoBinding) {
        this.projectJobNoBinding = projectJobNoBinding;
    }

    public RichInputComboboxListOfValues getProjectJobNoBinding() {
        return projectJobNoBinding;
    }

    public void setMaterialBinding(RichSelectOneChoice materialBinding) {
        this.materialBinding = materialBinding;
    }

    public RichSelectOneChoice getMaterialBinding() {
        return materialBinding;
    }

    public void setLocationBinding(RichInputComboboxListOfValues locationBinding) {
        this.locationBinding = locationBinding;
    }

    public RichInputComboboxListOfValues getLocationBinding() {
        return locationBinding;
    }

    public void setGstCdBinding(RichInputComboboxListOfValues gstCdBinding) {
        this.gstCdBinding = gstCdBinding;
    }

    public RichInputComboboxListOfValues getGstCdBinding() {
        return gstCdBinding;
    }

    public void setOtherBinding(RichInputText otherBinding) {
        this.otherBinding = otherBinding;
    }

    public RichInputText getOtherBinding() {
        return otherBinding;
    }

    public void setSacCodeBinding(RichInputText sacCodeBinding) {
        this.sacCodeBinding = sacCodeBinding;
    }

    public RichInputText getSacCodeBinding() {
        return sacCodeBinding;
    }

    public void setPaymentTermBinding(RichInputText paymentTermBinding) {
        this.paymentTermBinding = paymentTermBinding;
    }

    public RichInputText getPaymentTermBinding() {
        return paymentTermBinding;
    }

    public void setRemarksBinding(RichInputText remarksBinding) {
        this.remarksBinding = remarksBinding;
    }

    public RichInputText getRemarksBinding() {
        return remarksBinding;
    }

    public void setProductCdBinding(RichInputComboboxListOfValues productCdBinding) {
        this.productCdBinding = productCdBinding;
    }

    public RichInputComboboxListOfValues getProductCdBinding() {
        return productCdBinding;
    }

    public void setJobCodeBinding(RichInputComboboxListOfValues jobCodeBinding) {
        this.jobCodeBinding = jobCodeBinding;
    }

    public RichInputComboboxListOfValues getJobCodeBinding() {
        return jobCodeBinding;
    }

    public void setSacCdBinding(RichInputComboboxListOfValues sacCdBinding) {
        this.sacCdBinding = sacCdBinding;
    }

    public RichInputComboboxListOfValues getSacCdBinding() {
        return sacCdBinding;
    }

    public void setUomBinding(RichInputComboboxListOfValues uomBinding) {
        this.uomBinding = uomBinding;
    }

    public RichInputComboboxListOfValues getUomBinding() {
        return uomBinding;
    }

    public void setDiscountBinding(RichInputText discountBinding) {
        this.discountBinding = discountBinding;
    }

    public RichInputText getDiscountBinding() {
        return discountBinding;
    }

    public void setValidFromBinding(RichInputDate validFromBinding) {
        this.validFromBinding = validFromBinding;
    }

    public RichInputDate getValidFromBinding() {
        return validFromBinding;
    }

    public void setApprovalAmountBinding(RichInputText approvalAmountBinding) {
        this.approvalAmountBinding = approvalAmountBinding;
    }

    public RichInputText getApprovalAmountBinding() {
        return approvalAmountBinding;
    }

    public void setSgstAmountBinding(RichInputText sgstAmountBinding) {
        this.sgstAmountBinding = sgstAmountBinding;
    }

    public RichInputText getSgstAmountBinding() {
        return sgstAmountBinding;
    }

    public void setCgstAmountBinding(RichInputText cgstAmountBinding) {
        this.cgstAmountBinding = cgstAmountBinding;
    }

    public RichInputText getCgstAmountBinding() {
        return cgstAmountBinding;
    }

    public void setIgstAmountBinding(RichInputText igstAmountBinding) {
        this.igstAmountBinding = igstAmountBinding;
    }

    public RichInputText getIgstAmountBinding() {
        return igstAmountBinding;
    }

    public void setValidToBinding(RichInputDate validToBinding) {
        this.validToBinding = validToBinding;
    }

    public RichInputDate getValidToBinding() {
        return validToBinding;
    }

    public void ValueVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
       if(vce!=null)
       {
           vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
           if(vce.getNewValue()!=null)
           {
               BigDecimal val=(BigDecimal)vce.getNewValue();
               BigDecimal cgst =(BigDecimal)cgstBinding.getValue();
               BigDecimal sgst =(BigDecimal)sgstBinding.getValue();
               BigDecimal igst =(BigDecimal)igstBinding.getValue();
               if(cgst!=null && sgst!=null)
                          {
                              BigDecimal amt=new BigDecimal(0);
                              BigDecimal per=new BigDecimal(100);
                              BigDecimal tax=new BigDecimal(0);
                              tax=cgst.add(sgst);
                              System.out.println("Additional tax"+tax);
                              amt=(val).multiply(tax);
                              amt=amt.divide(per);
                              amt=amt.add(val);
                              amtBinding.setValue(amt);
                              amtTransBinding.setValue(amt);
                              AdfFacesContext.getCurrentInstance().addPartialTarget(amtBinding);
                              BigDecimal CgstAmt=((val.multiply(cgst)).divide(per));
                              System.out.println("CgstAmt"+CgstAmt);
                              cgstAmountBinding.setValue(CgstAmt);
                              AdfFacesContext.getCurrentInstance().addPartialTarget(cgstAmountBinding);
                              BigDecimal SgstAmt=((val.multiply(sgst)).divide(per));
                              System.out.println("SgstAmt"+SgstAmt);
                              sgstAmountBinding.setValue(SgstAmt);
                              AdfFacesContext.getCurrentInstance().addPartialTarget(sgstAmountBinding);

                          }
                          else if(igst!=null)
                          {
                              BigDecimal amt=new BigDecimal(0);
                              BigDecimal per=new BigDecimal(100);
                              amt=(val).multiply(igst);
                              amt=amt.divide(per);
                              amt=amt.add(val);
                              amtBinding.setValue(amt);
                              amtTransBinding.setValue(amt);
                              BigDecimal IgstAmt=(val.multiply(igst)).divide(per);
                              System.out.println("IgstAmt"+IgstAmt);
                              igstAmountBinding.setValue(IgstAmt);
                          }
           }
       }
    }

    public void setGstCd1Binding(RichInputComboboxListOfValues gstCd1Binding) {
        this.gstCd1Binding = gstCd1Binding;
    }

    public RichInputComboboxListOfValues getGstCd1Binding() {
        return gstCd1Binding;
    }

    public void setGstPer1Binding(RichInputText gstPer1Binding) {
        this.gstPer1Binding = gstPer1Binding;
    }

    public RichInputText getGstPer1Binding() {
        return gstPer1Binding;
    }

    public void gstVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce.getNewValue()!=null)
        {
            ADFUtils.findOperation("contractOrderDtlGstChanges").execute();
            AdfFacesContext.getCurrentInstance().addPartialTarget(jodetailtable);
        }
    }

    public void rateVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce.getNewValue()!=null && workqtyBinding.getValue()!=null)
        {
            BigDecimal qty=(BigDecimal)workqtyBinding.getValue();            
            System.out.println("A1=>" + qty);
            BigDecimal rate =(BigDecimal)vce.getNewValue();
            System.out.println("B1=>" + rate);
            BigDecimal val = (BigDecimal)qty.multiply(rate);
            System.out.println("Amount is=>" + val);
            valueBinding.setValue(val);
            BigDecimal cgst =(BigDecimal)cgstBinding.getValue();
            BigDecimal sgst =(BigDecimal)sgstBinding.getValue();
            BigDecimal igst =(BigDecimal)igstBinding.getValue();
            if(cgst!=null && sgst!=null)
                       {
                           BigDecimal amt=new BigDecimal(0);
                           BigDecimal per=new BigDecimal(100);
                           BigDecimal tax=new BigDecimal(0);
                           tax=cgst.add(sgst);
                           System.out.println("Additional tax"+tax);
                           amt=(val).multiply(tax);
                           amt=amt.divide(per);
                           amt=amt.add(val);
                           amtBinding.setValue(amt);
                           amtTransBinding.setValue(amt);
                           AdfFacesContext.getCurrentInstance().addPartialTarget(amtBinding);
                           BigDecimal CgstAmt=((val.multiply(cgst)).divide(per));
                           System.out.println("CgstAmt"+CgstAmt);
                           cgstAmountBinding.setValue(CgstAmt);
                           AdfFacesContext.getCurrentInstance().addPartialTarget(cgstAmountBinding);
                           BigDecimal SgstAmt=((val.multiply(sgst)).divide(per));
                           System.out.println("SgstAmt"+SgstAmt);
                           sgstAmountBinding.setValue(SgstAmt);
                           AdfFacesContext.getCurrentInstance().addPartialTarget(sgstAmountBinding);
                       }
                       else if(igst!=null)
                       {
                           BigDecimal amt=new BigDecimal(0);
                           BigDecimal per=new BigDecimal(100);
                           amt=(val).multiply(igst);
                           amt=amt.divide(per);
                           amt=amt.add(val);
                           amtBinding.setValue(amt);
                           amtTransBinding.setValue(amt);
                           AdfFacesContext.getCurrentInstance().addPartialTarget(amtBinding);
                           BigDecimal IgstAmt=(val.multiply(igst)).divide(per);
                           System.out.println("IgstAmt"+IgstAmt);
                           igstAmountBinding.setValue(IgstAmt);
                           AdfFacesContext.getCurrentInstance().addPartialTarget(igstAmountBinding);
                       }
//            AdfFacesContext.getCurrentInstance().addPartialTarget(jodetailtable);
        }
    }

    public void setValueBinding(RichInputText valueBinding) {
        this.valueBinding = valueBinding;
    }

    public RichInputText getValueBinding() {
        return valueBinding;
    }

    public void setAmtBinding(RichInputText amtBinding) {
        this.amtBinding = amtBinding;
    }

    public RichInputText getAmtBinding() {
        return amtBinding;
    }

    public void contractOrderQuotationCopyDtl(ActionEvent actionEvent) {
        ADFUtils.findOperation("copyQuotationInContractOrder").execute();
        ADFUtils.findOperation("setGstInDetail").execute();
        AdfFacesContext.getCurrentInstance().addPartialTarget(jodetailtable);
          if(validFromBinding.getValue()==null)
                      {
                      Calendar cal=Calendar.getInstance();
                      Date dt=cal.getTime();
                      int i=30;
        //                          Timestamp RefDate=(Timestamp)dt;
                      cal.setTimeInMillis(dt.getTime());
                      cal.add(Calendar.DAY_OF_MONTH,i);
                      Timestamp vaidTo=new Timestamp(cal.getTime().getTime());
                      validFromBinding.setValue(dt);
                      validToBinding.setValue(vaidTo);  
                      }
                      else if(validToBinding.getValue()==null)
                      {
                          Calendar cal=Calendar.getInstance();
                          Date dt=cal.getTime();
                          int i=30;
        //                              Timestamp RefDate=(Timestamp)validFromBinding.getValue();
                          cal.setTimeInMillis(dt.getTime());
                          cal.add(Calendar.DAY_OF_MONTH,i);
                          Timestamp vaidTo=new Timestamp(cal.getTime().getTime());
                          validToBinding.setValue(vaidTo);  
                      }    
          detailcreateBinding.setDisabled(true);
        productCdBinding.setDisabled(true);
        contractorBinding.setDisabled(true);
        gstCdBinding.setDisabled(true);
        quotationNoBinding.setDisabled(true);
        copuQuotButtonBinding.setDisabled(true);
        jobCodeBinding.setDisabled(true);
        jobDetailBinding.setDisabled(false);
        lumpSumBinding.setDisabled(true);
    }

    public void setLumpSumBinding(RichSelectBooleanCheckbox lumpSumBinding) {
        this.lumpSumBinding = lumpSumBinding;
    }

    public RichSelectBooleanCheckbox getLumpSumBinding() {
        return lumpSumBinding;
    }

    public void setQuotationNoBinding(RichInputComboboxListOfValues quotationNoBinding) {
        this.quotationNoBinding = quotationNoBinding;
    }

    public RichInputComboboxListOfValues getQuotationNoBinding() {
        return quotationNoBinding;
    }

    public void setCopuQuotButtonBinding(RichButton copuQuotButtonBinding) {
        this.copuQuotButtonBinding = copuQuotButtonBinding;
    }

    public RichButton getCopuQuotButtonBinding() {
        return copuQuotButtonBinding;
    }

    public void setAmtTransBinding(RichInputText amtTransBinding) {
        this.amtTransBinding = amtTransBinding;
    }

    public RichInputText getAmtTransBinding() {
        return amtTransBinding;
    }

    public void termsDeleteDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                    {
                    ADFUtils.findOperation("Delete2").execute();
                    System.out.println("Record Delete Successfully");
                   
                        FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                        FacesContext fc = FacesContext.getCurrentInstance();  
                        fc.addMessage(null, Message);                     
                    }
                AdfFacesContext.getCurrentInstance().addPartialTarget(termsTableBinding);
    }

    public void setTermsTableBinding(RichTable termsTableBinding) {
        this.termsTableBinding = termsTableBinding;
    }

    public RichTable getTermsTableBinding() {
        return termsTableBinding;
    }

    public void setTermsConditionButtonBinding(RichButton termsConditionButtonBinding) {
        this.termsConditionButtonBinding = termsConditionButtonBinding;
    }

    public RichButton getTermsConditionButtonBinding() {
        return termsConditionButtonBinding;
    }

    public void setGstPerTransBinding(RichInputText gstPerTransBinding) {
        this.gstPerTransBinding = gstPerTransBinding;
    }

    public RichInputText getGstPerTransBinding() {
        return gstPerTransBinding;
    }

    public void setJustificationBinding(RichInputText justificationBinding) {
        this.justificationBinding = justificationBinding;
    }

    public RichInputText getJustificationBinding() {
        return justificationBinding;
    }

    public void WorkQuantityVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce.getNewValue()!=null && quotationNoBinding.getValue()!=null || reqnobind.getValue()!=null)
        {
            BigDecimal zero=new BigDecimal(0);
            BigDecimal oldVal=(BigDecimal)vce.getOldValue() == null ? zero :(BigDecimal)vce.getOldValue() ;
            BigDecimal newVal=(BigDecimal) vce.getNewValue();
            System.out.println("Work Qty==>"+oldVal+" New Work Qty==>"+newVal);
            if(oldVal.compareTo(zero)==1 && newVal.compareTo(oldVal)==1 && quotationNoBinding.getValue()==null)
            {
                System.out.println("Work Qty==>"+oldVal+" New Work Qty==>"+newVal);
                ADFUtils.showMessage("Work Quantity cannot be greater than Requisition Quantity. ", 0);
                status="T";
            }
//            else if(ratebinding.getValue()!=null && newVal!=null && oldVal!=null)
//            {
//                status="F";
//                if(newVal.compareTo(oldVal)==1 && quotationNoBinding.getValue()!=null)
//                {
//                    ADFUtils.showMessage("Work Quantity cannot be greater than Quotation Quantity. ", 0);
//                    status1="T";
//                }
             else
                {
                    status="F";
                    BigDecimal rate=(BigDecimal)ratebinding.getValue();
                    BigDecimal val=newVal.multiply(rate);
                    valueBinding.setValue(val);
                }

//            }            
        }
    }

    public void validfromValidation(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if(object!=null && validToBinding.getValue()!=null)
        {
        oracle.jbo.domain.Timestamp from=(oracle.jbo.domain.Timestamp)object;
        String str_from = from.toString();
        System.out.println("FROMMMM"+str_from);
        String str_to = validToBinding.getValue().toString();
        System.out.println("TOOOO"+str_to);
        oracle.jbo.domain.Date  dt_frm = new oracle.jbo.domain.Date(str_from.substring(0, 10));
        oracle.jbo.domain.Date  dt_to = new oracle.jbo.domain.Date(str_to.substring(0, 10));            
        if(dt_frm.compareTo(dt_to)==1)
        {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Valid From cannot be greater than Valid To.", null));
        }
        }

    }

    public void checkedByValidation(ValueChangeEvent vce) {
//        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
//        OperationBinding ob = ADFUtils.findOperation("checkApprovalStatus");
//        ob.getParamsMap().put("formNm", "CT");
//        ob.getParamsMap().put("authoLim", "CH");
//        ob.getParamsMap().put("empcd", vce.getNewValue().toString());
//        ob.execute();
//        System.out.println(" EMP CD"+vce.getNewValue().toString());
//        if((ob.getResult() !=null && ob.getResult().equals("Y")))
//        {
//           Row row=(Row)ADFUtils.evaluateEL("#{bindings.ContractOrderHeaderVO1Iterator.currentRow}");
//           row.setAttribute("CheckedBy", null);
//           ADFUtils.showMessage("You have no authority to check this Contract Order.",0);
//        }
        
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce.getNewValue()!=null)
        {
        if(orderTypeBinding.getValue().equals("R"))
        {
        System.out.println("IN VCE Approved");
        OperationBinding ob = ADFUtils.findOperation("checkApprovalFunctionStatus");
        ob.getParamsMap().put("formNm", "CT");
        ob.getParamsMap().put("authoLim", "CH");
        ob.getParamsMap().put("UnitCd", unitCodeBinding.getValue());
        System.out.println("Total SUM========>"+ADFUtils.resolveExpression("#{bindings.ValueTrans.inputValue}"));
        ob.getParamsMap().put("Amount", ADFUtils.resolveExpression("#{bindings.ValueTrans.inputValue}"));
        ob.getParamsMap().put("empcd", vce.getNewValue().toString());
        ob.execute();
        System.out.println(" EMP CD"+vce.getNewValue().toString());
        if((ob.getResult() !=null && !ob.getResult().equals("Y")))
        {
           Row row=(Row)ADFUtils.evaluateEL("#{bindings.ContractOrderHeaderVO1Iterator.currentRow}");
           row.setAttribute("CheckedBy", null);
           row.setAttribute("CheckedDt", null);
           ADFUtils.showMessage(ob.getResult().toString(), 0);
        }
        }else
            {
        
            OperationBinding ob1 = ADFUtils.findOperation("checkApprovalFunctionStatus");
            ob1.getParamsMap().put("formNm", "CTC");
            ob1.getParamsMap().put("authoLim", "CH");
            ob1.getParamsMap().put("UnitCd", unitCodeBinding.getValue());
            System.out.println("Total SUM========>"+ADFUtils.resolveExpression("#{bindings.ValueTrans.inputValue}"));
            ob1.getParamsMap().put("Amount", ADFUtils.resolveExpression("#{bindings.ValueTrans.inputValue}"));
            ob1.getParamsMap().put("empcd", vce.getNewValue().toString());
            ob1.execute();
            System.out.println(" EMP CD"+vce.getNewValue().toString());
            if((ob1.getResult() !=null && !ob1.getResult().equals("Y")))
            {
               Row row=(Row)ADFUtils.evaluateEL("#{bindings.ContractOrderHeaderVO1Iterator.currentRow}");
               row.setAttribute("CheckedBy", null);
               row.setAttribute("CheckedDt", null);
               ADFUtils.showMessage(ob1.getResult().toString(), 0);
            }
        }
        }
    }

    public void verifyByVCL(ValueChangeEvent vce) {
//        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
//        OperationBinding ob = ADFUtils.findOperation("checkApprovalStatus");
//        ob.getParamsMap().put("formNm", "CT");
//        ob.getParamsMap().put("authoLim", "VR");
//        ob.getParamsMap().put("empcd", vce.getNewValue().toString());
//        ob.execute();
//        System.out.println(" EMP CD"+vce.getNewValue().toString());
//        if((ob.getResult() !=null && ob.getResult().equals("Y")))
//        {
//           Row row=(Row)ADFUtils.evaluateEL("#{bindings.ContractOrderHeaderVO1Iterator.currentRow}");
//           row.setAttribute("VerifiedBy", null);
//           ADFUtils.showMessage("You have no authority to verify this Contract Order.",0);
//        }
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce.getNewValue()!=null)
        {
        if(orderTypeBinding.getValue().equals("R"))
        {
        System.out.println("IN VCE Approved");
        OperationBinding ob = ADFUtils.findOperation("checkApprovalFunctionStatus");
        ob.getParamsMap().put("formNm", "CT");
        ob.getParamsMap().put("authoLim", "VR");
        ob.getParamsMap().put("UnitCd", unitCodeBinding.getValue());
        System.out.println("Total SUM========>"+ADFUtils.resolveExpression("#{bindings.ValueTrans.inputValue}"));
        ob.getParamsMap().put("Amount", ADFUtils.resolveExpression("#{bindings.ValueTrans.inputValue}"));
        ob.getParamsMap().put("empcd", vce.getNewValue().toString());
        ob.execute();
        System.out.println(" EMP CD"+vce.getNewValue().toString());
        if((ob.getResult() !=null && !ob.getResult().equals("Y")))
        {
           Row row=(Row)ADFUtils.evaluateEL("#{bindings.ContractOrderHeaderVO1Iterator.currentRow}");
           row.setAttribute("VerifiedBy", null);
           row.setAttribute("VerifiedDt", null);
           ADFUtils.showMessage(ob.getResult().toString(), 0);
        }
        }else
            {
        
            OperationBinding ob1 = ADFUtils.findOperation("checkApprovalFunctionStatus");
            ob1.getParamsMap().put("formNm", "CTC");
            ob1.getParamsMap().put("authoLim", "VR");
            ob1.getParamsMap().put("UnitCd", unitCodeBinding.getValue());
            System.out.println("Total SUM========>"+ADFUtils.resolveExpression("#{bindings.ValueTrans.inputValue}"));
            ob1.getParamsMap().put("Amount", ADFUtils.resolveExpression("#{bindings.ValueTrans.inputValue}"));
            ob1.getParamsMap().put("empcd", vce.getNewValue().toString());
            ob1.execute();
            System.out.println(" EMP CD"+vce.getNewValue().toString());
            if((ob1.getResult() !=null && !ob1.getResult().equals("Y")))
            {
               Row row=(Row)ADFUtils.evaluateEL("#{bindings.ContractOrderHeaderVO1Iterator.currentRow}");
               row.setAttribute("VerifiedBy", null);
               row.setAttribute("VerifiedDt", null);
               ADFUtils.showMessage(ob1.getResult().toString(), 0);
            }
        }
        }
    }

    public void approvedByVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce.getNewValue()!=null)
        {
        if(orderTypeBinding.getValue().equals("R"))
        {
        System.out.println("IN VCE Approved");
        OperationBinding ob = ADFUtils.findOperation("checkApprovalFunctionStatus");
        ob.getParamsMap().put("formNm", "CT");
        ob.getParamsMap().put("authoLim", "AP");
        ob.getParamsMap().put("UnitCd", unitCodeBinding.getValue());
        System.out.println("Total SUM========>"+ADFUtils.resolveExpression("#{bindings.ValueTrans.inputValue}"));
        ob.getParamsMap().put("Amount", ADFUtils.resolveExpression("#{bindings.ValueTrans.inputValue}"));
        ob.getParamsMap().put("empcd", vce.getNewValue().toString());
        ob.execute();
        System.out.println(" EMP CD"+vce.getNewValue().toString());
        if((ob.getResult() !=null && !ob.getResult().equals("Y")))
        {
           Row row=(Row)ADFUtils.evaluateEL("#{bindings.ContractOrderHeaderVO1Iterator.currentRow}");
           row.setAttribute("ApprovedBy", null);
           row.setAttribute("ApprovedDt", null);
           ADFUtils.showMessage(ob.getResult().toString(), 0);
        }
        }else
            {
        
            OperationBinding ob1 = ADFUtils.findOperation("checkApprovalFunctionStatus");
            ob1.getParamsMap().put("formNm", "CTC");
            ob1.getParamsMap().put("authoLim", "AP");
            ob1.getParamsMap().put("UnitCd", unitCodeBinding.getValue());
            System.out.println("Total SUM========>"+ADFUtils.resolveExpression("#{bindings.ValueTrans.inputValue}"));
            ob1.getParamsMap().put("Amount", ADFUtils.resolveExpression("#{bindings.ValueTrans.inputValue}"));
            ob1.getParamsMap().put("empcd", vce.getNewValue().toString());
            ob1.execute();
            System.out.println(" EMP CD"+vce.getNewValue().toString());
            if((ob1.getResult() !=null && !ob1.getResult().equals("Y")))
            {
               Row row=(Row)ADFUtils.evaluateEL("#{bindings.ContractOrderHeaderVO1Iterator.currentRow}");
               row.setAttribute("ApprovedBy", null);
               row.setAttribute("ApprovedDt", null);
               ADFUtils.showMessage(ob1.getResult().toString(), 0);
            }
        }
        }
    }

    public void CreateTerms(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert3").execute();
        Row row=(Row)ADFUtils.evaluateEL("#{bindings.ContractOrderTermDtlVO1.currentRow}");
        row.setAttribute("SrNo", ADFUtils.evaluateEL("#{bindings.ContractOrderTermDtlVO1.estimatedRowCount}"));
    }

    public void contractNoPopulateAL(ActionEvent actionEvent) {
        if(transContractBinding.getValue()!=null){
           OperationBinding op=ADFUtils.findOperation("ContractOrderTermsDataPopulate");
           op.getParamsMap().put("contractNo", transContractBinding.getValue());
           op.execute();
        }
        else{
            ADFUtils.showMessage("Please select Contract No.", 0);
            }
    }

    public void setTransContractBinding(RichInputComboboxListOfValues transContractBinding) {
        this.transContractBinding = transContractBinding;
    }

    public RichInputComboboxListOfValues getTransContractBinding() {
        return transContractBinding;
    }

    public void termsDataPopulate(ActionEvent actionEvent) {
        ADFUtils.findOperation("ContractOrderTermsConditionPopulate").execute();
    }

    public void multiPayDeleteDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
        {
                        ADFUtils.findOperation("Delete3").execute();
                        System.out.println("Record Delete Successfully");
                        FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                        FacesContext fc = FacesContext.getCurrentInstance();  
                        fc.addMessage(null, Message);                 
                    }
                AdfFacesContext.getCurrentInstance().addPartialTarget(multiPayTermTableBinding);
    }

    public void setMultiPayTermTableBinding(RichTable multiPayTermTableBinding) {
        this.multiPayTermTableBinding = multiPayTermTableBinding;
    }

    public RichTable getMultiPayTermTableBinding() {
        return multiPayTermTableBinding;
    }

    public void validateMultiPercentDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
        {
          OperationBinding op= ADFUtils.findOperation("percentageValidationCO");
          op.execute();
          System.out.println("Result"+op.getResult());
          if(op.getResult().equals("Y"))
          {
            ADFUtils.showMessage("Payment Percentage must be equals to 100.", 0);
          }
        }
    }

    public void percentValidateVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());            
        if(vce.getNewValue()!=null)
        {
           OperationBinding op= ADFUtils.findOperation("percentageValidationCO");
           op.execute();
           System.out.println("Result"+op.getResult());
           if(op.getResult().equals("Y"))
           {
               ADFUtils.showMessage("Payment Percentage must be equals to 100.", 0);
           }
        }
    }

    public void setPaymentTermButtonBinding(RichButton paymentTermButtonBinding) {
        this.paymentTermButtonBinding = paymentTermButtonBinding;
    }

    public RichButton getPaymentTermButtonBinding() {
        return paymentTermButtonBinding;
    }

    public void setRemrksBinding(RichInputText remrksBinding) {
        this.remrksBinding = remrksBinding;
    }

    public RichInputText getRemrksBinding() {
        return remrksBinding;
    }
    public void lumsumCaseEnable()
    {System.out.println("LUMSUMMMMM");
        DCIteratorBinding dci = ADFUtils.findIterator("ContractOrderDetailVO1Iterator");
        RowSetIterator rsi=dci.getViewObject().createRowSetIterator(null);
            while(rsi.hasNext()) {
                System.out.println("LUMSUMMMMM");
                Row r = rsi.next();
                String lumsum=(String) r.getAttribute("Lumsum");
                System.out.println("LUMSUM ITERATOR"+lumsum);
                if(lumsum!=null && lumsum.equalsIgnoreCase("N"))
                {
                    workqtyBinding.setDisabled(false);
                    ratebinding.setDisabled(false);
                }else
                {
                    valueBinding.setDisabled(false);
                }
            }
            rsi.closeRowSetIterator();
            ReqNo=(String)reqnobind.getValue();
            ReqAmdNo=(BigDecimal)requisitionAmdNoBinding.getValue();
            System.out.println("Before Amd Value=====>"+ReqNo+" AmdNo"+ReqAmdNo);
    }

    public void paymentTermVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance()); 
        String JoNo=(String)joNoBinding.getValue();
        if(vce.getNewValue()!=null && JoNo!=null)
        {
            System.out.println("IN VCE");
            Row row=(Row)ADFUtils.evaluateEL("#{bindings.ContractOrderMultiPayTermVO1.currentRow}");
            row.setAttribute("CoNo", joNoBinding.getValue());
            row.setAttribute("AmdNo", joAmdNoBinding.getValue());
        }
    }

    public void setAmountBinding(RichInputText amountBinding) {
        this.amountBinding = amountBinding;
    }

    public RichInputText getAmountBinding() {
        return amountBinding;
    }

    public void setRefDocBindingTab(RichShowDetailItem refDocBindingTab) {
        this.refDocBindingTab = refDocBindingTab;
    }

    public RichShowDetailItem getRefDocBindingTab() {
        return refDocBindingTab;
    }

    public void uploadAction(ValueChangeEvent vce) {
        //Get File Object from VC Event
            UploadedFile fileVal = (UploadedFile) vce.getNewValue();
            System.out.println("File Value==============<>" + fileVal);
            //Upload File to path- Return actual server path
            String path = ADFUtils.uploadFile(fileVal);
            System.out.println(fileVal.getContentType());
            //Method to insert data in table to keep track of uploaded files
            OperationBinding ob = ADFUtils.findOperation("setFileDataCO");
            ob.getParamsMap().put("name", fileVal.getFilename());
            ob.getParamsMap().put("path", path);
            ob.getParamsMap().put("contTyp", fileVal.getContentType());
            ob.getParamsMap().put("seqNo", ADFUtils.evaluateEL("#{bindings.DocAttachRefDtlVO7.estimatedRowCount+1}"));
            ob.execute();
            System.out.println("IN VCE EXECUTED");
            // Reset inputFile component after upload
            ResetUtils.reset(vce.getComponent());
                       }

    public void downloadAction(FacesContext facesContext, OutputStream outputStream) throws Exception{
        File filed = new File(pathBinding.getValue().toString());
                                  FileInputStream fis;
                                  byte[] b;
                                  try {
                                      fis = new FileInputStream(filed);

                                      int n;
                                      while ((n = fis.available()) > 0) {
                                          b = new byte[n];
                                          int result = fis.read(b);
                                          outputStream.write(b, 0, b.length);
                                          if (result == -1)
                                              break;
                                      }
                                  } catch (Exception e) {
                                      e.printStackTrace();
                                  }
                                  outputStream.flush();
        

    }

    public void setPathBinding(RichInputText pathBinding) {
        this.pathBinding = pathBinding;
    }

    public RichInputText getPathBinding() {
        return pathBinding;
    }

    public void setContentTypeBinding(RichInputText contentTypeBinding) {
        this.contentTypeBinding = contentTypeBinding;
    }

    public RichInputText getContentTypeBinding() {
        return contentTypeBinding;
    }

    public void setDocDateBinding(RichInputDate docDateBinding) {
        this.docDateBinding = docDateBinding;
    }

    public RichInputDate getDocDateBinding() {
        return docDateBinding;
    }

    public void setSeqNoBinding(RichInputText seqNoBinding) {
        this.seqNoBinding = seqNoBinding;
    }

    public RichInputText getSeqNoBinding() {
        return seqNoBinding;
    }

    public void setUnitCdBinding(RichInputText unitCdBinding) {
        this.unitCdBinding = unitCdBinding;
    }

    public RichInputText getUnitCdBinding() {
        return unitCdBinding;
    }

    public void setDocNoBinding(RichInputText docNoBinding) {
        this.docNoBinding = docNoBinding;
    }

    public RichInputText getDocNoBinding() {
        return docNoBinding;
    }

    public void setDocTypeBinding(RichInputText docTypeBinding) {
        this.docTypeBinding = docTypeBinding;
    }

    public RichInputText getDocTypeBinding() {
        return docTypeBinding;
    }

    public void setDocFileNmBinding(RichInputText docFileNmBinding) {
        this.docFileNmBinding = docFileNmBinding;
    }

    public RichInputText getDocFileNmBinding() {
        return docFileNmBinding;
    }

    public void deleteDocDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete4").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(docRefTableBinding);    }

    public void setDocRefTableBinding(RichTable docRefTableBinding) {
        this.docRefTableBinding = docRefTableBinding;
    }

    public RichTable getDocRefTableBinding() {
        return docRefTableBinding;
    }

    public void duplicateReqNoInCO(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance()); 
        String ReqNo=(String)vce.getNewValue();
        BigDecimal ReqAmdNo=(BigDecimal) requisitionAmdNoBinding.getValue();
        System.out.println("In VCE ReqNo==>"+ReqNo+" In VCE ReqAmdNo==>"+ReqAmdNo);
        if(ReqNo!=null && ReqAmdNo!=null)
        {
            OperationBinding op=ADFUtils.findOperation("checkRequisitionInCO");
            op.getParamsMap().put("ReqNo", ReqNo);
            op.getParamsMap().put("ReqAmdNo", ReqAmdNo);
            op.execute();
            System.out.println("Operation Result======>"+op.getResult());
            if(op.getResult()!=null && op.getResult().equals("Y"))
            {
                ADFUtils.showMessage("For the Respective Contract Requisition,Contract Order already Exit. ", 0);
                Row row=(Row)ADFUtils.evaluateEL("#{bindings.ContractOrderHeaderVO1Iterator.currentRow}");
                row.setAttribute("ReqNo", null);
                row.setAttribute("ReqAmdNo", null);
            }
        }
    }

    public void gstVCLAmend(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance()); 
        if(vce.getOldValue()!=null && vce.getNewValue()!=null)
        {
            System.out.println("Old Gst Code====>"+vce.getOldValue()+" New Gst Code====>"+vce.getNewValue());
            DCIteratorBinding dci = ADFUtils.findIterator("ContractOrderDetailVO1Iterator");
            RowSetIterator rsi=dci.getViewObject().createRowSetIterator(null);
                while(rsi.hasNext()) {
                    Row r = rsi.next();
                    if(vce.getNewValue()!=null)
                    {
                      r.setAttribute("GstCd", vce.getNewValue());
                    }
                }
                rsi.closeRowSetIterator();
            ADFUtils.findOperation("setGstInDetail").execute();
        }
    }

    public void setCopyJoNoBinding(RichInputComboboxListOfValues copyJoNoBinding) {
        this.copyJoNoBinding = copyJoNoBinding;
    }

    public RichInputComboboxListOfValues getCopyJoNoBinding() {
        return copyJoNoBinding;
    }

    public void setCopyJoAmdNoBinding(RichInputText copyJoAmdNoBinding) {
        this.copyJoAmdNoBinding = copyJoAmdNoBinding;
    }

    public RichInputText getCopyJoAmdNoBinding() {
        return copyJoAmdNoBinding;
    }

    public void setCopyJoIdBinding(RichInputText copyJoIdBinding) {
        this.copyJoIdBinding = copyJoIdBinding;
    }

    public RichInputText getCopyJoIdBinding() {
        return copyJoIdBinding;
    }

    public void copyContractOrderAL(ActionEvent actionEvent) {
        if(copyJoIdBinding.getValue()!=null && copyJoNoBinding.getValue()!=null && copyJoAmdNoBinding.getValue()!=null)
                {
                OperationBinding op=ADFUtils.findOperation("copyContractOrderData");
                op.getParamsMap().put("AmdNo", copyJoAmdNoBinding.getValue());
                op.getParamsMap().put("JoId", copyJoIdBinding.getValue());
                op.getParamsMap().put("JoNo", copyJoNoBinding.getValue());
                op.execute();
                }

    }

    public void setCopyOrderButtonBinding(RichButton copyOrderButtonBinding) {
        this.copyOrderButtonBinding = copyOrderButtonBinding;
    }

    public RichButton getCopyOrderButtonBinding() {
        return copyOrderButtonBinding;
    }

    public void setAmountHdBinding(RichInputText amountHdBinding) {
        this.amountHdBinding = amountHdBinding;
    }

    public RichInputText getAmountHdBinding() {
        return amountHdBinding;
    }

    public void setDownloadLinkBinding(RichCommandLink downloadLinkBinding) {
        this.downloadLinkBinding = downloadLinkBinding;
    }

    public RichCommandLink getDownloadLinkBinding() {
        return downloadLinkBinding;
    }

    public void setContractdateNepalBinding(RichInputText contractdateNepalBinding) {
        this.contractdateNepalBinding = contractdateNepalBinding;
    }

    public RichInputText getContractdateNepalBinding() {
        return contractdateNepalBinding;
    }

    public void setJoamendmentDateNepalBinding(RichInputText joamendmentDateNepalBinding) {
        this.joamendmentDateNepalBinding = joamendmentDateNepalBinding;
    }

    public RichInputText getJoamendmentDateNepalBinding() {
        return joamendmentDateNepalBinding;
    }
}
