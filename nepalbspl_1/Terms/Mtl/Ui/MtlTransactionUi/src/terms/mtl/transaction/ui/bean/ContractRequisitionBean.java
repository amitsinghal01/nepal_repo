package terms.mtl.transaction.ui.bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.sql.Timestamp;

import java.text.ParseException;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import javax.faces.validator.ValidatorException;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.input.RichSelectOneRadio;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.nav.RichCommandLink;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.adf.view.rich.util.ResetUtils;

import oracle.binding.OperationBinding;

import oracle.jbo.JboException;
import oracle.jbo.Row;

import oracle.jbo.RowSetIterator;

import org.apache.myfaces.trinidad.model.UploadedFile;


public class ContractRequisitionBean {
    private RichTable createContractRequisitionTableBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichSelectOneRadio typeBinding;
    private RichInputComboboxListOfValues capitalApprovalNoBinding;
    private RichInputText requisitionNoBinding;
    private RichInputDate requisitionDateBinding;
    private RichInputComboboxListOfValues preparedByBinding;
    private RichInputComboboxListOfValues checkedByBinding;
    private RichInputDate checkedByDateBinding;
    private RichInputComboboxListOfValues verifiedByBinding;
    private RichInputDate verifiedByDateBinding;
    private RichInputComboboxListOfValues approvedByBinding;
    private RichInputDate approvedByDateBinding;
    private RichInputText contractValueApproxBinding;
    private RichInputDate periodOfContractFromBinding;
    private RichButton headerEditBinding;
    private RichButton detailCreateBinding;
    //    private RichButton detailDeleteBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichOutputText outputTextBinding;
    private RichInputText unitNameBinding;
    private RichInputText preparedByNameBinding;
    private RichInputText checkedByNameBinding;
    private RichInputText verifiedByNameBinding;
    private RichInputText approvedByNameBinding;
    private RichInputText locationDescriptionBinding;
    private RichInputText preferredContractNameBinding;
    private RichInputText productDescriptionBinding;
    private RichInputText jobDetailBinding;
    private RichInputText workQtyBinding;
    private RichInputText rateBinding;
    private RichInputComboboxListOfValues locationCodeBinding;
    private RichPopup amendmentPoupConfirmationBinding;
    private RichInputText amendNoBinding;
    private RichInputComboboxListOfValues requisitionNoCopyBinding;
    private RichInputText amdNoCopyBinding;
    private RichButton copyButtonBinding;
    private RichInputComboboxListOfValues quotNoBinding;
    private RichSelectOneChoice requestTypeBinding;
    private RichInputComboboxListOfValues prodCdBinding;
    private RichButton copyQuotBinding;
    private RichInputComboboxListOfValues contractorBinding;
    private RichTable termsConditionTableBinding;
    private RichButton termsConditionButtonBinding;
    private RichInputDate periodOfContractToBinding;
    private RichInputComboboxListOfValues deptParentBinding;
    private RichTable contractMultiPaymentTermTableBinding;
    private RichButton paymentTermButtonBinding;
    private RichInputText pathBinding;
    private RichInputText contentTypeBinding;
    private RichInputText docFileNameBinding;
    private RichInputText docTypeBinding;
    private RichInputText docNoBinding;
    private RichInputDate docDateBinding;
    private RichInputText unitCdBinding;
    private RichInputText seqNoBinding;
    private RichTable docRefTableBinding;
    private RichShowDetailItem referenceDocTabBinding;
    private RichShowDetailItem reqDtlTabBinding;
    private RichCommandLink downloadLinkBinding;
    private RichSelectOneChoice statusBinding;
    private RichInputText reqIdBinding;
    private RichInputText reqNepDateBinding;
    private RichInputText reqDateNpBinding;

    public ContractRequisitionBean() {
    }

    public void saveGeneratedRequisitionNo(ActionEvent actionEvent) {
        String empcd = (String) ADFUtils.resolveExpression("#{pageFlowScope.empCode}");
        System.out.println("empcd" + empcd);
        ADFUtils.setLastUpdatedBy("ContractRequisitionHeaderVO1Iterator", "LastUpdatedBy");

        if (!requestTypeBinding.isDisabled()) {
            OperationBinding op = ADFUtils.findOperation("generateContractRequisitionNo");
            op.execute();
            ADFUtils.findOperation("Commit").execute();
            FacesMessage Message =
                new FacesMessage("Record Saved Successfully. New Contract Requisition Number  is " +
                                 requisitionNoBinding.getValue() + ".");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
        } else {
            if (requisitionNoBinding.getValue() != null) {
                DCIteratorBinding dci = ADFUtils.findIterator("DocAttachRefDtlVO10Iterator");
                RowSetIterator rsi = dci.getViewObject().createRowSetIterator(null);
                while (rsi.hasNext()) {
                    System.out.println("LUMSUMMMMM");
                    Row r = rsi.next();
                    String ReqNo = (String) requisitionNoBinding.getValue();
                    if (ReqNo != null) {
                        r.setAttribute("RefDocNo", ReqNo);
                    }
                }
                rsi.closeRowSetIterator();
            }
            ADFUtils.findOperation("Commit").execute();
            FacesMessage Message = new FacesMessage("Record Update Successfully.");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }


    }


    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(createContractRequisitionTableBinding);
    }

    public void setCreateContractRequisitionTableBinding(RichTable createContractRequisitionTableBinding) {
        this.createContractRequisitionTableBinding = createContractRequisitionTableBinding;
    }

    public RichTable getCreateContractRequisitionTableBinding() {
        return createContractRequisitionTableBinding;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setTypeBinding(RichSelectOneRadio typeBinding) {
        this.typeBinding = typeBinding;
    }

    public RichSelectOneRadio getTypeBinding() {
        return typeBinding;
    }

    public void setCapitalApprovalNoBinding(RichInputComboboxListOfValues capitalApprovalNoBinding) {
        this.capitalApprovalNoBinding = capitalApprovalNoBinding;
    }

    public RichInputComboboxListOfValues getCapitalApprovalNoBinding() {
        return capitalApprovalNoBinding;
    }

    public void setRequisitionNoBinding(RichInputText requisitionNoBinding) {
        this.requisitionNoBinding = requisitionNoBinding;
    }

    public RichInputText getRequisitionNoBinding() {
        return requisitionNoBinding;
    }

    public void setRequisitionDateBinding(RichInputDate requisitionDateBinding) {
        this.requisitionDateBinding = requisitionDateBinding;
    }

    public RichInputDate getRequisitionDateBinding() {
        try {
            System.out.println("set getEntryDateBinding" + ADFUtils.getTodayDate());
            String date = ADFUtils.convertAdToBs(ADFUtils.getTodayDate().toString());
            System.out.println("setter getvaluedate" + date);
            //            reqDateNepalBinding.setValue((String)date);
            DCIteratorBinding dci = ADFUtils.findIterator("ContractRequisitionHeaderVO1Iterator");
            dci.getCurrentRow().setAttribute("ReqDateNp", date);
            //            Row row=(Row)ADFUtils.evaluateEL("#{bindings.ContractRequisitionHeaderVO1Iterator.currentRow}");
            //            row.setAttribute("ReqDateNp", date);
        } catch (ParseException e) {
        }
        return requisitionDateBinding;
    }

    public void setPreparedByBinding(RichInputComboboxListOfValues preparedByBinding) {
        this.preparedByBinding = preparedByBinding;
    }

    public RichInputComboboxListOfValues getPreparedByBinding() {
        return preparedByBinding;
    }

    public void setCheckedByBinding(RichInputComboboxListOfValues checkedByBinding) {
        this.checkedByBinding = checkedByBinding;
    }

    public RichInputComboboxListOfValues getCheckedByBinding() {
        return checkedByBinding;
    }

    public void setCheckedByDateBinding(RichInputDate checkedByDateBinding) {
        this.checkedByDateBinding = checkedByDateBinding;
    }

    public RichInputDate getCheckedByDateBinding() {
        return checkedByDateBinding;
    }

    public void setVerifiedByBinding(RichInputComboboxListOfValues verifiedByBinding) {
        this.verifiedByBinding = verifiedByBinding;
    }

    public RichInputComboboxListOfValues getVerifiedByBinding() {
        return verifiedByBinding;
    }

    public void setVerifiedByDateBinding(RichInputDate verifiedByDateBinding) {
        this.verifiedByDateBinding = verifiedByDateBinding;
    }

    public RichInputDate getVerifiedByDateBinding() {
        return verifiedByDateBinding;
    }

    public void setApprovedByBinding(RichInputComboboxListOfValues approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputComboboxListOfValues getApprovedByBinding() {
        return approvedByBinding;
    }

    public void setApprovedByDateBinding(RichInputDate approvedByDateBinding) {
        this.approvedByDateBinding = approvedByDateBinding;
    }

    public RichInputDate getApprovedByDateBinding() {
        return approvedByDateBinding;
    }

    public void setContractValueApproxBinding(RichInputText contractValueApproxBinding) {
        this.contractValueApproxBinding = contractValueApproxBinding;
    }

    public RichInputText getContractValueApproxBinding() {
        return contractValueApproxBinding;
    }

    public void setPeriodOfContractFromBinding(RichInputDate periodOfContractFromBinding) {
        this.periodOfContractFromBinding = periodOfContractFromBinding;
    }

    public RichInputDate getPeriodOfContractFromBinding() {
        return periodOfContractFromBinding;
    }

    public void editButtonAL(ActionEvent actionEvent) {
        if (statusBinding.getValue() != null && statusBinding.getValue().toString().equalsIgnoreCase("N")) {
            System.out.println("Status of Requisition====>" + statusBinding.getValue());
            ADFUtils.showMessage("Cancelled Requisition cannot be edited further.", 2);
            ADFUtils.setEL("#{pageFlowScope.mode}", "V");
        } else {
            if (approvedByBinding.getValue().toString().isEmpty()) {
                cevmodecheck();
                if (checkedByBinding.getValue().toString().isEmpty()) {
                    verifiedByBinding.setDisabled(true);
                    approvedByBinding.setDisabled(true);
                } else if (verifiedByBinding.getValue().toString().isEmpty()) {
                    checkedByBinding.setDisabled(true);
                    approvedByBinding.setDisabled(true);
                } else if (approvedByBinding.getValue().toString().isEmpty()) {
                    checkedByBinding.setDisabled(true);
                    verifiedByBinding.setDisabled(true);
                }
                //        else if(!approvedByBinding.getValue().toString().isEmpty())
                //        {
                //            checkedByBinding.setDisabled(true);
                //            verifiedByBinding.setDisabled(true);
                //            approvedByBinding.setDisabled(true);
                //        }
            }
            //        else
            //        {
            ////            ADFUtils.showMessage("Approved Contract Requisition cannot be edited further.", 2);
            ////            ADFUtils.setEL("#{pageFlowScope.mode}", "V");
            //            RichPopup.PopupHints hints = new RichPopup.PopupHints();
            //            amendmentPoupConfirmationBinding.show(hints);
            //            headerEditBinding.setDisabled(true);
            //        }
        }
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setDetailCreateBinding(RichButton detailCreateBinding) {
        this.detailCreateBinding = detailCreateBinding;
    }

    public RichButton getDetailCreateBinding() {
        return detailCreateBinding;
    }

    //    public void setDetailDeleteBinding(RichButton detailDeleteBinding) {
    //        this.detailDeleteBinding = detailDeleteBinding;
    //    }
    //
    //    public RichButton getDetailDeleteBinding() {
    //        return detailDeleteBinding;
    //    }
    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }

    public void cevModeDisableComponent(String mode) {
        //            FacesContext fctx = FacesContext.getCurrentInstance();
        //            ELContext elctx = fctx.getELContext();
        //            Application jsfApp = fctx.getApplication();
        //            //create a ValueExpression that points to the ADF binding layer
        //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
        //            //
        //            ValueExpression valueExpr = exprFactory.createValueExpression(
        //                                         elctx,
        //                                         "#{pageFlowScope.mode=='E'}",
        //                                          Object.class
        //                                         );
        //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
        if (mode.equals("E")) {

            getHeaderEditBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
            getRequisitionNoBinding().setDisabled(true);
            getRequisitionDateBinding().setDisabled(true);
            getCheckedByDateBinding().setDisabled(true);
            getVerifiedByDateBinding().setDisabled(true);
            getApprovedByDateBinding().setDisabled(true);
            getUnitNameBinding().setDisabled(true);
            getPreparedByBinding().setDisabled(true);
            getPreparedByNameBinding().setDisabled(true);
            getApprovedByNameBinding().setDisabled(true);
            getCheckedByNameBinding().setDisabled(true);
            getVerifiedByNameBinding().setDisabled(true);
            getLocationDescriptionBinding().setDisabled(true);
            getPreferredContractNameBinding().setDisabled(true);
            // getJobDetailBinding().setDisabled(true);
            //            getDetailDeleteBinding().setDisabled(false);
            if (approvedByBinding.getValue() != null && !approvedByBinding.getValue().toString().isEmpty()) {
                getLocationCodeBinding().setDisabled(true);
            }
            //            getContractValueApproxBinding().setDisabled(true);
            getAmendNoBinding().setDisabled(true);
            getRequisitionNoCopyBinding().setDisabled(true);
            getAmdNoCopyBinding().setDisabled(true);
            getCopyButtonBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            contractorBinding.setDisabled(true);
            requestTypeBinding.setDisabled(true);
            quotNoBinding.setDisabled(true);
            copyButtonBinding.setDisabled(true);
            copyQuotBinding.setDisabled(true);
            if (approvedByBinding.getValue() != null && !approvedByBinding.getValue().toString().isEmpty()) {
                deptParentBinding.setDisabled(true);
                statusBinding.setDisabled(true);
            }
            seqNoBinding.setDisabled(true);
            docDateBinding.setDisabled(true);
            docTypeBinding.setDisabled(true);
            docNoBinding.setDisabled(true);
            docFileNameBinding.setDisabled(true);
            unitCdBinding.setDisabled(true);
            reqDateNpBinding.setDisabled(true);
        }

        else if (mode.equals("C"))

        {
            getPreparedByNameBinding().setDisabled(true);
            getPreparedByBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            getRequisitionNoBinding().setDisabled(true);
            getRequisitionDateBinding().setDisabled(true);
            getCheckedByBinding().setDisabled(true);
            getCheckedByDateBinding().setDisabled(true);
            getVerifiedByBinding().setDisabled(true);
            getVerifiedByDateBinding().setDisabled(true);
            getApprovedByBinding().setDisabled(true);
            getApprovedByDateBinding().setDisabled(true);
            //            getContractValueApproxBinding().setDisabled(true);
            //            getPeriodOfContractFromBinding().setDisabled(true);
            //  getJobDetailBinding().setDisabled(true);
            getPreparedByBinding().setDisabled(true);
            getAmendNoBinding().setDisabled(true);
            getAmdNoCopyBinding().setDisabled(true);
            quotNoBinding.setDisabled(true);
            copyQuotBinding.setDisabled(true);
            seqNoBinding.setDisabled(true);
            docDateBinding.setDisabled(true);
            docTypeBinding.setDisabled(true);
            docNoBinding.setDisabled(true);
            docFileNameBinding.setDisabled(true);
            unitCdBinding.setDisabled(true);
            statusBinding.setDisabled(true);
            reqDateNpBinding.setDisabled(true);


        } else if (mode.equals("V")) {
            copyButtonBinding.setDisabled(true);
            copyQuotBinding.setDisabled(true);
            //                  getDetailDeleteBinding().setDisabled(true);
            termsConditionButtonBinding.setDisabled(false);
            paymentTermButtonBinding.setDisabled(false);
            reqDtlTabBinding.setDisabled(false);
            referenceDocTabBinding.setDisabled(false);
            downloadLinkBinding.setDisabled(false);
        }
    }


    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setOutputTextBinding(RichOutputText outputTextBinding) {
        this.outputTextBinding = outputTextBinding;
    }

    public RichOutputText getOutputTextBinding() {
        cevmodecheck();
        return outputTextBinding;
    }

    public void setUnitNameBinding(RichInputText unitNameBinding) {
        this.unitNameBinding = unitNameBinding;
    }

    public RichInputText getUnitNameBinding() {
        return unitNameBinding;
    }

    public void setPreparedByNameBinding(RichInputText preparedByNameBinding) {
        this.preparedByNameBinding = preparedByNameBinding;
    }

    public RichInputText getPreparedByNameBinding() {
        return preparedByNameBinding;
    }

    public void setCheckedByNameBinding(RichInputText checkedByNameBinding) {
        this.checkedByNameBinding = checkedByNameBinding;
    }

    public RichInputText getCheckedByNameBinding() {
        return checkedByNameBinding;
    }

    public void setVerifiedByNameBinding(RichInputText verifiedByNameBinding) {
        this.verifiedByNameBinding = verifiedByNameBinding;
    }

    public RichInputText getVerifiedByNameBinding() {
        return verifiedByNameBinding;
    }

    public void setApprovedByNameBinding(RichInputText approvedByNameBinding) {
        this.approvedByNameBinding = approvedByNameBinding;
    }

    public RichInputText getApprovedByNameBinding() {
        return approvedByNameBinding;
    }

    public void setLocationDescriptionBinding(RichInputText locationDescriptionBinding) {
        this.locationDescriptionBinding = locationDescriptionBinding;
    }

    public RichInputText getLocationDescriptionBinding() {
        return locationDescriptionBinding;
    }

    public void setPreferredContractNameBinding(RichInputText preferredContractNameBinding) {
        this.preferredContractNameBinding = preferredContractNameBinding;
    }

    public RichInputText getPreferredContractNameBinding() {
        return preferredContractNameBinding;
    }

    public void setProductDescriptionBinding(RichInputText productDescriptionBinding) {
        this.productDescriptionBinding = productDescriptionBinding;
    }

    public RichInputText getProductDescriptionBinding() {
        return productDescriptionBinding;
    }

    public void setJobDetailBinding(RichInputText jobDetailBinding) {
        this.jobDetailBinding = jobDetailBinding;
    }

    public RichInputText getJobDetailBinding() {
        return jobDetailBinding;
    }

    public void setWorkQtyBinding(RichInputText workQtyBinding) {
        this.workQtyBinding = workQtyBinding;
    }

    public RichInputText getWorkQtyBinding() {
        return workQtyBinding;
    }

    public void setRateBinding(RichInputText rateBinding) {
        this.rateBinding = rateBinding;
    }

    public RichInputText getRateBinding() {
        return rateBinding;
    }

    public void workQtyVCE(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (valueChangeEvent.getNewValue() != null && rateBinding.getValue() != null) {

            BigDecimal Rate = (BigDecimal) rateBinding.getValue();
            BigDecimal WorkQty = (BigDecimal) valueChangeEvent.getNewValue();
            getContractValueApproxBinding().setValue(WorkQty.multiply(Rate));

        }
        if (periodOfContractToBinding.getValue() == null) {
            Calendar cal = Calendar.getInstance();
            Date dt = cal.getTime();
            int i = 30;
            cal.setTimeInMillis(dt.getTime());
            cal.add(Calendar.DAY_OF_MONTH, i);
            oracle.jbo.domain.Timestamp vaidTo = new oracle.jbo.domain.Timestamp(cal.getTime().getTime());
            periodOfContractToBinding.setValue(vaidTo);
        }
    }

    public void setLocationCodeBinding(RichInputComboboxListOfValues locationCodeBinding) {
        this.locationCodeBinding = locationCodeBinding;
    }

    public RichInputComboboxListOfValues getLocationCodeBinding() {
        return locationCodeBinding;
    }

    public void detailCreateButtonAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("copyDataInNewRow").execute();
        System.out.println("REQ TYPE " + requestTypeBinding.getValue());
        AdfFacesContext.getCurrentInstance().addPartialTarget(requestTypeBinding);
        AdfFacesContext.getCurrentInstance().addPartialTarget(quotNoBinding);
        //       ADFUtils.findOperation("CreateInsert").execute();
        AdfFacesContext.getCurrentInstance().addPartialTarget(createContractRequisitionTableBinding);
        if (!locationCodeBinding.getValue().toString().isEmpty()) {
            getLocationCodeBinding().setDisabled(true);
        }
    }

    public void prodCodeVCE(ValueChangeEvent valueChangeEvent) {
        ADFUtils.findOperation("clearRowOnItemVceConReq").execute();
        AdfFacesContext.getCurrentInstance().addPartialTarget(createContractRequisitionTableBinding);

    }

    public void approvedByVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce.getNewValue() != null) {
            if (typeBinding.getValue().equals("R")) {
                System.out.println("IN VCE Approved");
                OperationBinding ob = ADFUtils.findOperation("checkApprovalFunctionStatus");
                ob.getParamsMap().put("formNm", "CR");
                ob.getParamsMap().put("authoLim", "AP");
                ob.getParamsMap().put("UnitCd", unitCodeBinding.getValue());
                System.out.println("Total SUM========>" +
                                   ADFUtils.resolveExpression("#{bindings.ValueTrans.inputValue}"));
                ob.getParamsMap().put("Amount", ADFUtils.resolveExpression("#{bindings.ValueTrans.inputValue}"));
                ob.getParamsMap().put("empcd", vce.getNewValue().toString());
                ob.execute();
                System.out.println(" EMP CD" + vce.getNewValue().toString());
                if ((ob.getResult() != null && !ob.getResult().equals("Y"))) {
                    Row row = (Row) ADFUtils.evaluateEL("#{bindings.ContractRequisitionHeaderVO1Iterator.currentRow}");
                    row.setAttribute("ApprovedBy", null);
                    row.setAttribute("ApprovedDt", null);
                    ADFUtils.showMessage(ob.getResult().toString(), 0);
                }
            } else {

                OperationBinding ob1 = ADFUtils.findOperation("checkApprovalFunctionStatus");
                ob1.getParamsMap().put("formNm", "CRC");
                ob1.getParamsMap().put("authoLim", "AP");
                ob1.getParamsMap().put("UnitCd", unitCodeBinding.getValue());
                System.out.println("Total SUM========>" +
                                   ADFUtils.resolveExpression("#{bindings.ValueTrans.inputValue}"));
                ob1.getParamsMap().put("Amount", ADFUtils.resolveExpression("#{bindings.ValueTrans.inputValue}"));
                ob1.getParamsMap().put("empcd", vce.getNewValue().toString());
                ob1.execute();
                System.out.println(" EMP CD" + vce.getNewValue().toString());
                if ((ob1.getResult() != null && !ob1.getResult().equals("Y"))) {
                    Row row = (Row) ADFUtils.evaluateEL("#{bindings.ContractRequisitionHeaderVO1Iterator.currentRow}");
                    row.setAttribute("ApprovedBy", null);
                    row.setAttribute("ApprovedDt", null);
                    ADFUtils.showMessage(ob1.getResult().toString(), 0);
                }
            }
        }

    }

    public void setAmendmentPoupConfirmationBinding(RichPopup amendmentPoupConfirmationBinding) {
        this.amendmentPoupConfirmationBinding = amendmentPoupConfirmationBinding;
    }

    public RichPopup getAmendmentPoupConfirmationBinding() {
        return amendmentPoupConfirmationBinding;
    }

    public void amendPopUpConfirmationDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            System.out.println("in ok event");
            OperationBinding op = ADFUtils.findOperation("getAmdNoRequisitionDetails");
            op.getParamsMap().put("ReqId", reqIdBinding.getValue());
            op.execute();
            cevmodecheck();
            statusBinding.setDisabled(true);
            checkedByBinding.setDisabled(true);
            verifiedByBinding.setDisabled(true);
            getDetailCreateBinding().setDisabled(false);
        }
        if (dialogEvent.getOutcome().name().equals("cancel")) {
            amendmentPoupConfirmationBinding.hide();
            ADFUtils.setEL("#{pageFlowScope.mode}", "V");
        }
    }

    public void copyRequisitionNoAL(ActionEvent actionEvent) {
        if (requisitionNoCopyBinding.getValue() != null) {
            ADFUtils.findOperation("copyRequisitionDetail").execute();
            //        if(periodOfContractToBinding.getValue()==null)
            //                    {
            //                    Calendar cal=Calendar.getInstance();
            //                    Date dt=cal.getTime();
            //                    int i=30;
            //                    cal.setTimeInMillis(dt.getTime());
            //                    cal.add(Calendar.DAY_OF_MONTH,i);
            //                    oracle.jbo.domain.Timestamp vaidTo=new oracle.jbo.domain.Timestamp(cal.getTime().getTime());
            //                    periodOfContractToBinding.setValue(vaidTo);
            //                    }
        }
    }

    public void setAmendNoBinding(RichInputText amendNoBinding) {
        this.amendNoBinding = amendNoBinding;
    }

    public RichInputText getAmendNoBinding() {
        return amendNoBinding;
    }

    public void setRequisitionNoCopyBinding(RichInputComboboxListOfValues requisitionNoCopyBinding) {
        this.requisitionNoCopyBinding = requisitionNoCopyBinding;
    }

    public RichInputComboboxListOfValues getRequisitionNoCopyBinding() {
        return requisitionNoCopyBinding;
    }

    public void setAmdNoCopyBinding(RichInputText amdNoCopyBinding) {
        this.amdNoCopyBinding = amdNoCopyBinding;
    }

    public RichInputText getAmdNoCopyBinding() {
        return amdNoCopyBinding;
    }

    public void setCopyButtonBinding(RichButton copyButtonBinding) {
        this.copyButtonBinding = copyButtonBinding;
    }

    public RichButton getCopyButtonBinding() {
        return copyButtonBinding;
    }

    public void lumsumVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce.getNewValue() != vce.getOldValue()) {
            workQtyBinding.setValue(null);
            rateBinding.setValue(null);
            contractValueApproxBinding.setValue(null);
        }
    }

    public void rateVCE(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (valueChangeEvent.getNewValue() != null && workQtyBinding.getValue() != null) {

            BigDecimal Rate = (BigDecimal) valueChangeEvent.getNewValue();
            BigDecimal WorkQty = (BigDecimal) workQtyBinding.getValue();
            getContractValueApproxBinding().setValue(WorkQty.multiply(Rate));
        }
    }

    public void copyQuotationAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("requisitionQuotationNoPopulate").execute();
        if (periodOfContractToBinding.getValue() == null) {
            Calendar cal = Calendar.getInstance();
            Date dt = cal.getTime();
            int i = 30;
            cal.setTimeInMillis(dt.getTime());
            cal.add(Calendar.DAY_OF_MONTH, i);
            oracle.jbo.domain.Timestamp vaidTo = new oracle.jbo.domain.Timestamp(cal.getTime().getTime());
            periodOfContractToBinding.setValue(vaidTo);
        }
        quotNoBinding.setDisabled(true);
        prodCdBinding.setDisabled(true);
        contractorBinding.setDisabled(true);
        detailCreateBinding.setDisabled(true);

    }

    public void setQuotNoBinding(RichInputComboboxListOfValues quotNoBinding) {
        this.quotNoBinding = quotNoBinding;
    }

    public RichInputComboboxListOfValues getQuotNoBinding() {
        return quotNoBinding;
    }

    public void setRequestTypeBinding(RichSelectOneChoice requestTypeBinding) {
        this.requestTypeBinding = requestTypeBinding;
    }

    public RichSelectOneChoice getRequestTypeBinding() {
        return requestTypeBinding;
    }

    public void setProdCdBinding(RichInputComboboxListOfValues prodCdBinding) {
        this.prodCdBinding = prodCdBinding;
    }

    public RichInputComboboxListOfValues getProdCdBinding() {
        return prodCdBinding;
    }

    public void requisitionTypeVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce.getNewValue() != null && vce.getNewValue().toString().equals("D")) {
            quotNoBinding.setValue(null);
            quotNoBinding.setDisabled(true);
            copyQuotBinding.setDisabled(true);
            requisitionNoCopyBinding.setDisabled(false);
            copyButtonBinding.setDisabled(false);
        } else if (vce.getNewValue() != null && vce.getNewValue().toString().equals("Q")) {
            quotNoBinding.setDisabled(false);
            copyQuotBinding.setDisabled(false);
            requisitionNoCopyBinding.setDisabled(true);
            copyButtonBinding.setDisabled(true);
        }
    }

    public void setCopyQuotBinding(RichButton copyQuotBinding) {
        this.copyQuotBinding = copyQuotBinding;
    }

    public RichButton getCopyQuotBinding() {
        return copyQuotBinding;
    }

    public void setContractorBinding(RichInputComboboxListOfValues contractorBinding) {
        this.contractorBinding = contractorBinding;
    }

    public RichInputComboboxListOfValues getContractorBinding() {
        return contractorBinding;
    }

    public void termsPopUpDtlDeleteDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete1").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
            //                        ADFUtils.findOperation("updateReqTermsConditionSerialNo").execute();
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(termsConditionTableBinding);

    }

    public void setTermsConditionTableBinding(RichTable termsConditionTableBinding) {
        this.termsConditionTableBinding = termsConditionTableBinding;
    }

    public RichTable getTermsConditionTableBinding() {
        return termsConditionTableBinding;
    }

    public void setTermsConditionButtonBinding(RichButton termsConditionButtonBinding) {
        this.termsConditionButtonBinding = termsConditionButtonBinding;
    }

    public RichButton getTermsConditionButtonBinding() {
        return termsConditionButtonBinding;
    }

    public void setPeriodOfContractToBinding(RichInputDate periodOfContractToBinding) {
        this.periodOfContractToBinding = periodOfContractToBinding;
    }

    public RichInputDate getPeriodOfContractToBinding() {
        return periodOfContractToBinding;
    }

    public void contractValueVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce.getNewValue() != null) {

            if (periodOfContractToBinding.getValue() == null) {
                Calendar cal = Calendar.getInstance();
                Date dt = cal.getTime();
                int i = 30;
                cal.setTimeInMillis(dt.getTime());
                cal.add(Calendar.DAY_OF_MONTH, i);
                oracle.jbo.domain.Timestamp vaidTo = new oracle.jbo.domain.Timestamp(cal.getTime().getTime());
                periodOfContractToBinding.setValue(vaidTo);
            }
            AdfFacesContext.getCurrentInstance().addPartialTarget(createContractRequisitionTableBinding);
        }
    }

    public void contractFromValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (object != null && periodOfContractToBinding.getValue() != null) {
            oracle.jbo.domain.Timestamp from = (oracle.jbo.domain.Timestamp) object;
            String str_from = from.toString();
            System.out.println("FROMMMM" + str_from);
            String str_to = periodOfContractToBinding.getValue().toString();
            System.out.println("TOOOO" + str_to);
            oracle.jbo.domain.Date dt_frm = new oracle.jbo.domain.Date(str_from.substring(0, 10));
            oracle.jbo.domain.Date dt_to = new oracle.jbo.domain.Date(str_to.substring(0, 10));
            if (dt_frm.compareTo(dt_to) == 1) {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                              "Period of Contract From cannot be greater than Period of Contract To.",
                                                              null));
            }
        }

    }

    public void checkedByVCE(ValueChangeEvent vce) {
        //        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        //                     OperationBinding ob = ADFUtils.findOperation("checkApprovalStatus");
        //                    ob.getParamsMap().put("formNm", "CR");
        //                    ob.getParamsMap().put("authoLim", "CH");
        //                    ob.getParamsMap().put("empcd", vce.getNewValue().toString());
        //                    ob.execute();
        //                    System.out.println(" EMP CD"+vce.getNewValue().toString());
        //                    if((ob.getResult() !=null && ob.getResult().equals("Y")))
        //                    {
        //                        Row row=(Row)ADFUtils.evaluateEL("#{bindings.ContractRequisitionHeaderVO1Iterator.currentRow}");
        //                        row.setAttribute("CheckedBy", null);
        //                        ADFUtils.showMessage("You have no authority to check this Contract Requisition.",0);
        //                    }

        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce.getNewValue() != null) {
            if (typeBinding.getValue().equals("R")) {
                System.out.println("IN VCE Approved");
                OperationBinding ob = ADFUtils.findOperation("checkApprovalFunctionStatus");
                ob.getParamsMap().put("formNm", "CR");
                ob.getParamsMap().put("authoLim", "CH");
                ob.getParamsMap().put("UnitCd", unitCodeBinding.getValue());
                System.out.println("Total SUM========>" +
                                   ADFUtils.resolveExpression("#{bindings.ValueTrans.inputValue}"));
                ob.getParamsMap().put("Amount", ADFUtils.resolveExpression("#{bindings.ValueTrans.inputValue}"));
                ob.getParamsMap().put("empcd", vce.getNewValue().toString());
                ob.execute();
                System.out.println(" EMP CD" + vce.getNewValue().toString());
                if ((ob.getResult() != null && !ob.getResult().equals("Y"))) {
                    Row row = (Row) ADFUtils.evaluateEL("#{bindings.ContractRequisitionHeaderVO1Iterator.currentRow}");
                    row.setAttribute("CheckedBy", null);
                    row.setAttribute("CheckedDt", null);
                    ADFUtils.showMessage(ob.getResult().toString(), 0);
                }
            } else {

                OperationBinding ob1 = ADFUtils.findOperation("checkApprovalFunctionStatus");
                ob1.getParamsMap().put("formNm", "CRC");
                ob1.getParamsMap().put("authoLim", "CH");
                ob1.getParamsMap().put("UnitCd", unitCodeBinding.getValue());
                System.out.println("Total SUM========>" +
                                   ADFUtils.resolveExpression("#{bindings.ValueTrans.inputValue}"));
                ob1.getParamsMap().put("Amount", ADFUtils.resolveExpression("#{bindings.ValueTrans.inputValue}"));
                ob1.getParamsMap().put("empcd", vce.getNewValue().toString());
                ob1.execute();
                System.out.println(" EMP CD" + vce.getNewValue().toString());
                if ((ob1.getResult() != null && !ob1.getResult().equals("Y"))) {
                    Row row = (Row) ADFUtils.evaluateEL("#{bindings.ContractRequisitionHeaderVO1Iterator.currentRow}");
                    row.setAttribute("CheckedBy", null);
                    row.setAttribute("CheckedDt", null);
                    ADFUtils.showMessage(ob1.getResult().toString(), 0);
                }
            }
        }
    }

    public void verifyByVCL(ValueChangeEvent vce) {
        //        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        //                     OperationBinding ob = ADFUtils.findOperation("checkApprovalStatus");
        //                    ob.getParamsMap().put("formNm", "CR");
        //                    ob.getParamsMap().put("authoLim", "VR");
        //                    ob.getParamsMap().put("empcd", vce.getNewValue().toString());
        //                    ob.execute();
        //                    System.out.println(" EMP CD"+vce.getNewValue().toString());
        //                    if((ob.getResult() !=null && ob.getResult().equals("Y")))
        //                    {
        //                        Row row=(Row)ADFUtils.evaluateEL("#{bindings.ContractRequisitionHeaderVO1Iterator.currentRow}");
        //                        row.setAttribute("VerifiedBy", null);
        //                        ADFUtils.showMessage("You have no authority to verify this Contract Requisition.",0);
        //                    }
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce.getNewValue() != null) {
            if (typeBinding.getValue().equals("R")) {
                System.out.println("IN VCE Approved");
                OperationBinding ob = ADFUtils.findOperation("checkApprovalFunctionStatus");
                ob.getParamsMap().put("formNm", "CR");
                ob.getParamsMap().put("authoLim", "VR");
                ob.getParamsMap().put("UnitCd", unitCodeBinding.getValue());
                System.out.println("Total SUM========>" +
                                   ADFUtils.resolveExpression("#{bindings.ValueTrans.inputValue}"));
                ob.getParamsMap().put("Amount", ADFUtils.resolveExpression("#{bindings.ValueTrans.inputValue}"));
                ob.getParamsMap().put("empcd", vce.getNewValue().toString());
                ob.execute();
                System.out.println(" EMP CD" + vce.getNewValue().toString());
                if ((ob.getResult() != null && !ob.getResult().equals("Y"))) {
                    Row row = (Row) ADFUtils.evaluateEL("#{bindings.ContractRequisitionHeaderVO1Iterator.currentRow}");
                    row.setAttribute("VerifiedBy", null);
                    row.setAttribute("VerifiedDt", null);
                    ADFUtils.showMessage(ob.getResult().toString(), 0);
                }
            } else {

                OperationBinding ob1 = ADFUtils.findOperation("checkApprovalFunctionStatus");
                ob1.getParamsMap().put("formNm", "CRC");
                ob1.getParamsMap().put("authoLim", "VR");
                ob1.getParamsMap().put("UnitCd", unitCodeBinding.getValue());
                System.out.println("Total SUM========>" +
                                   ADFUtils.resolveExpression("#{bindings.ValueTrans.inputValue}"));
                ob1.getParamsMap().put("Amount", ADFUtils.resolveExpression("#{bindings.ValueTrans.inputValue}"));
                ob1.getParamsMap().put("empcd", vce.getNewValue().toString());
                ob1.execute();
                System.out.println(" EMP CD" + vce.getNewValue().toString());
                if ((ob1.getResult() != null && !ob1.getResult().equals("Y"))) {
                    Row row = (Row) ADFUtils.evaluateEL("#{bindings.ContractRequisitionHeaderVO1Iterator.currentRow}");
                    row.setAttribute("VerifiedBy", null);
                    row.setAttribute("VerifiedDt", null);
                    ADFUtils.showMessage(ob1.getResult().toString(), 0);
                }
            }
        }
    }

    public void createTerms(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert1").execute();
        Row row = (Row) ADFUtils.evaluateEL("#{bindings.ContractRequisitionTermDtlVO1Iterator.currentRow}");
        row.setAttribute("SrNo",
                         ADFUtils.evaluateEL("#{bindings.ContractRequisitionTermDtlVO1Iterator.estimatedRowCount}"));
    }

    public void jobCodeVCL(ValueChangeEvent valueChangeEvent) {
        AdfFacesContext.getCurrentInstance().addPartialTarget(createContractRequisitionTableBinding);

    }

    public void setDeptParentBinding(RichInputComboboxListOfValues deptParentBinding) {
        this.deptParentBinding = deptParentBinding;
    }

    public RichInputComboboxListOfValues getDeptParentBinding() {
        return deptParentBinding;
    }

    public void termsConditionPopulateAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("ContractRequsitionTermsDataPopulate").execute();
    }

    public void setContractMultiPaymentTermTableBinding(RichTable contractMultiPaymentTermTableBinding) {
        this.contractMultiPaymentTermTableBinding = contractMultiPaymentTermTableBinding;
    }

    public RichTable getContractMultiPaymentTermTableBinding() {
        return contractMultiPaymentTermTableBinding;
    }

    public void paymentTermDeleteDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete2").execute();
            System.out.println("Record Delete Successfully");
            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(contractMultiPaymentTermTableBinding);
    }

    public void percentageVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce.getNewValue() != null) {
            OperationBinding op = ADFUtils.findOperation("percentageValidation");
            op.execute();
            System.out.println("Result" + op.getResult());
            if (op.getResult().equals("Y")) {
                ADFUtils.showMessage("Payment Percentage must be equals to 100.", 0);
            }
        }
    }

    public void percentValidatorDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            OperationBinding op = ADFUtils.findOperation("percentageValidation");
            op.execute();
            System.out.println("Result" + op.getResult());
            if (op.getResult().equals("Y")) {
                ADFUtils.showMessage("Payment Percentage must be equals to 100.", 0);
            }
        }
    }

    public void setPaymentTermButtonBinding(RichButton paymentTermButtonBinding) {
        this.paymentTermButtonBinding = paymentTermButtonBinding;
    }

    public RichButton getPaymentTermButtonBinding() {
        return paymentTermButtonBinding;
    }

    public String saveRequisitionNo() {
        String empcd = (String) ADFUtils.resolveExpression("#{pageFlowScope.empCode}");
        System.out.println("empcd" + empcd);
        ADFUtils.setColumnValue("ContractRequisitionHeaderVO1Iterator", "LastUpdatedBy", empcd);

        if (!requestTypeBinding.isDisabled()) {
            OperationBinding op = ADFUtils.findOperation("generateContractRequisitionNo");
            op.execute();
            ADFUtils.findOperation("Commit").execute();
            FacesMessage Message =
                new FacesMessage("Record Saved Successfully. New Contract Requisition Number  is " +
                                 requisitionNoBinding.getValue() + ".");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
            return "SaveAndClose";
        } else {
            if (requisitionNoBinding.getValue() != null) {
                DCIteratorBinding dci = ADFUtils.findIterator("DocAttachRefDtlVO10Iterator");
                RowSetIterator rsi = dci.getViewObject().createRowSetIterator(null);
                while (rsi.hasNext()) {
                    System.out.println("LUMSUMMMMM");
                    Row r = rsi.next();
                    String ReqNo = (String) requisitionNoBinding.getValue();
                    if (ReqNo != null) {
                        r.setAttribute("RefDocNo", ReqNo);
                    }
                }
                rsi.closeRowSetIterator();
            }
            ADFUtils.findOperation("Commit").execute();
            FacesMessage Message = new FacesMessage("Record Update Successfully.");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
            return "SaveAndClose";

        }
    }

    public void paymentTermVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        String ReqNo = (String) requisitionNoBinding.getValue();
        if (vce.getNewValue() != null) {
            System.out.println("IN VCE");
            Row row = (Row) ADFUtils.evaluateEL("#{bindings.ContractRequisitionMultiPayTermVO1.currentRow}");
            row.setAttribute("CrNo", ReqNo);
            row.setAttribute("AmdNo", amendNoBinding.getValue());
        }
    }

    public void uploadAction(ValueChangeEvent vce) {
        //Get File Object from VC Event
        UploadedFile fileVal = (UploadedFile) vce.getNewValue();
        System.out.println("File Value==============<>" + fileVal);
        //Upload File to path- Return actual server path
        String path = ADFUtils.uploadFile(fileVal);
        System.out.println(fileVal.getContentType());
        //Method to insert data in table to keep track of uploaded files
        OperationBinding ob = ADFUtils.findOperation("setFileDataCR");
        ob.getParamsMap().put("name", fileVal.getFilename());
        ob.getParamsMap().put("path", path);
        ob.getParamsMap().put("contTyp", fileVal.getContentType());
        ob.getParamsMap().put("SrNo", ADFUtils.evaluateEL("#{bindings.DocAttachRefDtlVO10.estimatedRowCount+1}"));
        ob.execute();
        System.out.println("IN VCE EXECUTED");
        // Reset inputFile component after upload
        ResetUtils.reset(vce.getComponent());
    }

    public void downloadAction(FacesContext facesContext, OutputStream outputStream) throws Exception {
        File filed = new File(pathBinding.getValue().toString());
        FileInputStream fis;
        byte[] b;
        try {
            fis = new FileInputStream(filed);

            int n;
            while ((n = fis.available()) > 0) {
                b = new byte[n];
                int result = fis.read(b);
                outputStream.write(b, 0, b.length);
                if (result == -1)
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        outputStream.flush();

    }

    public void deleteDocDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete3").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(docRefTableBinding);
    }

    public void setPathBinding(RichInputText pathBinding) {
        this.pathBinding = pathBinding;
    }

    public RichInputText getPathBinding() {
        return pathBinding;
    }

    public void setContentTypeBinding(RichInputText contentTypeBinding) {
        this.contentTypeBinding = contentTypeBinding;
    }

    public RichInputText getContentTypeBinding() {
        return contentTypeBinding;
    }

    public void setDocFileNameBinding(RichInputText docFileNameBinding) {
        this.docFileNameBinding = docFileNameBinding;
    }

    public RichInputText getDocFileNameBinding() {
        return docFileNameBinding;
    }

    public void setDocTypeBinding(RichInputText docTypeBinding) {
        this.docTypeBinding = docTypeBinding;
    }

    public RichInputText getDocTypeBinding() {
        return docTypeBinding;
    }

    public void setDocNoBinding(RichInputText docNoBinding) {
        this.docNoBinding = docNoBinding;
    }

    public RichInputText getDocNoBinding() {
        return docNoBinding;
    }

    public void setDocDateBinding(RichInputDate docDateBinding) {
        this.docDateBinding = docDateBinding;
    }

    public RichInputDate getDocDateBinding() {
        return docDateBinding;
    }

    public void setUnitCdBinding(RichInputText unitCdBinding) {
        this.unitCdBinding = unitCdBinding;
    }

    public RichInputText getUnitCdBinding() {
        return unitCdBinding;
    }

    public void setSeqNoBinding(RichInputText seqNoBinding) {
        this.seqNoBinding = seqNoBinding;
    }

    public RichInputText getSeqNoBinding() {
        return seqNoBinding;
    }

    public void setDocRefTableBinding(RichTable docRefTableBinding) {
        this.docRefTableBinding = docRefTableBinding;
    }

    public RichTable getDocRefTableBinding() {
        return docRefTableBinding;
    }

    public void setReferenceDocTabBinding(RichShowDetailItem referenceDocTabBinding) {
        this.referenceDocTabBinding = referenceDocTabBinding;
    }

    public RichShowDetailItem getReferenceDocTabBinding() {
        return referenceDocTabBinding;
    }

    public void setReqDtlTabBinding(RichShowDetailItem reqDtlTabBinding) {
        this.reqDtlTabBinding = reqDtlTabBinding;
    }

    public RichShowDetailItem getReqDtlTabBinding() {
        return reqDtlTabBinding;
    }

    public void setDownloadLinkBinding(RichCommandLink downloadLinkBinding) {
        this.downloadLinkBinding = downloadLinkBinding;
    }

    public RichCommandLink getDownloadLinkBinding() {
        return downloadLinkBinding;
    }

    public void setStatusBinding(RichSelectOneChoice statusBinding) {
        this.statusBinding = statusBinding;
    }

    public RichSelectOneChoice getStatusBinding() {
        return statusBinding;
    }

    public void setReqIdBinding(RichInputText reqIdBinding) {
        this.reqIdBinding = reqIdBinding;
    }

    public RichInputText getReqIdBinding() {
        return reqIdBinding;
    }

    public void setReqDateNpBinding(RichInputText reqDateNpBinding) {
        this.reqDateNpBinding = reqDateNpBinding;
    }

    public RichInputText getReqDateNpBinding() {
        return reqDateNpBinding;
    }
}


