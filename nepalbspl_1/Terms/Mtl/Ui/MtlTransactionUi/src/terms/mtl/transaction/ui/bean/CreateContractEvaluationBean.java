package terms.mtl.transaction.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.math.RoundingMode;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.application.ViewHandler;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneRadio;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichPanelCollection;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import terms.mtl.transaction.model.applicationModule.MtlTransactionAMImpl;

public class CreateContractEvaluationBean {
    private RichTable contractEvaluationDtlTableBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText contractEvaluationNoBinding;
    private RichInputText contractEvaluationDate;
    private RichInputComboboxListOfValues agencyCodeBinding;
    private RichInputText totalCountBinding;
    private RichInputText totalScoreBinding;
    private RichInputText performanceOfAgencyBinding;
    private RichButton createDetailBinding;
    private RichButton deleteDetailBinding;
    private RichInputText bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichSelectOneRadio satisfactionBinding;
    private RichInputText checkPointBinding;
    private RichSelectOneRadio notSatisfiedBindings;
    private RichInputText checkValueBinding;
    private RichPanelCollection panelCollectionBinding;
    private RichInputText totalCountTableBinding;
    private RichInputText totalScoreTableBinding;
    private RichInputText performanceTableBinding;

    public CreateContractEvaluationBean() {
    }

    public void deletePopUpdialogListener(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                    {
                    ADFUtils.findOperation("Delete").execute();
                    System.out.println("Record Delete Successfully");
                   
                        FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                        FacesContext fc = FacesContext.getCurrentInstance();  
                        fc.addMessage(null, Message);  
                   
                    }
                AdfFacesContext.getCurrentInstance().addPartialTarget(contractEvaluationDtlTableBinding);
    }

    public void setContractEvaluationDtlTableBinding(RichTable contractEvaluationDtlTableBinding) {
        this.contractEvaluationDtlTableBinding = contractEvaluationDtlTableBinding;
    }

    public RichTable getContractEvaluationDtlTableBinding() {
        return contractEvaluationDtlTableBinding;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setContractEvaluationNoBinding(RichInputText contractEvaluationNoBinding) {
        this.contractEvaluationNoBinding = contractEvaluationNoBinding;
    }

    public RichInputText getContractEvaluationNoBinding() {
        return contractEvaluationNoBinding;
    }

    public void setContractEvaluationDate(RichInputText contractEvaluationDate) {
        this.contractEvaluationDate = contractEvaluationDate;
    }

    public RichInputText getContractEvaluationDate() {
        return contractEvaluationDate;
    }

    public void setAgencyCodeBinding(RichInputComboboxListOfValues agencyCodeBinding) {
        this.agencyCodeBinding = agencyCodeBinding;
    }

    public RichInputComboboxListOfValues getAgencyCodeBinding() {
        return agencyCodeBinding;
    }

    public void setTotalCountBinding(RichInputText totalCountBinding) {
        this.totalCountBinding = totalCountBinding;
    }

    public RichInputText getTotalCountBinding() {
        return totalCountBinding;
    }

    public void setTotalScoreBinding(RichInputText totalScoreBinding) {
        this.totalScoreBinding = totalScoreBinding;
    }

    public RichInputText getTotalScoreBinding() {
        return totalScoreBinding;
    }

    public void setPerformanceOfAgencyBinding(RichInputText performanceOfAgencyBinding) {
        this.performanceOfAgencyBinding = performanceOfAgencyBinding;
    }

    public RichInputText getPerformanceOfAgencyBinding() {
        return performanceOfAgencyBinding;
    }

    public void setCreateDetailBinding(RichButton createDetailBinding) {
        this.createDetailBinding = createDetailBinding;
    }

    public RichButton getCreateDetailBinding() {
        return createDetailBinding;
    }

    public void setDeleteDetailBinding(RichButton deleteDetailBinding) {
        this.deleteDetailBinding = deleteDetailBinding;
    }

    public RichButton getDeleteDetailBinding() {
        return deleteDetailBinding;
    }

    public void setBindingOutputText(RichInputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichInputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }
    
    private void cevmodecheck(){
            if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
                cevModeDisableComponent("V");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("C");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("E");
            }
        }
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                try {
                    Method method1 =
                        component.getClass().getMethod("setDisabled", boolean.class);
                    if (method1 != null) {
                        method1.invoke(component, valueComponent);
                    }
                } catch (NoSuchMethodException e) {
                    try {
                        Method method =
                            component.getClass().getMethod("setReadOnly", boolean.class);
                        if (method != null) {
                            method.invoke(component, valueComponent);
                        }
                    } catch (Exception e1) {
                        // e.printStackTrace();//silently eat this exception.
                    }


                } catch (Exception e) {
                    // e.printStackTrace();//silently eat this exception.
                }
                List<UIComponent> childComponents = component.getChildren();
                for (UIComponent comp : childComponents) {
                    makeComponentHierarchyReadOnly(comp, valueComponent);
                }
            }
    
    //Set Fields and Button disable.
        public void cevModeDisableComponent(String mode) {
        //            FacesContext fctx = FacesContext.getCurrentInstance();
        //            ELContext elctx = fctx.getELContext();
        //            Application jsfApp = fctx.getApplication();
        //            //create a ValueExpression that points to the ADF binding layer
        //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
        //            //
        //            ValueExpression valueExpr = exprFactory.createValueExpression(
        //                                         elctx,
        //                                         "#{pageFlowScope.mode=='E'}",
        //                                          Object.class
        //                                         );
        //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
            if (mode.equals("E")) {
              headerEditBinding.setDisabled(true);
              unitCodeBinding.setDisabled(true);
              contractEvaluationNoBinding.setDisabled(true);
              contractEvaluationDate.setDisabled(true);
              agencyCodeBinding.setDisabled(true);
              totalCountBinding.setDisabled(true);
              totalScoreBinding.setDisabled(true);
              performanceOfAgencyBinding.setDisabled(true);
                checkPointBinding.setDisabled(false);
              
            } else if (mode.equals("C")) {
              headerEditBinding.setDisabled(true);
                unitCodeBinding.setDisabled(true);
                contractEvaluationNoBinding.setDisabled(true);
                contractEvaluationDate.setDisabled(true);
                agencyCodeBinding.setDisabled(false);
                totalCountBinding.setDisabled(true);
                totalScoreBinding.setDisabled(true);
                performanceOfAgencyBinding.setDisabled(true);
                checkPointBinding.setDisabled(false);

            } else if (mode.equals("V")) {
                headerEditBinding.setDisabled(true);
                createDetailBinding.setDisabled(true);
                deleteDetailBinding.setDisabled(true);

            }
            
        }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void saveDataAL(ActionEvent actionEvent) {
        if(!agencyCodeBinding.isDisabled() && contractEvaluationNoBinding.getValue()==null)
        {
            ADFUtils.findOperation("contractEvaluationNoGeneration").execute();
            ADFUtils.showMessage("Record saved successfully"+contractEvaluationNoBinding.getValue(), 2);
        }
        else
        {
            ADFUtils.showMessage("Record updated. successfully", 2);

        }
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setSatisfactionBinding(RichSelectOneRadio satisfactionBinding) {
        this.satisfactionBinding = satisfactionBinding;
    }

    public RichSelectOneRadio getSatisfactionBinding() {
        return satisfactionBinding;
    }


    public void setCheckPointBinding(RichInputText checkPointBinding) {
        this.checkPointBinding = checkPointBinding;
    }

    public RichInputText getCheckPointBinding() {
        return checkPointBinding;
    }

    public void setNotSatisfiedBindings(RichSelectOneRadio notSatisfiedBindings) {
        this.notSatisfiedBindings = notSatisfiedBindings;
    }

    public RichSelectOneRadio getNotSatisfiedBindings() {
        return notSatisfiedBindings;
    }

    public void satisfactionlevelBindingVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        System.out.println("NEW VALUE"+vce.getNewValue());
        AdfFacesContext.getCurrentInstance().addPartialTarget(totalScoreBinding);
        AdfFacesContext.getCurrentInstance().addPartialTarget(performanceOfAgencyBinding);
        AdfFacesContext.getCurrentInstance().addPartialTarget(checkPointBinding);
        if(vce.getNewValue()!=null && vce.getOldValue()==null)
        {
            BigDecimal val=new BigDecimal(vce.getNewValue().toString());
            checkValueBinding.setValue(val);
            BigDecimal num=new BigDecimal(1);
            BigDecimal st=new BigDecimal(5);
            BigDecimal per=new BigDecimal(100);
            BigDecimal totalCount=(BigDecimal)totalCountBinding.getValue();
            System.out.println("TOTAL COUNT"+totalCount);
            AdfFacesContext.getCurrentInstance().addPartialTarget(totalCountBinding);

            if(totalCount==null)
            {
                totalCountBinding.setValue(num);
                totalScoreBinding.setValue(val);
                System.out.println("SCOREE"+val);
                BigDecimal performance=(BigDecimal)((val.divide(num.multiply(st))).multiply(per));
                System.out.println("Performance"+performance);
                performanceOfAgencyBinding.setValue(performance);
                totalCountTableBinding.setValue(num);
//                totalScoreTableBinding.setValue(val);
//                performanceTableBinding.setValue(performance);
            }
           else
            {
                BigDecimal num1=new BigDecimal(0);
                num1=num1.add(totalCount.add(num));
                System.out.println("NUM!!!!"+num1);
                BigDecimal score=(BigDecimal)totalScoreBinding.getValue();
                System.out.println("SCOREEEEEE"+score);
                BigDecimal totalScore=score.add(val);
                System.out.println("TOTAL SCORE"+totalScore);
                System.out.println("COUNTTTT"+num1);
                System.out.println("TOTAL SCORE"+totalScore);
                System.out.println("TOTAL SCORE"+totalScore);
                BigDecimal performance=(BigDecimal)((totalScore.divide(num1.multiply(st), 2, RoundingMode.HALF_UP)).multiply(per));
                System.out.println("Performance"+performance);
                totalScoreBinding.setValue(totalScore);
                performanceOfAgencyBinding.setValue(performance);
                totalCountBinding.setValue(num1); 
//                totalCountTableBinding.setValue(num1);
//                totalScoreTableBinding.setValue(totalScore);
//                performanceTableBinding.setValue(performance);
                OperationBinding op=(OperationBinding)ADFUtils.findOperation("setDataInEvaluationTable");
                op.getParamsMap().put("totalCount", num1);
                op.getParamsMap().put("totalScore", totalScore);
                op.getParamsMap().put("performance", performance);
                op.execute();
            }
        }
        if(vce.getNewValue()!=null && vce.getOldValue()!=null)
        {
            BigDecimal val=new BigDecimal(vce.getNewValue().toString());
            checkValueBinding.setValue(val);
            BigDecimal num=new BigDecimal(1);
            BigDecimal st=new BigDecimal(5);
            BigDecimal per=new BigDecimal(100);
            BigDecimal totalCount=(BigDecimal)totalCountBinding.getValue();
            System.out.println("TOTAL COUNT"+totalCount);
            AdfFacesContext.getCurrentInstance().addPartialTarget(totalCountBinding);
            BigDecimal num1=(BigDecimal)totalCountBinding.getValue();
//            num1=num1.add(totalCount.add(num));
            System.out.println("NUM!!!!"+num1);
            BigDecimal score=(BigDecimal)totalScoreBinding.getValue();
            System.out.println("SCOREEEEEE"+score);
            BigDecimal oldVal=(BigDecimal)vce.getOldValue();
            BigDecimal totalScore=(score.subtract(oldVal)).add(val);
            System.out.println("TOTAL SCORE"+totalScore);
            System.out.println("COUNTTTT"+num1);
            System.out.println("TOTAL SCORE"+totalScore);
            System.out.println("TOTAL SCORE"+totalScore);
            BigDecimal performance=(BigDecimal)((totalScore.divide(num1.multiply(st), 2, RoundingMode.HALF_UP)).multiply(per));
            System.out.println("Performance"+performance);
            totalScoreBinding.setValue(totalScore);
            performanceOfAgencyBinding.setValue(performance);
//            totalCountBinding.setValue(num1); 
//            totalCountTableBinding.setValue(num1);
//            totalScoreTableBinding.setValue(totalScore);
//            performanceTableBinding.setValue(performance);
            OperationBinding op=(OperationBinding)ADFUtils.findOperation("setDataInEvaluationTable");
                op.getParamsMap().put("totalCount", num1);
                op.getParamsMap().put("totalScore", totalScore);
                op.getParamsMap().put("performance", performance);
                op.execute();
        }
        
    }

    public void setCheckValueBinding(RichInputText checkValueBinding) {
        this.checkValueBinding = checkValueBinding;
    }

    public RichInputText getCheckValueBinding() {
        return checkValueBinding;
    }

    public void callAction() {
        ADFUtils.findOperation("CreateInsert").execute();
//        ADFUtils.findOperation("standardChecklistPopulate").execute();
    }

    public void checkAL(ActionEvent actionEvent) {
//        ADFUtils.findOperation("standardChecklistPopulate").execute();

    }

    public void setPanelCollectionBinding(RichPanelCollection panelCollectionBinding) {
        this.panelCollectionBinding = panelCollectionBinding;
    }

    public RichPanelCollection getPanelCollectionBinding() {
        return panelCollectionBinding;
    }


    public void agencyCodeVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce.getNewValue()!=null)
        {
//            ADFUtils.findOperation("standardChecklistPopulate").execute();
        }
    }

    public void setTotalCountTableBinding(RichInputText totalCountTableBinding) {
        this.totalCountTableBinding = totalCountTableBinding;
    }

    public RichInputText getTotalCountTableBinding() {
        return totalCountTableBinding;
    }

    public void setTotalScoreTableBinding(RichInputText totalScoreTableBinding) {
        this.totalScoreTableBinding = totalScoreTableBinding;
    }

    public RichInputText getTotalScoreTableBinding() {
        return totalScoreTableBinding;
    }

    public void setPerformanceTableBinding(RichInputText performanceTableBinding) {
        this.performanceTableBinding = performanceTableBinding;
    }

    public RichInputText getPerformanceTableBinding() {
        return performanceTableBinding;
    }

    public void evalTypeVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce.getNewValue()!=null)
        {
            ADFUtils.findOperation("standardChecklistPopulate").execute();
        }
    }
}
