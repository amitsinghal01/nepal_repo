package terms.mtl.transaction.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectBooleanCheckbox;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

public class CreateContractQuotationBean {
    private RichInputText totAmtBinding;
    private RichInputText quotationNoBinding;
    private RichInputText amtBinding;
    private RichInputDate quotationDtBinding;
    private RichInputComboboxListOfValues unitCdBinding;
    private RichInputText vendorQuotNoBinding;
    private RichInputDate vendorQuotDtBinding;
    private RichSelectBooleanCheckbox cancelledBinding;
    private RichInputText freightRemarksBinding;
    private RichInputText pfRemarkBinding;
    private RichInputComboboxListOfValues paymentTermBinding;
    private RichInputComboboxListOfValues preparedByBinding;
    private RichInputComboboxListOfValues approvedByBinding;
    private RichInputText freightAmtBinding;
    private RichInputText pfAmountBinding;
    private RichInputText exciseBinding;
    private RichInputText otherBinding;
    private RichInputText deliveryTermBinding;
    private RichInputText priceBaseBinding;
    private RichInputText saleTaxBinding;
    private RichTable quotationDtlTableBinding;
    private RichOutputText bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton detailcreateBinding;
    private RichButton detaildeleteBinding;
    private RichInputComboboxListOfValues enquiryNoBinding;
    private RichInputComboboxListOfValues vendorCodeBinding;
    private RichInputText itemCdBinding;
    private RichSelectOneChoice quotationTypeBinding;

    public CreateContractQuotationBean() {
    }

    public void deleteDtlPopUpDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(quotationDtlTableBinding);
    }

    public void saveDataAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("quotNo").execute();
        String empcd=(String)ADFUtils.resolveExpression("#{pageFlowScope.empCode}") ; 
        System.out.println("empcd"+empcd);
        ADFUtils.setColumnValue("QuotationHeaderVO4Iterator", "ModifiedBy", empcd);

        if (vendorCodeBinding.isDisabled()) {
            ADFUtils.findOperation("Commit").execute();
            FacesMessage Message = new FacesMessage("Record Update Successfully.");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        } else {
            System.out.println("-----*** Create Mode ***-----");

            ADFUtils.findOperation("Commit").execute();
            FacesMessage Message =
                new FacesMessage("Record Save Successfully.Quotation Number No. is " +
                                 getQuotationNoBinding().getValue());
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
        }
    }

    public void discountVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());            
        if(vce.getNewValue()!=null && totAmtBinding.getValue()!=null)
        {
           System.out.println("New Value"+vce.getNewValue()+" Amount"+totAmtBinding.getValue());
            BigDecimal amt1 = (BigDecimal)totAmtBinding.getValue();
            BigDecimal disAmt = new BigDecimal(0);
            BigDecimal per = new BigDecimal(100);
                System.out.println("amt is*****"+amt1);
                BigDecimal dis=(BigDecimal) vce.getNewValue();
                disAmt=amt1.subtract((amt1.multiply(dis)).divide(per));
                System.out.println("Discount Amount===>"+disAmt); 
            totAmtBinding.setValue(disAmt);
            
        }
    }

    public void setTotAmtBinding(RichInputText totAmtBinding) {
        this.totAmtBinding = totAmtBinding;
    }

    public RichInputText getTotAmtBinding() {
        return totAmtBinding;
    }

    public void setQuotationNoBinding(RichInputText quotationNoBinding) {
        this.quotationNoBinding = quotationNoBinding;
    }

    public RichInputText getQuotationNoBinding() {
        return quotationNoBinding;
    }

    public void setAmtBinding(RichInputText amtBinding) {
        this.amtBinding = amtBinding;
    }

    public RichInputText getAmtBinding() {
        return amtBinding;
    }

    public void setQuotationDtBinding(RichInputDate quotationDtBinding) {
        this.quotationDtBinding = quotationDtBinding;
    }

    public RichInputDate getQuotationDtBinding() {
        return quotationDtBinding;
    }

    public void setUnitCdBinding(RichInputComboboxListOfValues unitCdBinding) {
        this.unitCdBinding = unitCdBinding;
    }

    public RichInputComboboxListOfValues getUnitCdBinding() {
        return unitCdBinding;
    }

    public void setVendorQuotNoBinding(RichInputText vendorQuotNoBinding) {
        this.vendorQuotNoBinding = vendorQuotNoBinding;
    }

    public RichInputText getVendorQuotNoBinding() {
        return vendorQuotNoBinding;
    }

    public void setVendorQuotDtBinding(RichInputDate vendorQuotDtBinding) {
        this.vendorQuotDtBinding = vendorQuotDtBinding;
    }

    public RichInputDate getVendorQuotDtBinding() {
        return vendorQuotDtBinding;
    }

    public void setCancelledBinding(RichSelectBooleanCheckbox cancelledBinding) {
        this.cancelledBinding = cancelledBinding;
    }

    public RichSelectBooleanCheckbox getCancelledBinding() {
        return cancelledBinding;
    }

    public void setFreightRemarksBinding(RichInputText freightRemarksBinding) {
        this.freightRemarksBinding = freightRemarksBinding;
    }

    public RichInputText getFreightRemarksBinding() {
        return freightRemarksBinding;
    }

    public void setPfRemarkBinding(RichInputText pfRemarkBinding) {
        this.pfRemarkBinding = pfRemarkBinding;
    }

    public RichInputText getPfRemarkBinding() {
        return pfRemarkBinding;
    }

    public void setPaymentTermBinding(RichInputComboboxListOfValues paymentTermBinding) {
        this.paymentTermBinding = paymentTermBinding;
    }

    public RichInputComboboxListOfValues getPaymentTermBinding() {
        return paymentTermBinding;
    }

    public void setPreparedByBinding(RichInputComboboxListOfValues preparedByBinding) {
        this.preparedByBinding = preparedByBinding;
    }

    public RichInputComboboxListOfValues getPreparedByBinding() {
        return preparedByBinding;
    }

    public void setApprovedByBinding(RichInputComboboxListOfValues approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputComboboxListOfValues getApprovedByBinding() {
        return approvedByBinding;
    }

    public void setFreightAmtBinding(RichInputText freightAmtBinding) {
        this.freightAmtBinding = freightAmtBinding;
    }

    public RichInputText getFreightAmtBinding() {
        return freightAmtBinding;
    }

    public void setPfAmountBinding(RichInputText pfAmountBinding) {
        this.pfAmountBinding = pfAmountBinding;
    }

    public RichInputText getPfAmountBinding() {
        return pfAmountBinding;
    }

    public void setExciseBinding(RichInputText exciseBinding) {
        this.exciseBinding = exciseBinding;
    }

    public RichInputText getExciseBinding() {
        return exciseBinding;
    }

    public void setOtherBinding(RichInputText otherBinding) {
        this.otherBinding = otherBinding;
    }

    public RichInputText getOtherBinding() {
        return otherBinding;
    }

    public void setDeliveryTermBinding(RichInputText deliveryTermBinding) {
        this.deliveryTermBinding = deliveryTermBinding;
    }

    public RichInputText getDeliveryTermBinding() {
        return deliveryTermBinding;
    }

    public void setPriceBaseBinding(RichInputText priceBaseBinding) {
        this.priceBaseBinding = priceBaseBinding;
    }

    public RichInputText getPriceBaseBinding() {
        return priceBaseBinding;
    }

    public void setSaleTaxBinding(RichInputText saleTaxBinding) {
        this.saleTaxBinding = saleTaxBinding;
    }

    public RichInputText getSaleTaxBinding() {
        return saleTaxBinding;
    }

    public void setQuotationDtlTableBinding(RichTable quotationDtlTableBinding) {
        this.quotationDtlTableBinding = quotationDtlTableBinding;
    }

    public RichTable getQuotationDtlTableBinding() {
        return quotationDtlTableBinding;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void editAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setDetailcreateBinding(RichButton detailcreateBinding) {
        this.detailcreateBinding = detailcreateBinding;
    }

    public RichButton getDetailcreateBinding() {
        return detailcreateBinding;
    }

    public void setDetaildeleteBinding(RichButton detaildeleteBinding) {
        this.detaildeleteBinding = detaildeleteBinding;
    }

    public RichButton getDetaildeleteBinding() {
        return detaildeleteBinding;
    }
    
    private void cevmodecheck(){
            if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
                cevModeDisableComponent("V");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("C");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("E");
            }
        }
    
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
               try {
                   Method method1 =
                       component.getClass().getMethod("setDisabled", boolean.class);
                   if (method1 != null) {
                       method1.invoke(component, valueComponent);
                   }
               } catch (NoSuchMethodException e) {
                   try {
                       Method method =
                           component.getClass().getMethod("setReadOnly", boolean.class);
                       if (method != null) {
                           method.invoke(component, valueComponent);
                       }
                   } catch (Exception e1) {
                       // e.printStackTrace();//silently eat this exception.
                   }


               } catch (Exception e) {
                   // e.printStackTrace();//silently eat this exception.
               }
               List<UIComponent> childComponents = component.getChildren();
               for (UIComponent comp : childComponents) {
                   makeComponentHierarchyReadOnly(comp, valueComponent);
               }
           }
    
    
    
    //Set Fields and Button disable.
        public void cevModeDisableComponent(String mode) {
        //            FacesContext fctx = FacesContext.getCurrentInstance();
        //            ELContext elctx = fctx.getELContext();
        //            Application jsfApp = fctx.getApplication();
        //            //create a ValueExpression that points to the ADF binding layer
        //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
        //            //
        //            ValueExpression valueExpr = exprFactory.createValueExpression(
        //                                         elctx,
        //                                         "#{pageFlowScope.mode=='E'}",
        //                                          Object.class
        //                                         );
        //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
            if (mode.equals("E")) {
                getDetailcreateBinding().setDisabled(false);
                getDetaildeleteBinding().setDisabled(false);
                quotationNoBinding.setDisabled(true);
                quotationDtBinding.setDisabled(true);
                enquiryNoBinding.setDisabled(true);
                vendorQuotNoBinding.setDisabled(true);
                vendorQuotDtBinding.setDisabled(true);
                preparedByBinding.setDisabled(true);
                vendorCodeBinding.setDisabled(true);
                itemCdBinding.setDisabled(true);
                unitCdBinding.setDisabled(true);
                quotationTypeBinding.setDisabled(true);

            } else if (mode.equals("C")) {
                getDetailcreateBinding().setDisabled(false);
                getDetaildeleteBinding().setDisabled(false);
                quotationNoBinding.setDisabled(true);
                quotationDtBinding.setDisabled(true);
                enquiryNoBinding.setDisabled(true);
                quotationTypeBinding.setDisabled(true);
                preparedByBinding.setDisabled(true);
                approvedByBinding.setDisabled(true);
                itemCdBinding.setDisabled(true);
                unitCdBinding.setDisabled(false);
            } else if (mode.equals("V")) {
                getDetailcreateBinding().setDisabled(true);
                getDetaildeleteBinding().setDisabled(true);
            }
            
        }

    public void setEnquiryNoBinding(RichInputComboboxListOfValues enquiryNoBinding) {
        this.enquiryNoBinding = enquiryNoBinding;
    }

    public RichInputComboboxListOfValues getEnquiryNoBinding() {
        return enquiryNoBinding;
    }

    public void setVendorCodeBinding(RichInputComboboxListOfValues vendorCodeBinding) {
        this.vendorCodeBinding = vendorCodeBinding;
    }

    public RichInputComboboxListOfValues getVendorCodeBinding() {
        return vendorCodeBinding;
    }

    public void setItemCdBinding(RichInputText itemCdBinding) {
        this.itemCdBinding = itemCdBinding;
    }

    public RichInputText getItemCdBinding() {
        return itemCdBinding;
    }

    public void setQuotationTypeBinding(RichSelectOneChoice quotationTypeBinding) {
        this.quotationTypeBinding = quotationTypeBinding;
    }

    public RichSelectOneChoice getQuotationTypeBinding() {
        return quotationTypeBinding;
    }
}
