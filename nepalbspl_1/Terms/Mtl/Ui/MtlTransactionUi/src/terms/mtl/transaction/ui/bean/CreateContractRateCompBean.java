package terms.mtl.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.event.DialogEvent;

public class CreateContractRateCompBean {
    private RichTable rateCompareTableBinding;
    private RichTable detailDeletePopUpDL;
    private RichInputComboboxListOfValues unitCdBinding;
    private RichInputText compareNoBinding;
    private RichInputDate compareDateBinding;
    private RichInputComboboxListOfValues quotation1Binding;
    private RichInputComboboxListOfValues quotation2Binding;
    private RichInputComboboxListOfValues quotation3Binding;
    private RichInputComboboxListOfValues quotation4Binding;
    private RichInputComboboxListOfValues quotation5Binding;
    private RichInputComboboxListOfValues modifiedByBinding;
    private RichInputDate modifiedDateBinding;
    private RichInputComboboxListOfValues approvedByBinding;
    private RichInputComboboxListOfValues jobCdBinding;
    private RichInputText jobDetailBinding;
    private RichInputText uomBinding;
    private RichInputText lastPoNoBinding;
    private RichInputText materialRateBinding;
    private RichInputText qtyBinding;
    private RichInputText basicBinding;
    private RichInputText discBinding;
    private RichInputText discAmtBinding;
    private RichInputText netRateBinding;
    private RichInputText gstBinding;
    private RichInputText basicRate1Binding;
    private RichInputText discPer1Binding;
    private RichInputText discAmt1Binding;
    private RichInputText rate1Binding;
    private RichInputText gst1Binding;
    private RichInputText basicRate2Binding;
    private RichInputText basicRate3Binding;
    private RichInputText basicRate4Binding;
    private RichInputText basicRate5Binding;
    private RichInputText discPer2Binding;
    private RichInputText discPer3Binding;
    private RichInputText discPer4Binding;
    private RichInputText discPer5Binding;
    private RichInputText discAmt2Binding;
    private RichInputText discAmt3Binding;
    private RichInputText discAmt4Binding;
    private RichInputText discAmt5Binding;
    private RichInputText rate2Binding;
    private RichInputText rate3Binding;
    private RichInputText rate4Binding;
    private RichInputText rate5Binding;
    private RichInputText gst2Binding;
    private RichInputText gst3Binding;
    private RichInputText gst4Binding;
    private RichInputText gst5Binding;
    private RichInputText itemCodeBinding;
    private RichInputText quotNoBinding;
    private RichInputText rateBinding;
    private RichInputText vendorCdBinding;
    private RichInputText uom1Binding;
    private RichInputText remarksBinding;
    private RichOutputText bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton detailcreateBinding;
    private RichButton detailDeleteBinding;
    private RichButton detailDelete1Binding;
    private RichButton compareButtonBinding;
    private RichShowDetailItem rateChartComparisonBinding;
    private RichShowDetailItem compareMinRateBinding;
    private RichInputText itemCdBinding;

    public CreateContractRateCompBean() {
    }

    public void compRatedeletePopUpDL(DialogEvent dialogEvent) {
        // Add event code here...
    }

    public void setRateCompareTableBinding(RichTable rateCompareTableBinding) {
        this.rateCompareTableBinding = rateCompareTableBinding;
    }

    public RichTable getRateCompareTableBinding() {
        return rateCompareTableBinding;
    }

    public void deletePopUpDL(DialogEvent dialogEvent) {
        // Add event code here...
    }

    public void setDetailDeletePopUpDL(RichTable detailDeletePopUpDL) {
        this.detailDeletePopUpDL = detailDeletePopUpDL;
    }

    public RichTable getDetailDeletePopUpDL() {
        return detailDeletePopUpDL;
    }

    public void setUnitCdBinding(RichInputComboboxListOfValues unitCdBinding) {
        this.unitCdBinding = unitCdBinding;
    }

    public RichInputComboboxListOfValues getUnitCdBinding() {
        return unitCdBinding;
    }

    public void setCompareNoBinding(RichInputText compareNoBinding) {
        this.compareNoBinding = compareNoBinding;
    }

    public RichInputText getCompareNoBinding() {
        return compareNoBinding;
    }

    public void setCompareDateBinding(RichInputDate compareDateBinding) {
        this.compareDateBinding = compareDateBinding;
    }

    public RichInputDate getCompareDateBinding() {
        return compareDateBinding;
    }

    public void setQuotation1Binding(RichInputComboboxListOfValues quotation1Binding) {
        this.quotation1Binding = quotation1Binding;
    }

    public RichInputComboboxListOfValues getQuotation1Binding() {
        return quotation1Binding;
    }

    public void setQuotation2Binding(RichInputComboboxListOfValues quotation2Binding) {
        this.quotation2Binding = quotation2Binding;
    }

    public RichInputComboboxListOfValues getQuotation2Binding() {
        return quotation2Binding;
    }

    public void setQuotation3Binding(RichInputComboboxListOfValues quotation3Binding) {
        this.quotation3Binding = quotation3Binding;
    }

    public RichInputComboboxListOfValues getQuotation3Binding() {
        return quotation3Binding;
    }

    public void setQuotation4Binding(RichInputComboboxListOfValues quotation4Binding) {
        this.quotation4Binding = quotation4Binding;
    }

    public RichInputComboboxListOfValues getQuotation4Binding() {
        return quotation4Binding;
    }

    public void setQuotation5Binding(RichInputComboboxListOfValues quotation5Binding) {
        this.quotation5Binding = quotation5Binding;
    }

    public RichInputComboboxListOfValues getQuotation5Binding() {
        return quotation5Binding;
    }

    public void setModifiedByBinding(RichInputComboboxListOfValues modifiedByBinding) {
        this.modifiedByBinding = modifiedByBinding;
    }

    public RichInputComboboxListOfValues getModifiedByBinding() {
        return modifiedByBinding;
    }

    public void setModifiedDateBinding(RichInputDate modifiedDateBinding) {
        this.modifiedDateBinding = modifiedDateBinding;
    }

    public RichInputDate getModifiedDateBinding() {
        return modifiedDateBinding;
    }

    public void setApprovedByBinding(RichInputComboboxListOfValues approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputComboboxListOfValues getApprovedByBinding() {
        return approvedByBinding;
    }

    public void setJobCdBinding(RichInputComboboxListOfValues jobCdBinding) {
        this.jobCdBinding = jobCdBinding;
    }

    public RichInputComboboxListOfValues getJobCdBinding() {
        return jobCdBinding;
    }

    public void setJobDetailBinding(RichInputText jobDetailBinding) {
        this.jobDetailBinding = jobDetailBinding;
    }

    public RichInputText getJobDetailBinding() {
        return jobDetailBinding;
    }

    public void setUomBinding(RichInputText uomBinding) {
        this.uomBinding = uomBinding;
    }

    public RichInputText getUomBinding() {
        return uomBinding;
    }

    public void setLastPoNoBinding(RichInputText lastPoNoBinding) {
        this.lastPoNoBinding = lastPoNoBinding;
    }

    public RichInputText getLastPoNoBinding() {
        return lastPoNoBinding;
    }

    public void setMaterialRateBinding(RichInputText materialRateBinding) {
        this.materialRateBinding = materialRateBinding;
    }

    public RichInputText getMaterialRateBinding() {
        return materialRateBinding;
    }

    public void setQtyBinding(RichInputText qtyBinding) {
        this.qtyBinding = qtyBinding;
    }

    public RichInputText getQtyBinding() {
        return qtyBinding;
    }

    public void setBasicBinding(RichInputText basicBinding) {
        this.basicBinding = basicBinding;
    }

    public RichInputText getBasicBinding() {
        return basicBinding;
    }

    public void setDiscBinding(RichInputText discBinding) {
        this.discBinding = discBinding;
    }

    public RichInputText getDiscBinding() {
        return discBinding;
    }

    public void setDiscAmtBinding(RichInputText discAmtBinding) {
        this.discAmtBinding = discAmtBinding;
    }

    public RichInputText getDiscAmtBinding() {
        return discAmtBinding;
    }

    public void setNetRateBinding(RichInputText netRateBinding) {
        this.netRateBinding = netRateBinding;
    }

    public RichInputText getNetRateBinding() {
        return netRateBinding;
    }

    public void setGstBinding(RichInputText gstBinding) {
        this.gstBinding = gstBinding;
    }

    public RichInputText getGstBinding() {
        return gstBinding;
    }

    public void setBasicRate1Binding(RichInputText basicRate1Binding) {
        this.basicRate1Binding = basicRate1Binding;
    }

    public RichInputText getBasicRate1Binding() {
        return basicRate1Binding;
    }

    public void setDiscPer1Binding(RichInputText discPer1Binding) {
        this.discPer1Binding = discPer1Binding;
    }

    public RichInputText getDiscPer1Binding() {
        return discPer1Binding;
    }

    public void setDiscAmt1Binding(RichInputText discAmt1Binding) {
        this.discAmt1Binding = discAmt1Binding;
    }

    public RichInputText getDiscAmt1Binding() {
        return discAmt1Binding;
    }

    public void setRate1Binding(RichInputText rate1Binding) {
        this.rate1Binding = rate1Binding;
    }

    public RichInputText getRate1Binding() {
        return rate1Binding;
    }

    public void setGst1Binding(RichInputText gst1Binding) {
        this.gst1Binding = gst1Binding;
    }

    public RichInputText getGst1Binding() {
        return gst1Binding;
    }

    public void setBasicRate2Binding(RichInputText basicRate2Binding) {
        this.basicRate2Binding = basicRate2Binding;
    }

    public RichInputText getBasicRate2Binding() {
        return basicRate2Binding;
    }

    public void setBasicRate3Binding(RichInputText basicRate3Binding) {
        this.basicRate3Binding = basicRate3Binding;
    }

    public RichInputText getBasicRate3Binding() {
        return basicRate3Binding;
    }

    public void setBasicRate4Binding(RichInputText basicRate4Binding) {
        this.basicRate4Binding = basicRate4Binding;
    }

    public RichInputText getBasicRate4Binding() {
        return basicRate4Binding;
    }

    public void setBasicRate5Binding(RichInputText basicRate5Binding) {
        this.basicRate5Binding = basicRate5Binding;
    }

    public RichInputText getBasicRate5Binding() {
        return basicRate5Binding;
    }

    public void setDiscPer2Binding(RichInputText discPer2Binding) {
        this.discPer2Binding = discPer2Binding;
    }

    public RichInputText getDiscPer2Binding() {
        return discPer2Binding;
    }

    public void setDiscPer3Binding(RichInputText discPer3Binding) {
        this.discPer3Binding = discPer3Binding;
    }

    public RichInputText getDiscPer3Binding() {
        return discPer3Binding;
    }

    public void setDiscPer4Binding(RichInputText discPer4Binding) {
        this.discPer4Binding = discPer4Binding;
    }

    public RichInputText getDiscPer4Binding() {
        return discPer4Binding;
    }

    public void setDiscPer5Binding(RichInputText discPer5Binding) {
        this.discPer5Binding = discPer5Binding;
    }

    public RichInputText getDiscPer5Binding() {
        return discPer5Binding;
    }

    public void setDiscAmt2Binding(RichInputText discAmt2Binding) {
        this.discAmt2Binding = discAmt2Binding;
    }

    public RichInputText getDiscAmt2Binding() {
        return discAmt2Binding;
    }

    public void setDiscAmt3Binding(RichInputText discAmt3Binding) {
        this.discAmt3Binding = discAmt3Binding;
    }

    public RichInputText getDiscAmt3Binding() {
        return discAmt3Binding;
    }

    public void setDiscAmt4Binding(RichInputText discAmt4Binding) {
        this.discAmt4Binding = discAmt4Binding;
    }

    public RichInputText getDiscAmt4Binding() {
        return discAmt4Binding;
    }

    public void setDiscAmt5Binding(RichInputText discAmt5Binding) {
        this.discAmt5Binding = discAmt5Binding;
    }

    public RichInputText getDiscAmt5Binding() {
        return discAmt5Binding;
    }

    public void setRate2Binding(RichInputText rate2Binding) {
        this.rate2Binding = rate2Binding;
    }

    public RichInputText getRate2Binding() {
        return rate2Binding;
    }

    public void setRate3Binding(RichInputText rate3Binding) {
        this.rate3Binding = rate3Binding;
    }

    public RichInputText getRate3Binding() {
        return rate3Binding;
    }

    public void setRate4Binding(RichInputText rate4Binding) {
        this.rate4Binding = rate4Binding;
    }

    public RichInputText getRate4Binding() {
        return rate4Binding;
    }

    public void setRate5Binding(RichInputText rate5Binding) {
        this.rate5Binding = rate5Binding;
    }

    public RichInputText getRate5Binding() {
        return rate5Binding;
    }

    public void setGst2Binding(RichInputText gst2Binding) {
        this.gst2Binding = gst2Binding;
    }

    public RichInputText getGst2Binding() {
        return gst2Binding;
    }

    public void setGst3Binding(RichInputText gst3Binding) {
        this.gst3Binding = gst3Binding;
    }

    public RichInputText getGst3Binding() {
        return gst3Binding;
    }

    public void setGst4Binding(RichInputText gst4Binding) {
        this.gst4Binding = gst4Binding;
    }

    public RichInputText getGst4Binding() {
        return gst4Binding;
    }

    public void setGst5Binding(RichInputText gst5Binding) {
        this.gst5Binding = gst5Binding;
    }

    public RichInputText getGst5Binding() {
        return gst5Binding;
    }

    public void setItemCodeBinding(RichInputText itemCodeBinding) {
        this.itemCodeBinding = itemCodeBinding;
    }

    public RichInputText getItemCodeBinding() {
        return itemCodeBinding;
    }

    public void setQuotNoBinding(RichInputText quotNoBinding) {
        this.quotNoBinding = quotNoBinding;
    }

    public RichInputText getQuotNoBinding() {
        return quotNoBinding;
    }

    public void setRateBinding(RichInputText rateBinding) {
        this.rateBinding = rateBinding;
    }

    public RichInputText getRateBinding() {
        return rateBinding;
    }

    public void setVendorCdBinding(RichInputText vendorCdBinding) {
        this.vendorCdBinding = vendorCdBinding;
    }

    public RichInputText getVendorCdBinding() {
        return vendorCdBinding;
    }

    public void setUom1Binding(RichInputText uom1Binding) {
        this.uom1Binding = uom1Binding;
    }

    public RichInputText getUom1Binding() {
        return uom1Binding;
    }

    public void setRemarksBinding(RichInputText remarksBinding) {
        this.remarksBinding = remarksBinding;
    }

    public RichInputText getRemarksBinding() {
        return remarksBinding;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setDetailcreateBinding(RichButton detailcreateBinding) {
        this.detailcreateBinding = detailcreateBinding;
    }

    public RichButton getDetailcreateBinding() {
        return detailcreateBinding;
    }

    public void setDetailDeleteBinding(RichButton detailDeleteBinding) {
        this.detailDeleteBinding = detailDeleteBinding;
    }

    public RichButton getDetailDeleteBinding() {
        return detailDeleteBinding;
    }

    public void setDetailDelete1Binding(RichButton detailDelete1Binding) {
        this.detailDelete1Binding = detailDelete1Binding;
    }

    public RichButton getDetailDelete1Binding() {
        return detailDelete1Binding;
    }

    public void setCompareButtonBinding(RichButton compareButtonBinding) {
        this.compareButtonBinding = compareButtonBinding;
    }

    public RichButton getCompareButtonBinding() {
        return compareButtonBinding;
    }

    public void EditAL(ActionEvent actionEvent) {
        cevmodecheck();
    }
    
    private void cevmodecheck(){
            if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
                cevModeDisableComponent("V");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("C");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("E");
            }
        }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                try {
                    Method method1 =
                        component.getClass().getMethod("setDisabled", boolean.class);
                    if (method1 != null) {
                        method1.invoke(component, valueComponent);
                    }
                } catch (NoSuchMethodException e) {
                    try {
                        Method method =
                            component.getClass().getMethod("setReadOnly", boolean.class);
                        if (method != null) {
                            method.invoke(component, valueComponent);
                        }
                    } catch (Exception e1) {
                        // e.printStackTrace();//silently eat this exception.
                    }


                } catch (Exception e) {
                    // e.printStackTrace();//silently eat this exception.
                }
                List<UIComponent> childComponents = component.getChildren();
                for (UIComponent comp : childComponents) {
                    makeComponentHierarchyReadOnly(comp, valueComponent);
                }
            }

    //Set Fields and Button disable.
        public void cevModeDisableComponent(String mode) {
        //            FacesContext fctx = FacesContext.getCurrentInstance();
        //            ELContext elctx = fctx.getELContext();
        //            Application jsfApp = fctx.getApplication();
        //            //create a ValueExpression that points to the ADF binding layer
        //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
        //            //
        //            ValueExpression valueExpr = exprFactory.createValueExpression(
        //                                         elctx,
        //                                         "#{pageFlowScope.mode=='E'}",
        //                                          Object.class
        //                                         );
        //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
            if (mode.equals("E")) {
                unitCdBinding.setDisabled(true);
                itemCdBinding.setDisabled(true);
                compareNoBinding.setDisabled(true);
                compareDateBinding.setDisabled(true);
                quotation1Binding.setDisabled(true);
                quotation2Binding.setDisabled(true);
                quotation3Binding.setDisabled(true);
                quotation4Binding.setDisabled(true);
                quotation5Binding.setDisabled(true);
                jobCdBinding.setDisabled(true);
                jobDetailBinding.setDisabled(true);
                uomBinding.setDisabled(true);
                lastPoNoBinding.setDisabled(true);
                materialRateBinding.setDisabled(true);
                qtyBinding.setDisabled(true);
                basicBinding.setDisabled(true);
                discBinding.setDisabled(true);
                discAmtBinding.setDisabled(true);
                rateBinding.setDisabled(true);
                gstBinding.setDisabled(true);
                basicRate1Binding.setDisabled(true);
                basicRate2Binding.setDisabled(true);
                basicRate3Binding.setDisabled(true);
                basicRate4Binding.setDisabled(true);
                basicRate5Binding.setDisabled(true);
                discPer1Binding.setDisabled(true);
                discPer2Binding.setDisabled(true);
                discPer3Binding.setDisabled(true);
                discPer4Binding.setDisabled(true);
                discPer5Binding.setDisabled(true);
                discAmt1Binding.setDisabled(true);
                discAmt2Binding.setDisabled(true);               
                discAmt3Binding.setDisabled(true);
                discAmt4Binding.setDisabled(true);
                discAmt5Binding.setDisabled(true);
                rate1Binding.setDisabled(true);
                rate2Binding.setDisabled(true);
                rate3Binding.setDisabled(true);   
                rate4Binding.setDisabled(true);
                rate5Binding.setDisabled(true);
                gst1Binding.setDisabled(true);
                gst2Binding.setDisabled(true);
                gst3Binding.setDisabled(true);
                gst4Binding.setDisabled(true);
                gst5Binding.setDisabled(true);
                itemCodeBinding.setDisabled(true);
                quotNoBinding.setDisabled(true);
                rateBinding.setDisabled(true);
                vendorCdBinding.setDisabled(true);
                uom1Binding.setDisabled(true);
                remarksBinding.setDisabled(true);
                }
            else if (mode.equals("C")) {
                unitCdBinding.setDisabled(false);
                itemCdBinding.setDisabled(true);
                rateChartComparisonBinding.setDisabled(false);
                compareMinRateBinding.setDisabled(false);
                compareNoBinding.setDisabled(true);
                compareDateBinding.setDisabled(true);
                modifiedByBinding.setDisabled(true);
                modifiedDateBinding.setDisabled(true);
                approvedByBinding.setDisabled(true);
                jobCdBinding.setDisabled(false);
                jobDetailBinding.setDisabled(true);
                uomBinding.setDisabled(true);
                lastPoNoBinding.setDisabled(true);
                materialRateBinding.setDisabled(true);
                qtyBinding.setDisabled(true);
                basicBinding.setDisabled(true);
                discBinding.setDisabled(true);
                discAmtBinding.setDisabled(true);
                netRateBinding.setDisabled(true);
                gstBinding.setDisabled(true);
                basicRate1Binding.setDisabled(true);
                basicRate2Binding.setDisabled(true);
                basicRate3Binding.setDisabled(true);
                basicRate4Binding.setDisabled(true);
                basicRate5Binding.setDisabled(true);
                discPer1Binding.setDisabled(true);
                discPer2Binding.setDisabled(true);
                discPer3Binding.setDisabled(true);
                discPer4Binding.setDisabled(true);
                discPer5Binding.setDisabled(true);
                discAmt1Binding.setDisabled(true);
                discAmt2Binding.setDisabled(true);               
                discAmt3Binding.setDisabled(true);
                discAmt4Binding.setDisabled(true);
                discAmt5Binding.setDisabled(true);
                rate1Binding.setDisabled(true);
                rate2Binding.setDisabled(true);
                rate3Binding.setDisabled(true);   
                rate4Binding.setDisabled(true);
                rate5Binding.setDisabled(true);
                gst1Binding.setDisabled(true);
                gst2Binding.setDisabled(true);
                gst3Binding.setDisabled(true);
                gst4Binding.setDisabled(true);
                gst5Binding.setDisabled(true);
                itemCodeBinding.setDisabled(true);
                quotNoBinding.setDisabled(true);
                rateBinding.setDisabled(true);
                vendorCdBinding.setDisabled(true);
                uom1Binding.setDisabled(true);
                remarksBinding.setDisabled(true);
            }
            else if (mode.equals("V")) {
                            getDetailcreateBinding().setDisabled(true);
                            rateChartComparisonBinding.setDisabled(false);
                            compareMinRateBinding.setDisabled(false); 
                            detailDeleteBinding.setDisabled(true);
                            detailDelete1Binding.setDisabled(true);
                            compareButtonBinding.setDisabled(true);
                        }
            
        }

    public void setRateChartComparisonBinding(RichShowDetailItem rateChartComparisonBinding) {
        this.rateChartComparisonBinding = rateChartComparisonBinding;
    }

    public RichShowDetailItem getRateChartComparisonBinding() {
        return rateChartComparisonBinding;
    }

    public void setCompareMinRateBinding(RichShowDetailItem compareMinRateBinding) {
        this.compareMinRateBinding = compareMinRateBinding;
    }

    public RichShowDetailItem getCompareMinRateBinding() {
        return compareMinRateBinding;
    }

    public void setItemCdBinding(RichInputText itemCdBinding) {
        this.itemCdBinding = itemCdBinding;
    }

    public RichInputText getItemCdBinding() {
        return itemCdBinding;
    }

    public void jobCodeVCL(ValueChangeEvent vce) {
        // Add event code here...
    }
}
