package terms.mtl.transaction.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class CreateFreightLabourChargesBean {
    private RichPanelHeader getMyPageRootComponent;
    private RichTable detailTableBinding;
    private RichButton headerEditBinding;
    private RichInputText entryNoBinding;
    private RichOutputText outputTextBinding;
    private RichInputComboboxListOfValues unitCdBinding;
    private RichInputDate entryDateBinding;
    private RichInputComboboxListOfValues mrnNoBinding;
    private RichInputComboboxListOfValues materialCdBinding;
    private RichInputText transporterBinding;
    private RichInputText grNoBinding;
    private RichInputDate grDateBinding;
    private RichInputText grAmountBinding;
    private RichInputText localBinding;
    private RichInputText labourBinding;
    private RichInputText tdsAmountBinding;
    private RichInputText remarksBinding;
    private RichButton createButtonBinding;
    private RichButton deleteButtonBinding;
    private RichInputText unitNameBinding;
    private RichInputDate mrnDateBinding;
    private RichInputDate poDateBinding;
    private RichInputComboboxListOfValues preparedBinding;
    private RichInputComboboxListOfValues approvedBinding;
    private RichInputText preparedNameBinding;
    private RichInputText approvedNameBinding;
    private RichInputComboboxListOfValues tdsPerBinding;
    private RichInputText totalAmtBinding;

    public CreateFreightLabourChargesBean() {
    }
    
    public void saveButtonAL(ActionEvent actionEvent) {
        OperationBinding op=(OperationBinding)ADFUtils.findOperation("generateEntryNo");
        Object obj= op.execute();
        System.out.println("result after function calling: "+op.getResult());
        
        if(op.getResult() != null && op.getResult().toString().equalsIgnoreCase("E"))
        {
            ADFUtils.findOperation("Commit").execute();
            ADFUtils.showMessage(" Record Updated Successfully", 2);
        }
        else if (op.getResult() != null && op.getResult().toString() != ""  && op.getResult().equals("S"))
        {
            System.out.println("+++++ in the save mode+++++++++++");
            ADFUtils.findOperation("Commit").execute();
            ADFUtils.showMessage("Record Saved Successfully.Entry No.is "+ADFUtils.evaluateEL("#{bindings.EntryNo.inputValue}"), 2);
        }
        else if(op.getResult() != null && op.getResult().toString().equalsIgnoreCase("N"))
        {
            ADFUtils.showMessage("Entry No. could not be generated. Try Again !!", 0);
        }
    }
    
    public void deleteDialogDL(DialogEvent dialogEvent) {
            if (dialogEvent.getOutcome().name().equals("ok")) {
                ADFUtils.findOperation("Delete").execute();
                System.out.println("Record Delete Successfully");
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            }
        AdfFacesContext.getCurrentInstance().addPartialTarget(detailTableBinding);
        }

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
            try {
                Method method1 =
                    component.getClass().getMethod("setDisabled", boolean.class);
                if (method1 != null) {
                    method1.invoke(component, valueComponent);
                }
            } catch (NoSuchMethodException e) {
                try {
                    Method method =
                        component.getClass().getMethod("setReadOnly", boolean.class);
                    if (method != null) {
                        method.invoke(component, valueComponent);
                    }
                } catch (Exception e1) {
                    // e.printStackTrace();//silently eat this exception.
                }


            } catch (Exception e) {
                // e.printStackTrace();//silently eat this exception.
            }
            List<UIComponent> childComponents = component.getChildren();
            for (UIComponent comp : childComponents) {
                makeComponentHierarchyReadOnly(comp, valueComponent);
            }
        }
    
    private void cevmodecheck(){
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
                cevModeDisableComponent("V");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("C");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("E");
        }
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setDetailTableBinding(RichTable detailTableBinding) {
        this.detailTableBinding = detailTableBinding;
    }

    public RichTable getDetailTableBinding() {
        return detailTableBinding;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void EditButtonAL(ActionEvent actionEvent) {
        if (approvedBinding.getValue() == null) {
            cevmodecheck();
        }else{
            ADFUtils.showMessage("Record is Approved Can't be modified !", 2);
            ADFUtils.setEL("#{pageFlowScope.mode}", "V");
        }
    }

    public void setEntryNoBinding(RichInputText entryNoBinding) {
        this.entryNoBinding = entryNoBinding;
    }

    public RichInputText getEntryNoBinding() {
        return entryNoBinding;
    }

    public void setOutputTextBinding(RichOutputText outputTextBinding) {
        this.outputTextBinding = outputTextBinding;
    }

    public RichOutputText getOutputTextBinding() {
        cevmodecheck();
        return outputTextBinding;
    }

    public void setUnitCdBinding(RichInputComboboxListOfValues unitCdBinding) {
        this.unitCdBinding = unitCdBinding;
    }

    public RichInputComboboxListOfValues getUnitCdBinding() {
        return unitCdBinding;
    }

    public void setEntryDateBinding(RichInputDate entryDateBinding) {
        this.entryDateBinding = entryDateBinding;
    }

    public RichInputDate getEntryDateBinding() {
        return entryDateBinding;
    }

    public void setMrnNoBinding(RichInputComboboxListOfValues mrnNoBinding) {
        this.mrnNoBinding = mrnNoBinding;
    }

    public RichInputComboboxListOfValues getMrnNoBinding() {
        return mrnNoBinding;
    }

    public void setMaterialCdBinding(RichInputComboboxListOfValues materialCdBinding) {
        this.materialCdBinding = materialCdBinding;
    }

    public RichInputComboboxListOfValues getMaterialCdBinding() {
        return materialCdBinding;
    }

    public void setTransporterBinding(RichInputText transporterBinding) {
        this.transporterBinding = transporterBinding;
    }

    public RichInputText getTransporterBinding() {
        return transporterBinding;
    }

    public void setGrNoBinding(RichInputText grNoBinding) {
        this.grNoBinding = grNoBinding;
    }

    public RichInputText getGrNoBinding() {
        return grNoBinding;
    }

    public void setGrDateBinding(RichInputDate grDateBinding) {
        this.grDateBinding = grDateBinding;
    }

    public RichInputDate getGrDateBinding() {
        return grDateBinding;
    }

    public void setGrAmountBinding(RichInputText grAmountBinding) {
        this.grAmountBinding = grAmountBinding;
    }

    public RichInputText getGrAmountBinding() {
        return grAmountBinding;
    }

    public void setLocalBinding(RichInputText localBinding) {
        this.localBinding = localBinding;
    }

    public RichInputText getLocalBinding() {
        return localBinding;
    }

    public void setLabourBinding(RichInputText labourBinding) {
        this.labourBinding = labourBinding;
    }

    public RichInputText getLabourBinding() {
        return labourBinding;
    }

    public void setTdsAmountBinding(RichInputText tdsAmountBinding) {
        this.tdsAmountBinding = tdsAmountBinding;
    }

    public RichInputText getTdsAmountBinding() {
        return tdsAmountBinding;
    }

    public void setTdsPerBinding(RichInputComboboxListOfValues tdsPerBinding) {
        this.tdsPerBinding = tdsPerBinding;
    }

    public RichInputComboboxListOfValues getTdsPerBinding() {
        return tdsPerBinding;
    }

    public void setRemarksBinding(RichInputText remarksBinding) {
        this.remarksBinding = remarksBinding;
    }

    public RichInputText getRemarksBinding() {
        return remarksBinding;
    }

    public void setCreateButtonBinding(RichButton createButtonBinding) {
        this.createButtonBinding = createButtonBinding;
    }

    public RichButton getCreateButtonBinding() {
        return createButtonBinding;
    }

    public void setDeleteButtonBinding(RichButton deleteButtonBinding) {
        this.deleteButtonBinding = deleteButtonBinding;
    }

    public RichButton getDeleteButtonBinding() {
        return deleteButtonBinding;
    }

    public void setUnitNameBinding(RichInputText unitNameBinding) {
        this.unitNameBinding = unitNameBinding;
    }

    public RichInputText getUnitNameBinding() {
        return unitNameBinding;
    }

    public void setMrnDateBinding(RichInputDate mrnDateBinding) {
        this.mrnDateBinding = mrnDateBinding;
    }

    public RichInputDate getMrnDateBinding() {
        return mrnDateBinding;
    }

    public void setPoDateBinding(RichInputDate poDateBinding) {
        this.poDateBinding = poDateBinding;
    }

    public RichInputDate getPoDateBinding() {
        return poDateBinding;
    }

    public void setPreparedBinding(RichInputComboboxListOfValues preparedBinding) {
        this.preparedBinding = preparedBinding;
    }

    public RichInputComboboxListOfValues getPreparedBinding() {
        return preparedBinding;
    }

    public void setApprovedBinding(RichInputComboboxListOfValues approvedBinding) {
        this.approvedBinding = approvedBinding;
    }

    public RichInputComboboxListOfValues getApprovedBinding() {
        return approvedBinding;
    }

    public void setPreparedNameBinding(RichInputText preparedNameBinding) {
        this.preparedNameBinding = preparedNameBinding;
    }

    public RichInputText getPreparedNameBinding() {
        return preparedNameBinding;
    }

    public void setApprovedNameBinding(RichInputText approvedNameBinding) {
        this.approvedNameBinding = approvedNameBinding;
    }

    public RichInputText getApprovedNameBinding() {
        return approvedNameBinding;
    }

    
    public void cevModeDisableComponent(String mode) {
        
        if(mode.equals("C")){
            getHeaderEditBinding().setDisabled(true);
            getUnitCdBinding().setDisabled(true);
            getUnitNameBinding().setDisabled(true);
            getEntryNoBinding().setDisabled(true);
            getEntryDateBinding().setDisabled(true);
            getPreparedBinding().setDisabled(false);
            getPreparedNameBinding().setDisabled(true);
            getApprovedBinding().setDisabled(true);
            getApprovedNameBinding().setDisabled(true);
            getMrnNoBinding().setDisabled(false);
            getMrnDateBinding().setDisabled(true);
            getMaterialCdBinding().setDisabled(false);
            getGrNoBinding().setDisabled(false);
            getGrDateBinding().setDisabled(false);
            getGrAmountBinding().setDisabled(false);
            getLabourBinding().setDisabled(false);
            getLocalBinding().setDisabled(false);
            getPoDateBinding().setDisabled(true);
            getTdsAmountBinding().setDisabled(false);
            getTdsPerBinding().setDisabled(false);
            getRemarksBinding().setDisabled(false);
            getCreateButtonBinding().setDisabled(false);
            getDeleteButtonBinding().setDisabled(false);
        }
        if(mode.equals("E")){
            getHeaderEditBinding().setDisabled(true);
            getUnitCdBinding().setDisabled(true);
            getUnitNameBinding().setDisabled(true);
            getEntryNoBinding().setDisabled(true);
            getEntryDateBinding().setDisabled(true);
            getPreparedBinding().setDisabled(false);
            getPreparedNameBinding().setDisabled(true);
            getApprovedBinding().setDisabled(false);
            getApprovedNameBinding().setDisabled(true);
            getMrnNoBinding().setDisabled(false);
            getMrnDateBinding().setDisabled(true);
            getMaterialCdBinding().setDisabled(false);
            getGrNoBinding().setDisabled(false);
            getGrDateBinding().setDisabled(false);
            getGrAmountBinding().setDisabled(false);
            getLabourBinding().setDisabled(false);
            getLocalBinding().setDisabled(false);
            getPoDateBinding().setDisabled(true);
            getTdsAmountBinding().setDisabled(false);
            getTdsPerBinding().setDisabled(false);
            getRemarksBinding().setDisabled(false);
            getCreateButtonBinding().setDisabled(false);
            getDeleteButtonBinding().setDisabled(false);
        }
        if(mode.equals("V")){
            getUnitCdBinding().setDisabled(true);
            getUnitNameBinding().setDisabled(true);
            getEntryNoBinding().setDisabled(true);
            getEntryDateBinding().setDisabled(true);
            getPreparedBinding().setDisabled(true);
            getPreparedNameBinding().setDisabled(true);
            getApprovedBinding().setDisabled(true);
            getApprovedNameBinding().setDisabled(true);
            getMrnNoBinding().setDisabled(true);
            getMrnDateBinding().setDisabled(true);
            getMaterialCdBinding().setDisabled(true);
            getGrNoBinding().setDisabled(true);
            getGrDateBinding().setDisabled(true);
            getGrAmountBinding().setDisabled(true);
            getLabourBinding().setDisabled(true);
            getLocalBinding().setDisabled(true);
            getPoDateBinding().setDisabled(true);
            getTdsAmountBinding().setDisabled(true);
            getTdsPerBinding().setDisabled(true);
            getRemarksBinding().setDisabled(true);
            getCreateButtonBinding().setDisabled(true);
            getDeleteButtonBinding().setDisabled(true);
        }
        
    }

    public void tdsPerVL(ValueChangeEvent VCE) {
        VCE.getComponent().processUpdates(FacesContext.getCurrentInstance());
        BigDecimal total_amount = (BigDecimal)totalAmtBinding.getValue();
        BigDecimal tds_Per = (BigDecimal)VCE.getNewValue();
        BigDecimal tds_amount = (BigDecimal)tdsAmountBinding.getValue();
            if(total_amount !=null && tds_Per!=null){
                   tds_amount =total_amount.divide(new BigDecimal(100)).multiply(tds_Per);
                            System.out.println("TDS Amount is: "+tds_amount);
                        tdsAmountBinding.setValue(tds_amount);    
            }
    }

    public void setTotalAmtBinding(RichInputText totalAmtBinding) {
        this.totalAmtBinding = totalAmtBinding;
    }

    public RichInputText getTotalAmtBinding() {
        return totalAmtBinding;
    }
}
