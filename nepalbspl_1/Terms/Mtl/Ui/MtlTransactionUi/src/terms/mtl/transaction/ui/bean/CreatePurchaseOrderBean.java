package terms.mtl.transaction.ui.bean;


import groovy.json.internal.Exceptions;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.math.MathContext;

import java.text.ParseException;

import java.util.Calendar;
import java.util.List;
import java.text.SimpleDateFormat;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;


import oracle.adf.view.rich.component.rich.RichDialog;
import oracle.adf.view.rich.component.rich.data.RichTable;


import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;

import oracle.adf.view.rich.component.rich.input.RichSelectBooleanCheckbox;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.nav.RichCommandLink;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;


import org.apache.myfaces.trinidad.event.SelectionEvent;

;
import javax.faces.validator.ValidatorException;


import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichPopup;

import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.input.RichInputListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;

import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;


import oracle.adf.view.rich.event.ReturnPopupEvent;


import oracle.adf.view.rich.render.ClientEvent;

import oracle.adf.view.rich.util.ResetUtils;

import oracle.jbo.RowSetIterator;
import oracle.jbo.domain.Date;
import oracle.jbo.domain.Timestamp;

import org.apache.myfaces.trinidad.model.UploadedFile;

import prefuse.util.MathLib;

import terms.mtl.transaction.model.view.PurchaseOrderDetailVORowImpl;


public class CreatePurchaseOrderBean {

    private RichSelectOneChoice joPoLOVBind;
    private RichSelectOneChoice poTypeBind;
    private RichSelectOneChoice poType2Bind;
    private RichInputText venAdd1Bind;
    private RichInputText venAdd2Bind;
    private RichInputText venAdd3Bind;
    private RichInputText setFocusWhenEmptyMandatory;
    private RichPopup annexurePopUp;
    private RichPopup vendorAddressPopup;
    private RichTable poDetailTableBind;
    private RichInputListOfValues searchItemBind;
    private RichInputText quantityBinding;
    private RichPopup bindPopUpEight;
    private RichInputDate poDateBinding;
    private RichInputDate validFormDateBinding;
    private RichInputDate validToDateBinding;
    private RichInputComboboxListOfValues itemCodeDtlBinding;
    private RichPopup rateHistoryPopupNineBinding;
    private RichPopup popupThreeBinding;
    private RichPopup getPoCalPopFiveBinding;
    private RichTable payTermsTableBinding;
    private RichPopup confirmationPopupBinding;
    private RichOutputText bindingOutputText;
    private RichPanelGroupLayout getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichButton paytermsPopupBinding;
    private RichButton calculationPopupBinding;
    private RichButton itemDetailPopupBinding;
    private RichButton annexurePopupBinding;
    private RichInputComboboxListOfValues unitCdBinding;
    private RichInputComboboxListOfValues vendorCodeBinding;
    private RichSelectOneChoice postatusBinding;
    private RichInputText amendmentNoBinding;
    private RichInputDate amendmentDateBinding;
    private RichInputDate withEffectFromBinding;
    private RichInputComboboxListOfValues referencePoBindinf;
    private RichInputComboboxListOfValues referencePoBinding;
    private RichSelectOneChoice deliveryTermsBinding;
    private RichInputText amountPerBinding;
    private RichInputText poValueBinding;
    private RichInputDate checkDateBinding;
    private RichInputComboboxListOfValues approvedByBinding;
    private RichInputDate approvedDateBinding;
    private RichInputDate verifiedDateBinding;
    private RichInputComboboxListOfValues checkByBinding;
    private RichInputComboboxListOfValues verifiedByBinding;
    private RichButton detailCreateBinding;
    private RichButton detailDeleteBinding;
    private RichInputComboboxListOfValues prepareByBinding;
    private RichShowDetailItem vendorDetailTabBinding;
    private RichShowDetailItem termsTabBinding;
    private RichShowDetailItem othersTabBinding;
    private RichSelectOneChoice modeOfDispatchBinding;
    private RichSelectOneChoice currencyBinding;
    private RichInputText destinationBinding;
    private RichSelectOneChoice poUnitTypeBinding;
    private RichInputComboboxListOfValues vendorAddressTypeBinding;
    private RichColumn mtlRateBinding;
    private RichButton poCalcCreateBtnBinding;
    private RichButton paymentCreateBtnBinding;
    private RichButton paymentDeleteBtnBinding;
    private RichInputText insAmtBinding;
    private RichSelectOneChoice frieghtByBinding;
    private RichInputComboboxListOfValues transpoetrBinding;
    private RichInputText othersBinding;
    private RichInputText amtBinding;
    private RichInputText periodBinding;
    private RichSelectOneChoice insCodeBinding;
    private RichInputText toleranceBinding;
    private RichInputText remarksBinding;
    private RichInputComboboxListOfValues costCentreBinding;
    private RichInputComboboxListOfValues projectNoBinding;
    private RichInputComboboxListOfValues piosItemBinding;
    private RichInputDate edDateBinding;
    private RichInputDate deliveryDateBinding;
    private RichPopup annPopupBinding;
    private RichInputComboboxListOfValues annTypBinding;
    private RichSelectOneChoice annTypeBind;
    private RichInputText currRateBinding;
    private RichInputText rateBinding;
    private RichInputText poValNewbinding;
    private RichTable annexureTableBinding;
    private RichInputText subBinding;
    private RichInputText unitDocBinding;
    private RichInputText refDocNoBinding;
    private RichInputText docFileNameBinding;
    private RichInputText docSeqNoBinding;
    private RichInputDate refDocDateBinding;
    private RichInputText remDocBinding;
    private RichShowDetailItem referenceDocTabBinding;
    private RichInputText typeBinding;
    private RichInputText pathBinding;
    private RichInputText contentTypeBinding;
    private RichTable docAttachTableBinding;
    private RichSelectBooleanCheckbox closedBinding;
    private RichCommandLink linkBinding;
    private RichPopup cancelPopUpBinding;
    private RichButton cancelButtonBinding;
    private RichInputComboboxListOfValues procSeqBinding;
    String status="F";
    private RichInputDate newdeliveryDateBinding;


    public CreatePurchaseOrderBean() {
        super();
    }

    public void SaveIndentAl(ActionEvent actionEvent) {
        OperationBinding op = ADFUtils.findOperation("getIndentNo");
        Object rst = op.execute();
        //ADFUtils.findOperation("goForUnitRate").execute();
        
        
        
        System.out.println("--------Commit-------");
            System.out.println("value aftr getting result--->?"+rst);
            
        
            if (rst.toString() != null && rst.toString() != "") {
                if (op.getErrors().isEmpty()) {
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message = new FacesMessage("Record Saved Successfully. New Item Code is "+rst+".");   
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                    FacesContext fc = FacesContext.getCurrentInstance();   
                    fc.addMessage(null, Message);  
        
                }
            }
            
            if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ") {
                if (op.getErrors().isEmpty()) {
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message = new FacesMessage("Record Update Successfully.");   
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                    FacesContext fc = FacesContext.getCurrentInstance();   
                    fc.addMessage(null, Message);  
            
                }

            
            }
    }


    public void validteUnitNm(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if(object!=null){
            
                ADFUtils.findOperation("goForUnitRate").execute();    
            
            }
    
    }

    public void ItemVCE(ValueChangeEvent valueChangeEvent) {
       

      
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        ADFUtils.findOperation("goForUnitRate").execute(); 
//        if(valueChangeEvent!= null){
//        
//        if(!valueChangeEvent.getNewValue().equals(valueChangeEvent.getOldValue())){
//            
//            
//            ADFUtils.findOperation("clearSelectedRowOnChangeItemPO").execute();
//            AdfFacesContext.getCurrentInstance().addPartialTarget(poDetailTableBind);
//            
//            
//        }
//        }
       
    }

    public void ProcessVCE(ValueChangeEvent vce) {
        if(vce!=null){
            
           String prc_desc = vce.getNewValue().toString(); 
           System.out.println("what is process desc--->"+prc_desc);
                OperationBinding op = ADFUtils.findOperation("goForUnitRate");
                op.getParamsMap().put("desc", prc_desc);
                Object rst = op.execute(); 
            
            
            
            }
    }

    public void IndQtyVCE(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        ADFUtils.findOperation("goForindntQty").execute(); 
        
    }

    public void ApprvdQtyVCE(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        ADFUtils.findOperation("goForindntQty").execute(); 
    }   
    
    public void PoTypeVL(FacesContext facesContext, UIComponent uIComponent, Object object) {
                        if(object !=null && joPoLOVBind.getValue()!=null){
                            String PoType = object.toString();
                            String JoPoType = joPoLOVBind.getValue().toString();
                            System.out.println("values "+joPoLOVBind.getValue().toString()+","+object.toString());
                            if(JoPoType.equalsIgnoreCase("J") && !PoType.equals("JW") && !PoType.equals("RP") && !PoType.equals("JT")){
                                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Only JobWork / Report & Maintenance Type is Allowed for JobWork Order", null));
                            }
                if(JoPoType.equalsIgnoreCase("P") && PoType.equals("JW")||JoPoType.equalsIgnoreCase("P") && PoType.equals("JT") ||JoPoType.equalsIgnoreCase("P") && PoType.equals("RP")){
                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "This type is not allowed for Purchase Order", null));
                }
            }


    }

    public void JoPoTypeValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {

    }


    public void setJoPoLOVBind(RichSelectOneChoice joPoLOVBind) {
        this.joPoLOVBind = joPoLOVBind;
    }

    public RichSelectOneChoice getJoPoLOVBind() {
        return joPoLOVBind;
    }

    public void PoType2Validator(FacesContext facesContext, UIComponent uIComponent, Object object) {
                if(object !=null && poTypeBind.getValue()!=null){
                    String PoType = poTypeBind.getValue().toString();
                    String PoType2 = object.toString();
                    System.out.println("values "+poTypeBind.getValue().toString()+","+object.toString());
                    if(!PoType2.equalsIgnoreCase("C") && (PoType.equals("IN") || PoType.equals("RP") || PoType.equals("CA"))){
                        throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Only Closed PO can be made against Indent/Capital PO", null));
                    }
                }

    }

    public void setPoTypeBind(RichSelectOneChoice poTypeBind) {
        this.poTypeBind = poTypeBind;
    }

    public RichSelectOneChoice getPoTypeBind() {
        return poTypeBind;
    }

    public void setPoType2Bind(RichSelectOneChoice poType2Bind) {
        this.poType2Bind = poType2Bind;
    }

    public RichSelectOneChoice getPoType2Bind() {
        return poType2Bind;
    }

    public void SaveAL(ActionEvent actionEvent) {
       
    }

    public void VendorNameVCL(ValueChangeEvent vce) {
        if (vce.getNewValue() != null) {
            OperationBinding ob = ADFUtils.findOperation("ApplyVendorAddressesVC");
            ob.getParamsMap().put("VenCd", vce.getNewValue());
        }
    }


    public void AnnexureAL(ActionEvent actionEvent) 
    {
          
       }
    public void setVenAdd1Bind(RichInputText venAdd1Bind) {
        this.venAdd1Bind = venAdd1Bind;
    }

    public RichInputText getVenAdd1Bind() {
        return venAdd1Bind;
    }

    public void setVenAdd2Bind(RichInputText venAdd2Bind) {
        this.venAdd2Bind = venAdd2Bind;
    }

    public RichInputText getVenAdd2Bind() {
        return venAdd2Bind;
    }

    public void setVenAdd3Bind(RichInputText venAdd3Bind) {
        this.venAdd3Bind = venAdd3Bind;
    }

    public RichInputText getVenAdd3Bind() {
        return venAdd3Bind;
    }

    public void setSetFocusWhenEmptyMandatory(RichInputText setFocusWhenEmptyMandatory) {
        this.setFocusWhenEmptyMandatory = setFocusWhenEmptyMandatory;
    }

    public RichInputText getSetFocusWhenEmptyMandatory() {
        return setFocusWhenEmptyMandatory;
    }

    public void PoNoVCL(ValueChangeEvent valueChangeEvent) {
      
    }


    public void SaveActionListener(ActionEvent actionEvent)
    {
        if(status.equalsIgnoreCase("T"))
        {
            ADFUtils.showMessage("Qty must be less than or equals to Indent Qty.", 0);
        }
        else
        {
        OperationBinding op1=ADFUtils.findOperation("CheckPoForTrialVendor");
        Object ob1=op1.execute(); 
        
        if(op1.getResult().toString().equalsIgnoreCase("O")){
      OperationBinding op2=ADFUtils.findOperation("CheckFromDailyCurr");
        op2.getParamsMap().put("CurCd", currencyBinding.getValue());
        Object ob=op2.execute();
        System.out.println("result iss==>>"+op2.getResult());
        
        if(op2.getResult()!=null && op2.getResult().toString().equalsIgnoreCase("Y")){
            
        
        
        
        if((Long)ADFUtils.evaluateEL("#{bindings.MultiPayTermVO1Iterator.estimatedRowCount}")>0){

           System.out.println("Start Call purchaseOrderPaymentTerms method");
           OperationBinding operBind=ADFUtils.findOperation("purchaseOrderPaymentTerms");
           Object obj=operBind.execute();
           System.out.println("Result is ====>  "+operBind.getResult());
           if(operBind.getResult()!=null && operBind.getResult().equals("N"))
           {
               
               FacesMessage Message = new FacesMessage("Payment Term Must Be Defined 100%.");  
               Message.setSeverity(FacesMessage.SEVERITY_ERROR);  
               FacesContext fc = FacesContext.getCurrentInstance();  
               fc.addMessage(null, Message); 
               
           }
        
        
        else
           {
                DCIteratorBinding dcitr=ADFUtils.findIterator("PurchaseOrderDetailVO1Iterator");
               dcitr.executeQuery();
        PurchaseOrderDetailVORowImpl row = (PurchaseOrderDetailVORowImpl) dcitr.getCurrentRow();
               BigDecimal mtr=row.getMaterialRate();
               System.out.println("material rate from row in bean====>>"+mtr);
                    ADFUtils.findOperation("purchaseOrderLprCalculation").execute();
                     
                     OperationBinding op = ADFUtils.findOperation("generatePoNumberPurchaseOrder");
                           Object rst = op.execute();
                           System.out.println("--------Commit-------");
                           System.out.println("value aftr getting result--->?"+rst);
        System.out.println("material rate from row in bean after method call====>>"+mtr);
                               if (rst.toString() != null && rst.toString() != "") {
                                   if (op.getErrors().isEmpty()) {
                                       OperationBinding op20= ADFUtils.findOperation("Commit");
                                       Object rs=op20.execute();
                                                                      
                                       FacesMessage Message = new FacesMessage("Record Saved Successfully. New PO Number is "+rst+".");  
                                       Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                                       FacesContext fc = FacesContext.getCurrentInstance();  
                                       fc.addMessage(null, Message); 
                                   }
                               }

                              
                               if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ") {
                                   if (op.getErrors().isEmpty()) {
                                       ADFUtils.findOperation("Commit").execute();
                                       FacesMessage Message = new FacesMessage("Record Update Successfully.");  
                                       Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                                       FacesContext fc = FacesContext.getCurrentInstance();  
                                       fc.addMessage(null, Message); 
                              
                                   }

                              
                               }
        

    }
}
        else {
            ADFUtils.showMessage("Specify Payment Terms for PO.", 1);
        }
        }
        else{
            FacesMessage Message = new FacesMessage("Daily Currency is  not updated for this PO Date in Currency Master");   
            Message.setSeverity(FacesMessage.SEVERITY_INFO);   
            FacesContext fc = FacesContext.getCurrentInstance();   
            fc.addMessage(null, Message); 
        }
        }
        else{
            FacesMessage Message = new FacesMessage("Trial PO making of this Vendor has reached to its maximum limit.");   
            Message.setSeverity(FacesMessage.SEVERITY_INFO);   
            FacesContext fc = FacesContext.getCurrentInstance();   
            fc.addMessage(null, Message); 
        }
    }
    }
    public void ItemDetailsAL(ActionEvent actionEvent) {
       // if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C"))
            ADFUtils.findOperation("CreateInsertDetail").execute();
    }

    public void VendorReturn(ReturnPopupEvent returnPopupEvent) {
        //Disabled  By Vikas Deep 04/04/2018
//                RichPopup.PopupHints hints = new RichPopup.PopupHints();
//                          getVendorAddressPopup().show(hints);

    }

    public void setAnnexurePopUp(RichPopup annexurePopUp) {
        this.annexurePopUp = annexurePopUp;
    }

    public RichPopup getAnnexurePopUp() {
        return annexurePopUp;
    }

    public void setVendorAddressPopup(RichPopup vendorAddressPopup) {
        this.vendorAddressPopup = vendorAddressPopup;
    }

    public RichPopup getVendorAddressPopup() {
        return vendorAddressPopup;
    }

    public void VendorAddressSL(SelectionEvent se) {
        ADFUtils.invokeEL("#{bindings.PurchaseOrderHeaderVO1.collectionModel.makeCurrent}", new Class[] {SelectionEvent.class},
                                 new Object[] { se });
        Row selectedRow =
                    (Row)ADFUtils.evaluateEL("#{bindings.PurchaseOrderHeaderVO1Iterator.currentRow}"); 
//        OperationBinding ob=ADFUtils.findOperation("filterselectedAddress");
        System.out.println("selected address is "+selectedRow.getAttribute("Address1"));
//        ob.getParamsMap().put("SelectedAddress", selectedRow.getAttribute("Address1"));
//       RichPopup.PopupHints hints = new RichPopup.PopupHints();
       
                        getVendorAddressPopup().cancel();
       
    }

    public void HandleVendorTableDblClick(ClientEvent clientEvent) {

        
        getVendorAddressPopup().cancel();
    }



    public void PopulatePOAL(ActionEvent actionEvent) 
    {
        if(referencePoBinding.getValue() !=null)
        {
        ADFUtils.findOperation("PopulatePoDetail").execute();
            System.out.println("puchaseOrderCalculate Calling on item detail dialog listener ok outcome");
            ADFUtils.findOperation("puchaseOrderCalculate").execute();
            System.out.println("End puchaseOrderCalculate");
            ADFUtils.findOperation("purchaseOrderLprCalculation").execute();
        }
        else
        {
         ADFUtils.showMessage("Please select Reference PO for data populate.", 3);
         }
    }

    public void setPoDetailTableBind(RichTable poDetailTableBind) {
        this.poDetailTableBind = poDetailTableBind;
    }

    public RichTable getPoDetailTableBind() {
        return poDetailTableBind;
    }

    public void PoDetailSL(SelectionEvent selectionEvent) {
        
        ADFUtils.invokeEL("#{bindings.PurchaseOrderDetailVO1.collectionModel.makeCurrent}", new Class[] {SelectionEvent.class},
                                 new Object[] { selectionEvent });
        Row selectedRow = (Row) ADFUtils.evaluateEL("#{bindings.PurchaseOrderDetailVO1Iterator.currentRow}");
        System.out.println("selected row "+selectedRow.getAttribute("PiosItemDesc"));
        
    }

    public void setSearchItemBind(RichInputListOfValues searchItemBind) {
        this.searchItemBind = searchItemBind;
    }

    public RichInputListOfValues getSearchItemBind() {
        return searchItemBind;
    }

    public void ItemSearchAL(ActionEvent actionEvent) {
    
    }

    public void IndentNoValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if(object == null && (getPoTypeBind().equals("IN") || getPoTypeBind().equals("CA"))){
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Indent No. is must for Capital/Indent PO", null));
        }

    }

    public void materialRateVCL(ValueChangeEvent vce) 
    {
        
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        System.out.println("Order Type====>"+poType2Bind.getValue());
        if(vce.getNewValue()!=null && poType2Bind.getValue().equals("C"))
        {
            ADFUtils.findOperation("purchaseOrderGstCalculation").execute();
            ADFUtils.findOperation("purchaseOrderDisocuntPercentage").execute();
        }
//        else if(vce.getNewValue()!=null && poType2Bind.getValue().equals("O"))
//        {
//            ADFUtils.findOperation("openOrderTypePoCalculation").execute();
//        }
        
    }



    public void itemCodeSpecificationVCL(ValueChangeEvent valueChangeEvent)
    {
    System.out.println("Item Code Specification Start ");
    valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(valueChangeEvent!= null){
        
        if(!valueChangeEvent.getNewValue().equals(valueChangeEvent.getOldValue())){
            
            
            ADFUtils.findOperation("clearSelectedRowOnChangeItemPO").execute();
            AdfFacesContext.getCurrentInstance().addPartialTarget(poDetailTableBind);
            
            
        }
        }
    
    ADFUtils.findOperation("purchaseOrderDetailItemSpecification").execute();
    System.out.println("Item Code Specification End ");
    System.out.println("####Start");
//        ADFUtils.findOperation("getQtyPoDetail").execute();  
    ADFUtils.findOperation("purchaseOrderDetailMaterialRate").execute();  
    System.out.println("####End");
        ADFUtils.findOperation("purchaseOrderGstCalculation").execute();
    
    OperationBinding opp=ADFUtils.findOperation("LecoQtyPurchaseOrderDetail");
    Object obj=opp.execute();
    if(opp.getResult()!=null && opp.getResult()!="Y")
    {
        System.out.println("Value is ====> "+opp.getResult());
        if (opp.getErrors().isEmpty()) 
        {

            FacesMessage Message = new FacesMessage("For Item "+opp.getResult().toString());  
            Message.setSeverity(FacesMessage.SEVERITY_INFO);  
            FacesContext fc = FacesContext.getCurrentInstance();  
            fc.addMessage(null, Message); 
            //throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERORORROROROROROOR.", null));
        
        }

    }
    
        AdfFacesContext.getCurrentInstance().addPartialTarget(poDetailTableBind);

    
    }


    public void itemCodeValidator(FacesContext facesContext, UIComponent uIComponent, Object object) 
    {
         System.out.println("Start Validator");     
        if(object!=null){
        OperationBinding opp=ADFUtils.findOperation("purchaseOrderPoTypeCheckForVendor");
        Object obj=opp.execute();
        System.out.println("Get Result is====> "+opp.getResult());
        if(opp.getResult()!=null && opp.getResult().equals("N"))
        {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Valid PO Is Already Available For This Vendor And Item", null));
 
        }
        }
    System.out.println("End Validator");
    }




public void deletePopupDialogPoDetailDL(DialogEvent dialogEvent) 
{
    if(dialogEvent.getOutcome().name().equals("ok"))
                {
                ADFUtils.findOperation("Delete").execute();
                //ADFUtils.findOperation("Commit").execute();    //Unable on Search page.
                System.out.println("Record Delete Successfully");
               
                    FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                    FacesContext fc = FacesContext.getCurrentInstance();  
                    fc.addMessage(null, Message);  
               
                }

          AdfFacesContext.getCurrentInstance().addPartialTarget(poDetailTableBind);
}





public void uomValidator(FacesContext facesContext, UIComponent uIComponent, Object object)
{  
OperationBinding uomOpp=ADFUtils.findOperation("purchaseOrderUom");
Object uomObj=uomOpp.execute();
if(object!=null)
{

System.out.println("UOM ITEM=> "+uomOpp.getResult()+" UOM Form=> "+object.toString());
        if(uomOpp.getResult()!=null && uomOpp.getResult().toString().equalsIgnoreCase("ERROR"))
        {
         System.out.println("ERROR########");
         throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "This UOM must be same as defined in Master.", null));

        }
        else
        {
            System.out.println("Else UOM====> "+uomOpp.getResult()+" uom "+object.toString());
    
         }

  }

}




    public void setQuantityBinding(RichInputText quantityBinding) {
        this.quantityBinding = quantityBinding;
    }

    public RichInputText getQuantityBinding() {
        return quantityBinding;
    }
    
    

public void poTypeTwoVCL(ValueChangeEvent vce) 
{
    vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
    if(vce!=null)
    {
    System.out.println("Start setQuantityBasedOnPoTypeTwo");
//    ADFUtils.findOperation("setQuantityBasedOnPoTypeTwo").execute();
    System.out.println("End setQuantityBasedOnPoTypeTwo");

    }
//        if(valueChangeEvent.getNewValue().equals("O"))
//        {
//            System.out.println("valueChangeEvent====> "+valueChangeEvent.getNewValue()+"    quantityBinding====>"+quantityBinding.getValue());
//        if(quantityBinding.getValue()!=null)
//        {
//            System.out.println("set Value Null");
//            quantityBinding.setValue("");
//            System.out.println("####set Value Null Done");
//
//            
//         }
//        
//        }
}

    public void uomVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
    }


    public void quantityVCE(ValueChangeEvent vce) {
//        System.out.println("Inside Quantity VCE for process sequence validator");
//        System.out.println("Value of PO Type: "+joPoLOVBind.getValue());
//          if(joPoLOVBind.getValue().equals("J"))
//          {
//              System.out.println("Inside If of PO type");
//              BigDecimal ps=new BigDecimal(0);
//              BigDecimal ps1=(BigDecimal)procSeqBinding.getValue();
//              System.out.println("Value of Process Sequence: "+procSeqBinding.getValue());
//              if(ps1.compareTo(ps)==-1 || ps1.compareTo(ps)==0)
//              {
//                  System.out.println("Inside IF after compare!");
//                      FacesMessage message = new FacesMessage("Process Sequence is required and should be greater than zero.");
//                                  message.setSeverity(FacesMessage.SEVERITY_ERROR);
//                                  FacesContext context = FacesContext.getCurrentInstance();
//                                  context.addMessage(procSeqBinding.getClientId(), message);
//                  }
//              }
        
       System.out.println("enter in qty bean*************");
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        ADFUtils.findOperation("purchaseOrderGstCalculation").execute();
        ADFUtils.findOperation("purchaseOrderDetailItemSpecification").execute();
        OperationBinding binding = ADFUtils.findOperation("validateQtyPoDtl");
        //   binding.getParamsMap().put("gateEntry", vce.getNewValue().toString());
        Object opp=binding.execute();
        
        //  if(binding.getResult().equals("N"))
        //   {
        if(binding.getResult()!=null && binding.getResult().equals("Y")){
            status="T";
                FacesMessage message = new FacesMessage("Qty must be less than or equals to Indent Qty.");
                            message.setSeverity(FacesMessage.SEVERITY_ERROR);
                            FacesContext context = FacesContext.getCurrentInstance();
                            context.addMessage(quantityBinding.getClientId(), message);
        }else
        {
            status="F";
        }
        ADFUtils.findOperation("purchaseOrderDisocuntPercentage").execute();
    }


//Validator on material Rate.
    public void materialRateValidator(FacesContext facesContext, UIComponent uIComponent, Object object) 
    {
        System.out.println("Material rate Validator");
        if(object!=null)
        {
            System.out.println("Jo Po Value====> "+joPoLOVBind.getValue()+" Po Type====> "+poTypeBind.getValue());
            if(joPoLOVBind.getValue().toString().equalsIgnoreCase("P") && !poTypeBind.getValue().equals("RP"))// && !poTypeBind.getValue().equals("RP") && materialRateBinding.getValue().equals(0)) 
            {
                System.out.println("Inside Jo Po Value====> "+joPoLOVBind.getValue()+" Po Type====> "+poTypeBind.getValue());
                BigDecimal objVal=(BigDecimal)object;
                if(objVal.compareTo(new BigDecimal(0))==-1 && piosItemBinding.getValue()!=null)//(itemCodeDtlBinding.getValue()!=null || itemCodeDtlBinding.getValue()!=""))
                {
                System.out.println("Jo Po Value====> "+joPoLOVBind.getValue()+" Po Type====> "+poTypeBind.getValue());
                System.out.println("inside validator"+piosItemBinding.getValue().toString());
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Material rate is mandatory in case of Purchase order.", null)); 
                }

            }
           
           
        }
    }

//Set Discount Amount Value
    
    public void discountPercentgeVCE(ValueChangeEvent vce)
    {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
       if(vce!=null)
       {
        System.out.println("  vce Before Call Discount Percent");
        ADFUtils.findOperation("purchaseOrderDisocuntPercentage").execute();
        System.out.println("vce After Call Discount Percent");
       }
    }

    public void gstTransientVCE(ValueChangeEvent vce) 
    {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());

    if(vce!=null)
    {
        System.out.println("####gstTransientVCE####");
            ADFUtils.findOperation("purchaseOrderGstLov").execute();   
         System.out.println("=========gstTransientVCE=========");

     }
    
    }

public void stateHsnLovVCE(ValueChangeEvent vce) {
    vce.getComponent().processUpdates(FacesContext.getCurrentInstance());

    if(vce!=null)
    {
        System.out.println("####StateHsnCode####");
            ADFUtils.findOperation("purchaseOrderGstLov").execute();   
         System.out.println("=========StateHsnCode=========");

     }
}

public void rejectionAllowPercentageVCE(ValueChangeEvent vce) 
{
    vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
    if(vce!=null)
    {
    System.out.println("##rejectionAllowPercentageVCE");            
    ADFUtils.findOperation("purchaseOrderRejectionAllowPercentage").execute();  
    System.out.println("#rejectionAllowPercentageVCE");
     }

}

    public void dtlButtonAL(ActionEvent actionEvent)
    {
        ADFUtils.findOperation("purchaseOrderInputVVOFilterd").execute();
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
                  getBindPopUpEight().show(hints);
                  

                  
                  

    }

    public void setBindPopUpEight(RichPopup bindPopUpEight) {
        this.bindPopUpEight = bindPopUpEight;
    }

    public RichPopup getBindPopUpEight() {
        return bindPopUpEight;
    }


    public void endDateValidator(FacesContext facesContext, UIComponent uIComponent, Object object) 
    {
    
       if(object != null)
         {
        System.out.println("End Date==> "+object+"\n Po Date=> "+poDateBinding.getValue()+"\n Valid From Date=> "+validFormDateBinding.getValue()+" \n Valid To Date=> "+validToDateBinding.getValue());
        
            System.out.println("PODATE");
            Date PoDate=(Date)poDateBinding.getValue();
            System.out.println("ValidFrom");
            
            Date ValidFrom=(Date)validFormDateBinding.getValue();
            System.out.println("ValidTo");
            
            Date ValidTo=(Date)validToDateBinding.getValue();
            System.out.println("EndDate");
            Date EndDate=(Date)object;
            
            
              System.out.println("Po Date=> "+PoDate+"\n Valid From=> "+ValidFrom+"\n Valid To=> "+ValidTo+" \n End Date=> "+EndDate);
             if(EndDate.compareTo(PoDate)==-1)
             {
               System.out.println("Validation One");
               throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Expected date can`t be less than PO date.", null));

             }
             
             if(EndDate.compareTo(ValidFrom)==-1)
             {
                 System.out.println("Validation Two");
                 throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Expected date can`t be less than Valid From Date.", null));
             }
             
             if(EndDate.compareTo(ValidTo)==1)
             {
                 System.out.println("Validation Three");
                 throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Expected date can`t be more than Valid To Date.", null));
             }
             
             
           
         } 

    }

    public void setPoDateBinding(RichInputDate poDateBinding) {
        this.poDateBinding = poDateBinding;
    }

    public RichInputDate getPoDateBinding() {
        return poDateBinding;
    }

    public void setValidFormDateBinding(RichInputDate validFormDateBinding) {
        this.validFormDateBinding = validFormDateBinding;
    }

    public RichInputDate getValidFormDateBinding() {
        
        try {
                    System.out.println("set getEntryDateBinding" + ADFUtils.getTodayDate());
                    String date = ADFUtils.convertAdToBs(ADFUtils.getTodayDate().toString());
                    System.out.println("setter getvaluedate" + date);
                    //            reqDateNepalBinding.setValue((String)date);
                    DCIteratorBinding dci = ADFUtils.findIterator("PurchaseOrderHeaderVO1Iterator");
                    dci.getCurrentRow().setAttribute("ValidFrNepal", date);
                    //            Row row=(Row)ADFUtils.evaluateEL("#{bindings.ContractRequisitionHeaderVO1Iterator.currentRow}");
                    //            row.setAttribute("ReqDateNp", date);
                } catch (ParseException e) {
                } 
        return validFormDateBinding;
    }

    public void setValidToDateBinding(RichInputDate validToDateBinding) {
        this.validToDateBinding = validToDateBinding;
    }

    public RichInputDate getValidToDateBinding() {
        try {
                    System.out.println("set getEntryDateBinding" + ADFUtils.getTodayDate());
                    String date = ADFUtils.convertAdToBs(ADFUtils.getTodayDate().toString());
                    System.out.println("setter getvaluedate" + date);
                    //            reqDateNepalBinding.setValue((String)date);
                    DCIteratorBinding dci = ADFUtils.findIterator("PurchaseOrderHeaderVO1Iterator");
                    dci.getCurrentRow().setAttribute("ValidToNepal", date);
                    //            Row row=(Row)ADFUtils.evaluateEL("#{bindings.ContractRequisitionHeaderVO1Iterator.currentRow}");
                    //            row.setAttribute("ReqDateNp", date);
                } catch (ParseException e) {
                } 
        return validToDateBinding;
    }


    public void setItemCodeDtlBinding(RichInputComboboxListOfValues itemCodeDtlBinding) {
        this.itemCodeDtlBinding = itemCodeDtlBinding;
    }

    public RichInputComboboxListOfValues getItemCodeDtlBinding() {
        return itemCodeDtlBinding;
    }


    public void poCalculateAL(ActionEvent actionEvent) 
    {
        System.out.println("puchaseOrderCalculate Calling");
        ADFUtils.findOperation("puchaseOrderCalculate").execute();
        System.out.println("End puchaseOrderCalculate");
        ADFUtils.findOperation("purchaseOrderLprCalculation").execute();
        RichPopup.PopupHints hints1 = new RichPopup.PopupHints();
        getGetPoCalPopFiveBinding().show(hints1);
    }

    public void recWeightVCE(ValueChangeEvent vce) 
    {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        System.out.println("Start Calling purchaseOrderGstCalculation in Rec Weight");
        ADFUtils.findOperation("purchaseOrderGstCalculation").execute();
        System.out.println("End Calling purchaseOrderGstCalculation in Rec Weight");
    }

    public void setRateHistoryPopupNineBinding(RichPopup rateHistoryPopupNineBinding) {
        this.rateHistoryPopupNineBinding = rateHistoryPopupNineBinding;
    }

    public RichPopup getRateHistoryPopupNineBinding() {
        return rateHistoryPopupNineBinding;
    }

    public void rateHistoryButtonAL(ActionEvent actionEvent)
    {
            System.out.println("Rate History Popup Start");
            ADFUtils.findOperation("purchaseOrderRateHistoryPopupFiltered").execute();
            RichPopup.PopupHints hints=new RichPopup.PopupHints();
            getRateHistoryPopupNineBinding().show(hints);
            System.out.println("Rate History Popup End");

    }

    public void setPopupThreeBinding(RichPopup popupThreeBinding) {
        this.popupThreeBinding = popupThreeBinding;
    }

    public RichPopup getPopupThreeBinding() {
        return popupThreeBinding;
    }

    public void itemDetailButtonAL(ActionEvent actionEvent)
    {
        OperationBinding op1=ADFUtils.findOperation("CheckPoForTrialVendor");
        Object ob1=op1.execute(); 
        
        if(op1.getResult().toString().equalsIgnoreCase("O")){
        System.out.println("Popup Three puchaseOrderCalculate Calling");
        ADFUtils.findOperation("puchaseOrderCalculate").execute();
        System.out.println("Popup Three End puchaseOrderCalculate");
        OperationBinding op=ADFUtils.findOperation("CheckFromDailyCurr");
        op.getParamsMap().put("CurCd", currencyBinding.getValue());
        Object ob=op.execute();
        System.out.println("result iss==>>"+op.getResult());
        
        if(op.getResult()!=null && op.getResult().toString().equalsIgnoreCase("N")){
            System.out.println("inside if when result is ===>> not IM");
            
                FacesMessage Message = new FacesMessage("Daily Currency is  not updated for this PO Date in Currency Master");   
                Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                FacesContext fc = FacesContext.getCurrentInstance();   
                fc.addMessage(null, Message); 
        }
        else{
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        getPopupThreeBinding().show(hints);
            }
        }
        else{
            
            FacesMessage Message = new FacesMessage("Trial PO making of this Vendor has reached to its maximum limit.");   
            Message.setSeverity(FacesMessage.SEVERITY_INFO);   
            FacesContext fc = FacesContext.getCurrentInstance();   
            fc.addMessage(null, Message); 
        }
    }

    public void percentagePayTermVCE(ValueChangeEvent vce)
    {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
//        if(vce!=null)
//        {
           System.out.println("Object Value====>  "+vce.toString());
           System.out.println("Start Call purchaseOrderPaymentTerms method");
           OperationBinding operBind=ADFUtils.findOperation("purchaseOrderPaymentTerms");
           Object obj=operBind.execute();
           System.out.println("Result is ====>  "+operBind.getResult());
           if(operBind.getResult()!=null && operBind.getResult().equals("N"))
           {
//                FacesMessage Message = new FacesMessage("Payment Term Must Be Defined 100%.");  
//               Message.setSeverity(FacesMessage.SEVERITY_ERROR);  
//               FacesContext fc = FacesContext.getCurrentInstance();  
//               fc.addMessage(null, Message); 
               
           }
            System.out.println("End Calling purchaseOrderPaymentTerms");
//        }
    }


    public String saveAndCloseButtonAL() 
    {   
        if(status.equalsIgnoreCase("T"))
        {
            ADFUtils.showMessage("Qty must be less than or equals to Indent Qty.", 0);
        }
        else
        {
        OperationBinding op1=ADFUtils.findOperation("CheckPoForTrialVendor");
        Object ob1=op1.execute(); 
        
        if(op1.getResult().toString().equalsIgnoreCase("O")){
        OperationBinding op2=ADFUtils.findOperation("CheckFromDailyCurr");
        op2.getParamsMap().put("CurCd", currencyBinding.getValue());
        Object ob=op2.execute();
        System.out.println("result iss==>>"+op2.getResult());
        
        if(op2.getResult()!=null && op2.getResult().toString().equalsIgnoreCase("Y")){
            if((Long)ADFUtils.evaluateEL("#{bindings.MultiPayTermVO1Iterator.estimatedRowCount}")>0){
       System.out.println("Start Call purchaseOrderPaymentTerms method");
        OperationBinding operBind=ADFUtils.findOperation("purchaseOrderPaymentTerms");
        Object obj=operBind.execute();
        System.out.println("Result is ====>  "+operBind.getResult());
        if(operBind.getResult()!=null && operBind.getResult().equals("N"))
        {
            FacesMessage Message = new FacesMessage("Payment Term Must Be Defined 100%.");  
            Message.setSeverity(FacesMessage.SEVERITY_ERROR);  
            FacesContext fc = FacesContext.getCurrentInstance();  
            fc.addMessage(null, Message); 
            
        }
        
        
        else
        {
        
            ADFUtils.findOperation("purchaseOrderLprCalculation").execute();

             OperationBinding op = ADFUtils.findOperation("generatePoNumberPurchaseOrder");
                        Object rst = op.execute();
                        System.out.println("--------Commit-------");
                        System.out.println("value aftr getting result--->?"+rst);
                       
                            if (rst.toString() != null && rst.toString() != "") {
                                if (op.getErrors().isEmpty()) {
                                    OperationBinding op12= ADFUtils.findOperation("Commit");
                                    Object rs=op12.execute();
                                                                   
                                    FacesMessage Message = new FacesMessage("Record Saved Successfully. New PO Number is "+rst+".");  
                                    Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                                    FacesContext fc = FacesContext.getCurrentInstance();  
                                    fc.addMessage(null, Message); 
                                    return "SaveAndClose";
                                }
                            }

                           
                            if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ") {
                                if (op.getErrors().isEmpty()) {
                                    ADFUtils.findOperation("Commit").execute();
                                    FacesMessage Message = new FacesMessage("Record Update Successfully.");  
                                    Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                                    FacesContext fc = FacesContext.getCurrentInstance();  
                                    fc.addMessage(null, Message);
                                    return "SaveAndClose";
                           
                                }

                           
                            }
        

        }
            }
            else{
                ADFUtils.showMessage("Specify Payment Terms for PO.", 1);
                
            }
        }
        else{
            FacesMessage Message = new FacesMessage("Daily Currency is  not updated for this PO Date in Currency Master");   
            Message.setSeverity(FacesMessage.SEVERITY_INFO);   
            FacesContext fc = FacesContext.getCurrentInstance();   
            fc.addMessage(null, Message); 
        }
        }
        else{
            FacesMessage Message = new FacesMessage("Trial PO making of this Vendor has reached to its maximum limit.");   
            Message.setSeverity(FacesMessage.SEVERITY_INFO);   
            FacesContext fc = FacesContext.getCurrentInstance();   
            fc.addMessage(null, Message); 
        }
        }
        return null;
    }

    public void setGetPoCalPopFiveBinding(RichPopup getPoCalPopFiveBinding) {
        this.getPoCalPopFiveBinding = getPoCalPopFiveBinding;
    }

    public RichPopup getGetPoCalPopFiveBinding() {
        return getPoCalPopFiveBinding;
    }


    public void onPageLoad() 
    {
        ADFUtils.findOperation("HeaderCreateInsert").execute();
       // ADFUtils.findOperation("AnnexureCreateInsert").execute();
    }

    public void checkByVCE(ValueChangeEvent vce) 
    {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        ADFUtils.findOperation("purchaseOrderSetValuesOfDate").execute();
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce.getNewValue()!=null)
        {
            System.out.println("insdie==== vceeeeeeeeeeeeeeeeeee");
            BigDecimal amount=new BigDecimal(0);
            BigDecimal value=new BigDecimal(0);
            DCIteratorBinding dci = ADFUtils.findIterator("PurchaseOrderDetailVO1Iterator");
            RowSetIterator rsi=dci.getViewObject().createRowSetIterator(null);
                while(rsi.hasNext()) {
                    System.out.println("inside rsi");
                    Row r = rsi.next();
                    BigDecimal qty=new BigDecimal(0);
                    BigDecimal rate=new BigDecimal(0);
                     qty=(BigDecimal) r.getAttribute("Qty");
                     rate=(BigDecimal) r.getAttribute("MaterialRate");
                    System.out.println("qty is==>>"+qty+"rate==>>"+rate);
                    if(qty!=null && rate!=null){
                        amount=qty.multiply(rate);
                        System.out.println("AMount==>>"+amount);
                        value=value.add(amount);
                    }
                   
                }
                rsi.closeRowSetIterator();
                System.out.println("Value is==>>"+value);
        //            System.out.println("Total SUM========>"+ADFUtils.resolveExpression("#{bindings.PoValue.inputValue}"));
        System.out.println("IN VCE Approved");
        OperationBinding ob = ADFUtils.findOperation("checkApprovalFunctionStatus");
        System.out.println("Inde t type==>"+poTypeBind.getValue());
        if(poTypeBind.getValue().equals("IN")){
            System.out.println("Indsdie iiiiiiifffffffffff IN");
        ob.getParamsMap().put("formNm", "PO");
        ob.getParamsMap().put("authoLim", "CH");
        ob.getParamsMap().put("UnitCd", unitCdBinding.getValue());
        //        System.out.println("Total SUM========>"+ADFUtils.resolveExpression("#{bindings.TotalAmtTrans.inputValue}"));
        ob.getParamsMap().put("Amount",value);
        ob.getParamsMap().put("empcd", vce.getNewValue().toString());
        ob.execute();
        System.out.println(" EMP CD"+vce.getNewValue().toString());
        if((ob.getResult() !=null && !ob.getResult().equals("Y")))
        {
           Row row=(Row)ADFUtils.evaluateEL("#{bindings.PurchaseOrderHeaderVO1Iterator.currentRow}");
           row.setAttribute("ChckBy", null);
           ADFUtils.showMessage(ob.getResult().toString(), 0);
        }
        }
        else if(poTypeBind.getValue().equals("HO")){
            System.out.println("Indsdie ELSE HO");
            ob.getParamsMap().put("formNm", "PO_HO");
            ob.getParamsMap().put("authoLim", "CH");
            ob.getParamsMap().put("UnitCd", unitCdBinding.getValue());
            //        System.out.println("Total SUM========>"+ADFUtils.resolveExpression("#{bindings.TotalAmtTrans.inputValue}"));
            ob.getParamsMap().put("Amount", value);
            ob.getParamsMap().put("empcd", vce.getNewValue().toString());
            ob.execute();
            System.out.println(" EMP CD"+vce.getNewValue().toString());
            if((ob.getResult() !=null && !ob.getResult().equals("Y")))
            {
            Row row=(Row)ADFUtils.evaluateEL("#{bindings.PurchaseOrderHeaderVO1Iterator.currentRow}");
            row.setAttribute("ChckBy", null);
            ADFUtils.showMessage(ob.getResult().toString(), 0);
            }
        }
        else if(poTypeBind.getValue().equals("IM")){
            System.out.println("iNDSEDI ELSE==>> IM");
            ob.getParamsMap().put("formNm", "PO_IMP");
            ob.getParamsMap().put("authoLim", "CH");
            ob.getParamsMap().put("UnitCd", unitCdBinding.getValue());
            //        System.out.println("Total SUM========>"+ADFUtils.resolveExpression("#{bindings.TotalAmtTrans.inputValue}"));
            ob.getParamsMap().put("Amount", value);
            ob.getParamsMap().put("empcd", vce.getNewValue().toString());
            ob.execute();
            System.out.println(" EMP CD"+vce.getNewValue().toString());
            if((ob.getResult() !=null && !ob.getResult().equals("Y")))
            {
               Row row=(Row)ADFUtils.evaluateEL("#{bindings.PurchaseOrderHeaderVO1Iterator.currentRow}");
               row.setAttribute("ChckBy", null);
               ADFUtils.showMessage(ob.getResult().toString(), 0);
            } 
        }
        else if(poTypeBind.getValue().equals("ST")){
            System.out.println("iNDSEDI ELSE==>> ST");
            ob.getParamsMap().put("formNm", "PO_STOCK");
            ob.getParamsMap().put("authoLim", "CH");
            ob.getParamsMap().put("UnitCd", unitCdBinding.getValue());
            //        System.out.println("Total SUM========>"+ADFUtils.resolveExpression("#{bindings.TotalAmtTrans.inputValue}"));
            ob.getParamsMap().put("Amount", value);
            ob.getParamsMap().put("empcd", vce.getNewValue().toString());
            ob.execute();
            System.out.println(" EMP CD"+vce.getNewValue().toString());
            if((ob.getResult() !=null && !ob.getResult().equals("Y")))
            {
               Row row=(Row)ADFUtils.evaluateEL("#{bindings.PurchaseOrderHeaderVO1Iterator.currentRow}");
               row.setAttribute("ChckBy", null);
               ADFUtils.showMessage(ob.getResult().toString(), 0);
            }  
        }
        else if(poTypeBind.getValue().equals("CA")){
            System.out.println("iNDSEDI ELSE==>> CA");
            ob.getParamsMap().put("formNm", "PO_CAP");
            ob.getParamsMap().put("authoLim", "CH");
            ob.getParamsMap().put("UnitCd", unitCdBinding.getValue());
            //        System.out.println("Total SUM========>"+ADFUtils.resolveExpression("#{bindings.TotalAmtTrans.inputValue}"));
            ob.getParamsMap().put("Amount",value);
            ob.getParamsMap().put("empcd", vce.getNewValue().toString());
            ob.execute();
            System.out.println(" EMP CD"+vce.getNewValue().toString());
            if((ob.getResult() !=null && !ob.getResult().equals("Y")))
            {
               Row row=(Row)ADFUtils.evaluateEL("#{bindings.PurchaseOrderHeaderVO1Iterator.currentRow}");
               row.setAttribute("ChckBy", null);
               ADFUtils.showMessage(ob.getResult().toString(), 0);
            } 
        }
        }
    }

    public void verifiedByVCE(ValueChangeEvent vce)
    {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        ADFUtils.findOperation("purchaseOrderSetValuesOfDate").execute();
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce.getNewValue()!=null)
        {
            System.out.println("insdie==== vceeeeeeeeeeeeeeeeeee");
            BigDecimal amount=new BigDecimal(0);
            BigDecimal value=new BigDecimal(0);
            DCIteratorBinding dci = ADFUtils.findIterator("PurchaseOrderDetailVO1Iterator");
            RowSetIterator rsi=dci.getViewObject().createRowSetIterator(null);
                while(rsi.hasNext()) {
                    System.out.println("inside rsi");
                    Row r = rsi.next();
                    BigDecimal qty=new BigDecimal(0);
                    BigDecimal rate=new BigDecimal(0);
                     qty=(BigDecimal) r.getAttribute("Qty");
                     rate=(BigDecimal) r.getAttribute("MaterialRate");
                    System.out.println("qty is==>>"+qty+"rate==>>"+rate);
                    if(qty!=null && rate!=null){
                        amount=qty.multiply(rate);
                        System.out.println("AMount==>>"+amount);
                        value=value.add(amount);
                    }
                   
                }
                rsi.closeRowSetIterator();
                System.out.println("Value is==>>"+value);
        //            System.out.println("Total SUM========>"+ADFUtils.resolveExpression("#{bindings.PoValue.inputValue}"));
        System.out.println("IN VCE Approved");
        OperationBinding ob = ADFUtils.findOperation("checkApprovalFunctionStatus");
        System.out.println("Inde t type==>"+poTypeBind.getValue());
        if(poTypeBind.getValue().equals("IN")){
            System.out.println("Indsdie iiiiiiifffffffffff IN");
        ob.getParamsMap().put("formNm", "PO");
        ob.getParamsMap().put("authoLim", "VR");
        ob.getParamsMap().put("UnitCd", unitCdBinding.getValue());
        //        System.out.println("Total SUM========>"+ADFUtils.resolveExpression("#{bindings.TotalAmtTrans.inputValue}"));
        ob.getParamsMap().put("Amount",value);
        ob.getParamsMap().put("empcd", vce.getNewValue().toString());
        ob.execute();
        System.out.println(" EMP CD"+vce.getNewValue().toString());
        if((ob.getResult() !=null && !ob.getResult().equals("Y")))
        {
           Row row=(Row)ADFUtils.evaluateEL("#{bindings.PurchaseOrderHeaderVO1Iterator.currentRow}");
           row.setAttribute("ApprBy", null);
           ADFUtils.showMessage(ob.getResult().toString(), 0);
        }
        }
        else if(poTypeBind.getValue().equals("HO")){
            System.out.println("Indsdie ELSE HO");
            ob.getParamsMap().put("formNm", "PO_HO");
            ob.getParamsMap().put("authoLim", "VR");
            ob.getParamsMap().put("UnitCd", unitCdBinding.getValue());
            //        System.out.println("Total SUM========>"+ADFUtils.resolveExpression("#{bindings.TotalAmtTrans.inputValue}"));
            ob.getParamsMap().put("Amount", value);
            ob.getParamsMap().put("empcd", vce.getNewValue().toString());
            ob.execute();
            System.out.println(" EMP CD"+vce.getNewValue().toString());
            if((ob.getResult() !=null && !ob.getResult().equals("Y")))
            {
            Row row=(Row)ADFUtils.evaluateEL("#{bindings.PurchaseOrderHeaderVO1Iterator.currentRow}");
            row.setAttribute("ApprBy", null);
            ADFUtils.showMessage(ob.getResult().toString(), 0);
            }
        }
        else if(poTypeBind.getValue().equals("IM")){
            System.out.println("iNDSEDI ELSE==>> IM");
            ob.getParamsMap().put("formNm", "PO_IMP");
            ob.getParamsMap().put("authoLim", "VR");
            ob.getParamsMap().put("UnitCd", unitCdBinding.getValue());
            //        System.out.println("Total SUM========>"+ADFUtils.resolveExpression("#{bindings.TotalAmtTrans.inputValue}"));
            ob.getParamsMap().put("Amount", value);
            ob.getParamsMap().put("empcd", vce.getNewValue().toString());
            ob.execute();
            System.out.println(" EMP CD"+vce.getNewValue().toString());
            if((ob.getResult() !=null && !ob.getResult().equals("Y")))
            {
               Row row=(Row)ADFUtils.evaluateEL("#{bindings.PurchaseOrderHeaderVO1Iterator.currentRow}");
               row.setAttribute("ApprBy", null);
               ADFUtils.showMessage(ob.getResult().toString(), 0);
            } 
        }
        else if(poTypeBind.getValue().equals("ST")){
            System.out.println("iNDSEDI ELSE==>> ST");
            ob.getParamsMap().put("formNm", "PO_STOCK");
            ob.getParamsMap().put("authoLim", "VR");
            ob.getParamsMap().put("UnitCd", unitCdBinding.getValue());
            //        System.out.println("Total SUM========>"+ADFUtils.resolveExpression("#{bindings.TotalAmtTrans.inputValue}"));
            ob.getParamsMap().put("Amount", value);
            ob.getParamsMap().put("empcd", vce.getNewValue().toString());
            ob.execute();
            System.out.println(" EMP CD"+vce.getNewValue().toString());
            if((ob.getResult() !=null && !ob.getResult().equals("Y")))
            {
               Row row=(Row)ADFUtils.evaluateEL("#{bindings.PurchaseOrderHeaderVO1Iterator.currentRow}");
               row.setAttribute("ApprBy", null);
               ADFUtils.showMessage(ob.getResult().toString(), 0);
            }  
        }
        else if(poTypeBind.getValue().equals("CA")){
            System.out.println("iNDSEDI ELSE==>> CA");
            ob.getParamsMap().put("formNm", "PO_CAP");
            ob.getParamsMap().put("authoLim", "VR");
            ob.getParamsMap().put("UnitCd", unitCdBinding.getValue());
            //        System.out.println("Total SUM========>"+ADFUtils.resolveExpression("#{bindings.TotalAmtTrans.inputValue}"));
            ob.getParamsMap().put("Amount",value);
            ob.getParamsMap().put("empcd", vce.getNewValue().toString());
            ob.execute();
            System.out.println(" EMP CD"+vce.getNewValue().toString());
            if((ob.getResult() !=null && !ob.getResult().equals("Y")))
            {
               Row row=(Row)ADFUtils.evaluateEL("#{bindings.PurchaseOrderHeaderVO1Iterator.currentRow}");
               row.setAttribute("ApprBy", null);
               ADFUtils.showMessage(ob.getResult().toString(), 0);
            } 
        }
        }
        
    }

    public void approvedByVCE(ValueChangeEvent vce) 
    {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        ADFUtils.findOperation("purchaseOrderSetValuesOfDate").execute(); 
            vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
            if(vce.getNewValue()!=null)
            {
                System.out.println("insdie==== vceeeeeeeeeeeeeeeeeee");
                BigDecimal amount=new BigDecimal(0);
                BigDecimal value=new BigDecimal(0);
                DCIteratorBinding dci = ADFUtils.findIterator("PurchaseOrderDetailVO1Iterator");
                RowSetIterator rsi=dci.getViewObject().createRowSetIterator(null);
                    while(rsi.hasNext()) {
                        System.out.println("inside rsi");
                        Row r = rsi.next();
                        BigDecimal qty=new BigDecimal(0);
                        BigDecimal rate=new BigDecimal(0);
                         qty=(BigDecimal) r.getAttribute("Qty");
                         rate=(BigDecimal) r.getAttribute("MaterialRate");
                        System.out.println("qty is==>>"+qty+"rate==>>"+rate);
                        if(qty!=null && rate!=null){
                            amount=qty.multiply(rate);
                            System.out.println("AMount==>>"+amount);
                            value=value.add(amount);
                        }
                       
                    }
                    rsi.closeRowSetIterator();
                    System.out.println("Value is==>>"+value);
//            System.out.println("Total SUM========>"+ADFUtils.resolveExpression("#{bindings.PoValue.inputValue}"));
            System.out.println("IN VCE Approved");
            OperationBinding ob = ADFUtils.findOperation("checkApprovalFunctionStatus");
            System.out.println("Inde t type==>"+poTypeBind.getValue());
            if(poTypeBind.getValue().equals("IN")){
                System.out.println("Indsdie iiiiiiifffffffffff IN");
            ob.getParamsMap().put("formNm", "PO");
            ob.getParamsMap().put("authoLim", "AP");
            ob.getParamsMap().put("UnitCd", unitCdBinding.getValue());
            //        System.out.println("Total SUM========>"+ADFUtils.resolveExpression("#{bindings.TotalAmtTrans.inputValue}"));
            ob.getParamsMap().put("Amount",value);
            ob.getParamsMap().put("empcd", vce.getNewValue().toString());
            ob.execute();
            System.out.println(" EMP CD"+vce.getNewValue().toString());
            if((ob.getResult() !=null && !ob.getResult().equals("Y")))
            {
               Row row=(Row)ADFUtils.evaluateEL("#{bindings.PurchaseOrderHeaderVO1Iterator.currentRow}");
               row.setAttribute("FrezBy", null);
               ADFUtils.showMessage(ob.getResult().toString(), 0);
            }
            }
            else if(poTypeBind.getValue().equals("HO")){
                System.out.println("Indsdie ELSE HO");
                ob.getParamsMap().put("formNm", "PO_HO");
                ob.getParamsMap().put("authoLim", "AP");
                ob.getParamsMap().put("UnitCd", unitCdBinding.getValue());
                //        System.out.println("Total SUM========>"+ADFUtils.resolveExpression("#{bindings.TotalAmtTrans.inputValue}"));
                ob.getParamsMap().put("Amount", value);
                ob.getParamsMap().put("empcd", vce.getNewValue().toString());
                ob.execute();
                System.out.println(" EMP CD"+vce.getNewValue().toString());
                if((ob.getResult() !=null && !ob.getResult().equals("Y")))
                {
                Row row=(Row)ADFUtils.evaluateEL("#{bindings.PurchaseOrderHeaderVO1Iterator.currentRow}");
                row.setAttribute("FrezBy", null);
                ADFUtils.showMessage(ob.getResult().toString(), 0);
                }
            }
            else if(poTypeBind.getValue().equals("IM")){
                System.out.println("iNDSEDI ELSE==>> IM");
                ob.getParamsMap().put("formNm", "PO_IMP");
                ob.getParamsMap().put("authoLim", "AP");
                ob.getParamsMap().put("UnitCd", unitCdBinding.getValue());
                //        System.out.println("Total SUM========>"+ADFUtils.resolveExpression("#{bindings.TotalAmtTrans.inputValue}"));
                ob.getParamsMap().put("Amount", value);
                ob.getParamsMap().put("empcd", vce.getNewValue().toString());
                ob.execute();
                System.out.println(" EMP CD"+vce.getNewValue().toString());
                if((ob.getResult() !=null && !ob.getResult().equals("Y")))
                {
                   Row row=(Row)ADFUtils.evaluateEL("#{bindings.PurchaseOrderHeaderVO1Iterator.currentRow}");
                   row.setAttribute("FrezBy", null);
                   ADFUtils.showMessage(ob.getResult().toString(), 0);
                } 
            }
            else if(poTypeBind.getValue().equals("ST")){
                System.out.println("iNDSEDI ELSE==>> ST");
                ob.getParamsMap().put("formNm", "PO_STOCK");
                ob.getParamsMap().put("authoLim", "AP");
                ob.getParamsMap().put("UnitCd", unitCdBinding.getValue());
                //        System.out.println("Total SUM========>"+ADFUtils.resolveExpression("#{bindings.TotalAmtTrans.inputValue}"));
                ob.getParamsMap().put("Amount", value);
                ob.getParamsMap().put("empcd", vce.getNewValue().toString());
                ob.execute();
                System.out.println(" EMP CD"+vce.getNewValue().toString());
                if((ob.getResult() !=null && !ob.getResult().equals("Y")))
                {
                   Row row=(Row)ADFUtils.evaluateEL("#{bindings.PurchaseOrderHeaderVO1Iterator.currentRow}");
                   row.setAttribute("FrezBy", null);
                   ADFUtils.showMessage(ob.getResult().toString(), 0);
                }  
            }
            else if(poTypeBind.getValue().equals("CA")){
                System.out.println("iNDSEDI ELSE==>> CA");
                ob.getParamsMap().put("formNm", "PO_CAP");
                ob.getParamsMap().put("authoLim", "AP");
                ob.getParamsMap().put("UnitCd", unitCdBinding.getValue());
                //        System.out.println("Total SUM========>"+ADFUtils.resolveExpression("#{bindings.TotalAmtTrans.inputValue}"));
                ob.getParamsMap().put("Amount",value);
                ob.getParamsMap().put("empcd", vce.getNewValue().toString());
                ob.execute();
                System.out.println(" EMP CD"+vce.getNewValue().toString());
                if((ob.getResult() !=null && !ob.getResult().equals("Y")))
                {
                   Row row=(Row)ADFUtils.evaluateEL("#{bindings.PurchaseOrderHeaderVO1Iterator.currentRow}");
                   row.setAttribute("FrezBy", null);
                   ADFUtils.showMessage(ob.getResult().toString(), 0);
                } 
            }
            }

        
        }

    public void setPayTermsTableBinding(RichTable payTermsTableBinding) {
        this.payTermsTableBinding = payTermsTableBinding;
    }

    public RichTable getPayTermsTableBinding() {
        return payTermsTableBinding;
    }

    public void paytermsDeletePopUp(DialogEvent dialogEvent) 
    {
        if(dialogEvent.getOutcome().name().equals("ok"))
                    {
                    ADFUtils.findOperation("Delete1").execute();
                    System.out.println("Record Delete Successfully");
                        FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                        FacesContext fc = FacesContext.getCurrentInstance();  
                        fc.addMessage(null, Message);  
                   
                    }

              AdfFacesContext.getCurrentInstance().addPartialTarget(payTermsTableBinding);
    }

    public void amendConfirmationDL(DialogEvent dialogEvent) 
    {
        if(dialogEvent.getOutcome().name().equals("ok"))
        {
            
           OperationBinding op1=ADFUtils.findOperation("checkAmdNo");
            Object ob=op1.execute();
            if(op1.getResult().toString()!=null && op1.getResult().toString().equalsIgnoreCase("N")){
                
                ADFUtils.showMessage("You Can Update Latest Amendment Only.", 2);
                ADFUtils.setEL("#{pageFlowScope.mode}", "V");
            }
            else{
           
           
            
            OperationBinding op=ADFUtils.findOperation("getAmdPoNoDetails");
            op.getParamsMap().put("PoId",ADFUtils.resolveExpression("#{pageFlowScope.PoId}"));
            op.getParamsMap().put("PoNo",ADFUtils.resolveExpression("#{pageFlowScope.PoNo}"));
            op.getParamsMap().put("AmdNo",ADFUtils.resolveExpression("#{pageFlowScope.AmdNo}"));
            op.execute();
                
            cevmodecheck();
                withEffectFromBinding.setValue(new oracle.jbo.domain.Timestamp(System.currentTimeMillis()));
                                getInsAmtBinding().setDisabled(true);
//                    getFrieghtByBinding().setDisabled(true);
//               /     getTranspoetrBinding().setDisabled(true);
//                    getPeriodBinding().setDisabled(true);
//                    getOthersBinding().setDisabled(true);
//                    getAmtBinding().setDisabled(true);
//                    getInsCodeBinding().setDisabled(true);
//                    getToleranceBinding().setDisabled(true);
//                    getProjectNoBinding().setDisabled(true);
                    getCostCentreBinding().setDisabled(true);
//                    getRemarksBinding().setDisabled(true);
//                    getWithEffectFromBinding().setDisabled(true);
                    getHeaderEditBinding().setDisabled(true);
                    getPaytermsPopupBinding().setDisabled(false);
                    getCalculationPopupBinding().setDisabled(false);
                    getItemDetailPopupBinding().setDisabled(false);
                    getAnnexurePopupBinding().setDisabled(false);
                    getUnitCdBinding().setDisabled(true);
//                    getJoPoLOVBind().setDisabled(true);
//                    getPoTypeBind().setDisabled(true);
//                    getPoType2Bind().setDisabled(true);
                    getSetFocusWhenEmptyMandatory().setDisabled(true);
//                    getVendorCodeBinding().setDisabled(true);
                    getPoDateBinding().setDisabled(true);
                    getAmendmentNoBinding().setDisabled(true);
                    getAmendmentDateBinding().setDisabled(true);
                    // getWithEffectFromBinding().setDisabled(true);
                    getReferencePoBinding().setDisabled(true);
//                    getDeliveryTermsBinding().setDisabled(true);
                   
                    getAmountPerBinding().setDisabled(true);
                    getVerifiedDateBinding().setDisabled(true);
                    getApprovedDateBinding().setDisabled(true);
                    getCheckDateBinding().setDisabled(true);
                    getPrepareByBinding().setDisabled(true);
//                    getModeOfDispatchBinding().setDisabled(true);
//                    getValidFormDateBinding().setDisabled(true);
//                    getValidToDateBinding().setDisabled(true);
//                    getCurrencyBinding().setDisabled(true);
//                    getDestinationBinding().setDisabled(true);
//                    getPoUnitTypeBinding().setDisabled(true);
                    getPoValueBinding().setDisabled(true);
                    getVendorAddressTypeBinding().setDisabled(true);
//                    getPostatusBinding().setDisabled(false);
                getDetailDeleteBinding().setDisabled(true);
            //getWithEffectFromBinding().setDisabled(false);

        }
        }
        else
        {
        getConfirmationPopupBinding().hide();    
        }
      
    }

    public void setConfirmationPopupBinding(RichPopup confirmationPopupBinding) {
        this.confirmationPopupBinding = confirmationPopupBinding;
    }

    public RichPopup getConfirmationPopupBinding() {
        return confirmationPopupBinding;
    }

    public void editButtonAL(ActionEvent actionEvent) 
    {
        String mode = (String) ADFUtils.resolveExpression("#{pageFlowScope.mode}");
        System.out.println("Approved Vy Balue=>"+getApprovedByBinding().getValue());
            if (getApprovedByBinding().getValue() !=null && getPostatusBinding().getValue().equals("Y")) 
                {
                
                System.out.println("Approved By Not Null");
                RichPopup.PopupHints hints = new RichPopup.PopupHints();
                getConfirmationPopupBinding().show(hints);
                    //ADFUtils.setEL("#{pageFlowScope.mode}", "E");
                    //ADFUtils.showMessage("This PO is freezed.You can only update the status.", 2);
                    //cevmodecheck();
                
                } 
                else
                {
                    cevmodecheck();
                    if(verifiedByBinding.getValue()==null ){
                        
                        System.out.println("Verified by isssss when null>>>>"+verifiedByBinding.getValue());
                    
                        approvedByBinding.setDisabled(true);
                    }
                    else{
                        verifiedByBinding.setDisabled(true);
                        
                    }
                    if(checkByBinding.getValue()==null){ 
                        
                        System.out.println("Checked by isssss when null>>>>"+checkByBinding.getValue());
                        verifiedByBinding.setDisabled(true);
                        
                    }
                    else{
                        
                        checkByBinding.setDisabled(true);
                    }
                }
//            if(getApprovedByBinding().getValue() !=null && getPostatusBinding().getValue().equals("Y")){
//                ADFUtils.showMessage("This PO is freezed.You can only update the status.", 2);
//                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
//                cevModeDisableComponent("V");
//                getPostatusBinding().setDisabled(false);
//            }
            System.out.println("po staus in edit mode is==>>"+getPostatusBinding().getValue());
            if(getApprovedByBinding().getValue() !=null && getPostatusBinding().getValue().equals("N")){
                ADFUtils.showMessage("You cannot modify/update the cancelled PO.", 2);
//                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
//                cevModeDisableComponent("V");
//                getDetailCreateBinding().setDisabled(true);
//                getDetailDeleteBinding().setDisabled(true);
//                getPaymentCreateBtnBinding().setDisabled(true);
//                getPaymentDeleteBtnBinding().setDisabled(true);
//                getPoCalcCreateBtnBinding().setDisabled(true);
              ADFUtils.setEL("#{pageFlowScope.mode}", "V");
                cevmodecheck();

                System.out.println("mode is====>>>"+ADFUtils.resolveExpression("#{pageFlowScope.mode}"));
                
            }
  }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() 
    {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelGroupLayout getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelGroupLayout getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }
    
    
    private void cevmodecheck(){
         if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
             makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
             cevModeDisableComponent("V");
         }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
             makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
             cevModeDisableComponent("C");
         }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
             makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
             cevModeDisableComponent("E");
         }
     }
    
  
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                try {
                    Method method1 =
                        component.getClass().getMethod("setDisabled", boolean.class);
                    if (method1 != null) {
                        method1.invoke(component, valueComponent);
                    }
                } catch (NoSuchMethodException e) {
                    try {
                        Method method =
                            component.getClass().getMethod("setReadOnly", boolean.class);
                        if (method != null) {
                            method.invoke(component, valueComponent);
                        }
                    } catch (Exception e1) {
                        // e.printStackTrace();//silently eat this exception.
                    }


                } catch (Exception e) {
                    // e.printStackTrace();//silently eat this exception.
                }
                List<UIComponent> childComponents = component.getChildren();
                for (UIComponent comp : childComponents) {
                    makeComponentHierarchyReadOnly(comp, valueComponent);
                }
            }



    //Set Fields and Button disable.
        public void cevModeDisableComponent(String mode) {
        //            FacesContext fctx = FacesContext.getCurrentInstance();
        //            ELContext elctx = fctx.getELContext();
        //            Application jsfApp = fctx.getApplication();
        //            //create a ValueExpression that points to the ADF binding layer
        //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
        //            //
        //            ValueExpression valueExpr = exprFactory.createValueExpression(
        //                                         elctx,
        //                                         "#{pageFlowScope.mode=='E'}",
        //                                          Object.class
        //                                         );
        //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
            if (mode.equals("E")) 
            {
              postatusBinding.setDisabled(true);  
              unitDocBinding.setDisabled(true);
              refDocDateBinding.setDisabled(true);
              refDocNoBinding.setDisabled(true);
              docSeqNoBinding.setDisabled(true);
              typeBinding.setDisabled(true);
              docFileNameBinding.setDisabled(true);
                getPoValNewbinding().setDisabled(true);
             getCurrRateBinding().setDisabled(true);   
            getHeaderEditBinding().setDisabled(true);
            getPaytermsPopupBinding().setDisabled(false);
            getCalculationPopupBinding().setDisabled(false);
            getItemDetailPopupBinding().setDisabled(false);
            getAnnexurePopupBinding().setDisabled(false);
            getUnitCdBinding().setDisabled(true);
//            getJoPoLOVBind().setDisabled(true);
//            getPoTypeBind().setDisabled(true);
//            getPoType2Bind().setDisabled(true);
            getSetFocusWhenEmptyMandatory().setDisabled(true);
//            getVendorCodeBinding().setDisabled(true);
            getPoDateBinding().setDisabled(true);
            getAmendmentNoBinding().setDisabled(true);
            getAmendmentDateBinding().setDisabled(true);
           // getWithEffectFromBinding().setDisabled(true);
            getReferencePoBinding().setDisabled(true);
//            getDeliveryTermsBinding().setDisabled(true);

            getAmountPerBinding().setDisabled(true);
            getVerifiedDateBinding().setDisabled(true);
            getApprovedDateBinding().setDisabled(true);
            getCheckDateBinding().setDisabled(true);
            getPrepareByBinding().setDisabled(true);
//            getModeOfDispatchBinding().setDisabled(true);
//            getValidFormDateBinding().setDisabled(true);
//            getValidToDateBinding().setDisabled(true);
//            getCurrencyBinding().setDisabled(true);
//            getDestinationBinding().setDisabled(true);
//            getPoUnitTypeBinding().setDisabled(true);
            getPoValueBinding().setDisabled(true);
            getVendorAddressTypeBinding().setDisabled(true);
            
            }
            else if (mode.equals("C")) 
            {
                cancelButtonBinding.setDisabled(true);
                closedBinding.setDisabled(true);
                unitDocBinding.setDisabled(true);
                refDocDateBinding.setDisabled(true);
                refDocNoBinding.setDisabled(true);
                docSeqNoBinding.setDisabled(true);
                typeBinding.setDisabled(true);
                docFileNameBinding.setDisabled(true);
                getPoValNewbinding().setDisabled(true);   
            getHeaderEditBinding().setDisabled(true);
                
      getUnitCdBinding().setDisabled(true);
          getPrepareByBinding().setDisabled(true);

            getSetFocusWhenEmptyMandatory().setDisabled(true);
            getPostatusBinding().setDisabled(true);
            getPoDateBinding().setDisabled(true);
            getAmendmentNoBinding().setDisabled(true);
            getAmendmentDateBinding().setDisabled(true);
      //      getWithEffectFromBinding().setDisabled(false);
            getPoValueBinding().setDisabled(true);
            getCheckDateBinding().setDisabled(true);
            getCheckByBinding().setDisabled(true);
            getVerifiedDateBinding().setDisabled(true);
            getVerifiedByBinding().setDisabled(true);
            getApprovedByBinding().setDisabled(true);
            getApprovedDateBinding().setDisabled(true);
                postatusBinding.setDisabled(true);  
//            getCalculationPopupBinding().setDisabled(false);
//            getItemDetailPopupBinding().setDisabled(false);
//            getAnnexurePopupBinding().setDisabled(false);
            }
            else if (mode.equals("V")) 
            {
                referenceDocTabBinding.setDisabled(false);
                getPaytermsPopupBinding().setDisabled(false);
                getCalculationPopupBinding().setDisabled(false);
                getItemDetailPopupBinding().setDisabled(false);
                getAnnexurePopupBinding().setDisabled(false);
                linkBinding.setDisabled(false);
                getVendorDetailTabBinding().setDisabled(false);
                getTermsTabBinding().setDisabled(false);
                getOthersTabBinding().setDisabled(false);
                cancelButtonBinding.setDisabled(false);
            }
            
        }


    public void setPaytermsPopupBinding(RichButton paytermsPopupBinding) {
        this.paytermsPopupBinding = paytermsPopupBinding;
    }

    public RichButton getPaytermsPopupBinding() {
        return paytermsPopupBinding;
    }

    public void setCalculationPopupBinding(RichButton calculationPopupBinding) {
        this.calculationPopupBinding = calculationPopupBinding;
    }

    public RichButton getCalculationPopupBinding() {
        return calculationPopupBinding;
    }

    public void setItemDetailPopupBinding(RichButton itemDetailPopupBinding) {
        this.itemDetailPopupBinding = itemDetailPopupBinding;
    }

    public RichButton getItemDetailPopupBinding() {
        return itemDetailPopupBinding;
    }

    public void setAnnexurePopupBinding(RichButton annexurePopupBinding) {
        this.annexurePopupBinding = annexurePopupBinding;
    }

    public RichButton getAnnexurePopupBinding() {
        return annexurePopupBinding;
    }

    public void setUnitCdBinding(RichInputComboboxListOfValues unitCdBinding) {
        this.unitCdBinding = unitCdBinding;
    }

    public RichInputComboboxListOfValues getUnitCdBinding() {
        return unitCdBinding;
    }

    public void setVendorCodeBinding(RichInputComboboxListOfValues vendorCodeBinding) {
        this.vendorCodeBinding = vendorCodeBinding;
    }

    public RichInputComboboxListOfValues getVendorCodeBinding() {
        return vendorCodeBinding;
    }

    public void setPostatusBinding(RichSelectOneChoice postatusBinding) {
        this.postatusBinding = postatusBinding;
    }

    public RichSelectOneChoice getPostatusBinding() {
        return postatusBinding;
    }

    public void setAmendmentNoBinding(RichInputText amendmentNoBinding) {
        this.amendmentNoBinding = amendmentNoBinding;
    }

    public RichInputText getAmendmentNoBinding() {
        return amendmentNoBinding;
    }

    public void setAmendmentDateBinding(RichInputDate amendmentDateBinding) {
        this.amendmentDateBinding = amendmentDateBinding;
    }

    public RichInputDate getAmendmentDateBinding() {
        return amendmentDateBinding;
    }

    public void setWithEffectFromBinding(RichInputDate withEffectFromBinding) {
        this.withEffectFromBinding = withEffectFromBinding;
    }

    public RichInputDate getWithEffectFromBinding() {
        try {
                    System.out.println("set getEntryDateBinding" + ADFUtils.getTodayDate());
                    String date = ADFUtils.convertAdToBs(ADFUtils.getTodayDate().toString());
                    System.out.println("setter getvaluedate" + date);
                    //            reqDateNepalBinding.setValue((String)date);
                    DCIteratorBinding dci = ADFUtils.findIterator("PurchaseOrderHeaderVO1Iterator");
                    dci.getCurrentRow().setAttribute("AmdWefNepal", date);
                    //            Row row=(Row)ADFUtils.evaluateEL("#{bindings.ContractRequisitionHeaderVO1Iterator.currentRow}");
                    //            row.setAttribute("ReqDateNp", date);
                } catch (ParseException e) {
                } 
        return withEffectFromBinding;
    }

    public void setReferencePoBindinf(RichInputComboboxListOfValues referencePoBindinf) {
        this.referencePoBindinf = referencePoBindinf;
    }

    public RichInputComboboxListOfValues getReferencePoBindinf() {
        return referencePoBindinf;
    }

    public void setReferencePoBinding(RichInputComboboxListOfValues referencePoBinding) {
        this.referencePoBinding = referencePoBinding;
    }

    public RichInputComboboxListOfValues getReferencePoBinding() {
        return referencePoBinding;
    }

    public void setDeliveryTermsBinding(RichSelectOneChoice deliveryTermsBinding) {
        this.deliveryTermsBinding = deliveryTermsBinding;
    }

    public RichSelectOneChoice getDeliveryTermsBinding() {
        return deliveryTermsBinding;
    }

    

   

    public void setAmountPerBinding(RichInputText amountPerBinding) {
        this.amountPerBinding = amountPerBinding;
    }

    public RichInputText getAmountPerBinding() {
        return amountPerBinding;
    }

    public void setPoValueBinding(RichInputText poValueBinding) {
        this.poValueBinding = poValueBinding;
    }

    public RichInputText getPoValueBinding() {
        return poValueBinding;
    }

    public void setCheckDateBinding(RichInputDate checkDateBinding) {
        this.checkDateBinding = checkDateBinding;
    }

    public RichInputDate getCheckDateBinding() {
        return checkDateBinding;
    }

    public void setApprovedByBinding(RichInputComboboxListOfValues approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputComboboxListOfValues getApprovedByBinding() {
        return approvedByBinding;
    }

    public void setApprovedDateBinding(RichInputDate approvedDateBinding) {
        this.approvedDateBinding = approvedDateBinding;
    }

    public RichInputDate getApprovedDateBinding() {
        return approvedDateBinding;
    }

    public void setVerifiedDateBinding(RichInputDate verifiedDateBinding) {
        this.verifiedDateBinding = verifiedDateBinding;
    }

    public RichInputDate getVerifiedDateBinding() {
        return verifiedDateBinding;
    }

    public void setCheckByBinding(RichInputComboboxListOfValues checkByBinding) {
        this.checkByBinding = checkByBinding;
    }

    public RichInputComboboxListOfValues getCheckByBinding() {
        return checkByBinding;
    }

    public void setVerifiedByBinding(RichInputComboboxListOfValues verifiedByBinding) {
        this.verifiedByBinding = verifiedByBinding;
    }

    public RichInputComboboxListOfValues getVerifiedByBinding() {
        return verifiedByBinding;
    }

    public void setDetailCreateBinding(RichButton detailCreateBinding) {
        this.detailCreateBinding = detailCreateBinding;
    }

    public RichButton getDetailCreateBinding() {
        return detailCreateBinding;
    }

    public void setDetailDeleteBinding(RichButton detailDeleteBinding) {
        this.detailDeleteBinding = detailDeleteBinding;
    }

    public RichButton getDetailDeleteBinding() {
        return detailDeleteBinding;
    }

    public void setPrepareByBinding(RichInputComboboxListOfValues prepareByBinding) {
        this.prepareByBinding = prepareByBinding;
    }

    public RichInputComboboxListOfValues getPrepareByBinding() {
        return prepareByBinding;
    }

    public void setVendorDetailTabBinding(RichShowDetailItem vendorDetailTabBinding) {
        this.vendorDetailTabBinding = vendorDetailTabBinding;
    }

    public RichShowDetailItem getVendorDetailTabBinding() {
        return vendorDetailTabBinding;
    }

    public void setTermsTabBinding(RichShowDetailItem termsTabBinding) {
        this.termsTabBinding = termsTabBinding;
    }

    public RichShowDetailItem getTermsTabBinding() {
        return termsTabBinding;
    }

    public void setOthersTabBinding(RichShowDetailItem othersTabBinding) {
        this.othersTabBinding = othersTabBinding;
    }

    public RichShowDetailItem getOthersTabBinding() {
        return othersTabBinding;
    }
    
    public void setModeOfDispatchBinding(RichSelectOneChoice modeOfDispatchBinding) {
        this.modeOfDispatchBinding = modeOfDispatchBinding;
    }

    public RichSelectOneChoice getModeOfDispatchBinding() {
        return modeOfDispatchBinding;
    }

    public void setCurrencyBinding(RichSelectOneChoice currencyBinding) {
        this.currencyBinding = currencyBinding;
    }

    public RichSelectOneChoice getCurrencyBinding() {
        return currencyBinding;
    }

    public void setDestinationBinding(RichInputText destinationBinding) {
        this.destinationBinding = destinationBinding;
    }

    public RichInputText getDestinationBinding() {
        return destinationBinding;
    }


    public void detailCreateAL(ActionEvent actionEvent)
    {
        if(status.equalsIgnoreCase("T"))
        {
            FacesMessage message = new FacesMessage("Qty must be less than or equals to Indent Qty.");
                        message.setSeverity(FacesMessage.SEVERITY_ERROR);
                        FacesContext context = FacesContext.getCurrentInstance();
                        context.addMessage(quantityBinding.getClientId(), message);
        }
        else
        {
        ADFUtils.findOperation("CreateInsert3").execute();
        getVendorCodeBinding().setDisabled(true);
        getDeliveryTermsBinding().setDisabled(true);
        getAmountPerBinding().setDisabled(true);
        getModeOfDispatchBinding().setDisabled(true);
        getValidFormDateBinding().setDisabled(true);
        getValidToDateBinding().setDisabled(true);
//        getCurrencyBinding().setDisabled(true);
        getDestinationBinding().setDisabled(true);
        getJoPoLOVBind().setDisabled(true);
        getPoTypeBind().setDisabled(true);
        getPoType2Bind().setDisabled(true);
        getPoUnitTypeBinding().setDisabled(true);
        getCurrRateBinding().setDisabled(true);
        getCurrencyBinding().setDisabled(true);
        }
        
    }

    public void setPoUnitTypeBinding(RichSelectOneChoice poUnitTypeBinding) {
        this.poUnitTypeBinding = poUnitTypeBinding;
    }

    public RichSelectOneChoice getPoUnitTypeBinding() {
        return poUnitTypeBinding;
    }

    public void setVendorAddressTypeBinding(RichInputComboboxListOfValues vendorAddressTypeBinding) {
        this.vendorAddressTypeBinding = vendorAddressTypeBinding;
    }

    public RichInputComboboxListOfValues getVendorAddressTypeBinding() {
        return vendorAddressTypeBinding;
    }

    public void poTypeVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());     
        if(vce.getNewValue() != null){
            

            String potype = (String) poTypeBind.getValue();
            System.out.println("potype issss=>>+po"+potype);
            if(potype.equalsIgnoreCase("IM")){
                getCurrRateBinding().setDisabled(true);
            }
            else{
                getCurrRateBinding().setDisabled(false);
            }

//            System.out.println("Po type is==>>" +jopo);
//            if(jopo !=null){
//                
//          
//            if(jopo.equals("J")){
//                System.out.println("inside jopo if JOB WORK**********");
//                
//                poTypeBind.setValue("JW");
//            }
//            if(jopo.equals("P")){
//                System.out.println("inside jopo if PURCHASE **********");
//                
//                poTypeBind.setValue("RM");
//            
//            
//        }
//        
//       
//    }
        }
    }

//    public void indentIcsNoValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
//        //                             if(JoPoType.equalsIgnoreCase("J") && !PoType.equals("JW") && !PoType.equals("RP") && !PoType.equals("JT")){
//                              //  throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Only JobWork / Report & Maintenance Type is Allowed for JobWork Order", null));
//                          //  }
//                          String PoType = poTypeBind.getValue().toString();
//                          System.out.println("Po Type is in indent validator==>>"+PoType);
//        
//        if(poTypeBind.getValue()!=null && !PoType.equals("JW")) {
//            
//            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Indent/Ics No. is required.", null));
//            
//        }
//        else {
//            
//            
//            
//        }
//
//    }
    public void setMtlRateBinding(RichColumn mtlRateBinding) {
        this.mtlRateBinding = mtlRateBinding;
    }

    public RichColumn getMtlRateBinding() {
        return mtlRateBinding;
    }

    public void procSeqValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        BigDecimal a=new BigDecimal(0);
     System.out.println("value of process seq in val"+object.toString());
        if(object ==null || object.equals(".") ){
          //  System.out.println("value of process seq in val"+object.toString());
            String jopo=joPoLOVBind.getValue().toString();
            if(jopo.equals("J")){
                
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Process Seq is mandatory in case of Job work.", null)); 
            }
            
            
            
        }
      System.out.println("Inside process sequence validator");
System.out.println("Value of PO Type: "+joPoLOVBind.getValue());
        if(joPoLOVBind.getValue().equals("J"))
        {
            System.out.println("Inside If of PO type");
            BigDecimal ps=new BigDecimal(0);
            BigDecimal ps1=(BigDecimal)procSeqBinding.getValue();
            System.out.println("Value of Process Sequence: "+procSeqBinding.getValue());
            if(ps1.compareTo(ps)==-1 || ps1.compareTo(ps)==0)
            {
                System.out.println("Inside IF after compare!");
                    FacesMessage message = new FacesMessage("Process Sequence is required and should be greater than zero.");
                                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                                FacesContext context = FacesContext.getCurrentInstance();
                                context.addMessage(procSeqBinding.getClientId(), message);
                }
            }

    }

    public void indentNoValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
//        System.out.println("INSIDE INDENT NO VALIDATOR");
//        if(object!=null){
//       if(poTypeBind.getValue()!=null) {
//           
//           String PoType = poTypeBind.getValue().toString();
//            System.out.println("Po Type in indent  no. validator==>>"+PoType);
//                   
//                   if(PoType.equals("IN") || PoType.equals("CA")) {
//                       
//                       throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Indent/Ics No. is required.", null));
//                       
//                   }
//                   else {
//                       
//                       
//                       
//                   }
//       }}

    }

    public void jopoVCL(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        System.out.println("Inside PO type VCL");
        if(valueChangeEvent.getNewValue()!=null){
            
            String jopo = (String) joPoLOVBind.getValue();
            System.out.println("jo po value in bean is==>>"+jopo);
            if(jopo.equals("P")){
                System.out.println("in pppppppppppppppppppppppp");
                System.out.println("Value of procSeq before set: "+procSeqBinding.getValue());
                poTypeBind.setValue("RM");
                procSeqBinding.setValue(0);
                System.out.println("Value of procSeq: "+procSeqBinding.getValue());
            }
            if(jopo.equals("J")){
                System.out.println("in jjjjjjjjjjjjjjjjjjjjjjjjjjjjj");
                System.out.println("Value of procSeq before set: "+procSeqBinding.getValue());
                poTypeBind.setValue("JW");
                BigDecimal ps=new BigDecimal(0);
                           BigDecimal ps1=(BigDecimal)procSeqBinding.getValue();
                           if(ps1==ps)
                               procSeqBinding.setValue(null);
                System.out.println("Value of procSeq: "+procSeqBinding.getValue());
            }
            
        }
    }

  

    public void poStatusVCL(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().getCurrentComponent(FacesContext.getCurrentInstance());
        if(valueChangeEvent.getNewValue()!=null){
            System.out.println("inside bean meFacesthid vcl================");
            String poStatus = (String) getPostatusBinding().getValue();
            System.out.println("Po sttua is---=->>"+poStatus);
            if(poStatus.equalsIgnoreCase("N")){
                System.out.println("inside if of bean===============");
//                ADFUtils.findOperation("setClosedToCancePO").execute();
//                getClosedBinding().setValue(true);
//                System.out.println("value of cancel==>"+getClosedBinding().getValue());
            
            }
        }
    }

    public void setPoCalcCreateBtnBinding(RichButton poCalcCreateBtnBinding) {
        this.poCalcCreateBtnBinding = poCalcCreateBtnBinding;
    }

    public RichButton getPoCalcCreateBtnBinding() {
        return poCalcCreateBtnBinding;
    }

    public void setPaymentCreateBtnBinding(RichButton paymentCreateBtnBinding) {
        this.paymentCreateBtnBinding = paymentCreateBtnBinding;
    }

    public RichButton getPaymentCreateBtnBinding() {
        return paymentCreateBtnBinding;
    }

    public void setPaymentDeleteBtnBinding(RichButton paymentDeleteBtnBinding) {
        this.paymentDeleteBtnBinding = paymentDeleteBtnBinding;
    }

    public RichButton getPaymentDeleteBtnBinding() {
        return paymentDeleteBtnBinding;
    }

    public void setInsAmtBinding(RichInputText insAmtBinding) {
        this.insAmtBinding = insAmtBinding;
    }

    public RichInputText getInsAmtBinding() {
        return insAmtBinding;
    }

    public void setFrieghtByBinding(RichSelectOneChoice frieghtByBinding) {
        this.frieghtByBinding = frieghtByBinding;
    }

    public RichSelectOneChoice getFrieghtByBinding() {
        return frieghtByBinding;
    }

    public void setTranspoetrBinding(RichInputComboboxListOfValues transpoetrBinding) {
        this.transpoetrBinding = transpoetrBinding;
    }

    public RichInputComboboxListOfValues getTranspoetrBinding() {
        return transpoetrBinding;
    }

    public void setOthersBinding(RichInputText othersBinding) {
        this.othersBinding = othersBinding;
    }

    public RichInputText getOthersBinding() {
        return othersBinding;
    }

    public void setAmtBinding(RichInputText amtBinding) {
        this.amtBinding = amtBinding;
    }

    public RichInputText getAmtBinding() {
        return amtBinding;
    }

    public void setPeriodBinding(RichInputText periodBinding) {
        this.periodBinding = periodBinding;
    }

    public RichInputText getPeriodBinding() {
        return periodBinding;
    }

    public void setInsCodeBinding(RichSelectOneChoice insCodeBinding) {
        this.insCodeBinding = insCodeBinding;
    }

    public RichSelectOneChoice getInsCodeBinding() {
        return insCodeBinding;
    }

    public void setToleranceBinding(RichInputText toleranceBinding) {
        this.toleranceBinding = toleranceBinding;
    }

    public RichInputText getToleranceBinding() {
        return toleranceBinding;
    }

    public void setRemarksBinding(RichInputText remarksBinding) {
        this.remarksBinding = remarksBinding;
    }

    public RichInputText getRemarksBinding() {
        return remarksBinding;
    }

    public void setCostCentreBinding(RichInputComboboxListOfValues costCentreBinding) {
        this.costCentreBinding = costCentreBinding;
    }

    public RichInputComboboxListOfValues getCostCentreBinding() {
        return costCentreBinding;
    }

    public void setProjectNoBinding(RichInputComboboxListOfValues projectNoBinding) {
        this.projectNoBinding = projectNoBinding;
    }

    public RichInputComboboxListOfValues getProjectNoBinding() {
        return projectNoBinding;
    }

    public void ItemdetailsDL(DialogEvent dialogEvent) {   
        if(dialogEvent.getOutcome().name().equals("ok") || dialogEvent.getOutcome().name().equals("cancel")){
            System.out.println("puchaseOrderCalculate Calling on item detail dialog listener ok outcome");
            ADFUtils.findOperation("puchaseOrderCalculate").execute();
            System.out.println("End puchaseOrderCalculate");
            ADFUtils.findOperation("purchaseOrderLprCalculation").execute();
        }
    }

    public void setPiosItemBinding(RichInputComboboxListOfValues piosItemBinding) {
        this.piosItemBinding = piosItemBinding;
    }

    public RichInputComboboxListOfValues getPiosItemBinding() {
        return piosItemBinding;
    }

    public void piosItemVCL(ValueChangeEvent valueChangeEvent) {
        System.out.println("Item Code Specification Start ");
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        
            
        
             
    
      ADFUtils.findOperation("purchaseOrderDetailItemSpecification").execute();

        System.out.println("Item Code Specification End ");
        System.out.println("####Start");
        //        ADFUtils.findOperation("getQtyPoDetail").execute();
     ADFUtils.findOperation("purchaseOrderDetailMaterialRate").execute();  
        System.out.println("####End");
        ADFUtils.findOperation("purchaseOrderGstLov").execute();   
        ADFUtils.findOperation("purchaseOrderGstCalculation").execute();
        
        OperationBinding opp=ADFUtils.findOperation("LecoQtyPurchaseOrderDetail");
        Object obj=opp.execute();
        if(opp.getResult()!=null && opp.getResult()!="Y")
        {
            System.out.println("Value is ====> "+opp.getResult());
            if (opp.getErrors().isEmpty()) 
            {

                FacesMessage Message = new FacesMessage("For Item "+opp.getResult().toString());  
                Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                FacesContext fc = FacesContext.getCurrentInstance();  
                fc.addMessage(null, Message); 
                //throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERORORROROROROROOR.", null));
            
            }

        }
        
        ADFUtils.findOperation("clearSelectedRowOnChangeItemPO").execute();
           AdfFacesContext.getCurrentInstance().addPartialTarget(poDetailTableBind);
        


            
    }

    public void paymentTermDL(DialogEvent dialogEvent) {
        
        if(dialogEvent.getOutcome().name().equals("ok")){
            
            OperationBinding operBind=ADFUtils.findOperation("purchaseOrderPaymentTerms");
            Object obj=operBind.execute();
            System.out.println("Result is ====>  "+operBind.getResult());
            if(operBind.getResult()!=null && operBind.getResult().equals("N"))
            {
                            FacesMessage Message = new FacesMessage("Payment Term Must Be Defined 100%.");
                           Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                           FacesContext fc = FacesContext.getCurrentInstance();
                           fc.addMessage(null, Message);
                
            }
        }
    }

    public void setEdDateBinding(RichInputDate edDateBinding) {
        this.edDateBinding = edDateBinding;
    }

    public RichInputDate getEdDateBinding() {
        return edDateBinding;
    }


    public void setDeliveryDateBinding(RichInputDate deliveryDateBinding) {
        this.deliveryDateBinding = deliveryDateBinding;
    }

    public RichInputDate getDeliveryDateBinding() {
        try {
            System.out.println("set getEntryDateBinding" + ADFUtils.getTodayDate());
            String date = ADFUtils.convertAdToBs(ADFUtils.getTodayDate().toString());
            System.out.println("setter getvaluedate" + date);

            DCIteratorBinding dci = ADFUtils.findIterator("PurchaseOrderDetailVO1Iterator");
            //                    dci.getCurrentRow().setAttribute("ReqDateNp", date);
            System.out.println("EdDateC"+dci.getCurrentRow());

        } catch (ParseException e) {
        }
        return deliveryDateBinding;
    }

    public void annTypeVCE(ValueChangeEvent valueChangeEvent) {
        System.out.println("inside bean of vce");
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(valueChangeEvent!=null){
       OperationBinding opr= ADFUtils.findOperation("AnnexurePoPopulate");
       
        opr.getParamsMap().put("AnnType",annTypeBind.getValue());
        
        opr.execute();
        }
    }

    public void annAL(ActionEvent actionEvent) {
        System.out.println("insie al");
       
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
                  getAnnPopupBinding().show(hints);
//        ADFUtils.findOperation("createInsertinAnnexureTyp").execute();
    }

    public void setAnnPopupBinding(RichPopup annPopupBinding) {
        this.annPopupBinding = annPopupBinding;
    }

    public RichPopup getAnnPopupBinding() {
        return annPopupBinding;
    }

    public void setAnnTypBinding(RichInputComboboxListOfValues annTypBinding) {
        this.annTypBinding = annTypBinding;
    }

    public RichInputComboboxListOfValues getAnnTypBinding() {
        return annTypBinding;
    }

    public void setAnnTypeBind(RichSelectOneChoice annTypeBind) {
        this.annTypeBind = annTypeBind;
    }

    public RichSelectOneChoice getAnnTypeBind() {
        return annTypeBind;
    }

    public void popAnnAL(ActionEvent actionEvent) {
        OperationBinding opr= ADFUtils.findOperation("AnnexurePoPopulate");
        
         opr.getParamsMap().put("AnnType",annTypeBind.getValue());
         
         opr.execute();
    }

    public void venCdVCE(ValueChangeEvent valueChangeEvent) {
        System.out.println("inside beanof method populate");
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(valueChangeEvent!=null){
            System.out.println("inside beanof method populate ifffffff");
            OperationBinding op1=ADFUtils.findOperation("CheckPoForTrialVendor");
            Object ob1=op1.execute(); 
            
            if(op1.getResult().toString().equalsIgnoreCase("N")){
                
                FacesMessage Message = new FacesMessage("Trial PO making of this Vendor has reached to its maximum limit.");   
                Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                FacesContext fc = FacesContext.getCurrentInstance();   
                fc.addMessage(null, Message); 
                
            }
            else{
        OperationBinding op=ADFUtils.findOperation("populatePreviousAnnexureAndTerms");
        Object ob=op.execute();
                System.out.println("result is===?>>"+op.getResult());
        if(op.getResult().toString().equalsIgnoreCase("U")){
            getPoUnitTypeBinding().setDisabled(true);
            getPoUnitTypeBinding().setValue("H");
        }
        else{
            getPoUnitTypeBinding().setDisabled(false);
            getPoUnitTypeBinding().setValue("U");
        }
        }
        }
    }

    public void poTypeVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce!=null){
            System.out.println("inside vce of urrency code");
            OperationBinding op=ADFUtils.findOperation("CheckFromDailyCurr");
            op.getParamsMap().put("CurCd", currencyBinding.getValue());
            Object ob=op.execute();
            System.out.println("result iss==>>"+op.getResult());
            
            if(op.getResult()!=null && op.getResult().toString().equalsIgnoreCase("N")){
                System.out.println("inside if when result is ===>> not IM");
                
                    FacesMessage Message = new FacesMessage("Daily Currency is  not updated for this PO Date in Currency Master");   
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                    FacesContext fc = FacesContext.getCurrentInstance();   
                    fc.addMessage(null, Message); 
            }
//            if(op.getResult().toString().equalsIgnoreCase("IM")){
//                
//                System.out.println("inside if when result is ===>> IM");
//                getCurrRateBinding().setDisabled(true);
//            }
//            else{
//                getCurrRateBinding().setDisabled(false);
//            }
        }
    }

    public void setCurrRateBinding(RichInputText currRateBinding) {
        this.currRateBinding = currRateBinding;
    }

    public RichInputText getCurrRateBinding() {
        return currRateBinding;
    }

    public void populateTermsAndCondAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("populateTermsOfPrevPo").execute();
    }


    public void quotRefVCE(ValueChangeEvent valueChangeEvent) {
        System.out.println("********value in vce==>>>"+valueChangeEvent.getNewValue()+"old vallll"+valueChangeEvent.getOldValue());
        if(valueChangeEvent.getNewValue().toString().isEmpty()){
//           rateBinding.setValue(new BigDecimal(0));
            System.out.println("value in vce==>>>"+valueChangeEvent.getNewValue()+"old vallll"+valueChangeEvent.getOldValue());
            System.out.println("####Start");
            System.out.println("get value before"+rateBinding.getValue());
           ADFUtils.findOperation("purchaseOrderDetailMaterialRate").execute();  
            System.out.println("####End111111111111111"); 
            BigDecimal rate = (BigDecimal) rateBinding.getValue();
            System.out.println("get value agter"+rateBinding.getValue());
            rateBinding.setValue(rate);
        }
    }

    public void setRateBinding(RichInputText rateBinding) {
        this.rateBinding = rateBinding;
    }

    public RichInputText getRateBinding() {
        return rateBinding;
    }

    public void setPoValNewbinding(RichInputText poValNewbinding) {
        this.poValNewbinding = poValNewbinding;
    }

    public RichInputText getPoValNewbinding() {
        return poValNewbinding;
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                    {
                    ADFUtils.findOperation("Delete2").execute();
                    System.out.println("Record Delete Successfully");
                   
                        FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                        FacesContext fc = FacesContext.getCurrentInstance();  
                        fc.addMessage(null, Message);  
                   
                    }
                AdfFacesContext.getCurrentInstance().addPartialTarget(annexureTableBinding);
    }

    public void setAnnexureTableBinding(RichTable annexureTableBinding) {
        this.annexureTableBinding = annexureTableBinding;
    }

    public RichTable getAnnexureTableBinding() {
        return annexureTableBinding;
    }

    public void annexureCreateAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert1").execute();
        DCIteratorBinding binding = ADFUtils.findIterator("AnnexurePoVO2Iterator"); 
        if(binding.getCurrentRow()!=null){
            System.out.println("inside al");
            String anntype = (String) annTypeBind.getValue();
            System.out.println("vfdnjdgdvg"+anntype);
            binding.getCurrentRow().setAttribute("AnnexType", anntype);
        }
    }

    public void subjectButtonAL(ActionEvent actionEvent) {
//        DCIteratorBinding binding = ADFUtils.findIterator("AnnexurePoVO2Iterator"); 
//        if(binding.getCurrentRow()!=null){
//            System.out.println("inside al");
//            String subject = (String) sub.getValue();
//            System.out.println("vfdnjdgdvg"+anntype);
//            binding.getCurrentRow().setAttribute("AnnexType", anntype);
//        }
    }

    public void setSubBinding(RichInputText subBinding) {
        this.subBinding = subBinding;
    }

    public RichInputText getSubBinding() {
        return subBinding;
    }

    public void setUnitDocBinding(RichInputText unitDocBinding) {
        this.unitDocBinding = unitDocBinding;
    }

    public RichInputText getUnitDocBinding() {
        return unitDocBinding;
    }

    public void setRefDocNoBinding(RichInputText refDocNoBinding) {
        this.refDocNoBinding = refDocNoBinding;
    }

    public RichInputText getRefDocNoBinding() {
        return refDocNoBinding;
    }

    public void setDocFileNameBinding(RichInputText docFileNameBinding) {
        this.docFileNameBinding = docFileNameBinding;
    }

    public RichInputText getDocFileNameBinding() {
        return docFileNameBinding;
    }

    public void setDocSeqNoBinding(RichInputText docSeqNoBinding) {
        this.docSeqNoBinding = docSeqNoBinding;
    }

    public RichInputText getDocSeqNoBinding() {
        return docSeqNoBinding;
    }

    public void setRefDocDateBinding(RichInputDate refDocDateBinding) {
        this.refDocDateBinding = refDocDateBinding;
    }

    public RichInputDate getRefDocDateBinding() {
        return refDocDateBinding;
    }

    public void setRemDocBinding(RichInputText remDocBinding) {
        this.remDocBinding = remDocBinding;
    }

    public RichInputText getRemDocBinding() {
        return remDocBinding;
    }

    public void setReferenceDocTabBinding(RichShowDetailItem referenceDocTabBinding) {
        this.referenceDocTabBinding = referenceDocTabBinding;
    }

    public RichShowDetailItem getReferenceDocTabBinding() {
        return referenceDocTabBinding;
    }

    public void setTypeBinding(RichInputText typeBinding) {
        this.typeBinding = typeBinding;
    }

    public RichInputText getTypeBinding() {
        return typeBinding;
    }

    public void uploadAction(ValueChangeEvent vce) {
        // Add event code here...
        if (vce.getNewValue() != null) {
                    //Get File Object from VC Event
                    UploadedFile fileVal = (UploadedFile) vce.getNewValue();
                    //Upload File to path- Return actual server path
                    String path = ADFUtils.uploadFile(fileVal);
                      System.out.println(fileVal.getContentType());
                      System.out.println("Content Type==>>"+fileVal.getContentType()+"Path==>>"+path+"File name==>>"+fileVal.getFilename());
                    //Method to insert data in table to keep track of uploaded files
                    OperationBinding ob = ADFUtils.findOperation("setFileData");
                    ob.getParamsMap().put("name", fileVal.getFilename());
                    ob.getParamsMap().put("path", path);
                    ob.getParamsMap().put("contTyp", fileVal.getContentType());
                    ob.getParamsMap().put("SrNo", ADFUtils.evaluateEL("#{bindings.DocAttachRefDtlVO4Iterator.estimatedRowCount+1}"));
                    ob.execute();
                    // Reset inputFile component after upload
                    ResetUtils.reset(vce.getComponent());
                }
    }

    public void downloadAction(FacesContext facesContext, OutputStream outputStream) throws Exception {
        
        File filed = new File(pathBinding.getValue().toString());
                               FileInputStream fis;
                               byte[] b;
                               try {
                                   fis = new FileInputStream(filed);

                                   int n;
                                   while ((n = fis.available()) > 0) {
                                       b = new byte[n];
                                       int result = fis.read(b);
                                       outputStream.write(b, 0, b.length);
                                       if (result == -1)
                                           break;
                                   }
                               } catch (Exception e) {
                                   e.printStackTrace();
                               }
                               outputStream.flush();
        
    }

    public void setPathBinding(RichInputText pathBinding) {
        this.pathBinding = pathBinding;
    }

    public RichInputText getPathBinding() {
        return pathBinding;
    }

    public void setContentTypeBinding(RichInputText contentTypeBinding) {
        this.contentTypeBinding = contentTypeBinding;
    }

    public RichInputText getContentTypeBinding() {
        return contentTypeBinding;
    }

    public void deletePopupDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                    {
                    ADFUtils.findOperation("Delete3").execute();
                    //ADFUtils.findOperation("Commit").execute();    //Unable on Search page.
                    System.out.println("Record Delete Successfully");
                   
                        FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                        FacesContext fc = FacesContext.getCurrentInstance();  
                        fc.addMessage(null, Message);  
                   
                    }

              AdfFacesContext.getCurrentInstance().addPartialTarget(docAttachTableBinding);
    }

    public void setDocAttachTableBinding(RichTable docAttachTableBinding) {
        this.docAttachTableBinding = docAttachTableBinding;
    }

    public RichTable getDocAttachTableBinding() {
        return docAttachTableBinding;
    }

    public void setClosedBinding(RichSelectBooleanCheckbox closedBinding) {
        this.closedBinding = closedBinding;
    }

    public RichSelectBooleanCheckbox getClosedBinding() {
        return closedBinding;
    }

    public void setLinkBinding(RichCommandLink linkBinding) {
        this.linkBinding = linkBinding;
    }

    public RichCommandLink getLinkBinding() {
        return linkBinding;
    }

    public void cancelPoDialogDL(DialogEvent de) {
        if(de.getOutcome().name().equals("ok")){
            System.out.println("inside Ok outcome"+postatusBinding.getValue());
            if(postatusBinding.getValue().toString().equalsIgnoreCase("Y")){
                OperationBinding op = ADFUtils.findOperation("checkDocumentStatus");
                op.getParamsMap().put("formname", "PO");
             //   op.getParamsMap().put("empcd", "E-001");
              op.getParamsMap().put("empcd", ADFUtils.resolveExpression("#{pageFlowScope.empCode}"));
              
                Integer rst = (Integer) op.execute();
                System.out.println("Rst Value :- "+rst);  
                if (rst != null) {
                                   if (op.getErrors().isEmpty()) {
                                       if (rst == 0){
                                           ADFUtils.showMessage("You are not authorize to cancel the PO.", 1);
                                       }
                                       else{
                                           
                                           ADFUtils.showMessage("PO successfully cancelled.", 2);
                                          ADFUtils.findOperation("setPostatusInPO").execute();
                                           ADFUtils.findOperation("Commit").execute();
                                       }
                                   }
                               }
              
            }
            else{
                ADFUtils.showMessage("PO already cancelled.", 2);
            }
        }
    }

    public void setCancelPopUpBinding(RichPopup cancelPopUpBinding) {
        this.cancelPopUpBinding = cancelPopUpBinding;
    }

    public RichPopup getCancelPopUpBinding() {
        return cancelPopUpBinding;
    }

    public void setCancelButtonBinding(RichButton cancelButtonBinding) {
        this.cancelButtonBinding = cancelButtonBinding;
    }

    public RichButton getCancelButtonBinding() {
        return cancelButtonBinding;
    }

    public void setProcSeqBinding(RichInputComboboxListOfValues procSeqBinding) {
        this.procSeqBinding = procSeqBinding;
    }

    public RichInputComboboxListOfValues getProcSeqBinding() {
        return procSeqBinding;
    }

    public void AmendWithEffectVCL(ValueChangeEvent vce) {
        if(vce.getNewValue() != null){
            try {
                        System.out.println("set getEntryDateBinding" + vce.getNewValue());
                        String date = ADFUtils.convertAdToBs(vce.getNewValue().toString());
                        System.out.println("setter getvaluedate" + date);
                        //            reqDateNepalBinding.setValue((String)date);
                        DCIteratorBinding dci = ADFUtils.findIterator("PurchaseOrderHeaderVO1Iterator");
                        dci.getCurrentRow().setAttribute("AmdWefNepal", date);
                        //            Row row=(Row)ADFUtils.evaluateEL("#{bindings.ContractRequisitionHeaderVO1Iterator.currentRow}");
                        //            row.setAttribute("ReqDateNp", date);
                    } catch (ParseException e) {
                    } 
            
        }

    }

    public void setNewdeliveryDateBinding(RichInputDate newdeliveryDateBinding) {
        this.newdeliveryDateBinding = newdeliveryDateBinding;
    }

    public RichInputDate getNewdeliveryDateBinding() {        
        return newdeliveryDateBinding;
    }

    public void ValidFromVCL(ValueChangeEvent vce) {
        if(vce.getNewValue() != null){
            try {
                    System.out.println("set getEntryDateBinding" + ADFUtils.getTodayDate());
                    String date = ADFUtils.convertAdToBs(vce.getNewValue().toString());
                    System.out.println("setter getvaluedate" + date);
                    //            reqDateNepalBinding.setValue((String)date);
                    DCIteratorBinding dci = ADFUtils.findIterator("PurchaseOrderHeaderVO1Iterator");
                    dci.getCurrentRow().setAttribute("ValidFrNepal", date);
                    //            Row row=(Row)ADFUtils.evaluateEL("#{bindings.ContractRequisitionHeaderVO1Iterator.currentRow}");
                    //            row.setAttribute("ReqDateNp", date);
                } catch (ParseException e) {
                 }  
        }
        }

    public void ValidToVCL(ValueChangeEvent vce) {
        if(vce.getNewValue() != null){
            try {
                        System.out.println("set getEntryDateBinding" + ADFUtils.getTodayDate());
                        String date = ADFUtils.convertAdToBs(vce.getNewValue().toString());
                        System.out.println("setter getvaluedate" + date);
                        //            reqDateNepalBinding.setValue((String)date);
                        DCIteratorBinding dci = ADFUtils.findIterator("PurchaseOrderHeaderVO1Iterator");
                        dci.getCurrentRow().setAttribute("ValidToNepal", date);
                        //            Row row=(Row)ADFUtils.evaluateEL("#{bindings.ContractRequisitionHeaderVO1Iterator.currentRow}");
                        //            row.setAttribute("ReqDateNp", date);
                    } catch (ParseException e) {
                    } 
        }
    }

//    public void closePO(ActionEvent actionEvent) {
//        
//        DCIteratorBinding dci = ADFUtils.findIterator("PurchaseOrderHeaderVO1Iterator");
//        
//        OperationBinding op = ADFUtils.findOperation("closePO");
//        op.getParamsMap().put("poNo",dci.getCurrentRow().getAttribute("PoNo"));
//        op.getParamsMap().put("amdNo",dci.getCurrentRow().getAttribute("AmdNo"));
//        op.execute();
//        
//        if(op.getResult().equals("SHOW_NOT_NULL")){
//            
//            FacesMessage Message = new FacesMessage("PO Closed Successfully");   
//            Message.setSeverity(FacesMessage.SEVERITY_INFO);   
//            FacesContext fc = FacesContext.getCurrentInstance();   
//            fc.addMessage(null, Message);
//        
//        }
//        
////        System.out.println("PoNo: "+dci.getCurrentRow().getAttribute("PoNo"));
////        System.out.println("AmdNo: "+dci.getCurrentRow().getAttribute("AmdNo"));
//        
//    }


}
