package terms.mtl.transaction.ui.bean;


import java.lang.reflect.Method;

import java.sql.Date;

import java.sql.Timestamp;

import java.util.ArrayList;

import java.util.Calendar;

import javax.faces.application.ViewHandler;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;


import java.util.List;

import java.util.Map;

import javax.el.ELContext;
import javax.el.ExpressionFactory;

import javax.el.ValueExpression;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.event.ValueChangeEvent;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.model.SelectItem;

import javax.faces.validator.ValidatorException;

import oracle.adf.model.binding.DCIteratorBinding;

import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;

import oracle.adf.view.rich.component.rich.input.RichSelectManyChoice;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;

import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;


import org.apache.myfaces.trinidad.context.RequestContext;

import org.eclipse.persistence.jpa.jpql.parser.DateTime;

import terms.mtl.transaction.model.view.DeliveryScheduleHeadVORowImpl;


public class DeliveryScheduleBean {
    private RichInputText yearBinding;
    private RichSelectOneChoice monthBinding;
    private RichPopup editPopBinding;
    private RichInputComboboxListOfValues preparedByBinding;
    private RichInputComboboxListOfValues itemBinding;
    private String disable = "N";
    private RichInputText schdNoBinding;
    private RichPopup releasepopBinding;
    private RichPopup venpopBinding;
    private String itemvalue;
    private RichOutputText itemOnpopBinding;
    private RichColumn createVendorDeliveryTableBinding;
    private RichOutputText bindingOutputText;
    private RichPanelHeader myPageRootComponent;
    private RichButton headerEditBinding;
    private RichButton detailcreateBinding;
    private RichButton detaildeleteBinding;
    private RichPanelGroupLayout myPageRootComponentPage;
    private RichInputComboboxListOfValues bindUnitCd;
    private RichInputDate bindScheduleDte1;
    private RichInputDate bindScheduleDte2;
    private RichInputComboboxListOfValues bindVendorCde;
    private RichSelectManyChoice bindPo;
    private RichInputText bindScheduleAmnNo;
    private RichInputComboboxListOfValues bindPreparedBy;
    private RichSelectOneChoice bindStatus;
    private RichTable bindDtlDlvySchedule;
    private RichInputComboboxListOfValues bindDtlPoNo;
    private RichInputText bindDtlAmndNo;
    private RichInputComboboxListOfValues bindDtlItemCd;
    private RichInputText bindDtlProcCd;
    private RichInputText bindDtlTotSch;
    private RichInputText bindDtlMinStock;
    private RichInputText bindqty1;
    private RichInputText bindQty2;
    private RichInputText bindQty3;
    private RichInputText bindQty4;
    private RichInputText bindQty5;
    private RichInputText bindQty6;
    private RichInputText bindQty7;
    private RichInputText bindQty8;
    private RichInputText bindQty9;
    private RichInputText bindQty10;
    private RichInputText bindQty11;
    private RichInputText bindQty12;
    private RichInputText bindQty13;
    private RichInputText bindQty14;
    private RichInputText bindQty15;
    private RichInputText bindQty16;
    private RichInputText bindQty17;
    private RichInputText bindQty18;
    private RichInputText bindQty19;
    private RichInputText bindQty20;
    private RichInputText bindQty21;
    private RichInputText bindQty22;
    private RichInputText bindQty23;
    private RichInputText bindQty24;
    private RichInputText bindQty25;
    private RichInputText bindQty26;
    private RichInputText bindQty27;
    private RichInputText bindQty28;
    private RichInputText bindQty29;
    private RichInputText bindQty30;
    private RichInputText bindQty31;
    private RichInputText bindQtyTwo;
    private RichInputText bindQtyThree;
    private RichInputDate scheduleAmdDate;
    private RichInputDate appDateBinding;
    private RichInputComboboxListOfValues appByBinding;
    private RichInputComboboxListOfValues poNoBinding;


    public DeliveryScheduleBean() {
    }

    public void getSchDuleNoAL(ActionEvent actionEvent) {
        String empcd=(String)ADFUtils.resolveExpression("#{pageFlowScope.empCode}") ; 
        System.out.println("empcd"+empcd);
        ADFUtils.setColumnValue("DeliveryScheduleHeadVO1Iterator", "LastUpdatedBy", empcd);
        OperationBinding ob = ADFUtils.findOperation("generateSchDuleNo");
        ob.execute();
        System.out.println("ob==========" + ob.getResult());
        if (ob.getResult() != null && ob.getResult().toString().equalsIgnoreCase("N")) {
            ADFUtils.findOperation("Commit").execute();
            FacesMessage Message = new FacesMessage("Record Update Successfully.");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
        } else if (ob.getResult() != null && ob.getErrors().isEmpty()) {
            ADFUtils.findOperation("Commit").execute();
            FacesMessage Message =
                new FacesMessage("Record Saved Successfully.Delivery schedule No is " + ob.getResult() + ".");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
        }
    }


    public void SchdSubDateVCL(ValueChangeEvent vce) {
        //        // Add event code here...
        //        if (vce.getNewValue() != null) {
        //            Timestamp Date = (Timestamp) vce.getNewValue();
        //
        //            Calendar cal = Calendar.getInstance();
        //            cal.setTime(Date);
        //            //System.out.println("date====="+Date);
        //            // System.out.println("month==="+month);
        //            // int day = cal.get(Calendar.DATE);
        //            //  System.out.println("day==="+day);
        //            // System.out.println("year==="+year);
        //            int year = cal.get(Calendar.YEAR);
        //            yearBinding.setValue(year);
        //            int month = cal.get(Calendar.MONTH) + 1;
        //            if (month == 1) {
        //                monthBinding.setValue("JAN");
        //
        //            } else if (month == 2) {
        //                monthBinding.setValue("FEB");
        //            }
        //
        //            else if (month == 3) {
        //                monthBinding.setValue("MAR");
        //            }
        //
        //            else if (month == 4) {
        //                monthBinding.setValue("APR");
        //            }
        //
        //
        //            else if (month == 5) {
        //                monthBinding.setValue("MAY");
        //            }
        //
        //            else if (month == 6) {
        //                monthBinding.setValue("JUN");
        //            }
        //
        //            else if (month == 7) {
        //                monthBinding.setValue("JUL");
        //            }
        //
        //            else if (month == 8) {
        //                monthBinding.setValue("AUG");
        //            }
        //
        //            else if (month == 9) {
        //                monthBinding.setValue("SEP");
        //            } else if (month == 10) {
        //                monthBinding.setValue("OCT");
        //            }
        //
        //            else if (month == 11) {
        //                monthBinding.setValue("NOV");
        //            }
        //
        //            else if (month == 12) {
        //                monthBinding.setValue("DEC");
        //            }
        //
        //
        //        }
    }

    public void setYearBinding(RichInputText yearBinding) {
        this.yearBinding = yearBinding;
    }

    public RichInputText getYearBinding() {
        return yearBinding;
    }

    public void setMonthBinding(RichSelectOneChoice monthBinding) {
        this.monthBinding = monthBinding;
    }

    public RichSelectOneChoice getMonthBinding() {
        return monthBinding;
    }

    public void addDelveryDetails(ActionEvent actionEvent) {

        System.out.println("it is inside");
        ADFUtils.findOperation("CreateInsert1").execute();
        checkDaysInMonth();
        AdfFacesContext.getCurrentInstance().addPartialTarget(bindDtlDlvySchedule);
        OperationBinding ob = ADFUtils.findOperation("setSchDuleNo");
        ob.execute();
    }


    public void editDeliverySchheadAL(ActionEvent actionEvent) {
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        getEditPopBinding().show(hints);

    }

    public void setEditPopBinding(RichPopup editPopBinding) {
        this.editPopBinding = editPopBinding;
    }

    public RichPopup getEditPopBinding() {
        return editPopBinding;
    }


    public void setPreparedByBinding(RichInputComboboxListOfValues preparedByBinding) {
        this.preparedByBinding = preparedByBinding;
    }

    public RichInputComboboxListOfValues getPreparedByBinding() {
        return preparedByBinding;
    }

    protected void refreshPage() {

        FacesContext fctx = FacesContext.getCurrentInstance();
        String page = fctx.getViewRoot().getViewId();
        ViewHandler ViewH = fctx.getApplication().getViewHandler();
        UIViewRoot UIV = ViewH.createView(fctx, page);
        UIV.setViewId(page);
        fctx.setViewRoot(UIV);

    }

    public void okPopAL(ActionEvent actionEvent) {
        DCIteratorBinding binding = ADFUtils.findIterator("DeliveryScheduleHeadVO1Iterator");
        if (binding.getEstimatedRowCount() > 0) {
            DeliveryScheduleHeadVORowImpl row = (DeliveryScheduleHeadVORowImpl) binding.getCurrentRow();

            if (row.getAppBy() != null) {
                FacesMessage Message = new FacesMessage("Document Has been Already Approved!!");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            } else {

                Map paramMap = RequestContext.getCurrentInstance().getPageFlowScope();
                paramMap.put("Mode", "E");


                getEditPopBinding().cancel();
                cevmodecheck();

            }
        }
    }

    public void newPopAl(ActionEvent actionEvent) {

        OperationBinding op1 = ADFUtils.findOperation("checkAmdNoVDS");
        Object ob = op1.execute();
        if (op1.getResult().toString() != null && op1.getResult().toString().equalsIgnoreCase("N")) {

            ADFUtils.showMessage("You Can Update Latest Amendment Only.", 2);
            Map paramMap1 = RequestContext.getCurrentInstance().getPageFlowScope();
            paramMap1.put("Mode", "V");
        }
        else{
        OperationBinding binding = ADFUtils.findOperation("CheckApprovedBy");
        binding.execute();
        if (binding.getResult() != null && (binding.getResult().toString().equalsIgnoreCase("-1")) && getAppByBinding().getValue()!=null) {
            ADFUtils.findOperation("generateNewAmdNo").execute();


            System.out.println("bean");
            Map paramMap = RequestContext.getCurrentInstance().getPageFlowScope();
            paramMap.put("Mode", "C");
            getEditPopBinding().cancel();
            cevmodecheck();
            checkDaysInMonth();

        } else {

            FacesMessage Message =
                new FacesMessage("Can Not Create New Document  Because This Document Has Been Not Approved !!");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
        }
        }
    }

    public void CanPopAL(ActionEvent actionEvent) {
        getEditPopBinding().cancel();
    }

    public void populateopITemAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("populateAllDeleveryDetailsRow").execute();
        checkDaysInMonth();
        //refreshPage();
    }


    public void itemVcl(ValueChangeEvent vce) {
        if (vce.getNewValue() != null) {
            //System.out.println("object====" + vce.getNewValue());
            OperationBinding ob = ADFUtils.findOperation("updateItemStoc");
            ob.getParamsMap().put("Item", vce.getNewValue());
            ob.execute();
            AdfFacesContext.getCurrentInstance().addPartialTarget(bindDtlDlvySchedule);
            if (ob.getResult() != null && ob.getResult().toString().equalsIgnoreCase("Y")) {
                Map paramMap = RequestContext.getCurrentInstance().getPageFlowScope();
                paramMap.put("Mode", "V");
                FacesMessage message = new FacesMessage("QTY HAS BEEN REALESD FOR THIS ITEM");
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, message);


            } else if (ob.getResult() != null && ob.getResult().toString().equalsIgnoreCase("K")) {

                FacesMessage Message = new FacesMessage("Some Thing Went Worng ,Fill Proper Date");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            }
        }

    }

    public void setItemBinding(RichInputComboboxListOfValues itemBinding) {
        this.itemBinding = itemBinding;
    }

    public RichInputComboboxListOfValues getItemBinding() {
        return itemBinding;
    }

    public void itemCdValidation(FacesContext facesContext, UIComponent uIComponent, Object object) {
        /*  if(object!=null)
        {
        OperationBinding ob = ADFUtils.findOperation("itemDuplicateValidation");
        ob.getParamsMap().put("item", object);
        ob.execute();
        if (ob.getResult() != null && ob.getResult().toString().equalsIgnoreCase("Y"))
        {
            disable="Y";

            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Item Duplicate", null));
        }
    }  */


    }


    public void setDisable(String disable) {
        this.disable = disable;
    }

    public String getDisable() {
        return disable;
    }


    public void setSchdNoBinding(RichInputText schdNoBinding) {
        this.schdNoBinding = schdNoBinding;
    }

    public RichInputText getSchdNoBinding() {
        return schdNoBinding;
    }

    public void showWeeklyReleasedAL(ActionEvent actionEvent) {
        //System.out.println("in method");
        ADFUtils.findOperation("showWeeklyReleasedDate").execute();
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        this.getReleasepopBinding().show(hints);
    }


    public void setReleasepopBinding(RichPopup releasepopBinding) {
        this.releasepopBinding = releasepopBinding;
    }

    public RichPopup getReleasepopBinding() {
        return releasepopBinding;
    }

    public void venPopAL(ActionEvent actionEvent) {

        //  System.out.println("itemOnpopBinding===="+itemOnpopBinding.getValue());
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        this.getVenpopBinding().show(hints);
        // System.out.println("in method");
        OperationBinding binding = ADFUtils.findOperation("showWeeklyReleasedvendor");
        binding.getParamsMap().put("item", itemOnpopBinding.getValue());
        binding.execute();
        itemvalue = (String) itemOnpopBinding.getValue();
        // System.out.println("itemvalue"+itemvalue);


    }

    public void setVenpopBinding(RichPopup venpopBinding) {
        this.venpopBinding = venpopBinding;
    }

    public RichPopup getVenpopBinding() {
        return venpopBinding;
    }

    public void setItemOnpopBinding(RichOutputText itemOnpopBinding) {
        this.itemOnpopBinding = itemOnpopBinding;
    }

    public RichOutputText getItemOnpopBinding() {
        return itemOnpopBinding;
    }


    public void setItemvalue(String itemvalue) {
        this.itemvalue = itemvalue;
    }

    public String getItemvalue() {
        return itemvalue;
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(createVendorDeliveryTableBinding);
    }

    public void setCreateVendorDeliveryTableBinding(RichColumn createVendorDeliveryTableBinding) {
        this.createVendorDeliveryTableBinding = createVendorDeliveryTableBinding;
    }

    public RichColumn getCreateVendorDeliveryTableBinding() {
        return createVendorDeliveryTableBinding;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setMyPageRootComponent(RichPanelHeader myPageRootComponent) {
        this.myPageRootComponent = myPageRootComponent;
    }

    public RichPanelHeader getMyPageRootComponent() {
        return myPageRootComponent;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    private void cevmodecheck() {
        System.out.println("value for mode --->> " + ADFUtils.resolveExpression("#{pageFlowScope.Mode}"));
        if (ADFUtils.resolveExpression("#{pageFlowScope.Mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getMyPageRootComponentPage(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.Mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getMyPageRootComponentPage(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.Mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getMyPageRootComponentPage(), false);
            cevModeDisableComponent("E");
        }
    }

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }

    public void cevModeDisableComponent(String mode) {
        if (mode.equals("E")) {
            getAppDateBinding().setDisabled(true);
            getScheduleAmdDate().setDisabled(true);
            getBindDtlDlvySchedule().setDisableColumnReordering(true);
            getPoNoBinding().setDisabled(true);
            if (getBindPreparedBy().getValue() != null)
                getBindPreparedBy().setDisabled(true);
            getBindDtlTotSch().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            getBindScheduleAmnNo().setDisabled(true);
            getBindScheduleDte1().setDisabled(true);
            getBindScheduleDte2().setDisabled(true);
            getBindStatus().setDisabled(true);
            getBindUnitCd().setDisabled(true);
            getBindVendorCde().setDisabled(true);
            getSchdNoBinding().setDisabled(true);
            getDetailcreateBinding().setDisabled(false);
            getDetaildeleteBinding().setDisabled(false);
            getBindDtlAmndNo().setDisabled(true);
            getBindDtlItemCd().setDisabled(true);
            getBindDtlMinStock().setDisabled(true);
            getBindDtlPoNo().setDisabled(true);
            getBindDtlProcCd().setDisabled(true);
            getBindDtlTotSch().setDisabled(true);
            getBindqty1().setDisabled(true);
            getBindQtyTwo().setDisabled(true);
            getBindQtyThree().setDisabled(true);
            getBindQty4().setDisabled(true);
            getBindQty5().setDisabled(true);
            getBindQty6().setDisabled(true);
            getBindQty7().setDisabled(true);
            getBindQty8().setDisabled(true);
            getBindQty9().setDisabled(true);
            getBindQty10().setDisabled(true);
            getBindQty11().setDisabled(true);
            getBindQty12().setDisabled(true);
            getBindQty13().setDisabled(true);
            getBindQty14().setDisabled(true);
            getBindQty15().setDisabled(true);
            getBindQty16().setDisabled(true);
            getBindQty17().setDisabled(true);
            getBindQty18().setDisabled(true);
            getBindQty19().setDisabled(true);
            getBindQty20().setDisabled(true);
            getBindQty21().setDisabled(true);
            getBindQty22().setDisabled(true);
            getBindQty23().setDisabled(true);
            getBindQty24().setDisabled(true);
            getBindQty25().setDisabled(true);
            getBindQty26().setDisabled(true);
            getBindQty27().setDisabled(true);
            getBindQty28().setDisabled(true);
            getBindQty29().setDisabled(true);
            getBindQty30().setDisabled(true);
            getBindQty31().setDisabled(true);

            getMonthBinding().setDisabled(true);
            getYearBinding().setDisabled(true);
            // refreshPage();
        } else if (mode.equals("C")) {
            getAppDateBinding().setDisabled(true);
            getAppByBinding().setDisabled(true);
            getBindPreparedBy().setDisabled(true);
            getScheduleAmdDate().setDisabled(true);
            getBindStatus().setDisabled(true);
            getBindDtlAmndNo().setDisabled(true);
            getBindScheduleAmnNo().setDisabled(true);
            getSchdNoBinding().setDisabled(true);
            getBindScheduleDte1().setDisabled(true);
            getBindDtlTotSch().setDisabled(true);
            getBindUnitCd().setDisabled(true);
            getDetailcreateBinding().setDisabled(false);
            getDetaildeleteBinding().setDisabled(false);
            // refreshPage();
        } else if (mode.equals("V")) {
            getDetailcreateBinding().setDisabled(true);
            getDetaildeleteBinding().setDisabled(true);
            // refreshPage();
        }
    }

    public void setDetailcreateBinding(RichButton detailcreateBinding) {
        this.detailcreateBinding = detailcreateBinding;
    }

    public RichButton getDetailcreateBinding() {
        return detailcreateBinding;
    }

    public void setDetaildeleteBinding(RichButton detaildeleteBinding) {
        this.detaildeleteBinding = detaildeleteBinding;
    }

    public RichButton getDetaildeleteBinding() {
        return detaildeleteBinding;
    }

    public void setMyPageRootComponentPage(RichPanelGroupLayout myPageRootComponentPage) {
        this.myPageRootComponentPage = myPageRootComponentPage;
    }

    public RichPanelGroupLayout getMyPageRootComponentPage() {
        return myPageRootComponentPage;
    }

    public void setBindUnitCd(RichInputComboboxListOfValues bindUnitCd) {
        this.bindUnitCd = bindUnitCd;
    }

    public RichInputComboboxListOfValues getBindUnitCd() {
        return bindUnitCd;
    }

    public void setBindScheduleDte1(RichInputDate bindScheduleDte1) {
        this.bindScheduleDte1 = bindScheduleDte1;
    }

    public RichInputDate getBindScheduleDte1() {
        return bindScheduleDte1;
    }

    public void setBindScheduleDte2(RichInputDate bindScheduleDte2) {
        this.bindScheduleDte2 = bindScheduleDte2;
    }

    public RichInputDate getBindScheduleDte2() {
        return bindScheduleDte2;
    }

    public void setBindVendorCde(RichInputComboboxListOfValues bindVendorCde) {
        this.bindVendorCde = bindVendorCde;
    }

    public RichInputComboboxListOfValues getBindVendorCde() {
        return bindVendorCde;
    }

    public void setBindPo(RichSelectManyChoice bindPo) {
        this.bindPo = bindPo;
    }

    public RichSelectManyChoice getBindPo() {
        return bindPo;
    }

    public void setBindScheduleAmnNo(RichInputText bindScheduleAmnNo) {
        this.bindScheduleAmnNo = bindScheduleAmnNo;
    }

    public RichInputText getBindScheduleAmnNo() {
        return bindScheduleAmnNo;
    }

    public void setBindPreparedBy(RichInputComboboxListOfValues bindPreparedBy) {
        this.bindPreparedBy = bindPreparedBy;
    }

    public RichInputComboboxListOfValues getBindPreparedBy() {
        return bindPreparedBy;
    }

    public void setBindStatus(RichSelectOneChoice bindStatus) {
        this.bindStatus = bindStatus;
    }

    public RichSelectOneChoice getBindStatus() {
        return bindStatus;
    }

    public void setBindDtlDlvySchedule(RichTable bindDtlDlvySchedule) {
        this.bindDtlDlvySchedule = bindDtlDlvySchedule;
    }

    public RichTable getBindDtlDlvySchedule() {
        return bindDtlDlvySchedule;
    }

    public void setBindDtlPoNo(RichInputComboboxListOfValues bindDtlPoNo) {
        this.bindDtlPoNo = bindDtlPoNo;
    }

    public RichInputComboboxListOfValues getBindDtlPoNo() {
        return bindDtlPoNo;
    }

    public void setBindDtlAmndNo(RichInputText bindDtlAmndNo) {
        this.bindDtlAmndNo = bindDtlAmndNo;
    }

    public RichInputText getBindDtlAmndNo() {
        return bindDtlAmndNo;
    }

    public void setBindDtlItemCd(RichInputComboboxListOfValues bindDtlItemCd) {
        this.bindDtlItemCd = bindDtlItemCd;
    }

    public RichInputComboboxListOfValues getBindDtlItemCd() {
        return bindDtlItemCd;
    }

    public void setBindDtlProcCd(RichInputText bindDtlProcCd) {
        this.bindDtlProcCd = bindDtlProcCd;
    }

    public RichInputText getBindDtlProcCd() {
        return bindDtlProcCd;
    }

    public void setBindDtlTotSch(RichInputText bindDtlTotSch) {
        this.bindDtlTotSch = bindDtlTotSch;
    }

    public RichInputText getBindDtlTotSch() {
        return bindDtlTotSch;
    }

    public void setBindDtlMinStock(RichInputText bindDtlMinStock) {
        this.bindDtlMinStock = bindDtlMinStock;
    }

    public RichInputText getBindDtlMinStock() {
        return bindDtlMinStock;
    }

    public void setBindqty1(RichInputText bindqty1) {
        this.bindqty1 = bindqty1;
    }

    public RichInputText getBindqty1() {
        return bindqty1;
    }

    public void setBindQty2(RichInputText bindQty2) {
        this.bindQty2 = bindQty2;
    }

    public RichInputText getBindQty2() {
        return bindQty2;
    }

    public void setBindQty3(RichInputText bindQty3) {
        this.bindQty3 = bindQty3;
    }

    public RichInputText getBindQty3() {
        return bindQty3;
    }

    public void setBindQty4(RichInputText bindQty4) {
        this.bindQty4 = bindQty4;
    }

    public RichInputText getBindQty4() {
        return bindQty4;
    }

    public void setBindQty5(RichInputText bindQty5) {
        this.bindQty5 = bindQty5;
    }

    public RichInputText getBindQty5() {
        return bindQty5;
    }

    public void setBindQty6(RichInputText bindQty6) {
        this.bindQty6 = bindQty6;
    }

    public RichInputText getBindQty6() {
        return bindQty6;
    }

    public void setBindQty7(RichInputText bindQty7) {
        this.bindQty7 = bindQty7;
    }

    public RichInputText getBindQty7() {
        return bindQty7;
    }

    public void setBindQty8(RichInputText bindQty8) {
        this.bindQty8 = bindQty8;
    }

    public RichInputText getBindQty8() {
        return bindQty8;
    }

    public void setBindQty9(RichInputText bindQty9) {
        this.bindQty9 = bindQty9;
    }

    public RichInputText getBindQty9() {
        return bindQty9;
    }

    public void setBindQty10(RichInputText bindQty10) {
        this.bindQty10 = bindQty10;
    }

    public RichInputText getBindQty10() {
        return bindQty10;
    }

    public void setBindQty11(RichInputText bindQty11) {
        this.bindQty11 = bindQty11;
    }

    public RichInputText getBindQty11() {
        return bindQty11;
    }

    public void setBindQty12(RichInputText bindQty12) {
        this.bindQty12 = bindQty12;
    }

    public RichInputText getBindQty12() {
        return bindQty12;
    }

    public void setBindQty13(RichInputText bindQty13) {
        this.bindQty13 = bindQty13;
    }

    public RichInputText getBindQty13() {
        return bindQty13;
    }

    public void setBindQty14(RichInputText bindQty14) {
        this.bindQty14 = bindQty14;
    }

    public RichInputText getBindQty14() {
        return bindQty14;
    }

    public void setBindQty15(RichInputText bindQty15) {
        this.bindQty15 = bindQty15;
    }

    public RichInputText getBindQty15() {
        return bindQty15;
    }

    public void setBindQty16(RichInputText bindQty16) {
        this.bindQty16 = bindQty16;
    }

    public RichInputText getBindQty16() {
        return bindQty16;
    }

    public void setBindQty17(RichInputText bindQty17) {
        this.bindQty17 = bindQty17;
    }

    public RichInputText getBindQty17() {
        return bindQty17;
    }

    public void setBindQty18(RichInputText bindQty18) {
        this.bindQty18 = bindQty18;
    }

    public RichInputText getBindQty18() {
        return bindQty18;
    }

    public void setBindQty19(RichInputText bindQty19) {
        this.bindQty19 = bindQty19;
    }

    public RichInputText getBindQty19() {
        return bindQty19;
    }

    public void setBindQty20(RichInputText bindQty20) {
        this.bindQty20 = bindQty20;
    }

    public RichInputText getBindQty20() {
        return bindQty20;
    }

    public void setBindQty21(RichInputText bindQty21) {
        this.bindQty21 = bindQty21;
    }

    public RichInputText getBindQty21() {
        return bindQty21;
    }

    public void setBindQty22(RichInputText bindQty22) {
        this.bindQty22 = bindQty22;
    }

    public RichInputText getBindQty22() {
        return bindQty22;
    }

    public void setBindQty23(RichInputText bindQty23) {
        this.bindQty23 = bindQty23;
    }

    public RichInputText getBindQty23() {
        return bindQty23;
    }

    public void setBindQty24(RichInputText bindQty24) {
        this.bindQty24 = bindQty24;
    }

    public RichInputText getBindQty24() {
        return bindQty24;
    }

    public void setBindQty25(RichInputText bindQty25) {
        this.bindQty25 = bindQty25;
    }

    public RichInputText getBindQty25() {
        return bindQty25;
    }

    public void setBindQty26(RichInputText bindQty26) {
        this.bindQty26 = bindQty26;
    }

    public RichInputText getBindQty26() {
        return bindQty26;
    }

    public void setBindQty27(RichInputText bindQty27) {
        this.bindQty27 = bindQty27;
    }

    public RichInputText getBindQty27() {
        return bindQty27;
    }

    public void setBindQty28(RichInputText bindQty28) {
        this.bindQty28 = bindQty28;
    }

    public RichInputText getBindQty28() {
        return bindQty28;
    }

    public void setBindQty29(RichInputText bindQty29) {
        this.bindQty29 = bindQty29;
    }

    public RichInputText getBindQty29() {
        return bindQty29;
    }

    public void setBindQty30(RichInputText bindQty30) {
        this.bindQty30 = bindQty30;
    }

    public RichInputText getBindQty30() {
        return bindQty30;
    }

    public void setBindQty31(RichInputText bindQty31) {
        this.bindQty31 = bindQty31;
    }

    public RichInputText getBindQty31() {
        return bindQty31;
    }

    public void setBindQtyTwo(RichInputText bindQtyTwo) {
        this.bindQtyTwo = bindQtyTwo;
    }

    public RichInputText getBindQtyTwo() {
        return bindQtyTwo;
    }

    public void setBindQtyThree(RichInputText bindQtyThree) {
        this.bindQtyThree = bindQtyThree;
    }

    public RichInputText getBindQtyThree() {
        return bindQtyThree;
    }

    public void yearValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (object != null) {
            Timestamp Date = (Timestamp) bindScheduleDte1.getValue();
            Calendar cal = Calendar.getInstance();
            cal.setTime(Date);
            int year = cal.get(Calendar.YEAR);
            if ((Integer) object < year) {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                              "Year should be greater than or equals to Schedule Date Year",
                                                              null));
            }
        }

    }

    public void monthValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (object != null && yearBinding.getValue() != null) {
            Timestamp Date = (Timestamp) bindScheduleDte1.getValue();
            Calendar cal = Calendar.getInstance();
            cal.setTime(Date);
            int month = cal.get(Calendar.MONTH) + 1;
            int year = cal.get(Calendar.YEAR);
            int MonthCase = 0;

            if (object.toString().equals("JAN")) {
                MonthCase = 1;
            } else if (object.toString().equals("FEB")) {
                MonthCase = 2;
            } else if (object.toString().equals("MAR")) {
                MonthCase = 3;
            } else if (object.toString().equals("APR")) {
                MonthCase = 4;
            } else if (object.toString().equals("MAY")) {
                MonthCase = 5;
            } else if (object.toString().equals("JUN")) {
                MonthCase = 6;
            } else if (object.toString().equals("JUL")) {
                MonthCase = 7;
            } else if (object.toString().equals("AUG")) {
                MonthCase = 8;
            } else if (object.toString().equals("SEP")) {
                MonthCase = 9;
            } else if (object.toString().equals("OCT")) {
                MonthCase = 10;
            } else if (object.toString().equals("NOV")) {
                MonthCase = 11;
            } else if (object.toString().equals("DEC")) {
                MonthCase = 12;
            } else {
                System.out.println("No Month Found");
            }
            System.out.println("Month Case is=>" + MonthCase);
            if (MonthCase < month && (Integer) yearBinding.getValue() > year) {
                //throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Month should be greater than or equals to Schedule Date Month", null));

            } else if (MonthCase < month && (Integer) yearBinding.getValue() <= year) {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                              "Month should be greater than or equals to Schedule Date Month",
                                                              null));
            }
        }

    }

    public void setScheduleAmdDate(RichInputDate scheduleAmdDate) {
        this.scheduleAmdDate = scheduleAmdDate;
    }

    public RichInputDate getScheduleAmdDate() {
        return scheduleAmdDate;
    }


    public void checkDaysInMonth() {
        if (monthBinding.getValue() != null) {
            getMonthBinding().setDisabled(true);
            getYearBinding().setDisabled(true);
            if (monthBinding.getValue().equals("JAN") || monthBinding.getValue().equals("MAR") ||
                monthBinding.getValue().equals("MAY") || monthBinding.getValue().equals("JUL") ||
                monthBinding.getValue().equals("AUG") || monthBinding.getValue().equals("OCT") ||
                monthBinding.getValue().equals("DEC")) {
                bindQty31.setDisabled(false);
            } else if (monthBinding.getValue().equals("APR") || monthBinding.getValue().equals("JUN") ||
                       monthBinding.getValue().equals("SEP") || monthBinding.getValue().equals("NOV")) {
                bindQty31.setDisabled(true);

            } else if (monthBinding.getValue().equals("FEB")) {

                int Check = 0;
                int year = (Integer) yearBinding.getValue();
                if ((year % 4 == 0) && year % 100 != 0) {
                    Check = 1;
                    System.out.println("Is leap Year" + year);
                }

                else {
                    Check = 0;

                }
                if (Check == 1) {
                    bindQty29.setDisabled(false);
                    bindQty30.setDisabled(true);
                    bindQty31.setDisabled(true);
                } else {
                    bindQty29.setDisabled(true);
                    bindQty30.setDisabled(true);
                    bindQty31.setDisabled(true);

                }

            }

        }
    }

    public void setAppDateBinding(RichInputDate appDateBinding) {
        this.appDateBinding = appDateBinding;
    }

    public RichInputDate getAppDateBinding() {
        return appDateBinding;
    }

    public void setAppByBinding(RichInputComboboxListOfValues appByBinding) {
        this.appByBinding = appByBinding;
    }

    public RichInputComboboxListOfValues getAppByBinding() {
        return appByBinding;
    }

    public void setPoNoBinding(RichInputComboboxListOfValues poNoBinding) {
        this.poNoBinding = poNoBinding;
    }

    public RichInputComboboxListOfValues getPoNoBinding() {
        return poNoBinding;
    }
}
