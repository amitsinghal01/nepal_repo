package terms.mtl.transaction.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichPanelLabelAndMessage;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

public class DepartmentQualityCheckBean {
    private RichPanelHeader myRootBinding;
    private RichButton populateButtonBinding;
    private RichInputComboboxListOfValues unitBinding;
    private RichInputText entryNoBinding;
    private RichInputDate entryDateBinding;
    private RichInputComboboxListOfValues grnNoBinding;
    private RichInputDate grnDateBinding;
    private RichInputComboboxListOfValues checkedByBinding;
    private RichSelectOneChoice inspectionTypeBinding;
    private RichOutputText editTransbinding;
    private RichInputText reasonBinding;
    private RichInputText rejectedBinding;
    private RichInputText acceptedBinding;
    private RichInputText receivedBinding;
    private RichInputText procSRNBinding;
    private RichInputText procSBinding;
    private RichInputText procNBinding;
    private RichInputText uomBinding;
    private RichInputText poNoBinding;
    private RichInputText storeBinding;
    private RichInputText itemBinding;

    public DepartmentQualityCheckBean() {
    }

    public void setMyRootBinding(RichPanelHeader myRootBinding) {
        this.myRootBinding = myRootBinding;
    }

    public RichPanelHeader getMyRootBinding() {
        return myRootBinding;
    }

    public RichButton getPopulateButtonBinding() {
        return populateButtonBinding;
    }

    public void setUnitBinding(RichInputComboboxListOfValues unitBinding) {
        this.unitBinding = unitBinding;
    }

    public RichInputComboboxListOfValues getUnitBinding() {
        return unitBinding;
    }

    public void setEntryNoBinding(RichInputText entryNoBinding) {
        this.entryNoBinding = entryNoBinding;
    }

    public RichInputText getEntryNoBinding() {
        return entryNoBinding;
    }

    public void setEntryDateBinding(RichInputDate entryDateBinding) {
        this.entryDateBinding = entryDateBinding;
    }

    public RichInputDate getEntryDateBinding() {
        return entryDateBinding;
    }

    public void setGrnNoBinding(RichInputComboboxListOfValues grnNoBinding) {
        this.grnNoBinding = grnNoBinding;
    }

    public RichInputComboboxListOfValues getGrnNoBinding() {
        return grnNoBinding;
    }

    public void setGrnDateBinding(RichInputDate grnDateBinding) {
        this.grnDateBinding = grnDateBinding;
    }

    public RichInputDate getGrnDateBinding() {
        return grnDateBinding;
    }
    
    public void setInspectionTypeBinding(RichSelectOneChoice inspectionTypeBinding) {
        this.inspectionTypeBinding = inspectionTypeBinding;
    }

    public RichSelectOneChoice getInspectionTypeBinding() {
        return inspectionTypeBinding;
    }

    public void setCheckedByBinding(RichInputComboboxListOfValues checkedByBinding) {
        this.checkedByBinding = checkedByBinding;
    }

    public RichInputComboboxListOfValues getCheckedByBinding() {
        return checkedByBinding;
    }

    public void setEditTransbinding(RichOutputText editTransbinding) {
        this.editTransbinding = editTransbinding;
    }

    public RichOutputText getEditTransbinding() {
        cevmodecheck();
        return editTransbinding;
    }

    public void setPopulateButtonBinding(RichButton populateButtonBinding) {
        this.populateButtonBinding = populateButtonBinding;
    }

    public void setReasonBinding(RichInputText reasonBinding) {
        this.reasonBinding = reasonBinding;
    }

    public RichInputText getReasonBinding() {
        return reasonBinding;
    }

    public void setRejectedBinding(RichInputText rejectedBinding) {
        this.rejectedBinding = rejectedBinding;
    }

    public RichInputText getRejectedBinding() {
        return rejectedBinding;
    }

    public void setAcceptedBinding(RichInputText acceptedBinding) {
        this.acceptedBinding = acceptedBinding;
    }

    public RichInputText getAcceptedBinding() {
        return acceptedBinding;
    }

    public void setReceivedBinding(RichInputText receivedBinding) {
        this.receivedBinding = receivedBinding;
    }

    public RichInputText getReceivedBinding() {
        return receivedBinding;
    }

    public void setProcSRNBinding(RichInputText procSRNBinding) {
        this.procSRNBinding = procSRNBinding;
    }

    public RichInputText getProcSRNBinding() {
        return procSRNBinding;
    }

    public void setProcSBinding(RichInputText procSBinding) {
        this.procSBinding = procSBinding;
    }

    public RichInputText getProcSBinding() {
        return procSBinding;
    }

    public void setProcNBinding(RichInputText procNBinding) {
        this.procNBinding = procNBinding;
    }

    public RichInputText getProcNBinding() {
        return procNBinding;
    }

    public void setUomBinding(RichInputText uomBinding) {
        this.uomBinding = uomBinding;
    }

    public RichInputText getUomBinding() {
        return uomBinding;
    }

    public void setPoNoBinding(RichInputText poNoBinding) {
        this.poNoBinding = poNoBinding;
    }

    public RichInputText getPoNoBinding() {
        return poNoBinding;
    }

    public void setStoreBinding(RichInputText storeBinding) {
        this.storeBinding = storeBinding;
    }

    public RichInputText getStoreBinding() {
        return storeBinding;
    }

    public void setItemBinding(RichInputText itemBinding) {
        this.itemBinding = itemBinding;
    }

    public RichInputText getItemBinding() {
        return itemBinding;
    }
    
    private void cevmodecheck(){
         if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
             makeComponentHierarchyReadOnly(this.getMyRootBinding(), true);
             cevModeDisableComponent("V");
         }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
             makeComponentHierarchyReadOnly(this.getMyRootBinding(), false);
             cevModeDisableComponent("C");
         }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
             makeComponentHierarchyReadOnly(this.getMyRootBinding(), false);
             cevModeDisableComponent("E");
         }
     }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                try {
                    Method method1 =
                        component.getClass().getMethod("setDisabled", boolean.class);
                    if (method1 != null) {
                        method1.invoke(component, valueComponent);
                    }
                } catch (NoSuchMethodException e) {
                    try {
                        Method method =
                            component.getClass().getMethod("setReadOnly", boolean.class);
                        if (method != null) {
                            method.invoke(component, valueComponent);
                        }
                    } catch (Exception e1) {
                        // e.printStackTrace();//silently eat this exception.
                    }


                } catch (Exception e) {
                    // e.printStackTrace();//silently eat this exception.
                }
                List<UIComponent> childComponents = component.getChildren();
                for (UIComponent comp : childComponents) {
                    makeComponentHierarchyReadOnly(comp, valueComponent);
                }
            }

        public void cevModeDisableComponent(String mode) {
            if (mode.equals("C")) {
                //Head
                unitBinding.setDisabled(true);
                entryNoBinding.setDisabled(true);
                entryDateBinding.setDisabled(true);
                grnNoBinding.setDisabled(false);
                grnDateBinding.setDisabled(true);
                inspectionTypeBinding.setDisabled(false);
                checkedByBinding.setDisabled(true);
                //Detail
                itemBinding.setDisabled(true);
                storeBinding.setDisabled(true);
                poNoBinding.setDisabled(true);
                uomBinding.setDisabled(true);
                procNBinding.setDisabled(true);
                procSBinding.setDisabled(true);
                procSRNBinding.setDisabled(true);
                receivedBinding.setDisabled(true);
                acceptedBinding.setDisabled(false);
                rejectedBinding.setDisabled(true);
                reasonBinding.setDisabled(false);
            }else if (mode.equals("V")) {
                //Head
                unitBinding.setDisabled(true);
                entryNoBinding.setDisabled(true);
                entryDateBinding.setDisabled(true);
                grnNoBinding.setDisabled(true);
                grnDateBinding.setDisabled(true);
                inspectionTypeBinding.setDisabled(true);
                checkedByBinding.setDisabled(true); 
                //Detail
                itemBinding.setDisabled(true);
                storeBinding.setDisabled(true);
                poNoBinding.setDisabled(true);
                uomBinding.setDisabled(true);
                procNBinding.setDisabled(true);
                procSBinding.setDisabled(true);
                procSRNBinding.setDisabled(true);
                receivedBinding.setDisabled(true);
                acceptedBinding.setDisabled(true);
                rejectedBinding.setDisabled(true);
                reasonBinding.setDisabled(true);
            }else if (mode.equals("E")) {
                //Head
                unitBinding.setDisabled(true);
                entryNoBinding.setDisabled(true);
                entryDateBinding.setDisabled(true);
                grnNoBinding.setDisabled(true);
                grnDateBinding.setDisabled(true);
                inspectionTypeBinding.setDisabled(true);
                checkedByBinding.setDisabled(true);
                //Detail
                itemBinding.setDisabled(true);
                storeBinding.setDisabled(true);
                poNoBinding.setDisabled(true);
                uomBinding.setDisabled(true);
                procNBinding.setDisabled(true);
                procSBinding.setDisabled(true);
                procSRNBinding.setDisabled(true);
                receivedBinding.setDisabled(true);
                acceptedBinding.setDisabled(false);
                rejectedBinding.setDisabled(true);
                reasonBinding.setDisabled(false);
            }
        }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void saveAL(ActionEvent actionEvent) {
        Integer TransValue=(Integer)editTransbinding.getValue();
            System.out.println("EDIT TRANS VALUE "+TransValue);
        ADFUtils.setLastUpdatedBy("DepartmentQualityCheckHeadVO1Iterator","ModifiedBy");
        if(TransValue==0 && entryNoBinding.getValue()==null) {
            ADFUtils.findOperation("getQclEntryNo").execute();
            ADFUtils.findOperation("Commit").execute();
           FacesMessage Message = new FacesMessage("Record Save Successfully, Entry No is: "+entryNoBinding.getValue());   
           Message.setSeverity(FacesMessage.SEVERITY_INFO);   
           FacesContext fc = FacesContext.getCurrentInstance();   
           fc.addMessage(null, Message);      
        }else {
            ADFUtils.findOperation("Commit").execute();
            FacesMessage Message = new FacesMessage("Record Update Successfully");   
            Message.setSeverity(FacesMessage.SEVERITY_INFO);   
            FacesContext fc = FacesContext.getCurrentInstance();   
            fc.addMessage(null, Message);      
        }
    }
    
    public void populateAL(ActionEvent actionEvent) {
        if(unitBinding.getValue()!=null && inspectionTypeBinding.getValue()!=null && grnNoBinding.getValue()!=null){
                ADFUtils.findOperation("populateQCLData").execute();
            }
    }

    public void acceptQtyVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(receivedBinding.getValue()!=null && acceptedBinding.getValue()!=null){
                BigDecimal a = (BigDecimal)receivedBinding.getValue()==null?new BigDecimal(0):(BigDecimal)receivedBinding.getValue();
                    System.out.println("Received Quantity in BEAN: "+receivedBinding.getValue());
                BigDecimal b = (BigDecimal)acceptedBinding.getValue()==null?new BigDecimal(0):(BigDecimal)acceptedBinding.getValue();
                    System.out.println("Accepted Quantity in BEAN: "+acceptedBinding.getValue());
            if(a.compareTo(b)==1 || a.compareTo(b)==0 || a.compareTo(b)==-1){
                    System.out.println("Rejected Quantity in BEAN: "+a.subtract(b));
                  rejectedBinding.setValue(a.subtract(b));
            }
        }
    }
}
