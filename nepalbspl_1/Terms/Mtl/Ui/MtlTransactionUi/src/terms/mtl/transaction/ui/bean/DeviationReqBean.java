package terms.mtl.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.el.ELContext;

import javax.el.ExpressionFactory;

import javax.el.ValueExpression;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;
import javax.faces.validator.ValidatorException;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;

import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectBooleanCheckbox;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

public class DeviationReqBean {
    private RichInputComboboxListOfValues srvNoBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText requestNoBinding;
    private RichInputDate requestDateBinding;
    private RichInputComboboxListOfValues approvedByBinding;
    private RichInputDate srvDateBinding;
    private RichInputDate approvedDateBinding;
    private RichInputText qcApproveByBinding;
    private RichSelectOneChoice deviationTypeBinding;
    private RichInputText reoffrQtyBinding;
    private RichOutputText bindingOutputText;
    private RichPanelHeader myPageRootComponent;
    private RichButton headerEditBinding;
    private RichInputComboboxListOfValues preparedByBinding;
    private RichInputComboboxListOfValues vendorCodeBinding;
    private RichSelectBooleanCheckbox cancelBinding;
    private RichTable detailTableBinding;
    private RichInputComboboxListOfValues itemCodeBinding;
    private RichInputText uomBinding;
    private RichInputText rejectQtyBinding;
    private RichInputText reOfferQtyBinding;

    public DeviationReqBean() {
    }

    public void devReqAL(ActionEvent actionEvent) 
    {
          String empcd=(String)ADFUtils.resolveExpression("#{pageFlowScope.empCode}") ; 
          System.out.println("empcd"+empcd);
          ADFUtils.setColumnValue("DeviationRequestHeaderVO1Iterator", "ModifiedBy", empcd);
          
      if ((Long) ADFUtils.evaluateEL("#{bindings.DeviationRequestDetailVO1Iterator.estimatedRowCount}")>0)
      {  
       OperationBinding op = ADFUtils.findOperation("getGenDeviationRequestNo");
                   Object rst = op.execute();
                   //ADFUtils.findOperation("goForUnitRate").execute();

                   
                   System.out.println("--------Commit-------");
                   System.out.println("value aftr getting result--->?"+rst);
                   
                       if (rst.toString() != null && rst.toString() != "") {
                           if (op.getErrors().isEmpty()) {
                               OperationBinding op1= ADFUtils.findOperation("Commit");
                               Object rs=op1.execute();
                                                               
                               FacesMessage Message = new FacesMessage("Record Saved Successfully.Deviation Request No is "+rst+".");   
                               Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                               FacesContext fc = FacesContext.getCurrentInstance();   
                               fc.addMessage(null, Message);  
                           }
                       }

                       
                       if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ") {
                           
                          
                           
                           if (op.getErrors().isEmpty()) {
                               
                               
                               
                               OperationBinding qc= ADFUtils.findOperation("setQcApprovedBy");
                               qc.execute();
                               
                               ADFUtils.findOperation("Commit").execute();
                               FacesMessage Message = new FacesMessage("Record Update Successfully.");   
                               Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                               FacesContext fc = FacesContext.getCurrentInstance();   
                               fc.addMessage(null, Message);  
                              
                               
                               
                       
                           }

                       
                       }
      }else{
          ADFUtils.showMessage("Please Enter any record in the detail table.", 0);
      }

      }


    public void srvNumberValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (object != null) {
            String srvNo = object.toString();
            OperationBinding op = ADFUtils.findOperation("srvNumberValidator");
            op.getParamsMap().put("srvNo", srvNo);
            op.execute();
            if (op.getResult() != null) {
                String rs = op.getResult().toString();
//                if ("Y".equalsIgnoreCase(rs)) {
//                    throw new ValidatorException(new FacesMessage("Duplicate Record", "Srv No. already exists!!"));
//
//                }
                if ("B".equalsIgnoreCase(rs)) {
                    throw new ValidatorException(new FacesMessage("Srv No. is required.",""));
                }
            }
        }


    }

//    public void deviationReqDtlCrtInstAL(ActionEvent actionEvent) {
//           System.out.println("Mode:"+ADFUtils.resolveExpression("#{pageFlowScope.mode}"));
//       
//           if(unitCodeBinding.getValue()!=null && unitCodeBinding.getValue().toString().length()>0){
//        
//           if (srvNoBinding.getValue() == null || srvNoBinding.getValue().equals("")) {
//           FacesMessage message = new FacesMessage("Please enter Srv No.");
//            message.setSeverity(FacesMessage.SEVERITY_ERROR);
//            FacesContext context = FacesContext.getCurrentInstance();
//            context.addMessage(srvNoBinding.getClientId(), message);
//   
//        } else {
//            OperationBinding binding = ADFUtils.findOperation("getGenDeviationRequestNo");
//            Object ob = binding.execute();
//
//            OperationBinding binding1 = ADFUtils.findOperation("CreateInsert");
//            binding1.execute();
//            if(binding.getResult()!=null)
//              {
//                String rs= binding.getResult().toString();
//                if("Z".equalsIgnoreCase(rs))
//                {
//                FacesMessage message = new FacesMessage("Please select item Code");
//                message.setSeverity(FacesMessage.SEVERITY_ERROR);
//                FacesContext context = FacesContext.getCurrentInstance();
//                context.addMessage(null, message);
//            
//                    }
//                }
//        }
//     
//   
//    }
//           else
//           {FacesMessage message = new FacesMessage("Please enter Unit Code.");
//            message.setSeverity(FacesMessage.SEVERITY_ERROR);
//            FacesContext context = FacesContext.getCurrentInstance();
//            context.addMessage(unitCodeBinding.getClientId(), message);}
//    }




    public void setSrvNoBinding(RichInputComboboxListOfValues srvNoBinding) {
        this.srvNoBinding = srvNoBinding;
    }

    public RichInputComboboxListOfValues getSrvNoBinding() {
        return srvNoBinding;
    }


    public void approveByVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());            
                     OperationBinding ob = ADFUtils.findOperation("checkApprovalStatus");
                    ob.getParamsMap().put("formNm", "DVR");
                    ob.getParamsMap().put("authoLim", "AP");
                    ob.getParamsMap().put("empcd", vce.getNewValue().toString());
                    ob.execute();
                    System.out.println(" EMP CD"+vce.getNewValue().toString());
                    if((ob.getResult() !=null && ob.getResult().equals("Y")))
                    {
                        Row row=(Row)ADFUtils.evaluateEL("#{bindings.DeviationRequestHeaderVO1Iterator.currentRow}");
                        row.setAttribute("ApprovedBy", null);
                        ADFUtils.showMessage("You Don't Have Permission To Approve This Record.",0);
                    }

        

        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
    }


    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }


    public void setRequestNoBinding(RichInputText requestNoBinding) {
        this.requestNoBinding = requestNoBinding;
    }

    public RichInputText getRequestNoBinding() {
        return requestNoBinding;
    }

    public void setRequestDateBinding(RichInputDate requestDateBinding) {
        this.requestDateBinding = requestDateBinding;
    }

    public RichInputDate getRequestDateBinding() {
        return requestDateBinding;
    }

    public void setApprovedByBinding(RichInputComboboxListOfValues approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputComboboxListOfValues getApprovedByBinding() {
        return approvedByBinding;
    }

    public void setSrvDateBinding(RichInputDate srvDateBinding) {
        this.srvDateBinding = srvDateBinding;
    }

    public RichInputDate getSrvDateBinding() {
        return srvDateBinding;
    }

    public void setApprovedDateBinding(RichInputDate approvedDateBinding) {
        this.approvedDateBinding = approvedDateBinding;
    }

    public RichInputDate getApprovedDateBinding() {
        return approvedDateBinding;
    }

    public void setQcApproveByBinding(RichInputText qcApproveByBinding) {
        this.qcApproveByBinding = qcApproveByBinding;
    }

    public RichInputText getQcApproveByBinding() {
        return qcApproveByBinding;
    }

    public void setDeviationTypeBinding(RichSelectOneChoice deviationTypeBinding) {
        this.deviationTypeBinding = deviationTypeBinding;
    }

    public RichSelectOneChoice getDeviationTypeBinding() {
        return deviationTypeBinding;
    }

    public void setReoffrQtyBinding(RichInputText reoffrQtyBinding) {
        this.reoffrQtyBinding = reoffrQtyBinding;
    }

    public RichInputText getReoffrQtyBinding() {
        return reoffrQtyBinding;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setMyPageRootComponent(RichPanelHeader myPageRootComponent) {
        this.myPageRootComponent = myPageRootComponent;
    }

    public RichPanelHeader getMyPageRootComponent() {
        return myPageRootComponent;
    }

    public void EditButtonAL(ActionEvent actionEvent) {
        if(approvedByBinding.getValue() !=null){
        if(approvedByBinding.getValue() !=null)
        {
            ADFUtils.showMessage(" You can not update approved deviation request record.", 2);
        }
        else
        {
        cevmodecheck();
        }
        }else{
        
        System.out.println("active status"+cancelBinding.getValue().toString());
        if(cancelBinding.getValue()!=null && cancelBinding.getValue().equals(true)){
            
            ADFUtils.showMessage(" You can not Update Cancelled Deviation Request record.", 2);
        }
        else{
            cevmodecheck();
        }
        }
    }
    
    private void cevmodecheck(){
        System.out.println("in cevmodecheck");
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
            makeComponentHierarchyReadOnly(this.getMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            System.out.println("in E Scope");
            makeComponentHierarchyReadOnly(this.getMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }
    
    public void cevModeDisableComponent(String mode) {
        if (mode.equals("E")) {
            
                getUomBinding().setDisabled(true);
                getRejectQtyBinding().setDisabled(true);
            
            if(approvedByBinding.getValue() !=null)
            {
                getItemCodeBinding().setDisabled(true);
                getReOfferQtyBinding().setDisabled(true);
            }
                getHeaderEditBinding().setDisabled(true);
                getUnitCodeBinding().setDisabled(true);
                getRequestNoBinding().setDisabled(true);
                getRequestDateBinding().setDisabled(true);
                getSrvDateBinding().setDisabled(true);
                getSrvNoBinding().setDisabled(true);
//                getDeviationTypeBinding().setDisabled(true);
                getQcApproveByBinding().setDisabled(true);
                getVendorCodeBinding().setDisabled(false);
                getApprovedDateBinding().setDisabled(true);
                getPreparedByBinding().setDisabled(true);
                
        } else if (mode.equals("C")) {
            
            getUomBinding().setDisabled(true);
            getRejectQtyBinding().setDisabled(true);
            
            getCancelBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            getRequestNoBinding().setDisabled(true);
            getRequestDateBinding().setDisabled(true);
            getSrvDateBinding().setDisabled(true);
            getApprovedByBinding().setDisabled(true);
          
            getQcApproveByBinding().setDisabled(true);
            getApprovedDateBinding().setDisabled(true);
            getVendorCodeBinding().setDisabled(false);
        getPreparedByBinding().setDisabled(true);

        } else if (mode.equals("V")) {
        }
    }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
            try {
                Method method1 =
                    component.getClass().getMethod("setDisabled", boolean.class);
                if (method1 != null) {
                    method1.invoke(component, valueComponent);
                }
            } catch (NoSuchMethodException e) {
                try {
                    Method method =
                        component.getClass().getMethod("setReadOnly", boolean.class);
                    if (method != null) {
                        method.invoke(component, valueComponent);
                    }
                } catch (Exception e1) {
                    // e.printStackTrace();//silently eat this exception.
                }


            } catch (Exception e) {
                // e.printStackTrace();//silently eat this exception.
            }
            List<UIComponent> childComponents = component.getChildren();
            for (UIComponent comp : childComponents) {
                makeComponentHierarchyReadOnly(comp, valueComponent);
            }
        }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void unitCodeVCE(ValueChangeEvent valueChangeEvent) {
        if(unitCodeBinding.getValue()!=null){
            getSrvNoBinding().setDisabled(false);
        }else{
            getSrvNoBinding().setDisabled(true);
        }
        
    }

    public void setPreparedByBinding(RichInputComboboxListOfValues preparedByBinding) {
        this.preparedByBinding = preparedByBinding;
    }

    public RichInputComboboxListOfValues getPreparedByBinding() {
        return preparedByBinding;
    }

    public void setVendorCodeBinding(RichInputComboboxListOfValues vendorCodeBinding) {
        this.vendorCodeBinding = vendorCodeBinding;
    }

    public RichInputComboboxListOfValues getVendorCodeBinding() {
        return vendorCodeBinding;
    }

    public void setCancelBinding(RichSelectBooleanCheckbox cancelBinding) {
        this.cancelBinding = cancelBinding;
    }

    public RichSelectBooleanCheckbox getCancelBinding() {
        return cancelBinding;
    }

    public void createDetailAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert").execute();
        getSrvNoBinding().setDisabled(true);
    }

    public void deleteDialogListenerAL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
        {
            ADFUtils.findOperation("Delete").execute();
            System.out.println("Record Delete Successfully");
       
            FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
            Message.setSeverity(FacesMessage.SEVERITY_INFO);  
            FacesContext fc = FacesContext.getCurrentInstance();  
            fc.addMessage(null, Message);  
       
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(detailTableBinding);
    }

    public void setDetailTableBinding(RichTable detailTableBinding) {
        this.detailTableBinding = detailTableBinding;
    }

    public RichTable getDetailTableBinding() {
        return detailTableBinding;
    }

    public void setItemCodeBinding(RichInputComboboxListOfValues itemCodeBinding) {
        this.itemCodeBinding = itemCodeBinding;
    }

    public RichInputComboboxListOfValues getItemCodeBinding() {
        return itemCodeBinding;
    }

    public void setUomBinding(RichInputText uomBinding) {
        this.uomBinding = uomBinding;
    }

    public RichInputText getUomBinding() {
        return uomBinding;
    }

    public void setRejectQtyBinding(RichInputText rejectQtyBinding) {
        this.rejectQtyBinding = rejectQtyBinding;
    }

    public RichInputText getRejectQtyBinding() {
        return rejectQtyBinding;
    }

    public void setReOfferQtyBinding(RichInputText reOfferQtyBinding) {
        this.reOfferQtyBinding = reOfferQtyBinding;
    }

    public RichInputText getReOfferQtyBinding() {
        return reOfferQtyBinding;
    }
}
