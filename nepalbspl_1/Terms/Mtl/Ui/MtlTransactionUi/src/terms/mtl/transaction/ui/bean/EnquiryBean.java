package terms.mtl.transaction.ui.bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import javax.faces.validator.ValidatorException;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectBooleanCheckbox;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichPanelStretchLayout;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.nav.RichCommandLink;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.adf.view.rich.component.rich.output.RichPanelCollection;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.adf.view.rich.util.ResetUtils;

import oracle.binding.OperationBinding;

import org.apache.myfaces.trinidad.model.UploadedFile;

public class EnquiryBean {


    private RichButton headerEditBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichOutputText bindingOutputText;
    private RichInputText itemDescBinding;
    private RichInputText transItemBinding;
    private RichInputText transVenBinding;

    private RichInputText procBinding;
    private RichInputText procSeqBinding;
   
    private RichInputText enqNoBinding;
    private RichButton dtlCreate1ButtonBinding;
    private RichButton dtlCreateButtonBinding;
    private RichButton dtlDelete1ButtonBinding;
    private RichButton dtlDeleteButtonBinding;
    private RichButton generateBtnBinding;
    private RichButton poVenButtonBinding;
    private RichInputComboboxListOfValues unitBinding;
    private RichTable table1Binding;
    private RichTable table2Binding;
    private RichPanelCollection pcBinding;
    private RichInputText enqNoDtlBinding;
    private RichSelectBooleanCheckbox cancelBinding;
    private RichSelectOneChoice typeBinding;
    private RichInputComboboxListOfValues indentNoBinding;
 
    private RichInputComboboxListOfValues itemCd1Binding;
    String ErrorFlag="false",ErrorFlag1="false";
    private RichShowDetailItem itemDtlTabBinding;
    private RichShowDetailItem enquiryHeaderTabBinding;
    private RichInputDate enquiryDateBinding;
    private RichInputText qtyBinding;
    private RichShowDetailItem refDocTabBinding;
    private RichInputText pathBinding;
    private RichInputText contentType;
    private RichInputText docFileNameBinding;
    private RichInputText docTypeBinding;
    private RichInputDate docDateBinding;
    private RichInputText docNoBinding;
    private RichInputText unitCdBinding;
    private RichInputText seqNoBinding;
    private RichTable docRefTableBinding;
    private RichCommandLink downloadLinkBinding;

    public EnquiryBean() {
    }

    public void EditButtonAL(ActionEvent actionEvent) {
        System.out.println("ENQ NO====>"+getEnqNoBinding().getValue());
        OperationBinding op=ADFUtils.findOperation("checkQuotationEnquiry");
        op.getParamsMap().put("EnqNo", getEnqNoBinding().getValue());
        op.execute();
        if(op.getResult().equals("E"))
        {
            ADFUtils.showMessage("Enquiry cannot be edited further, as its Quotation has been made.", 2);
            ADFUtils.setEL("#{pageFlowScope.mode}", "V");
        }
        else{
            ADFUtils.findOperation("partiallyAddData").execute();
        cevmodecheck();   
        }
    }


    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }
    private void cevmodecheck(){
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }
    public void cevModeDisableComponent(String mode) {
        
        if(mode.equals("C")){
           // getDtlCreate1ButtonBinding().setDisabled(true);
            getUnitBinding().setDisabled(true);
            getProcBinding().setDisabled(true);
            getTransItemBinding().setDisabled(true);
            getTransVenBinding().setDisabled(true);
           
            getProcSeqBinding().setDisabled(true);
            
         getItemDescBinding().setDisabled(true);   
         getEnqNoBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            getCancelBinding().setDisabled(true);
            getEnqNoDtlBinding().setDisabled(true);
            enquiryDateBinding.setDisabled(true);
            seqNoBinding.setDisabled(true);
            docDateBinding.setDisabled(true);
            docFileNameBinding.setDisabled(true);
            docNoBinding.setDisabled(true);
            docTypeBinding.setDisabled(true);
            unitCdBinding.setDisabled(true);
        }
        if(mode.equals("E")){
            getDtlCreate1ButtonBinding().setDisabled(false);
            getEnqNoDtlBinding().setDisabled(true);
            getDtlDelete1ButtonBinding().setDisabled(false);
            getUnitBinding().setDisabled(true);
            getProcBinding().setDisabled(true);
            getDtlCreateButtonBinding().setDisabled(false);
            getDtlDeleteButtonBinding().setDisabled(false);
            getGenerateBtnBinding().setDisabled(false);
            getPoVenButtonBinding().setDisabled(false);
            getProcSeqBinding().setDisabled(true);         
            getItemDescBinding().setDisabled(true);  
            getTransItemBinding().setDisabled(true);
            getTransVenBinding().setDisabled(true);
            getEnqNoBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            enquiryDateBinding.setDisabled(true);
            seqNoBinding.setDisabled(true);
            docDateBinding.setDisabled(true);
            docFileNameBinding.setDisabled(true);
            docNoBinding.setDisabled(true);
//            docTypeBinding.setDisabled(true);
            unitCdBinding.setDisabled(true);


        }
        if(mode.equals("V")){
            
            
         
         getDtlCreate1ButtonBinding().setDisabled(true);
         getDtlCreateButtonBinding().setDisabled(true);
         getDtlDelete1ButtonBinding().setDisabled(true);
         getDtlDeleteButtonBinding().setDisabled(true);
            getGenerateBtnBinding().setDisabled(true);
            getPoVenButtonBinding().setDisabled(true);
            getEnquiryHeaderTabBinding().setDisabled(false);
            getItemDtlTabBinding().setDisabled(false);
            refDocTabBinding.setDisabled(false);
            downloadLinkBinding.setDisabled(false);
        
            
            
        }
        
}
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
            try {
                Method method1 =
                    component.getClass().getMethod("setDisabled", boolean.class);
                if (method1 != null) {
                    method1.invoke(component, valueComponent);
                }
            } catch (NoSuchMethodException e) {
                try {
                    Method method =
                        component.getClass().getMethod("setReadOnly", boolean.class);
                    if (method != null) {
                        method.invoke(component, valueComponent);
                    }
                } catch (Exception e1) {
                    // e.printStackTrace();//silently eat this exception.
                }


            } catch (Exception e) {
                // e.printStackTrace();//silently eat this exception.
            }
            List<UIComponent> childComponents = component.getChildren();
            for (UIComponent comp : childComponents) {
                makeComponentHierarchyReadOnly(comp, valueComponent);
            }
        }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }


    public void setItemDescBinding(RichInputText itemDescBinding) {
        this.itemDescBinding = itemDescBinding;
    }

    public RichInputText getItemDescBinding() {
        return itemDescBinding;
    }

    public void saveButtonAL(ActionEvent actionEvent) {
        String empcd=(String)ADFUtils.resolveExpression("#{pageFlowScope.empCode}") ; 
        System.out.println("empcd"+empcd);
        ADFUtils.setColumnValue("EnquiryHeaderVO1Iterator", "ModifiedBy", empcd);
        if(ErrorFlag.equals("true") || ErrorFlag1.equals("true"))
        {
            ADFUtils.showMessage("Qty cannot be greater than Indent Qty.", 0);
        }
        else
        {
//       OperationBinding op=ADFUtils.findOperation("getEnqNo");
//       Object rst=op.execute();
        ADFUtils.findOperation("copyEnqDtlNo").execute();
        Integer TransValue=(Integer)bindingOutputText.getValue();
        System.out.println("TRANS VALUE"+TransValue);
           
        if(TransValue==0)       
        {
            ADFUtils.findOperation("Commit").execute();
           FacesMessage Message = new FacesMessage("Record Save Successfully for Enquiry No:"+getEnqNoBinding().getValue());   
           Message.setSeverity(FacesMessage.SEVERITY_INFO);   
           FacesContext fc = FacesContext.getCurrentInstance();   
           fc.addMessage(null, Message);      
            
        }
        else 
        {
            ADFUtils.findOperation("Commit").execute();
            FacesMessage Message = new FacesMessage("Record Update Successfully");   
            Message.setSeverity(FacesMessage.SEVERITY_INFO);   
            FacesContext fc = FacesContext.getCurrentInstance();   
            fc.addMessage(null, Message);      
        }
        
//            if (rst != null && rst != "") 
//            {
//                if (op.getErrors().isEmpty()) {
//                    ADFUtils.findOperation("Commit").execute();
//                    ADFUtils.showMessage("Record Saved Successfully" + "Invoice No is::"+ rst, 2);
//                }
//            }
//            
//            if (rst == null || rst == "" || rst == " ") 
//            {
//                if (op.getErrors().isEmpty()) {
//                    ADFUtils.findOperation("Commit").execute();
//                    ADFUtils.showMessage(" Record Updated Successfully.", 2); 
//              }
        }
       
    }


    public void poVendorButtonAL(ActionEvent actionEvent) {
        
        System.out.println("enter in bean");
        
        ADFUtils.findOperation("getPoVendorData").execute();
    }

    public void generateButtonAL(ActionEvent actionEvent) {
        System.out.println("enter in bean in generate button al");
        
        ADFUtils.findOperation("getGenerateButtonData").execute();
    }

    public void setTransItemBinding(RichInputText transItemBinding) {
        this.transItemBinding = transItemBinding;
    }

    public RichInputText getTransItemBinding() {
        return transItemBinding;
    }

    public void setTransVenBinding(RichInputText transVenBinding) {
        this.transVenBinding = transVenBinding;
    }

    public RichInputText getTransVenBinding() {
        return transVenBinding;
    }

    

    public void setProcBinding(RichInputText procBinding) {
        this.procBinding = procBinding;
    }

    public RichInputText getProcBinding() {
        return procBinding;
    }

    public void setProcSeqBinding(RichInputText procSeqBinding) {
        this.procSeqBinding = procSeqBinding;
    }

    public RichInputText getProcSeqBinding() {
        return procSeqBinding;
    }


    public void setEnqNoBinding(RichInputText enqNoBinding) {
        this.enqNoBinding = enqNoBinding;
    }

    public RichInputText getEnqNoBinding() {
        return enqNoBinding;
    }

    public void setDtlCreate1ButtonBinding(RichButton dtlCreate1ButtonBinding) {
        this.dtlCreate1ButtonBinding = dtlCreate1ButtonBinding;
    }

    public RichButton getDtlCreate1ButtonBinding() {
        return dtlCreate1ButtonBinding;
    }

    public void setDtlCreateButtonBinding(RichButton dtlCreateButtonBinding) {
        this.dtlCreateButtonBinding = dtlCreateButtonBinding;
    }

    public RichButton getDtlCreateButtonBinding() {
        return dtlCreateButtonBinding;
    }

    public void setDtlDelete1ButtonBinding(RichButton dtlDelete1ButtonBinding) {
        this.dtlDelete1ButtonBinding = dtlDelete1ButtonBinding;
    }

    public RichButton getDtlDelete1ButtonBinding() {
        return dtlDelete1ButtonBinding;
    }

    public void setDtlDeleteButtonBinding(RichButton dtlDeleteButtonBinding) {
        this.dtlDeleteButtonBinding = dtlDeleteButtonBinding;
    }

    public RichButton getDtlDeleteButtonBinding() {
        return dtlDeleteButtonBinding;
    }

    public void setGenerateBtnBinding(RichButton generateBtnBinding) {
        this.generateBtnBinding = generateBtnBinding;
    }

    public RichButton getGenerateBtnBinding() {
        return generateBtnBinding;
    }

    public void setPoVenButtonBinding(RichButton poVenButtonBinding) {
        this.poVenButtonBinding = poVenButtonBinding;
    }

    public RichButton getPoVenButtonBinding() {
        return poVenButtonBinding;
    }

    public void setUnitBinding(RichInputComboboxListOfValues unitBinding) {
        this.unitBinding = unitBinding;
    }

    public RichInputComboboxListOfValues getUnitBinding() {
        return unitBinding;
    }

    public void deletePopupDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                    {
                    ADFUtils.findOperation("Delete").execute();
                    System.out.println("Record Delete Successfully");
                   
                        FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                        FacesContext fc = FacesContext.getCurrentInstance();  
                        fc.addMessage(null, Message);  
                        
//                        ADFUtils.findOperation("RemoveRowFromVVOEnquiry").execute();
                   
                    }
                AdfFacesContext.getCurrentInstance().addPartialTarget(table1Binding);
        ADFUtils.findOperation("RemoveRowFromVVOEnquiry").execute();
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        ADFUtils.findOperation("partiallyHoldRecords").execute();
        if(dialogEvent.getOutcome().name().equals("ok"))
                    {
                    ADFUtils.findOperation("Delete1").execute();
                    System.out.println("Record Delete Successfully");
                   
                        FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                        FacesContext fc = FacesContext.getCurrentInstance();  
                        fc.addMessage(null, Message);  
                   
                    }
                AdfFacesContext.getCurrentInstance().addPartialTarget(table2Binding);
    }

    public void setTable1Binding(RichTable table1Binding) {
        this.table1Binding = table1Binding;
    }

    public RichTable getTable1Binding() {
        return table1Binding;
    }

    public void setTable2Binding(RichTable table2Binding) {
        this.table2Binding = table2Binding;
    }

    public RichTable getTable2Binding() {
        return table2Binding;
    }

    public void itemCodeCommitVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        System.out.println("inside vcl of bean of commit"+vce.getNewValue());
        if(vce.getNewValue()!=null && qtyBinding.getValue()!=null){
            
            System.out.println("inside if of bean of commit");
           ADFUtils.findOperation("partialCommitItem").execute();
            //ADFUtils.findOperation("clearRowOnItemVceEnquiryDtl").execute();
            AdfFacesContext.getCurrentInstance().addPartialTarget(table2Binding);
           
        }
    }

    public void createDtl1AL(ActionEvent actionEvent) {
        System.out.println("inside create al of bean of commit");
       // DCIteratorBinding binding = ADFUtils.findIterator("EnquiryDetailVO1Iterator");
      // ADFUtils.findOperation("partialCommitItem").execute();
        ADFUtils.findOperation("CreateInsert1").execute();
       
        
    }

    public void setPcBinding(RichPanelCollection pcBinding) {
        this.pcBinding = pcBinding;
    }

    public RichPanelCollection getPcBinding() {
        return pcBinding;
    }

    public void itemVCL(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(valueChangeEvent!=null){
      //  AdfFacesContext.getCurrentInstance().addPartialTarget(pcBinding);
        }
    }

    public void setEnqNoDtlBinding(RichInputText enqNoDtlBinding) {
        this.enqNoDtlBinding = enqNoDtlBinding;
    }

    public RichInputText getEnqNoDtlBinding() {
        return enqNoDtlBinding;
    }

    public void setCancelBinding(RichSelectBooleanCheckbox cancelBinding) {
        this.cancelBinding = cancelBinding;
    }

    public RichSelectBooleanCheckbox getCancelBinding() {
        return cancelBinding;
    }

    public void setTypeBinding(RichSelectOneChoice typeBinding) {
        this.typeBinding = typeBinding;
    }

    public RichSelectOneChoice getTypeBinding() {
        return typeBinding;
    }

    public void setIndentNoBinding(RichInputComboboxListOfValues indentNoBinding) {
        this.indentNoBinding = indentNoBinding;
    }

    public RichInputComboboxListOfValues getIndentNoBinding() {
        return indentNoBinding;
    }

    

    public void setItemCd1Binding(RichInputComboboxListOfValues itemCd1Binding) {
        this.itemCd1Binding = itemCd1Binding;
    }

    public RichInputComboboxListOfValues getItemCd1Binding() {
        return itemCd1Binding;
    }


    public void itemCdDtl2VCL(ValueChangeEvent valueChangeEvent) {
        ADFUtils.findOperation("clearRowOnItemVceEnquiry").execute();
        AdfFacesContext.getCurrentInstance().addPartialTarget(table1Binding);
    }

    public void QtyVCE(ValueChangeEvent vce){
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce.getNewValue()!=null && vce.getOldValue()!=null)
        {
            BigDecimal oldVal=(BigDecimal)vce.getOldValue();
            BigDecimal newVal=(BigDecimal)vce.getNewValue();
            if(newVal.compareTo(oldVal)==1 && newVal!=null)
            {
                System.out.println("IN IF");
                ErrorFlag="true";
                ADFUtils.showMessage("Qty cannot be greater than Indent Qty", 0);
             }
            else
            {
                System.out.println("IN ELSE");
                ErrorFlag="false";
            }

        }
    }

    public void QuantityVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce.getNewValue()!=null)
        {
            BigDecimal oldVal=(BigDecimal)vce.getOldValue();
            BigDecimal newVal=(BigDecimal)vce.getNewValue();
            if(newVal.compareTo(oldVal)==1)
            {
                 System.out.println("IN IF");

                ErrorFlag1="true";
                ADFUtils.showMessage("Qty cannot be greater than Indent Qty", 0);
             }
            else
            {
                System.out.println("IN ELSE");
                ErrorFlag1="false";
            }

        }
    }

    public String saveAndCloseAL() {
        String empcd=(String)ADFUtils.resolveExpression("#{pageFlowScope.empCode}") ; 
        System.out.println("empcd"+empcd);
        ADFUtils.setColumnValue("EnquiryHeaderVO1Iterator", "ModifiedBy", empcd);
            if(ErrorFlag.equals("true") || ErrorFlag1.equals("true"))
            {
                ADFUtils.showMessage("Qty cannot be greater than Indent Qty.", 0);
            }
            else
            {
            Integer TransValue=(Integer)bindingOutputText.getValue();
            System.out.println("TRANS VALUE"+TransValue);
            ADFUtils.findOperation("copyEnqDtlNo").execute();  
            if(TransValue==0)       
            {
                    ADFUtils.findOperation("Commit").execute();
                   FacesMessage Message = new FacesMessage("Record Saved Successfully:"+getEnqNoBinding().getValue());
                   Message.setSeverity(FacesMessage.SEVERITY_INFO);
                   FacesContext fc = FacesContext.getCurrentInstance();
                   fc.addMessage(null, Message);
                            return "SaveAndClose";  
            }
            else 
            {
                    ADFUtils.findOperation("Commit").execute();
                    ADFUtils.findOperation("Rollback").execute();
                    FacesMessage Message = new FacesMessage("Record Update Successfully");
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);
                          return "SaveAndClose";              }
            }
        return null;
    }

    public void setItemDtlTabBinding(RichShowDetailItem itemDtlTabBinding) {
        this.itemDtlTabBinding = itemDtlTabBinding;
    }

    public RichShowDetailItem getItemDtlTabBinding() {
        return itemDtlTabBinding;
    }

    public void setEnquiryHeaderTabBinding(RichShowDetailItem enquiryHeaderTabBinding) {
        this.enquiryHeaderTabBinding = enquiryHeaderTabBinding;
    }

    public RichShowDetailItem getEnquiryHeaderTabBinding() {
        return enquiryHeaderTabBinding;
    }

    public void setEnquiryDateBinding(RichInputDate enquiryDateBinding) {
        this.enquiryDateBinding = enquiryDateBinding;
    }

    public RichInputDate getEnquiryDateBinding() {
        return enquiryDateBinding;
    }

    public void setQtyBinding(RichInputText qtyBinding) {
        this.qtyBinding = qtyBinding;
    }

    public RichInputText getQtyBinding() {
        return qtyBinding;
    }

    public void setRefDocTabBinding(RichShowDetailItem refDocTabBinding) {
        this.refDocTabBinding = refDocTabBinding;
    }

    public RichShowDetailItem getRefDocTabBinding() {
        return refDocTabBinding;
    }
    
    public void uploadAction(ValueChangeEvent vce) {
        //Get File Object from VC Event
            UploadedFile fileVal = (UploadedFile) vce.getNewValue();
            System.out.println("File Value==============<>" + fileVal);
            //Upload File to path- Return actual server path
            String path = ADFUtils.uploadFile(fileVal);
            System.out.println(fileVal.getContentType());
            //Method to insert data in table to keep track of uploaded files
            OperationBinding ob = ADFUtils.findOperation("setFileDataEnquiry");
            ob.getParamsMap().put("name", fileVal.getFilename());
            ob.getParamsMap().put("path", path);
            ob.getParamsMap().put("contTyp", fileVal.getContentType());
            ob.getParamsMap().put("SrNo", ADFUtils.evaluateEL("#{bindings.DocAttachRefDtlVO12.estimatedRowCount+1}"));
            ob.execute();
            System.out.println("IN VCE EXECUTED");
            // Reset inputFile component after upload
            ResetUtils.reset(vce.getComponent());
                       }

    public void downloadAction(FacesContext facesContext, OutputStream outputStream) throws Exception{
        File filed = new File(pathBinding.getValue().toString());
                                  FileInputStream fis;
                                  byte[] b;
                                  try {
                                      fis = new FileInputStream(filed);

                                      int n;
                                      while ((n = fis.available()) > 0) {
                                          b = new byte[n];
                                          int result = fis.read(b);
                                          outputStream.write(b, 0, b.length);
                                          if (result == -1)
                                              break;
                                      }
                                  } catch (Exception e) {
                                      e.printStackTrace();
                                  }
                                  outputStream.flush();
        

    }
    
    public void deleteDocDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete2").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(docRefTableBinding);   
        }

    public void setPathBinding(RichInputText pathBinding) {
        this.pathBinding = pathBinding;
    }

    public RichInputText getPathBinding() {
        return pathBinding;
    }

    public void setContentType(RichInputText contentType) {
        this.contentType = contentType;
    }

    public RichInputText getContentType() {
        return contentType;
    }

    public void setDocFileNameBinding(RichInputText docFileNameBinding) {
        this.docFileNameBinding = docFileNameBinding;
    }

    public RichInputText getDocFileNameBinding() {
        return docFileNameBinding;
    }

    public void setDocTypeBinding(RichInputText docTypeBinding) {
        this.docTypeBinding = docTypeBinding;
    }

    public RichInputText getDocTypeBinding() {
        return docTypeBinding;
    }

    public void setDocDateBinding(RichInputDate docDateBinding) {
        this.docDateBinding = docDateBinding;
    }

    public RichInputDate getDocDateBinding() {
        return docDateBinding;
    }

    public void setDocNoBinding(RichInputText docNoBinding) {
        this.docNoBinding = docNoBinding;
    }

    public RichInputText getDocNoBinding() {
        return docNoBinding;
    }

    public void setUnitCdBinding(RichInputText unitCdBinding) {
        this.unitCdBinding = unitCdBinding;
    }

    public RichInputText getUnitCdBinding() {
        return unitCdBinding;
    }

    public void setSeqNoBinding(RichInputText seqNoBinding) {
        this.seqNoBinding = seqNoBinding;
    }

    public RichInputText getSeqNoBinding() {
        return seqNoBinding;
    }

    public void setDocRefTableBinding(RichTable docRefTableBinding) {
        this.docRefTableBinding = docRefTableBinding;
    }

    public RichTable getDocRefTableBinding() {
        return docRefTableBinding;
    }

    public void setDownloadLinkBinding(RichCommandLink downloadLinkBinding) {
        this.downloadLinkBinding = downloadLinkBinding;
    }

    public RichCommandLink getDownloadLinkBinding() {
        return downloadLinkBinding;
    }
}
