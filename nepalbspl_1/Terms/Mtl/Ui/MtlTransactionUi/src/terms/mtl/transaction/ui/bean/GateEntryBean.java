package terms.mtl.transaction.ui.bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import java.io.OutputStream;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.text.ParseException;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import javax.faces.validator.ValidatorException;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;

import oracle.adf.view.rich.component.rich.input.RichSelectBooleanCheckbox;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.input.RichSelectOneRadio;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.event.ReturnPopupEvent;
import oracle.adf.view.rich.util.ResetUtils;

import oracle.binding.OperationBinding;

import oracle.jbo.JboWarning;
import oracle.jbo.Row;

import org.apache.myfaces.trinidad.event.DisclosureEvent;
import org.apache.myfaces.trinidad.model.UploadedFile;

import terms.mtl.transaction.model.applicationModule.MtlTransactionAMImpl;
import terms.mtl.transaction.model.view.PurchaseOrderDetailVORowImpl;

public class GateEntryBean {


    private RichInputText bindGteEtime;
    private RichInputText bindGteENo;
    private RichInputText bindPath;
    private RichSelectOneChoice bindDocType;
    private RichSelectOneChoice bindChllnType;
    private RichSelectOneChoice bindPoType;
    private RichSelectOneChoice bindRetVal;
    private RichInputText bindBillNo;
    private RichTable createGateEntryTableBinding;
    private RichTable createDocAttachTableBinding;
    private RichTable createChallanTableBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputDate dateBinding;
    private RichInputText transporterNameBinding;
    private RichInputComboboxListOfValues svcBinding;
    private RichInputText vensupNameBinding;
    private RichInputText suppNameBinding;
    private RichInputComboboxListOfValues suppByBinding;
    private RichInputText suppByNameBinding;
    private RichInputText srvNobinding;
    private RichSelectOneRadio cityCodeBinding;
    private RichInputText plantCodeBinding;
    private RichInputComboboxListOfValues poJoSoRgNoBinding;
    private RichInputText amdNoBinding;
    private RichInputDate poDateBinding;
    private RichInputComboboxListOfValues itemCdBinding;
    private RichInputText itemDescBinding;
    private RichInputText challanQtyBinding;
    private RichInputText challanUOMBinding;
    private RichInputText shortDescriptionBnding;

    private RichInputText seqBinding;
    private RichSelectOneChoice fgrejBinding;
    private RichInputText poDetHsnBinding;
    private RichButton headerEditButton;
    private RichPanelHeader getMyPageRootBinding;
    private RichOutputText getOutputTextBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton detailCreateBinding;
    private RichButton detailDeleteBinding;
    private RichInputText unitNameBinding;
    private RichInputText cityBinding;
    private RichInputText gstBillNoBinding;
    private RichInputDate docDateBinding;
    private RichInputText invoiceValueBinding;
    private RichInputText noOfpacketsBinding;
    private RichShowDetailItem tab1Binding;
    private RichShowDetailItem tab2Binding;
    private RichButton challanDtlBtnBinding;
    private RichInputComboboxListOfValues venHsnBinding;
    private RichInputText cashAmtBinding;
    private RichPopup cancelPopupBinding;
    private RichButton cancelBinding;
    private RichInputComboboxListOfValues appByBinding;
    private RichSelectBooleanCheckbox creditBinding;
    private RichInputComboboxListOfValues deptBinding;
    private RichInputComboboxListOfValues empBinding;
    private RichInputComboboxListOfValues prepBinding;
    private RichInputComboboxListOfValues devAppByBinding;
    private RichSelectOneChoice recAgBinding;
    private RichShowDetailItem tab2Bindingoth;
    private RichShowDetailItem tab1BindingDtl;
    private RichSelectOneChoice itemTypeBinding;
    private RichSelectOneChoice mriFlagBinding;
    private RichSelectOneChoice devyNBinding;
    private RichSelectBooleanCheckbox srvReqBinding;
    private RichInputComboboxListOfValues manfCodeBinding;
    private RichInputText venhsnBinding;
    private RichInputText statusBinding;
    private RichInputText billnoBinding;
    private RichInputText potype2Binding;
    private RichInputText gateEdNepalbinding;
    private RichInputDate boeDatebinding;
    private RichInputComboboxListOfValues stockTransferGEbinding;

    public GateEntryBean() {
    }

    private static final ADFLogger logger = ADFLogger.createADFLogger(GateEntryBean.class);

    public void SaveGteAl(ActionEvent actionEvent) {

        String empcd =
            ((String) ADFUtils.resolveExpression("#{pageFlowScope.empCode}") == null) ? "SWE161" :
            (String) ADFUtils.resolveExpression("#{pageFlowScope.empCode}");

        ADFUtils.setColumnValue("GtEheadsVO1Iterator", "LastUpdatedBy", empcd);

        String challan1 = (String) bindChllnType.getValue();
        logger.info("challan1*********==??" + challan1 + "Challan Type" + bindChllnType.getValue());
        // ADFUtils.findOperation("setGateEntryStatus").execute();
        //        if(challan1.equalsIgnoreCase("MRA") || challan1.equalsIgnoreCase("JOB")){
        //           System.out.println("gggggggggggggggg");
        //            statusBinding.setValue("G");
        //        }
        //        else{
        //            System.out.println("ssssssssssssssssss");
        //            statusBinding.setValue("S");
        //        }
        //        if(challan1.equalsIgnoreCase("NOC") && deptBinding.getValue()==null && empBinding.getValue()==null){
        //            ADFUtils.showMessage("Department Code and Employee Code is required.", 0);
        //            deptBinding.setRequired(true);
        //            empBinding.setRequired(true);
        //        }
        //        else{


        String itemType = (String) itemTypeBinding.getValue();
        String manf = (String) manfCodeBinding.getValue();
       logger.info("Item type on save is===>>" + itemType + "manf code ===>>" + manf);
        if (manf == null && !itemType.equalsIgnoreCase("O")) {
            if (itemType.equalsIgnoreCase("L") || itemType.equalsIgnoreCase("R")) {
                ADFUtils.showMessage("Manufacturer Code is Required.", 0);
            }
        } else {

            String rec = (String) recAgBinding.getValue();
            String challan = (String) bindChllnType.getValue();
            if (challan.equalsIgnoreCase("MRA")) {
                if (challan.equalsIgnoreCase("MRA") && (rec.equalsIgnoreCase("P") || rec.equalsIgnoreCase("W"))) {
                    OperationBinding ob = ADFUtils.findOperation("sumChallanQtyInChallan");
                    Object op1 = ob.execute();
                    if (ob.getResult() != null && ob.getResult().equals("N")) {
                        ADFUtils.showMessage(" Quantity must be equal to the entered Challan qty.", 0);

                    } else {

                        OperationBinding binding = ADFUtils.findOperation("challanQty");
                        //   binding.getParamsMap().put("gateEntry", vce.getNewValue().toString());
                        Object opp = binding.execute();

                        logger.info("The Chalan Qty fun Result is===>>" + binding.getResult());
                        String res = (String) binding.getResult();
                        if (binding.getResult() != null && res.substring(0, 1).equals("N") ||
                            potype2Binding.getValue() != (null) && potype2Binding.getValue().equals("O")) {

                            if (getBindGteENo().getValue() == null) {

                                logger.info("what the sys time is in bean while commiting--->" +
                                                   new java.util.Date(System.currentTimeMillis()));
                                java.util.Date dt = new java.util.Date(System.currentTimeMillis());
                                String dateWithoutTime = dt.toString().substring(10, 16);

                               logger.info(" is time extracted from date in bean??" + dateWithoutTime);
                                getBindGteEtime().setValue(dateWithoutTime);
                                AdfFacesContext.getCurrentInstance().addPartialTarget(getBindGteEtime());
                            }
                            OperationBinding op = ADFUtils.findOperation("getGateEntryNo");
                            Object rst = op.execute();
                            //ADFUtils.findOperation("goForUnitRate").execute();
                            System.out.println("--------Commit-------"+rst);

                            if (rst.toString() != null && rst.toString() != "") {
                                if (op.getErrors().isEmpty()) {

                                    if (bindChllnType.getValue() != null && bindChllnType.getValue().equals("REO") ||
                                        bindChllnType.getValue().equals("ARG") ||
                                        bindChllnType.getValue().equals("FOC") ||
                                        bindChllnType.getValue().equals("NOC")) {
                                        appByBinding.setValue(prepBinding.getValue());
                                    }

                                    ADFUtils.findOperation("Commit").execute();
                                    FacesMessage Message =
                                        new FacesMessage("Record Saved Successfully.Gate Entry No is " + rst + ".");
                                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
                                    FacesContext fc = FacesContext.getCurrentInstance();
                                    fc.addMessage(null, Message);

                                    ADFUtils.setEL("#{pageFlowScope.mode}", "V");
                                    cevmodecheck();
                                    AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);


                                }
                            }

                            if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ") {
                                if (op.getErrors().isEmpty()) {
                                    ADFUtils.findOperation("Commit").execute();
                                    FacesMessage Message = new FacesMessage("Record Update Successfully.");
                                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
                                    FacesContext fc = FacesContext.getCurrentInstance();
                                    fc.addMessage(null, Message);
                                    ADFUtils.setEL("#{pageFlowScope.mode}", "V");
                                    cevmodecheck();
                                    AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);

                                }
                            }
                        } else {
                           logger.info("inside else ===>>" + binding.getResult());
                            FacesMessage message =
                                new FacesMessage("Challan Qty cannot be more than PO Qty for this Item : " +
                                                 binding.getResult().toString());
                            message.setSeverity(FacesMessage.SEVERITY_ERROR);
                            FacesContext context = FacesContext.getCurrentInstance();
                            context.addMessage(challanQtyBinding.getClientId(), message);
                        }
                    }
                } else {
                    if (challan.equalsIgnoreCase("MRA") && rec.equalsIgnoreCase("R")) 
                    {
                        FacesMessage message = new FacesMessage("This Challan Type is not allowed with Against RGP.");
                        message.setSeverity(FacesMessage.SEVERITY_ERROR);
                        FacesContext context = FacesContext.getCurrentInstance();
                        context.addMessage(recAgBinding.getClientId(), message);
                    }
                    if (challan.equalsIgnoreCase("MRA") && rec.equalsIgnoreCase("I")) {
                        logger.info("inside 2nd if");
                        FacesMessage message =
                            new FacesMessage("This Challan Type is not allowed with Against Sales Invoice.");
                        message.setSeverity(FacesMessage.SEVERITY_ERROR);
                        FacesContext context = FacesContext.getCurrentInstance();
                        context.addMessage(recAgBinding.getClientId(), message);
                    }
                    if (challan.equalsIgnoreCase("MRA") && rec.equalsIgnoreCase("C")) {
                        logger.info("inside 3rd if");
                        FacesMessage message =
                            new FacesMessage("This Challan Type is not allowed with Against Contract.");
                        message.setSeverity(FacesMessage.SEVERITY_ERROR);
                        FacesContext context = FacesContext.getCurrentInstance();
                        context.addMessage(recAgBinding.getClientId(), message);
                    }
                }
            } else {
                OperationBinding ob = ADFUtils.findOperation("sumChallanQtyInChallan");
                Object op1 = ob.execute();
                if (ob.getResult() != null && ob.getResult().equals("N")) {
                    ADFUtils.showMessage(" Quantity must be equal to the entered Challan qty.", 0);

                } else {

                    OperationBinding binding = ADFUtils.findOperation("challanQty");
                    //   binding.getParamsMap().put("gateEntry", vce.getNewValue().toString());
                    Object opp = binding.execute();

                    logger.info("Challan Qty Fun Result is===>>" + binding.getResult());
                    if (binding.getResult() != null && binding.getResult().equals("N")) {

                        if (getBindGteENo().getValue() == null) {

                            logger.info("what the sys time is in bean while commiting--->" +
                                               new java.util.Date(System.currentTimeMillis()));
                            java.util.Date dt = new java.util.Date(System.currentTimeMillis());
                            String dateWithoutTime = dt.toString().substring(10, 16);

                           logger.info(" is time extracted from date in bean??" + dateWithoutTime);
                            getBindGteEtime().setValue(dateWithoutTime);
                            AdfFacesContext.getCurrentInstance().addPartialTarget(getBindGteEtime());
                        }
                        OperationBinding op = ADFUtils.findOperation("getGateEntryNo");
                        Object rst = op.execute();
                        //ADFUtils.findOperation("goForUnitRate").execute();
                        logger.info("----====----Commit-------"+rst);

                        if (rst.toString() != null && rst.toString() != "") {
                            if (op.getErrors().isEmpty()) {

                                if (bindChllnType.getValue() != null && bindChllnType.getValue().equals("REO") ||
                                    bindChllnType.getValue().equals("ARG") || bindChllnType.getValue().equals("FOC") ||
                                    bindChllnType.getValue().equals("NOC")) {
                                    appByBinding.setValue(prepBinding.getValue());
                                }


                                ADFUtils.findOperation("Commit").execute();
                                FacesMessage Message =
                                    new FacesMessage("Record Saved Successfully.Gate Entry No is " + rst + ".");
                                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                                FacesContext fc = FacesContext.getCurrentInstance();
                                fc.addMessage(null, Message);


                            }
                        }

                        if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ") {
                            if (op.getErrors().isEmpty()) {
                                ADFUtils.findOperation("Commit").execute();
                                FacesMessage Message = new FacesMessage("Record Update Successfully.");
                                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                                FacesContext fc = FacesContext.getCurrentInstance();
                                fc.addMessage(null, Message);

                            }


                        }
                    } else {
                        logger.info("inside else ===>>" + binding.getResult());
                        FacesMessage message =
                            new FacesMessage("Challan Qty cannot be more than PO Qty for this Item : " +
                                             binding.getResult().toString());
                        message.setSeverity(FacesMessage.SEVERITY_ERROR);
                        FacesContext context = FacesContext.getCurrentInstance();
                        context.addMessage(challanQtyBinding.getClientId(), message);
                    }
                }
            }
        }
        // }
    }

    public void docFileUpldVCE(ValueChangeEvent vce) {
        if (vce.getNewValue() != null) {
            //Get File Object from VC Event
            UploadedFile fileVal = (UploadedFile) vce.getNewValue();
            //Upload File to path- Return actual server path
            String path = uploadFile(fileVal);
            System.out.println(fileVal.getContentType());
            //Method to insert data in table to keep track of uploaded files
            OperationBinding ob = ADFUtils.findOperation("setuplaodedFileDataData");
            ob.getParamsMap().put("name", fileVal.getFilename());
            ob.getParamsMap().put("path", path);
            ob.getParamsMap().put("contTyp", fileVal.getContentType());
            ob.execute();
            // Reset inputFile component after upload
            // ResetUtils.reset(vce.getComponent());
        }
    }

    private String uploadFile(UploadedFile file) {

        String UserName = System.getProperty("user.name");
        System.out.println("User Name is=>" + UserName);
        UploadedFile myfile = file;
        String path = null;
        if (myfile == null) {

        } else {
            // All uploaded files will be stored in below path
            path = "/home/" + UserName + "/uplaodedDoc" + myfile.getFilename();
            System.out.println("name of the file ---->" + myfile.getFilename());
            InputStream inputStream = null;
            try {
                FileOutputStream out = new FileOutputStream(path);
                inputStream = myfile.getInputStream();
                byte[] buffer = new byte[8192];
                int bytesRead = 0;
                while ((bytesRead = inputStream.read(buffer, 0, 8192)) != -1) {
                    out.write(buffer, 0, bytesRead);
                }
                out.flush();
                out.close();
            } catch (Exception ex) {
                // handle exception
                ex.printStackTrace();
            } finally {
                try {
                    inputStream.close();
                } catch (IOException e) {
                }
            }

        }
        //Returns the path where file is stored
        return path;
    }

    public void downloadFileAl(FacesContext facesContext, OutputStream outputStream) {
        if (bindPath.getValue() != null) {
            File filed = new File(bindPath.getValue().toString());
            FileInputStream fis;
            byte[] b;
            try {
                fis = new FileInputStream(filed);

                int n;
                while ((n = fis.available()) > 0) {
                    b = new byte[n];
                    int result = fis.read(b);
                    outputStream.write(b, 0, b.length);
                    if (result == -1)
                        break;
                }
            } catch (IOException e) {
                //  e.printStackTrace();
                ADFUtils.showMessage("File Not Found.", 0);

            }
        } else {
            ADFUtils.showMessage("File Not Found.", 0);

        }
    }

    public void setBindPath(RichInputText bindPath) {
        this.bindPath = bindPath;
    }

    public RichInputText getBindPath() {
        return bindPath;
    }

    public void createGteDtlAL(ActionEvent actionEvent) {

        OperationBinding op = ADFUtils.findOperation("getGateEntryNo");
        Object rst = op.execute();
        ADFUtils.findOperation("CreateInsert1").execute();
    }

    public void challanTypeVCE(ValueChangeEvent vce) {
        if (vce.getNewValue() != null) {

            String chlln_type = getBindChllnType().getValue().toString();
            System.out.println("**********chlln_type***********" + chlln_type);
            vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
            if (chlln_type != null && chlln_type != "") {
                System.out.println("chlln_type***********" + chlln_type);
                if (chlln_type.equalsIgnoreCase("MRA")) {
                    getBindDocType().setValue("PUC");
                    //                          getAppByBinding().setDisabled(true);
                    getDeptBinding().setDisabled(true);
                    getEmpBinding().setDisabled(true);
                    getCreditBinding().setDisabled(true);
                    getRecAgBinding().setValue("P");
                    getRecAgBinding().setDisabled(false);
                    getPoJoSoRgNoBinding().setDisabled(false);
                    srvReqBinding.setValue(true);
                    System.out.println("srv req binding" + srvReqBinding.getValue());


                }
                if (chlln_type.equalsIgnoreCase("REO")) {
                    getBindDocType().setValue("CRC");
                    getPoJoSoRgNoBinding().setDisabled(false);
                    //                              getAppByBinding().setDisabled(true);
                    getDeptBinding().setDisabled(true);
                    getEmpBinding().setDisabled(true);
                    getCreditBinding().setDisabled(true);
                    getRecAgBinding().setValue("I");
                    getRecAgBinding().setDisabled(true);
                    getSrvReqBinding().setValue(false);
                }
                if (chlln_type.equalsIgnoreCase("MRR")) {
                    getBindDocType().setValue("VDC");
                    getPoJoSoRgNoBinding().setDisabled(true);
                    getChallanDtlBtnBinding().setDisabled(true);
                    //                              getAppByBinding().setDisabled(true);
                    getDeptBinding().setDisabled(true);
                    getEmpBinding().setDisabled(true);
                    getCreditBinding().setDisabled(true);
                    getRecAgBinding().setValue("W");
                    getRecAgBinding().setDisabled(true);
                    getSrvReqBinding().setValue(false);
                }
                if (chlln_type.equalsIgnoreCase("ARG")) {
                    getBindDocType().setValue("ARG");
                    //                              getAppByBinding().setDisabled(true);
                    getDeptBinding().setDisabled(true);
                    getEmpBinding().setDisabled(true);
                    getCreditBinding().setDisabled(true);
                    getRecAgBinding().setValue("R");
                    getRecAgBinding().setDisabled(true);
                    getSrvReqBinding().setValue(false);

                }
                if (chlln_type.equalsIgnoreCase("FOC")) {
                    getBindDocType().setValue("FOC");
                    getPoJoSoRgNoBinding().setDisabled(true);
                    //                              getAppByBinding().setDisabled(true);
                    getDeptBinding().setDisabled(true);
                    getEmpBinding().setDisabled(true);
                    getCreditBinding().setDisabled(true);
                    getRecAgBinding().setValue("W");
                    getRecAgBinding().setDisabled(true);
                    getSrvReqBinding().setValue(false);
                    System.out.println("srv req binding in foc" + srvReqBinding.getValue());
                }
                if (chlln_type.equalsIgnoreCase("NOC")) {
                    getBindDocType().setValue("NOC");
                    //                              getAppByBinding().setDisabled(false);
                    getDeptBinding().setDisabled(false);
                    getEmpBinding().setDisabled(false);
                    getCreditBinding().setDisabled(false);
                    getPoJoSoRgNoBinding().setDisabled(true);
                    getRecAgBinding().setValue("W");
                    getRecAgBinding().setDisabled(true);
                    getSrvReqBinding().setValue(false);

                }
                if (chlln_type.equalsIgnoreCase("API")) {
                    getBindDocType().setValue("API");
                    //                              getAppByBinding().setDisabled(true);
                    getDeptBinding().setDisabled(true);
                    getEmpBinding().setDisabled(true);
                    getCreditBinding().setDisabled(true);

                }
                if (chlln_type.equalsIgnoreCase("FGE")) {
                    getBindDocType().setValue("FGE");
                    //                              getAppByBinding().setDisabled(true);
                    getDeptBinding().setDisabled(true);
                    getEmpBinding().setDisabled(true);
                    getCreditBinding().setDisabled(true);

                }
                if (chlln_type.equalsIgnoreCase("JOB")) {
                    getBindDocType().setValue("JOB");
                    //                                  getAppByBinding().setDisabled(true);
                    getDeptBinding().setDisabled(true);
                    getEmpBinding().setDisabled(true);
                    getCreditBinding().setDisabled(true);
                    getPoJoSoRgNoBinding().setDisabled(true);
                    getRecAgBinding().setValue("J");
                    getRecAgBinding().setDisabled(true);
                    srvReqBinding.setValue(true);
                    System.out.println("srv req bindingin  job" + srvReqBinding.getValue());
                }

            }
            setPoType();

        }

    }

    public void retValValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {

        if (object != null) {
            if (getBindChllnType().getValue() != null) {
                //                    Object chlln_type =  getBindChllnType().getValue().toString();
                //                    Object rt = object.toString();
                String chlln_type = (String) getBindChllnType().getValue();
                String rt = object.toString();
                //                    if((chlln_type.equals("MRA") && rt.equals("R")) || (chlln_type.equals("MRR") && rt.equals("R")) || (chlln_type.equals("API") && rt.equals("R")) || (chlln_type.equals("NEO") && rt.equals("R")))

                System.out.println("value for chlln typr--" + chlln_type + "value for rtnable--->" + rt);
                if ((chlln_type.equals("MRA") && rt.equals("R")))

                {
                    System.out.println("Callan type is in validator PRIYANKA===>>>" + chlln_type);
                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                  "For the selected challant type - Type should be Non-Returnable",
                                                                  null));

                }
                if ((chlln_type.equals("MRR") && rt.equals("R")))

                {
                    System.out.println("Callan type is in validator PRIYANKA===>>>" + chlln_type);
                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                  "For the selected challant type - Type should be Non-Returnable",
                                                                  null));

                }
                if ((chlln_type.equals("API") && rt.equals("R")))

                {
                    System.out.println("Callan type is in validator PRIYANKA===>>>" + chlln_type);
                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                  "For the selected challant type - Type should be Non-Returnable",
                                                                  null));

                }
                if ((chlln_type.equals("REO") && rt.equals("R")))

                {
                    System.out.println("Callan type is in validator PRIYANKA===>>>" + chlln_type);
                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                  "For the selected challant type - Type should be Non-Returnable",
                                                                  null));

                }
                if ((chlln_type.equals("ARG") && rt.equals("R")))

                {
                    System.out.println("Callan type is in validator PRIYANKA===>>>" + chlln_type);
                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                  "For the selected challant type - Type should be Non-Returnable",
                                                                  null));

                }


                //                    if((chlln_type.equals("API") && rt.equals("R")) || (chlln_type.equals("REO") && rt.equals("R")) || (chlln_type.equals("MRR") && rt.equals("R")) ||(chlln_type.equals("MRA") && rt.equals("R"))){
                //
                //                        System.out.println("Callan type is in validator PRIYANKA===>>>"+chlln_type);
                //
                //                        System.out.println("Can make returnable entry========****************");
                //
                //
                //                        }
                //                    else{
                //
                //                        throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "For the selected challant type - Type should be Non-Returnable", null));
                //
                //                    }
            }


        }

    }

    public void docTypeVCE(ValueChangeEvent vce) {
        if (vce != null) {
            vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
            setPoType();
        }
    }

    public void setPoType() {

        String chlln_type = getBindChllnType().getValue().toString();
        if (chlln_type != null && getBindDocType().getValue() != null) {
            String doc_Type = getBindDocType().getValue().toString();
            if (chlln_type.equals("MRA") && doc_Type.equals("PUC")) {
                getBindPoType().setValue("P");

            }

            if (chlln_type.equals("MRA") && doc_Type.equals("CSM")) {
                getBindPoType().setValue("O");

            }

            if (chlln_type.equals("MRA") && doc_Type.equals("VDC")) {
                getBindPoType().setValue("J");

            }
            if (chlln_type.equals("JOB") && doc_Type.equals("JOB")) {
                getBindPoType().setValue("S");
            }
            if (chlln_type.equals("REO") && doc_Type.equals("CRC")) {
                getBindPoType().setValue("V");

            }
            if (chlln_type.equals("REO") && doc_Type.equals("CSC")) {
                getBindPoType().setValue("S");

            }
            if (chlln_type.equals("MRR") && doc_Type.equals("VDC")) {
                getBindPoType().setValue("O");

            }
            if (chlln_type.equals("ARG") && doc_Type.equals("ARG")) {
                getBindPoType().setValue("R");

            }
            if (chlln_type.equals("FOC") && doc_Type.equals("FOC")) {
                getBindPoType().setValue("O");

            }
            if (chlln_type.equals("NOC") && doc_Type.equals("NOC")) {
                getBindPoType().setValue("O");

            }
            if (chlln_type.equals("FGE") && doc_Type.equals("FGE")) {
                getBindPoType().setValue("O");

            }
            if (chlln_type.equals("API") && doc_Type.equals("API")) {
                getBindPoType().setValue("I");

            }

        }

    }


    public void DuplicateBillNo(FacesContext facesContext, UIComponent uIComponent, Object object) {

        // String vendor = (String)suppNameBinding.getValue();
        String vendor = (String) svcBinding.getValue();
        String doct = (String) bindDocType.getValue();
        String Billno = (String) object;
        System.out.println("values in validator" + vendor + doct + Billno);
        if (vendor != null && doct != null && Billno != null) {
            OperationBinding op = ADFUtils.findOperation("getFinYearValue");
            op.getParamsMap().put("doct", doct);
            //op.getParamsMap().put("pvnu", pvnu);
            op.getParamsMap().put("supply", vendor);
            op.getParamsMap().put("Billno", Billno);
            op.execute();
            if (op.getResult() != null && op.getResult().equals("Y")) {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                              "Bill No. Already Entered For Same Vendor.", null));
            }
        }


        ////commented on 23-03-2018
        //        System.out.println();
        //        if(object!=null && getBindBillNo().getValue()!=null){
        //            String fr_yr = getBindBillNo().getValue().toString();
        //          // Integer FrCnt=Integer.parseInt(fr_yr);
        //
        //           // System.out.println("---Fin Year fr_yr--- >"+fr_yr+"  FrCnt==>"+FrCnt);
        //           if(!fr_yr.equals("0"))
        //          //if(FrCnt>1)
        //            {
        //
        //                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Bill No. Already Entered For Same Vendor.", null));
        //
        //
        //                }
        //
        //
        //            }

    }

    public void setBindDocType(RichSelectOneChoice bindDocType) {
        this.bindDocType = bindDocType;
    }

    public RichSelectOneChoice getBindDocType() {
        return bindDocType;
    }

    public void setBindChllnType(RichSelectOneChoice bindChllnType) {
        this.bindChllnType = bindChllnType;
    }

    public RichSelectOneChoice getBindChllnType() {
        return bindChllnType;
    }

    public void setBindPoType(RichSelectOneChoice bindPoType) {
        this.bindPoType = bindPoType;
    }

    public RichSelectOneChoice getBindPoType() {
        return bindPoType;
    }


    public void setBindRetVal(RichSelectOneChoice bindRetVal) {
        this.bindRetVal = bindRetVal;
    }

    public RichSelectOneChoice getBindRetVal() {
        return bindRetVal;
    }

    public void setBindBillNo(RichInputText bindBillNo) {
        this.bindBillNo = bindBillNo;
    }

    public RichInputText getBindBillNo() {
        return bindBillNo;
    }

    public void setBindGteEtime(RichInputText bindGteEtime) {
        this.bindGteEtime = bindGteEtime;
    }

    public RichInputText getBindGteEtime() {
        return bindGteEtime;
    }

    public void setBindGteENo(RichInputText bindGteENo) {
        this.bindGteENo = bindGteENo;
    }

    public RichInputText getBindGteENo() {
        return bindGteENo;
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete1").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(createGateEntryTableBinding);
    }

    public void setCreateGateEntryTableBinding(RichTable createGateEntryTableBinding) {
        this.createGateEntryTableBinding = createGateEntryTableBinding;
    }

    public RichTable getCreateGateEntryTableBinding() {
        return createGateEntryTableBinding;
    }

    public void setCreateDocAttachTableBinding(RichTable createDocAttachTableBinding) {
        this.createDocAttachTableBinding = createDocAttachTableBinding;
    }

    public RichTable getCreateDocAttachTableBinding() {
        return createDocAttachTableBinding;
    }

    public void deleteDocAttachPopupDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete4").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(createDocAttachTableBinding);
    }

    public void deletePopupChallanDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete2").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(createChallanTableBinding);
    }

    public void setCreateChallanTableBinding(RichTable createChallanTableBinding) {
        this.createChallanTableBinding = createChallanTableBinding;
    }

    public RichTable getCreateChallanTableBinding() {
        return createChallanTableBinding;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setDateBinding(RichInputDate dateBinding) {
        this.dateBinding = dateBinding;
    }

    public RichInputDate getDateBinding() {
        try {
                    System.out.println("set getEntryDateBinding"+ADFUtils.getTodayDate());
                    Object date=ADFUtils.convertAdToBs(ADFUtils.getTodayDate().toString());
                    System.out.println("setter getvaluedate"+date);
//                    gateEdNepalbinding.setValue(date.toString());
                    DCIteratorBinding dci = ADFUtils.findIterator("GtEheadsVO1Iterator");
                    dci.getCurrentRow().setAttribute("GateEdtNepal",date);
                } catch (ParseException e) {
       }
        return dateBinding;
    }

    public void setTransporterNameBinding(RichInputText transporterNameBinding) {
        this.transporterNameBinding = transporterNameBinding;
    }

    public RichInputText getTransporterNameBinding() {
        return transporterNameBinding;
    }

    public void setSvcBinding(RichInputComboboxListOfValues svcBinding) {
        this.svcBinding = svcBinding;
    }

    public RichInputComboboxListOfValues getSvcBinding() {
        return svcBinding;
    }

    public void setVensupNameBinding(RichInputText vensupNameBinding) {
        this.vensupNameBinding = vensupNameBinding;
    }

    public RichInputText getVensupNameBinding() {
        return vensupNameBinding;
    }

    public void setSuppNameBinding(RichInputText suppNameBinding) {
        this.suppNameBinding = suppNameBinding;
    }

    public RichInputText getSuppNameBinding() {
        return suppNameBinding;
    }

    public void setSuppByBinding(RichInputComboboxListOfValues suppByBinding) {
        this.suppByBinding = suppByBinding;
    }

    public RichInputComboboxListOfValues getSuppByBinding() {
        return suppByBinding;
    }

    public void setSuppByNameBinding(RichInputText suppByNameBinding) {
        this.suppByNameBinding = suppByNameBinding;
    }

    public RichInputText getSuppByNameBinding() {
        return suppByNameBinding;
    }

    public void setSrvNobinding(RichInputText srvNobinding) {
        this.srvNobinding = srvNobinding;
    }

    public RichInputText getSrvNobinding() {
        return srvNobinding;
    }

    public void setCityCodeBinding(RichSelectOneRadio cityCodeBinding) {
        this.cityCodeBinding = cityCodeBinding;
    }

    public RichSelectOneRadio getCityCodeBinding() {
        return cityCodeBinding;
    }

    public void setPlantCodeBinding(RichInputText plantCodeBinding) {
        this.plantCodeBinding = plantCodeBinding;
    }

    public RichInputText getPlantCodeBinding() {
        return plantCodeBinding;
    }

    public void setPoJoSoRgNoBinding(RichInputComboboxListOfValues poJoSoRgNoBinding) {
        this.poJoSoRgNoBinding = poJoSoRgNoBinding;
    }

    public RichInputComboboxListOfValues getPoJoSoRgNoBinding() {
        return poJoSoRgNoBinding;
    }

    public void setAmdNoBinding(RichInputText amdNoBinding) {
        this.amdNoBinding = amdNoBinding;
    }

    public RichInputText getAmdNoBinding() {
        return amdNoBinding;
    }

    public void setPoDateBinding(RichInputDate poDateBinding) {
        this.poDateBinding = poDateBinding;
    }

    public RichInputDate getPoDateBinding() {
        return poDateBinding;
    }

    public void setItemCdBinding(RichInputComboboxListOfValues itemCdBinding) {
        this.itemCdBinding = itemCdBinding;
    }

    public RichInputComboboxListOfValues getItemCdBinding() {
        return itemCdBinding;
    }

    public void setItemDescBinding(RichInputText itemDescBinding) {
        this.itemDescBinding = itemDescBinding;
    }

    public RichInputText getItemDescBinding() {
        return itemDescBinding;
    }

    public void setChallanQtyBinding(RichInputText challanQtyBinding) {
        this.challanQtyBinding = challanQtyBinding;
    }

    public RichInputText getChallanQtyBinding() {
        return challanQtyBinding;
    }

    public void setChallanUOMBinding(RichInputText challanUOMBinding) {
        this.challanUOMBinding = challanUOMBinding;
    }

    public RichInputText getChallanUOMBinding() {
        return challanUOMBinding;
    }

    public void setShortDescriptionBnding(RichInputText shortDescriptionBnding) {
        this.shortDescriptionBnding = shortDescriptionBnding;
    }

    public RichInputText getShortDescriptionBnding() {
        return shortDescriptionBnding;
    }


    public void setSeqBinding(RichInputText seqBinding) {
        this.seqBinding = seqBinding;
    }

    public RichInputText getSeqBinding() {
        return seqBinding;
    }

    public void setFgrejBinding(RichSelectOneChoice fgrejBinding) {
        this.fgrejBinding = fgrejBinding;
    }

    public RichSelectOneChoice getFgrejBinding() {
        return fgrejBinding;
    }

    public void setPoDetHsnBinding(RichInputText poDetHsnBinding) {
        this.poDetHsnBinding = poDetHsnBinding;
    }

    public RichInputText getPoDetHsnBinding() {
        return poDetHsnBinding;
    }

    public void EditButtonAL(ActionEvent actionEvent) {
        if (appByBinding.getValue() != null) {
            ADFUtils.showMessage("This Gate Entry is Approved.Hence it cannot be edited further.", 2);
            ADFUtils.setEL("#{pageFlowScope.mode}", "V");
        } else {
            cevmodecheck();
            OperationBinding op = ADFUtils.findOperation("disableChallanQty");
            Object ob = op.execute();
            if (op.getResult() != null && op.getResult().equals("Y")) {

                getChallanQtyBinding().setDisabled(true);

            }

            Row selectedRow = (Row) ADFUtils.evaluateEL("#{bindings.GtEheadsVO1Iterator.currentRow}");
            System.out.println("status of selected row isss==>>" + selectedRow.getAttribute("Status"));
            if (selectedRow.getAttribute("Status").toString().equalsIgnoreCase("C")) {

                ADFUtils.showMessage("This Gate Entry is cancelled.You Cannot Update", 1);
                ADFUtils.setEL("#{pageFlowScope.mode}", "V");
                cevmodecheck();
                System.out.println("mode is====>>>" + ADFUtils.resolveExpression("#{pageFlowScope.mode}"));

            }
            if (selectedRow.getAttribute("Status").toString().equalsIgnoreCase("S") &&
                selectedRow.getAttribute("SrvHeadSrvNo") != null) {

                ADFUtils.showMessage("SRV is created of this Gate Entry.Hence cannot be Modified.", 1);
                ADFUtils.setEL("#{pageFlowScope.mode}", "V");
                cevmodecheck();
                System.out.println("mode is====>>>" + ADFUtils.resolveExpression("#{pageFlowScope.mode}"));

            }

            if (prepBinding.getValue() != null) {

                prepBinding.setDisabled(true);
            }
        }
    }

    public void setHeaderEditButton(RichButton headerEditButton) {
        this.headerEditButton = headerEditButton;
    }

    public RichButton getHeaderEditButton() {
        return headerEditButton;
    }

    public void setGetMyPageRootBinding(RichPanelHeader getMyPageRootBinding) {
        this.getMyPageRootBinding = getMyPageRootBinding;
    }

    public RichPanelHeader getGetMyPageRootBinding() {
        return getMyPageRootBinding;
    }

    public void setGetOutputTextBinding(RichOutputText getOutputTextBinding) {
        this.getOutputTextBinding = getOutputTextBinding;
    }

    public RichOutputText getGetOutputTextBinding() {
        cevmodecheck();
        return getOutputTextBinding;
    }

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }

    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }

    public void cevModeDisableComponent(String mode) {
        //            FacesContext fctx = FacesContext.getCurrentInstance();
        //            ELContext elctx = fctx.getELContext();
        //            Application jsfApp = fctx.getApplication();
        //            //create a ValueExpression that points to the ADF binding layer
        //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
        //            //
        //            ValueExpression valueExpr = exprFactory.createValueExpression(
        //                                         elctx,
        //                                         "#{pageFlowScope.mode=='E'}",
        //                                          Object.class
        //                                         );
        //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
        if (mode.equals("E")) {

            //            if(bindChllnType.getValue().equals("NOC")){
            getAppByBinding().setDisabled(false);


            //            }
            //            else{
            //                            getAppByBinding().setDisabled(true);
            //
            //            }
            getUnitCodeBinding().setDisabled(true);
            getSvcBinding().setDisabled(true);
            getBindRetVal().setDisabled(true);
            getSuppByBinding().setDisabled(true);
            //           getVenHsnBinding().setDisabled(true);
            getItemCdBinding().setDisabled(false);
            getBindGteENo().setDisabled(true);
            getUnitNameBinding().setDisabled(true);
            getDateBinding().setDisabled(true);
            getTransporterNameBinding().setDisabled(true);
            getBindPoType().setDisabled(true);
            getVensupNameBinding().setDisabled(true);
            getSuppNameBinding().setDisabled(true);
            getSuppByNameBinding().setDisabled(true);
            getSrvNobinding().setDisabled(true);
            getBindGteEtime().setDisabled(true);
            getCityCodeBinding().setDisabled(true);
            getPlantCodeBinding().setDisabled(true);
            getAmdNoBinding().setDisabled(true);
            getPoDateBinding().setDisabled(true);
            getItemDescBinding().setDisabled(true);
            //getChallanQtyBinding().setDisabled(true);
            getChallanUOMBinding().setDisabled(true);
            getShortDescriptionBnding().setDisabled(true);
            getSeqBinding().setDisabled(true);
            getFgrejBinding().setDisabled(true);
            getPoDetHsnBinding().setDisabled(true);
            getCityBinding().setDisabled(true);
            getPrepBinding().setDisabled(true);
            getHeaderEditButton().setDisabled(true);
            getDetailCreateBinding().setDisabled(false);
            //            getDetailDeletmodeeqeBinding().setDisabled(false);
            getBindChllnType().setDisabled(true);
            getGstBillNoBinding().setDisabled(true);
            getDocDateBinding().setDisabled(true);
            getInvoiceValueBinding().setDisabled(true);
            getNoOfpacketsBinding().setDisabled(true);
            getMriFlagBinding().setDisabled(true);
            itemTypeBinding.setDisabled(true);
            //            getVenHsnBinding().setDisabled(true);
            //            venhsnBinding.setDisabled(true);

        } else if (mode.equals("C")) {
            //            mriFlagBinding.setDisabled(true);
            getAppByBinding().setDisabled(true);
            itemTypeBinding.setDisabled(true);
            if (bindChllnType.getValue().equals("MRA")) {
                //               getAppByBinding().setDisabled(true);
                getDeptBinding().setDisabled(true);
                getEmpBinding().setDisabled(true);
                getCreditBinding().setDisabled(true);

            } else {
                //                getAppByBinding().setDisabled(false);
                getDeptBinding().setDisabled(false);
                getEmpBinding().setDisabled(false);
                getCreditBinding().setDisabled(false);
            }
            //            if(mriFlagBinding.getValue().toString().equalsIgnoreCase("N")){
            //            mriFlagBinding.setDisabled(false);
            //            }else{
            //                mriFlagBinding.setDisabled(true);
            //            }
            //            if(itemTypeBinding.getValue().toString().equalsIgnoreCase("O")){
            //                itemTypeBinding.setDisabled(false);
            //            }
            //            else{
            //                itemTypeBinding.setDisabled(true);
            //            }
            //            venhsnBinding.setDisabled(true);
            getAppByBinding().setDisabled(true);
            //            getCashAmtBinding().setDisabled(true);
            //            getVenHsnBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
            getCityBinding().setDisabled(true);
            getBindGteENo().setDisabled(true);
            getUnitNameBinding().setDisabled(true);
            getDateBinding().setDisabled(true);
            getTransporterNameBinding().setDisabled(true);
            getBindPoType().setDisabled(true);
            getVensupNameBinding().setDisabled(true);
            getSuppNameBinding().setDisabled(true);
            getSuppByNameBinding().setDisabled(true);
            getSrvNobinding().setDisabled(true);
            getBindGteEtime().setDisabled(true);
            getCityCodeBinding().setDisabled(true);
            getPlantCodeBinding().setDisabled(true);
            getAmdNoBinding().setDisabled(true);
            getPoDateBinding().setDisabled(true);
            getItemDescBinding().setDisabled(true);
            //   getChallanQtyBinding().setDisabled(true);
            getChallanUOMBinding().setDisabled(true);
            getShortDescriptionBnding().setDisabled(true);
            getSeqBinding().setDisabled(true);
            getFgrejBinding().setDisabled(true);
            getPoDetHsnBinding().setDisabled(true);
            //            getVenHsnBinding().setDisabled(true);
            getHeaderEditButton().setDisabled(true);
            getDetailCreateBinding().setDisabled(false);
            // getDetailDeleteBinding().setDisabled(false);
            getDevAppByBinding().setDisabled(true);
            getPrepBinding().setDisabled(true);


        } else if (mode.equals("V")) {
            getTab2Bindingoth().setDisabled(false);
            getTab1Binding().setDisabled(false);
            getTab2Binding().setDisabled(false);
            getDetailCreateBinding().setDisabled(true);
            getDetailDeleteBinding().setDisabled(true);
            getTab1BindingDtl().setDisabled(false);
        }
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setDetailCreateBinding(RichButton detailCreateBinding) {
        this.detailCreateBinding = detailCreateBinding;
    }

    public RichButton getDetailCreateBinding() {
        return detailCreateBinding;
    }

    public void setDetailDeleteBinding(RichButton detailDeleteBinding) {
        this.detailDeleteBinding = detailDeleteBinding;
    }

    public RichButton getDetailDeleteBinding() {
        return detailDeleteBinding;
    }

    public void setUnitNameBinding(RichInputText unitNameBinding) {
        this.unitNameBinding = unitNameBinding;
    }

    public RichInputText getUnitNameBinding() {
        return unitNameBinding;
    }

    public void setCityBinding(RichInputText cityBinding) {
        this.cityBinding = cityBinding;
    }

    public RichInputText getCityBinding() {
        return cityBinding;
    }

    public void detailCreateButtonAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert3").execute();

        getBindRetVal().setDisabled(true);
        getBindChllnType().setDisabled(true);
        getGstBillNoBinding().setDisabled(true);
        getDocDateBinding().setDisabled(true);
        getInvoiceValueBinding().setDisabled(true);
        getNoOfpacketsBinding().setDisabled(true);
        getSvcBinding().setDisabled(true);
        recAgBinding.setDisabled(true);
        if (bindChllnType.getValue().toString().equalsIgnoreCase("MRA")) {
            getCashAmtBinding().setDisabled(true);
        } else {
            getCashAmtBinding().setDisabled(false);
        }
    }
    // Add event code here...


    public void setGstBillNoBinding(RichInputText gstBillNoBinding) {
        this.gstBillNoBinding = gstBillNoBinding;
    }

    public RichInputText getGstBillNoBinding() {
        return gstBillNoBinding;
    }

    public void setDocDateBinding(RichInputDate docDateBinding) {
        this.docDateBinding = docDateBinding;
    }

    public RichInputDate getDocDateBinding() {
        try {
                    System.out.println("set getEntryDateBinding" + ADFUtils.getTodayDate());
                    String date = ADFUtils.convertAdToBs(ADFUtils.getTodayDate().toString());
                    System.out.println("setter getvaluedate" + date);
                    //            reqDateNepalBinding.setValue((String)date);
                    DCIteratorBinding dci = ADFUtils.findIterator("GtEheadsVO1Iterator");
                    dci.getCurrentRow().setAttribute("DocDtNepal", date);
                    //            Row row=(Row)ADFUtils.evaluateEL("#{bindings.ContractRequisitionHeaderVO1Iterator.currentRow}");
                    //            row.setAttribute("ReqDateNp", date);
                } catch (ParseException e) {
                } 
        return docDateBinding;
    }

    public void setInvoiceValueBinding(RichInputText invoiceValueBinding) {
        this.invoiceValueBinding = invoiceValueBinding;
    }

    public RichInputText getInvoiceValueBinding() {
        return invoiceValueBinding;
    }

    public void setNoOfpacketsBinding(RichInputText noOfpacketsBinding) {
        this.noOfpacketsBinding = noOfpacketsBinding;
    }

    public RichInputText getNoOfpacketsBinding() {
        return noOfpacketsBinding;
    }

    public void poJoSoRgDetailVCE(ValueChangeEvent vce) {
        System.out.println("vce.getNewValue()=>" + vce.getNewValue() + "   vce.getOldValue()==>" + vce.getOldValue());
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce != null) {
            if (!vce.getNewValue().equals(vce.getOldValue())) {
                System.out.println("Insdie vce.getNewValue()=>" + vce.getNewValue() + "\nvce.getOldValue()=>" +
                                   vce.getOldValue());
                System.out.println("************Inside VCE***************");
                ADFUtils.findOperation("clearSelectedRowGateEntry").execute();
                System.out.println("************END VCE***************");
            }
        }
        // Add event code here...
    }

    public void procCodeValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        System.out.println("sop,obj before===" + object);
        BigDecimal a = new BigDecimal(0);

        if (object == null || object.equals(".") || object.equals("0") || object.toString().isEmpty()) {
            System.out.println("sop object" + object);
            String poType = (String) bindPoType.getValue();
            if (poType.equals("J")) {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                              "Process Code is required in case of JobWork Order.",
                                                              null));
            }

        }

    }


    public void poNoValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        //        if(object == null && bindChllnType.getValue()!=null){
        //
        //            String chlnType=bindChllnType.getValue().toString();
        //            System.out.println("challan type in validator PRIYANKA"+chlnType);
        //            if(chlnType.equals("MRA")|| chlnType.equals("REO") || chlnType.equals("MRR")||
        //               chlnType.equals("ARG")||chlnType.equals("API") ){
        //
        //                   throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "PO/JO/SO/RG No is required.", null));
        //               }
        //        }


    }

    public void challanQtyValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {

        if (object != null) {
            BigDecimal a = new BigDecimal(0);
            BigDecimal chllnqty = (BigDecimal) object;
            System.out.println("accept qty is===>" + chllnqty);
            if (object != null && (chllnqty.compareTo(a) == -1 || chllnqty.compareTo(a) == 0)) {
                System.out.println("iside compare" + chllnqty);

                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                              "Challan Qty. must be greater 0.", null));
            } else {
            }
        }

    }

    public void challanQtyVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        String challan_type = (String) bindChllnType.getValue();
        System.out.println("Challan value is "+challan_type);
        if (vce != null) {
            System.out.println("value of potype2" + potype2Binding.getValue());
            if (potype2Binding.getValue() != (null) && potype2Binding.getValue().equals("O")) {
                OperationBinding op = ADFUtils.findOperation("ChallanqtyFunctioncall");
                Integer ob = (Integer) op.execute();
                System.out.println("object value" + ob);
                if (ob != null) {
                    BigDecimal i = new BigDecimal(ob);
                    System.out.println("value in BigDecimal" + i);
                    BigDecimal qw = (BigDecimal) challanQtyBinding.getValue();
                    System.out.println("value of challan qty" + qw);
                    if (qw.compareTo(i) == 1) {
                        FacesMessage message = new FacesMessage("Challan Qty must be less than Schedule Qty");
                        message.setSeverity(FacesMessage.SEVERITY_ERROR);
                        FacesContext context = FacesContext.getCurrentInstance();
                        context.addMessage(challanQtyBinding.getClientId(), message);
                        challanQtyBinding.setValue(null);

                        //            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Challan Qty.must be less than Schedule Qty.", null));
                    }

                }
            } else if(challan_type.equalsIgnoreCase("MRA")){
                OperationBinding binding = ADFUtils.findOperation("challanQtyBalance");
                //   binding.getParamsMap().put("gateEntry", vce.getNewValue().toString());
                //            Object opp=binding.execute();

                String ob = (String) binding.execute();
                BigDecimal value = new BigDecimal(0);
                if(ob!=null && !ob.equalsIgnoreCase(""))
                {
                    value = new BigDecimal(ob);
                }
                else
                {
                    value = new BigDecimal(0);
                }
                                    
                System.out.println("object value" + ob);
                System.out.println("ChllanQnty New " + vce.getNewValue());
                BigDecimal newValue = (BigDecimal) vce.getNewValue();
                //                System.out.println("Result is===>>" + binding.getResult());

                if (ob != null && newValue.compareTo(value) == 1){
                    //  System.out.println("inside else ===>>" + binding.getResult());
                    FacesMessage message = new FacesMessage("Challan Qty cannot be more than PO Qty for this Item.");
                    //  binding.getResult().toString());
                    message.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext context = FacesContext.getCurrentInstance();
                    context.addMessage(challanQtyBinding.getClientId(), message);
                    challanQtyBinding.setValue(null);

                }

            }
        }
    }

    public String saveAndCloseButton() {

        String empcd = (String) ADFUtils.resolveExpression("#{pageFlowScope.empCode}");
        System.out.println("empcd" + empcd);
        ADFUtils.setColumnValue("GtEheadsVO1Iterator", "LastUpdatedBy", empcd);

        String challan1 = (String) bindChllnType.getValue();
        System.out.println("challan1*********==??" + challan1);
        //        if(challan1.equalsIgnoreCase("NOC") && deptBinding.getValue()==null && empBinding.getValue()==null){
        //            ADFUtils.showMessage("Department Code and Employee Code is required.", 0);
        //            deptBinding.setRequired(true);
        //            empBinding.setRequired(true);
        //        }
        //        else{
        String itemType = (String) itemTypeBinding.getValue();
        String manf = (String) manfCodeBinding.getValue();
        System.out.println("Item type on save is===>>" + itemType + "manf code ===>>" + manf);
        if (manf == null && !itemType.equalsIgnoreCase("O")) {
            if (itemType.equalsIgnoreCase("L") || itemType.equalsIgnoreCase("R")) {
                ADFUtils.showMessage("Manufacturer Code is Required.", 0);
            }
        } else {

            String rec = (String) recAgBinding.getValue();
            String challan = (String) bindChllnType.getValue();
            if (challan.equalsIgnoreCase("MRA")) {
                if (challan.equalsIgnoreCase("MRA") && (rec.equalsIgnoreCase("P") || rec.equalsIgnoreCase("W"))) {
                    OperationBinding ob = ADFUtils.findOperation("sumChallanQtyInChallan");
                    Object op1 = ob.execute();
                    if (ob.getResult() != null && ob.getResult().equals("N")) {
                        ADFUtils.showMessage(" Quantity must be equal to the entered Challan qty.", 0);

                    } else {

                        OperationBinding binding = ADFUtils.findOperation("challanQty");
                        //   binding.getParamsMap().put("gateEntry", vce.getNewValue().toString());
                        Object opp = binding.execute();

                        //  if(binding.getResult().equals("N"))
                        //   {
                        if (binding.getResult() != null && binding.getResult().equals("N")) {

                            if (getBindGteENo().getValue() == null) {

                                System.out.println("what the sys time is in bean while commiting--->" +
                                                   new java.util.Date(System.currentTimeMillis()));
                                java.util.Date dt = new java.util.Date(System.currentTimeMillis());
                                String dateWithoutTime = dt.toString().substring(10, 16);

                                System.out.println(" is time extracted from date in bean??" + dateWithoutTime);
                                getBindGteEtime().setValue(dateWithoutTime);
                                AdfFacesContext.getCurrentInstance().addPartialTarget(getBindGteEtime());
                            }
                            OperationBinding op = ADFUtils.findOperation("getGateEntryNo");
                            Object rst = op.execute();
                            //ADFUtils.findOperation("goForUnitRate").execute();
                            System.out.println("--------Commit-------");
                            System.out.println("VALUE AFTER GETTING RESULT =================>   " + rst);


                            if (rst.toString() != null && rst.toString() != "") {
                                if (op.getErrors().isEmpty()) {
                                    if (bindChllnType.getValue() != null && bindChllnType.getValue().equals("REO") ||
                                        bindChllnType.getValue().equals("ARG") ||
                                        bindChllnType.getValue().equals("FOC") ||
                                        bindChllnType.getValue().equals("NOC")) {
                                        appByBinding.setValue(prepBinding.getValue());
                                    }

                                    ADFUtils.findOperation("Commit").execute();
                                    FacesMessage Message =
                                        new FacesMessage("Record Saved Successfully.Gate Entry No is " + rst + ".");
                                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
                                    FacesContext fc = FacesContext.getCurrentInstance();
                                    fc.addMessage(null, Message);
                                    return "Save";

                                }
                            }

                            if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ") {
                                if (op.getErrors().isEmpty()) {
                                    ADFUtils.findOperation("Commit").execute();
                                    FacesMessage Message = new FacesMessage("Record Update Successfully.");
                                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
                                    FacesContext fc = FacesContext.getCurrentInstance();
                                    fc.addMessage(null, Message);
                                    return "Save";

                                }


                            }
                        } else {
                            System.out.println("inside else ===>>" + binding.getResult());
                            FacesMessage message =
                                new FacesMessage("Challan Qty cannot be more than PO Qty for this Item : " +
                                                 binding.getResult().toString());
                            message.setSeverity(FacesMessage.SEVERITY_ERROR);
                            FacesContext context = FacesContext.getCurrentInstance();
                            context.addMessage(challanQtyBinding.getClientId(), message);
                        }
                    }

                    //        }
                } else {

                    if (challan.equalsIgnoreCase("MRA") && rec.equalsIgnoreCase("R")) {
                        System.out.println("inside 1st if");
                        FacesMessage message = new FacesMessage("This Challan Type is not allowed with Against RGP.");
                        message.setSeverity(FacesMessage.SEVERITY_ERROR);
                        FacesContext context = FacesContext.getCurrentInstance();
                        context.addMessage(recAgBinding.getClientId(), message);
                    }
                    if (challan.equalsIgnoreCase("MRA") && rec.equalsIgnoreCase("I")) {
                        System.out.println("inside 2nd if");
                        FacesMessage message =
                            new FacesMessage("This Challan Type is not allowed with Against Sales Invoice.");
                        message.setSeverity(FacesMessage.SEVERITY_ERROR);
                        FacesContext context = FacesContext.getCurrentInstance();
                        context.addMessage(recAgBinding.getClientId(), message);
                    }
                    if (challan.equalsIgnoreCase("MRA") && rec.equalsIgnoreCase("C")) {
                        System.out.println("inside 3rd if");
                        FacesMessage message =
                            new FacesMessage("This Challan Type is not allowed with Against Contract.");
                        message.setSeverity(FacesMessage.SEVERITY_ERROR);
                        FacesContext context = FacesContext.getCurrentInstance();
                        context.addMessage(recAgBinding.getClientId(), message);
                    }

                }
            } else {
                OperationBinding ob = ADFUtils.findOperation("sumChallanQtyInChallan");
                Object op1 = ob.execute();
                if (ob.getResult() != null && ob.getResult().equals("N")) {
                    ADFUtils.showMessage(" Quantity must be equal to the entered Challan qty.", 0);

                } else {

                    OperationBinding binding = ADFUtils.findOperation("challanQty");
                    //   binding.getParamsMap().put("gateEntry", vce.getNewValue().toString());
                    Object opp = binding.execute();

                    System.out.println("Result is===>>" + binding.getResult());
                    if (binding.getResult() != null && binding.getResult().equals("N")) {

                        if (getBindGteENo().getValue() == null) {

                            System.out.println("what the sys time is in bean while commiting--->" +
                                               new java.util.Date(System.currentTimeMillis()));
                            java.util.Date dt = new java.util.Date(System.currentTimeMillis());
                            String dateWithoutTime = dt.toString().substring(10, 16);

                            System.out.println(" is time extracted from date in bean??" + dateWithoutTime);
                            getBindGteEtime().setValue(dateWithoutTime);
                            AdfFacesContext.getCurrentInstance().addPartialTarget(getBindGteEtime());
                        }
                        OperationBinding op = ADFUtils.findOperation("getGateEntryNo");
                        Object rst = op.execute();
                        //ADFUtils.findOperation("goForUnitRate").execute();
                        System.out.println("--------Commit-------");
                        System.out.println("VALUE AFTER GETTING RESULT =================>   " + rst);


                        if (rst.toString() != null && rst.toString() != "") {
                            if (op.getErrors().isEmpty()) {

                                if (bindChllnType.getValue() != null && bindChllnType.getValue().equals("REO") ||
                                    bindChllnType.getValue().equals("ARG") || bindChllnType.getValue().equals("FOC") ||
                                    bindChllnType.getValue().equals("NOC")) {
                                    appByBinding.setValue(prepBinding.getValue());
                                }

                                ADFUtils.findOperation("Commit").execute();
                                FacesMessage Message =
                                    new FacesMessage("Record Saved Successfully.Gate Entry No is " + rst + ".");
                                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                                FacesContext fc = FacesContext.getCurrentInstance();
                                fc.addMessage(null, Message);
                                return "Save";

                            }
                        }

                        if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ") {
                            if (op.getErrors().isEmpty()) {
                                ADFUtils.findOperation("Commit").execute();
                                FacesMessage Message = new FacesMessage("Record Update Successfully.");
                                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                                FacesContext fc = FacesContext.getCurrentInstance();
                                fc.addMessage(null, Message);
                                return "Save";
                            }


                        }
                    } else {
                        System.out.println("inside else ===>>" + binding.getResult());
                        FacesMessage message =
                            new FacesMessage("Challan Qty cannot be more than PO Qty for this Item : " +
                                             binding.getResult().toString());
                        message.setSeverity(FacesMessage.SEVERITY_ERROR);
                        FacesContext context = FacesContext.getCurrentInstance();
                        context.addMessage(challanQtyBinding.getClientId(), message);
                    }
                }
            }
        }
        //  }

        return null;


    }

    public void setTab1Binding(RichShowDetailItem tab1Binding) {
        this.tab1Binding = tab1Binding;
    }

    public RichShowDetailItem getTab1Binding() {
        return tab1Binding;
    }

    public void setTab2Binding(RichShowDetailItem tab2Binding) {
        this.tab2Binding = tab2Binding;
    }

    public RichShowDetailItem getTab2Binding() {
        return tab2Binding;
    }

    public void qtyVCE(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (valueChangeEvent != null) {

            OperationBinding ob = ADFUtils.findOperation("sumChallanQtyInChallan");
            Object op = ob.execute();
            if (ob.getResult() != null && ob.getResult().equals("N")) {
                ADFUtils.showMessage(" Quantity Qty must be equal to the entered Challan qty.", 0);

            }


        }


    }

    public void challanButtonDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok") || dialogEvent.getOutcome().name().equals("cancel")) {

            System.out.println("inside yes outcome of dialog=========>>>");
            OperationBinding ob = ADFUtils.findOperation("sumChallanQtyInChallan");
            Object op = ob.execute();
            if (ob.getResult() != null && ob.getResult().equals("N")) {
                ADFUtils.showMessage(" Quantity must be equal to the entered Challan qty.", 0);

            }
            if (dialogEvent.getOutcome().name().equals("cancel")) {

                System.out.println("inside yes outcome of dialog=========>>>");
                OperationBinding ob1 = ADFUtils.findOperation("sumChallanQtyInChallan");
                Object op1 = ob1.execute();
                if (ob1.getResult() != null && ob1.getResult().equals("N")) {
                    ADFUtils.showMessage(" Quantity must be equal to the entered Challan qty.", 0);

                }

            }
        }
    }

    public void setChallanDtlBtnBinding(RichButton challanDtlBtnBinding) {
        this.challanDtlBtnBinding = challanDtlBtnBinding;
    }

    public RichButton getChallanDtlBtnBinding() {
        return challanDtlBtnBinding;
    }

    public void invoiceValueValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {

        BigDecimal a = new BigDecimal(0);
        BigDecimal chllnqty = (BigDecimal) object;
        System.out.println("accept qty is===>" + chllnqty);
        //                   if(object != null && (chllnqty.compareTo(a)==-1 || chllnqty.compareTo(a)==0)){
        //                   System.out.println("iside compare"+chllnqty);
        //
        //                   throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Invoice Value must be greater 0.", null));
        //                   }
        //                   else{
        //
        //
        //                   }

        String bn = (String) billnoBinding.getValue();
        BigDecimal iv = (BigDecimal) invoiceValueBinding.getValue();
        if (bn == null && iv != null) {
            System.out.println("Inside validator to reset value!");
            invoiceValueBinding.setValue(null);
            System.out.println("Inside validator, value reset done!!");
        }
        //        else if(bn!=null && iv==null){
        //            System.out.println("Inside validator to print message!");
        //                FacesMessage Message = new FacesMessage("Please enter Invoice Value.");
        //                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
        //                FacesContext fc = FacesContext.getCurrentInstance();
        //                fc.addMessage(null, Message);
        //                System.out.println("Inside validator, Message Printed!!");
        //            }
        else {
            System.out.println("Both has values....!!!");
        }

    }


    public void setCashAmtBinding(RichInputText cashAmtBinding) {
        this.cashAmtBinding = cashAmtBinding;
    }

    public RichInputText getCashAmtBinding() {
        return cashAmtBinding;
    }

    public void setCancelPopupBinding(RichPopup cancelPopupBinding) {
        this.cancelPopupBinding = cancelPopupBinding;
    }

    public RichPopup getCancelPopupBinding() {
        return cancelPopupBinding;
    }

    public void cancelButtonAL(ActionEvent actionEvent) {
        if (getBindGteENo().getValue() != null) {

        }
    }

    public void cancelDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equalsIgnoreCase("yes")) {
            if (appByBinding.getValue() == null) {
                OperationBinding binding = ADFUtils.findOperation("checkForCancelAuthoGE");
                binding.getParamsMap().put("formname", "GNCN");
                binding.getParamsMap().put("empcd", ADFUtils.resolveExpression("#{pageFlowScope.empCode}"));
                //              binding.getParamsMap().put("empcd","SWE161");
                Integer rst = (Integer) binding.execute();
                System.out.println("Rst Value :- " + rst);
                if (rst != null) {
                    if (binding.getErrors().isEmpty()) {
                        if (rst == 0) {
                            ADFUtils.showMessage("Sorry you are not authorize to cancel gate entry.", 1);
                        } else {
                            OperationBinding op = ADFUtils.findOperation("setStatusToCancel");
                            Object ob = op.execute();
                            System.out.println("resuklt when cancel set status is==>>" + op.getResult());
                            if (op.getResult() != null && op.getResult().toString().equalsIgnoreCase("G")) {
                                ADFUtils.showMessage("Gate Entry cancelled successfully.", 2);
                                ADFUtils.findOperation("Commit").execute();
                                ADFUtils.setEL("#{pageFlowScope.mode}", "V");
                                cevmodecheck();
                                System.out.println("mode is====>>>" +
                                                   ADFUtils.resolveExpression("#{pageFlowScope.mode}"));

                            }
                            if (op.getResult() != null && op.getResult().toString().equalsIgnoreCase("C")) {
                                ADFUtils.showMessage("Gate Entry Already Cancelled.", 2);
                            }
                            if (op.getResult() != null && op.getResult().toString().equalsIgnoreCase("S")) {
                                ADFUtils.showMessage("SRV is created of this Gate Entry.Hence cannot be cancelled.", 2);
                            }
                            if (appByBinding.getValue() != null) {
                                ADFUtils.showMessage("This Gate Entry is Approved.It cannot be cancelled.", 2);
                            }
                        }
                    }
                }
            } else if (appByBinding.getValue() != null) {
                ADFUtils.showMessage("This Gate Entry is Approved.It cannot be cancelled.", 2);
            }
        }
    }

    public void setCancelBinding(RichButton cancelBinding) {
        this.cancelBinding = cancelBinding;
    }

    public RichButton getCancelBinding() {
        return cancelBinding;
    }

    public void setAppByBinding(RichInputComboboxListOfValues appByBinding) {
        this.appByBinding = appByBinding;
    }

    public RichInputComboboxListOfValues getAppByBinding() {
        return appByBinding;
    }

    public void setCreditBinding(RichSelectBooleanCheckbox creditBinding) {
        this.creditBinding = creditBinding;
    }

    public RichSelectBooleanCheckbox getCreditBinding() {
        return creditBinding;
    }

    public void setDeptBinding(RichInputComboboxListOfValues deptBinding) {
        this.deptBinding = deptBinding;
    }

    public RichInputComboboxListOfValues getDeptBinding() {
        return deptBinding;
    }

    public void setEmpBinding(RichInputComboboxListOfValues empBinding) {
        this.empBinding = empBinding;
    }

    public RichInputComboboxListOfValues getEmpBinding() {
        return empBinding;
    }

    public void setPrepBinding(RichInputComboboxListOfValues prepBinding) {
        this.prepBinding = prepBinding;
    }

    public RichInputComboboxListOfValues getPrepBinding() {
        return prepBinding;
    }


    public void appByVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        OperationBinding ob = ADFUtils.findOperation("checkApprovalStatus");
        ob.getParamsMap().put("formNm", "GNTR");
        ob.getParamsMap().put("authoLim", "AP");
        ob.getParamsMap().put("empcd", vce.getNewValue().toString());
        ob.execute();
        System.out.println(" EMP CD" + vce.getNewValue().toString());
        if ((ob.getResult() != null && ob.getResult().equals("Y"))) {
            Row row = (Row) ADFUtils.evaluateEL("#{bindings.GtEheadsVO1Iterator.currentRow}");
            row.setAttribute("ApprovedBy", null);
            ADFUtils.showMessage("You have no authority to approve this Gate Entry.", 0);
        }

    }

    public void devAppByVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        OperationBinding ob = ADFUtils.findOperation("checkApprovalStatus");
        ob.getParamsMap().put("formNm", "GNTR");
        ob.getParamsMap().put("authoLim", "DA");
        ob.getParamsMap().put("empcd", vce.getNewValue().toString());
        ob.execute();
        System.out.println(" EMP CD" + vce.getNewValue().toString());
        if ((ob.getResult() != null && ob.getResult().equals("Y"))) {
            Row row = (Row) ADFUtils.evaluateEL("#{bindings.GtEheadsVO1Iterator.currentRow}");
            row.setAttribute("DevAppBy", null);
            ADFUtils.showMessage("You have no authority to approve this Gate Entry.", 0);
        }

    }

    public void setDevAppByBinding(RichInputComboboxListOfValues devAppByBinding) {
        this.devAppByBinding = devAppByBinding;
    }

    public RichInputComboboxListOfValues getDevAppByBinding() {
        return devAppByBinding;
    }

    public void setRecAgBinding(RichSelectOneChoice recAgBinding) {
        this.recAgBinding = recAgBinding;
    }

    public RichSelectOneChoice getRecAgBinding() {
        return recAgBinding;
    }


    public void recAgainstValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (object != null) {

            String rec = (String) object;
            String challan = (String) bindChllnType.getValue();
            System.out.println("Rec against" + rec + "challn type==>>" + challan);
            if (challan.equalsIgnoreCase("MRA") && rec.equalsIgnoreCase("R")) {
                System.out.println("inside 1st if");
                FacesMessage message = new FacesMessage("This Challan Type is not allowed with Against RGP.");
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(recAgBinding.getClientId(), message);
            }
            if (challan.equalsIgnoreCase("MRA") && rec.equalsIgnoreCase("I")) {
                System.out.println("inside 2nd if");
                FacesMessage message = new FacesMessage("This Challan Type is not allowed with Against Sales Invoice.");
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(recAgBinding.getClientId(), message);
            }
            if (challan.equalsIgnoreCase("MRA") && rec.equalsIgnoreCase("C")) {
                System.out.println("inside 3rd if");
                FacesMessage message = new FacesMessage("This Challan Type is not allowed with Against Contract.");
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(recAgBinding.getClientId(), message);
            }
        }

    }

    public void setTab2Bindingoth(RichShowDetailItem tab2Bindingoth) {
        this.tab2Bindingoth = tab2Bindingoth;
    }

    public RichShowDetailItem getTab2Bindingoth() {
        return tab2Bindingoth;
    }

    public void setTab1BindingDtl(RichShowDetailItem tab1BindingDtl) {
        this.tab1BindingDtl = tab1BindingDtl;
    }

    public RichShowDetailItem getTab1BindingDtl() {
        return tab1BindingDtl;
    }

    public void itemCodeVCE(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (valueChangeEvent != null) {
            BigDecimal a = new BigDecimal(0);
            OperationBinding op = ADFUtils.findOperation("setItemTypeGateEntry");
            Object ob = op.execute();
            getDevyNBinding().setDisabled(true);
            System.out.println("item ddfrom binding===>>" + itemCdBinding.getValue());
            op.getParamsMap().put("itemCd", a);
            System.out.println("item type ==>>" + itemTypeBinding.getValue() + "  mri flag   " +
                               mriFlagBinding.getValue());

            if (itemTypeBinding.getValue().toString().equalsIgnoreCase("L") &&
                mriFlagBinding.getValue().toString().equalsIgnoreCase("Y")) {
                System.out.println("item type ==>>1" + itemTypeBinding.getValue() + "mri flag" +
                                   mriFlagBinding.getValue());
                itemTypeBinding.setDisabled(true);
                mriFlagBinding.setDisabled(true);
            }
            if (itemTypeBinding.getValue().toString().equalsIgnoreCase("O") &&
                mriFlagBinding.getValue().toString().equalsIgnoreCase("N")) {
                System.out.println("item type ==>>2" + itemTypeBinding.getValue() + "mri flag" +
                                   mriFlagBinding.getValue());
                itemTypeBinding.setDisabled(true);
                mriFlagBinding.setDisabled(true);

            }
            if (itemTypeBinding.getValue().toString().equalsIgnoreCase("R") &&
                mriFlagBinding.getValue().toString().equalsIgnoreCase("Y")) {
                System.out.println("item type ==>>3" + itemTypeBinding.getValue() + "mri flag" +
                                   mriFlagBinding.getValue());
                itemTypeBinding.setDisabled(true);
                mriFlagBinding.setDisabled(false);

            }


        }

    }

    public void setItemTypeBinding(RichSelectOneChoice itemTypeBinding) {
        this.itemTypeBinding = itemTypeBinding;
    }

    public RichSelectOneChoice getItemTypeBinding() {
        return itemTypeBinding;
    }

    public void setMriFlagBinding(RichSelectOneChoice mriFlagBinding) {
        this.mriFlagBinding = mriFlagBinding;
    }

    public RichSelectOneChoice getMriFlagBinding() {
        return mriFlagBinding;
    }

    public void itemTypeVCE(ValueChangeEvent valueChangeEvent) {
        //        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        //        if(valueChangeEvent != null){
        //            String itemTp=
        //            if(itemTypeBinding.getValue().toString().equalsIgnoreCase("L")){
        //                mriFlagBinding.setValue("L");
        //            }
        //            if(itemTypeBinding.getValue().toString().equalsIgnoreCase("R")){
        //                mriFlagBinding.setValue("R");
        //            }
        //
        //        }
    }

    public void setDevyNBinding(RichSelectOneChoice devyNBinding) {
        this.devyNBinding = devyNBinding;
    }

    public RichSelectOneChoice getDevyNBinding() {
        return devyNBinding;
    }

    public void setSrvReqBinding(RichSelectBooleanCheckbox srvReqBinding) {
        this.srvReqBinding = srvReqBinding;
    }

    public RichSelectBooleanCheckbox getSrvReqBinding() {
        return srvReqBinding;
    }

    public void setManfCodeBinding(RichInputComboboxListOfValues manfCodeBinding) {
        this.manfCodeBinding = manfCodeBinding;
    }

    public RichInputComboboxListOfValues getManfCodeBinding() {
        return manfCodeBinding;
    }

    public void setVenhsnBinding(RichInputText venhsnBinding) {
        this.venhsnBinding = venhsnBinding;
    }

    public RichInputText getVenhsnBinding() {
        return venhsnBinding;
    }

    public void setStatusBinding(RichInputText statusBinding) {
        this.statusBinding = statusBinding;
    }

    public RichInputText getStatusBinding() {
        return statusBinding;
    }

    public void challanNoVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce.getNewValue() != null && svcBinding.getValue() != null) {
            String challanNo = (String) gstBillNoBinding.getValue();
            System.out.println("bean doc no" + challanNo);
            String vendor = (String) svcBinding.getValue();
            System.out.println("bean vendor" + vendor);
            OperationBinding ob = ADFUtils.findOperation("ChallanNumberValidator");
            ob.getParamsMap().put("Vendor", vendor);
            ob.getParamsMap().put("ChallanNo", challanNo);
            ob.execute();

            if (ob.getResult() != null) {
                String flag = ob.getResult().toString();

                if ("Y".equalsIgnoreCase(flag)) {
                    ADFUtils.showMessage("This Challan Number exists with this Vendor/Customer in Database", 1);
                }
            }
        }
    }

    public void setBillnoBinding(RichInputText billnoBinding) {
        this.billnoBinding = billnoBinding;
    }

    public RichInputText getBillnoBinding() {
        return billnoBinding;
    }

    public void billnoVCL(ValueChangeEvent valueChangeEvent) {
        String bn = (String) billnoBinding.getValue();
        BigDecimal iv = (BigDecimal) invoiceValueBinding.getValue();
        if (bn == null && iv != null) {
            System.out.println("Inside VCL to reset value!");
            invoiceValueBinding.setValue(null);
            System.out.println("Inside VCL, value reset done!!");
        } else if (bn != null && iv == null) {
            System.out.println("Inside VCL to print message!");
            FacesMessage Message = new FacesMessage("Please enter Invoice Value.");
            Message.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
            System.out.println("Inside VCL, Message Printed!!");
        }


    }

    public void setPotype2Binding(RichInputText potype2Binding) {
        this.potype2Binding = potype2Binding;
    }

    public RichInputText getPotype2Binding() {
        return potype2Binding;
    }

    public void setGateEdNepalbinding(RichInputText gateEdNepalbinding) {
        this.gateEdNepalbinding = gateEdNepalbinding;
    }

    public RichInputText getGateEdNepalbinding() {
        return gateEdNepalbinding;
    }

    public void ChallanDateVCL(ValueChangeEvent vce) {
        if(vce.getNewValue() != null){
            try {
                        System.out.println("set getEntryDateBinding" + ADFUtils.getTodayDate());
                        String date = ADFUtils.convertAdToBs(vce.getNewValue().toString());
                        System.out.println("setter getvaluedate" + date);
                        //            reqDateNepalBinding.setValue((String)date);
                        DCIteratorBinding dci = ADFUtils.findIterator("GtEheadsVO1Iterator");
                        dci.getCurrentRow().setAttribute("DocDtNepal", date);
                        //            Row row=(Row)ADFUtils.evaluateEL("#{bindings.ContractRequisitionHeaderVO1Iterator.currentRow}");
                        //            row.setAttribute("ReqDateNp", date);
                    } catch (ParseException e) {
                    } 
        }
    }

    public void BoeDateVCL(ValueChangeEvent vce) {
        if(vce.getNewValue() != null){
            try {
                        System.out.println("set getEntryDateBinding" + ADFUtils.getTodayDate());
                        String date = ADFUtils.convertAdToBs(vce.getNewValue().toString());
                        System.out.println("setter getvaluedate" + date);
                        //            reqDateNepalBinding.setValue((String)date);
                        DCIteratorBinding dci = ADFUtils.findIterator("GtEheadsVO1Iterator");
                        dci.getCurrentRow().setAttribute("BoeDtNepal", date);
                        //            Row row=(Row)ADFUtils.evaluateEL("#{bindings.ContractRequisitionHeaderVO1Iterator.currentRow}");
                        //            row.setAttribute("ReqDateNp", date);
                    } catch (ParseException e) {
                    } 
        }
    }

    public void setBoeDatebinding(RichInputDate boeDatebinding) {
        this.boeDatebinding = boeDatebinding;
    }

    public RichInputDate getBoeDatebinding() {
//        try {
//                    System.out.println("set getEntryDateBinding" + ADFUtils.getTodayDate());
//                    String date = ADFUtils.convertAdToBs(ADFUtils.getTodayDate().toString());
//                    System.out.println("setter getvaluedate" + date);
//                    //            reqDateNepalBinding.setValue((String)date);
//                    DCIteratorBinding dci = ADFUtils.findIterator("GtEheadsVO1Iterator");
//                    dci.getCurrentRow().setAttribute("BoeDtNepal", date);
//                    //            Row row=(Row)ADFUtils.evaluateEL("#{bindings.ContractRequisitionHeaderVO1Iterator.currentRow}");
//                    //            row.setAttribute("ReqDateNp", date);
//                } catch (ParseException e) {
//                } 
        return boeDatebinding;
    }

    public void populateInDetailAL(ActionEvent actionEvent) {
                System.out.println("fromPopulateIndetailAL");
                OperationBinding ob = ADFUtils.findOperation("populateForItemDetailinGateEntry");
                String GeTrans = (String) stockTransferGEbinding.getValue();
                ob.getParamsMap().put("GateEntryTrans", GeTrans);
                ob.execute();
    }

    public void setStockTransferGEbinding(RichInputComboboxListOfValues stockTransferGEbinding) {
        this.stockTransferGEbinding = stockTransferGEbinding;
    }

    public RichInputComboboxListOfValues getStockTransferGEbinding() {
        return stockTransferGEbinding;
    }
}
