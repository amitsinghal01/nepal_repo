package terms.mtl.transaction.ui.bean;

import com.tangosol.util.Gate;

import java.lang.reflect.Method;

import java.sql.Timestamp;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

public class GateExitBean {


    private RichSelectOneChoice bindstatus;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText unitNameBinding;
    private RichSelectOneChoice challanTypeBinding;
    private RichSelectOneChoice exitTypeBinding;
    private RichInputText challanNoBinding;
    private RichInputDate challanDate;
    private RichInputDate returnableTargetDateBinding;
    private RichInputText gateExitNoBinding;
    private RichInputDate gateExitDateBinding;
    private RichInputText partyCodeBinding;
    private RichInputText partyNameBinding;
    private RichInputText vehicleNoBinding;
    private RichInputComboboxListOfValues deliveryByBinding;
    private RichInputText transporterBinding;
    private RichInputText transporterNameBinding;
    private RichInputText preparedByBinding;
    private RichInputText preparedByNameBinding;
    private RichInputText forProcessRemarksBinding;
    private RichInputText documentNoBinding;
    private RichInputText itemCodeBinding;
    private RichInputText itemDescriptionBinding;
    private RichInputText quantityBinding;
    private RichInputText amountBinding;
    private RichInputText remarksBinding;
    private RichButton headerEditBinding;
    private RichOutputText outputTextBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichSelectOneChoice statusBinding;
    private RichButton saveBinding;
    private RichButton saveAndCloseBinding;

    public GateExitBean() {
    }

    public void saveGateExitAL(ActionEvent actionEvent) {

//        OperationBinding op=ADFUtils.findOperation("getStatusExit");
//        op.execute();
        System.out.println("STATUS VALUE"+bindstatus.getValue());
        if(bindstatus.getValue().equals("E"))
        {
        OperationBinding op1=ADFUtils.findOperation("generateGateExitNumber");
        Object rst = op1.execute();

        if(op1.getResult()!=null && !op1.getResult().equals("N"))
        {
            ADFUtils.findOperation("Commit").execute();
            ADFUtils.showMessage("Record Saved Successfully. New Gate Exit Number"+op1.getResult(), 2);  
        }
        else
        {
            ADFUtils.findOperation("Commit").execute();
            ADFUtils.showMessage("Record Update Successfully.", 2);  
  
        }}
        else{
                ADFUtils.findOperation("Commit").execute();
                ADFUtils.showMessage("Record Saved Successfully.", 2);  
            }
    }


    public void setBindstatus(RichSelectOneChoice bindstatus) {
        this.bindstatus = bindstatus;
    }

    public RichSelectOneChoice getBindstatus() {
        return bindstatus;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setUnitNameBinding(RichInputText unitNameBinding) {
        this.unitNameBinding = unitNameBinding;
    }

    public RichInputText getUnitNameBinding() {
        return unitNameBinding;
    }

    public void setChallanTypeBinding(RichSelectOneChoice challanTypeBinding) {
        this.challanTypeBinding = challanTypeBinding;
    }

    public RichSelectOneChoice getChallanTypeBinding() {
        return challanTypeBinding;
    }

    public void setExitTypeBinding(RichSelectOneChoice exitTypeBinding) {
        this.exitTypeBinding = exitTypeBinding;
    }

    public RichSelectOneChoice getExitTypeBinding() {
        return exitTypeBinding;
    }

    public void setChallanNoBinding(RichInputText challanNoBinding) {
        this.challanNoBinding = challanNoBinding;
    }

    public RichInputText getChallanNoBinding() {
        return challanNoBinding;
    }

    public void setChallanDate(RichInputDate challanDate) {
        this.challanDate = challanDate;
    }

    public RichInputDate getChallanDate() {
        return challanDate;
    }

    public void setReturnableTargetDateBinding(RichInputDate returnableTargetDateBinding) {
        this.returnableTargetDateBinding = returnableTargetDateBinding;
    }

    public RichInputDate getReturnableTargetDateBinding() {
        return returnableTargetDateBinding;
    }

    public void setGateExitNoBinding(RichInputText gateExitNoBinding) {
        this.gateExitNoBinding = gateExitNoBinding;
    }

    public RichInputText getGateExitNoBinding() {
        return gateExitNoBinding;
    }

    public void setGateExitDateBinding(RichInputDate gateExitDateBinding) {
        this.gateExitDateBinding = gateExitDateBinding;
    }

    public RichInputDate getGateExitDateBinding() {
        return gateExitDateBinding;
    }

    public void setPartyCodeBinding(RichInputText partyCodeBinding) {
        this.partyCodeBinding = partyCodeBinding;
    }

    public RichInputText getPartyCodeBinding() {
        return partyCodeBinding;
    }

    public void setPartyNameBinding(RichInputText partyNameBinding) {
        this.partyNameBinding = partyNameBinding;
    }

    public RichInputText getPartyNameBinding() {
        return partyNameBinding;
    }

    public void setVehicleNoBinding(RichInputText vehicleNoBinding) {
        this.vehicleNoBinding = vehicleNoBinding;
    }

    public RichInputText getVehicleNoBinding() {
        return vehicleNoBinding;
    }

    public void setDeliveryByBinding(RichInputComboboxListOfValues deliveryByBinding) {
        this.deliveryByBinding = deliveryByBinding;
    }

    public RichInputComboboxListOfValues getDeliveryByBinding() {
        return deliveryByBinding;
    }

    public void setTransporterBinding(RichInputText transporterBinding) {
        this.transporterBinding = transporterBinding;
    }

    public RichInputText getTransporterBinding() {
        return transporterBinding;
    }

    public void setTransporterNameBinding(RichInputText transporterNameBinding) {
        this.transporterNameBinding = transporterNameBinding;
    }

    public RichInputText getTransporterNameBinding() {
        return transporterNameBinding;
    }

    public void setPreparedByBinding(RichInputText preparedByBinding) {
        this.preparedByBinding = preparedByBinding;
    }

    public RichInputText getPreparedByBinding() {
        return preparedByBinding;
    }

    public void setPreparedByNameBinding(RichInputText preparedByNameBinding) {
        this.preparedByNameBinding = preparedByNameBinding;
    }

    public RichInputText getPreparedByNameBinding() {
        return preparedByNameBinding;
    }

    public void setForProcessRemarksBinding(RichInputText forProcessRemarksBinding) {
        this.forProcessRemarksBinding = forProcessRemarksBinding;
    }

    public RichInputText getForProcessRemarksBinding() {
        return forProcessRemarksBinding;
    }

    public void setDocumentNoBinding(RichInputText documentNoBinding) {
        this.documentNoBinding = documentNoBinding;
    }

    public RichInputText getDocumentNoBinding() {
        return documentNoBinding;
    }

    public void setItemCodeBinding(RichInputText itemCodeBinding) {
        this.itemCodeBinding = itemCodeBinding;
    }

    public RichInputText getItemCodeBinding() {
        return itemCodeBinding;
    }

    public void setItemDescriptionBinding(RichInputText itemDescriptionBinding) {
        this.itemDescriptionBinding = itemDescriptionBinding;
    }

    public RichInputText getItemDescriptionBinding() {
        return itemDescriptionBinding;
    }

    public void setQuantityBinding(RichInputText quantityBinding) {
        this.quantityBinding = quantityBinding;
    }

    public RichInputText getQuantityBinding() {
        return quantityBinding;
    }

    public void setAmountBinding(RichInputText amountBinding) {
        this.amountBinding = amountBinding;
    }

    public RichInputText getAmountBinding() {
        return amountBinding;
    }

    public void setRemarksBinding(RichInputText remarksBinding) {
        this.remarksBinding = remarksBinding;
    }

    public RichInputText getRemarksBinding() {
        return remarksBinding;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setOutputTextBinding(RichOutputText outputTextBinding) {
        this.outputTextBinding = outputTextBinding;
    }

    public RichOutputText getOutputTextBinding() {
        cevmodecheck();
        return outputTextBinding;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }
    
    private void cevmodecheck(){
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }
    public void cevModeDisableComponent(String mode) {
    //            FacesContext fctx = FacesContext.getCurrentInstance();
    //            ELContext elctx = fctx.getELContext();
    //            Application jsfApp = fctx.getApplication();
    //            //create a ValueExpression that points to the ADF binding layer
    //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
    //            //
    //            ValueExpression valueExpr = exprFactory.createValueExpression(
    //                                         elctx,
    //                                         "#{pageFlowScope.mode=='E'}",
    //                                          Object.class
    //                                         );
    //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
        if (mode.equals("E")) {
            getHeaderEditBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
            getUnitNameBinding().setDisabled(true);
            getChallanTypeBinding().setDisabled(true);
            getExitTypeBinding().setDisabled(true);
            getReturnableTargetDateBinding().setDisabled(true);
            getChallanNoBinding().setDisabled(true);
            getChallanDate().setDisabled(true);
            getGateExitNoBinding().setDisabled(true);
            getGateExitDateBinding().setDisabled(false);
            getBindstatus().setDisabled(false);
            getPartyCodeBinding().setDisabled(true);
            getPartyNameBinding().setDisabled(true);
            getDeliveryByBinding().setDisabled(true);
            getTransporterBinding().setDisabled(true);
            getTransporterNameBinding().setDisabled(true);
            getPreparedByBinding().setDisabled(true);
            getPreparedByNameBinding().setDisabled(true);
            getForProcessRemarksBinding().setDisabled(true);
            getDocumentNoBinding().setDisabled(true);
            getItemCodeBinding().setDisabled(true);
            getItemDescriptionBinding().setDisabled(true);
            getAmountBinding().setDisabled(true);
            getQuantityBinding().setDisabled(true);
            getRemarksBinding().setDisabled(true);
            getVehicleNoBinding().setDisabled(true);
            getSaveBinding().setDisabled(false);
            getSaveAndCloseBinding().setDisabled(false);

        } else if (mode.equals("C")) {
            getHeaderEditBinding().setDisabled(true);
           

        } else if (mode.equals("V")) {
            getSaveBinding().setDisabled(true);
            getSaveAndCloseBinding().setDisabled(true);
            
            

        }
    }
    
    
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
            try {
                Method method1 =
                    component.getClass().getMethod("setDisabled", boolean.class);
                if (method1 != null) {
                    method1.invoke(component, valueComponent);
                }
            } catch (NoSuchMethodException e) {
                try {
                    Method method =
                        component.getClass().getMethod("setReadOnly", boolean.class);
                    if (method != null) {
                        method.invoke(component, valueComponent);
                    }
                } catch (Exception e1) {
                    // e.printStackTrace();//silently eat this exception.
                }


            } catch (Exception e) {
                // e.printStackTrace();//silently eat this exception.
            }
            List<UIComponent> childComponents = component.getChildren();
            for (UIComponent comp : childComponents) {
                makeComponentHierarchyReadOnly(comp, valueComponent);
            }
        }

    public void editButtonAL(ActionEvent actionEvent) {
        System.out.println("STATUS VALUE"+bindstatus.getValue());
        if(bindstatus.getValue().equals("C") || bindstatus.getValue().equals("E"))
        {
            System.out.println("STATUS VALUE 1"+bindstatus.getValue());
            ADFUtils.showMessage("Exited/Cancelled record cannot be updated further.", 1);  
            ADFUtils.setEL("#{pageFlowScope.mode}", "V");
        }
        else
        {
        cevmodecheck();
        
        Timestamp t = new Timestamp(System.currentTimeMillis());
        gateExitDateBinding.setValue(t);
        System.out.println("Gate exit date====" + t);
        }
    }

    public void setStatusBinding(RichSelectOneChoice statusBinding) {
        this.statusBinding = statusBinding;
    }

    public RichSelectOneChoice getStatusBinding() {
        return statusBinding;
    }

    public void setSaveBinding(RichButton saveBinding) {
        this.saveBinding = saveBinding;
    }

    public RichButton getSaveBinding() {
        return saveBinding;
    }

    public void setSaveAndCloseBinding(RichButton saveAndCloseBinding) {
        this.saveAndCloseBinding = saveAndCloseBinding;
    }

    public RichButton getSaveAndCloseBinding() {
        return saveAndCloseBinding;
    }


}
