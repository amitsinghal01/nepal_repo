package terms.mtl.transaction.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;
import javax.faces.validator.ValidatorException;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

public class InterStoreBean {
    private RichTable createInterStoreTableBinding;
    private RichButton detailCreateBinding;
    private RichButton detailDeleteBinding;
    private RichButton headerEditBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText transferNoBinding;
    private RichInputText unitNameBinding;
    private RichInputComboboxListOfValues transferToBinding;
    private RichInputDate transferDateBinding;
    private RichInputComboboxListOfValues transferFromBinding;
    private RichInputText transferToNameBinding;
    private RichInputComboboxListOfValues transferByBinding;
    private RichInputText transferFromNameBinding;
    private RichInputText transferPurposeBinding;
    private RichOutputText bindingOutputText;
    private RichPanelHeader getMyPageRootBinding;
    private RichInputText transferByNameBinding;
    private RichInputComboboxListOfValues itemCodeBinding;
    private RichInputText processCodeBinding;
    private RichInputText processSequenceBinding;
    private RichInputText transferQuantityBinding;
    private RichInputText remarksBinding;
    private RichInputText stockBinding;
    private RichInputComboboxListOfValues procCdBinding;
    private RichSelectOneChoice transferTypeBinding;
    private RichColumn batchNoBinding;
    private RichColumn expryDateBinding;
    private RichInputText batchNBinding;
    private RichInputText eprDtBinding;

    public InterStoreBean() {
    }

    public void saveInterAl(ActionEvent actionEvent) {
        
        String empcd=(String)ADFUtils.resolveExpression("#{pageFlowScope.empCode}") ; 
        System.out.println("empcd"+empcd);
        ADFUtils.setLastUpdatedBy("InterStoreTransferHeaderVO1Iterator", "LastUpdatedBy");

        //        OperationBinding op1 = ADFUtils.findOperation("trnsfrQtyCompareWidStock");
        //        if (op1.getResult() != null && op1.getResult().equals("N")) {


            OperationBinding op = ADFUtils.findOperation("getGenTransferNo");
            Object rst = op.execute();

            Integer TransValue = (Integer) bindingOutputText.getValue();
            System.out.println("TRANS VALUE" + TransValue);
            //        String param=resolvEl("#{pageFlowScope.mode=='E'}");
            //        System.out.println("Save Mode is ====> "+param);
            if (TransValue == 0) {

        //                if (rst.toString() != null && rst.toString() != "") {
        //                    if (op.getErrors().isEmpty()) {
        //                        OperationBinding binding = ADFUtils.findOperation("insertIntoStkLedgerInterStore");
        //                        Object ob = binding.execute();
        //                        if (binding.getResult() != null && binding.getResult().equals("T")) {
                            ADFUtils.findOperation("Commit").execute();
                            FacesMessage Message =
                                new FacesMessage("Record Saved Successfully.New Transfer no is:: is " + rst + ".");
                            Message.setSeverity(FacesMessage.SEVERITY_INFO);
                            FacesContext fc = FacesContext.getCurrentInstance();
                            fc.addMessage(null, Message);

                       // } 
                       // else {
        //
        //                            System.out.println("error in funvv");
        //                            ADFUtils.findOperation("Rollback").execute();
        //                            ADFUtils.findOperation("CreateInsert2").execute();
        //                            FacesMessage Message =
        //                                new FacesMessage("Cannot generate the new records Because of internal issues.");
        //                            Message.setSeverity(FacesMessage.SEVERITY_ERROR);
        //                            FacesContext fc = FacesContext.getCurrentInstance();
        //                            fc.addMessage(null, Message);
        //                            AdfFacesContext.getCurrentInstance().addPartialTarget(createInterStoreTableBinding);
        //                        }
                  //  }
        //                } else {
        //
        //
        //                    ADFUtils.findOperation("Rollback").execute();
        //                    ADFUtils.findOperation("CreateInsert2").execute();
        //                    System.out.println("cannot generate the new records coz of internal issues.");
        //                    ADFUtils.showMessage("Cannot generate the new records Because of internal issues", 0);
        //                }
            } else {

        //                if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ") {
                   // if (op.getErrors().isEmpty()) {
                        //                      ADFUtils.showMessage("Transfer Quantity is Equal To Close Stock" , 0);
                        //                      ADFUtils.showMessage("Transfer Quantity is less than or equal to Closing Stock" ,0);
                        //                      ADFUtils.showMessage("Transfer Quantity is more than 0" ,0);
                        ADFUtils.findOperation("Commit").execute();
                        FacesMessage Message = new FacesMessage("Record Update Successfully.");
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);
                        FacesContext fc = FacesContext.getCurrentInstance();
                        fc.addMessage(null, Message);
                        //ADFUtils.showMessage("Record Update Successfully.", 0);

                    }
                //}
            
        // }

        //        else {
        //
        //
        //            FacesMessage message = new FacesMessage("Transfer Qty must be less than or equals to Current Stock.");
        //            message.setSeverity(FacesMessage.SEVERITY_ERROR);
        //            FacesContext context = FacesContext.getCurrentInstance();
        //            context.addMessage(transferQuantityBinding.getClientId(), message);
        //        }

        //        else if(op1.getResult().equals("B"))
        //        {
        //            ADFUtils.showMessage("Transfer Quantity is less than or equal to Closing Stock." ,0);
        //
        //        }
        //        else if(op1.getResult().equals("C")) {
        //            ADFUtils.showMessage("Transfer Quantity is greater than zero." ,0);
        //        }
        //        else {
        //            ADFUtils.showMessage("Transfer From Location Code not matched the Current Closing Stock Location Code. Please Check." ,0);
        //        }
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete").execute();
            System.out.println("Record Delete Successfully.");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully.");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(createInterStoreTableBinding);
    }


    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }


    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootBinding(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootBinding(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootBinding(), false);
            cevModeDisableComponent("E");
        }
    }

    public void cevModeDisableComponent(String mode) {
        if (mode.equals("E")) {
            getStockBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
            getUnitNameBinding().setDisabled(true);
            getTransferToBinding().setDisabled(true);
            getTransferDateBinding().setDisabled(true);
            getTransferFromBinding().setDisabled(true);
            getTransferToNameBinding().setDisabled(true);
            getTransferFromNameBinding().setDisabled(true);
            getTransferNoBinding().setDisabled(true);
            getTransferPurposeBinding().setDisabled(true);
            getTransferByNameBinding().setDisabled(true);
            getTransferByBinding().setDisabled(true);
            getItemCodeBinding().setDisabled(true);
            getProcCdBinding().setDisabled(true);
            getProcessSequenceBinding().setDisabled(true);
            getTransferQuantityBinding().setDisabled(true);
            getRemarksBinding().setDisabled(true);
            getDetailCreateBinding().setDisabled(false);
            getDetailDeleteBinding().setDisabled(true);
           getBatchNBinding().setDisabled(true);
           getEprDtBinding().setDisabled(true);
        } else if (mode.equals("C")) {
            getStockBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
            getUnitNameBinding().setDisabled(true);
            getTransferNoBinding().setDisabled(true);
            getTransferDateBinding().setDisabled(true);
            getTransferByBinding().setDisabled(true);
            getTransferByNameBinding().setDisabled(true);
            getProcessSequenceBinding().setDisabled(true);
            getDetailCreateBinding().setDisabled(false);
            getDetailDeleteBinding().setDisabled(false);
            getBatchNBinding().setDisabled(true);
            getEprDtBinding().setDisabled(true);

        } else if (mode.equals("V")) {
            getDetailCreateBinding().setDisabled(true);
            getDetailDeleteBinding().setDisabled(true);
            getBatchNBinding().setDisabled(true);
            getEprDtBinding().setDisabled(true);
        }
    }

    public void setCreateInterStoreTableBinding(RichTable createInterStoreTableBinding) {
        this.createInterStoreTableBinding = createInterStoreTableBinding;
    }

    public RichTable getCreateInterStoreTableBinding() {
        return createInterStoreTableBinding;
    }

    public void setDetailCreateBinding(RichButton detailCreateBinding) {
        this.detailCreateBinding = detailCreateBinding;
    }

    public RichButton getDetailCreateBinding() {
        return detailCreateBinding;
    }

    public void setDetailDeleteBinding(RichButton detailDeleteBinding) {
        this.detailDeleteBinding = detailDeleteBinding;
    }

    public RichButton getDetailDeleteBinding() {
        return detailDeleteBinding;
    }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setTransferNoBinding(RichInputText transferNoBinding) {
        this.transferNoBinding = transferNoBinding;
    }

    public RichInputText getTransferNoBinding() {
        return transferNoBinding;
    }

    public void setUnitNameBinding(RichInputText unitNameBinding) {
        this.unitNameBinding = unitNameBinding;
    }

    public RichInputText getUnitNameBinding() {
        return unitNameBinding;
    }

    public void setTransferToBinding(RichInputComboboxListOfValues transferToBinding) {
        this.transferToBinding = transferToBinding;
    }

    public RichInputComboboxListOfValues getTransferToBinding() {
        return transferToBinding;
    }

    public void setTransferDateBinding(RichInputDate transferDateBinding) {
        this.transferDateBinding = transferDateBinding;
    }

    public RichInputDate getTransferDateBinding() {
        return transferDateBinding;
    }

    public void setTransferFromBinding(RichInputComboboxListOfValues transferFromBinding) {
        this.transferFromBinding = transferFromBinding;
    }

    public RichInputComboboxListOfValues getTransferFromBinding() {
        return transferFromBinding;
    }

    public void setTransferToNameBinding(RichInputText transferToNameBinding) {
        this.transferToNameBinding = transferToNameBinding;
    }

    public RichInputText getTransferToNameBinding() {
        return transferToNameBinding;
    }

    public void setTransferByBinding(RichInputComboboxListOfValues transferByBinding) {
        this.transferByBinding = transferByBinding;
    }

    public RichInputComboboxListOfValues getTransferByBinding() {
        return transferByBinding;
    }

    public void setTransferFromNameBinding(RichInputText transferFromNameBinding) {
        this.transferFromNameBinding = transferFromNameBinding;
    }

    public RichInputText getTransferFromNameBinding() {
        return transferFromNameBinding;
    }

    public void setTransferPurposeBinding(RichInputText transferPurposeBinding) {
        this.transferPurposeBinding = transferPurposeBinding;
    }

    public RichInputText getTransferPurposeBinding() {
        return transferPurposeBinding;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        cevmodecheck();
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        return bindingOutputText;
    }

    public void setGetMyPageRootBinding(RichPanelHeader getMyPageRootBinding) {
        this.getMyPageRootBinding = getMyPageRootBinding;
    }

    public RichPanelHeader getGetMyPageRootBinding() {
        return getMyPageRootBinding;
    }

    public void setTransferByNameBinding(RichInputText transferByNameBinding) {
        this.transferByNameBinding = transferByNameBinding;
    }

    public RichInputText getTransferByNameBinding() {
        return transferByNameBinding;
    }

    public void setItemCodeBinding(RichInputComboboxListOfValues itemCodeBinding) {
        this.itemCodeBinding = itemCodeBinding;
    }

    public RichInputComboboxListOfValues getItemCodeBinding() {
        return itemCodeBinding;
    }

    public void setProcessCodeBinding(RichInputText processCodeBinding) {
        this.processCodeBinding = processCodeBinding;
    }

    public RichInputText getProcessCodeBinding() {
        return processCodeBinding;
    }

    public void setProcessSequenceBinding(RichInputText processSequenceBinding) {
        this.processSequenceBinding = processSequenceBinding;
    }

    public RichInputText getProcessSequenceBinding() {
        return processSequenceBinding;
    }

    public void setTransferQuantityBinding(RichInputText transferQuantityBinding) {
        this.transferQuantityBinding = transferQuantityBinding;
    }

    public RichInputText getTransferQuantityBinding() {
        return transferQuantityBinding;
    }

    public void setRemarksBinding(RichInputText remarksBinding) {
        this.remarksBinding = remarksBinding;
    }

    public RichInputText getRemarksBinding() {
        return remarksBinding;
    }


    public void transferQuantityPopulateBean(ValueChangeEvent valueChangeEvent) {
        OperationBinding op = ADFUtils.findOperation("checkTransferQuantityInterStore");
        op.execute();
    }

    public void saveCurrentStockAC() {
        OperationBinding op = ADFUtils.findOperation("saveCurrentStockAction");
        op.execute();

    }

    public void saveStockVCL(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (valueChangeEvent != null) {
            System.out.println("inside bean condd====>>");
            OperationBinding op = ADFUtils.findOperation("trnsfrQtyCompareWidStock");
            op.execute();
            if (op.getResult() != null && op.getResult().equals("Y")) {
                FacesMessage message = new FacesMessage("Transfer Qty must be less than or equals to Current Stock.");
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(transferQuantityBinding.getClientId(), message);
            }

        }

        //        if(op.getResult().equals("A"))
        //        {
        //
        ////            ADFUtils.showMessage("Transfer Quantity is Equal To Closing Stock" , 0);
        //        }
        //        else if(op.getResult().equals("B"))
        //        {
        //            ADFUtils.showMessage("Transfer Quantity is less than or equal to Closing Stock" ,0);
        //        }
        //        else if(op.getResult().equals("C"))
        //        {
        //            ADFUtils.showMessage("Transfer Quantity is greater than zero" ,0);
        //        }
    }

    public String saveAndCloseAC() {

//        OperationBinding op1 = ADFUtils.findOperation("trnsfrQtyCompareWidStock");
//        if (op1.getResult() != null && op1.getResult().equals("N")) 
//        {

            OperationBinding op = ADFUtils.findOperation("getGenTransferNo");
            Object rst = op.execute();
            Integer TransValue = (Integer) bindingOutputText.getValue();
            System.out.println("TRANS VALUE" + TransValue);

            if (TransValue == 0) {
                if (rst.toString() != null && rst.toString() != "") {
                    if (op.getErrors().isEmpty()) {
                        OperationBinding binding = ADFUtils.findOperation("insertIntoStkLedgerInterStore");
                        Object ob = binding.execute();
                        if (binding.getResult() != null && binding.getResult().equals("T")) {
                            ADFUtils.findOperation("Commit").execute();
                            FacesMessage Message =
                                new FacesMessage("Record Saved Successfully.New Transfer no is:: is " + rst + ".");
                            Message.setSeverity(FacesMessage.SEVERITY_INFO);
                            FacesContext fc = FacesContext.getCurrentInstance();
                            fc.addMessage(null, Message);
                            return "save and close";

                        } else {

                            System.out.println("error in funvv");
                            ADFUtils.findOperation("Rollback").execute();
                            ADFUtils.findOperation("CreateInsert2").execute();
                            FacesMessage Message =
                                new FacesMessage("Cannot generate the new records Because of internal issues." );                 
                            Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                            FacesContext fc = FacesContext.getCurrentInstance();
                            fc.addMessage(null, Message);
                            AdfFacesContext.getCurrentInstance().addPartialTarget(createInterStoreTableBinding);
                        }
                    }
                }


                else {


                    ADFUtils.findOperation("Rollback").execute();
                    ADFUtils.findOperation("CreateInsert2").execute();
                    System.out.println("cannot generate the new records coz of internal issues.");
                    ADFUtils.showMessage("Cannot generate the new records Because of internal issues", 0);
                }
            } else {

                if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ") {
                    if (op.getErrors().isEmpty()) {
                        ADFUtils.findOperation("Commit").execute();
                        FacesMessage Message = new FacesMessage("Record Update Successfully.");
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);
                        FacesContext fc = FacesContext.getCurrentInstance();
                        fc.addMessage(null, Message);
                        //ADFUtils.showMessage("Record Update Successfully.", 0);

                    }
                }
            }
//        } else {
//
//            FacesMessage message = new FacesMessage("Transfer Qty must be less than or equals to Current Stock.");
//            message.setSeverity(FacesMessage.SEVERITY_ERROR);
//            FacesContext context = FacesContext.getCurrentInstance();
//            context.addMessage(transferQuantityBinding.getClientId(), message);
//            return null;
//
//        }


        return null;
    }

    public void setStockBinding(RichInputText stockBinding) {
        this.stockBinding = stockBinding;
    }

    public RichInputText getStockBinding() {
        return stockBinding;
    }

    public void trnQtyValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
     if(object!=null)
     {
             BigDecimal trq=(BigDecimal)object;
             System.out.println("transfer qty  is*****"+trq);
             BigDecimal stk=(BigDecimal)stockBinding.getValue();
             System.out.println("Stock is****** is*****"+stk);
             
             if(stk!=null && trq.compareTo(stk)==1)
             {
               System.out.println("in the if block****");
              throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,"Transfer Qty must be less than or equals to Current Stock..", null));
             
         
         
         }
     }
    }

    public void setProcCdBinding(RichInputComboboxListOfValues procCdBinding) {
        this.procCdBinding = procCdBinding;
    }

    public RichInputComboboxListOfValues getProcCdBinding() {
        return procCdBinding;
    }

    public void setTransferTypeBinding(RichSelectOneChoice transferTypeBinding) {
        this.transferTypeBinding = transferTypeBinding;
    }

    public RichSelectOneChoice getTransferTypeBinding() {
        return transferTypeBinding;
    }

    public void transferTypeVcl(ValueChangeEvent valueChangeEvent) {
        if (valueChangeEvent != null) {
            transferFromBinding.resetValue();
            System.out.println("Transfer Type--->" + valueChangeEvent.getNewValue());
            String flag = (String) valueChangeEvent.getNewValue();
            DCIteratorBinding dci=ADFUtils.findIterator("InterStoreTransferHeaderVO1Iterator");
            if (flag.equalsIgnoreCase("FG")) {
                
                dci.getCurrentRow().setAttribute("TrnfFrom", "FG");
                // transferFromBinding.setValue("FG");
                getTransferFromBinding().setDisabled(true);
               
            } else
                //                String value = "WO";
                dci.getCurrentRow().setAttribute("TrnfFrom", null);
                getTransferTypeBinding().setDisabled(false);

        }
    }


    public void setBatchNBinding(RichInputText batchNBinding) {
        this.batchNBinding = batchNBinding;
    }

    public RichInputText getBatchNBinding() {
        return batchNBinding;
    }

    public void setEprDtBinding(RichInputText eprDtBinding) {
        this.eprDtBinding = eprDtBinding;
    }

    public RichInputText getEprDtBinding() {
        return eprDtBinding;
    }
}
