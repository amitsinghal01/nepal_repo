package terms.mtl.transaction.ui.bean;


import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.text.ParseException;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.validator.ValidatorException;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.jbo.Row;

public class IssueSlipBean {
    private RichInputComboboxListOfValues getDocNumberBinding;
    private RichSelectOneChoice getIssueTypeBinding;
    private RichSelectOneChoice getIssueForBinding;
    private RichInputComboboxListOfValues getItemCodeBinding;
    private RichTable createIssueSlipTableBinding;
    private RichInputDate slipDateBinding;
    private RichSelectOneChoice retTypeBinding;
    private RichInputDate targetDateBinding;
    private RichInputDate aprrovedDateBinding;
    private RichInputText procCodeBinding;
    private RichInputText stockQtyBinding;
    private RichInputText rateBinding;
    private RichOutputText bindingOutputText;
    private RichButton headerEditBinding;
    private RichPanelHeader myPageRootComponent;
    private RichButton detailcreateBinding;
    //    private RichButton detaildeleteBinding;
    private RichInputComboboxListOfValues issuedBy;
    private RichInputComboboxListOfValues procSeqBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputComboboxListOfValues issueToBinding;
    private RichInputComboboxListOfValues authByBinding;
    private RichOutputText editBindingValue;
    private RichSelectOneChoice uomDetailBinding;
    private RichInputText issueToDescBinding;
    private RichInputText issueQtyBinding;
    private RichInputComboboxListOfValues issFromBinding;
    private RichSelectOneChoice slipTypeBinding;
    private RichShowDetailItem issueTab2Binding;
    private RichShowDetailItem issueTab1Binding;
    private RichInputText jobCodeBinding;
    private RichSelectOneChoice itemTypeBinding;
    private RichInputText slipdatebindingNP;

    public IssueSlipBean() {
        System.out.println("Issue slip Constructor");
    }

    public void SaveAL(ActionEvent actionEvent) {

        String empcd = (String) ADFUtils.resolveExpression("#{pageFlowScope.empCode}");
        empcd = empcd != null ? empcd : "E-001";
        System.out.println("empcd" + empcd);
        ADFUtils.setColumnValue("IssueSlipHeaderVO1Iterator", "LastUpdatedBy", empcd);

        OperationBinding opVal = ADFUtils.findOperation("validateJCQty");
        opVal.execute();

        if (opVal.getResult() != null && opVal.getResult().toString().equalsIgnoreCase("Y")) {

            if (authByBinding.getValue() != null) {
                OperationBinding op = ADFUtils.findOperation("getIssueSlipNumber");
                Object rst = op.execute();
                ADFUtils.findOperation("setProcessDataissueSlipDetail").execute();
                //ADFUtils.findOperation("goForUnitRate").execute();


                System.out.println("--------Commit-------");
                System.out.println("value aftr getting result--->?" + rst);

                if (rst.toString() != null && rst.toString() != "") {
                    if (op.getErrors().isEmpty()) {
                        OperationBinding op1 = ADFUtils.findOperation("Commit");
                        Object rs = op1.execute();

                        FacesMessage Message =
                            new FacesMessage("Record Saved Successfully. New Issue Slip Number is " + rst + ".");
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);
                        FacesContext fc = FacesContext.getCurrentInstance();
                        fc.addMessage(null, Message);
                    }
                }


                if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ") {
                    if (op.getErrors().isEmpty()) {
                        ADFUtils.findOperation("Commit").execute();
                        FacesMessage Message = new FacesMessage("Record Update Successfully.");
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);
                        FacesContext fc = FacesContext.getCurrentInstance();
                        fc.addMessage(null, Message);

                    }


                }
            } else {
                FacesMessage Message = new FacesMessage("Please approve issue slip first then save.");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);

            }
        }
    }

    public void issueQtyVCL(ValueChangeEvent vcl) {

        vcl.getComponent().processUpdates(FacesContext.getCurrentInstance());
        System.out.println("------------------------------------------VCL");
        if (vcl.getNewValue() != null) {
            if (getIssueTypeBinding.getValue() != null && getItemCodeBinding.getValue() != null) {

                FacesContext context = FacesContext.getCurrentInstance();
                RichInputText input = (RichInputText) vcl.getComponent();
                Integer counterSecondProc = 0;
                // vcl.getComponent().processUpdates(FacesContext.getCurrentInstance());
                // ADFUtils.findOperation("goForStockQty").execute();

                vcl.getComponent().processUpdates(FacesContext.getCurrentInstance());
                ADFUtils.findOperation("setQtyFromStrQty").execute();

                //        qtyBinding.setValue(strQtyBinding.getValue());
                String IssType = getIssueTypeBinding.getValue().toString();
                String itemCode = getItemCodeBinding.getValue().toString();
                System.out.println("isstype:" + IssType + " itemcode:" + itemCode);
                //                if (!IssType.equalsIgnoreCase("O")) {
                //                    if (itemCode != null) {
                //
                //                        OperationBinding op2 = ADFUtils.findOperation("goForIssueQty");
                //                        Object rs = op2.execute();
                //                        System.out.println("msg in bean:" + op2.getResult());
                //                        //                    System.out.println("compare value of op2result with T:" +
                //                        //                                       op2.getResult().toString().compareTo("T"));
                //
                //                        System.out.println("compare value of op2result with T:" + op2.getResult().equals("T"));
                //
                //                        if (op2.getResult().toString().compareTo("T") != 0) {
                //                            // if (!op2.getResult().equals("T")) {
                //                            //input.setValid(false);
                //                            //context.validationFailed();
                //                            context.addMessage(input.getClientId(context),
                //                                               new FacesMessage(FacesMessage.SEVERITY_ERROR, op2.getResult().toString(),
                //                                                                null));
                ////                            issueQtyBinding.setValue(null);
                ////                            AdfFacesContext.getCurrentInstance().addPartialTarget(issueQtyBinding);
                //
                //                            counterSecondProc = 1;
                //
                //                        }
                //                    }
                //
                //                }

                if (counterSecondProc != 1) {
                    if (!IssType.equalsIgnoreCase("O")) {
                        if (itemCode != null) {
                            OperationBinding op3 = ADFUtils.findOperation("goForIssueQtyStrQty");
                            Object rs1 = op3.execute();
                            System.out.println("msg in bean:" + op3.getResult());
                            System.out.println("compare value of op3result with T:" +
                                               op3.getResult().toString().compareTo("T"));
                            if (op3.getResult().toString().compareTo("T") != 0) {
                                input.setValid(false);
                                context.validationFailed();
                                context.addMessage(input.getClientId(context),
                                                   new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                    op3.getResult().toString(), null));
                                //                                issueQtyBinding.setValue(null);
                                //                                AdfFacesContext.getCurrentInstance().addPartialTarget(issueQtyBinding);
                            }
                        }

                    }
                }
            }
        }
    }

    public void ItemCodeVCL(ValueChangeEvent valueChangeEvent) {


        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        ADFUtils.findOperation("goForStockQty").execute();

        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        ADFUtils.findOperation("goForIssueRate").execute();
        AdfFacesContext.getCurrentInstance().addPartialTarget(createIssueSlipTableBinding);
        //        ADFUtils.findOperation("getDetailsFromReqSlip").execute();


    }

    public void setGetDocNumberBinding(RichInputComboboxListOfValues getDocNumberBinding) {
        this.getDocNumberBinding = getDocNumberBinding;
    }

    public RichInputComboboxListOfValues getGetDocNumberBinding() {
        return getDocNumberBinding;
    }

    public void setGetIssueTypeBinding(RichSelectOneChoice getIssueTypeBinding) {
        this.getIssueTypeBinding = getIssueTypeBinding;
    }

    public RichSelectOneChoice getGetIssueTypeBinding() {
        System.out.println("getter issue type");
        return getIssueTypeBinding;
    }

    public void setGetIssueForBinding(RichSelectOneChoice getIssueForBinding) {
        this.getIssueForBinding = getIssueForBinding;
    }

    public RichSelectOneChoice getGetIssueForBinding() {
        return getIssueForBinding;
    }

    public void issueToVCL(ValueChangeEvent valueChangeEvent) {
        String IssType = getIssueTypeBinding.getValue().toString();
        String IssFor = getIssueForBinding.getValue().toString();
        System.out.println("Iss Type Value " + IssType + " Iss For Value" + IssFor);

        if (issueToBinding.getValue() != null) {
            getGetIssueTypeBinding().setDisabled(true);
            getGetIssueForBinding().setDisabled(true);
        } else {
            getGetIssueTypeBinding().setDisabled(false);
            getGetIssueForBinding().setDisabled(false);
        }

        if (IssType.equalsIgnoreCase("M") || IssType.equalsIgnoreCase("J") || IssType.equalsIgnoreCase("A") ||
            IssType.equalsIgnoreCase("S") || IssType.equalsIgnoreCase("R") || IssType.equalsIgnoreCase("F") ||
            IssType.equalsIgnoreCase("P") || IssType.equalsIgnoreCase("T"))

        {
            getDocNumberBinding.setDisabled(false);

        }

        else if (IssType.equalsIgnoreCase("W") && IssFor.equalsIgnoreCase("V")) {
            getDocNumberBinding.setDisabled(false);
        }

        else if (IssType.equalsIgnoreCase("C") && IssFor.equalsIgnoreCase("V")) {
            getDocNumberBinding.setDisabled(false);
        }

        else if (IssType.equalsIgnoreCase("G") && IssFor.equalsIgnoreCase("C")) {
            getDocNumberBinding.setDisabled(false);
        } else {
            System.out.println("Disabled");
            getDocNumberBinding.setDisabled(true);
        }
    }


    public void uomVCL(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        ADFUtils.findOperation("goForGetConversion").execute();
    }

    public void setGetItemCodeBinding(RichInputComboboxListOfValues getItemCodeBinding) {
        this.getItemCodeBinding = getItemCodeBinding;
    }

    public RichInputComboboxListOfValues getGetItemCodeBinding() {
        return getItemCodeBinding;
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(createIssueSlipTableBinding);
    }

    public void setCreateIssueSlipTableBinding(RichTable createIssueSlipTableBinding) {
        this.createIssueSlipTableBinding = createIssueSlipTableBinding;
    }

    public RichTable getCreateIssueSlipTableBinding() {
        return createIssueSlipTableBinding;
    }

    public void setSlipDateBinding(RichInputDate slipDateBinding) {
        this.slipDateBinding = slipDateBinding;
    }

    public RichInputDate getSlipDateBinding() {
        try {
            System.out.println("set getEntryDateBinding" + ADFUtils.getTodayDate());
            String date = ADFUtils.convertAdToBs(ADFUtils.getTodayDate().toString());
            System.out.println("setter getvaluedate" + date);
            getSlipdatebindingNP().setValue((String) date);
            ////            DCIteratorBinding dci = ADFUtils.findIterator("ContractRequisitionHeaderVO1Iterator");
            ////            dci.getCurrentRow().setAttribute("ReqDateNp", date);
            //            //            Row row=(Row)ADFUtils.evaluateEL("#{bindings.ContractRequisitionHeaderVO1Iterator.currentRow}");
            //            //            row.setAttribute("ReqDateNp", date);
        } catch (ParseException e) {
        }
        return slipDateBinding;
    }

    public void setRetTypeBinding(RichSelectOneChoice retTypeBinding) {
        this.retTypeBinding = retTypeBinding;
    }

    public RichSelectOneChoice getRetTypeBinding() {
        return retTypeBinding;
    }

    public void setTargetDateBinding(RichInputDate targetDateBinding) {
        this.targetDateBinding = targetDateBinding;
    }

    public RichInputDate getTargetDateBinding() {
        return targetDateBinding;
    }

    public void setAprrovedDateBinding(RichInputDate aprrovedDateBinding) {
        this.aprrovedDateBinding = aprrovedDateBinding;
    }

    public RichInputDate getAprrovedDateBinding() {
        return aprrovedDateBinding;
    }

    public void setProcCodeBinding(RichInputText procCodeBinding) {
        this.procCodeBinding = procCodeBinding;
    }

    public RichInputText getProcCodeBinding() {
        return procCodeBinding;
    }

    public void setStockQtyBinding(RichInputText stockQtyBinding) {
        this.stockQtyBinding = stockQtyBinding;
    }

    public RichInputText getStockQtyBinding() {
        return stockQtyBinding;
    }

    public void setRateBinding(RichInputText rateBinding) {
        this.rateBinding = rateBinding;
    }

    public RichInputText getRateBinding() {
        return rateBinding;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setMyPageRootComponent(RichPanelHeader myPageRootComponent) {
        this.myPageRootComponent = myPageRootComponent;
    }

    public RichPanelHeader getMyPageRootComponent() {
        return myPageRootComponent;
    }

    public void setDetailcreateBinding(RichButton detailcreateBinding) {
        this.detailcreateBinding = detailcreateBinding;
    }

    public RichButton getDetailcreateBinding() {
        return detailcreateBinding;
    }

    //    public void setDetaildeleteBinding(RichButton detaildeleteBinding) {
    //        this.detaildeleteBinding = detaildeleteBinding;
    //    }
    //
    //    public RichButton getDetaildeleteBinding() {
    //        return detaildeleteBinding;
    //    }


    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getMyPageRootComponent(), false);
            cevModeDisableComponent("E");

        }
    }


    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }


    public void cevModeDisableComponent(String mode) {
        if (mode.equals("E")) {
            getJobCodeBinding().setDisabled(true);
            getUomDetailBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            getDetailcreateBinding().setDisabled(false);
            //getDetaildeleteBinding().setDisabled(false);
            getSlipdatebindingNP().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
            getSlipDateBinding().setDisabled(true);
            //getProcSeqBinding().setDisabled(true);
            getStockQtyBinding().setDisabled(true);
            getGetIssueTypeBinding().setDisabled(true);
            getRetTypeBinding().setDisabled(true);
            getSlipDateBinding().setDisabled(true);
            getAprrovedDateBinding().setDisabled(true);
            getIssueToDescBinding().setDisabled(true);
            getProcCodeBinding().setDisabled(true);
            getIssuedBy().setDisabled(true);
            getSlipTypeBinding().setDisabled(false);
        } else if (mode.equals("C")) {
            getJobCodeBinding().setDisabled(true);
            getUomDetailBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            getDetailcreateBinding().setDisabled(false);
            getSlipdatebindingNP().setDisabled(true);
            //getDetaildeleteBinding().setDisabled(false);
            getSlipDateBinding().setDisabled(true);
            getIssuedBy().setDisabled(true);
            //getProcSeqBinding().setDisabled(true);
            getStockQtyBinding().setDisabled(true);
            getTargetDateBinding().setDisabled(true);
            // getGetIssueForBinding().setDisabled(true);
            // getIssueToBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
            // getAuthByBinding().setDisabled(true);
            getProcCodeBinding().setDisabled(true);
            getIssueToDescBinding().setDisabled(true);
            getAprrovedDateBinding().setDisabled(true);
            getRateBinding().setDisabled(true);
        } else if (mode.equals("V")) {
            getDetailcreateBinding().setDisabled(true);
            //getDetaildeleteBinding().setDisabled(true);
            getIssueToDescBinding().setDisabled(true);
            getIssueTab1Binding().setDisabled(false);
            getIssueTab2Binding().setDisabled(false);

        }

    }

    public void editButtonAL(ActionEvent actionEvent) {
        System.out.println("authByBinding.getValue()");
        if (authByBinding.getValue() == null) {
            cevmodecheck();
        } else if (authByBinding.getValue() != null && !getIssueTypeBinding.getValue().equals("M")) {
            getSlipTypeBinding().setDisabled(false);
        } else {
            ADFUtils.showMessage("Except Item Type You Can Not Modify Issue Slip After Approval", 2);
            getItemTypeBinding().setDisabled(false);
        }
    }

    public void setIssuedBy(RichInputComboboxListOfValues issuedBy) {
        this.issuedBy = issuedBy;
    }

    public RichInputComboboxListOfValues getIssuedBy() {
        return issuedBy;
    }

    public void setProcSeqBinding(RichInputComboboxListOfValues procSeqBinding) {
        this.procSeqBinding = procSeqBinding;
    }

    public RichInputComboboxListOfValues getProcSeqBinding() {
        return procSeqBinding;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void retTypeVCL(ValueChangeEvent valueChangeEvent) {
        if (valueChangeEvent.getNewValue() != null) {
            if (valueChangeEvent.getNewValue().toString().equalsIgnoreCase("R")) {
                getTargetDateBinding().setDisabled(false);

            } else {
                getTargetDateBinding().setDisabled(true);
            }
        }
    }


    public void setIssueToBinding(RichInputComboboxListOfValues issueToBinding) {
        this.issueToBinding = issueToBinding;
    }

    public RichInputComboboxListOfValues getIssueToBinding() {
        return issueToBinding;
    }

    public void setAuthByBinding(RichInputComboboxListOfValues authByBinding) {
        this.authByBinding = authByBinding;
    }

    public RichInputComboboxListOfValues getAuthByBinding() {
        return authByBinding;
    }

    public void issueTypeVCL(ValueChangeEvent valueChangeEvent) {

        if (getIssueTypeBinding.getValue() != null) {
            getGetIssueForBinding().setDisabled(false);
        } else {
            // getGetIssueForBinding().setDisabled(true);
        }
        if (getIssueTypeBinding.getValue().equals("W") || getIssueTypeBinding.getValue().equals("C")) {
            getIssueForBinding.setValue("V");

        } else {
            getIssueForBinding.setValue("L");
        }

    }

    public void issueForVCL(ValueChangeEvent valueChangeEvent) {

        if (getIssueForBinding.getValue() != null) {
            getIssueToBinding().setDisabled(false);
        } else {
            getIssueToBinding().setDisabled(true);
        }
    }

    public void setEditBindingValue(RichOutputText editBindingValue) {
        this.editBindingValue = editBindingValue;
    }

    public RichOutputText getEditBindingValue() {
        return editBindingValue;
    }

    public void setUomDetailBinding(RichSelectOneChoice uomDetailBinding) {
        this.uomDetailBinding = uomDetailBinding;
    }

    public RichSelectOneChoice getUomDetailBinding() {
        return uomDetailBinding;
    }

    public void setIssueToDescBinding(RichInputText issueToDescBinding) {
        this.issueToDescBinding = issueToDescBinding;
    }

    public RichInputText getIssueToDescBinding() {
        return issueToDescBinding;
    }

    public void setIssueQtyBinding(RichInputText issueQtyBinding) {
        this.issueQtyBinding = issueQtyBinding;
    }

    public RichInputText getIssueQtyBinding() {
        return issueQtyBinding;
    }

    public void setIssFromBinding(RichInputComboboxListOfValues issFromBinding) {
        this.issFromBinding = issFromBinding;
    }

    public RichInputComboboxListOfValues getIssFromBinding() {
        return issFromBinding;
    }

    public void setSlipTypeBinding(RichSelectOneChoice slipTypeBinding) {
        this.slipTypeBinding = slipTypeBinding;
    }

    public RichSelectOneChoice getSlipTypeBinding() {
        return slipTypeBinding;
    }

    public void detailCreateAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert").execute();
        getIssueTypeBinding.setDisabled(true);
        getIssFromBinding().setDisabled(true);
        getRetTypeBinding().setDisabled(true);
        getIssueForBinding.setDisabled(true);
        // getIssuedBy().setDisabled(true);
        // getSlipTypeBinding().setDisabled(true);
        getIssueToBinding().setDisabled(true);

    }


    public void clearSelectedRowDocNoVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce != null) {

            if (!vce.getNewValue().equals(vce.getOldValue())) {


                ADFUtils.findOperation("clearSelectedRow").execute();
                AdfFacesContext.getCurrentInstance().addPartialTarget(createIssueSlipTableBinding);


            }
        }
    }

    public String saveandCloseAL() {

        String empcd = (String) ADFUtils.resolveExpression("#{pageFlowScope.empCode}");
        System.out.println("empcd" + empcd);
        ADFUtils.setColumnValue("IssueSlipHeaderVO1Iterator", "LastUpdatedBy", empcd);

        OperationBinding opVal = ADFUtils.findOperation("validateJCQty");
        opVal.execute();

        if (opVal.getResult() != null && opVal.getResult().toString().equalsIgnoreCase("Y")) {

            if (authByBinding.getValue() != null) {
                OperationBinding op = ADFUtils.findOperation("getIssueSlipNumber");
                Object rst = op.execute();
                ADFUtils.findOperation("setProcessDataissueSlipDetail").execute();
                //ADFUtils.findOperation("goForUnitRate").execute();
    
    
                System.out.println("--------Commit-------");
                System.out.println("value aftr getting result--->?" + rst);
    
                if (rst.toString() != null && rst.toString() != "") {
                    if (op.getErrors().isEmpty()) {
                        OperationBinding op1 = ADFUtils.findOperation("Commit");
                        Object rs = op1.execute();
    
                        FacesMessage Message =
                            new FacesMessage("Record Saved Successfully. New Issue Slip Number is " + rst + ".");
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);
                        FacesContext fc = FacesContext.getCurrentInstance();
                        fc.addMessage(null, Message);
                        return "save and close";
                    }
                }
    
    
                if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ") {
                    if (op.getErrors().isEmpty()) {
                        ADFUtils.findOperation("Commit").execute();
                        FacesMessage Message = new FacesMessage("Record Update Successfully.");
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);
                        FacesContext fc = FacesContext.getCurrentInstance();
                        fc.addMessage(null, Message);
                        return "save and close";
    
                    }
    
    
                }
            } else {
                FacesMessage Message = new FacesMessage("Please approve issue slip first then save.");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
    
            }
        }
        return null;
    }

    public void issueQtyValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (object != null) {
            BigDecimal stockqty = (BigDecimal) getStockQtyBinding().getValue();
            BigDecimal issueQty = (BigDecimal) object;
            System.out.println("stock qty is--===>>>" + stockqty + "issue quantity=====>>>" + issueQty);
            if (stockqty != null) {
                if (stockqty.equals(new BigDecimal(0))) {

                    System.out.println("helllooooooo in validator======");
                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                  "Item Out of Stock Cannot be Issued", null));
                }
                if (issueQty.compareTo(stockqty) == 1) {

                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "More than Stock Qty.",
                                                                  null));
                }
            }
        }

    }

    public void setIssueTab2Binding(RichShowDetailItem issueTab2Binding) {
        this.issueTab2Binding = issueTab2Binding;
    }

    public RichShowDetailItem getIssueTab2Binding() {
        return issueTab2Binding;
    }

    public void setIssueTab1Binding(RichShowDetailItem issueTab1Binding) {
        this.issueTab1Binding = issueTab1Binding;
    }

    public RichShowDetailItem getIssueTab1Binding() {
        return issueTab1Binding;
    }

    public void authorisedByVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        OperationBinding ob = ADFUtils.findOperation("checkApprovalStatus");
        ob.getParamsMap().put("formNm", "ISSUE");
        ob.getParamsMap().put("authoLim", "AP");
        ob.getParamsMap().put("empcd", vce.getNewValue().toString());
        ob.execute();
        System.out.println(" EMP CD" + vce.getNewValue().toString());
        if ((ob.getResult() != null && ob.getResult().equals("Y"))) {
            Row row = (Row) ADFUtils.evaluateEL("#{bindings.IssueSlipHeaderVO1Iterator.currentRow}");
            row.setAttribute("AuthBy", null);
            ADFUtils.showMessage("You have no authority to approve this Issue Slip.", 0);
        }

    }

    public void QcdNoValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {

        System.out.println("in the validator is 666***" + object);
        if (object != null) {
            System.out.println("in the validator is***" + object);
            OperationBinding ob = ADFUtils.findOperation("getExpireDateIssue");
            ob.execute();
            System.out.println("result is " + ob.getResult());
            if (ob.getResult().equals("Y")) {

                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                              "Item is Expired,Please select another Item.", null));

            }
        }

    }

    public void qcdNoValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        System.out.println("****&&&&&&&***in the validator is ***");

        System.out.println("in the validator is 666***" + object);
        if (object != null) {
            System.out.println("in the validator is***" + object);
            OperationBinding ob = ADFUtils.findOperation("getExpireDateIssue");
            ob.execute();
            System.out.println("result is " + ob.getResult());
            if (ob.getResult().equals("Y")) {

                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                              "Item is Expired,Please select another Item.", null));

            }
        }

    }

    public void setJobCodeBinding(RichInputText jobCodeBinding) {
        this.jobCodeBinding = jobCodeBinding;
    }

    public RichInputText getJobCodeBinding() {
        return jobCodeBinding;
    }

    public void setItemTypeBinding(RichSelectOneChoice itemTypeBinding) {
        this.itemTypeBinding = itemTypeBinding;
    }

    public RichSelectOneChoice getItemTypeBinding() {
        return itemTypeBinding;
    }

    public void setSlipdatebindingNP(RichInputText slipdatebindingNP) {
        this.slipdatebindingNP = slipdatebindingNP;
    }

    public RichInputText getSlipdatebindingNP() {
        return slipdatebindingNP;
    }

    public void populateButtonAL(ActionEvent actionEvent) {
        OperationBinding ob = ADFUtils.findOperation("populateIssueSlipDetail");
        ob.execute();
    }
}
