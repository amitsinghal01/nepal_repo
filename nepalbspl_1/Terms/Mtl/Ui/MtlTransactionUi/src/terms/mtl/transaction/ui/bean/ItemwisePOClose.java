package terms.mtl.transaction.ui.bean;

import java.math.BigDecimal;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.binding.DCIteratorBinding;

import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;
import oracle.jbo.ViewCriteria;
import oracle.jbo.ViewObject;
import oracle.jbo.server.RowQualifier;
import oracle.jbo.server.ViewObjectImpl;

public class ItemwisePOClose {
    
    private int checkStatus = 0;
    private RichSelectOneChoice bindingClosed;
    //    private int blncQty;
    
    public ItemwisePOClose() {
    }
    

    public void setCheckStatus(int checkStatus) {
        this.checkStatus = checkStatus;
    }

    public int getCheckStatus() {
        return checkStatus;
    }


//    public void setBlncQty(int blncQty) {
//        this.blncQty = blncQty;
//    }
//
//    public int getBlncQty() {
//        return blncQty;
//    }

    public void save(ActionEvent actionEvent) {
//        OperationBinding op = ADFUtils.findOperation("Commit");
//        op.execute();
//        if(op.getErrors().isEmpty()){
//            FacesMessage Message = new FacesMessage("Operation Successfully Performed.");   
//            Message.setSeverity(FacesMessage.SEVERITY_INFO);   
//            FacesContext fc = FacesContext.getCurrentInstance();   
//            fc.addMessage(null, Message);  
//        }
//        
//        this.setEditModeStatus(Boolean.TRUE);
        DCIteratorBinding dci = ADFUtils.findIterator("PoDtlForPoCloseVVO1Iterator");
        
        
        OperationBinding op = ADFUtils.findOperation("saveClosePo");
        op.getParamsMap().put("poNo",dci.getCurrentRow().getAttribute("PoHeadPoNo"));
        op.getParamsMap().put("poAmdNo",dci.getCurrentRow().getAttribute("PoAmdNo"));
        op.execute();
        if(op.getErrors().isEmpty()){
            FacesMessage Message = new FacesMessage("Operation Successfully Performed.");   
            Message.setSeverity(FacesMessage.SEVERITY_INFO);   
            FacesContext fc = FacesContext.getCurrentInstance();   
            fc.addMessage(null, Message);
            
        }
        //this.bindingClosed.setDisabled(Boolean.TRUE);

    }
    
    public void enableEdit(ActionEvent ae){
            this.setCheckStatus(0);
            DCIteratorBinding dci = ADFUtils.findIterator("PoDtlForPoCloseVVO1Iterator");
            RowSetIterator rsi=dci.getViewObject().createRowSetIterator(null);
            Row[] rows = rsi.getFilteredRows("PoHeadPoNo", dci.getCurrentRow().getAttribute("PoHeadPoNo"));
            
            if(dci.getCurrentRow().getAttribute("Closed").equals("O")){
                //this.bindingClosed.setDisabled(Boolean.FALSE);
            }
            
            for(Row r : rows){
                if(r.getAttribute("Closed").toString().equals("C")){
                    this.setCheckStatus(checkStatus+1);
                }
            }
            if(this.getCheckStatus()==rows.length){
//                System.out.println("Check Status"+this.getCheckStatus());
                FacesMessage Message = new FacesMessage("All Items are Closed Already.");   
                Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                FacesContext fc = FacesContext.getCurrentInstance();   
                fc.addMessage(null, Message);
            }
        
        
              
        
    }

//    public void closePO(ActionEvent actionEvent) {
//        DCIteratorBinding dci_detail = ADFUtils.findIterator("PoDtlForPoCloseVVO1Iterator");
//        
//        OperationBinding op = ADFUtils.findOperation("closePO");
//        op.getParamsMap().put("poNo",dci_detail.getCurrentRow().getAttribute("PoHeadPoNo"));
//        op.getParamsMap().put("amdNo",dci_detail.getCurrentRow().getAttribute("PoAmdNo"));
//        op.execute();
////        System.out.println("close po op result: "+op.getResult());
//        if(op.getResult().equals("SHOW_NOT_NULL")){
//            
//            FacesMessage Message = new FacesMessage("PO Closed Successfully");   
//            Message.setSeverity(FacesMessage.SEVERITY_INFO);   
//            FacesContext fc = FacesContext.getCurrentInstance();   
//            fc.addMessage(null, Message);
//        
//        }
//        
////        System.out.println("Po No. : "+dci_detail.getCurrentRow().getAttribute("PoHeadPoNo"));
////        System.out.println("Amd No. : "+dci_detail.getCurrentRow().getAttribute("PoAmdNo"));
//        
//    }
    
//    public void calculateBlncQty(){
//        DCIteratorBinding dci_detail = ADFUtils.findIterator("PurchaseOrderDetailVO6Iterator");
//        DCIteratorBinding gte_detail = ADFUtils.findIterator("GtEdetailsVO2Iterator");
//        
//        Integer dtl_qty = (Integer)dci_detail.getCurrentRow().getAttribute("Qty");
//        Integer gte_qty = (Integer)gte_detail.getCurrentRow().getAttribute("ChallanQty");
//        
//        int blnc_qty = (dtl_qty-gte_qty);
//        this.setBlncQty(blnc_qty);
//        
//        
//    }

    public void setBindingClosed(RichSelectOneChoice bindingClosed) {
        this.bindingClosed = bindingClosed;
    }

    public RichSelectOneChoice getBindingClosed() {
        return bindingClosed;
    }
}
