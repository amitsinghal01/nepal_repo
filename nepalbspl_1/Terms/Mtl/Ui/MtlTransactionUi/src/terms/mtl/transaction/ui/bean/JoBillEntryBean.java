package terms.mtl.transaction.ui.bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.math.RoundingMode;

import java.text.ParseException;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.validator.ValidatorException;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichDialog;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputFile;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.event.DialogEvent.Outcome;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

import oracle.jbo.RowSetIterator;

import org.apache.myfaces.trinidad.model.UploadedFile;

public class JoBillEntryBean {
    private RichInputText bindDocFileNamePath;
    private RichInputComboboxListOfValues unitCdBinding;
    private RichInputComboboxListOfValues joNoBinding;
    private RichInputComboboxListOfValues preprdByBinding;
    private RichInputFile chooseFileBinding;
    public String vMode = "V";
    private RichDialog docDialogBinding;
    private RichInputText contractorBillNoBinding;
    private RichInputText wrkQtyBinding;
    private RichInputText approvdQtyBinding;
    private RichInputDate billDateBinding;
    private RichInputText contractorBillAmountBinding;
    private RichOutputText outputTextBinding;
    private RichButton headerEditBinding;
    private RichInputText joBillNoBinding;
    private RichInputText amendmentNoBinding;
    private RichInputText contractorBinding;
    private RichInputDate contractDateBinding;
    private RichInputDate joBilldateBinding;
    private RichInputDate amendmentDateBinding;
    private RichSelectOneChoice statusBinding;
    private RichInputComboboxListOfValues approvedByBinding;
    private RichInputDate approvedDateBinding;
    private RichInputText panNoBinding;
    private RichSelectOneChoice materialStatusBinding;
    private RichTable docDtlTableBinding;
    private RichInputText totalAppAmtBinding;
    private RichInputText valueBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichShowDetailItem tabLabel1Binding;
    private RichShowDetailItem tabLabel2Binding;
    private RichShowDetailItem tablabel3Binding;
    private RichTable allocationTableBinding;
    String val = "F", val1 = "F";
    private RichInputText billQtyBinding;
    private RichTable joDetailTableBinding;
    private RichInputText rateBinding;
    private RichInputText lumsumBinding;
    private RichInputText valueCoBinding;
    private RichInputText jobCdBinding;
    private RichInputText billAmountBinding;
    private RichInputText opClBinding;
    private RichInputDate verifiedDateBinding;
    private RichInputComboboxListOfValues verifiedByBinding;
    private RichInputText jobilldateNepalbinding;
    private RichInputText joamendmentdatenepalBinding;
    private RichInputText billdatenepalinding;

    public JoBillEntryBean() {
    }


    public void uploadFileVCE(ValueChangeEvent vce) {
        if (vce.getNewValue() != null) {
            System.out.println("vce.getNewValue-->" + vce.getNewValue());
            UploadedFile lF = (UploadedFile) vce.getNewValue();
            System.out.println("LF-->" + lF);
            //Upload File to path- Return actual server path
            String path = uploadFile(lF);
            //Method to insert data in table to keep track of uploaded files

            OperationBinding ob = ADFUtils.findOperation("setJoBillEntryFileDataInDocTable");
            ob.getParamsMap().put("name", lF.getFilename());
            ob.getParamsMap().put("path", path);
            ob.getParamsMap().put("contTyp", lF.getContentType());
            ob.execute();
            //  vce.getComponent().processUpdates(FacesContext.getCurrentInstance());

        }
    }


    // Method For upload file

    private String uploadFile(UploadedFile file) {
        String path = null;
        UploadedFile myFile = file;
        if (myFile == null) {
        } else {
            //  Path to Store all File in Folder
            path = "/home/beta13/uplaodedDoc/" + myFile.getFilename();
            InputStream inputStream = null;
            try {
                FileOutputStream out = new FileOutputStream(path);
                System.out.println("myFile.getInputStream()-->" + myFile.getInputStream());
                inputStream = myFile.getInputStream();
                System.out.println("InputStream IN Bean val-->" + inputStream);
                byte[] buffer = new byte[8192];
                int bytesRead = 0;
                while ((bytesRead = inputStream.read(buffer, 0, 8192)) != -1) {
                    out.write(buffer, 0, bytesRead);
                }
                out.flush();
                out.close();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    inputStream.close();
                } catch (IOException e) {

                }
            }


        }


        return path;

    }


    /**Method to download file from actual path
     * @param facesContext
     * @param outputStream */
    public void downloadFileAL(FacesContext facesContext, OutputStream outputStream) throws IOException { //Read file from particular path, path bind is binding of table field that contains path
        File filed = new File(bindDocFileNamePath.getValue().toString());
        FileInputStream fis;
        byte[] b;
        try {
            fis = new FileInputStream(filed);
            int n;
            while ((n = fis.available()) > 0) {
                b = new byte[n];
                int result = fis.read(b);
                outputStream.write(b, 0, b.length);
                if (result == -1)
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        outputStream.flush();
    }

    public void setBindDocFileNamePath(RichInputText bindDocFileNamePath) {
        this.bindDocFileNamePath = bindDocFileNamePath;
    }

    public RichInputText getBindDocFileNamePath() {
        return bindDocFileNamePath;
    }

    public void saveJoBillEntryAL(ActionEvent actionEvent) {
        
        String empcd=(String)ADFUtils.resolveExpression("#{pageFlowScope.empCode}") ; 
        System.out.println("empcd"+empcd);
        ADFUtils.setLastUpdatedBy("JoBillHeadVO1Iterator", "LastUpdatedBy");
        
        //         //   if (unitCdBinding.getValue() != null && unitCdBinding.getValue().toString().length() > 0) {
        //                       if (joNoBinding.getValue() != null && joNoBinding.getValue().toString().length() > 0) {
        ////                           if (preprdByBinding.getValue() != null && preprdByBinding.getValue().toString().length() > 0) {
        ////                               if (contractorBillNoBinding.getValue() != null &&
        ////                                   contractorBillNoBinding.getValue().toString().length() > 0 &&
        ////                                   !contractorBillNoBinding.getValue().equals("")) {
        ////                                   if(billDateBinding.getValue() != null &&
        ////                                       billDateBinding.getValue().toString().length() > 0 &&
        ////                                      !billDateBinding.getValue().equals(""))
        ////                                     {
        ////                                         if(contractorBillAmountBinding.getValue() != null &&
        ////                                             contractorBillAmountBinding.getValue().toString().length() > 0 &&
        ////                                            !contractorBillAmountBinding.getValue().equals(""))
        ////                                           {
        //
        //                             //      if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
        //                             if (!getJoNoBinding().isDisabled()) {
        //                                       OperationBinding binding = ADFUtils.findOperation("generateJoBillEntryNo");
        //                                       Object ob = binding.execute();
        //                                       System.out.println("Object Value in Bean-- " + ob);
        //                                       if (ob.toString() != null && ob.toString() != "") {
        //                                           if (binding.getErrors().isEmpty() && !ob.equals("NOT")) {
        //                                           OperationBinding opQty = ADFUtils.findOperation("checkWABQtyVal");
        //                                           Object obQty= opQty.execute();
        //                                           if(obQty.toString()!=null && obQty.toString()!="" && obQty.equals("Y"))
        //                                           {
        //
        //                                               ADFUtils.findOperation("Commit").execute();
        //                                               FacesMessage message =
        //                                                   new FacesMessage("Record Saved Successfully.Jo Bill No is" + " " + ob);
        //                                               message.setSeverity(FacesMessage.SEVERITY_INFO);
        //                                               FacesContext fc = FacesContext.getCurrentInstance();
        //                                               fc.addMessage(null, message);
        //                                                   vMode = "P";
        //                                                   System.out.println("VMode-==-->"+vMode);
        //                                               }
        //                                               else{
        //                                                   if(obQty.toString()!=null && obQty.toString()!="" && obQty.equals("W"))
        //                                                   {
        //
        //                                                           FacesMessage message = new FacesMessage("Please enter Work Qty and it should be greater than Zero.");
        //                                                           message.setSeverity(FacesMessage.SEVERITY_ERROR);
        //                                                           FacesContext fc = FacesContext.getCurrentInstance();
        //                                                           fc.addMessage(wrkQtyBinding.getClientId(), message);
        //                                                       }
        //
        //                                                   if(obQty.toString()!=null && obQty.toString()!="" && obQty.equals("A"))
        //                                                   {
        //
        //                                                           FacesMessage message = new FacesMessage("Please enter Approved Qty and it should be greater than Zero.");
        //                                                           message.setSeverity(FacesMessage.SEVERITY_ERROR);
        //                                                           FacesContext fc = FacesContext.getCurrentInstance();
        //                                                           fc.addMessage(approvdQtyBinding.getClientId(), message);
        //                                                   }
        //                                                   if(obQty.toString()!=null && obQty.toString()!="" && obQty.equals("G"))
        //                                                   {
        //
        //                                                           FacesMessage message = new FacesMessage("Approved Qty Can Not Be More Then Work Qty");
        //                                                           message.setSeverity(FacesMessage.SEVERITY_ERROR);
        //                                                           FacesContext fc = FacesContext.getCurrentInstance();
        //                                                           fc.addMessage(approvdQtyBinding.getClientId(), message);
        //                                                   }
        //
        //                                               }
        //
        //
        //                                           } else {
        //                                               FacesMessage message = new FacesMessage("Please Polulate Details");
        //                                               message.setSeverity(FacesMessage.SEVERITY_ERROR);
        //                                               FacesContext fc = FacesContext.getCurrentInstance();
        //                                               fc.addMessage(null, message);
        //                                           }
        //                                       }
        //                                   }
        //
        //
        //                                   if (getJoNoBinding().isDisabled()) {
        //                                       vMode = "P";
        //                                       ADFUtils.findOperation("Commit").execute();
        //                                       FacesMessage message = new FacesMessage("Record updated Successfully.");
        //                                       message.setSeverity(FacesMessage.SEVERITY_INFO);
        //                                       FacesContext fc = FacesContext.getCurrentInstance();
        //                                       fc.addMessage(null, message);
        //                                   }
        ////
        ////                                     }
        ////
        ////
        ////
        ////                                   }
        ////
        ////
        ////                                   // msg for contractor Bill no.
        ////                               }
        ////
        ////                           }
        ////
        ////                       }
        ////
        //
        //                   }
        DCIteratorBinding binding = ADFUtils.findIterator("JoBillAllocationDtlVO1Iterator");
        RowSetIterator rsi=binding.getViewObject().createRowSetIterator(null);
        while(rsi.hasNext() && joBillNoBinding.getValue()!=null)
        {
            Row r=rsi.next();
            r.setAttribute("JoHeadNo", joBillNoBinding.getValue());
        }
        rsi.closeRowSetIterator();
        
        DCIteratorBinding bind = ADFUtils.findIterator("JoBillDetailVO1Iterator");
        RowSetIterator rs=bind.getViewObject().createRowSetIterator(null);
        while(rs.hasNext() && joBillNoBinding.getValue()!=null)
        {
            Row r=rs.next();
            r.setAttribute("JoHeadNo", joBillNoBinding.getValue());
        }
        rs.closeRowSetIterator();
        
        if(binding.getCurrentRow()!=null)
        {
        String AllocationAmt="";
        AllocationAmt=(String)ADFUtils.resolveExpression("#{bindings.AmountTrans.inputValue}");
        BigDecimal BillAmount=(BigDecimal)ADFUtils.resolveExpression("#{bindings.AppAmt.inputValue}");
        BigDecimal BillAmt=BillAmount.setScale(3, RoundingMode.HALF_UP);
        BigDecimal AllocationAmount=new BigDecimal(AllocationAmt);
        System.out.println("Bill Amount==>"+BillAmt+"    Allocation Amount"+AllocationAmount);
        if(AllocationAmount.compareTo(BillAmt)==1)
        {
            val="T";
        }
        }
        if (val.equals("F")) {
               if (joBillNoBinding.getValue()==null)  {
                        OperationBinding op = ADFUtils.findOperation("generateJoBillEntryNo");
                        Object rst = op.execute();
                        ADFUtils.findOperation("Commit").execute();
                        FacesMessage Message = new FacesMessage("Record Saved Successfully.Advice No is " + rst + ".");
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);
                        FacesContext fc = FacesContext.getCurrentInstance();
                        fc.addMessage(null, Message);
                }
                 else{
                        ADFUtils.findOperation("Commit").execute();
                        FacesMessage Message = new FacesMessage("Record Updated Successfully.");
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);
                        FacesContext fc = FacesContext.getCurrentInstance();
                        fc.addMessage(null, Message);

                    }


            } else {
                ADFUtils.showMessage("Location Wise Amount must be equals to than Bill Amount.", 0);
            }
    }


    public void JoBillNoVCE(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
    }

    public void populateJoBillDetailAL(ActionEvent actionEvent) {

        if (unitCdBinding.getValue() != null && unitCdBinding.getValue().toString().length() > 0) {
            if (joNoBinding.getValue() != null && joNoBinding.getValue().toString().length() > 0 && val.equals("F")) {
                OperationBinding op = (OperationBinding) ADFUtils.findOperation("populateJoBillDetail");
                Object rst = op.execute();
                System.out.println("RESULT" + rst);
                getJoNoBinding().setDisabled(true);
                getContractorBillNoBinding().setDisabled(true);
            }
        }
        vMode = "P";
    }

    public void JoNoVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce.getNewValue() != null) {
            OperationBinding op = (OperationBinding) ADFUtils.findOperation("joBillValidation");
            op.execute();
            System.out.println(" OP RESULT" + op.getResult());
            if (op.getResult() != null) {
                if (!op.getResult().equals(null) && op.getResult().equals("N")) {
                    val = "T";
                    ADFUtils.showMessage("For the respective Contract Order Jo Bill already exist.", 0);
                } else {
                    val = "F";
                }
            }
        }
    }

    public void setUnitCdBinding(RichInputComboboxListOfValues unitCdBinding) {
        this.unitCdBinding = unitCdBinding;
    }

    public RichInputComboboxListOfValues getUnitCdBinding() {
        return unitCdBinding;
    }

    public void setJoNoBinding(RichInputComboboxListOfValues joNoBinding) {
        this.joNoBinding = joNoBinding;
    }

    public RichInputComboboxListOfValues getJoNoBinding() {
        return joNoBinding;
    }

    public void setPreprdByBinding(RichInputComboboxListOfValues preprdByBinding) {
        this.preprdByBinding = preprdByBinding;
    }

    public RichInputComboboxListOfValues getPreprdByBinding() {
        return preprdByBinding;
    }

    public void createDocSerialNoAL(ActionEvent actionEvent) {
        // Add event code here...
        ADFUtils.findOperation("CreateInsert1").execute();
        ADFUtils.findOperation("generateDocSerialNoInJoBillDtl").execute();
    }

    public void setChooseFileBinding(RichInputFile chooseFileBinding) {
        this.chooseFileBinding = chooseFileBinding;
    }

    public RichInputFile getChooseFileBinding() {
        return chooseFileBinding;
    }


    public void docUploadDialogListener(DialogEvent dialogEvent) {
        //   dialogEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        Outcome outcome = dialogEvent.getOutcome();
        if (outcome == Outcome.ok) {

            System.out.println("bindDocFileNamePath.getValue()===" + bindDocFileNamePath.getValue());
        }
    }

    public void setContractorBillNoBinding(RichInputText contractorBillNoBinding) {
        this.contractorBillNoBinding = contractorBillNoBinding;
    }

    public RichInputText getContractorBillNoBinding() {
        return contractorBillNoBinding;
    }


    public void setWrkQtyBinding(RichInputText wrkQtyBinding) {
        this.wrkQtyBinding = wrkQtyBinding;
    }

    public RichInputText getWrkQtyBinding() {
        return wrkQtyBinding;
    }

    public void setApprovdQtyBinding(RichInputText approvdQtyBinding) {
        this.approvdQtyBinding = approvdQtyBinding;
    }

    public RichInputText getApprovdQtyBinding() {
        return approvdQtyBinding;
    }

    public void setBillDateBinding(RichInputDate billDateBinding) {
        this.billDateBinding = billDateBinding;
    }

    public RichInputDate getBillDateBinding() {
        try {
        //              System.out.println("set getdate"+ADFUtils.getTodayDate());
                  String date=ADFUtils.convertAdToBs(ADFUtils.getTodayDate().toString());
        //              System.out.println("setter getvaluedate"+date);
        //   srvdatenepalbinding.setValue(date.toString());
                DCIteratorBinding dci = ADFUtils.findIterator("JoBillHeadVO1Iterator");
                dci.getCurrentRow().setAttribute("BillDtNepal",date);
              }
            catch (ParseException e) {
        
              }
        return billDateBinding;
    }

    public void setContractorBillAmountBinding(RichInputText contractorBillAmountBinding) {
        this.contractorBillAmountBinding = contractorBillAmountBinding;
    }

    public RichInputText getContractorBillAmountBinding() {
        return contractorBillAmountBinding;
    }

    public void setVMode(String vMode) {
        this.vMode = vMode;
    }

    public String getVMode() {
        return vMode;
    }

    public void setOutputTextBinding(RichOutputText outputTextBinding) {
        this.outputTextBinding = outputTextBinding;
    }

    public RichOutputText getOutputTextBinding() {
        cevmodecheck();
        return outputTextBinding;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }

    //Set Fields and Button disable.
    public void cevModeDisableComponent(String mode) {
        //            FacesContext fctx = FacesContext.getCurrentInstance();
        //            ELContext elctx = fctx.getELContext();
        //            Application jsfApp = fctx.getApplication();
        //            //create a ValueExpression that points to the ADF binding layer
        //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
        //            //
        //            ValueExpression valueExpr = exprFactory.createValueExpression(
        //                                         elctx,
        //                                         "#{pageFlowScope.mode=='E'}",
        //                                          Object.class
        //                                         );
        //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
        if (mode.equals("E")) {
            getHeaderEditBinding().setDisabled(true);
            getUnitCdBinding().setDisabled(true);
            getJoNoBinding().setDisabled(true);
            getJoBillNoBinding().setDisabled(true);
            getAmendmentNoBinding().setDisabled(true);
            getContractorBinding().setDisabled(true);
            getPreprdByBinding().setDisabled(true);
            getContractDateBinding().setDisabled(true);
            getJoBilldateBinding().setDisabled(true);
            getAmendmentDateBinding().setDisabled(true);
            getStatusBinding().setDisabled(true);
//            getContractorBillNoBinding().setDisabled(true);
            getContractorBillAmountBinding().setDisabled(true);
            getApprovedDateBinding().setDisabled(true);
            getPanNoBinding().setDisabled(true);
//            getBillDateBinding().setDisabled(true);
            getMaterialStatusBinding().setDisabled(true);
            getTotalAppAmtBinding().setDisabled(true);
            jobCdBinding.setDisabled(true);
            billAmountBinding.setDisabled(true);
            verifiedDateBinding.setDisabled(true);
            getJoamendmentdatenepalBinding().setDisabled(true);
            getJobilldateNepalbinding().setDisabled(true);
            getBilldatenepalinding().setDisabled(true);
        } else if (mode.equals("C")) {
            getHeaderEditBinding().setDisabled(true);
            getUnitCdBinding().setDisabled(true);
            getPreprdByBinding().setDisabled(true);
            getJoBillNoBinding().setDisabled(true);
            getAmendmentNoBinding().setDisabled(true);
            getContractorBinding().setDisabled(true);
            getContractDateBinding().setDisabled(true);
            getJoBilldateBinding().setDisabled(true);
            getAmendmentDateBinding().setDisabled(true);
            getApprovedByBinding().setDisabled(true);
            getApprovedDateBinding().setDisabled(true);
            getPanNoBinding().setDisabled(true);
            getTotalAppAmtBinding().setDisabled(true);
            jobCdBinding.setDisabled(true);
            billAmountBinding.setDisabled(true);
            verifiedDateBinding.setDisabled(true);
            verifiedByBinding.setDisabled(true);
            getJoamendmentdatenepalBinding().setDisabled(true);
            getJobilldateNepalbinding().setDisabled(true);
            getBilldatenepalinding().setDisabled(true);
        } else if (mode.equals("V")) {
            getTabLabel1Binding().setDisabled(false);
            getTabLabel2Binding().setDisabled(false);
            tablabel3Binding.setDisabled(false);
        }

    }


    public void editButtonAL(ActionEvent actionEvent) {
        if (approvedByBinding.getValue() != null) {
            ADFUtils.showMessage("JO Bill Has Been Approved,So You Can Not  Modify it.", 2);
        }else {
            cevmodecheck();
                   if(verifiedByBinding.getValue()==null && approvedByBinding.getValue()==null)
                    {
                        approvedByBinding.setDisabled(true);
                    }
                   else if(approvedByBinding.getValue()==null && verifiedByBinding.getValue()!=null)
                    {
                        verifiedByBinding.setDisabled(true);
                    }
            
            try {
                System.out.println("set getEntryDateBinding"+ADFUtils.getTodayDate());
                String date=ADFUtils.convertAdToBs(ADFUtils.getTodayDate().toString());
                System.out.println("setter getvaluedate"+date);
              //  joamendmentdatenepalBinding.setValue(date.toString());
                        DCIteratorBinding dci = ADFUtils.findIterator("JoBillHeadVO1Iterator");
                        dci.getCurrentRow().setAttribute("JoAmdDateNepal",date);
            } catch (ParseException e) {
            }
        }
       
        
    }

    public void setJoBillNoBinding(RichInputText joBillNoBinding) {
        this.joBillNoBinding = joBillNoBinding;
    }

    public RichInputText getJoBillNoBinding() {
        return joBillNoBinding;
    }

    public void setAmendmentNoBinding(RichInputText amendmentNoBinding) {
        this.amendmentNoBinding = amendmentNoBinding;
    }

    public RichInputText getAmendmentNoBinding() {
        return amendmentNoBinding;
    }

    public void setContractorBinding(RichInputText contractorBinding) {
        this.contractorBinding = contractorBinding;
    }

    public RichInputText getContractorBinding() {
        return contractorBinding;
    }

    public void setContractDateBinding(RichInputDate contractDateBinding) {
        this.contractDateBinding = contractDateBinding;
    }

    public RichInputDate getContractDateBinding() {
        return contractDateBinding;
    }

    public void setJoBilldateBinding(RichInputDate joBilldateBinding) {
        this.joBilldateBinding = joBilldateBinding;
    }

    public RichInputDate getJoBilldateBinding() {
        try {
             System.out.println("set getEntryDateBinding"+ADFUtils.getTodayDate());
             String date=ADFUtils.convertAdToBs(ADFUtils.getTodayDate().toString());
             System.out.println("setter getvaluedate"+date);
          //   jobilldateNepalbinding.setValue(date.toString());
                    DCIteratorBinding dci = ADFUtils.findIterator("JoBillHeadVO1Iterator");
                    dci.getCurrentRow().setAttribute("JoDateNepal",date);
         } catch (ParseException e) {
         }

        return joBilldateBinding;
    }

    public void setAmendmentDateBinding(RichInputDate amendmentDateBinding) {
        this.amendmentDateBinding = amendmentDateBinding;
    }

    public RichInputDate getAmendmentDateBinding() {
        

        return amendmentDateBinding;
    }

    public void setStatusBinding(RichSelectOneChoice statusBinding) {
        this.statusBinding = statusBinding;
    }

    public RichSelectOneChoice getStatusBinding() {
        return statusBinding;
    }

    public void setApprovedByBinding(RichInputComboboxListOfValues approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputComboboxListOfValues getApprovedByBinding() {
        return approvedByBinding;
    }

    public void setApprovedDateBinding(RichInputDate approvedDateBinding) {
        this.approvedDateBinding = approvedDateBinding;
    }

    public RichInputDate getApprovedDateBinding() {
        return approvedDateBinding;
    }

    public void setPanNoBinding(RichInputText panNoBinding) {
        this.panNoBinding = panNoBinding;
    }

    public RichInputText getPanNoBinding() {
        return panNoBinding;
    }

    public void setMaterialStatusBinding(RichSelectOneChoice materialStatusBinding) {
        this.materialStatusBinding = materialStatusBinding;
    }

    public RichSelectOneChoice getMaterialStatusBinding() {
        return materialStatusBinding;
    }

    public void deletePopUpDtlDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            OperationBinding op = null;
            ADFUtils.findOperation("Delete").execute();
            op = (OperationBinding) ADFUtils.findOperation("Commit");
            Object rst = op.execute();
            System.out.println("Record Delete Successfully");
            if (op.getErrors().isEmpty()) {
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            } else if (!op.getErrors().isEmpty()) {
                OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
                Object rstr = opr.execute();
                FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            }
        }

        AdfFacesContext.getCurrentInstance().addPartialTarget(docDtlTableBinding);

    }

    public void setDocDtlTableBinding(RichTable docDtlTableBinding) {
        this.docDtlTableBinding = docDtlTableBinding;
    }

    public RichTable getDocDtlTableBinding() {
        return docDtlTableBinding;
    }

    public void setTotalAppAmtBinding(RichInputText totalAppAmtBinding) {
        this.totalAppAmtBinding = totalAppAmtBinding;
    }

    public RichInputText getTotalAppAmtBinding() {
        return totalAppAmtBinding;
    }

    public void setValueBinding(RichInputText valueBinding) {
        this.valueBinding = valueBinding;
    }

    public RichInputText getValueBinding() {
        return valueBinding;
    }

    public void ApprovedQtyValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (object != null && billQtyBinding.getValue() != null) {
            BigDecimal v1 = (BigDecimal) billQtyBinding.getValue();
            BigDecimal v2 = (BigDecimal) object;
            if (v2.compareTo(v1) == 1) {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                              "Approved Qty should be less than or equal to Bill Qty.",
                                                              null));
            }
        }
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setTabLabel1Binding(RichShowDetailItem tabLabel1Binding) {
        this.tabLabel1Binding = tabLabel1Binding;
    }

    public RichShowDetailItem getTabLabel1Binding() {
        return tabLabel1Binding;
    }

    public void setTabLabel2Binding(RichShowDetailItem tabLabel2Binding) {
        this.tabLabel2Binding = tabLabel2Binding;
    }

    public RichShowDetailItem getTabLabel2Binding() {
        return tabLabel2Binding;
    }

    public void ValueVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce.getNewValue() != null && vce.getNewValue() != vce.getOldValue()) {
            OperationBinding op = (OperationBinding) ADFUtils.findOperation("sumValueJoBillEntry");
            op.execute();
            System.out.println("RESULT VALIDATION" + op.execute());
        }
//            if (op.getResult() != null) {
//                if (op.getResult().equals("N")) {
//                    ADFUtils.showMessage("Jo Bill Amount cannot be greater than Contract Order Amount..", 0);
//                    val1 = "T";
//                } else {
//                    val1 = "F";
//                }
//            }
//        }
    }

    public void allocationDeleteDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete1").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(allocationTableBinding);
    }

    public void setTablabel3Binding(RichShowDetailItem tablabel3Binding) {
        this.tablabel3Binding = tablabel3Binding;
    }

    public RichShowDetailItem getTablabel3Binding() {
        return tablabel3Binding;
    }

    public void setAllocationTableBinding(RichTable allocationTableBinding) {
        this.allocationTableBinding = allocationTableBinding;
    }

    public RichTable getAllocationTableBinding() {
        return allocationTableBinding;
    }

    public void amountVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce != null) {
            OperationBinding op = ADFUtils.findOperation("amountValidation");
            op.execute();
            if (op.getResult().toString().equals("N")) {
                val = "T";
               ADFUtils.showMessage("Location Wise Amount must be equals to Bill Amount.", 0);
            } else {
                val = "F";
            }
        }
    }

    public void allocationAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert2").execute();
    }

    public void setBillQtyBinding(RichInputText billQtyBinding) {
        this.billQtyBinding = billQtyBinding;
    }

    public RichInputText getBillQtyBinding() {
        return billQtyBinding;
    }

    public void billQtyValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        System.out.println("Open Close Order Status====>"+opClBinding.getValue());
        if (object != null && wrkQtyBinding.getValue() != null && opClBinding.getValue().equals("C")) {
            BigDecimal v1 = (BigDecimal) wrkQtyBinding.getValue();
            BigDecimal v2 = (BigDecimal) object;
            if (v2.compareTo(v1) == 1) {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                              "Bill Qty should be less than or equal to Order Qty.",
                                                              null));
            }
        }

    }

    public void approvedByVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        OperationBinding ob = ADFUtils.findOperation("checkApprovalFunctionStatus");
        ob.getParamsMap().put("formNm", "JB");
        ob.getParamsMap().put("authoLim", "AP");
        ob.getParamsMap().put("empcd", vce.getNewValue().toString());
        ob.getParamsMap().put("UnitCd", unitCdBinding.getValue());
        ob.getParamsMap().put("Amount", ADFUtils.resolveExpression("#{bindings.AppAmt.inputValue}"));
        ob.execute();
        if ((ob.getResult()!=null && !ob.getResult().equals("Y"))) {
            Row row = (Row) ADFUtils.evaluateEL("#{bindings.JoBillHeadVO1Iterator.currentRow}");
            row.setAttribute("ApprovedBy", null);
            row.setAttribute("ApprovedDt", null);
            ADFUtils.showMessage(ob.getResult().toString(), 0);
        }
    }

    public void rateVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        BigDecimal oldVal=(BigDecimal)vce.getOldValue();
        BigDecimal newVal=(BigDecimal)vce.getNewValue();
        System.out.println("Old Value: "+oldVal+"New Value: "+newVal);
        if (vce.getNewValue() != null && approvdQtyBinding.getValue() != null) {
            BigDecimal rate = (BigDecimal) vce.getNewValue();
            BigDecimal approvedQty = (BigDecimal) approvdQtyBinding.getValue();
            BigDecimal val = new BigDecimal(0);
            val = approvedQty.multiply(rate);
            System.out.println("Val====>" + val);
            valueBinding.setValue(val);
        }
    }

    public void deletePopUpDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete2").execute();
            System.out.println("Record Delete Successfully");
            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
        }
        Row row = (Row) ADFUtils.evaluateEL("#{bindings.JoBillDetailVO1Iterator.currentRow}");
        if(row!=null)
        {
            System.out.println("Row Found");
        String Amt=(String)ADFUtils.resolveExpression("#{bindings.ValueTrans.inputValue}");
        if(!Amt.isEmpty())
        {
        BigDecimal BillAmount=new BigDecimal(Amt);
        getBillAmountBinding().setValue(BillAmount);
        totalAppAmtBinding.setValue(BillAmount);
        }
        }else
        {
            totalAppAmtBinding.setValue(null);
            System.out.println("Row not Found");
            Row r = (Row) ADFUtils.evaluateEL("#{bindings.JoBillHeadVO1Iterator.currentRow}");
            r.setAttribute("AppAmt", null);
            getBillAmountBinding().setValue(null);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(joDetailTableBinding);
    }

    public void setJoDetailTableBinding(RichTable joDetailTableBinding) {
        this.joDetailTableBinding = joDetailTableBinding;
    }

    public RichTable getJoDetailTableBinding() {
        return joDetailTableBinding;
    }

    public void setRateBinding(RichInputText rateBinding) {
        this.rateBinding = rateBinding;
    }

    public RichInputText getRateBinding() {
        return rateBinding;
    }

    public void rateValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        String lumsum=(String)lumsumBinding.getValue();
        if(lumsum!=null && lumsum.equals("Y")){
        if(object!=null && valueCoBinding.getValue()!=null){
        BigDecimal rate=(BigDecimal)object;
        BigDecimal value=(BigDecimal)valueCoBinding.getValue();
        if(value.compareTo(rate)==-1){
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                              "Lumsum case! Rate should not be greater than "+value+".",
                                                              null));
            }
       }
     }

    }

    public void setLumsumBinding(RichInputText lumsumBinding) {
        this.lumsumBinding = lumsumBinding;
    }

    public RichInputText getLumsumBinding() {
        return lumsumBinding;
    }

    public void setValueCoBinding(RichInputText valueCoBinding) {
        this.valueCoBinding = valueCoBinding;
    }

    public RichInputText getValueCoBinding() {
        return valueCoBinding;
    }

    public void setJobCdBinding(RichInputText jobCdBinding) {
        this.jobCdBinding = jobCdBinding;
    }

    public RichInputText getJobCdBinding() {
        return jobCdBinding;
    }

    public void setBillAmountBinding(RichInputText billAmountBinding) {
        this.billAmountBinding = billAmountBinding;
    }

    public RichInputText getBillAmountBinding() {
        return billAmountBinding;
    }

    public String saveAction() {
        
        String empcd=(String)ADFUtils.resolveExpression("#{pageFlowScope.empCode}") ; 
        System.out.println("empcd"+empcd);
        ADFUtils.setColumnValue("JoBillHeadVO1Iterator", "LastUpdatedBy", empcd);
        
        DCIteratorBinding binding = ADFUtils.findIterator("JoBillAllocationDtlVO1Iterator");
        RowSetIterator rsi=binding.getViewObject().createRowSetIterator(null);
        while(rsi.hasNext() && joBillNoBinding.getValue()!=null)
        {
            Row r=rsi.next();
            r.setAttribute("JoHeadNo", joBillNoBinding.getValue());
        }
        rsi.closeRowSetIterator();
        
        DCIteratorBinding bind = ADFUtils.findIterator("JoBillDetailVO1Iterator");
        RowSetIterator rs=bind.getViewObject().createRowSetIterator(null);
        while(rs.hasNext() && joBillNoBinding.getValue()!=null)
        {
            Row r=rs.next();
            r.setAttribute("JoHeadNo", joBillNoBinding.getValue());
        }
        rs.closeRowSetIterator();
        
        if(binding.getCurrentRow()!=null)
        {
        String AllocationAmt="";
        AllocationAmt=(String)ADFUtils.resolveExpression("#{bindings.AmountTrans.inputValue}");
        BigDecimal BillAmount=(BigDecimal)ADFUtils.resolveExpression("#{bindings.AppAmt.inputValue}");
        BigDecimal BillAmt=BillAmount.setScale(3, RoundingMode.HALF_UP);
        BigDecimal AllocationAmount=new BigDecimal(AllocationAmt);
        System.out.println("Bill Amount==>"+BillAmt+"    Allocation Amount"+AllocationAmount);
        if(AllocationAmount.compareTo(BillAmt)==1)
        {
            val="T";
        }
        }
        if (val.equals("F")) {
                        if (joBillNoBinding.getValue()==null) {
                            OperationBinding op = ADFUtils.findOperation("generateJoBillEntryNo");
                            Object rst = op.execute();
                            System.out.println("--------Commit-------");
                            System.out.println("value aftr getting result--->?" + rst);
                            ADFUtils.findOperation("generateJoBillEntryNo").execute();
                                ADFUtils.findOperation("Commit").execute();
                                FacesMessage Message = new FacesMessage("Record Saved Successfully.Advice No is " + rst + ".");
                                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                                FacesContext fc = FacesContext.getCurrentInstance();
                                fc.addMessage(null, Message);
                                return "saveAndclose";
                        }else
                            {
                                ADFUtils.findOperation("Commit").execute();
                                FacesMessage Message = new FacesMessage("Record Updated Successfully.");
                                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                                FacesContext fc = FacesContext.getCurrentInstance();
                                fc.addMessage(null, Message);
                                return "saveAndclose";
                            }
                    }

                     else {
                        ADFUtils.showMessage("Location Wise Amount must be equals to than Bill Amount.", 0);
                    }

        return null;
    }

    public void setOpClBinding(RichInputText opClBinding) {
        this.opClBinding = opClBinding;
    }

    public RichInputText getOpClBinding() {
        return opClBinding;
    }
    
public void joHeadNo()
{
    
}

    public void setVerifiedDateBinding(RichInputDate verifiedDateBinding) {
        this.verifiedDateBinding = verifiedDateBinding;
    }

    public RichInputDate getVerifiedDateBinding() {
        return verifiedDateBinding;
    }

    public void VerifiedByVCL(ValueChangeEvent vce) {
//        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
//        OperationBinding ob = ADFUtils.findOperation("checkApprovalStatus");
//        ob.getParamsMap().put("formNm", "JB");
//        ob.getParamsMap().put("authoLim", "VR");
//        ob.getParamsMap().put("empcd", vce.getNewValue().toString());
//        ob.execute();
//        System.out.println(" EMP CD" + vce.getNewValue().toString());
//        if ((ob.getResult() != null && ob.getResult().equals("Y"))) {
//            Row row = (Row) ADFUtils.evaluateEL("#{bindings.JoBillHeadVO1Iterator.currentRow}");
//            row.setAttribute("VerifiedBy", null);
//            row.setAttribute("VerifiedDt", null);
//            ADFUtils.showMessage("You have no authority to verify this JO Bill.", 0);
//        }
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        OperationBinding ob = ADFUtils.findOperation("checkApprovalFunctionStatus");
        ob.getParamsMap().put("formNm", "JB");
        ob.getParamsMap().put("authoLim", "VR");
        ob.getParamsMap().put("empcd", vce.getNewValue().toString());
        ob.getParamsMap().put("UnitCd", unitCdBinding.getValue());
        ob.getParamsMap().put("Amount", ADFUtils.resolveExpression("#{bindings.AppAmt.inputValue}"));
        ob.execute();
        if ((ob.getResult()!=null && !ob.getResult().equals("Y"))) {
            Row row = (Row) ADFUtils.evaluateEL("#{bindings.JoBillHeadVO1Iterator.currentRow}");
            row.setAttribute("VerifiedBy", null);
            row.setAttribute("VerifiedDt", null);
            ADFUtils.showMessage(ob.getResult().toString(), 0);
        }
    }

    public void setVerifiedByBinding(RichInputComboboxListOfValues verifiedByBinding) {
        this.verifiedByBinding = verifiedByBinding;
    }

    public RichInputComboboxListOfValues getVerifiedByBinding() {
        return verifiedByBinding;
    }

    public void setJobilldateNepalbinding(RichInputText jobilldateNepalbinding) {
        this.jobilldateNepalbinding = jobilldateNepalbinding;
    }

    public RichInputText getJobilldateNepalbinding() {
        return jobilldateNepalbinding;
    }

    public void setJoamendmentdatenepalBinding(RichInputText joamendmentdatenepalBinding) {
        this.joamendmentdatenepalBinding = joamendmentdatenepalBinding;
    }

    public RichInputText getJoamendmentdatenepalBinding() {
        return joamendmentdatenepalBinding;
    }

    public void billdateVCL(ValueChangeEvent valueChangeEvent) {
        try {
        //              System.out.println("set getdate"+ADFUtils.getTodayDate());
                  String date=ADFUtils.convertAdToBs(valueChangeEvent.getNewValue().toString());
        //              System.out.println("setter getvaluedate"+date);
        //   srvdatenepalbinding.setValue(date.toString());
                DCIteratorBinding dci = ADFUtils.findIterator("JoBillHeadVO1Iterator");
                dci.getCurrentRow().setAttribute("BillDtNepal",date);
              }
            catch (ParseException e) {
        
              }
    }

    public void setBilldatenepalinding(RichInputText billdatenepalinding) {
        this.billdatenepalinding = billdatenepalinding;
    }

    public RichInputText getBilldatenepalinding() {
        return billdatenepalinding;
    }
}
