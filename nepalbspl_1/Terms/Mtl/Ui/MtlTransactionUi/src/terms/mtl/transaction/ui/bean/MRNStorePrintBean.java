package terms.mtl.transaction.ui.bean;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.naming.Context;
import javax.naming.InitialContext;

import javax.servlet.http.HttpServletResponse;

import javax.sql.DataSource;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCIteratorBinding;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

public class MRNStorePrintBean {
    public MRNStorePrintBean() {
    }
    String printMode = "";

    public void mrnButtonAL(ActionEvent actionEvent) {
        // Add event code here...
    }

    public BindingContainer getBindings() {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
    }

    public void downloadJrxml(FacesContext facesContext, OutputStream outputStream) {
        Connection conn = null;
        try {

            System.out.println("print mode===>" + printMode);
            String file_name = "";
            if (printMode.equalsIgnoreCase("store")) {
                file_name = "mrn_store_reg.jasper";
            }
            if (printMode.equalsIgnoreCase("accounts")) {
                file_name = "srv_accounts_m.jasper";
            }

            System.out.println("After Set Value File Name is===>" + file_name);

            OperationBinding binding = ADFUtils.findOperation("fileNameForPrint");
            binding.getParamsMap().put("fileId", "ERP0000001922");
            binding.execute();

            System.out.println("Binding Result :" + binding.getResult().toString());
            if (binding.getResult() != null) {
                String result = binding.getResult().toString();
                int last_index = result.lastIndexOf("/");
                String path = result.substring(0, last_index + 1) + file_name;
                System.out.println("FILE PATH IS===>" + path);
                InputStream input = new FileInputStream(path);

                String srvNo = null;
                DCIteratorBinding poIter = (DCIteratorBinding) getBindings().get("MRNStorePrintVO1Iterator");
                if (poIter.getCurrentRow().getAttribute("MrnNo") != null) {
                    srvNo = poIter.getCurrentRow().getAttribute("MrnNo").toString();
                }
                String unitCode = poIter.getCurrentRow().getAttribute("UnitCd").toString();
                java.util.Date fromDt = (java.util.Date) poIter.getCurrentRow().getAttribute("FromDate");
                java.util.Date toDt = (java.util.Date) poIter.getCurrentRow().getAttribute("ToDate");
                System.out.println("srvNo :- " + srvNo + " Unit code" + unitCode + "From date" + fromDt +
                                   "To Date==>>" + toDt);
                Map n = new HashMap();
                n.put("p_unit", unitCode);
                n.put("p_srvno", srvNo);
                n.put("p_fr_dt", fromDt);
                n.put("p_to_dt", toDt);

                conn = getConnection();

                JasperReport design = (JasperReport) JRLoader.loadObject(input);
                System.out.println("Path : " + input + " -------" + design + " param:" + n);

                @SuppressWarnings("unchecked")
                net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                byte pdf[] = JasperExportManager.exportReportToPdf(print);
                HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                response.getOutputStream().write(pdf);
                response.getOutputStream().flush();
                response.getOutputStream().close();
                facesContext.responseComplete();
            } else
                System.out.println("File Name/File Path not found.");
        } catch (FileNotFoundException fnfe) {
            // TODO: Add catch code
            fnfe.printStackTrace();
            try {
                System.out.println("in finally connection closed");
                conn.close();
                conn = null;

            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            // TODO: Add catch code
            e.printStackTrace();
            try {
                System.out.println("in finally connection closed");
                conn.close();
                conn = null;

            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        } finally {
            try {
                System.out.println("in finally connection closed");
                conn.close();
                conn = null;

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public Connection getConnection() throws Exception {
        Context ctx = null;
        Connection conn = null;
        ctx = new InitialContext();
        DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/ApplicationDBDS");
        conn = ds.getConnection();
        return conn;
    }


    public void setPrintMode(String printMode) {
        this.printMode = printMode;
    }

    public String getPrintMode() {
        return printMode;
    }
}
