package terms.mtl.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;

import oracle.adf.view.rich.component.rich.input.RichInputText;

import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;

import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.binding.OperationBinding;

public class MainStoreRecptBean {
    private RichInputDate entryDateBinding;
    private RichInputText entryNoBinding;
    private RichInputDate documentationDateBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText poNoBinding;
    private RichInputText itemCodeBinding;
    private RichInputText processCodeBinding;
    private RichInputText uomBinding;
    private RichInputText convRecvQtyBinding;
    private RichInputText issueUomBinding;
    private RichInputText rateBinding;
    private RichPanelHeader myPageRootComponent;
    private RichOutputText bindingOutputText;
    private RichButton headerEditBinding;
    private RichInputText recieptQtyBinding;
    private RichInputText lotNoBinding;
    private RichInputComboboxListOfValues recByBinding;
    private RichButton populateButtonBinding;
    private RichInputComboboxListOfValues docNoBinding;
    private RichSelectOneChoice docTypeBinding;
    private RichInputText rercByNameBinding;

    public MainStoreRecptBean() {
    }

    public void saveAL(ActionEvent actionEvent) {
        OperationBinding op = ADFUtils.findOperation("getRecptNo");
        Object rst = op.execute();
        if (rst.toString() != null && rst.toString() != "") {
            if (op.getErrors().isEmpty()) {
                ADFUtils.findOperation("Commit").execute();
                ADFUtils.showMessage("Main store receipt have been Successfully Saved. " + "Receipt no is: " + rst, 2);
            }
        }

        if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ") {
            if (op.getErrors().isEmpty()) {
                ADFUtils.findOperation("Commit").execute();
                ADFUtils.showMessage("Record Update Successfully.", 2);

            }


        }
    }

    public void PopulateRcrdsAL(ActionEvent actionEvent) {

        //ADFUtils.findOperation("getPopulatedMainStrRecrds").execute();
        
        OperationBinding opp = ADFUtils.findOperation("getPopulatedMainStrRecrds");
        opp.execute();
opp.getParamsMap().put("recQty", recieptQtyBinding.getValue());
        docNoBinding.setDisabled(true);
        docTypeBinding.setDisabled(true);
        recByBinding.setDisabled(true);
        
        
    }


    public void setEntryDateBinding(RichInputDate entryDateBinding) {
        this.entryDateBinding = entryDateBinding;
    }

    public RichInputDate getEntryDateBinding() {
        return entryDateBinding;
    }

    public void setEntryNoBinding(RichInputText entryNoBinding) {
        this.entryNoBinding = entryNoBinding;
    }

    public RichInputText getEntryNoBinding() {
        return entryNoBinding;
    }

    public void setDocumentationDateBinding(RichInputDate documentationDateBinding) {
        this.documentationDateBinding = documentationDateBinding;
    }

    public RichInputDate getDocumentationDateBinding() {
        return documentationDateBinding;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setPoNoBinding(RichInputText poNoBinding) {
        this.poNoBinding = poNoBinding;
    }

    public RichInputText getPoNoBinding() {
        return poNoBinding;
    }

    public void setItemCodeBinding(RichInputText itemCodeBinding) {
        this.itemCodeBinding = itemCodeBinding;
    }

    public RichInputText getItemCodeBinding() {
        return itemCodeBinding;
    }

    public void setProcessCodeBinding(RichInputText processCodeBinding) {
        this.processCodeBinding = processCodeBinding;
    }

    public RichInputText getProcessCodeBinding() {
        return processCodeBinding;
    }

    public void setUomBinding(RichInputText uomBinding) {
        this.uomBinding = uomBinding;
    }

    public RichInputText getUomBinding() {
        return uomBinding;
    }

    public void setConvRecvQtyBinding(RichInputText convRecvQtyBinding) {
        this.convRecvQtyBinding = convRecvQtyBinding;
    }

    public RichInputText getConvRecvQtyBinding() {
        return convRecvQtyBinding;
    }

    public void setIssueUomBinding(RichInputText issueUomBinding) {
        this.issueUomBinding = issueUomBinding;
    }

    public RichInputText getIssueUomBinding() {
        return issueUomBinding;
    }

    public void setRateBinding(RichInputText rateBinding) {
        this.rateBinding = rateBinding;
    }

    public RichInputText getRateBinding() {
        return rateBinding;
    }

    public void setMyPageRootComponent(RichPanelHeader myPageRootComponent) {
        this.myPageRootComponent = myPageRootComponent;
    }

    public RichPanelHeader getMyPageRootComponent() {
        return myPageRootComponent;
    }

    public void setbindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }
    
    public void cevModeDisableComponent(String mode) {
        if (mode.equals("E")) {
            getRercByNameBinding().setDisabled(true);
                getHeaderEditBinding().setDisabled(true);
            getEntryNoBinding().setDisabled(true);
            getEntryDateBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
            getPopulateButtonBinding().setDisabled(true);
            getDocumentationDateBinding().setDisabled(true);
            getItemCodeBinding().setDisabled(true);
            getProcessCodeBinding().setDisabled(true);
            getRecieptQtyBinding().setDisabled(true);
            getUomBinding().setDisabled(true);
            getConvRecvQtyBinding().setDisabled(true);
            getIssueUomBinding().setDisabled(true);
            getRateBinding().setDisabled(true);
            getLotNoBinding().setDisabled(true);
            getPoNoBinding().setDisabled(true);
            recieptQtyBinding.setDisabled(true);
                
        } else if (mode.equals("C")) {
            getUnitCodeBinding().setDisabled(true);
            
            getHeaderEditBinding().setDisabled(true);
            getEntryNoBinding().setDisabled(true);
            getEntryDateBinding().setDisabled(true);
            getDocumentationDateBinding().setDisabled(true);
            getItemCodeBinding().setDisabled(true);
            getProcessCodeBinding().setDisabled(true);
           // getRecieptQtyBinding().setDisabled(true);
            getUomBinding().setDisabled(true);
            getConvRecvQtyBinding().setDisabled(true);
            getIssueUomBinding().setDisabled(true);
            getRateBinding().setDisabled(true);
            getLotNoBinding().setDisabled(true);
            getPoNoBinding().setDisabled(true);
            getRercByNameBinding().setDisabled(true);
            recieptQtyBinding.setDisabled(true);
            
        } else if (mode.equals("V")) {
            
        }
    }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setRecieptQtyBinding(RichInputText recieptQtyBinding) {
        this.recieptQtyBinding = recieptQtyBinding;
    }

    public RichInputText getRecieptQtyBinding() {
        return recieptQtyBinding;
    }

    public void setLotNoBinding(RichInputText lotNoBinding) {
        this.lotNoBinding = lotNoBinding;
    }

    public RichInputText getLotNoBinding() {
        return lotNoBinding;
    }

    public void setRecByBinding(RichInputComboboxListOfValues recByBinding) {
        this.recByBinding = recByBinding;
    }

    public RichInputComboboxListOfValues getRecByBinding() {
        return recByBinding;
    }

    public void setPopulateButtonBinding(RichButton populateButtonBinding) {
        this.populateButtonBinding = populateButtonBinding;
    }

    public RichButton getPopulateButtonBinding() {
        return populateButtonBinding;
    }

    public void setDocNoBinding(RichInputComboboxListOfValues docNoBinding) {
        this.docNoBinding = docNoBinding;
    }

    public RichInputComboboxListOfValues getDocNoBinding() {
        return docNoBinding;
    }

    public void setDocTypeBinding(RichSelectOneChoice docTypeBinding) {
        this.docTypeBinding = docTypeBinding;
    }

    public RichSelectOneChoice getDocTypeBinding() {
        return docTypeBinding;
    }

    public void setRercByNameBinding(RichInputText rercByNameBinding) {
        this.rercByNameBinding = rercByNameBinding;
    }

    public RichInputText getRercByNameBinding() {
        return rercByNameBinding;
    }
}
