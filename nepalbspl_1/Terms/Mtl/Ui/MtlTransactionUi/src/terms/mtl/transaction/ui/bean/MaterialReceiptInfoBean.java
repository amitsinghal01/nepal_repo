package terms.mtl.transaction.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class MaterialReceiptInfoBean {
    private RichButton headerEditBinding;
    private RichOutputText outputTextBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichTable detailTableBinding;
    private RichShowDetailItem detailTabBinding;
    private RichShowDetailItem headerTabBinding;
    private RichInputComboboxListOfValues approvedByBinding;
    private RichInputText entryNoBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputDate entryDateBinding;
    private RichInputComboboxListOfValues preparedByBinding;
    private RichButton createDetailBinding;
    private RichButton deleteDetailBinding;
    private RichInputDate approvedDateBinding;

    public MaterialReceiptInfoBean() {
    }

    public void saveAL(ActionEvent actionEvent) 
    {
        String empcd=(String)ADFUtils.resolveExpression("#{pageFlowScope.empCode}") ; 
        System.out.println("empcd"+empcd);
        ADFUtils.setColumnValue("MaterialReceiptInfoHeaderVO1Iterator", "LastUpdatedBy", empcd);
        
        if ((Long) ADFUtils.evaluateEL("#{bindings.MaterialReceiptInfoDetailVO1Iterator.estimatedRowCount}")>0)
        {              
            OperationBinding op = ADFUtils.findOperation("generateMaterialReceiptNo");
            op.execute();
            System.out.println("Result is===> "+op.getResult());
            if(op.getResult() != null && op.getResult().toString().equalsIgnoreCase("E"))
            {
                ADFUtils.findOperation("Commit").execute();
                ADFUtils.showMessage(" Record Updated Successfully", 2);
                
            }
            else if (op.getResult() != null && op.getResult().toString() != ""  && op.getResult().equals("S"))
            {
                System.out.println("+++++ in the save mode+++++++++++");
                ADFUtils.findOperation("Commit").execute();
                ADFUtils.showMessage("Record Saved Successfully.Material Receipt Info No.is "+ADFUtils.evaluateEL("##{bindings.MriNo.inputValue}"), 2);
                
            }
            else if(op.getResult() != null && op.getResult().toString().equalsIgnoreCase("N"))
            {
                ADFUtils.showMessage("MRI No. could not be generated. Try Again !!", 0);
            }
        }
        else{
            ADFUtils.showMessage("Please Enter any record in the Material Receipt Detail tab.", 0);
        }
    }

    public void editButtonAL(ActionEvent actionEvent) {
            cevmodecheck();
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setOutputTextBinding(RichOutputText outputTextBinding) {
        this.outputTextBinding = outputTextBinding;
    }

    public RichOutputText getOutputTextBinding() {
        cevmodecheck();
        return outputTextBinding;
    }

    public void createDetailAL(ActionEvent actionEvent) {
           ADFUtils.findOperation("CreateInsert").execute();
           ADFUtils.findOperation("setMaterialReceiptNo").execute();
    }

    public void deleteDialogListenerDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete").execute();
            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(detailTableBinding);
    }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }

    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }

    public void cevModeDisableComponent(String mode) {
        if (mode.equals("E")) 
        {
            getApprovedDateBinding().setDisabled(true);
            getPreparedByBinding().setDisabled(true);
            getEntryDateBinding().setDisabled(true);
            getEntryNoBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
            //getHeaderEditBinding().setDisabled(true);

        } else if(mode.equals("C")) {
        
            getApprovedDateBinding().setDisabled(true);
            getApprovedByBinding().setDisabled(true);
            //getEntryDateBinding().setDisabled(true);
            getEntryNoBinding().setDisabled(true);
           getUnitCodeBinding().setDisabled(true);
            //getHeaderEditBinding().setDisabled(true);
        } else if (mode.equals("V")) {
            
            getHeaderTabBinding().setDisabled(false);
            getDetailTabBinding().setDisabled(false);
//            getCreateDetailBinding().setDisabled(true);
//            getDeleteDetailBinding().setDisabled(true);
        }
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setDetailTableBinding(RichTable detailTableBinding) {
        this.detailTableBinding = detailTableBinding;
    }

    public RichTable getDetailTableBinding() {
        return detailTableBinding;
    }


    public void setDetailTabBinding(RichShowDetailItem detailTabBinding) {
        this.detailTabBinding = detailTabBinding;
    }

    public RichShowDetailItem getDetailTabBinding() {
        return detailTabBinding;
    }

    public void setHeaderTabBinding(RichShowDetailItem headerTabBinding) {
        this.headerTabBinding = headerTabBinding;
    }

    public RichShowDetailItem getHeaderTabBinding() {
        return headerTabBinding;
    }

    public void setApprovedByBinding(RichInputComboboxListOfValues approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputComboboxListOfValues getApprovedByBinding() {
        return approvedByBinding;
    }

    public String saveandCloseAL() {
        
        String empcd=(String)ADFUtils.resolveExpression("#{pageFlowScope.empCode}") ; 
        System.out.println("empcd"+empcd);
        ADFUtils.setColumnValue("MaterialReceiptInfoHeaderVO1Iterator", "LastUpdatedBy", empcd);
        
        if ((Long) ADFUtils.evaluateEL("#{bindings.MaterialReceiptInfoDetailVO1Iterator.estimatedRowCount}")>0)
        {              
            OperationBinding op = ADFUtils.findOperation("generateMaterialReceiptNo");
            op.execute();
            System.out.println("Result is===> "+op.getResult());
            if(op.getResult() != null && op.getResult().toString().equalsIgnoreCase("E"))
            {
                ADFUtils.findOperation("Commit").execute();
                ADFUtils.showMessage(" Record Updated Successfully", 2);
                return "saveAndClose";
            }
            else if (op.getResult() != null && op.getResult().toString() != ""  && op.getResult().equals("S"))
            {
                System.out.println("+++++ in the save mode+++++++++++");
                ADFUtils.findOperation("Commit").execute();
                ADFUtils.showMessage("Record Saved Successfully.Material Receipt Info No.is "+ADFUtils.evaluateEL("##{bindings.MriNo.inputValue}"), 2);
                return "saveAndClose";
            }
            else if(op.getResult() != null && op.getResult().toString().equalsIgnoreCase("N"))
            {
                ADFUtils.showMessage("MRI No. could not be generated. Try Again !!", 0);
                return null;
            }
        }
        else{
            ADFUtils.showMessage("Please Enter any record in the Material Receipt Detail tab.", 0);
            return null;
        }
        return null;
    }

    public void setEntryNoBinding(RichInputText entryNoBinding) {
        this.entryNoBinding = entryNoBinding;
    }

    public RichInputText getEntryNoBinding() {
        return entryNoBinding;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setEntryDateBinding(RichInputDate entryDateBinding) {
        this.entryDateBinding = entryDateBinding;
    }

    public RichInputDate getEntryDateBinding() {
        return entryDateBinding;
    }

    public void setPreparedByBinding(RichInputComboboxListOfValues preparedByBinding) {
        this.preparedByBinding = preparedByBinding;
    }

    public RichInputComboboxListOfValues getPreparedByBinding() {
        return preparedByBinding;
    }

    public void setCreateDetailBinding(RichButton createDetailBinding) {
        this.createDetailBinding = createDetailBinding;
    }

    public RichButton getCreateDetailBinding() {
        return createDetailBinding;
    }

    public void setDeleteDetailBinding(RichButton deleteDetailBinding) {
        this.deleteDetailBinding = deleteDetailBinding;
    }

    public RichButton getDeleteDetailBinding() {
        return deleteDetailBinding;
    }

    public void setApprovedDateBinding(RichInputDate approvedDateBinding) {
        this.approvedDateBinding = approvedDateBinding;
    }

    public RichInputDate getApprovedDateBinding() {
        return approvedDateBinding;
    }
}
