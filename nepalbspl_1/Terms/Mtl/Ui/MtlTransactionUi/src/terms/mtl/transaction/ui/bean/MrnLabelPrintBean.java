package terms.mtl.transaction.ui.bean;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.naming.Context;
import javax.naming.InitialContext;

import javax.servlet.http.HttpServletResponse;

import javax.sql.DataSource;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCIteratorBinding;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

public class MrnLabelPrintBean {
    public MrnLabelPrintBean() {
    }

    public void mrnLabelPrintingAL(ActionEvent actionEvent) {
        // Add event code here...
    }

    public BindingContainer getBindings() {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
    }

    public void downloadJrxml(FacesContext facesContext, OutputStream outputStream) {
        Connection conn = null;
        try {

            OperationBinding binding = ADFUtils.findOperation("fileNameForPrint");
            binding.getParamsMap().put("fileId", "ERP0000001909");
            binding.execute();

            System.out.println("Binding Result :" + binding.getResult().toString());
            if (binding.getResult() != null) {
                InputStream input = new FileInputStream(binding.getResult().toString());
                String srvNo = null;
                DCIteratorBinding poIter = (DCIteratorBinding) getBindings().get("MrnLabelPrintVO1Iterator");
                if (poIter.getCurrentRow().getAttribute("SrvNo") != null) {
                    srvNo = poIter.getCurrentRow().getAttribute("SrvNo").toString();
                }
                String unitCode = poIter.getCurrentRow().getAttribute("UnitCd").toString();
                java.util.Date fromDt = (java.util.Date) poIter.getCurrentRow().getAttribute("FromDate");
                java.util.Date toDt = (java.util.Date) poIter.getCurrentRow().getAttribute("ToDAte");
                System.out.println("srvNo :- " + srvNo + " Unit code" + unitCode + "From date" + fromDt +
                                   "To Date==>>" + toDt);
                String LV_SID = oracle.jbo.server.uniqueid.UniqueIdHelper.getNextId().toString();
                System.out.println("SID======>" + LV_SID);
                OperationBinding binding1 = ADFUtils.findOperation("MrnLabelPrintCallingProcedure");
                binding1.getParamsMap().put("GET_UNIT", unitCode);
                binding1.getParamsMap().put("GET_SESSION", LV_SID);
                binding1.getParamsMap().put("GET_FRDT", fromDt);
                binding1.getParamsMap().put("GET_TODT", toDt);
                binding1.execute();
                Map n = new HashMap();
                n.put("p_unit", unitCode);
                n.put("p_srvno", srvNo);
                n.put("p_fr_dt", fromDt);
                n.put("p_to_dt", toDt);

                conn = getConnection();

                JasperReport design = (JasperReport) JRLoader.loadObject(input);
                System.out.println("Path : " + input + " -------" + design + " param:" + n);

                @SuppressWarnings("unchecked")
                net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                byte pdf[] = JasperExportManager.exportReportToPdf(print);
                HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                response.getOutputStream().write(pdf);
                response.getOutputStream().flush();
                response.getOutputStream().close();
                facesContext.responseComplete();
            } else
                System.out.println("File Name/File Path not found.");
        } catch (FileNotFoundException fnfe) {
            // TODO: Add catch code
            fnfe.printStackTrace();
            try {
                System.out.println("in finally connection closed");
                conn.close();
                conn = null;

            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            // TODO: Add catch code
            e.printStackTrace();
            try {
                System.out.println("in finally connection closed");
                conn.close();
                conn = null;

            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        } finally {
            try {
                System.out.println("in finally connection closed");
                conn.close();
                conn = null;

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public Connection getConnection() throws Exception {
        Context ctx = null;
        Connection conn = null;
        ctx = new InitialContext();
        DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/ApplicationDBDS");
        conn = ds.getConnection();
        return conn;
    }


}
