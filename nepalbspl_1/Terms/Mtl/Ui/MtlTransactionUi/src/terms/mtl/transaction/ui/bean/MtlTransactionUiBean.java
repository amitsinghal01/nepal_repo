package terms.mtl.transaction.ui.bean;


import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.validator.ValidatorException;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichDialog;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectBooleanCheckbox;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.nav.RichCommandLink;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.event.ReturnPopupEvent;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.adf.view.rich.util.ResetUtils;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;
import oracle.jbo.domain.Timestamp;

import org.apache.myfaces.trinidad.event.SelectionEvent;
import org.apache.myfaces.trinidad.model.UploadedFile;


public class MtlTransactionUiBean {

    private RichSelectOneChoice joPoLOVBind;
    private RichSelectOneChoice poTypeBind;
    private RichSelectOneChoice poType2Bind;
    private RichInputText venAdd1Bind;
    private RichInputText venAdd2Bind;
    private RichInputText venAdd3Bind;
    private RichInputText setFocusWhenEmptyMandatory;
    private RichPopup annexurePopUp;
    private RichPopup vendorAddressPopup;
    private RichTable poDetailTableBind;
    private RichInputListOfValues searchItemBind;
    private RichInputText bindApprdQty;
    private RichInputText indQtyBinding;
    private RichTable createPurchaseIndentTableBinding;
    private RichOutputText outputTextBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichInputText indentNoBinding;
    private RichInputDate indentDateBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText unitDescriptionBinding;
    private RichSelectOneChoice indentStatusBinding;
    private RichInputComboboxListOfValues jwVendorBinding;
    private RichInputText locationNameBinding;
    private RichInputComboboxListOfValues indentByBinding;
    private RichInputText indentByNameBinding;
    private RichInputDate hodApprovalDateBinding;
    private RichInputComboboxListOfValues authorisedByBinding;
    private RichInputText authorisedByNameBinding;
    private RichInputDate approvalDateBinding;
    private RichInputText itemDescriptionBinding;
    private RichInputText processDescriptionBinding;
    private RichInputText stockBinding;
    private RichInputText finalIndBinding;
    private RichInputText rsdInPoBinding;
    private RichInputText storeRecdBinding;
    private RichInputText valueBinding;
    private RichButton headerEditBinding;
    private RichButton detailCreateBinding;
    private RichButton detailDeleteBinding;
    private RichSelectBooleanCheckbox detailStatusBinding;
    private RichInputComboboxListOfValues preparedByBinding;
    String IndentStatus="";
    private RichSelectOneChoice indentForBinding;
    private RichSelectOneChoice indentTypeBinding;
    private RichInputComboboxListOfValues locationBinding;
    private RichPopup indentCancelPopup;
    private RichDialog indentCancelDialog;
    private RichInputComboboxListOfValues prcsCdBinding;
    private RichPopup dtlPopupBinding;
    private RichColumn dstatusBinding;
    private RichInputText pendQtyBinding;
    private RichInputText indQty1Binding;
    private RichInputDate cancelDtBinding;
    private RichInputComboboxListOfValues cancelByBinding;
    private String itemvalue;
    private String itemName;
    private RichInputComboboxListOfValues itemBinding;
    private RichInputComboboxListOfValues itemCodeBinding;
    private RichInputText itemIndtlBinding;
    private RichInputText itemInDtlBind;
    private RichPopup confirmationPopupBinding;
    private RichInputText amdNoBinding;
    private RichColumn totalConsBinding;
    private RichInputText reqQtyBinding;
    private RichInputText totConsBinding;
    private RichInputText poNoBinding;
    private RichInputDate poDateBinding;
    private RichInputText tagNoBinding;
    private RichColumn procSeqBinding;
    private RichInputText proSeqBinding;
    private RichButton cancelBtnBinding;
    private RichInputDate amdDateBinding;
    private RichInputText rateBinding;
    private RichInputText itemSpecification;
    private RichTable docTableBinding;
    private RichInputText unitBinding;
    private RichInputText seqNoBinding;
    private RichInputText docNoBinding;
    private RichInputText typeBinding;
    private RichInputText fileNameBinding;
    private RichInputDate docDateBinding;
    private RichInputText contenetTypeBinding;
    private RichInputText pathBinding;
    private RichShowDetailItem docTabbinding;
    private RichCommandLink linkBinding;
    private RichInputComboboxListOfValues supplierBinding;

    public MtlTransactionUiBean() {
        super();
    }

    public void SaveIndentAl(ActionEvent actionEvent) {
        String empcd=(String)ADFUtils.resolveExpression("#{pageFlowScope.empCode}") ; 
        System.out.println("empcd"+empcd);
        ADFUtils.setColumnValue("PurchaseIndentHeaderVO1Iterator", "LastUpdatedBy", empcd);
        System.out.println("The value of unit code...........>>>>" +unitCodeBinding.getValue().toString());
//        if(unitCodeBinding.getValue().toString()!=null && !unitCodeBinding.getValue().toString().isEmpty()){
            System.out.println("indent no is===>>"+indentNoBinding.getValue());
//            if(authorisedByBinding.getValue()==null){
                    System.out.println("indentTypeBinding:"+indentTypeBinding.getValue()+" supplier Code:"+supplierBinding.getValue());
            String indentType = indentTypeBinding.getValue().toString();
            if((indentType.equalsIgnoreCase("J") || indentType.equalsIgnoreCase("R") || indentType.equalsIgnoreCase("T"))&& supplierBinding.getValue()==null){
                
                ADFUtils.showMessage("Supplier must have some value.", 0);
        
            }else{
        OperationBinding op = ADFUtils.findOperation("getIndentNo");
        Object rst = op.execute();
        
        //ADFUtils.findOperation("goForUnitRate").execute();
        
        
        
        System.out.println("--------Commit-------");
            System.out.println("value aftr getting result--->?"+rst);
            
        
            if (rst.toString() != null && rst.toString() != "") {
                if (op.getErrors().isEmpty()) {
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message = new FacesMessage("Record Saved Successfully. New Indent No. is "+rst+".");   
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                    FacesContext fc = FacesContext.getCurrentInstance();   
                    fc.addMessage(null, Message);  
                    ADFUtils.setEL("#{pageFlowScope.mode}", "V");
                    cevmodecheck();
                    AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
        
                }
            }
            
            if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ") {
                if (op.getErrors().isEmpty()) 
              {
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message = new FacesMessage("Record Update Successfully.");   
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                    FacesContext fc = FacesContext.getCurrentInstance();   
                    fc.addMessage(null, Message);  
                    ADFUtils.setEL("#{pageFlowScope.mode}", "V");
                    cevmodecheck();
                    AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
            
                }
//            }
            
            
//        else{                
//                    FacesMessage Message = new FacesMessage("Record can't be saved as Unit Code is null.");   
//                    Message.setSeverity(FacesMessage.SEVERITY_INFO);   
//                    FacesContext fc = FacesContext.getCurrentInstance();   
//                    fc.addMessage(null, Message);
//            
            }           
//        }
//            else{
//                System.out.println("Inside else====>>>"+indentNoBinding.getValue());
//                OperationBinding op1=ADFUtils.findOperation("getIndentNo");
//                Object ob=op1.execute();
//                System.out.println("--------Commit-------");
//                    System.out.println("value aftr getting result--->?"+op1.getResult().toString());
//                    
//                
////                    if (ob.toString() != null && ob.toString() != "") {
//                        if (op1.getResult().toString()!=null) {
//                            ADFUtils.findOperation("Commit").execute();
//                            FacesMessage Message = new FacesMessage("Record Saved Successfully. New Indent No. is "+ob+".");   
//                            Message.setSeverity(FacesMessage.SEVERITY_INFO);   
//                            FacesContext fc = FacesContext.getCurrentInstance();   
//                            fc.addMessage(null, Message);  
//                            ADFUtils.setEL("#{pageFlowScope.mode}", "V");
//                            cevmodecheck();
//                            AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
//                
//                        }
                    
//           }         
//                    if (ob.toString() == null || ob.toString() == "" || ob.toString() == " ") {
//                        if (op1.getErrors().isEmpty()) 
//                      {
//                            ADFUtils.findOperation("Commit").execute();
//                            FacesMessage Message = new FacesMessage("Record Update Successfully.");   
//                            Message.setSeverity(FacesMessage.SEVERITY_INFO);   
//                            FacesContext fc = FacesContext.getCurrentInstance();   
//                            fc.addMessage(null, Message);  
//                            ADFUtils.setEL("#{pageFlowScope.mode}", "V");
//                            cevmodecheck();
//                            AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
//                    
//                        }
//                
//            }
//        }
//        } 
            }
    }


    public void validteUnitNm(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if(object!=null){
            
                ADFUtils.findOperation("goForUnitRate").execute();    
            
            }
    
    }

    public void ItemVCE(ValueChangeEvent valueChangeEvent) {
       

      
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        ADFUtils.findOperation("goForUnitRate").execute(); 
//        ADFUtils.findOperation("clearRowOnItemVcePurIndent").execute(); 
//        AdfFacesContext.getCurrentInstance().addPartialTarget(createPurchaseIndentTableBinding);
        
       
    }

    public void ProcessVCE(ValueChangeEvent vce) {
        if(vce!=null){
            
           String prc_desc = vce.getNewValue().toString(); 
           System.out.println("what is process desc--->"+prc_desc);
                OperationBinding op = ADFUtils.findOperation("goForUnitRate");
                op.getParamsMap().put("desc", prc_desc);
                Object rst = op.execute(); 
            
            
            
            }
    }

    public void IndQtyVCE(ValueChangeEvent vce) {
        if(vce!=null){
       // bindApprdQty.setValue(indQtyBinding.getValue());
     AdfFacesContext.getCurrentInstance().addPartialTarget(createPurchaseIndentTableBinding);
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        ADFUtils.findOperation("goForindntQty").execute(); 
          //  AdfFacesContext.getCurrentInstance().addPartialTarget(createPurchaseIndentTableBinding);
        }  
    }

    
    public void PoTypeVL(FacesContext facesContext, UIComponent uIComponent, Object object) {
                        if(object !=null && joPoLOVBind.getValue()!=null){
                            String PoType = object.toString();
                            String JoPoType = joPoLOVBind.getValue().toString();
                            System.out.println("values "+joPoLOVBind.getValue().toString()+","+object.toString());
                            if(JoPoType.equalsIgnoreCase("J") && !PoType.equals("JW") && !PoType.equals("RP") && !PoType.equals("JT")){
                                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Only JobWork / Report & Maintenance Type is Allowed for JobWork Order", null));
                            }
            }


    }

    public void JoPoTypeValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {

    }


    public void setJoPoLOVBind(RichSelectOneChoice joPoLOVBind) {
        this.joPoLOVBind = joPoLOVBind;
    }

    public RichSelectOneChoice getJoPoLOVBind() {
        return joPoLOVBind;
    }

    public void PoType2Validator(FacesContext facesContext, UIComponent uIComponent, Object object) {
                if(object !=null && poTypeBind.getValue()!=null){
                    String PoType = poTypeBind.getValue().toString();
                    String PoType2 = object.toString();
                    System.out.println("values "+poTypeBind.getValue().toString()+","+object.toString());
                    if(!PoType2.equalsIgnoreCase("C") && (PoType.equals("IN") || PoType.equals("RP") || PoType.equals("CA"))){
                        throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Only Closed PO can be made against Indent/Capital PO", null));
                    }
                }

    }

    public void setPoTypeBind(RichSelectOneChoice poTypeBind) {
        this.poTypeBind = poTypeBind;
    }

    public RichSelectOneChoice getPoTypeBind() {
        return poTypeBind;
    }

    public void setPoType2Bind(RichSelectOneChoice poType2Bind) {
        this.poType2Bind = poType2Bind;
    }

    public RichSelectOneChoice getPoType2Bind() {
        return poType2Bind;
    }

    public void SaveAL(ActionEvent actionEvent) {
       
    }

    public void PayTermAL(ActionEvent actionEvent) {
        System.out.println("after popup");
        ADFUtils.findOperation("ApplyPaymentTermsVC").execute();
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            ADFUtils.findOperation("CreateInsert").execute();
        }
    }

    public void PayTermDialogListener(DialogEvent de) {
      
        if (de.getOutcome() == DialogEvent.Outcome.ok) {
            ADFUtils.findOperation("InsertPoNoInPayTerm").execute();
        }
    }

    public void VendorNameVCL(ValueChangeEvent vce) {
        if (vce.getNewValue() != null) {
            OperationBinding ob = ADFUtils.findOperation("ApplyVendorAddressesVC");
            ob.getParamsMap().put("VenCd", vce.getNewValue());
        }
    }


    public void AnnexureAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("ApplyAnnexureVC").execute();
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {

            ADFUtils.findOperation("CreateInsert1").execute();
        }
    }

    public void AnnexureDialogListener(DialogEvent de) {
    
        if (de.getOutcome() == DialogEvent.Outcome.ok) {
            ADFUtils.findOperation("InsertPoNoInAnnexure").execute();
        }
    }

    public void setVenAdd1Bind(RichInputText venAdd1Bind) {
        this.venAdd1Bind = venAdd1Bind;
    }

    public RichInputText getVenAdd1Bind() {
        return venAdd1Bind;
    }

    public void setVenAdd2Bind(RichInputText venAdd2Bind) {
        this.venAdd2Bind = venAdd2Bind;
    }

    public RichInputText getVenAdd2Bind() {
        return venAdd2Bind;
    }

    public void setVenAdd3Bind(RichInputText venAdd3Bind) {
        this.venAdd3Bind = venAdd3Bind;
    }

    public RichInputText getVenAdd3Bind() {
        return venAdd3Bind;
    }

    public void setSetFocusWhenEmptyMandatory(RichInputText setFocusWhenEmptyMandatory) {
        this.setFocusWhenEmptyMandatory = setFocusWhenEmptyMandatory;
    }

    public RichInputText getSetFocusWhenEmptyMandatory() {
        return setFocusWhenEmptyMandatory;
    }

    public void PoNoVCL(ValueChangeEvent valueChangeEvent) {
      
    }


    public void SaveActionListener(ActionEvent actionEvent) {
      
        
        
        ADFUtils.showMessage("Purchase Order have been Successfully Saved", 2);
    }

    public void ItemDetailsAL(ActionEvent actionEvent) {
//        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C"))
//            ADFUtils.findOperation("CreateInsertDetail").execute();
    }

    public void VendorReturn(ReturnPopupEvent returnPopupEvent) {
                RichPopup.PopupHints hints = new RichPopup.PopupHints();
                          getVendorAddressPopup().show(hints);

    }

    public void setAnnexurePopUp(RichPopup annexurePopUp) {
        this.annexurePopUp = annexurePopUp;
    }

    public RichPopup getAnnexurePopUp() {
        return annexurePopUp;
    }

    public void setVendorAddressPopup(RichPopup vendorAddressPopup) {
        this.vendorAddressPopup = vendorAddressPopup;
    }

    public RichPopup getVendorAddressPopup() {
        return vendorAddressPopup;
    }

    public void VendorAddressSL(SelectionEvent se) {
        ADFUtils.invokeEL("#{bindings.PurchaseOrderHeaderVO1.collectionModel.makeCurrent}", new Class[] {SelectionEvent.class},
                                 new Object[] { se });
        Row selectedRow =
                    (Row)ADFUtils.evaluateEL("#{bindings.PurchaseOrderHeaderVO1Iterator.currentRow}"); 
//        OperationBinding ob=ADFUtils.findOperation("filterselectedAddress");
        System.out.println("selected address is "+selectedRow.getAttribute("Address1"));
//        ob.getParamsMap().put("SelectedAddress", selectedRow.getAttribute("Address1"));
//       RichPopup.PopupHints hints = new RichPopup.PopupHints();
       
                        getVendorAddressPopup().cancel();
       
    }

    public void HandleVendorTableDblClick(ClientEvent clientEvent) {

        
        getVendorAddressPopup().cancel();
    }

    public void PODetailDL(DialogEvent de) {
        if(de.getOutcome()==DialogEvent.Outcome.ok){
            ADFUtils.findOperation("CalculatePoDetail").execute();
        }

    }

    public void PopulatePOAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("PopulatePoDetail").execute();
    }

    public void setPoDetailTableBind(RichTable poDetailTableBind) {
        this.poDetailTableBind = poDetailTableBind;
    }

    public RichTable getPoDetailTableBind() {
        return poDetailTableBind;
    }

    public void PoDetailSL(SelectionEvent selectionEvent) {
        
        ADFUtils.invokeEL("#{bindings.PurchaseOrderDetailVO1.collectionModel.makeCurrent}", new Class[] {SelectionEvent.class},
                                 new Object[] { selectionEvent });
        Row selectedRow = (Row) ADFUtils.evaluateEL("#{bindings.PurchaseOrderDetailVO1Iterator.currentRow}");
        System.out.println("selected row "+selectedRow.getAttribute("PiosItemDesc"));
        
    }

    public void setSearchItemBind(RichInputListOfValues searchItemBind) {
        this.searchItemBind = searchItemBind;
    }

    public RichInputListOfValues getSearchItemBind() {
        return searchItemBind;
    }

    public void ItemSearchAL(ActionEvent actionEvent) {
    
    }

    public void IndentNoValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if(object == null && (getPoTypeBind().equals("IN") || getPoTypeBind().equals("CA"))){
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Indent No. is must for Capital/Indent PO", null));
        }

    }


    public void setBindApprdQty(RichInputText bindApprdQty) {
        this.bindApprdQty = bindApprdQty;
    }

    public RichInputText getBindApprdQty() {
        return bindApprdQty;
    }

    public void setIndQtyBinding(RichInputText indQtyBinding) {
        this.indQtyBinding = indQtyBinding;
    }

    public RichInputText getIndQtyBinding() {
        return indQtyBinding;
    }

    public void setCreatePurchaseIndentTableBinding(RichTable createPurchaseIndentTableBinding) {
        this.createPurchaseIndentTableBinding = createPurchaseIndentTableBinding;
    }

    public RichTable getCreatePurchaseIndentTableBinding() {
        return createPurchaseIndentTableBinding;
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                    {
                    ADFUtils.findOperation("Delete").execute();
                    System.out.println("Record Delete Successfully");
                   
                        FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                        FacesContext fc = FacesContext.getCurrentInstance();  
                        fc.addMessage(null, Message);  
                   
                    }
                AdfFacesContext.getCurrentInstance().addPartialTarget(createPurchaseIndentTableBinding);
    }

    public void IndentCancelVCE(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
        if(valueChangeEvent!=null){
            valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
            System.out.println("goForIndentCancellation");
            ADFUtils.findOperation("goForIndentCancellation").execute(); 
        }  
    }

    public void setOutputTextBinding(RichOutputText outputTextBinding) {
        this.outputTextBinding = outputTextBinding;
    }

    public RichOutputText getOutputTextBinding() {
        cevmodecheck();
        return outputTextBinding;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setIndentNoBinding(RichInputText indentNoBinding) {
        this.indentNoBinding = indentNoBinding;
    }

    public RichInputText getIndentNoBinding() {
        return indentNoBinding;
    }

    public void setIndentDateBinding(RichInputDate indentDateBinding) {
        this.indentDateBinding = indentDateBinding;
    }

    public RichInputDate getIndentDateBinding() {
        return indentDateBinding;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setUnitDescriptionBinding(RichInputText unitDescriptionBinding) {
        this.unitDescriptionBinding = unitDescriptionBinding;
    }

    public RichInputText getUnitDescriptionBinding() {
        return unitDescriptionBinding;
    }

    public void setIndentStatusBinding(RichSelectOneChoice indentStatusBinding) {
        this.indentStatusBinding = indentStatusBinding;
    }

    public RichSelectOneChoice getIndentStatusBinding() {
        return indentStatusBinding;
    }

    public void setJwVendorBinding(RichInputComboboxListOfValues jwVendorBinding) {
        this.jwVendorBinding = jwVendorBinding;
    }

    public RichInputComboboxListOfValues getJwVendorBinding() {
        return jwVendorBinding;
    }


    public void setLocationNameBinding(RichInputText locationNameBinding) {
        this.locationNameBinding = locationNameBinding;
    }

    public RichInputText getLocationNameBinding() {
        return locationNameBinding;
    }

    public void setIndentByBinding(RichInputComboboxListOfValues indentByBinding) {
        this.indentByBinding = indentByBinding;
    }

    public RichInputComboboxListOfValues getIndentByBinding() {
        return indentByBinding;
    }

    public void setIndentByNameBinding(RichInputText indentByNameBinding) {
        this.indentByNameBinding = indentByNameBinding;
    }

    public RichInputText getIndentByNameBinding() {
        return indentByNameBinding;
    }

    public void setHodApprovalDateBinding(RichInputDate hodApprovalDateBinding) {
        this.hodApprovalDateBinding = hodApprovalDateBinding;
    }

    public RichInputDate getHodApprovalDateBinding() {
        return hodApprovalDateBinding;
    }

    public void setAuthorisedByBinding(RichInputComboboxListOfValues authorisedByBinding) {
        this.authorisedByBinding = authorisedByBinding;
    }

    public RichInputComboboxListOfValues getAuthorisedByBinding() {
        return authorisedByBinding;
    }

    public void setAuthorisedByNameBinding(RichInputText authorisedByNameBinding) {
        this.authorisedByNameBinding = authorisedByNameBinding;
    }

    public RichInputText getAuthorisedByNameBinding() {
        return authorisedByNameBinding;
    }

    public void setApprovalDateBinding(RichInputDate approvalDateBinding) {
        this.approvalDateBinding = approvalDateBinding;
    }

    public RichInputDate getApprovalDateBinding() {
        return approvalDateBinding;
    }

    public void setItemDescriptionBinding(RichInputText itemDescriptionBinding) {
        this.itemDescriptionBinding = itemDescriptionBinding;
    }

    public RichInputText getItemDescriptionBinding() {
        return itemDescriptionBinding;
    }

    public void setProcessDescriptionBinding(RichInputText processDescriptionBinding) {
        this.processDescriptionBinding = processDescriptionBinding;
    }

    public RichInputText getProcessDescriptionBinding() {
        return processDescriptionBinding;
    }

    public void setStockBinding(RichInputText stockBinding) {
        this.stockBinding = stockBinding;
    }

    public RichInputText getStockBinding() {
        return stockBinding;
    }

    public void setFinalIndBinding(RichInputText finalIndBinding) {
        this.finalIndBinding = finalIndBinding;
    }

    public RichInputText getFinalIndBinding() {
        return finalIndBinding;
    }

    public void setRsdInPoBinding(RichInputText rsdInPoBinding) {
        this.rsdInPoBinding = rsdInPoBinding;
    }

    public RichInputText getRsdInPoBinding() {
        return rsdInPoBinding;
    }

    public void setStoreRecdBinding(RichInputText storeRecdBinding) {
        this.storeRecdBinding = storeRecdBinding;
    }

    public RichInputText getStoreRecdBinding() {
        return storeRecdBinding;
    }

    public void setValueBinding(RichInputText valueBinding) {
        this.valueBinding = valueBinding;
    }

    public RichInputText getValueBinding() {
        return valueBinding;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void editButtonAL(ActionEvent actionEvent)
        {
        
            Boolean flag=true;
            Integer amd =(Integer)getAmdNoBinding().getValue();
            if(indentStatusBinding.getValue().equals("C") )
            {
                ADFUtils.showMessage("Cancel Indent Can Not Be Modified  ", 2);
                ADFUtils.setEL("#{pageFlowScope.mode}", "V");
                flag =false;
            }
//            else if (authorisedByBinding.getValue() !=null && flag==true)
//            {
//                ADFUtils.showMessage("This Is Approved Indent You can not update this record,Only Can Be Cancelled. ", 2);
//                ADFUtils.setEL("#{pageFlowScope.mode}", "V");
//            }
            else if (authorisedByBinding.getValue() != null && authorisedByBinding.getValue()!= "") 
            {
                
                ADFUtils.showMessage("Approved Indent Can Not Be Modified  ", 2);
                ADFUtils.setEL("#{pageFlowScope.mode}", "V");
                flag =false;
//                System.out.println("=========Now Amendment=======++++++");
//                RichPopup.PopupHints hints = new RichPopup.PopupHints();
//                ADFUtils.setEL("#{pageFlowScope.mode}", "V");
//                getConfirmationPopupBinding().show(hints);
                 
            } else if(preparedByBinding.getValue() != null && preparedByBinding.getValue()!= ""){
                
                OperationBinding ob = ADFUtils.findOperation("checkApprovalUnitStatus");
                System.out.println("Inde t type==>"+indentTypeBinding.getValue()+" EMP CD:"+ADFUtils.resolveExpression("#{pageFlowScope.empCode}"));
                String empccd =
                (String) (ADFUtils.resolveExpression("#{pageFlowScope.empCode}") != null ?
                          ADFUtils.resolveExpression("#{pageFlowScope.empCode}") : "SWE161");
                System.out.println("EMPCD:"+empccd);
//                if(indentTypeBinding.getValue().equals("D")){
//                    System.out.println("Indsdie iiiiiiifffffffffff");
                ob.getParamsMap().put("formNm", "IND");
                ob.getParamsMap().put("authoLim", "AP");
                ob.getParamsMap().put("unitcd", unitCodeBinding.getValue());
                //        System.out.println("Total SUM========>"+ADFUtils.resolveExpression("#{bindings.TotalAmtTrans.inputValue}"));
//                ob.getParamsMap().put("Amount", amount);
                ob.getParamsMap().put("empcd", empccd);
                ob.execute();
                System.out.println("OP.RESULT:"+ob.getResult());
                if((ob.getResult() !=null && ob.getResult().equals("Y")))
                {
//                   Row row=(Row)ADFUtils.evaluateEL("#{bindings.PurchaseIndentHeaderVO1Iterator.currentRow}");
//                   row.setAttribute("StoreAppBy", null);
                   ADFUtils.showMessage("You have not privledge to edit this record for selected authentication type.", 2);
                    ADFUtils.setEL("#{pageFlowScope.mode}", "V");
                    flag =false;
                }else{
                    ADFUtils.setEL("#{pageFlowScope.mode}", "E");
                    cevmodecheck();
                }
//                }
//                else{
//                    System.out.println("iNDSEDI ELSE==>>");
//                    ob.getParamsMap().put("formNm", "IND_CAP");
//                    ob.getParamsMap().put("authoLim", "VR");
//                    ob.getParamsMap().put("UnitCd", unitCodeBinding.getValue());
//                    //        System.out.println("Total SUM========>"+ADFUtils.resolveExpression("#{bindings.TotalAmtTrans.inputValue}"));
//                    ob.getParamsMap().put("Amount", amount);
//                    ob.getParamsMap().put("empcd", vce.getNewValue().toString());
//                    ob.execute();
//                    System.out.println(" EMP CD"+vce.getNewValue().toString());
//                    if((ob.getResult() !=null && !ob.getResult().equals("Y")))
//                    {
//                       Row row=(Row)ADFUtils.evaluateEL("#{bindings.PurchaseIndentHeaderVO1Iterator.currentRow}");
//                       row.setAttribute("StoreAppBy", null);
//                       ADFUtils.showMessage(ob.getResult().toString(), 0);
//                    } 
//                }
                
            }
            else{
                System.out.println("insdie else of edit button");
                ADFUtils.setEL("#{pageFlowScope.mode}", "E");
                cevmodecheck();
            }
            
//            if(amd==0)
//            {
//                cevmodecheck();
//                System.out.println("++++++++++++++++yessss editmode++++++="+amd);
                if(preparedByBinding.getValue()==null ){

                    System.out.println("Verified by isssss when null>>>>"+preparedByBinding.getValue());

                    authorisedByBinding.setDisabled(true);
                }
                else{
                    preparedByBinding.setDisabled(true);
                }
//            }
//            else
//            {
////                amdEnableDisable();
//                System.out.println("---------------hello amend --------->>.>"+amd);
//
//            }
//            else
//            {
//            cevmodecheck();
//
//                if(preparedByBinding.getValue()==null ){
//
//                    System.out.println("Verified by isssss when null>>>>"+preparedByBinding.getValue());
//
//                    authorisedByBinding.setDisabled(true);
//                }
//                else{
//                    preparedByBinding.setDisabled(true);
//                }
//            }
        } 

    public void setDetailCreateBinding(RichButton detailCreateBinding) {
        this.detailCreateBinding = detailCreateBinding;
    }

    public RichButton getDetailCreateBinding() {
        return detailCreateBinding;
    }

    public void setDetailDeleteBinding(RichButton detailDeleteBinding) {
        this.detailDeleteBinding = detailDeleteBinding;
    }

    public RichButton getDetailDeleteBinding() {
        return detailDeleteBinding;
    }
    
    private void cevmodecheck(){
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E"))
        {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");

        }
    }
    public void cevModeDisableComponent(String mode) {
    //            FacesContext fctx = FacesContext.getCurrentInstance();
    //            ELContext elctx = fctx.getELContext();
    //            Application jsfApp = fctx.getApplication();
    //            //create a ValueExpression that points to the ADF binding layer
    //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
    //            //
    //            ValueExpression valueExpr = exprFactory.createValueExpression(
    //                                         elctx,
    //                                         "#{pageFlowScope.mode=='E'}",
    //                                          Object.class
    //                                         );
    //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
        if (mode.equals("E")) 
       {
//            if(preparedByBinding.getValue().toString()!=null){
//               getPreparedByBinding().setDisabled(true);
//               System.out.println("inside disable  if of hod");
//                
//            }
            unitBinding.setDisabled(true);
            fileNameBinding.setDisabled(true);
            docDateBinding.setDisabled(true);
            docNoBinding.setDisabled(true);
            typeBinding.setDisabled(true);
            seqNoBinding.setDisabled(true);
            getAmdDateBinding().setDisabled(true);
            getProSeqBinding().setDisabled(true);
            getCancelByBinding().setDisabled(true);
            getCancelDtBinding().setDisabled(true);
            getAmdNoBinding().setDisabled(true);
            getTotConsBinding().setDisabled(true);
            getPoDateBinding().setDisabled(true);
            getPoNoBinding().setDisabled(true);
//            getTagNoBinding().setDisabled(true);
            getIndentStatusBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            getIndentNoBinding().setDisabled(true);
            getIndentDateBinding().setDisabled(true);
            getPendQtyBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
            getUnitDescriptionBinding().setDisabled(true);
            getLocationNameBinding().setDisabled(true);
            getIndentByBinding().setDisabled(true);
            getIndentByNameBinding().setDisabled(true);
            getHodApprovalDateBinding().setDisabled(true);
            getAuthorisedByBinding().setDisabled(false);
            getItemDescriptionBinding().setDisabled(true);
            getStockBinding().setDisabled(true);
         //   getFinalIndBinding().setDisabled(true);
            getRsdInPoBinding().setDisabled(true);
            getStoreRecdBinding().setDisabled(true);
            getValueBinding().setDisabled(true);
            getItemDescriptionBinding().setDisabled(true);
            getProcessDescriptionBinding().setDisabled(true);
            getApprovalDateBinding().setDisabled(true);
            getDetailCreateBinding().setDisabled(false);
         // getDetailDeleteBinding().setDisabled(false);  
             getPreparedByBinding().setDisabled(false);
            getIndentForBinding().setDisabled(true);
            getIndentTypeBinding().setDisabled(true);
            getLocationBinding().setDisabled(true);
            getItemSpecification().setDisabled(true);
            if(indentTypeBinding.getValue().equals("D") && indentForBinding.getValue().equals("P")){
                
                jwVendorBinding.setDisabled(true);
            }
            else{
                
                jwVendorBinding.setDisabled(false);
                
            }
           

        } else if (mode.equals("C")) {
            
            unitBinding.setDisabled(true);
            fileNameBinding.setDisabled(true);
            docDateBinding.setDisabled(true);
            docNoBinding.setDisabled(true);
            typeBinding.setDisabled(true);
            seqNoBinding.setDisabled(true);
            getAmdDateBinding().setDisabled(true);
            getCancelBtnBinding().setDisabled(true);
            getProSeqBinding().setDisabled(true);
            getAmdNoBinding().setDisabled(true);
            getTotConsBinding().setDisabled(true);
            getPoDateBinding().setDisabled(true);
            getPoNoBinding().setDisabled(true);
//            getTagNoBinding().setDisabled(true);
            getCancelByBinding().setDisabled(true);
            getCancelDtBinding().setDisabled(true);
            getPendQtyBinding().setDisabled(true);
            getIndentByBinding().setDisabled(true);
            getIndentStatusBinding().setDisabled(true);
         getUnitCodeBinding().setDisabled(true);
            getUnitDescriptionBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            getIndentNoBinding().setDisabled(true);
            getIndentDateBinding().setDisabled(true);
            getUnitDescriptionBinding().setDisabled(true);
            getLocationNameBinding().setDisabled(true);
            getIndentByNameBinding().setDisabled(true);

            getHodApprovalDateBinding().setDisabled(true);
            getAuthorisedByBinding().setDisabled(true);
            getItemDescriptionBinding().setDisabled(true);
            getStockBinding().setDisabled(true);
        //    getFinalIndBinding().setDisabled(true);
            getRsdInPoBinding().setDisabled(true);
            getStoreRecdBinding().setDisabled(true);
            getValueBinding().setDisabled(true);
            getApprovalDateBinding().setDisabled(true);
            getDetailCreateBinding().setDisabled(false);
            getDetailStatusBinding().setDisabled(true);
            
            getPreparedByBinding().setDisabled(true);
            getAuthorisedByBinding().setDisabled(true);
            getIndentByBinding().setDisabled(true);
            getItemSpecification().setDisabled(true);
            if(indentTypeBinding.getValue().equals("D") && indentForBinding.getValue().equals("P")){
                
                jwVendorBinding.setDisabled(true);
            }
            
            else{
                
                jwVendorBinding.setDisabled(false);
                
            }

        } else if (mode.equals("V")) {
            linkBinding.setDisabled(false);
            docTabbinding.setDisabled(false);
            getDetailCreateBinding().setDisabled(true);
            //getDetailDeleteBinding().setDisabled(true);
            

        }
    }
    
    
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
            try {
                Method method1 =
                    component.getClass().getMethod("setDisabled", boolean.class);
                if (method1 != null) {
                    method1.invoke(component, valueComponent);
                }
            } catch (NoSuchMethodException e) {
                try {
                    Method method =
                        component.getClass().getMethod("setReadOnly", boolean.class);
                    if (method != null) {
                        method.invoke(component, valueComponent);
                    }
                } catch (Exception e1) {
                    // e.printStackTrace();//silently eat this exception.
                }


            } catch (Exception e) {
                // e.printStackTrace();//silently eat this exception.
            }
            List<UIComponent> childComponents = component.getChildren();
            for (UIComponent comp : childComponents) {
                makeComponentHierarchyReadOnly(comp, valueComponent);
            }
        }


    public void setDetailStatusBinding(RichSelectBooleanCheckbox detailStatusBinding) {
        this.detailStatusBinding = detailStatusBinding;
    }

    public RichSelectBooleanCheckbox getDetailStatusBinding() {
        return detailStatusBinding;
    }

    public void preparedByReturnListener(ReturnPopupEvent returnPopupEvent)
    {
        System.out.println("Mode is=>"+ADFUtils.resolveExpression("#{pageFlowScope.mode}"));
        System.out.println("User id is=>"+ADFUtils.resolveExpression("#{pageFlowScope.userName}"));
    if(ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E"))
        {
            OperationBinding OprBind=ADFUtils.findOperation("verifiedAndAuthorisedPriviledge");  
            OprBind.getParamsMap().put("UserName",ADFUtils.resolveExpression("#{pageFlowScope.userName}"));
            OprBind.getParamsMap().put("FormName","IND");
            OprBind.getParamsMap().put("AuthLevel","VR");
            OprBind.getParamsMap().put("AuthSequnce","1");
            OprBind.execute();
            System.out.println("Result in Verified By Conditions=>"+OprBind.getResult());
                if(OprBind.getResult() !=null && OprBind.getResult().equals("Y"))
                {
                }
                else
                {
                    ADFUtils.showMessage("You can not verified this record.", 0); 
                }
        }
        else
        {
        ADFUtils.showMessage("Record verified only in edit mode.", 0);  
        }
    }

    public void authorisedByReturnListener(ReturnPopupEvent returnPopupEvent) 
    {
        System.out.println("Mode is=>"+ADFUtils.resolveExpression("#{pageFlowScope.mode}"));
        System.out.println("User id is=>"+ADFUtils.resolveExpression("#{pageFlowScope.userName}"));
        
            if(ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E"))
            {
                System.out.println("user Name is=>"+ADFUtils.resolveExpression("#{pageFlowScope.userName}"));
                            
                OperationBinding OprBind=ADFUtils.findOperation("checkApprovalFunctionStatus");  
                OprBind.getParamsMap().put("UserName",ADFUtils.resolveExpression("#{pageFlowScope.userName}"));
                OprBind.getParamsMap().put("FormName","IND");
                OprBind.getParamsMap().put("AuthLevel","AP");
                OprBind.getParamsMap().put("AuthSequnce","2");
                OprBind.execute();
                System.out.println("Result in Authorised By Conditions=>"+OprBind.getResult());
                        if(OprBind.getResult() !=null && OprBind.getResult().equals("Y"))
                        {
                        }
                        else
                        {
                        ADFUtils.showMessage("You can not authorised this record.", 0); 
                        }
            }
        else
            {
                ADFUtils.showMessage("Record authorised only in edit mode.", 0);  
            }
    }

    public void setPreparedByBinding(RichInputComboboxListOfValues preparedByBinding) {
        this.preparedByBinding = preparedByBinding;
    }

    public RichInputComboboxListOfValues getPreparedByBinding() 
    {
        return preparedByBinding;
    }

    public void indentStatusValidator(FacesContext facesContext, UIComponent uIComponent, Object object) 
    {
        if(indentByBinding.getValue()!=null)
        {
                
            if(IndentStatus !=null)
            {
            IndentStatus=(String) indentStatusBinding.getValue();
            }
                String UserCode=(String) ADFUtils.resolveExpression("#{pageFlowScope.empCode}");
                if(indentByBinding.getValue().equals(UserCode) && object.equals(IndentStatus))
                {
                    System.out.println("****************if***************************");
        
                }
                else if(!indentByBinding.getValue().equals(UserCode))
                {
                    System.out.println("****************else if***************************");
                    if(!object.equals(IndentStatus))
                    {
                        System.out.println("**************************else if if***************************");
                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Only Indenter Can Cancel Indent.", null));
                    }
                }
        
        
          }
 }

    public void detailCreateAL(ActionEvent actionEvent) {
       ADFUtils.findOperation("CreateInsert").execute();
       getIndentForBinding().setDisabled(true);
       getIndentTypeBinding().setDisabled(true);
       getLocationBinding().setDisabled(true);
       getSupplierBinding().setDisabled(true);
       if(indentTypeBinding.getValue().equals("R") || indentTypeBinding.getValue().equals("T") || indentTypeBinding.getValue().equals("J"))   
       {
//       getJwVendorBinding().setDisabled(true);
       }
       
       
       
    }

    public void setIndentForBinding(RichSelectOneChoice indentForBinding) {
        this.indentForBinding = indentForBinding;
    }

    public RichSelectOneChoice getIndentForBinding() {
        return indentForBinding;
    }

    public void setIndentTypeBinding(RichSelectOneChoice indentTypeBinding) {
        this.indentTypeBinding = indentTypeBinding;
    }

    public RichSelectOneChoice getIndentTypeBinding() {
        return indentTypeBinding;
    }

    public void setLocationBinding(RichInputComboboxListOfValues locationBinding) {
        this.locationBinding = locationBinding;
    }

    public RichInputComboboxListOfValues getLocationBinding() {
        return locationBinding;
    }

    public void cancelIndent(ActionEvent actionEvent) {
        // Add event code here...
//        System.out.println("Indent Status :- "+getIndentStatusBinding().getValue().toString());        
//        if (!getIndentStatusBinding().getValue().toString().equals("C")) {
//            System.out.println("Prepared By Binding"+getPreparedByBinding().getValue());
//            if (getPreparedByBinding().getValue() != null) {
//                Row selectedRow =
//                            (Row)ADFUtils.evaluateEL("#{bindings.PurchaseIndentHeaderVO1Iterator.currentRow}"); 
//                System.out.println("Indent Department :- "+selectedRow.getAttribute("IndDept"));
//                OperationBinding op = ADFUtils.findOperation("checkDocumentStatus");
//                op.getParamsMap().put("deptcd", selectedRow.getAttribute("IndDept"));
//                op.getParamsMap().put("formname", "CN");
//                op.getParamsMap().put("empcd", ADFUtils.resolveExpression("#{pageFlowScope.empCode}"));
//                Integer rst = (Integer) op.execute();
//                System.out.println("Rst Value :- "+rst);
//                if (rst != null) {
//                    if (op.getErrors().isEmpty()) {
//                        if (rst == 0){
//                            ADFUtils.showMessage("Sorry you are not authorize to cancel indent.", 1);
//                        }else{
//                            RichPopup.PopupHints hints = new RichPopup.PopupHints();
//                            getIndentCancelPopup().show(hints);
//                        }
//                    }
//                }
//            } else if (getPreparedByBinding().getValue() == null) {
//                System.out.println("Employee Code :- "+ADFUtils.resolveExpression("#{pageFlowScope.empCode}")+" Indent Binding "+getIndentByBinding().getValue().toString());
//                if (getIndentByBinding().getValue().toString().equals(ADFUtils.resolveExpression("#{pageFlowScope.empCode}"))){
//                    ADFUtils.findOperation("setIndentCancelStatus").execute();
//                    ADFUtils.showMessage("Indent successfully canceled.", 2);
//                }else {
//                    ADFUtils.showMessage("Only indenter can cancel indent.", 1);
//                }
//            }
//        }
//        else{
//            ADFUtils.showMessage("Indent already canceled.", 2);
//        }
    }
    
    public void setIndentCancelPopup(RichPopup indentCancelPopup) {
        this.indentCancelPopup = indentCancelPopup;
    }

    public RichPopup getIndentCancelPopup() {
        return indentCancelPopup;
    }

    public void indentCancelDL(DialogEvent dialogEvent) {
        // Add event code here...
        if(dialogEvent.getOutcome()==DialogEvent.Outcome.yes){
            ADFUtils.findOperation("setIndentCancelStatus").execute();
            ADFUtils.showMessage("Indent successfully canceled.", 2);
        }else{
            getIndentCancelPopup().cancel();
        }
    }

    public void IndentTypeVCL(ValueChangeEvent vce) {
        if(indentTypeBinding.getValue().equals("R") || indentTypeBinding.getValue().equals("T") || indentTypeBinding.getValue().equals("J"))   
        {
        getJwVendorBinding().setRequired(true);
        }
        
        if(indentTypeBinding.getValue().equals("R") || indentTypeBinding.getValue().equals("T") || indentTypeBinding.getValue().equals("J") || indentTypeBinding.getValue().equals("C"))   
        {
            jwVendorBinding.setDisabled(false);
        }
        
        if(indentTypeBinding.getValue().equals("D") && indentForBinding.getValue().equals("P")){
            
            jwVendorBinding.setDisabled(true);
        }
//        if(!indentTypeBinding.getValue().equals("D") && !indentForBinding.getValue().equals("P")){
//                
//                jwVendorBinding.setDisabled(false);
//            }

    
    }

    public void itemCdVCL(ValueChangeEvent valueChangeEvent) {
//        AdfFacesContext.getCurrentInstance().addPartialTarget(createPurchaseIndentTableBinding);
           valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
           if(valueChangeEvent!=null){
           ADFUtils.findOperation("goForindntQty").execute(); 
           }
//        ADFUtils.findOperation("clearRowOnItemVcePurIndent").execute(); 
//        AdfFacesContext.getCurrentInstance().addPartialTarget(createPurchaseIndentTableBinding);
        AdfFacesContext.getCurrentInstance().addPartialTarget(createPurchaseIndentTableBinding);
    }

    public void processCodeVCL(ValueChangeEvent valueChangeEvent) {
        AdfFacesContext.getCurrentInstance().addPartialTarget(createPurchaseIndentTableBinding);
    }

    public void proccdvalidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
      BigDecimal a=new BigDecimal(0);
        if(object == null || object.equals(".") || object.equals("0") || object.toString().isEmpty()){
           
            System.out.println("@@@@@@indent$$"+indentTypeBinding.getValue());
       if(indentTypeBinding.getValue().equals("J"))
       {
               throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Process Code is required.", null));
           }

    }
    }
    public void setPrcsCdBinding(RichInputComboboxListOfValues prcsCdBinding) {
        this.prcsCdBinding = prcsCdBinding;
    }

    public RichInputComboboxListOfValues getPrcsCdBinding() {
        return prcsCdBinding;
    }



    

    public void dtlButtonAL(ActionEvent actionEvent) {
      OperationBinding binding = (OperationBinding) ADFUtils.findOperation("fetchDatainDtl");
      Object op=binding.execute();
        RichPopup.PopupHints hints=new RichPopup.PopupHints();
        dtlPopupBinding.show(hints);
       String itemN = (String) itemDescriptionBinding.getValue(); 
//       System.out.println("item desc is===>>"+itemN);
       System.out.println("result from emthod==>>"+binding.getResult().toString());
       String item = (String) binding.getResult();
       if(binding.getResult().toString()!=null){
//           itemvalue=item;
           itemName=item;
           System.out.println("ITEM VALUE IS:="+itemName);
       }
       
        System.out.println("VALUE :==="+ADFUtils.evaluateEL("#{row.bindings.ItemCd.inputValue}"));
    }

    public void setDtlPopupBinding(RichPopup dtlPopupBinding) {
        this.dtlPopupBinding = dtlPopupBinding;
    }

    public RichPopup getDtlPopupBinding() {
        return dtlPopupBinding;
    }

    public void setDstatusBinding(RichColumn dstatusBinding) {
        this.dstatusBinding = dstatusBinding;
    }

    public RichColumn getDstatusBinding() {
        return dstatusBinding;
    }

    public void setPendQtyBinding(RichInputText pendQtyBinding) {
        this.pendQtyBinding = pendQtyBinding;
    }

    public RichInputText getPendQtyBinding() {
        return pendQtyBinding;
    }

    public void cancelIndentDialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok")){
        System.out.println("Indent Status :- "+getIndentStatusBinding().getValue().toString());        
        if (!getIndentStatusBinding().getValue().toString().equals("C")) {
            System.out.println("Prepared By Binding"+getPreparedByBinding().getValue());
            if (getPreparedByBinding().getValue() != null) {
                Row selectedRow =
                            (Row)ADFUtils.evaluateEL("#{bindings.PurchaseIndentHeaderVO1Iterator.currentRow}"); 
                System.out.println("Indent Department :- "+selectedRow.getAttribute("IndDept"));
                OperationBinding op = ADFUtils.findOperation("checkDocumentStatus");
                op.getParamsMap().put("deptcd", selectedRow.getAttribute("IndDept"));
                op.getParamsMap().put("formname", "CN");
                op.getParamsMap().put("empcd", ADFUtils.resolveExpression("#{pageFlowScope.empCode}"));
                Integer rst = (Integer) op.execute();
                System.out.println("Rst Value :- "+rst);
                if (rst != null) {
                    if (op.getErrors().isEmpty()) {
                        if (rst == 0){
                            ADFUtils.showMessage("Sorry you are not authorize to cancel indent.", 1);
                        }else{
                            RichPopup.PopupHints hints = new RichPopup.PopupHints();
                            getIndentCancelPopup().show(hints);
                        }
                    }
                }
            } else if (getPreparedByBinding().getValue() == null) {
                System.out.println("Employee Code :- "+ADFUtils.resolveExpression("#{pageFlowScope.empCode}")+" Indent Binding "+getIndentByBinding().getValue().toString());
                if (getIndentByBinding().getValue().toString().equals(ADFUtils.resolveExpression("#{pageFlowScope.empCode}"))){
                    ADFUtils.findOperation("setIndentCancelStatus").execute();
                    ADFUtils.showMessage("Indent successfully canceled.", 2);
                    Timestamp t = new Timestamp(System.currentTimeMillis());
                    getCancelDtBinding().setValue(t);
                    getCancelByBinding().setValue(ADFUtils.resolveExpression("#{pageFlowScope.empCode}"));
                }else {
                    ADFUtils.showMessage("Only indenter can cancel indent.", 1);
                }
            }
        }
        else{
            ADFUtils.showMessage("Indent already canceled.", 2);
        }
    }
    }

    public void setIndQty1Binding(RichInputText indQty1Binding) {
        this.indQty1Binding = indQty1Binding;
    }

    public RichInputText getIndQty1Binding() {
        return indQty1Binding;
    }

    public void appQtyValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if(object !=null){
        BigDecimal a=new BigDecimal(0);
                      BigDecimal appQty = (BigDecimal) object;
                      BigDecimal reqQty=new BigDecimal(0);
//                      reqQty = (BigDecimal) indQty1Binding.getValue()==null ? new BigDecimal(0):(BigDecimal) indQty1Binding.getValue();
                      reqQty = (BigDecimal) reqQtyBinding.getValue()==null ? new BigDecimal(0):(BigDecimal) reqQtyBinding.getValue();
           System.out.println("accept qty is===>"+appQty+"req qty is==>>"+reqQty);
                      if(object != null && (appQty.compareTo(a)==0 || appQty.compareTo(a)==-1)){
                      System.out.println("iside compare"+appQty);
                          
                      throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Approved Qty. must be greater than 0.", null));
                      }
                      if(appQty!=null && appQty.compareTo(reqQty)==1){
                        
                          throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Approved Qty. cannot be more than Indent/Authorised Qty.", null));  
                      }
        }

    }

    public void indForVCL(ValueChangeEvent valueChangeEvent) {
        if(valueChangeEvent !=null){
            if(indentForBinding.getValue().equals("C")){
        jwVendorBinding.setDisabled(false);   
    }
            if(indentTypeBinding.getValue().equals("D") && indentForBinding.getValue().equals("P")){
                
                jwVendorBinding.setDisabled(true);
            }
            
            
        }
}

    public void setCancelDtBinding(RichInputDate cancelDtBinding) {
        this.cancelDtBinding = cancelDtBinding;
    }

    public RichInputDate getCancelDtBinding() {
        return cancelDtBinding;
    }

    public void setCancelByBinding(RichInputComboboxListOfValues cancelByBinding) {
        this.cancelByBinding = cancelByBinding;
    }

    public RichInputComboboxListOfValues getCancelByBinding() {
        return cancelByBinding;
    }
    
    public void setItemvalue(String itemvalue) {
        this.itemvalue = itemvalue;
    }

    public String getItemvalue() {
        return itemvalue;
    }

   

    public void setItemIndtlBinding(RichInputText itemIndtlBinding) {
        this.itemIndtlBinding = itemIndtlBinding;
    }

    public RichInputText getItemIndtlBinding() {
        return itemIndtlBinding;
    }

    public void setItemInDtlBind(RichInputText itemInDtlBind) {
        this.itemInDtlBind = itemInDtlBind;
    }

    public RichInputText getItemInDtlBind() {
        return itemInDtlBind;
    }
    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemName() {
        return itemName;
    }

    public void setConfirmationPopupBinding(RichPopup confirmationPopupBinding) {
        this.confirmationPopupBinding = confirmationPopupBinding;
    }

    public RichPopup getConfirmationPopupBinding() {
        return confirmationPopupBinding;
    }

    public void amendConfirmationDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
        {
            OperationBinding op1=ADFUtils.findOperation("checkAmdNoPI");
             Object ob=op1.execute();
             if(op1.getResult().toString()!=null && op1.getResult().toString().equalsIgnoreCase("N")){
                 
                 ADFUtils.showMessage("You Can Update Latest Amendment Only.", 2);
                 ADFUtils.setEL("#{pageFlowScope.mode}", "V");
             }
             else{
            System.out.println("in ok event");
            OperationBinding op=ADFUtils.findOperation("getAmdNoPurchaseIndDetails");
            op.getParamsMap().put("IndId",ADFUtils.resolveExpression("#{pageFlowScope.IndId}"));
            op.getParamsMap().put("IndNo",ADFUtils.resolveExpression("#{pageFlowScope.indNo}"));
            op.getParamsMap().put("AmdNo",ADFUtils.resolveExpression("#{pageFlowScope.AmdNo}"));
            op.execute();
            ADFUtils.setEL("#{pageFlowScope.mode}", "E");
            cevmodecheck();
            AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
//            amdEnableDisable();
                 preparedByBinding.setDisabled(true);
        }
        }
        else{
        getConfirmationPopupBinding().hide();
        }
        
        
    }
    public void amdEnableDisable()
    {
        getIndentStatusBinding().setDisabled(true);
        getHeaderEditBinding().setDisabled(true);
        getIndentNoBinding().setDisabled(true);
        getIndentDateBinding().setDisabled(true);
        getPendQtyBinding().setDisabled(true);
        getUnitCodeBinding().setDisabled(true);
        getUnitDescriptionBinding().setDisabled(true);
        getLocationNameBinding().setDisabled(true);
        getIndentByBinding().setDisabled(true);
        getIndentByNameBinding().setDisabled(true);
        getPreparedByBinding().setDisabled(true);
        getHodApprovalDateBinding().setDisabled(true);
        getAuthorisedByBinding().setDisabled(false);
        getItemDescriptionBinding().setDisabled(true);
        getStockBinding().setDisabled(true);
        getRsdInPoBinding().setDisabled(true);
        getStoreRecdBinding().setDisabled(true);
        getValueBinding().setDisabled(true);
        getItemDescriptionBinding().setDisabled(true);
        getProcessDescriptionBinding().setDisabled(true);
        getApprovalDateBinding().setDisabled(true);
        // getDetailDeleteBinding().setDisabled(false);
        getIndentForBinding().setDisabled(true);
        getIndentTypeBinding().setDisabled(true);
        getLocationBinding().setDisabled(true);
        if(indentTypeBinding.getValue().equals("D") && indentForBinding.getValue().equals("P")){
            
            jwVendorBinding.setDisabled(true);
        }
        else{
            
            jwVendorBinding.setDisabled(false);
            
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
    }

    public void setAmdNoBinding(RichInputText amdNoBinding) {
        this.amdNoBinding = amdNoBinding;
    }

    public RichInputText getAmdNoBinding() {
        return amdNoBinding;
    }

    public void setTotalConsBinding(RichColumn totalConsBinding) {
        this.totalConsBinding = totalConsBinding;
    }

    public RichColumn getTotalConsBinding() {
        return totalConsBinding;
    }

    public void reqQtyvce(ValueChangeEvent valueChangeEvent) {
      
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
                       if(valueChangeEvent.getNewValue()!=null){
                           BigDecimal req = (BigDecimal) reqQtyBinding.getValue()==null ? new BigDecimal(0) :(BigDecimal) reqQtyBinding.getValue();
                           BigDecimal stck = (BigDecimal) stockBinding.getValue()==null ? new BigDecimal(0):(BigDecimal) stockBinding.getValue();

                           System.out.println("req qty -->>"+req+"stock is"+stck);
                           BigDecimal diff=req.subtract(stck);
                           System.out.println("difference is==>>>"+diff);
                           if(diff.compareTo(new BigDecimal(0))==1){
                                   indQty1Binding.setValue(diff);
                                   finalIndBinding.setValue(diff);
                                   AdfFacesContext.getCurrentInstance().addPartialTarget(createPurchaseIndentTableBinding);
                                   BigDecimal finalInd = (BigDecimal) finalIndBinding.getValue()==null ? new BigDecimal(0) :(BigDecimal) finalIndBinding.getValue();
                                   BigDecimal rate = (BigDecimal) rateBinding.getValue()==null ? new BigDecimal(0) :(BigDecimal)rateBinding.getValue();
                                   System.out.println("rate==>>"+rate+"final ind==>>"+finalInd);
                       valueBinding.setValue(finalInd.multiply(rate));
                       System.out.println("value is==>>"+valueBinding.getValue());
                               }
                           
                           BigDecimal a = (BigDecimal) reqQtyBinding.getValue();
                           System.out.println("req qty in bean iss==>>"+a);
//                           indQty1Binding.setValue(a);
                       }  
    }

    public void setReqQtyBinding(RichInputText reqQtyBinding) {
        this.reqQtyBinding = reqQtyBinding;
    }

    public RichInputText getReqQtyBinding() {
        return reqQtyBinding;
    }

    public void setTotConsBinding(RichInputText totConsBinding) {
        this.totConsBinding = totConsBinding;
    }

    public RichInputText getTotConsBinding() {
        return totConsBinding;
    }

    public void setPoNoBinding(RichInputText poNoBinding) {
        this.poNoBinding = poNoBinding;
    }

    public RichInputText getPoNoBinding() {
        return poNoBinding;
    }

    public void setPoDateBinding(RichInputDate poDateBinding) {
        this.poDateBinding = poDateBinding;
    }

    public RichInputDate getPoDateBinding() {
        return poDateBinding;
    }

    public void setTagNoBinding(RichInputText tagNoBinding) {
        this.tagNoBinding = tagNoBinding;
    }

    public RichInputText getTagNoBinding() {
        return tagNoBinding;
    }

    public void setProcSeqBinding(RichColumn procSeqBinding) {
        this.procSeqBinding = procSeqBinding;
    }

    public RichColumn getProcSeqBinding() {
        return procSeqBinding;
    }

    public void setProSeqBinding(RichInputText proSeqBinding) {
        this.proSeqBinding = proSeqBinding;
    }

    public RichInputText getProSeqBinding() {
        return proSeqBinding;
    }

    public void setCancelBtnBinding(RichButton cancelBtnBinding) {
        this.cancelBtnBinding = cancelBtnBinding;
    }

    public RichButton getCancelBtnBinding() {
        return cancelBtnBinding;
    }

    public void setAmdDateBinding(RichInputDate amdDateBinding) {
        this.amdDateBinding = amdDateBinding;
    }

    public RichInputDate getAmdDateBinding() {
        return amdDateBinding;
    }

    public void indQtyValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if(object !=null){
            BigDecimal ind = (BigDecimal) object;
            BigDecimal req = (BigDecimal) reqQtyBinding.getValue()==null ? new BigDecimal(0) :(BigDecimal) reqQtyBinding.getValue();
            BigDecimal stck = (BigDecimal) stockBinding.getValue()==null ? new BigDecimal(0):(BigDecimal) stockBinding.getValue();

            System.out.println("req qty -->>"+req+"ind qty-->"+ind+"stock is"+stck);
            BigDecimal diff=req.subtract(stck);
            System.out.println("difference is==>>>"+diff);
            if(diff.compareTo(new BigDecimal(0))==1){
                if(ind.compareTo(diff)==1){
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Authorised Qty cannot be greater than the difference of Requisition and Store/Stock qty.", null));
                } 
            }
//            else{
//                
//                if(ind.compareTo(diff)==1){
//                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Authorised Qty cannot be greater than the difference of Requisition and Store/Stock qty.", null));
//                }
//                
//            }
//            if(ind.compareTo(stck)==1){
//                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Authorised Qty must be less than or equals to Store/Stock Qty.", null));
//            }
//            if(ind.compareTo(req)==1){
//                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Authorised Qty must be less than or equals to Requisition", null));
//            }
        }
    }

    public void setRateBinding(RichInputText rateBinding) {
        this.rateBinding = rateBinding;
    }

    public RichInputText getRateBinding() {
        return rateBinding;
    }

    public void setItemSpecification(RichInputText itemSpecification) {
        this.itemSpecification = itemSpecification;
    }

    public RichInputText getItemSpecification() {
        return itemSpecification;
    }

    public void authByVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce.getNewValue()!=null)
        {
            OperationBinding ob = ADFUtils.findOperation("checkApprovalUnitStatus");
            System.out.println("Inde t type==>"+indentTypeBinding.getValue()+" EMP CD:"+ADFUtils.resolveExpression("#{pageFlowScope.empCode}"));
            String empccd =
            (String) (ADFUtils.resolveExpression("#{pageFlowScope.empCode}") != null ?
                      ADFUtils.resolveExpression("#{pageFlowScope.empCode}") : "SWE161");
            System.out.println("EMPCD:"+empccd);
            //                if(indentTypeBinding.getValue().equals("D")){
            //                    System.out.println("Indsdie iiiiiiifffffffffff");
            ob.getParamsMap().put("formNm", "IND");
            ob.getParamsMap().put("authoLim", "AP");
            ob.getParamsMap().put("unitcd", unitCodeBinding.getValue());
            //        System.out.println("Total SUM========>"+ADFUtils.resolveExpression("#{bindings.TotalAmtTrans.inputValue}"));
            //                ob.getParamsMap().put("Amount", amount);
            ob.getParamsMap().put("empcd", vce.getNewValue().toString());
            ob.execute();
            System.out.println("OP.RESULT:"+ob.getResult());
            if((ob.getResult() !=null && ob.getResult().equals("Y")))
            {
            Row row=(Row)ADFUtils.evaluateEL("#{bindings.PurchaseIndentHeaderVO1Iterator.currentRow}");
            row.setAttribute("AuthBy", null);
            ADFUtils.showMessage("You have not privledge to authorised this record for selected authentication type", 0);
            }
            
           /* BigDecimal amount=new BigDecimal(0);
            DCIteratorBinding dci = ADFUtils.findIterator("PurchaseIndentDetailVO1Iterator");
            RowSetIterator rsi=dci.getViewObject().createRowSetIterator(null);
                while(rsi.hasNext()) {
                    System.out.println("Value");
                    Row r = rsi.next();
                    BigDecimal sum=(BigDecimal) r.getAttribute("Value");
                    if(sum!=null){
                        amount=amount.add(sum);
                        
                    }
                   
                }
                rsi.closeRowSetIterator();
            System.out.println("Amountttttttttttttt==>>"+amount);
        System.out.println("IN VCE Approved");
        OperationBinding ob = ADFUtils.findOperation("checkApprovalFunctionStatus");
        System.out.println("Inde t type==>"+indentTypeBinding.getValue());
        if(indentTypeBinding.getValue().equals("D")){
            System.out.println("Indsdie iiiiiiifffffffffff");
        ob.getParamsMap().put("formNm", "IND");
        ob.getParamsMap().put("authoLim", "AP");
        ob.getParamsMap().put("UnitCd", unitCodeBinding.getValue());
//        System.out.println("Total SUM========>"+ADFUtils.resolveExpression("#{bindings.TotalAmtTrans.inputValue}"));
        ob.getParamsMap().put("Amount", amount);
        ob.getParamsMap().put("empcd", vce.getNewValue().toString());
        ob.execute();
        System.out.println(" EMP CD"+vce.getNewValue().toString());
        if((ob.getResult() !=null && !ob.getResult().equals("Y")))
        {
           Row row=(Row)ADFUtils.evaluateEL("#{bindings.PurchaseIndentHeaderVO1Iterator.currentRow}");
           row.setAttribute("AuthBy", null);
           ADFUtils.showMessage(ob.getResult().toString(), 0);
        }
        }
        else{
            System.out.println("iNDSEDI ELSE==>>");
            ob.getParamsMap().put("formNm", "IND_CAP");
            ob.getParamsMap().put("authoLim", "AP");
            ob.getParamsMap().put("UnitCd", unitCodeBinding.getValue());
            //        System.out.println("Total SUM========>"+ADFUtils.resolveExpression("#{bindings.TotalAmtTrans.inputValue}"));
            ob.getParamsMap().put("Amount", amount);
            ob.getParamsMap().put("empcd", vce.getNewValue().toString());
            ob.execute();
            System.out.println(" EMP CD"+vce.getNewValue().toString());
            if((ob.getResult() !=null && !ob.getResult().equals("Y")))
            {
               Row row=(Row)ADFUtils.evaluateEL("#{bindings.PurchaseIndentHeaderVO1Iterator.currentRow}");
               row.setAttribute("AuthBy", null);
               ADFUtils.showMessage(ob.getResult().toString(), 0);
            } 
        }*/
        }
    }

    public void uploadAction(ValueChangeEvent vce) {
        if (vce.getNewValue() != null) {
                    //Get File Object from VC Event
                    UploadedFile fileVal = (UploadedFile) vce.getNewValue();
                    //Upload File to path- Return actual server path
                    String path = ADFUtils.uploadFile(fileVal);
                      System.out.println(fileVal.getContentType());
                      System.out.println("Content Type==>>"+fileVal.getContentType()+"Path==>>"+path+"File name==>>"+fileVal.getFilename());
                    //Method to insert data in table to keep track of uploaded files
                    OperationBinding ob = ADFUtils.findOperation("setFileDataPI");
                    ob.getParamsMap().put("name", fileVal.getFilename());
                    ob.getParamsMap().put("path", path);
                    ob.getParamsMap().put("contTyp", fileVal.getContentType());
                    ob.getParamsMap().put("SrNo", ADFUtils.evaluateEL("#{bindings.DocAttachRefDtlVO9Iterator.estimatedRowCount+1}"));
                    ob.execute();
                    // Reset inputFile component after upload
                    ResetUtils.reset(vce.getComponent());
                }
    }

    public void setDocTableBinding(RichTable docTableBinding) {
        this.docTableBinding = docTableBinding;
    }

    public RichTable getDocTableBinding() {
        return docTableBinding;
    }

    public void deletepopupDocDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                    {
                    ADFUtils.findOperation("Delete1").execute();
                    System.out.println("Record Delete Successfully");
                   
                        FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                        FacesContext fc = FacesContext.getCurrentInstance();  
                        fc.addMessage(null, Message);  
                   
                    }
                AdfFacesContext.getCurrentInstance().addPartialTarget(docTableBinding);
    }

    public void setUnitBinding(RichInputText unitBinding) {
        this.unitBinding = unitBinding;
    }

    public RichInputText getUnitBinding() {
        return unitBinding;
    }

    public void setSeqNoBinding(RichInputText seqNoBinding) {
        this.seqNoBinding = seqNoBinding;
    }

    public RichInputText getSeqNoBinding() {
        return seqNoBinding;
    }

    public void setDocNoBinding(RichInputText docNoBinding) {
        this.docNoBinding = docNoBinding;
    }

    public RichInputText getDocNoBinding() {
        return docNoBinding;
    }

    public void setTypeBinding(RichInputText typeBinding) {
        this.typeBinding = typeBinding;
    }

    public RichInputText getTypeBinding() {
        return typeBinding;
    }

    public void setFileNameBinding(RichInputText fileNameBinding) {
        this.fileNameBinding = fileNameBinding;
    }

    public RichInputText getFileNameBinding() {
        return fileNameBinding;
    }

    public void setDocDateBinding(RichInputDate docDateBinding) {
        this.docDateBinding = docDateBinding;
    }

    public RichInputDate getDocDateBinding() {
        return docDateBinding;
    }

    public void setContenetTypeBinding(RichInputText contenetTypeBinding) {
        this.contenetTypeBinding = contenetTypeBinding;
    }

    public RichInputText getContenetTypeBinding() {
        return contenetTypeBinding;
    }

    public void setPathBinding(RichInputText pathBinding) {
        this.pathBinding = pathBinding;
    }

    public RichInputText getPathBinding() {
        return pathBinding;
    }

    public void downloadAction(FacesContext facesContext, OutputStream outputStream) throws  Exception{
        File filed = new File(pathBinding.getValue().toString());
                               FileInputStream fis;
                               byte[] b;
                               try {
                                   fis = new FileInputStream(filed);

                                   int n;
                                   while ((n = fis.available()) > 0) {
                                       b = new byte[n];
                                       int result = fis.read(b);
                                       outputStream.write(b, 0, b.length);
                                       if (result == -1)
                                           break;
                                   }
                               } catch (Exception e) {
                                   e.printStackTrace();
                               }
                               outputStream.flush();

    }

    public void setDocTabbinding(RichShowDetailItem docTabbinding) {
        this.docTabbinding = docTabbinding;
    }

    public RichShowDetailItem getDocTabbinding() {
        return docTabbinding;
    }

    public void setLinkBinding(RichCommandLink linkBinding) {
        this.linkBinding = linkBinding;
    }

    public RichCommandLink getLinkBinding() {
        return linkBinding;
    }

    public void verifiedByVCL(ValueChangeEvent vce) {
        System.out.println("INSIDE verifiedByVCL ##### ");
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce.getNewValue()!=null)
        {
            OperationBinding ob = ADFUtils.findOperation("checkApprovalUnitStatus");
            System.out.println("Inde t type==>"+indentTypeBinding.getValue()+" EMP CD:"+ADFUtils.resolveExpression("#{pageFlowScope.empCode}"));
            String empccd =
            (String) (ADFUtils.resolveExpression("#{pageFlowScope.empCode}") != null ?
                      ADFUtils.resolveExpression("#{pageFlowScope.empCode}") : "SWE161");
            System.out.println("EMPCD:"+empccd);
            //                if(indentTypeBinding.getValue().equals("D")){
            //                    System.out.println("Indsdie iiiiiiifffffffffff");
            ob.getParamsMap().put("formNm", "IND");
            ob.getParamsMap().put("authoLim", "VR");
            ob.getParamsMap().put("unitcd", unitCodeBinding.getValue());
            //        System.out.println("Total SUM========>"+ADFUtils.resolveExpression("#{bindings.TotalAmtTrans.inputValue}"));
            //                ob.getParamsMap().put("Amount", amount);
            ob.getParamsMap().put("empcd", vce.getNewValue().toString());
            ob.execute();
            System.out.println("OP.RESULT:"+ob.getResult());
            if((ob.getResult() !=null && ob.getResult().equals("Y")))
            {
            Row row=(Row)ADFUtils.evaluateEL("#{bindings.PurchaseIndentHeaderVO1Iterator.currentRow}");
            row.setAttribute("StoreAppBy", null);
            ADFUtils.showMessage("You have not privledge to verify this record for selected authentication type", 0);
            }
            /*BigDecimal amount=new BigDecimal(0);
            DCIteratorBinding dci = ADFUtils.findIterator("PurchaseIndentDetailVO1Iterator");
            RowSetIterator rsi=dci.getViewObject().createRowSetIterator(null);
                while(rsi.hasNext()) {
                    System.out.println("Value");
                    Row r = rsi.next();
                    BigDecimal sum=(BigDecimal) r.getAttribute("Value");
                    if(sum!=null){
                        amount=amount.add(sum);
                        
                    }
                   
                }
                rsi.closeRowSetIterator();
            System.out.println("Amountttttttttttttt==>>"+amount);
        System.out.println("IN VCE Approved");
        OperationBinding ob = ADFUtils.findOperation("checkApprovalFunctionStatus");
        System.out.println("Inde t type==>"+indentTypeBinding.getValue());
        if(indentTypeBinding.getValue().equals("D")){
            System.out.println("Indsdie iiiiiiifffffffffff");
        ob.getParamsMap().put("formNm", "IND");
        ob.getParamsMap().put("authoLim", "VR");
        ob.getParamsMap().put("UnitCd", unitCodeBinding.getValue());
        //        System.out.println("Total SUM========>"+ADFUtils.resolveExpression("#{bindings.TotalAmtTrans.inputValue}"));
        ob.getParamsMap().put("Amount", amount);
        ob.getParamsMap().put("empcd", vce.getNewValue().toString());
        ob.execute();
        System.out.println(" EMP CD"+vce.getNewValue().toString());
        if((ob.getResult() !=null && !ob.getResult().equals("Y")))
        {
           Row row=(Row)ADFUtils.evaluateEL("#{bindings.PurchaseIndentHeaderVO1Iterator.currentRow}");
           row.setAttribute("StoreAppBy", null);
           ADFUtils.showMessage(ob.getResult().toString(), 0);
        }
        }
        else{
            System.out.println("iNDSEDI ELSE==>>");
            ob.getParamsMap().put("formNm", "IND_CAP");
            ob.getParamsMap().put("authoLim", "VR");
            ob.getParamsMap().put("UnitCd", unitCodeBinding.getValue());
            //        System.out.println("Total SUM========>"+ADFUtils.resolveExpression("#{bindings.TotalAmtTrans.inputValue}"));
            ob.getParamsMap().put("Amount", amount);
            ob.getParamsMap().put("empcd", vce.getNewValue().toString());
            ob.execute();
            System.out.println(" EMP CD"+vce.getNewValue().toString());
            if((ob.getResult() !=null && !ob.getResult().equals("Y")))
            {
               Row row=(Row)ADFUtils.evaluateEL("#{bindings.PurchaseIndentHeaderVO1Iterator.currentRow}");
               row.setAttribute("StoreAppBy", null);
               ADFUtils.showMessage(ob.getResult().toString(), 0);
            } 
        }*/
        }
    }

    public void setSupplierBinding(RichInputComboboxListOfValues supplierBinding) {
        this.supplierBinding = supplierBinding;
    }

    public RichInputComboboxListOfValues getSupplierBinding() {
        return supplierBinding;
    }

    public void rateVCE(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
                               if(valueChangeEvent.getNewValue()!=null){
                                   BigDecimal req = (BigDecimal) reqQtyBinding.getValue()==null ? new BigDecimal(0) :(BigDecimal) reqQtyBinding.getValue();
                                   BigDecimal stck = (BigDecimal) stockBinding.getValue()==null ? new BigDecimal(0):(BigDecimal) stockBinding.getValue();

                                   System.out.println("rateVCL req qty -->>"+req+"stock is"+stck);
                                   BigDecimal diff=req.subtract(stck);
                                   System.out.println("difference is==>>>"+diff);
                                   if(diff.compareTo(new BigDecimal(0))==1){
        //                                   indQty1Binding.setValue(diff);
        //                                   finalIndBinding.setValue(diff);
                                           AdfFacesContext.getCurrentInstance().addPartialTarget(createPurchaseIndentTableBinding);
                                           BigDecimal finalInd = (BigDecimal) finalIndBinding.getValue()==null ? new BigDecimal(0) :(BigDecimal) finalIndBinding.getValue();
                                           BigDecimal rate = (BigDecimal) rateBinding.getValue()==null ? new BigDecimal(0) :(BigDecimal)rateBinding.getValue();
                                           System.out.println("rate==>>"+rate+"final ind==>>"+finalInd);
                               valueBinding.setValue(finalInd.multiply(rate));
                               System.out.println("value is==>>"+valueBinding.getValue());
                                       }
                                   
                                   BigDecimal a = (BigDecimal) reqQtyBinding.getValue();
                                   System.out.println("req qty in bean iss==>>"+a);
                //                           indQty1Binding.setValue(a);
                               }
    }
}
