package terms.mtl.transaction.ui.bean;

import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.view.rich.component.rich.RichPopup;

import oracle.adf.view.rich.component.rich.data.RichColumn;

import oracle.jbo.Row;
import oracle.jbo.domain.Timestamp;

public class POHeadBean {
    private RichColumn getApprBinding;
    private String editAction="V";

    public POHeadBean() {
    }

    public void POStatusVCE(ValueChangeEvent valueChangeEvent) {
    System.out.println("***************************");
     Row selectedRow =
                            (Row)ADFUtils.evaluateEL("#{bindings.POHeadVO1Iterator.currentRow}"); 

                oracle.binding.OperationBinding op = ADFUtils.findOperation("checkDocumentStatus");
                
                op.getParamsMap().put("formname", "POCNM");
       //         op.getParamsMap().put("empcd", "SWE161");
                op.getParamsMap().put("empcd", ADFUtils.resolveExpression("#{pageFlowScope.empCode}"));
                Integer rst = (Integer) op.execute();
                System.out.println("Rst Value:- "+rst);
                if (rst != null) {
                    if (op.getErrors().isEmpty()) {
                        if (rst == 0){
                            ADFUtils.showMessage("Sorry you are not authorize to cancel Po.", 1);
                        }
                    }
                }
            }
            
        
 

    public void setGetApprBinding(RichColumn getApprBinding) {
        this.getApprBinding = getApprBinding;
    }

    public RichColumn getGetApprBinding() {
        return getApprBinding;
    }

    public void SaveAL(ActionEvent actionEvent) {
        {
                           
                            ADFUtils.showMessage("Your Record has been Saved Sucessfully.", 2);
                            ADFUtils.findOperation("Commit").execute();
                        }
    }

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }
}
