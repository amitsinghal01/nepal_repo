package terms.mtl.transaction.ui.bean;

import java.text.SimpleDateFormat;

import javax.el.ELContext;

import javax.el.ExpressionFactory;

import javax.el.ValueExpression;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.ViewCriteria;
import oracle.jbo.ViewObject;

public class PurchaseIndentCloseBean {
    private RichInputDate fromDate;
    private RichInputDate toDate;
    private RichInputText indentno;

    public PurchaseIndentCloseBean() {
    }

    public Object evaluteEL(String el) {
        FacesContext fctx = FacesContext.getCurrentInstance();
        ELContext elContext = fctx.getELContext();
        Application app = fctx.getApplication();
        ExpressionFactory expFactory = app.getExpressionFactory();
        ValueExpression valExp =
            expFactory.createValueExpression(elContext, el, Object.class);
        return valExp.getValue(elContext);
    } 

    public String saveAl() {
        

        BindingContainer bindings = ADFUtils.getBindingContainer();
        OperationBinding operationBinding = bindings.getOperationBinding("PurchaseIndentCloseDataSave");
        Object result = operationBinding.execute();
        
        if (!operationBinding.getErrors().isEmpty()) {
            FacesMessage Message =
                new FacesMessage("Data not saved");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
             
        return null;
        }
        //return null;
       // System.out.println("Result= " + result); // result will be the output of PL function
        
        //System.out.println("=========test=======");
        
        try{ 
        if(result.equals("Yes")){
       FacesMessage Message =
           new FacesMessage("Record Saved Successfully.");
       Message.setSeverity(FacesMessage.SEVERITY_INFO);
       FacesContext fc = FacesContext.getCurrentInstance();
       fc.addMessage(null, Message);
        
       ADFUtils.findOperation("emptyPurchaseIndentClose").execute();
        }
        else{
            FacesMessage Message =
                new FacesMessage("Data not saved");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
        }
        }
        catch(Exception ex) {
           // ex.printStackTrace();
            FacesMessage Message =
                new FacesMessage(FacesMessage.SEVERITY_ERROR,"Please first Tick CheckBox","Please first Tick CheckBox");
           // Message.setSeverity(FacesMessage.SEVERITY_ERROR);
           FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
        }
              
        
        
       
          
        
        return null;
       
    }

    //@SuppressWarnings("unchecked")
    public void onClickSearch(ActionEvent actionEvent) {
        
              String string_from_date = null;
                String string_to_date = null;
                SimpleDateFormat sdf = null;
                String string_Indent_no=null;
               sdf = new SimpleDateFormat("yyyy-MM-dd");
                if (null == fromDate.getValue()) {
                } else {
                    string_from_date = sdf.format(fromDate.getValue());
                }
                if (null == toDate.getValue()) {
                } else {
                    string_to_date = sdf.format(toDate.getValue());
                }
                
                string_Indent_no=(String)indentno.getValue();
                
                //System.out.println("method call");
                try{
             OperationBinding ob = ADFUtils.findOperation("getIndCloseDetail");         
             ob.getParamsMap().put("fromdate",string_from_date );
             ob.getParamsMap().put("todate", string_to_date);
             ob.getParamsMap().put("Indentno", string_Indent_no);      
             ob.execute();
                }
                catch (Exception ex){
                    ex.printStackTrace();
                }

         }
        
                
        
    

    public void setFromDate(RichInputDate fromDate) {
        this.fromDate = fromDate;
    }

    public RichInputDate getFromDate() {
        return fromDate;
    }

    public void setToDate(RichInputDate toDate) {
        this.toDate = toDate;
    }

    public RichInputDate getToDate() {
        return toDate;
    }

    public String onClickSearch() {
        // Add event code here...
        return null;
    }


    public void setIndentno(RichInputText indentno) {
        this.indentno = indentno;
    }

    public RichInputText getIndentno() {
        return indentno;
    }
}
