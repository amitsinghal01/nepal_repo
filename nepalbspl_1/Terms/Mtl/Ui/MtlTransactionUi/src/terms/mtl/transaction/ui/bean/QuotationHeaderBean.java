package terms.mtl.transaction.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.sql.Timestamp;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import javax.faces.validator.ValidatorException;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectBooleanCheckbox;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;
import oracle.jbo.domain.Date;

public class QuotationHeaderBean {

    private RichButton editBinding;
    private RichOutputText bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichTable tableBinding;
    private RichSelectBooleanCheckbox cancelledBinding;
    private RichInputText quotationNoBinding;
    private RichInputComboboxListOfValues enqNoBinding;
    private RichSelectOneChoice getQuotTypeBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText paymentTermNameBinding;
    private RichInputComboboxListOfValues preparedByBinding;
    private RichInputComboboxListOfValues approvedByBinding;
    private RichInputComboboxListOfValues bindingEnquiryNo;
    private RichInputDate quotationDateBinding;
    private RichInputDate venQuotDt;
    private RichInputDate enqDt;
    private RichInputDate venQtDtBinding;
    private RichInputDate enqDtBinding;
    private RichInputText totAmtBinding;
    private RichInputComboboxListOfValues vendorCdBinding;
    private RichInputText totalAmountBinding;
    private RichInputText vendorQuotNoBinding;
    String Status2="F" ,Status3="F";
    private RichInputText totalAmtBinding;

    public QuotationHeaderBean() {
    }

    //    public String QuotationSaveAL() {
    //        OperationBinding op = ADFUtils.findOperation("getQuotNo");
    //        Object rst = op.execute();
    //        if (rst != null && rst != "")
    //                   {
    //                       if (op.getErrors().isEmpty()) {
    //                           ADFUtils.findOperation("Commit").execute();
    //                           ADFUtils.showMessage("Record Saved Successfully" + "Quotation No is::"+ rst, 2);
    //                       }
    //                   }
    //
    //                   if (rst == null || rst == "" || rst == " ")
    //                   {
    //                       if (op.getErrors().isEmpty()) {
    //                           ADFUtils.findOperation("Commit").execute();
    //                           ADFUtils.showMessage(" Record Updated Successfully.", 2);
    //                     }
    //
    //                   }
    //
    //        return "Save";
    //    }

    public void EditActionAL(ActionEvent actionEvent) {
        if(cancelledBinding.getValue().equals(true))
        {
            ADFUtils.showMessage("Cancelled Quotation cannot be edited further.", 1);
            ADFUtils.setEL("#{pageFlowScope.mode}", "V");
        }else
        {
        OperationBinding op =(OperationBinding)ADFUtils.findOperation("CheckPurchaseForQuotation");
        op.execute();
        if(op.getResult().toString().equals("E"))
        {
            ADFUtils.showMessage("Quotation cannot be edited further as its Purchase Order has been made.", 1);
            ADFUtils.setEL("#{pageFlowScope.mode}", "V");
        }else
        {
            cevmodecheck();
        }
        }
    }

    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }

    public void cevModeDisableComponent(String mode) {

        if (mode.equals("C")) {
            getEditBinding().setDisabled(true);
            getCancelledBinding().setDisabled(true);
            getQuotationNoBinding().setDisabled(true);
            System.out.println("Quot Type "+getQuotTypeBinding.getValue());
            if(getQuotTypeBinding.getValue()!=null && getQuotTypeBinding.getValue().equals("D"))
            {
                getBindingEnquiryNo().setDisabled(true);
                getVendorCdBinding().setDisabled(false);
            }
            else{
                getBindingEnquiryNo().setDisabled(false);
                getVendorCdBinding().setDisabled(true);
            }
            getUnitCodeBinding().setDisabled(true);
            getPaymentTermNameBinding().setDisabled(true);
            getPreparedByBinding().setDisabled(true);
            getApprovedByBinding().setDisabled(true);
            getQuotationDateBinding().setDisabled(true);
            if(Status2.equalsIgnoreCase("T") && Status3.equalsIgnoreCase("T"))
            {
            getQuotTypeBinding.setDisabled(false);
            }else
            {
                getQuotTypeBinding.setDisabled(true);
            }
//            getTotalAmtBinding().setDisabled(true);
        }
        if (mode.equals("E")) {
            getQuotTypeBinding.setDisabled(true);
            getEditBinding().setDisabled(true);
            getQuotationNoBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
            getPaymentTermNameBinding().setDisabled(true);
            getPreparedByBinding().setDisabled(true);
            getBindingEnquiryNo().setDisabled(true);
            getQuotationDateBinding().setDisabled(true);
            getVendorCdBinding().setDisabled(true);
            vendorQuotNoBinding.setDisabled(true);
            getVenQtDtBinding().setDisabled(true);
//            getTotalAmtBinding().setDisabled(true);
        }
        if (mode.equals("V")) {
            getQuotationNoBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
            getPaymentTermNameBinding().setDisabled(true);
            getPreparedByBinding().setDisabled(true);

        }
    }

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }

    public void setEditBinding(RichButton editBinding) {
        this.editBinding = editBinding;
    }

    public RichButton getEditBinding() {
        return editBinding;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void DeleteDialogPopup(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(tableBinding);

    }

    public void setTableBinding(RichTable tableBinding) {
        this.tableBinding = tableBinding;
    }

    public RichTable getTableBinding() {
        return tableBinding;
    }

    public void setCancelledBinding(RichSelectBooleanCheckbox cancelledBinding) {
        this.cancelledBinding = cancelledBinding;
    }

    public RichSelectBooleanCheckbox getCancelledBinding() {
        return cancelledBinding;
    }

    public void setQuotationNoBinding(RichInputText quotationNoBinding) {
        this.quotationNoBinding = quotationNoBinding;
    }

    public RichInputText getQuotationNoBinding() {
        return quotationNoBinding;
    }


    public void setGetQuotTypeBinding(RichSelectOneChoice getQuotTypeBinding) {
        this.getQuotTypeBinding = getQuotTypeBinding;
    }

    public RichSelectOneChoice getGetQuotTypeBinding() {
        return getQuotTypeBinding;
    }

    public void QuotTypeVce(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        System.out.println("value of Quot type is" + getGetQuotTypeBinding().getValue());
        if (getQuotTypeBinding.getValue().toString().equalsIgnoreCase("D")) {
            getBindingEnquiryNo().setDisabled(true);
            getVendorCdBinding().setDisabled(false);
        } else {

            getBindingEnquiryNo().setDisabled(false);
            getVendorCdBinding().setDisabled(true);



        }
//        System.out.println("getCurrentSystemDate() value is:="+getCurrentSystemDate());
//        getCurrentSystemDate();
        AdfFacesContext.getCurrentInstance().addPartialTarget(venQtDtBinding);
        

    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setPaymentTermNameBinding(RichInputText paymentTermNameBinding) {
        this.paymentTermNameBinding = paymentTermNameBinding;
    }

    public RichInputText getPaymentTermNameBinding() {
        return paymentTermNameBinding;
    }

    public void setPreparedByBinding(RichInputComboboxListOfValues preparedByBinding) {
        this.preparedByBinding = preparedByBinding;
    }

    public RichInputComboboxListOfValues getPreparedByBinding() {
        return preparedByBinding;
    }

    public void setApprovedByBinding(RichInputComboboxListOfValues approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputComboboxListOfValues getApprovedByBinding() {
        return approvedByBinding;
    }

    public void setBindingEnquiryNo(RichInputComboboxListOfValues bindingEnquiryNo) {
        this.bindingEnquiryNo = bindingEnquiryNo;
    }

    public RichInputComboboxListOfValues getBindingEnquiryNo() {
        return bindingEnquiryNo;
    }

    public void createAction(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert").execute();
        ADFUtils.findOperation("setQuotationNumber").execute();
    }

    public void saveAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("quotNo").execute();
        BigDecimal sum=new BigDecimal(0);
        DCIteratorBinding dci = ADFUtils.findIterator("QuotationDetailVO1Iterator");
        RowSetIterator rsi=dci.getViewObject().createRowSetIterator(null);
            while(rsi.hasNext()) {
                Row r = rsi.next();
                BigDecimal total=(BigDecimal) r.getAttribute("TOTALAMT");
                System.out.println("SUM ITERATOR"+total);
                if(total!=null)
                {
                 sum=sum.add(total);
                }
            }
            rsi.closeRowSetIterator();
            System.out.println("SUM"+sum);
        Row row=(Row)ADFUtils.evaluateEL("#{bindings.QuotationHeaderVO1Iterator.currentRow}");
        row.setAttribute("Amount", sum);
        if (vendorQuotNoBinding.isDisabled()) {
            ADFUtils.findOperation("Commit").execute();
            FacesMessage Message = new FacesMessage("Record Update Successfully.");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        } else {
            System.out.println("-----*** Create Mode ***-----");

            ADFUtils.findOperation("Commit").execute();
            FacesMessage Message =
                new FacesMessage("Record Save Successfully.Quotation Number No. is " +
                                 getQuotationNoBinding().getValue());
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
        }
    }


    public void setQuotationDateBinding(RichInputDate quotationDateBinding) {
        this.quotationDateBinding = quotationDateBinding;
    }

    public RichInputDate getQuotationDateBinding() {
        return quotationDateBinding;
    }

    public void setVenQtDtBinding(RichInputDate venQtDtBinding) {
        this.venQtDtBinding = venQtDtBinding;
    }

    public RichInputDate getVenQtDtBinding() {
        return venQtDtBinding;
    }

    public void setEnqDtBinding(RichInputDate enqDtBinding) {
        this.enqDtBinding = enqDtBinding;
    }

    public RichInputDate getEnqDtBinding() {
        return enqDtBinding;
    }

    public void itemCdVCE(ValueChangeEvent valueChangeEvent) {
        ADFUtils.findOperation("clearRowOnItemVceQuotation").execute();
        AdfFacesContext.getCurrentInstance().addPartialTarget(tableBinding);
    }


    public void setTotAmtBinding(RichInputText totAmtBinding) {
        this.totAmtBinding = totAmtBinding;
    }

    public RichInputText getTotAmtBinding() {
        return totAmtBinding;
    }


    public void disValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (object != null) {
            BigDecimal dis = (BigDecimal) object;
            System.out.println("discount is*****" + dis);
            BigDecimal tot = (BigDecimal) totAmtBinding.getValue();
            System.out.println("Item * qty is*****" + tot);
            if (dis.compareTo(tot) == 1) {
                System.out.println("in the if block****");
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                              "Discount should not be greater than from item rate* qty.",
                                                              null));


            }


        }

    }


    public Date getCurrentSystemDate()
    {
        if (getQuotTypeBinding.getValue() != null)
        {
            if (getQuotTypeBinding.getValue().equals("E") && enqDtBinding.getValue() != null) {
                return (Date) enqDtBinding.getValue();
            }
            if (getQuotTypeBinding.getValue().equals("D") && quotationDateBinding.getValue() != null) {
                return (Date) quotationDateBinding.getValue();
            }
            return null;
        }
        return null;
    }

    public void setVendorCdBinding(RichInputComboboxListOfValues vendorCdBinding) {
        this.vendorCdBinding = vendorCdBinding;
    }

    public RichInputComboboxListOfValues getVendorCdBinding() {
        return vendorCdBinding;
    }

    public void discountVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());            
        if(vce.getNewValue()!=null && totAmtBinding.getValue()!=null)
        {
           System.out.println("New Value"+vce.getNewValue()+" Amount"+totAmtBinding.getValue());
            BigDecimal amt1 = (BigDecimal)totAmtBinding.getValue();
            BigDecimal disAmt = new BigDecimal(0);
            BigDecimal per = new BigDecimal(100);
                System.out.println("amt is*****"+amt1);
                BigDecimal dis=(BigDecimal) vce.getNewValue();
                disAmt=amt1.subtract((amt1.multiply(dis)).divide(per));
                System.out.println("Discount Amount===>"+disAmt); 
            totAmtBinding.setValue(disAmt);
            
        }
    }

    public void setTotalAmountBinding(RichInputText totalAmountBinding) {
        this.totalAmountBinding = totalAmountBinding;
    }

    public RichInputText getTotalAmountBinding() {
        return totalAmountBinding;
    }

    public void cancelledAuthority(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce.getNewValue().equals(true))
        {
        System.out.println("Cancel Value"+vce.getOldValue()+" "+ADFUtils.resolveExpression("#{pageFlowScope.empCode}"));
        OperationBinding ob = ADFUtils.findOperation("checkApprovalStatus");
        ob.getParamsMap().put("formNm", "QUOT_CN");
        ob.getParamsMap().put("authoLim", "CN");
//        ob.getParamsMap().put("empcd", "SWE161");
        ob.getParamsMap().put("empcd", ADFUtils.resolveExpression("#{pageFlowScope.empCode}"));
        ob.execute();
//        System.out.println(" Cancel "+vce.getNewValue().toString());
        if((ob.getResult() !=null && ob.getResult().equals("Y")))
        {
           cancelledBinding.setValue(false);
           AdfFacesContext.getCurrentInstance().addPartialTarget(cancelledBinding);
           ADFUtils.showMessage("You have no authority to cancel this Quotation.",0);
        }
        }
//        else{
//            cancelledBinding.setValue(true);
//            ADFUtils.showMessage("This Quotation is already cancelled.",0);
//        }
    }

    public void approvedByVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        OperationBinding ob = ADFUtils.findOperation("checkApprovalStatus");
        ob.getParamsMap().put("formNm", "QUOT_AP");
        ob.getParamsMap().put("authoLim", "AP");
        ob.getParamsMap().put("empcd", vce.getNewValue().toString());
        ob.execute();
        System.out.println(" EMP CD"+vce.getNewValue().toString());
        if((ob.getResult() !=null && ob.getResult().equals("Y")))
        {
           Row row=(Row)ADFUtils.evaluateEL("#{bindings.QuotationHeaderVO1Iterator.currentRow}");
           row.setAttribute("ApprovedBy", null);
           ADFUtils.showMessage("You have no authority to approve this Quotation.",0);
        }
    }

    public void onPageLoad() {
        ADFUtils.findOperation("CreateInsert").execute();
        OperationBinding ob = ADFUtils.findOperation("CheckApprovalAuthoQuotCreation");
        ob.getParamsMap().put("formNm", "QUOT_CN_AP");
        ob.getParamsMap().put("authoLim", "PR");
        ob.getParamsMap().put("empcd", ADFUtils.resolveExpression("#{pageFlowScope.empCode}") == null ? "SWE161" : ADFUtils.resolveExpression("#{pageFlowScope.empCode}"));
        ob.getParamsMap().put("unitCd", ADFUtils.resolveExpression("#{pageFlowScope.unitCode}") == null ? "10001" : ADFUtils.resolveExpression("#{pageFlowScope.unitCode}"));
        ob.execute();
        if((ob.getResult() !=null && ob.getResult().equals("N")))
        {   
            Status2="T";
        }
        
        OperationBinding ob1 = ADFUtils.findOperation("CheckApprovalAuthoQuotCreation");
        ob1.getParamsMap().put("formNm", "QUOT_EN_AP");
        ob1.getParamsMap().put("authoLim", "PR");
        ob1.getParamsMap().put("empcd", ADFUtils.resolveExpression("#{pageFlowScope.empCode}") == null ? "SWE161" : ADFUtils.resolveExpression("#{pageFlowScope.empCode}"));
        ob1.getParamsMap().put("unitCd", ADFUtils.resolveExpression("#{pageFlowScope.unitCode}") == null ? "10001" : ADFUtils.resolveExpression("#{pageFlowScope.unitCode}"));
        ob1.execute();
        if((ob1.getResult() !=null && ob1.getResult().equals("N")))
        {   
            Status3="T";
        }

        if(Status2.equalsIgnoreCase("T") && Status3.equalsIgnoreCase("F"))
        {
            Row row=(Row)ADFUtils.evaluateEL("#{bindings.QuotationHeaderVO1Iterator.currentRow}");
            row.setAttribute("QuotTyp", "D");
            
        }else if(Status2.equalsIgnoreCase("F") && Status3.equalsIgnoreCase("T"))
        {
            Row row=(Row)ADFUtils.evaluateEL("#{bindings.QuotationHeaderVO1Iterator.currentRow}");
            row.setAttribute("QuotTyp", "E");
        }
        
       
    }

    public void setVendorQuotNoBinding(RichInputText vendorQuotNoBinding) {
        this.vendorQuotNoBinding = vendorQuotNoBinding;
    }

    public RichInputText getVendorQuotNoBinding() {
        return vendorQuotNoBinding;
    }

    public void setTotalAmtBinding(RichInputText totalAmtBinding) {
        this.totalAmtBinding = totalAmtBinding;
    }

    public RichInputText getTotalAmtBinding() {
        return totalAmtBinding;
    }
}


