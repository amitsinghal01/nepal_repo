package terms.mtl.transaction.ui.bean;

public class RackMappingBean {
    private String editAction="V";
    public RackMappingBean() {
    }

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }
}
