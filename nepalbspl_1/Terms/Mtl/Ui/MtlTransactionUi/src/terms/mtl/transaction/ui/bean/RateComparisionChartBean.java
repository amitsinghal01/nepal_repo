package terms.mtl.transaction.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectBooleanCheckbox;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;

public class RateComparisionChartBean {
    private RichInputText compNoBinding;
    private RichInputComboboxListOfValues bindingQuotType1;
    private RichInputComboboxListOfValues bindingQuotType2;
    private RichInputComboboxListOfValues bindingQuotType3;
    private RichInputComboboxListOfValues bindingQuotType4;
    private RichInputComboboxListOfValues bindingQuotType5;
    private RichColumn binddingItemCode;
    private RichOutputText bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichShowDetailItem rateChartComparisonBinding;
    private RichShowDetailItem compareMinRateBinding;
    private RichInputText basicRate1Binding;
    private RichInputText discPer1Binding;
    private RichInputText discAmt1Binding;
    private RichInputText rate1Binding;
    private RichInputText gst1Binding;
    private RichInputText basicRate2Binding;
    private RichInputText basicRate3Binding;
    private RichInputText basicRate4Binding;
    private RichInputText basicRate5Binding;
    private RichInputText discPer2Binding;
    private RichInputText discPer3Binding;
    private RichInputText discPer4Binding;
    private RichInputText discPer5Binding;
    private RichInputText discAmt2Binding;
    private RichInputText discAmt3Binding;
    private RichInputText discAmt4Binding;
    private RichInputText discAmt5Binding;
    private RichInputText rate2Binding;
    private RichInputText rate3Binding;
    private RichInputText rate4Binding;
    private RichInputText rate5Binding;
    private RichInputText gst2Binding;
    private RichInputText gst3Binding;
    private RichInputText gst4Binding;
    private RichInputText gst5Binding;
    private RichInputText basicBinding;
    private RichInputText discBinding;
    private RichInputText discAmtBinding;
    private RichInputText netRateBinding;
    private RichInputText gstBinding;
    private RichInputText processSeqRevNoBinding;
    private RichInputText processSeqNoBinding;
    private RichInputText processCodeBinding;
    private RichInputText qtyBinding;
    private RichInputComboboxListOfValues unitCdBinding;
    private RichTable rateComparisonDtlTableBinding;
    private RichInputText uomBinding;
    private RichInputText itemCodeBinding;
    private RichInputText quotationNoBinding;
    private RichInputText ratesBinding;
    private RichInputText vendorCodeBinding;
    private RichInputText uoMBinding;
    private RichInputText lastPoNoBinding;
    private RichInputText materialRateBinding;
    private RichInputText vendNameBinding;
    private RichInputComboboxListOfValues modifiedByBinding;
    private RichInputDate modifiedDateBinding;
    private RichInputComboboxListOfValues approvedByBinding;
    private RichInputDate comparisonDateBinding;
    private RichTable rateCompTableBinding;
    String Status2="F",Status3="F";
    private RichInputText jobCdBinding;
    private RichInputText jobDtlBinding;
    private RichSelectBooleanCheckbox quotCheck1Binding;
    private RichSelectBooleanCheckbox quotCheck2Binding;
    private RichSelectBooleanCheckbox quotCheck3Binding;
    private RichSelectBooleanCheckbox quotCheck4Binding;
    private RichSelectBooleanCheckbox quotCheck5Binding;

    public RateComparisionChartBean() {
    }

    public void SaveAL(ActionEvent actionEvent) {
        String empcd=(String)ADFUtils.resolveExpression("#{pageFlowScope.empCode}") ; 
        System.out.println("empcd"+empcd);
        ADFUtils.setColumnValue("RateComparisionChartVO1Iterator", "LastUpdatedBy", empcd);
       OperationBinding op = (OperationBinding) ADFUtils.findOperation("rateCheckValidation");
       Object r=op.execute();
       if(op.getResult()=="N")
       {
           ADFUtils.showMessage("Rate cannot be null", 0);
       }
       else
       {
        if(compNoBinding.getValue()==null){
        ADFUtils.findOperation("getCompNo").execute();
        System.out.println("ComppNo1");
        System.out.println("-----*** Create Mode ***-----");

        ADFUtils.findOperation("Commit").execute();
        FacesMessage Message =
            new FacesMessage("Record Save Successfully.CompNo. is " + getCompNoBinding().getValue());
        Message.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);

               }

                else {
                        DCIteratorBinding dci = ADFUtils.findIterator("RateComparisonDtlVO1Iterator");
                        DCIteratorBinding dci1 = ADFUtils.findIterator("RateCompareVO1Iterator");
                        RowSetIterator rsi=dci.getViewObject().createRowSetIterator(null);
                        RowSetIterator rs=dci1.getViewObject().createRowSetIterator(null);   
                            while(rsi.hasNext()) {
                                Row r2 = rsi.next();                                
                                  r2.setAttribute("CompNo", compNoBinding.getValue());
                            }
                            rsi.closeRowSetIterator();
                        while(rs.hasNext()) {
                            Row r2 = rs.next();                                
                              r2.setAttribute("CompNo", compNoBinding.getValue());
                        }
                        rs.closeRowSetIterator();
                        ADFUtils.findOperation("Commit").execute();
                        FacesMessage Message = new FacesMessage("Record Update Successfully.");
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);
                        FacesContext fc = FacesContext.getCurrentInstance();
                        fc.addMessage(null, Message);
        
                    }
       }
    }

    public void setCompNoBinding(RichInputText compNoBinding) {
        this.compNoBinding = compNoBinding;
    }

    public RichInputText getCompNoBinding() {
        return compNoBinding;
    }

    public void ItemCodeVCE(ValueChangeEvent vcl) {
        vcl.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vcl!=null){
          OperationBinding op = (OperationBinding) ADFUtils.findOperation("finMinRateRateComparisionChart");
          Object r=op.execute();
            if(op.getResult()=="N")
            {
                ADFUtils.showMessage("Rate cannot be null", 0);
            }  // ADFUtils.findOperation("QtyRateComparision").execute();
            System.out.println("value is");
        }
      
    }

    public void setBindingQuotType1(RichInputComboboxListOfValues bindingQuotType1) {
        this.bindingQuotType1 = bindingQuotType1;
    }

    public RichInputComboboxListOfValues getBindingQuotType1() {
        return bindingQuotType1;
    }

    public void setBindingQuotType2(RichInputComboboxListOfValues bindingQuotType2) {
        this.bindingQuotType2 = bindingQuotType2;
    }

    public RichInputComboboxListOfValues getBindingQuotType2() {
        return bindingQuotType2;
    }

    public void setBindingQuotType3(RichInputComboboxListOfValues bindingQuotType3) {
        this.bindingQuotType3 = bindingQuotType3;
    }

    public RichInputComboboxListOfValues getBindingQuotType3() {
        return bindingQuotType3;
    }

    public void setBindingQuotType4(RichInputComboboxListOfValues bindingQuotType4) {
        this.bindingQuotType4 = bindingQuotType4;
    }

    public RichInputComboboxListOfValues getBindingQuotType4() {
        return bindingQuotType4;
    }

    public void setBindingQuotType5(RichInputComboboxListOfValues bindingQuotType5) {
        this.bindingQuotType5 = bindingQuotType5;
    }

    public RichInputComboboxListOfValues getBindingQuotType5() {
        return bindingQuotType5;
    }

    public void setBinddingItemCode(RichColumn binddingItemCode) {
        this.binddingItemCode = binddingItemCode;
    }

    public RichColumn getBinddingItemCode() {
        return binddingItemCode;
    }

    public void CompareAL(ActionEvent actionEvent) {
        OperationBinding op = (OperationBinding) ADFUtils.findOperation("rateCheckValidation");
        Object r=op.execute();
        System.out.println("OBJECT VALUE"+r);
        if(op.getResult()=="N")
        {
            ADFUtils.showMessage("Rate cannot be null", 0);
        }
        else
        {
        ADFUtils.findOperation("QtyRateComparision").execute();
        }
    }

    public void CreateInsertRow(ActionEvent actionEvent) {
    ADFUtils.findOperation("CreateInsert").execute();
        //ADFUtils.findOperation("RateSet").execute();
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }
    
    private void cevmodecheck(){
            if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
                cevModeDisableComponent("V");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("C");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("E");
            }
        }
    
 
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
               try {
                   Method method1 =
                       component.getClass().getMethod("setDisabled", boolean.class);
                   if (method1 != null) {
                       method1.invoke(component, valueComponent);
                   }
               } catch (NoSuchMethodException e) {
                   try {
                       Method method =
                           component.getClass().getMethod("setReadOnly", boolean.class);
                       if (method != null) {
                           method.invoke(component, valueComponent);
                       }
                   } catch (Exception e1) {
                       // e.printStackTrace();//silently eat this exception.
                   }


               } catch (Exception e) {
                   // e.printStackTrace();//silently eat this exception.
               }
               List<UIComponent> childComponents = component.getChildren();
               for (UIComponent comp : childComponents) {
                   makeComponentHierarchyReadOnly(comp, valueComponent);
               }
           }
    

    public void cevModeDisableComponent(String mode) {
        //            FacesContext fctx = FacesContext.getCurrentInstance();
        //            ELContext elctx = fctx.getELContext();
        //            Application jsfApp = fctx.getApplication();
        //            //create a ValueExpression that points to the ADF binding layer
        //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
        //            //
        //            ValueExpression valueExpr = exprFactory.createValueExpression(
        //                                         elctx,
        //                                         "#{pageFlowScope.mode=='E'}",
        //                                          Object.class
        //                                         );
        //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
            if (mode.equals("E")) {

                getHeaderEditBinding().setDisabled(true);
                getQtyBinding().setDisabled(true);
                getProcessCodeBinding().setDisabled(true);
                getProcessSeqNoBinding().setDisabled(true);
                getProcessSeqRevNoBinding().setDisabled(true);
                getBasicBinding().setDisabled(true);
                getBasicRate1Binding().setDisabled(true);
                getBasicRate2Binding().setDisabled(true);
                getBasicRate3Binding().setDisabled(true);
                getBasicRate4Binding().setDisabled(true);
                getBasicRate5Binding().setDisabled(true);
                getDiscBinding().setDisabled(true);
                getDiscPer1Binding().setDisabled(true);
                getDiscPer2Binding().setDisabled(true);
                getDiscPer3Binding().setDisabled(true);
                getDiscPer4Binding().setDisabled(true);
                getDiscPer5Binding().setDisabled(true);
                getDiscAmtBinding().setDisabled(true);
                getDiscAmt1Binding().setDisabled(true);
                getDiscAmt2Binding().setDisabled(true);
                getDiscAmt3Binding().setDisabled(true);
                getDiscAmt4Binding().setDisabled(true);
                getDiscAmt5Binding().setDisabled(true);
                getNetRateBinding().setDisabled(true);
                getRate1Binding().setDisabled(true);
                getRate2Binding().setDisabled(true);
                getRate3Binding().setDisabled(true);
                getRate4Binding().setDisabled(true);
                getRate5Binding().setDisabled(true);
                getGstBinding().setDisabled(true);
                getGst1Binding().setDisabled(true);
                getGst2Binding().setDisabled(true);
                getGst3Binding().setDisabled(true);
                getGst4Binding().setDisabled(true);
                getGst5Binding().setDisabled(true);
                getCompNoBinding().setDisabled(true);
                getUnitCdBinding().setDisabled(true);
                getUoMBinding().setDisabled(true);
                getUomBinding().setDisabled(true);
                getItemCodeBinding().setDisabled(true);
                getQuotationNoBinding().setDisabled(true);
                getRatesBinding().setDisabled(true);
                getVendorCodeBinding().setDisabled(true);
                getLastPoNoBinding().setDisabled(true);
                getMaterialRateBinding().setDisabled(true);
                getVendNameBinding().setDisabled(true);
                getHeaderEditBinding().setDisabled(true);
                getModifiedDateBinding().setDisabled(true);
                getComparisonDateBinding().setDisabled(true);
                jobCdBinding.setDisabled(true);
                jobDtlBinding.setDisabled(true);

            } else if (mode.equals("C")) {
                getQtyBinding().setDisabled(true);
                getProcessCodeBinding().setDisabled(true);
                getProcessSeqNoBinding().setDisabled(true);
                getProcessSeqRevNoBinding().setDisabled(true);
                getBasicBinding().setDisabled(true);
                getBasicRate1Binding().setDisabled(true);
                getBasicRate2Binding().setDisabled(true);
                getBasicRate3Binding().setDisabled(true);
                getBasicRate4Binding().setDisabled(true);
                getBasicRate5Binding().setDisabled(true);
                getDiscBinding().setDisabled(true);
                getDiscPer1Binding().setDisabled(true);
                getDiscPer2Binding().setDisabled(true);
                getDiscPer3Binding().setDisabled(true);
                getDiscPer4Binding().setDisabled(true);
                getDiscPer5Binding().setDisabled(true);
                getDiscAmtBinding().setDisabled(true);
                getDiscAmt1Binding().setDisabled(true);
                getDiscAmt2Binding().setDisabled(true);
                getDiscAmt3Binding().setDisabled(true);
                getDiscAmt4Binding().setDisabled(true);
                getDiscAmt5Binding().setDisabled(true);
                getNetRateBinding().setDisabled(true);
                getRate1Binding().setDisabled(true);
                getRate2Binding().setDisabled(true);
                getRate3Binding().setDisabled(true);
                getRate4Binding().setDisabled(true);
                getRate5Binding().setDisabled(true);
                getGstBinding().setDisabled(true);
                getGst1Binding().setDisabled(true);
                getGst2Binding().setDisabled(true);
                getGst3Binding().setDisabled(true);
                getGst4Binding().setDisabled(true);
                getGst5Binding().setDisabled(true);
                getCompNoBinding().setDisabled(true);
                getUnitCdBinding().setDisabled(true);
                getUoMBinding().setDisabled(true);
                getUomBinding().setDisabled(true);
                getItemCodeBinding().setDisabled(true);
                getQuotationNoBinding().setDisabled(true);
                getRatesBinding().setDisabled(true);
                getVendorCodeBinding().setDisabled(true);
                getLastPoNoBinding().setDisabled(true);
                getMaterialRateBinding().setDisabled(true);
                getVendNameBinding().setDisabled(true);
                getHeaderEditBinding().setDisabled(true);
                getModifiedByBinding().setDisabled(true);
                getModifiedDateBinding().setDisabled(true);
                getApprovedByBinding().setDisabled(true);
                getComparisonDateBinding().setDisabled(true);
                jobCdBinding.setDisabled(true);
                jobDtlBinding.setDisabled(true);

            } else if (mode.equals("V")) {
                getCompareMinRateBinding().setDisabled(false);
                getRateChartComparisonBinding().setDisabled(false);

            }
            
        }

    public void editButtonAL(ActionEvent actionEvent) {
        if(modifiedByBinding.getValue()!=null && approvedByBinding.getValue()!=null)
        {
            ADFUtils.showMessage("Modified and Approved Rate Comparison cannot not be edited further.", 2);
            ADFUtils.setEL("#{pageFlowScope.mode}", "V");
        }
        else
        {
        cevmodecheck();
        }
    }

    public void setRateChartComparisonBinding(RichShowDetailItem rateChartComparisonBinding) {
        this.rateChartComparisonBinding = rateChartComparisonBinding;
    }

    public RichShowDetailItem getRateChartComparisonBinding() {
        return rateChartComparisonBinding;
    }

    public void setCompareMinRateBinding(RichShowDetailItem compareMinRateBinding) {
        this.compareMinRateBinding = compareMinRateBinding;
    }

    public RichShowDetailItem getCompareMinRateBinding() {
        return compareMinRateBinding;
    }

    public void setBasicRate1Binding(RichInputText basicRate1Binding) {
        this.basicRate1Binding = basicRate1Binding;
    }

    public RichInputText getBasicRate1Binding() {
        return basicRate1Binding;
    }

    public void setDiscPer1Binding(RichInputText discPer1Binding) {
        this.discPer1Binding = discPer1Binding;
    }

    public RichInputText getDiscPer1Binding() {
        return discPer1Binding;
    }

    public void setDiscAmt1Binding(RichInputText discAmt1Binding) {
        this.discAmt1Binding = discAmt1Binding;
    }

    public RichInputText getDiscAmt1Binding() {
        return discAmt1Binding;
    }

    public void setRate1Binding(RichInputText rate1Binding) {
        this.rate1Binding = rate1Binding;
    }

    public RichInputText getRate1Binding() {
        return rate1Binding;
    }

    public void setGst1Binding(RichInputText gst1Binding) {
        this.gst1Binding = gst1Binding;
    }

    public RichInputText getGst1Binding() {
        return gst1Binding;
    }

    public void setBasicRate2Binding(RichInputText basicRate2Binding) {
        this.basicRate2Binding = basicRate2Binding;
    }

    public RichInputText getBasicRate2Binding() {
        return basicRate2Binding;
    }

    public void setBasicRate3Binding(RichInputText basicRate3Binding) {
        this.basicRate3Binding = basicRate3Binding;
    }

    public RichInputText getBasicRate3Binding() {
        return basicRate3Binding;
    }

    public void setBasicRate4Binding(RichInputText basicRate4Binding) {
        this.basicRate4Binding = basicRate4Binding;
    }

    public RichInputText getBasicRate4Binding() {
        return basicRate4Binding;
    }

    public void setBasicRate5Binding(RichInputText basicRate5Binding) {
        this.basicRate5Binding = basicRate5Binding;
    }

    public RichInputText getBasicRate5Binding() {
        return basicRate5Binding;
    }

    public void setDiscPer2Binding(RichInputText discPer2Binding) {
        this.discPer2Binding = discPer2Binding;
    }

    public RichInputText getDiscPer2Binding() {
        return discPer2Binding;
    }

    public void setDiscPer3Binding(RichInputText discPer3Binding) {
        this.discPer3Binding = discPer3Binding;
    }

    public RichInputText getDiscPer3Binding() {
        return discPer3Binding;
    }

    public void setDiscPer4Binding(RichInputText discPer4Binding) {
        this.discPer4Binding = discPer4Binding;
    }

    public RichInputText getDiscPer4Binding() {
        return discPer4Binding;
    }

    public void setDiscPer5Binding(RichInputText discPer5Binding) {
        this.discPer5Binding = discPer5Binding;
    }

    public RichInputText getDiscPer5Binding() {
        return discPer5Binding;
    }

    public void setDiscAmt2Binding(RichInputText discAmt2Binding) {
        this.discAmt2Binding = discAmt2Binding;
    }

    public RichInputText getDiscAmt2Binding() {
        return discAmt2Binding;
    }

    public void setDiscAmt3Binding(RichInputText discAmt3Binding) {
        this.discAmt3Binding = discAmt3Binding;
    }

    public RichInputText getDiscAmt3Binding() {
        return discAmt3Binding;
    }

    public void setDiscAmt4Binding(RichInputText discAmt4Binding) {
        this.discAmt4Binding = discAmt4Binding;
    }

    public RichInputText getDiscAmt4Binding() {
        return discAmt4Binding;
    }

    public void setDiscAmt5Binding(RichInputText discAmt5Binding) {
        this.discAmt5Binding = discAmt5Binding;
    }

    public RichInputText getDiscAmt5Binding() {
        return discAmt5Binding;
    }

    public void setRate2Binding(RichInputText rate2Binding) {
        this.rate2Binding = rate2Binding;
    }

    public RichInputText getRate2Binding() {
        return rate2Binding;
    }

    public void setRate3Binding(RichInputText rate3Binding) {
        this.rate3Binding = rate3Binding;
    }

    public RichInputText getRate3Binding() {
        return rate3Binding;
    }

    public void setRate4Binding(RichInputText rate4Binding) {
        this.rate4Binding = rate4Binding;
    }

    public RichInputText getRate4Binding() {
        return rate4Binding;
    }

    public void setRate5Binding(RichInputText rate5Binding) {
        this.rate5Binding = rate5Binding;
    }

    public RichInputText getRate5Binding() {
        return rate5Binding;
    }

    public void setGst2Binding(RichInputText gst2Binding) {
        this.gst2Binding = gst2Binding;
    }

    public RichInputText getGst2Binding() {
        return gst2Binding;
    }

    public void setGst3Binding(RichInputText gst3Binding) {
        this.gst3Binding = gst3Binding;
    }

    public RichInputText getGst3Binding() {
        return gst3Binding;
    }

    public void setGst4Binding(RichInputText gst4Binding) {
        this.gst4Binding = gst4Binding;
    }

    public RichInputText getGst4Binding() {
        return gst4Binding;
    }

    public void setGst5Binding(RichInputText gst5Binding) {
        this.gst5Binding = gst5Binding;
    }

    public RichInputText getGst5Binding() {
        return gst5Binding;
    }

    public void setBasicBinding(RichInputText basicBinding) {
        this.basicBinding = basicBinding;
    }

    public RichInputText getBasicBinding() {
        return basicBinding;
    }

    public void setDiscBinding(RichInputText discBinding) {
        this.discBinding = discBinding;
    }

    public RichInputText getDiscBinding() {
        return discBinding;
    }

    public void setDiscAmtBinding(RichInputText discAmtBinding) {
        this.discAmtBinding = discAmtBinding;
    }

    public RichInputText getDiscAmtBinding() {
        return discAmtBinding;
    }

    public void setNetRateBinding(RichInputText netRateBinding) {
        this.netRateBinding = netRateBinding;
    }

    public RichInputText getNetRateBinding() {
        return netRateBinding;
    }

    public void setGstBinding(RichInputText gstBinding) {
        this.gstBinding = gstBinding;
    }

    public RichInputText getGstBinding() {
        return gstBinding;
    }

    public void setProcessSeqRevNoBinding(RichInputText processSeqRevNoBinding) {
        this.processSeqRevNoBinding = processSeqRevNoBinding;
    }

    public RichInputText getProcessSeqRevNoBinding() {
        return processSeqRevNoBinding;
    }

    public void setProcessSeqNoBinding(RichInputText processSeqNoBinding) {
        this.processSeqNoBinding = processSeqNoBinding;
    }

    public RichInputText getProcessSeqNoBinding() {
        return processSeqNoBinding;
    }

    public void setProcessCodeBinding(RichInputText processCodeBinding) {
        this.processCodeBinding = processCodeBinding;
    }

    public RichInputText getProcessCodeBinding() {
        return processCodeBinding;
    }

    public void setQtyBinding(RichInputText qtyBinding) {
        this.qtyBinding = qtyBinding;
    }

    public RichInputText getQtyBinding() {
        return qtyBinding;
    }

    public void setUnitCdBinding(RichInputComboboxListOfValues unitCdBinding) {
        this.unitCdBinding = unitCdBinding;
    }

    public RichInputComboboxListOfValues getUnitCdBinding() {
        return unitCdBinding;
    }

    public void DeleteDialogPopupDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                    {
                    ADFUtils.findOperation("Delete").execute();
                    System.out.println("Record Delete Successfully");
                   
                        FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                        FacesContext fc = FacesContext.getCurrentInstance();  
                        fc.addMessage(null, Message);  
                   
                    }
                AdfFacesContext.getCurrentInstance().addPartialTarget(rateComparisonDtlTableBinding);
        
        }

    public void setRateComparisonDtlTableBinding(RichTable rateComparisonDtlTableBinding) {
        this.rateComparisonDtlTableBinding = rateComparisonDtlTableBinding;
    }

    public RichTable getRateComparisonDtlTableBinding() {
        return rateComparisonDtlTableBinding;
    }

    public void setUomBinding(RichInputText uomBinding) {
        this.uomBinding = uomBinding;
    }

    public RichInputText getUomBinding() {
        return uomBinding;
    }

    public void setItemCodeBinding(RichInputText itemCodeBinding) {
        this.itemCodeBinding = itemCodeBinding;
    }

    public RichInputText getItemCodeBinding() {
        return itemCodeBinding;
    }

    public void setQuotationNoBinding(RichInputText quotationNoBinding) {
        this.quotationNoBinding = quotationNoBinding;
    }

    public RichInputText getQuotationNoBinding() {
        return quotationNoBinding;
    }

    public void setRatesBinding(RichInputText ratesBinding) {
        this.ratesBinding = ratesBinding;
    }

    public RichInputText getRatesBinding() {
        return ratesBinding;
    }

    public void setVendorCodeBinding(RichInputText vendorCodeBinding) {
        this.vendorCodeBinding = vendorCodeBinding;
    }

    public RichInputText getVendorCodeBinding() {
        return vendorCodeBinding;
    }

    public void setUoMBinding(RichInputText uoMBinding) {
        this.uoMBinding = uoMBinding;
    }

    public RichInputText getUoMBinding() {
        return uoMBinding;
    }

    public void setLastPoNoBinding(RichInputText lastPoNoBinding) {
        this.lastPoNoBinding = lastPoNoBinding;
    }

    public RichInputText getLastPoNoBinding() {
        return lastPoNoBinding;
    }

    public void setMaterialRateBinding(RichInputText materialRateBinding) {
        this.materialRateBinding = materialRateBinding;
    }

    public RichInputText getMaterialRateBinding() {
        return materialRateBinding;
    }

    public void setVendNameBinding(RichInputText vendNameBinding) {
        this.vendNameBinding = vendNameBinding;
    }

    public RichInputText getVendNameBinding() {
        return vendNameBinding;
    }

    public void setModifiedByBinding(RichInputComboboxListOfValues modifiedByBinding) {
        this.modifiedByBinding = modifiedByBinding;
    }

    public RichInputComboboxListOfValues getModifiedByBinding() {
        return modifiedByBinding;
    }

    public void setModifiedDateBinding(RichInputDate modifiedDateBinding) {
        this.modifiedDateBinding = modifiedDateBinding;
    }

    public RichInputDate getModifiedDateBinding() {
        return modifiedDateBinding;
    }

    public void setApprovedByBinding(RichInputComboboxListOfValues approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputComboboxListOfValues getApprovedByBinding() {
        return approvedByBinding;
    }

    public void setComparisonDateBinding(RichInputDate comparisonDateBinding) {
        this.comparisonDateBinding = comparisonDateBinding;
    }

    public RichInputDate getComparisonDateBinding() {
        return comparisonDateBinding;
    }

    public void deletePopupDLCompare(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                    {
                    ADFUtils.findOperation("Delete1").execute();
                    System.out.println("Record Delete Successfully");
                   
                        FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                        FacesContext fc = FacesContext.getCurrentInstance();  
                        fc.addMessage(null, Message);  
                   
                    }
                AdfFacesContext.getCurrentInstance().addPartialTarget(rateCompTableBinding);
    }

    public void setRateCompTableBinding(RichTable rateCompTableBinding) {
        this.rateCompTableBinding = rateCompTableBinding;
    }

    public RichTable getRateCompTableBinding() {
        return rateCompTableBinding;
    }
    
    public void onPageLoad() {
        ADFUtils.findOperation("CreateInsert").execute();
        oracle.binding.OperationBinding ob = ADFUtils.findOperation("CheckApprovalAuthoQuotCreation");
        ob.getParamsMap().put("formNm", "RATE_CN_AP");
        ob.getParamsMap().put("authoLim", "PR");
        ob.getParamsMap().put("empcd", ADFUtils.resolveExpression("#{pageFlowScope.empCode}") == null ? "SWE161" : ADFUtils.resolveExpression("#{pageFlowScope.empCode}"));
        ob.getParamsMap().put("unitCd", ADFUtils.resolveExpression("#{pageFlowScope.unitCode}") == null ? "10001" : ADFUtils.resolveExpression("#{pageFlowScope.unitCode}"));
        ob.execute();
        if((ob.getResult() !=null && ob.getResult().equals("N")))
        {   
            Status2="T";
        }
        
        oracle.binding.OperationBinding ob1 = ADFUtils.findOperation("CheckApprovalAuthoQuotCreation");
        ob1.getParamsMap().put("formNm", "RATE_EN_AP");
        ob1.getParamsMap().put("authoLim", "PR");
        ob1.getParamsMap().put("empcd", ADFUtils.resolveExpression("#{pageFlowScope.empCode}") == null ? "SWE161" : ADFUtils.resolveExpression("#{pageFlowScope.empCode}"));
        ob1.getParamsMap().put("unitCd", ADFUtils.resolveExpression("#{pageFlowScope.unitCode}") == null ? "10001" : ADFUtils.resolveExpression("#{pageFlowScope.unitCode}"));
        ob1.execute();
        if((ob1.getResult() !=null && ob1.getResult().equals("N")))
        {   
            Status3="T";
        }

        if(Status2.equalsIgnoreCase("T") && Status3.equalsIgnoreCase("F"))
        {
            Row row=(Row)ADFUtils.evaluateEL("#{bindings.RateComparisionChartVO1Iterator.currentRow}");
            row.setAttribute("QuotType", "D");
            
        }else if(Status2.equalsIgnoreCase("F") && Status3.equalsIgnoreCase("T"))
        {
            Row row=(Row)ADFUtils.evaluateEL("#{bindings.RateComparisionChartVO1Iterator.currentRow}");
            row.setAttribute("QuotType", "E");
        }
        
       
    }

    public void setJobCdBinding(RichInputText jobCdBinding) {
        this.jobCdBinding = jobCdBinding;
    }

    public RichInputText getJobCdBinding() {
        return jobCdBinding;
    }

    public void setJobDtlBinding(RichInputText jobDtlBinding) {
        this.jobDtlBinding = jobDtlBinding;
    }

    public RichInputText getJobDtlBinding() {
        return jobDtlBinding;
    }

    public void ApprovedByVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        oracle.binding.OperationBinding ob = ADFUtils.findOperation("checkApprovalStatus");
        ob.getParamsMap().put("formNm", "RCO");
        ob.getParamsMap().put("authoLim", "AP");
        ob.getParamsMap().put("empcd", vce.getNewValue().toString());
        ob.execute();
        System.out.println(" EMP CD"+vce.getNewValue().toString());
        if((ob.getResult() !=null && ob.getResult().equals("Y")))
        {
           Row row=(Row)ADFUtils.evaluateEL("#{bindings.RateComparisionChartVO1Iterator.currentRow}");
           row.setAttribute("ApproveBy", null);
           ADFUtils.showMessage("You have no authority to approve this Rate Comparison.",0);
        }
    }

    public void setQuotCheck1Binding(RichSelectBooleanCheckbox quotCheck1Binding) {
        this.quotCheck1Binding = quotCheck1Binding;
    }

    public RichSelectBooleanCheckbox getQuotCheck1Binding() {
        return quotCheck1Binding;
    }

    public void setQuotCheck2Binding(RichSelectBooleanCheckbox quotCheck2Binding) {
        this.quotCheck2Binding = quotCheck2Binding;
    }

    public RichSelectBooleanCheckbox getQuotCheck2Binding() {
        return quotCheck2Binding;
    }

    public void setQuotCheck3Binding(RichSelectBooleanCheckbox quotCheck3Binding) {
        this.quotCheck3Binding = quotCheck3Binding;
    }

    public RichSelectBooleanCheckbox getQuotCheck3Binding() {
        return quotCheck3Binding;
    }

    public void setQuotCheck4Binding(RichSelectBooleanCheckbox quotCheck4Binding) {
        this.quotCheck4Binding = quotCheck4Binding;
    }

    public RichSelectBooleanCheckbox getQuotCheck4Binding() {
        return quotCheck4Binding;
    }

    public void setQuotCheck5Binding(RichSelectBooleanCheckbox quotCheck5Binding) {
        this.quotCheck5Binding = quotCheck5Binding;
    }

    public RichSelectBooleanCheckbox getQuotCheck5Binding() {
        return quotCheck5Binding;
    }

    public void checkQuot1VCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        System.out.println("CHECK QUOT"+vce.getNewValue());
        if(vce.getNewValue().equals(true))
        {
            quotCheck2Binding.setDisabled(true);
            quotCheck3Binding.setDisabled(true);
            quotCheck4Binding.setDisabled(true);
            quotCheck5Binding.setDisabled(true);
        }
        else if(vce.getNewValue().equals(false))
        {
            {
                quotCheck2Binding.setDisabled(false);
                quotCheck3Binding.setDisabled(false);
                quotCheck4Binding.setDisabled(false);
                quotCheck5Binding.setDisabled(false);
            }
        }
    }

    public void checkQuot2VCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce.getNewValue().equals(true))
        {
            quotCheck1Binding.setDisabled(true);
            quotCheck3Binding.setDisabled(true);
            quotCheck4Binding.setDisabled(true);
            quotCheck5Binding.setDisabled(true);
        }
        else if(vce.getNewValue().equals(false))
        {
                quotCheck1Binding.setDisabled(false);
                quotCheck3Binding.setDisabled(false);
                quotCheck4Binding.setDisabled(false);
                quotCheck5Binding.setDisabled(false);
        }  
    }

    public void checkQuot3VCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce.getNewValue().equals(true))
        {
            quotCheck2Binding.setDisabled(true);
            quotCheck1Binding.setDisabled(true);
            quotCheck4Binding.setDisabled(true);
            quotCheck5Binding.setDisabled(true);
        }
        else if(vce.getNewValue().equals(false))
        {
                quotCheck2Binding.setDisabled(false);
                quotCheck1Binding.setDisabled(false);
                quotCheck4Binding.setDisabled(false);
                quotCheck5Binding.setDisabled(false);
        }
    }

    public void checkQuot4VCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce.getNewValue().equals(true))
        {
            quotCheck2Binding.setDisabled(true);
            quotCheck3Binding.setDisabled(true);
            quotCheck1Binding.setDisabled(true);
            quotCheck5Binding.setDisabled(true);
        }
        else if(vce.getNewValue().equals(false))
        {
                quotCheck2Binding.setDisabled(false);
                quotCheck3Binding.setDisabled(false);
                quotCheck1Binding.setDisabled(false);
                quotCheck5Binding.setDisabled(false);
        }
    }

    public void checkQuot5VCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce.getNewValue().equals(true))
        {
            quotCheck2Binding.setDisabled(true);
            quotCheck3Binding.setDisabled(true);
            quotCheck4Binding.setDisabled(true);
            quotCheck1Binding.setDisabled(true);
        }
        else if(vce.getNewValue().equals(false))
        {
                quotCheck2Binding.setDisabled(false);
                quotCheck3Binding.setDisabled(false);
                quotCheck4Binding.setDisabled(false);
                quotCheck1Binding.setDisabled(false);
        }
    }
}

