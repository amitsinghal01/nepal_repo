package terms.mtl.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.binding.OperationBinding;

public class RegularizeMrnWithPoBean {
    private RichPanelHeader getMyPageRootComponent;
    private RichOutputText getBindingOutputText;
    private RichButton editBinding;
    private RichInputText itemCodeBinding;
    private RichInputText poAmendNoBinding;
    private RichInputText acceptedQtyBinding;
    private RichInputText poQtyBinding;

    public RegularizeMrnWithPoBean() {
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setGetBindingOutputText(RichOutputText getBindingOutputText) {
        this.getBindingOutputText = getBindingOutputText;
    }

    public RichOutputText getGetBindingOutputText() {
        cevmodecheck();
        return getBindingOutputText;
    }

    public void setEditBinding(RichButton editBinding) {
        this.editBinding = editBinding;
    }

    public RichButton getEditBinding() {
        return editBinding;
    }

    public void EditButtonAL(ActionEvent actionEvent) {
        
//        else{
        OperationBinding op=ADFUtils.findOperation("checkPoInBillDetail");
        Object ob=op.execute();
        Integer rst = (Integer) op.getResult();
        System.out.println("result is==>>>"+rst);
        if(rst!=null && rst==1){
            ADFUtils.showMessage("You cannot Modify this MRN as Bill Has Passed.", 2);
            ADFUtils.setEL("#{pageFlowScope.mode}", "V");
            cevmodecheck();
        }
        else{
            cevmodecheck();
        }
//        }
        
    }
    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }

    public void cevModeDisableComponent(String mode) {
        if (mode.equals("E")) {
            getEditBinding().setDisabled(true);
            getItemCodeBinding().setDisabled(true);
            getPoAmendNoBinding().setDisabled(true);
            getAcceptedQtyBinding().setDisabled(true);
            poQtyBinding.setDisabled(true);
        }
        if (mode.equals("C")) {
            
        }
        if (mode.equals("V")) {


        }

    }

    public void SaveAl(ActionEvent actionEvent) {
        OperationBinding op1=ADFUtils.findOperation("compareQtyRegMRN");
        Object ob1=op1.execute();
        System.out.println("opi result1===>>"+op1.getResult());
//        System.out.println("opi result2===>>"+op1.getResult().toString().substring(1));
//        System.out.println("opi result3===>>"+op1.getResult().toString().substring(1,7));
        if(op1.getResult()!=null && !op1.getResult().toString().equalsIgnoreCase("T")){
            ADFUtils.showMessage("Cannot Regularize this MRN as Accept Qty is more than PO Balance Qty."
                               , 0);
        }
        else{
            ADFUtils.findOperation("Commit").execute();
        ADFUtils.showMessage("Record Updated Successfully.", 2);
        }
    }

    public void setItemCodeBinding(RichInputText itemCodeBinding) {
        this.itemCodeBinding = itemCodeBinding;
    }

    public RichInputText getItemCodeBinding() {
        return itemCodeBinding;
    }

    public void setPoAmendNoBinding(RichInputText poAmendNoBinding) {
        this.poAmendNoBinding = poAmendNoBinding;
    }

    public RichInputText getPoAmendNoBinding() {
        return poAmendNoBinding;
    }

    public void setAcceptedQtyBinding(RichInputText acceptedQtyBinding) {
        this.acceptedQtyBinding = acceptedQtyBinding;
    }

    public RichInputText getAcceptedQtyBinding() {
        return acceptedQtyBinding;
    }

    public void poNoVCL(ValueChangeEvent valueChangeEvent) {
       
    }


    public void setPoQtyBinding(RichInputText poQtyBinding) {
        this.poQtyBinding = poQtyBinding;
    }

    public RichInputText getPoQtyBinding() {
        return poQtyBinding;
    }
}
