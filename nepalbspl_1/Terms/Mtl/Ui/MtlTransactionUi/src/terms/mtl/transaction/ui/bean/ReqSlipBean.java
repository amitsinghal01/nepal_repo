package terms.mtl.transaction.ui.bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import java.io.OutputStream;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.application.ViewHandler;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import javax.faces.validator.ValidatorException;

import oracle.adf.model.BindingContext;
import oracle.adf.view.rich.component.rich.RichDialog;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;

import oracle.adf.view.rich.component.rich.input.RichSelectBooleanCheckbox;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.util.ResetUtils;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.Row;

import org.apache.myfaces.trinidad.event.SelectionEvent;
import org.apache.myfaces.trinidad.model.UploadedFile;

public class ReqSlipBean {


    private RichInputText bindJcNo;
    private RichButton bindPopulate;
    private RichPopup bindpopulate;
    private RichTable createReqSlipTableBinding;
    private RichTable jobCardDetailTableBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichOutputText bindingOutputText;
    private RichButton detailcreateBinding;
    private RichButton detaildeleteBinding;
    private RichInputText slipNumberBinding;
    private RichInputDate slipDateBinding;
    private RichInputComboboxListOfValues authByBinding;
    private RichInputComboboxListOfValues reqByBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText purIndentBinding;
    private RichInputText issueSlipNoBinding;
    private RichSelectOneChoice statusBinding;
    private RichInputDate authDateBinding;
    private RichInputText pckngStdBinding;
    private RichInputText reqQtyBinding;
    private RichInputText reqUomBinding;
    private RichInputText stockQtyBinding;
    private RichInputText wipStockQtyBinding;
    private RichInputText bookQtyBinding;
    private RichInputText indentReferQtyBinding;
    private RichInputText jobCardQtyBinding;
    private RichInputText prodCodeBinding;
    private RichButton headerEditBinding;
    private RichButton jobCardButtonBinding;
    private RichInputComboboxListOfValues procCodeBinding;
    private RichInputText issueQtyBinding;
    private RichInputText bindItemDesc;
    private RichInputText revNoBinding;
    private RichInputText unitDescBinding;
    private RichInputText fromDeptNameBinding;
    private RichInputText authByNameBinding;
    private RichInputText reqByNameBinding;
    private RichButton saveButtonBinding;
    private RichButton saveandCloseButtonBinding;
    private RichInputComboboxListOfValues itemCdBinding;
    private RichInputComboboxListOfValues processCodeBinding;
    private RichInputComboboxListOfValues fromDepartmenetBinding;
    private RichShowDetailItem requiTabOneBinding;
    private RichShowDetailItem requiItemTabBinding;
    private RichColumn itemStBinding;
    private RichSelectBooleanCheckbox statusItemBinding;
    private RichPopup popupQtyBinding;
    private RichSelectBooleanCheckbox indentChkBoxBinding;
    private RichShowDetailItem reqTab1Binding;
    private RichShowDetailItem reqTab2Binding;
    private RichSelectOneChoice itemCapRevBinding;

    private String mode_chekc="C";
    private RichInputComboboxListOfValues jobCodeBinding;
    private RichInputText specificationBinding;

    public ReqSlipBean() {
    }

    public void SaveReqSlipAL(ActionEvent actionEvent) {

        String empcd=(String)ADFUtils.resolveExpression("#{pageFlowScope.empCode}") ; 
        System.out.println("empcd"+empcd);
        ADFUtils.setLastUpdatedBy("RequisitionSlipHeaderVO1Iterator", "LastUpdatedBy");
        OperationBinding op = ADFUtils.findOperation("getReqSlipNo");
        Object rst = op.execute();

        System.out.println("--------Commit-------");
        System.out.println("value aftr getting result--->?" + rst);


        if (rst.toString() != null && rst.toString() != "") {
            if (op.getErrors().isEmpty()) {
                ADFUtils.findOperation("Commit").execute();
                FacesMessage Message =
                    new FacesMessage("Record Saved Successfully.Requisition Slip No is " + rst + ".");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
                // ADFUtils.findOperation("CreateInsert2").execute();
            }
        }

        if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ") {
            if (op.getErrors().isEmpty()) {
                ADFUtils.findOperation("Commit").execute();
                FacesMessage Message = new FacesMessage("Record Updated Successfully.");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
                // ADFUtils.findOperation("CreateInsert2").execute();
            }


        }
       
    }


    /*   public void PopulateReqJobCardAL(ActionEvent actionEvent) {

       //getBindPopulate().cancel();

           // getVendorAddressPopup().cancel();


        ADFUtils.findOperation("PopulateReqJobCardAL").execute();


             getBindpopulate().cancel();


        } */
    // Add event code here...
    public void updateStockQuantAL(ActionEvent actionEvent) {
        OperationBinding op = ADFUtils.findOperation("getReqStockQty");
        Object rst = op.execute();
    }


    public void setBindJcNo(RichInputText bindJcNo) {
        this.bindJcNo = bindJcNo;
    }

    public RichInputText getBindJcNo() {
        return bindJcNo;
    }


    public void setBindpopulate(RichPopup bindpopulate) {
        this.bindpopulate = bindpopulate;
    }

    public RichPopup getBindpopulate() {
        return bindpopulate;
    }

    public BindingContainer getBindings() {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
    }


    public void populateItemCdAL(ActionEvent actionEvent) {
        
        try {
            OperationBinding opp = ADFUtils.findOperation("PopulateReqJobCardAL");
            Object rst = opp.execute();
            if (rst != null && rst.equals("N")) {
                {
                    FacesMessage Message = new FacesMessage("No Item Found For this Job Card Number!");
                    Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);

                }
            } else {
                fromDepartmenetBinding.setDisabled(true);
//                getSaveButtonBinding().setDisabled(false);
//                AdfFacesContext.getCurrentInstance().addPartialTarget(saveButtonBinding);


            }
            getBindpopulate().cancel();

            try {
                Row rr = (Row) ADFUtils.evaluateEL("#{bindings.RequisitionSlipDetailVO1Iterator.currentRow}");
                String itemcd = (String) (rr.getAttribute("ItemCd") != null ? rr.getAttribute("ItemCd") : "N");
                if (!itemcd.equalsIgnoreCase("N")) {
                    OperationBinding op1 = ADFUtils.findOperation("getReqStockQty");
                    Object rst1 = op1.execute();
                }

            } catch (Exception e) {
                System.out.println("error");
                e.printStackTrace();
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
       
    }
    

    public void reqQtyVCL(ValueChangeEvent valueChangeEvent) {
        if(valueChangeEvent!=null){
             valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
               System.out.println("in the if block of qty&&&&&&");
                       oracle.jbo.domain.Number reqQty = (oracle.jbo.domain.Number)reqQtyBinding.getValue();
                      System.out.println("");
                       oracle.jbo.domain.Number stk = (oracle.jbo.domain.Number)stockQtyBinding.getValue();
                   if(reqQty.intValue()>stk.intValue())
                   {
                       System.out.println("in the if block of stk comparison**********"+stockQtyBinding.getValue());
                           RichPopup.PopupHints hints = new RichPopup.PopupHints();
                           getPopupQtyBinding().show(hints);
                       
                       
                       }

    }
    }
    

    public void setCreateReqSlipTableBinding(RichTable createReqSlipTableBinding) {
        this.createReqSlipTableBinding = createReqSlipTableBinding;
    }

    public RichTable getCreateReqSlipTableBinding() {
        return createReqSlipTableBinding;
    }


    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(createReqSlipTableBinding);
    }


    public void setJobCardDetailTableBinding(RichTable jobCardDetailTableBinding) {
        this.jobCardDetailTableBinding = jobCardDetailTableBinding;
    }

    public RichTable getJobCardDetailTableBinding() {
        return jobCardDetailTableBinding;
    }

    public void jobDialogPopupDeleteDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete1").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(jobCardDetailTableBinding);
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {

        cevmodecheck();
        return bindingOutputText;
    }

    public void setDetailcreateBinding(RichButton detailcreateBinding) {
        this.detailcreateBinding = detailcreateBinding;
    }

    public RichButton getDetailcreateBinding() {
        return detailcreateBinding;
    }

    public void setDetaildeleteBinding(RichButton detaildeleteBinding) {
        this.detaildeleteBinding = detaildeleteBinding;
    }

    public RichButton getDetaildeleteBinding() {
        return detaildeleteBinding;
    }

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }

    public void EditButtonAL(ActionEvent actionEvent) {
     
     if(ADFUtils.evaluateEL("#{bindings.ActiveSt.inputValue}").equals("C"))
     {
         System.out.println("in the status chk loop****");
             ADFUtils.showMessage("This Requisition Slip is cancelled.You Cannot Update",2);
             ADFUtils.setEL("#{pageFlowScope.mode}", "V");
             cevmodecheck();
         }
     
        OperationBinding opp = ADFUtils.findOperation("getCancelReqSlip");
        Object rst = opp.execute();
        System.out.println("Result is****of methdo execute" + rst);
        if (rst != null && rst.equals("N"))
        {
            System.out.println("in the if block whn row is presnt");
            ADFUtils.showMessage("This Is Approved Requisition Slip.For this document issue slip is generated. You can not modified this record,Except Item(Capital/Revenue) ",
                                 2);
            itemCapRevBinding.setDisabled(false);
//            ADFUtils.setEL("#{pageFlowScope.mode}", "V");
//            cevmodecheck();

        } else if (rst != null && rst.equals("Y"))
        {
            System.out.println("in the else if block**%%%%%%");
            ADFUtils.showMessage("This Is Approved Requisition Slip You can not modified this record,Except Item(Capital/Revenue). ",
                                 2);
//            statusItemBinding.setDisabled(false);
            itemCapRevBinding.setDisabled(false);


        }

        else if(rst!=null && rst.equals("Z")) 
        {
            System.out.println("in the else block**&&");
            cevmodecheck();
        }
        
    }
   

    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }

    public void cevModeDisableComponent(String mode) {
        if (mode.equals("E")) {
            mode_chekc="E";
//            getJobCodeBinding().setDisabled(true);
            getReqTab1Binding().setDisabled(false);
            specificationBinding.setDisabled(true);
            getReqTab2Binding().setDisabled(false);
            getItemCapRevBinding().setDisabled(false);
            getSlipNumberBinding().setDisabled(true);
            getSlipDateBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
            getAuthDateBinding().setDisabled(true);
            getDetailcreateBinding().setDisabled(false);
            getPurIndentBinding().setDisabled(true);
            getIssueSlipNoBinding().setDisabled(true);
            getStatusBinding().setDisabled(true);
            getPckngStdBinding().setDisabled(true);
            getStockQtyBinding().setDisabled(true);
            getWipStockQtyBinding().setDisabled(true);
            getBookQtyBinding().setDisabled(true);
            getIndentReferQtyBinding().setDisabled(true);
            getJobCardButtonBinding().setDisabled(false);
            //getReqByBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            getBindItemDesc().setDisabled(true);
            getReqUomBinding().setDisabled(true);
            getIssueQtyBinding().setDisabled(true);
            getRevNoBinding().setDisabled(true);
            getFromDeptNameBinding().setDisabled(true);
            // getProcessCodeBinding().setDisabled(true);
            fromDepartmenetBinding.setDisabled(true);
            // getReqQtyBinding().setDisabled(true);
            //getItemCdBinding().setDisabled(true);
        }
        if (mode.equals("C")) {
            mode_chekc ="C";
//            getJobCodeBinding().setDisabled(true);
            getStatusItemBinding().setDisabled(true);
            specificationBinding.setDisabled(true);
            //getSaveButtonBinding().setDisabled(true);
            getReqTab1Binding().setDisabled(false);
            getReqTab2Binding().setDisabled(false);
            getItemCapRevBinding().setDisabled(false);
            getAuthDateBinding().setDisabled(true);
            getAuthByBinding().setDisabled(true);
            getSlipNumberBinding().setDisabled(true);
            getSlipDateBinding().setDisabled(true);
            getPurIndentBinding().setDisabled(true);
            getStatusBinding().setDisabled(true);
            getProcessCodeBinding().setDisabled(true);
            getPckngStdBinding().setDisabled(true);
            getBindItemDesc().setDisabled(true);
            getStockQtyBinding().setDisabled(true);
            getWipStockQtyBinding().setDisabled(true);
            getBookQtyBinding().setDisabled(true);
            getIssueSlipNoBinding().setDisabled(true);
            getIndentReferQtyBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            getReqUomBinding().setDisabled(true);
            getIssueQtyBinding().setDisabled(true);
            getRevNoBinding().setDisabled(true);
            getReqByBinding().setDisabled(true);
            getFromDeptNameBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
        }
        if (mode.equals("V")) {
          
            mode_chekc="V";
          getReqTab1Binding().setDisabled(false);
          getReqTab2Binding().setDisabled(false);
            getDetailcreateBinding().setDisabled(true);
            getJobCardButtonBinding().setDisabled(true);
          //  getSaveButtonBinding().setDisabled(true);

        }

    }

    public void setSlipNumberBinding(RichInputText slipNumberBinding) {
        this.slipNumberBinding = slipNumberBinding;
    }

    public RichInputText getSlipNumberBinding() {
        return slipNumberBinding;
    }

    public void setSlipDateBinding(RichInputDate slipDateBinding) {
        this.slipDateBinding = slipDateBinding;
    }

    public RichInputDate getSlipDateBinding() {
        return slipDateBinding;
    }

    public void setAuthByBinding(RichInputComboboxListOfValues authByBinding) {
        this.authByBinding = authByBinding;
    }

    public RichInputComboboxListOfValues getAuthByBinding() {
        return authByBinding;
    }

    public void setReqByBinding(RichInputComboboxListOfValues reqByBinding) {
        this.reqByBinding = reqByBinding;
    }

    public RichInputComboboxListOfValues getReqByBinding() {
        return reqByBinding;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setPurIndentBinding(RichInputText purIndentBinding) {
        this.purIndentBinding = purIndentBinding;
    }

    public RichInputText getPurIndentBinding() {
        return purIndentBinding;
    }

    public void setIssueSlipNoBinding(RichInputText issueSlipNoBinding) {
        this.issueSlipNoBinding = issueSlipNoBinding;
    }

    public RichInputText getIssueSlipNoBinding() {
        return issueSlipNoBinding;
    }

    public void setStatusBinding(RichSelectOneChoice statusBinding) {
        this.statusBinding = statusBinding;
    }

    public RichSelectOneChoice getStatusBinding() {
        return statusBinding;
    }

    public void setAuthDateBinding(RichInputDate authDateBinding) {
        this.authDateBinding = authDateBinding;
    }

    public RichInputDate getAuthDateBinding() {
        return authDateBinding;
    }

    public void setPckngStdBinding(RichInputText pckngStdBinding) {
        this.pckngStdBinding = pckngStdBinding;
    }

    public RichInputText getPckngStdBinding() {
        return pckngStdBinding;
    }

    public void setReqQtyBinding(RichInputText reqQtyBinding) {
        this.reqQtyBinding = reqQtyBinding;
    }

    public RichInputText getReqQtyBinding() {
        return reqQtyBinding;
    }

    public void setReqUomBinding(RichInputText reqUomBinding) {
        this.reqUomBinding = reqUomBinding;
    }

    public RichInputText getReqUomBinding() {
        return reqUomBinding;
    }

    public void setStockQtyBinding(RichInputText stockQtyBinding) {
        this.stockQtyBinding = stockQtyBinding;
    }

    public RichInputText getStockQtyBinding() {
        return stockQtyBinding;
    }

    public void setWipStockQtyBinding(RichInputText wipStockQtyBinding) {
        this.wipStockQtyBinding = wipStockQtyBinding;
    }

    public RichInputText getWipStockQtyBinding() {
        return wipStockQtyBinding;
    }

    public void setBookQtyBinding(RichInputText bookQtyBinding) {
        this.bookQtyBinding = bookQtyBinding;
    }

    public RichInputText getBookQtyBinding() {
        return bookQtyBinding;
    }

    public void setIndentReferQtyBinding(RichInputText indentReferQtyBinding) {
        this.indentReferQtyBinding = indentReferQtyBinding;
    }

    public RichInputText getIndentReferQtyBinding() {
        return indentReferQtyBinding;
    }


    public void setJobCardQtyBinding(RichInputText jobCardQtyBinding) {
        this.jobCardQtyBinding = jobCardQtyBinding;
    }

    public RichInputText getJobCardQtyBinding() {
        return jobCardQtyBinding;
    }

    public void setProdCodeBinding(RichInputText prodCodeBinding) {
        this.prodCodeBinding = prodCodeBinding;
    }

    public RichInputText getProdCodeBinding() {
        return prodCodeBinding;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setJobCardButtonBinding(RichButton jobCardButtonBinding) {
        this.jobCardButtonBinding = jobCardButtonBinding;
    }

    public RichButton getJobCardButtonBinding() {
        return jobCardButtonBinding;
    }

    public void setProcCodeBinding(RichInputComboboxListOfValues procCodeBinding) {
        this.procCodeBinding = procCodeBinding;
    }

    public RichInputComboboxListOfValues getProcCodeBinding() {
        return procCodeBinding;
    }

    public void setIssueQtyBinding(RichInputText issueQtyBinding) {
        this.issueQtyBinding = issueQtyBinding;
    }

    public RichInputText getIssueQtyBinding() {
        return issueQtyBinding;
    }

    public void setBindItemDesc(RichInputText bindItemDesc) {
        this.bindItemDesc = bindItemDesc;
    }

    public RichInputText getBindItemDesc() {
        return bindItemDesc;
    }

    public void setRevNoBinding(RichInputText revNoBinding) {
        this.revNoBinding = revNoBinding;
    }

    public RichInputText getRevNoBinding() {
        return revNoBinding;
    }

    public void setUnitDescBinding(RichInputText unitDescBinding) {
        this.unitDescBinding = unitDescBinding;
    }

    public RichInputText getUnitDescBinding() {
        return unitDescBinding;
    }

    public void setFromDeptNameBinding(RichInputText fromDeptNameBinding) {
        this.fromDeptNameBinding = fromDeptNameBinding;
    }

    public RichInputText getFromDeptNameBinding() {
        return fromDeptNameBinding;
    }

    public void setAuthByNameBinding(RichInputText authByNameBinding) {
        this.authByNameBinding = authByNameBinding;
    }

    public RichInputText getAuthByNameBinding() {
        return authByNameBinding;
    }

    public void setReqByNameBinding(RichInputText reqByNameBinding) {
        this.reqByNameBinding = reqByNameBinding;
    }

    public RichInputText getReqByNameBinding() {
        return reqByNameBinding;
    }

    public void setSaveButtonBinding(RichButton saveButtonBinding) {
        this.saveButtonBinding = saveButtonBinding;
    }

    public RichButton getSaveButtonBinding() {
        return saveButtonBinding;
    }

    public void setSaveandCloseButtonBinding(RichButton saveandCloseButtonBinding) {
        this.saveandCloseButtonBinding = saveandCloseButtonBinding;
    }

    public RichButton getSaveandCloseButtonBinding() {
        return saveandCloseButtonBinding;
    }

    public void setItemCdBinding(RichInputComboboxListOfValues itemCdBinding) {
        this.itemCdBinding = itemCdBinding;
    }

    public RichInputComboboxListOfValues getItemCdBinding() {
        return itemCdBinding;
    }

    public void setProcessCodeBinding(RichInputComboboxListOfValues processCodeBinding) {
        this.processCodeBinding = processCodeBinding;
    }

    public RichInputComboboxListOfValues getProcessCodeBinding() {
        return processCodeBinding;
    }

    public void setFromDepartmenetBinding(RichInputComboboxListOfValues fromDepartmenetBinding) {
        this.fromDepartmenetBinding = fromDepartmenetBinding;
    }

    public RichInputComboboxListOfValues getFromDepartmenetBinding() {
        return fromDepartmenetBinding;
    }

    public void detailCreateButtonAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert3").execute();
        //saveButtonBinding.
      //  getSaveButtonBinding().setDisabled(false);
        fromDepartmenetBinding.setDisabled(true);
 //       AdfFacesContext.getCurrentInstance().addPartialTarget(saveButtonBinding);

        // Add event code here...
    }

    public void itemVCL(ValueChangeEvent valueChangeEvent) {
        System.out.println("inside bean of item vcl");
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        System.out.println("inside entry");
        OperationBinding op2 = ADFUtils.findOperation("getReqStockQty");
        System.out.println("after stk%%******");
        OperationBinding op3 = ADFUtils.findOperation("getWipStockQty");
        System.out.println("after wipis%%******");

        Object rst2 = op2.execute();
        Object rst3 = op3.execute();
        System.out.println("inside exit");
    }

 

    public void setRequiTabOneBinding(RichShowDetailItem requiTabOneBinding) {
        this.requiTabOneBinding = requiTabOneBinding;
    }

   

    public void setRequiItemTabBinding(RichShowDetailItem requiItemTabBinding) {
        this.requiItemTabBinding = requiItemTabBinding;
    }

   

//    public void checkAuthorityVCE(ValueChangeEvent valueChangeEvent) {
//        if (valueChangeEvent != null) {
//            valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
//            oracle.binding.OperationBinding op = ADFUtils.findOperation("CheckAuthorityReqSlip");
//            op.getParamsMap().put("unitCd", unitCodeBinding.getValue());
//            op.getParamsMap().put("EmpCd", authByBinding.getValue());
//            op.execute();
//
//        }
//    }
//    
    
    public void authorisedByVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());            
                     OperationBinding ob = ADFUtils.findOperation("checkApprovalStatus");
                    ob.getParamsMap().put("formNm", "REQ");
                    ob.getParamsMap().put("authoLim", "AP");
                    ob.getParamsMap().put("empcd", vce.getNewValue().toString());
                    ob.execute();
                    System.out.println(" EMP CD"+vce.getNewValue().toString());
                    if((ob.getResult() !=null && ob.getResult().equals("Y")))
                    {
                        Row row=(Row)ADFUtils.evaluateEL("#{bindings.RequisitionSlipHeaderVO1Iterator.currentRow}");
                        row.setAttribute("AuthBy", null);
                        ADFUtils.showMessage("You have no authority to approve this Requisition Slip.",0);
                    }

        }


    public void setStatusItemBinding(RichSelectBooleanCheckbox statusItemBinding) {
        this.statusItemBinding = statusItemBinding;
    }

    public RichSelectBooleanCheckbox getStatusItemBinding() {
        return statusItemBinding;
    }

    public void reqQtyDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome() == DialogEvent.Outcome.yes) {
     OperationBinding op = ADFUtils.findOperation("getreqchk");
     Object rst = op.execute();
        System.out.println("in the popup of req qty in method calling^^^^ *************");
     
     
        
 }
        if (dialogEvent.getOutcome() == DialogEvent.Outcome.no) {
            System.out.println("In the popup of req qty in no**********");
            OperationBinding op1 = ADFUtils.findOperation("getreqchkbox");
            Object rst = op1.execute();
               System.out.println("in the popup of req qty in no  in method calling^^^^ *************");

        }
    }
 

    public void setPopupQtyBinding(RichPopup popupQtyBinding) {
        this.popupQtyBinding = popupQtyBinding;
    }

    public RichPopup getPopupQtyBinding() {
        return popupQtyBinding;
    }

    public void setIndentChkBoxBinding(RichSelectBooleanCheckbox indentChkBoxBinding) {
        this.indentChkBoxBinding = indentChkBoxBinding;
    }

    public RichSelectBooleanCheckbox getIndentChkBoxBinding() {
        return indentChkBoxBinding;
    }

    public void setReqTab1Binding(RichShowDetailItem reqTab1Binding) {
        this.reqTab1Binding = reqTab1Binding;
    }

    public RichShowDetailItem getReqTab1Binding() {
        return reqTab1Binding;
    }

    public void setReqTab2Binding(RichShowDetailItem reqTab2Binding) {
        this.reqTab2Binding = reqTab2Binding;
    }

    public RichShowDetailItem getReqTab2Binding() {
        return reqTab2Binding;
    }

    public void setItemCapRevBinding(RichSelectOneChoice itemCapRevBinding) {
        this.itemCapRevBinding = itemCapRevBinding;
    }

    public RichSelectOneChoice getItemCapRevBinding() {
        return itemCapRevBinding;
    }

    public void cancelButtonAL(ActionEvent actionEvent) {
        if(statusBinding.toString().equalsIgnoreCase("C"))
        {
            System.out.println("in the cancel al***");
                ADFUtils.findOperation("Commit").execute();
            
            }
    }

    public String cancelAction() 
    {
        System.out.println("in the cancel action statusBinding.toString()***"+statusBinding.toString());
System.out.println("ADFUtils.evaluateEL(\"#{bindings.ActiveSt.inputValue}\")"+ADFUtils.evaluateEL("#{bindings.ActiveSt.inputValue}"));
        System.out.println("mode_chekc***"+mode_chekc);

       
        if(mode_chekc.equals("V") && ADFUtils.evaluateEL("#{bindings.ActiveSt.inputValue}").equals("C"))
        {
            ADFUtils.findOperation("Commit").execute();
        }
        else
        {
            ADFUtils.findOperation("Rollback").execute();

        
        }
        return "Cancel";
    }

    public void setJobCodeBinding(RichInputComboboxListOfValues jobCodeBinding) {
        this.jobCodeBinding = jobCodeBinding;
    }

    public RichInputComboboxListOfValues getJobCodeBinding() {
        return jobCodeBinding;
    }

    public void requiredQtyValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (object != null && stockQtyBinding.getValue() != null) {
            oracle.jbo.domain.Number v1 = (oracle.jbo.domain.Number) stockQtyBinding.getValue();
            oracle.jbo.domain.Number v2 = (oracle.jbo.domain.Number) object;
            if ((v2.compareTo(v1) == 1) || (v2.compareTo(0) == 0)) {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                              "Required Quantity must be greater than 0 or less than or equals to Stock.",
                                                              null));
            }
        }

    }

    public void setSpecificationBinding(RichInputText specificationBinding) {
        this.specificationBinding = specificationBinding;
    }

    public RichInputText getSpecificationBinding() {
        return specificationBinding;
    }
}


