package terms.mtl.transaction.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.application.ViewHandler;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import javax.faces.validator.ValidatorException;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;

import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;

import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

import terms.mtl.transaction.model.view.ReturnSlipDetailVORowImpl;

public class ReturnSlipBean {
    private RichInputComboboxListOfValues bindDocTyp;
    private RichSelectOneChoice bindDocTypVal;
    private RichInputComboboxListOfValues bindDocNumber;
    private RichSelectOneChoice bindRetType;
    private RichPopup bindPopup;
    private RichInputComboboxListOfValues bindUnit;
    private RichInputText bindPassword;
    private RichInputText bindTransPopup;
    private RichInputComboboxListOfValues bindItemCode;
    private RichInputText bindReturnedQuantity;
    private RichInputText bindQuantityVen;
    private RichInputText bindQtyDept;
    private RichInputText bindSendBack;
    private RichInputText bindMsQty;
    private RichTable returnSlipTableBinding;
    private RichInputText returnSlipBinding;
    private RichInputDate dateBinding;
    private RichInputText unitNameBinding;
    private RichInputText empNameBinding;
    private RichInputComboboxListOfValues prepByBinding;
    private RichInputText retFromBinding;
    private RichInputText prepByNameBinding;
    private RichInputText returnFromName;

    private RichInputText itemDescBinding;
    private RichInputText porateBinding;
    private RichInputText prcsDescBinding;
    private RichInputText retScpBinding;
    private RichInputText retDeptBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichButton detailcreateBinding;
    private RichButton detaildeleteBinding;
    private RichOutputText editTransOutputBinding;
    private RichInputText revisionNoDetail;
    private RichButton deleteButtonBinding;
    private RichInputComboboxListOfValues authBinding;
    private RichInputComboboxListOfValues checkByBinding;
    private RichInputComboboxListOfValues returnFromBinding;
    private RichInputText qcNameBinding1;
    String status="A";


    //Generating Return slip no function....

    public ReturnSlipBean() {
    }

    public void saveReturnSlipNoAL(ActionEvent actionEvent) {
        
        String empcd=(String)ADFUtils.resolveExpression("#{pageFlowScope.empCode}") ; 
        empcd=empcd!=null?empcd:"Admin";
        System.out.println("empcd"+empcd);
        ADFUtils.setColumnValue("ReturnSlipHeaderVO1Iterator", "LastUpdatedBy", empcd);
        
        if(status!=null && !status.equalsIgnoreCase("A"))
        {
           ADFUtils.showMessage(status, 0); 
        }
        else
        {
        OperationBinding op = ADFUtils.findOperation("getRetSlipNo");
        Object rst = op.execute();
        System.out.println("--------Commit-------");
        System.out.println("value aftr getting result--->?" + rst);
        if (rst.toString() != null && rst.toString() != "") {
            if (op.getErrors().isEmpty()) {
                ADFUtils.findOperation("Commit").execute();
                FacesMessage Message =
                    new FacesMessage("Record Saved Successfully. New Return Slip No is "+rst+".");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);

            }
        }

        if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ") {
            if (op.getErrors().isEmpty()) {
                ADFUtils.findOperation("Commit").execute();
                FacesMessage Message = new FacesMessage("Record Update Successfully.");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);

            }
        }

        }
    }


    //Validations of Document Type......

    public void docTypeVCE(ValueChangeEvent vce) {

        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce != null) {
            if (!vce.getNewValue().equals(vce.getOldValue())) {
                DCIteratorBinding dciter = (DCIteratorBinding) ADFUtils.findIterator("ReturnSlipDetailVO1Iterator");

                ReturnSlipDetailVORowImpl empRow = (ReturnSlipDetailVORowImpl) dciter.getCurrentRow();
                if (empRow != null && empRow.getDocType() != null) {
                    String RetType = getBindRetType().getValue().toString();
                    System.out.println("Value of Ret Type" + RetType);
                    System.out.println("value of doc type gecf======" + empRow.getDocType());
                    if (empRow.getDocType().equals("FG")) {

                        FacesMessage message = new FacesMessage("This type is allowed only thru FG segg. Entry");
                        message.setSeverity(FacesMessage.SEVERITY_ERROR);
                        FacesContext context = FacesContext.getCurrentInstance();
                        context.addMessage(bindDocTyp.getClientId(), message);
                        empRow.setDocType("JA");

                    } else if ((RetType.equals("L") && (empRow.getDocType().equals("LM")))) {

                        System.out.println("Value of Ret Type" + RetType);
                        System.out.println("value of doc type for LM======" + empRow.getDocType());

                      //  RichPopup.PopupHints hints = new RichPopup.PopupHints();
                      //  getBindPopup().show(hints);
                        getBindDocNumber().setDisabled(true);
                        //   String DocNo = getBindDocNumber().getValue().toString();
                     //   System.out.println("Value of Doc number" + DocNo);
                    }

                   else if (RetType.equals("E") && !empRow.getDocType().equalsIgnoreCase("IS")) {
                        getBindDocNumber().setValue("");
                        System.out.println("value of doc type ret type is or ot======" + empRow.getDocType());
                        FacesMessage message =
                            new FacesMessage("'An Employee Can Return Againts Issue Slip And Others Only'");
                        message.setSeverity(FacesMessage.SEVERITY_ERROR);
                        FacesContext context = FacesContext.getCurrentInstance();
                        context.addMessage(bindDocTyp.getClientId(), message);
                    }

                    else if (((empRow.getDocType().equals("JC")) || (empRow.getDocType().equals("JA")) ||
                              (empRow.getDocType().equals("JP")) || (empRow.getDocType().equals("SH")) ||
                              (empRow.getDocType().equals("FD")) || (empRow.getDocType().equals("IS")) ||
                              (empRow.getDocType().equals("IN")) || (!empRow.getDocType().equals("FG")) ||
                              (!empRow.getDocType().equals("LM")) && (empRow.getDocNo().equals("."))))

                    {

                        
                        
                        FacesMessage message = new FacesMessage("Document Must Be Entered");
                        message.setSeverity(FacesMessage.SEVERITY_ERROR);
                        FacesContext context = FacesContext.getCurrentInstance();
//                        context.addMessage(bindDocNumber.getClientId(), message);

                    }


                    else {
                        

                    }
                }
                ADFUtils.findOperation("clearDetailRowReturnSlip").execute();
            }

        }
    }

    public void setBindDocTyp(RichInputComboboxListOfValues bindDocTyp) {
        this.bindDocTyp = bindDocTyp;
    }

    public RichInputComboboxListOfValues getBindDocTyp() {
        return bindDocTyp;
    }

    public void setBindDocTypVal(RichSelectOneChoice bindDocTypVal) {
        this.bindDocTypVal = bindDocTypVal;
    }

    public RichSelectOneChoice getBindDocTypVal() {
        return bindDocTypVal;
    }


    public void setBindDocNumber(RichInputComboboxListOfValues bindDocNumber) {
        this.bindDocNumber = bindDocNumber;
    }

    public RichInputComboboxListOfValues getBindDocNumber() {
        return bindDocNumber;
    }


    public void setBindRetType(RichSelectOneChoice bindRetType) {
        this.bindRetType = bindRetType;
    }

    public RichSelectOneChoice getBindRetType() {
        return bindRetType;
    }

    public void setBindPopup(RichPopup bindPopup) {
        this.bindPopup = bindPopup;
    }

    public RichPopup getBindPopup() {

        return bindPopup;
    }


   
    public void setBindUnit(RichInputComboboxListOfValues bindUnit) {
        this.bindUnit = bindUnit;
    }

    public RichInputComboboxListOfValues getBindUnit() {
        return bindUnit;
    }

    public void setBindPassword(RichInputText bindPassword) {
        this.bindPassword = bindPassword;
    }

    public RichInputText getBindPassword() {
        return bindPassword;
    }


    public void setBindTransPopup(RichInputText bindTransPopup) {
        this.bindTransPopup = bindTransPopup;
    }

    public RichInputText getBindTransPopup() {
        return bindTransPopup;
    }

    public void setBindItemCode(RichInputComboboxListOfValues bindItemCode) {
        this.bindItemCode = bindItemCode;
    }

    public RichInputComboboxListOfValues getBindItemCode() {
        return bindItemCode;
    }


    public void setBindReturnedQuantity(RichInputText bindReturnedQuantity) {
        this.bindReturnedQuantity = bindReturnedQuantity;
    }

    public RichInputText getBindReturnedQuantity() {
        return bindReturnedQuantity;
    }


    //Function calling for Po_Rate
    public void itemCodeVCE(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        OperationBinding op2 =  ADFUtils.findOperation("goForPoRate");
        System.out.println("after porate%%******");
        //OperationBinding op3 = ADFUtils.findOperation("getWipStockQtyforRetSlip");
        System.out.println("after wipis%%******");
        Object rst2 = op2.execute();
        //Object rst3 = op3.execute();
        System.out.println("inside exit");
        AdfFacesContext.getCurrentInstance().addPartialTarget(returnSlipTableBinding);
    }

    public void setBindQuantityVen(RichInputText bindQuantityVen) {
        this.bindQuantityVen = bindQuantityVen;
    }


    public RichInputText getBindQuantityVen() {
        return bindQuantityVen;
    }

    public void setBindQtyDept(RichInputText bindQtyDept) {
        this.bindQtyDept = bindQtyDept;
    }

    public RichInputText getBindQtyDept() {
        return bindQtyDept;
    }

    public void setBindSendBack(RichInputText bindSendBack) {
        this.bindSendBack = bindSendBack;
    }

    public RichInputText getBindSendBack() {
        return bindSendBack;
    }

    public void setBindMsQty(RichInputText bindMsQty) {
        this.bindMsQty = bindMsQty;
    }

    public RichInputText getBindMsQty() {
        return bindMsQty;
    }

    public void retQtyValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {

        if(returnSlipBinding.getValue()==null){
        OperationBinding ob = ADFUtils.findOperation("retQtyValidationCheck");
        ob.getParamsMap().put("newRetQty", object);
        ob.execute();
        System.out.println("NEW QTY" + object);
        System.out.println("Msg in bean:" + ob.getResult());
        if (ob.getResult().toString().compareTo("F") == 0) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                          "Return Quantity must be greater than total quantity.",
                                                          null));
        }


//        OperationBinding op = ADFUtils.findOperation("goForRetQty");
//        op.getParamsMap().put("newRetQty", object);
//        op.execute();
//        System.out.println("MSG IN BEAN:" + op.getResult());
//        //  System.out.println("compare value of op2result with T:"+op.getResult().toString().compareTo("T"));
//        if (op.getResult() != null) {
//            if (op.getResult().toString().compareTo("T") != 0) {
//                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, op.getResult().toString(),
//                                                              null));
//            }
//        }
        BigDecimal a = new BigDecimal(0);
        BigDecimal retQty = (BigDecimal) object;
        System.out.println("accept qty is===>" + retQty);
        if (object != null && (retQty.compareTo(a) == -1 || retQty.compareTo(a)==0)){
            System.out.println("iside compare" + retQty);

            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                          "Return Qty. must be greater than 0.", null));
        } else {


        }
        }
    }

    public void scrapVendorValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (object != null) {
            System.out.println("OBJECT VALUE " + object);
            System.out.println("Calling  goForScrapVendor method start ");
            OperationBinding ov = ADFUtils.findOperation("goForScrapVendor");
            ov.getParamsMap().put("ScrapVen", object);
            Object obj = ov.execute();
            System.out.println("After calling Result is :   " + ov.getResult());
        }


    }


    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete1").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully.");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(returnSlipTableBinding);
    }

    public void setReturnSlipTableBinding(RichTable returnSlipTableBinding) {
        this.returnSlipTableBinding = returnSlipTableBinding;
    }

    public RichTable getReturnSlipTableBinding() {
        return returnSlipTableBinding;
    }

    public void setReturnSlipBinding(RichInputText returnSlipBinding) {
        this.returnSlipBinding = returnSlipBinding;
    }

    public RichInputText getReturnSlipBinding() {
        return returnSlipBinding;
    }

    public void setDateBinding(RichInputDate dateBinding) {
        this.dateBinding = dateBinding;
    }

    public RichInputDate getDateBinding() {
        return dateBinding;
    }

    public void setUnitNameBinding(RichInputText unitNameBinding) {
        this.unitNameBinding = unitNameBinding;
    }

    public RichInputText getUnitNameBinding() {
        return unitNameBinding;
    }

    public void setEmpNameBinding(RichInputText empNameBinding) {
        this.empNameBinding = empNameBinding;
    }

    public RichInputText getEmpNameBinding() {
        return empNameBinding;
    }

    public void setPrepByBinding(RichInputComboboxListOfValues prepByBinding) {
        this.prepByBinding = prepByBinding;
    }

    public RichInputComboboxListOfValues getPrepByBinding() {
        return prepByBinding;
    }

    public void setRetFromBinding(RichInputText retFromBinding) {
        this.retFromBinding = retFromBinding;
    }

    public RichInputText getRetFromBinding() {
        return retFromBinding;
    }

    public void setPrepByNameBinding(RichInputText prepByNameBinding) {
        this.prepByNameBinding = prepByNameBinding;
    }

    public RichInputText getPrepByNameBinding() {
        return prepByNameBinding;
    }

    public void setReturnFromName(RichInputText returnFromName) {
        this.returnFromName = returnFromName;
    }

    public RichInputText getReturnFromName() {
        return returnFromName;
    }


    public void setItemDescBinding(RichInputText itemDescBinding) {
        this.itemDescBinding = itemDescBinding;
    }

    public RichInputText getItemDescBinding() {
        return itemDescBinding;
    }

    public void setPorateBinding(RichInputText porateBinding) {
        this.porateBinding = porateBinding;
    }

    public RichInputText getPorateBinding() {
        return porateBinding;
    }

    public void setPrcsDescBinding(RichInputText prcsDescBinding) {
        this.prcsDescBinding = prcsDescBinding;
    }

    public RichInputText getPrcsDescBinding() {
        return prcsDescBinding;
    }

    public void setRetScpBinding(RichInputText retScpBinding) {
        this.retScpBinding = retScpBinding;
    }

    public RichInputText getRetScpBinding() {
        return retScpBinding;
    }

    public void setRetDeptBinding(RichInputText retDeptBinding) {
        this.retDeptBinding = retDeptBinding;
    }

    public RichInputText getRetDeptBinding() {
        return retDeptBinding;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setDetailcreateBinding(RichButton detailcreateBinding) {
        this.detailcreateBinding = detailcreateBinding;
    }

    public RichButton getDetailcreateBinding() {
        return detailcreateBinding;
    }

    public void setDetaildeleteBinding(RichButton detaildeleteBinding) {
        this.detaildeleteBinding = detaildeleteBinding;
    }

    public RichButton getDetaildeleteBinding() {
        return detaildeleteBinding;
    }

    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }
    //Set Fields and Button disable.
    public void cevModeDisableComponent(String mode) 
    {
    if (mode.equals("E")) 
        {
            getDateBinding().setDisabled(true);
            getUnitNameBinding().setDisabled(true);
            getEmpNameBinding().setDisabled(true);
            getPrepByBinding().setDisabled(true);
            getPrepByNameBinding().setDisabled(true);
            getRetFromBinding().setDisabled(true);
            getReturnSlipBinding().setDisabled(true);
            getBindUnit().setDisabled(true);
            getItemDescBinding().setDisabled(true);
            getPrcsDescBinding().setDisabled(true);
            getRetScpBinding().setDisabled(true);
            getRetDeptBinding().setDisabled(true);
            getQcNameBinding1().setDisabled(true);
            getReturnFromName().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            getDetailcreateBinding().setDisabled(false);
            getDetaildeleteBinding().setDisabled(false);
            getRevisionNoDetail().setDisabled(true);
            getBindRetType().setDisabled(true);
            getReturnFromBinding().setDisabled(true);
            getPorateBinding().setDisabled(true);
         } 
        else if (mode.equals("C")) 
        {
            getDateBinding().setDisabled(true);
            getUnitNameBinding().setDisabled(true);
            getEmpNameBinding().setDisabled(true);
            getPrepByBinding().setDisabled(true);
            getPrepByNameBinding().setDisabled(true);
            getRetFromBinding().setDisabled(true);
            getReturnSlipBinding().setDisabled(true);
           getBindUnit().setDisabled(true);
            getItemDescBinding().setDisabled(true);
            getPrcsDescBinding().setDisabled(true);
            getRetScpBinding().setDisabled(true);
            getRetDeptBinding().setDisabled(true);
            getQcNameBinding1().setDisabled(true);
            getReturnFromName().setDisabled(true);
            getDetailcreateBinding().setDisabled(false);
            getDetaildeleteBinding().setDisabled(false);
            getRevisionNoDetail().setDisabled(true);
            getAuthBinding().setDisabled(true);
            getCheckByBinding().setDisabled(true);

        } 
        else if (mode.equals("V")) 
        {
            deleteButtonBinding.setDisabled(true);
            getDetailcreateBinding().setDisabled(true);
            getDetaildeleteBinding().setDisabled(true);
        }
    }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
        if(checkByBinding.getValue()==null){
            System.out.println("inside if");
            authBinding.setDisabled(true);
        }
        else{
            System.out.println("inside else");
            authBinding.setDisabled(false);
            checkByBinding.setDisabled(true);
        }
        
//        if(checkByBinding.getValue()!=null){
//            
//        }
//        cevmodecheck();
    }

    public void setEditTransOutputBinding(RichOutputText editTransOutputBinding) {
        this.editTransOutputBinding = editTransOutputBinding;
    }

    public RichOutputText getEditTransOutputBinding() {
        cevmodecheck();
        return editTransOutputBinding;
    }

    public void setRevisionNoDetail(RichInputText revisionNoDetail) {
        this.revisionNoDetail = revisionNoDetail;
    }

    public RichInputText getRevisionNoDetail() {
        return revisionNoDetail;
    }

    public void setDeleteButtonBinding(RichButton deleteButtonBinding) {
        this.deleteButtonBinding = deleteButtonBinding;
    }

    public RichButton getDeleteButtonBinding() {
        return deleteButtonBinding;
    }

    public void setAuthBinding(RichInputComboboxListOfValues authBinding) {
        this.authBinding = authBinding;
    }

    public RichInputComboboxListOfValues getAuthBinding() {
        return authBinding;
    }

    public void setCheckByBinding(RichInputComboboxListOfValues checkByBinding) {
        this.checkByBinding = checkByBinding;
    }

    public RichInputComboboxListOfValues getCheckByBinding() {
        return checkByBinding;
    }

    public void detailCreateButtonAL(ActionEvent actionEvent) {
        // Add event code here...
        ADFUtils.findOperation("CreateInsert").execute();
        getBindRetType().setDisabled(true);
        getReturnFromBinding().setDisabled(true);

    }

    public void setReturnFromBinding(RichInputComboboxListOfValues returnFromBinding) {
        this.returnFromBinding = returnFromBinding;
    }

    public RichInputComboboxListOfValues getReturnFromBinding() {
        return returnFromBinding;
    }

    public void resetFieldsDtlVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        System.out.println("vce.getOldValue()=>" + vce.getOldValue() + "\nvce.getNewValue()=>" + vce.getNewValue());

        if (vce != null && vce.getOldValue() != null) {
            if (!vce.getOldValue().equals(vce.getNewValue())) {
                System.out.println("Task Start");
                ADFUtils.findOperation("clearDetailRowReturnSlip").execute();
            }
        }
    }

    public void setQcNameBinding1(RichInputText qcNameBinding1) {
        this.qcNameBinding1 = qcNameBinding1;
    }

    public RichInputText getQcNameBinding1() {
        return qcNameBinding1;
    }

    public void AuthByAuthorityVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());            
                     OperationBinding ob = ADFUtils.findOperation("checkApprovalStatus");
                    ob.getParamsMap().put("formNm", "RTN");
                    ob.getParamsMap().put("authoLim", "AP");
                    ob.getParamsMap().put("empcd", vce.getNewValue().toString());
                    ob.execute();
                    System.out.println(" EMP CD in auth "+vce.getNewValue().toString());
                    if((ob.getResult() !=null && ob.getResult().equals("Y")))
                    {
                        Row row=(Row)ADFUtils.evaluateEL("#{bindings.ReturnSlipHeaderVO1Iterator.currentRow}");
                        row.setAttribute("AuthBy", null);
                        ADFUtils.showMessage("You have no authority to approve this Return Slip.",0);
                    }
    }

    public void ChkByAuthorityVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());            
                     OperationBinding ob = ADFUtils.findOperation("checkApprovalStatus");
                    ob.getParamsMap().put("formNm", "RTN");
                    ob.getParamsMap().put("authoLim", "CH");
                    ob.getParamsMap().put("empcd", vce.getNewValue().toString());
                    ob.execute();
                    System.out.println(" EMP CD in check by "+vce.getNewValue().toString());
                    if((ob.getResult() !=null && ob.getResult().equals("Y")))
                    {
                        Row row=(Row)ADFUtils.evaluateEL("#{bindings.ReturnSlipHeaderVO1Iterator.currentRow}");
                        row.setAttribute("CheckBy", null);
                        ADFUtils.showMessage("You have no authority to check this Return Slip.",0);
                    }
    }

    public void returnQtyVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());            
        if(vce.getNewValue()!=null)
        {
            OperationBinding op = ADFUtils.findOperation("goForRetQty");
            op.getParamsMap().put("newRetQty", vce.getNewValue());
            op.execute();
            System.out.println("MSG IN BEAN:" + op.getResult());
            if (op.getResult() != null) {
                if (op.getResult().toString().compareTo("T") != 0) {
                    status=(String)op.getResult();
                    FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, op.getResult().toString(),null);
                    message.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext context = FacesContext.getCurrentInstance();
                    context.addMessage(bindReturnedQuantity.getClientId(), message);
//                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, op.getResult().toString(),
//                                                                  null));
                }else
                {
                    status="A";
                }
            }
        }
    }

    public String saveCloseButtonAL() {
        
        String empcd=(String)ADFUtils.resolveExpression("#{pageFlowScope.empCode}") ; 
        empcd=empcd!=null?empcd:"Admin";
        System.out.println("empcd"+empcd);
        ADFUtils.setColumnValue("ReturnSlipHeaderVO1Iterator", "LastUpdatedBy", empcd);
        
        if(status!=null && !status.equalsIgnoreCase("A"))
        {
           ADFUtils.showMessage(status, 0); 
        }
        else
        {
        OperationBinding op = ADFUtils.findOperation("getRetSlipNo");
        Object rst = op.execute();
        System.out.println("--------Commit-------");
        System.out.println("value aftr getting result--->?" + rst);
        if (rst.toString() != null && rst.toString() != "") {
            if (op.getErrors().isEmpty()) {
                ADFUtils.findOperation("Commit").execute();
                FacesMessage Message =
                    new FacesMessage("Record Saved Successfully. New Return Slip No is "+rst+".");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
                return "Save and Close";

            }
        }

        if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ") {
            if (op.getErrors().isEmpty()) {
                ADFUtils.findOperation("Commit").execute();
                FacesMessage Message = new FacesMessage("Record Update Successfully.");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
                return "Save and Close";

            }
        }

        }
        return null;
    }

    public void msQtyValidator(FacesContext facesContext, UIComponent uIComponent, Object msQty) {
        if(msQty!=null && bindReturnedQuantity.getValue()!=null)
        {
            BigDecimal mQty=new BigDecimal(msQty.toString());
            BigDecimal rQty=(BigDecimal) bindReturnedQuantity.getValue();
            if(mQty.compareTo(rQty)==-1 || mQty.compareTo(rQty)==1)
            {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,"Main Store Quantity must be equals to Returned Quantity.",
                                                              null));
            }
        }

    }
}


