package terms.mtl.transaction.ui.bean;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import javax.naming.Context;

import oracle.binding.OperationBinding;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.BindingContainer;

import javax.naming.InitialContext;

import javax.servlet.http.HttpServletResponse;

import javax.sql.DataSource;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.jbo.domain.Date;

public class SearchComparisonChartBean {
    private RichTable searchRateComparisonHeaderTableBinding;
    String Status = "F", Status1 = "F", Status2 = "F", Status3 = "F";

    private String quotType = "";

    public SearchComparisonChartBean() {
    }

    public void dialogListenerDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            OperationBinding op = null;
            ADFUtils.findOperation("Delete").execute();
            op = (OperationBinding) ADFUtils.findOperation("Commit");
            Object rst = op.execute();
            System.out.println("Record Delete Successfully");
            if (op.getErrors().isEmpty()) {
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            } else if (!op.getErrors().isEmpty()) {
                OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
                Object rstr = opr.execute();
                FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(searchRateComparisonHeaderTableBinding);
    }

    public void setSearchRateComparisonHeaderTableBinding(RichTable searchRateComparisonHeaderTableBinding) {
        this.searchRateComparisonHeaderTableBinding = searchRateComparisonHeaderTableBinding;
    }

    public RichTable getSearchRateComparisonHeaderTableBinding() {
        return searchRateComparisonHeaderTableBinding;
    }

    //*********************Report Calling Code***********************//

    public BindingContainer getBindings() {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
    }

    public void downloadJrxml(FacesContext facesContext, OutputStream outputStream) {
        Connection conn = null;
        try {

            String file_name = "";
            System.out.println("invoceMode===================>" + quotType);
            String rateComNo = null;
            System.out.println("After Set Value File Name is===>" + file_name);

            OperationBinding binding = ADFUtils.findOperation("fileNameForPrint");
            binding.getParamsMap().put("fileId", "ERP0000000038");
            binding.execute();

            System.out.println("Binding Result :" + binding.getResult().toString());

            if (binding.getResult() != null) {
                InputStream input = new FileInputStream(binding.getResult().toString());

                DCIteratorBinding poIter =
                    (DCIteratorBinding) getBindings().get("SearchRateComparisionChartVO1Iterator");

                String unitCode = poIter.getCurrentRow().getAttribute("UnitCd").toString();
                rateComNo = poIter.getCurrentRow().getAttribute("CompNo").toString();

                System.out.println("UNIT CODE:" + unitCode);

                if (poIter.getCurrentRow().getAttribute("QuotType").equals("E")) {
                    System.out.println("Entry No :- " + rateComNo + " " + unitCode);
                    Map n = new HashMap();
                    n.put("p_unit", unitCode);
                    n.put("p_entry_no", rateComNo);
                    conn = getConnection();

                    JasperReport design = (JasperReport) JRLoader.loadObject(input);
                    System.out.println("Path : " + input + " -------" + design + " param:" + n);

                    @SuppressWarnings("unchecked")
                    net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                    byte pdf[] = JasperExportManager.exportReportToPdf(print);
                    HttpServletResponse response =
                        (HttpServletResponse) facesContext.getExternalContext().getResponse();
                    response.getOutputStream().write(pdf);
                    response.getOutputStream().flush();
                    response.getOutputStream().close();
                    facesContext.responseComplete();
                } else if (poIter.getCurrentRow().getAttribute("QuotType").equals("D")) {
                    String result = binding.getResult().toString();
                    int last_index = result.lastIndexOf("/");
                    String path = result.substring(0, last_index + 1) + "r_rate_comp_con.jasper";
                    System.out.println("------------Path-----------" + path);
                    InputStream input1 = new FileInputStream(path);

                    System.out.println("Entry No :- " + rateComNo + " " + unitCode);
                    Map n = new HashMap();
                    n.put("p_unit", unitCode);
                    n.put("p_entry_no", rateComNo);
                    conn = getConnection();

                    JasperReport design = (JasperReport) JRLoader.loadObject(input);
                    System.out.println("Path : " + input + " -------" + design + " param:" + n);

                    @SuppressWarnings("unchecked")
                    net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                    byte pdf[] = JasperExportManager.exportReportToPdf(print);
                    HttpServletResponse response =
                        (HttpServletResponse) facesContext.getExternalContext().getResponse();
                    response.getOutputStream().write(pdf);
                    response.getOutputStream().flush();
                    response.getOutputStream().close();
                    facesContext.responseComplete();
                }
                //rateComNo=null;
            } else
                System.out.println("File Name/File Path not found.");
        } catch (FileNotFoundException fnfe) {
            // TODO: Add catch code
            fnfe.printStackTrace();
            try {
                System.out.println("in finally connection closed");
                conn.close();
                conn = null;

            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            // TODO: Add catch code
            e.printStackTrace();
            try {
                System.out.println("in finally connection closed");
                conn.close();
                conn = null;

            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        } finally {
            try {
                System.out.println("in finally connection closed");
                conn.close();
                conn = null;

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public Connection getConnection() throws Exception {
        Context ctx = null;
        Connection conn = null;
        ctx = new InitialContext();
        DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/ApplicationDBDS");
        conn = ds.getConnection();
        return conn;
    }


    public void loginFilteration() {

        OperationBinding ob = ADFUtils.findOperation("CheckApprovalAuthoQuotLogIn");
        ob.getParamsMap().put("formNm", "RATE_CN_AP");
        ob.getParamsMap().put("empcd",
                              ADFUtils.resolveExpression("#{pageFlowScope.empCode}") == null ? "SWE161" :
                              ADFUtils.resolveExpression("#{pageFlowScope.empCode}"));
        ob.getParamsMap().put("unitCd",
                              ADFUtils.resolveExpression("#{pageFlowScope.unitCode}") == null ? "10001" :
                              ADFUtils.resolveExpression("#{pageFlowScope.unitCode}"));
        ob.execute();
        if ((ob.getResult() != null && ob.getResult().equals("N"))) {
            Status = "T";
        }

        OperationBinding ob1 = ADFUtils.findOperation("CheckApprovalAuthoQuotLogIn");
        ob1.getParamsMap().put("formNm", "RATE_EN_AP");
        ob1.getParamsMap().put("empcd",
                               ADFUtils.resolveExpression("#{pageFlowScope.empCode}") == null ? "SWE161" :
                               ADFUtils.resolveExpression("#{pageFlowScope.empCode}"));
        ob1.getParamsMap().put("unitCd",
                               ADFUtils.resolveExpression("#{pageFlowScope.unitCode}") == null ? "10001" :
                               ADFUtils.resolveExpression("#{pageFlowScope.unitCode}"));
        ob1.execute();
        if ((ob1.getResult() != null && ob1.getResult().equals("N"))) {
            Status1 = "T";
        }


        if (Status.equalsIgnoreCase("T") && Status1.equalsIgnoreCase("T")) {
            System.out.println("Both APPROVAL");
            OperationBinding op = ADFUtils.findOperation("loginFilterRateCompMethod");
            op.getParamsMap().put("status", "A");
            op.execute();
        } else {
            if (Status.equalsIgnoreCase("T") && Status1.equalsIgnoreCase("F")) {
                System.out.println("CONTRACT APPROVAL");
                OperationBinding op = ADFUtils.findOperation("loginFilterRateCompMethod");
                op.getParamsMap().put("status", "D");
                op.execute();
            }
            if (Status.equalsIgnoreCase("F") && Status1.equalsIgnoreCase("T")) {
                System.out.println("Enquiry APPROVAL");
                OperationBinding op = ADFUtils.findOperation("loginFilterRateCompMethod");
                op.getParamsMap().put("status", "E");
                op.execute();
            }
        }

    }

    public String rateLoginAL() {
        System.out.println("IN RATE AL");
        OperationBinding ob = ADFUtils.findOperation("CheckApprovalAuthoQuotCreation");
        ob.getParamsMap().put("formNm", "RATE_CN_AP");
        ob.getParamsMap().put("authoLim", "PR");
        ob.getParamsMap().put("empcd",
                              ADFUtils.resolveExpression("#{pageFlowScope.empCode}") == null ? "SWE161" :
                              ADFUtils.resolveExpression("#{pageFlowScope.empCode}"));
        ob.getParamsMap().put("unitCd",
                              ADFUtils.resolveExpression("#{pageFlowScope.unitCode}") == null ? "10001" :
                              ADFUtils.resolveExpression("#{pageFlowScope.unitCode}"));
        ob.execute();
        if ((ob.getResult() != null && ob.getResult().equals("N"))) {
            Status2 = "T";
            System.out.println("IN CONTRACT RATE AL");
        }

        OperationBinding ob1 = ADFUtils.findOperation("CheckApprovalAuthoQuotCreation");
        ob1.getParamsMap().put("formNm", "RATE_EN_AP");
        ob1.getParamsMap().put("authoLim", "PR");
        ob1.getParamsMap().put("empcd",
                               ADFUtils.resolveExpression("#{pageFlowScope.empCode}") == null ? "SWE161" :
                               ADFUtils.resolveExpression("#{pageFlowScope.empCode}"));
        ob1.getParamsMap().put("unitCd",
                               ADFUtils.resolveExpression("#{pageFlowScope.unitCode}") == null ? "10001" :
                               ADFUtils.resolveExpression("#{pageFlowScope.unitCode}"));
        ob1.execute();
        if ((ob1.getResult() != null && ob1.getResult().equals("N"))) {
            Status3 = "T";
            System.out.println("IN ENQUIRY RATE AL");
        }

        if (Status2.equalsIgnoreCase("T") || Status3.equalsIgnoreCase("T")) {
            return "Go to Create";
        } else if (Status2.equalsIgnoreCase("F") && Status3.equalsIgnoreCase("F")) {
            ADFUtils.showMessage("User have no authority to Create Rate Comparison Chart. ", 0);
            return null;
        }
        return null;
    }

}
