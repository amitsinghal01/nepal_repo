package terms.mtl.transaction.ui.bean;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.application.ViewHandler;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.naming.Context;
import javax.naming.InitialContext;

import javax.servlet.http.HttpServletResponse;

import javax.sql.DataSource;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

public class SearchContractOrderBean {
    private RichOutputText jonobinding;
    private RichTable searchContractTableBinding;
    private RichSelectOneChoice contractParaBinding;


    public SearchContractOrderBean() {
    }

    public String checkApprovedByAC() {
        OperationBinding binding = ADFUtils.findOperation("amdApprovedNumber");
        binding.getParamsMap().put("JoNo", jonobinding.getValue());

        binding.execute();

        if (binding.getResult() != null && binding.getResult().toString().equalsIgnoreCase("0"))

        {

            return "goContractOrderDetail";

        } else {
            FacesMessage Message =
                new FacesMessage("You are unable to generate new document because " + jonobinding.getValue() +
                                 " Amendment Number " + binding.getResult() + " is not Approved.");
            Message.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }
        return null;
    }

    public void setJonobinding(RichOutputText jonobinding) {
        this.jonobinding = jonobinding;
    }

    public RichOutputText getJonobinding() {
        return jonobinding;
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            OperationBinding op = null;
            ADFUtils.findOperation("Delete").execute();
            op = (OperationBinding) ADFUtils.findOperation("Commit");
            Object rst = op.execute();
            System.out.println("Record Delete Successfully");
            if (op.getErrors().isEmpty()) {
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            } else if (!op.getErrors().isEmpty()) {
                OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
                Object rstr = opr.execute();
                FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(searchContractTableBinding);
    }

    public void setSearchContractTableBinding(RichTable searchContractTableBinding) {
        this.searchContractTableBinding = searchContractTableBinding;
    }

    public RichTable getSearchContractTableBinding() {
        return searchContractTableBinding;
    }

    //*********************Report Calling Code***********************//

    public BindingContainer getBindings() {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
    }

    public void downloadJrxml(FacesContext facesContext, OutputStream outputStream) {
        Connection conn = null;
        try {

            OperationBinding binding = ADFUtils.findOperation("fileNameForPrint");
            binding.getParamsMap().put("fileId", "ERP0000000043");
            binding.execute();

            System.out.println("Binding Result :" + binding.getResult().toString());
            if (binding.getResult() != null) {
                InputStream input = new FileInputStream(binding.getResult().toString());

                DCIteratorBinding poIter = (DCIteratorBinding) getBindings().get("SearchContractOrderVO1Iterator");
                String joNo = poIter.getCurrentRow().getAttribute("JoNo").toString();
                String unitCode = poIter.getCurrentRow().getAttribute("UnitCd").toString();
                Integer joAmdNo = (Integer) poIter.getCurrentRow().getAttribute("JoAmdNo");
                System.out.println("Jo No :- " + joNo + " " + unitCode + " " + joAmdNo);
                Map n = new HashMap();
                n.put("P_UNIT", unitCode);
                n.put("P_CO_NO", joNo);
                n.put("P_CO_AMD_NO", joAmdNo);
                n.put("p_tp", contractParaBinding.getValue());

                conn = getConnection();

                JasperReport design = (JasperReport) JRLoader.loadObject(input);
                System.out.println("Path : " + input + " -------" + design + " param:" + n);

                @SuppressWarnings("unchecked")
                net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                byte pdf[] = JasperExportManager.exportReportToPdf(print);
                HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                response.getOutputStream().write(pdf);
                response.getOutputStream().flush();
                response.getOutputStream().close();
                facesContext.responseComplete();
            } else
                System.out.println("File Name/File Path not found.");
        } catch (FileNotFoundException fnfe) {
            // TODO: Add catch code
            fnfe.printStackTrace();
            try {
                System.out.println("in finally connection closed");
                conn.close();
                conn = null;

            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            // TODO: Add catch code
            e.printStackTrace();
            try {
                System.out.println("in finally connection closed");
                conn.close();
                conn = null;

            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        } finally {
            try {
                System.out.println("in finally connection closed");
                conn.close();
                conn = null;

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public Connection getConnection() throws Exception {
        Context ctx = null;
        Connection conn = null;
        ctx = new InitialContext();
        DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/ApplicationDBDS");
        conn = ds.getConnection();
        return conn;
    }


    public void setContractParaBinding(RichSelectOneChoice contractParaBinding) {
        this.contractParaBinding = contractParaBinding;
    }

    public RichSelectOneChoice getContractParaBinding() {
        return contractParaBinding;
    }
}
