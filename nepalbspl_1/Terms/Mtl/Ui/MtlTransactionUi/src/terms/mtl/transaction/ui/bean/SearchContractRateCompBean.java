package terms.mtl.transaction.ui.bean;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class SearchContractRateCompBean {
    private RichTable searchContractRateCompTableBinding;

    public SearchContractRateCompBean() {
    }

    public void deletePopUpDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                        {
                        OperationBinding op = null;
                        ADFUtils.findOperation("Delete").execute();
                        op = (OperationBinding) ADFUtils.findOperation("Commit");
                        Object rst = op.execute();
                        System.out.println("Record Delete Successfully");
                            if(op.getErrors().isEmpty()){
                                FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                                FacesContext fc = FacesContext.getCurrentInstance();
                                fc.addMessage(null, Message);
                            } else if (!op.getErrors().isEmpty())
                            {
                               OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
                               Object rstr = opr.execute();
                               FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                               Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                               FacesContext fc = FacesContext.getCurrentInstance();
                               fc.addMessage(null, Message);
                             }
                        } 
                AdfFacesContext.getCurrentInstance().addPartialTarget(searchContractRateCompTableBinding);
    }

    public void setSearchContractRateCompTableBinding(RichTable searchContractRateCompTableBinding) {
        this.searchContractRateCompTableBinding = searchContractRateCompTableBinding;
    }

    public RichTable getSearchContractRateCompTableBinding() {
        return searchContractRateCompTableBinding;
    }
}
