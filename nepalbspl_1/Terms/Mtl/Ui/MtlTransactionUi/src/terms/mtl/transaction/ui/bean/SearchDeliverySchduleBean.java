package terms.mtl.transaction.ui.bean;

import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import org.apache.myfaces.trinidad.context.RequestContext;

public class SearchDeliverySchduleBean {
    private RichOutputText searchSchdNoBinding;
    private RichTable searchVendorSchdleTableBinding;

    public SearchDeliverySchduleBean() {
    }

    public String AddAmdNumAC() {
        /*  OperationBinding binding = ADFUtils.findOperation("CheckApprovedBy");
        binding.execute();
        if (binding.getResult() != null && (binding.getResult().toString().equalsIgnoreCase("0"))) {
            ADFUtils.findOperation("generateNewAmdNo").execute();
            System.out.println("bean");
            Map paramMap = RequestContext.getCurrentInstance().getPageFlowScope();
            paramMap.put("Mode", "K");
            
        }else {
            
            FacesMessage Message =
                new FacesMessage("You not create new document  because perivious document has been Not Approved!-- Schd  No is  " +
                                 searchSchdNoBinding.getValue() + " and Amd No is-- "+binding.getResult()+"");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
        }*/
        return "null"; 
    }

    public void setSearchSchdNoBinding(RichOutputText searchSchdNoBinding) {
        this.searchSchdNoBinding = searchSchdNoBinding;
    }

    public RichOutputText getSearchSchdNoBinding() {
        return searchSchdNoBinding;
    }

    public void setSearchVendorSchdleTableBinding(RichTable searchVendorSchdleTableBinding) {
        this.searchVendorSchdleTableBinding = searchVendorSchdleTableBinding;
    }

    public RichTable getSearchVendorSchdleTableBinding() {
        return searchVendorSchdleTableBinding;
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                       {
                       OperationBinding op = null;
                       ADFUtils.findOperation("Delete").execute();
                       op = (OperationBinding) ADFUtils.findOperation("Commit");
                       Object rst = op.execute();
                       System.out.println("Record Delete Successfully");
                           if(op.getErrors().isEmpty()){
                               FacesMessage Message = new FacesMessage("Record Deleted Successfully");
        Message.setSeverity(FacesMessage.SEVERITY_INFO);
                               FacesContext fc = FacesContext.getCurrentInstance();
                               fc.addMessage(null, Message);
                           } else if (!op.getErrors().isEmpty())
                           {
                              OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
                              Object rstr = opr.execute();
                              FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
        Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                              FacesContext fc = FacesContext.getCurrentInstance();
                              fc.addMessage(null, Message);
                            }
                       } 
                AdfFacesContext.getCurrentInstance().addPartialTarget(searchVendorSchdleTableBinding);
    }
}
