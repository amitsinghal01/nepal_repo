package terms.mtl.transaction.ui.bean;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.faces.application.FacesMessage;

import javax.faces.context.FacesContext;

import javax.naming.Context;
import javax.naming.InitialContext;

import javax.servlet.http.HttpServletResponse;

import javax.sql.DataSource;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

public class SearchGatePassChallanBean {
    private RichColumn searchGatePassChallanTableBinding;
    private RichSelectOneChoice paramBinding;
    private String PoMode = "";

    public void setPoMode(String PoMode) {
        this.PoMode = PoMode;
    }

    public String getPoMode() {
        return PoMode;
    }

    public SearchGatePassChallanBean() {
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete").execute();
            ADFUtils.findOperation("Commit").execute(); //Unable on Search page.
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }

        AdfFacesContext.getCurrentInstance().addPartialTarget(searchGatePassChallanTableBinding);


    }

    public void setSearchGatePassChallanTableBinding(RichColumn searchGatePassChallanTableBinding) {
        this.searchGatePassChallanTableBinding = searchGatePassChallanTableBinding;
    }

    public RichColumn getSearchGatePassChallanTableBinding() {
        return searchGatePassChallanTableBinding;
    }

    //*******************************************REPORT CODE**********************************

    public BindingContainer getBindings() {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
    }

    public void downloadJrxml(FacesContext facesContext, OutputStream outputStream) {
        Connection conn = null;
        try {

            String file_name = "";
            System.out.println("PoMode===================>" + PoMode);

            if (PoMode.equals("CHLN")) {
                file_name = "r_challan.jasper";
            } else if (PoMode.equals("CHRM")) {
                file_name = "challan_reminder.jasper";
            }

            System.out.println("After Set Value File Name is===>" + file_name);

            OperationBinding binding = ADFUtils.findOperation("fileNameForPrint");
            binding.getParamsMap().put("fileId", "ERP0000000058");
            binding.execute();

            System.out.println("Binding Result :" + binding.getResult());
            if (binding.getResult() != null) {
                String result = binding.getResult().toString();
                int last_index = result.lastIndexOf("/");
                String path = result.substring(0, last_index + 1) + file_name;
                System.out.println("FILE PATH IS===>" + path);
                InputStream input = new FileInputStream(path);

                DCIteratorBinding chItr =
                    (DCIteratorBinding) getBindings().get("SearchGatePassChallanHeaderVO1Iterator");
                String chNo = chItr.getCurrentRow().getAttribute("ChallanNo").toString();
                String unitCode = chItr.getCurrentRow().getAttribute("UnitCd").toString();

                Map n = new HashMap();
                if (PoMode.equals("CHLN")) {
                    System.out.println("inside regular challan=>>" + PoMode);
                    n.put("p_no", chNo);
                    n.put("p_unit", unitCode);
                    n.put("p_tp", paramBinding.getValue());
                } else {
                    System.out.println("inside challan reminder");
                    n.put("p_no", chNo);
                    n.put("p_unit", unitCode);
                }

                conn = getConnection();

                JasperReport design = (JasperReport) JRLoader.loadObject(input);
                System.out.println("Path : " + input + " -------" + design + " param:" + n);

                @SuppressWarnings("unchecked")
                net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                byte pdf[] = JasperExportManager.exportReportToPdf(print);
                HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                response.getOutputStream().write(pdf);
                response.getOutputStream().flush();
                response.getOutputStream().close();
                facesContext.responseComplete();
            } else
                System.out.println("File Name/File Path not found.");
        } catch (FileNotFoundException fnfe) {
            // TODO: Add catch code
            fnfe.printStackTrace();
            try {
                System.out.println("in finally connection closed");
                conn.close();
                conn = null;

            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            // TODO: Add catch code
            e.printStackTrace();
            try {
                System.out.println("in finally connection closed");
                conn.close();
                conn = null;

            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        } finally {
            try {
                System.out.println("in finally connection closed");
                conn.close();
                conn = null;

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public Connection getConnection() throws Exception {
        Context ctx = null;
        Connection conn = null;
        ctx = new InitialContext();
        DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/ApplicationDBDS");
        conn = ds.getConnection();
        return conn;
    }


    public void setParamBinding(RichSelectOneChoice paramBinding) {
        this.paramBinding = paramBinding;
    }

    public RichSelectOneChoice getParamBinding() {
        return paramBinding;
    }
}
