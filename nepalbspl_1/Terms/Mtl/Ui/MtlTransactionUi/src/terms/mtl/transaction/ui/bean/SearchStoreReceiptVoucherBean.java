package terms.mtl.transaction.ui.bean;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.faces.application.FacesMessage;

import javax.faces.context.FacesContext;

import javax.naming.Context;
import javax.naming.InitialContext;

import javax.servlet.http.HttpServletResponse;

import javax.sql.DataSource;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.domain.Timestamp;

public class SearchStoreReceiptVoucherBean {
    private RichTable searchSrvTableBinding;
    private String PoMode = "";

    public void setPoMode(String PoMode) {
        this.PoMode = PoMode;
    }

    public String getPoMode() {
        return PoMode;
    }

    public SearchStoreReceiptVoucherBean() {
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            OperationBinding op = null;
            ADFUtils.findOperation("Delete").execute();
            op = (OperationBinding) ADFUtils.findOperation("Commit");
            Object rst = op.execute();
            System.out.println("Record Delete Successfully");
            if (op.getErrors().isEmpty()) {
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            } else if (!op.getErrors().isEmpty()) {
                OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
                Object rstr = opr.execute();
                FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            }
        }

        AdfFacesContext.getCurrentInstance().addPartialTarget(searchSrvTableBinding);
    }

    public void setSearchSrvTableBinding(RichTable searchSrvTableBinding) {
        this.searchSrvTableBinding = searchSrvTableBinding;
    }

    public RichTable getSearchSrvTableBinding() {
        return searchSrvTableBinding;
    }

    //*********************Report Calling Code***********************//

    public BindingContainer getBindings() {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
    }

    public void downloadJrxml(FacesContext facesContext, OutputStream outputStream) {
        Connection conn = null;
        try {

            String file_name = "";
            String vouNo = null;
            System.out.println("PoMode===================>" + PoMode);
            DCIteratorBinding poIter = (DCIteratorBinding) getBindings().get("SearchStoreReceiptVoucherVO1Iterator");
            if (poIter.getCurrentRow().getAttribute("VouNo") != null) {
                vouNo = poIter.getCurrentRow().getAttribute("VouNo").toString();
            }

            if (PoMode.equals("Acc")) {
                file_name = "srv_accounts.jasper";
            } else if (PoMode.equals("Str")) {
                file_name = "srv_store.jasper";
            } else if (PoMode.equals("Shr")) {
                file_name = "r_shortage_mrn.jasper";
            } else if (PoMode.equals("Rej")) {
                file_name = "r_rejection_mrn.jasper";
            } else if (PoMode.equals("Exc")) {
                file_name = "r_excess_mrn.jasper";
            } else if (PoMode.equals("Lbl")) {
                System.out.println("inside label printing");
                file_name = "r_slabl.jasper";
            } else if (PoMode.equals("Crn")) {
                file_name = "r_req_crnote.jasper";
            } else if (vouNo != null && PoMode.equals("Bill")) {
                System.out.println("inside bill vou not null printing");
                file_name = "r_jour_prn.jasper";
            } else if (vouNo == null && PoMode.equals("Bill")) {
                System.out.println("inside bill vou is null printing");
                file_name = "r_bill_prn.jasper";
            }

            OperationBinding binding = ADFUtils.findOperation("fileNameForPrint");
            binding.getParamsMap().put("fileId", "ERP0000000047");
            binding.execute();

            System.out.println("Binding Result :" + binding.getResult().toString());
            if (binding.getResult() != null) {
                String result = binding.getResult().toString();
                int last_index = result.lastIndexOf("/");
                String path = result.substring(0, last_index + 1) + file_name;
                //                System.out.println("FILE PATH IS===>"+path);
                InputStream input = new FileInputStream(path);
                String str_app_date = "";
                Timestamp billDT = new Timestamp();
                Timestamp billAppDt = new Timestamp();

                String str_date = "";
                if (poIter.getCurrentRow().getAttribute("BillAppDate") != null) {
                    billAppDt = (Timestamp) poIter.getCurrentRow().getAttribute("BillAppDate");
                    str_app_date = billAppDt.toString().substring(0, 10);
                }
                if (poIter.getCurrentRow().getAttribute("BillPassDate") != null) {
                    billDT = (Timestamp) poIter.getCurrentRow().getAttribute("BillPassDate");
                    str_date = billDT.toString().substring(0, 10);
                    //                    billDT = new oracle.jbo.domain.Date(str_date.substring(0, 10));
                }
                System.out.println("vou date and vou no is==>>" + str_app_date + "  vou no   " + vouNo);
                String srvNum = poIter.getCurrentRow().getAttribute("SrvNo").toString();

                String unitCode = poIter.getCurrentRow().getAttribute("UnitCd").toString();
                System.out.println("SRV Number :- " + srvNum + " " + unitCode);
                Map n = new HashMap();
                n.put("p_unit", unitCode);
                n.put("p_no", srvNum);
                if (vouNo != null && PoMode.equals("Bill")) {

                    n.put("p_vou_fr", vouNo);
                    n.put("p_vou_to", vouNo);
                    n.put("p_fr_dt", str_app_date);
                    n.put("p_to_dt", str_app_date);
                    n.put("p_voutp", "P");
                    n.put("p_unit", unitCode);

                } else if (PoMode.equals("Bill") && vouNo == null) {

                    n.put("p_vou_fr", poIter.getCurrentRow().getAttribute("SrvId"));
                    n.put("p_vou_to", poIter.getCurrentRow().getAttribute("SrvId"));
                    n.put("p_fr_dt", str_date);
                    n.put("p_to_dt", str_date);
                    n.put("p_voutp", "P");
                    n.put("p_unit", unitCode);

                }
                conn = getConnection();

                JasperReport design = (JasperReport) JRLoader.loadObject(input);
                System.out.println("Path : " + input + " -------" + design + " param:" + n);

                @SuppressWarnings("unchecked")
                net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                byte pdf[] = JasperExportManager.exportReportToPdf(print);
                HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                response.getOutputStream().write(pdf);
                response.getOutputStream().flush();
                response.getOutputStream().close();
                facesContext.responseComplete();
            } else
                System.out.println("File Name/File Path not found.");
        } catch (FileNotFoundException fnfe) {
            // TODO: Add catch code
            fnfe.printStackTrace();
            try {
                System.out.println("in finally connection closed");
                conn.close();
                conn = null;

            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            // TODO: Add catch code
            e.printStackTrace();
            try {
                System.out.println("in finally connection closed");
                conn.close();
                conn = null;

            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        } finally {
            try {
                System.out.println("in finally connection closed");
                conn.close();
                conn = null;

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public Connection getConnection() throws Exception {
        Context ctx = null;
        Connection conn = null;
        ctx = new InitialContext();
        DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/ApplicationDBDS");
        conn = ds.getConnection();
        return conn;
    }
}
