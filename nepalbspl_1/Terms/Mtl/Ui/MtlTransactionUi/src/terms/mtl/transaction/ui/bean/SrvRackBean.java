package terms.mtl.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.model.binding.DCIteratorBinding;

import oracle.adf.view.rich.component.rich.input.RichInputText;

import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichPanelLabelAndMessage;

import oracle.binding.OperationBinding;

import terms.mtl.transaction.model.view.SearchSrvRackFillingVORowImpl;

public class SrvRackBean {
   
    private RichInputText bindUnit;
    private RichPanelLabelAndMessage bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;

    public SrvRackBean() {
    }
        
       

       


    public void setBindUnit(RichInputText bindUnit) {
        this.bindUnit = bindUnit;
    }

    public RichInputText getBindUnit() {
        return bindUnit;
    }


    public void backAL(ActionEvent actionEvent) {
        Object Val=getBindUnit().getValue();
        OperationBinding op = ADFUtils.findOperation("getSrvBack");
        op.getParamsMap().put("bindUnitCd",Val);
        op.execute();
    }

    public void setBindingOutputText(RichPanelLabelAndMessage bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichPanelLabelAndMessage getBindingOutputText() {
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }
    private void cevmodecheck(){
          if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
              makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
              cevModeDisableComponent("V");
          }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
              makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
              cevModeDisableComponent("C");
          }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
              makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
              cevModeDisableComponent("E");
          }
      }
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
             try {
                 Method method1 =
                     component.getClass().getMethod("setDisabled", boolean.class);
                 if (method1 != null) {
                     method1.invoke(component, valueComponent);
                 }
             } catch (NoSuchMethodException e) {
                 try {
                     Method method =
                         component.getClass().getMethod("setReadOnly", boolean.class);
                     if (method != null) {
                         method.invoke(component, valueComponent);
                     }
                 } catch (Exception e1) {
                     // e.printStackTrace();//silently eat this exception.
                 }


             } catch (Exception e) {
                 // e.printStackTrace();//silently eat this exception.
             }
             List<UIComponent> childComponents = component.getChildren();
             for (UIComponent comp : childComponents) {
                 makeComponentHierarchyReadOnly(comp, valueComponent);
             }
         }
    public void cevModeDisableComponent(String mode) {
      //  getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
                if (mode.equals("E")) {
                      
                } else if (mode.equals("C")) {

                } else if (mode.equals("V")) {
                   
                
    }

    }

    public void saveAL(ActionEvent actionEvent) {
        {
            ADFUtils.findOperation("Commit").execute();
            FacesMessage Message = new FacesMessage("Record Updated Successfully.");   
            Message.setSeverity(FacesMessage.SEVERITY_INFO);   
            FacesContext fc = FacesContext.getCurrentInstance();   
            fc.addMessage(null, Message); 
        }
    }
}

