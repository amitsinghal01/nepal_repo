package terms.mtl.transaction.ui.bean;

import com.tangosol.dev.assembler.Return;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.input.RichSelectBooleanCheckbox;

import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import terms.mtl.transaction.model.view.SearchSrvRackFillingVORowImpl;

public class SrvStockBean {
    private RichSelectBooleanCheckbox bindEditSelect;

    public SrvStockBean() {
    }


    public void setBindEditSelect(RichSelectBooleanCheckbox bindEditSelect) {
        this.bindEditSelect = bindEditSelect;
    }

    public RichSelectBooleanCheckbox getBindEditSelect() {


        return bindEditSelect;
    }

    public String ViewSlctdDataAC() {
        DCIteratorBinding dciter = (DCIteratorBinding) ADFUtils.findIterator("SearchSrvRackFillingVO1Iterator");

        SearchSrvRackFillingVORowImpl empRow = (SearchSrvRackFillingVORowImpl) dciter.getCurrentRow();
        if (empRow != null && empRow.getEditSelect() != null) {
            //Object Val1 = empRow.getUnitCd();
            Object Val = empRow.getEditSelect();
            System.out.println("value is------>>>" + Val);
            if(Val.equals("Y")){
            // String Val=getBindEditSelect().getValue().toString();
            OperationBinding op = ADFUtils.findOperation("getSrveditSelect");
            
            op.getParamsMap().put("bindEditSelect", Val);
            Object rst = op.execute();
            }
        }
        return "goToNextLevel";
    }

    public void deletePopupDialog(DialogEvent dialogEvent) {
        
             ADFUtils.findOperation("Delete").execute();
            oracle.adf.model.OperationBinding op = (oracle.adf.model.OperationBinding) ADFUtils.findOperation("Commit");
            Object rst = op.execute();
            System.out.println("Record Delete Successfully");
            
            if(op.getErrors().isEmpty())
             {
            FacesMessage Message = new FacesMessage("Record Deleted Successfully");   
            Message.setSeverity(FacesMessage.SEVERITY_INFO);   
            FacesContext fc = FacesContext.getCurrentInstance();   
            fc.addMessage(null, Message);
             }
            else
            {
               FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
               Message.setSeverity(FacesMessage.SEVERITY_ERROR);
               FacesContext fc = FacesContext.getCurrentInstance();
               fc.addMessage(null, Message);

            }}
}
