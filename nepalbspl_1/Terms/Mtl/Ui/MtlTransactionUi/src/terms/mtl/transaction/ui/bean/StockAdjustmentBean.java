package terms.mtl.transaction.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import javax.faces.validator.ValidatorException;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class StockAdjustmentBean {
    private RichTable createStockAdjustTableBinding;
    private RichButton detailcreateBinding;
    private RichButton detaildeleteBinding;
    private RichOutputText bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichInputText adjustmentNoBinding;
    private RichInputText itemDescriptionBinding;
    private RichInputText processCodeBinding;
    private RichInputText processDescBinding;
    private RichInputText stockBinding;
    private RichInputText lotNoBinding;
    private RichInputDate adjustDateBinding;
    private RichInputText locationNameBinding;
    private RichInputText enteredByNameBinding;
    private RichInputText unitNameBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputComboboxListOfValues enteredByBinding;
    private RichSelectOneChoice tranTypeBinding;
    private RichInputText qtyBinding;
    private RichInputComboboxListOfValues transactionTypeBinding;
    private RichInputComboboxListOfValues approvedByBinding;
    private RichInputComboboxListOfValues preparedByBinding;

    public StockAdjustmentBean() {
    }

    public void saveAdjustNoAL(ActionEvent actionEvent) {

        OperationBinding opp = ADFUtils.findOperation("checkRrmkStockAdjstmnt");
        Object obb = opp.execute();

        if (opp.getErrors() != null && opp.getResult().equals(false)) 
        {
            String empcd = (String) ADFUtils.resolveExpression("#{pageFlowScope.empCode}");
            System.out.println("empcd" + empcd);
            ADFUtils.setColumnValue("StockAdjustmentHeaderVO1Iterator", "LastUpdatedBy", empcd);

            OperationBinding op = ADFUtils.findOperation("getAdjustNo");
            Object rst = op.execute();
            // System.out.println("value aftr getting result--->?" + rst);
            Integer TransValue = (Integer) bindingOutputText.getValue();
            System.out.println("TRANS VALUE" + TransValue);
            if (TransValue == 0) {
                if (rst.toString() != null && rst.toString() != "") {
                    if (op.getErrors().isEmpty()) {
                        OperationBinding binding = ADFUtils.findOperation("insertIntoStkLedgerStkAdjust");
                        Object ob = binding.execute();
                        if (binding.getResult() != null && binding.getResult().equals("T")) {
                            ADFUtils.findOperation("Commit").execute();
                            FacesMessage Message =
                                new FacesMessage("Record Saved Successfully. New Adjust No is " + rst + ".");
                            Message.setSeverity(FacesMessage.SEVERITY_INFO);
                            FacesContext fc = FacesContext.getCurrentInstance();
                            fc.addMessage(null, Message);
                        } else {
                            ADFUtils.findOperation("Rollback").execute();
                            ADFUtils.findOperation("CreateInsert1").execute();
                            ADFUtils.showMessage("Cannot generate the new records Because of internal issues.", 0);
                            AdfFacesContext.getCurrentInstance().addPartialTarget(createStockAdjustTableBinding);
                        }
                    }
                } else {
                    ADFUtils.findOperation("Rollback").execute();
                    ADFUtils.findOperation("CreateInsert1").execute();
                    ADFUtils.showMessage("Cannot generate the new records Because of internal issues", 0);
                }
            }

            else {
                if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ") {
                    if (op.getErrors().isEmpty()) {
                        ADFUtils.findOperation("Commit").execute();
                        ADFUtils.showMessage("Record Update Successfully.", 2);
                    }
                }
            }
        } else {
            ADFUtils.showMessage("Remarks should be mandatory", 0);
        }

    }

    public void itemVCE(ValueChangeEvent vce) {

        if (vce != null) {
            vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
            ADFUtils.findOperation("goForItemCd").execute();
        }

    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete").execute();
            //System.out.println("Record Delete Successfully");

            ADFUtils.showMessage("Record Update Successfully.", 2);


        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(createStockAdjustTableBinding);
    }

    public void setCreateStockAdjustTableBinding(RichTable createStockAdjustTableBinding) {
        this.createStockAdjustTableBinding = createStockAdjustTableBinding;
    }

    public RichTable getCreateStockAdjustTableBinding() {
        return createStockAdjustTableBinding;
    }

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        }
        //        else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
        //            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
        //            cevModeDisableComponent("E");
        //        }
    }

    public void cevModeDisableComponent(String mode) {
        if (mode.equals("C")) {
            getPreparedByBinding().setDisabled(true);
            getAdjustDateBinding().setDisabled(true);
            getProcessCodeBinding().setDisabled(true);
            getProcessDescBinding().setDisabled(true);
            getItemDescriptionBinding().setDisabled(true);
            getUnitNameBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
            //  getLotNoBinding().setDisabled(true);

            getLocationNameBinding().setDisabled(true);
            getEnteredByBinding().setDisabled(true);
            getEnteredByNameBinding().setDisabled(true);
            getStockBinding().setDisabled(true);
            getAdjustmentNoBinding().setDisabled(true);
            //             getDetaildeleteBinding().setDisabled(false);
        }
        if (mode.equals("V")) {
            getDetailcreateBinding().setDisabled(true);
            getDetaildeleteBinding().setDisabled(true);
        }
        if (mode.equals("E")) {
            getPreparedByBinding().setDisabled(true);
            getUnitNameBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);

            getLocationNameBinding().setDisabled(true);
            getEnteredByNameBinding().setDisabled(true);
        }
    }

    public void setDetailcreateBinding(RichButton detailcreateBinding) {
        this.detailcreateBinding = detailcreateBinding;
    }

    public RichButton getDetailcreateBinding() {
        return detailcreateBinding;
    }

    public void setDetaildeleteBinding(RichButton detaildeleteBinding) {
        this.detaildeleteBinding = detaildeleteBinding;
    }

    public RichButton getDetaildeleteBinding() {
        return detaildeleteBinding;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    //        public void EditButtonAL(ActionEvent actionEvent) {
    //            cevmodecheck();
    //        }

    public void setAdjustmentNoBinding(RichInputText adjustmentNoBinding) {
        this.adjustmentNoBinding = adjustmentNoBinding;
    }

    public RichInputText getAdjustmentNoBinding() {
        return adjustmentNoBinding;
    }

    public void setItemDescriptionBinding(RichInputText itemDescriptionBinding) {
        this.itemDescriptionBinding = itemDescriptionBinding;
    }

    public RichInputText getItemDescriptionBinding() {
        return itemDescriptionBinding;
    }

    public void setProcessCodeBinding(RichInputText processCodeBinding) {
        this.processCodeBinding = processCodeBinding;
    }

    public RichInputText getProcessCodeBinding() {
        return processCodeBinding;
    }

    public void setProcessDescBinding(RichInputText processDescBinding) {
        this.processDescBinding = processDescBinding;
    }

    public RichInputText getProcessDescBinding() {
        return processDescBinding;
    }

    public void setStockBinding(RichInputText stockBinding) {
        this.stockBinding = stockBinding;
    }

    public RichInputText getStockBinding() {
        return stockBinding;
    }

    public void setLotNoBinding(RichInputText lotNoBinding) {
        this.lotNoBinding = lotNoBinding;
    }

    public RichInputText getLotNoBinding() {
        return lotNoBinding;
    }

    public void setAdjustDateBinding(RichInputDate adjustDateBinding) {
        this.adjustDateBinding = adjustDateBinding;
    }

    public RichInputDate getAdjustDateBinding() {
        return adjustDateBinding;
    }

    public void setLocationNameBinding(RichInputText locationNameBinding) {
        this.locationNameBinding = locationNameBinding;
    }

    public RichInputText getLocationNameBinding() {
        return locationNameBinding;
    }

    public void setEnteredByNameBinding(RichInputText enteredByNameBinding) {
        this.enteredByNameBinding = enteredByNameBinding;
    }

    public RichInputText getEnteredByNameBinding() {
        return enteredByNameBinding;
    }

    public void setUnitNameBinding(RichInputText unitNameBinding) {
        this.unitNameBinding = unitNameBinding;
    }

    public RichInputText getUnitNameBinding() {
        return unitNameBinding;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setEnteredByBinding(RichInputComboboxListOfValues enteredByBinding) {
        this.enteredByBinding = enteredByBinding;
    }

    public RichInputComboboxListOfValues getEnteredByBinding() {
        return enteredByBinding;
    }

    public void setTranTypeBinding(RichSelectOneChoice tranTypeBinding) {
        this.tranTypeBinding = tranTypeBinding;
    }

    public RichSelectOneChoice getTranTypeBinding() {
        return tranTypeBinding;
    }

    public void setQtyBinding(RichInputText qtyBinding) {
        this.qtyBinding = qtyBinding;
    }

    public RichInputText getQtyBinding() {
        return qtyBinding;
    }

    public void qtyValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (object != null) {
            BigDecimal qty = (BigDecimal) object;

            BigDecimal stk = (BigDecimal) stockBinding.getValue();

            String typ = transactionTypeBinding.getValue().toString();

            if (typ.equalsIgnoreCase("I")) {
                //  System.out.println("In the if block of type*****");
                if (qty.compareTo(stk) ==
                    1) {
                    //System.out.println("in the if block of comparison*********");
                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                  "Quantity must be less than or equal to currenet stock.",
                                                                  null));
                }
            }
        }
    }

    public void setTransactionTypeBinding(RichInputComboboxListOfValues transactionTypeBinding) {
        this.transactionTypeBinding = transactionTypeBinding;
    }

    public RichInputComboboxListOfValues getTransactionTypeBinding() {
        return transactionTypeBinding;
    }

    public void setApprovedByBinding(RichInputComboboxListOfValues approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputComboboxListOfValues getApprovedByBinding() {
        return approvedByBinding;
    }

    public void ApprovedCheckVCE(ValueChangeEvent valueChangeEvent) {
        if (valueChangeEvent != null) {
            valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
            oracle.binding.OperationBinding op = ADFUtils.findOperation("CheckAuthority");
            op.getParamsMap().put("unitCd", unitCodeBinding.getValue());
            op.getParamsMap().put("EmpCd", approvedByBinding.getValue());
            op.execute();

        }
    }

    public void setPreparedByBinding(RichInputComboboxListOfValues preparedByBinding) {
        this.preparedByBinding = preparedByBinding;
    }

    public RichInputComboboxListOfValues getPreparedByBinding() {
        return preparedByBinding;
    }
}

