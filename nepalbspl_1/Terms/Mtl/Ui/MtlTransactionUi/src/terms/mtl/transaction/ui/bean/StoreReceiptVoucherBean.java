
package terms.mtl.transaction.ui.bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.text.ParseException;

import java.util.Calendar;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.validator.ValidatorException;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputFile;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.input.RichSelectOneRadio;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.nav.RichCommandLink;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.adf.view.rich.util.ResetUtils;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

import oracle.jbo.RowSetIterator;
import oracle.jbo.domain.Timestamp;

import org.apache.myfaces.trinidad.event.DisclosureEvent;
import org.apache.myfaces.trinidad.model.UploadedFile;

import terms.mtl.transaction.model.view.StoreReceiptVoucherDetailsVORowImpl;
import terms.mtl.transaction.model.view.StoreReceiptVoucherHeadVORowImpl;


public class StoreReceiptVoucherBean {
    private RichInputFile uploadFileBind;
    private RichOutputText filenNmPath;
    private RichSelectOneChoice entryTypeBinding;
    private RichInputText bingRecptQty;
    private RichPopup bindWrngPopUp;
    int res;
    int count = 1;
    private RichInputComboboxListOfValues bindEntryNo;
    private RichPopup testBindPopup;
    private RichPopup bindPopProp;
    private RichPanelHeader myPageRootComponent;
    private RichOutputText bindingOutputText;
    private RichButton detaildeleteBinding;
    private RichInputComboboxListOfValues bindUnitCde;
    private RichInputText bindUnitNme;
    private RichInputText bindSrvNo;
    private RichInputComboboxListOfValues bindVndrCde;
    private RichInputText bindPoVendr;
    private RichInputText bindChallanNo;
    private RichInputText bindnoOfCases;
    private RichInputDate bindSrvDate;
    private RichInputDate bindSrvgteDate;
    private RichInputText bindName1;
    private RichInputText bindName2;
    private RichInputDate bindDateTwo;
    private RichInputText bindGateEntryTime;
    private RichInputText bindShortAcess;
    private RichInputText bindLongName;
    private RichInputText bindPreparedNme;
    private RichInputText bindCountedByName;
    private RichTable attachedTableBinding;
    private RichShowDetailItem packingDetailTableBinding;
    private RichShowDetailItem srvDetailTableBinding;
    private RichInputComboboxListOfValues preparedByBinding;
    private RichInputText invoiceNumberBinding;
    private RichInputText srNoDocAttachBinding;
    private RichInputText netConvRateBinding;
    private RichInputText landedCostBinding;
    private RichInputText hsnCodeBinding;
    private RichInputText igstAmountBinding;
    private RichInputText igstRateBinding;
    private RichInputText cgstAmountBinding;
    private RichInputText cgstRateBinding;
    private RichInputText sgstAmountBinding;
    private RichInputText packingForwardBinding;
    private RichInputText discountedAmountBinding;
    private RichInputText basicAmountBinding;
    private RichInputText rejectedQtyBinding;
    private RichInputText acceptedQtyBinding;
    private RichInputText packingStandardBinding;
    private RichInputText receiptUomBinding;
    private RichInputText challanWeightBinding;
    private RichInputText challanQtyBinding;
    private RichInputText lotNumberBinding;
    private RichInputText processSeqBinding;
    private RichInputText processCodeBinding;
    private RichButton editButtonBinding;
    private RichButton proportionateCalculationButtonBinding;
    private RichShowDetailItem attachementTableBinding;
    private RichButton createDocAttachButtonBinding;
    private RichButton deleteDocAttachButtonBinding;
    private RichButton createpackingDetailsButtonBinding;
    private RichButton deletepackingDetailsButtonBinding;
    private RichInputText refrenceDocNumberBinding;
    private RichInputText seqNoPackingDetailBinding;
    private RichInputText freightAmountDetailBinding;
    private RichInputComboboxListOfValues itemCodeDetailBinding;
    private RichInputComboboxListOfValues bindLocation;
    private RichInputDate bindInvoiceDate;
    private RichInputText poNoBinding;
    private RichInputText poAmdNoBinding;
    private RichInputText statusBinding;
    private RichInputText discBinding;
    private RichInputText basicAmtBinding;
    private RichInputText discountBinding;
    private RichInputComboboxListOfValues authorisedByBinding;
    private RichButton cancelButtonBinding;
    private RichInputText cancelstatusBinding;
    private RichInputComboboxListOfValues cancelstatusBinding1;
    private RichSelectOneChoice cancelStatusBinding;
    private RichInputText currRateBinding;
    private RichInputText currValBinding;
    private RichInputText srvAddNoBinding;
    private RichInputComboboxListOfValues sgstRateBinding;
    private RichInputText grandCostBinding;
    private RichInputText netRateBinding;
    private RichInputDate approvedDateBinding;
    private RichInputDate countedDateBinding;
    private RichInputComboboxListOfValues countedByBinding;
    private RichInputText otherAmtBinding;
    private RichInputText indentByTransBinding;
    private RichInputText manfBinding;
    private RichInputText packAmountBinding;
    private RichInputText qcdNoBinding;
    private RichInputDate qcdDateBinding;
    private RichInputText docNoBinding;
    private RichInputText unitCdBinding;
    private RichInputText refDocBinding;
    private RichInputText refDocTypeBinding;
    private RichInputText fileNameBinding;
    private RichInputDate docdateBinding;
    private RichInputText conTypeBinding;
    private RichInputText pathBinding;
    private RichTable docAttachTableBinding;
    private RichInputText freightUnitBinding;
    private RichShowDetailItem billPassingTabBinding;
    private RichTable billPassTableBinding;
    private RichInputText diffAmtBinding;
    private RichInputText noDaysBinding;
    private RichInputDate dueDateBinding;
    private RichSelectOneRadio againstTypeBinding;
    private RichInputDate billPassbinding;
    private RichInputComboboxListOfValues billAuthByBinding;
    private RichShowDetailItem billPassTabBinding;
    private RichInputDate billAppDateBinding;
    private RichInputText vouNoBinding;
    private RichInputComboboxListOfValues billPrepByBinding;
    private RichInputText subCodeBinding;
    private RichInputText customBinding;
    private RichInputText other1Binding;
    private RichInputText other2Binding;
    private RichShowDetailItem mrnTabBinding;
    private RichInputText otherUnitBinding;
    private RichInputText shortQtyBinding;
    private RichInputText dedQtyBinding;
    private RichInputText allowQtyBinding;
    private RichTable detailTableBinding;
    private RichInputText impIgstBinding;
    private RichInputText invoiceValueBinding;
    private RichInputComboboxListOfValues gstBinding;
    private RichCommandLink linkBinding;
    private RichInputText indentNoTransBinding;
    private RichInputText processNameBinding;
    private RichInputText srvdatenepalbinding;

    public void setBindEntryNo(RichInputComboboxListOfValues bindEntryNo) {
        this.bindEntryNo = bindEntryNo;
    }

    public RichInputComboboxListOfValues getBindEntryNo() {
        return bindEntryNo;
    }

    public StoreReceiptVoucherBean() {
    }

    public void setUploadFileBind(RichInputFile uploadFileBind) {
        this.uploadFileBind = uploadFileBind;
    }

    public RichInputFile getUploadFileBind() {
        return uploadFileBind;
    }

    InputStream inputStream = null;
    InputStream SaveInputStream = null;
    private UploadedFile PhotoFile = null;
    private String AbsoluteFilePath = "";
    String imagePath = "";
    Boolean check = false;

    public void setPhotoFile(UploadedFile PhotoFile) {
        this.PhotoFile = PhotoFile;
    }

    public UploadedFile getPhotoFile() {
        return PhotoFile;
    }
    String Imagepath = "";

    public void fileUploadVCL(ValueChangeEvent vce) {
        if (vce.getNewValue() != null) {
            setPhotoFile((UploadedFile) vce.getNewValue());
            UploadedFile myfile = (UploadedFile) vce.getNewValue();
            String path = null;

            if (myfile == null) {

            } else {
                File dir = new File("/tmp/pht");
                File savedir = new File("/home/beta6/pics");
                if (!dir.exists()) {
                    try {
                        dir.mkdir();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (!savedir.exists()) {
                    try {
                        savedir.mkdir();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                // All uploaded files will be stored in below path
                path = "/home/beta6/pics/" + myfile.getFilename();
                //                Imagepath=path;
                File f = new File(path);
                if (!f.exists()) {
                    try {
                        f.createNewFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                inputStream = null;
                try {

                    inputStream = myfile.getInputStream();
                    SaveInputStream = myfile.getInputStream();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                check = true;
                //                System.out.println("check in bean" + check);
                Imagepath = uploadFile(PhotoFile, "/tmp/pht/" + PhotoFile.getFilename());

            }
        }
    }


    private String uploadFile(UploadedFile file, String path) {

        UploadedFile myfile = file;

        try {

            FileOutputStream out = new FileOutputStream(path);
            byte[] buffer = new byte[8192];
            int bytesRead = 0;
            while ((bytesRead = inputStream.read(buffer, 0, 8192)) != -1) {
                out.write(buffer, 0, bytesRead);
            }
            out.flush();
            out.close();
        } catch (Exception ex) {
            // handle exception
            ex.printStackTrace();
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
            }
        }


        //Returns the path where file is stored
        return path;
    }

    public void saveStoreRecieptAL(ActionEvent actionEvent) {
        String empcd=(String)ADFUtils.resolveExpression("#{pageFlowScope.empCode}") ; 
        System.out.println("empcd"+empcd);
        ADFUtils.setColumnValue("StoreReceiptVoucherHeadVO1Iterator", "LastUpdatedBy", empcd);
//        OperationBinding binding = ADFUtils.findOperation("checkForSrvNoDuplicacy");
//        binding.getParamsMap().put("gateEntry", bindEntryNo.getValue());
//        binding.execute();
//        if (binding.getResult() != null && binding.getResult().equals("Y")) {
//           ADFUtils.showMessage("MRN for this Gate Entry is already created", 1);
//        //
//        }
//        else{
        
       BigDecimal diff_amount=(BigDecimal) (diffAmtBinding.getValue()!=null?diffAmtBinding.getValue():new BigDecimal(0));
      System.out.println("diff amount is==>>"+diff_amount);
       if(diff_amount.compareTo(new BigDecimal(0))==0){
           System.out.println("inside iffffff");
        check = false;
        String UserName = System.getProperty("user.name");
        String path;
        if (getPhotoFile() != null) {
            path = "/home/" + UserName + "/" + PhotoFile.getFilename();
            Imagepath = SaveuploadFile(PhotoFile, path);
            //        System.out.println("path " + Imagepath);
            OperationBinding ob = ADFUtils.findOperation("StoreVoucherAddImagePath");
            ob.getParamsMap().put("ImagePath", Imagepath);
            ob.execute();
        }
        File directory = new File("/tmp/pht");
        //get all the files from a directory
        File[] fList = directory.listFiles();
        if (fList != null) {
            for (File file : fList) {
                //Delete all previously uploaded files
                // if (!"NoImage.png".equalsIgnoreCase(file.getName())) {
                file.delete();
                //}






            }
        }
        //setReceiptValue
        //ADFUtils.findOperation("setReceiptValue").execute();
        //             boolean result=reqDataDetails();
        //             if(result){
        // if (ADFUtils.resolveExpression("#{pageFlowScope.Mode}").equals("C")) {
        System.out.println("***************One****************");
        if (!getPackingStandardBinding().isDisabled()) {
            System.out.println("***************One****************");
            System.out.println("********************Inside********************");
            ADFUtils.findOperation("sumOfAccessAmunt").execute();
            DCIteratorBinding dciter =
                (DCIteratorBinding) ADFUtils.findIterator("StoreReceiptVoucherDetailsVO1Iterator");
            StoreReceiptVoucherDetailsVORowImpl srv_dtl_row =
                (StoreReceiptVoucherDetailsVORowImpl) dciter.getCurrentRow();
            if (srv_dtl_row != null && srv_dtl_row.getChallanQty() != null) {
                System.out.println("******************Inside srv_dtl_row!=null****************");
                BigDecimal recpt_qty = (BigDecimal) srv_dtl_row.getReceiptQty();
                BigDecimal Challan_qty = (BigDecimal) srv_dtl_row.getChallanQty();
                res = recpt_qty.compareTo(Challan_qty);
                System.out.println("*******************Result is*****************" + res);
                if (res == 0) {
                    boolean result = reqDataDetails();
                    if (result == true) {
                        ADFUtils.findOperation("Commit").execute();
                        System.out.println("*******************CREATE MODE SAVE Button ***********************");
                        FacesMessage Message =
                            new FacesMessage("Record Save Successfully.New MRN No. is " + bindSrvNo.getValue());
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);
                        FacesContext fc = FacesContext.getCurrentInstance();
                        fc.addMessage(null, Message);
                        ADFUtils.setEL("#{pageFlowScope.Mode}", "V");
                        cevmodecheck();
                        AdfFacesContext.getCurrentInstance().addPartialTarget(myPageRootComponent);
                        System.out.println("***************Two****************");
                    }
                } 
                
//                else if (res == -1 && count == 1) {
//                    System.out.println("***************Second Else if*************");
//                    RichPopup.PopupHints hints = new RichPopup.PopupHints();
//                    getBindWrngPopUp().show(hints);
//                } 
                else {
                    System.out.println("****************Save else Case************888");
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message = new FacesMessage("Record Saved Successfully.MRN No. is :"+bindSrvNo.getValue());
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);
                    ADFUtils.setEL("#{pageFlowScope.Mode}", "V");
                    cevmodecheck();
                    AdfFacesContext.getCurrentInstance().addPartialTarget(myPageRootComponent);
                    System.out.println("***************Third****************");
                }

            }

        } else {
            if(billAuthByBinding.getValue()==null){
            ADFUtils.findOperation("sumOfAccessAmunt").execute();
            ADFUtils.findOperation("Commit").execute();
            FacesMessage Message = new FacesMessage("Record updated successfully.");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
            ADFUtils.setEL("#{pageFlowScope.Mode}", "V");
            cevmodecheck();
            AdfFacesContext.getCurrentInstance().addPartialTarget(myPageRootComponent);
            System.out.println("***************Four****************");
            }
            else{
                System.out.println("inside== else");
                if(dueDateBinding.getValue()!=null){
                OperationBinding Opr=ADFUtils.findOperation("generateBillPassingMRN");
//                Opr.getParamsMap().put("UserName","E0609");
                 Opr.getParamsMap().put("UserName",ADFUtils.resolveExpression("#{pageFlowScope.empCode}"));
                Object Obj=Opr.execute();
                System.out.println("******Result is:"+Opr.getResult()); 
                String str_result=Opr.getResult().toString().substring(0,1);
                System.out.println("string for 1st char==>"+str_result);
                if(Opr.getResult()!=null  && str_result.equalsIgnoreCase("Y"))
                {
                    ADFUtils.findOperation("Commit").execute();
                    String vouStr=Opr.getResult().toString();
                String newR=vouStr.replaceFirst("Y", "");
                    vouNoBinding.setValue(newR);
                    ADFUtils.showMessage("Record Saved Successfully.New Purchase Bill No. is "+ADFUtils.evaluateEL("#{bindings.VouNo.inputValue}"), 2);
            }
                else{
                    System.out.println("inside else==>");
                    ADFUtils.showMessage("Error in generating voucher No."+Opr.getResult().toString(),2);
                    ADFUtils.findOperation("Rollback").execute();
                }
        }
                else{
                    ADFUtils.showMessage("Due Date is required.", 2);
                }
            }
        }
       }
       else{
           System.out.println("inside else==>>");
           ADFUtils.showMessage("Debit Credit Amount must be equal.", 0);
       }
//        }
       
    }


    private String SaveuploadFile(UploadedFile file, String path) {

        UploadedFile myfile = file;

        try {

            FileOutputStream out = new FileOutputStream(path);
            byte[] buffer = new byte[8192];
            int bytesRead = 0;
            while ((bytesRead = SaveInputStream.read(buffer, 0, 8192)) != -1) {
                out.write(buffer, 0, bytesRead);
            }
            out.flush();
            out.close();
        } catch (Exception ex) {
            // handle exception
            ex.printStackTrace();
        } finally {
            try {
                SaveInputStream.close();
            } catch (IOException e) {
            }
        }
        return path;
    }


    public void downloadDocment(FacesContext facesContext, OutputStream outputStream) {
        //Read file from particular path, path bind is binding of table field that contains path
        if (filenNmPath.getValue() != null) {
            File filed = new File(filenNmPath.getValue().toString());
            FileInputStream fis;
            byte[] b;
            try {
                fis = new FileInputStream(filed);

                int n;
                while ((n = fis.available()) > 0) {
                    b = new byte[n];
                    int result = fis.read(b);
                    outputStream.write(b, 0, b.length);
                    if (result == -1)
                        break;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    public void setFilenNmPath(RichOutputText filenNmPath) {
        this.filenNmPath = filenNmPath;
    }

    public RichOutputText getFilenNmPath() {
        return filenNmPath;
    }

    public void srvCreate() {
        ADFUtils.findOperation("SRVCreateInsert").execute();
        ADFUtils.findOperation("PropCreateInsert").execute();
    }

    public void addDocAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert1").execute();
        getBindEntryNo().setDisabled(true);
        getEntryTypeBinding().setDisabled(true);
        ADFUtils.findOperation("getStoreReciptSequenceDocNo").execute();
    }

    public void gateEntryNoValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (object != null) {
//            OperationBinding binding = ADFUtils.findOperation("checkForSrvNoDuplicacy");
//            binding.getParamsMap().put("gateEntry", object);
//            binding.execute();
//            if (binding.getResult() != null && binding.getResult().equals("Y")) {
//               ADFUtils.showMessage("MRN for this Gate Entry is already created", 1);
//            //
//            } 
//            else{
            OperationBinding binding1 = ADFUtils.findOperation("generateSrvNO");
            binding1.getParamsMap().put("gateEntry", object);
            binding1.execute();
//            }
        }

    }

    public void setEntryTypeBinding(RichSelectOneChoice entryTypeBinding) {
        this.entryTypeBinding = entryTypeBinding;
    }

    public RichSelectOneChoice getEntryTypeBinding() {
        return entryTypeBinding;
    }

    public void entryTypeVCL(ValueChangeEvent valueChangeEvent) {
        System.out.println("valueChangeEvent===" + valueChangeEvent.getNewValue());
        System.out.println("entryTypeBinding===" + entryTypeBinding.getValue());
    }


    public void populateSRVDetailsAL(ActionEvent actionEvent) {

        //  ADFUtils.findOperation("CreateInsert5").execute();
        OperationBinding binding = ADFUtils.findOperation("checkForSrvNoDuplicacy");
        binding.getParamsMap().put("gateEntry", bindEntryNo.getValue());
        binding.execute();
        if (binding.getResult() != null && binding.getResult().equals("Y")) {
           ADFUtils.showMessage("MRN for this Gate Entry is already created", 1);
        //
        } else{
        getBindEntryNo().setDisabled(true);
        getEntryTypeBinding().setDisabled(true);
        ADFUtils.findOperation("populateSrvDetailAM").execute();
        bindEntryNo.setDisabled(true);
        }

        //        ADFUtils.findOperation("gstCalc").execute();
        //        if(entryTypeBinding.getValue().toString().equalsIgnoreCase("SV")){
        //            statusBinding.setValue("I");
        //        }
    }

    public void bidDetailAdd(ActionEvent actionEvent) {
        //getStoreReciptSequenceBinDetails
        ADFUtils.findOperation("CreateInsert2").execute();
        getBindEntryNo().setDisabled(true);
        getEntryTypeBinding().setDisabled(true);
        seqNoPackingDetailBinding.setValue(ADFUtils.evaluateEL("#{bindings.StoreReceiptVoucherBinDetailsVO1Iterator.estimatedRowCount}"));
        ADFUtils.findOperation("getStoreReciptSequenceBinDetails").execute();
    }

    public void setBingRecptQty(RichInputText bingRecptQty) {
        this.bingRecptQty = bingRecptQty;
    }

    public RichInputText getBingRecptQty() {
        return bingRecptQty;
    }

    public void setBindWrngPopUp(RichPopup bindWrngPopUp) {
        this.bindWrngPopUp = bindWrngPopUp;
    }

    public RichPopup getBindWrngPopUp() {
        return bindWrngPopUp;
    }


    public String recptQtyValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
//        try {
//            System.out.println("Start Validator " + object);
//            if (object != null) {
//                DCIteratorBinding dciter =
//                    (DCIteratorBinding) ADFUtils.findIterator("StoreReceiptVoucherDetailsVO1Iterator");
//
//                StoreReceiptVoucherDetailsVORowImpl srv_dtl_row =
//                    (StoreReceiptVoucherDetailsVORowImpl) dciter.getCurrentRow();
//                System.out.println("Row Count :-" + dciter.getRangeSize() + "  " + srv_dtl_row.getChallanQty());
//                if (srv_dtl_row != null && srv_dtl_row.getChallanQty() != null) {
//                    BigDecimal recpt_qty = new BigDecimal(object.toString());
//                    BigDecimal Challan_qty = (BigDecimal) srv_dtl_row.getChallanQty();
//                    res = recpt_qty.compareTo(Challan_qty);
//                    System.out.println("Res Value " + res + "  " + count);
//                    if (res == 0) {
//                        System.out.println("Sop1");
//                        reqDataDetails();
//                    } else if (res == 1) {
//                                                System.out.println("Sop2");
//                                                FacesMessage message =
//                                                    new FacesMessage("Receipt Quantity can  not  greater than Challan Quantity!!");
//                                                message.setSeverity(FacesMessage.SEVERITY_ERROR);
//                                                FacesContext context = FacesContext.getCurrentInstance();
//                                                context.addMessage(bingRecptQty.getClientId(), message);
//
//
//
//
//
//                    } else if (res == -1 && count == 1) {
//                        System.out.println("Sop3");
//                        RichPopup.PopupHints hints = new RichPopup.PopupHints();
//                        getBindWrngPopUp().show(hints);
//                    }
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        return "Y";
    }

    public void CnfrmtnVal(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome() == DialogEvent.Outcome.yes) {
            System.out.println("***********POPUP*********");
            getBindWrngPopUp().cancel();
            res = 2;
            count = 2;
            System.out.println("Sop4");
            boolean result = reqDataDetails();
            //            if (result == true) {
            //                ADFUtils.findOperation("Commit").execute();
            //                System.out.println("*************Without Save Button Save Data********************");
            //                if (!getPackingStandardBinding().isDisabled()) {
            //                    FacesMessage Message =
            //                        new FacesMessage("Record Save Successfully.New SRV No. is " + bindSrvNo.getValue());
            //                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
            //                    FacesContext fc = FacesContext.getCurrentInstance();
            //                    fc.addMessage(null, Message);
            //                }
            //            }
        }

        if (dialogEvent.getOutcome() == DialogEvent.Outcome.no) {
            getBindWrngPopUp().cancel();
            FacesMessage message = new FacesMessage("Received quantity is less then challan quantity,Please Check");
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(bingRecptQty.getClientId(), message);
        }


    }

    public Boolean reqDataDetails() {
        Boolean Status = true;
        OperationBinding op = ADFUtils.findOperation("getRequredqtyDetails");
        Object rst = op.execute();
        System.out.println("what is the result " + rst);
        if (rst != null && rst != "" && rst.toString().equalsIgnoreCase("NO")) {
            FacesMessage message = new FacesMessage("Key NO. 102 for Main Store location not set to mtl prameter!!");
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(bingRecptQty.getClientId(), message);
            Status = false;
        }
        if (rst != null && rst != "" && rst.toString().equalsIgnoreCase("GT")) {
            FacesMessage message = new FacesMessage("Gate Entry No Not found!!");
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(bingRecptQty.getClientId(), message);
            Status = false;
        }
        if (rst != null && rst != "" && rst.toString().equalsIgnoreCase("cur")) {
            FacesMessage message = new FacesMessage("Problem in finding Currency conversion factor!!");
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(bingRecptQty.getClientId(), message);
            Status = false;
        }
        if (rst != null && rst != "" && rst.toString().equalsIgnoreCase("Po")) {
            FacesMessage message = new FacesMessage("Purchase Order Number not found!!");
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(bingRecptQty.getClientId(), message);
            Status = false;
        }

        return Status;
    }


    public void gateEntryVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce != null) {
            
            
            OperationBinding binding = ADFUtils.findOperation("checkForSrvNoDuplicacy");
            binding.getParamsMap().put("gateEntry", vce.getNewValue().toString());
            binding.execute();
            if (binding.getResult() != null && binding.getResult().equals("Y")) {
               ADFUtils.showMessage("MRN for this Gate Entry is already created", 1);
//
            } else{
            

                OperationBinding bindings = ADFUtils.findOperation("generateSrvNO");
                bindings.getParamsMap().put("gateEntry", vce.getNewValue().toString());
                bindings.execute();
                System.out.println("Result from generate srv is==>>" + bindings.getResult());
                if (bindings.getResult() != null && bindings.getResult().toString().equalsIgnoreCase("Y")) {

                    ADFUtils.showMessage("This Is Inter Unit Stock Transfer Receiving.Only Store Entry Can Be Made.",
                                         1);
              
                //                bindEntryNo.setDisabled(true);

            }
            }
        }
    }

    public void gstCalVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());

        System.out.println("in gst cal vce");
        if (vce != null) {
           // vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
            OperationBinding opr = ADFUtils.findOperation("getCalculatedDataForGst");
            opr.getParamsMap().put("new_amt", discountBinding.getValue());
            opr.getParamsMap().put("flag", "G");
            opr.getParamsMap().put("pckAmt", packAmountBinding.getValue()==null? new BigDecimal(0) :packAmountBinding.getValue());
            opr.getParamsMap().put("freightAmt", freightAmountDetailBinding.getValue()==null? new BigDecimal(0) :freightAmountDetailBinding.getValue());
            opr.execute();
//            ADFUtils.findOperation("setItemDetailDataInBillJVPurchaseBill").execute();
                    if(authorisedByBinding.getValue()!=null ){
            ADFUtils.findOperation("setMaxBillDate").execute();
            ADFUtils.findOperation("setNoOfDaysFromPOInMRN").execute();
            ADFUtils.findOperation("setItemDetailDataInBillJVPurchaseBill").execute();
        DCIteratorBinding dciter =
            (DCIteratorBinding) ADFUtils.findIterator("StoreReceiptVoucherHeadVO1Iterator");
        StoreReceiptVoucherHeadVORowImpl srv_dtl_row =
            (StoreReceiptVoucherHeadVORowImpl) dciter.getCurrentRow();
        System.out.println("narraion==>"+srv_dtl_row.getAttribute("Narration"));
        if(srv_dtl_row.getAttribute("Narration")==null){
            ADFUtils.findOperation("narrationValueMrnBillPass").execute();
        }
        }  

            AdfFacesContext.getCurrentInstance().addPartialTarget(myPageRootComponent);
            //            ADFUtils.findOperation("gstCalc").execute();
        }
    }

    public void puCalcVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce.getNewValue() != null) {
            ADFUtils.findOperation("getPuCalculation").execute();
            OperationBinding opr = ADFUtils.findOperation("getCalculatedDataForGst");
            opr.getParamsMap().put("new_amt", discountBinding.getValue());
            opr.getParamsMap().put("flag", "G");
            opr.getParamsMap().put("pckAmt", packAmountBinding.getValue()==null? new BigDecimal(0) :packAmountBinding.getValue());
            opr.getParamsMap().put("freightAmt", freightAmountDetailBinding.getValue()==null? new BigDecimal(0) :freightAmountDetailBinding.getValue());
            opr.execute();
//            AdfFacesContext.getCurrentInstance().addPartialTarget(srvDetailTableBinding);
                    if(authorisedByBinding.getValue()!=null ){
            ADFUtils.findOperation("setMaxBillDate").execute();
            ADFUtils.findOperation("setNoOfDaysFromPOInMRN").execute();
            ADFUtils.findOperation("setItemDetailDataInBillJVPurchaseBill").execute();
        DCIteratorBinding dciter =
            (DCIteratorBinding) ADFUtils.findIterator("StoreReceiptVoucherHeadVO1Iterator");
        StoreReceiptVoucherHeadVORowImpl srv_dtl_row =
            (StoreReceiptVoucherHeadVORowImpl) dciter.getCurrentRow();
        System.out.println("narraion==>"+srv_dtl_row.getAttribute("Narration"));
        if(srv_dtl_row.getAttribute("Narration")==null){
            ADFUtils.findOperation("narrationValueMrnBillPass").execute();
        }
        }  
        }
    }


    public void setBindPopProp(RichPopup bindPopProp) {
        this.bindPopProp = bindPopProp;
    }

    public RichPopup getBindPopProp() {
        return bindPopProp;
    }


    public void toCacluate(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome() == DialogEvent.Outcome.ok) {
            System.out.println("**************Calculate*******************");
            getBindPopProp().cancel();
            ADFUtils.findOperation("puPropotionateCalculation").execute();
        }
    }


    public void editButtonAL(ActionEvent actionEvent) {
        setShortQty();
        Row row = (Row) ADFUtils.evaluateEL("#{bindings.StoreReceiptVoucherHeadVO1Iterator.currentRow}");
        String bill_no = (String) (row.getAttribute("BillNo") != null ? row.getAttribute("BillNo") : "N");
        System.out.print("Bill No ROw:=" + row.getAttribute("BillNo") + " Bill No:-" + bill_no);
        System.out.println("authorisedByBinding.getValue()=================>"+authorisedByBinding.getValue());
        
        if (cancelStatusBinding.getValue().equals("C")) {
            String mode = "V";
            System.out.println("mode at time srv cancel==>>>"+ADFUtils.resolveExpression("#{pageFlowScope.mode}"));
            ADFUtils.showMessage("Cancel MRN Can Not Be Modified  ", 2);
            
            ADFUtils.setEL("#{pageFlowScope.Mode}", "V");
//            cevmodecheck();
            
            System.out.println("mode at time srv cancel==>>>after"+ADFUtils.resolveExpression("#{pageFlowScope.Mode}"));
        }
        if (billAuthByBinding.getValue() != null && cancelStatusBinding.getValue().equals("A") && bill_no.equalsIgnoreCase("N")) {
            ADFUtils.showMessage("Bill is approved for this MRN.It cannot be Modified", 2);
             getCancelButtonBinding().setDisabled(false);
            ADFUtils.setEL("#{pageFlowScope.Mode}", "V");

        }

        else if (authorisedByBinding.getValue() != null && !bill_no.equalsIgnoreCase("N")) {
            OperationBinding opp = ADFUtils.findOperation("StatusToCancelSRV");
            Object rst = opp.execute();
            System.out.println("Result is****of methdo execute" + rst);
            if (rst != null && rst.equals("A")) {
                System.out.println("in the if block whn row is presnt");
                ADFUtils.showMessage("MRN can not be cancelled,Bill for this MRN has been generated.", 2);
                ADFUtils.setEL("#{pageFlowScope.mode}", "V");
                

            }
        }

        else {
            cevmodecheck();
//            disclosureEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
                 
            System.out.println("counted bby value==>>>"+countedByBinding.getValue());
            System.out.println("prpeare by bby value==>>>"+preparedByBinding.getValue());
            System.out.println("authorised bby value==>>>"+authorisedByBinding.getValue());
            if(countedByBinding.getValue()==null || countedByBinding.getValue()==""){ 
                
                System.out.println("Checked by isssss when null>>>>"+preparedByBinding.getValue());
                authorisedByBinding.setDisabled(true);
                countedByBinding.setDisabled(false);
                
            }
            else{
                
                System.out.println("counted bby value==>>>"+countedByBinding.getValue());
                countedByBinding.setDisabled(true);
//                authorisedByBinding.setDisabled(false);
//                preparedByBinding.setDisabled(true);
            }
        }
        
        if(authorisedByBinding.getValue()!=null){
            cancelButtonBinding.setDisabled(true);
        }
        else{
            cancelButtonBinding.setDisabled(false);
        }

    }

    public void setMyPageRootComponent(RichPanelHeader myPageRootComponent) {
        this.myPageRootComponent = myPageRootComponent;
    }

    public RichPanelHeader getMyPageRootComponent() {
        return myPageRootComponent;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setDetaildeleteBinding(RichButton detaildeleteBinding) {
        this.detaildeleteBinding = detaildeleteBinding;
    }

    public RichButton getDetaildeleteBinding() {

        return detaildeleteBinding;
    }

    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.Mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.Mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.Mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }

    public void cevModeDisableComponent(String mode) {
        if (mode.equals("E")) {

            //editmodeq
            indentNoTransBinding.setDisabled(true);
            invoiceValueBinding.setDisabled(true);
            billPrepByBinding.setDisabled(true);
            if(dueDateBinding.getValue()==null){
                billAuthByBinding.setDisabled(true);
            }
            else{
                billAuthByBinding.setDisabled(false);
            }
            if(billAuthByBinding.getValue()!=null){
                billAuthByBinding.setDisabled(true);
            }
            else{
                billAuthByBinding.setDisabled(false);
            }
            subCodeBinding.setDisabled(true);
            billPassTabBinding.setDisabled(false);
            unitCdBinding.setDisabled(true);
            refDocBinding.setDisabled(true);
            refDocTypeBinding.setDisabled(true);
            docNoBinding.setDisabled(true);
            docdateBinding.setDisabled(true);
            fileNameBinding.setDisabled(true);
            qcdDateBinding.setDisabled(true);
            qcdNoBinding.setDisabled(true);
            getCancelStatusBinding().setDisabled(true);
//            packAmountBinding.setDisabled(true);
            manfBinding.setDisabled(true);
            getIndentByTransBinding().setDisabled(true);
//            getOtherAmtBinding().setDisabled(true);
            getApprovedDateBinding().setDisabled(true);
//            getBingRecptQty().setDisabled(true);
            getNetRateBinding().setDisabled(true);
            getGrandCostBinding().setDisabled(true);
            getSgstRateBinding().setDisabled(true);
            getSrvAddNoBinding().setDisabled(true);
//            getCurrRateBinding().setDisabled(true);
            getCurrValBinding().setDisabled(true);
            getAuthorisedByBinding().setDisabled(false);
            getDiscBinding().setDisabled(true);
            getBasicAmtBinding().setDisabled(true);
            getSrNoDocAttachBinding().setDisabled(true);
            getPreparedByBinding().setDisabled(true);
            getBindSrvgteDate().setDisabled(true);
            getBindCountedByName().setDisabled(true);
            getBindShortAcess().setDisabled(true);
            getBindPreparedNme().setDisabled(true);
            getBindLongName().setDisabled(true);
            getBindChallanNo().setDisabled(true);
            getBindSrvNo().setDisabled(true);
            getBindDateTwo().setDisabled(true);
            getBindDateTwo().setDisabled(true);
            getBindGateEntryTime().setDisabled(true);
            getBindName1().setDisabled(true);
            getBindName2().setDisabled(true);
            getBindPoVendr().setDisabled(true);
            getBindSrvDate().setDisabled(true);
            getBindVndrCde().setDisabled(true);
            getBindUnitNme().setDisabled(true);
            getBindUnitCde().setDisabled(true);
            getDetaildeleteBinding().setDisabled(false);
            //getInvoiceNumberBinding().setDisabled(true);
            if(authorisedByBinding.getValue()!=null){
                authorisedByBinding.setDisabled(true);
            }
            else{
                authorisedByBinding.setDisabled(false);
            }
            getSrvdatenepalbinding().setDisabled(true);
            vouNoBinding.setDisabled(true);
            getNetConvRateBinding().setDisabled(true);
            getLandedCostBinding().setDisabled(true);
            getHsnCodeBinding().setDisabled(true);
//            getIgstAmountBinding().setDisabled(true);
            getIgstRateBinding().setDisabled(true);
//            getCgstAmountBinding().setDisabled(true);
            getCgstRateBinding().setDisabled(true);
//            getSgstAmountBinding().setDisabled(true);
            getDiscountedAmountBinding().setDisabled(true);
            getBasicAmountBinding().setDisabled(true);
            getRejectedQtyBinding().setDisabled(true);
            getAcceptedQtyBinding().setDisabled(true);
            getPackingStandardBinding().setDisabled(true); //only Edit mode

            getLotNumberBinding().setDisabled(true);
            getReceiptUomBinding().setDisabled(true);
            getChallanWeightBinding().setDisabled(true);
            getChallanQtyBinding().setDisabled(true);
            getProcessSeqBinding().setDisabled(true);
            getProcessCodeBinding().setDisabled(true);

            getBindEntryNo().setDisabled(true);
            getEntryTypeBinding().setDisabled(true);
            getEditButtonBinding().setDisabled(true);
            getRefrenceDocNumberBinding().setDisabled(true);
            getProportionateCalculationButtonBinding().setDisabled(false);
            getSeqNoPackingDetailBinding().setDisabled(true);


            getDetaildeleteBinding().setDisabled(false);
            getCreateDocAttachButtonBinding().setDisabled(false);
            getCreatepackingDetailsButtonBinding().setDisabled(false);
            getDeleteDocAttachButtonBinding().setDisabled(false);
            getDeletepackingDetailsButtonBinding().setDisabled(false);
            getProportionateCalculationButtonBinding().setDisabled(false);
//            getFreightAmountDetailBinding().setDisabled(true);
            getItemCodeDetailBinding().setDisabled(true);
            getCountedDateBinding().setDisabled(true);
            getBindLocation().setDisabled(true);
            //getBindInvoiceDate().setDisabled(true);
            //getInvoiceNumberBinding().setDisabled(true);
            getPoNoBinding().setDisabled(true);
            getPoAmdNoBinding().setDisabled(true);
//            billAppDateBinding.setDisabled(true);
            getCancelButtonBinding().setDisabled(false);
            if(authorisedByBinding.getValue()==null){
                billPassTabBinding.setDisabled(true);
            }
            else{
                billPassTabBinding.setDisabled(false);
            }
            // getCancelstatusBinding1().setDisabled(true);
            processNameBinding.setDisabled(true);
            sgstAmountBinding.setDisabled(true);
            cgstAmountBinding.setDisabled(true);
            igstAmountBinding.setDisabled(true);

        } else if (mode.equals("C")) {
            indentNoTransBinding.setDisabled(true);
            invoiceValueBinding.setDisabled(true);
            billPrepByBinding.setDisabled(true);
            billPassTabBinding.setDisabled(false);
            vouNoBinding.setDisabled(true);
            billPassTabBinding.setDisabled(true);
            unitCdBinding.setDisabled(true);
            refDocBinding.setDisabled(true);
            refDocTypeBinding.setDisabled(true);
            docNoBinding.setDisabled(true);
            docdateBinding.setDisabled(true);
            fileNameBinding.setDisabled(true);
            qcdDateBinding.setDisabled(true);
            qcdNoBinding.setDisabled(true);
            getCancelStatusBinding().setDisabled(true);
//            packAmountBinding.setDisabled(true);
            manfBinding.setDisabled(true);
            getIndentByTransBinding().setDisabled(true);
//            getOtherAmtBinding().setDisabled(true);
            getCountedByBinding().setDisabled(true);
            getApprovedDateBinding().setDisabled(true);
            getNetRateBinding().setDisabled(true);
            getGrandCostBinding().setDisabled(true);
            getSgstRateBinding().setDisabled(true);
            getSrvAddNoBinding().setDisabled(true);
//            getCurrRateBinding().setDisabled(true);
            getCurrValBinding().setDisabled(true);
            getAuthorisedByBinding().setDisabled(true);
            getDiscBinding().setDisabled(true);
            getBasicAmtBinding().setDisabled(true);
            getSrNoDocAttachBinding().setDisabled(true);
            getPreparedByBinding().setDisabled(true);
            getBindSrvgteDate().setDisabled(true);
            getBindCountedByName().setDisabled(true);
            getBindShortAcess().setDisabled(true);
            getBindPreparedNme().setDisabled(true);
            getBindLongName().setDisabled(true);
            getBindUnitCde().setDisabled(true);
            getBindChallanNo().setDisabled(true);
            getBindSrvNo().setDisabled(true);
            getBindDateTwo().setDisabled(true);
            getBindDateTwo().setDisabled(true);
            getBindGateEntryTime().setDisabled(true);
            getBindName1().setDisabled(true);
            getBindName2().setDisabled(true);
            getBindPoVendr().setDisabled(true);
            getBindSrvDate().setDisabled(true);
            getBindVndrCde().setDisabled(true);
            getBindUnitNme().setDisabled(true);
            getCountedDateBinding().setDisabled(true);
            getNetConvRateBinding().setDisabled(true);
            getLandedCostBinding().setDisabled(true);
            getHsnCodeBinding().setDisabled(true);
//            getIgstAmountBinding().setDisabled(true);
            getIgstRateBinding().setDisabled(true);
//            getCgstAmountBinding().setDisabled(true);
            getCgstRateBinding().setDisabled(true);
//            getSgstAmountBinding().setDisabled(true);
//            getPackingForwardBinding().setDisabled(true); //create mode only

            getDiscountedAmountBinding().setDisabled(true);
            getBasicAmountBinding().setDisabled(true);
            getRejectedQtyBinding().setDisabled(true);
            getAcceptedQtyBinding().setDisabled(true);
            getLotNumberBinding().setDisabled(true);
            getReceiptUomBinding().setDisabled(true);
            getChallanWeightBinding().setDisabled(true);
            getChallanQtyBinding().setDisabled(true);
            getProcessSeqBinding().setDisabled(true);
            getProcessCodeBinding().setDisabled(true);
            getRefrenceDocNumberBinding().setDisabled(true);
            getSeqNoPackingDetailBinding().setDisabled(true);
//            getFreightAmountDetailBinding().setDisabled(true);
            getItemCodeDetailBinding().setDisabled(true);
            getBindLocation().setDisabled(true);
            //getBindInvoiceDate().setDisabled(true);
            //getInvoiceNumberBinding().setDisabled(true);
            getSrvdatenepalbinding().setDisabled(true);
            getPoNoBinding().setDisabled(true);
            getPoAmdNoBinding().setDisabled(true);
            getCancelButtonBinding().setDisabled(true);
            //  getCancelstatusBinding1().setDisabled(true);
            processNameBinding.setDisabled(true);
            sgstAmountBinding.setDisabled(true);
            cgstAmountBinding.setDisabled(true);
            igstAmountBinding.setDisabled(true);

        } else if (mode.equals("V")) {
            linkBinding.setDisabled(false);
            getAttachementTableBinding().setDisabled(false);
            getPackingDetailTableBinding().setDisabled(false);
            getSrvDetailTableBinding().setDisabled(false);
            getDetaildeleteBinding().setDisabled(true);
            getCreateDocAttachButtonBinding().setDisabled(true);
            getCreatepackingDetailsButtonBinding().setDisabled(true);
            getDeleteDocAttachButtonBinding().setDisabled(true);
            getDeletepackingDetailsButtonBinding().setDisabled(true);
            getProportionateCalculationButtonBinding().setDisabled(true);
            getCancelButtonBinding().setDisabled(true);
            if(authorisedByBinding.getValue()!=null){
            billPassTabBinding.setDisabled(false);
            }
            mrnTabBinding.setDisabled(false);
        }
    }

    public void setBindUnitCde(RichInputComboboxListOfValues bindUnitCde) {
        this.bindUnitCde = bindUnitCde;
    }

    public RichInputComboboxListOfValues getBindUnitCde() {
        return bindUnitCde;
    }

    public void setBindUnitNme(RichInputText bindUnitNme) {
        this.bindUnitNme = bindUnitNme;
    }

    public RichInputText getBindUnitNme() {
        return bindUnitNme;
    }

    public void setBindSrvNo(RichInputText bindSrvNo) {
        this.bindSrvNo = bindSrvNo;
    }

    public RichInputText getBindSrvNo() {
        return bindSrvNo;
    }

    public void setBindVndrCde(RichInputComboboxListOfValues bindVndrCde) {
        this.bindVndrCde = bindVndrCde;
    }

    public RichInputComboboxListOfValues getBindVndrCde() {
        return bindVndrCde;
    }

    public void setBindPoVendr(RichInputText bindPoVendr) {
        this.bindPoVendr = bindPoVendr;
    }

    public RichInputText getBindPoVendr() {
        return bindPoVendr;
    }

    public void setBindChallanNo(RichInputText bindChallanNo) {
        this.bindChallanNo = bindChallanNo;
    }

    public RichInputText getBindChallanNo() {
        return bindChallanNo;
    }

    public void setBindnoOfCases(RichInputText bindnoOfCases) {
        this.bindnoOfCases = bindnoOfCases;
    }

    public RichInputText getBindnoOfCases() {
        return bindnoOfCases;
    }

    public void setBindSrvDate(RichInputDate bindSrvDate) {
        this.bindSrvDate = bindSrvDate;
    }

    public RichInputDate getBindSrvDate() {
        try {
              System.out.println("set getBindSrvDate"+ADFUtils.getTodayDate());
              Object date=ADFUtils.convertAdToBs(ADFUtils.getTodayDate().toString());
              System.out.println("setter getvaluedate"+date);
              srvdatenepalbinding.setValue(date.toString());
        //            DCIteratorBinding dci = ADFUtils.findIterator("SaleOrderHeaderVO1Iterator");
        //            dci.getCurrentRow().setAttribute("AckDateNepal",date);
          } catch (ParseException e) {
          }
        return bindSrvDate;
    }

    public void setBindSrvgteDate(RichInputDate bindSrvgteDate) {
        this.bindSrvgteDate = bindSrvgteDate;
    }

    public RichInputDate getBindSrvgteDate() {
        return bindSrvgteDate;
    }

    public void setBindName1(RichInputText bindName1) {
        this.bindName1 = bindName1;
    }

    public RichInputText getBindName1() {
        return bindName1;
    }

    public void setBindName2(RichInputText bindName2) {
        this.bindName2 = bindName2;
    }

    public RichInputText getBindName2() {
        return bindName2;
    }

    public void setBindDateTwo(RichInputDate bindDateTwo) {
        this.bindDateTwo = bindDateTwo;
    }

    public RichInputDate getBindDateTwo() {
        return bindDateTwo;
    }

    public void setBindGateEntryTime(RichInputText bindGateEntryTime) {
        this.bindGateEntryTime = bindGateEntryTime;
    }

    public RichInputText getBindGateEntryTime() {
        return bindGateEntryTime;
    }

    public void setBindShortAcess(RichInputText bindShortAcess) {
        this.bindShortAcess = bindShortAcess;
    }

    public RichInputText getBindShortAcess() {
        return bindShortAcess;
    }

    public void setBindLongName(RichInputText bindLongName) {
        this.bindLongName = bindLongName;
    }

    public RichInputText getBindLongName() {
        return bindLongName;
    }

    public void setBindPreparedNme(RichInputText bindPreparedNme) {
        this.bindPreparedNme = bindPreparedNme;
    }

    public RichInputText getBindPreparedNme() {
        return bindPreparedNme;
    }

    public void setBindCountedByName(RichInputText bindCountedByName) {
        this.bindCountedByName = bindCountedByName;
    }

    public RichInputText getBindCountedByName() {
        return bindCountedByName;
    }

    public void deletePopupDetailDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(srvDetailTableBinding);

    }

    public void deletePopupPackingDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete2").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(packingDetailTableBinding);

    }

    public void deletePopupDocAttachDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete1").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(attachedTableBinding);
    }

    public void setAttachedTableBinding(RichTable attachedTableBinding) {
        this.attachedTableBinding = attachedTableBinding;
    }

    public RichTable getAttachedTableBinding() {
        return attachedTableBinding;
    }

    public void setPackingDetailTableBinding(RichShowDetailItem packingDetailTableBinding) {
        this.packingDetailTableBinding = packingDetailTableBinding;
    }

    public RichShowDetailItem getPackingDetailTableBinding() {
        return packingDetailTableBinding;
    }

    public void setSrvDetailTableBinding(RichShowDetailItem srvDetailTableBinding) {
        this.srvDetailTableBinding = srvDetailTableBinding;
    }

    public RichShowDetailItem getSrvDetailTableBinding() {
        return srvDetailTableBinding;
    }

    public void setPreparedByBinding(RichInputComboboxListOfValues preparedByBinding) {
        this.preparedByBinding = preparedByBinding;
    }

    public RichInputComboboxListOfValues getPreparedByBinding() {
        return preparedByBinding;
    }

    public void setInvoiceNumberBinding(RichInputText invoiceNumberBinding) {
        this.invoiceNumberBinding = invoiceNumberBinding;
    }

    public RichInputText getInvoiceNumberBinding() {
        return invoiceNumberBinding;
    }

    public void setSrNoDocAttachBinding(RichInputText srNoDocAttachBinding) {
        this.srNoDocAttachBinding = srNoDocAttachBinding;
    }

    public RichInputText getSrNoDocAttachBinding() {
        return srNoDocAttachBinding;
    }

    public void setNetConvRateBinding(RichInputText netConvRateBinding) {
        this.netConvRateBinding = netConvRateBinding;
    }

    public RichInputText getNetConvRateBinding() {
        return netConvRateBinding;
    }

    public void setLandedCostBinding(RichInputText landedCostBinding) {
        this.landedCostBinding = landedCostBinding;
    }

    public RichInputText getLandedCostBinding() {
        return landedCostBinding;
    }

    public void setHsnCodeBinding(RichInputText hsnCodeBinding) {
        this.hsnCodeBinding = hsnCodeBinding;
    }

    public RichInputText getHsnCodeBinding() {
        return hsnCodeBinding;
    }

    public void setIgstAmountBinding(RichInputText igstAmountBinding) {
        this.igstAmountBinding = igstAmountBinding;
    }

    public RichInputText getIgstAmountBinding() {
        return igstAmountBinding;
    }

    public void setIgstRateBinding(RichInputText igstRateBinding) {
        this.igstRateBinding = igstRateBinding;
    }

    public RichInputText getIgstRateBinding() {
        return igstRateBinding;
    }

    public void setCgstAmountBinding(RichInputText cgstAmountBinding) {
        this.cgstAmountBinding = cgstAmountBinding;
    }

    public RichInputText getCgstAmountBinding() {
        return cgstAmountBinding;
    }

    public void setCgstRateBinding(RichInputText cgstRateBinding) {
        this.cgstRateBinding = cgstRateBinding;
    }

    public RichInputText getCgstRateBinding() {
        return cgstRateBinding;
    }

    public void setSgstAmountBinding(RichInputText sgstAmountBinding) {
        this.sgstAmountBinding = sgstAmountBinding;
    }

    public RichInputText getSgstAmountBinding() {
        return sgstAmountBinding;
    }

    public void setPackingForwardBinding(RichInputText packingForwardBinding) {
        this.packingForwardBinding = packingForwardBinding;
    }

    public RichInputText getPackingForwardBinding() {
        return packingForwardBinding;
    }

    public void setDiscountedAmountBinding(RichInputText discountedAmountBinding) {
        this.discountedAmountBinding = discountedAmountBinding;
    }

    public RichInputText getDiscountedAmountBinding() {
        return discountedAmountBinding;
    }

    public void setBasicAmountBinding(RichInputText basicAmountBinding) {
        this.basicAmountBinding = basicAmountBinding;
    }

    public RichInputText getBasicAmountBinding() {
        return basicAmountBinding;
    }

    public void setRejectedQtyBinding(RichInputText rejectedQtyBinding) {
        this.rejectedQtyBinding = rejectedQtyBinding;
    }

    public RichInputText getRejectedQtyBinding() {
        return rejectedQtyBinding;
    }

    public void setAcceptedQtyBinding(RichInputText acceptedQtyBinding) {
        this.acceptedQtyBinding = acceptedQtyBinding;
    }

    public RichInputText getAcceptedQtyBinding() {
        return acceptedQtyBinding;
    }

    public void setPackingStandardBinding(RichInputText packingStandardBinding) {
        this.packingStandardBinding = packingStandardBinding;
    }

    public RichInputText getPackingStandardBinding() {
        return packingStandardBinding;
    }

    public void setReceiptUomBinding(RichInputText receiptUomBinding) {
        this.receiptUomBinding = receiptUomBinding;
    }

    public RichInputText getReceiptUomBinding() {
        return receiptUomBinding;
    }

    public void setChallanWeightBinding(RichInputText challanWeightBinding) {
        this.challanWeightBinding = challanWeightBinding;
    }

    public RichInputText getChallanWeightBinding() {
        return challanWeightBinding;
    }

    public void setChallanQtyBinding(RichInputText challanQtyBinding) {
        this.challanQtyBinding = challanQtyBinding;
    }

    public RichInputText getChallanQtyBinding() {
        return challanQtyBinding;
    }

    public void setLotNumberBinding(RichInputText lotNumberBinding) {
        this.lotNumberBinding = lotNumberBinding;
    }

    public RichInputText getLotNumberBinding() {
        return lotNumberBinding;
    }

    public void setProcessSeqBinding(RichInputText processSeqBinding) {
        this.processSeqBinding = processSeqBinding;
    }

    public RichInputText getProcessSeqBinding() {
        return processSeqBinding;
    }

    public void setProcessCodeBinding(RichInputText processCodeBinding) {
        this.processCodeBinding = processCodeBinding;
    }

    public RichInputText getProcessCodeBinding() {
        return processCodeBinding;
    }

    public void setEditButtonBinding(RichButton editButtonBinding) {
        this.editButtonBinding = editButtonBinding;
    }

    public RichButton getEditButtonBinding() {
        return editButtonBinding;
    }

    public void setProportionateCalculationButtonBinding(RichButton proportionateCalculationButtonBinding) {
        this.proportionateCalculationButtonBinding = proportionateCalculationButtonBinding;
    }

    public RichButton getProportionateCalculationButtonBinding() {
        return proportionateCalculationButtonBinding;
    }

    public void setAttachementTableBinding(RichShowDetailItem attachementTableBinding) {
        this.attachementTableBinding = attachementTableBinding;
    }

    public RichShowDetailItem getAttachementTableBinding() {
        return attachementTableBinding;
    }

    public void setCreateDocAttachButtonBinding(RichButton createDocAttachButtonBinding) {
        this.createDocAttachButtonBinding = createDocAttachButtonBinding;
    }

    public RichButton getCreateDocAttachButtonBinding() {
        return createDocAttachButtonBinding;
    }

    public void setDeleteDocAttachButtonBinding(RichButton deleteDocAttachButtonBinding) {
        this.deleteDocAttachButtonBinding = deleteDocAttachButtonBinding;
    }

    public RichButton getDeleteDocAttachButtonBinding() {
        return deleteDocAttachButtonBinding;
    }

    public void setCreatepackingDetailsButtonBinding(RichButton createpackingDetailsButtonBinding) {
        this.createpackingDetailsButtonBinding = createpackingDetailsButtonBinding;
    }

    public RichButton getCreatepackingDetailsButtonBinding() {
        return createpackingDetailsButtonBinding;
    }

    public void setDeletepackingDetailsButtonBinding(RichButton deletepackingDetailsButtonBinding) {
        this.deletepackingDetailsButtonBinding = deletepackingDetailsButtonBinding;
    }

    public RichButton getDeletepackingDetailsButtonBinding() {
        return deletepackingDetailsButtonBinding;
    }

    public void setRefrenceDocNumberBinding(RichInputText refrenceDocNumberBinding) {
        this.refrenceDocNumberBinding = refrenceDocNumberBinding;
    }

    public RichInputText getRefrenceDocNumberBinding() {
        return refrenceDocNumberBinding;
    }

    public void setSeqNoPackingDetailBinding(RichInputText seqNoPackingDetailBinding) {
        this.seqNoPackingDetailBinding = seqNoPackingDetailBinding;
    }

    public RichInputText getSeqNoPackingDetailBinding() {
        return seqNoPackingDetailBinding;
    }

    public void setFreightAmountDetailBinding(RichInputText freightAmountDetailBinding) {
        this.freightAmountDetailBinding = freightAmountDetailBinding;
    }

    public RichInputText getFreightAmountDetailBinding() {
        return freightAmountDetailBinding;
    }

    public void setItemCodeDetailBinding(RichInputComboboxListOfValues itemCodeDetailBinding) {
        this.itemCodeDetailBinding = itemCodeDetailBinding;
    }

    public RichInputComboboxListOfValues getItemCodeDetailBinding() {
        return itemCodeDetailBinding;
    }

    public void setBindLocation(RichInputComboboxListOfValues bindLocation) {
        this.bindLocation = bindLocation;
    }

    public RichInputComboboxListOfValues getBindLocation() {
        return bindLocation;
    }

    public void setBindInvoiceDate(RichInputDate bindInvoiceDate) {
        this.bindInvoiceDate = bindInvoiceDate;
    }

    public RichInputDate getBindInvoiceDate() {
        return bindInvoiceDate;
    }

    public void setPoNoBinding(RichInputText poNoBinding) {
        this.poNoBinding = poNoBinding;
    }

    public RichInputText getPoNoBinding() {
        return poNoBinding;
    }

    public void setPoAmdNoBinding(RichInputText poAmdNoBinding) {
        this.poAmdNoBinding = poAmdNoBinding;
    }

    public RichInputText getPoAmdNoBinding() {
        return poAmdNoBinding;
    }


    public String saveAndCloseButton() {
        String empcd=(String)ADFUtils.resolveExpression("#{pageFlowScope.empCode}") ; 
        System.out.println("empcd"+empcd);
        ADFUtils.setColumnValue("StoreReceiptVoucherHeadVO1Iterator", "LastUpdatedBy", empcd);
        
//        OperationBinding binding = ADFUtils.findOperation("checkForSrvNoDuplicacy");
//        binding.getParamsMap().put("gateEntry", bindEntryNo.getValue());
//        binding.execute();
//        if (binding.getResult() != null && binding.getResult().equals("Y")) {
//           ADFUtils.showMessage("MRN for this Gate Entry is already created", 1);
//        //
//        }
//        else{
        BigDecimal diff_amount=(BigDecimal) (diffAmtBinding.getValue()!=null?diffAmtBinding.getValue():new BigDecimal(0));
        System.out.println("diff amount is==>>"+diff_amount);
        if(diff_amount.compareTo(new BigDecimal(0))==0){
            System.out.println("inside iffffff");
        check = false;
        String UserName = System.getProperty("user.name");
        String path;
        if (getPhotoFile() != null) {
            path = "/home/" + UserName + "/" + PhotoFile.getFilename();
            Imagepath = SaveuploadFile(PhotoFile, path);
            //        System.out.println("path " + Imagepath);
            OperationBinding ob = ADFUtils.findOperation("StoreVoucherAddImagePath");
            ob.getParamsMap().put("ImagePath", Imagepath);
            ob.execute();
        }


        File directory = new File("/tmp/pht");
        //get all the files from a directory
        File[] fList = directory.listFiles();
        if (fList != null) {
            for (File file : fList) {
                //Delete all previously uploaded files
                // if (!"NoImage.png".equalsIgnoreCase(file.getName())) {
                file.delete();
      }
        }
        DCIteratorBinding dciter =
            (DCIteratorBinding) ADFUtils.findIterator("StoreReceiptVoucherDetailsVO1Iterator");

        StoreReceiptVoucherDetailsVORowImpl srv_dtl_row =
            (StoreReceiptVoucherDetailsVORowImpl) dciter.getCurrentRow();
        System.out.println("Row Count :-" + dciter.getRangeSize() + "  " + srv_dtl_row.getChallanQty());
        if (srv_dtl_row != null && srv_dtl_row.getChallanQty() != null) {
            BigDecimal recpt_qty = (BigDecimal)(srv_dtl_row.getReceiptQty());
            BigDecimal Challan_qty = (BigDecimal) srv_dtl_row.getChallanQty();
            System.out.println("recpt_qty"+recpt_qty+"Challan_qty"+Challan_qty);
            res = recpt_qty.compareTo(Challan_qty);
            System.out.println("Res Value " + res + "  " + count);
          
           
//            else{
                
                System.out.println("***************One****************");
                if (!getPackingStandardBinding().isDisabled()) {
                    System.out.println("***************One****************");
                    System.out.println("********************Inside********************");
                    ADFUtils.findOperation("sumOfAccessAmunt").execute();
                    DCIteratorBinding dciter1 =
                        (DCIteratorBinding) ADFUtils.findIterator("StoreReceiptVoucherDetailsVO1Iterator");

                    StoreReceiptVoucherDetailsVORowImpl srv_dtl_row1 =
                        (StoreReceiptVoucherDetailsVORowImpl) dciter.getCurrentRow();
                    if (srv_dtl_row != null && srv_dtl_row.getChallanQty() != null) {

                        System.out.println("******************Inside srv_dtl_row!=null****************");


                        BigDecimal recpt_qty1 = (BigDecimal) srv_dtl_row.getReceiptQty();
                        BigDecimal Challan_qty1 = (BigDecimal) srv_dtl_row.getChallanQty();


                        res = recpt_qty1.compareTo(Challan_qty1);
                        System.out.println("*******************Result is*****************" + res);
                        if (res == 0) {


                            boolean result = reqDataDetails();
                            if (result == true) {
                                ADFUtils.findOperation("Commit").execute();
                                System.out.println("*******************CREATE MODE SAVE Button ***********************");
                                FacesMessage Message =
                                    new FacesMessage("Record Save Successfully.New MRN No. is " + bindSrvNo.getValue());
                                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                                FacesContext fc = FacesContext.getCurrentInstance();
                                fc.addMessage(null, Message);
                                System.out.println("***************Two****************");
                                return "Save";


                            }
                        }
                        
                        else {
                            System.out.println("****************Save else Case************888");
                            ADFUtils.findOperation("Commit").execute();
                            FacesMessage Message = new FacesMessage("Record Saved Successfully.MRN No. is :"+bindSrvNo.getValue());
                            Message.setSeverity(FacesMessage.SEVERITY_INFO);
                            FacesContext fc = FacesContext.getCurrentInstance();
                            fc.addMessage(null, Message);
                            System.out.println("***************Third****************");
                            return "Save";
                        }

                    }

                }

                else {
                    if(billAuthByBinding.getValue()==null){
                    ADFUtils.findOperation("sumOfAccessAmunt").execute();
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message = new FacesMessage("Record updated successfully.");
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);
                    ADFUtils.setEL("#{pageFlowScope.Mode}", "V");
                    cevmodecheck();
                    AdfFacesContext.getCurrentInstance().addPartialTarget(myPageRootComponent);
                    System.out.println("***************Four****************");
                    }
                    else{
                        System.out.println("inside== else");
                        if(dueDateBinding.getValue()!=null){
                        OperationBinding Opr=ADFUtils.findOperation("generateBillPassingMRN");
                        //                Opr.getParamsMap().put("UserName","E0609");
                         Opr.getParamsMap().put("UserName",ADFUtils.resolveExpression("#{pageFlowScope.empCode}"));
                        Object Obj=Opr.execute();
                        System.out.println("******Result is:"+Opr.getResult()); 
                        String str_result=Opr.getResult().toString().substring(0,1);
                        System.out.println("string for 1st char==>"+str_result);
                        if(Opr.getResult()!=null  && str_result.equalsIgnoreCase("Y"))
                        {
                            ADFUtils.findOperation("Commit").execute();
                            ADFUtils.showMessage("Record Saved Successfully.New Purchase Bill No. is "+ADFUtils.evaluateEL("#{bindings.VouNo.inputValue}"), 2);
                        }
                        else{
                            System.out.println("inside else==>");
                            ADFUtils.showMessage("Error in generating voucher No."+Opr.getResult().toString(),2);
                            ADFUtils.findOperation("Rollback").execute();
                        }
                        }
                        else{
                            ADFUtils.showMessage("Due Date is required.", 2);
                        }
                    }
                    return "Save";

                }
                }
        }
        else{
            System.out.println("inside else==>>");
            ADFUtils.showMessage("Debit Credit Amount must be equal.", 0);
        }
//        }
     
        return null;
        //}
    }

    public void setStatusBinding(RichInputText statusBinding) {
        this.statusBinding = statusBinding;
    }

    public RichInputText getStatusBinding() {
        return statusBinding;
    }

    public void recQtyVCE(ValueChangeEvent vce) {
        System.out.println("in rec qty vce");
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce != null) {
        OperationBinding opr = ADFUtils.findOperation("getCalculatedDataForGst");
            opr.getParamsMap().put("new_amt", discBinding.getValue());
            opr.getParamsMap().put("flag", "A");
        opr.getParamsMap().put("freightAmt", freightAmountDetailBinding.getValue()==null? new BigDecimal(0) :freightAmountDetailBinding.getValue());
        opr.getParamsMap().put("pckAmt", packAmountBinding.getValue()==null? new BigDecimal(0) :packAmountBinding.getValue());
            opr.execute();
        ADFUtils.findOperation("getPuCalculation").execute();
        if(authorisedByBinding.getValue()!=null ){
            ADFUtils.findOperation("setMaxBillDate").execute();
            ADFUtils.findOperation("setNoOfDaysFromPOInMRN").execute();
            ADFUtils.findOperation("setItemDetailDataInBillJVPurchaseBill").execute();
        DCIteratorBinding dciter =
            (DCIteratorBinding) ADFUtils.findIterator("StoreReceiptVoucherHeadVO1Iterator");
        StoreReceiptVoucherHeadVORowImpl srv_dtl_row =
            (StoreReceiptVoucherHeadVORowImpl) dciter.getCurrentRow();
        System.out.println("narraion==>"+srv_dtl_row.getAttribute("Narration"));
        if(srv_dtl_row.getAttribute("Narration")==null){
            ADFUtils.findOperation("narrationValueMrnBillPass").execute();
        }
        }  

    }
        }

    public void disCountVCE(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
    }

    public void discountVCE(ValueChangeEvent vce) {
        System.out.println("in discount vce");
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce != null) {
            vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
            OperationBinding opr = ADFUtils.findOperation("getCalculatedDataForGst");
            opr.getParamsMap().put("new_amt", vce.getNewValue());
            opr.getParamsMap().put("flag", "V");
            opr.getParamsMap().put("pckAmt", packAmountBinding.getValue()==null? new BigDecimal(0) :packAmountBinding.getValue());
            opr.getParamsMap().put("freightAmt", freightAmountDetailBinding.getValue()==null? new BigDecimal(0) :freightAmountDetailBinding.getValue());
            opr.execute();
//            ADFUtils.findOperation("setItemDetailDataInBillJVPurchaseBill").execute();
        if(authorisedByBinding.getValue()!=null ){
            ADFUtils.findOperation("setMaxBillDate").execute();
            ADFUtils.findOperation("setNoOfDaysFromPOInMRN").execute();
            ADFUtils.findOperation("setItemDetailDataInBillJVPurchaseBill").execute();
        DCIteratorBinding dciter =
            (DCIteratorBinding) ADFUtils.findIterator("StoreReceiptVoucherHeadVO1Iterator");
        StoreReceiptVoucherHeadVORowImpl srv_dtl_row =
            (StoreReceiptVoucherHeadVORowImpl) dciter.getCurrentRow();
        System.out.println("narraion==>"+srv_dtl_row.getAttribute("Narration"));
        if(srv_dtl_row.getAttribute("Narration")==null){
            ADFUtils.findOperation("narrationValueMrnBillPass").execute();
        }
        }  
        }
    }

    public void setDiscBinding(RichInputText discBinding) {
        this.discBinding = discBinding;
    }

    public RichInputText getDiscBinding() {
        return discBinding;
    }

    public void setBasicAmtBinding(RichInputText basicAmtBinding) {
        this.basicAmtBinding = basicAmtBinding;
    }

    public RichInputText getBasicAmtBinding() {
        return basicAmtBinding;
    }

    public void setDiscountBinding(RichInputText discountBinding) {
        this.discountBinding = discountBinding;
    }

    public RichInputText getDiscountBinding() {
        return discountBinding;
    }

    public void materialRateVCE(ValueChangeEvent valueChangeEvent) {

        System.out.println("in mate rate vce");
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (valueChangeEvent != null) {

            OperationBinding opr = ADFUtils.findOperation("getCalculatedDataForGst");
            opr.getParamsMap().put("new_amt", discountBinding.getValue());
            opr.getParamsMap().put("flag", "M");
            opr.getParamsMap().put("pckAmt", packAmountBinding.getValue()==null? new BigDecimal(0) :packAmountBinding.getValue());
            opr.getParamsMap().put("freightAmt", freightAmountDetailBinding.getValue()==null? new BigDecimal(0) :freightAmountDetailBinding.getValue());
            opr.execute();
            ADFUtils.findOperation("getPuCalculation").execute();
//            ADFUtils.findOperation("setItemDetailDataInBillJVPurchaseBill").execute();
        if(authorisedByBinding.getValue()!=null ){
            ADFUtils.findOperation("setMaxBillDate").execute();
            ADFUtils.findOperation("setNoOfDaysFromPOInMRN").execute();
            ADFUtils.findOperation("setItemDetailDataInBillJVPurchaseBill").execute();
        DCIteratorBinding dciter =
            (DCIteratorBinding) ADFUtils.findIterator("StoreReceiptVoucherHeadVO1Iterator");
        StoreReceiptVoucherHeadVORowImpl srv_dtl_row =
            (StoreReceiptVoucherHeadVORowImpl) dciter.getCurrentRow();
        System.out.println("narraion==>"+srv_dtl_row.getAttribute("Narration"));
        if(srv_dtl_row.getAttribute("Narration")==null){
            ADFUtils.findOperation("narrationValueMrnBillPass").execute();
        }
        }  
        }
    }

    public void cancelDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equalsIgnoreCase("ok")) {
                   OperationBinding op = ADFUtils.findOperation("StatusToCancelSRV");
                   Object ob = op.execute();
                   System.out.println("Return Result is==>>" + op.getResult());


                   if (op.getResult() != null) {
                       System.out.println("When Result not null:-" + op.getResult());
                       if (op.getResult().equals("A")) {
                           System.out.println("When Result A:-" + op.getResult());

                           ADFUtils.showMessage("Bill is prepared for this MRN. Hence,it cannot be Cancelled.", 2);
                           ADFUtils.setEL("#{pageFlowScope.Mode}", "V");
                           cevmodecheck();
                           System.out.println("mode is====>>>" + ADFUtils.resolveExpression("#{pageFlowScope.mode}"));

                       } else if (op.getResult().equals("C")) {
                           System.out.println("When Result C:-" + op.getResult());
                           ADFUtils.showMessage("MRN Cancelled Succesfully.", 2);
                           ADFUtils.findOperation("setStatusGinGateEntry").execute();
                           ADFUtils.findOperation("Commit").execute();
                       }
                       
                   }

               }
    }


    public void setAuthorisedByBinding(RichInputComboboxListOfValues authorisedByBinding) {
        this.authorisedByBinding = authorisedByBinding;
    }

    public RichInputComboboxListOfValues getAuthorisedByBinding() {
        return authorisedByBinding;
    }

    public void setCancelButtonBinding(RichButton cancelButtonBinding) {
        this.cancelButtonBinding = cancelButtonBinding;
    }

    public RichButton getCancelButtonBinding() {
        return cancelButtonBinding;
    }

    public void setCancelstatusBinding(RichInputText cancelstatusBinding) {
        this.cancelstatusBinding = cancelstatusBinding;
    }

    public RichInputText getCancelstatusBinding() {
        return cancelstatusBinding;
    }

    public void authByVCE(ValueChangeEvent vce) {
//        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
//        if(vce.getNewValue()!=null){
            String poNo=null;
        DCIteratorBinding dciter12 =
            (DCIteratorBinding) ADFUtils.findIterator("StoreReceiptVoucherDetailsVO1Iterator");
        StoreReceiptVoucherDetailsVORowImpl row12 =
            (StoreReceiptVoucherDetailsVORowImpl) dciter12.getCurrentRow();
        System.out.println("row12.getPoNo()==>>"+row12.getPoNo());
        RowSetIterator rsi=dciter12.getViewObject().createRowSetIterator(null);
        while(rsi.hasNext()){
            Row r=rsi.next();
             poNo = (String) r.getAttribute("PoNo");
        }
        rsi.closeRowSetIterator();
        if(poNo==null){
          authorisedByBinding.setValue(null);
            AdfFacesContext.getCurrentInstance().addPartialTarget(authorisedByBinding);
            ADFUtils.showMessage("As this is PO based MRN,You cannot approved it.", 0);
            
        }
        
    
        else{
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        OperationBinding ob = ADFUtils.findOperation("checkApprovalStatus");
        ob.getParamsMap().put("formNm", "SRV");
        ob.getParamsMap().put("authoLim", "AP");
        ob.getParamsMap().put("empcd", vce.getNewValue().toString());
        ob.execute();
        System.out.println("Before pack amount=============="+packAmountBinding.getValue()+"Before freight maount===////////"+freightAmountDetailBinding.getValue());
        BigDecimal packRes=new BigDecimal(0);
        DCIteratorBinding dciter1 =
            (DCIteratorBinding) ADFUtils.findIterator("StoreReceiptVoucherDetailsVO1Iterator");
        StoreReceiptVoucherDetailsVORowImpl row =
            (StoreReceiptVoucherDetailsVORowImpl) dciter1.getCurrentRow();
        BigDecimal pckamt=new BigDecimal(0);
        BigDecimal disc=new BigDecimal(0);
        disc=row.getDisc();
        pckamt=row.getPackAmt();
        System.out.println("packmat==>>"+pckamt+"disc==>"+disc);
        BigDecimal packingamt=(BigDecimal)pckamt==null  ? new BigDecimal(0) :(BigDecimal)pckamt;
//        System.out.println("pack amount=============="+packAmountBinding.getValue()+"freight maount===////////"+freightAmountDetailBinding.getValue());
      BigDecimal freightamt=row.getFreight();
        System.out.println("freight amount--==>"+freightamt);
        
        OperationBinding opr = ADFUtils.findOperation("getCalculatedDataForGst");
        opr.getParamsMap().put("new_amt", disc);
        opr.getParamsMap().put("flag", "M");
        opr.getParamsMap().put("pckAmt", packingamt);
        opr.getParamsMap().put("freightAmt", freightamt);
        opr.execute();
//        calculatePackAmt();
        System.out.println("After pack amount=============="+packAmountBinding.getValue()+"freight maount===////////"+freightAmountDetailBinding.getValue());
        ADFUtils.findOperation("getPuCalculation").execute();
        System.out.println(" EMP CD" + vce.getNewValue().toString());
        if ((ob.getResult() != null && ob.getResult().equals("Y"))) {
            Row row1 = (Row) ADFUtils.evaluateEL("#{bindings.StoreReceiptVoucherHeadVO1Iterator.currentRow}");
            row1.setAttribute("AuthBy", null);
            ADFUtils.showMessage("You have no authority to approve this Store Receipt Voucher.", 0);
        }
        ADFUtils.findOperation("MRNSetValuesOfDate").execute();
        if(authorisedByBinding.getValue()!=null ){
            ADFUtils.findOperation("setMaxBillDate").execute();
            ADFUtils.findOperation("setNoOfDaysFromPOInMRN").execute();
            ADFUtils.findOperation("setItemDetailDataInBillJVPurchaseBill").execute();
        DCIteratorBinding dciter =
            (DCIteratorBinding) ADFUtils.findIterator("StoreReceiptVoucherHeadVO1Iterator");
        StoreReceiptVoucherHeadVORowImpl srv_dtl_row =
            (StoreReceiptVoucherHeadVORowImpl) dciter.getCurrentRow();
        System.out.println("narraion==>"+srv_dtl_row.getAttribute("Narration"));
        if(srv_dtl_row.getAttribute("Narration")==null){
            ADFUtils.findOperation("narrationValueMrnBillPass").execute();
        }
        }
//        }
        }
//        
    }

    public void setCancelstatusBinding1(RichInputComboboxListOfValues cancelstatusBinding1) {
        this.cancelstatusBinding1 = cancelstatusBinding1;
    }

    public RichInputComboboxListOfValues getCancelstatusBinding1() {
        return cancelstatusBinding1;
    }

    public void setCancelStatusBinding(RichSelectOneChoice cancelStatusBinding) {
        this.cancelStatusBinding = cancelStatusBinding;
    }

    public RichSelectOneChoice getCancelStatusBinding() {
        return cancelStatusBinding;
    }

    public void setCurrRateBinding(RichInputText currRateBinding) {
        this.currRateBinding = currRateBinding;
    }

    public RichInputText getCurrRateBinding() {
        return currRateBinding;
    }

    public void setCurrValBinding(RichInputText currValBinding) {
        this.currValBinding = currValBinding;
    }

    public RichInputText getCurrValBinding() {
        return currValBinding;
    }

    public void setSrvAddNoBinding(RichInputText srvAddNoBinding) {
        this.srvAddNoBinding = srvAddNoBinding;
    }

    public RichInputText getSrvAddNoBinding() {
        return srvAddNoBinding;
    }

    public void setSgstRateBinding(RichInputComboboxListOfValues sgstRateBinding) {
        this.sgstRateBinding = sgstRateBinding;
    }

    public RichInputComboboxListOfValues getSgstRateBinding() {
        return sgstRateBinding;
    }

    public void setGrandCostBinding(RichInputText grandCostBinding) {
        this.grandCostBinding = grandCostBinding;
    }

    public RichInputText getGrandCostBinding() {
        return grandCostBinding;
    }

    public void recQtyNewValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
//            System.out.println("Start Validator " + object);
//            if (object != null) {
//                DCIteratorBinding dciter =
//                    (DCIteratorBinding) ADFUtils.findIterator("StoreReceiptVoucherDetailsVO1Iterator");
//
//                StoreReceiptVoucherDetailsVORowImpl srv_dtl_row =
//                    (StoreReceiptVoucherDetailsVORowImpl) dciter.getCurrentRow();
//                System.out.println("Row Count :-" + dciter.getRangeSize() + "  " + srv_dtl_row.getChallanQty());
//                if (srv_dtl_row != null && srv_dtl_row.getChallanQty() != null) {
//                    BigDecimal recpt_qty = new BigDecimal(object.toString());
//                    BigDecimal Challan_qty = (BigDecimal) srv_dtl_row.getChallanQty();
//                    res = recpt_qty.compareTo(Challan_qty);
//                    System.out.println("Res Value " + res + "  " + count);
//                    if (res == 0) {
//                        System.out.println("Sop1");
//                        reqDataDetails();
//                    } else if (res == 1) {
//                                                System.out.println("Sop2");
//                                                FacesMessage message =
//                                                    new FacesMessage("Receipt Quantity can  not  greater than Challan Quantity!!");
//                                                message.setSeverity(FacesMessage.SEVERITY_ERROR);
//                                                FacesContext context = FacesContext.getCurrentInstance();
//                                                context.addMessage(bingRecptQty.getClientId(), message);
//
//
//
//                    }
////                    else if(recpt_qty.equals(new BigDecimal(0))){
////                        FacesMessage message =
////                            new FacesMessage("Receipt Quantity must be greater than 0");
////                        message.setSeverity(FacesMessage.SEVERITY_ERROR);
////                        FacesContext context = FacesContext.getCurrentInstance();
////                        context.addMessage(bingRecptQty.getClientId(), message);
////                    }
//
////                    } else if (res == -1 && count == 1) {
////                        System.out.println("Sop3");
//////                        RichPopup.PopupHints hints = new RichPopup.PopupHints();
//////                        getBindWrngPopUp().show(hints);
////                    }
//                }
//            }
            System.out.println("Object :"+object+" Challan Qty : "+challanQtyBinding.getValue());
            if(object!=null && challanQtyBinding.getValue()!=null){
            BigDecimal v1=(BigDecimal)challanQtyBinding.getValue();
            BigDecimal v2=(BigDecimal)object;
//            if(v2.compareTo(v1)==1 ){
//                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,"Receipt Quantity can  not  greater than Challan Quantity.", null));
//                }
//            else{
//                    System.out.println("Inside Else!!");
//                    reqDataDetails();
//                }
            if(v2.compareTo(new BigDecimal(0))==-1){
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,"Receipt Quantity must be greater than or equals to 0.", null));
            }
                else{
                        System.out.println("Inside Else!!");
                        reqDataDetails();
                    }
            }

    }

    public void setNetRateBinding(RichInputText netRateBinding) {
        this.netRateBinding = netRateBinding;
    }

    public RichInputText getNetRateBinding() {
        return netRateBinding;
    }

    public void setApprovedDateBinding(RichInputDate approvedDateBinding) {
        this.approvedDateBinding = approvedDateBinding;
    }

    public RichInputDate getApprovedDateBinding() {
        return approvedDateBinding;
    }

    public void setCountedDateBinding(RichInputDate countedDateBinding) {
        this.countedDateBinding = countedDateBinding;
    }

    public RichInputDate getCountedDateBinding() {
        return countedDateBinding;
    }

    public void setCountedByBinding(RichInputComboboxListOfValues countedByBinding) {
        this.countedByBinding = countedByBinding;
    }

    public RichInputComboboxListOfValues getCountedByBinding() {
        return countedByBinding;
    }

    public void setOtherAmtBinding(RichInputText otherAmtBinding) {
        this.otherAmtBinding = otherAmtBinding;
    }

    public RichInputText getOtherAmtBinding() {
        return otherAmtBinding;
    }

    public void setIndentByTransBinding(RichInputText indentByTransBinding) {
        this.indentByTransBinding = indentByTransBinding;
    }

    public RichInputText getIndentByTransBinding() {
        return indentByTransBinding;
    }

    public void setManfBinding(RichInputText manfBinding) {
        this.manfBinding = manfBinding;
    }

    public RichInputText getManfBinding() {
        return manfBinding;
    }

    public void currRateVCE(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        BigDecimal rcQty = (BigDecimal) (acceptedQtyBinding.getValue()==null ? bingRecptQty.getValue():acceptedQtyBinding.getValue());
        if(valueChangeEvent!=null && rcQty!=null && currRateBinding.getValue()!=null){
            
            BigDecimal curRate = (BigDecimal) currRateBinding.getValue();
            System.out.println("cur rate==>>"+curRate+"rec qty==>>"+rcQty);
            BigDecimal multiply=rcQty.multiply(curRate);
            System.out.println(" multiplication is==>>"+multiply);
            currValBinding.setValue(multiply);
            OperationBinding opr = ADFUtils.findOperation("getCalculatedDataForGst");
            opr.getParamsMap().put("new_amt", discBinding.getValue());
            opr.getParamsMap().put("flag", "M");
            opr.getParamsMap().put("pckAmt", packAmountBinding.getValue()==null? new BigDecimal(0) :packAmountBinding.getValue());
            opr.getParamsMap().put("freightAmt", freightAmountDetailBinding.getValue()==null? new BigDecimal(0) :freightAmountDetailBinding.getValue());
            opr.execute();

        }
    }

    public void setPackAmountBinding(RichInputText packAmountBinding) {
        this.packAmountBinding = packAmountBinding;
    }

    public RichInputText getPackAmountBinding() {
        return packAmountBinding;
    }

    public void packForwardVCE(ValueChangeEvent vce) {
        System.out.println("cjkdfjhgd");
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce != null) {
            System.out.println("kdfjkdkg");
           
                    BigDecimal pckamt=new BigDecimal(0);
                    BigDecimal pckfor = (BigDecimal) packingForwardBinding.getValue()==null ? new BigDecimal(0) :(BigDecimal) packingForwardBinding.getValue();
                    BigDecimal rcQty = (BigDecimal) (acceptedQtyBinding.getValue()==null ? bingRecptQty.getValue():acceptedQtyBinding.getValue());
            BigDecimal discAmt = (BigDecimal) discBinding.getValue() ==null ? new BigDecimal(0) : (BigDecimal) discBinding.getValue();
                    System.out.println("pack for==>>"+pckfor+"rec qty==>>"+rcQty);
                    pckamt=discAmt.multiply(pckfor.multiply(new BigDecimal(0.01)));
                    System.out.println("packmat==>>"+pckamt);
                   BigDecimal packingamt=(BigDecimal)pckamt==null  ? new BigDecimal(0) :(BigDecimal)pckamt;
           System.out.println("packingamt&&&&&&&&&&&&&&&&&&"+packingamt);
//            packAmountBinding.setValue(packingamt);
            OperationBinding opr = ADFUtils.findOperation("getCalculatedDataForGst");
            opr.getParamsMap().put("new_amt", discountBinding.getValue());
            opr.getParamsMap().put("flag", "M");
            opr.getParamsMap().put("pckAmt", packAmountBinding.getValue()==null? new BigDecimal(0) :packAmountBinding.getValue());
            opr.getParamsMap().put("freightAmt", freightAmountDetailBinding.getValue()==null? new BigDecimal(0) :freightAmountDetailBinding.getValue());
            opr.execute();
            if(authorisedByBinding.getValue()!=null ){
                ADFUtils.findOperation("setMaxBillDate").execute();
                ADFUtils.findOperation("setNoOfDaysFromPOInMRN").execute();
                ADFUtils.findOperation("setItemDetailDataInBillJVPurchaseBill").execute();
            DCIteratorBinding dciter =
                (DCIteratorBinding) ADFUtils.findIterator("StoreReceiptVoucherHeadVO1Iterator");
            StoreReceiptVoucherHeadVORowImpl srv_dtl_row =
                (StoreReceiptVoucherHeadVORowImpl) dciter.getCurrentRow();
            System.out.println("narraion==>"+srv_dtl_row.getAttribute("Narration"));
            if(srv_dtl_row.getAttribute("Narration")==null){
                ADFUtils.findOperation("narrationValueMrnBillPass").execute();
            }
            }  
            
//        BigDecimal pckamt=new BigDecimal(0);
//        BigDecimal pckfor = (BigDecimal) packingForwardBinding.getValue()==null ? new BigDecimal(0) :(BigDecimal) packingForwardBinding.getValue();
//        BigDecimal rcQty = (BigDecimal) bingRecptQty.getValue() ==null ? new BigDecimal(0) : (BigDecimal) bingRecptQty.getValue();
//        System.out.println("pack for==>>"+pckfor+"rec qty==>>"+rcQty);
//        pckamt=rcQty.multiply(pckfor);
//        System.out.println("packmat==>>"+pckamt);
//        packAmountBinding.setValue(pckamt);
           
        } 
    }

    public void otherRsVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce.getNewValue()!=null ){
           BigDecimal igst =
               (BigDecimal) (igstAmountBinding.getValue() == null ? new BigDecimal(0) : igstAmountBinding.getValue());
           BigDecimal cgst =
               (BigDecimal) (cgstAmountBinding.getValue() == null ? new BigDecimal(0) : cgstAmountBinding.getValue());
           BigDecimal sgst =
               (BigDecimal) (sgstAmountBinding.getValue() == null ? new BigDecimal(0) : sgstAmountBinding.getValue());
           BigDecimal landedCost=new BigDecimal(0);
            BigDecimal otherUnit =
                (BigDecimal) (otherUnitBinding.getValue() == null ? new BigDecimal(0) : otherUnitBinding.getValue());
        BigDecimal grandCost = (BigDecimal) grandCostBinding.getValue();
            System.out.println("Grsnd cost is==>"+grandCost+"other unit==>"+otherUnit);
            BigDecimal othGrandCost=otherUnit.add(grandCost);
            System.out.println("Grandcost after other==>>"+othGrandCost);
            grandCostBinding.setValue(othGrandCost);
           landedCost=grandCost.subtract(igst.add(cgst.add(sgst)));
           System.out.println("Landed cost is==>"+landedCost);
           landedCostBinding.setValue(landedCost);
           BigDecimal netRate=new BigDecimal(0);
           BigDecimal recQty= 
           (BigDecimal) (acceptedQtyBinding.getValue()==null ? bingRecptQty.getValue():acceptedQtyBinding.getValue());
           try{
            netRate=landedCost.divide(recQty);
           }
           catch(ArithmeticException ae){
               System.out.println("Division Undefined!!");
           }
           System.out.println("net rate==>>"+netRate);
           netRateBinding.setValue(netRate);
           netConvRateBinding.setValue(netRate);
       }
        if(authorisedByBinding.getValue()!=null ){
        ADFUtils.findOperation("setMaxBillDate").execute();
        ADFUtils.findOperation("setNoOfDaysFromPOInMRN").execute();
        ADFUtils.findOperation("setItemDetailDataInBillJVPurchaseBill").execute();
        DCIteratorBinding dciter =
        (DCIteratorBinding) ADFUtils.findIterator("StoreReceiptVoucherHeadVO1Iterator");
        StoreReceiptVoucherHeadVORowImpl srv_dtl_row =
        (StoreReceiptVoucherHeadVORowImpl) dciter.getCurrentRow();
        System.out.println("narraion==>"+srv_dtl_row.getAttribute("Narration"));
        if(srv_dtl_row.getAttribute("Narration")==null){
        ADFUtils.findOperation("narrationValueMrnBillPass").execute();
        }
        }
    }

    public void setQcdNoBinding(RichInputText qcdNoBinding) {
        this.qcdNoBinding = qcdNoBinding;
    }

    public RichInputText getQcdNoBinding() {
        return qcdNoBinding;
    }

    public void setQcdDateBinding(RichInputDate qcdDateBinding) {
        this.qcdDateBinding = qcdDateBinding;
    }

    public RichInputDate getQcdDateBinding() {
        return qcdDateBinding;
    }

    public void uploadAction(ValueChangeEvent vce) {
        if (vce.getNewValue() != null) {
                    //Get File Object from VC Event
                    UploadedFile fileVal = (UploadedFile) vce.getNewValue();
                    //Upload File to path- Return actual server path
                    String path = ADFUtils.uploadFile(fileVal);
                      System.out.println(fileVal.getContentType());
                      System.out.println("Content Type==>>"+fileVal.getContentType()+"Path==>>"+path+"File name==>>"+fileVal.getFilename());
                    //Method to insert data in table to keep track of uploaded files
                    OperationBinding ob = ADFUtils.findOperation("setFileDataMRN");
                    ob.getParamsMap().put("name", fileVal.getFilename());
                    ob.getParamsMap().put("path", path);
                    ob.getParamsMap().put("contTyp", fileVal.getContentType());
                    ob.getParamsMap().put("SrNo", ADFUtils.evaluateEL("#{bindings.DocAttachRefDtlVO8Iterator.estimatedRowCount+1}"));
                    ob.execute();
                    // Reset inputFile component after upload
                    ResetUtils.reset(vce.getComponent());
                }
    }

    public void setDocNoBinding(RichInputText docNoBinding) {
        this.docNoBinding = docNoBinding;
    }

    public RichInputText getDocNoBinding() {
        return docNoBinding;
    }

    public void setUnitCdBinding(RichInputText unitCdBinding) {
        this.unitCdBinding = unitCdBinding;
    }

    public RichInputText getUnitCdBinding() {
        return unitCdBinding;
    }

    public void setRefDocBinding(RichInputText refDocBinding) {
        this.refDocBinding = refDocBinding;
    }

    public RichInputText getRefDocBinding() {
        return refDocBinding;
    }

    public void setRefDocTypeBinding(RichInputText refDocTypeBinding) {
        this.refDocTypeBinding = refDocTypeBinding;
    }

    public RichInputText getRefDocTypeBinding() {
        return refDocTypeBinding;
    }

    public void setFileNameBinding(RichInputText fileNameBinding) {
        this.fileNameBinding = fileNameBinding;
    }

    public RichInputText getFileNameBinding() {
        return fileNameBinding;
    }

    public void setDocdateBinding(RichInputDate docdateBinding) {
        this.docdateBinding = docdateBinding;
    }

    public RichInputDate getDocdateBinding() {
        return docdateBinding;
    }

    public void setConTypeBinding(RichInputText conTypeBinding) {
        this.conTypeBinding = conTypeBinding;
    }

    public RichInputText getConTypeBinding() {
        return conTypeBinding;
    }

    public void setPathBinding(RichInputText pathBinding) {
        this.pathBinding = pathBinding;
    }

    public RichInputText getPathBinding() {
        return pathBinding;
    }

    public void downloadAction(FacesContext facesContext, OutputStream outputStream) throws Exception{
        
        File filed = new File(pathBinding.getValue().toString());
                               FileInputStream fis;
                               byte[] b;
                               try {
                                   fis = new FileInputStream(filed);

                                   int n;
                                   while ((n = fis.available()) > 0) {
                                       b = new byte[n];
                                       int result = fis.read(b);
                                       outputStream.write(b, 0, b.length);
                                       if (result == -1)
                                           break;
                                   }
                               } catch (Exception e) {
                                   e.printStackTrace();
                               }
                               outputStream.flush();

    }

    public void deletePopupDocDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete4").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(docAttachTableBinding);
    }

    public void setDocAttachTableBinding(RichTable docAttachTableBinding) {
        this.docAttachTableBinding = docAttachTableBinding;
    }

    public RichTable getDocAttachTableBinding() {
        return docAttachTableBinding;
    }

    public void freightUnitVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce!=null){
            OperationBinding opr = ADFUtils.findOperation("getCalculatedDataForGst");
            opr.getParamsMap().put("new_amt", discountBinding.getValue());
            opr.getParamsMap().put("flag", "M");
            opr.getParamsMap().put("pckAmt", packAmountBinding.getValue()==null? new BigDecimal(0) :packAmountBinding.getValue());
            opr.getParamsMap().put("freightAmt", freightAmountDetailBinding.getValue()==null? new BigDecimal(0) :freightAmountDetailBinding.getValue());
            opr.execute();
            if(authorisedByBinding.getValue()!=null ){
                ADFUtils.findOperation("setMaxBillDate").execute();
                ADFUtils.findOperation("setNoOfDaysFromPOInMRN").execute();
                ADFUtils.findOperation("setItemDetailDataInBillJVPurchaseBill").execute();
            DCIteratorBinding dciter =
                (DCIteratorBinding) ADFUtils.findIterator("StoreReceiptVoucherHeadVO1Iterator");
            StoreReceiptVoucherHeadVORowImpl srv_dtl_row =
                (StoreReceiptVoucherHeadVORowImpl) dciter.getCurrentRow();
            System.out.println("narraion==>"+srv_dtl_row.getAttribute("Narration"));
            if(srv_dtl_row.getAttribute("Narration")==null){
                ADFUtils.findOperation("narrationValueMrnBillPass").execute();
            }
            }  
        }
    }

    public void setFreightUnitBinding(RichInputText freightUnitBinding) {
        this.freightUnitBinding = freightUnitBinding;
    }

    public RichInputText getFreightUnitBinding() {
        return freightUnitBinding;
    }

    public void setNoDaysBinding(RichInputText noDaysBinding) {
        this.noDaysBinding = noDaysBinding;
    }

    public RichInputText getNoDaysBinding() {
        return noDaysBinding;
    }

    public void setDueDateBinding(RichInputDate dueDateBinding) {
        this.dueDateBinding = dueDateBinding;
    }

    public RichInputDate getDueDateBinding() {
        return dueDateBinding;
    }

    public void setAgainstTypeBinding(RichSelectOneRadio againstTypeBinding) {
        this.againstTypeBinding = againstTypeBinding;
    }

    public RichSelectOneRadio getAgainstTypeBinding() {
        return againstTypeBinding;
    }

    public void setBillPassbinding(RichInputDate billPassbinding) {
        this.billPassbinding = billPassbinding;
    }

    public RichInputDate getBillPassbinding() {
        return billPassbinding;
    }

    public void setBillAuthByBinding(RichInputComboboxListOfValues billAuthByBinding) {
        this.billAuthByBinding = billAuthByBinding;
    }

    public RichInputComboboxListOfValues getBillAuthByBinding() {
        return billAuthByBinding;
    }

    public void setBillPassTabBinding(RichShowDetailItem billPassTabBinding) {
        this.billPassTabBinding = billPassTabBinding;
    }

    public RichShowDetailItem getBillPassTabBinding() {
        return billPassTabBinding;
    }

    public void billAuthByVCL(ValueChangeEvent vce) {
            vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
            OperationBinding ob = ADFUtils.findOperation("checkApprovalStatus");
            ob.getParamsMap().put("formNm", "BSRV");
            ob.getParamsMap().put("authoLim", "AP");
            ob.getParamsMap().put("empcd", vce.getNewValue().toString());
            ob.execute();
            System.out.println(" EMP CD" + vce.getNewValue().toString());
            if ((ob.getResult() != null && ob.getResult().equals("Y"))) {
                Row row = (Row) ADFUtils.evaluateEL("#{bindings.StoreReceiptVoucherHeadVO1Iterator.currentRow}");
                row.setAttribute("BillAppBy", null);
                row.setAttribute("BillAppDate", null);
                
                ADFUtils.showMessage("You have no authority to approve this Bill", 0);
            }
        }

    public void setBillAppDateBinding(RichInputDate billAppDateBinding) {
        this.billAppDateBinding = billAppDateBinding;
    }

    public RichInputDate getBillAppDateBinding() {
        return billAppDateBinding;
    }

    public void setVouNoBinding(RichInputText vouNoBinding) {
        this.vouNoBinding = vouNoBinding;
    }

    public RichInputText getVouNoBinding() {
        return vouNoBinding;
    }

    public void setBillPrepByBinding(RichInputComboboxListOfValues billPrepByBinding) {
        this.billPrepByBinding = billPrepByBinding;
    }

    public RichInputComboboxListOfValues getBillPrepByBinding() {
        return billPrepByBinding;
    }

    public void setSubCodeBinding(RichInputText subCodeBinding) {
        this.subCodeBinding = subCodeBinding;
    }

    public RichInputText getSubCodeBinding() {
        return subCodeBinding;
    }

    public void gstAmountVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(authorisedByBinding.getValue()!=null)
        ADFUtils.findOperation("setItemDetailDataInBillJVPurchaseBill").execute();

       
    }

    public void setCustomBinding(RichInputText customBinding) {
        this.customBinding = customBinding;
    }

    public RichInputText getCustomBinding() {
        return customBinding;
    }

    public void setOther1Binding(RichInputText other1Binding) {
        this.other1Binding = other1Binding;
    }

    public RichInputText getOther1Binding() {
        return other1Binding;
    }

    public void setOther2Binding(RichInputText other2Binding) {
        this.other2Binding = other2Binding;
    }

    public RichInputText getOther2Binding() {
        return other2Binding;
    }

    public void setMrnTabBinding(RichShowDetailItem mrnTabBinding) {
        this.mrnTabBinding = mrnTabBinding;
    }

    public RichShowDetailItem getMrnTabBinding() {
        return mrnTabBinding;
    }

    public void customDutyVCL(ValueChangeEvent valueChangeEvent) {
//        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
//        if(valueChangeEvent!=null){
//        System.out.println("(Long)ADFUtils.evaluateEL(\"#{bindings.StoreReceiptVoucherBillPassingVO1Iterator.estimatedRowCount}\")"+(Long)ADFUtils.evaluateEL("#{bindings.StoreReceiptVoucherBillPassingVO1Iterator.estimatedRowCount}"));
////        if((Long)ADFUtils.evaluateEL("#{bindings.StoreReceiptVoucherBillPassingVO1Iterator.estimatedRowCount}")==0){
//        if(authorisedByBinding.getValue()!=null ){
//        ADFUtils.findOperation("setMaxBillDate").execute();
//        ADFUtils.findOperation("setNoOfDaysFromPOInMRN").execute();
//        ADFUtils.findOperation("setItemDetailDataInBillJVPurchaseBill").execute();
//        DCIteratorBinding dciter1 =
//        (DCIteratorBinding) ADFUtils.findIterator("StoreReceiptVoucherHeadVO1Iterator");
//        StoreReceiptVoucherHeadVORowImpl srv_dtl_row =
//        (StoreReceiptVoucherHeadVORowImpl) dciter1.getCurrentRow();
//        System.out.println("narraion==>"+srv_dtl_row.getAttribute("Narration"));
//        if(srv_dtl_row.getAttribute("Narration")==null){
//        ADFUtils.findOperation("narrationValueMrnBillPass").execute();
//        }
//        }
//    }
////        else{
////            
////        }
////        }
    }

    public void setOtherUnitBinding(RichInputText otherUnitBinding) {
        this.otherUnitBinding = otherUnitBinding;
    }

    public RichInputText getOtherUnitBinding() {
        return otherUnitBinding;
    }

    public void otherUnitRsVCE(ValueChangeEvent valueChangeEvent) {
      
    }

    public void other1VCE(ValueChangeEvent valueChangeEvent) {
       
    }

    public void other2VCE(ValueChangeEvent valueChangeEvent) {
       
    }
    
    public void setShortQty(){
        System.out.println("dhdfhdvhjdvdvf");
        DCIteratorBinding dciter =
            (DCIteratorBinding) ADFUtils.findIterator("StoreReceiptVoucherDetailsVO1Iterator");
        StoreReceiptVoucherDetailsVORowImpl row =
            (StoreReceiptVoucherDetailsVORowImpl) dciter.getCurrentRow();
        if(row.getChallanQty()!=null && row.getAcceptQty()!=null){
            System.out.println("inside ifff===");
            BigDecimal chlnQty = (BigDecimal) row.getChallanQty();
            BigDecimal acptQty = (BigDecimal) row.getAcceptQty();
            
            BigDecimal shortQty=chlnQty.subtract(acptQty);
            System.out.println("shortQty==>>"+shortQty);
            row.setBillShortQty(shortQty);
            
            if(row.getBillShortQty()!=null && row.getBillAllowQty()!=null){
                 BigDecimal allow=new BigDecimal(0);
                 allow = (BigDecimal) row.getBillAllowQty();
                System.out.println("allow--==>"+allow+"short==="+shortQty);
                BigDecimal dedQty=shortQty.subtract(allow);
                System.out.println("deduction qty==>>"+dedQty);
                row.setBillDedQty(dedQty);
            }
            }
        
    }

    public void setShortQtyBinding(RichInputText shortQtyBinding) {
        this.shortQtyBinding = shortQtyBinding;
    }

    public RichInputText getShortQtyBinding() {
        return shortQtyBinding;
    }

    public void setDedQtyBinding(RichInputText dedQtyBinding) {
        this.dedQtyBinding = dedQtyBinding;
    }

    public RichInputText getDedQtyBinding() {
        return dedQtyBinding;
    }

    public void setAllowQtyBinding(RichInputText allowQtyBinding) {
        this.allowQtyBinding = allowQtyBinding;
    }

    public RichInputText getAllowQtyBinding() {
        return allowQtyBinding;
    }

    public void allowQtyVCL(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        System.out.println("method call");
        setShortQty();
        System.out.println("method call after");
        if(authorisedByBinding.getValue()!=null ){
        ADFUtils.findOperation("setMaxBillDate").execute();
        ADFUtils.findOperation("setNoOfDaysFromPOInMRN").execute();
        ADFUtils.findOperation("setItemDetailDataInBillJVPurchaseBill").execute();
        DCIteratorBinding dciter =
        (DCIteratorBinding) ADFUtils.findIterator("StoreReceiptVoucherHeadVO1Iterator");
        StoreReceiptVoucherHeadVORowImpl srv_dtl_row =
        (StoreReceiptVoucherHeadVORowImpl) dciter.getCurrentRow();
        System.out.println("narraion==>"+srv_dtl_row.getAttribute("Narration"));
        if(srv_dtl_row.getAttribute("Narration")==null){
        ADFUtils.findOperation("narrationValueMrnBillPass").execute();
        }
        }
    }

    public void setDetailTableBinding(RichTable detailTableBinding) {
        this.detailTableBinding = detailTableBinding;
    }

    public RichTable getDetailTableBinding() {
        return detailTableBinding;
    }

    public void impIgstVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());

    }

    public void setImpIgstBinding(RichInputText impIgstBinding) {
        this.impIgstBinding = impIgstBinding;
    }

    public RichInputText getImpIgstBinding() {
        return impIgstBinding;
    }
    
    public void countedByVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        ADFUtils.findOperation("MRNSetValuesOfDate").execute();
    }
    
    public void setBillPassingTabBinding(RichShowDetailItem billPassingTabBinding) {
        this.billPassingTabBinding = billPassingTabBinding;
    }

    public RichShowDetailItem getBillPassingTabBinding() {
        return billPassingTabBinding;
    }

    public void deleteDialogBillPassDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete5").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(billPassTableBinding);
    }

    public void setBillPassTableBinding(RichTable billPassTableBinding) {
        this.billPassTableBinding = billPassTableBinding;
    }

    public RichTable getBillPassTableBinding() {
        return billPassTableBinding;
    }

    public void billPassingListener(DisclosureEvent disclosureEvent) {
        System.out.println("(Long)ADFUtils.evaluateEL(\"#{bindings.StoreReceiptVoucherBillPassingVO1Iterator.estimatedRowCount}\")"+(Long)ADFUtils.evaluateEL("#{bindings.StoreReceiptVoucherBillPassingVO1Iterator.estimatedRowCount}"));
        if((Long)ADFUtils.evaluateEL("#{bindings.StoreReceiptVoucherBillPassingVO1Iterator.estimatedRowCount}")==0){
      if(authorisedByBinding.getValue()!=null ){
          
          
//      if(billPrepByBinding.getValue()==null)
//      {
      ADFUtils.findOperation("setMaxBillDate").execute();
      ADFUtils.findOperation("setNoOfDaysFromPOInMRN").execute();
      ADFUtils.findOperation("setItemDetailDataInBillJVPurchaseBill").execute();
      DCIteratorBinding dciter1 =
      (DCIteratorBinding) ADFUtils.findIterator("StoreReceiptVoucherHeadVO1Iterator");
      StoreReceiptVoucherHeadVORowImpl srv_dtl_row =
      (StoreReceiptVoucherHeadVORowImpl) dciter1.getCurrentRow();
      System.out.println("narraion==>"+srv_dtl_row.getAttribute("Narration"));
      if(srv_dtl_row.getAttribute("Narration")==null){
      ADFUtils.findOperation("narrationValueMrnBillPass").execute();
      }
//      billPrepByBinding.setValue(ADFUtils.evaluateEL("#{pageFlowScope.empCode}"));
//
//      }
      
      
      }

            System.out.println("Auth by Value==>>"+authorisedByBinding.getValue());
            String billPrep = (String) ADFUtils.resolveExpression("#{pageFlowScope.empCode}");
            System.out.println("prep by bill==>"+ADFUtils.resolveExpression("#{pageFlowScope.empCode}"));
  }
        else
        {
        if(billPrepByBinding.getValue()==null)
        {
        ADFUtils.findOperation("updateBillPvValuesInSrVBillSRV").execute();
        }
        }
        System.out.println("Emp Code Value: "+ADFUtils.evaluateEL("#{pageFlowScope.empCode}"));
        if(billPrepByBinding.getValue()==null)
        {
        billPrepByBinding.setValue(ADFUtils.evaluateEL("#{pageFlowScope.empCode}"));
        }
        try {
            ADFUtils.findOperation("setMaxBillDate").execute();
        } catch (Exception e) {
            // TODO: Add catch code
            e.printStackTrace();
        }
    }

    public void setDiffAmtBinding(RichInputText diffAmtBinding) {
        this.diffAmtBinding = diffAmtBinding;
    }

    public RichInputText getDiffAmtBinding() {
        return diffAmtBinding;
    }

    public void againstTypeVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
                    if(vce!=null)
                    {
                        String agType = (String) againstTypeBinding.getValue();
            System.out.println("Against type is==>>"+agType);
            BigDecimal BgDec=(BigDecimal)( noDaysBinding.getValue()==null ?new BigDecimal(0):noDaysBinding.getValue());
                        //System.out.println("Value of Before dayBinding ====>" + dayBinding.getValue());
                        int days=BgDec.intValue();
                        //System.out.println("Value of After dayBinding ====>" + dayBinding.getValue());
                        Timestamp srvDate=(Timestamp)bindSrvDate.getValue();
                       // Timestamp billPassDt = (Timestamp) billPassbinding.getValue();
                        
                       Timestamp billPassDt = (Timestamp) bindInvoiceDate.getValue();
                        
                        System.out.println("srv date--=>"+srvDate+"\n======INVOICE DATE==>>"+billPassDt);
                        if(agType.equalsIgnoreCase("MRN") && bindSrvDate.getValue()!=null){
                        Calendar cal=Calendar.getInstance();
                        cal.setTimeInMillis(srvDate.getTime());
                        cal.add(Calendar.DAY_OF_MONTH,days);
                        Timestamp dueDt=new Timestamp(cal.getTime().getTime());
                        System.out.println("TimesStamp########==>in mrn "+dueDt);
                        dueDateBinding.setValue(dueDt); 
                        }
                        else if(agType.equalsIgnoreCase("BILL") && billPassbinding.getValue()!=null){
                            System.out.println("if ag type is bill==");
                            Calendar cal=Calendar.getInstance();
                            cal.setTimeInMillis(billPassDt.getTime());
                            cal.add(Calendar.DAY_OF_MONTH,days);
                            Timestamp dueDt=new Timestamp(cal.getTime().getTime());
                            System.out.println("TimesStamp########==>in bill "+dueDt);
                            dueDateBinding.setValue(dueDt);  
                        }
                        //System.out.println("After VCE 
    }            
                
}

    public void checkedByVCL(ValueChangeEvent vce) {
       
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        ADFUtils.findOperation("MRNSetValuesOfDate").execute();
        //        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        //        if(vce.getNewValue()!=null){
                    String poNo=null;
                DCIteratorBinding dciter12 =
                    (DCIteratorBinding) ADFUtils.findIterator("StoreReceiptVoucherDetailsVO1Iterator");
                StoreReceiptVoucherDetailsVORowImpl row12 =
                    (StoreReceiptVoucherDetailsVORowImpl) dciter12.getCurrentRow();
                System.out.println("row12.getPoNo()==>>"+row12.getPoNo());
//                RowSetIterator rsi=dciter12.getViewObject().createRowSetIterator(null);
//                while(rsi.hasNext()){
//                    Row r=rsi.next();
//                     poNo = (String) r.getAttribute("PoNo");
//                }
//                if(poNo==null){
//                  countedByBinding.setValue(null);
//                    AdfFacesContext.getCurrentInstance().addPartialTarget(countedByBinding);
//                    ADFUtils.showMessage("As this is PO based MRN,You cannot checked it.", 0);
//                    
//                }
                
            
                //else{
                vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
                OperationBinding ob = ADFUtils.findOperation("checkApprovalStatus");
                ob.getParamsMap().put("formNm", "SRV");
                ob.getParamsMap().put("authoLim", "CH");
                ob.getParamsMap().put("empcd", vce.getNewValue().toString());
                ob.execute();
                System.out.println("Before pack amount=============="+packAmountBinding.getValue()+"Before freight maount===////////"+freightAmountDetailBinding.getValue());
                BigDecimal packRes=new BigDecimal(0);
                DCIteratorBinding dciter1 =
                    (DCIteratorBinding) ADFUtils.findIterator("StoreReceiptVoucherDetailsVO1Iterator");
                StoreReceiptVoucherDetailsVORowImpl row =
                    (StoreReceiptVoucherDetailsVORowImpl) dciter1.getCurrentRow();
                BigDecimal pckamt=new BigDecimal(0);
                BigDecimal disc=new BigDecimal(0);
                disc=row.getDisc();
                pckamt=row.getPackAmt();
                System.out.println("packmat==>>"+pckamt+"disc==>"+disc);
                BigDecimal packingamt=(BigDecimal)pckamt==null  ? new BigDecimal(0) :(BigDecimal)pckamt;
        //        System.out.println("pack amount=============="+packAmountBinding.getValue()+"freight maount===////////"+freightAmountDetailBinding.getValue());
              BigDecimal freightamt=row.getFreight();
                System.out.println("freight amount--==>"+freightamt);
                
                OperationBinding opr = ADFUtils.findOperation("getCalculatedDataForGst");
                opr.getParamsMap().put("new_amt", disc);
                opr.getParamsMap().put("flag", "M");
                opr.getParamsMap().put("pckAmt", packingamt);
                opr.getParamsMap().put("freightAmt", freightamt);
                opr.execute();
        //        calculatePackAmt();
                System.out.println("After pack amount=============="+packAmountBinding.getValue()+"freight maount===////////"+freightAmountDetailBinding.getValue());
                ADFUtils.findOperation("getPuCalculation").execute();
                System.out.println(" EMP CD" + vce.getNewValue().toString());
                if ((ob.getResult() != null && ob.getResult().equals("Y"))) {
                    Row row1 = (Row) ADFUtils.evaluateEL("#{bindings.StoreReceiptVoucherHeadVO1Iterator.currentRow}");
                    row1.setAttribute("CountedBy", null);
                    ADFUtils.showMessage("You have no authority to checked this Store Receipt Voucher.", 0);
                }
                ADFUtils.findOperation("MRNSetValuesOfDate").execute();
                if(countedByBinding.getValue()!=null ){
                    ADFUtils.findOperation("setMaxBillDate").execute();
                    ADFUtils.findOperation("setNoOfDaysFromPOInMRN").execute();
                    ADFUtils.findOperation("setItemDetailDataInBillJVPurchaseBill").execute();
                DCIteratorBinding dciter =
                    (DCIteratorBinding) ADFUtils.findIterator("StoreReceiptVoucherHeadVO1Iterator");
                StoreReceiptVoucherHeadVORowImpl srv_dtl_row =
                    (StoreReceiptVoucherHeadVORowImpl) dciter.getCurrentRow();
                System.out.println("narraion==>"+srv_dtl_row.getAttribute("Narration"));
                if(srv_dtl_row.getAttribute("Narration")==null){
                    ADFUtils.findOperation("narrationValueMrnBillPass").execute();
                }
                }
        //        }
              //  }
        //
    }


    public void setInvoiceValueBinding(RichInputText invoiceValueBinding) {
        this.invoiceValueBinding = invoiceValueBinding;
    }

    public RichInputText getInvoiceValueBinding() {
        return invoiceValueBinding;
    }

    public void setGstBinding(RichInputComboboxListOfValues gstBinding) {
        this.gstBinding = gstBinding;
    }

    public RichInputComboboxListOfValues getGstBinding() {
        return gstBinding;
    }

    public void setLinkBinding(RichCommandLink linkBinding) {
        this.linkBinding = linkBinding;
    }

    public RichCommandLink getLinkBinding() {
        return linkBinding;
    }

    public void setIndentNoTransBinding(RichInputText indentNoTransBinding) {
        this.indentNoTransBinding = indentNoTransBinding;
    }

    public RichInputText getIndentNoTransBinding() {
        return indentNoTransBinding;
    }

    public void setProcessNameBinding(RichInputText processNameBinding) {
        this.processNameBinding = processNameBinding;
    }

    public RichInputText getProcessNameBinding() {
        return processNameBinding;
    }

    public void setSrvdatenepalbinding(RichInputText srvdatenepalbinding) {
        this.srvdatenepalbinding = srvdatenepalbinding;
    }

    public RichInputText getSrvdatenepalbinding() {
        return srvdatenepalbinding;
    }
}
