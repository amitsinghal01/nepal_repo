package terms.mtl.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import javax.faces.validator.ValidatorException;

import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;


public class SupplementarySrvBean {
    private RichTable createTableSuppSrvBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText invoiceNoBinding;
    private RichInputComboboxListOfValues parentSrvBinding;
    private RichInputDate parentSrvDateBinding;
    private RichInputText supplementSRVBinding;
    private RichInputDate srvDateBinding;
    private RichInputDate countedDtBinding;
    private RichInputText vendorCodeBinding;
    private RichInputText locationBinding;
    private RichInputText suppliedByBInding;
    private RichInputText shortExcessNoBinding;
    private RichInputText challanNoBInding;
    private RichInputDate challanDataBinding;
    private RichInputText deviationRequestNoBinding;
    private RichInputText cityCodeBinding;
    private RichInputText numberOfCasesBinding;
    private RichInputText plantCodeBinding;
    private RichButton headerEditBinding;
    private RichButton detailCreateBinding;
    private RichInputText poNoBinding;
    private RichInputDate poDateBinding;
    private RichInputText revisionNoBinding;
    private RichInputText processBinding;
    private RichInputText challanQuantityBinding;
    private RichInputText receivedQuantityBinding;
    private RichInputText uomBinding;
    private RichInputText reOfferQtyBinding;
    private RichInputText netRateBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichOutputText outputText;
    private RichInputDate invoiceDateBinding;
    private RichInputText rrGrNoBinding;
    private RichInputDate dateBinding;
    private RichOutputText dtlLocationBinding;
    private RichInputText remarksBinding;
    private RichInputText getAmendNoBinding;
    private RichInputComboboxListOfValues preparedByBinding;
    private RichShowDetailItem tab2Binding;
    private RichShowDetailItem tab1Binding;


    public SupplementarySrvBean() {
    }

    public void itemCodeVCL(ValueChangeEvent valueChangeEvent) 
    {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        System.out.println("revisionNoBinding=>"+revisionNoBinding.getValue());
        System.out.println("processBinding=>"+processBinding.getValue());
        
        if(valueChangeEvent.getNewValue()!= null){
                System.out.println("in vcl");
                System.out.println("item value in vcl:"+valueChangeEvent.getNewValue());
            OperationBinding od=ADFUtils.findOperation("goReofferNetlocSupplementarySrv");
             od.getParamsMap().put("itemcd",valueChangeEvent.getNewValue());
            od.execute();
        }
    }

    public void saveAL(ActionEvent actionEvent)
    {
        String empcd=(String)ADFUtils.resolveExpression("#{pageFlowScope.empCode}") ; 
        System.out.println("empcd"+empcd);
        ADFUtils.setColumnValue("SupplementarySrvHeaderVO1Iterator", "LastUpdatedBy", empcd);
        
        OperationBinding op = ADFUtils.findOperation("getSupplementarySrvNumber");
        Object rst = op.execute();
        System.out.println("VALUE AFTER GETTING RESULT =================>   "+rst);
        
        OperationBinding op1 = ADFUtils.findOperation("setProcdProcSeqProcSeqRevNoSrvDetail");
        op1.execute();
        
        String param=resolvEl("#{pageFlowScope.mode=='E'}"); 
        System.out.println("Mode is ====> "+param);
        Integer i=0;
        i=(Integer)outputText.getValue();
        System.out.println("EDIT TRANS VALUE"+i);
        
        if (rst.toString() != null && rst.toString() != "") {
            if (op.getErrors().isEmpty()) {
                   ADFUtils.findOperation("Commit").execute();
                FacesMessage Message = new FacesMessage("Record Saved Successfully.Supplementary SRV No is "+rst+".");   
                Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                FacesContext fc = FacesContext.getCurrentInstance();   
                fc.addMessage(null, Message);  
//                ADFUtils.setEL("#{pageFlowScope.mode}", "V");
//                cevmodecheck();
//                AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
        
            
            }
        }
        
        if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ") {
            if (op.getErrors().isEmpty()) {
                ADFUtils.findOperation("Commit").execute();
                FacesMessage Message = new FacesMessage("Record Update Successfully.");   
                Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                FacesContext fc = FacesContext.getCurrentInstance();   
                fc.addMessage(null, Message);  
//                ADFUtils.setEL("#{pageFlowScope.mode}", "V");
//                cevmodecheck();
//                AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
//        
            }
            

        
        }
//           if(i.equals(0))
//               {
//                  FacesMessage Message = new FacesMessage("Record Save Successfully."); 
//                  Message.setSeverity(FacesMessage.SEVERITY_INFO); 
//                  FacesContext fc = FacesContext.getCurrentInstance(); 
//                  fc.addMessage(null, Message);    
//                 
//               }
//               else
//               {
//                   FacesMessage Message = new FacesMessage("Record Updated Successfully."); 
//                   Message.setSeverity(FacesMessage.SEVERITY_INFO); 
//                   FacesContext fc = FacesContext.getCurrentInstance(); 
//                   fc.addMessage(null, Message);    
//               }
        
    }
    
    
    public String resolvEl(String data)
    {
                      FacesContext fc = FacesContext.getCurrentInstance();
                      Application app = fc.getApplication();
                      ExpressionFactory elFactory = app.getExpressionFactory();
                      ELContext elContext = fc.getELContext();
                      ValueExpression valueExp = elFactory.createValueExpression(elContext, data, Object.class);
                      String Message=valueExp.getValue(elContext).toString();
                      return Message;
    }

    public void popupDeleteDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                    {
                    ADFUtils.findOperation("Delete").execute();
                    System.out.println("Record Delete Successfully");
                   
                        FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                        FacesContext fc = FacesContext.getCurrentInstance();  
                        fc.addMessage(null, Message);  
                   
                    }
                AdfFacesContext.getCurrentInstance().addPartialTarget(createTableSuppSrvBinding);
    }

    public void setCreateTableSuppSrvBinding(RichTable createTableSuppSrvBinding) {
        this.createTableSuppSrvBinding = createTableSuppSrvBinding;
    }

    public RichTable getCreateTableSuppSrvBinding() {
        return createTableSuppSrvBinding;
    }

    public void reofferQtyValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        System.out.println("reoffer qty from ui in bean:"+object);
        OperationBinding op = ADFUtils.findOperation("checkReofferQty");
        op.getParamsMap().put("reoffrQty",object);
        op.execute();
        
        System.out.println("return value:"+op.getResult());                      
        if (op.getResult().toString().compareTo("F1")==0)
        {                                          
               throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "QTY CAN NOT BE GREATER THAN DEVIATION AUTHORISED QTY", null));
        }
        
        if (op.getResult().toString().compareTo("F2")==0)
        {                                          
               throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "REOFFER QTY CAN NOT BE GREATER THAN RECEIVED QTY", null));
        }
    }

    public void netRateValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        System.out.println("netrate qty from ui in bean:"+object);
        OperationBinding op = ADFUtils.findOperation("checkNetRateQty");
        op.getParamsMap().put("netrateQty",object);
        op.execute();
        
        System.out.println("return value from netrate validator:"+op.getResult());                      
        if (op.getResult().toString().compareTo("F1")==0)
        {                                          
               throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "QTY CAN NOT BE GREATER THAN DEVIATION AUTHORISED QTY", null));
        }
        
        if (op.getResult().toString().compareTo("F2")==0)
        {                                          
               throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "REOFFER QTY CAN NOT BE GREATER THAN RECEIVED QTY", null));
        }

    }

    public void parentSrvVCL(ValueChangeEvent valueChangeEvent) {
        if(valueChangeEvent.getNewValue()!=null){
        System.out.println("Parent SRV Number VCL");
            valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        OperationBinding op = ADFUtils.findOperation("parentSrvValidator");
        op.execute();
        }
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setInvoiceNoBinding(RichInputText invoiceNoBinding) {
        this.invoiceNoBinding = invoiceNoBinding;
    }

    public RichInputText getInvoiceNoBinding() {
        return invoiceNoBinding;
    }

    public void setParentSrvBinding(RichInputComboboxListOfValues parentSrvBinding) {
        this.parentSrvBinding = parentSrvBinding;
    }

    public RichInputComboboxListOfValues getParentSrvBinding() {
        return parentSrvBinding;
    }

    public void setParentSrvDateBinding(RichInputDate parentSrvDateBinding) {
        this.parentSrvDateBinding = parentSrvDateBinding;
    }

    public RichInputDate getParentSrvDateBinding() {
        return parentSrvDateBinding;
    }

    public void setSupplementSRVBinding(RichInputText supplementSRVBinding) {
        this.supplementSRVBinding = supplementSRVBinding;
    }

    public RichInputText getSupplementSRVBinding() {
        return supplementSRVBinding;
    }

    public void setSrvDateBinding(RichInputDate srvDateBinding) {
        this.srvDateBinding = srvDateBinding;
    }

    public RichInputDate getSrvDateBinding() {
        return srvDateBinding;
    }

    public void setCountedDtBinding(RichInputDate countedDtBinding) {
        this.countedDtBinding = countedDtBinding;
    }

    public RichInputDate getCountedDtBinding() {
        return countedDtBinding;
    }

    public void setVendorCodeBinding(RichInputText vendorCodeBinding) {
        this.vendorCodeBinding = vendorCodeBinding;
    }

    public RichInputText getVendorCodeBinding() {
        return vendorCodeBinding;
    }

    public void setLocationBinding(RichInputText locationBinding) {
        this.locationBinding = locationBinding;
    }

    public RichInputText getLocationBinding() {
        return locationBinding;
    }

    public void setSuppliedByBInding(RichInputText suppliedByBInding) {
        this.suppliedByBInding = suppliedByBInding;
    }

    public RichInputText getSuppliedByBInding() {
        return suppliedByBInding;
    }

    public void setShortExcessNoBinding(RichInputText shortExcessNoBinding) {
        this.shortExcessNoBinding = shortExcessNoBinding;
    }

    public RichInputText getShortExcessNoBinding() {
        return shortExcessNoBinding;
    }

    public void setChallanNoBInding(RichInputText challanNoBInding) {
        this.challanNoBInding = challanNoBInding;
    }

    public RichInputText getChallanNoBInding() {
        return challanNoBInding;
    }

    public void setChallanDataBinding(RichInputDate challanDataBinding) {
        this.challanDataBinding = challanDataBinding;
    }

    public RichInputDate getChallanDataBinding() {
        return challanDataBinding;
    }

    public void setDeviationRequestNoBinding(RichInputText deviationRequestNoBinding) {
        this.deviationRequestNoBinding = deviationRequestNoBinding;
    }

    public RichInputText getDeviationRequestNoBinding() {
        return deviationRequestNoBinding;
    }

    public void setCityCodeBinding(RichInputText cityCodeBinding) {
        this.cityCodeBinding = cityCodeBinding;
    }

    public RichInputText getCityCodeBinding() {
        return cityCodeBinding;
    }

    public void setNumberOfCasesBinding(RichInputText numberOfCasesBinding) {
        this.numberOfCasesBinding = numberOfCasesBinding;
    }

    public RichInputText getNumberOfCasesBinding() {
        return numberOfCasesBinding;
    }

    public void setPlantCodeBinding(RichInputText plantCodeBinding) {
        this.plantCodeBinding = plantCodeBinding;
    }

    public RichInputText getPlantCodeBinding() {
        return plantCodeBinding;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setDetailCreateBinding(RichButton detailCreateBinding) {
        this.detailCreateBinding = detailCreateBinding;
    }

    public RichButton getDetailCreateBinding() {
        return detailCreateBinding;
    }

    public void setPoNoBinding(RichInputText poNoBinding) {
        this.poNoBinding = poNoBinding;
    }

    public RichInputText getPoNoBinding() {
        return poNoBinding;
    }

    public void setPoDateBinding(RichInputDate poDateBinding) {
        this.poDateBinding = poDateBinding;
    }

    public RichInputDate getPoDateBinding() {
        return poDateBinding;
    }

    public void setRevisionNoBinding(RichInputText revisionNoBinding) {
        this.revisionNoBinding = revisionNoBinding;
    }

    public RichInputText getRevisionNoBinding() {
        return revisionNoBinding;
    }

    public void setProcessBinding(RichInputText processBinding) {
        this.processBinding = processBinding;
    }

    public RichInputText getProcessBinding() {
        return processBinding;
    }

    public void setChallanQuantityBinding(RichInputText challanQuantityBinding) {
        this.challanQuantityBinding = challanQuantityBinding;
    }

    public RichInputText getChallanQuantityBinding() {
        return challanQuantityBinding;
    }

    public void setReceivedQuantityBinding(RichInputText receivedQuantityBinding) {
        this.receivedQuantityBinding = receivedQuantityBinding;
    }

    public RichInputText getReceivedQuantityBinding() {
        return receivedQuantityBinding;
    }

    public void setUomBinding(RichInputText uomBinding) {
        this.uomBinding = uomBinding;
    }

    public RichInputText getUomBinding() {
        return uomBinding;
    }

    public void setReOfferQtyBinding(RichInputText reOfferQtyBinding) {
        this.reOfferQtyBinding = reOfferQtyBinding;
    }

    public RichInputText getReOfferQtyBinding() {
        return reOfferQtyBinding;
    }

    public void setNetRateBinding(RichInputText netRateBinding) {
        this.netRateBinding = netRateBinding;
    }

    public RichInputText getNetRateBinding() {
        return netRateBinding;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setOutputText(RichOutputText outputText) {
        this.outputText = outputText;
    }

    public RichOutputText getOutputText() {
        cevmodecheck();
        return outputText;
    }

    public void EditButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }
    
    
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                try {
                    Method method1 =
                        component.getClass().getMethod("setDisabled", boolean.class);
                    if (method1 != null) {
                        method1.invoke(component, valueComponent);
                    }
                } catch (NoSuchMethodException e) {
                    try {
                        Method method =
    component.getClass().getMethod("setReadOnly", boolean.class);
                        if (method != null) {
                            method.invoke(component, valueComponent);
                        }
                    } catch (Exception e1) {
                        // e.printStackTrace();//silently eat this exception.
                    }


                } catch (Exception e) {
                    // e.printStackTrace();//silently eat this exception.
                }
                List<UIComponent> childComponents = component.getChildren();
                for (UIComponent comp : childComponents) {
                    makeComponentHierarchyReadOnly(comp, valueComponent);
                }
            }




        private void cevmodecheck(){
            if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
    makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
                cevModeDisableComponent("V");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
    makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("C");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
    makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("E");
            }
        }
        public void cevModeDisableComponent(String mode) {
    //            FacesContext fctx = FacesContext.getCurrentInstance();
    //            ELContext elctx = fctx.getELContext();
    //            Application jsfApp = fctx.getApplication();
    //            //create a ValueExpression that points to the ADF binding layer
    //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
    //            //
    //            ValueExpression valueExpr = exprFactory.createValueExpression(
    //                                         elctx,
    // "#{pageFlowScope.mode=='E'}",
    //                                          Object.class
    //                                         );
    // getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
        if (mode.equals("E")) {
            getUnitCodeBinding().setDisabled(true);
            getParentSrvBinding().setDisabled(true);
            getRemarksBinding().setDisabled(true);
            getNetRateBinding().setDisabled(true);
            getInvoiceNoBinding().setDisabled(true);
            getInvoiceDateBinding().setDisabled(true); 
            getParentSrvDateBinding().setDisabled(true);
            getRrGrNoBinding().setDisabled(true);
            getSupplementSRVBinding().setDisabled(true);
            getSrvDateBinding().setDisabled(true);
            getVendorCodeBinding().setDisabled(true);
            getLocationBinding().setDisabled(true);
            getSuppliedByBInding().setDisabled(true);
            getShortExcessNoBinding().setDisabled(true);
            getChallanNoBInding().setDisabled(true);
            getChallanDataBinding().setDisabled(true);
            getDeviationRequestNoBinding().setDisabled(true);
            getCityCodeBinding().setDisabled(true);
            getNumberOfCasesBinding().setDisabled(true);
            getDateBinding().setDisabled(true);
            getPlantCodeBinding().setDisabled(true);
            getPoNoBinding().setDisabled(true);
            getPoDateBinding().setDisabled(true);
            getRevisionNoBinding().setDisabled(true);
            getProcessBinding().setDisabled(true);
            getChallanQuantityBinding().setDisabled(true);
            getReceivedQuantityBinding().setDisabled(true);
            getUomBinding().setDisabled(true);
            getReOfferQtyBinding().setDisabled(true);
            getNetRateBinding().setDisabled(true);
            getDetailCreateBinding().setDisabled(false);
            getGetAmendNoBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            getPreparedByBinding().setDisabled(true);
            getCountedDtBinding().setDisabled(true);
            
        } else if (mode.equals("C")) {
            getInvoiceNoBinding().setDisabled(true);
            getInvoiceDateBinding().setDisabled(true); 
            getParentSrvDateBinding().setDisabled(true);
            getRrGrNoBinding().setDisabled(true);
            getSupplementSRVBinding().setDisabled(true);
            getSrvDateBinding().setDisabled(true);
            getVendorCodeBinding().setDisabled(true);
            getLocationBinding().setDisabled(true);
            getSuppliedByBInding().setDisabled(true);
            getShortExcessNoBinding().setDisabled(true);
            getChallanNoBInding().setDisabled(true);
            getChallanDataBinding().setDisabled(true);
            getDeviationRequestNoBinding().setDisabled(true);
            getCityCodeBinding().setDisabled(true);
            getNumberOfCasesBinding().setDisabled(true);
            getDateBinding().setDisabled(true);
            getPlantCodeBinding().setDisabled(true);
            getPoNoBinding().setDisabled(true);
            getPoDateBinding().setDisabled(true);
            getRevisionNoBinding().setDisabled(true);
            getProcessBinding().setDisabled(true);
            getChallanQuantityBinding().setDisabled(true);
            getReceivedQuantityBinding().setDisabled(true);
            getUomBinding().setDisabled(true);
            getReOfferQtyBinding().setDisabled(true);
      //      getNetRateBinding().setDisabled(true);
            getGetAmendNoBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
            getPreparedByBinding().setDisabled(true);
            

        } else if (mode.equals("V")) {
            getTab1Binding().setDisabled(false);
            getTab2Binding().setDisabled(false);
            getDetailCreateBinding().setDisabled(true);

        }
        }

    public void setInvoiceDateBinding(RichInputDate invoiceDateBinding) {
        this.invoiceDateBinding = invoiceDateBinding;
    }

    public RichInputDate getInvoiceDateBinding() {
        return invoiceDateBinding;
    }

    public void setRrGrNoBinding(RichInputText rrGrNoBinding) {
        this.rrGrNoBinding = rrGrNoBinding;
    }

    public RichInputText getRrGrNoBinding() {
        return rrGrNoBinding;
    }

    public void setDateBinding(RichInputDate dateBinding) {
        this.dateBinding = dateBinding;
    }

    public RichInputDate getDateBinding() {
        return dateBinding;
    }

    public void setDtlLocationBinding(RichOutputText dtlLocationBinding) {
        this.dtlLocationBinding = dtlLocationBinding;
    }

    public RichOutputText getDtlLocationBinding() {
        return dtlLocationBinding;
    }

    public void setRemarksBinding(RichInputText remarksBinding) {
        this.remarksBinding = remarksBinding;
    }

    public RichInputText getRemarksBinding() {
        return remarksBinding;
    }


    public void setGetAmendNoBinding(RichInputText getAmendNoBinding) {
        this.getAmendNoBinding = getAmendNoBinding;
    }

    public RichInputText getGetAmendNoBinding() {
        return getAmendNoBinding;
    }

    public void setPreparedByBinding(RichInputComboboxListOfValues preparedByBinding) {
        this.preparedByBinding = preparedByBinding;
    }

    public RichInputComboboxListOfValues getPreparedByBinding() {
        return preparedByBinding;
    }

    public void setTab2Binding(RichShowDetailItem tab2Binding) {
        this.tab2Binding = tab2Binding;
    }

    public RichShowDetailItem getTab2Binding() {
        return tab2Binding;
    }

    public void setTab1Binding(RichShowDetailItem tab1Binding) {
        this.tab1Binding = tab1Binding;
    }

    public RichShowDetailItem getTab1Binding() {
        return tab1Binding;
    }
}
