package terms.mtl.transaction.ui.bean;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

public class UnitStoreBean {
    private RichInputText locationCodeBinding;
    private RichTable tableBinding;
    private String editAction="V";
    private RichInputComboboxListOfValues transLocCdBinding;
    private RichInputText unitCodeBinding;

    public UnitStoreBean() {
    }

    public void LocationVCL(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (valueChangeEvent.getNewValue()!=null) {

            System.out.println("Unit Code:" + unitCodeBinding.getValue() + " Location Code: " +
                               valueChangeEvent.getNewValue());
            if (unitCodeBinding.getValue() != null && valueChangeEvent.getNewValue() != null) {
                OperationBinding op = (OperationBinding) ADFUtils.findOperation("unitStoreUniqueCheck");
                op.getParamsMap().put("unitCode", unitCodeBinding.getValue());
                op.getParamsMap().put("locCd", valueChangeEvent.getNewValue());
                op.execute();
                System.out.println("Result: " + op.getResult());
                if (op.getResult() != null && op.getResult().equals("N")) {
                    locationCodeBinding.setValue(valueChangeEvent.getNewValue());
                } else {
                    AdfFacesContext.getCurrentInstance().addPartialTarget(transLocCdBinding);
                    ADFUtils.showMessage("Unit Code and Location Code combination alredy exists .", 0);
                }
            }
            AdfFacesContext.getCurrentInstance().addPartialTarget(tableBinding);
        }
    }

    public void setLocationCodeBinding(RichInputText locationCodeBinding) {
        this.locationCodeBinding = locationCodeBinding;
    }

    public RichInputText getLocationCodeBinding() {
        return locationCodeBinding;
    }

    public void popUpDialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
            {
            OperationBinding op = null;
            ADFUtils.findOperation("Delete").execute();
            op = (OperationBinding) ADFUtils.findOperation("Commit");
            if (getEditAction().equals("V")) {
                Object rst = op.execute();
            }
            System.out.println("Record Delete Successfully");
                if(op.getErrors().isEmpty()){
                    FacesMessage Message = new FacesMessage("Record Deleted Successfully");   
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                    FacesContext fc = FacesContext.getCurrentInstance();   
                    fc.addMessage(null, Message);
                } else if (!op.getErrors().isEmpty())
                {
                   OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
                   Object rstr = opr.execute();
                   FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                   Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                   FacesContext fc = FacesContext.getCurrentInstance();
                   fc.addMessage(null, Message);
                 }
            }

        AdfFacesContext.getCurrentInstance().addPartialTarget(tableBinding);
    }

    public void setTableBinding(RichTable tableBinding) {
        this.tableBinding = tableBinding;
    }

    public RichTable getTableBinding() {
        return tableBinding;
    }

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public void setTransLocCdBinding(RichInputComboboxListOfValues transLocCdBinding) {
        this.transLocCdBinding = transLocCdBinding;
    }

    public RichInputComboboxListOfValues getTransLocCdBinding() {
        return transLocCdBinding;
    }

    public void setUnitCodeBinding(RichInputText unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputText getUnitCodeBinding() {
        return unitCodeBinding;
    }
}
