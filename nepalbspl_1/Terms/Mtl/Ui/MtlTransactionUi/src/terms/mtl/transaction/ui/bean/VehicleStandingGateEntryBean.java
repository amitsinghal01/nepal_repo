package terms.mtl.transaction.ui.bean;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

public class VehicleStandingGateEntryBean {
    private String editAction="V";
    private RichTable tableBinding;
    private Integer code=0;
    private RichInputDate reportTimeBinding;
    private RichInputDate outTimeBinding;
    private RichInputText durationBinding;

    public VehicleStandingGateEntryBean() {
        
    }

    public void CreateAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert").execute();
//  
//            if(code==0)
//            {
            OperationBinding opr= ADFUtils.findOperation("generateserialNoVehicleStandingAtGateEntry");
            Integer sr_no=(Integer)opr.execute();
//            code=sr_no;
//            }
//            else
//            {
//                code=code+1;
//                Row rr=(Row) ADFUtils.evaluateEL("#{bindings.VehicleStandingatGateEntryVO1Iterator.currentRow}");
//                rr.setAttribute("SrNo",code);
//            }
//         
    }

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public void DeleteDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
        {
        OperationBinding op = null;
        ADFUtils.findOperation("Delete").execute();
        op = (OperationBinding) ADFUtils.findOperation("Commit");
        if (getEditAction().equals("V")) {
        Object rst = op.execute();
        }
        System.out.println("Record Delete Successfully");
        if(op.getErrors().isEmpty()){
        FacesMessage Message = new FacesMessage("Record Deleted Successfully");
        Message.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        } else if (!op.getErrors().isEmpty())
        {
        OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
        Object rstr = opr.execute();
        FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
        Message.setSeverity(FacesMessage.SEVERITY_ERROR);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(tableBinding);
        
        
    }

    public void setTableBinding(RichTable tableBinding) {
        this.tableBinding = tableBinding;
    }

    public RichTable getTableBinding() {
        return tableBinding;
    }

    public void CancelbuttonAL(ActionEvent actionEvent) {
       ADFUtils.findOperation("Rollback").execute();
       code=0;
    }

    public void setReportTimeBinding(RichInputDate reportTimeBinding) {
        this.reportTimeBinding = reportTimeBinding;
    }

    public RichInputDate getReportTimeBinding() {
        return reportTimeBinding;
    }

    public void setOutTimeBinding(RichInputDate outTimeBinding) {
        this.outTimeBinding = outTimeBinding;
    }

    public RichInputDate getOutTimeBinding() {
        return outTimeBinding;
    }

    public void setDurationBinding(RichInputText durationBinding) {
        this.durationBinding = durationBinding;
    }

    public RichInputText getDurationBinding() {
        return durationBinding;
    }
}
