package terms.mtl.transaction.ui.bean;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.binding.OperationBinding;

public class VendorSrvBillMappingBean {
    private String editAction="V";

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public VendorSrvBillMappingBean() {
    }

    public void saveAL(ActionEvent actionEvent) {
        OperationBinding ob=ADFUtils.findOperation("CheckForStatusOfApprovedMRN");
        Object op=ob.execute();
        System.out.println("result is==>>"+ob.getResult());
        if(ob.getResult()!=null && ob.getResult().toString().equalsIgnoreCase("F")){
            ADFUtils.showMessage("You cannot modify the Status of Approved MRN", 1);
        }
        else{
      ADFUtils.findOperation("UpdateValuesInSrvHeads").execute();
      ADFUtils.findOperation("Commit").execute();
        }
    }

    public void statusValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if(object!=null){
//            OperationBinding ob=ADFUtils.findOperation("CheckForStatusOfApprovedMRN");
//            Object op=ob.execute();
//            System.out.println("result is==>>"+ob.getResult());
//            if(ob.getResult()!=null && ob.getResult().toString().equalsIgnoreCase("F")){
//                ADFUtils.showMessage("MRN is already created for this Gate Entry", 1);
//            }
        }

    }

    public void statusVCL(ValueChangeEvent valueChangeEvent) {
        if(valueChangeEvent.getNewValue() != null){
            OperationBinding ob=ADFUtils.findOperation("CheckForStatusOfApprovedMRN");
            Object op=ob.execute();
            System.out.println("result is==>>"+ob.getResult());
            if(ob.getResult()!=null && ob.getResult().toString().equalsIgnoreCase("F")){
                ADFUtils.showMessage("You cannot modify the Status of Approved MRN", 1);
            }
        }
    }
}
