package terms.mtl.transaction.ui.workarea.bean;

import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.nav.RichLink;

import oracle.ui.pattern.dynamicShell.TabContext;

public class MtlTransactionWorkareaBean {
    public MtlTransactionWorkareaBean() {
    }

    public void selectOrCreateTab(ActionEvent actionEvent) {
        // Add event code here...
    }
    public void selectOrCreateNewTab(ActionEvent actionEvent) {
        RichLink link=(RichLink)actionEvent.getComponent();
        String TaskFlowName = link.getAttributes().get("TaskFlowName").toString();
        String TaskFlowSource = link.getAttributes().get("TaskFlowSource").toString();
        try {
            TabContext.getCurrentInstance().addOrSelectTab(TaskFlowName, TaskFlowSource);
        } catch (TabContext.TabOverflowException e) {
            e.printStackTrace();
        }
    }

}
