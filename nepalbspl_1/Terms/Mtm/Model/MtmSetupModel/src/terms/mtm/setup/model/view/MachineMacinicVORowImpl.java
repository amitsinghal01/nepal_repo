package terms.mtm.setup.model.view;

import java.sql.Timestamp;

import oracle.jbo.RowSet;
import oracle.jbo.server.EntityImpl;
import oracle.jbo.server.ViewRowImpl;

import terms.hrm.setup.model.entity.EmployeeMasterEOImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Wed Aug 08 16:26:42 IST 2018
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class MachineMacinicVORowImpl extends ViewRowImpl {
    public static final int ENTITY_MACHINEMACINICEO = 0;
    public static final int ENTITY_EMPLOYEEMASTEREO = 1;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        CreatedBy,
        CreationDate,
        EmpCd,
        LastUpdateDate,
        LastUpdatedBy,
        MachCode,
        MachId,
        ObjectVersionNumber,
        Remarks,
        EmpFirstName,
        EmpNumber,
        ObjectVersionNumber1,
        EmpLastName,
        TagNo,
        EmpViewVVO1;
        static AttributesEnum[] vals = null;
        ;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int CREATEDBY = AttributesEnum.CreatedBy.index();
    public static final int CREATIONDATE = AttributesEnum.CreationDate.index();
    public static final int EMPCD = AttributesEnum.EmpCd.index();
    public static final int LASTUPDATEDATE = AttributesEnum.LastUpdateDate.index();
    public static final int LASTUPDATEDBY = AttributesEnum.LastUpdatedBy.index();
    public static final int MACHCODE = AttributesEnum.MachCode.index();
    public static final int MACHID = AttributesEnum.MachId.index();
    public static final int OBJECTVERSIONNUMBER = AttributesEnum.ObjectVersionNumber.index();
    public static final int REMARKS = AttributesEnum.Remarks.index();
    public static final int EMPFIRSTNAME = AttributesEnum.EmpFirstName.index();
    public static final int EMPNUMBER = AttributesEnum.EmpNumber.index();
    public static final int OBJECTVERSIONNUMBER1 = AttributesEnum.ObjectVersionNumber1.index();
    public static final int EMPLASTNAME = AttributesEnum.EmpLastName.index();
    public static final int TAGNO = AttributesEnum.TagNo.index();
    public static final int EMPVIEWVVO1 = AttributesEnum.EmpViewVVO1.index();

    /**
     * This is the default constructor (do not remove).
     */
    public MachineMacinicVORowImpl() {
    }

    /**
     * Gets MachineMacinicEO entity object.
     * @return the MachineMacinicEO
     */
    public EntityImpl getMachineMacinicEO() {
        return (EntityImpl) getEntity(ENTITY_MACHINEMACINICEO);
    }

    /**
     * Gets EmployeeMasterEO entity object.
     * @return the EmployeeMasterEO
     */
    public EmployeeMasterEOImpl getEmployeeMasterEO() {
        return (EmployeeMasterEOImpl) getEntity(ENTITY_EMPLOYEEMASTEREO);
    }

    /**
     * Gets the attribute value for CREATED_BY using the alias name CreatedBy.
     * @return the CREATED_BY
     */
    public String getCreatedBy() {
        return (String) getAttributeInternal(CREATEDBY);
    }


    /**
     * Gets the attribute value for CREATION_DATE using the alias name CreationDate.
     * @return the CREATION_DATE
     */
    public Timestamp getCreationDate() {
        return (Timestamp) getAttributeInternal(CREATIONDATE);
    }


    /**
     * Gets the attribute value for EMP_CD using the alias name EmpCd.
     * @return the EMP_CD
     */
    public String getEmpCd() {
        return (String) getAttributeInternal(EMPCD);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_CD using the alias name EmpCd.
     * @param value value to set the EMP_CD
     */
    public void setEmpCd(String value) {
        setAttributeInternal(EMPCD, value);
    }

    /**
     * Gets the attribute value for LAST_UPDATE_DATE using the alias name LastUpdateDate.
     * @return the LAST_UPDATE_DATE
     */
    public Timestamp getLastUpdateDate() {
        return (Timestamp) getAttributeInternal(LASTUPDATEDATE);
    }


    /**
     * Gets the attribute value for LAST_UPDATED_BY using the alias name LastUpdatedBy.
     * @return the LAST_UPDATED_BY
     */
    public String getLastUpdatedBy() {
        return (String) getAttributeInternal(LASTUPDATEDBY);
    }


    /**
     * Gets the attribute value for MACH_CODE using the alias name MachCode.
     * @return the MACH_CODE
     */
    public String getMachCode() {
        return (String) getAttributeInternal(MACHCODE);
    }

    /**
     * Sets <code>value</code> as attribute value for MACH_CODE using the alias name MachCode.
     * @param value value to set the MACH_CODE
     */
    public void setMachCode(String value) {
        setAttributeInternal(MACHCODE, value);
    }

    /**
     * Gets the attribute value for MACH_ID using the alias name MachId.
     * @return the MACH_ID
     */
    public Long getMachId() {
        return (Long) getAttributeInternal(MACHID);
    }

    /**
     * Sets <code>value</code> as attribute value for MACH_ID using the alias name MachId.
     * @param value value to set the MACH_ID
     */
    public void setMachId(Long value) {
        setAttributeInternal(MACHID, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER);
    }


    /**
     * Gets the attribute value for REMARKS using the alias name Remarks.
     * @return the REMARKS
     */
    public String getRemarks() {
        return (String) getAttributeInternal(REMARKS);
    }

    /**
     * Sets <code>value</code> as attribute value for REMARKS using the alias name Remarks.
     * @param value value to set the REMARKS
     */
    public void setRemarks(String value) {
        setAttributeInternal(REMARKS, value);
    }

    /**
     * Gets the attribute value for EMP_FIRST_NAME using the alias name EmpFirstName.
     * @return the EMP_FIRST_NAME
     */
    public String getEmpFirstName() {
        if((getAttributeInternal(EMPFIRSTNAME)!=null) && (getAttributeInternal(EMPLASTNAME)!=null)){
            return (String) getAttributeInternal(EMPFIRSTNAME) + " " + (String) getAttributeInternal(EMPLASTNAME);            
        }else{
        return (String) getAttributeInternal(EMPFIRSTNAME);
        }
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_FIRST_NAME using the alias name EmpFirstName.
     * @param value value to set the EMP_FIRST_NAME
     */
    public void setEmpFirstName(String value) {
        setAttributeInternal(EMPFIRSTNAME, value);
    }

    /**
     * Gets the attribute value for EMP_NUMBER using the alias name EmpNumber.
     * @return the EMP_NUMBER
     */
    public String getEmpNumber() {
        return (String) getAttributeInternal(EMPNUMBER);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_NUMBER using the alias name EmpNumber.
     * @param value value to set the EMP_NUMBER
     */
    public void setEmpNumber(String value) {
        setAttributeInternal(EMPNUMBER, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber1.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber1() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER1);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber1.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber1(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER1, value);
    }

    /**
     * Gets the attribute value for EMP_LAST_NAME using the alias name EmpLastName.
     * @return the EMP_LAST_NAME
     */
    public String getEmpLastName() {
        return (String) getAttributeInternal(EMPLASTNAME);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_LAST_NAME using the alias name EmpLastName.
     * @param value value to set the EMP_LAST_NAME
     */
    public void setEmpLastName(String value) {
        setAttributeInternal(EMPLASTNAME, value);
    }

    /**
     * Gets the attribute value for TAG_NO using the alias name TagNo.
     * @return the TAG_NO
     */
    public String getTagNo() {
        return (String) getAttributeInternal(TAGNO);
    }

    /**
     * Sets <code>value</code> as attribute value for TAG_NO using the alias name TagNo.
     * @param value value to set the TAG_NO
     */
    public void setTagNo(String value) {
        setAttributeInternal(TAGNO, value);
    }


    /**
     * Gets the view accessor <code>RowSet</code> EmpViewVVO1.
     */
    public RowSet getEmpViewVVO1() {
        return (RowSet) getAttributeInternal(EMPVIEWVVO1);
    }
}

