package terms.mtm.transaction.model.view;

import java.math.BigDecimal;

import oracle.jbo.Row;
import oracle.jbo.RowIterator;
import oracle.jbo.RowSet;
import oracle.jbo.ViewCriteria;
import oracle.jbo.domain.Timestamp;
import oracle.jbo.server.EntityImpl;
import oracle.jbo.server.ViewRowImpl;

import terms.hrm.setup.model.entity.EmployeeMasterEOImpl;

import terms.mtm.transaction.model.applicationModule.MtmTransactionAMImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Mon Jan 15 17:21:10 IST 2018
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class ActualPmAmcMachineDetailVORowImpl extends ViewRowImpl {

    public static final int ENTITY_ACTUALPMAMCMACHINEDETAILEO = 0;
    public static final int ENTITY_MACHINEMASTEREO = 1;
    public static final int ENTITY_EMPLOYEEMASTEREO = 2;
    public static final int ENTITY_ACTUALPMAMCMACHINEHEADEREO = 3;
    public static final int ENTITY_CHECKPOINTMASTEREO = 4;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        CreatedBy,
        CreationDate,
        LastUpdateDate,
        LastUpdatedBy,
        Line,
        Location,
        MLMachineCode,
        ObjectVersionNumber,
        PmAmcDoneBy,
        PmAmcStatus,
        PmElectricalHrs,
        PmMechanicalHrsHrs,
        PmScHeAmendNo,
        PmScHeScheduleNo,
        PmType,
        ProposDt,
        Remarks,
        SchApprovYn,
        SchedApprovedBy,
        ScheduleId,
        ScheduleLineId,
        StartDate,
        StartTime,
        Description,
        MachCode,
        MachId,
        EmpFirstName,
        EmpLastName,
        EmpNumber,
        ObjectVersionNumber1,
        HoverDateTrans,
        HoverTimeTrans,
        RemarksTrans,
        RcvByTrans,
        DescriptionTrans,
        UnitCd,
        ObjectVersionNumber2,
        ScheduleId1,
        TagNo,
        CMCode,
        CorrecAction,
        Observation,
        OkayYn,
        CMDesc,
        ChkId,
        ObjectVersionNumber3,
        ActualPmAmcMcHandoverVO,
        ActualPmAmcMachineActualPmCheckPointVO,
        ActualPmAmcMachineActualPmItemConsumptionVO,
        ActualPmAmcPmActualVO,
        SecControlVO1,
        EmpViewVVO1,
        MachineMasterVO1,
        SecControlVO2;
        static AttributesEnum[] vals = null;
        ;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int CREATEDBY = AttributesEnum.CreatedBy.index();
    public static final int CREATIONDATE = AttributesEnum.CreationDate.index();
    public static final int LASTUPDATEDATE = AttributesEnum.LastUpdateDate.index();
    public static final int LASTUPDATEDBY = AttributesEnum.LastUpdatedBy.index();
    public static final int LINE = AttributesEnum.Line.index();
    public static final int LOCATION = AttributesEnum.Location.index();
    public static final int MLMACHINECODE = AttributesEnum.MLMachineCode.index();
    public static final int OBJECTVERSIONNUMBER = AttributesEnum.ObjectVersionNumber.index();
    public static final int PMAMCDONEBY = AttributesEnum.PmAmcDoneBy.index();
    public static final int PMAMCSTATUS = AttributesEnum.PmAmcStatus.index();
    public static final int PMELECTRICALHRS = AttributesEnum.PmElectricalHrs.index();
    public static final int PMMECHANICALHRSHRS = AttributesEnum.PmMechanicalHrsHrs.index();
    public static final int PMSCHEAMENDNO = AttributesEnum.PmScHeAmendNo.index();
    public static final int PMSCHESCHEDULENO = AttributesEnum.PmScHeScheduleNo.index();
    public static final int PMTYPE = AttributesEnum.PmType.index();
    public static final int PROPOSDT = AttributesEnum.ProposDt.index();
    public static final int REMARKS = AttributesEnum.Remarks.index();
    public static final int SCHAPPROVYN = AttributesEnum.SchApprovYn.index();
    public static final int SCHEDAPPROVEDBY = AttributesEnum.SchedApprovedBy.index();
    public static final int SCHEDULEID = AttributesEnum.ScheduleId.index();
    public static final int SCHEDULELINEID = AttributesEnum.ScheduleLineId.index();
    public static final int STARTDATE = AttributesEnum.StartDate.index();
    public static final int STARTTIME = AttributesEnum.StartTime.index();
    public static final int DESCRIPTION = AttributesEnum.Description.index();
    public static final int MACHCODE = AttributesEnum.MachCode.index();
    public static final int MACHID = AttributesEnum.MachId.index();
    public static final int EMPFIRSTNAME = AttributesEnum.EmpFirstName.index();
    public static final int EMPLASTNAME = AttributesEnum.EmpLastName.index();
    public static final int EMPNUMBER = AttributesEnum.EmpNumber.index();
    public static final int OBJECTVERSIONNUMBER1 = AttributesEnum.ObjectVersionNumber1.index();
    public static final int HOVERDATETRANS = AttributesEnum.HoverDateTrans.index();
    public static final int HOVERTIMETRANS = AttributesEnum.HoverTimeTrans.index();
    public static final int REMARKSTRANS = AttributesEnum.RemarksTrans.index();
    public static final int RCVBYTRANS = AttributesEnum.RcvByTrans.index();
    public static final int DESCRIPTIONTRANS = AttributesEnum.DescriptionTrans.index();
    public static final int UNITCD = AttributesEnum.UnitCd.index();
    public static final int OBJECTVERSIONNUMBER2 = AttributesEnum.ObjectVersionNumber2.index();
    public static final int SCHEDULEID1 = AttributesEnum.ScheduleId1.index();
    public static final int TAGNO = AttributesEnum.TagNo.index();
    public static final int CMCODE = AttributesEnum.CMCode.index();
    public static final int CORRECACTION = AttributesEnum.CorrecAction.index();
    public static final int OBSERVATION = AttributesEnum.Observation.index();
    public static final int OKAYYN = AttributesEnum.OkayYn.index();
    public static final int CMDESC = AttributesEnum.CMDesc.index();
    public static final int CHKID = AttributesEnum.ChkId.index();
    public static final int OBJECTVERSIONNUMBER3 = AttributesEnum.ObjectVersionNumber3.index();
    public static final int ACTUALPMAMCMCHANDOVERVO = AttributesEnum.ActualPmAmcMcHandoverVO.index();
    public static final int ACTUALPMAMCMACHINEACTUALPMCHECKPOINTVO =
        AttributesEnum.ActualPmAmcMachineActualPmCheckPointVO.index();
    public static final int ACTUALPMAMCMACHINEACTUALPMITEMCONSUMPTIONVO =
        AttributesEnum.ActualPmAmcMachineActualPmItemConsumptionVO.index();
    public static final int ACTUALPMAMCPMACTUALVO = AttributesEnum.ActualPmAmcPmActualVO.index();
    public static final int SECCONTROLVO1 = AttributesEnum.SecControlVO1.index();
    public static final int EMPVIEWVVO1 = AttributesEnum.EmpViewVVO1.index();
    public static final int MACHINEMASTERVO1 = AttributesEnum.MachineMasterVO1.index();
    public static final int SECCONTROLVO2 = AttributesEnum.SecControlVO2.index();

    /**
     * This is the default constructor (do not remove).
     */
    public ActualPmAmcMachineDetailVORowImpl() {
    }

    /**
     * Gets ActualPmAmcMachineDetailEO entity object.
     * @return the ActualPmAmcMachineDetailEO
     */
    public EntityImpl getActualPmAmcMachineDetailEO() {
        return (EntityImpl) getEntity(ENTITY_ACTUALPMAMCMACHINEDETAILEO);
    }

    /**
     * Gets MachineMasterEO entity object.
     * @return the MachineMasterEO
     */
    public EntityImpl getMachineMasterEO() {
        return (EntityImpl) getEntity(ENTITY_MACHINEMASTEREO);
    }

    /**
     * Gets EmployeeMasterEO entity object.
     * @return the EmployeeMasterEO
     */
    public EmployeeMasterEOImpl getEmployeeMasterEO() {
        return (EmployeeMasterEOImpl) getEntity(ENTITY_EMPLOYEEMASTEREO);
    }

    /**
     * Gets ActualPmAmcMachineHeaderEO entity object.
     * @return the ActualPmAmcMachineHeaderEO
     */
    public EntityImpl getActualPmAmcMachineHeaderEO() {
        return (EntityImpl) getEntity(ENTITY_ACTUALPMAMCMACHINEHEADEREO);
    }


    /**
     * Gets CheckPointMasterEO entity object.
     * @return the CheckPointMasterEO
     */
    public EntityImpl getCheckPointMasterEO() {
        return (EntityImpl) getEntity(ENTITY_CHECKPOINTMASTEREO);
    }

    /**
     * Gets the attribute value for CREATED_BY using the alias name CreatedBy.
     * @return the CREATED_BY
     */
    public String getCreatedBy() {
        return (String) getAttributeInternal(CREATEDBY);
    }

    /**
     * Gets the attribute value for CREATION_DATE using the alias name CreationDate.
     * @return the CREATION_DATE
     */
    public Timestamp getCreationDate() {
        return (Timestamp) getAttributeInternal(CREATIONDATE);
    }

    /**
     * Gets the attribute value for LAST_UPDATE_DATE using the alias name LastUpdateDate.
     * @return the LAST_UPDATE_DATE
     */
    public Timestamp getLastUpdateDate() {
        return (Timestamp) getAttributeInternal(LASTUPDATEDATE);
    }

    /**
     * Gets the attribute value for LAST_UPDATED_BY using the alias name LastUpdatedBy.
     * @return the LAST_UPDATED_BY
     */
    public String getLastUpdatedBy() {
        return (String) getAttributeInternal(LASTUPDATEDBY);
    }

    /**
     * Gets the attribute value for LINE using the alias name Line.
     * @return the LINE
     */
    public Integer getLine() {
        return (Integer) getAttributeInternal(LINE);
    }

    /**
     * Sets <code>value</code> as attribute value for LINE using the alias name Line.
     * @param value value to set the LINE
     */
    public void setLine(Integer value) {
        setAttributeInternal(LINE, value);
    }

    /**
     * Gets the attribute value for LOCATION using the alias name Location.
     * @return the LOCATION
     */
    public String getLocation() {
        return (String) getAttributeInternal(LOCATION);
    }

    /**
     * Sets <code>value</code> as attribute value for LOCATION using the alias name Location.
     * @param value value to set the LOCATION
     */
    public void setLocation(String value) {
        setAttributeInternal(LOCATION, value);
    }

    /**
     * Gets the attribute value for M_L_MACHINE_CODE using the alias name MLMachineCode.
     * @return the M_L_MACHINE_CODE
     */
    public String getMLMachineCode() {
        return (String) getAttributeInternal(MLMACHINECODE);
    }

    /**
     * Sets <code>value</code> as attribute value for M_L_MACHINE_CODE using the alias name MLMachineCode.
     * @param value value to set the M_L_MACHINE_CODE
     */
    public void setMLMachineCode(String value) {
        setAttributeInternal(MLMACHINECODE, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER, value);
    }

    /**
     * Gets the attribute value for PM_AMC_DONE_BY using the alias name PmAmcDoneBy.
     * @return the PM_AMC_DONE_BY
     */
    public String getPmAmcDoneBy() {
        return (String) getAttributeInternal(PMAMCDONEBY);
    }

    /**
     * Sets <code>value</code> as attribute value for PM_AMC_DONE_BY using the alias name PmAmcDoneBy.
     * @param value value to set the PM_AMC_DONE_BY
     */
    public void setPmAmcDoneBy(String value) {
        setAttributeInternal(PMAMCDONEBY, value);
    }

    /**
     * Gets the attribute value for PM_AMC_STATUS using the alias name PmAmcStatus.
     * @return the PM_AMC_STATUS
     */
    public String getPmAmcStatus() {
        return (String) getAttributeInternal(PMAMCSTATUS);
    }

    /**
     * Sets <code>value</code> as attribute value for PM_AMC_STATUS using the alias name PmAmcStatus.
     * @param value value to set the PM_AMC_STATUS
     */
    public void setPmAmcStatus(String value) {
        setAttributeInternal(PMAMCSTATUS, value);
    }

    /**
     * Gets the attribute value for PM_ELECTRICAL_HRS using the alias name PmElectricalHrs.
     * @return the PM_ELECTRICAL_HRS
     */
    public BigDecimal getPmElectricalHrs() {
        return (BigDecimal) getAttributeInternal(PMELECTRICALHRS);
    }

    /**
     * Sets <code>value</code> as attribute value for PM_ELECTRICAL_HRS using the alias name PmElectricalHrs.
     * @param value value to set the PM_ELECTRICAL_HRS
     */
    public void setPmElectricalHrs(BigDecimal value) {
        setAttributeInternal(PMELECTRICALHRS, value);
    }

    /**
     * Gets the attribute value for PM_MECHANICAL_HRS_HRS using the alias name PmMechanicalHrsHrs.
     * @return the PM_MECHANICAL_HRS_HRS
     */
    public BigDecimal getPmMechanicalHrsHrs() {
        return (BigDecimal) getAttributeInternal(PMMECHANICALHRSHRS);
    }

    /**
     * Sets <code>value</code> as attribute value for PM_MECHANICAL_HRS_HRS using the alias name PmMechanicalHrsHrs.
     * @param value value to set the PM_MECHANICAL_HRS_HRS
     */
    public void setPmMechanicalHrsHrs(BigDecimal value) {
        setAttributeInternal(PMMECHANICALHRSHRS, value);
    }

    /**
     * Gets the attribute value for PM_SC_HE_AMEND_NO using the alias name PmScHeAmendNo.
     * @return the PM_SC_HE_AMEND_NO
     */
    public Integer getPmScHeAmendNo() {
        return (Integer) getAttributeInternal(PMSCHEAMENDNO);
    }

    /**
     * Sets <code>value</code> as attribute value for PM_SC_HE_AMEND_NO using the alias name PmScHeAmendNo.
     * @param value value to set the PM_SC_HE_AMEND_NO
     */
    public void setPmScHeAmendNo(Integer value) {
        setAttributeInternal(PMSCHEAMENDNO, value);
    }

    /**
     * Gets the attribute value for PM_SC_HE_SCHEDULE_NO using the alias name PmScHeScheduleNo.
     * @return the PM_SC_HE_SCHEDULE_NO
     */
    public Integer getPmScHeScheduleNo() {
        return (Integer) getAttributeInternal(PMSCHESCHEDULENO);
    }

    /**
     * Sets <code>value</code> as attribute value for PM_SC_HE_SCHEDULE_NO using the alias name PmScHeScheduleNo.
     * @param value value to set the PM_SC_HE_SCHEDULE_NO
     */
    public void setPmScHeScheduleNo(Integer value) {
        setAttributeInternal(PMSCHESCHEDULENO, value);
    }

    /**
     * Gets the attribute value for PM_TYPE using the alias name PmType.
     * @return the PM_TYPE
     */
    public String getPmType() {
        return (String) getAttributeInternal(PMTYPE);
    }

    /**
     * Sets <code>value</code> as attribute value for PM_TYPE using the alias name PmType.
     * @param value value to set the PM_TYPE
     */
    public void setPmType(String value) {
        setAttributeInternal(PMTYPE, value);
    }

    /**
     * Gets the attribute value for PROPOS_DT using the alias name ProposDt.
     * @return the PROPOS_DT
     */
    public Timestamp getProposDt() {
        return (Timestamp) getAttributeInternal(PROPOSDT);
    }

    /**
     * Sets <code>value</code> as attribute value for PROPOS_DT using the alias name ProposDt.
     * @param value value to set the PROPOS_DT
     */
    public void setProposDt(Timestamp value) {
        setAttributeInternal(PROPOSDT, value);
    }

    /**
     * Gets the attribute value for REMARKS using the alias name Remarks.
     * @return the REMARKS
     */
    public String getRemarks() {
        return (String) getAttributeInternal(REMARKS);
    }

    /**
     * Sets <code>value</code> as attribute value for REMARKS using the alias name Remarks.
     * @param value value to set the REMARKS
     */
    public void setRemarks(String value) {
        setAttributeInternal(REMARKS, value);
    }

    /**
     * Gets the attribute value for SCH_APPROV_YN using the alias name SchApprovYn.
     * @return the SCH_APPROV_YN
     */
    public String getSchApprovYn() {
        return (String) getAttributeInternal(SCHAPPROVYN);
    }

    /**
     * Sets <code>value</code> as attribute value for SCH_APPROV_YN using the alias name SchApprovYn.
     * @param value value to set the SCH_APPROV_YN
     */
    public void setSchApprovYn(String value) {
        setAttributeInternal(SCHAPPROVYN, value);
    }

    /**
     * Gets the attribute value for SCHED_APPROVED_BY using the alias name SchedApprovedBy.
     * @return the SCHED_APPROVED_BY
     */
    public String getSchedApprovedBy() {
        return (String) getAttributeInternal(SCHEDAPPROVEDBY);
    }

    /**
     * Sets <code>value</code> as attribute value for SCHED_APPROVED_BY using the alias name SchedApprovedBy.
     * @param value value to set the SCHED_APPROVED_BY
     */
    public void setSchedApprovedBy(String value) {
        setAttributeInternal(SCHEDAPPROVEDBY, value);
    }

    /**
     * Gets the attribute value for SCHEDULE_ID using the alias name ScheduleId.
     * @return the SCHEDULE_ID
     */
    public Long getScheduleId() {
        return (Long) getAttributeInternal(SCHEDULEID);
    }

    /**
     * Sets <code>value</code> as attribute value for SCHEDULE_ID using the alias name ScheduleId.
     * @param value value to set the SCHEDULE_ID
     */
    public void setScheduleId(Long value) {
        setAttributeInternal(SCHEDULEID, value);
    }

    /**
     * Gets the attribute value for SCHEDULE_LINE_ID using the alias name ScheduleLineId.
     * @return the SCHEDULE_LINE_ID
     */
    public Long getScheduleLineId() {
        return (Long) getAttributeInternal(SCHEDULELINEID);
    }

    /**
     * Sets <code>value</code> as attribute value for SCHEDULE_LINE_ID using the alias name ScheduleLineId.
     * @param value value to set the SCHEDULE_LINE_ID
     */
    public void setScheduleLineId(Long value) {
        setAttributeInternal(SCHEDULELINEID, value);
    }

    /**
     * Gets the attribute value for START_DATE using the alias name StartDate.
     * @return the START_DATE
     */
    public Timestamp getStartDate() {
        return (Timestamp) getAttributeInternal(STARTDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for START_DATE using the alias name StartDate.
     * @param value value to set the START_DATE
     */
    public void setStartDate(Timestamp value) {
        setAttributeInternal(STARTDATE, value);
    }

    /**
     * Gets the attribute value for START_TIME using the alias name StartTime.
     * @return the START_TIME
     */
    public Timestamp getStartTime() {
        return (Timestamp) getAttributeInternal(STARTTIME);
    }

    /**
     * Sets <code>value</code> as attribute value for START_TIME using the alias name StartTime.
     * @param value value to set the START_TIME
     */
    public void setStartTime(Timestamp value) {
        setAttributeInternal(STARTTIME, value);
    }

    /**
     * Gets the attribute value for DESCRIPTION using the alias name Description.
     * @return the DESCRIPTION
     */
    public String getDescription() 
    {
      return (String) getAttributeInternal(DESCRIPTION);
    }

    /**
     * Sets <code>value</code> as attribute value for DESCRIPTION using the alias name Description.
     * @param value value to set the DESCRIPTION
     */
    public void setDescription(String value) {
        setAttributeInternal(DESCRIPTION, value);
    }

    /**
     * Gets the attribute value for MACH_CODE using the alias name MachCode.
     * @return the MACH_CODE
     */
    public String getMachCode() {
        return (String) getAttributeInternal(MACHCODE);
    }

    /**
     * Sets <code>value</code> as attribute value for MACH_CODE using the alias name MachCode.
     * @param value value to set the MACH_CODE
     */
    public void setMachCode(String value) {
        setAttributeInternal(MACHCODE, value);
    }

    /**
     * Gets the attribute value for MACH_ID using the alias name MachId.
     * @return the MACH_ID
     */
    public Long getMachId() {
        return (Long) getAttributeInternal(MACHID);
    }

    /**
     * Sets <code>value</code> as attribute value for MACH_ID using the alias name MachId.
     * @param value value to set the MACH_ID
     */
    public void setMachId(Long value) {
        setAttributeInternal(MACHID, value);
    }

    /**
     * Gets the attribute value for EMP_FIRST_NAME using the alias name EmpFirstName.
     * @return the EMP_FIRST_NAME
     */
    public String getEmpFirstName()
    {
        if ((getAttributeInternal(EMPFIRSTNAME) != null) && (getAttributeInternal(EMPLASTNAME) != null))
         {

            return (String) getAttributeInternal(EMPFIRSTNAME) + " " + getAttributeInternal(EMPLASTNAME);
        }
         else {
            return (String) getAttributeInternal(EMPFIRSTNAME);
        }
        
       // return (String) getAttributeInternal(EMPFIRSTNAME);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_FIRST_NAME using the alias name EmpFirstName.
     * @param value value to set the EMP_FIRST_NAME
     */
    public void setEmpFirstName(String value) {
        setAttributeInternal(EMPFIRSTNAME, value);
    }

    /**
     * Gets the attribute value for EMP_LAST_NAME using the alias name EmpLastName.
     * @return the EMP_LAST_NAME
     */
    public String getEmpLastName() {
        return (String) getAttributeInternal(EMPLASTNAME);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_LAST_NAME using the alias name EmpLastName.
     * @param value value to set the EMP_LAST_NAME
     */
    public void setEmpLastName(String value) {
        setAttributeInternal(EMPLASTNAME, value);
    }

    /**
     * Gets the attribute value for EMP_NUMBER using the alias name EmpNumber.
     * @return the EMP_NUMBER
     */
    public String getEmpNumber() {
        return (String) getAttributeInternal(EMPNUMBER);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_NUMBER using the alias name EmpNumber.
     * @param value value to set the EMP_NUMBER
     */
    public void setEmpNumber(String value) {
        setAttributeInternal(EMPNUMBER, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber1.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber1() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER1);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber1.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber1(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER1, value);
    }

    /**
     * Gets the attribute value for the calculated attribute HoverDateTrans.
     * @return the HoverDateTrans
     */
    public Timestamp getHoverDateTrans() 
    {
        if(getAttributeInternal(HOVERDATETRANS)==null && getPmScHeAmendNo()!=null)
        {
        MtmTransactionAMImpl MtmAM=(MtmTransactionAMImpl) getApplicationModule();
        System.out.println("Get Row Count Before=>"+MtmAM.getActualPmAmcMcHandoverVO1().getEstimatedRowCount());
        ViewCriteria hVc=MtmAM.getActualPmAmcMcHandoverVO1().getViewCriteria("MtmTransactionAMImpl");
        MtmAM.getActualPmAmcMcHandoverVO1().applyViewCriteria(hVc, false);
        MtmAM.getActualPmAmcMcHandoverVO1().setNamedWhereClauseParam("bindAmdNo", getPmScHeAmendNo());
        MtmAM.getActualPmAmcMcHandoverVO1().setNamedWhereClauseParam("bindMachineCode", getMLMachineCode());
        MtmAM.getActualPmAmcMcHandoverVO1().setNamedWhereClauseParam("bindPmAmcType", getPmType());
        MtmAM.getActualPmAmcMcHandoverVO1().setNamedWhereClauseParam("bindScheduleNo",getPmScHeScheduleNo());
        MtmAM.getActualPmAmcMcHandoverVO1().executeQuery();
        System.out.println("Get Row Count After=>"+MtmAM.getActualPmAmcMcHandoverVO1().getEstimatedRowCount());
        Row r[]=MtmAM.getActualPmAmcMcHandoverVO1().getAllRowsInRange();
        if(r.length>0)
        {
            //Timestamp ts=null;
            System.out.println("Hover Date=> "+r[0].getAttribute("HoverDate")+"\nHover Time=>"+r[0].getAttribute("HoverTime")+"\nRemarks=> "+r[0].getAttribute("HoverTime")+"\nRec By=>"+r[0].getAttribute("RcvBy"));
            //ts=(Timestamp) r[0].getAttribute("HoverDate");
          //  return ts; 
             setAttributeInternal(HOVERDATETRANS,r[0].getAttribute("HoverDate")); 
             setAttributeInternal(HOVERTIMETRANS,r[0].getAttribute("HoverTime"));
            setAttributeInternal(REMARKSTRANS,r[0].getAttribute("Remarks"));
            setAttributeInternal(RCVBYTRANS,r[0].getAttribute("RcvBy"));
        }
        }
        
     return (Timestamp) getAttributeInternal(HOVERDATETRANS);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute HoverDateTrans.
     * @param value value to set the  HoverDateTrans
     */
    public void setHoverDateTrans(Timestamp value) {
        setAttributeInternal(HOVERDATETRANS, value);
    }

    /**
     * Gets the attribute value for the calculated attribute HoverTimeTrans.
     * @return the HoverTimeTrans
     */
    public Timestamp getHoverTimeTrans() {
        return (Timestamp) getAttributeInternal(HOVERTIMETRANS);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute HoverTimeTrans.
     * @param value value to set the  HoverTimeTrans
     */
    public void setHoverTimeTrans(Timestamp value) {
        setAttributeInternal(HOVERTIMETRANS, value);
    }

    /**
     * Gets the attribute value for the calculated attribute RemarksTrans.
     * @return the RemarksTrans
     */
    public String getRemarksTrans() {
        return (String) getAttributeInternal(REMARKSTRANS);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute RemarksTrans.
     * @param value value to set the  RemarksTrans
     */
    public void setRemarksTrans(String value) {
        setAttributeInternal(REMARKSTRANS, value);
    }

    /**
     * Gets the attribute value for the calculated attribute RcvByTrans.
     * @return the RcvByTrans
     */
    public String getRcvByTrans() {
        return (String) getAttributeInternal(RCVBYTRANS);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute RcvByTrans.
     * @param value value to set the  RcvByTrans
     */
    public void setRcvByTrans(String value) {
        setAttributeInternal(RCVBYTRANS, value);
    }

    /**
     * Gets the attribute value for the calculated attribute DescriptionTrans.
     * @return the DescriptionTrans
     */
    public String getDescriptionTrans()
    {
        String RecvBy="";
        System.out.println("Rec By "+getRcvByTrans());
        if(getAttributeInternal(DESCRIPTIONTRANS)==null && getRcvByTrans()!=null)
        {
        Row[] rows = getEmpViewVVO1().getFilteredRows("Eno", getRcvByTrans());
            if(rows.length>0)
            {
                setAttributeInternal(DESCRIPTIONTRANS, rows[0].getAttribute("Ename"));
//            RecvBy = rows[0].getAttribute("Ename").toString();
//            return RecvBy; 
            } 
           
        }
        return (String) getAttributeInternal(DESCRIPTIONTRANS);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute DescriptionTrans.
     * @param value value to set the  DescriptionTrans
     */
    public void setDescriptionTrans(String value) {
        setAttributeInternal(DESCRIPTIONTRANS, value);
    }

    /**
     * Gets the attribute value for UNIT_CD using the alias name UnitCd.
     * @return the UNIT_CD
     */
    public String getUnitCd() {
        return (String) getAttributeInternal(UNITCD);
    }

    /**
     * Sets <code>value</code> as attribute value for UNIT_CD using the alias name UnitCd.
     * @param value value to set the UNIT_CD
     */
    public void setUnitCd(String value) {
        setAttributeInternal(UNITCD, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber2.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber2() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER2);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber2.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber2(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER2, value);
    }

    /**
     * Gets the attribute value for SCHEDULE_ID using the alias name ScheduleId1.
     * @return the SCHEDULE_ID
     */
    public Long getScheduleId1() {
        return (Long) getAttributeInternal(SCHEDULEID1);
    }

    /**
     * Sets <code>value</code> as attribute value for SCHEDULE_ID using the alias name ScheduleId1.
     * @param value value to set the SCHEDULE_ID
     */
    public void setScheduleId1(Long value) {
        setAttributeInternal(SCHEDULEID1, value);
    }

    /**
     * Gets the attribute value for TAG_NO using the alias name TagNo.
     * @return the TAG_NO
     */
    public String getTagNo() {
        return (String) getAttributeInternal(TAGNO);
    }

    /**
     * Sets <code>value</code> as attribute value for TAG_NO using the alias name TagNo.
     * @param value value to set the TAG_NO
     */
    public void setTagNo(String value) {
        setAttributeInternal(TAGNO, value);
    }

    /**
     * Gets the attribute value for C_M_CODE using the alias name CMCode.
     * @return the C_M_CODE
     */
    public String getCMCode() {
        return (String) getAttributeInternal(CMCODE);
    }

    /**
     * Sets <code>value</code> as attribute value for C_M_CODE using the alias name CMCode.
     * @param value value to set the C_M_CODE
     */
    public void setCMCode(String value) {
        setAttributeInternal(CMCODE, value);
    }

    /**
     * Gets the attribute value for CORREC_ACTION using the alias name CorrecAction.
     * @return the CORREC_ACTION
     */
    public String getCorrecAction() {
        return (String) getAttributeInternal(CORRECACTION);
    }

    /**
     * Sets <code>value</code> as attribute value for CORREC_ACTION using the alias name CorrecAction.
     * @param value value to set the CORREC_ACTION
     */
    public void setCorrecAction(String value) {
        setAttributeInternal(CORRECACTION, value);
    }

    /**
     * Gets the attribute value for OBSERVATION using the alias name Observation.
     * @return the OBSERVATION
     */
    public String getObservation() {
        return (String) getAttributeInternal(OBSERVATION);
    }

    /**
     * Sets <code>value</code> as attribute value for OBSERVATION using the alias name Observation.
     * @param value value to set the OBSERVATION
     */
    public void setObservation(String value) {
        setAttributeInternal(OBSERVATION, value);
    }

    /**
     * Gets the attribute value for OKAY_YN using the alias name OkayYn.
     * @return the OKAY_YN
     */
    public String getOkayYn() {
        return (String) getAttributeInternal(OKAYYN);
    }

    /**
     * Sets <code>value</code> as attribute value for OKAY_YN using the alias name OkayYn.
     * @param value value to set the OKAY_YN
     */
    public void setOkayYn(String value) {
        setAttributeInternal(OKAYYN, value);
    }


    /**
     * Gets the attribute value for C_M_DESC using the alias name CMDesc.
     * @return the C_M_DESC
     */
    public String getCMDesc() {
        return (String) getAttributeInternal(CMDESC);
    }

    /**
     * Sets <code>value</code> as attribute value for C_M_DESC using the alias name CMDesc.
     * @param value value to set the C_M_DESC
     */
    public void setCMDesc(String value) {
        setAttributeInternal(CMDESC, value);
    }

    /**
     * Gets the attribute value for CHK_ID using the alias name ChkId.
     * @return the CHK_ID
     */
    public Long getChkId() {
        return (Long) getAttributeInternal(CHKID);
    }

    /**
     * Sets <code>value</code> as attribute value for CHK_ID using the alias name ChkId.
     * @param value value to set the CHK_ID
     */
    public void setChkId(Long value) {
        setAttributeInternal(CHKID, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber3.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber3() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER3);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber3.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber3(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER3, value);
    }

    /**
     * Gets the associated <code>RowIterator</code> using master-detail link ActualPmAmcMcHandoverVO.
     */
    public RowIterator getActualPmAmcMcHandoverVO() {
        return (RowIterator) getAttributeInternal(ACTUALPMAMCMCHANDOVERVO);
    }

    /**
     * Gets the associated <code>RowIterator</code> using master-detail link ActualPmAmcMachineActualPmCheckPointVO.
     */
    public RowIterator getActualPmAmcMachineActualPmCheckPointVO() {
        return (RowIterator) getAttributeInternal(ACTUALPMAMCMACHINEACTUALPMCHECKPOINTVO);
    }

    /**
     * Gets the associated <code>RowIterator</code> using master-detail link ActualPmAmcMachineActualPmItemConsumptionVO.
     */
    public RowIterator getActualPmAmcMachineActualPmItemConsumptionVO() {
        return (RowIterator) getAttributeInternal(ACTUALPMAMCMACHINEACTUALPMITEMCONSUMPTIONVO);
    }

    /**
     * Gets the associated <code>RowIterator</code> using master-detail link ActualPmAmcPmActualVO.
     */
    public RowIterator getActualPmAmcPmActualVO() {
        return (RowIterator) getAttributeInternal(ACTUALPMAMCPMACTUALVO);
    }


    /**
     * Gets the view accessor <code>RowSet</code> SecControlVO1.
     */
    public RowSet getSecControlVO1() {
        return (RowSet) getAttributeInternal(SECCONTROLVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> EmpViewVVO1.
     */
    public RowSet getEmpViewVVO1() {
        return (RowSet) getAttributeInternal(EMPVIEWVVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> MachineMasterVO1.
     */
    public RowSet getMachineMasterVO1() {
        return (RowSet) getAttributeInternal(MACHINEMASTERVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> SecControlVO2.
     */
    public RowSet getSecControlVO2() {
        return (RowSet) getAttributeInternal(SECCONTROLVO2);
    }


}

