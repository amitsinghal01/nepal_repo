package terms.mtm.transaction.model.view;

import java.math.BigDecimal;

import java.sql.ResultSet;

import oracle.jbo.Row;
import oracle.jbo.RowIterator;
import oracle.jbo.RowSet;
import oracle.jbo.domain.Timestamp;
import oracle.jbo.server.EntityImpl;
import oracle.jbo.server.ViewRowImpl;

import terms.mtm.setup.model.applicationModule.MtmSetupAMImpl;
import terms.mtm.transaction.model.applicationModule.MtmTransactionAMImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Mon Jan 15 10:52:00 IST 2018
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class ActualPmAmcPmActualVORowImpl extends ViewRowImpl {


    public static final int ENTITY_ACTUALPMAMCPMACTUALEO = 0;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        CreatedBy,
        CreationDate,
        EndDate,
        LastPmAmcDate,
        LastUpdateDate,
        LastUpdatedBy,
        NoOfHrsElec,
        NoOfHrsMech,
        ObjectVersionNumber,
        PSDMLMachineCode,
        PSDPmScHeAmendNo,
        PSDPmScHeScheduleNo,
        PSDPmType,
        PmActualId,
        PmActualLineId,
        PmAmcRunHrs,
        PmDoneBy,
        PmDoneIV,
        PrvAmcDate,
        PrvAmcRunHrs,
        PrvPmDate,
        PrvPmRunHrs,
        Remarks,
        StartDate,
        TransSwitch,
        PmDoneByDesTrans,
        UnitCodeTrans,
        SlipReason,
        CorrAction,
        PmDoneIVSecControlVO1,
        EmpViewVVO1,
        VendorRegisteredVVO1;
        static AttributesEnum[] vals = null;
        ;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int CREATEDBY = AttributesEnum.CreatedBy.index();
    public static final int CREATIONDATE = AttributesEnum.CreationDate.index();
    public static final int ENDDATE = AttributesEnum.EndDate.index();
    public static final int LASTPMAMCDATE = AttributesEnum.LastPmAmcDate.index();
    public static final int LASTUPDATEDATE = AttributesEnum.LastUpdateDate.index();
    public static final int LASTUPDATEDBY = AttributesEnum.LastUpdatedBy.index();
    public static final int NOOFHRSELEC = AttributesEnum.NoOfHrsElec.index();
    public static final int NOOFHRSMECH = AttributesEnum.NoOfHrsMech.index();
    public static final int OBJECTVERSIONNUMBER = AttributesEnum.ObjectVersionNumber.index();
    public static final int PSDMLMACHINECODE = AttributesEnum.PSDMLMachineCode.index();
    public static final int PSDPMSCHEAMENDNO = AttributesEnum.PSDPmScHeAmendNo.index();
    public static final int PSDPMSCHESCHEDULENO = AttributesEnum.PSDPmScHeScheduleNo.index();
    public static final int PSDPMTYPE = AttributesEnum.PSDPmType.index();
    public static final int PMACTUALID = AttributesEnum.PmActualId.index();
    public static final int PMACTUALLINEID = AttributesEnum.PmActualLineId.index();
    public static final int PMAMCRUNHRS = AttributesEnum.PmAmcRunHrs.index();
    public static final int PMDONEBY = AttributesEnum.PmDoneBy.index();
    public static final int PMDONEIV = AttributesEnum.PmDoneIV.index();
    public static final int PRVAMCDATE = AttributesEnum.PrvAmcDate.index();
    public static final int PRVAMCRUNHRS = AttributesEnum.PrvAmcRunHrs.index();
    public static final int PRVPMDATE = AttributesEnum.PrvPmDate.index();
    public static final int PRVPMRUNHRS = AttributesEnum.PrvPmRunHrs.index();
    public static final int REMARKS = AttributesEnum.Remarks.index();
    public static final int STARTDATE = AttributesEnum.StartDate.index();
    public static final int TRANSSWITCH = AttributesEnum.TransSwitch.index();
    public static final int PMDONEBYDESTRANS = AttributesEnum.PmDoneByDesTrans.index();
    public static final int UNITCODETRANS = AttributesEnum.UnitCodeTrans.index();
    public static final int SLIPREASON = AttributesEnum.SlipReason.index();
    public static final int CORRACTION = AttributesEnum.CorrAction.index();
    public static final int PMDONEIVSECCONTROLVO1 = AttributesEnum.PmDoneIVSecControlVO1.index();
    public static final int EMPVIEWVVO1 = AttributesEnum.EmpViewVVO1.index();
    public static final int VENDORREGISTEREDVVO1 = AttributesEnum.VendorRegisteredVVO1.index();

    /**
     * This is the default constructor (do not remove).
     */
    public ActualPmAmcPmActualVORowImpl() {
    }

    /**
     * Gets ActualPmAmcPmActualEO entity object.
     * @return the ActualPmAmcPmActualEO
     */
    public EntityImpl getActualPmAmcPmActualEO() {
        return (EntityImpl) getEntity(ENTITY_ACTUALPMAMCPMACTUALEO);
    }


    /**
     * Gets the attribute value for CREATED_BY using the alias name CreatedBy.
     * @return the CREATED_BY
     */
    public String getCreatedBy() {
        return (String) getAttributeInternal(CREATEDBY);
    }

    /**
     * Gets the attribute value for CREATION_DATE using the alias name CreationDate.
     * @return the CREATION_DATE
     */
    public Timestamp getCreationDate() {
        return (Timestamp) getAttributeInternal(CREATIONDATE);
    }

    /**
     * Gets the attribute value for END_DATE using the alias name EndDate.
     * @return the END_DATE
     */
    public Timestamp getEndDate() {
        return (Timestamp) getAttributeInternal(ENDDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for END_DATE using the alias name EndDate.
     * @param value value to set the END_DATE
     */
    public void setEndDate(Timestamp value) {
        setAttributeInternal(ENDDATE, value);
    }

    /**
     * Gets the attribute value for LAST_PM_AMC_DATE using the alias name LastPmAmcDate.
     * @return the LAST_PM_AMC_DATE
     */
    public Timestamp getLastPmAmcDate() {
        return (Timestamp) getAttributeInternal(LASTPMAMCDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for LAST_PM_AMC_DATE using the alias name LastPmAmcDate.
     * @param value value to set the LAST_PM_AMC_DATE
     */
    public void setLastPmAmcDate(Timestamp value) {
        setAttributeInternal(LASTPMAMCDATE, value);
    }

    /**
     * Gets the attribute value for LAST_UPDATE_DATE using the alias name LastUpdateDate.
     * @return the LAST_UPDATE_DATE
     */
    public Timestamp getLastUpdateDate() {
        return (Timestamp) getAttributeInternal(LASTUPDATEDATE);
    }

    /**
     * Gets the attribute value for LAST_UPDATED_BY using the alias name LastUpdatedBy.
     * @return the LAST_UPDATED_BY
     */
    public String getLastUpdatedBy() {
        return (String) getAttributeInternal(LASTUPDATEDBY);
    }

    /**
     * Gets the attribute value for NO_OF_HRS_ELEC using the alias name NoOfHrsElec.
     * @return the NO_OF_HRS_ELEC
     */
    public BigDecimal getNoOfHrsElec() {
        return (BigDecimal) getAttributeInternal(NOOFHRSELEC);
    }

    /**
     * Sets <code>value</code> as attribute value for NO_OF_HRS_ELEC using the alias name NoOfHrsElec.
     * @param value value to set the NO_OF_HRS_ELEC
     */
    public void setNoOfHrsElec(BigDecimal value) {
        setAttributeInternal(NOOFHRSELEC, value);
    }

    /**
     * Gets the attribute value for NO_OF_HRS_MECH using the alias name NoOfHrsMech.
     * @return the NO_OF_HRS_MECH
     */
    public BigDecimal getNoOfHrsMech() {
        return (BigDecimal) getAttributeInternal(NOOFHRSMECH);
    }

    /**
     * Sets <code>value</code> as attribute value for NO_OF_HRS_MECH using the alias name NoOfHrsMech.
     * @param value value to set the NO_OF_HRS_MECH
     */
    public void setNoOfHrsMech(BigDecimal value) {
        setAttributeInternal(NOOFHRSMECH, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER);
    }

    /**
     * Gets the attribute value for P_S_D_M_L_MACHINE_CODE using the alias name PSDMLMachineCode.
     * @return the P_S_D_M_L_MACHINE_CODE
     */
    public String getPSDMLMachineCode() {
        return (String) getAttributeInternal(PSDMLMACHINECODE);
    }

    /**
     * Sets <code>value</code> as attribute value for P_S_D_M_L_MACHINE_CODE using the alias name PSDMLMachineCode.
     * @param value value to set the P_S_D_M_L_MACHINE_CODE
     */
    public void setPSDMLMachineCode(String value) {
        setAttributeInternal(PSDMLMACHINECODE, value);
    }

    /**
     * Gets the attribute value for P_S_D_PM_SC_HE_AMEND_NO using the alias name PSDPmScHeAmendNo.
     * @return the P_S_D_PM_SC_HE_AMEND_NO
     */
    public Integer getPSDPmScHeAmendNo() {
        return (Integer) getAttributeInternal(PSDPMSCHEAMENDNO);
    }

    /**
     * Sets <code>value</code> as attribute value for P_S_D_PM_SC_HE_AMEND_NO using the alias name PSDPmScHeAmendNo.
     * @param value value to set the P_S_D_PM_SC_HE_AMEND_NO
     */
    public void setPSDPmScHeAmendNo(Integer value) {
        setAttributeInternal(PSDPMSCHEAMENDNO, value);
    }

    /**
     * Gets the attribute value for P_S_D_PM_SC_HE_SCHEDULE_NO using the alias name PSDPmScHeScheduleNo.
     * @return the P_S_D_PM_SC_HE_SCHEDULE_NO
     */
    public Integer getPSDPmScHeScheduleNo() {
        return (Integer) getAttributeInternal(PSDPMSCHESCHEDULENO);
    }

    /**
     * Sets <code>value</code> as attribute value for P_S_D_PM_SC_HE_SCHEDULE_NO using the alias name PSDPmScHeScheduleNo.
     * @param value value to set the P_S_D_PM_SC_HE_SCHEDULE_NO
     */
    public void setPSDPmScHeScheduleNo(Integer value) {
        setAttributeInternal(PSDPMSCHESCHEDULENO, value);
    }

    /**
     * Gets the attribute value for P_S_D_PM_TYPE using the alias name PSDPmType.
     * @return the P_S_D_PM_TYPE
     */
    public String getPSDPmType() {
        return (String) getAttributeInternal(PSDPMTYPE);
    }

    /**
     * Sets <code>value</code> as attribute value for P_S_D_PM_TYPE using the alias name PSDPmType.
     * @param value value to set the P_S_D_PM_TYPE
     */
    public void setPSDPmType(String value) {
        setAttributeInternal(PSDPMTYPE, value);
    }

    /**
     * Gets the attribute value for PM_ACTUAL_ID using the alias name PmActualId.
     * @return the PM_ACTUAL_ID
     */
    public Long getPmActualId() {
        return (Long) getAttributeInternal(PMACTUALID);
    }

    /**
     * Sets <code>value</code> as attribute value for PM_ACTUAL_ID using the alias name PmActualId.
     * @param value value to set the PM_ACTUAL_ID
     */
    public void setPmActualId(Long value) {
        setAttributeInternal(PMACTUALID, value);
    }

    /**
     * Gets the attribute value for PM_ACTUAL_LINE_ID using the alias name PmActualLineId.
     * @return the PM_ACTUAL_LINE_ID
     */
    public Long getPmActualLineId() {
        return (Long) getAttributeInternal(PMACTUALLINEID);
    }

    /**
     * Sets <code>value</code> as attribute value for PM_ACTUAL_LINE_ID using the alias name PmActualLineId.
     * @param value value to set the PM_ACTUAL_LINE_ID
     */
    public void setPmActualLineId(Long value) {
        setAttributeInternal(PMACTUALLINEID, value);
    }

    /**
     * Gets the attribute value for PM_AMC_RUN_HRS using the alias name PmAmcRunHrs.
     * @return the PM_AMC_RUN_HRS
     */
    public BigDecimal getPmAmcRunHrs() {
        return (BigDecimal) getAttributeInternal(PMAMCRUNHRS);
    }

    /**
     * Sets <code>value</code> as attribute value for PM_AMC_RUN_HRS using the alias name PmAmcRunHrs.
     * @param value value to set the PM_AMC_RUN_HRS
     */
    public void setPmAmcRunHrs(BigDecimal value) {
        setAttributeInternal(PMAMCRUNHRS, value);
    }

    /**
     * Gets the attribute value for PM_DONE_BY using the alias name PmDoneBy.
     * @return the PM_DONE_BY
     */
    public String getPmDoneBy() {
        return (String) getAttributeInternal(PMDONEBY);
    }

    /**
     * Sets <code>value</code> as attribute value for PM_DONE_BY using the alias name PmDoneBy.
     * @param value value to set the PM_DONE_BY
     */
    public void setPmDoneBy(String value) {
        setAttributeInternal(PMDONEBY, value);
    }

    /**
     * Gets the attribute value for PM_DONE_I_V using the alias name PmDoneIV.
     * @return the PM_DONE_I_V
     */
    public String getPmDoneIV() {
        return (String) getAttributeInternal(PMDONEIV);
    }

    /**
     * Sets <code>value</code> as attribute value for PM_DONE_I_V using the alias name PmDoneIV.
     * @param value value to set the PM_DONE_I_V
     */
    public void setPmDoneIV(String value) {
        setAttributeInternal(PMDONEIV, value);
    }

    /**
     * Gets the attribute value for PRV_AMC_DATE using the alias name PrvAmcDate.
     * @return the PRV_AMC_DATE
     */
    public Timestamp getPrvAmcDate() {
        return (Timestamp) getAttributeInternal(PRVAMCDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for PRV_AMC_DATE using the alias name PrvAmcDate.
     * @param value value to set the PRV_AMC_DATE
     */
    public void setPrvAmcDate(Timestamp value) {
        setAttributeInternal(PRVAMCDATE, value);
    }

    /**
     * Gets the attribute value for PRV_AMC_RUN_HRS using the alias name PrvAmcRunHrs.
     * @return the PRV_AMC_RUN_HRS
     */
    public BigDecimal getPrvAmcRunHrs() {
        return (BigDecimal) getAttributeInternal(PRVAMCRUNHRS);
    }

    /**
     * Sets <code>value</code> as attribute value for PRV_AMC_RUN_HRS using the alias name PrvAmcRunHrs.
     * @param value value to set the PRV_AMC_RUN_HRS
     */
    public void setPrvAmcRunHrs(BigDecimal value) {
        setAttributeInternal(PRVAMCRUNHRS, value);
    }

    /**
     * Gets the attribute value for PRV_PM_DATE using the alias name PrvPmDate.
     * @return the PRV_PM_DATE
     */
    public Timestamp getPrvPmDate() {
        return (Timestamp) getAttributeInternal(PRVPMDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for PRV_PM_DATE using the alias name PrvPmDate.
     * @param value value to set the PRV_PM_DATE
     */
    public void setPrvPmDate(Timestamp value) {
        setAttributeInternal(PRVPMDATE, value);
    }

    /**
     * Gets the attribute value for PRV_PM_RUN_HRS using the alias name PrvPmRunHrs.
     * @return the PRV_PM_RUN_HRS
     */
    public BigDecimal getPrvPmRunHrs() {
        return (BigDecimal) getAttributeInternal(PRVPMRUNHRS);
    }

    /**
     * Sets <code>value</code> as attribute value for PRV_PM_RUN_HRS using the alias name PrvPmRunHrs.
     * @param value value to set the PRV_PM_RUN_HRS
     */
    public void setPrvPmRunHrs(BigDecimal value) {
        setAttributeInternal(PRVPMRUNHRS, value);
    }

    /**
     * Gets the attribute value for REMARKS using the alias name Remarks.
     * @return the REMARKS
     */
    public String getRemarks() {
        return (String) getAttributeInternal(REMARKS);
    }

    /**
     * Sets <code>value</code> as attribute value for REMARKS using the alias name Remarks.
     * @param value value to set the REMARKS
     */
    public void setRemarks(String value) {
        setAttributeInternal(REMARKS, value);
    }

    /**
     * Gets the attribute value for START_DATE using the alias name StartDate.
     * @return the START_DATE
     */
    public Timestamp getStartDate() {
        return (Timestamp) getAttributeInternal(STARTDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for START_DATE using the alias name StartDate.
     * @param value value to set the START_DATE
     */
    public void setStartDate(Timestamp value) {
        setAttributeInternal(STARTDATE, value);
    }

    /**
     * Gets the attribute value for the calculated attribute TransSwitch.
     * @return the TransSwitch
     */
    public String getTransSwitch() {
        return (String) getAttributeInternal(TRANSSWITCH);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute TransSwitch.
     * @param value value to set the  TransSwitch
     */
    public void setTransSwitch(String value) {
        setAttributeInternal(TRANSSWITCH, value);
    }

    /**
     * Gets the attribute value for the calculated attribute PmDoneByDesTrans.
     * @return the PmDoneByDesTrans
     */
    public String getPmDoneByDesTrans()
    {
        String PmDoneByDes="";
        if(getPmDoneBy() !=null && getAttributeInternal(PMDONEBYDESTRANS)==null)
        {
            if(getPmDoneIV().equalsIgnoreCase("I"))
            {
            Row[] rows = getEmpViewVVO1().getFilteredRows("Eno", getPmDoneBy());
                if(rows.length>0)
                {
                PmDoneByDes = rows[0].getAttribute("Ename").toString();
                } 
            }
            else if(getPmDoneIV().equalsIgnoreCase("V"))
            {
            Row[] rows = getVendorRegisteredVVO1().getFilteredRows("VendorCode", getPmDoneBy());
                if(rows.length>0)
                {
                PmDoneByDes = rows[0].getAttribute("Name").toString();
                }
            }
            return PmDoneByDes; 
        }
        
        return (String) getAttributeInternal(PMDONEBYDESTRANS);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute PmDoneByDesTrans.
     * @param value value to set the  PmDoneByDesTrans
     */
    public void setPmDoneByDesTrans(String value) {
        setAttributeInternal(PMDONEBYDESTRANS, value);
    }

    /**
     * Gets the attribute value for the calculated attribute UnitCodeTrans.
     * @return the UnitCodeTrans
     */
    public String getUnitCodeTrans()
    {
        String StrUnitCd="";
        MtmTransactionAMImpl MtmAM=(MtmTransactionAMImpl)getApplicationModule();
        if(MtmAM.getActualPmAmcMachineHeaderVO1().getCurrentRow().getAttribute("UnitCd")!=null)
        {
            StrUnitCd=MtmAM.getActualPmAmcMachineHeaderVO1().getCurrentRow().getAttribute("UnitCd").toString();
            return StrUnitCd; 
        }
        return (String) getAttributeInternal(UNITCODETRANS);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute UnitCodeTrans.
     * @param value value to set the  UnitCodeTrans
     */
    public void setUnitCodeTrans(String value) {
        setAttributeInternal(UNITCODETRANS, value);
    }


    /**
     * Gets the attribute value for SLIP_REASON using the alias name SlipReason.
     * @return the SLIP_REASON
     */
    public String getSlipReason() {
        return (String) getAttributeInternal(SLIPREASON);
    }

    /**
     * Sets <code>value</code> as attribute value for SLIP_REASON using the alias name SlipReason.
     * @param value value to set the SLIP_REASON
     */
    public void setSlipReason(String value) {
        setAttributeInternal(SLIPREASON, value);
    }

    /**
     * Gets the attribute value for CORR_ACTION using the alias name CorrAction.
     * @return the CORR_ACTION
     */
    public String getCorrAction() {
        return (String) getAttributeInternal(CORRACTION);
    }

    /**
     * Sets <code>value</code> as attribute value for CORR_ACTION using the alias name CorrAction.
     * @param value value to set the CORR_ACTION
     */
    public void setCorrAction(String value) {
        setAttributeInternal(CORRACTION, value);
    }


    /**
     * Gets the view accessor <code>RowSet</code> PmDoneIVSecControlVO1.
     */
    public RowSet getPmDoneIVSecControlVO1() {
        return (RowSet) getAttributeInternal(PMDONEIVSECCONTROLVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> EmpViewVVO1.
     */
    public RowSet getEmpViewVVO1() {
        return (RowSet) getAttributeInternal(EMPVIEWVVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> VendorRegisteredVVO1.
     */
    public RowSet getVendorRegisteredVVO1() {
        return (RowSet) getAttributeInternal(VENDORREGISTEREDVVO1);
    }
}

