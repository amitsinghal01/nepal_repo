package terms.mtm.transaction.model.view;

import oracle.jbo.RowIterator;
import oracle.jbo.RowSet;
import oracle.jbo.domain.Timestamp;
import oracle.jbo.server.EntityImpl;
import oracle.jbo.server.ViewRowImpl;

import terms.hrm.setup.model.entity.EmployeeMasterEOImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Tue Jan 16 16:07:42 IST 2018
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class MachineAcceptanceAfterPmAmcHandoverVORowImpl extends ViewRowImpl {
    public static final int ENTITY_MACHINEACCEPTANCEAFTERPMAMCH1 = 0;
    public static final int ENTITY_EMPLOYEEMASTEREO = 1;
    public static final int ENTITY_EMPLOYEEMASTEREO1 = 2;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        AmendNo,
        CreatedBy,
        CreationDate,
        HoverBy,
        HoverDate,
        HoverEntryDate,
        HoverEntryTime,
        HoverTime,
        LastUpdateDate,
        LastUpdatedBy,
        MachineCode,
        ObjectVersionNumber,
        PmAmcType,
        RcvBy,
        Remarks,
        ScheduleId,
        ScheduleLineId,
        ScheduleNo,
        EmpFirstName,
        EmpLastName,
        EmpNumber,
        ObjectVersionNumber1,
        EmpFirstName1,
        EmpLastName1,
        EmpNumber1,
        ObjectVersionNumber2,
        MachineAcceptanceAfterPmAmcActualVO,
        HandOverByEmpViewVVO1,
        ReceivedByEmpViewVVO1;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }
    public static final int AMENDNO = AttributesEnum.AmendNo.index();
    public static final int CREATEDBY = AttributesEnum.CreatedBy.index();
    public static final int CREATIONDATE = AttributesEnum.CreationDate.index();
    public static final int HOVERBY = AttributesEnum.HoverBy.index();
    public static final int HOVERDATE = AttributesEnum.HoverDate.index();
    public static final int HOVERENTRYDATE = AttributesEnum.HoverEntryDate.index();
    public static final int HOVERENTRYTIME = AttributesEnum.HoverEntryTime.index();
    public static final int HOVERTIME = AttributesEnum.HoverTime.index();
    public static final int LASTUPDATEDATE = AttributesEnum.LastUpdateDate.index();
    public static final int LASTUPDATEDBY = AttributesEnum.LastUpdatedBy.index();
    public static final int MACHINECODE = AttributesEnum.MachineCode.index();
    public static final int OBJECTVERSIONNUMBER = AttributesEnum.ObjectVersionNumber.index();
    public static final int PMAMCTYPE = AttributesEnum.PmAmcType.index();
    public static final int RCVBY = AttributesEnum.RcvBy.index();
    public static final int REMARKS = AttributesEnum.Remarks.index();
    public static final int SCHEDULEID = AttributesEnum.ScheduleId.index();
    public static final int SCHEDULELINEID = AttributesEnum.ScheduleLineId.index();
    public static final int SCHEDULENO = AttributesEnum.ScheduleNo.index();
    public static final int EMPFIRSTNAME = AttributesEnum.EmpFirstName.index();
    public static final int EMPLASTNAME = AttributesEnum.EmpLastName.index();
    public static final int EMPNUMBER = AttributesEnum.EmpNumber.index();
    public static final int OBJECTVERSIONNUMBER1 = AttributesEnum.ObjectVersionNumber1.index();
    public static final int EMPFIRSTNAME1 = AttributesEnum.EmpFirstName1.index();
    public static final int EMPLASTNAME1 = AttributesEnum.EmpLastName1.index();
    public static final int EMPNUMBER1 = AttributesEnum.EmpNumber1.index();
    public static final int OBJECTVERSIONNUMBER2 = AttributesEnum.ObjectVersionNumber2.index();
    public static final int MACHINEACCEPTANCEAFTERPMAMCACTUALVO =
        AttributesEnum.MachineAcceptanceAfterPmAmcActualVO.index();
    public static final int HANDOVERBYEMPVIEWVVO1 = AttributesEnum.HandOverByEmpViewVVO1.index();
    public static final int RECEIVEDBYEMPVIEWVVO1 = AttributesEnum.ReceivedByEmpViewVVO1.index();

    /**
     * This is the default constructor (do not remove).
     */
    public MachineAcceptanceAfterPmAmcHandoverVORowImpl() {
    }

    /**
     * Gets MachineAcceptanceAfterPmAmcH1 entity object.
     * @return the MachineAcceptanceAfterPmAmcH1
     */
    public EntityImpl getMachineAcceptanceAfterPmAmcH1() {
        return (EntityImpl) getEntity(ENTITY_MACHINEACCEPTANCEAFTERPMAMCH1);
    }

    /**
     * Gets EmployeeMasterEO entity object.
     * @return the EmployeeMasterEO
     */
    public EmployeeMasterEOImpl getEmployeeMasterEO() {
        return (EmployeeMasterEOImpl) getEntity(ENTITY_EMPLOYEEMASTEREO);
    }

    /**
     * Gets EmployeeMasterEO1 entity object.
     * @return the EmployeeMasterEO1
     */
    public EmployeeMasterEOImpl getEmployeeMasterEO1() {
        return (EmployeeMasterEOImpl) getEntity(ENTITY_EMPLOYEEMASTEREO1);
    }

    /**
     * Gets the attribute value for AMEND_NO using the alias name AmendNo.
     * @return the AMEND_NO
     */
    public Integer getAmendNo() {
        return (Integer) getAttributeInternal(AMENDNO);
    }

    /**
     * Sets <code>value</code> as attribute value for AMEND_NO using the alias name AmendNo.
     * @param value value to set the AMEND_NO
     */
    public void setAmendNo(Integer value) {
        setAttributeInternal(AMENDNO, value);
    }

    /**
     * Gets the attribute value for CREATED_BY using the alias name CreatedBy.
     * @return the CREATED_BY
     */
    public String getCreatedBy() {
        return (String) getAttributeInternal(CREATEDBY);
    }

    /**
     * Sets <code>value</code> as attribute value for CREATED_BY using the alias name CreatedBy.
     * @param value value to set the CREATED_BY
     */
    public void setCreatedBy(String value) {
        setAttributeInternal(CREATEDBY, value);
    }

    /**
     * Gets the attribute value for CREATION_DATE using the alias name CreationDate.
     * @return the CREATION_DATE
     */
    public Timestamp getCreationDate() {
        return (Timestamp) getAttributeInternal(CREATIONDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for CREATION_DATE using the alias name CreationDate.
     * @param value value to set the CREATION_DATE
     */
    public void setCreationDate(Timestamp value) {
        setAttributeInternal(CREATIONDATE, value);
    }

    /**
     * Gets the attribute value for HOVER_BY using the alias name HoverBy.
     * @return the HOVER_BY
     */
    public String getHoverBy() {
        return (String) getAttributeInternal(HOVERBY);
    }

    /**
     * Sets <code>value</code> as attribute value for HOVER_BY using the alias name HoverBy.
     * @param value value to set the HOVER_BY
     */
    public void setHoverBy(String value) {
        setAttributeInternal(HOVERBY, value);
    }

    /**
     * Gets the attribute value for HOVER_DATE using the alias name HoverDate.
     * @return the HOVER_DATE
     */
    public Timestamp getHoverDate() {
        return (Timestamp) getAttributeInternal(HOVERDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for HOVER_DATE using the alias name HoverDate.
     * @param value value to set the HOVER_DATE
     */
    public void setHoverDate(Timestamp value) {
        setAttributeInternal(HOVERDATE, value);
    }

    /**
     * Gets the attribute value for HOVER_ENTRY_DATE using the alias name HoverEntryDate.
     * @return the HOVER_ENTRY_DATE
     */
    public Timestamp getHoverEntryDate() {
        return (Timestamp) getAttributeInternal(HOVERENTRYDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for HOVER_ENTRY_DATE using the alias name HoverEntryDate.
     * @param value value to set the HOVER_ENTRY_DATE
     */
    public void setHoverEntryDate(Timestamp value) {
        setAttributeInternal(HOVERENTRYDATE, value);
    }

    /**
     * Gets the attribute value for HOVER_ENTRY_TIME using the alias name HoverEntryTime.
     * @return the HOVER_ENTRY_TIME
     */
    public Timestamp getHoverEntryTime() {
        return (Timestamp) getAttributeInternal(HOVERENTRYTIME);
    }

    /**
     * Sets <code>value</code> as attribute value for HOVER_ENTRY_TIME using the alias name HoverEntryTime.
     * @param value value to set the HOVER_ENTRY_TIME
     */
    public void setHoverEntryTime(Timestamp value) {
        setAttributeInternal(HOVERENTRYTIME, value);
    }

    /**
     * Gets the attribute value for HOVER_TIME using the alias name HoverTime.
     * @return the HOVER_TIME
     */
    public Timestamp getHoverTime() {
        return (Timestamp) getAttributeInternal(HOVERTIME);
    }

    /**
     * Sets <code>value</code> as attribute value for HOVER_TIME using the alias name HoverTime.
     * @param value value to set the HOVER_TIME
     */
    public void setHoverTime(Timestamp value) {
        setAttributeInternal(HOVERTIME, value);
    }

    /**
     * Gets the attribute value for LAST_UPDATE_DATE using the alias name LastUpdateDate.
     * @return the LAST_UPDATE_DATE
     */
    public Timestamp getLastUpdateDate() {
        return (Timestamp) getAttributeInternal(LASTUPDATEDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for LAST_UPDATE_DATE using the alias name LastUpdateDate.
     * @param value value to set the LAST_UPDATE_DATE
     */
    public void setLastUpdateDate(Timestamp value) {
        setAttributeInternal(LASTUPDATEDATE, value);
    }

    /**
     * Gets the attribute value for LAST_UPDATED_BY using the alias name LastUpdatedBy.
     * @return the LAST_UPDATED_BY
     */
    public String getLastUpdatedBy() {
        return (String) getAttributeInternal(LASTUPDATEDBY);
    }

    /**
     * Sets <code>value</code> as attribute value for LAST_UPDATED_BY using the alias name LastUpdatedBy.
     * @param value value to set the LAST_UPDATED_BY
     */
    public void setLastUpdatedBy(String value) {
        setAttributeInternal(LASTUPDATEDBY, value);
    }

    /**
     * Gets the attribute value for MACHINE_CODE using the alias name MachineCode.
     * @return the MACHINE_CODE
     */
    public String getMachineCode() {
        return (String) getAttributeInternal(MACHINECODE);
    }

    /**
     * Sets <code>value</code> as attribute value for MACHINE_CODE using the alias name MachineCode.
     * @param value value to set the MACHINE_CODE
     */
    public void setMachineCode(String value) {
        setAttributeInternal(MACHINECODE, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER, value);
    }

    /**
     * Gets the attribute value for PM_AMC_TYPE using the alias name PmAmcType.
     * @return the PM_AMC_TYPE
     */
    public String getPmAmcType() {
        return (String) getAttributeInternal(PMAMCTYPE);
    }

    /**
     * Sets <code>value</code> as attribute value for PM_AMC_TYPE using the alias name PmAmcType.
     * @param value value to set the PM_AMC_TYPE
     */
    public void setPmAmcType(String value) {
        setAttributeInternal(PMAMCTYPE, value);
    }

    /**
     * Gets the attribute value for RCV_BY using the alias name RcvBy.
     * @return the RCV_BY
     */
    public String getRcvBy() {
        return (String) getAttributeInternal(RCVBY);
    }

    /**
     * Sets <code>value</code> as attribute value for RCV_BY using the alias name RcvBy.
     * @param value value to set the RCV_BY
     */
    public void setRcvBy(String value) {
        setAttributeInternal(RCVBY, value);
    }

    /**
     * Gets the attribute value for REMARKS using the alias name Remarks.
     * @return the REMARKS
     */
    public String getRemarks() {
        return (String) getAttributeInternal(REMARKS);
    }

    /**
     * Sets <code>value</code> as attribute value for REMARKS using the alias name Remarks.
     * @param value value to set the REMARKS
     */
    public void setRemarks(String value) {
        setAttributeInternal(REMARKS, value);
    }

    /**
     * Gets the attribute value for SCHEDULE_ID using the alias name ScheduleId.
     * @return the SCHEDULE_ID
     */
    public Long getScheduleId() {
        return (Long) getAttributeInternal(SCHEDULEID);
    }

    /**
     * Sets <code>value</code> as attribute value for SCHEDULE_ID using the alias name ScheduleId.
     * @param value value to set the SCHEDULE_ID
     */
    public void setScheduleId(Long value) {
        setAttributeInternal(SCHEDULEID, value);
    }

    /**
     * Gets the attribute value for SCHEDULE_LINE_ID using the alias name ScheduleLineId.
     * @return the SCHEDULE_LINE_ID
     */
    public Long getScheduleLineId() {
        return (Long) getAttributeInternal(SCHEDULELINEID);
    }

    /**
     * Sets <code>value</code> as attribute value for SCHEDULE_LINE_ID using the alias name ScheduleLineId.
     * @param value value to set the SCHEDULE_LINE_ID
     */
    public void setScheduleLineId(Long value) {
        setAttributeInternal(SCHEDULELINEID, value);
    }

    /**
     * Gets the attribute value for SCHEDULE_NO using the alias name ScheduleNo.
     * @return the SCHEDULE_NO
     */
    public Integer getScheduleNo() {
        return (Integer) getAttributeInternal(SCHEDULENO);
    }

    /**
     * Sets <code>value</code> as attribute value for SCHEDULE_NO using the alias name ScheduleNo.
     * @param value value to set the SCHEDULE_NO
     */
    public void setScheduleNo(Integer value) {
        setAttributeInternal(SCHEDULENO, value);
    }

    /**
     * Gets the attribute value for EMP_FIRST_NAME using the alias name EmpFirstName.
     * @return the EMP_FIRST_NAME
     */
    public String getEmpFirstName() 
    {
        if ((getAttributeInternal(EMPFIRSTNAME) != null) && (getAttributeInternal(EMPLASTNAME) != null))
         {

            return (String) getAttributeInternal(EMPFIRSTNAME) + " " + getAttributeInternal(EMPLASTNAME);
        }
         else {
            return (String) getAttributeInternal(EMPFIRSTNAME);
        }

    //    return (String) getAttributeInternal(EMPFIRSTNAME);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_FIRST_NAME using the alias name EmpFirstName.
     * @param value value to set the EMP_FIRST_NAME
     */
    public void setEmpFirstName(String value) {
        setAttributeInternal(EMPFIRSTNAME, value);
    }

    /**
     * Gets the attribute value for EMP_LAST_NAME using the alias name EmpLastName.
     * @return the EMP_LAST_NAME
     */
    public String getEmpLastName() {
        return (String) getAttributeInternal(EMPLASTNAME);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_LAST_NAME using the alias name EmpLastName.
     * @param value value to set the EMP_LAST_NAME
     */
    public void setEmpLastName(String value) {
        setAttributeInternal(EMPLASTNAME, value);
    }

    /**
     * Gets the attribute value for EMP_NUMBER using the alias name EmpNumber.
     * @return the EMP_NUMBER
     */
    public String getEmpNumber() {
        return (String) getAttributeInternal(EMPNUMBER);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_NUMBER using the alias name EmpNumber.
     * @param value value to set the EMP_NUMBER
     */
    public void setEmpNumber(String value) {
        setAttributeInternal(EMPNUMBER, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber1.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber1() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER1);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber1.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber1(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER1, value);
    }

    /**
     * Gets the attribute value for EMP_FIRST_NAME using the alias name EmpFirstName1.
     * @return the EMP_FIRST_NAME
     */
    public String getEmpFirstName1()
    {
        if ((getAttributeInternal(EMPFIRSTNAME1) != null) && (getAttributeInternal(EMPFIRSTNAME1) != null))
         {

            return (String) getAttributeInternal(EMPFIRSTNAME1) + " " + getAttributeInternal(EMPFIRSTNAME1);
        }
         else {
            return (String) getAttributeInternal(EMPFIRSTNAME1);
        }

      //  return (String) getAttributeInternal(EMPFIRSTNAME1);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_FIRST_NAME using the alias name EmpFirstName1.
     * @param value value to set the EMP_FIRST_NAME
     */
    public void setEmpFirstName1(String value) {
        setAttributeInternal(EMPFIRSTNAME1, value);
    }

    /**
     * Gets the attribute value for EMP_LAST_NAME using the alias name EmpLastName1.
     * @return the EMP_LAST_NAME
     */
    public String getEmpLastName1() {
        return (String) getAttributeInternal(EMPLASTNAME1);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_LAST_NAME using the alias name EmpLastName1.
     * @param value value to set the EMP_LAST_NAME
     */
    public void setEmpLastName1(String value) {
        setAttributeInternal(EMPLASTNAME1, value);
    }

    /**
     * Gets the attribute value for EMP_NUMBER using the alias name EmpNumber1.
     * @return the EMP_NUMBER
     */
    public String getEmpNumber1() {
        return (String) getAttributeInternal(EMPNUMBER1);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_NUMBER using the alias name EmpNumber1.
     * @param value value to set the EMP_NUMBER
     */
    public void setEmpNumber1(String value) {
        setAttributeInternal(EMPNUMBER1, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber2.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber2() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER2);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber2.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber2(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER2, value);
    }

    /**
     * Gets the associated <code>RowIterator</code> using master-detail link MachineAcceptanceAfterPmAmcActualVO.
     */
    public RowIterator getMachineAcceptanceAfterPmAmcActualVO() {
        return (RowIterator) getAttributeInternal(MACHINEACCEPTANCEAFTERPMAMCACTUALVO);
    }

    /**
     * Gets the view accessor <code>RowSet</code> HandOverByEmpViewVVO1.
     */
    public RowSet getHandOverByEmpViewVVO1() {
        return (RowSet) getAttributeInternal(HANDOVERBYEMPVIEWVVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> ReceivedByEmpViewVVO1.
     */
    public RowSet getReceivedByEmpViewVVO1() {
        return (RowSet) getAttributeInternal(RECEIVEDBYEMPVIEWVVO1);
    }
}

