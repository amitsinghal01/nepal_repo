package terms.mtm.transaction.model.view;

import java.math.BigDecimal;

import java.sql.Timestamp;

import oracle.jbo.RowIterator;
import oracle.jbo.RowSet;
import oracle.jbo.domain.Date;
import oracle.jbo.server.EntityImpl;
import oracle.jbo.server.ViewRowImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Fri Jan 19 12:29:58 IST 2018
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class MachineHandoverForPmAmcHeaderVORowImpl extends ViewRowImpl {
    public static final int ENTITY_MACHINEHANDOVERFORPMAMCHEADE1 = 0;
    public static final int ENTITY_UNITEO = 1;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        AmendApprovBy,
        AmendNo,
        CreatedBy,
        CreationDate,
        HoursRunPercent,
        LastUpdateDate,
        LastUpdatedBy,
        ObjectVersionNumber,
        PeriodFrom,
        PeriodTo,
        ProposDt,
        ReasonOfAmend,
        ScheduleDate,
        ScheduleId,
        ScheduleNo,
        UnitCd,
        Name,
        Code,
        ObjectVersionNumber1,
        EditTrans,
        ScheduleNo1,
        MachineHandoverForPmAmcDetailVO,
        UnitVO1;
        static AttributesEnum[] vals = null;
        ;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int AMENDAPPROVBY = AttributesEnum.AmendApprovBy.index();
    public static final int AMENDNO = AttributesEnum.AmendNo.index();
    public static final int CREATEDBY = AttributesEnum.CreatedBy.index();
    public static final int CREATIONDATE = AttributesEnum.CreationDate.index();
    public static final int HOURSRUNPERCENT = AttributesEnum.HoursRunPercent.index();
    public static final int LASTUPDATEDATE = AttributesEnum.LastUpdateDate.index();
    public static final int LASTUPDATEDBY = AttributesEnum.LastUpdatedBy.index();
    public static final int OBJECTVERSIONNUMBER = AttributesEnum.ObjectVersionNumber.index();
    public static final int PERIODFROM = AttributesEnum.PeriodFrom.index();
    public static final int PERIODTO = AttributesEnum.PeriodTo.index();
    public static final int PROPOSDT = AttributesEnum.ProposDt.index();
    public static final int REASONOFAMEND = AttributesEnum.ReasonOfAmend.index();
    public static final int SCHEDULEDATE = AttributesEnum.ScheduleDate.index();
    public static final int SCHEDULEID = AttributesEnum.ScheduleId.index();
    public static final int SCHEDULENO = AttributesEnum.ScheduleNo.index();
    public static final int UNITCD = AttributesEnum.UnitCd.index();
    public static final int NAME = AttributesEnum.Name.index();
    public static final int CODE = AttributesEnum.Code.index();
    public static final int OBJECTVERSIONNUMBER1 = AttributesEnum.ObjectVersionNumber1.index();
    public static final int EDITTRANS = AttributesEnum.EditTrans.index();
    public static final int SCHEDULENO1 = AttributesEnum.ScheduleNo1.index();
    public static final int MACHINEHANDOVERFORPMAMCDETAILVO = AttributesEnum.MachineHandoverForPmAmcDetailVO.index();
    public static final int UNITVO1 = AttributesEnum.UnitVO1.index();

    /**
     * This is the default constructor (do not remove).
     */
    public MachineHandoverForPmAmcHeaderVORowImpl() {
    }

    /**
     * Gets MachineHandoverForPmAmcHeade1 entity object.
     * @return the MachineHandoverForPmAmcHeade1
     */
    public EntityImpl getMachineHandoverForPmAmcHeade1() {
        return (EntityImpl) getEntity(ENTITY_MACHINEHANDOVERFORPMAMCHEADE1);
    }

    /**
     * Gets UnitEO entity object.
     * @return the UnitEO
     */
    public EntityImpl getUnitEO() {
        return (EntityImpl) getEntity(ENTITY_UNITEO);
    }

    /**
     * Gets the attribute value for AMEND_APPROV_BY using the alias name AmendApprovBy.
     * @return the AMEND_APPROV_BY
     */
    public String getAmendApprovBy() {
        return (String) getAttributeInternal(AMENDAPPROVBY);
    }

    /**
     * Sets <code>value</code> as attribute value for AMEND_APPROV_BY using the alias name AmendApprovBy.
     * @param value value to set the AMEND_APPROV_BY
     */
    public void setAmendApprovBy(String value) {
        setAttributeInternal(AMENDAPPROVBY, value);
    }

    /**
     * Gets the attribute value for AMEND_NO using the alias name AmendNo.
     * @return the AMEND_NO
     */
    public Integer getAmendNo() {
        return (Integer) getAttributeInternal(AMENDNO);
    }

    /**
     * Sets <code>value</code> as attribute value for AMEND_NO using the alias name AmendNo.
     * @param value value to set the AMEND_NO
     */
    public void setAmendNo(Integer value) {
        setAttributeInternal(AMENDNO, value);
    }

    /**
     * Gets the attribute value for CREATED_BY using the alias name CreatedBy.
     * @return the CREATED_BY
     */
    public String getCreatedBy() {
        return (String) getAttributeInternal(CREATEDBY);
    }

    /**
     * Sets <code>value</code> as attribute value for CREATED_BY using the alias name CreatedBy.
     * @param value value to set the CREATED_BY
     */
    public void setCreatedBy(String value) {
        setAttributeInternal(CREATEDBY, value);
    }

    /**
     * Gets the attribute value for CREATION_DATE using the alias name CreationDate.
     * @return the CREATION_DATE
     */
    public Timestamp getCreationDate() {
        return (Timestamp) getAttributeInternal(CREATIONDATE);
    }


    /**
     * Gets the attribute value for HOURS_RUN_PERCENT using the alias name HoursRunPercent.
     * @return the HOURS_RUN_PERCENT
     */
    public BigDecimal getHoursRunPercent() {
        return (BigDecimal) getAttributeInternal(HOURSRUNPERCENT);
    }

    /**
     * Sets <code>value</code> as attribute value for HOURS_RUN_PERCENT using the alias name HoursRunPercent.
     * @param value value to set the HOURS_RUN_PERCENT
     */
    public void setHoursRunPercent(BigDecimal value) {
        setAttributeInternal(HOURSRUNPERCENT, value);
    }

    /**
     * Gets the attribute value for LAST_UPDATE_DATE using the alias name LastUpdateDate.
     * @return the LAST_UPDATE_DATE
     */
    public oracle.jbo.domain.Timestamp getLastUpdateDate() {
        return (oracle.jbo.domain.Timestamp) getAttributeInternal(LASTUPDATEDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for LAST_UPDATE_DATE using the alias name LastUpdateDate.
     * @param value value to set the LAST_UPDATE_DATE
     */
    public void setLastUpdateDate(oracle.jbo.domain.Timestamp value) {
        setAttributeInternal(LASTUPDATEDATE, value);
    }

    /**
     * Gets the attribute value for LAST_UPDATED_BY using the alias name LastUpdatedBy.
     * @return the LAST_UPDATED_BY
     */
    public String getLastUpdatedBy() {
        return (String) getAttributeInternal(LASTUPDATEDBY);
    }

    /**
     * Sets <code>value</code> as attribute value for LAST_UPDATED_BY using the alias name LastUpdatedBy.
     * @param value value to set the LAST_UPDATED_BY
     */
    public void setLastUpdatedBy(String value) {
        setAttributeInternal(LASTUPDATEDBY, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER, value);
    }

    /**
     * Gets the attribute value for PERIOD_FROM using the alias name PeriodFrom.
     * @return the PERIOD_FROM
     */
    public Timestamp getPeriodFrom() {
        return (Timestamp) getAttributeInternal(PERIODFROM);
    }

    /**
     * Sets <code>value</code> as attribute value for PERIOD_FROM using the alias name PeriodFrom.
     * @param value value to set the PERIOD_FROM
     */
    public void setPeriodFrom(Timestamp value) {
        setAttributeInternal(PERIODFROM, value);
    }

    /**
     * Gets the attribute value for PERIOD_TO using the alias name PeriodTo.
     * @return the PERIOD_TO
     */
    public Timestamp getPeriodTo() {
        return (Timestamp) getAttributeInternal(PERIODTO);
    }

    /**
     * Sets <code>value</code> as attribute value for PERIOD_TO using the alias name PeriodTo.
     * @param value value to set the PERIOD_TO
     */
    public void setPeriodTo(Timestamp value) {
        setAttributeInternal(PERIODTO, value);
    }

    /**
     * Gets the attribute value for PROPOS_DT using the alias name ProposDt.
     * @return the PROPOS_DT
     */
    public Date getProposDt() {
        return (Date) getAttributeInternal(PROPOSDT);
    }

    /**
     * Sets <code>value</code> as attribute value for PROPOS_DT using the alias name ProposDt.
     * @param value value to set the PROPOS_DT
     */
    public void setProposDt(Date value) {
        setAttributeInternal(PROPOSDT, value);
    }

    /**
     * Gets the attribute value for REASON_OF_AMEND using the alias name ReasonOfAmend.
     * @return the REASON_OF_AMEND
     */
    public String getReasonOfAmend() {
        return (String) getAttributeInternal(REASONOFAMEND);
    }

    /**
     * Sets <code>value</code> as attribute value for REASON_OF_AMEND using the alias name ReasonOfAmend.
     * @param value value to set the REASON_OF_AMEND
     */
    public void setReasonOfAmend(String value) {
        setAttributeInternal(REASONOFAMEND, value);
    }

    /**
     * Gets the attribute value for SCHEDULE_DATE using the alias name ScheduleDate.
     * @return the SCHEDULE_DATE
     */
    public Timestamp getScheduleDate() {
        return (Timestamp) getAttributeInternal(SCHEDULEDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for SCHEDULE_DATE using the alias name ScheduleDate.
     * @param value value to set the SCHEDULE_DATE
     */
    public void setScheduleDate(Timestamp value) {
        setAttributeInternal(SCHEDULEDATE, value);
    }

    /**
     * Gets the attribute value for SCHEDULE_ID using the alias name ScheduleId.
     * @return the SCHEDULE_ID
     */
    public Long getScheduleId() {
        return (Long) getAttributeInternal(SCHEDULEID);
    }

    /**
     * Sets <code>value</code> as attribute value for SCHEDULE_ID using the alias name ScheduleId.
     * @param value value to set the SCHEDULE_ID
     */
    public void setScheduleId(Long value) {
        setAttributeInternal(SCHEDULEID, value);
    }

    /**
     * Gets the attribute value for SCHEDULE_NO using the alias name ScheduleNo.
     * @return the SCHEDULE_NO
     */
    public Integer getScheduleNo() {
        return (Integer) getAttributeInternal(SCHEDULENO);
    }

    /**
     * Sets <code>value</code> as attribute value for SCHEDULE_NO using the alias name ScheduleNo.
     * @param value value to set the SCHEDULE_NO
     */
    public void setScheduleNo(Integer value) {
        setAttributeInternal(SCHEDULENO, value);
    }

    /**
     * Gets the attribute value for UNIT_CD using the alias name UnitCd.
     * @return the UNIT_CD
     */
    public String getUnitCd() {
        return (String) getAttributeInternal(UNITCD);
    }

    /**
     * Sets <code>value</code> as attribute value for UNIT_CD using the alias name UnitCd.
     * @param value value to set the UNIT_CD
     */
    public void setUnitCd(String value) {
        setAttributeInternal(UNITCD, value);
    }

    /**
     * Gets the attribute value for NAME using the alias name Name.
     * @return the NAME
     */
    public String getName() {
        return (String) getAttributeInternal(NAME);
    }

    /**
     * Sets <code>value</code> as attribute value for NAME using the alias name Name.
     * @param value value to set the NAME
     */
    public void setName(String value) {
        setAttributeInternal(NAME, value);
    }

    /**
     * Gets the attribute value for CODE using the alias name Code.
     * @return the CODE
     */
    public String getCode() {
        return (String) getAttributeInternal(CODE);
    }

    /**
     * Sets <code>value</code> as attribute value for CODE using the alias name Code.
     * @param value value to set the CODE
     */
    public void setCode(String value) {
        setAttributeInternal(CODE, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber1.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber1() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER1);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber1.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber1(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER1, value);
    }

    /**
     * Gets the attribute value for the calculated attribute EditTrans.
     * @return the EditTrans
     */
    public Integer getEditTrans() {
        byte entityState = this.getEntity(0).getEntityState();
             return new Integer(entityState);
       // return (Integer) getAttributeInternal(EDITTRANS);
    }

    /**
     * Gets the attribute value for SCHEDULE_NO using the alias name ScheduleNo1.
     * @return the SCHEDULE_NO
     */
    public Integer getScheduleNo1() {
        return (Integer) getAttributeInternal(SCHEDULENO1);
    }

    /**
     * Sets <code>value</code> as attribute value for SCHEDULE_NO using the alias name ScheduleNo1.
     * @param value value to set the SCHEDULE_NO
     */
    public void setScheduleNo1(Integer value) {
        setAttributeInternal(SCHEDULENO1, value);
    }

    /**
     * Gets the associated <code>RowIterator</code> using master-detail link MachineHandoverForPmAmcDetailVO.
     */
    public RowIterator getMachineHandoverForPmAmcDetailVO() {
        return (RowIterator) getAttributeInternal(MACHINEHANDOVERFORPMAMCDETAILVO);
    }

    /**
     * Gets the view accessor <code>RowSet</code> UnitVO1.
     */
    public RowSet getUnitVO1() {
        return (RowSet) getAttributeInternal(UNITVO1);
    }
}

