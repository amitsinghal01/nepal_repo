package terms.mtm.setup.ui.bean;

import java.math.BigDecimal;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;

import terms.mtm.setup.model.view.CheckPtCodeMasterVORowImpl;

public class CheckPointCodeBean {
    private RichTable tableChkPtMstBinding;
    private String editAction="V";
    private RichOutputText chkPtCdBinding;

    public CheckPointCodeBean() {
    }

    public void setTableChkPtMstBinding(RichTable tableChkPtMstBinding) {
        this.tableChkPtMstBinding = tableChkPtMstBinding;
    }

    public RichTable getTableChkPtMstBinding() {
        return tableChkPtMstBinding;
        
    }
    
    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    
    }

    public void deletePopUpDl(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok")){
                OperationBinding op = null;
                ADFUtils.findOperation("Delete").execute();
                    op = (OperationBinding) ADFUtils.findOperation("Commit");
        if (getEditAction().equals("V")) {
                Object rst = op.execute();
        }
                    System.out.println("Record Delete Successfully");
        if(op.getErrors().isEmpty()){
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);
        } else if (!op.getErrors().isEmpty()){
                OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
                         Object rstr = opr.execute();
                FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                             Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc = FacesContext.getCurrentInstance();
                             fc.addMessage(null, Message);
            }
        }
                        AdfFacesContext.getCurrentInstance().addPartialTarget(tableChkPtMstBinding);
    }

//    private BigDecimal fetchMaxLineNumber(DCIteratorBinding itr) {
//           System.out.println("####################In fetchmaxline number###########################");
//           BigDecimal max = new BigDecimal(0);
//           System.out.println("Starting Max value:" + max);
//           CheckPtCodeMasterVORowImpl currRow = (CheckPtCodeMasterVORowImpl) itr.getCurrentRow();
//           if (currRow != null && currRow.getChkptCd() != null)
//               max = currRow.getChkptCd();
//           System.out.println("Max Value after setting currentseqno:" + max);
//           RowSetIterator rsi = itr.getRowSetIterator();
//           if (rsi != null) {
//               Row[] allRowsInRange = rsi.getAllRowsInRange();
//               for (Row rw : allRowsInRange) {
//                   if (rw != null && rw.getAttribute("ChkptCd") != null &&
//                       max.compareTo(new BigDecimal(rw.getAttribute("ChkptCd").toString())) < 0)
//                       System.out.println("rw.getAttribute(\"ChkptCd\")" + rw.getAttribute("ChkptCd"));
//                   max =new  BigDecimal(rw.getAttribute("ChkptCd").toString());
//               }
//           }
//           max = max.add(new BigDecimal(1));
//           System.out.println("max Value:" + max);
//           System.out.println("####################out fetchmaxline number###########################");
//           return max;
//       }

//    public void createButtonAl(ActionEvent actionEvent) {
//        System.out.println("Create button");
//                DCIteratorBinding dci = ADFUtils.findIterator("CheckPtCodeMasterVO1Iterator");
//                if (dci != null) {
//                    BigDecimal CHKPT_CD = fetchMaxLineNumber(dci);
//                    System.out.println("Sequence Number after fetchMaxLineNumber:" + CHKPT_CD);
//                    RowSetIterator rsi = dci.getRowSetIterator();
//                    if (rsi != null) {
//                        Row last = rsi.last();
//                        int i = rsi.getRangeIndexOf(last);
//                        CheckPtCodeMasterVORowImpl newRow = (CheckPtCodeMasterVORowImpl) rsi.createRow();
//                        newRow.setNewRowState(Row.STATUS_INITIALIZED);
//                        rsi.insertRowAtRangeIndex(i + 1, newRow);
//                        rsi.setCurrentRow(newRow);
//                        newRow.setChkptCd(CHKPT_CD);
//                        }
//                }
//    }

    public void MachCatLovVCL(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
            if(!editAction.equals("E") && chkPtCdBinding.getValue()==null){
                       System.out.println("In If block!");
        ADFUtils.findOperation("generateCheckPointCodeSequence").execute();
        AdfFacesContext.getCurrentInstance().addPartialTarget(chkPtCdBinding);
        }
    }

    public void setChkPtCdBinding(RichOutputText chkPtCdBinding) {
        this.chkPtCdBinding = chkPtCdBinding;
    }

    public RichOutputText getChkPtCdBinding() {
        return chkPtCdBinding;
    }
}
