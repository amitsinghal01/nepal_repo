package terms.mtm.setup.ui.bean;

import java.math.BigDecimal;

import java.sql.Timestamp;

import java.util.Calendar;

import javax.faces.application.FacesMessage;

import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class CheckPointMasterBean {
    private String editAction="V";
    private RichTable checkPointMasterbinding;
    private RichPopup showPopupbehaviour;
    private RichInputDate dateBinding;
    private RichInputDate timetakenBinding;
    private RichInputText prevBinding;
    private RichInputText prevmantaBinding;
    private RichInputText codeGenerateBinding;
    private RichInputText checkFreqBinding;
    private RichInputDate lastSchedDateBinding;
    private RichInputDate lastDateBinding;

    public CheckPointMasterBean() {

        
}

    public void deletePopupdialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
        {
        OperationBinding op = null;
        ADFUtils.findOperation("Delete").execute();
        op = (OperationBinding) ADFUtils.findOperation("Commit");
        if (getEditAction().equals("V")) {
        Object rst = op.execute();
        }
        System.out.println("Record Delete Successfully");
        if(op.getErrors().isEmpty()){
        FacesMessage Message = new FacesMessage("Record Deleted Successfully");
        Message.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        } else if (!op.getErrors().isEmpty())
        {
        OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
        Object rstr = opr.execute();
        FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
        Message.setSeverity(FacesMessage.SEVERITY_ERROR);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(checkPointMasterbinding);
        }

  
    public void setCheckPointMasterbinding(RichTable checkPointMasterbinding) {
        this.checkPointMasterbinding = checkPointMasterbinding;
    }

    public RichTable getCheckPointMasterbinding() {
        return checkPointMasterbinding;
    }
    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public void setShowPopupbehaviour(RichPopup showPopupbehaviour) {
        this.showPopupbehaviour = showPopupbehaviour;
    }

    public RichPopup getShowPopupbehaviour() {
        return showPopupbehaviour;
    }

    public void otherDetailsPopup(ActionEvent actionEvent) {
        if(editAction.equals("A")){
                    System.out.println("In If block!!");
                ADFUtils.findOperation("CreateInsert1").execute();
                }
                ADFUtils.showPopup(showPopupbehaviour);
    }

    public void codeGenerationVCE(ValueChangeEvent vce) 
    {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        //ADFUtils.findOperation("checkPointMasterCodeGeneration").execute();
        //AdfFacesContext.getCurrentInstance().addPartialTarget(codeGenerateBinding);
    
    }

    public void setDateBinding(RichInputDate dateBinding) {
        this.dateBinding = dateBinding;
    }

    public RichInputDate getDateBinding() {
        return dateBinding;
    }

    public void setTimetakenBinding(RichInputDate timetakenBinding) {
        this.timetakenBinding = timetakenBinding;
    }

    public RichInputDate getTimetakenBinding() {
        return timetakenBinding;
    }

    public void setPrevBinding(RichInputText prevBinding) {
        this.prevBinding = prevBinding;
    }

    public RichInputText getPrevBinding() {
        return prevBinding;
    }

    public void setPrevmantaBinding(RichInputText prevmantaBinding) {
        this.prevmantaBinding = prevmantaBinding;
    }

    public RichInputText getPrevmantaBinding() {
        return prevmantaBinding;
    }

    public void setCodeGenerateBinding(RichInputText codeGenerateBinding) {
        this.codeGenerateBinding = codeGenerateBinding;
    }

    public RichInputText getCodeGenerateBinding() {
        return codeGenerateBinding;
    }

    public void otherDetailDialogListner(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
        {
            System.out.println("In ok dialog");
           OperationBinding op=(OperationBinding) ADFUtils.findOperation("Commit");
           op.execute();
        }
    }

    public void lastDateVCL(ValueChangeEvent lsd) {
//        lsd.getComponent().processUpdates(FacesContext.getCurrentInstance());
//                if(lsd!=null){
//                    Integer ing = (Integer) checkFreqBinding.getValue();
//                        int days = ing.intValue();
//                    Timestamp RefDate=(Timestamp)lastSchedDateBinding.getValue();
//                    Calendar cal=Calendar.getInstance();
//                        cal.setTimeInMillis(RefDate.getTime());
//                            cal.add(Calendar.DAY_OF_MONTH,days);
//                    Timestamp PaymentDate=new Timestamp(cal.getTime().getTime());
//                        lastDateBinding.setValue(PaymentDate);
//                }
    }

    public void setCheckFreqBinding(RichInputText checkFreqBinding) {
        this.checkFreqBinding = checkFreqBinding;
    }

    public RichInputText getCheckFreqBinding() {
        return checkFreqBinding;
    }

    public void setLastSchedDateBinding(RichInputDate lastSchedDateBinding) {
        this.lastSchedDateBinding = lastSchedDateBinding;
    }

    public RichInputDate getLastSchedDateBinding() {
        return lastSchedDateBinding;
    }

    public void setLastDateBinding(RichInputDate lastDateBinding) {
        this.lastDateBinding = lastDateBinding;
    }

    public RichInputDate getLastDateBinding() {
        return lastDateBinding;
    }
}

