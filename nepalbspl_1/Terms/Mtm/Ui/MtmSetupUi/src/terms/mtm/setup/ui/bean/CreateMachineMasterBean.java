package terms.mtm.setup.ui.bean;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import java.lang.reflect.Method;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;

import javax.el.ELContext;
import javax.el.ExpressionFactory;

import javax.el.ValueExpression;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.RichDialog;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.nav.RichCommandLink;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.adf.view.rich.util.ResetUtils;

import oracle.binding.OperationBinding;

import org.apache.myfaces.trinidad.model.UploadedFile;

public class CreateMachineMasterBean {

    private RichInputText codeBinding;
    private RichOutputText editTransBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton machineMasterEditBinding;
    private RichTable spareConsumableTableBinding;
    private RichTable machineDependencyTableBinding;
    private RichSelectOneChoice itemTypeBinding;
    private RichShowDetailItem otherMachineDetailTabBinding;
    private RichShowDetailItem spareConsumableTabBinding;
    private RichShowDetailItem machineDependencyTabBinding;
    private RichTable machineMacinicTableBinding;
    private RichShowDetailItem machineMachinicBinding;
    private RichInputDate lastPmdateBinding;
    private RichInputDate transOldDateBinding;
    private RichSelectOneChoice reqPmAmcBinding;
    private RichInputText pmFreqDaysBinding;
    private RichInputText avgPmElecHrsBinding;
    private RichInputText avgPmMechHrsBinding;
    private RichInputText amcFreqDaysBinding;
    private RichInputText avgAmcElecHrsBinding;
    private RichInputText avgAmcMechHrsBinding;
    private RichInputDate installationDateBinding;
    private RichShowDetailItem tabMachineHead;
    private RichSelectOneChoice criticalBinding;
    private RichInputText documentSeqNoBinding;
    private RichInputText unitCdRefDocBinding;
    private RichInputText refDocNoBinding;
    private RichInputText refDocTypeBinding;
    private RichInputText docFileNameBinding;
    private RichInputDate refDocDateBinding;
    private RichInputText remarksBinding;
    private RichTable referenceDocumentTableBinding;
    private RichInputText pathBinding;
    private RichShowDetailItem referenceDocumentTabBinding;
    private RichCommandLink downloadLinkBinding;
    private RichInputText tagBinding;
    private RichInputText refTagBinding;

    public CreateMachineMasterBean() {
    }

    public void onPageLoad() {
        ADFUtils.findOperation("MachineMasterCreateInsert").execute();
        ADFUtils.findOperation("MachLineCreateInsert").execute();
    }

    public void SaveButtonAL(ActionEvent actionEvent){
                    System.out.println("Inside i1 Save");
        ADFUtils.setLastUpdatedBy("MachineMasterVO1Iterator","LastUpdatedBy");
            if(criticalBinding.getValue() != null && reqPmAmcBinding.getValue() != null){
                Integer i1=0;
                i1=(Integer)editTransBinding.getValue();
                    System.out.println("EDIT TRANS VALUE"+i1);
                    System.out.println("Inside i1 save");
                if(i1.equals(0)){

                    ADFUtils.findOperation("getMachineCode").execute();
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message = new FacesMessage("Record Save Successfully. Machine Code and Tag No Generated are: "+codeBinding.getValue()+".");   
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                    FacesContext fc = FacesContext.getCurrentInstance();   
                        fc.addMessage(null, Message);      
                }else{
                    ADFUtils.findOperation("getMachineCode").execute();
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message = new FacesMessage("Record Updated Successfully");   
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                    FacesContext fc = FacesContext.getCurrentInstance();   
                        fc.addMessage(null, Message);      
                }
            }else {
                ADFUtils.showMessage("Please Fill Other Machine Detail.", 0);
        }   
    }
    
    public String saveAndCloseBean() {
        ADFUtils.setLastUpdatedBy("MachineMasterVO1Iterator","LastUpdatedBy");
                    System.out.println("Inside i2 Save n close");
            if(criticalBinding.getValue() != null && reqPmAmcBinding.getValue() != null){
                Integer i2=0;
                i2=(Integer)editTransBinding.getValue();
                    System.out.println("EDIT TRANS VALUE"+i2);
                    System.out.println("Inside i2 save n close");
                if(i2.equals(0)){
                    ADFUtils.findOperation("getMachineCode").execute();
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message = new FacesMessage("Record Save Successfully. Machine Code Generated is: "+codeBinding.getValue()+".");
                    System.out.println("MAchine Code " + codeBinding.getValue());
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                    FacesContext fc = FacesContext.getCurrentInstance();   
                        fc.addMessage(null, Message);      
                            return "Save and Close";
                }else{
                    ADFUtils.findOperation("getMachineCode").execute();
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message = new FacesMessage("Record Updated Successfully");   
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                    FacesContext fc = FacesContext.getCurrentInstance();   
                    fc.addMessage(null, Message);    
                        return "Save and Close";
                }
            }else{
                ADFUtils.showMessage("Please Fill Other Machine Detail.", 0);
                    return null;
            }
    }

    public void setCodeBinding(RichInputText codeBinding) {
        this.codeBinding = codeBinding;
    }

    public RichInputText getCodeBinding() {
        return codeBinding;
    }

    public void setEditTransBinding(RichOutputText editTransBinding) {
        this.editTransBinding = editTransBinding;
    }

    public RichOutputText getEditTransBinding() {
        cevmodecheck();
        return editTransBinding;
    }
    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setMachineMasterEditBinding(RichButton machineMasterEditBinding) {
        this.machineMasterEditBinding = machineMasterEditBinding;
    }

    public RichButton getMachineMasterEditBinding() {
        return machineMasterEditBinding;
    }
    
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }

    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }
    
    public void cevModeDisableComponent(String mode) {
        if (mode.equals("E")) {
            getCodeBinding().setDisabled(true);
            getMachineMasterEditBinding().setDisabled(true);
            getDocumentSeqNoBinding().setDisabled(true);
            getUnitCdRefDocBinding().setDisabled(true);
            getRefDocNoBinding().setDisabled(true);
            getRefDocTypeBinding().setDisabled(true);
            getDocFileNameBinding().setDisabled(true);
            getRefDocDateBinding().setDisabled(true);
            getTagBinding().setDisabled(true);
            getRefTagBinding().setDisabled(false);
            

        } else if (mode.equals("C")) {
            getCodeBinding().setDisabled(true);
            getDocumentSeqNoBinding().setDisabled(true);
            getUnitCdRefDocBinding().setDisabled(true);
            getRefDocNoBinding().setDisabled(true);
            getRefDocTypeBinding().setDisabled(true);
            getDocFileNameBinding().setDisabled(true);
            getRefDocDateBinding().setDisabled(true);
            getTagBinding().setDisabled(true);
            getRefTagBinding().setDisabled(false);
        } else if (mode.equals("V")) {
            getTabMachineHead().setDisabled(false);
            getOtherMachineDetailTabBinding().setDisabled(false);
            getSpareConsumableTabBinding().setDisabled(false);
            getMachineDependencyTabBinding().setDisabled(false);
            getMachineMachinicBinding().setDisabled(false);
            getReferenceDocumentTabBinding().setDisabled(false);
            getDownloadLinkBinding().setDisabled(false);
            getTagBinding().setDisabled(true);
            getRefTagBinding().setDisabled(true);
        }
    }


    public void spareConsumableDeletePopupDialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                   {
                   ADFUtils.findOperation("Delete1").execute();
                       OperationBinding op=  ADFUtils.findOperation("Commit");
                       Object rst = op.execute();
                   System.out.println("Record Delete Successfully");
                  
                       if(op.getErrors().isEmpty())
                       {
                       FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                       Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                       FacesContext fc = FacesContext.getCurrentInstance();  
                       fc.addMessage(null, Message);
                       }
                       else
                       {
                       FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                       Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                       FacesContext fc = FacesContext.getCurrentInstance();
                       fc.addMessage(null, Message);
                        }
               }
               AdfFacesContext.getCurrentInstance().addPartialTarget(spareConsumableTableBinding);   
        }

    public void setSpareConsumableTableBinding(RichTable spareConsumableTableBinding) {
        this.spareConsumableTableBinding = spareConsumableTableBinding;
    }

    public RichTable getSpareConsumableTableBinding() {
        return spareConsumableTableBinding;
    }

    public void machineDependencyDeletePopupDialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                   {
                   ADFUtils.findOperation("Delete").execute();
                       OperationBinding op=  ADFUtils.findOperation("Commit");
                       Object rst = op.execute();
                   System.out.println("Record Delete Successfully");
                  
                       if(op.getErrors().isEmpty())
                       {
                       FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                       Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                       FacesContext fc = FacesContext.getCurrentInstance();  
                       fc.addMessage(null, Message);
                       }
                       else
                       {
                       FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                       Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                       FacesContext fc = FacesContext.getCurrentInstance();
                       fc.addMessage(null, Message);
                        }
               }
               AdfFacesContext.getCurrentInstance().addPartialTarget(machineDependencyTableBinding);
    }

    public void setMachineDependencyTableBinding(RichTable machineDependencyTableBinding) {
        this.machineDependencyTableBinding = machineDependencyTableBinding;
    }

    public RichTable getMachineDependencyTableBinding() {
        return machineDependencyTableBinding;
    }

    public void setItemTypeBinding(RichSelectOneChoice itemTypeBinding) {
        this.itemTypeBinding = itemTypeBinding;
    }

    public RichSelectOneChoice getItemTypeBinding() {
        return itemTypeBinding;
    }

    public void spareConsMasItemTypeVCL(ValueChangeEvent vce) {
        if(vce.getNewValue()!=vce.getOldValue())
        {
               System.out.println("Item Type VCL:"+itemTypeBinding.getValue());
               OperationBinding op = ADFUtils.findOperation("valueSetNullInMachineMaster");
               op.execute();
        }
    }

    public void setOtherMachineDetailTabBinding(RichShowDetailItem otherMachineDetailTabBinding) {
        this.otherMachineDetailTabBinding = otherMachineDetailTabBinding;
    }

    public RichShowDetailItem getOtherMachineDetailTabBinding() {
        return otherMachineDetailTabBinding;
    }

    public void setSpareConsumableTabBinding(RichShowDetailItem spareConsumableTabBinding) {
        this.spareConsumableTabBinding = spareConsumableTabBinding;
    }

    public RichShowDetailItem getSpareConsumableTabBinding() {
        return spareConsumableTabBinding;
    }

    public void setMachineDependencyTabBinding(RichShowDetailItem machineDependencyTabBinding) {
        this.machineDependencyTabBinding = machineDependencyTabBinding;
    }

    public RichShowDetailItem getMachineDependencyTabBinding() {
        return machineDependencyTabBinding;
    }

    public void machineCodeVCL(ValueChangeEvent vce) {
        if(vce!=null){
                ADFUtils.findOperation("machineCodeGenrate").execute();
            }
    }

    public void deleteMachineMacinicDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                   {
                   ADFUtils.findOperation("Delete2").execute();
                       OperationBinding op=  ADFUtils.findOperation("Commit");
                       Object rst = op.execute();
                   System.out.println("Record Delete Successfully");
                  
                       if(op.getErrors().isEmpty())
                       {
                       FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                       Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                       FacesContext fc = FacesContext.getCurrentInstance();  
                       fc.addMessage(null, Message);
                       }
                       else
                       {
                       FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                       Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                       FacesContext fc = FacesContext.getCurrentInstance();
                       fc.addMessage(null, Message);
                        }
               }
               AdfFacesContext.getCurrentInstance().addPartialTarget(machineMacinicTableBinding);
    }

    public void setMachineMacinicTableBinding(RichTable machineMacinicTableBinding) {
        this.machineMacinicTableBinding = machineMacinicTableBinding;
    }

    public RichTable getMachineMacinicTableBinding() {
        return machineMacinicTableBinding;
    }

    public void setMachineMachinicBinding(RichShowDetailItem machineMachinicBinding) {
        this.machineMachinicBinding = machineMachinicBinding;
    }

    public RichShowDetailItem getMachineMachinicBinding() {
        return machineMachinicBinding;
    }

//    public void lastPmDateCheckVL(ValueChangeEvent pmchk) {
//        Date oldDate = (Date) transOldDateBinding.getValue();
//            System.out.println("Old Date: " + oldDate);
//        Date newDate = (Date) pmchk.getNewValue();
//            System.out.println("New Date: " +newDate);
//            System.out.println("After date comparision: "+newDate.compareTo(oldDate));
//            System.out.println("Mode: "+ADFUtils.resolveExpression("#{pageFlowScope.mode}"));
//        if(newDate.compareTo(oldDate)==-1 && ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")){
//                    FacesMessage message = new FacesMessage("New Date must be greater than or equal to Previous date");
//                        message.setSeverity(FacesMessage.SEVERITY_ERROR);
//                    FacesContext context = FacesContext.getCurrentInstance();
//                        context.addMessage(lastPmdateBinding.getClientId(), message);
//                    lastPmdateBinding.setValue(oldDate);       
//                        
//                }
//    }

    public void setLastPmdateBinding(RichInputDate lastPmdateBinding) {
        this.lastPmdateBinding = lastPmdateBinding;
    }

    public RichInputDate getLastPmdateBinding() {
        return lastPmdateBinding;
    }

    public void setTransOldDateBinding(RichInputDate transOldDateBinding) {
        this.transOldDateBinding = transOldDateBinding;
    }

    public RichInputDate getTransOldDateBinding() {
        return transOldDateBinding;
    }

    public void setReqPmAmcBinding(RichSelectOneChoice reqPmAmcBinding) {
        this.reqPmAmcBinding = reqPmAmcBinding;
    }

    public RichSelectOneChoice getReqPmAmcBinding() {
        return reqPmAmcBinding;
    }

    public void setPmFreqDaysBinding(RichInputText pmFreqDaysBinding) {
        this.pmFreqDaysBinding = pmFreqDaysBinding;
    }

    public RichInputText getPmFreqDaysBinding() {
        return pmFreqDaysBinding;
    }

    public void setAvgPmElecHrsBinding(RichInputText avgPmElecHrsBinding) {
        this.avgPmElecHrsBinding = avgPmElecHrsBinding;
    }

    public RichInputText getAvgPmElecHrsBinding() {
        return avgPmElecHrsBinding;
    }

    public void setAvgPmMechHrsBinding(RichInputText avgPmMechHrsBinding) {
        this.avgPmMechHrsBinding = avgPmMechHrsBinding;
    }

    public RichInputText getAvgPmMechHrsBinding() {
        return avgPmMechHrsBinding;
    }

    public void setAmcFreqDaysBinding(RichInputText amcFreqDaysBinding) {
        this.amcFreqDaysBinding = amcFreqDaysBinding;
    }

    public RichInputText getAmcFreqDaysBinding() {
        return amcFreqDaysBinding;
    }

    public void setAvgAmcElecHrsBinding(RichInputText avgAmcElecHrsBinding) {
        this.avgAmcElecHrsBinding = avgAmcElecHrsBinding;
    }

    public RichInputText getAvgAmcElecHrsBinding() {
        return avgAmcElecHrsBinding;
    }

    public void setAvgAmcMechHrsBinding(RichInputText avgAmcMechHrsBinding) {
        this.avgAmcMechHrsBinding = avgAmcMechHrsBinding;
    }

    public RichInputText getAvgAmcMechHrsBinding() {
        return avgAmcMechHrsBinding;
    }

    public void pmAmcReqVL(ValueChangeEvent change) {
        change.getComponent().processUpdates(FacesContext.getCurrentInstance());
                System.out.println("change.getNewValue() "+change.getNewValue());
            if(change.getNewValue().equals("P")){
                System.out.println("Choice 1 " +change.getNewValue());
            getAmcFreqDaysBinding().setDisabled(true);
            getAvgAmcElecHrsBinding().setDisabled(true);
            getAvgAmcMechHrsBinding().setDisabled(true);
            
            getPmFreqDaysBinding().setDisabled(false);
            getAvgPmElecHrsBinding().setDisabled(false);
            getAvgPmMechHrsBinding().setDisabled(false);
            }
       else if(change.getNewValue().equals("A")){
                System.out.println("Choice 2 " +change.getNewValue());
            getPmFreqDaysBinding().setDisabled(true);
            getAvgPmElecHrsBinding().setDisabled(true);
            getAvgPmMechHrsBinding().setDisabled(true);
            
            getAmcFreqDaysBinding().setDisabled(false);
            getAvgAmcElecHrsBinding().setDisabled(false);
            getAvgAmcMechHrsBinding().setDisabled(false);
            }
       else if(change.getNewValue().equals("B")){
                    System.out.println("Choice 3 " +change.getNewValue());
                getAmcFreqDaysBinding().setDisabled(false);
                getAvgAmcElecHrsBinding().setDisabled(false);
                getAvgAmcMechHrsBinding().setDisabled(false);
                
                getPmFreqDaysBinding().setDisabled(false);
                getAvgPmElecHrsBinding().setDisabled(false);
                getAvgPmMechHrsBinding().setDisabled(false);
            }else{
                    System.out.println("Choice Non " +change.getNewValue());
                getAmcFreqDaysBinding().setDisabled(true);
                getAvgAmcElecHrsBinding().setDisabled(true);
                getAvgAmcMechHrsBinding().setDisabled(true);
                
                getPmFreqDaysBinding().setDisabled(true);
                getAvgPmElecHrsBinding().setDisabled(true);
                getAvgPmMechHrsBinding().setDisabled(true);
            }
                        AdfFacesContext.getCurrentInstance().addPartialTarget(amcFreqDaysBinding);
                        AdfFacesContext.getCurrentInstance().addPartialTarget(avgAmcElecHrsBinding);
                        AdfFacesContext.getCurrentInstance().addPartialTarget(avgAmcMechHrsBinding);
                        
                        AdfFacesContext.getCurrentInstance().addPartialTarget(pmFreqDaysBinding);
                        AdfFacesContext.getCurrentInstance().addPartialTarget(avgPmElecHrsBinding);
                        AdfFacesContext.getCurrentInstance().addPartialTarget(avgPmMechHrsBinding);
        
    }

    public void setInstallationDateBinding(RichInputDate installationDateBinding) {
        this.installationDateBinding = installationDateBinding;
    }

    public RichInputDate getInstallationDateBinding() {
        return installationDateBinding;
    }

    public void pODateVL(ValueChangeEvent pod) {
        pod.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(pod != null){
                lastPmdateBinding.setValue(null);
        }
    }

    public void installDateVL(ValueChangeEvent idvl) {
        idvl.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(idvl != null){
                lastPmdateBinding.setValue(null);
        }
    }

    public void setTabMachineHead(RichShowDetailItem tabMachineHead) {
        this.tabMachineHead = tabMachineHead;
    }

    public RichShowDetailItem getTabMachineHead() {
        return tabMachineHead;
    }

    public void setCriticalBinding(RichSelectOneChoice criticalBinding) {
        this.criticalBinding = criticalBinding;
    }

    public RichSelectOneChoice getCriticalBinding() {
        return criticalBinding;
    }

    public void uploadAction(ValueChangeEvent vce) {
        System.out.println("inside uploadAction #### ");
                    //Get File Object from VC Event
                    UploadedFile fileVal = (UploadedFile) vce.getNewValue();
                    //Upload File to path- Return actual server path
                    String path = ADFUtils.uploadFile(fileVal);
                      System.out.println(fileVal.getContentType());
        //                      System.out.println("Content Type==>>"+fileVal.getContentType()+"Path==>>"+path+"File name==>>"+fileVal.getFilename());
                    //Method to insert data in table to keep track of uploaded files
                    OperationBinding ob = ADFUtils.findOperation("setFileDataForMachineMaster");
                    ob.getParamsMap().put("name", fileVal.getFilename());
                    ob.getParamsMap().put("path", path);
                    ob.getParamsMap().put("contTyp", fileVal.getContentType());
                    ob.getParamsMap().put("SrNo", ADFUtils.evaluateEL("#{bindings.DocAttachRefDtlVO1Iterator.estimatedRowCount+1}"));
                    ob.execute();
                    // Reset inputFile component after upload
                    ResetUtils.reset(vce.getComponent());
    }

    public void setDocumentSeqNoBinding(RichInputText documentSeqNoBinding) {
        this.documentSeqNoBinding = documentSeqNoBinding;
    }

    public RichInputText getDocumentSeqNoBinding() {
        return documentSeqNoBinding;
    }

    public void setUnitCdRefDocBinding(RichInputText unitCdRefDocBinding) {
        this.unitCdRefDocBinding = unitCdRefDocBinding;
    }

    public RichInputText getUnitCdRefDocBinding() {
        return unitCdRefDocBinding;
    }

    public void setRefDocNoBinding(RichInputText refDocNoBinding) {
        this.refDocNoBinding = refDocNoBinding;
    }

    public RichInputText getRefDocNoBinding() {
        return refDocNoBinding;
    }

    public void setRefDocTypeBinding(RichInputText refDocTypeBinding) {
        this.refDocTypeBinding = refDocTypeBinding;
    }

    public RichInputText getRefDocTypeBinding() {
        return refDocTypeBinding;
    }

    public void setDocFileNameBinding(RichInputText docFileNameBinding) {
        this.docFileNameBinding = docFileNameBinding;
    }

    public RichInputText getDocFileNameBinding() {
        return docFileNameBinding;
    }

    public void setRefDocDateBinding(RichInputDate refDocDateBinding) {
        this.refDocDateBinding = refDocDateBinding;
    }

    public RichInputDate getRefDocDateBinding() {
        return refDocDateBinding;
    }

    public void setRemarksBinding(RichInputText remarksBinding) {
        this.remarksBinding = remarksBinding;
    }

    public RichInputText getRemarksBinding() {
        return remarksBinding;
    }

    public void setReferenceDocumentTableBinding(RichTable referenceDocumentTableBinding) {
        this.referenceDocumentTableBinding = referenceDocumentTableBinding;
    }

    public RichTable getReferenceDocumentTableBinding() {
        return referenceDocumentTableBinding;
    }

    public void downloadAction(FacesContext facesContext, OutputStream outputStream) throws IOException {
        File filed = new File(pathBinding.getValue().toString());
                               FileInputStream fis;
                               byte[] b;
                               try {
                                   fis = new FileInputStream(filed);

                                   int n;
                                   while ((n = fis.available()) > 0) {
                                       b = new byte[n];
                                       int result = fis.read(b);
                                       outputStream.write(b, 0, b.length);
                                       if (result == -1)
                                           break;
                                   }
                               } catch (Exception e) {
                                   e.printStackTrace();
                               }
                               outputStream.flush();

    }

    public void setPathBinding(RichInputText pathBinding) {
        this.pathBinding = pathBinding;
    }

    public RichInputText getPathBinding() {
        return pathBinding;
    }

    public void deletePopupDLReferenceDocument(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                    {
                    ADFUtils.findOperation("Delete3").execute();
                    //ADFUtils.findOperation("Commit").execute();    //Unable on Search page.
                    System.out.println("Record Delete Successfully");
                   
                        FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                        FacesContext fc = FacesContext.getCurrentInstance();  
                        fc.addMessage(null, Message);  
                   
                    }

              AdfFacesContext.getCurrentInstance().addPartialTarget(referenceDocumentTableBinding);

    }

    public void setReferenceDocumentTabBinding(RichShowDetailItem referenceDocumentTabBinding) {
        this.referenceDocumentTabBinding = referenceDocumentTabBinding;
    }

    public RichShowDetailItem getReferenceDocumentTabBinding() {
        return referenceDocumentTabBinding;
    }

    public void setDownloadLinkBinding(RichCommandLink downloadLinkBinding) {
        this.downloadLinkBinding = downloadLinkBinding;
    }

    public RichCommandLink getDownloadLinkBinding() {
        return downloadLinkBinding;
    }

    public void setTagBinding(RichInputText tagBinding) {
        this.tagBinding = tagBinding;
    }

    public RichInputText getTagBinding() {
        return tagBinding;
    }

    public void setRefTagBinding(RichInputText refTagBinding) {
        this.refTagBinding = refTagBinding;
    }

    public RichInputText getRefTagBinding() {
        return refTagBinding;
    }

    public void sectionToTagVCL(ValueChangeEvent vcl) {
        vcl.getComponent().processUpdates(FacesContext.getCurrentInstance());
            ADFUtils.findOperation("getTagNo").execute();
    }
}
