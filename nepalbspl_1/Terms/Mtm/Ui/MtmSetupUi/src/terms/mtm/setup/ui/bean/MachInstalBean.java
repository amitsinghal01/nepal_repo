package terms.mtm.setup.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichPanelLabelAndMessage;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.binding.OperationBinding;

public class MachInstalBean {
    private RichPanelHeader getMyPageRootBinding;
    private RichButton headerEditBinding;
    private RichOutputText bindingOutputText;
    private RichInputText nameBinding;
    private RichInputText locationBinding;

    public MachInstalBean() {
    }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
            try {
                Method method1 =
                    component.getClass().getMethod("setDisabled", boolean.class);
                if (method1 != null) {
                    method1.invoke(component, valueComponent);
                }
            } catch (NoSuchMethodException e) {
                try {
                    Method method =
                        component.getClass().getMethod("setReadOnly", boolean.class);
                    if (method != null) {
                        method.invoke(component, valueComponent);
                    }
                } catch (Exception e1) {
                    // e.printStackTrace();//silently eat this exception.
                }


            } catch (Exception e) {
                // e.printStackTrace();//silently eat this exception.
            }
            List<UIComponent> childComponents = component.getChildren();
            for (UIComponent comp : childComponents) {
                makeComponentHierarchyReadOnly(comp, valueComponent);
            }
        }
        
        
                        private void cevmodecheck(){
                            if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
                                makeComponentHierarchyReadOnly(this.getGetMyPageRootBinding(), true);
                                cevModeDisableComponent("V");
                            }/* else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
                                makeComponentHierarchyReadOnly(this.getGetMyPageRootBinding(), false);
                                cevModeDisableComponent("C");
                            } */else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
                                makeComponentHierarchyReadOnly(this.getGetMyPageRootBinding(), false);
                                cevModeDisableComponent("E");
                            }
                        }
        
    public void cevModeDisableComponent(String mode) {
        if (mode.equals("E")) {
            getHeaderEditBinding().setDisabled(true);
            getNameBinding().setDisabled(true);
            getLocationBinding().setDisabled(true);
        }  
    }    
    

    public void setGetMyPageRootBinding(RichPanelHeader getMyPageRootBinding) {
        this.getMyPageRootBinding = getMyPageRootBinding;
    }

    public RichPanelHeader getGetMyPageRootBinding() {
        return getMyPageRootBinding;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
       
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        cevmodecheck();
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        return bindingOutputText;
    }

    public void setNameBinding(RichInputText nameBinding) {
        this.nameBinding = nameBinding;
    }

    public RichInputText getNameBinding() {
        return nameBinding;
    }

    public void setLocationBinding(RichInputText locationBinding) {
        this.locationBinding = locationBinding;
    }

    public RichInputText getLocationBinding() {
        return locationBinding;
    }

    public void SaveButtonAL(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedBy("MachineInstallationVO1Iterator","LastUpdatedBy");
        Integer i=0;
        i=(Integer)bindingOutputText.getValue();
        System.out.println("EDIT TRANS VALUE"+i);
        if(i.equals(0))
        {
        FacesMessage Message = new FacesMessage("Record Save Successfully");
        Message.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        
        }
        else
        {
        FacesMessage Message = new FacesMessage("Record Updated Successfully");   
        Message.setSeverity(FacesMessage.SEVERITY_INFO);   
        FacesContext fc = FacesContext.getCurrentInstance();   
        fc.addMessage(null, Message);      
        }
        
        
    }
}
