package terms.mtm.setup.ui.bean;

import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;

public class TagMasterBean {
    private String editAction="V";
    private RichTable tagMasterTableBinding;

    public TagMasterBean() {
    }
    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public void setTagMasterTableBinding(RichTable tagMasterTableBinding) {
        this.tagMasterTableBinding = tagMasterTableBinding;
    }

    public RichTable getTagMasterTableBinding() {
        return tagMasterTableBinding;
    }
}

