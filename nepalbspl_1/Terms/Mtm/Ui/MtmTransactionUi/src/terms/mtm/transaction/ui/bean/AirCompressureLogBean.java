package terms.mtm.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

public class AirCompressureLogBean {
    private RichOutputText bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichButton detailcreateBinding;
    private RichTable createAirCompressureTableBinding;
    private RichInputText srNoBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputComboboxListOfValues locationCodeBinding;
    private RichInputComboboxListOfValues machineCodeBinding;
    private RichInputDate operationDateBinding;

    public AirCompressureLogBean() {
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setDetailcreateBinding(RichButton detailcreateBinding) {
        this.detailcreateBinding = detailcreateBinding;
    }

    public RichButton getDetailcreateBinding() {
        return detailcreateBinding;
    }

    
    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }
    private void cevmodecheck(){
          if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
              makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
              cevModeDisableComponent("V");
          }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
              makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
              cevModeDisableComponent("C");
          }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
              makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
              cevModeDisableComponent("E");
          }
      }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                try {
                    Method method1 =
                        component.getClass().getMethod("setDisabled", boolean.class);
                    if (method1 != null) {
                        method1.invoke(component, valueComponent);
                    }
                } catch (NoSuchMethodException e) {
                    try {
                        Method method =
                            component.getClass().getMethod("setReadOnly", boolean.class);
                        if (method != null) {
                            method.invoke(component, valueComponent);
                        }
                    } catch (Exception e1) {
                        // e.printStackTrace();//silently eat this exception.
                    }


                } catch (Exception e) {
                    // e.printStackTrace();//silently eat this exception.
                }
                List<UIComponent> childComponents = component.getChildren();
                for (UIComponent comp : childComponents) {
                    makeComponentHierarchyReadOnly(comp, valueComponent);
                }
            }
    
    //Set Fields and Button disable.
        public void cevModeDisableComponent(String mode) {
        //            FacesContext fctx = FacesContext.getCurrentInstance();
        //            ELContext elctx = fctx.getELContext();
        //            Application jsfApp = fctx.getApplication();
        //            //create a ValueExpression that points to the ADF binding layer
        //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
        //            //
        //            ValueExpression valueExpr = exprFactory.createValueExpression(
        //                                         elctx,
        //                                         "#{pageFlowScope.mode=='E'}",
        //                                          Object.class
        //                                         );
        //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
            if (mode.equals("E")) 
            {
                    
                    getHeaderEditBinding().setDisabled(true);
                    getDetailcreateBinding().setDisabled(false);
                    getSrNoBinding().setDisabled(true);
                    getUnitCodeBinding().setDisabled(true);
                    getLocationCodeBinding().setDisabled(true);
                    getMachineCodeBinding().setDisabled(true);
                    getOperationDateBinding().setDisabled(true);
            } 
            
            else if (mode.equals("C")) 
            {
                getHeaderEditBinding().setDisabled(true);
                getUnitCodeBinding().setDisabled(true);
                getDetailcreateBinding().setDisabled(false);
                
                getSrNoBinding().setDisabled(true);
            } 
            else if (mode.equals("V"))
            {
                getDetailcreateBinding().setDisabled(true);
               
            }
            
        }


    public void deletePopupDialogDL(DialogEvent dialogEvent) {
       
            if (dialogEvent.getOutcome().name().equals("ok")) {
                ADFUtils.findOperation("Delete").execute();
                System.out.println("Record Delete Successfully");

                FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);

            }
            AdfFacesContext.getCurrentInstance().addPartialTarget(createAirCompressureTableBinding);
        }

    public void setCreateAirCompressureTableBinding(RichTable createAirCompressureTableBinding) {
        this.createAirCompressureTableBinding = createAirCompressureTableBinding;
    }

    public RichTable getCreateAirCompressureTableBinding() {
        return createAirCompressureTableBinding;
    }
    public void setSrNoBinding(RichInputText srNoBinding) {
        this.srNoBinding = srNoBinding;
    }
    public RichInputText getSrNoBinding() {
        return srNoBinding;
    }
    public void createInsertAL(ActionEvent actionEvent) 
    {
    ADFUtils.findOperation("CreateInsert").execute();
    srNoBinding.setValue(ADFUtils.evaluateEL("#{bindings.AirCompLogDetailVO1Iterator.estimatedRowCount}"));
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setLocationCodeBinding(RichInputComboboxListOfValues locationCodeBinding) {
        this.locationCodeBinding = locationCodeBinding;
    }

    public RichInputComboboxListOfValues getLocationCodeBinding() {
        return locationCodeBinding;
    }

    public void setMachineCodeBinding(RichInputComboboxListOfValues machineCodeBinding) {
        this.machineCodeBinding = machineCodeBinding;
    }

    public RichInputComboboxListOfValues getMachineCodeBinding() {
        return machineCodeBinding;
    }

    public void setOperationDateBinding(RichInputDate operationDateBinding) {
        this.operationDateBinding = operationDateBinding;
    }

    public RichInputDate getOperationDateBinding() {
        return operationDateBinding;
    }


    public void saveAL(ActionEvent actionEvent) {
                    ADFUtils.setLastUpdatedBy("AirCompLogHeaderVO1Iterator","LastUpdatedBy");
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message = new FacesMessage("Record Saved Successfully");
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);
    }

    public void saveAndCloseAL(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedBy("AirCompLogHeaderVO1Iterator","LastUpdatedBy");
        ADFUtils.findOperation("Commit").execute();
        FacesMessage Message = new FacesMessage("Record Updated Successfully");
        Message.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
    }
}

