package terms.mtm.transaction.ui.bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichButton;

import oracle.adf.view.rich.component.rich.nav.RichCommandLink;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.util.ResetUtils;

import oracle.binding.OperationBinding;

import org.apache.myfaces.trinidad.model.UploadedFile;

public class BreakdownComplaintBean {
    private RichButton headerEditBinding;
    private RichPanelGroupLayout getMyPageRootComponent;
    private RichOutputText bindingOutputText;
    private RichInputText complaintNoBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText lineBinding;
    private RichInputText breakdownTimeBinding;
    private RichInputDate targetDateBinding;
    private RichInputText targetRemarksBinding;
    private RichInputDate targetEntryDateBinding;
    private RichInputDate rectEntryDateBinding;
    private RichInputDate rectDateBinding;
    private RichInputText rectByBinding;
    private RichInputText rectRemarksBinding;
    private RichInputDate handoverDateBinding;
    private RichInputDate handoverEntryDateBinding;
    private RichInputText handoverAcceptedByBinding;
    private RichInputText handoverRemarkBinding;
    private RichInputText rectTimeBinding;
    private RichInputText rectEntryTimeBinding;
    private RichInputText targetTimeBinding;
    private RichInputText targetEntryTimeBinding;
    private RichInputText handoverEntryTimeBinding;
    private RichInputText handoverTimeBinding;
    private RichInputText machineCodeBinding;
    private RichInputText complainTimeBinding;
    private RichInputComboboxListOfValues complainByBinding;
    private String Message="C";
    private RichInputComboboxListOfValues deptCodeBinding;
    private RichInputText deptNameBinding;
    private RichInputComboboxListOfValues handoverByBinding;
    private RichInputDate hndOvrDateBinding;
    private RichInputText fltOtherBinding;
    private RichInputComboboxListOfValues tagNoBinding;
    private RichInputText docSeqNoBinding;
    private RichInputText unitCdRefDocBinding;
    private RichInputText refDocNoBinding;
    private RichInputText refDocTypeBinding;
    private RichInputText docFileNameBinding;
    private RichInputText pathBinding;
    private RichInputDate refDocDateBinding;
    private RichInputText remarksBinding;
    private RichTable referenceDocumentTableBinding;
    private RichShowDetailItem referenceDocumentTabBinding;
    private RichShowDetailItem breakdownComplaintTabBinding;
    private RichCommandLink downloadLinkBinding;

    public BreakdownComplaintBean() {
    }


    public void saveComplainAL(ActionEvent actionEvent) {
//           ADFUtils.findOperation("CreateInsert2").execute();
//           ADFUtils.findOperation("CreateInsert3").execute();
//           ADFUtils.findOperation("CreateInsert4").execute(); 
             ADFUtils.setLastUpdatedBy("BreakdownComplaintVO1Iterator","LastUpdatedBy");
            System.out.println("Current Complain no is"+complaintNoBinding.getValue());
            ADFUtils.findOperation("generateComplaintNumber").execute();
            
            System.out.println("--------Commit-------");
           
            Integer TransValue=(Integer)bindingOutputText.getValue();
            System.out.println("TRANS VALUE"+TransValue);
            if(TransValue == 0){
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message = new FacesMessage("Record Saved Successfully. New Item Code is "+complaintNoBinding.getValue()+".");   
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                    FacesContext fc = FacesContext.getCurrentInstance();   
                    fc.addMessage(null, Message);  
               }
                 else{
                           
                         ADFUtils.findOperation("Commit").execute();
                          FacesMessage Message = new FacesMessage("Record Update Successfully.");   
                          Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                          FacesContext fc = FacesContext.getCurrentInstance();   
                          fc.addMessage(null, Message);   
                    }
    }

        

    

    public void EditButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setGetMyPageRootComponent(RichPanelGroupLayout getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelGroupLayout getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
            try {
                Method method1 =
                    component.getClass().getMethod("setDisabled", boolean.class);
                if (method1 != null) {
                    method1.invoke(component, valueComponent);
                }
            } catch (NoSuchMethodException e) {
                try {
                    Method method =
                        component.getClass().getMethod("setReadOnly", boolean.class);
                    if (method != null) {
                        method.invoke(component, valueComponent);
                    }
                } catch (Exception e1) {
                    // e.printStackTrace();//silently eat this exception.
                }


            } catch (Exception e) {
                // e.printStackTrace();//silently eat this exception.
            }
            List<UIComponent> childComponents = component.getChildren();
            for (UIComponent comp : childComponents) {
                makeComponentHierarchyReadOnly(comp, valueComponent);
            }
        }
    private void cevmodecheck(){
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }
    public void cevModeDisableComponent(String mode) {
    //            FacesContext fctx = FacesContext.getCurrentInstance();
    //            ELContext elctx = fctx.getELContext();
    //            Application jsfApp = fctx.getApplication();
    //            //create a ValueExpression that points to the ADF binding layer
    //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
    //            //
    //            ValueExpression valueExpr = exprFactory.createValueExpression(
    //                                         elctx,
    //                                         "#{pageFlowScope.mode=='E'}",
    //                                          Object.class
    //                                         );
    //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
        if (mode.equals("E")) {
            Message="E";
            System.out.println("Mode message in edit "+Message);
            getUnitCodeBinding().setDisabled(true);
           // getBreakdownTimeBinding().setDisabled(true);
            getComplaintNoBinding().setDisabled(true);
            
            
            getTargetDateBinding().setDisabled(true);
            getTargetTimeBinding().setDisabled(true);
            getTargetRemarksBinding().setDisabled(true);
            getTargetEntryDateBinding().setDisabled(true);
            getTargetEntryTimeBinding().setDisabled(true);
            
            getRectDateBinding().setDisabled(true);
            getRectTimeBinding().setDisabled(true);
            getRectEntryDateBinding().setDisabled(true);
            getRectEntryTimeBinding().setDisabled(true);
            getRectByBinding().setDisabled(true);
            getRectRemarksBinding().setDisabled(true);
            
            getHandoverDateBinding().setDisabled(true);
            getHandoverTimeBinding().setDisabled(true);
            getHandoverEntryDateBinding().setDisabled(true);
            getHandoverEntryTimeBinding().setDisabled(true);
            getHandoverAcceptedByBinding().setDisabled(true);
            getHandoverRemarkBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            getLineBinding().setDisabled(true);
            getComplainTimeBinding().setDisabled(true);
            getComplainByBinding().setDisabled(true);
            getDeptCodeBinding().setDisabled(true);
            getDeptNameBinding().setDisabled(true);
            getHndOvrDateBinding().setDisabled(true);
            getHandoverByBinding().setDisabled(true);
            getMachineCodeBinding().setDisabled(true);
            getDocSeqNoBinding().setDisabled(true);
            getUnitCdRefDocBinding().setDisabled(true);
            getRefDocNoBinding().setDisabled(true);
            getRefDocTypeBinding().setDisabled(true);
            getDocFileNameBinding().setDisabled(true);
            getRefDocDateBinding().setDisabled(true);


        } else if (mode.equals("C")) {
            System.out.println("Mode message in Create "+Message);
             getUnitCodeBinding().setDisabled(true);
             getLineBinding().setDisabled(true);
//             getBreakdownTimeBinding().setDisabled(true);
             getComplaintNoBinding().setDisabled(true);
//             getComplainByBinding().setDisabled(true);
             getFltOtherBinding().setDisabled(true);
             getMachineCodeBinding().setDisabled(true);
            getDocSeqNoBinding().setDisabled(true);
            getUnitCdRefDocBinding().setDisabled(true);
            getRefDocNoBinding().setDisabled(true);
            getRefDocTypeBinding().setDisabled(true);
            getDocFileNameBinding().setDisabled(true);
            getRefDocDateBinding().setDisabled(true);
             
        } else if (mode.equals("V")) {
//            getDetailcreateBinding().setDisabled(true);
//            getDetaildeleteBinding().setDisabled(true);
            getReferenceDocumentTabBinding().setDisabled(false);
            getBreakdownComplaintTabBinding().setDisabled(false);
            getDownloadLinkBinding().setDisabled(false);
        } 
    }

    public void setComplaintNoBinding(RichInputText complaintNoBinding) {
        this.complaintNoBinding = complaintNoBinding;
    }

    public RichInputText getComplaintNoBinding() {
        return complaintNoBinding;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setLineBinding(RichInputText lineBinding) {
        this.lineBinding = lineBinding;
    }

    public RichInputText getLineBinding() {
        return lineBinding;
    }

    public void setBreakdownTimeBinding(RichInputText breakdownTimeBinding) {
        this.breakdownTimeBinding = breakdownTimeBinding;
    }

    public RichInputText getBreakdownTimeBinding() {
        return breakdownTimeBinding;
    }

    public void setTargetDateBinding(RichInputDate targetDateBinding) {
        this.targetDateBinding = targetDateBinding;
    }

    public RichInputDate getTargetDateBinding() {
        return targetDateBinding;
    }
    public void setTargetEntryDateBinding(RichInputDate targetEntryDateBinding) {
        this.targetEntryDateBinding = targetEntryDateBinding;
    }

    public RichInputDate getTargetEntryDateBinding() {
        return targetEntryDateBinding;
    }
    public void setRectEntryDateBinding(RichInputDate rectEntryDateBinding) {
        this.rectEntryDateBinding = rectEntryDateBinding;
    }

    public RichInputDate getRectEntryDateBinding() {
        return rectEntryDateBinding;
    }
    public void setRectDateBinding(RichInputDate rectDateBinding) {
        this.rectDateBinding = rectDateBinding;
    }

    public RichInputDate getRectDateBinding() {
        return rectDateBinding;
    }

    public void setRectByBinding(RichInputText rectByBinding) {
        this.rectByBinding = rectByBinding;
    }

    public RichInputText getRectByBinding() {
        return rectByBinding;
    }

    public void setRectRemarksBinding(RichInputText rectRemarksBinding) {
        this.rectRemarksBinding = rectRemarksBinding;
    }

    public RichInputText getRectRemarksBinding() {
        return rectRemarksBinding;
    }

    public void setHandoverDateBinding(RichInputDate handoverDateBinding) {
        this.handoverDateBinding = handoverDateBinding;
    }

    public RichInputDate getHandoverDateBinding() {
        return handoverDateBinding;
    }

    public void setHandoverEntryDateBinding(RichInputDate handoverEntryDateBinding) {
        this.handoverEntryDateBinding = handoverEntryDateBinding;
    }

    public RichInputDate getHandoverEntryDateBinding() {
        return handoverEntryDateBinding;
    }

    public void setHandoverAcceptedByBinding(RichInputText handoverAcceptedByBinding) {
        this.handoverAcceptedByBinding = handoverAcceptedByBinding;
    }

    public RichInputText getHandoverAcceptedByBinding() {
        return handoverAcceptedByBinding;
    }

    public void setHandoverRemarkBinding(RichInputText handoverRemarkBinding) {
        this.handoverRemarkBinding = handoverRemarkBinding;
    }

    public RichInputText getHandoverRemarkBinding() {
        return handoverRemarkBinding;
    }

    public void setRectTimeBinding(RichInputText rectTimeBinding) {
        this.rectTimeBinding = rectTimeBinding;
    }

    public RichInputText getRectTimeBinding() {
        return rectTimeBinding;
    }

    public void setRectEntryTimeBinding(RichInputText rectEntryTimeBinding) {
        this.rectEntryTimeBinding = rectEntryTimeBinding;
    }

    public RichInputText getRectEntryTimeBinding() {
        return rectEntryTimeBinding;
    }

    public void setTargetTimeBinding(RichInputText targetTimeBinding) {
        this.targetTimeBinding = targetTimeBinding;
    }

    public RichInputText getTargetTimeBinding() {
        return targetTimeBinding;
    }

    public void setTargetEntryTimeBinding(RichInputText targetEntryTimeBinding) {
        this.targetEntryTimeBinding = targetEntryTimeBinding;
    }

    public RichInputText getTargetEntryTimeBinding() {
        return targetEntryTimeBinding;
    }

    public void setHandoverEntryTimeBinding(RichInputText handoverEntryTimeBinding) {
        this.handoverEntryTimeBinding = handoverEntryTimeBinding;
    }

    public RichInputText getHandoverEntryTimeBinding() {
        return handoverEntryTimeBinding;
    }

    public void setHandoverTimeBinding(RichInputText handoverTimeBinding) {
        this.handoverTimeBinding = handoverTimeBinding;
    }

    public RichInputText getHandoverTimeBinding() {
        return handoverTimeBinding;
    }

    public void setTargetRemarksBinding(RichInputText targetRemarksBinding) {
        this.targetRemarksBinding = targetRemarksBinding;
    }

    public RichInputText getTargetRemarksBinding() {
        return targetRemarksBinding;
    }

    public void setMachineCodeBinding(RichInputText machineCodeBinding) {
        this.machineCodeBinding = machineCodeBinding;
    }

    public RichInputText getMachineCodeBinding() {
        return machineCodeBinding;
    }

    public void setComplainTimeBinding(RichInputText complainTimeBinding) {
        this.complainTimeBinding = complainTimeBinding;
    }

    public RichInputText getComplainTimeBinding() {
        return complainTimeBinding;
    }

    public void setComplainByBinding(RichInputComboboxListOfValues complainByBinding) {
        this.complainByBinding = complainByBinding;
    }

    public RichInputComboboxListOfValues getComplainByBinding() {
        return complainByBinding;
    }

    public void methodAction() {
        System.out.println("Before Create");
        ADFUtils.findOperation("CreateInsert").execute();
        ADFUtils.findOperation("CreateInsert1").execute();
        ADFUtils.findOperation("CreateInsert5").execute();
        ADFUtils.findOperation("CreateInsert3").execute();
        ADFUtils.findOperation("CreateInsert4").execute();
        
        System.out.println("Create Insert " + "CreateInsert1 " + "CreateInsert2 " + "CreateInsert3 " + "CreateInsert4 ");

    }

    public void setDeptCodeBinding(RichInputComboboxListOfValues deptCodeBinding) {
        this.deptCodeBinding = deptCodeBinding;
    }

    public RichInputComboboxListOfValues getDeptCodeBinding() {
        return deptCodeBinding;
    }

    public void setDeptNameBinding(RichInputText deptNameBinding) {
        this.deptNameBinding = deptNameBinding;
    }

    public RichInputText getDeptNameBinding() {
        return deptNameBinding;
    }

    public void setHandoverByBinding(RichInputComboboxListOfValues handoverByBinding) {
        this.handoverByBinding = handoverByBinding;
    }

    public RichInputComboboxListOfValues getHandoverByBinding() {
        return handoverByBinding;
    }

    public void setHndOvrDateBinding(RichInputDate hndOvrDateBinding) {
        this.hndOvrDateBinding = hndOvrDateBinding;
    }

    public RichInputDate getHndOvrDateBinding() {
        return hndOvrDateBinding;
    }

    public void FaultCodeVl(ValueChangeEvent change) {
        change.getComponent().processUpdates(FacesContext.getCurrentInstance());
                System.out.println("change.getNewValue() "+change.getNewValue());
                if(change.getNewValue().equals("O"))
                {
                System.out.println("Choice 3 " +change.getNewValue());
                getFltOtherBinding().setDisabled(false);
                }
                else
                {
                getFltOtherBinding().setDisabled(true);
                }
                
        AdfFacesContext.getCurrentInstance().addPartialTarget(fltOtherBinding);
    }

    public void setFltOtherBinding(RichInputText fltOtherBinding) {
        this.fltOtherBinding = fltOtherBinding;
    }

    public RichInputText getFltOtherBinding() {
        return fltOtherBinding;
    }

    public void setTagNoBinding(RichInputComboboxListOfValues tagNoBinding) {
        this.tagNoBinding = tagNoBinding;
    }

    public RichInputComboboxListOfValues getTagNoBinding() {
        return tagNoBinding;
    }

    public void uploadAction(ValueChangeEvent vce) {
        System.out.println("inside uploadAction #### ");
                    //Get File Object from VC Event
                    UploadedFile fileVal = (UploadedFile) vce.getNewValue();
                    //Upload File to path- Return actual server path
                    String path = ADFUtils.uploadFile(fileVal);
                      System.out.println(fileVal.getContentType());
        //                      System.out.println("Content Type==>>"+fileVal.getContentType()+"Path==>>"+path+"File name==>>"+fileVal.getFilename());
                    //Method to insert data in table to keep track of uploaded files
                    OperationBinding ob = ADFUtils.findOperation("setFileDataForBreakdownComplaint");
                    ob.getParamsMap().put("name", fileVal.getFilename());
                    ob.getParamsMap().put("path", path);
                    ob.getParamsMap().put("contTyp", fileVal.getContentType());
                    ob.getParamsMap().put("SrNo", ADFUtils.evaluateEL("#{bindings.DocAttachRefDtlVO1Iterator.estimatedRowCount+1}"));
                    ob.execute();
                    // Reset inputFile component after upload
                    ResetUtils.reset(vce.getComponent());
    }

    public void setDocSeqNoBinding(RichInputText docSeqNoBinding) {
        this.docSeqNoBinding = docSeqNoBinding;
    }

    public RichInputText getDocSeqNoBinding() {
        return docSeqNoBinding;
    }

    public void setUnitCdRefDocBinding(RichInputText unitCdRefDocBinding) {
        this.unitCdRefDocBinding = unitCdRefDocBinding;
    }

    public RichInputText getUnitCdRefDocBinding() {
        return unitCdRefDocBinding;
    }

    public void setRefDocNoBinding(RichInputText refDocNoBinding) {
        this.refDocNoBinding = refDocNoBinding;
    }

    public RichInputText getRefDocNoBinding() {
        return refDocNoBinding;
    }

    public void setRefDocTypeBinding(RichInputText refDocTypeBinding) {
        this.refDocTypeBinding = refDocTypeBinding;
    }

    public RichInputText getRefDocTypeBinding() {
        return refDocTypeBinding;
    }

    public void setDocFileNameBinding(RichInputText docFileNameBinding) {
        this.docFileNameBinding = docFileNameBinding;
    }

    public RichInputText getDocFileNameBinding() {
        return docFileNameBinding;
    }

    public void setPathBinding(RichInputText pathBinding) {
        this.pathBinding = pathBinding;
    }

    public RichInputText getPathBinding() {
        return pathBinding;
    }

    public void setRefDocDateBinding(RichInputDate refDocDateBinding) {
        this.refDocDateBinding = refDocDateBinding;
    }

    public RichInputDate getRefDocDateBinding() {
        return refDocDateBinding;
    }

    public void setRemarksBinding(RichInputText remarksBinding) {
        this.remarksBinding = remarksBinding;
    }

    public RichInputText getRemarksBinding() {
        return remarksBinding;
    }

    public void setReferenceDocumentTableBinding(RichTable referenceDocumentTableBinding) {
        this.referenceDocumentTableBinding = referenceDocumentTableBinding;
    }

    public RichTable getReferenceDocumentTableBinding() {
        return referenceDocumentTableBinding;
    }

    public void downloadAction(FacesContext facesContext, OutputStream outputStream) throws IOException {
        File filed = new File(pathBinding.getValue().toString());
                               FileInputStream fis;
                               byte[] b;
                               try {
                                   fis = new FileInputStream(filed);

                                   int n;
                                   while ((n = fis.available()) > 0) {
                                       b = new byte[n];
                                       int result = fis.read(b);
                                       outputStream.write(b, 0, b.length);
                                       if (result == -1)
                                           break;
                                   }
                               } catch (Exception e) {
                                   e.printStackTrace();
                               }
                               outputStream.flush();

    }

    public void deletePopupDLReferenceDocument(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                    {
                    ADFUtils.findOperation("Delete").execute();
                    //ADFUtils.findOperation("Commit").execute();    //Unable on Search page.
                    System.out.println("Record Delete Successfully");
                   
                        FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                        FacesContext fc = FacesContext.getCurrentInstance();  
                        fc.addMessage(null, Message);  
                   
                    }

              AdfFacesContext.getCurrentInstance().addPartialTarget(referenceDocumentTableBinding);

    }

    public void setReferenceDocumentTabBinding(RichShowDetailItem referenceDocumentTabBinding) {
        this.referenceDocumentTabBinding = referenceDocumentTabBinding;
    }

    public RichShowDetailItem getReferenceDocumentTabBinding() {
        return referenceDocumentTabBinding;
    }

    public void setBreakdownComplaintTabBinding(RichShowDetailItem breakdownComplaintTabBinding) {
        this.breakdownComplaintTabBinding = breakdownComplaintTabBinding;
    }

    public RichShowDetailItem getBreakdownComplaintTabBinding() {
        return breakdownComplaintTabBinding;
    }

    public void setDownloadLinkBinding(RichCommandLink downloadLinkBinding) {
        this.downloadLinkBinding = downloadLinkBinding;
    }

    public RichCommandLink getDownloadLinkBinding() {
        return downloadLinkBinding;
    }
}
