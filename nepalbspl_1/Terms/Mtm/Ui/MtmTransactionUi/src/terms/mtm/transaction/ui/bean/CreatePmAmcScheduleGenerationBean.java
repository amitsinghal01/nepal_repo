package terms.mtm.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class CreatePmAmcScheduleGenerationBean {
    private RichOutputText bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichButton detailCreateBinding;
    private RichButton detailDeleteBinding;
    private RichInputComboboxListOfValues unitCdBinding;
    private RichInputDate scheduleDateBinding;
    private RichInputDate periodFromBinding;
    private RichInputDate periodToBinding;
    private RichInputText unitNameBinding;
    private RichInputText scheduleNoBinding;
    private RichInputText mlMachineCodeBinding;
    private RichInputText machineNameBinding;
    private RichInputText locationBinding;
    private RichSelectOneChoice pmTypeBinding;
    private RichSelectOneChoice schApprovYnBinding;
    private RichTable pmAmcScheduleGenerationTableBinding;
    private RichInputText chkptBinding;
    private RichInputText cmDescBinding;

    public CreatePmAmcScheduleGenerationBean() {
    }

    public void recordSavePmAmcScheduleGeneration(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedBy("PmAmcScheduleGenerationHeaderVO1Iterator","LastUpdatedBy");
        OperationBinding op = ADFUtils.findOperation("generatePmAmcNo");
               Object rst = op.execute();
             
               System.out.println("--------Commit-------");
                   System.out.println("value aftr getting result: "+rst);
                   
               
                   if (rst != null && !rst.equals("N")) {
                       if (op.getErrors().isEmpty()) {
                           ADFUtils.findOperation("Commit").execute();
                           FacesMessage Message = new FacesMessage("Record Saved Successfully. New Pm Amc Schedule No is: "+rst+".");   
                           Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                           FacesContext fc = FacesContext.getCurrentInstance();   
                           fc.addMessage(null, Message);  
               
                       }
                   }
                   else
                   {
                       if (op.getErrors().isEmpty())
                       {
                           ADFUtils.findOperation("Commit").execute();
                           FacesMessage Message = new FacesMessage("Record Update Successfully.");   
                           Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                           FacesContext fc = FacesContext.getCurrentInstance();   
                           fc.addMessage(null, Message);  
                   
                       }

                   }
        AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
//        OperationBinding op = ADFUtils.findOperation("generatePmAmcNo");
//        Object rst = op.execute();
//        String param=resolvEl("#{pageFlowScope.mode=='E'}");
//                System.out.println("Mode is ====> "+param);
//                if(param.equalsIgnoreCase("true"))
//                {
//                   FacesMessage Message = new FacesMessage("Record Update Successfully");  
//                   Message.setSeverity(FacesMessage.SEVERITY_INFO);  
//                   FacesContext fc = FacesContext.getCurrentInstance();  
//                   fc.addMessage(null, Message);     
//                   
//                }
//                else
//                {
//                    FacesMessage Message = new FacesMessage("Record Save Successfully");  
//                    Message.setSeverity(FacesMessage.SEVERITY_INFO);  
//                    FacesContext fc = FacesContext.getCurrentInstance();  
//                    fc.addMessage(null, Message);     
//                }
    }

    private String resolvEl(String data) {
        FacesContext fc = FacesContext.getCurrentInstance();
        Application app = fc.getApplication();
        ExpressionFactory elFactory = app.getExpressionFactory();
        ELContext elContext = fc.getELContext();
        ValueExpression valueExp = elFactory.createValueExpression(elContext, data, Object.class);
        String Message=valueExp.getValue(elContext).toString();
        return Message;
    }

    public void populateDataPmAmcScheduleGeneration(ActionEvent actionEvent) {
        
          OperationBinding op = ADFUtils.findOperation("populateDataPmAmcScheduleGeneration1");
          Object obj = op.execute();
          
          System.out.println("Result is: "+op.getResult());
          
          if(op.getResult().equals("Y"))
          {
                OperationBinding op1 = ADFUtils.findOperation("populateDataPmAmcScheduleGeneration");
                op1.execute();
          }
        
          else
          {
                ADFUtils.showMessage("A Schedule is already generated for these Dates, Please select another.", 0);
          }
     }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
            try {
                Method method1 =
                    component.getClass().getMethod("setDisabled", boolean.class);
                if (method1 != null) {
                    method1.invoke(component, valueComponent);
                }
            } catch (NoSuchMethodException e) {
                try {
                    Method method =
                        component.getClass().getMethod("setReadOnly", boolean.class);
                    if (method != null) {
                        method.invoke(component, valueComponent);
                    }
                } catch (Exception e1) {
                    // e.printStackTrace();//silently eat this exception.
                }


            } catch (Exception e) {
                // e.printStackTrace();//silently eat this exception.
            }
            List<UIComponent> childComponents = component.getChildren();
            for (UIComponent comp : childComponents) {
                makeComponentHierarchyReadOnly(comp, valueComponent);
            }
        }
    
    private void cevmodecheck(){
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }
    
    public void cevModeDisableComponent(String mode) {
    
        if (mode.equals("E")) {
            getDetailCreateBinding().setDisabled(true);
              getUnitCdBinding().setDisabled(true);
              getUnitNameBinding().setDisabled(true);
              getScheduleNoBinding().setDisabled(true);
              getMlMachineCodeBinding().setDisabled(true);
              getMachineNameBinding().setDisabled(true);
              getLocationBinding().setDisabled(true);   
              getPmTypeBinding().setDisabled(true);
              getSchApprovYnBinding().setDisabled(true);
              getHeaderEditBinding().setDisabled(true);
              getChkptBinding().setDisabled(true);
              getCmDescBinding().setDisabled(true);
             
             
        } else if (mode.equals("C")) {
              getUnitCdBinding().setDisabled(true);
              getDetailCreateBinding().setDisabled(true);
              getDetailDeleteBinding().setDisabled(false);
              getUnitNameBinding().setDisabled(true);
              getScheduleNoBinding().setDisabled(true);
              getMlMachineCodeBinding().setDisabled(true);
              getMachineNameBinding().setDisabled(true);
              getLocationBinding().setDisabled(true);
              getPmTypeBinding().setDisabled(true);
              getSchApprovYnBinding().setDisabled(true);
              getHeaderEditBinding().setDisabled(true);
              getChkptBinding().setDisabled(true);
              getCmDescBinding().setDisabled(true);
             
            
            
        } else if (mode.equals("V")) {
              getDetailCreateBinding().setDisabled(true);
            
        }
    }

    public void EditButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setDetailCreateBinding(RichButton detailCreateBinding) {
        this.detailCreateBinding = detailCreateBinding;
    }

    public RichButton getDetailCreateBinding() {
        return detailCreateBinding;
    }

    public void setDetailDeleteBinding(RichButton detailDeleteBinding) {
        this.detailDeleteBinding = detailDeleteBinding;
    }

    public RichButton getDetailDeleteBinding() {
        return detailDeleteBinding;
    }

    public void setUnitCdBinding(RichInputComboboxListOfValues unitCdBinding) {
        this.unitCdBinding = unitCdBinding;
    }

    public RichInputComboboxListOfValues getUnitCdBinding() {
        return unitCdBinding;
    }

    public void setScheduleDateBinding(RichInputDate scheduleDateBinding) {
        this.scheduleDateBinding = scheduleDateBinding;
    }

    public RichInputDate getScheduleDateBinding() {
        return scheduleDateBinding;
    }

    public void setPeriodFromBinding(RichInputDate periodFromBinding) {
        this.periodFromBinding = periodFromBinding;
    }

    public RichInputDate getPeriodFromBinding() {
        return periodFromBinding;
    }

    public void setPeriodToBinding(RichInputDate periodToBinding) {
        this.periodToBinding = periodToBinding;
    }

    public RichInputDate getPeriodToBinding() {
        return periodToBinding;
    }

    public void setUnitNameBinding(RichInputText unitNameBinding) {
        this.unitNameBinding = unitNameBinding;
    }

    public RichInputText getUnitNameBinding() {
        return unitNameBinding;
    }

    public void setScheduleNoBinding(RichInputText scheduleNoBinding) {
        this.scheduleNoBinding = scheduleNoBinding;
    }

    public RichInputText getScheduleNoBinding() {
        return scheduleNoBinding;
    }

    public void setMlMachineCodeBinding(RichInputText mlMachineCodeBinding) {
        this.mlMachineCodeBinding = mlMachineCodeBinding;
    }

    public RichInputText getMlMachineCodeBinding() {
        return mlMachineCodeBinding;
    }

    public void setMachineNameBinding(RichInputText machineNameBinding) {
        this.machineNameBinding = machineNameBinding;
    }

    public RichInputText getMachineNameBinding() {
        return machineNameBinding;
    }

    public void setLocationBinding(RichInputText locationBinding) {
        this.locationBinding = locationBinding;
    }

    public RichInputText getLocationBinding() {
        return locationBinding;
    }

    public void setPmTypeBinding(RichSelectOneChoice pmTypeBinding) {
        this.pmTypeBinding = pmTypeBinding;
    }

    public RichSelectOneChoice getPmTypeBinding() {
        return pmTypeBinding;
    }

    public void setSchApprovYnBinding(RichSelectOneChoice schApprovYnBinding) {
        this.schApprovYnBinding = schApprovYnBinding;
    }

    public RichSelectOneChoice getSchApprovYnBinding() {
        return schApprovYnBinding;
    }

    public void deletePopupDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                    {
                    ADFUtils.findOperation("Delete1").execute();
                    //ADFUtils.findOperation("Commit").execute();    
                    System.out.println("Record Delete Successfully");
                   
                        FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                        FacesContext fc = FacesContext.getCurrentInstance();  
                        fc.addMessage(null, Message);  
                   
                    }

                AdfFacesContext.getCurrentInstance().addPartialTarget(pmAmcScheduleGenerationTableBinding);

    }

    public void setPmAmcScheduleGenerationTableBinding(RichTable pmAmcScheduleGenerationTableBinding) {
        this.pmAmcScheduleGenerationTableBinding = pmAmcScheduleGenerationTableBinding;
    }

    public RichTable getPmAmcScheduleGenerationTableBinding() {
        return pmAmcScheduleGenerationTableBinding;
    }

    public void periodFromValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        // Add event code here...

    }

    public void periodToValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        // Add event code here...

    }

    public void setChkptBinding(RichInputText chkptBinding) {
        this.chkptBinding = chkptBinding;
    }

    public RichInputText getChkptBinding() {
        return chkptBinding;
    }

    public void setCmDescBinding(RichInputText cmDescBinding) {
        this.cmDescBinding = cmDescBinding;
    }

    public RichInputText getCmDescBinding() {
        return cmDescBinding;
    }

}
