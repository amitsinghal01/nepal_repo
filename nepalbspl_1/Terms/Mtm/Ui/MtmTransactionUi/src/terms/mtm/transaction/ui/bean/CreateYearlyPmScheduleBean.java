package terms.mtm.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class CreateYearlyPmScheduleBean {
    private RichTable createYearlyPmSchdDtlTableBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichOutputText bindingOutputText;
    private RichButton detailCreateBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText scheduleNoBinding;
    private RichButton detailDeleteBinding;
    private RichInputDate periodFromBinding;
    private RichInputDate periodToBinding;
    private RichInputComboboxListOfValues finYearBinding;

    public CreateYearlyPmScheduleBean() {
    }

    public void getScheduleNoYearlyPmSchedule(ActionEvent actionEvent) {
    ADFUtils.setLastUpdatedBy("YearlyPmScheduleHeaderVO1Iterator","LastUpdatedBy");
        ADFUtils.findOperation("getSequenceNoYearlyPmSchedule").execute();
        
        Integer TransValue = (Integer)bindingOutputText.getValue();
        System.out.println("Trans Value: " + TransValue);
        
        if(TransValue==0) {
            ADFUtils.findOperation("Commit").execute();
            FacesMessage Message = new FacesMessage("Record Save Successfully, New Yearly PM Schedule Generated is: "+scheduleNoBinding.getValue());   
            Message.setSeverity(FacesMessage.SEVERITY_INFO);   
            FacesContext fc = FacesContext.getCurrentInstance();   
            fc.addMessage(null, Message);    
        }
        else {
            ADFUtils.findOperation("Commit").execute();
            FacesMessage Message = new FacesMessage("Record Update Successfully");   
            Message.setSeverity(FacesMessage.SEVERITY_INFO);   
            FacesContext fc = FacesContext.getCurrentInstance();   
            fc.addMessage(null, Message);      
        }
//        OperationBinding op = ADFUtils.findOperation("getSequenceNoYearlyPmSchedule");
//        Object rst = op.execute();
//        
//        System.out.println("--------Commit-------");
//            System.out.println("value aftr getting result--->?"+rst);
//            
//        
//            if (rst != null && !rst.equals("N")) {
//                if (op.getErrors().isEmpty()) {
//                    ADFUtils.findOperation("Commit").execute();
//                    FacesMessage Message = new FacesMessage("Record Saved Successfully. New Pm Amc Schedule No is "+rst+".");   
//                    Message.setSeverity(FacesMessage.SEVERITY_INFO);   
//                    FacesContext fc = FacesContext.getCurrentInstance();   
//                    fc.addMessage(null, Message);  
//        
//                }
//            }
//            else
//            {
//                if (op.getErrors().isEmpty())
//                {
//                    ADFUtils.findOperation("Commit").execute();
//                    FacesMessage Message = new FacesMessage("Record Update Successfully.");   
//                    Message.setSeverity(FacesMessage.SEVERITY_INFO);   
//                    FacesContext fc = FacesContext.getCurrentInstance();   
//                    fc.addMessage(null, Message);  
//            
//                }
//
//            }
//        ADFUtils.findOperation("getSequenceNoYearlyPmSchedule").execute();
}

    public void deletePopupDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                    {
                    ADFUtils.findOperation("Delete").execute();
                    //ADFUtils.findOperation("Commit").execute();    
                    System.out.println("Record Delete Successfully");
                   
                        FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                        FacesContext fc = FacesContext.getCurrentInstance();  
                        fc.addMessage(null, Message);  
                   
                    }

                AdfFacesContext.getCurrentInstance().addPartialTarget(createYearlyPmSchdDtlTableBinding);

    }

    public void setCreateYearlyPmSchdDtlTableBinding(RichTable createYearlyPmSchdDtlTableBinding) {
        this.createYearlyPmSchdDtlTableBinding = createYearlyPmSchdDtlTableBinding;
    }

    public RichTable getCreateYearlyPmSchdDtlTableBinding() {
        return createYearlyPmSchdDtlTableBinding;
    }

    public void populatePmAmcDataYearlyPmSchedule(ActionEvent actionEvent) {
        OperationBinding op1=ADFUtils.findOperation("GenrateProcedureCalling");
        op1.getParamsMap().put("UnitCd", unitCodeBinding.getValue());
        op1.getParamsMap().put("FinYear", finYearBinding.getValue());
        op1.getParamsMap().put("PeriodFrom", periodFromBinding.getValue());
        op1.getParamsMap().put("PeriodTo", periodToBinding.getValue());
        op1.execute();
        OperationBinding op2=ADFUtils.findOperation("populateDataYearlyPmSchedule");
        op2.execute();
//        ADFUtils.findOperation("populateDataYearlyPmSchedule").execute();
    }

    public void EditButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
            try {
                Method method1 =
                    component.getClass().getMethod("setDisabled", boolean.class);
                if (method1 != null) {
                    method1.invoke(component, valueComponent);
                }
            } catch (NoSuchMethodException e) {
                try {
                    Method method =
                        component.getClass().getMethod("setReadOnly", boolean.class);
                    if (method != null) {
                        method.invoke(component, valueComponent);
                    }
                } catch (Exception e1) {
                    // e.printStackTrace();//silently eat this exception.
                }


            } catch (Exception e) {
                // e.printStackTrace();//silently eat this exception.
            }
            List<UIComponent> childComponents = component.getChildren();
            for (UIComponent comp : childComponents) {
                makeComponentHierarchyReadOnly(comp, valueComponent);
            }
        }
    
    private void cevmodecheck(){
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }
    
    public void cevModeDisableComponent(String mode) {
    
        if (mode.equals("E")) {
            
              getUnitCodeBinding().setDisabled(true);
              getScheduleNoBinding().setDisabled(true);
//              getMachineCodeBinding().setDisabled(true);
//              getLocationBinding().setDisabled(true);
//              getPmTypeBinding().setDisabled(true);
              getPeriodFromBinding().setDisabled(true);
              getPeriodToBinding().setDisabled(true);
              getHeaderEditBinding().setDisabled(true);
              getDetailCreateBinding().setDisabled(true);
             
        } else if (mode.equals("C")) {
            getUnitCodeBinding().setDisabled(true);
            getScheduleNoBinding().setDisabled(true);
            getPeriodFromBinding().setDisabled(true);
            getPeriodToBinding().setDisabled(true);
//            getMachineCodeBinding().setDisabled(true);
//            getLocationBinding().setDisabled(true);
//            getPmTypeBinding().setDisabled(true);
            getDetailCreateBinding().setDisabled(true);
            getDetailDeleteBinding().setDisabled(false);
//            getDetailCreateBinding().setDisabled(true);
              getHeaderEditBinding().setDisabled(true);
              
        } else if (mode.equals("V")) {
              getDetailCreateBinding().setDisabled(true);
              getDetailDeleteBinding().setDisabled(true);
        }
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setDetailCreateBinding(RichButton detailCreateBinding) {
        this.detailCreateBinding = detailCreateBinding;
    }

    public RichButton getDetailCreateBinding() {
        return detailCreateBinding;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setScheduleNoBinding(RichInputText scheduleNoBinding) {
        this.scheduleNoBinding = scheduleNoBinding;
    }

    public RichInputText getScheduleNoBinding() {
        return scheduleNoBinding;
    }

    public void setDetailDeleteBinding(RichButton detailDeleteBinding) {
        this.detailDeleteBinding = detailDeleteBinding;
    }

    public RichButton getDetailDeleteBinding() {
        return detailDeleteBinding;
    }

    public void setPeriodFromBinding(RichInputDate periodFromBinding) {
        this.periodFromBinding = periodFromBinding;
    }

    public RichInputDate getPeriodFromBinding() {
        return periodFromBinding;
    }

    public void setPeriodToBinding(RichInputDate periodToBinding) {
        this.periodToBinding = periodToBinding;
    }

    public RichInputDate getPeriodToBinding() {
        return periodToBinding;
    }

    public void setFinYearBinding(RichInputComboboxListOfValues finYearBinding) {
        this.finYearBinding = finYearBinding;
    }

    public RichInputComboboxListOfValues getFinYearBinding() {
        return finYearBinding;
    }
}
