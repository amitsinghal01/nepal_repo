package terms.mtm.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

public class DGLogBookBean {
    private RichTable dgLogBookTableBinding;
    private RichButton headerEditButton;
    private RichPanelHeader getMyPageRootComponent;
    private RichOutputText outputTextBinding;
    private RichButton detailcreateBinding;
    private RichInputText srNoBinding;
    private RichInputComboboxListOfValues unitCodeBinding;

    public DGLogBookBean() {
    }

    public void deletepopupdialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                    {
                    ADFUtils.findOperation("Delete").execute();
                    System.out.println("Record Delete Successfully");
                   
                        FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                        FacesContext fc = FacesContext.getCurrentInstance();  
                        fc.addMessage(null, Message);  
                   
                    }
            AdfFacesContext.getCurrentInstance().addPartialTarget(dgLogBookTableBinding);
        }

    public void setDgLogBookTableBinding(RichTable dgLogBookTableBinding) {
        this.dgLogBookTableBinding = dgLogBookTableBinding;
    }

    public RichTable getDgLogBookTableBinding() {
        return dgLogBookTableBinding;
    }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setHeaderEditButton(RichButton headerEditButton) {
        this.headerEditButton = headerEditButton;
    }

    public RichButton getHeaderEditButton() {
        return headerEditButton;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setOutputTextBinding(RichOutputText outputTextBinding) {
        this.outputTextBinding = outputTextBinding;
    }

    public RichOutputText getOutputTextBinding() {
        cevmodecheck();
        return outputTextBinding;
    }

    public void setDetailcreateBinding(RichButton detailcreateBinding) {
        this.detailcreateBinding = detailcreateBinding;
    }

    public RichButton getDetailcreateBinding() {
        return detailcreateBinding;
    }
    private void cevmodecheck(){
          if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
              makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
              cevModeDisableComponent("V");
          }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
              makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
              cevModeDisableComponent("C");
          }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
              makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
              cevModeDisableComponent("E");
          }
      }
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
               try {
                   Method method1 =
                       component.getClass().getMethod("setDisabled", boolean.class);
                   if (method1 != null) {
                       method1.invoke(component, valueComponent);
                   }
               } catch (NoSuchMethodException e) {
                   try {
                       Method method =
                           component.getClass().getMethod("setReadOnly", boolean.class);
                       if (method != null) {
                           method.invoke(component, valueComponent);
                       }
                   } catch (Exception e1) {
                       // e.printStackTrace();//silently eat this exception.
                   }


               } catch (Exception e) {
                   // e.printStackTrace();//silently eat this exception.
               }
               List<UIComponent> childComponents = component.getChildren();
               for (UIComponent comp : childComponents) {
                   makeComponentHierarchyReadOnly(comp, valueComponent);
               }
           }
    public void cevModeDisableComponent(String mode) {
        //            FacesContext fctx = FacesContext.getCurrentInstance();
        //            ELContext elctx = fctx.getELContext();
        //            Application jsfApp = fctx.getApplication();
        //            //create a ValueExpression that points to the ADF binding layer
        //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
        //            //
        //            ValueExpression valueExpr = exprFactory.createValueExpression(
        //                                         elctx,
        //                                         "#{pageFlowScope.mode=='E'}",
        //                                          Object.class
        //                                         );
        //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
            if (mode.equals("E")) {
                  getUnitCodeBinding().setDisabled(true);
                    getDetailcreateBinding().setDisabled(false);
            } else if (mode.equals("C")) {
                 getUnitCodeBinding().setDisabled(true);
              getHeaderEditButton().setDisabled(true);
                getDetailcreateBinding().setDisabled(false);
            } else if (mode.equals("V")) {
                getDetailcreateBinding().setDisabled(true);
            }
            
        }

    public void detailCreate(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert").execute();
        srNoBinding.setValue((ADFUtils.evaluateEL("#{bindings.DGLogBookDetailVO1Iterator.estimatedRowCount}")));
    }

    public void setSrNoBinding(RichInputText srNoBinding) {
        this.srNoBinding = srNoBinding;
    }

    public RichInputText getSrNoBinding() {
        return srNoBinding;
    }

    public void SaveAL(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedBy("DGLogBookHeaderVO1Iterator","LastUpdatedBy");
        Integer i=0;
        i=(Integer)outputTextBinding.getValue();
        System.out.println("EDIT TRANS VALUE"+i);
        if(i.equals(0))
        {
           FacesMessage Message = new FacesMessage("Record Save Successfully");   
           Message.setSeverity(FacesMessage.SEVERITY_INFO);   
           FacesContext fc = FacesContext.getCurrentInstance();   
           fc.addMessage(null, Message);      
            
        }
        else 
        {
            FacesMessage Message = new FacesMessage("Record Updated Successfully");   
            Message.setSeverity(FacesMessage.SEVERITY_INFO);   
            FacesContext fc = FacesContext.getCurrentInstance();   
            fc.addMessage(null, Message);      
        }
            
        
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }
}

