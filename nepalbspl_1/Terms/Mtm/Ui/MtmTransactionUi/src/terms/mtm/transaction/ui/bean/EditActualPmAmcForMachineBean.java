package terms.mtm.transaction.ui.bean;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;

import java.lang.reflect.Method;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import javax.naming.Context;
import javax.naming.InitialContext;

import javax.servlet.http.HttpServletResponse;

import javax.sql.DataSource;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

public class EditActualPmAmcForMachineBean {
    private RichOutputText bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichButton detailcreateBinding;
    private RichButton detaildeleteBinding;
    private RichTable pmScheduleDetailTableBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText scheduleNoBinding;
    private RichInputDate periodFromBinding;
    private RichInputDate scheduleDateBinding;
    private RichInputDate periodToBinding;
    private RichButton detailOptionButtonBinding;
    private RichSelectOneChoice pmTypeBinding;
    private RichInputDate startDateBinding;
    private RichInputDate startTimeBinding;
    private RichInputText noOfElectricalBinding;
    private RichInputText hrsMechanicalBinding;
    private RichInputText yorNBinding;
    private RichInputComboboxListOfValues approvedByBinding;
    private RichInputText remarksBinding;
    private RichInputDate handoverDateTransBinding;
    private RichInputDate handoverTimeTransBinding;
    private RichInputText handoverRemarksTransBinding;
    private RichInputText receivedByTransBinding;
    private RichInputText rcvByDescBinding;
    private RichTable actualPmCheckPointTableBinding;
    private RichTable itemConsumptionTableBinding;
    private RichPopup showPopUpHandOver;
    private RichPopup showPopUpActualPm;
    private RichInputComboboxListOfValues handOverByBinding;
    private RichInputText hoverByBinding;
    private RichInputText pmBinding;
    private RichInputComboboxListOfValues pmDoneByBinding;
    private RichInputText pnDoneInOutBinding;
    private RichSelectOneChoice pmOutInBinding;
    private RichInputText slipBinding;
    private RichInputComboboxListOfValues tagNoBinding;
    private RichPopup itemConsumptionPopupBind;

    public EditActualPmAmcForMachineBean() {
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setDetailcreateBinding(RichButton detailcreateBinding) {
        this.detailcreateBinding = detailcreateBinding;
    }

    public RichButton getDetailcreateBinding() {
        return detailcreateBinding;
    }

    public void setDetaildeleteBinding(RichButton detaildeleteBinding) {
        this.detaildeleteBinding = detaildeleteBinding;
    }

    public RichButton getDetaildeleteBinding() {
        return detaildeleteBinding;
    }

    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }


    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }


    //Set Fields and Button disable.
    public void cevModeDisableComponent(String mode) {
        //            FacesContext fctx = FacesContext.getCurrentInstance();
        //            ELContext elctx = fctx.getELContext();
        //            Application jsfApp = fctx.getApplication();
        //            //create a ValueExpression that points to the ADF binding layer
        //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
        //            //
        //            ValueExpression valueExpr = exprFactory.createValueExpression(
        //                                         elctx,
        //                                         "#{pageFlowScope.mode=='E'}",
        //                                          Object.class
        //                                         );
        //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
        if (mode.equals("E")) {
            getDetailcreateBinding().setDisabled(true);
            getDetaildeleteBinding().setDisabled(false);
            getHeaderEditBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
            getScheduleNoBinding().setDisabled(true);
            getPeriodFromBinding().setDisabled(true);
            getScheduleDateBinding().setDisabled(true);
            getPeriodToBinding().setDisabled(true);
            getPmTypeBinding().setDisabled(true);
            getStartDateBinding().setDisabled(true);
            getStartTimeBinding().setDisabled(true);
            getNoOfElectricalBinding().setDisabled(true);
            getHrsMechanicalBinding().setDisabled(true);
            getYorNBinding().setDisabled(true);
            getApprovedByBinding().setDisabled(true);
            getRemarksBinding().setDisabled(true);
            getHandoverDateTransBinding().setDisabled(true);
            getHandoverTimeTransBinding().setDisabled(true);
            getHandoverRemarksTransBinding().setDisabled(true);
            getReceivedByTransBinding().setDisabled(true);
            getRcvByDescBinding().setDisabled(true);
            getTagNoBinding().setDisabled(true);
            getSlipBinding().setDisabled(true);

        } else if (mode.equals("C")) {
            //                getDetailcreateBinding().setDisabled(false);
            getDetaildeleteBinding().setDisabled(false);

        } else if (mode.equals("V")) {
            getDetailcreateBinding().setDisabled(true);
            getDetaildeleteBinding().setDisabled(true);
        }

    }


    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }

        AdfFacesContext.getCurrentInstance().addPartialTarget(pmScheduleDetailTableBinding);


    }

    public void setPmScheduleDetailTableBinding(RichTable pmScheduleDetailTableBinding) {
        this.pmScheduleDetailTableBinding = pmScheduleDetailTableBinding;
    }

    public RichTable getPmScheduleDetailTableBinding() {
        return pmScheduleDetailTableBinding;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setScheduleNoBinding(RichInputText scheduleNoBinding) {
        this.scheduleNoBinding = scheduleNoBinding;
    }

    public RichInputText getScheduleNoBinding() {
        return scheduleNoBinding;
    }

    public void setPeriodFromBinding(RichInputDate periodFromBinding) {
        this.periodFromBinding = periodFromBinding;
    }

    public RichInputDate getPeriodFromBinding() {
        return periodFromBinding;
    }

    public void setScheduleDateBinding(RichInputDate scheduleDateBinding) {
        this.scheduleDateBinding = scheduleDateBinding;
    }

    public RichInputDate getScheduleDateBinding() {
        return scheduleDateBinding;
    }

    public void setPeriodToBinding(RichInputDate periodToBinding) {
        this.periodToBinding = periodToBinding;
    }

    public RichInputDate getPeriodToBinding() {
        return periodToBinding;
    }

    public void setDetailOptionButtonBinding(RichButton detailOptionButtonBinding) {
        this.detailOptionButtonBinding = detailOptionButtonBinding;
    }

    public RichButton getDetailOptionButtonBinding() {
        return detailOptionButtonBinding;
    }

    public void setPmTypeBinding(RichSelectOneChoice pmTypeBinding) {
        this.pmTypeBinding = pmTypeBinding;
    }

    public RichSelectOneChoice getPmTypeBinding() {
        return pmTypeBinding;
    }

    public void setStartDateBinding(RichInputDate startDateBinding) {
        this.startDateBinding = startDateBinding;
    }

    public RichInputDate getStartDateBinding() {
        return startDateBinding;
    }

    public void setStartTimeBinding(RichInputDate startTimeBinding) {
        this.startTimeBinding = startTimeBinding;
    }

    public RichInputDate getStartTimeBinding() {
        return startTimeBinding;
    }

    public void setNoOfElectricalBinding(RichInputText noOfElectricalBinding) {
        this.noOfElectricalBinding = noOfElectricalBinding;
    }

    public RichInputText getNoOfElectricalBinding() {
        return noOfElectricalBinding;
    }

    public void setHrsMechanicalBinding(RichInputText hrsMechanicalBinding) {
        this.hrsMechanicalBinding = hrsMechanicalBinding;
    }

    public RichInputText getHrsMechanicalBinding() {
        return hrsMechanicalBinding;
    }

    public void setYorNBinding(RichInputText yorNBinding) {
        this.yorNBinding = yorNBinding;
    }

    public RichInputText getYorNBinding() {
        return yorNBinding;
    }

    public void setApprovedByBinding(RichInputComboboxListOfValues approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputComboboxListOfValues getApprovedByBinding() {
        return approvedByBinding;
    }

    public void setRemarksBinding(RichInputText remarksBinding) {
        this.remarksBinding = remarksBinding;
    }

    public RichInputText getRemarksBinding() {
        return remarksBinding;
    }

    public void setHandoverDateTransBinding(RichInputDate handoverDateTransBinding) {
        this.handoverDateTransBinding = handoverDateTransBinding;
    }

    public RichInputDate getHandoverDateTransBinding() {
        return handoverDateTransBinding;
    }

    public void setHandoverTimeTransBinding(RichInputDate handoverTimeTransBinding) {
        this.handoverTimeTransBinding = handoverTimeTransBinding;
    }

    public RichInputDate getHandoverTimeTransBinding() {
        return handoverTimeTransBinding;
    }

    public void setHandoverRemarksTransBinding(RichInputText handoverRemarksTransBinding) {
        this.handoverRemarksTransBinding = handoverRemarksTransBinding;
    }

    public RichInputText getHandoverRemarksTransBinding() {
        return handoverRemarksTransBinding;
    }

    public void setReceivedByTransBinding(RichInputText receivedByTransBinding) {
        this.receivedByTransBinding = receivedByTransBinding;
    }

    public RichInputText getReceivedByTransBinding() {
        return receivedByTransBinding;
    }

    public void setRcvByDescBinding(RichInputText rcvByDescBinding) {
        this.rcvByDescBinding = rcvByDescBinding;
    }

    public RichInputText getRcvByDescBinding() {
        return rcvByDescBinding;
    }

    public void saveButtonAL(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedBy("ActualPmAmcMachineHeaderVO1Iterator", "LastUpdatedBy");
        ADFUtils.findOperation("Commit").execute();
        FacesMessage Message = new FacesMessage("Record Save And Update Successfully");
        Message.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
    }

    public void deletepopupCheckPointDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            oracle.adf.model.OperationBinding op = null;
            ADFUtils.findOperation("Delete1").execute();
            op = (oracle.adf.model.OperationBinding) ADFUtils.findOperation("Commit");
            Object rst = op.execute();
            System.out.println("Record Delete Successfully");
            if (op.getErrors().isEmpty()) {
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            } else if (!op.getErrors().isEmpty()) {
                oracle.adf.model.OperationBinding opr =
                    (oracle.adf.model.OperationBinding) ADFUtils.findOperation("Rollback");
                Object rstr = opr.execute();
                FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(actualPmCheckPointTableBinding);
    }

    public void deletePopupConsumptionDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            oracle.adf.model.OperationBinding op = null;
            ADFUtils.findOperation("Delete2").execute();
            op = (oracle.adf.model.OperationBinding) ADFUtils.findOperation("Commit");
            Object rst = op.execute();
            System.out.println("Record Delete Successfully");
            if (op.getErrors().isEmpty()) {
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            } else if (!op.getErrors().isEmpty()) {
                oracle.adf.model.OperationBinding opr =
                    (oracle.adf.model.OperationBinding) ADFUtils.findOperation("Rollback");
                Object rstr = opr.execute();
                FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(itemConsumptionTableBinding);
    }


    public void setActualPmCheckPointTableBinding(RichTable actualPmCheckPointTableBinding) {
        this.actualPmCheckPointTableBinding = actualPmCheckPointTableBinding;
    }

    public RichTable getActualPmCheckPointTableBinding() {
        return actualPmCheckPointTableBinding;
    }

    public void setItemConsumptionTableBinding(RichTable itemConsumptionTableBinding) {
        this.itemConsumptionTableBinding = itemConsumptionTableBinding;
    }

    public RichTable getItemConsumptionTableBinding() {
        return itemConsumptionTableBinding;
    }

    public void handOverButtonAL(ActionEvent actionEvent) {
        if (hoverByBinding.getValue() == null) {
            System.out.println("HandOver Binding Value: " + hoverByBinding.getValue());

            ADFUtils.showPopup(showPopUpHandOver);
            ADFUtils.findOperation("CreateInsert3").execute();
        } else {
            ADFUtils.showPopup(showPopUpHandOver);
        }

    }

    public void actualPmButtonAL(ActionEvent actionEvent) {
        //        ADFUtils.findOperation("CreateInsert4").execute();

        if (pnDoneInOutBinding.getValue() == null) {
            System.out.println("PM Binding Value: " + pnDoneInOutBinding.getValue());
            ADFUtils.showPopup(showPopUpActualPm);
            ADFUtils.findOperation("CreateInsert4").execute();
        } else {
            ADFUtils.showPopup(showPopUpActualPm);
        }

    }

    public void setShowPopUpHandOver(RichPopup showPopUpHandOver) {
        this.showPopUpHandOver = showPopUpHandOver;
    }

    public RichPopup getShowPopUpHandOver() {
        return showPopUpHandOver;
    }

    public void setShowPopUpActualPm(RichPopup showPopUpActualPm) {
        this.showPopUpActualPm = showPopUpActualPm;
    }

    public RichPopup getShowPopUpActualPm() {
        return showPopUpActualPm;
    }


    public void setHandOverByBinding(RichInputComboboxListOfValues handOverByBinding) {
        this.handOverByBinding = handOverByBinding;
    }

    public RichInputComboboxListOfValues getHandOverByBinding() {
        return handOverByBinding;
    }

    public void setHoverByBinding(RichInputText hoverByBinding) {
        this.hoverByBinding = hoverByBinding;
    }

    public RichInputText getHoverByBinding() {
        return hoverByBinding;
    }

    public void setPnDoneInOutBinding(RichInputText pnDoneInOutBinding) {
        this.pnDoneInOutBinding = pnDoneInOutBinding;
    }

    public RichInputText getPnDoneInOutBinding() {
        return pnDoneInOutBinding;
    }

    public void setPmOutInBinding(RichSelectOneChoice pmOutInBinding) {
        this.pmOutInBinding = pmOutInBinding;
    }

    public RichSelectOneChoice getPmOutInBinding() {
        return pmOutInBinding;
    }

    public void currentStockVCL(ValueChangeEvent vce) {
        System.out.println("Inside Current Stock ######");
        if (vce != null) {
            System.out.println("Inside vce ######");
            vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
            ADFUtils.findOperation("getCurrentStockActualPmAmcItemConsumption").execute();
        }
    }


    public void setSlipBinding(RichInputText slipBinding) {
        this.slipBinding = slipBinding;
    }

    public RichInputText getSlipBinding() {
        return slipBinding;
    }

    public void endDateVCE(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        OperationBinding opr = ADFUtils.findOperation("ComplailTargetData");
        opr.execute();
        if (opr.getResult() != null) {
            if (opr.getResult().equals("Y")) {
                slipBinding.setDisabled(false);
            } else {
                slipBinding.setDisabled(true);
            }
            AdfFacesContext.getCurrentInstance().addPartialTarget(slipBinding);
        }
    }

    public void setTagNoBinding(RichInputComboboxListOfValues tagNoBinding) {
        this.tagNoBinding = tagNoBinding;
    }

    public RichInputComboboxListOfValues getTagNoBinding() {
        return tagNoBinding;
    }

    public void CheckItemAL(ActionEvent actionEvent) {
        System.out.println("Now Running ;)");
        ADFUtils.showPopup(itemConsumptionPopupBind);
        if ((Long) ADFUtils.evaluateEL("#{bindings.ActualPmAmcMachineActualPmItemConsumptionVO1Iterator.estimatedRowCount}") ==
            0) {
            ADFUtils.findOperation("MachineSpareCodeItemCode").execute();
        }
    }

    public void setItemConsumptionPopupBind(RichPopup itemConsumptionPopupBind) {
        this.itemConsumptionPopupBind = itemConsumptionPopupBind;
    }

    public RichPopup getItemConsumptionPopupBind() {
        return itemConsumptionPopupBind;
    }

    public String saveAndCloseAL() {
        ADFUtils.setLastUpdatedBy("ActualPmAmcMachineHeaderVO1Iterator", "LastUpdatedBy");
        ADFUtils.findOperation("Commit").execute();
        FacesMessage Message = new FacesMessage("Record Save And Update Successfully");
        Message.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);

        return "SaveAndClose";
    }

    public BindingContainer getBindings() {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
    }

    public void downloadJrxml(FacesContext facesContext, OutputStream outputStream) {
        Connection conn = null;
        try {
            OperationBinding binding = ADFUtils.findOperation("fileNameForPrint");
            binding.getParamsMap().put("fileId", "ERP0000000271");
            binding.execute();
            //*************End Find File name***********//
            System.out.println("Binding Result :" + binding.getResult());
            if (binding.getResult() != null) {
                InputStream input = new FileInputStream(binding.getResult().toString());
                DCIteratorBinding pmIterHd =
                    (DCIteratorBinding) getBindings().get("ActualPmAmcMachineHeaderVO1Iterator");
                DCIteratorBinding pvIter = (DCIteratorBinding) getBindings().get("ActualPmAmcMachineDetailVO1Iterator");

                String machCode = pvIter.getCurrentRow().getAttribute("MLMachineCode").toString();
                String unitCode = pmIterHd.getCurrentRow().getAttribute("UnitCd").toString();


                System.out.println("JV parameters: ||UNIT CODE: " + unitCode);
                Map n = new HashMap();

                n.put("p_unit", unitCode);
                n.put("p_macd", machCode);
                conn = getConnection();

                JasperReport design = (JasperReport) JRLoader.loadObject(input);
                System.out.println("Path : " + input + " -------" + design + " param:" + n);

                @SuppressWarnings("unchecked")
                net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                byte pdf[] = JasperExportManager.exportReportToPdf(print);
                HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                response.getOutputStream().write(pdf);
                response.getOutputStream().flush();
                response.getOutputStream().close();
                facesContext.responseComplete();
            } else
                System.out.println("File Name/File Path not found.");
        } catch (FileNotFoundException fnfe) {
            // TODO: Add catch code
            fnfe.printStackTrace();
            try {
                System.out.println("in finally connection closed");
                conn.close();
                conn = null;

            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            // TODO: Add catch code
            e.printStackTrace();
            try {
                System.out.println("in finally connection closed");
                conn.close();
                conn = null;

            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        } finally {
            try {
                System.out.println("in finally connection closed");
                conn.close();
                conn = null;

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public Connection getConnection() throws Exception {
        Context ctx = null;
        Connection conn = null;
        ctx = new InitialContext();
        DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/ApplicationDBDS");
        conn = ds.getConnection();
        return conn;
    }
}
