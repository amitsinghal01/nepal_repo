package terms.mtm.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.application.ViewHandler;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import javax.faces.validator.ValidatorException;

import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.jbo.Row;

import org.apache.myfaces.trinidad.event.DisclosureEvent;

public class EditBreakdownAcknowledgement {
    private RichPanelHeader getMyPageRootComponent;
    private RichOutputText outputTextBinding;
    private RichButton headerEditBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputComboboxListOfValues complaintNoBinding;
    private RichInputDate complaintDateBinding;
    private RichInputDate complaintTimeBinding;
    private RichInputText complaintByBinding;
    private RichInputText remarksBinding;
    private RichInputDate breakDownDateBinding;
    private RichInputDate breakDownTimeBinding;
    private RichInputText machineCodeBinding;
    private RichInputText descriptionBinding;
    private RichInputDate targetDateBinding;
    private RichInputDate targetTimeBinding;
    private RichInputDate targetEntryTimeBinding;
    private RichInputDate targetEntryDateBinding;
    private RichInputComboboxListOfValues targetGivenByBinding;
    private RichInputDate entryDateBinding;
    private RichInputComboboxListOfValues targetsGivenByBinding;
    private RichTable breakdownSpareConTableBinding;
    private RichInputText nbtComplainNoBinding;
    private RichInputDate nbtCompDateBinding;
    private RichInputText nbtNatureOfDefectBinding;
    private RichInputText nbtRectifiedByBinding;
    private RichInputDate nbtRectificationDateBinding;
    private RichInputText nbtRectRemarkBinding;
    private RichShowDetailItem targetDetailsTabbedBinding;
    private RichShowDetailItem rectificationDetailsTabbedBinding;
    private RichShowDetailItem lastBreakdownDetailTabbedBinding;
    private RichShowDetailItem defectDetailTabbedBinding;
    private RichShowDetailItem handoverDetailTabbedBinding;
    private RichInputDate handoverDateDetailBinding;
    private RichInputComboboxListOfValues acceptedByDetailBinding;
    private RichInputDate rectificationDateBinding;
    private RichInputComboboxListOfValues targetGivenByReckBinding;
    private RichInputComboboxListOfValues rectifiedByRectificationBinding;
    private String tabflag="N";
    private RichInputText nbtRectifiedByNameBinding;
    private RichInputText tagNoBinding;
    private RichPopup spareitemBinding;

    public EditBreakdownAcknowledgement() {
    }

    public void editAL(ActionEvent actionEvent) {
//       ADFUtils.findOperation("CreateInsert").execute();
//       ADFUtils.findOperation("populateBDRectificationLastBreakdown").execute();
//       ADFUtils.findOperation("CreateInsert3").execute();
       
//       ADFUtils.findOperation("CreateInsert3").execute();
//        ADFUtils.findOperation("CreateInsert5").execute();
        cevmodecheck();

    }

    public void saveMessageAL(ActionEvent actionEvent) {
//        System.out.println("Inside Save Message ");
//        if(getHandoverDateDetailBinding().getValue() != null)
//        {
        ADFUtils.setLastUpdatedBy("BreakdownComplaintAckVO1Iterator","LastUpdatedBy");
        System.out.println("After Condition");
        ADFUtils.findOperation("Commit").execute();
        FacesMessage Message = new FacesMessage("Record Updated Successfully.");  
        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
        FacesContext fc = FacesContext.getCurrentInstance();  
        fc.addMessage(null, Message);
//        }
//        else {
//            FacesMessage Message = new FacesMessage("Handover Date is Required."); 
//            Message.setSeverity(FacesMessage.SEVERITY_ERROR);
//            FacesContext fc = FacesContext.getCurrentInstance();
//            fc.addMessage(null, Message);
//        }
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setOutputTextBinding(RichOutputText outputTextBinding) {
        this.outputTextBinding = outputTextBinding;
    }

    public RichOutputText getOutputTextBinding() {
        cevmodecheck();
        return outputTextBinding;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }
    
    private void cevmodecheck(){
          if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
              makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
              cevModeDisableComponent("V");
          }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
              makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
              cevModeDisableComponent("C");
          }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
              makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
              cevModeDisableComponent("E");
          }
      }
    
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
            try {
                Method method1 =
                    component.getClass().getMethod("setDisabled", boolean.class);
                if (method1 != null) {
                    method1.invoke(component, valueComponent);
                }
            } catch (NoSuchMethodException e) {
                try {
                    Method method =
                        component.getClass().getMethod("setReadOnly", boolean.class);
                    if (method != null) {
                        method.invoke(component, valueComponent);
                    }
                } catch (Exception e1) {
                    // e.printStackTrace();//silently eat this exception.
                }


            } catch (Exception e) {
                // e.printStackTrace();//silently eat this exception.
            }
            List<UIComponent> childComponents = component.getChildren();
            for (UIComponent comp : childComponents) {
                makeComponentHierarchyReadOnly(comp, valueComponent);
            }
        }
    

    //Set Fields and Button disable.
        public void cevModeDisableComponent(String mode) {
        //            FacesContext fctx = FacesContext.getCurrentInstance();
        //            ELContext elctx = fctx.getELContext();
        //            Application jsfApp = fctx.getApplication();
        //            //create a ValueExpression that points to the ADF binding layer
        //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
        //            //
        //            ValueExpression valueExpr = exprFactory.createValueExpression(
        //                                         elctx,
        //                                         "#{pageFlowScope.mode=='E'}",
        //                                          Object.class
        //                                         );
        //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
            if (mode.equals("E")) {
                   
                    getHeaderEditBinding().setDisabled(true);
                    getUnitCodeBinding().setDisabled(true);
                    getComplaintNoBinding().setDisabled(true);
                    getComplaintDateBinding().setDisabled(true);
                    getComplaintTimeBinding().setDisabled(true);
                    getComplaintByBinding().setDisabled(true);
                    getRemarksBinding().setDisabled(true);
                    getBreakDownDateBinding().setDisabled(true);
                    getBreakDownTimeBinding().setDisabled(true);
                    getMachineCodeBinding().setDisabled(true);
                    getTargetGivenByReckBinding().setDisabled(true);
                    getNbtComplainNoBinding().setDisabled(true);
                    getNbtCompDateBinding().setDisabled(true);
                    getNbtNatureOfDefectBinding().setDisabled(true);
                    getNbtRectifiedByBinding().setDisabled(true);
                    getNbtRectificationDateBinding().setDisabled(true);
                    getNbtRectRemarkBinding().setDisabled(true);
                    getNbtRectifiedByNameBinding().setDisabled(true);
//                    getHandoverDetailTabbedBinding().setDisabled(false);
//                    getTargetDateBinding().setDisabled(true);
//                    getTargetTimeBinding().setDisabled(true);
                    getTagNoBinding().setDisabled(true);
                    
            } else if (mode.equals("C")) {              
               
            } else if (mode.equals("V")) {
                getTargetDetailsTabbedBinding().setDisabled(false);
                getRectificationDetailsTabbedBinding().setDisabled(false);
                getLastBreakdownDetailTabbedBinding().setDisabled(false);
                getDefectDetailTabbedBinding().setDisabled(false);
                getHandoverDetailTabbedBinding().setDisabled(false);
                getTagNoBinding().setDisabled(true);
               
            }
            
        }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setComplaintNoBinding(RichInputComboboxListOfValues complaintNoBinding) {
        this.complaintNoBinding = complaintNoBinding;
    }

    public RichInputComboboxListOfValues getComplaintNoBinding() {
        return complaintNoBinding;
    }


    public void setComplaintDateBinding(RichInputDate complaintDateBinding) {
        this.complaintDateBinding = complaintDateBinding;
    }

    public RichInputDate getComplaintDateBinding() {
        return complaintDateBinding;
    }

    public void setComplaintTimeBinding(RichInputDate complaintTimeBinding) {
        this.complaintTimeBinding = complaintTimeBinding;
    }

    public RichInputDate getComplaintTimeBinding() {
        return complaintTimeBinding;
    }

    public void setComplaintByBinding(RichInputText complaintByBinding) {
        this.complaintByBinding = complaintByBinding;
    }

    public RichInputText getComplaintByBinding() {
        return complaintByBinding;
    }

    public void setRemarksBinding(RichInputText remarksBinding) {
        this.remarksBinding = remarksBinding;
    }

    public RichInputText getRemarksBinding() {
        return remarksBinding;
    }

    public void setBreakDownDateBinding(RichInputDate breakDownDateBinding) {
        this.breakDownDateBinding = breakDownDateBinding;
    }

    public RichInputDate getBreakDownDateBinding() {
        return breakDownDateBinding;
    }

    public void setBreakDownTimeBinding(RichInputDate breakDownTimeBinding) {
        this.breakDownTimeBinding = breakDownTimeBinding;
    }

    public RichInputDate getBreakDownTimeBinding() {
        return breakDownTimeBinding;
    }

    public void setMachineCodeBinding(RichInputText machineCodeBinding) {
        this.machineCodeBinding = machineCodeBinding;
    }

    public RichInputText getMachineCodeBinding() {
        return machineCodeBinding;
    }

    public void setDescriptionBinding(RichInputText descriptionBinding) {
        this.descriptionBinding = descriptionBinding;
    }

    public RichInputText getDescriptionBinding() {
        return descriptionBinding;
    }

    public void setTargetDateBinding(RichInputDate targetDateBinding) {
        this.targetDateBinding = targetDateBinding;
    }

    public RichInputDate getTargetDateBinding() {
        return targetDateBinding;
    }

    public void setTargetTimeBinding(RichInputDate targetTimeBinding) {
        this.targetTimeBinding = targetTimeBinding;
    }

    public RichInputDate getTargetTimeBinding() {
        return targetTimeBinding;
    }

    public void setTargetEntryTimeBinding(RichInputDate targetEntryTimeBinding) {
        this.targetEntryTimeBinding = targetEntryTimeBinding;
    }

    public RichInputDate getTargetEntryTimeBinding() {
        return targetEntryTimeBinding;
    }

    public void setTargetEntryDateBinding(RichInputDate targetEntryDateBinding) {
        this.targetEntryDateBinding = targetEntryDateBinding;
    }

    public RichInputDate getTargetEntryDateBinding() {
        return targetEntryDateBinding;
    }

//    public void compNoVCL(ValueChangeEvent vce) 
//    {
//        if(vce != null) 
//        {
//            ADFUtils.findOperation("previousComplain").execute();
//        }
//        
//    }

    public void setTargetGivenByBinding(RichInputComboboxListOfValues targetGivenByBinding) {
        this.targetGivenByBinding = targetGivenByBinding;
    }

    public RichInputComboboxListOfValues getTargetGivenByBinding() {
        return targetGivenByBinding;
    }

    public void setEntryDateBinding(RichInputDate entryDateBinding) {
        this.entryDateBinding = entryDateBinding;
    }

    public RichInputDate getEntryDateBinding() {
        return entryDateBinding;
    }

    public void setTargetsGivenByBinding(RichInputComboboxListOfValues targetsGivenByBinding) {
        this.targetsGivenByBinding = targetsGivenByBinding;
    }

    public RichInputComboboxListOfValues getTargetsGivenByBinding() {
        return targetsGivenByBinding;
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
            {
            ADFUtils.findOperation("Delete").execute();
            System.out.println("Record Delete Successfully");
            
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");   
                Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                FacesContext fc = FacesContext.getCurrentInstance();   
                fc.addMessage(null, Message);   
            
            }

        AdfFacesContext.getCurrentInstance().addPartialTarget(breakdownSpareConTableBinding);

    }

    public void setBreakdownSpareConTableBinding(RichTable breakdownSpareConTableBinding) {
        this.breakdownSpareConTableBinding = breakdownSpareConTableBinding;
    }

    public RichTable getBreakdownSpareConTableBinding() {
        return breakdownSpareConTableBinding;
    }

    public void setNbtComplainNoBinding(RichInputText nbtComplainNoBinding) {
        this.nbtComplainNoBinding = nbtComplainNoBinding;
    }

    public RichInputText getNbtComplainNoBinding() {
        return nbtComplainNoBinding;
    }

    public void setNbtCompDateBinding(RichInputDate nbtCompDateBinding) {
        this.nbtCompDateBinding = nbtCompDateBinding;
    }

    public RichInputDate getNbtCompDateBinding() {
        return nbtCompDateBinding;
    }

    public void setNbtNatureOfDefectBinding(RichInputText nbtNatureOfDefectBinding) {
        this.nbtNatureOfDefectBinding = nbtNatureOfDefectBinding;
    }

    public RichInputText getNbtNatureOfDefectBinding() {
        return nbtNatureOfDefectBinding;
    }

    public void setNbtRectifiedByBinding(RichInputText nbtRectifiedByBinding) {
        this.nbtRectifiedByBinding = nbtRectifiedByBinding;
    }

    public RichInputText getNbtRectifiedByBinding() {
        return nbtRectifiedByBinding;
    }

    public void setNbtRectificationDateBinding(RichInputDate nbtRectificationDateBinding) {
        this.nbtRectificationDateBinding = nbtRectificationDateBinding;
    }

    public RichInputDate getNbtRectificationDateBinding() {
        return nbtRectificationDateBinding;
    }

    public void setNbtRectRemarkBinding(RichInputText nbtRectRemarkBinding) {
        this.nbtRectRemarkBinding = nbtRectRemarkBinding;
    }

    public RichInputText getNbtRectRemarkBinding() {
        return nbtRectRemarkBinding;
    }

    public void setTargetDetailsTabbedBinding(RichShowDetailItem targetDetailsTabbedBinding) {
        this.targetDetailsTabbedBinding = targetDetailsTabbedBinding;
    }

    public RichShowDetailItem getTargetDetailsTabbedBinding() {
        return targetDetailsTabbedBinding;
    }

    public void setRectificationDetailsTabbedBinding(RichShowDetailItem rectificationDetailsTabbedBinding) {
        this.rectificationDetailsTabbedBinding = rectificationDetailsTabbedBinding;
    }

    public RichShowDetailItem getRectificationDetailsTabbedBinding() {
        return rectificationDetailsTabbedBinding;
    }

    public void setLastBreakdownDetailTabbedBinding(RichShowDetailItem lastBreakdownDetailTabbedBinding) {
        this.lastBreakdownDetailTabbedBinding = lastBreakdownDetailTabbedBinding;
    }

    public RichShowDetailItem getLastBreakdownDetailTabbedBinding() {
        return lastBreakdownDetailTabbedBinding;
    }

    public void setDefectDetailTabbedBinding(RichShowDetailItem defectDetailTabbedBinding) {
        this.defectDetailTabbedBinding = defectDetailTabbedBinding;
    }

    public RichShowDetailItem getDefectDetailTabbedBinding() {
        return defectDetailTabbedBinding;
    }

    public void setHandoverDetailTabbedBinding(RichShowDetailItem handoverDetailTabbedBinding) {
        this.handoverDetailTabbedBinding = handoverDetailTabbedBinding;
    }

    public RichShowDetailItem getHandoverDetailTabbedBinding() {
        return handoverDetailTabbedBinding;
    }

    public void setHandoverDateDetailBinding(RichInputDate handoverDateDetailBinding) {
        this.handoverDateDetailBinding = handoverDateDetailBinding;
    }

    public RichInputDate getHandoverDateDetailBinding() {
        return handoverDateDetailBinding;
    }

    public void setAcceptedByDetailBinding(RichInputComboboxListOfValues acceptedByDetailBinding) {
        this.acceptedByDetailBinding = acceptedByDetailBinding;
    }

    public RichInputComboboxListOfValues getAcceptedByDetailBinding() {
        return acceptedByDetailBinding;
    }

    public void handoverDateValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if(object == null && acceptedByDetailBinding.getValue() != null) 
        {
            System.out.println("Inside Validator Condition Hnadover Date");
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Handover Date can't be blank", null));
        }
        else if(rectificationDateBinding.getValue() == null && object != null) 
        {
            System.out.println("Inside Another If");
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Before filling Rectification Detail you can't fill Handover Date", null));
        }

    }

    public void setRectificationDateBinding(RichInputDate rectificationDateBinding) {
        this.rectificationDateBinding = rectificationDateBinding;
    }

    public RichInputDate getRectificationDateBinding() {
        return rectificationDateBinding;
    }

    public void setTargetGivenByReckBinding(RichInputComboboxListOfValues targetGivenByReckBinding) {
        this.targetGivenByReckBinding = targetGivenByReckBinding;
    }

    public RichInputComboboxListOfValues getTargetGivenByReckBinding() {
        return targetGivenByReckBinding;
    }

    public void populatePreviousComplainNo(DisclosureEvent disclosureEvent) {
        if(disclosureEvent.isExpanded()){
                    if(complaintNoBinding.getValue()!=null && !ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")){
                        System.out.println("jhkhkjhsdjkh");
                        ADFUtils.findOperation("populateBDRectificationLastBreakdown").execute(); 
                    }
                }
    }

    public void rectificationDateVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        
        if(rectificationDateBinding.getValue() == null) {
            System.out.println("Inside VCE");
            getHandoverDetailTabbedBinding().setDisabled(true);
        }
        else {
            System.out.println("Inside Else");
            getHandoverDetailTabbedBinding().setDisabled(false);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
    }

    public void entryDateVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        
        if(entryDateBinding.getValue() == null) {
            System.out.println("Inside entry VCE");
            getHandoverDetailTabbedBinding().setDisabled(true);
        }
        else {
            System.out.println("Inside entry Else");
            getHandoverDetailTabbedBinding().setDisabled(false);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
    }

    public void handOverDisclosureDL(DisclosureEvent disclosureEvent) {
        if(disclosureEvent.isExpanded()){
            tabflag="N";
            if(rectificationDateBinding.getValue() == null || entryDateBinding.getValue() == null || rectifiedByRectificationBinding.getValue() == null) {                      
                    
                    ADFUtils.showMessage("Rectification Details Tab is Null, So Handover tab is Disable.", 2);
                    getHandoverDetailTabbedBinding().setDisclosed(false);
                   }
                   else {
                       getHandoverDetailTabbedBinding().setDisclosed(true);
                       AdfFacesContext.getCurrentInstance().addPartialTarget(handoverDetailTabbedBinding);
                   }
//            AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
        }
    }

    public void setRectifiedByRectificationBinding(RichInputComboboxListOfValues rectifiedByRectificationBinding) {
        this.rectifiedByRectificationBinding = rectifiedByRectificationBinding;
    }

    public RichInputComboboxListOfValues getRectifiedByRectificationBinding() {
        return rectifiedByRectificationBinding;
    }

    public void rectificationDtlDisclosureDL(DisclosureEvent disclosureEvent) {
        if(disclosureEvent.isExpanded()){
            tabflag="N";
        }
    }

    public void setNbtRectifiedByNameBinding(RichInputText nbtRectifiedByNameBinding) {
        this.nbtRectifiedByNameBinding = nbtRectifiedByNameBinding;
    }

    public RichInputText getNbtRectifiedByNameBinding() {
        return nbtRectifiedByNameBinding;
    }

    public void currentStockPopulate(ActionEvent actionEvent) {
//        ADFUtils.findOperation("CreateInsert").execute();
//        ADFUtils.findOperation("getCurrentStockBreakdownRectification").execute();
                        System.out.println("Now Running ;)");
        ADFUtils.showPopup(spareitemBinding);
        System.out.println("ADFUtils.evaluateEL(\"#{bindings.BreakdownSpareConVO2Iterator.estimatedRowCount}\")"+ADFUtils.evaluateEL("#{bindings.BreakdownSpareConVO2Iterator.estimatedRowCount}"));
            if((Long)ADFUtils.evaluateEL("#{bindings.BreakdownSpareConVO2Iterator.estimatedRowCount}")==0) {
                System.out.println("inside condition################");
                     ADFUtils.findOperation("RectificationSpareItem").execute();
            }  
    }
    public void currentStockVCL(ValueChangeEvent vce) {
        System.out.println("Inside Current Stock ######");
        if(vce != null) {
            System.out.println("Inside vce ######");
            vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
            ADFUtils.findOperation("getCurrentStockBreakdownRectification").execute();
        }
    }

    public void setTagNoBinding(RichInputText tagNoBinding) {
        this.tagNoBinding = tagNoBinding;
    }

    public RichInputText getTagNoBinding() {
        return tagNoBinding;
    }

    public void setSpareitemBinding(RichPopup spareitemBinding) {
        this.spareitemBinding = spareitemBinding;
    }

    public RichPopup getSpareitemBinding() {
        return spareitemBinding;
    }
}
