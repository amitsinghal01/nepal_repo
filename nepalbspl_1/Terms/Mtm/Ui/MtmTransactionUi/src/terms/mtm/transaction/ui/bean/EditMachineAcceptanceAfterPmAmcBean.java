package terms.mtm.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;
import java.util.Set;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

public class EditMachineAcceptanceAfterPmAmcBean {
    private RichOutputText bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichButton detailcreateBinding;
    private RichButton detaildeleteBinding;
    private RichInputText receivedByNameTransBinding;
    private RichInputText scheduleNoBinding;
    private RichButton machineAccButtonBinding;
    private RichTable editMachineAcceptanceTableBinding;
    private RichInputText handoverRemarksBinding;
    private RichInputText receivedByTransBinding;
    private RichInputDate handoverTimeTransBinding;
    private RichInputDate handoverDateTransBinding;
    private RichInputComboboxListOfValues unitBinding;

    public EditMachineAcceptanceAfterPmAmcBean() {
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText()
    {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setDetailcreateBinding(RichButton detailcreateBinding) {
        this.detailcreateBinding = detailcreateBinding;
    }

    public RichButton getDetailcreateBinding() {
        return detailcreateBinding;
    }

    public void setDetaildeleteBinding(RichButton detaildeleteBinding) {
        this.detaildeleteBinding = detaildeleteBinding;
    }

    public RichButton getDetaildeleteBinding() {
        return detaildeleteBinding;
    }
    
    private void cevmodecheck(){
           if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
               cevModeDisableComponent("V");
           }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
               cevModeDisableComponent("C");
           }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
               cevModeDisableComponent("E");
           }
       }
    
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                try {
                    Method method1 =
                        component.getClass().getMethod("setDisabled", boolean.class);
                    if (method1 != null) {
                        method1.invoke(component, valueComponent);
                    }
                } catch (NoSuchMethodException e) {
                    try {
                        Method method =
                            component.getClass().getMethod("setReadOnly", boolean.class);
                        if (method != null) {
                            method.invoke(component, valueComponent);
                        }
                    } catch (Exception e1) {
                        // e.printStackTrace();//silently eat this exception.
                    }


                } catch (Exception e) {
                    // e.printStackTrace();//silently eat this exception.
                }
                List<UIComponent> childComponents = component.getChildren();
                for (UIComponent comp : childComponents) {
                    makeComponentHierarchyReadOnly(comp, valueComponent);
                }
            }
    
    
        //Set Fields and Button disable.
            public void cevModeDisableComponent(String mode) {
            //            FacesContext fctx = FacesContext.getCurrentInstance();
            //            ELContext elctx = fctx.getELContext();
            //            Application jsfApp = fctx.getApplication();
            //            //create a ValueExpression that points to the ADF binding layer
            //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
            //            //
            //            ValueExpression valueExpr = exprFactory.createValueExpression(
            //                                         elctx,
            //                                         "#{pageFlowScope.mode=='E'}",
            //                                          Object.class
            //                                         );
            //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
                if (mode.equals("E")) {
;
                        getHeaderEditBinding().setDisabled(true);
                        getDetailcreateBinding().setDisabled(false);
                        getDetaildeleteBinding().setDisabled(false);
                        getReceivedByNameTransBinding().setDisabled(true);
                        getScheduleNoBinding().setDisabled(true);
                        getMachineAccButtonBinding().setDisabled(false);
                        getHandoverRemarksBinding().setDisabled(true);
                        getReceivedByTransBinding().setDisabled(true);
                        getHandoverDateTransBinding().setDisabled(true);
                        getHandoverTimeTransBinding().setDisabled(true);
                    getUnitBinding().setDisabled(true);
                } else if (mode.equals("C")) {
                    getHeaderEditBinding().setDisabled(true);
                    getDetailcreateBinding().setDisabled(false);
                    getDetaildeleteBinding().setDisabled(false);
                    getReceivedByNameTransBinding().setDisabled(true);
                    getScheduleNoBinding().setDisabled(false);
                    getMachineAccButtonBinding().setDisabled(false);
                    getHandoverRemarksBinding().setDisabled(true);
                    getUnitBinding().setDisabled(true);
                } else if (mode.equals("V")) {
                    getDetailcreateBinding().setDisabled(true);
                    getDetaildeleteBinding().setDisabled(true);
                    getUnitBinding().setDisabled(true);
                }
                
            }


    public void editButtonAL(ActionEvent actionEvent) 
    {
        cevmodecheck();
    }

    public void setReceivedByNameTransBinding(RichInputText receivedByNameTransBinding) {
        this.receivedByNameTransBinding = receivedByNameTransBinding;
    }

    public RichInputText getReceivedByNameTransBinding() {
        return receivedByNameTransBinding;
    }

    public void setScheduleNoBinding(RichInputText scheduleNoBinding) {
        this.scheduleNoBinding = scheduleNoBinding;
    }

    public RichInputText getScheduleNoBinding() {
        return scheduleNoBinding;
    }

    public void setMachineAccButtonBinding(RichButton machineAccButtonBinding) {
        this.machineAccButtonBinding = machineAccButtonBinding;
    }

    public RichButton getMachineAccButtonBinding() {
        return machineAccButtonBinding;
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) 
    {
        if(dialogEvent.getOutcome().name().equals("ok"))
            {
            ADFUtils.findOperation("Delete").execute();
            System.out.println("Record Delete Successfully");
            
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");   
                Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                FacesContext fc = FacesContext.getCurrentInstance();   
                fc.addMessage(null, Message);   
            
            }

        AdfFacesContext.getCurrentInstance().addPartialTarget(editMachineAcceptanceTableBinding);

    }

    public void setEditMachineAcceptanceTableBinding(RichTable editMachineAcceptanceTableBinding) {
        this.editMachineAcceptanceTableBinding = editMachineAcceptanceTableBinding;
    }

    public RichTable getEditMachineAcceptanceTableBinding() {
        return editMachineAcceptanceTableBinding;
    }

    public void setHandoverRemarksBinding(RichInputText handoverRemarksBinding) {
        this.handoverRemarksBinding = handoverRemarksBinding;
    }

    public RichInputText getHandoverRemarksBinding() {
        return handoverRemarksBinding;
    }

    public void setReceivedByTransBinding(RichInputText receivedByTransBinding) {
        this.receivedByTransBinding = receivedByTransBinding;
    }

    public RichInputText getReceivedByTransBinding() {
        return receivedByTransBinding;
    }

    public void setHandoverTimeTransBinding(RichInputDate handoverTimeTransBinding) {
        this.handoverTimeTransBinding = handoverTimeTransBinding;
    }

    public RichInputDate getHandoverTimeTransBinding() {
        return handoverTimeTransBinding;
    }

    public void setHandoverDateTransBinding(RichInputDate handoverDateTransBinding) {
        this.handoverDateTransBinding = handoverDateTransBinding;
    }

    public RichInputDate getHandoverDateTransBinding() {
        return handoverDateTransBinding;
    }

    public void saveButtonAL(ActionEvent actionEvent) {        
        ADFUtils.setLastUpdatedBy("MachineAcceptanceAfterPmAmcHeaderVO1Iterator","LastUpdatedBy");
        FacesMessage Message = new FacesMessage("Record Updated Successfully");   
        Message.setSeverity(FacesMessage.SEVERITY_INFO);   
        FacesContext fc = FacesContext.getCurrentInstance();   
        fc.addMessage(null, Message); 
    }

    public void setUnitBinding(RichInputComboboxListOfValues unitBinding) {
        this.unitBinding = unitBinding;
    }

    public RichInputComboboxListOfValues getUnitBinding() {
        return unitBinding;
    }
}
