package terms.mtm.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

public class MachineAcceptanceBreakBean {
//    private RichButton headerEditBinding;
    private RichPanelGroupLayout getMyPageRootComponent;
    private RichOutputText editTransBinding;
    private RichButton headerEditBinding1;
    private String editAction="V";
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText complaintNoBinding;
    //    private RichPanelHeader getMyPageRootComponent1;

    public MachineAcceptanceBreakBean() {
    }

    public void EditButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

//    public void setHeaderEditBinding(RichButton headerEditBinding) {
//        this.headerEditBinding = headerEditBinding;
//    }
//
//    public RichButton getHeaderEditBinding() {
//        return headerEditBinding;
//    }

    public void setGetMyPageRootComponent(RichPanelGroupLayout getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelGroupLayout getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setEditTransBinding(RichOutputText editTransBinding) {
        this.editTransBinding = editTransBinding;
    }

    public RichOutputText getEditTransBinding(){
        cevmodecheck();
       
        return editTransBinding;
    }
    private void cevmodecheck(){
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }}
    
        public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                  try {
                      Method method1 =
                          component.getClass().getMethod("setDisabled", boolean.class);
                      if (method1 != null) {
                          method1.invoke(component, valueComponent);
                      }
                  } catch (NoSuchMethodException e) {
                      try {
                          Method method =
                              component.getClass().getMethod("setReadOnly", boolean.class);
                          if (method != null) {
                              method.invoke(component, valueComponent);
                          }
                      } catch (Exception e1) {
                          // e.printStackTrace();//silently eat this exception.
                      }


                  } catch (Exception e) {
                      // e.printStackTrace();//silently eat this exception.
                  }
                  List<UIComponent> childComponents = component.getChildren();
                  for (UIComponent comp : childComponents) {
                      makeComponentHierarchyReadOnly(comp, valueComponent);
                  }
              }
        //Set Fields and Button disable.
            public void cevModeDisableComponent(String mode) {
            //            FacesContext fctx = FacesContext.getCurrentInstance();
            //            ELContext elctx = fctx.getELContext();
            //            Application jsfApp = fctx.getApplication();
            //            //create a ValueExpression that points to the ADF binding layer
            //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
            //            //
            //            ValueExpression valueExpr = exprFactory.createValueExpression(
            //                                         elctx,
            //                                         "#{pageFlowScope.mode=='E'}",
            //                                          Object.class
            //                                         );
            //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
                if (mode.equals("E")) {
//                        getProcessCodeBinding().setDisabled(true);
//                        getJobworkIhOutBinding().setDisabled(true);
//                        getHeaderEditBinding().setDisabled(true);
//                        getDetailcreateBinding().setDisabled(false);
//                        getDetaildeleteBinding().setDisabled(false);
                    getComplaintNoBinding().setDisabled(true);
                    getUnitCodeBinding().setDisabled(true);
                } else if (mode.equals("C")) {
//                    getProcessCodeBinding().setDisabled(true);
//                    getJobworkIhOutBinding().setDisabled(true);
//                    getDetailcreateBinding().setDisabled(false);
//                    getDetaildeleteBinding().setDisabled(false);
                } else if (mode.equals("V")) {
//                    getDetailcreateBinding().setDisabled(true);
//                    getDetaildeleteBinding().setDisabled(true);
                }
                
            }


    public void setHeaderEditBinding1(RichButton headerEditBinding1) {
        this.headerEditBinding1 = headerEditBinding1;
    }

    public RichButton getHeaderEditBinding1() {
        return headerEditBinding1;
    }

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setComplaintNoBinding(RichInputText complaintNoBinding) {
        this.complaintNoBinding = complaintNoBinding;
    }

    public RichInputText getComplaintNoBinding() {
        return complaintNoBinding;
    }

    public void saveAll(ActionEvent actionEvent) {
        {
            ADFUtils.setLastUpdatedBy("MachineAcceptanceBreakHeaderVO1Iterator","LastUpdatedBy");
                ADFUtils.findOperation("Commit").execute();
                FacesMessage Message = new FacesMessage("Record Updated Successfully");  
                Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                FacesContext fc = FacesContext.getCurrentInstance();  
                fc.addMessage(null, Message);
           
        }
    }

    public void SaveandClose(ActionEvent actionEvent) {
        {
            ADFUtils.setLastUpdatedBy("MachineAcceptanceBreakHeaderVO1Iterator","LastUpdatedBy");
                ADFUtils.findOperation("Commit").execute();
                FacesMessage Message = new FacesMessage("Record Updated Successfully");  
                Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                FacesContext fc = FacesContext.getCurrentInstance();  
                fc.addMessage(null, Message);
           
        }
        
    }
}


