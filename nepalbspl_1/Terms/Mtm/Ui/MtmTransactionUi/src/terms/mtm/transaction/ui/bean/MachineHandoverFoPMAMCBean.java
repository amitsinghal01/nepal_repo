package terms.mtm.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichPanelLabelAndMessage;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

public class MachineHandoverFoPMAMCBean {
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichPanelLabelAndMessage bindingOutputText;
    private RichButton detailDeleteBinding;
    private RichOutputText bindingOutputText1;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText scheduleNoBinding;
    private RichTable machineHandoverForPmAmcBinding;

    public MachineHandoverFoPMAMCBean() {
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void EditActionAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setBindingOutputText(RichPanelLabelAndMessage bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    

    public void setDetailDeleteBinding(RichButton detailDeleteBinding) {
        this.detailDeleteBinding = detailDeleteBinding;
    }

    public RichButton getDetailDeleteBinding() {
        return detailDeleteBinding;
    }

    public void setBindingOutputText1(RichOutputText bindingOutputText1) {
        this.bindingOutputText1 = bindingOutputText1;
    }

    public RichOutputText getBindingOutputText1() {
        cevmodecheck();
        return bindingOutputText1;
    }
    private void cevmodecheck(){
            if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
                cevModeDisableComponent("V");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("C");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("E");
            }}
        public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                    try {
                        Method method1 =
                            component.getClass().getMethod("setDisabled", boolean.class);
                        if (method1 != null) {
                            method1.invoke(component, valueComponent);
                        }
                    } catch (NoSuchMethodException e) {
                        try {
                            Method method =
                                component.getClass().getMethod("setReadOnly", boolean.class);
                            if (method != null) {
                                method.invoke(component, valueComponent);
                            }
                        } catch (Exception e1) {
                            // e.printStackTrace();//silently eat this exception.
                        }


                    } catch (Exception e) {
                        // e.printStackTrace();//silently eat this exception.
                    }
                    List<UIComponent> childComponents = component.getChildren();
                    for (UIComponent comp : childComponents) {
                        makeComponentHierarchyReadOnly(comp, valueComponent);
                    }
                }
        
        public void cevModeDisableComponent(String mode) {
            
            if(mode.equals("E")){
                
                getHeaderEditBinding().setDisabled(false);
               // getDetailCreateBinding().setDisabled(false);
                getDetailDeleteBinding().setDisabled(false);
//                getScheduleTypeBinding().setDisabled(true);
//                getLocationBinding().setDisabled(true);
//                getProposeDateBinding().setDisabled(true);
//                getStartDateBinding().setDisabled(true);
//                getStartTimeBinding().setDisabled(true);
//                getElectricalBinding().setDisabled(true);
//                getMechanicalBinding().setDisabled(true);
//                getWhetherBinding().setDisabled(true);
//                getUnitBinding().setDisabled(true);
//                getScheduleNoBinding().setDisabled(true);
                getUnitCodeBinding().setDisabled(true);
                getScheduleNoBinding().setDisabled(true);
            }
            
            if(mode.equals("V")){
                
                
                getDetailDeleteBinding().setDisabled(true);
            }
            if(mode.equals("C")){
                
//                getDetailCreateBinding().setDisabled(false);
                getDetailDeleteBinding().setDisabled(false);
            }
            
            }

        public void saveAll(ActionEvent actionEvent) {
            
            ADFUtils.setLastUpdatedBy("MachineHandoverForPmAmcHeaderVO1Iterator","LastUpdatedBy");
        
                ADFUtils.findOperation("Commit").execute();
                FacesMessage Message = new FacesMessage("Record Update Successfully");  
                Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                FacesContext fc = FacesContext.getCurrentInstance();  
                fc.addMessage(null, Message);
           
        
        
        

        
        }


    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setScheduleNoBinding(RichInputText scheduleNoBinding) {
        this.scheduleNoBinding = scheduleNoBinding;
    }

    public RichInputText getScheduleNoBinding() {
        return scheduleNoBinding;
    }

    public void deleteDialogPopup(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
            {
            
            ADFUtils.findOperation("Delete").execute();
            System.out.println("Record Delete Successfully");
            
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");   
                Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                FacesContext fc = FacesContext.getCurrentInstance();   
                fc.addMessage(null, Message);   
            
            }

        AdfFacesContext.getCurrentInstance().addPartialTarget(machineHandoverForPmAmcBinding);
        }

    public void setMachineHandoverForPmAmcBinding(RichTable machineHandoverForPmAmcBinding) {
        this.machineHandoverForPmAmcBinding = machineHandoverForPmAmcBinding;
    }

    public RichTable getMachineHandoverForPmAmcBinding() {
        return machineHandoverForPmAmcBinding;
    }

    public void saveandclose(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedBy("MachineHandoverForPmAmcHeaderVO1Iterator","LastUpdatedBy");
                ADFUtils.findOperation("Commit").execute();
                FacesMessage Message = new FacesMessage("Record Update Successfully");  
                Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                FacesContext fc = FacesContext.getCurrentInstance();  
                fc.addMessage(null, Message);
           
      
        
        
    }
}
