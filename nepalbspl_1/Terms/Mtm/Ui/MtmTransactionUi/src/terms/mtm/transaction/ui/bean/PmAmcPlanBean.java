package terms.mtm.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class PmAmcPlanBean {
  
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichOutputText bindingOutputText;
    private RichButton detailCreateBinding;
    private RichTable tableBinding;
    private RichInputComboboxListOfValues locationBinding;
    private RichSelectOneChoice scheduleTypeBinding;
    private RichInputDate proposeDateBinding;
    private RichInputDate startDateBinding;
    private RichInputDate startTimeBinding;
    private RichInputText electricalBinding;
    private RichInputText mechanicalBinding;
    private RichSelectOneChoice whetherBinding;
    private RichColumn remarksBinding;
    private RichInputComboboxListOfValues unitBinding;
    private RichInputText scheduleNoBinding;


    public PmAmcPlanBean() {
    }



    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void EditButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        
        return bindingOutputText;
    }


    public void setDetailCreateBinding(RichButton detailCreateBinding) {
        this.detailCreateBinding = detailCreateBinding;
    }

    public RichButton getDetailCreateBinding() {
        return detailCreateBinding;
    }

    public void setTableBinding(RichTable tableBinding) {
        this.tableBinding = tableBinding;
    }

    public RichTable getTableBinding() {
        return tableBinding;
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
            {
            oracle.adf.model.OperationBinding op = null;
            ADFUtils.findOperation("Delete").execute();
            op = (oracle.adf.model.OperationBinding) ADFUtils.findOperation("Commit");
            Object rst = op.execute();
            System.out.println("Record Delete Successfully");
                if(op.getErrors().isEmpty()){
                    FacesMessage Message = new FacesMessage("Record Deleted Successfully");
        Message.setSeverity(FacesMessage.SEVERITY_INFO);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);
                } else if (!op.getErrors().isEmpty())
                {
                   oracle.adf.model.OperationBinding opr = (oracle.adf.model.OperationBinding) ADFUtils.findOperation("Rollback");
                   Object rstr = opr.execute();
                   FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
        Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                   FacesContext fc = FacesContext.getCurrentInstance();
                   fc.addMessage(null, Message);
                 }
            }

        AdfFacesContext.getCurrentInstance().addPartialTarget(tableBinding);
    }
    
    private void cevmodecheck(){
            if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
                cevModeDisableComponent("V");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("C");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("E");
            }
        }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                try {
                    Method method1 =
                        component.getClass().getMethod("setDisabled", boolean.class);
                    if (method1 != null) {
                        method1.invoke(component, valueComponent);
                    }
                } catch (NoSuchMethodException e) {
                    try {
                        Method method =
                            component.getClass().getMethod("setReadOnly", boolean.class);
                        if (method != null) {
                            method.invoke(component, valueComponent);
                        }
                    } catch (Exception e1) {
                        // e.printStackTrace();//silently eat this exception.
                    }


                } catch (Exception e) {
                    // e.printStackTrace();//silently eat this exception.
                }
                List<UIComponent> childComponents = component.getChildren();
                for (UIComponent comp : childComponents) {
                    makeComponentHierarchyReadOnly(comp, valueComponent);
                }
            }
    
    public void cevModeDisableComponent(String mode) {
        
        if(mode.equals("E")){
            
            getHeaderEditBinding().setDisabled(true);
            getDetailCreateBinding().setDisabled(false);
            getUnitBinding().setDisabled(true);
            getScheduleNoBinding().setDisabled(true);
        }
        
        if(mode.equals("V")){
            
            getDetailCreateBinding().setDisabled(true);
        }
        if(mode.equals("C")){
            
            getDetailCreateBinding().setDisabled(false);
        }
        
        }

    public void saveAL(ActionEvent actionEvent) {
    ADFUtils.setLastUpdatedBy("PmAmcPlanHeaderVO1Iterator","LastUpdatedBy");
    ADFUtils.findOperation("Commit").execute();
    
    if(ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C"))
    {
        ADFUtils.findOperation("Commit").execute();
        FacesMessage Message = new FacesMessage("Record Save Successfully");  
        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
        FacesContext fc = FacesContext.getCurrentInstance();  
        fc.addMessage(null, Message); 
    }
    else
    {
            ADFUtils.findOperation("Commit").execute();
            FacesMessage Message = new FacesMessage("Record Update Successfully");  
            Message.setSeverity(FacesMessage.SEVERITY_INFO);  
            FacesContext fc = FacesContext.getCurrentInstance();  
            fc.addMessage(null, Message);
       
    }
    
    

  
    }

    public void setLocationBinding(RichInputComboboxListOfValues locationBinding) {
        this.locationBinding = locationBinding;
    }

    public RichInputComboboxListOfValues getLocationBinding() {
        return locationBinding;
    }

    public void setScheduleTypeBinding(RichSelectOneChoice scheduleTypeBinding) {
        this.scheduleTypeBinding = scheduleTypeBinding;
    }

    public RichSelectOneChoice getScheduleTypeBinding() {
        return scheduleTypeBinding;
    }

    public void setProposeDateBinding(RichInputDate proposeDateBinding) {
        this.proposeDateBinding = proposeDateBinding;
    }

    public RichInputDate getProposeDateBinding() {
        return proposeDateBinding;
    }

    public void setStartDateBinding(RichInputDate startDateBinding) {
        this.startDateBinding = startDateBinding;
    }

    public RichInputDate getStartDateBinding() {
        return startDateBinding;
    }

    public void setStartTimeBinding(RichInputDate startTimeBinding) {
        this.startTimeBinding = startTimeBinding;
    }

    public RichInputDate getStartTimeBinding() {
        return startTimeBinding;
    }

    public void setElectricalBinding(RichInputText electricalBinding) {
        this.electricalBinding = electricalBinding;
    }

    public RichInputText getElectricalBinding() {
        return electricalBinding;
    }

    public void setMechanicalBinding(RichInputText mechanicalBinding) {
        this.mechanicalBinding = mechanicalBinding;
    }

    public RichInputText getMechanicalBinding() {
        return mechanicalBinding;
    }

    public void setWhetherBinding(RichSelectOneChoice whetherBinding) {
        this.whetherBinding = whetherBinding;
    }

    public RichSelectOneChoice getWhetherBinding() {
        return whetherBinding;
    }

    public void setRemarksBinding(RichColumn remarksBinding) {
        this.remarksBinding = remarksBinding;
    }

    public RichColumn getRemarksBinding() {
        return remarksBinding;
    }

    public void setUnitBinding(RichInputComboboxListOfValues unitBinding) {
        this.unitBinding = unitBinding;
    }

    public RichInputComboboxListOfValues getUnitBinding() {
        return unitBinding;
    }

    public void setScheduleNoBinding(RichInputText scheduleNoBinding) {
        this.scheduleNoBinding = scheduleNoBinding;
    }

    public RichInputText getScheduleNoBinding() {
        return scheduleNoBinding;
    }
}
