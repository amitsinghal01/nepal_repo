package terms.mtm.transaction.ui.bean;

import java.lang.reflect.Method;
import oracle.jbo.domain.Number;
import java.math.BigDecimal;

import java.math.MathContext;

import java.math.RoundingMode;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

public class PowerFactorLogBookBean {
    private RichButton headerEditBinding;
    private RichButton detailCreateBinding;
    private RichTable tableBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichOutputText bindingOutputText;
    private RichInputComboboxListOfValues bindingUnitCode;
    private RichInputDate bindingOperationDate;
    private RichInputText bindingKVarh;
    private RichInputText bindingKvah;
    private RichInputText bindingKwh;
    private RichInputText bindingMdi;
    private RichInputText bindingTodayPf;
    private RichInputText bindingAvgPf;
    private RichInputText srlNoBinding;

    public PowerFactorLogBookBean() {
        
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setDetailCreateBinding(RichButton detailCreateBinding) {
        this.detailCreateBinding = detailCreateBinding;
    }

    public RichButton getDetailCreateBinding() {
        return detailCreateBinding;
    }

    public void deletePopupDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                    {
                    ADFUtils.findOperation("Delete").execute();
                    System.out.println("Record Delete Successfully");
                   
                        FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                        FacesContext fc = FacesContext.getCurrentInstance();  
                        fc.addMessage(null, Message);  
                   
                    }
            AdfFacesContext.getCurrentInstance().addPartialTarget(tableBinding);
    }

    public void setTableBinding(RichTable tableBinding) {
        this.tableBinding = tableBinding;
    }

    public RichTable getTableBinding() {
        return tableBinding;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }
    private void cevmodecheck(){
           if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
               cevModeDisableComponent("V");
           }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
               cevModeDisableComponent("C");
           }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
               cevModeDisableComponent("E");
           }
       }
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean  valueComponent) {
               try {
                   Method method1 =
                       component.getClass().getMethod("setDisabled", boolean.class);
                   if (method1 != null) {
                       method1.invoke(component, valueComponent);
                   }
               } catch (NoSuchMethodException e) {
                   try {
                       Method method =
                           component.getClass().getMethod("setReadOnly", boolean.class);
                       if (method != null) {
                           method.invoke(component, valueComponent);
                       }
                   } catch (Exception e1) {
                       // e.printStackTrace();//silently eat this exception.
                   }


               } catch (Exception e) {
                   // e.printStackTrace();//silently eat this exception.
               }
               List<UIComponent> childComponents = component.getChildren();
               for (UIComponent comp : childComponents) {
                   makeComponentHierarchyReadOnly(comp, valueComponent);
               }
           }
    public void cevModeDisableComponent(String mode) {
        //            FacesContext fctx = FacesContext.getCurrentInstance();
        //            ELContext elctx = fctx.getELContext();
        //            Application jsfApp = fctx.getApplication();
        //            //create a ValueExpression that points to the ADF binding layer
        //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
        //            //
        //            ValueExpression valueExpr = exprFactory.createValueExpression(
        //                                         elctx,
        //                                         "#{pageFlowScope.mode=='E'}",
        //                                          Object.class
        //                                         );
        //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
            if (mode.equals("E")) {
                getSrlNoBinding().setDisabled(true);
                getBindingUnitCode().setDisabled(true);
                getBindingOperationDate().setDisabled(true);
                getDetailCreateBinding().setDisabled(false);
                getHeaderEditBinding().setDisabled(true);
            } else if (mode.equals("C")) {
                getHeaderEditBinding().setDisabled(true);
                getSrlNoBinding().setDisabled(true);
                //getBindingUnitCode().setDisabled(true);
                getBindingOperationDate().setDisabled(false);
                getDetailCreateBinding().setDisabled(false);
            } else if (mode.equals("V")) {
                getDetailCreateBinding().setDisabled(true);
            }
            
        }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setBindingUnitCode(RichInputComboboxListOfValues bindingUnitCode) {
        this.bindingUnitCode = bindingUnitCode;
    }

    public RichInputComboboxListOfValues getBindingUnitCode() {
        return bindingUnitCode;
    }

    public void setBindingOperationDate(RichInputDate bindingOperationDate) {
        this.bindingOperationDate = bindingOperationDate;
    }

    public RichInputDate getBindingOperationDate() {
        return bindingOperationDate;
    }

    public void setBindingKVarh(RichInputText bindingKVarh) {
        this.bindingKVarh = bindingKVarh;
    }

    public RichInputText getBindingKVarh() {
        return bindingKVarh;
    }

    public void setBindingKvah(RichInputText bindingKvah) {
        this.bindingKvah = bindingKvah;
    }

    public RichInputText getBindingKvah() {
        return bindingKvah;
    }

    public void setBindingKwh(RichInputText bindingKwh) {
        this.bindingKwh = bindingKwh;
    }

    public RichInputText getBindingKwh() {
        return bindingKwh;
    }

    public void setBindingMdi(RichInputText bindingMdi) {
        this.bindingMdi = bindingMdi;
    }

    public RichInputText getBindingMdi() {
        return bindingMdi;
    }

    public void setBindingTodayPf(RichInputText bindingTodayPf) {
        this.bindingTodayPf = bindingTodayPf;
    }

    public RichInputText getBindingTodayPf() {
        return bindingTodayPf;
    }

    public void setBindingAvgPf(RichInputText bindingAvgPf) {
        this.bindingAvgPf = bindingAvgPf;
    }

    public RichInputText getBindingAvgPf() {
        return bindingAvgPf;
    }

       public void createButtonAL(ActionEvent actionEvent)
    {
        ADFUtils.findOperation("CreateInsert").execute();
         srlNoBinding.setValue(ADFUtils.evaluateEL("#{bindings.PowerFactorLogBookDetailVO1Iterator.estimatedRowCount}"));   
     }

    public void setSrlNoBinding(RichInputText srlNoBinding) {
        this.srlNoBinding = srlNoBinding;
    }

    public RichInputText getSrlNoBinding() {
        return srlNoBinding;
    }

    public void todayPfValue(ValueChangeEvent vce) {
        
        if(vce!=null){
            
           
                if(getBindingKvah().getValue()!=null && getBindingKwh().getValue()!=null )
                {
                BigDecimal i=(BigDecimal)getBindingKvah().getValue();
                BigDecimal j=(BigDecimal)getBindingKwh().getValue();
                BigDecimal TPf=j.divide(i, 2, RoundingMode.HALF_UP);
                System.out.println("TPF"+TPf);
                //BigDecimal bd=TPf;
               // bd=bd.divide(arg0, arg1, arg2);
                //bd=bd.setScale(2, BigDecimal.ROUND_DOWN);
                getBindingTodayPf().setValue(TPf);
                
            }
    }
    }

    public void saveButtonAL(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedBy("PowerFactorLogBookHeaderVO1Iterator","LastUpdatedBy");
        Integer i=0;
        i=(Integer)bindingOutputText.getValue();
        System.out.println("EDIT TRANS VALUE"+i);
        if(i.equals(0))
        {
        FacesMessage Message = new FacesMessage("Record Save Successfully");
        Message.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
        
        }
        else
        {
        FacesMessage Message = new FacesMessage("Record Updated Successfully");   
        Message.setSeverity(FacesMessage.SEVERITY_INFO);   
        FacesContext fc = FacesContext.getCurrentInstance();   
        fc.addMessage(null, Message);      
        }
        
        
        
    }
}
