package terms.mtm.transaction.ui.bean;

import javax.faces.application.FacesMessage;

import javax.faces.context.FacesContext;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class SearchMachineHandoverForPMAMCBean {
    private RichTable machineHandoverPMAMCBinding;

    public SearchMachineHandoverForPMAMCBean() {
    }

    public void deletedialogPopupDL(DialogEvent dialogEvent) {
            if(dialogEvent.getOutcome().name().equals("ok"))
                {
                oracle.adf.model.OperationBinding op = null;
                ADFUtils.findOperation("Delete").execute();
                op = (oracle.adf.model.OperationBinding) ADFUtils.findOperation("Commit");
                Object rst = op.execute();
                System.out.println("Record Delete Successfully");
                    if(op.getErrors().isEmpty()){
                        FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
                        FacesContext fc = FacesContext.getCurrentInstance();
                        fc.addMessage(null, Message);
                   }

            else if (!op.getErrors().isEmpty())
                    {
                       oracle.adf.model.OperationBinding opr = (oracle.adf.model.OperationBinding) ADFUtils.findOperation("Rollback");
                       Object rstr = opr.execute();
                       FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
            Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                       FacesContext fc = FacesContext.getCurrentInstance();
                       fc.addMessage(null, Message);
                     }
                }

            AdfFacesContext.getCurrentInstance().addPartialTarget(machineHandoverPMAMCBinding);
            }
            
    public void setMachineHandoverPMAMCBinding(RichTable machineHandoverPMAMCBinding) {
        this.machineHandoverPMAMCBinding = machineHandoverPMAMCBinding;
    }

    public RichTable getMachineHandoverPMAMCBinding() {
        return machineHandoverPMAMCBinding;
    }

}
