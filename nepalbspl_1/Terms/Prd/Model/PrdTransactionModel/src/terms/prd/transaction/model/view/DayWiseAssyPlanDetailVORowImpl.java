package terms.prd.transaction.model.view;

import java.math.BigDecimal;

import java.sql.Timestamp;

import javax.faces.application.FacesMessage;

import javax.faces.context.FacesContext;

import oracle.jbo.JboException;
import oracle.jbo.Row;
import oracle.jbo.RowSet;
import oracle.jbo.server.EntityImpl;
import oracle.jbo.server.ViewRowImpl;

import terms.prd.transaction.model.applicationModule.PrdTransactionAMImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Wed May 30 18:04:23 IST 2018
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class DayWiseAssyPlanDetailVORowImpl extends ViewRowImpl {

    public static final int ENTITY_DAYWISEPLANDETAILEO = 0;
    public static final int ENTITY_LINEMASTEREO = 1;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        CreatedBy,
        CreationDate,
        CustType,
        ForMonth,
        ForYear,
        ItemCode,
        LastUpdateDate,
        LastUpdatedBy,
        LineNo,
        MfgPlan,
        Month,
        ObjectVersionNumber,
        Plan,
        PlanId,
        PlanLineId,
        TotReq,
        UnitCode,
        X1,
        X10,
        X11,
        X12,
        X13,
        X14,
        X15,
        X16,
        X17,
        X18,
        X19,
        X2,
        X20,
        X21,
        X22,
        X23,
        X24,
        X25,
        X26,
        X27,
        X28,
        X29,
        X3,
        X30,
        X31,
        X4,
        X5,
        X6,
        X7,
        X8,
        X9,
        Year,
        SDesc,
        LineId,
        FgStock,
        LineMasterVO1,
        ManufacturingPlanDetailVO1;
        static AttributesEnum[] vals = null;
        ;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int CREATEDBY = AttributesEnum.CreatedBy.index();
    public static final int CREATIONDATE = AttributesEnum.CreationDate.index();
    public static final int CUSTTYPE = AttributesEnum.CustType.index();
    public static final int FORMONTH = AttributesEnum.ForMonth.index();
    public static final int FORYEAR = AttributesEnum.ForYear.index();
    public static final int ITEMCODE = AttributesEnum.ItemCode.index();
    public static final int LASTUPDATEDATE = AttributesEnum.LastUpdateDate.index();
    public static final int LASTUPDATEDBY = AttributesEnum.LastUpdatedBy.index();
    public static final int LINENO = AttributesEnum.LineNo.index();
    public static final int MFGPLAN = AttributesEnum.MfgPlan.index();
    public static final int MONTH = AttributesEnum.Month.index();
    public static final int OBJECTVERSIONNUMBER = AttributesEnum.ObjectVersionNumber.index();
    public static final int PLAN = AttributesEnum.Plan.index();
    public static final int PLANID = AttributesEnum.PlanId.index();
    public static final int PLANLINEID = AttributesEnum.PlanLineId.index();
    public static final int TOTREQ = AttributesEnum.TotReq.index();
    public static final int UNITCODE = AttributesEnum.UnitCode.index();
    public static final int X1 = AttributesEnum.X1.index();
    public static final int X10 = AttributesEnum.X10.index();
    public static final int X11 = AttributesEnum.X11.index();
    public static final int X12 = AttributesEnum.X12.index();
    public static final int X13 = AttributesEnum.X13.index();
    public static final int X14 = AttributesEnum.X14.index();
    public static final int X15 = AttributesEnum.X15.index();
    public static final int X16 = AttributesEnum.X16.index();
    public static final int X17 = AttributesEnum.X17.index();
    public static final int X18 = AttributesEnum.X18.index();
    public static final int X19 = AttributesEnum.X19.index();
    public static final int X2 = AttributesEnum.X2.index();
    public static final int X20 = AttributesEnum.X20.index();
    public static final int X21 = AttributesEnum.X21.index();
    public static final int X22 = AttributesEnum.X22.index();
    public static final int X23 = AttributesEnum.X23.index();
    public static final int X24 = AttributesEnum.X24.index();
    public static final int X25 = AttributesEnum.X25.index();
    public static final int X26 = AttributesEnum.X26.index();
    public static final int X27 = AttributesEnum.X27.index();
    public static final int X28 = AttributesEnum.X28.index();
    public static final int X29 = AttributesEnum.X29.index();
    public static final int X3 = AttributesEnum.X3.index();
    public static final int X30 = AttributesEnum.X30.index();
    public static final int X31 = AttributesEnum.X31.index();
    public static final int X4 = AttributesEnum.X4.index();
    public static final int X5 = AttributesEnum.X5.index();
    public static final int X6 = AttributesEnum.X6.index();
    public static final int X7 = AttributesEnum.X7.index();
    public static final int X8 = AttributesEnum.X8.index();
    public static final int X9 = AttributesEnum.X9.index();
    public static final int YEAR = AttributesEnum.Year.index();
    public static final int SDESC = AttributesEnum.SDesc.index();
    public static final int LINEID = AttributesEnum.LineId.index();
    public static final int FGSTOCK = AttributesEnum.FgStock.index();
    public static final int LINEMASTERVO1 = AttributesEnum.LineMasterVO1.index();
    public static final int MANUFACTURINGPLANDETAILVO1 = AttributesEnum.ManufacturingPlanDetailVO1.index();

    /**
     * This is the default constructor (do not remove).
     */
    public DayWiseAssyPlanDetailVORowImpl() {
    }

    /**
     * Gets DayWisePlanDetailEO entity object.
     * @return the DayWisePlanDetailEO
     */
    public EntityImpl getDayWisePlanDetailEO() {
        return (EntityImpl) getEntity(ENTITY_DAYWISEPLANDETAILEO);
    }

    /**
     * Gets LineMasterEO entity object.
     * @return the LineMasterEO
     */
    public EntityImpl getLineMasterEO() {
        return (EntityImpl) getEntity(ENTITY_LINEMASTEREO);
    }


    /**
     * Gets the attribute value for CREATED_BY using the alias name CreatedBy.
     * @return the CREATED_BY
     */
    public String getCreatedBy() {
    return (String) getAttributeInternal(CREATEDBY);
    }

    /**
     * Gets the attribute value for CREATION_DATE using the alias name CreationDate.
     * @return the CREATION_DATE
     */
    public Timestamp getCreationDate() {
        return (Timestamp) getAttributeInternal(CREATIONDATE);
    }

    /**
     * Gets the attribute value for CUST_TYPE using the alias name CustType.
     * @return the CUST_TYPE
     */
    public String getCustType() {
        return (String) getAttributeInternal(CUSTTYPE);
    }

    /**
     * Sets <code>value</code> as attribute value for CUST_TYPE using the alias name CustType.
     * @param value value to set the CUST_TYPE
     */
    public void setCustType(String value) {
        setAttributeInternal(CUSTTYPE, value);
    }

    /**
     * Gets the attribute value for FOR_MONTH using the alias name ForMonth.
     * @return the FOR_MONTH
     */
    public String getForMonth() {
        return (String) getAttributeInternal(FORMONTH);
    }

    /**
     * Sets <code>value</code> as attribute value for FOR_MONTH using the alias name ForMonth.
     * @param value value to set the FOR_MONTH
     */
    public void setForMonth(String value) {
        setAttributeInternal(FORMONTH, value);
    }

    /**
     * Gets the attribute value for FOR_YEAR using the alias name ForYear.
     * @return the FOR_YEAR
     */
    public Integer getForYear() {
        return (Integer) getAttributeInternal(FORYEAR);
    }

    /**
     * Sets <code>value</code> as attribute value for FOR_YEAR using the alias name ForYear.
     * @param value value to set the FOR_YEAR
     */
    public void setForYear(Integer value) {
        setAttributeInternal(FORYEAR, value);
    }

    /**
     * Gets the attribute value for ITEM_CODE using the alias name ItemCode.
     * @return the ITEM_CODE
     */
    public String getItemCode() {
        
        
        if(getAttributeInternal(ITEMCODE)!=null)
        System.out.println("n the item code ***********");
                     {
                         try
                         {
                         Row rr[]=this.getManufacturingPlanDetailVO1().getFilteredRows("ProdCode", getAttributeInternal(ITEMCODE));
                         if(rr[0].getAttribute("FgStock")!=null)
                         {
                             setAttributeInternal(FGSTOCK, rr[0].getAttribute("FgStock")); 
                            
                         }
                         
                         }
                     catch(Exception ee)
                     {
                     System.out.println("Error");
                     }
                    }
        
        return (String) getAttributeInternal(ITEMCODE);
    }

    /**
     * Sets <code>value</code> as attribute value for ITEM_CODE using the alias name ItemCode.
     * @param value value to set the ITEM_CODE
     */
    public void setItemCode(String value) {
        setAttributeInternal(ITEMCODE, value);
    }

    /**
     * Gets the attribute value for LAST_UPDATE_DATE using the alias name LastUpdateDate.
     * @return the LAST_UPDATE_DATE
     */
    public Timestamp getLastUpdateDate() {
        return (Timestamp) getAttributeInternal(LASTUPDATEDATE);
    }

    /**
     * Gets the attribute value for LAST_UPDATED_BY using the alias name LastUpdatedBy.
     * @return the LAST_UPDATED_BY
     */
    public String getLastUpdatedBy() {
        return (String) getAttributeInternal(LASTUPDATEDBY);
    }

    /**
     * Gets the attribute value for LINE_NO using the alias name LineNo.
     * @return the LINE_NO
     */
    public String getLineNo() {
        return (String) getAttributeInternal(LINENO);
    }

    /**
     * Sets <code>value</code> as attribute value for LINE_NO using the alias name LineNo.
     * @param value value to set the LINE_NO
     */
    public void setLineNo(String value) {
        setAttributeInternal(LINENO, value);
    }

    /**
     * Gets the attribute value for MFG_PLAN using the alias name MfgPlan.
     * @return the MFG_PLAN
     */
    public BigDecimal getMfgPlan() {
        return (BigDecimal) getAttributeInternal(MFGPLAN);
    }

    /**
     * Sets <code>value</code> as attribute value for MFG_PLAN using the alias name MfgPlan.
     * @param value value to set the MFG_PLAN
     */
    public void setMfgPlan(BigDecimal value) {
        setAttributeInternal(MFGPLAN, value);
    }

    /**
     * Gets the attribute value for MONTH using the alias name Month.
     * @return the MONTH
     */
    public String getMonth() {
        return (String) getAttributeInternal(MONTH);
    }

    /**
     * Sets <code>value</code> as attribute value for MONTH using the alias name Month.
     * @param value value to set the MONTH
     */
    public void setMonth(String value) {
        setAttributeInternal(MONTH, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER);
    }

    /**
     * Gets the attribute value for PLAN using the alias name Plan.
     * @return the PLAN
     */
    public BigDecimal getPlan() {

        return (BigDecimal) getAttributeInternal(PLAN);
    }

    /**
     * Sets <code>value</code> as attribute value for PLAN using the alias name Plan.
     * @param value value to set the PLAN
     */
    public void setPlan(BigDecimal value) {
        setAttributeInternal(PLAN, value);
    }

    /**
     * Gets the attribute value for PLAN_ID using the alias name PlanId.
     * @return the PLAN_ID
     */
    public Long getPlanId() {
        return (Long) getAttributeInternal(PLANID);
    }

    /**
     * Sets <code>value</code> as attribute value for PLAN_ID using the alias name PlanId.
     * @param value value to set the PLAN_ID
     */
    public void setPlanId(Long value) {
        setAttributeInternal(PLANID, value);
    }

    /**
     * Gets the attribute value for PLAN_LINE_ID using the alias name PlanLineId.
     * @return the PLAN_LINE_ID
     */
    public Long getPlanLineId() {
        return (Long) getAttributeInternal(PLANLINEID);
    }

    /**
     * Sets <code>value</code> as attribute value for PLAN_LINE_ID using the alias name PlanLineId.
     * @param value value to set the PLAN_LINE_ID
     */
    public void setPlanLineId(Long value) {
        setAttributeInternal(PLANLINEID, value);
    }

    /**
     * Gets the attribute value for TOT_REQ using the alias name TotReq.
     * @return the TOT_REQ
     */
    public BigDecimal getTotReq() {
        return (BigDecimal) getAttributeInternal(TOTREQ);
    }

    /**
     * Sets <code>value</code> as attribute value for TOT_REQ using the alias name TotReq.
     * @param value value to set the TOT_REQ
     */
    public void setTotReq(BigDecimal value) {
        setAttributeInternal(TOTREQ, value);
    }

    /**
     * Gets the attribute value for UNIT_CODE using the alias name UnitCode.
     * @return the UNIT_CODE
     */
    public String getUnitCode() {
        return (String) getAttributeInternal(UNITCODE);
    }

    /**
     * Sets <code>value</code> as attribute value for UNIT_CODE using the alias name UnitCode.
     * @param value value to set the UNIT_CODE
     */
    public void setUnitCode(String value) {
        setAttributeInternal(UNITCODE, value);
    }

    /**
     * Gets the attribute value for X_1 using the alias name X1.
     * @return the X_1
     */
    public BigDecimal getX1() {
        return (BigDecimal) getAttributeInternal(X1);
    }

    /**
     * Sets <code>value</code> as attribute value for X_1 using the alias name X1.
     * @param value value to set the X_1
     */
    public void setX1(BigDecimal value) {
        setAttributeInternal(X1, value);
    }

    /**
     * Gets the attribute value for X_10 using the alias name X10.
     * @return the X_10
     */
    public BigDecimal getX10() {
        return (BigDecimal) getAttributeInternal(X10);
    }

    /**
     * Sets <code>value</code> as attribute value for X_10 using the alias name X10.
     * @param value value to set the X_10
     */
    public void setX10(BigDecimal value) {
        setAttributeInternal(X10, value);
    }

    /**
     * Gets the attribute value for X_11 using the alias name X11.
     * @return the X_11
     */
    public BigDecimal getX11() {
        return (BigDecimal) getAttributeInternal(X11);
    }

    /**
     * Sets <code>value</code> as attribute value for X_11 using the alias name X11.
     * @param value value to set the X_11
     */
    public void setX11(BigDecimal value) {
        setAttributeInternal(X11, value);
    }

    /**
     * Gets the attribute value for X_12 using the alias name X12.
     * @return the X_12
     */
    public BigDecimal getX12() {
        return (BigDecimal) getAttributeInternal(X12);
    }

    /**
     * Sets <code>value</code> as attribute value for X_12 using the alias name X12.
     * @param value value to set the X_12
     */
    public void setX12(BigDecimal value) {
        setAttributeInternal(X12, value);
    }

    /**
     * Gets the attribute value for X_13 using the alias name X13.
     * @return the X_13
     */
    public BigDecimal getX13() {
        return (BigDecimal) getAttributeInternal(X13);
    }

    /**
     * Sets <code>value</code> as attribute value for X_13 using the alias name X13.
     * @param value value to set the X_13
     */
    public void setX13(BigDecimal value) {
        setAttributeInternal(X13, value);
    }

    /**
     * Gets the attribute value for X_14 using the alias name X14.
     * @return the X_14
     */
    public BigDecimal getX14() {
        return (BigDecimal) getAttributeInternal(X14);
    }

    /**
     * Sets <code>value</code> as attribute value for X_14 using the alias name X14.
     * @param value value to set the X_14
     */
    public void setX14(BigDecimal value) {
        setAttributeInternal(X14, value);
    }

    /**
     * Gets the attribute value for X_15 using the alias name X15.
     * @return the X_15
     */
    public BigDecimal getX15() {
        return (BigDecimal) getAttributeInternal(X15);
    }

    /**
     * Sets <code>value</code> as attribute value for X_15 using the alias name X15.
     * @param value value to set the X_15
     */
    public void setX15(BigDecimal value) {
        setAttributeInternal(X15, value);
    }

    /**
     * Gets the attribute value for X_16 using the alias name X16.
     * @return the X_16
     */
    public BigDecimal getX16() {
        return (BigDecimal) getAttributeInternal(X16);
    }

    /**
     * Sets <code>value</code> as attribute value for X_16 using the alias name X16.
     * @param value value to set the X_16
     */
    public void setX16(BigDecimal value) {
        setAttributeInternal(X16, value);
    }

    /**
     * Gets the attribute value for X_17 using the alias name X17.
     * @return the X_17
     */
    public BigDecimal getX17() {
        return (BigDecimal) getAttributeInternal(X17);
    }

    /**
     * Sets <code>value</code> as attribute value for X_17 using the alias name X17.
     * @param value value to set the X_17
     */
    public void setX17(BigDecimal value) {
        setAttributeInternal(X17, value);
    }

    /**
     * Gets the attribute value for X_18 using the alias name X18.
     * @return the X_18
     */
    public BigDecimal getX18() {
        return (BigDecimal) getAttributeInternal(X18);
    }

    /**
     * Sets <code>value</code> as attribute value for X_18 using the alias name X18.
     * @param value value to set the X_18
     */
    public void setX18(BigDecimal value) {
        setAttributeInternal(X18, value);
    }

    /**
     * Gets the attribute value for X_19 using the alias name X19.
     * @return the X_19
     */
    public BigDecimal getX19() {
        return (BigDecimal) getAttributeInternal(X19);
    }

    /**
     * Sets <code>value</code> as attribute value for X_19 using the alias name X19.
     * @param value value to set the X_19
     */
    public void setX19(BigDecimal value) {
        setAttributeInternal(X19, value);
    }

    /**
     * Gets the attribute value for X_2 using the alias name X2.
     * @return the X_2
     */
    public BigDecimal getX2() {
        return (BigDecimal) getAttributeInternal(X2);
    }

    /**
     * Sets <code>value</code> as attribute value for X_2 using the alias name X2.
     * @param value value to set the X_2
     */
    public void setX2(BigDecimal value) {
        setAttributeInternal(X2, value);
    }

    /**
     * Gets the attribute value for X_20 using the alias name X20.
     * @return the X_20
     */
    public BigDecimal getX20() {
        return (BigDecimal) getAttributeInternal(X20);
    }

    /**
     * Sets <code>value</code> as attribute value for X_20 using the alias name X20.
     * @param value value to set the X_20
     */
    public void setX20(BigDecimal value) {
        setAttributeInternal(X20, value);
    }

    /**
     * Gets the attribute value for X_21 using the alias name X21.
     * @return the X_21
     */
    public BigDecimal getX21() {
        return (BigDecimal) getAttributeInternal(X21);
    }

    /**
     * Sets <code>value</code> as attribute value for X_21 using the alias name X21.
     * @param value value to set the X_21
     */
    public void setX21(BigDecimal value) {
        setAttributeInternal(X21, value);
    }

    /**
     * Gets the attribute value for X_22 using the alias name X22.
     * @return the X_22
     */
    public BigDecimal getX22() {
        return (BigDecimal) getAttributeInternal(X22);
    }

    /**
     * Sets <code>value</code> as attribute value for X_22 using the alias name X22.
     * @param value value to set the X_22
     */
    public void setX22(BigDecimal value) {
        setAttributeInternal(X22, value);
    }

    /**
     * Gets the attribute value for X_23 using the alias name X23.
     * @return the X_23
     */
    public BigDecimal getX23() {
        return (BigDecimal) getAttributeInternal(X23);
    }

    /**
     * Sets <code>value</code> as attribute value for X_23 using the alias name X23.
     * @param value value to set the X_23
     */
    public void setX23(BigDecimal value) {
        setAttributeInternal(X23, value);
    }

    /**
     * Gets the attribute value for X_24 using the alias name X24.
     * @return the X_24
     */
    public BigDecimal getX24() {
        return (BigDecimal) getAttributeInternal(X24);
    }

    /**
     * Sets <code>value</code> as attribute value for X_24 using the alias name X24.
     * @param value value to set the X_24
     */
    public void setX24(BigDecimal value) {
        setAttributeInternal(X24, value);
    }

    /**
     * Gets the attribute value for X_25 using the alias name X25.
     * @return the X_25
     */
    public BigDecimal getX25() {
        return (BigDecimal) getAttributeInternal(X25);
    }

    /**
     * Sets <code>value</code> as attribute value for X_25 using the alias name X25.
     * @param value value to set the X_25
     */
    public void setX25(BigDecimal value) {
        setAttributeInternal(X25, value);
    }

    /**
     * Gets the attribute value for X_26 using the alias name X26.
     * @return the X_26
     */
    public BigDecimal getX26() {
        return (BigDecimal) getAttributeInternal(X26);
    }

    /**
     * Sets <code>value</code> as attribute value for X_26 using the alias name X26.
     * @param value value to set the X_26
     */
    public void setX26(BigDecimal value) {
        setAttributeInternal(X26, value);
    }

    /**
     * Gets the attribute value for X_27 using the alias name X27.
     * @return the X_27
     */
    public BigDecimal getX27() {
        return (BigDecimal) getAttributeInternal(X27);
    }

    /**
     * Sets <code>value</code> as attribute value for X_27 using the alias name X27.
     * @param value value to set the X_27
     */
    public void setX27(BigDecimal value) {
        setAttributeInternal(X27, value);
    }

    /**
     * Gets the attribute value for X_28 using the alias name X28.
     * @return the X_28
     */
    public BigDecimal getX28() {
        return (BigDecimal) getAttributeInternal(X28);
    }

    /**
     * Sets <code>value</code> as attribute value for X_28 using the alias name X28.
     * @param value value to set the X_28
     */
    public void setX28(BigDecimal value) {
        setAttributeInternal(X28, value);
    }

    /**
     * Gets the attribute value for X_29 using the alias name X29.
     * @return the X_29
     */
    public BigDecimal getX29() {
        return (BigDecimal) getAttributeInternal(X29);
    }

    /**
     * Sets <code>value</code> as attribute value for X_29 using the alias name X29.
     * @param value value to set the X_29
     */
    public void setX29(BigDecimal value) {
        setAttributeInternal(X29, value);
    }

    /**
     * Gets the attribute value for X_3 using the alias name X3.
     * @return the X_3
     */
    public BigDecimal getX3() {
        return (BigDecimal) getAttributeInternal(X3);
    }

    /**
     * Sets <code>value</code> as attribute value for X_3 using the alias name X3.
     * @param value value to set the X_3
     */
    public void setX3(BigDecimal value) {
        setAttributeInternal(X3, value);
    }

    /**
     * Gets the attribute value for X_30 using the alias name X30.
     * @return the X_30
     */
    public BigDecimal getX30() {
        return (BigDecimal) getAttributeInternal(X30);
    }

    /**
     * Sets <code>value</code> as attribute value for X_30 using the alias name X30.
     * @param value value to set the X_30
     */
    public void setX30(BigDecimal value) {
        setAttributeInternal(X30, value);
    }

    /**
     * Gets the attribute value for X_31 using the alias name X31.
     * @return the X_31
     */
    public BigDecimal getX31() {
        return (BigDecimal) getAttributeInternal(X31);
    }

    /**
     * Sets <code>value</code> as attribute value for X_31 using the alias name X31.
     * @param value value to set the X_31
     */
    public void setX31(BigDecimal value) {
        setAttributeInternal(X31, value);
    }

    /**
     * Gets the attribute value for X_4 using the alias name X4.
     * @return the X_4
     */
    public BigDecimal getX4() {
        return (BigDecimal) getAttributeInternal(X4);
    }

    /**
     * Sets <code>value</code> as attribute value for X_4 using the alias name X4.
     * @param value value to set the X_4
     */
    public void setX4(BigDecimal value) {
        setAttributeInternal(X4, value);
    }

    /**
     * Gets the attribute value for X_5 using the alias name X5.
     * @return the X_5
     */
    public BigDecimal getX5() {
        return (BigDecimal) getAttributeInternal(X5);
    }

    /**
     * Sets <code>value</code> as attribute value for X_5 using the alias name X5.
     * @param value value to set the X_5
     */
    public void setX5(BigDecimal value) {
        setAttributeInternal(X5, value);
    }

    /**
     * Gets the attribute value for X_6 using the alias name X6.
     * @return the X_6
     */
    public BigDecimal getX6() {
        return (BigDecimal) getAttributeInternal(X6);
    }

    /**
     * Sets <code>value</code> as attribute value for X_6 using the alias name X6.
     * @param value value to set the X_6
     */
    public void setX6(BigDecimal value) {
        setAttributeInternal(X6, value);
    }

    /**
     * Gets the attribute value for X_7 using the alias name X7.
     * @return the X_7
     */
    public BigDecimal getX7() {
        return (BigDecimal) getAttributeInternal(X7);
    }

    /**
     * Sets <code>value</code> as attribute value for X_7 using the alias name X7.
     * @param value value to set the X_7
     */
    public void setX7(BigDecimal value) {
        setAttributeInternal(X7, value);
    }

    /**
     * Gets the attribute value for X_8 using the alias name X8.
     * @return the X_8
     */
    public BigDecimal getX8() {
        return (BigDecimal) getAttributeInternal(X8);
    }

    /**
     * Sets <code>value</code> as attribute value for X_8 using the alias name X8.
     * @param value value to set the X_8
     */
    public void setX8(BigDecimal value) {
        setAttributeInternal(X8, value);
    }

    /**
     * Gets the attribute value for X_9 using the alias name X9.
     * @return the X_9
     */
    public BigDecimal getX9() {
        return (BigDecimal) getAttributeInternal(X9);
    }

    /**
     * Sets <code>value</code> as attribute value for X_9 using the alias name X9.
     * @param value value to set the X_9
     */
    public void setX9(BigDecimal value) {
        setAttributeInternal(X9, value);
    }

    /**
     * Gets the attribute value for YEAR using the alias name Year.
     * @return the YEAR
     */
    public Integer getYear() {
        return (Integer) getAttributeInternal(YEAR);
    }

    /**
     * Sets <code>value</code> as attribute value for YEAR using the alias name Year.
     * @param value value to set the YEAR
     */
    public void setYear(Integer value) {
        setAttributeInternal(YEAR, value);
    }

    /**
     * Gets the attribute value for S_DESC using the alias name SDesc.
     * @return the S_DESC
     */
    public String getSDesc() {
        return (String) getAttributeInternal(SDESC);
    }

    /**
     * Sets <code>value</code> as attribute value for S_DESC using the alias name SDesc.
     * @param value value to set the S_DESC
     */
    public void setSDesc(String value) {
        setAttributeInternal(SDESC, value);
    }

    /**
     * Gets the attribute value for LINE_ID using the alias name LineId.
     * @return the LINE_ID
     */
    public Long getLineId() {
        return (Long) getAttributeInternal(LINEID);
    }

    /**
     * Sets <code>value</code> as attribute value for LINE_ID using the alias name LineId.
     * @param value value to set the LINE_ID
     */
    public void setLineId(Long value) {
        setAttributeInternal(LINEID, value);
    }

    /**
     * Gets the attribute value for the calculated attribute FgStock.
     * @return the FgStock
     */
    public BigDecimal getFgStock() {
    return (BigDecimal) getAttributeInternal(FGSTOCK);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute FgStock.
     * @param value value to set the  FgStock
     */
    public void setFgStock(BigDecimal value) {
        setAttributeInternal(FGSTOCK, value);
    }


    /**
     * Gets the view accessor <code>RowSet</code> LineMasterVO1.
     */
    public RowSet getLineMasterVO1() {
        return (RowSet) getAttributeInternal(LINEMASTERVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> ManufacturingPlanDetailVO1.
     */
    public RowSet getManufacturingPlanDetailVO1() {
        return (RowSet) getAttributeInternal(MANUFACTURINGPLANDETAILVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> LineMasterVO1.
     */

}

