package terms.prd.transaction.model.view;

import java.math.BigDecimal;

import java.sql.Timestamp;

import oracle.jbo.JboException;
import oracle.jbo.RowSet;
import oracle.jbo.server.EntityImpl;
import oracle.jbo.server.ViewRowImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Mon Dec 16 14:44:12 IST 2019
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class ManufacturingPlanDetailVORowImpl extends ViewRowImpl {
    public static final int ENTITY_MANUFACTURINGPLANDETAILEO = 0;
    public static final int ENTITY_ITEMSTOCKEO = 1;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        AddlPlan,
        CreatedBy,
        CreationDate,
        CustType,
        FgStock,
        JcGenReq,
        JcGenSt,
        JcGenType,
        JcQty,
        LastUpdateDate,
        LastUpdatedBy,
        MinStock,
        MktIndQty,
        Month,
        NetPlan,
        ObjectVersionNumber,
        PlanId,
        PlanLineId,
        ProdCode,
        RejPer,
        RejPlan,
        UnitCd,
        W1Plan,
        W2Plan,
        W3Plan,
        W4Plan,
        WipQty,
        Year,
        ItemDesc,
        ItemCd,
        sum,
        ManufactionPlanProductCodeLovVVO1;
        static AttributesEnum[] vals = null;
        ;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int ADDLPLAN = AttributesEnum.AddlPlan.index();
    public static final int CREATEDBY = AttributesEnum.CreatedBy.index();
    public static final int CREATIONDATE = AttributesEnum.CreationDate.index();
    public static final int CUSTTYPE = AttributesEnum.CustType.index();
    public static final int FGSTOCK = AttributesEnum.FgStock.index();
    public static final int JCGENREQ = AttributesEnum.JcGenReq.index();
    public static final int JCGENST = AttributesEnum.JcGenSt.index();
    public static final int JCGENTYPE = AttributesEnum.JcGenType.index();
    public static final int JCQTY = AttributesEnum.JcQty.index();
    public static final int LASTUPDATEDATE = AttributesEnum.LastUpdateDate.index();
    public static final int LASTUPDATEDBY = AttributesEnum.LastUpdatedBy.index();
    public static final int MINSTOCK = AttributesEnum.MinStock.index();
    public static final int MKTINDQTY = AttributesEnum.MktIndQty.index();
    public static final int MONTH = AttributesEnum.Month.index();
    public static final int NETPLAN = AttributesEnum.NetPlan.index();
    public static final int OBJECTVERSIONNUMBER = AttributesEnum.ObjectVersionNumber.index();
    public static final int PLANID = AttributesEnum.PlanId.index();
    public static final int PLANLINEID = AttributesEnum.PlanLineId.index();
    public static final int PRODCODE = AttributesEnum.ProdCode.index();
    public static final int REJPER = AttributesEnum.RejPer.index();
    public static final int REJPLAN = AttributesEnum.RejPlan.index();
    public static final int UNITCD = AttributesEnum.UnitCd.index();
    public static final int W1PLAN = AttributesEnum.W1Plan.index();
    public static final int W2PLAN = AttributesEnum.W2Plan.index();
    public static final int W3PLAN = AttributesEnum.W3Plan.index();
    public static final int W4PLAN = AttributesEnum.W4Plan.index();
    public static final int WIPQTY = AttributesEnum.WipQty.index();
    public static final int YEAR = AttributesEnum.Year.index();
    public static final int ITEMDESC = AttributesEnum.ItemDesc.index();
    public static final int ITEMCD = AttributesEnum.ItemCd.index();
    public static final int SUM = AttributesEnum.sum.index();
    public static final int MANUFACTIONPLANPRODUCTCODELOVVVO1 =
        AttributesEnum.ManufactionPlanProductCodeLovVVO1.index();

    /**
     * This is the default constructor (do not remove).
     */
    public ManufacturingPlanDetailVORowImpl() {
    }

    /**
     * Gets ManufacturingPlanDetailEO entity object.
     * @return the ManufacturingPlanDetailEO
     */
    public EntityImpl getManufacturingPlanDetailEO() {
        return (EntityImpl) getEntity(ENTITY_MANUFACTURINGPLANDETAILEO);
    }

    /**
     * Gets ItemStockEO entity object.
     * @return the ItemStockEO
     */
    public EntityImpl getItemStockEO() {
        return (EntityImpl) getEntity(ENTITY_ITEMSTOCKEO);
    }

    /**
     * Gets the attribute value for ADDL_PLAN using the alias name AddlPlan.
     * @return the ADDL_PLAN
     */
    public BigDecimal getAddlPlan() {
        return (BigDecimal) getAttributeInternal(ADDLPLAN);
    }

    /**
     * Sets <code>value</code> as attribute value for ADDL_PLAN using the alias name AddlPlan.
     * @param value value to set the ADDL_PLAN
     */
    public void setAddlPlan(BigDecimal value) {
        setAttributeInternal(ADDLPLAN, value);
    }

    /**
     * Gets the attribute value for CREATED_BY using the alias name CreatedBy.
     * @return the CREATED_BY
     */
    public String getCreatedBy() {
        return (String) getAttributeInternal(CREATEDBY);
    }

    /**
     * Gets the attribute value for CREATION_DATE using the alias name CreationDate.
     * @return the CREATION_DATE
     */
    public Timestamp getCreationDate() {
        return (Timestamp) getAttributeInternal(CREATIONDATE);
    }

    /**
     * Gets the attribute value for CUST_TYPE using the alias name CustType.
     * @return the CUST_TYPE
     */
    public String getCustType() {
        return (String) getAttributeInternal(CUSTTYPE);
    }

    /**
     * Sets <code>value</code> as attribute value for CUST_TYPE using the alias name CustType.
     * @param value value to set the CUST_TYPE
     */
    public void setCustType(String value) {
        setAttributeInternal(CUSTTYPE, value);
    }

    /**
     * Gets the attribute value for FG_STOCK using the alias name FgStock.
     * @return the FG_STOCK
     */
    public BigDecimal getFgStock() {
        return (BigDecimal) getAttributeInternal(FGSTOCK);
    }

    /**
     * Sets <code>value</code> as attribute value for FG_STOCK using the alias name FgStock.
     * @param value value to set the FG_STOCK
     */
    public void setFgStock(BigDecimal value) {
        setAttributeInternal(FGSTOCK, value);
    }

    /**
     * Gets the attribute value for JC_GEN_REQ using the alias name JcGenReq.
     * @return the JC_GEN_REQ
     */
    public String getJcGenReq() {
        return (String) getAttributeInternal(JCGENREQ);
    }

    /**
     * Sets <code>value</code> as attribute value for JC_GEN_REQ using the alias name JcGenReq.
     * @param value value to set the JC_GEN_REQ
     */
    public void setJcGenReq(String value) {
        setAttributeInternal(JCGENREQ, value);
    }

    /**
     * Gets the attribute value for JC_GEN_ST using the alias name JcGenSt.
     * @return the JC_GEN_ST
     */
    public String getJcGenSt() {
        return (String) getAttributeInternal(JCGENST);
    }

    /**
     * Sets <code>value</code> as attribute value for JC_GEN_ST using the alias name JcGenSt.
     * @param value value to set the JC_GEN_ST
     */
    public void setJcGenSt(String value) {
        setAttributeInternal(JCGENST, value);
    }

    /**
     * Gets the attribute value for JC_GEN_TYPE using the alias name JcGenType.
     * @return the JC_GEN_TYPE
     */
    public String getJcGenType() {
        return (String) getAttributeInternal(JCGENTYPE);
    }

    /**
     * Sets <code>value</code> as attribute value for JC_GEN_TYPE using the alias name JcGenType.
     * @param value value to set the JC_GEN_TYPE
     */
    public void setJcGenType(String value) {
        setAttributeInternal(JCGENTYPE, value);
    }

    /**
     * Gets the attribute value for JC_QTY using the alias name JcQty.
     * @return the JC_QTY
     */
    public BigDecimal getJcQty() {
        return (BigDecimal) getAttributeInternal(JCQTY);
    }

    /**
     * Sets <code>value</code> as attribute value for JC_QTY using the alias name JcQty.
     * @param value value to set the JC_QTY
     */
    public void setJcQty(BigDecimal value) {
        setAttributeInternal(JCQTY, value);
    }

    /**
     * Gets the attribute value for LAST_UPDATE_DATE using the alias name LastUpdateDate.
     * @return the LAST_UPDATE_DATE
     */
    public Timestamp getLastUpdateDate() {
        return (Timestamp) getAttributeInternal(LASTUPDATEDATE);
    }

    /**
     * Gets the attribute value for LAST_UPDATED_BY using the alias name LastUpdatedBy.
     * @return the LAST_UPDATED_BY
     */
    public String getLastUpdatedBy() {
        return (String) getAttributeInternal(LASTUPDATEDBY);
    }

    /**
     * Gets the attribute value for MIN_STOCK using the alias name MinStock.
     * @return the MIN_STOCK
     */
    public BigDecimal getMinStock() {
        return (BigDecimal) getAttributeInternal(MINSTOCK);
    }

    /**
     * Sets <code>value</code> as attribute value for MIN_STOCK using the alias name MinStock.
     * @param value value to set the MIN_STOCK
     */
    public void setMinStock(BigDecimal value) {
        setAttributeInternal(MINSTOCK, value);
    }

    /**
     * Gets the attribute value for MKT_IND_QTY using the alias name MktIndQty.
     * @return the MKT_IND_QTY
     */
    public BigDecimal getMktIndQty() {
        return (BigDecimal) getAttributeInternal(MKTINDQTY);
    }

    /**
     * Sets <code>value</code> as attribute value for MKT_IND_QTY using the alias name MktIndQty.
     * @param value value to set the MKT_IND_QTY
     */
    public void setMktIndQty(BigDecimal value) {
        setAttributeInternal(MKTINDQTY, value);
    }

    /**
     * Gets the attribute value for MONTH using the alias name Month.
     * @return the MONTH
     */
    public String getMonth() {
        return (String) getAttributeInternal(MONTH);
    }

    /**
     * Sets <code>value</code> as attribute value for MONTH using the alias name Month.
     * @param value value to set the MONTH
     */
    public void setMonth(String value) {
        setAttributeInternal(MONTH, value);
    }

    /**
     * Gets the attribute value for NET_PLAN using the alias name NetPlan.
     * @return the NET_PLAN
     */
    public BigDecimal getNetPlan() {
        return (BigDecimal) getAttributeInternal(NETPLAN);
    }

    /**
     * Sets <code>value</code> as attribute value for NET_PLAN using the alias name NetPlan.
     * @param value value to set the NET_PLAN
     */
    public void setNetPlan(BigDecimal value) {
        setAttributeInternal(NETPLAN, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER);
    }

    /**
     * Gets the attribute value for PLAN_ID using the alias name PlanId.
     * @return the PLAN_ID
     */
    public Long getPlanId() {
        return (Long) getAttributeInternal(PLANID);
    }

    /**
     * Sets <code>value</code> as attribute value for PLAN_ID using the alias name PlanId.
     * @param value value to set the PLAN_ID
     */
    public void setPlanId(Long value) {
        setAttributeInternal(PLANID, value);
    }

    /**
     * Gets the attribute value for PLAN_LINE_ID using the alias name PlanLineId.
     * @return the PLAN_LINE_ID
     */
    public Long getPlanLineId() {
        return (Long) getAttributeInternal(PLANLINEID);
    }

    /**
     * Sets <code>value</code> as attribute value for PLAN_LINE_ID using the alias name PlanLineId.
     * @param value value to set the PLAN_LINE_ID
     */
    public void setPlanLineId(Long value) {
        setAttributeInternal(PLANLINEID, value);
    }

    /**
     * Gets the attribute value for PROD_CODE using the alias name ProdCode.
     * @return the PROD_CODE
     */
    public String getProdCode() {
        return (String) getAttributeInternal(PRODCODE);
    }

    /**
     * Sets <code>value</code> as attribute value for PROD_CODE using the alias name ProdCode.
     * @param value value to set the PROD_CODE
     */
    public void setProdCode(String value) {
        setAttributeInternal(PRODCODE, value);
    }

    /**
     * Gets the attribute value for REJ_PER using the alias name RejPer.
     * @return the REJ_PER
     */
    public BigDecimal getRejPer() {
        return (BigDecimal) getAttributeInternal(REJPER);
    }

    /**
     * Sets <code>value</code> as attribute value for REJ_PER using the alias name RejPer.
     * @param value value to set the REJ_PER
     */
    public void setRejPer(BigDecimal value) {
        setAttributeInternal(REJPER, value);
    }

    /**
     * Gets the attribute value for REJ_PLAN using the alias name RejPlan.
     * @return the REJ_PLAN
     */
    public BigDecimal getRejPlan() {
        return (BigDecimal) getAttributeInternal(REJPLAN);
    }

    /**
     * Sets <code>value</code> as attribute value for REJ_PLAN using the alias name RejPlan.
     * @param value value to set the REJ_PLAN
     */
    public void setRejPlan(BigDecimal value) {
        setAttributeInternal(REJPLAN, value);
    }

    /**
     * Gets the attribute value for UNIT_CD using the alias name UnitCd.
     * @return the UNIT_CD
     */
    public String getUnitCd() {
        return (String) getAttributeInternal(UNITCD);
    }

    /**
     * Sets <code>value</code> as attribute value for UNIT_CD using the alias name UnitCd.
     * @param value value to set the UNIT_CD
     */
    public void setUnitCd(String value) {
        setAttributeInternal(UNITCD, value);
    }

    /**
     * Gets the attribute value for W1_PLAN using the alias name W1Plan.
     * @return the W1_PLAN
     */
    public BigDecimal getW1Plan() {
//        BigDecimal W1 =(BigDecimal) getAttributeInternal(W1PLAN)==null ? new BigDecimal(0):(BigDecimal) getAttributeInternal(W1PLAN);
//        BigDecimal W2 =getW2Plan()==null ? new BigDecimal(0):getW2Plan();
//        BigDecimal W3 =getW3Plan()==null ? new BigDecimal(0):getW3Plan();
//        BigDecimal W4 =getW4Plan()==null ? new BigDecimal(0):getW4Plan();
//        BigDecimal Tot=W1.add(W2).add(W3).add(W4);
//        setAttributeInternal(SUM, Tot);
        return (BigDecimal) getAttributeInternal(W1PLAN);
    }

    /**
     * Sets <code>value</code> as attribute value for W1_PLAN using the alias name W1Plan.
     * @param value value to set the W1_PLAN
     */
    public void setW1Plan(BigDecimal value) {
        setAttributeInternal(W1PLAN, value);
    }

    /**
     * Gets the attribute value for W2_PLAN using the alias name W2Plan.
     * @return the W2_PLAN
     */
    public BigDecimal getW2Plan() {
        return (BigDecimal) getAttributeInternal(W2PLAN);
    }

    /**
     * Sets <code>value</code> as attribute value for W2_PLAN using the alias name W2Plan.
     * @param value value to set the W2_PLAN
     */
    public void setW2Plan(BigDecimal value) {
        setAttributeInternal(W2PLAN, value);
    }

    /**
     * Gets the attribute value for W3_PLAN using the alias name W3Plan.
     * @return the W3_PLAN
     */
    public BigDecimal getW3Plan() {
//        BigDecimal W1 =getW1Plan()==null ? new BigDecimal(0):getW1Plan();
//        BigDecimal W2 =getW2Plan()==null ? new BigDecimal(0):getW2Plan();
//        BigDecimal W3 =(BigDecimal) getAttributeInternal(W3PLAN)==null ? new BigDecimal(0):(BigDecimal) getAttributeInternal(W3PLAN);
//        BigDecimal W4 =getW4Plan()==null ? new BigDecimal(0):getW4Plan();
//        BigDecimal Tot=W1.add(W2).add(W3).add(W4);
//            setAttributeInternal(SUM, Tot);
        return (BigDecimal) getAttributeInternal(W3PLAN);
    }

    /**
     * Sets <code>value</code> as attribute value for W3_PLAN using the alias name W3Plan.
     * @param value value to set the W3_PLAN
     */
    public void setW3Plan(BigDecimal value) {
        setAttributeInternal(W3PLAN, value);
    }

    /**
     * Gets the attribute value for W4_PLAN using the alias name W4Plan.
     * @return the W4_PLAN
     */
    public BigDecimal getW4Plan() {
//        BigDecimal W1 =getW1Plan()==null ? new BigDecimal(0):getW1Plan();
//        BigDecimal W2 =getW2Plan()==null ? new BigDecimal(0):getW2Plan();
//        BigDecimal W3 =getW3Plan()==null ? new BigDecimal(0):getW3Plan();
//        BigDecimal W4 =(BigDecimal)getAttributeInternal(W4PLAN)==null ? new BigDecimal(0):(BigDecimal)getAttributeInternal(W4PLAN);
//        BigDecimal Tot=W1.add(W2).add(W3).add(W4);
//            setAttributeInternal(SUM, Tot);
        return (BigDecimal) getAttributeInternal(W4PLAN);
    }

    /**
     * Sets <code>value</code> as attribute value for W4_PLAN using the alias name W4Plan.
     * @param value value to set the W4_PLAN
     */
    public void setW4Plan(BigDecimal value) {
        setAttributeInternal(W4PLAN, value);
    }

    /**
     * Gets the attribute value for WIP_QTY using the alias name WipQty.
     * @return the WIP_QTY
     */
    public BigDecimal getWipQty() {
        return (BigDecimal) getAttributeInternal(WIPQTY);
    }

    /**
     * Sets <code>value</code> as attribute value for WIP_QTY using the alias name WipQty.
     * @param value value to set the WIP_QTY
     */
    public void setWipQty(BigDecimal value) {
        setAttributeInternal(WIPQTY, value);
    }

    /**
     * Gets the attribute value for YEAR using the alias name Year.
     * @return the YEAR
     */
    public Integer getYear() {
        return (Integer) getAttributeInternal(YEAR);
    }

    /**
     * Sets <code>value</code> as attribute value for YEAR using the alias name Year.
     * @param value value to set the YEAR
     */
    public void setYear(Integer value) {
        setAttributeInternal(YEAR, value);
    }

    /**
     * Gets the attribute value for ITEM_DESC using the alias name ItemDesc.
     * @return the ITEM_DESC
     */
    public String getItemDesc() {
        return (String) getAttributeInternal(ITEMDESC);
    }

    /**
     * Sets <code>value</code> as attribute value for ITEM_DESC using the alias name ItemDesc.
     * @param value value to set the ITEM_DESC
     */
    public void setItemDesc(String value) {
        setAttributeInternal(ITEMDESC, value);
    }

    /**
     * Gets the attribute value for ITEM_CD using the alias name ItemCd.
     * @return the ITEM_CD
     */
    public String getItemCd() {
        return (String) getAttributeInternal(ITEMCD);
    }

    /**
     * Sets <code>value</code> as attribute value for ITEM_CD using the alias name ItemCd.
     * @param value value to set the ITEM_CD
     */
    public void setItemCd(String value) {
        setAttributeInternal(ITEMCD, value);
    }

    /**
     * Gets the attribute value for the calculated attribute sum.
     * @return the sum
     */
    public BigDecimal getsum() {
        return (BigDecimal) getAttributeInternal(SUM);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute sum.
     * @param value value to set the  sum
     */
    public void setsum(BigDecimal value) {
        setAttributeInternal(SUM, value);
    }

    /**
     * Gets the view accessor <code>RowSet</code> ManufactionPlanProductCodeLovVVO1.
     */
    public RowSet getManufactionPlanProductCodeLovVVO1() {
        return (RowSet) getAttributeInternal(MANUFACTIONPLANPRODUCTCODELOVVVO1);
    }

}

