package terms.prd.transaction.model.view;

import oracle.jbo.domain.Timestamp;

import oracle.jbo.Row;
import oracle.jbo.RowSet;
import oracle.jbo.server.EntityImpl;
import oracle.jbo.server.ViewRowImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Wed Jun 20 15:17:05 IST 2018
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class MultipleProductShortageDummyVORowImpl extends ViewRowImpl {


    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        Dummy,
        bindUnitCode,
        bindUnitName,
        bindFromDate,
        bindToDate,
        UnitVO1,
        MultipleProductShortagePopulatePlanVVO1,
        DayWiseAssyPlanDetailVO1;
        static AttributesEnum[] vals = null;
        ;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int DUMMY = AttributesEnum.Dummy.index();
    public static final int BINDUNITCODE = AttributesEnum.bindUnitCode.index();
    public static final int BINDUNITNAME = AttributesEnum.bindUnitName.index();
    public static final int BINDFROMDATE = AttributesEnum.bindFromDate.index();
    public static final int BINDTODATE = AttributesEnum.bindToDate.index();
    public static final int UNITVO1 = AttributesEnum.UnitVO1.index();
    public static final int MULTIPLEPRODUCTSHORTAGEPOPULATEPLANVVO1 =
        AttributesEnum.MultipleProductShortagePopulatePlanVVO1.index();
    public static final int DAYWISEASSYPLANDETAILVO1 = AttributesEnum.DayWiseAssyPlanDetailVO1.index();

    /**
     * This is the default constructor (do not remove).
     */
    public MultipleProductShortageDummyVORowImpl() {
    }


    /**
     * Gets the attribute value for the calculated attribute Dummy.
     * @return the Dummy
     */
    public String getDummy() {
        return (String) getAttributeInternal(DUMMY);
    }

    /**
     * Gets the attribute value for the calculated attribute bindUnitCode.
     * @return the bindUnitCode
     */
    public String getbindUnitCode() {
//        if(getAttributeInternal(BINDUNITCODE)==null)
//        {
//                    String UnitCode = "";
//                               
//                    Row[] rows = getMultipleProductShortagePopulatePlanVVO1().getFilteredRows("UnitCode", getu());
//                    if(rows.length>0)
//                    {
//                          UnitCode = rows[0].getAttribute("bindUnitCode").toString();
//                    }
//                    setbindUnitCode(UnitCode);
//                           
//                    return UnitCode;
//        
//        }
        return (String) getAttributeInternal(BINDUNITCODE);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute bindUnitCode.
     * @param value value to set the  bindUnitCode
     */
    public void setbindUnitCode(String value) {
        setAttributeInternal(BINDUNITCODE, value);
    }

    /**
     * Gets the attribute value for the calculated attribute bindUnitName.
     * @return the bindUnitName
     */
    public String getbindUnitName() {
        String UnitDesc="";
                if(getAttributeInternal(BINDUNITNAME)==null && getbindUnitCode()!=null)
                {
                    Row rows[]=getUnitVO1().getFilteredRows("Code",getbindUnitCode());
                        if(rows.length>0)
                        {
                            UnitDesc= rows[0].getAttribute("Name").toString();
                            System.out.println("Unit Desc is******"+UnitDesc);
                            return UnitDesc;
                            
                        }
                    
                }
        return (String) getAttributeInternal(BINDUNITNAME);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute bindUnitName.
     * @param value value to set the  bindUnitName
     */
    public void setbindUnitName(String value) {
        setAttributeInternal(BINDUNITNAME, value);
    }

    /**
     * Gets the attribute value for the calculated attribute bindFromDate.
     * @return the bindFromDate
     */
    public Timestamp getbindFromDate() {
        return (Timestamp) getAttributeInternal(BINDFROMDATE);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute bindFromDate.
     * @param value value to set the  bindFromDate
     */
    public void setbindFromDate(Timestamp value) {
        setAttributeInternal(BINDFROMDATE, value);
    }

    /**
     * Gets the attribute value for the calculated attribute bindToDate.
     * @return the bindToDate
     */
    public Timestamp getbindToDate() {
        return (Timestamp) getAttributeInternal(BINDTODATE);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute bindToDate.
     * @param value value to set the  bindToDate
     */
    public void setbindToDate(Timestamp value) {
        setAttributeInternal(BINDTODATE, value);
    }

    /**
     * Gets the view accessor <code>RowSet</code> UnitVO1.
     */
    public RowSet getUnitVO1() {
        return (RowSet) getAttributeInternal(UNITVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> MultipleProductShortagePopulatePlanVVO1.
     */
    public RowSet getMultipleProductShortagePopulatePlanVVO1() {
        return (RowSet) getAttributeInternal(MULTIPLEPRODUCTSHORTAGEPOPULATEPLANVVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> DayWiseAssyPlanDetailVO1.
     */
    public RowSet getDayWiseAssyPlanDetailVO1() {
        return (RowSet) getAttributeInternal(DAYWISEASSYPLANDETAILVO1);
    }
}

