package terms.prd.transaction.model.view;

import java.math.BigDecimal;

import java.sql.Timestamp;

import oracle.jbo.server.EntityImpl;
import oracle.jbo.server.ViewRowImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Fri Jun 08 16:09:48 IST 2018
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class PDIRejectionDetailReworkInputItemDtlVORowImpl extends ViewRowImpl {
    public static final int ENTITY_PDIREJECTIONDETAILREWORKINPU1 = 0;
    public static final int ENTITY_ITEMSTOCKEO = 1;
    public static final int ENTITY_PARTMASTEREO = 2;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        CreatedBy,
        CreationDate,
        ItemCd,
        LastUpdateDate,
        LastUpdatedBy,
        NewReqQty,
        NoOff,
        ObjectVersionNumber,
        OkQty,
        ProdCd,
        RejQty,
        ReworkId,
        ReworkLineId,
        ReworkNo,
        TotUseQty,
        ItemDesc,
        ItemCd1,
        Description,
        ObjectVersionNumber1,
        PartId;
        static AttributesEnum[] vals = null;
        ;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int CREATEDBY = AttributesEnum.CreatedBy.index();
    public static final int CREATIONDATE = AttributesEnum.CreationDate.index();
    public static final int ITEMCD = AttributesEnum.ItemCd.index();
    public static final int LASTUPDATEDATE = AttributesEnum.LastUpdateDate.index();
    public static final int LASTUPDATEDBY = AttributesEnum.LastUpdatedBy.index();
    public static final int NEWREQQTY = AttributesEnum.NewReqQty.index();
    public static final int NOOFF = AttributesEnum.NoOff.index();
    public static final int OBJECTVERSIONNUMBER = AttributesEnum.ObjectVersionNumber.index();
    public static final int OKQTY = AttributesEnum.OkQty.index();
    public static final int PRODCD = AttributesEnum.ProdCd.index();
    public static final int REJQTY = AttributesEnum.RejQty.index();
    public static final int REWORKID = AttributesEnum.ReworkId.index();
    public static final int REWORKLINEID = AttributesEnum.ReworkLineId.index();
    public static final int REWORKNO = AttributesEnum.ReworkNo.index();
    public static final int TOTUSEQTY = AttributesEnum.TotUseQty.index();
    public static final int ITEMDESC = AttributesEnum.ItemDesc.index();
    public static final int ITEMCD1 = AttributesEnum.ItemCd1.index();
    public static final int DESCRIPTION = AttributesEnum.Description.index();
    public static final int OBJECTVERSIONNUMBER1 = AttributesEnum.ObjectVersionNumber1.index();
    public static final int PARTID = AttributesEnum.PartId.index();

    /**
     * This is the default constructor (do not remove).
     */
    public PDIRejectionDetailReworkInputItemDtlVORowImpl() {
    }

    /**
     * Gets PDIRejectionDetailReworkInpu1 entity object.
     * @return the PDIRejectionDetailReworkInpu1
     */
    public EntityImpl getPDIRejectionDetailReworkInpu1() {
        return (EntityImpl) getEntity(ENTITY_PDIREJECTIONDETAILREWORKINPU1);
    }

    /**
     * Gets ItemStockEO entity object.
     * @return the ItemStockEO
     */
    public EntityImpl getItemStockEO() {
        return (EntityImpl) getEntity(ENTITY_ITEMSTOCKEO);
    }

    /**
     * Gets PartMasterEO entity object.
     * @return the PartMasterEO
     */
    public EntityImpl getPartMasterEO() {
        return (EntityImpl) getEntity(ENTITY_PARTMASTEREO);
    }

    /**
     * Gets the attribute value for CREATED_BY using the alias name CreatedBy.
     * @return the CREATED_BY
     */
    public String getCreatedBy() {
        return (String) getAttributeInternal(CREATEDBY);
    }

    /**
     * Gets the attribute value for CREATION_DATE using the alias name CreationDate.
     * @return the CREATION_DATE
     */
    public Timestamp getCreationDate() {
        return (Timestamp) getAttributeInternal(CREATIONDATE);
    }

    /**
     * Gets the attribute value for ITEM_CD using the alias name ItemCd.
     * @return the ITEM_CD
     */
    public String getItemCd() {
        return (String) getAttributeInternal(ITEMCD);
    }

    /**
     * Sets <code>value</code> as attribute value for ITEM_CD using the alias name ItemCd.
     * @param value value to set the ITEM_CD
     */
    public void setItemCd(String value) {
        setAttributeInternal(ITEMCD, value);
    }

    /**
     * Gets the attribute value for LAST_UPDATE_DATE using the alias name LastUpdateDate.
     * @return the LAST_UPDATE_DATE
     */
    public Timestamp getLastUpdateDate() {
        return (Timestamp) getAttributeInternal(LASTUPDATEDATE);
    }

    /**
     * Gets the attribute value for LAST_UPDATED_BY using the alias name LastUpdatedBy.
     * @return the LAST_UPDATED_BY
     */
    public String getLastUpdatedBy() {
        return (String) getAttributeInternal(LASTUPDATEDBY);
    }

    /**
     * Gets the attribute value for NEW_REQ_QTY using the alias name NewReqQty.
     * @return the NEW_REQ_QTY
     */
    public BigDecimal getNewReqQty() {
        return (BigDecimal) getAttributeInternal(NEWREQQTY);
    }

    /**
     * Sets <code>value</code> as attribute value for NEW_REQ_QTY using the alias name NewReqQty.
     * @param value value to set the NEW_REQ_QTY
     */
    public void setNewReqQty(BigDecimal value) {
        setAttributeInternal(NEWREQQTY, value);
    }

    /**
     * Gets the attribute value for NO_OFF using the alias name NoOff.
     * @return the NO_OFF
     */
    public BigDecimal getNoOff() {
        return (BigDecimal) getAttributeInternal(NOOFF);
    }

    /**
     * Sets <code>value</code> as attribute value for NO_OFF using the alias name NoOff.
     * @param value value to set the NO_OFF
     */
    public void setNoOff(BigDecimal value) {
        setAttributeInternal(NOOFF, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER, value);
    }

    /**
     * Gets the attribute value for OK_QTY using the alias name OkQty.
     * @return the OK_QTY
     */
    public BigDecimal getOkQty() {
        return (BigDecimal) getAttributeInternal(OKQTY);
    }

    /**
     * Sets <code>value</code> as attribute value for OK_QTY using the alias name OkQty.
     * @param value value to set the OK_QTY
     */
    public void setOkQty(BigDecimal value) {
                if(getTotUseQty() != null && value != null) 
                {
                    System.out.println("Used Qty ####" + getTotUseQty().subtract(value));
                    setAttributeInternal(REJQTY, getTotUseQty().subtract(value));
                }
        setAttributeInternal(OKQTY, value);
    }

    /**
     * Gets the attribute value for PROD_CD using the alias name ProdCd.
     * @return the PROD_CD
     */
    public String getProdCd() {
        return (String) getAttributeInternal(PRODCD);
    }

    /**
     * Sets <code>value</code> as attribute value for PROD_CD using the alias name ProdCd.
     * @param value value to set the PROD_CD
     */
    public void setProdCd(String value) {
        setAttributeInternal(PRODCD, value);
    }

    /**
     * Gets the attribute value for REJ_QTY using the alias name RejQty.
     * @return the REJ_QTY
     */
    public BigDecimal getRejQty() {
//        if(getTotUseQty() != null && getOkQty() != null) 
//        {
//            System.out.println("Used Qty ####" + getTotUseQty().subtract(getOkQty()));
//            setAttributeInternal(REJQTY, getTotUseQty().subtract(getOkQty()));
//        }
        return (BigDecimal) getAttributeInternal(REJQTY);
    }

    /**
     * Sets <code>value</code> as attribute value for REJ_QTY using the alias name RejQty.
     * @param value value to set the REJ_QTY
     */
    public void setRejQty(BigDecimal value) {
//        if(getTotUseQty() != null && getOkQty() != null)
//        System.out.println("Used Qty ####" + getTotUseQty().subtract(getOkQty()));   
        setAttributeInternal(REJQTY, value);
    }

    /**
     * Gets the attribute value for REWORK_ID using the alias name ReworkId.
     * @return the REWORK_ID
     */
    public Long getReworkId() {
        return (Long) getAttributeInternal(REWORKID);
    }

    /**
     * Sets <code>value</code> as attribute value for REWORK_ID using the alias name ReworkId.
     * @param value value to set the REWORK_ID
     */
    public void setReworkId(Long value) {
        setAttributeInternal(REWORKID, value);
    }

    /**
     * Gets the attribute value for REWORK_LINE_ID using the alias name ReworkLineId.
     * @return the REWORK_LINE_ID
     */
    public Long getReworkLineId() {
        return (Long) getAttributeInternal(REWORKLINEID);
    }

    /**
     * Sets <code>value</code> as attribute value for REWORK_LINE_ID using the alias name ReworkLineId.
     * @param value value to set the REWORK_LINE_ID
     */
    public void setReworkLineId(Long value) {
        setAttributeInternal(REWORKLINEID, value);
    }

    /**
     * Gets the attribute value for REWORK_NO using the alias name ReworkNo.
     * @return the REWORK_NO
     */
    public String getReworkNo() {
        return (String) getAttributeInternal(REWORKNO);
    }

    /**
     * Sets <code>value</code> as attribute value for REWORK_NO using the alias name ReworkNo.
     * @param value value to set the REWORK_NO
     */
    public void setReworkNo(String value) {
        setAttributeInternal(REWORKNO, value);
    }

    /**
     * Gets the attribute value for TOT_USE_QTY using the alias name TotUseQty.
     * @return the TOT_USE_QTY
     */
    public BigDecimal getTotUseQty() {
        return (BigDecimal) getAttributeInternal(TOTUSEQTY);
    }

    /**
     * Sets <code>value</code> as attribute value for TOT_USE_QTY using the alias name TotUseQty.
     * @param value value to set the TOT_USE_QTY
     */
    public void setTotUseQty(BigDecimal value) {
        setAttributeInternal(TOTUSEQTY, value);
    }

    /**
     * Gets the attribute value for ITEM_DESC using the alias name ItemDesc.
     * @return the ITEM_DESC
     */
    public String getItemDesc() {
        return (String) getAttributeInternal(ITEMDESC);
    }

    /**
     * Sets <code>value</code> as attribute value for ITEM_DESC using the alias name ItemDesc.
     * @param value value to set the ITEM_DESC
     */
    public void setItemDesc(String value) {
        setAttributeInternal(ITEMDESC, value);
    }

    /**
     * Gets the attribute value for ITEM_CD using the alias name ItemCd1.
     * @return the ITEM_CD
     */
    public String getItemCd1() {
        return (String) getAttributeInternal(ITEMCD1);
    }

    /**
     * Sets <code>value</code> as attribute value for ITEM_CD using the alias name ItemCd1.
     * @param value value to set the ITEM_CD
     */
    public void setItemCd1(String value) {
        setAttributeInternal(ITEMCD1, value);
    }

    /**
     * Gets the attribute value for DESCRIPTION using the alias name Description.
     * @return the DESCRIPTION
     */
    public String getDescription() {
        return (String) getAttributeInternal(DESCRIPTION);
    }

    /**
     * Sets <code>value</code> as attribute value for DESCRIPTION using the alias name Description.
     * @param value value to set the DESCRIPTION
     */
    public void setDescription(String value) {
        setAttributeInternal(DESCRIPTION, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber1.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber1() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER1);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber1.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber1(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER1, value);
    }

    /**
     * Gets the attribute value for PART_ID using the alias name PartId.
     * @return the PART_ID
     */
    public Long getPartId() {
        return (Long) getAttributeInternal(PARTID);
    }

    /**
     * Sets <code>value</code> as attribute value for PART_ID using the alias name PartId.
     * @param value value to set the PART_ID
     */
    public void setPartId(Long value) {
        setAttributeInternal(PARTID, value);
    }
}

