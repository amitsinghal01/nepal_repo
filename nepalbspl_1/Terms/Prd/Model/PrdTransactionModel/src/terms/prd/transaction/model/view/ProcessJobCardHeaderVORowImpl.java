package terms.prd.transaction.model.view;

import java.math.BigDecimal;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;

import java.util.Calendar;

import oracle.jbo.Row;
import oracle.jbo.RowIterator;
import oracle.jbo.RowSet;
import oracle.jbo.domain.Timestamp;
import oracle.jbo.server.EntityImpl;
import oracle.jbo.server.ViewRowImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Wed Apr 18 15:10:30 IST 2018
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class ProcessJobCardHeaderVORowImpl extends ViewRowImpl {

    public static final int ENTITY_PROCESSJOBCARDHEADEREO = 0;
    public static final int ENTITY_UNITEO = 1;
    public static final int ENTITY_PROCESSEO = 2;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        CreatedBy,
        CreationDate,
        CustRef,
        LastUpdateDate,
        LastUpdatedBy,
        LeafCd,
        LotDate,
        LotLoc,
        LotSt,
        MainProcCd,
        Month,
        ObjectVersionNumber,
        PlanDate,
        Qty,
        UnitCd,
        Year,
        Name,
        Code,
        ObjectVersionNumber1,
        LotId,
        LotNo,
        EditTrans,
        PartDescription,
        TransProcSeq,
        ShortDescrip,
        ObjectVersionNumber2,
        ProcId,
        ProcessJobCardDetailVO,
        ProcessJobCardLotRmReqDetailVO,
        ProcessJobCardAltPpJcDetailVO,
        UnitVO1,
        SecControlVO1,
        ProcessJobCardLeafCdLovVVO1,
        WipProcessJobCardProcessCdLovVVO1;
        static AttributesEnum[] vals = null;
        ;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int CREATEDBY = AttributesEnum.CreatedBy.index();
    public static final int CREATIONDATE = AttributesEnum.CreationDate.index();
    public static final int CUSTREF = AttributesEnum.CustRef.index();
    public static final int LASTUPDATEDATE = AttributesEnum.LastUpdateDate.index();
    public static final int LASTUPDATEDBY = AttributesEnum.LastUpdatedBy.index();
    public static final int LEAFCD = AttributesEnum.LeafCd.index();
    public static final int LOTDATE = AttributesEnum.LotDate.index();
    public static final int LOTLOC = AttributesEnum.LotLoc.index();
    public static final int LOTST = AttributesEnum.LotSt.index();
    public static final int MAINPROCCD = AttributesEnum.MainProcCd.index();
    public static final int MONTH = AttributesEnum.Month.index();
    public static final int OBJECTVERSIONNUMBER = AttributesEnum.ObjectVersionNumber.index();
    public static final int PLANDATE = AttributesEnum.PlanDate.index();
    public static final int QTY = AttributesEnum.Qty.index();
    public static final int UNITCD = AttributesEnum.UnitCd.index();
    public static final int YEAR = AttributesEnum.Year.index();
    public static final int NAME = AttributesEnum.Name.index();
    public static final int CODE = AttributesEnum.Code.index();
    public static final int OBJECTVERSIONNUMBER1 = AttributesEnum.ObjectVersionNumber1.index();
    public static final int LOTID = AttributesEnum.LotId.index();
    public static final int LOTNO = AttributesEnum.LotNo.index();
    public static final int EDITTRANS = AttributesEnum.EditTrans.index();
    public static final int PARTDESCRIPTION = AttributesEnum.PartDescription.index();
    public static final int TRANSPROCSEQ = AttributesEnum.TransProcSeq.index();
    public static final int SHORTDESCRIP = AttributesEnum.ShortDescrip.index();
    public static final int OBJECTVERSIONNUMBER2 = AttributesEnum.ObjectVersionNumber2.index();
    public static final int PROCID = AttributesEnum.ProcId.index();
    public static final int PROCESSJOBCARDDETAILVO = AttributesEnum.ProcessJobCardDetailVO.index();
    public static final int PROCESSJOBCARDLOTRMREQDETAILVO = AttributesEnum.ProcessJobCardLotRmReqDetailVO.index();
    public static final int PROCESSJOBCARDALTPPJCDETAILVO = AttributesEnum.ProcessJobCardAltPpJcDetailVO.index();
    public static final int UNITVO1 = AttributesEnum.UnitVO1.index();
    public static final int SECCONTROLVO1 = AttributesEnum.SecControlVO1.index();
    public static final int PROCESSJOBCARDLEAFCDLOVVVO1 = AttributesEnum.ProcessJobCardLeafCdLovVVO1.index();
    public static final int WIPPROCESSJOBCARDPROCESSCDLOVVVO1 =
        AttributesEnum.WipProcessJobCardProcessCdLovVVO1.index();

    /**
     * This is the default constructor (do not remove).
     */
    public ProcessJobCardHeaderVORowImpl() {
    }

    /**
     * Gets ProcessJobCardHeaderEO entity object.
     * @return the ProcessJobCardHeaderEO
     */
    public EntityImpl getProcessJobCardHeaderEO() {
        return (EntityImpl) getEntity(ENTITY_PROCESSJOBCARDHEADEREO);
    }

    /**
     * Gets UnitEO entity object.
     * @return the UnitEO
     */
    public EntityImpl getUnitEO() {
        return (EntityImpl) getEntity(ENTITY_UNITEO);
    }


    /**
     * Gets ProcessEO entity object.
     * @return the ProcessEO
     */
    public EntityImpl getProcessEO() {
        return (EntityImpl) getEntity(ENTITY_PROCESSEO);
    }

    /**
     * Gets the attribute value for CREATED_BY using the alias name CreatedBy.
     * @return the CREATED_BY
     */
    public String getCreatedBy() {
        return (String) getAttributeInternal(CREATEDBY);
    }

    /**
     * Sets <code>value</code> as attribute value for CREATED_BY using the alias name CreatedBy.
     * @param value value to set the CREATED_BY
     */
    public void setCreatedBy(String value) {
        setAttributeInternal(CREATEDBY, value);
    }

    /**
     * Gets the attribute value for CREATION_DATE using the alias name CreationDate.
     * @return the CREATION_DATE
     */
    public Timestamp getCreationDate() {
        return (Timestamp) getAttributeInternal(CREATIONDATE);
    }

    /**
     * Gets the attribute value for CUST_REF using the alias name CustRef.
     * @return the CUST_REF
     */
    public String getCustRef() {
        return (String) getAttributeInternal(CUSTREF);
    }

    /**
     * Sets <code>value</code> as attribute value for CUST_REF using the alias name CustRef.
     * @param value value to set the CUST_REF
     */
    public void setCustRef(String value) {
        setAttributeInternal(CUSTREF, value);
    }

    /**
     * Gets the attribute value for LAST_UPDATE_DATE using the alias name LastUpdateDate.
     * @return the LAST_UPDATE_DATE
     */
    public oracle.jbo.domain.Timestamp getLastUpdateDate() {
        return (oracle.jbo.domain.Timestamp) getAttributeInternal(LASTUPDATEDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for LAST_UPDATE_DATE using the alias name LastUpdateDate.
     * @param value value to set the LAST_UPDATE_DATE
     */
    public void setLastUpdateDate(oracle.jbo.domain.Timestamp value) {
        setAttributeInternal(LASTUPDATEDATE, value);
    }

    /**
     * Gets the attribute value for LAST_UPDATED_BY using the alias name LastUpdatedBy.
     * @return the LAST_UPDATED_BY
     */
    public String getLastUpdatedBy() {
        return (String) getAttributeInternal(LASTUPDATEDBY);
    }

    /**
     * Sets <code>value</code> as attribute value for LAST_UPDATED_BY using the alias name LastUpdatedBy.
     * @param value value to set the LAST_UPDATED_BY
     */
    public void setLastUpdatedBy(String value) {
        setAttributeInternal(LASTUPDATEDBY, value);
    }

    /**
     * Gets the attribute value for LEAF_CD using the alias name LeafCd.
     * @return the LEAF_CD
     */
    public String getLeafCd() {
        return (String) getAttributeInternal(LEAFCD);
    }

    /**
     * Sets <code>value</code> as attribute value for LEAF_CD using the alias name LeafCd.
     * @param value value to set the LEAF_CD
     */
    public void setLeafCd(String value) {
        setAttributeInternal(LEAFCD, value);
    }

    /**
     * Gets the attribute value for LOT_DATE using the alias name LotDate.
     * @return the LOT_DATE
     */
    public Timestamp getLotDate() {
        return (Timestamp) getAttributeInternal(LOTDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for LOT_DATE using the alias name LotDate.
     * @param value value to set the LOT_DATE
     */
    public void setLotDate(Timestamp value) {
        setAttributeInternal(LOTDATE, value);
    }

    /**
     * Gets the attribute value for LOT_LOC using the alias name LotLoc.
     * @return the LOT_LOC
     */
    public String getLotLoc() {
        return (String) getAttributeInternal(LOTLOC);
    }

    /**
     * Sets <code>value</code> as attribute value for LOT_LOC using the alias name LotLoc.
     * @param value value to set the LOT_LOC
     */
    public void setLotLoc(String value) {
        setAttributeInternal(LOTLOC, value);
    }

    /**
     * Gets the attribute value for LOT_ST using the alias name LotSt.
     * @return the LOT_ST
     */
    public String getLotSt() {
        return (String) getAttributeInternal(LOTST);
    }

    /**
     * Sets <code>value</code> as attribute value for LOT_ST using the alias name LotSt.
     * @param value value to set the LOT_ST
     */
    public void setLotSt(String value) {
        setAttributeInternal(LOTST, value);
    }

    /**
     * Gets the attribute value for MAIN_PROC_CD using the alias name MainProcCd.
     * @return the MAIN_PROC_CD
     */
    public String getMainProcCd() {
        return (String) getAttributeInternal(MAINPROCCD);
    }

    /**
     * Sets <code>value</code> as attribute value for MAIN_PROC_CD using the alias name MainProcCd.
     * @param value value to set the MAIN_PROC_CD
     */
    public void setMainProcCd(String value) {
        setAttributeInternal(MAINPROCCD, value);
    }

    /**
     * Gets the attribute value for MONTH using the alias name Month.
     * @return the MONTH
     */
    public String getMonth() {
        String[] Mon={"JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"};
        Calendar cal=Calendar.getInstance();
        String month="";
        month=(String)Mon[cal.get(Calendar.MONTH)];
        System.out.println("CURRRENT MONTHHHHHHHHH"+month);
        if(getAttributeInternal(MONTH)==null || getAttributeInternal(MONTH).equals("") )
        {
            setAttributeInternal(MONTH, month);
        }
        return (String) getAttributeInternal(MONTH);
 
}

    /**
     * Sets <code>value</code> as attribute value for MONTH using the alias name Month.
     * @param value value to set the MONTH
     */
    public void setMonth(String value) {
        setAttributeInternal(MONTH, value);
        
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER, value);
    }

    /**
     * Gets the attribute value for PLAN_DATE using the alias name PlanDate.
     * @return the PLAN_DATE
     */
    public Timestamp getPlanDate() {
        return (Timestamp) getAttributeInternal(PLANDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for PLAN_DATE using the alias name PlanDate.
     * @param value value to set the PLAN_DATE
     */
    public void setPlanDate(Timestamp value) {
        setAttributeInternal(PLANDATE, value);
    }

    /**
     * Gets the attribute value for QTY using the alias name Qty.
     * @return the QTY
     */
    public BigDecimal getQty() {
        return (BigDecimal) getAttributeInternal(QTY);
    }

    /**
     * Sets <code>value</code> as attribute value for QTY using the alias name Qty.
     * @param value value to set the QTY
     */
    public void setQty(BigDecimal value) {
        setAttributeInternal(QTY, value);
    }

    /**
     * Gets the attribute value for UNIT_CD using the alias name UnitCd.
     * @return the UNIT_CD
     */
    public String getUnitCd() {
        return (String) getAttributeInternal(UNITCD);
    }

    /**
     * Sets <code>value</code> as attribute value for UNIT_CD using the alias name UnitCd.
     * @param value value to set the UNIT_CD
     */
    public void setUnitCd(String value) {
        setAttributeInternal(UNITCD, value);
    }

    /**
     * Gets the attribute value for YEAR using the alias name Year.
     * @return the YEAR
     */
    public Integer getYear() {   
        Calendar cal=Calendar.getInstance();
        Integer year=0;
        year=(Integer)cal.get(Calendar.YEAR);
        System.out.println("CURRENT YEARRRR"+year);
        if(getAttributeInternal(YEAR)==null)
        {
            setAttributeInternal(YEAR, year);
        }
        return (Integer) getAttributeInternal(YEAR);

    }

    /**
     * Sets <code>value</code> as attribute value for YEAR using the alias name Year.
     * @param value value to set the YEAR
     */
    public void setYear(Integer value) {
            setAttributeInternal(YEAR, value);
    
    }

    /**
     * Gets the attribute value for NAME using the alias name Name.
     * @return the NAME
     */
    public String getName() {
        return (String) getAttributeInternal(NAME);
    }

    /**
     * Sets <code>value</code> as attribute value for NAME using the alias name Name.
     * @param value value to set the NAME
     */
    public void setName(String value) {
        setAttributeInternal(NAME, value);
    }

    /**
     * Gets the attribute value for CODE using the alias name Code.
     * @return the CODE
     */
    public String getCode() {
        return (String) getAttributeInternal(CODE);
    }

    /**
     * Sets <code>value</code> as attribute value for CODE using the alias name Code.
     * @param value value to set the CODE
     */
    public void setCode(String value) {
        setAttributeInternal(CODE, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber1.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber1() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER1);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber1.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber1(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER1, value);
    }

    /**
     * Gets the attribute value for LOT_ID using the alias name LotId.
     * @return the LOT_ID
     */
    public Long getLotId() {
        return (Long) getAttributeInternal(LOTID);
    }

    /**
     * Sets <code>value</code> as attribute value for LOT_ID using the alias name LotId.
     * @param value value to set the LOT_ID
     */
    public void setLotId(Long value) {
        setAttributeInternal(LOTID, value);
    }

    /**
     * Gets the attribute value for LOT_NO using the alias name LotNo.
     * @return the LOT_NO
     */
    public String getLotNo() {
        return (String) getAttributeInternal(LOTNO);
    }

    /**
     * Sets <code>value</code> as attribute value for LOT_NO using the alias name LotNo.
     * @param value value to set the LOT_NO
     */
    public void setLotNo(String value) {
        setAttributeInternal(LOTNO, value);
    }

    /**
     * Gets the attribute value for the calculated attribute EditTrans.
     * @return the EditTrans
     */
    public Integer getEditTrans() {
        byte entityState = this.getEntity(0).getEntityState();
        return new Integer(entityState);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute EditTrans.
     * @param value value to set the  EditTrans
     */
    public void setEditTrans(Integer value) {
        setAttributeInternal(EDITTRANS, value);
    }

    /**
     * Gets the attribute value for the calculated attribute PartDescription.
     * @return the PartDescription
     */
    public String getPartDescription() {
        if(getAttributeInternal(PARTDESCRIPTION)==null && getLeafCd()!=null){
        String  Description ="";
        Row r[]=this.getProcessJobCardLeafCdLovVVO1().getFilteredRows("PartCode", getLeafCd());
        if(r.length>0)
        {
        Description=r[0].getAttribute("Description").toString();
        }
        setPartDescription(Description);
        return (Description);
      }
        return (String) getAttributeInternal(PARTDESCRIPTION);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute PartDescription.
     * @param value value to set the  PartDescription
     */
    public void setPartDescription(String value) {
        setAttributeInternal(PARTDESCRIPTION, value);
    }

    /**
     * Gets the attribute value for the calculated attribute TransProcSeq.
     * @return the TransProcSeq
     */
    public Integer getTransProcSeq() {
        if(getLeafCd()!=null && getMainProcCd()!=null)
        {
            Row r[]=this.getWipProcessJobCardProcessCdLovVVO1().getFilteredRows("PartCode", getLeafCd());
            r=this.getWipProcessJobCardProcessCdLovVVO1().getFilteredRows("ProcCode", getMainProcCd());
            if(r.length>0 && r.length<2)
            {
                setAttributeInternal(TRANSPROCSEQ,r[0].getAttribute("ProcSeq") ); 
            }
                
        }
        return (Integer) getAttributeInternal(TRANSPROCSEQ);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute TransProcSeq.
     * @param value value to set the  TransProcSeq
     */
    public void setTransProcSeq(Integer value) {
        setAttributeInternal(TRANSPROCSEQ, value);
    }

    /**
     * Gets the attribute value for SHORT_DESCRIP using the alias name ShortDescrip.
     * @return the SHORT_DESCRIP
     */
    public String getShortDescrip() {
        return (String) getAttributeInternal(SHORTDESCRIP);
    }

    /**
     * Sets <code>value</code> as attribute value for SHORT_DESCRIP using the alias name ShortDescrip.
     * @param value value to set the SHORT_DESCRIP
     */
    public void setShortDescrip(String value) {
        setAttributeInternal(SHORTDESCRIP, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber2.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber2() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER2);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber2.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber2(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER2, value);
    }

    /**
     * Gets the attribute value for PROC_ID using the alias name ProcId.
     * @return the PROC_ID
     */
    public Long getProcId() {
        return (Long) getAttributeInternal(PROCID);
    }

    /**
     * Sets <code>value</code> as attribute value for PROC_ID using the alias name ProcId.
     * @param value value to set the PROC_ID
     */
    public void setProcId(Long value) {
        setAttributeInternal(PROCID, value);
    }

    /**
     * Gets the associated <code>RowIterator</code> using master-detail link ProcessJobCardDetailVO.
     */
    public RowIterator getProcessJobCardDetailVO() {
        return (RowIterator) getAttributeInternal(PROCESSJOBCARDDETAILVO);
    }

    /**
     * Gets the associated <code>RowIterator</code> using master-detail link ProcessJobCardLotRmReqDetailVO.
     */
    public RowIterator getProcessJobCardLotRmReqDetailVO() {
        return (RowIterator) getAttributeInternal(PROCESSJOBCARDLOTRMREQDETAILVO);
    }

    /**
     * Gets the associated <code>RowIterator</code> using master-detail link ProcessJobCardAltPpJcDetailVO.
     */
    public RowIterator getProcessJobCardAltPpJcDetailVO() {
        return (RowIterator) getAttributeInternal(PROCESSJOBCARDALTPPJCDETAILVO);
    }

    /**
     * Gets the view accessor <code>RowSet</code> UnitVO1.
     */
    public RowSet getUnitVO1() {
        return (RowSet) getAttributeInternal(UNITVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> SecControlVO1.
     */
    public RowSet getSecControlVO1() {
        return (RowSet) getAttributeInternal(SECCONTROLVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> ProcessJobCardLeafCdLovVVO1.
     */
    public RowSet getProcessJobCardLeafCdLovVVO1() {
        return (RowSet) getAttributeInternal(PROCESSJOBCARDLEAFCDLOVVVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> WipProcessJobCardProcessCdLovVVO1.
     */
    public RowSet getWipProcessJobCardProcessCdLovVVO1() {
        return (RowSet) getAttributeInternal(WIPPROCESSJOBCARDPROCESSCDLOVVVO1);
    }

}

