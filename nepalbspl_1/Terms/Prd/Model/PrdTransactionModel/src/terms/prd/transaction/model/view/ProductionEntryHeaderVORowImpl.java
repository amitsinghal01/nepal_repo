package terms.prd.transaction.model.view;

import java.sql.Timestamp;

import oracle.jbo.RowIterator;
import oracle.jbo.RowSet;
import oracle.jbo.server.EntityImpl;
import oracle.jbo.server.ViewRowImpl;

import terms.hrm.setup.model.entity.EmployeeMasterEOImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Fri Oct 26 11:11:26 IST 2018
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class ProductionEntryHeaderVORowImpl extends ViewRowImpl {
    public static final int ENTITY_PRODUCTIONENTRYHEADEREO = 0;
    public static final int ENTITY_UNITEO = 1;
    public static final int ENTITY_EMPLOYEEMASTEREO = 2;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        CreatedBy,
        CreationDate,
        EntryDate,
        EntryId,
        EntryNo,
        LastUpdateDate,
        LastUpdatedBy,
        ObjectVersionNumber,
        PlanDate,
        UnitCd,
        Name,
        Code,
        ObjectVersionNumber1,
        EditTrans,
        ShiftCdTrans,
        QcdNo,
        PrdType,
        ApproveDt,
        ApprovedBy,
        EmpLastName,
        EmpFirstName,
        EmpNumber,
        ObjectVersionNumber2,
        ProductionEntryDetailVO,
        ProductionEntryDetailOperatorVO,
        DocAttachRefDtlVO,
        UnitVO1,
        ShiftMasterVO1,
        EmpViewVVO1,
        PrdTypeSecControlVO1;
        static AttributesEnum[] vals = null;
        ;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int CREATEDBY = AttributesEnum.CreatedBy.index();
    public static final int CREATIONDATE = AttributesEnum.CreationDate.index();
    public static final int ENTRYDATE = AttributesEnum.EntryDate.index();
    public static final int ENTRYID = AttributesEnum.EntryId.index();
    public static final int ENTRYNO = AttributesEnum.EntryNo.index();
    public static final int LASTUPDATEDATE = AttributesEnum.LastUpdateDate.index();
    public static final int LASTUPDATEDBY = AttributesEnum.LastUpdatedBy.index();
    public static final int OBJECTVERSIONNUMBER = AttributesEnum.ObjectVersionNumber.index();
    public static final int PLANDATE = AttributesEnum.PlanDate.index();
    public static final int UNITCD = AttributesEnum.UnitCd.index();
    public static final int NAME = AttributesEnum.Name.index();
    public static final int CODE = AttributesEnum.Code.index();
    public static final int OBJECTVERSIONNUMBER1 = AttributesEnum.ObjectVersionNumber1.index();
    public static final int EDITTRANS = AttributesEnum.EditTrans.index();
    public static final int SHIFTCDTRANS = AttributesEnum.ShiftCdTrans.index();
    public static final int QCDNO = AttributesEnum.QcdNo.index();
    public static final int PRDTYPE = AttributesEnum.PrdType.index();
    public static final int APPROVEDT = AttributesEnum.ApproveDt.index();
    public static final int APPROVEDBY = AttributesEnum.ApprovedBy.index();
    public static final int EMPLASTNAME = AttributesEnum.EmpLastName.index();
    public static final int EMPFIRSTNAME = AttributesEnum.EmpFirstName.index();
    public static final int EMPNUMBER = AttributesEnum.EmpNumber.index();
    public static final int OBJECTVERSIONNUMBER2 = AttributesEnum.ObjectVersionNumber2.index();
    public static final int PRODUCTIONENTRYDETAILVO = AttributesEnum.ProductionEntryDetailVO.index();
    public static final int PRODUCTIONENTRYDETAILOPERATORVO = AttributesEnum.ProductionEntryDetailOperatorVO.index();
    public static final int DOCATTACHREFDTLVO = AttributesEnum.DocAttachRefDtlVO.index();
    public static final int UNITVO1 = AttributesEnum.UnitVO1.index();
    public static final int SHIFTMASTERVO1 = AttributesEnum.ShiftMasterVO1.index();
    public static final int EMPVIEWVVO1 = AttributesEnum.EmpViewVVO1.index();
    public static final int PRDTYPESECCONTROLVO1 = AttributesEnum.PrdTypeSecControlVO1.index();

    /**
     * This is the default constructor (do not remove).
     */
    public ProductionEntryHeaderVORowImpl() {
    }

    /**
     * Gets ProductionEntryHeaderEO entity object.
     * @return the ProductionEntryHeaderEO
     */
    public EntityImpl getProductionEntryHeaderEO() {
        return (EntityImpl) getEntity(ENTITY_PRODUCTIONENTRYHEADEREO);
    }

    /**
     * Gets UnitEO entity object.
     * @return the UnitEO
     */
    public EntityImpl getUnitEO() {
        return (EntityImpl) getEntity(ENTITY_UNITEO);
    }

    /**
     * Gets EmployeeMasterEO entity object.
     * @return the EmployeeMasterEO
     */
    public EmployeeMasterEOImpl getEmployeeMasterEO() {
        return (EmployeeMasterEOImpl) getEntity(ENTITY_EMPLOYEEMASTEREO);
    }

    /**
     * Gets the attribute value for CREATED_BY using the alias name CreatedBy.
     * @return the CREATED_BY
     */
    public String getCreatedBy() {
        return (String) getAttributeInternal(CREATEDBY);
    }

    /**
     * Sets <code>value</code> as attribute value for CREATED_BY using the alias name CreatedBy.
     * @param value value to set the CREATED_BY
     */
    public void setCreatedBy(String value) {
        setAttributeInternal(CREATEDBY, value);
    }

    /**
     * Gets the attribute value for CREATION_DATE using the alias name CreationDate.
     * @return the CREATION_DATE
     */
    public Timestamp getCreationDate() {
        return (Timestamp) getAttributeInternal(CREATIONDATE);
    }

    /**
     * Gets the attribute value for ENTRY_DATE using the alias name EntryDate.
     * @return the ENTRY_DATE
     */
    public Timestamp getEntryDate() {
        return (Timestamp) getAttributeInternal(ENTRYDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for ENTRY_DATE using the alias name EntryDate.
     * @param value value to set the ENTRY_DATE
     */
    public void setEntryDate(Timestamp value) {
        setAttributeInternal(ENTRYDATE, value);
    }

    /**
     * Gets the attribute value for ENTRY_ID using the alias name EntryId.
     * @return the ENTRY_ID
     */
    public Long getEntryId() {
        return (Long) getAttributeInternal(ENTRYID);
    }

    /**
     * Sets <code>value</code> as attribute value for ENTRY_ID using the alias name EntryId.
     * @param value value to set the ENTRY_ID
     */
    public void setEntryId(Long value) {
        setAttributeInternal(ENTRYID, value);
    }

    /**
     * Gets the attribute value for ENTRY_NO using the alias name EntryNo.
     * @return the ENTRY_NO
     */
    public String getEntryNo() {
        return (String) getAttributeInternal(ENTRYNO);
    }

    /**
     * Sets <code>value</code> as attribute value for ENTRY_NO using the alias name EntryNo.
     * @param value value to set the ENTRY_NO
     */
    public void setEntryNo(String value) {
        setAttributeInternal(ENTRYNO, value);
    }

    /**
     * Gets the attribute value for LAST_UPDATE_DATE using the alias name LastUpdateDate.
     * @return the LAST_UPDATE_DATE
     */
    public oracle.jbo.domain.Timestamp getLastUpdateDate() {
        return (oracle.jbo.domain.Timestamp) getAttributeInternal(LASTUPDATEDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for LAST_UPDATE_DATE using the alias name LastUpdateDate.
     * @param value value to set the LAST_UPDATE_DATE
     */
    public void setLastUpdateDate(oracle.jbo.domain.Timestamp value) {
        setAttributeInternal(LASTUPDATEDATE, value);
    }

    /**
     * Gets the attribute value for LAST_UPDATED_BY using the alias name LastUpdatedBy.
     * @return the LAST_UPDATED_BY
     */
    public String getLastUpdatedBy() {
        return (String) getAttributeInternal(LASTUPDATEDBY);
    }

    /**
     * Sets <code>value</code> as attribute value for LAST_UPDATED_BY using the alias name LastUpdatedBy.
     * @param value value to set the LAST_UPDATED_BY
     */
    public void setLastUpdatedBy(String value) {
        setAttributeInternal(LASTUPDATEDBY, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER, value);
    }

    /**
     * Gets the attribute value for PLAN_DATE using the alias name PlanDate.
     * @return the PLAN_DATE
     */
    public Timestamp getPlanDate() {
        return (Timestamp) getAttributeInternal(PLANDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for PLAN_DATE using the alias name PlanDate.
     * @param value value to set the PLAN_DATE
     */
    public void setPlanDate(Timestamp value) {
        setAttributeInternal(PLANDATE, value);
    }

    /**
     * Gets the attribute value for UNIT_CD using the alias name UnitCd.
     * @return the UNIT_CD
     */
    public String getUnitCd() {
        return (String) getAttributeInternal(UNITCD);
    }

    /**
     * Sets <code>value</code> as attribute value for UNIT_CD using the alias name UnitCd.
     * @param value value to set the UNIT_CD
     */
    public void setUnitCd(String value) {
        setAttributeInternal(UNITCD, value);
    }

    /**
     * Gets the attribute value for NAME using the alias name Name.
     * @return the NAME
     */
    public String getName() {
        return (String) getAttributeInternal(NAME);
    }

    /**
     * Sets <code>value</code> as attribute value for NAME using the alias name Name.
     * @param value value to set the NAME
     */
    public void setName(String value) {
        setAttributeInternal(NAME, value);
    }

    /**
     * Gets the attribute value for CODE using the alias name Code.
     * @return the CODE
     */
    public String getCode() {
        return (String) getAttributeInternal(CODE);
    }

    /**
     * Sets <code>value</code> as attribute value for CODE using the alias name Code.
     * @param value value to set the CODE
     */
    public void setCode(String value) {
        setAttributeInternal(CODE, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber1.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber1() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER1);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber1.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber1(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER1, value);
    }

    /**
     * Gets the attribute value for the calculated attribute EditTrans.
     * @return the EditTrans
     */
    public Integer getEditTrans() {
        byte entityState = this.getEntity(0).getEntityState();
        return new Integer(entityState);
//        return (Integer) getAttributeInternal(EDITTRANS);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute EditTrans.
     * @param value value to set the  EditTrans
     */
    public void setEditTrans(Integer value) {
        setAttributeInternal(EDITTRANS, value);
    }

    /**
     * Gets the attribute value for the calculated attribute ShiftCdTrans.
     * @return the ShiftCdTrans
     */
    public String getShiftCdTrans() {
        return (String) getAttributeInternal(SHIFTCDTRANS);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute ShiftCdTrans.
     * @param value value to set the  ShiftCdTrans
     */
    public void setShiftCdTrans(String value) {
        setAttributeInternal(SHIFTCDTRANS, value);
    }

    /**
     * Gets the attribute value for QCD_NO using the alias name QcdNo.
     * @return the QCD_NO
     */
    public String getQcdNo() {
        return (String) getAttributeInternal(QCDNO);
    }

    /**
     * Sets <code>value</code> as attribute value for QCD_NO using the alias name QcdNo.
     * @param value value to set the QCD_NO
     */
    public void setQcdNo(String value) {
        setAttributeInternal(QCDNO, value);
    }

    /**
     * Gets the attribute value for PRD_TYPE using the alias name PrdType.
     * @return the PRD_TYPE
     */
    public String getPrdType() {
        return (String) getAttributeInternal(PRDTYPE);
    }

    /**
     * Sets <code>value</code> as attribute value for PRD_TYPE using the alias name PrdType.
     * @param value value to set the PRD_TYPE
     */
    public void setPrdType(String value) {
        setAttributeInternal(PRDTYPE, value);
    }

    /**
     * Gets the attribute value for APPROVE_DT using the alias name ApproveDt.
     * @return the APPROVE_DT
     */
    public Timestamp getApproveDt() {
        return (Timestamp) getAttributeInternal(APPROVEDT);
    }

    /**
     * Sets <code>value</code> as attribute value for APPROVE_DT using the alias name ApproveDt.
     * @param value value to set the APPROVE_DT
     */
    public void setApproveDt(Timestamp value) {
        setAttributeInternal(APPROVEDT, value);
    }

    /**
     * Gets the attribute value for APPROVED_BY using the alias name ApprovedBy.
     * @return the APPROVED_BY
     */
    public String getApprovedBy() {
        return (String) getAttributeInternal(APPROVEDBY);
    }

    /**
     * Sets <code>value</code> as attribute value for APPROVED_BY using the alias name ApprovedBy.
     * @param value value to set the APPROVED_BY
     */
    public void setApprovedBy(String value) {
        setAttributeInternal(APPROVEDBY, value);
    }

    /**
     * Gets the attribute value for EMP_LAST_NAME using the alias name EmpLastName.
     * @return the EMP_LAST_NAME
     */
    public String getEmpLastName() {
        return (String) getAttributeInternal(EMPLASTNAME);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_LAST_NAME using the alias name EmpLastName.
     * @param value value to set the EMP_LAST_NAME
     */
    public void setEmpLastName(String value) {
        setAttributeInternal(EMPLASTNAME, value);
    }

    /**
     * Gets the attribute value for EMP_FIRST_NAME using the alias name EmpFirstName.
     * @return the EMP_FIRST_NAME
     */
    public String getEmpFirstName() {
        if((getAttributeInternal(EMPFIRSTNAME) !=null)
        &&(getAttributeInternal(EMPLASTNAME)!=null)){
                return (String) getAttributeInternal(EMPFIRSTNAME)+" "+(String)
        getAttributeInternal(EMPLASTNAME);
                } else {
                    return (String) getAttributeInternal(EMPFIRSTNAME);
                }
//        return (String) getAttributeInternal(EMPFIRSTNAME);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_FIRST_NAME using the alias name EmpFirstName.
     * @param value value to set the EMP_FIRST_NAME
     */
    public void setEmpFirstName(String value) {
        setAttributeInternal(EMPFIRSTNAME, value);
    }

    /**
     * Gets the attribute value for EMP_NUMBER using the alias name EmpNumber.
     * @return the EMP_NUMBER
     */
    public String getEmpNumber() {
        return (String) getAttributeInternal(EMPNUMBER);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_NUMBER using the alias name EmpNumber.
     * @param value value to set the EMP_NUMBER
     */
    public void setEmpNumber(String value) {
        setAttributeInternal(EMPNUMBER, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber2.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber2() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER2);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber2.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber2(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER2, value);
    }

    /**
     * Gets the associated <code>RowIterator</code> using master-detail link ProductionEntryDetailVO.
     */
    public RowIterator getProductionEntryDetailVO() {
        return (RowIterator) getAttributeInternal(PRODUCTIONENTRYDETAILVO);
    }

    /**
     * Gets the associated <code>RowIterator</code> using master-detail link ProductionEntryDetailOperatorVO.
     */
    public RowIterator getProductionEntryDetailOperatorVO() {
        return (RowIterator) getAttributeInternal(PRODUCTIONENTRYDETAILOPERATORVO);
    }

    /**
     * Gets the associated <code>RowIterator</code> using master-detail link DocAttachRefDtlVO.
     */
    public RowIterator getDocAttachRefDtlVO() {
        return (RowIterator) getAttributeInternal(DOCATTACHREFDTLVO);
    }

    /**
     * Gets the view accessor <code>RowSet</code> UnitVO1.
     */
    public RowSet getUnitVO1() {
        return (RowSet) getAttributeInternal(UNITVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> ShiftMasterVO1.
     */
    public RowSet getShiftMasterVO1() {
        return (RowSet) getAttributeInternal(SHIFTMASTERVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> EmpViewVVO1.
     */
    public RowSet getEmpViewVVO1() {
        return (RowSet) getAttributeInternal(EMPVIEWVVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> SecControlVO1.
     */
    public RowSet getPrdTypeSecControlVO1() {
        return (RowSet) getAttributeInternal(PRDTYPESECCONTROLVO1);
    }
}

