package terms.prd.setup.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichPanelLabelAndMessage;
import oracle.adf.view.rich.component.rich.nav.RichButton;

public class CreateProductLineMappingBean {
    private RichInputComboboxListOfValues productCodeBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText unitDescriptionBinding;
    private RichInputText productDescriptionBinding;
    private RichInputComboboxListOfValues lineNoBinding;
    private RichInputText pcsInvolvingBinding;
    private RichPanelLabelAndMessage bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichButton saveALBinding;
    private RichButton saveAndCloseALBinding;
    private RichInputText assemblyCodeBinding;
    private RichInputText menInvolveBinding;

    public CreateProductLineMappingBean() {
    }

    public void saveDataAL(ActionEvent actionEvent) {
             ADFUtils.setLastUpdatedBy("ProductLineMappingVO1Iterator","LastUpdatedBy");
                  if(!productCodeBinding.isDisabled())
                      {
                      System.out.println("In Bean");
             ADFUtils.findOperation("Commit").execute();
                          System.out.println("In commit");
             FacesMessage Message = new FacesMessage("Record Saved Successfully."); 
                         Message.setSeverity(FacesMessage.SEVERITY_INFO); 
                         FacesContext fc = FacesContext.getCurrentInstance(); 
                         fc.addMessage(null, Message);    
                        
                      }
                      else
                      {
                          FacesMessage Message = new FacesMessage("Record Updated Successfully."); 
                          Message.setSeverity(FacesMessage.SEVERITY_INFO); 
                          FacesContext fc = FacesContext.getCurrentInstance(); 
                          fc.addMessage(null, Message);    
                      }
    }

    public void setProductCodeBinding(RichInputComboboxListOfValues productCodeBinding) {
        this.productCodeBinding = productCodeBinding;
    }

    public RichInputComboboxListOfValues getProductCodeBinding() {
        return productCodeBinding;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setUnitDescriptionBinding(RichInputText unitDescriptionBinding) {
        this.unitDescriptionBinding = unitDescriptionBinding;
    }

    public RichInputText getUnitDescriptionBinding() {
        return unitDescriptionBinding;
    }

    public void setProductDescriptionBinding(RichInputText productDescriptionBinding) {
        this.productDescriptionBinding = productDescriptionBinding;
    }

    public RichInputText getProductDescriptionBinding() {
        return productDescriptionBinding;
    }

    public void setLineNoBinding(RichInputComboboxListOfValues lineNoBinding) {
        this.lineNoBinding = lineNoBinding;
    }

    public RichInputComboboxListOfValues getLineNoBinding() {
        return lineNoBinding;
    }

    public void setPcsInvolvingBinding(RichInputText pcsInvolvingBinding) {
        this.pcsInvolvingBinding = pcsInvolvingBinding;
    }

    public RichInputText getPcsInvolvingBinding() {
        return pcsInvolvingBinding;
    }

    public void setBindingOutputText(RichPanelLabelAndMessage bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichPanelLabelAndMessage getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }
    
    //Set Fields and Button disable.
        public void cevModeDisableComponent(String mode) {
        //            FacesContext fctx = FacesContext.getCurrentInstance();
        //            ELContext elctx = fctx.getELContext();
        //            Application jsfApp = fctx.getApplication();
        //            //create a ValueExpression that points to the ADF binding layer
        //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
        //            //
        //            ValueExpression valueExpr = exprFactory.createValueExpression(
        //                                         elctx,
        //                                         "#{pageFlowScope.mode=='E'}",
        //                                          Object.class
        //                                         );
        //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
            if (mode.equals("E")) {
                getUnitCodeBinding().setDisabled(true);
                getUnitDescriptionBinding().setDisabled(true);
                getProductCodeBinding().setDisabled(true);
                getProductDescriptionBinding().setDisabled(true);
                getLineNoBinding().setDisabled(true);
                getSaveALBinding().setDisabled(false);
                getSaveAndCloseALBinding().setDisabled(false);
                getHeaderEditBinding().setDisabled(true);
                getAssemblyCodeBinding().setDisabled(true);
                getMenInvolveBinding().setDisabled(true);
            
                  
            } else if (mode.equals("C")) {
                getUnitCodeBinding().setDisabled(true);
                getUnitDescriptionBinding().setDisabled(true);
                getProductCodeBinding().setDisabled(false);
                getProductDescriptionBinding().setDisabled(true);
                getLineNoBinding().setDisabled(false);
                getSaveALBinding().setDisabled(false);
                getSaveAndCloseALBinding().setDisabled(false);
                getAssemblyCodeBinding().setDisabled(true);
                getMenInvolveBinding().setDisabled(true);
               
            } else if (mode.equals("V")) {
                getSaveALBinding().setDisabled(true);
                getSaveAndCloseALBinding().setDisabled(true);
            }
            
        }
    
    private void cevmodecheck(){
         if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
             makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
             cevModeDisableComponent("V");
         }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
             makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
             cevModeDisableComponent("C");
         }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
             makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
             cevModeDisableComponent("E");
         }
     }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                try {
                    Method method1 =
                        component.getClass().getMethod("setDisabled", boolean.class);
                    if (method1 != null) {
                        method1.invoke(component, valueComponent);
                    }
                } catch (NoSuchMethodException e) {
                    try {
                        Method method =
                            component.getClass().getMethod("setReadOnly", boolean.class);
                        if (method != null) {
                            method.invoke(component, valueComponent);
                        }
                    } catch (Exception e1) {
                        // e.printStackTrace();//silently eat this exception.
                    }


                } catch (Exception e) {
                    // e.printStackTrace();//silently eat this exception.
                }
                List<UIComponent> childComponents = component.getChildren();
                for (UIComponent comp : childComponents) {
                    makeComponentHierarchyReadOnly(comp, valueComponent);
                }
            }

    public void setSaveALBinding(RichButton saveALBinding) {
        this.saveALBinding = saveALBinding;
    }

    public RichButton getSaveALBinding() {
        return saveALBinding;
    }

    public void setSaveAndCloseALBinding(RichButton saveAndCloseALBinding) {
        this.saveAndCloseALBinding = saveAndCloseALBinding;
    }

    public RichButton getSaveAndCloseALBinding() {
        return saveAndCloseALBinding;
    }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }


    public void setAssemblyCodeBinding(RichInputText assemblyCodeBinding) {
        this.assemblyCodeBinding = assemblyCodeBinding;
    }

    public RichInputText getAssemblyCodeBinding() {
        return assemblyCodeBinding;
    }

    public void setMenInvolveBinding(RichInputText menInvolveBinding) {
        this.menInvolveBinding = menInvolveBinding;
    }

    public RichInputText getMenInvolveBinding() {
        return menInvolveBinding;
    }
}
