package terms.prd.setup.ui.bean;

import java.lang.reflect.Method;

import java.sql.Date;
import java.sql.Timestamp;

import java.util.Calendar;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;
import javax.faces.validator.ValidatorException;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;

import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;

import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

import oracle.sql.DATE;


public class EmployeeMasterBean {
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText empNumberBinding;
    private RichOutputText bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichInputDate joinDateBinding;
    private RichInputDate dobBinding;
    private RichInputDate doMarriageBinding;
    private RichSelectOneChoice martialStatusBinding;
    private String pageMode="C";

    public EmployeeMasterBean() {
    }

    public void saveAL(ActionEvent actionEvent) {
        System.out.println("doMarriageBinding: "+doMarriageBinding.getValue());
                ADFUtils.setLastUpdatedBy("EmployeeMasterVO1Iterator","LastUpdatedBy");
        if(doMarriageBinding.getValue() != null || martialStatusBinding.getValue().equals("S")){
            
        OperationBinding op = ADFUtils.findOperation("getEmpNo");

        Object rst = op.execute();
        System.out.println("--------Commit-------");
        System.out.println("value aftr getting result--->?" + rst);
        if (rst.toString() != null && rst.toString() != "" && pageMode.equalsIgnoreCase("C")) {
            if (op.getErrors().isEmpty()) {
                ADFUtils.findOperation("Commit").execute();
                FacesMessage Message =
                    new FacesMessage("Record Saved Successfully. New Employee Number. is " +empNumberBinding.getValue() + ".");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
                pageMode="E";
            }
        }

      //  if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ") {
     else if ((rst.toString() == null || rst.toString() == "" || rst.toString() == " " || rst.toString().equals("M"))  && !pageMode.equalsIgnoreCase("C")) {
            if (op.getErrors().isEmpty()) {
                ADFUtils.findOperation("Commit").execute();
                FacesMessage Message = new FacesMessage("Record Update Successfully.");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
                pageMode="E";

            }
        }
        }else{
            ADFUtils.showMessage("Please enter Marriage Date.", 0);
            }
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setEmpNumberBinding(RichInputText empNumberBinding) {
        this.empNumberBinding = empNumberBinding;
    }

    public RichInputText getEmpNumberBinding() {
        return empNumberBinding;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }

    //Set Fields and Button disable.
    public void cevModeDisableComponent(String mode) {
        //            FacesContext fctx = FacesContext.getCurrentInstance();
        //            ELContext elctx = fctx.getELContext();
        //            Application jsfApp = fctx.getApplication();
        //            //create a ValueExpression that points to the ADF binding layer
        //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
        //            //
        //            ValueExpression valueExpr = exprFactory.createValueExpression(
        //                                         elctx,
        //                                         "#{pageFlowScope.mode=='E'}",
        //                                          Object.class
        //                                         );
        //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
        if (mode.equals("E")) {
//            if(unitCodeBinding.getValue().equals("10001") || unitCodeBinding.getValue().equals("10002"))
//            {
//            getEmpNumberBinding().setDisabled(false);
//            }else
//            {
//                getEmpNumberBinding().setDisabled(true);
//            }
            getUnitCodeBinding().setDisabled(true);  
            getHeaderEditBinding().setDisabled(true);
            getEmpNumberBinding().setDisabled(true);
            getJoinDateBinding().setDisabled(true);
            pageMode="E";

        } else if (mode.equals("C")) {
            
            pageMode="C";
            OperationBinding opr =ADFUtils.findOperation("checkEmpNoGenerationMode");
            opr.execute();
            System.out.println("Check Emp No Generation Mode Result value"+opr.getResult());
            if(opr.getResult().equals("A"))
            {
                getEmpNumberBinding().setDisabled(true);
            }
            else
            {
                getEmpNumberBinding().setDisabled(false);
            }
            
            
//            if(unitCodeBinding.getValue().equals("10001") || unitCodeBinding.getValue().equals("10002"))
//            {
//                getEmpNumberBinding().setDisabled(false);  
//            }else
//            {
//                getEmpNumberBinding().setDisabled(true);  
//            }
            getHeaderEditBinding().setDisabled(true);
            //getEmpNumberBinding().setDisabled(true);
            getDoMarriageBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);  


        } else if (mode.equals("V")) {
            pageMode="V";

        }

    }


    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setJoinDateBinding(RichInputDate joinDateBinding) {
        this.joinDateBinding = joinDateBinding;
    }

    public RichInputDate getJoinDateBinding() {
        return joinDateBinding;
    }

    public void setDobBinding(RichInputDate dobBinding) {
        this.dobBinding = dobBinding;
    }

    public RichInputDate getDobBinding() {
        return dobBinding;
    }


    public void joinDtValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (object != null) {

            Timestamp jDt = (Timestamp) object;
            System.out.println("Joining Date is" + jDt);
            Timestamp dbDt = (Timestamp) dobBinding.getValue();
            System.out.println("Date of birth is" + dbDt);
            if (dbDt !=null && jDt.before(dbDt)) {
                System.out.println("in the if block****");
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                              "Join Date must be greater than Date Of Birth.", null));

            } else {

                System.out.println("in the else block****");
            }

        }

    }

    public void dobValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (object != null) {

            Timestamp dbdt = (Timestamp) object;
            System.out.println("date of birth is" + dbdt);
             Timestamp currdt;
            currdt = new Timestamp(System.currentTimeMillis());
            System.out.println("Current Date is*****" + currdt);
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(dbdt.getTime());
            cal.add(Calendar.YEAR, 18);
            Timestamp db = new Timestamp(cal.getTime().getTime());
            System.out.println("Date after years" + db);
            if (currdt.before(db)) {
                System.out.println("in the if block****");
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                              "Employee age must be greater than 18", null));

            }


        }

    }

    public void setDoMarriageBinding(RichInputDate doMarriageBinding) {
        this.doMarriageBinding = doMarriageBinding;
    }

    public RichInputDate getDoMarriageBinding() {
        return doMarriageBinding;
    }

    public void mStatusVL(ValueChangeEvent mstat) {
        mstat.getComponent().processUpdates(FacesContext.getCurrentInstance());
            String str=(String)mstat.getNewValue();
                System.out.println("Value of Marriage Status: "+str);
        if(str.equals("S")){
                getDoMarriageBinding().setDisabled(true);
            }
            else{
                getDoMarriageBinding().setDisabled(false);
            }
    }

    public void setMartialStatusBinding(RichSelectOneChoice martialStatusBinding) {
        this.martialStatusBinding = martialStatusBinding;
    }

    public RichSelectOneChoice getMartialStatusBinding() {
        return martialStatusBinding;
    }
}

