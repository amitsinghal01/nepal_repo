package terms.prd.transaction.ui.bean;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;

import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

public class ConsumptionEntryBean {
    private RichTable tableConsumptionEntryBinding;
    
    private String editAction="V";
    private RichInputText entryNoBinding;

    public ConsumptionEntryBean() {
    }

    public void setTableConsumptionEntryBinding(RichTable tableConsumptionEntryBinding) {
        this.tableConsumptionEntryBinding = tableConsumptionEntryBinding;
    }

    public RichTable getTableConsumptionEntryBinding() {
        return tableConsumptionEntryBinding;
    }
    
    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public void createAL(ActionEvent actionEvent) {
//        ADFUtils.findOperation("modeSetVal").execute();
        ADFUtils.findOperation("CreateInsert").execute();
//        ADFUtils.findOperation("modeSetVal").execute();
        ADFUtils.findOperation("getEntryNoConsEntry").execute();
    }

    public void setEntryNoBinding(RichInputText entryNoBinding) {
        this.entryNoBinding = entryNoBinding;
    }

    public RichInputText getEntryNoBinding() {
        return entryNoBinding;
    }

    public void deleteAL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
            {
            OperationBinding op = null;
            ADFUtils.findOperation("Delete1").execute();
            op = (OperationBinding) ADFUtils.findOperation("Commit");
            if (getEditAction().equals("V")) {
                Object rst = op.execute();
            }
            System.out.println("Record Delete Successfully");
                if(op.getErrors().isEmpty()){
                    FacesMessage Message = new FacesMessage("Record Deleted Successfully");   
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                    FacesContext fc = FacesContext.getCurrentInstance();   
                    fc.addMessage(null, Message);
                } else if (!op.getErrors().isEmpty())
                {
                   OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
                   Object rstr = opr.execute();
                   FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                   Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                   FacesContext fc = FacesContext.getCurrentInstance();
                   fc.addMessage(null, Message);
                 }
            }

        AdfFacesContext.getCurrentInstance().addPartialTarget(tableConsumptionEntryBinding);
    }

    public void prodIssueQtyVL(ValueChangeEvent vce) {
        System.out.println("INSIDE VCE PROD CODE #### " );
        if(vce != null){
                vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
                ADFUtils.findOperation("getConsumptionEntryConsumptionQtyCalculate").execute();
            }
    }
}
