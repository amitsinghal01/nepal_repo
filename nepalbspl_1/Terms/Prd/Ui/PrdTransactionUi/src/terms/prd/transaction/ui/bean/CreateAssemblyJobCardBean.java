package terms.prd.transaction.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.sql.Timestamp;

import java.util.Calendar;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;
import javax.faces.validator.ValidatorException;

import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectBooleanCheckbox;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichButton;

import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

import oracle.jbo.JboException;
import oracle.jbo.Row;
import oracle.jbo.ValidationException;

public class CreateAssemblyJobCardBean {
    private RichButton headerEditBinding;
    private RichOutputText outputTextBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText unitNameBinding;
    private RichSelectOneChoice jobCardTypeBinding;
    private RichInputText yearBinding;
    private RichSelectOneChoice monthBinding;
    private RichInputText jobCardNoBinding;
    private RichInputDate jobCardDateBinding;
    private RichInputComboboxListOfValues productCodeBinding;
    private RichInputText productDescriptionBinding;
    private RichInputText jobCardQtyBinding;
    private RichInputText producedQtyBinding;
    private RichInputText balanceQtyBinding;
    private RichInputText alternatePartBinding;
    private RichInputText alternateDescriptionBinding;
    private RichInputText requiredQuantityBinding;
    private RichInputText itemCodeBinding;
    private RichInputText revisionNoBinding;
    private RichInputText itemDescriptionBinding;
    private RichInputText uomBinding;
    private RichInputText processSeqBinding;
    private RichInputText processDetailBinding;
    private RichInputText requiredQuantityReqPartsBinding;
    private RichButton populateBinding;
    private RichButton detailCreateBinding;
    private RichButton detailDeleteBinding;
    private RichButton detailCreateAlternatePartsBinding;
    private RichButton detailDeleteAlternatePartsBinding;
    private RichButton detailCreateRequiredPartsBinding;
    private RichButton detailDeleteRequiredPartsBinding;
    private RichColumn revisionNumberBinding;
    private RichInputText revNumberBinding;
    private RichInputText alternatepartBinding;
    private RichInputText alternateDescBinding;
    private RichSelectBooleanCheckbox closeBinding;
    private RichColumn closeFieldBinding;
    private RichSelectBooleanCheckbox closeBind;
    private RichInputText jobCardQuantityBinding;
    private RichInputText producedQuantityBinding;
    private RichInputText balanceQuantityBinding;
    private RichInputText issuedQtyReqPartsBinding;
    private RichInputText returnedQtyReqPartsBinding;
    private RichInputText consumQtyReqPartsBinding;
    private RichColumn itemDescriptionReqPartsBinding;
    private RichButton headerSaveBinding;
    private RichTable assemblyJobCardTableDetailBinding;
    private String pop_flag = "N";
    private RichShowDetailItem requiredPartsBinding;
    private RichShowDetailItem alternatePartsBinding;

    public CreateAssemblyJobCardBean() {
    }

    public void saveGenerateJobCardNo(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedBy("AssemblyJobCardJcHeaderVO1Iterator", "LastUpdatedBy");
        OperationBinding op = ADFUtils.findOperation("generateAssemblyJobCardNo");
        Object rst = op.execute();

        if (rst != null && !(rst.toString().equalsIgnoreCase("N"))) {

            System.out.println("Balance Qty" + balanceQuantityBinding.getValue() + " Produced Binding" +
                               producedQuantityBinding.getValue());
            //                BigDecimal bal = new BigDecimal(0);
            //                bal = (BigDecimal)balanceQuantityBinding.getValue();


            Row rr = (Row) ADFUtils.evaluateEL("#{bindings.AssemblyJobCardJcHeadVO1Iterator.currentRow}");

            BigDecimal JobQty = (BigDecimal) rr.getAttribute("JobcardQty");
            BigDecimal BalQty = (BigDecimal) rr.getAttribute("BalanceQty");
            System.out.println("Job Card Qty ########### " + JobQty);
            System.out.println("Balance Qty ########### " + BalQty);


            System.out.println("rr.getAttribute(\"JobcardQty\")" + rr.getAttribute("JobcardQty") +
                               "rr.getAttribute(\"BalanceQty\")" + rr.getAttribute("BalanceQty"));


            if (JobQty.compareTo(new BigDecimal(0)) == 1 || BalQty.compareTo(new BigDecimal(0)) == 1) {
                System.out.println("***********Inside***********");

                if (!getJobCardTypeBinding().isDisabled()) {
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message =
                        new FacesMessage("Record Saved Successfully.New Assembly Job Card No. is " + rst + ".");
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);


                }
                //                if(getJobCardQtyBinding().getValue() != null) {
                //                    ADFUtils.showMessage("Before populate can't save this Record.", 0);
                //                }
                else {
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message = new FacesMessage("Record Updated Successfully.");
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);
                }
            } else {
                ADFUtils.showMessage("This Job Card Has Been Completed.", 0);
            }
        }
    }


    public void populateDataJcDetailAssemblyJobCard(ActionEvent actionEvent) {
        OperationBinding op = ADFUtils.findOperation("populateMultiPopDetAssemblyJobCard");
        //        OperationBinding op1=ADFUtils.findOperation("setAssemblyJobCardNo");
        op.execute();
        pop_flag = "Y";
        //        op1.execute();
        OperationBinding op1 = ADFUtils.findOperation("populateAlternateAssemblyJobCard");
        op1.execute();

    }

    public void editButtonAL(ActionEvent actionEvent) {
        //        System.out.println("closeBinding Before Value ===> " + getCloseBinding().getValue());
        System.out.println("closeBind Before Value ===> " + getCloseBinding().getValue());
        System.out.println("getJobCardQtyBinding Before Value #### " + getJobCardQuantityBinding().getValue());
        BigDecimal jobCardQty = new BigDecimal(0);
        BigDecimal producedQty = new BigDecimal(0);
        BigDecimal balanceQty = new BigDecimal(0);
        if (balanceQuantityBinding.getValue() != null) {
            balanceQty = (BigDecimal) balanceQuantityBinding.getValue();
        }

        System.out.println("***********CONDITION********" + balanceQty.equals("0"));
        System.out.println("************888balanceQty*************" + balanceQty);
        if (balanceQty.compareTo(new BigDecimal(0)) == 0) {
            System.out.println("************Balance Qty Binding Before Value #### " +
                               getBalanceQuantityBinding().getValue());
            //                   System.out.println("closeBind After Value ===> " + getCloseBinding().getValue());
            ADFUtils.setEL("#{pageFlowScope.mode}", "V");
            ADFUtils.showMessage("Close Job Card cannot be modified.", 0);
            System.out.println("end is*****************************8");

        }
        //              || producedQuantityBinding.getValue().equals(jobCardQuantityBinding.getValue()))

        //        System.out.println("Close Field After Value ===> " + getCloseBinding());

        if (closeBinding.getValue().equals(true))
        //              || producedQuantityBinding.getValue().equals(jobCardQuantityBinding.getValue()))

        //        System.out.println("Close Field After Value ===> " + getCloseBinding());
        {
            System.out.println("getJobCardQtyBinding After Value #### " + getJobCardQuantityBinding().getValue());
            System.out.println("closeBind After Value ===> " + getCloseBinding().getValue());
            ADFUtils.showMessage("Close Job Card cannot be modified.", 0);

        } else {

            cevmodecheck();

        }

    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setOutputTextBinding(RichOutputText outputTextBinding) {
        this.outputTextBinding = outputTextBinding;
    }

    public RichOutputText getOutputTextBinding() {
        cevmodecheck();
        return outputTextBinding;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }


    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }

    public void cevModeDisableComponent(String mode) {
        if (mode.equals("E")) {
            getUnitCodeBinding().setDisabled(true);
            getUnitNameBinding().setDisabled(true);
            getJobCardTypeBinding().setDisabled(true);
            getYearBinding().setDisabled(true);
            getMonthBinding().setDisabled(true);
            getJobCardNoBinding().setDisabled(true);
            getJobCardDateBinding().setDisabled(true);
            getProductCodeBinding().setDisabled(true);
            //                  getJobCardQtyBinding().setDisabled(true);
            //                  getJobCardTotalTransBinding().setDisabled(true);
            getProducedQtyBinding().setDisabled(true);
            //                  getProducedQtyTotalTransBinding().setDisabled(true);
            getBalanceQtyBinding().setDisabled(true);
            getBalanceQuantityBinding().setDisabled(true);
            //                  getViewDrgBinding().setDisabled(true);
            //                  getPopulateBinding().setDisabled(true);
            getItemCodeBinding().setDisabled(true);
            getRevNumberBinding().setDisabled(true);
            getUomBinding().setDisabled(true);
            getProcessSeqBinding().setDisabled(true);
            getProcessDetailBinding().setDisabled(true);
            getRequiredQuantityReqPartsBinding().setDisabled(false);
            getIssuedQtyReqPartsBinding().setDisabled(true);
            getReturnedQtyReqPartsBinding().setDisabled(true);
            getConsumQtyReqPartsBinding().setDisabled(true);
            getAlternatepartBinding().setDisabled(true);
            getAlternateDescBinding().setDisabled(true);
            getRequiredQuantityBinding().setDisabled(false);
            getHeaderEditBinding().setDisabled(true);
            pop_flag = "Y";
            //                  getIssuedQtyAltAsyBinding().setDisabled(true);
            //                  getConsumedQtyAltAsyBinding().setDisabled(true);

        } else if (mode.equals("C")) {
            getUnitCodeBinding().setDisabled(true);
            getUnitNameBinding().setDisabled(true);
            getJobCardNoBinding().setDisabled(true);
            getJobCardDateBinding().setDisabled(true);
            //                  getJobCardTotalTransBinding().setDisabled(true);
            getProducedQtyBinding().setDisabled(true);
            //                  getProducedQtyTotalTransBinding().setDisabled(true);
            getBalanceQtyBinding().setDisabled(true);
            getBalanceQuantityBinding().setDisabled(true);
            getItemCodeBinding().setDisabled(true);
            getRevNumberBinding().setDisabled(true);
            getUomBinding().setDisabled(true);
            getProcessSeqBinding().setDisabled(true);
            getProcessDetailBinding().setDisabled(true);
            getRequiredQuantityReqPartsBinding().setDisabled(false);
            getIssuedQtyReqPartsBinding().setDisabled(true);
            getReturnedQtyReqPartsBinding().setDisabled(true);
            getConsumQtyReqPartsBinding().setDisabled(true);
            //                  getRevisionNoBinding().setDisabled(true);
            //                  getUomBinding().setDisabled(true);
            //                  getProcessSeqBinding().setDisabled(true);
            //                  getProcessDetailBinding().setDisabled(true);
            //                  getRequiredQuantityReqPartsBinding().setDisabled(true);
            getAlternatepartBinding().setDisabled(true);
            getRequiredQuantityBinding().setDisabled(false);
            //                  getAlternateDescriptionBinding().setDisabled(true);
            //                  getRequiredQuantityBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);


        } else if (mode.equals("V")) {
            getDetailCreateBinding().setDisabled(true);
            getDetailDeleteBinding().setDisabled(true);
            getDetailCreateRequiredPartsBinding().setDisabled(true);
            getDetailDeleteRequiredPartsBinding().setDisabled(true);
            getDetailCreateAlternatePartsBinding().setDisabled(true);
            getDetailDeleteAlternatePartsBinding().setDisabled(true);

            getRequiredPartsBinding().setDisabled(false);
            getAlternatePartsBinding().setDisabled(false);
        }
    }


    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setUnitNameBinding(RichInputText unitNameBinding) {
        this.unitNameBinding = unitNameBinding;
    }

    public RichInputText getUnitNameBinding() {
        return unitNameBinding;
    }

    public void setJobCardTypeBinding(RichSelectOneChoice jobCardTypeBinding) {
        this.jobCardTypeBinding = jobCardTypeBinding;
    }

    public RichSelectOneChoice getJobCardTypeBinding() {
        return jobCardTypeBinding;
    }

    public void setYearBinding(RichInputText yearBinding) {
        this.yearBinding = yearBinding;
    }

    public RichInputText getYearBinding() {
        return yearBinding;
    }

    public void setMonthBinding(RichSelectOneChoice monthBinding) {
        this.monthBinding = monthBinding;
    }

    public RichSelectOneChoice getMonthBinding() {
        return monthBinding;
    }

    public void setJobCardNoBinding(RichInputText jobCardNoBinding) {
        this.jobCardNoBinding = jobCardNoBinding;
    }

    public RichInputText getJobCardNoBinding() {
        return jobCardNoBinding;
    }

    public void setJobCardDateBinding(RichInputDate jobCardDateBinding) {
        this.jobCardDateBinding = jobCardDateBinding;
    }

    public RichInputDate getJobCardDateBinding() {
        return jobCardDateBinding;
    }

    public void setProductCodeBinding(RichInputComboboxListOfValues productCodeBinding) {
        this.productCodeBinding = productCodeBinding;
    }

    public RichInputComboboxListOfValues getProductCodeBinding() {
        return productCodeBinding;
    }

    public void setProductDescriptionBinding(RichInputText productDescriptionBinding) {
        this.productDescriptionBinding = productDescriptionBinding;
    }

    public RichInputText getProductDescriptionBinding() {
        return productDescriptionBinding;
    }

    public void setJobCardQtyBinding(RichInputText jobCardQtyBinding) {
        this.jobCardQtyBinding = jobCardQtyBinding;
    }

    public RichInputText getJobCardQtyBinding() {
        return jobCardQtyBinding;
    }

    public void setProducedQtyBinding(RichInputText producedQtyBinding) {
        this.producedQtyBinding = producedQtyBinding;
    }

    public RichInputText getProducedQtyBinding() {
        return producedQtyBinding;
    }

    public void setBalanceQtyBinding(RichInputText balanceQtyBinding) {
        this.balanceQtyBinding = balanceQtyBinding;
    }

    public RichInputText getBalanceQtyBinding() {
        return balanceQtyBinding;
    }

    public void setAlternatePartBinding(RichInputText alternatePartBinding) {
        this.alternatePartBinding = alternatePartBinding;
    }

    public RichInputText getAlternatePartBinding() {
        return alternatePartBinding;
    }

    public void setAlternateDescriptionBinding(RichInputText alternateDescriptionBinding) {
        this.alternateDescriptionBinding = alternateDescriptionBinding;
    }

    public RichInputText getAlternateDescriptionBinding() {
        return alternateDescriptionBinding;
    }

    public void setRequiredQuantityBinding(RichInputText requiredQuantityBinding) {
        this.requiredQuantityBinding = requiredQuantityBinding;
    }

    public RichInputText getRequiredQuantityBinding() {
        return requiredQuantityBinding;
    }

    public void setItemCodeBinding(RichInputText itemCodeBinding) {
        this.itemCodeBinding = itemCodeBinding;
    }

    public RichInputText getItemCodeBinding() {
        return itemCodeBinding;
    }

    public void setRevisionNoBinding(RichInputText revisionNoBinding) {
        this.revisionNoBinding = revisionNoBinding;
    }

    public RichInputText getRevisionNoBinding() {
        return revisionNoBinding;
    }

    public void setItemDescriptionBinding(RichInputText itemDescriptionBinding) {
        this.itemDescriptionBinding = itemDescriptionBinding;
    }

    public RichInputText getItemDescriptionBinding() {
        return itemDescriptionBinding;
    }

    public void setUomBinding(RichInputText uomBinding) {
        this.uomBinding = uomBinding;
    }

    public RichInputText getUomBinding() {
        return uomBinding;
    }

    public void setProcessSeqBinding(RichInputText processSeqBinding) {
        this.processSeqBinding = processSeqBinding;
    }

    public RichInputText getProcessSeqBinding() {
        return processSeqBinding;
    }

    public void setProcessDetailBinding(RichInputText processDetailBinding) {
        this.processDetailBinding = processDetailBinding;
    }

    public RichInputText getProcessDetailBinding() {
        return processDetailBinding;
    }

    public void setRequiredQuantityReqPartsBinding(RichInputText requiredQuantityReqPartsBinding) {
        this.requiredQuantityReqPartsBinding = requiredQuantityReqPartsBinding;
    }

    public RichInputText getRequiredQuantityReqPartsBinding() {
        return requiredQuantityReqPartsBinding;
    }

    public void setPopulateBinding(RichButton populateBinding) {
        this.populateBinding = populateBinding;
    }

    public RichButton getPopulateBinding() {
        return populateBinding;
    }

    public void setDetailCreateBinding(RichButton detailCreateBinding) {
        this.detailCreateBinding = detailCreateBinding;
    }

    public RichButton getDetailCreateBinding() {
        return detailCreateBinding;
    }

    public void setDetailDeleteBinding(RichButton detailDeleteBinding) {
        this.detailDeleteBinding = detailDeleteBinding;
    }

    public RichButton getDetailDeleteBinding() {
        return detailDeleteBinding;
    }

    public void setDetailCreateAlternatePartsBinding(RichButton detailCreateAlternatePartsBinding) {
        this.detailCreateAlternatePartsBinding = detailCreateAlternatePartsBinding;
    }

    public RichButton getDetailCreateAlternatePartsBinding() {
        return detailCreateAlternatePartsBinding;
    }

    public void setDetailDeleteAlternatePartsBinding(RichButton detailDeleteAlternatePartsBinding) {
        this.detailDeleteAlternatePartsBinding = detailDeleteAlternatePartsBinding;
    }

    public RichButton getDetailDeleteAlternatePartsBinding() {
        return detailDeleteAlternatePartsBinding;
    }

    public void setDetailCreateRequiredPartsBinding(RichButton detailCreateRequiredPartsBinding) {
        this.detailCreateRequiredPartsBinding = detailCreateRequiredPartsBinding;
    }

    public RichButton getDetailCreateRequiredPartsBinding() {
        return detailCreateRequiredPartsBinding;
    }

    public void setDetailDeleteRequiredPartsBinding(RichButton detailDeleteRequiredPartsBinding) {
        this.detailDeleteRequiredPartsBinding = detailDeleteRequiredPartsBinding;
    }

    public RichButton getDetailDeleteRequiredPartsBinding() {
        return detailDeleteRequiredPartsBinding;
    }

    public void setRevisionNumberBinding(RichColumn revisionNumberBinding) {
        this.revisionNumberBinding = revisionNumberBinding;
    }

    public RichColumn getRevisionNumberBinding() {
        return revisionNumberBinding;
    }

    public void setRevNumberBinding(RichInputText revNumberBinding) {
        this.revNumberBinding = revNumberBinding;
    }

    public RichInputText getRevNumberBinding() {
        return revNumberBinding;
    }

    public void setAlternatepartBinding(RichInputText alternatepartBinding) {
        this.alternatepartBinding = alternatepartBinding;
    }

    public RichInputText getAlternatepartBinding() {
        return alternatepartBinding;
    }

    public void setAlternateDescBinding(RichInputText alternateDescBinding) {
        this.alternateDescBinding = alternateDescBinding;
    }

    public RichInputText getAlternateDescBinding() {
        return alternateDescBinding;
    }


    public void monthValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (object != null && yearBinding.getValue() != null) {
            Timestamp Date = (Timestamp) jobCardDateBinding.getValue();
            Calendar cal = Calendar.getInstance();
            cal.setTime(Date);
            int month = cal.get(Calendar.MONTH) + 1;
            int year = cal.get(Calendar.YEAR);
            int MonthCase = 0;

            if (object.toString().equals("JAN")) {
                MonthCase = 1;
            } else if (object.toString().equals("FEB")) {
                MonthCase = 2;
            } else if (object.toString().equals("MAR")) {
                MonthCase = 3;
            } else if (object.toString().equals("APR")) {
                MonthCase = 4;
            } else if (object.toString().equals("MAY")) {
                MonthCase = 5;
            } else if (object.toString().equals("JUN")) {
                MonthCase = 6;
            } else if (object.toString().equals("JUL")) {
                MonthCase = 7;
            } else if (object.toString().equals("AUG")) {
                MonthCase = 8;
            } else if (object.toString().equals("SEP")) {
                MonthCase = 9;
            } else if (object.toString().equals("OCT")) {
                MonthCase = 10;
            } else if (object.toString().equals("NOV")) {
                MonthCase = 11;
            } else if (object.toString().equals("DEC")) {
                MonthCase = 12;
            } else {
                System.out.println("No Month Found");
            }
            System.out.println("Month Case is=>" + MonthCase);
            if (MonthCase < month && (Integer) yearBinding.getValue() > year) {
                //throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Month should be greater than or equals to Job Card Date Month", null));


            } else if (MonthCase < month && (Integer) yearBinding.getValue() <= year) {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                              "Month should be greater than or equals to Job Card Date Month",
                                                              null));
            }
        }

    }

    public void yearValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (object != null) {
            Timestamp Date = (Timestamp) jobCardDateBinding.getValue();
            Calendar cal = Calendar.getInstance();
            cal.setTime(Date);
            int year = cal.get(Calendar.YEAR);
            if ((Integer) object < year) {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                              "Year should be greater than or equals to Job Card Date Year",
                                                              null));
            }
        }

    }

    public void setCloseBinding(RichSelectBooleanCheckbox closeBinding) {
        this.closeBinding = closeBinding;
    }

    public RichSelectBooleanCheckbox getCloseBinding() {
        return closeBinding;
    }

    public void setCloseFieldBinding(RichColumn closeFieldBinding) {
        this.closeFieldBinding = closeFieldBinding;
    }

    public RichColumn getCloseFieldBinding() {
        return closeFieldBinding;
    }

    public void setCloseBind(RichSelectBooleanCheckbox closeBind) {
        this.closeBind = closeBind;
    }

    public RichSelectBooleanCheckbox getCloseBind() {
        return closeBind;
    }

    public void setJobCardQuantityBinding(RichInputText jobCardQuantityBinding) {
        this.jobCardQuantityBinding = jobCardQuantityBinding;
    }

    public RichInputText getJobCardQuantityBinding() {
        return jobCardQuantityBinding;
    }

    public void setProducedQuantityBinding(RichInputText producedQuantityBinding) {
        this.producedQuantityBinding = producedQuantityBinding;
    }

    public RichInputText getProducedQuantityBinding() {
        return producedQuantityBinding;
    }

    public void setBalanceQuantityBinding(RichInputText balanceQuantityBinding) {
        this.balanceQuantityBinding = balanceQuantityBinding;
    }

    public RichInputText getBalanceQuantityBinding() {
        return balanceQuantityBinding;
    }


    public String saveAndCloseGenerateJobCardNo() {

        OperationBinding op = ADFUtils.findOperation("generateAssemblyJobCardNo");
        Object rst = op.execute();
        
        if (rst != null && !(rst.toString().equalsIgnoreCase("N"))) {

            System.out.println("Balance Qty" + balanceQuantityBinding.getValue() + " Produced Binding" +
                               producedQuantityBinding.getValue());
            //                BigDecimal bal = new BigDecimal(0);
            //                bal = (BigDecimal)balanceQuantityBinding.getValue();
    
    
            Row rr = (Row) ADFUtils.evaluateEL("#{bindings.AssemblyJobCardJcHeadVO1Iterator.currentRow}");
    
            BigDecimal JobQty = (BigDecimal) rr.getAttribute("JobcardQty");
            BigDecimal BalQty = (BigDecimal) rr.getAttribute("BalanceQty");
            System.out.println("Job Card Qty ########### " + JobQty);
            System.out.println("Balance Qty ########### " + BalQty);
    
            ADFUtils.setLastUpdatedBy("AssemblyJobCardJcHeaderVO1Iterator", "LastUpdatedBy");
            System.out.println("rr.getAttribute(\"JobcardQty\")" + rr.getAttribute("JobcardQty") +
                               "rr.getAttribute(\"BalanceQty\")" + rr.getAttribute("BalanceQty"));
            if (JobQty.compareTo(new BigDecimal(0)) == 1 || BalQty.compareTo(new BigDecimal(0)) == 1) {
                System.out.println("***********Inside***********");
    
                if (!getJobCardTypeBinding().isDisabled()) {
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message =
                        new FacesMessage("Record Saved Successfully.New Assembly Job Card No. is " + rst + ".");
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);
                    return "saveandclose";
                } else {
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message = new FacesMessage("Record Updated Successfully.");
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);
                    return "saveandclose";
                }
            } else {
                ADFUtils.showMessage("This Job Card Has Been Completed.", 0);
            }
        }
        return null;
    }


    //    public void jobCardQtyValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
    //        BigDecimal jobCardQty = (BigDecimal)jobCardQuantityBinding.getValue();
    //        BigDecimal jobCardQty = (BigDecimal)jobCardQuantityBinding.getValue();
    //    }
    public void jobCardQtyValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        BigDecimal obj = (BigDecimal) object;
        //        BigDecimal producedQty = new BigDecimal(0);
        //        BigDecimal jobCardQty = (BigDecimal)jobCardQuantityBinding.getValue();

        BigDecimal producedQty = new BigDecimal(0);
        BigDecimal balQty = new BigDecimal(0);
        if (producedQuantityBinding.getValue() != null) {
            producedQty = (BigDecimal) producedQuantityBinding.getValue();
        }
        if (obj.compareTo(producedQty) == -1) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                          "Job Card Quantity should be greater than or equal to Produced Quantity.",
                                                          null));
        }
        //        if(obj.compareTo(balQty)==1) {
        //            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Before populate can't save this Record .", null));
        //        }
    }

    public void setIssuedQtyReqPartsBinding(RichInputText issuedQtyReqPartsBinding) {
        this.issuedQtyReqPartsBinding = issuedQtyReqPartsBinding;
    }

    public RichInputText getIssuedQtyReqPartsBinding() {
        return issuedQtyReqPartsBinding;
    }

    public void setReturnedQtyReqPartsBinding(RichInputText returnedQtyReqPartsBinding) {
        this.returnedQtyReqPartsBinding = returnedQtyReqPartsBinding;
    }

    public RichInputText getReturnedQtyReqPartsBinding() {
        return returnedQtyReqPartsBinding;
    }

    public void setConsumQtyReqPartsBinding(RichInputText consumQtyReqPartsBinding) {
        this.consumQtyReqPartsBinding = consumQtyReqPartsBinding;
    }

    public RichInputText getConsumQtyReqPartsBinding() {
        return consumQtyReqPartsBinding;
    }

    public void setItemDescriptionReqPartsBinding(RichColumn itemDescriptionReqPartsBinding) {
        this.itemDescriptionReqPartsBinding = itemDescriptionReqPartsBinding;
    }

    public RichColumn getItemDescriptionReqPartsBinding() {
        return itemDescriptionReqPartsBinding;
    }

    public void setHeaderSaveBinding(RichButton headerSaveBinding) {
        this.headerSaveBinding = headerSaveBinding;
    }

    public RichButton getHeaderSaveBinding() {
        return headerSaveBinding;
    }

    public void jobCardQtyRefreshValueVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        System.out.println("Inside Vce #####" + vce.getNewValue());

        if (vce.getNewValue() != null && pop_flag.equalsIgnoreCase("Y")) {

            OperationBinding op = ADFUtils.findOperation("populateMultiPopDetAssemblyJobCard");
            op.execute();
            //                   vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
            //                   if(vce.getNewValue() != null)
            //                   {
            //                       System.out.println("Inside If Condition ####");
            ////                       throw new JboException("Chcking Populate $$$$$$$");
            //                       headerSaveBinding.setDisabled(true);
            //                   }
            //                   else
            //                   {
            //                       System.out.println("Inside Else Condition ####");
            //                       populateBinding.setDisabled(false);
            //                       headerSaveBinding.setDisabled(false);
            //                   }

            //           Object oldValue = jobCardQtyBinding.getValue();
            //           Object newValue = vce;
            //           if(getJobCardQtyBinding().getValue() != null && newValue != null)
            //           {
            //              throw new ValidationException("Before populate can't save this Record");
            //           }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(assemblyJobCardTableDetailBinding);
    }

    public void setAssemblyJobCardTableDetailBinding(RichTable assemblyJobCardTableDetailBinding) {
        this.assemblyJobCardTableDetailBinding = assemblyJobCardTableDetailBinding;
    }

    public RichTable getAssemblyJobCardTableDetailBinding() {
        return assemblyJobCardTableDetailBinding;
    }

    public void setRequiredPartsBinding(RichShowDetailItem requiredPartsBinding) {
        this.requiredPartsBinding = requiredPartsBinding;
    }

    public RichShowDetailItem getRequiredPartsBinding() {
        return requiredPartsBinding;
    }

    public void setAlternatePartsBinding(RichShowDetailItem alternatePartsBinding) {
        this.alternatePartsBinding = alternatePartsBinding;
    }

    public RichShowDetailItem getAlternatePartsBinding() {
        return alternatePartsBinding;
    }
}

