package terms.prd.transaction.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.Calendar;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.application.ViewHandler;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.jbo.domain.Number;

import javax.faces.event.ValueChangeEvent;
import javax.faces.validator.ValidatorException;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;

public class CreateAssemblyProductionBean {
    private RichOutputText outputTextBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText unitNameBinding;
    private RichInputComboboxListOfValues lineNoBinding;
    private RichInputText locationCodeBinding;
    private RichInputText productCodeBinding;
    private RichInputText productNameBinding;
    private RichSelectOneChoice customerTypeBinding;
    private RichInputText productionSlipNoBinding;
    private RichInputDate dateBinding;
    private RichInputText yearBinding;
    private RichSelectOneChoice monthBinding;
    private RichInputComboboxListOfValues shiftBinding;
    private RichInputDate startTimeBinding;
    private RichInputDate endTimeBinding;
    private RichInputComboboxListOfValues indentNoBinding;
    private RichInputText indentAmdNoBinding;
    private RichInputComboboxListOfValues jobCardNoBinding;
    private RichInputText jobCardQtyBinding;
    private RichInputText jobCardBalBinding;
    private RichInputText planQtyBinding;
    private RichInputText productionQtyBinding;
    private RichInputText rejectedQtyBinding;
    private RichInputText accpetedQtyBinding;
    private RichInputText lotNoBinding;
    private RichInputComboboxListOfValues enteredByBinding;
    private RichInputText enteredByNameBinding;
    private RichInputComboboxListOfValues approvedByBinding;
    private RichInputText approvedByNameBinding;
    private RichInputDate approvalDateandTimeBinding;
    private RichInputText reasonDescriptionBinding;
    private RichInputText operatorNameBinding;
    private RichInputText itemNameBinding;
    private RichInputText uomBinding;
    private RichInputText availableStockBinding;
    private RichInputText shortStockBinding;
    private RichInputText nbtPrvBinding;
    private RichInputText nbtPrvReqBinding;
    private RichInputText nbtPrvStkBinding;
    private RichInputComboboxListOfValues itemCodeBinding;
    private RichInputText requiredQtyBinding;
    private RichInputText lot;
    private RichInputText lotBalanceBinding;
    private RichInputText rejectedQuantityBinding;
    private RichButton detailCreateOperatorBinding;
    private RichButton detailDeleteOperatorBinding;
    private RichButton detailCreatePartBreakupBinding;
    private RichButton detaildeletePartBreakupBinding;
    private RichButton detailCreateLotTableBinding;
    private RichButton detailDeleteLotTableBinding;
    private RichButton detailCreateBreakdownReasonBinding;
    private RichButton detailDeleteBreakdownReasonBinding;
    private RichTable assemblyOperatorTableBinding;
    private RichTable assemblyProdBreakupTableBinding;
    private RichTable assemblyBreakdownTableBinding;
    private RichOutputText getIndentAmdNoBinding;
    private RichOutputText indentAmdtNoBinding;
    private RichPopup populateAssyProdBreakupBinding;
    private RichButton headerSaveBinding;
    private RichButton headerSaveandCloseBinding;
    private RichInputComboboxListOfValues itemStBinding;
    private RichButton operatorButtonBinding;
    private RichButton partBreakupButtonBinding;
    private RichButton breakdownReasonButtonBinding;
    private RichInputText acceptQtyBinding;
    String stockStatus = "F", mtlStatus = "N";
    private RichInputText extraQtyBinding;

    public CreateAssemblyProductionBean() {
    }

    public void saveGenerateProductionSlipNo(ActionEvent actionEvent) {
        mtlStatus = (String) ADFUtils.findOperation("mtlParaStockProduction").execute();
        DCIteratorBinding dci = ADFUtils.findIterator("AssemblyProductionAssyProdnBreakupVO1Iterator");
        RowSetIterator rsi = dci.getViewObject().createRowSetIterator(null);
        while (rsi.hasNext()) {
            System.out.println("IN DIALOG EVENT WHILE");
            Row r = rsi.next();
            
            BigDecimal stock = new BigDecimal(0);
            stock =
                (BigDecimal) r.getAttribute("ShortStock") == null ? new BigDecimal(0) :
                (BigDecimal) r.getAttribute("ShortStock");
            System.out.println("IN DIALOG STOCK AVAILABLE===>" + stock);
            if (stock.compareTo(new BigDecimal(0)) == 1) {
                System.out.println("IN IF OF WHILE");
                stockStatus = "T";               
            }
            
// Added validation if Required Quantity is  Greater Than Available Stock -- Atul Gupta On: 09-09-2020           
            BigDecimal avlStock = (BigDecimal) r.getAttribute("AvailStock");
            BigDecimal reqQty = (BigDecimal) r.getAttribute("ReqQty");
            if(reqQty.compareTo(avlStock) > 0){
                stockStatus = "R";
                break;
            } 
                      
        }
        rsi.closeRowSetIterator();

        System.out.println("Short Stock Validate greater than 0==>" + stockStatus +
                           "==Mtl Para Stock Check key No. 141" + mtlStatus);
        if (stockStatus.equalsIgnoreCase("T") && mtlStatus.equalsIgnoreCase("Y")) {
            ADFUtils.showMessage("Input Stock not available.Please Check!!!!!", 0);
        } 
        else if(stockStatus.equalsIgnoreCase("R")){
            ADFUtils.showMessage("Required Quantity Can Not be Greater Than Available Stock.Please Check!!!!!", 0);
        }
        else {
            if (approvedByBinding.getValue() != null) {
                OperationBinding op = ADFUtils.findOperation("generateAssemblyProductionSlipNo");
                Object rst = op.execute();
                AdfFacesContext.getCurrentInstance().addPartialTarget(populateAssyProdBreakupBinding);
                ADFUtils.setLastUpdatedBy("AssemblyProductionAssyProdnSlipVO1Iterator", "LastUpdatedBy");
                //        refreshPage();
                if (rst.toString() != null && rst.toString() != "") {
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message =
                        new FacesMessage("Record Saved Successfully.New Production Slip No. is " + rst + ".");
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);
                } else {
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message = new FacesMessage("Record Updated Successfully.");
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);
                }
            } else {
                //            FacesMessage message = new FacesMessage(".");
                //            message.setSeverity(FacesMessage.SEVERITY_ERROR);
                //            FacesContext context = FacesContext.getCurrentInstance();
                //            context.addMessage(address2Binding.getClientId(), message);
                ADFUtils.showMessage("Approved By is mandatory.", 0);
            }
        }
    }


    public void setOutputTextBinding(RichOutputText outputTextBinding) {
        this.outputTextBinding = outputTextBinding;
    }

    public RichOutputText getOutputTextBinding() {
        cevmodecheck();
        return outputTextBinding;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }

    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }

    public void cevModeDisableComponent(String mode) {
        if (mode.equals("E")) {
            getUnitCodeBinding().setDisabled(true);
            getUnitNameBinding().setDisabled(true);
            getLineNoBinding().setDisabled(true);
            getProductCodeBinding().setDisabled(true);
            getProductNameBinding().setDisabled(true);
            getProductionSlipNoBinding().setDisabled(true);
            getDateBinding().setDisabled(true);
            getStartTimeBinding().setDisabled(true);
            getEndTimeBinding().setDisabled(true);
            //                getIndentAmdNoBinding().setDisabled(true);
            getJobCardNoBinding().setDisabled(true);
            getJobCardQtyBinding().setDisabled(true);
            getJobCardBalBinding().setDisabled(true);
            getPlanQtyBinding().setDisabled(true);
            getRejectedQuantityBinding().setDisabled(true);
            getLotNoBinding().setDisabled(true);
            getEnteredByNameBinding().setDisabled(true);
            getApprovedByNameBinding().setDisabled(true);
            getApprovalDateandTimeBinding().setDisabled(true);
            //                getOperatorNameBinding().setDisabled(true);
            //                getItemStBinding().setDisabled(true);
            //                getItemCodeBinding().setDisabled(true);
            //                getItemNameBinding().setDisabled(true);
            //                getUomBinding().setDisabled(true);
            //                getRequiredQtyBinding().setDisabled(true);
            //                getAvailableStockBinding().setDisabled(true);
            //                getShortStockBinding().setDisabled(true);
            //                getLotBalanceBinding().setDisabled(true);
            //                getReasonDescriptionBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            //                getOperatorButtonBinding().setDisabled(false);


        } else if (mode.equals("C")) {
            getUnitCodeBinding().setDisabled(true);
            getUnitNameBinding().setDisabled(true);
            getLocationCodeBinding().setDisabled(true);
            getProductCodeBinding().setDisabled(true);
            getProductNameBinding().setDisabled(true);
            getProductionSlipNoBinding().setDisabled(true);
            getDateBinding().setDisabled(true);
            getStartTimeBinding().setDisabled(true);
            getEndTimeBinding().setDisabled(true);
            //                getIndentAmdtNoBinding().setDis
            getJobCardQtyBinding().setDisabled(true);
            getJobCardBalBinding().setDisabled(true);
            getPlanQtyBinding().setDisabled(true);
            getRejectedQuantityBinding().setDisabled(true);
            //                getAccpetedQtyBinding().setDisabled(true);
            getLotNoBinding().setDisabled(true);
            getEnteredByBinding().setDisabled(true);
            getEnteredByNameBinding().setDisabled(true);
            getApprovedByNameBinding().setDisabled(true);
            getApprovalDateandTimeBinding().setDisabled(true);
            itemCodeBinding.setDisabled(true);
            uomBinding.setDisabled(true);
            requiredQtyBinding.setDisabled(true);
            availableStockBinding.setDisabled(true);
            shortStockBinding.setDisabled(true);
            //                getApprovalDateandTimeBinding().setDisabled(true);
            //                getReasonDescriptionBinding().setDisabled(true);
            //                getOperatorNameBinding().setDisabled(true);
            //                getItemNameBinding().setDisabled(true);
            //                getUomBinding().setDisabled(true);
            //                getAvailableStockBinding().setDisabled(true);
            //                getShortStockBinding().setDisabled(true);
            //                getNbtPrvBinding().setDisabled(true);
            //                getNbtPrvReqBinding().setDisabled(true);
            //                getNbtPrvStkBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            //                itemCodeBinding.setDisabled(true);
            //                uomBinding.setDisabled(true);
            //                requiredQtyBinding.setDisabled(true);
            //                availableStockBinding.setDisabled(true);
            //                shortStockBinding.setDisabled(true);

        } else if (mode.equals("V")) {
            getHeaderEditBinding().setDisabled(true);
            getHeaderSaveBinding().setDisabled(true);
            getHeaderSaveandCloseBinding().setDisabled(true);
            getOperatorButtonBinding().setDisabled(false);
            getPartBreakupButtonBinding().setDisabled(false);
            getBreakdownReasonButtonBinding().setDisabled(false);
        }
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setUnitNameBinding(RichInputText unitNameBinding) {
        this.unitNameBinding = unitNameBinding;
    }

    public RichInputText getUnitNameBinding() {
        return unitNameBinding;
    }

    public void setLineNoBinding(RichInputComboboxListOfValues lineNoBinding) {
        this.lineNoBinding = lineNoBinding;
    }

    public RichInputComboboxListOfValues getLineNoBinding() {
        return lineNoBinding;
    }

    public void setLocationCodeBinding(RichInputText locationCodeBinding) {
        this.locationCodeBinding = locationCodeBinding;
    }

    public RichInputText getLocationCodeBinding() {
        return locationCodeBinding;
    }

    public void setProductCodeBinding(RichInputText productCodeBinding) {
        this.productCodeBinding = productCodeBinding;
    }

    public RichInputText getProductCodeBinding() {
        return productCodeBinding;
    }

    public void setProductNameBinding(RichInputText productNameBinding) {
        this.productNameBinding = productNameBinding;
    }

    public RichInputText getProductNameBinding() {
        return productNameBinding;
    }

    public void setCustomerTypeBinding(RichSelectOneChoice customerTypeBinding) {
        this.customerTypeBinding = customerTypeBinding;
    }

    public RichSelectOneChoice getCustomerTypeBinding() {
        return customerTypeBinding;
    }

    public void setProductionSlipNoBinding(RichInputText productionSlipNoBinding) {
        this.productionSlipNoBinding = productionSlipNoBinding;
    }

    public RichInputText getProductionSlipNoBinding() {
        return productionSlipNoBinding;
    }

    public void setDateBinding(RichInputDate dateBinding) {
        this.dateBinding = dateBinding;
    }

    public RichInputDate getDateBinding() {
        return dateBinding;
    }

    public void setYearBinding(RichInputText yearBinding) {
        this.yearBinding = yearBinding;
    }

    public RichInputText getYearBinding() {
        return yearBinding;
    }

    public void setMonthBinding(RichSelectOneChoice monthBinding) {
        this.monthBinding = monthBinding;
    }

    public RichSelectOneChoice getMonthBinding() {
        return monthBinding;
    }

    public void setShiftBinding(RichInputComboboxListOfValues shiftBinding) {
        this.shiftBinding = shiftBinding;
    }

    public RichInputComboboxListOfValues getShiftBinding() {
        return shiftBinding;
    }

    public void setStartTimeBinding(RichInputDate startTimeBinding) {
        this.startTimeBinding = startTimeBinding;
    }

    public RichInputDate getStartTimeBinding() {
        return startTimeBinding;
    }

    public void setEndTimeBinding(RichInputDate endTimeBinding) {
        this.endTimeBinding = endTimeBinding;
    }

    public RichInputDate getEndTimeBinding() {
        return endTimeBinding;
    }

    public void setIndentNoBinding(RichInputComboboxListOfValues indentNoBinding) {
        this.indentNoBinding = indentNoBinding;
    }

    public RichInputComboboxListOfValues getIndentNoBinding() {
        return indentNoBinding;
    }

    public void setIndentAmdNoBinding(RichInputText indentAmdNoBinding) {
        this.indentAmdNoBinding = indentAmdNoBinding;
    }

    public RichInputText getIndentAmdNoBinding() {
        return indentAmdNoBinding;
    }

    public void setJobCardNoBinding(RichInputComboboxListOfValues jobCardNoBinding) {
        this.jobCardNoBinding = jobCardNoBinding;
    }

    public RichInputComboboxListOfValues getJobCardNoBinding() {
        return jobCardNoBinding;
    }

    public void setJobCardQtyBinding(RichInputText jobCardQtyBinding) {
        this.jobCardQtyBinding = jobCardQtyBinding;
    }

    public RichInputText getJobCardQtyBinding() {
        return jobCardQtyBinding;
    }

    public void setJobCardBalBinding(RichInputText jobCardBalBinding) {
        this.jobCardBalBinding = jobCardBalBinding;
    }

    public RichInputText getJobCardBalBinding() {
        return jobCardBalBinding;
    }

    public void setPlanQtyBinding(RichInputText planQtyBinding) {
        this.planQtyBinding = planQtyBinding;
    }

    public RichInputText getPlanQtyBinding() {
        return planQtyBinding;
    }

    public void setProductionQtyBinding(RichInputText productionQtyBinding) {
        this.productionQtyBinding = productionQtyBinding;
    }

    public RichInputText getProductionQtyBinding() {
        return productionQtyBinding;
    }

    public void setRejectedQtyBinding(RichInputText rejectedQtyBinding) {
        this.rejectedQtyBinding = rejectedQtyBinding;
    }

    public RichInputText getRejectedQtyBinding() {
        return rejectedQtyBinding;
    }

    public void setAccpetedQtyBinding(RichInputText accpetedQtyBinding) {
        this.accpetedQtyBinding = accpetedQtyBinding;
    }

    public RichInputText getAccpetedQtyBinding() {
        return accpetedQtyBinding;
    }

    public void setLotNoBinding(RichInputText lotNoBinding) {
        this.lotNoBinding = lotNoBinding;
    }

    public RichInputText getLotNoBinding() {
        return lotNoBinding;
    }

    public void setEnteredByBinding(RichInputComboboxListOfValues enteredByBinding) {
        this.enteredByBinding = enteredByBinding;
    }

    public RichInputComboboxListOfValues getEnteredByBinding() {
        return enteredByBinding;
    }

    public void setEnteredByNameBinding(RichInputText enteredByNameBinding) {
        this.enteredByNameBinding = enteredByNameBinding;
    }

    public RichInputText getEnteredByNameBinding() {
        return enteredByNameBinding;
    }

    public void setApprovedByBinding(RichInputComboboxListOfValues approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputComboboxListOfValues getApprovedByBinding() {
        return approvedByBinding;
    }

    public void setApprovedByNameBinding(RichInputText approvedByNameBinding) {
        this.approvedByNameBinding = approvedByNameBinding;
    }

    public RichInputText getApprovedByNameBinding() {
        return approvedByNameBinding;
    }

    public void setApprovalDateandTimeBinding(RichInputDate approvalDateandTimeBinding) {
        this.approvalDateandTimeBinding = approvalDateandTimeBinding;
    }

    public RichInputDate getApprovalDateandTimeBinding() {
        return approvalDateandTimeBinding;
    }

    public void setReasonDescriptionBinding(RichInputText reasonDescriptionBinding) {
        this.reasonDescriptionBinding = reasonDescriptionBinding;
    }

    public RichInputText getReasonDescriptionBinding() {
        return reasonDescriptionBinding;
    }

    public void setOperatorNameBinding(RichInputText operatorNameBinding) {
        this.operatorNameBinding = operatorNameBinding;
    }

    public RichInputText getOperatorNameBinding() {
        return operatorNameBinding;
    }

    public void setItemNameBinding(RichInputText itemNameBinding) {
        this.itemNameBinding = itemNameBinding;
    }

    public RichInputText getItemNameBinding() {
        return itemNameBinding;
    }

    public void setUomBinding(RichInputText uomBinding) {
        this.uomBinding = uomBinding;
    }

    public RichInputText getUomBinding() {
        return uomBinding;
    }

    public void setAvailableStockBinding(RichInputText availableStockBinding) {
        this.availableStockBinding = availableStockBinding;
    }

    public RichInputText getAvailableStockBinding() {
        return availableStockBinding;
    }

    public void setShortStockBinding(RichInputText shortStockBinding) {
        this.shortStockBinding = shortStockBinding;
    }

    public RichInputText getShortStockBinding() {
        return shortStockBinding;
    }

    public void setNbtPrvBinding(RichInputText nbtPrvBinding) {
        this.nbtPrvBinding = nbtPrvBinding;
    }

    public RichInputText getNbtPrvBinding() {
        return nbtPrvBinding;
    }

    public void setNbtPrvReqBinding(RichInputText nbtPrvReqBinding) {
        this.nbtPrvReqBinding = nbtPrvReqBinding;
    }

    public RichInputText getNbtPrvReqBinding() {
        return nbtPrvReqBinding;
    }

    public void setNbtPrvStkBinding(RichInputText nbtPrvStkBinding) {
        this.nbtPrvStkBinding = nbtPrvStkBinding;
    }

    public RichInputText getNbtPrvStkBinding() {
        return nbtPrvStkBinding;
    }

    public void setItemCodeBinding(RichInputComboboxListOfValues itemCodeBinding) {
        this.itemCodeBinding = itemCodeBinding;
    }

    public RichInputComboboxListOfValues getItemCodeBinding() {
        return itemCodeBinding;
    }

    public void setRequiredQtyBinding(RichInputText requiredQtyBinding) {
        this.requiredQtyBinding = requiredQtyBinding;
    }

    public RichInputText getRequiredQtyBinding() {
        return requiredQtyBinding;
    }

    public void setLot(RichInputText lot) {
        this.lot = lot;
    }

    public RichInputText getLot() {
        return lot;
    }

    public void setLotBalanceBinding(RichInputText lotBalanceBinding) {
        this.lotBalanceBinding = lotBalanceBinding;
    }

    public RichInputText getLotBalanceBinding() {
        return lotBalanceBinding;
    }

    public void setRejectedQuantityBinding(RichInputText rejectedQuantityBinding) {
        this.rejectedQuantityBinding = rejectedQuantityBinding;
    }

    public RichInputText getRejectedQuantityBinding() {
        return rejectedQuantityBinding;
    }

    public void setDetailCreateOperatorBinding(RichButton detailCreateOperatorBinding) {
        this.detailCreateOperatorBinding = detailCreateOperatorBinding;
    }

    public RichButton getDetailCreateOperatorBinding() {
        return detailCreateOperatorBinding;
    }

    public void setDetailDeleteOperatorBinding(RichButton detailDeleteOperatorBinding) {
        this.detailDeleteOperatorBinding = detailDeleteOperatorBinding;
    }

    public RichButton getDetailDeleteOperatorBinding() {
        return detailDeleteOperatorBinding;
    }

    public void setDetailCreatePartBreakupBinding(RichButton detailCreatePartBreakupBinding) {
        this.detailCreatePartBreakupBinding = detailCreatePartBreakupBinding;
    }

    public RichButton getDetailCreatePartBreakupBinding() {
        return detailCreatePartBreakupBinding;
    }

    public void setDetaildeletePartBreakupBinding(RichButton detaildeletePartBreakupBinding) {
        this.detaildeletePartBreakupBinding = detaildeletePartBreakupBinding;
    }

    public RichButton getDetaildeletePartBreakupBinding() {
        return detaildeletePartBreakupBinding;
    }

    public void setDetailCreateLotTableBinding(RichButton detailCreateLotTableBinding) {
        this.detailCreateLotTableBinding = detailCreateLotTableBinding;
    }

    public RichButton getDetailCreateLotTableBinding() {
        return detailCreateLotTableBinding;
    }

    public void setDetailDeleteLotTableBinding(RichButton detailDeleteLotTableBinding) {
        this.detailDeleteLotTableBinding = detailDeleteLotTableBinding;
    }

    public RichButton getDetailDeleteLotTableBinding() {
        return detailDeleteLotTableBinding;
    }

    public void setDetailCreateBreakdownReasonBinding(RichButton detailCreateBreakdownReasonBinding) {
        this.detailCreateBreakdownReasonBinding = detailCreateBreakdownReasonBinding;
    }

    public RichButton getDetailCreateBreakdownReasonBinding() {
        return detailCreateBreakdownReasonBinding;
    }

    public void setDetailDeleteBreakdownReasonBinding(RichButton detailDeleteBreakdownReasonBinding) {
        this.detailDeleteBreakdownReasonBinding = detailDeleteBreakdownReasonBinding;
    }

    public RichButton getDetailDeleteBreakdownReasonBinding() {
        return detailDeleteBreakdownReasonBinding;
    }

    public void assemblyOperatordeletePopupDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(assemblyOperatorTableBinding);
    }

    public void setAssemblyOperatorTableBinding(RichTable assemblyOperatorTableBinding) {
        this.assemblyOperatorTableBinding = assemblyOperatorTableBinding;
    }

    public RichTable getAssemblyOperatorTableBinding() {
        return assemblyOperatorTableBinding;
    }

    public void assemblyProdBreakupdeletePopupDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete1").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(assemblyProdBreakupTableBinding);

    }

    public void setAssemblyProdBreakupTableBinding(RichTable assemblyProdBreakupTableBinding) {
        this.assemblyProdBreakupTableBinding = assemblyProdBreakupTableBinding;
    }

    public RichTable getAssemblyProdBreakupTableBinding() {
        return assemblyProdBreakupTableBinding;
    }

    public void assemblyBreakdowndeletePopupDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete2").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(assemblyBreakdownTableBinding);

    }

    public void setAssemblyBreakdownTableBinding(RichTable assemblyBreakdownTableBinding) {
        this.assemblyBreakdownTableBinding = assemblyBreakdownTableBinding;
    }

    public RichTable getAssemblyBreakdownTableBinding() {
        return assemblyBreakdownTableBinding;
    }

    public void setGetIndentAmdNoBinding(RichOutputText getIndentAmdNoBinding) {
        this.getIndentAmdNoBinding = getIndentAmdNoBinding;
    }

    public RichOutputText getGetIndentAmdNoBinding() {
        return getIndentAmdNoBinding;
    }

    public void setIndentAmdtNoBinding(RichOutputText indentAmdtNoBinding) {
        this.indentAmdtNoBinding = indentAmdtNoBinding;
    }

    public RichOutputText getIndentAmdtNoBinding() {
        return indentAmdtNoBinding;
    }

    public void monthValidator(FacesContext facesContext, UIComponent uIComponent, Object ob) {
        if (ob != null && yearBinding.getValue() != null) {
            Integer MonthCase = 0;
            if (ob.toString().equals("JAN")) {
                MonthCase = 1;
            } else if (ob.toString().equals("FEB")) {
                MonthCase = 2;
            } else if (ob.toString().equals("MAR")) {
                MonthCase = 3;
            } else if (ob.toString().equals("APR")) {
                MonthCase = 4;
            } else if (ob.toString().equals("MAY")) {
                MonthCase = 5;
            } else if (ob.toString().equals("JUN")) {
                MonthCase = 6;
            } else if (ob.toString().equals("JUL")) {
                MonthCase = 7;
            } else if (ob.toString().equals("AUG")) {
                MonthCase = 8;
            } else if (ob.toString().equals("SEP")) {
                MonthCase = 9;
            } else if (ob.toString().equals("OCT")) {
                MonthCase = 10;
            } else if (ob.toString().equals("NOV")) {
                MonthCase = 11;
            } else if (ob.toString().equals("DEC")) {
                MonthCase = 12;
            } else {
                System.out.println("No Month Found");
            }
            Integer j = 1;
            j = j + (Integer) Calendar.getInstance().get(Calendar.MONTH);
            System.out.println("CURRENT MONTH" + j + "GIVEN MONTH" + MonthCase);
            if (MonthCase.compareTo(j) == -1) {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                              "Month cannot be less than Current Month", null));
            }
        }

    }

    public void yearValidator(FacesContext facesContext, UIComponent uIComponent, Object ob) {
        Integer year = (Integer) ob;
        Integer year1 = (Integer) Calendar.getInstance().get(Calendar.YEAR);
        System.out.println("CURRENT YEAR" + year);
        System.out.println("CURRENT YEAR" + year1);
        if (year.compareTo(year1) == -1) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                          "Year cannot be less than Current Year", null));
        }

    }

    public void productionQtyLessThanValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        BigDecimal obj = (BigDecimal) object;
        BigDecimal jobCardBalQty = (BigDecimal) getJobCardBalBinding().getValue();
        System.out.println("PRODN Qty" + obj + " Batch" + jobCardBalQty);
        if (obj.compareTo(jobCardBalQty) == 1) {
            FacesMessage Message =
                new FacesMessage("Production Qty. should be less than or equal to Job Card Balance.");
            Message.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
            throw new ValidatorException(Message);
        }

    }

    public void populateDataAssyProdBreakupAssemblyProduction(ActionEvent actionEvent) {
        OperationBinding op = ADFUtils.findOperation("populateAssemblyProductionBreakupData");
        op.execute();
        RichPopup.PopupHints popup = new RichPopup.PopupHints();
        getPopulateAssyProdBreakupBinding().show(popup);
        getProductionQtyBinding().setDisabled(true);
        getAcceptQtyBinding().setDisabled(true);
        //        AdfFacesContext.getCurrentInstance().addPartialTarget(assemblyProdBreakupTableBinding);
        AdfFacesContext.getCurrentInstance().addPartialTarget(productionQtyBinding);
        AdfFacesContext.getCurrentInstance().addPartialTarget(acceptQtyBinding);
    }

    public void setPopulateAssyProdBreakupBinding(RichPopup populateAssyProdBreakupBinding) {
        this.populateAssyProdBreakupBinding = populateAssyProdBreakupBinding;
    }

    public RichPopup getPopulateAssyProdBreakupBinding() {
        return populateAssyProdBreakupBinding;
    }

    public void setHeaderSaveBinding(RichButton headerSaveBinding) {
        this.headerSaveBinding = headerSaveBinding;
    }

    public RichButton getHeaderSaveBinding() {
        return headerSaveBinding;
    }

    public void setHeaderSaveandCloseBinding(RichButton headerSaveandCloseBinding) {
        this.headerSaveandCloseBinding = headerSaveandCloseBinding;
    }

    public RichButton getHeaderSaveandCloseBinding() {
        return headerSaveandCloseBinding;
    }

    public void itemStVCL(ValueChangeEvent vce) {
        System.out.println("Inside VCE #########");

        if (vce != null) {
            vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
            System.out.println("Item St Binding Value :- " + itemStBinding.getValue());
            String itemType = (String) itemStBinding.getValue();
            System.out.println("------- Item Type value is-----*****" + itemType);
            if (vce.getNewValue().equals("MAIN")) {
                System.out.println("Inside vce new value #####" + itemType);
                System.out.println("------ In if block------*****");
                itemCodeBinding.setDisabled(true);
                uomBinding.setDisabled(true);
                requiredQtyBinding.setDisabled(true);
                availableStockBinding.setDisabled(true);
                shortStockBinding.setDisabled(true);

            } else {
                System.out.println("------ In else block------*****");
                itemCodeBinding.setDisabled(false);
                uomBinding.setDisabled(true);
                requiredQtyBinding.setDisabled(true);
                availableStockBinding.setDisabled(true);
                shortStockBinding.setDisabled(true);
            }
        }

    }

    public void setItemStBinding(RichInputComboboxListOfValues itemStBinding) {
        this.itemStBinding = itemStBinding;
    }

    public RichInputComboboxListOfValues getItemStBinding() {
        return itemStBinding;
    }

    public void setOperatorButtonBinding(RichButton operatorButtonBinding) {
        this.operatorButtonBinding = operatorButtonBinding;
    }

    public RichButton getOperatorButtonBinding() {
        return operatorButtonBinding;
    }

    public void setPartBreakupButtonBinding(RichButton partBreakupButtonBinding) {
        this.partBreakupButtonBinding = partBreakupButtonBinding;
    }

    public RichButton getPartBreakupButtonBinding() {
        return partBreakupButtonBinding;
    }

    public void setBreakdownReasonButtonBinding(RichButton breakdownReasonButtonBinding) {
        this.breakdownReasonButtonBinding = breakdownReasonButtonBinding;
    }

    public RichButton getBreakdownReasonButtonBinding() {
        return breakdownReasonButtonBinding;
    }

    public void jobCardNoVCL(ValueChangeEvent valueChangeEvent) {
        OperationBinding op = ADFUtils.findOperation("generateAssemblyProductionLotNo");
        op.execute();
    }

    public void setAcceptQtyBinding(RichInputText acceptQtyBinding) {
        this.acceptQtyBinding = acceptQtyBinding;
    }

    public RichInputText getAcceptQtyBinding() {
        return acceptQtyBinding;
    }

    public void shortStockCheckDialogListener(DialogEvent dialogEvent) {
        System.out.println("IN DIALOG EVENT");
        if (dialogEvent.getOutcome().name().equals("ok") || dialogEvent.getOutcome().name().equals("cancel")) {
            System.out.println("IN DIALOG EVENT IF");
            DCIteratorBinding dci = ADFUtils.findIterator("AssemblyProductionAssyProdnBreakupVO1Iterator");
            RowSetIterator rsi = dci.getViewObject().createRowSetIterator(null);
            while (rsi.hasNext()) {
                System.out.println("IN DIALOG EVENT WHILE");
                BigDecimal stock = new BigDecimal(0);
                Row r = rsi.next();
                stock =
                    (BigDecimal) r.getAttribute("ShortStock") == null ? new BigDecimal(0) :
                    (BigDecimal) r.getAttribute("ShortStock");
                System.out.println("IN DIALOG STOCK AVAILABLE===>" + stock);
                if (stock.compareTo(new BigDecimal(0)) == 1) {
                    System.out.println("IN IF OF WHILE");
                    stockStatus = "T";
                }
            }
            rsi.closeRowSetIterator();
        }

    }

    public String saveAndCloseAL() {
        mtlStatus = (String) ADFUtils.findOperation("mtlParaStockProduction").execute();
        DCIteratorBinding dci = ADFUtils.findIterator("AssemblyProductionAssyProdnBreakupVO1Iterator");
        RowSetIterator rsi = dci.getViewObject().createRowSetIterator(null);
        while (rsi.hasNext()) {
//            System.out.println("IN DIALOG EVENT WHILE");
            Row r = rsi.next(); 
            
            BigDecimal stock = new BigDecimal(0);
            stock =
                (BigDecimal) r.getAttribute("ShortStock") == null ? new BigDecimal(0) :
                (BigDecimal) r.getAttribute("ShortStock");
//            System.out.println("IN DIALOG STOCK AVAILABLE===>" + stock);
            if (stock.compareTo(new BigDecimal(0)) == 1) {
                System.out.println("IN IF OF WHILE");
                stockStatus = "T";
            }
            
// Added validation if Required Quantity is  Greater Than Available Stock -- Atul Gupta On: 09-09-2020           
            BigDecimal avlStock = (BigDecimal) r.getAttribute("AvailStock");
            BigDecimal reqQty = (BigDecimal) r.getAttribute("ReqQty");
            if(reqQty.compareTo(avlStock) > 0){
                stockStatus = "R";
                break;
            } 
            
        }
        rsi.closeRowSetIterator();

//        System.out.println("Short Stock Validate greater than 0==>" + stockStatus +
//                           "==Mtl Para Stock Check key No. 141" + mtlStatus);
        if (stockStatus.equalsIgnoreCase("T") && mtlStatus.equalsIgnoreCase("Y")) {
            ADFUtils.showMessage("Input Stock not available.Please Check!!!!!", 0);
        } 
        else if(stockStatus.equalsIgnoreCase("R")){
            ADFUtils.showMessage("Required Quantity Can Not be Greater Than Available Stock.Please Check!!!!!", 0);
        }
        else {
            if (approvedByBinding.getValue() != null) {
                OperationBinding op = ADFUtils.findOperation("generateAssemblyProductionSlipNo");
                Object rst = op.execute();
                AdfFacesContext.getCurrentInstance().addPartialTarget(populateAssyProdBreakupBinding);
                ADFUtils.setLastUpdatedBy("AssemblyProductionAssyProdnSlipVO1Iterator", "LastUpdatedBy");
                //        refreshPage();
                if (rst.toString() != null && rst.toString() != "") {
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message =
                        new FacesMessage("Record Saved Successfully.New Production Slip No. is " + rst + ".");
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);
                    return "save and close";
                } else {
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message = new FacesMessage("Record Updated Successfully.");
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);
                    return "save and close";
                }
            } else {
                //            FacesMessage message = new FacesMessage(".");
                //            message.setSeverity(FacesMessage.SEVERITY_ERROR);
                //            FacesContext context = FacesContext.getCurrentInstance();
                //            context.addMessage(address2Binding.getClientId(), message);
                ADFUtils.showMessage("Approved By is mandatory.", 0);
            }
        }
        return null;
    }

    public void itemCodePartVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce != null) {
            OperationBinding op = ADFUtils.findOperation("assemblyProdAvailableStock");
            op.execute();
        }

    }

    public void accpQtyValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (object != null) {
            System.out.println("Inside validator");

            BigDecimal accpQty = (BigDecimal) object;
            System.out.println("Inside validator");
            BigDecimal prdQty = (BigDecimal) getProductionQtyBinding().getValue();
            System.out.println("Inside validator");
            BigDecimal extQty = (BigDecimal) getExtraQtyBinding().getValue();
            System.out.println("Inside validator");
            BigDecimal result = prdQty.add(extQty);
            System.out.println("accptdQty: " + accpQty + " prodQty : " + prdQty + "extra qty   " + extQty + " result " +
                               "result");

            if (accpQty.compareTo(result) == 1) {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                              "Accepted Quantity should be less or equal to Production Quantity and Extra Quantity.",
                                                              null));

                //  throw new ValidatorException("Accepted Quantity should be less or equal to Production Quantity and Extra Quantity.",null);
            }

        }
    }

    public void setExtraQtyBinding(RichInputText extraQtyBinding) {
        this.extraQtyBinding = extraQtyBinding;
    }

    public RichInputText getExtraQtyBinding() {
        return extraQtyBinding;
    }
}
