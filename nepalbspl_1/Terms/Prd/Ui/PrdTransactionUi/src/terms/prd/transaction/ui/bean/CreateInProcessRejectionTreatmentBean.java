package terms.prd.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;

public class CreateInProcessRejectionTreatmentBean {
    private RichInputComboboxListOfValues unitCdBinding;
    private RichInputText docNoBinding;
    private RichInputComboboxListOfValues approvedByBinding;
    private RichInputDate docDateBinding;
    private RichOutputText outputTextBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichPopup getPopUpBinding;
    private RichInputText scrapQtyBinding;
    private RichInputComboboxListOfValues enteredByBinding;
    private RichInputText prodnSlipNoBinding;
    private RichInputText jobCardNoBinding;
    private RichInputText itemCdBinding;
    private RichInputText procCdBinding;
    private RichInputText defectCdBinding;
    private RichInputText rejQtyBinding;
    private RichInputText inputItemCdBinding;
    private RichInputText itemTypeBinding;
    private RichInputText scrapQty1Binding;
    private RichInputText scrapSlipNoBInding;
    private RichShowDetailItem inputScrapTabBinding;
    private RichShowDetailItem pendingRejectionTabBinding;
    private RichButton pendingRejButtonBinding;

    public CreateInProcessRejectionTreatmentBean() {
    }

    public void setUnitCdBinding(RichInputComboboxListOfValues unitCdBinding) {
        this.unitCdBinding = unitCdBinding;
    }

    public RichInputComboboxListOfValues getUnitCdBinding() {
        return unitCdBinding;
    }

    public void setDocNoBinding(RichInputText docNoBinding) {
        this.docNoBinding = docNoBinding;
    }

    public RichInputText getDocNoBinding() {
        return docNoBinding;
    }

    public void setApprovedByBinding(RichInputComboboxListOfValues approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputComboboxListOfValues getApprovedByBinding() {
        return approvedByBinding;
    }

    public void setDocDateBinding(RichInputDate docDateBinding) {
        this.docDateBinding = docDateBinding;
    }

    public RichInputDate getDocDateBinding() {
        return docDateBinding;
    }

    public void setOutputTextBinding(RichOutputText outputTextBinding) {
        this.outputTextBinding = outputTextBinding;
    }

    public RichOutputText getOutputTextBinding() {
        cevmodecheck();
        return outputTextBinding;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }
    
    private void cevmodecheck(){
            if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
                cevModeDisableComponent("V");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("C");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("E");
            }
        }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                try {
                    Method method1 =
                        component.getClass().getMethod("setDisabled", boolean.class);
                    if (method1 != null) {
                        method1.invoke(component, valueComponent);
                    }
                } catch (NoSuchMethodException e) {
                    try {
                        Method method =
                            component.getClass().getMethod("setReadOnly", boolean.class);
                        if (method != null) {
                            method.invoke(component, valueComponent);
                        }
                    } catch (Exception e1) {
                        // e.printStackTrace();//silently eat this exception.
                    }


                } catch (Exception e) {
                    // e.printStackTrace();//silently eat this exception.
                }
                List<UIComponent> childComponents = component.getChildren();
                for (UIComponent comp : childComponents) {
                    makeComponentHierarchyReadOnly(comp, valueComponent);
                }
            }
    
    //Set Fields and Button disable.
        public void cevModeDisableComponent(String mode) {
        //            FacesContext fctx = FacesContext.getCurrentInstance();
        //            ELContext elctx = fctx.getELContext();
        //            Application jsfApp = fctx.getApplication();
        //            //create a ValueExpression that points to the ADF binding layer
        //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
        //            //
        //            ValueExpression valueExpr = exprFactory.createValueExpression(
        //                                         elctx,
        //                                         "#{pageFlowScope.mode=='E'}",
        //                                          Object.class
        //                                         );
        //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
            if (mode.equals("E")) {
                    getHeaderEditBinding().setDisabled(true);
                getScrapQtyBinding().setDisabled(true);
                getEnteredByBinding().setDisabled(true);
                getDocNoBinding().setDisabled(true);
                getProdnSlipNoBinding().setDisabled(true);
                getJobCardNoBinding().setDisabled(true);
                getItemCdBinding().setDisabled(true);
                getProcCdBinding().setDisabled(true);    
                getDefectCdBinding().setDisabled(true);
                getRejQtyBinding().setDisabled(true);
                getInputItemCdBinding().setDisabled(true);
                getItemTypeBinding().setDisabled(true);
                getScrapQty1Binding().setDisabled(true);
                getScrapSlipNoBInding().setDisabled(true);
                getUnitCdBinding().setDisabled(true);
                getDocDateBinding().setDisabled(true);
                getPendingRejButtonBinding().setDisabled(true);

            } else if (mode.equals("C")) {
                    getUnitCdBinding().setDisabled(true);
                getHeaderEditBinding().setDisabled(true);
                getScrapQtyBinding().setDisabled(true);
                getEnteredByBinding().setDisabled(true);
                getDocNoBinding().setDisabled(true);
                getProdnSlipNoBinding().setDisabled(true);
                getJobCardNoBinding().setDisabled(true);
                getItemCdBinding().setDisabled(true);
                getProcCdBinding().setDisabled(true); 
                getDefectCdBinding().setDisabled(true);
                getRejQtyBinding().setDisabled(true);
                getInputItemCdBinding().setDisabled(true);
                getItemTypeBinding().setDisabled(true);
                getScrapQty1Binding().setDisabled(true);
                getScrapSlipNoBInding().setDisabled(true);
                getDocDateBinding().setDisabled(true);
            } else if (mode.equals("V")) {
                getInputScrapTabBinding().setDisabled(false);
                getPendingRejectionTabBinding().setDisabled(false);
                getPendingRejButtonBinding().setDisabled(true);
            }
            
        }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void getCurrentRowDataAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("copyDataPopulate").execute();
        getPopUpBinding.cancel();
    }

    public void popUpDataAL(ActionEvent actionEvent) {
        System.out.println("IN METHOD BEAN");
        ADFUtils.findOperation("filterInProcessPopulateVVO").execute();
        RichPopup.PopupHints popup=new RichPopup.PopupHints();
                getPopUpBinding.show(popup); 

    }

    public void setGetPopUpBinding(RichPopup getPopUpBinding) {
        this.getPopUpBinding = getPopUpBinding;
    }

    public RichPopup getGetPopUpBinding() {
        return getPopUpBinding;
    }

    public void setScrapQtyBinding(RichInputText scrapQtyBinding) {
        this.scrapQtyBinding = scrapQtyBinding;
    }

    public RichInputText getScrapQtyBinding() {
        return scrapQtyBinding;
    }

    public void saveDataAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("generateReworkNo").execute();
         ADFUtils.setLastUpdatedBy("InProcessRejectionTreatmentHeaderVO1Iterator","LastUpdatedBy");
        if(!pendingRejButtonBinding.isDisabled())
        { 
            ADFUtils.findOperation("Commit").execute();
            FacesMessage Message = new FacesMessage("Record Saved Successfully.New Job Card No. is "+docNoBinding.getValue()+"."); 
            Message.setSeverity(FacesMessage.SEVERITY_INFO); 
            FacesContext fc = FacesContext.getCurrentInstance(); 
            fc.addMessage(null, Message); 
        }
        else
        {
            ADFUtils.findOperation("Commit").execute();
            FacesMessage Message = new FacesMessage("Record Updated Successfully."); 
            Message.setSeverity(FacesMessage.SEVERITY_INFO); 
            FacesContext fc = FacesContext.getCurrentInstance(); 
            fc.addMessage(null, Message); 
        }
    }

    public void setEnteredByBinding(RichInputComboboxListOfValues enteredByBinding) {
        this.enteredByBinding = enteredByBinding;
    }

    public RichInputComboboxListOfValues getEnteredByBinding() {
        return enteredByBinding;
    }

    public void generateScrapAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("generateScrapDtlPopulate").execute();
        inputScrapTabBinding.setDisclosed(true);
        pendingRejectionTabBinding.setDisclosed(false);
    }

    public void setProdnSlipNoBinding(RichInputText prodnSlipNoBinding) {
        this.prodnSlipNoBinding = prodnSlipNoBinding;
    }

    public RichInputText getProdnSlipNoBinding() {
        return prodnSlipNoBinding;
    }

    public void setJobCardNoBinding(RichInputText jobCardNoBinding) {
        this.jobCardNoBinding = jobCardNoBinding;
    }

    public RichInputText getJobCardNoBinding() {
        return jobCardNoBinding;
    }

    public void setItemCdBinding(RichInputText itemCdBinding) {
        this.itemCdBinding = itemCdBinding;
    }

    public RichInputText getItemCdBinding() {
        return itemCdBinding;
    }

    public void setProcCdBinding(RichInputText procCdBinding) {
        this.procCdBinding = procCdBinding;
    }

    public RichInputText getProcCdBinding() {
        return procCdBinding;
    }

    public void setDefectCdBinding(RichInputText defectCdBinding) {
        this.defectCdBinding = defectCdBinding;
    }

    public RichInputText getDefectCdBinding() {
        return defectCdBinding;
    }

    public void setRejQtyBinding(RichInputText rejQtyBinding) {
        this.rejQtyBinding = rejQtyBinding;
    }

    public RichInputText getRejQtyBinding() {
        return rejQtyBinding;
    }

    public void setInputItemCdBinding(RichInputText inputItemCdBinding) {
        this.inputItemCdBinding = inputItemCdBinding;
    }

    public RichInputText getInputItemCdBinding() {
        return inputItemCdBinding;
    }

    public void setItemTypeBinding(RichInputText itemTypeBinding) {
        this.itemTypeBinding = itemTypeBinding;
    }

    public RichInputText getItemTypeBinding() {
        return itemTypeBinding;
    }

    public void setScrapQty1Binding(RichInputText scrapQty1Binding) {
        this.scrapQty1Binding = scrapQty1Binding;
    }

    public RichInputText getScrapQty1Binding() {
        return scrapQty1Binding;
    }

    public void setScrapSlipNoBInding(RichInputText scrapSlipNoBInding) {
        this.scrapSlipNoBInding = scrapSlipNoBInding;
    }

    public RichInputText getScrapSlipNoBInding() {
        return scrapSlipNoBInding;
    }

    public void setInputScrapTabBinding(RichShowDetailItem inputScrapTabBinding) {
        this.inputScrapTabBinding = inputScrapTabBinding;
    }

    public RichShowDetailItem getInputScrapTabBinding() {
        return inputScrapTabBinding;
    }

    public void setPendingRejectionTabBinding(RichShowDetailItem pendingRejectionTabBinding) {
        this.pendingRejectionTabBinding = pendingRejectionTabBinding;
    }

    public RichShowDetailItem getPendingRejectionTabBinding() {
        return pendingRejectionTabBinding;
    }

    public void setPendingRejButtonBinding(RichButton pendingRejButtonBinding) {
        this.pendingRejButtonBinding = pendingRejButtonBinding;
    }

    public RichButton getPendingRejButtonBinding() {
        return pendingRejButtonBinding;
    }
}
