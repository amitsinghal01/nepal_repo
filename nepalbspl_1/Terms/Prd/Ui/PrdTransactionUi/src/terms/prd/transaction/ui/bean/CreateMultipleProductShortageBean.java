package terms.prd.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

public class CreateMultipleProductShortageBean {
    private RichOutputText outputTextBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputComboboxListOfValues productCodeBindingHeader;
    private RichInputText qtyBindingHeader;
    private RichInputText partCodeBinding;
    private RichInputText processTypeBinding;
    private RichInputText qtyBindingDetail;
    private RichInputText shortageBinding;
    private RichInputText stockBinding;
    private RichInputText ucQtyBinding;
    private RichInputText uiQtyBinding;
    private RichButton generateButtonDtlBinding;
    private RichTable multProdShortgGenerateDtlTableBinding;
    private RichColumn partCdBinding;
    private RichInputText unitNameBinding;

    public CreateMultipleProductShortageBean() {
    }

    public void populateMultPrdShortagePopulatePlan(ActionEvent actionEvent) {
        System.out.println("Inside Populate Plan Bean ########");
        ADFUtils.findOperation("multiplePrdSrtgPopulatePlan").execute();
        
        ADFUtils.findOperation("populateMultipleProductShortagePopulatePlan").execute();
        ADFUtils.findOperation("Commit").execute();
        OperationBinding op=ADFUtils.findOperation("multiplePrdSrtgGenerate");
        op.execute();
        ADFUtils.findOperation("Commit").execute();
    }

    public void saveRecordMultProdShortage(ActionEvent actionEvent) {
        Integer TransValue=(Integer)outputTextBinding.getValue();
        System.out.println("TRANS VALUE"+TransValue);
        ADFUtils.setLastUpdatedBy("MultipleProductShortageVO1Iterator","LastUpdatedBy");
        if(TransValue==1 || TransValue==2)       
        {
           FacesMessage Message = new FacesMessage("Record Save Successfully");   
           Message.setSeverity(FacesMessage.SEVERITY_INFO);   
           FacesContext fc = FacesContext.getCurrentInstance();   
           fc.addMessage(null, Message);      
            
        }
        else 
        {
            FacesMessage Message = new FacesMessage("Record Update Successfully");   
            Message.setSeverity(FacesMessage.SEVERITY_INFO);   
            FacesContext fc = FacesContext.getCurrentInstance();   
            fc.addMessage(null, Message);      
        }
    }

    public void multPrdSrtgGenerateData(ActionEvent actionEvent) {
        System.out.println("Inside Bean ########");
        
        ADFUtils.findOperation("Commit").execute();
        OperationBinding op=ADFUtils.findOperation("multiplePrdSrtgGenerate");
        op.execute();
        ADFUtils.findOperation("multiplePrdSrtgDataPopulateGenerate").execute();
//        ADFUtils.findOperation("Commit").execute();
        System.out.println("Exceute Data Populate #####");
       // AdfFacesContext.getCurrentInstance().addPartialTarget(multProdShortgGenerateDtlTableBinding);
        System.out.println("After Bean ########");
    }

    public void setOutputTextBinding(RichOutputText outputTextBinding) {
        this.outputTextBinding = outputTextBinding;
    }

    public RichOutputText getOutputTextBinding() {
        cevmodecheck();
        return outputTextBinding;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
               try {
                   Method method1 =
                       component.getClass().getMethod("setDisabled", boolean.class);
                   if (method1 != null) {
                       method1.invoke(component, valueComponent);
                   }
               } catch (NoSuchMethodException e) {
                   try {
                       Method method =
                           component.getClass().getMethod("setReadOnly", boolean.class);
                       if (method != null) {
                           method.invoke(component, valueComponent);
                       }
                   } catch (Exception e1) {
                       // e.printStackTrace();//silently eat this exception.
                   }


               } catch (Exception e) {
                   // e.printStackTrace();//silently eat this exception.
               }
               List<UIComponent> childComponents = component.getChildren();
               for (UIComponent comp : childComponents) {
                   makeComponentHierarchyReadOnly(comp, valueComponent);
               }
           }
    
    private void cevmodecheck(){
           if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
               cevModeDisableComponent("V");
           }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
               cevModeDisableComponent("C");
           }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
               cevModeDisableComponent("E");
           }
       }
    
    public void cevModeDisableComponent(String mode) 
        {
              if (mode.equals("E")) {
                getUnitCodeBinding().setDisabled(true);
                getUnitNameBinding().setDisabled(true);
                getProductCodeBindingHeader().setDisabled(true);
                getQtyBindingHeader().setDisabled(true);
                getPartCodeBinding().setDisabled(true);
                getProcessTypeBinding().setDisabled(true);
                getQtyBindingDetail().setDisabled(true);
                getShortageBinding().setDisabled(true);
                getStockBinding().setDisabled(true);
                getUcQtyBinding().setDisabled(true);
                getUiQtyBinding().setDisabled(true);
                getHeaderEditBinding().setDisabled(true);
    //                getOperatorButtonBinding().setDisabled(false);
                
               
            } else if (mode.equals("C")) {
                getUnitCodeBinding().setDisabled(true);
                getUnitNameBinding().setDisabled(true);
//                getProductCodeBindingHeader().setDisabled(true);
//                getQtyBindingHeader().setDisabled(true);
                getPartCodeBinding().setDisabled(true);
                getProcessTypeBinding().setDisabled(true);
                getQtyBindingDetail().setDisabled(true);
                getShortageBinding().setDisabled(true);
                getStockBinding().setDisabled(true);
                getUcQtyBinding().setDisabled(true);
                getUiQtyBinding().setDisabled(true);
                getHeaderEditBinding().setDisabled(true);
                
    //                itemCodeBinding.setDisabled(true);
    //                uomBinding.setDisabled(true);
    //                requiredQtyBinding.setDisabled(true);
    //                availableStockBinding.setDisabled(true);
    //                shortStockBinding.setDisabled(true);
            
            } else if (mode.equals("V")) {
                  getGenerateButtonDtlBinding().setDisabled(true);
//                getHeaderEditBinding().setDisabled(true);
    
            }
        }


    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setProductCodeBindingHeader(RichInputComboboxListOfValues productCodeBindingHeader) {
        this.productCodeBindingHeader = productCodeBindingHeader;
    }

    public RichInputComboboxListOfValues getProductCodeBindingHeader() {
        return productCodeBindingHeader;
    }

    public void setQtyBindingHeader(RichInputText qtyBindingHeader) {
        this.qtyBindingHeader = qtyBindingHeader;
    }

    public RichInputText getQtyBindingHeader() {
        return qtyBindingHeader;
    }

    public void setPartCodeBinding(RichInputText partCodeBinding) {
        this.partCodeBinding = partCodeBinding;
    }

    public RichInputText getPartCodeBinding() {
        return partCodeBinding;
    }

    public void setProcessTypeBinding(RichInputText processTypeBinding) {
        this.processTypeBinding = processTypeBinding;
    }

    public RichInputText getProcessTypeBinding() {
        return processTypeBinding;
    }

    public void setQtyBindingDetail(RichInputText qtyBindingDetail) {
        this.qtyBindingDetail = qtyBindingDetail;
    }

    public RichInputText getQtyBindingDetail() {
        return qtyBindingDetail;
    }

    public void setShortageBinding(RichInputText shortageBinding) {
        this.shortageBinding = shortageBinding;
    }

    public RichInputText getShortageBinding() {
        return shortageBinding;
    }

    public void setStockBinding(RichInputText stockBinding) {
        this.stockBinding = stockBinding;
    }

    public RichInputText getStockBinding() {
        return stockBinding;
    }

    public void setUcQtyBinding(RichInputText ucQtyBinding) {
        this.ucQtyBinding = ucQtyBinding;
    }

    public RichInputText getUcQtyBinding() {
        return ucQtyBinding;
    }

    public void setUiQtyBinding(RichInputText uiQtyBinding) {
        this.uiQtyBinding = uiQtyBinding;
    }

    public RichInputText getUiQtyBinding() {
        return uiQtyBinding;
    }

    public void setGenerateButtonDtlBinding(RichButton generateButtonDtlBinding) {
        this.generateButtonDtlBinding = generateButtonDtlBinding;
    }

    public RichButton getGenerateButtonDtlBinding() {
        return generateButtonDtlBinding;
    }

    public void setMultProdShortgGenerateDtlTableBinding(RichTable multProdShortgGenerateDtlTableBinding) {
        this.multProdShortgGenerateDtlTableBinding = multProdShortgGenerateDtlTableBinding;
    }

    public RichTable getMultProdShortgGenerateDtlTableBinding() {
        return multProdShortgGenerateDtlTableBinding;
    }

    public void setPartCdBinding(RichColumn partCdBinding) {
        this.partCdBinding = partCdBinding;
    }

    public RichColumn getPartCdBinding() {
        return partCdBinding;
    }

    public void setUnitNameBinding(RichInputText unitNameBinding) {
        this.unitNameBinding = unitNameBinding;
    }

    public RichInputText getUnitNameBinding() {
        return unitNameBinding;
    }
}
