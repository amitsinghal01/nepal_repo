package terms.prd.transaction.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import javax.faces.validator.ValidatorException;

import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class CreatePDIRejectionDetailBean {
    private RichTable createReworkInputItemDtlTableBinding;
    private RichOutputText outputTextBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichInputText okQtyBinding;
    private RichInputText usedQtyBinding;
    private RichInputText itemCdBinding;
    private RichButton headerEditBinding;
    private RichButton headerSaveBinding;
    private RichButton headerSaveandCloseBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText entryNoBinding;
    private RichInputDate entryDateBinding;
    private RichInputText productionSlipNoBinding;
    private RichInputText defectCodeBinding;
    private RichInputText finalInspNoBinding;
    private RichInputText rejectQtyBinding;
    private RichInputText scrapQtyBinding;
    private RichInputText jobCardNoBinding;
    private RichInputText reworkQtyBinding;
    private RichInputText reworkHrsBinding;
    private RichInputText manPowerInvBinding;
    private RichInputText costInvolvedBinding;
    private RichInputText seggQtyBinding;
    private RichInputText seggHrsBinding;
    private RichInputText seggManBinding;
    private RichInputText seggCostBinding;
    private RichInputText itemCdGenBreakupBinding;
    private RichInputText noOffReworkDtlBinding;
    private RichInputText rejectedQtyBinding;
    private RichInputText requiredQtyBinding;
    private RichInputText itemCdInputScrapBinding;
    private RichInputText uomScrapBinding;
    private RichInputText scrapQtyInputScrapBinding;
    private RichInputText scrapSlipNoBinding;
    private RichButton generateBreakupButtonBinding;
    private RichButton inputScrapDetailButtonBinding;
    private RichButton pendingRejectionsButtonBinding;
    private RichPopup populateAssyReworkHeadPendingRejectionsBinding;
    private RichTable pdiAssyReworkHeadTableBinding;

    public CreatePDIRejectionDetailBean() {
    }

    public void saveGenerateEntryNo(ActionEvent actionEvent) 
    {
        OperationBinding op=  ADFUtils.findOperation("generatePDIRejectionDetailEntryNo");
        Object rst=op.execute();

        ADFUtils.setLastUpdatedBy("PDIRejectionDetailAssyReworkHeadVO1Iterator","LastUpdatedBy");
          if(rst.toString() != null && rst.toString() != "")
          { 
              ADFUtils.findOperation("Commit").execute();
              FacesMessage Message = new FacesMessage("Record Saved Successfully.New Entry No. is "+rst+"."); 
              Message.setSeverity(FacesMessage.SEVERITY_INFO); 
              FacesContext fc = FacesContext.getCurrentInstance(); 
              fc.addMessage(null, Message); 
          }
          else
          {
              ADFUtils.findOperation("Commit").execute();
              FacesMessage Message = new FacesMessage("Record Updated Successfully."); 
              Message.setSeverity(FacesMessage.SEVERITY_INFO); 
              FacesContext fc = FacesContext.getCurrentInstance(); 
              fc.addMessage(null, Message); 
          } 
    }

    public void reworkInputItemDtldeletePopupDialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
            {
            ADFUtils.findOperation("Delete").execute();
            System.out.println("Record Delete Successfully");
            
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");   
                Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                FacesContext fc = FacesContext.getCurrentInstance();   
                fc.addMessage(null, Message);   
            
            }

        AdfFacesContext.getCurrentInstance().addPartialTarget(createReworkInputItemDtlTableBinding);
    }

    public void setCreateReworkInputItemDtlTableBinding(RichTable createReworkInputItemDtlTableBinding) {
        this.createReworkInputItemDtlTableBinding = createReworkInputItemDtlTableBinding;
    }

    public RichTable getCreateReworkInputItemDtlTableBinding() {
        return createReworkInputItemDtlTableBinding;
    }

    public void setOutputTextBinding(RichOutputText outputTextBinding) {
        this.outputTextBinding = outputTextBinding;
    }

    public RichOutputText getOutputTextBinding() {
        cevmodecheck();
        return outputTextBinding;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void populateDataAssyReworkHeadPendingRejections(ActionEvent actionEvent) {
        System.out.println("Before Method Call #####");
        ADFUtils.findOperation("filterPDIRejectionDetailPopulateVVO").execute();
        RichPopup.PopupHints popup=new RichPopup.PopupHints();
        getPopulateAssyReworkHeadPendingRejectionsBinding().show(popup);
        System.out.println("After Method Call #####");
//        OperationBinding op=ADFUtils.findOperation("populatePDIRejectionDtlPendingRejections");
//        op.execute();
    }

    public void populateDataReworkInputItemDtlGenerateBreakup(ActionEvent actionEvent) {
        OperationBinding op=ADFUtils.findOperation("populatePDIRejectionDtlGenerateBreakup");
        op.execute();
    }

    public void setOkQtyBinding(RichInputText okQtyBinding) {
        this.okQtyBinding = okQtyBinding;
    }

    public RichInputText getOkQtyBinding() {
        return okQtyBinding;
    }

    public void setUsedQtyBinding(RichInputText usedQtyBinding) {
        this.usedQtyBinding = usedQtyBinding;
    }

    public RichInputText getUsedQtyBinding() {
        return usedQtyBinding;
    }

    public void populateDataPDIRejectionDetailInputScrapDetail(ActionEvent actionEvent) {
        OperationBinding op=ADFUtils.findOperation("populatePDIRejectionDtlInputScrapDetail");
        op.execute();
    }

    public void setItemCdBinding(RichInputText itemCdBinding) {
        this.itemCdBinding = itemCdBinding;
    }

    public RichInputText getItemCdBinding() {
        return itemCdBinding;
    }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
               try {
                   Method method1 =
                       component.getClass().getMethod("setDisabled", boolean.class);
                   if (method1 != null) {
                       method1.invoke(component, valueComponent);
                   }
               } catch (NoSuchMethodException e) {
                   try {
                       Method method =
                           component.getClass().getMethod("setReadOnly", boolean.class);
                       if (method != null) {
                           method.invoke(component, valueComponent);
                       }
                   } catch (Exception e1) {
                       // e.printStackTrace();//silently eat this exception.
                   }


               } catch (Exception e) {
                   // e.printStackTrace();//silently eat this exception.
               }
               List<UIComponent> childComponents = component.getChildren();
               for (UIComponent comp : childComponents) {
                   makeComponentHierarchyReadOnly(comp, valueComponent);
               }
           }
    
    private void cevmodecheck(){
           if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
               cevModeDisableComponent("V");
           }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
               cevModeDisableComponent("C");
           }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
               cevModeDisableComponent("E");
           }
       }
    
    public void cevModeDisableComponent(String mode) 
        {
              if (mode.equals("E")) {
                  getUnitCodeBinding().setDisabled(true);
                  getEntryNoBinding().setDisabled(true);
                  getEntryDateBinding().setDisabled(true);
                  getItemCdBinding().setDisabled(true);
                  getProductionSlipNoBinding().setDisabled(true);
                  getDefectCodeBinding().setDisabled(true);
                  getFinalInspNoBinding().setDisabled(true);
                  getRejectQtyBinding().setDisabled(true);
                  getJobCardNoBinding().setDisabled(true);
                  getScrapQtyBinding().setDisabled(true);
                  getItemCdGenBreakupBinding().setDisabled(true);
                  getNoOffReworkDtlBinding().setDisabled(true);
                  getUsedQtyBinding().setDisabled(true);
                  getRejectedQtyBinding().setDisabled(true);
                  getItemCdInputScrapBinding().setDisabled(true);
                  getUomScrapBinding().setDisabled(true);
                  getScrapQtyInputScrapBinding().setDisabled(true);
                  getScrapSlipNoBinding().setDisabled(true);
                  getGenerateBreakupButtonBinding().setDisabled(false);
                  getInputScrapDetailButtonBinding().setDisabled(false);
                  getHeaderEditBinding().setDisabled(true);
            } else if (mode.equals("C")) {
                  getUnitCodeBinding().setDisabled(true);
                  getEntryNoBinding().setDisabled(true);
                  getEntryDateBinding().setDisabled(true);
                  getItemCdBinding().setDisabled(true);
                  getProductionSlipNoBinding().setDisabled(true);
                  getDefectCodeBinding().setDisabled(true);
                  getFinalInspNoBinding().setDisabled(true);
                  getRejectQtyBinding().setDisabled(true);
                  getJobCardNoBinding().setDisabled(true);
                  getScrapQtyBinding().setDisabled(true);
                  getItemCdGenBreakupBinding().setDisabled(true);
                  getNoOffReworkDtlBinding().setDisabled(true);
                  getUsedQtyBinding().setDisabled(true);
                  getRejectedQtyBinding().setDisabled(true);
                  getItemCdInputScrapBinding().setDisabled(true);
                  getUomScrapBinding().setDisabled(true);
                  getScrapQtyInputScrapBinding().setDisabled(true);
                  getScrapSlipNoBinding().setDisabled(true);
                  getHeaderEditBinding().setDisabled(true);
            
            } else if (mode.equals("V")) {
//                getHeaderEditBinding().setDisabled(true);
                getHeaderSaveBinding().setDisabled(true);
                getHeaderSaveandCloseBinding().setDisabled(true);
                getGenerateBreakupButtonBinding().setDisabled(true);
                getInputScrapDetailButtonBinding().setDisabled(true);
            }
        }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setHeaderSaveBinding(RichButton headerSaveBinding) {
        this.headerSaveBinding = headerSaveBinding;
    }

    public RichButton getHeaderSaveBinding() {
        return headerSaveBinding;
    }

    public void setHeaderSaveandCloseBinding(RichButton headerSaveandCloseBinding) {
        this.headerSaveandCloseBinding = headerSaveandCloseBinding;
    }

    public RichButton getHeaderSaveandCloseBinding() {
        return headerSaveandCloseBinding;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setEntryNoBinding(RichInputText entryNoBinding) {
        this.entryNoBinding = entryNoBinding;
    }

    public RichInputText getEntryNoBinding() {
        return entryNoBinding;
    }

    public void setEntryDateBinding(RichInputDate entryDateBinding) {
        this.entryDateBinding = entryDateBinding;
    }

    public RichInputDate getEntryDateBinding() {
        return entryDateBinding;
    }

    public void setProductionSlipNoBinding(RichInputText productionSlipNoBinding) {
        this.productionSlipNoBinding = productionSlipNoBinding;
    }

    public RichInputText getProductionSlipNoBinding() {
        return productionSlipNoBinding;
    }

    public void setDefectCodeBinding(RichInputText defectCodeBinding) {
        this.defectCodeBinding = defectCodeBinding;
    }

    public RichInputText getDefectCodeBinding() {
        return defectCodeBinding;
    }

    public void setFinalInspNoBinding(RichInputText finalInspNoBinding) {
        this.finalInspNoBinding = finalInspNoBinding;
    }

    public RichInputText getFinalInspNoBinding() {
        return finalInspNoBinding;
    }

    public void setRejectQtyBinding(RichInputText rejectQtyBinding) {
        this.rejectQtyBinding = rejectQtyBinding;
    }

    public RichInputText getRejectQtyBinding() {
        return rejectQtyBinding;
    }

    public void setScrapQtyBinding(RichInputText scrapQtyBinding) {
        this.scrapQtyBinding = scrapQtyBinding;
    }

    public RichInputText getScrapQtyBinding() {
        return scrapQtyBinding;
    }

    public void setJobCardNoBinding(RichInputText jobCardNoBinding) {
        this.jobCardNoBinding = jobCardNoBinding;
    }

    public RichInputText getJobCardNoBinding() {
        return jobCardNoBinding;
    }

    public void setReworkQtyBinding(RichInputText reworkQtyBinding) {
        this.reworkQtyBinding = reworkQtyBinding;
    }

    public RichInputText getReworkQtyBinding() {
        return reworkQtyBinding;
    }

    public void setReworkHrsBinding(RichInputText reworkHrsBinding) {
        this.reworkHrsBinding = reworkHrsBinding;
    }

    public RichInputText getReworkHrsBinding() {
        return reworkHrsBinding;
    }

    public void setManPowerInvBinding(RichInputText manPowerInvBinding) {
        this.manPowerInvBinding = manPowerInvBinding;
    }

    public RichInputText getManPowerInvBinding() {
        return manPowerInvBinding;
    }

    public void setCostInvolvedBinding(RichInputText costInvolvedBinding) {
        this.costInvolvedBinding = costInvolvedBinding;
    }

    public RichInputText getCostInvolvedBinding() {
        return costInvolvedBinding;
    }

    public void setSeggQtyBinding(RichInputText seggQtyBinding) {
        this.seggQtyBinding = seggQtyBinding;
    }

    public RichInputText getSeggQtyBinding() {
        return seggQtyBinding;
    }

    public void setSeggHrsBinding(RichInputText seggHrsBinding) {
        this.seggHrsBinding = seggHrsBinding;
    }

    public RichInputText getSeggHrsBinding() {
        return seggHrsBinding;
    }

    public void setSeggManBinding(RichInputText seggManBinding) {
        this.seggManBinding = seggManBinding;
    }

    public RichInputText getSeggManBinding() {
        return seggManBinding;
    }

    public void setSeggCostBinding(RichInputText seggCostBinding) {
        this.seggCostBinding = seggCostBinding;
    }

    public RichInputText getSeggCostBinding() {
        return seggCostBinding;
    }

    public void setItemCdGenBreakupBinding(RichInputText itemCdGenBreakupBinding) {
        this.itemCdGenBreakupBinding = itemCdGenBreakupBinding;
    }

    public RichInputText getItemCdGenBreakupBinding() {
        return itemCdGenBreakupBinding;
    }

    public void setNoOffReworkDtlBinding(RichInputText noOffReworkDtlBinding) {
        this.noOffReworkDtlBinding = noOffReworkDtlBinding;
    }

    public RichInputText getNoOffReworkDtlBinding() {
        return noOffReworkDtlBinding;
    }

    public void setRejectedQtyBinding(RichInputText rejectedQtyBinding) {
        this.rejectedQtyBinding = rejectedQtyBinding;
    }

    public RichInputText getRejectedQtyBinding() {
        return rejectedQtyBinding;
    }

    public void setRequiredQtyBinding(RichInputText requiredQtyBinding) {
        this.requiredQtyBinding = requiredQtyBinding;
    }

    public RichInputText getRequiredQtyBinding() {
        return requiredQtyBinding;
    }

    public void setItemCdInputScrapBinding(RichInputText itemCdInputScrapBinding) {
        this.itemCdInputScrapBinding = itemCdInputScrapBinding;
    }

    public RichInputText getItemCdInputScrapBinding() {
        return itemCdInputScrapBinding;
    }

    public void setUomScrapBinding(RichInputText uomScrapBinding) {
        this.uomScrapBinding = uomScrapBinding;
    }

    public RichInputText getUomScrapBinding() {
        return uomScrapBinding;
    }

    public void setScrapQtyInputScrapBinding(RichInputText scrapQtyInputScrapBinding) {
        this.scrapQtyInputScrapBinding = scrapQtyInputScrapBinding;
    }

    public RichInputText getScrapQtyInputScrapBinding() {
        return scrapQtyInputScrapBinding;
    }

    public void setScrapSlipNoBinding(RichInputText scrapSlipNoBinding) {
        this.scrapSlipNoBinding = scrapSlipNoBinding;
    }

    public RichInputText getScrapSlipNoBinding() {
        return scrapSlipNoBinding;
    }

    public void setGenerateBreakupButtonBinding(RichButton generateBreakupButtonBinding) {
        this.generateBreakupButtonBinding = generateBreakupButtonBinding;
    }

    public RichButton getGenerateBreakupButtonBinding() {
        return generateBreakupButtonBinding;
    }

    public void setInputScrapDetailButtonBinding(RichButton inputScrapDetailButtonBinding) {
        this.inputScrapDetailButtonBinding = inputScrapDetailButtonBinding;
    }

    public RichButton getInputScrapDetailButtonBinding() {
        return inputScrapDetailButtonBinding;
    }

    public void setPendingRejectionsButtonBinding(RichButton pendingRejectionsButtonBinding) {
        this.pendingRejectionsButtonBinding = pendingRejectionsButtonBinding;
    }

    public RichButton getPendingRejectionsButtonBinding() {
        return pendingRejectionsButtonBinding;
    }

    public void setPopulateAssyReworkHeadPendingRejectionsBinding(RichPopup populateAssyReworkHeadPendingRejectionsBinding) {
        this.populateAssyReworkHeadPendingRejectionsBinding = populateAssyReworkHeadPendingRejectionsBinding;
    }

    public RichPopup getPopulateAssyReworkHeadPendingRejectionsBinding() {
        return populateAssyReworkHeadPendingRejectionsBinding;
    }

    public void setPdiAssyReworkHeadTableBinding(RichTable pdiAssyReworkHeadTableBinding) {
        this.pdiAssyReworkHeadTableBinding = pdiAssyReworkHeadTableBinding;
    }

    public RichTable getPdiAssyReworkHeadTableBinding() {
        return pdiAssyReworkHeadTableBinding;
    }

    public void populateDataAssyReworkHeadTable(ActionEvent actionEvent) {
        OperationBinding op=ADFUtils.findOperation("populatePDIRejDtlPendingRejections");
        op.execute();
        populateAssyReworkHeadPendingRejectionsBinding.hide();
    }

    public void reworkQtyLessThanValidator(FacesContext facesContext, UIComponent uIComponent, Object object) 
    {
        BigDecimal obj = (BigDecimal) object;
        BigDecimal rejectQuantity = (BigDecimal) rejectQtyBinding.getValue();
        System.out.println("REWORK QTY" + obj + "REJECT QTY" + rejectQuantity);
        
        if(obj != null && rejectQuantity != null)
        {
        if(obj.compareTo(rejectQuantity)==1) 
        {
            FacesMessage Message = new FacesMessage("Rework Qty. should be less than or equal to Reject Qty."); 
            Message.setSeverity(FacesMessage.SEVERITY_ERROR); 
            FacesContext fc = FacesContext.getCurrentInstance(); 
            fc.addMessage(null, Message);
            throw new ValidatorException(Message);
        }
        }
    }

    public void seggQtyLessThanValidator(FacesContext facesContext, UIComponent uIComponent, Object object)
    {
        BigDecimal obj = (BigDecimal) object;
        BigDecimal rejectQuantity = (BigDecimal) rejectQtyBinding.getValue();
        System.out.println("SEGG QTY" + obj + "REJECT QTY" + rejectQuantity);
        
        if(obj != null && rejectQuantity != null)
        {
        if(obj.compareTo(rejectQuantity)==1) 
        {
            FacesMessage Message = new FacesMessage("Segg. Qty. should be less than or equal to Reject Qty."); 
            Message.setSeverity(FacesMessage.SEVERITY_ERROR); 
            FacesContext fc = FacesContext.getCurrentInstance(); 
            fc.addMessage(null, Message);
            throw new ValidatorException(Message);
        }
        }
     }

    public void oKQtyLessThanValidator(FacesContext facesContext, UIComponent uIComponent, Object object) 
    {
        BigDecimal obj = (BigDecimal) object;
        BigDecimal usedQuantity = (BigDecimal) usedQtyBinding.getValue();
        System.out.println("OK QTY" + obj + "USED QTY" + usedQuantity);
        
        if(obj != null && usedQuantity != null)
        {
        if(obj.compareTo(usedQuantity)==1) 
        {
            FacesMessage Message = new FacesMessage("Ok Qty. should be less than or equal to Used Qty."); 
            Message.setSeverity(FacesMessage.SEVERITY_ERROR); 
            FacesContext fc = FacesContext.getCurrentInstance(); 
            fc.addMessage(null, Message);
            throw new ValidatorException(Message);
        }
        }
    }
}
