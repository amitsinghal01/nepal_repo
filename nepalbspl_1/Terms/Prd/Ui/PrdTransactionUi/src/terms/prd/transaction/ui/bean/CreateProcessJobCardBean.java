package terms.prd.transaction.ui.bean;

import java.lang.reflect.Method;

import java.sql.Timestamp;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import java.util.Locale;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;
import javax.faces.validator.ValidatorException;

import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichButton;

import oracle.adf.view.rich.component.rich.output.RichPanelCollection;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

public class CreateProcessJobCardBean {
    private RichInputText jobCardNoBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputDate jobCardDateBinding;
    private RichSelectOneChoice monthBinding;
    private RichInputText yearBinding;
    private RichInputText processCodeBinding;
    private RichInputText seqNoBinding;
    private RichInputText requiredQuantityBinding;
    private RichInputText itemCdBinding;
    private RichInputText itemTypeBinding;
    private RichInputText requiredQtyDetailBinding;
    private RichInputText alternateItemBinding;
    private RichButton headerEditBinding;
    private RichInputText outputTextBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichInputText partDescBinding;
    private RichInputText reqQty1Binding;
    private RichInputText rmTypeBinding;
    private RichShowDetailItem lotRmReqTabBinding;
    private RichShowDetailItem altPpJcTabBinding;
    private RichInputText uomBinding;
    private RichInputText producedQtyBinding;
    private RichInputText issuedQtyBinding;
    private RichInputText returnQtyBinding;
    private RichInputText consumedQtyBinding;
    private RichInputText issuedQty1Binding;
    private RichInputText consQtyBinding;
    private RichInputText balanceQtyBinding;
    private RichPanelCollection panelCollectionBinding;
    private RichButton saveButtonBinding;
    private RichInputComboboxListOfValues partCodeBinding;
    private String pop_Flag="N";
    private RichInputText currentStockBinding;
    private RichInputText closStockBinding;
    private RichInputText currentStocksBinding;
    private RichShowDetailItem processDtlTabBinding;

    public CreateProcessJobCardBean() {
    }

    public void setJobCardNoBinding(RichInputText jobCardNoBinding) {
        this.jobCardNoBinding = jobCardNoBinding;
    }

    public RichInputText getJobCardNoBinding() {
        return jobCardNoBinding;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setJobCardDateBinding(RichInputDate jobCardDateBinding) {
        this.jobCardDateBinding = jobCardDateBinding;
    }

    public RichInputDate getJobCardDateBinding() {
        return jobCardDateBinding;
    }

    public void setMonthBinding(RichSelectOneChoice monthBinding) {
        this.monthBinding = monthBinding;
    }

    public RichSelectOneChoice getMonthBinding() {
        return monthBinding;
    }

    public void setYearBinding(RichInputText yearBinding) {
        this.yearBinding = yearBinding;
    }

    public RichInputText getYearBinding() {
        return yearBinding;
    }


    public void setProcessCodeBinding(RichInputText processCodeBinding) {
        this.processCodeBinding = processCodeBinding;
    }

    public RichInputText getProcessCodeBinding() {
        return processCodeBinding;
    }

    public void setSeqNoBinding(RichInputText seqNoBinding) {
        this.seqNoBinding = seqNoBinding;
    }

    public RichInputText getSeqNoBinding() {
        return seqNoBinding;
    }

    public void setRequiredQuantityBinding(RichInputText requiredQuantityBinding) {
        this.requiredQuantityBinding = requiredQuantityBinding;
    }

    public RichInputText getRequiredQuantityBinding() {
        return requiredQuantityBinding;
    }


    public void setItemCdBinding(RichInputText itemCdBinding) {
        this.itemCdBinding = itemCdBinding;
    }

    public RichInputText getItemCdBinding() {
        return itemCdBinding;
    }

    public void setItemTypeBinding(RichInputText itemTypeBinding) {
        this.itemTypeBinding = itemTypeBinding;
    }

    public RichInputText getItemTypeBinding() {
        return itemTypeBinding;
    }

    public void setRequiredQtyDetailBinding(RichInputText requiredQtyDetailBinding) {
        this.requiredQtyDetailBinding = requiredQtyDetailBinding;
    }

    public RichInputText getRequiredQtyDetailBinding() {
        return requiredQtyDetailBinding;
    }

    public void setAlternateItemBinding(RichInputText alternateItemBinding) {
        this.alternateItemBinding = alternateItemBinding;
    }

    public RichInputText getAlternateItemBinding() {
        return alternateItemBinding;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setOutputTextBinding(RichInputText outputTextBinding) {
        this.outputTextBinding = outputTextBinding;
    }

    public RichInputText getOutputTextBinding() {
        cevmodecheck();
        return outputTextBinding;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }
    
    private void cevmodecheck(){
            if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
                cevModeDisableComponent("V");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("C");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("E");
            }
        }
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                try {
                    Method method1 =
                        component.getClass().getMethod("setDisabled", boolean.class);
                    if (method1 != null) {
                        method1.invoke(component, valueComponent);
                    }
                } catch (NoSuchMethodException e) {
                    try {
                        Method method =
                            component.getClass().getMethod("setReadOnly", boolean.class);
                        if (method != null) {
                            method.invoke(component, valueComponent);
                        }
                    } catch (Exception e1) {
                        // e.printStackTrace();//silently eat this exception.
                    }


                } catch (Exception e) {
                    // e.printStackTrace();//silently eat this exception.
                }
                List<UIComponent> childComponents = component.getChildren();
                for (UIComponent comp : childComponents) {
                    makeComponentHierarchyReadOnly(comp, valueComponent);
                }
            }
    
    //Set Fields and Button disable.
        public void cevModeDisableComponent(String mode) {
        //            FacesContext fctx = FacesContext.getCurrentInstance();
        //            ELContext elctx = fctx.getELContext();
        //            Application jsfApp = fctx.getApplication();
        //            //create a ValueExpression that points to the ADF binding layer
        //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
        //            //
        //            ValueExpression valueExpr = exprFactory.createValueExpression(
        //                                         elctx,
        //                                         "#{pageFlowScope.mode=='E'}",
        //                                          Object.class
        //                                         );
        //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
          
          
            if (mode.equals("E")) {
                getHeaderEditBinding().setDisabled(true);
                getMonthBinding().setDisabled(true);
                getYearBinding().setDisabled(true);
                getProcessCodeBinding().setDisabled(true);
                getSeqNoBinding().setDisabled(true);
                getRequiredQuantityBinding().setDisabled(true);
                getItemCdBinding().setDisabled(true);
                getItemTypeBinding().setDisabled(true);
                getRequiredQtyDetailBinding().setDisabled(true);
                getAlternateItemBinding().setDisabled(true);
                getRequiredQtyDetailBinding().setDisabled(true);
                getPartDescBinding().setDisabled(true);
                getJobCardNoBinding().setDisabled(true); 
                getReqQty1Binding().setDisabled(true);
                getRmTypeBinding().setDisabled(true);
                getUomBinding().setDisabled(true);   
                getProducedQtyBinding().setDisabled(true);
                getIssuedQtyBinding().setDisabled(true);
                getReturnQtyBinding().setDisabled(true);
                getConsumedQtyBinding().setDisabled(true);
                getIssuedQty1Binding().setDisabled(true);
                getConsQtyBinding().setDisabled(true);
                getBalanceQtyBinding().setDisabled(true);
                getUnitCodeBinding().setDisabled(true);
                getPartCodeBinding().setDisabled(true);
                getJobCardDateBinding().setDisabled(true);
                getCurrentStocksBinding().setDisabled(true);
//                getCurrentStockBinding().setDisabled(true);
                pop_Flag="Y";
            } else if (mode.equals("C")) {
                getHeaderEditBinding().setDisabled(true);
                getUnitCodeBinding().setDisabled(true);
                getJobCardNoBinding().setDisabled(false);
                getJobCardDateBinding().setDisabled(false);
                getProcessCodeBinding().setDisabled(true);
                getSeqNoBinding().setDisabled(true);
                getRequiredQuantityBinding().setDisabled(true);
                getItemCdBinding().setDisabled(true);
                getItemTypeBinding().setDisabled(true);
                getRequiredQtyDetailBinding().setDisabled(true);
                getAlternateItemBinding().setDisabled(true);
                getRequiredQtyDetailBinding().setDisabled(true);
                getPartDescBinding().setDisabled(true);
                getJobCardNoBinding().setDisabled(true); 
                getReqQty1Binding().setDisabled(true);
                getRmTypeBinding().setDisabled(true);
                getUomBinding().setDisabled(true);
                getProducedQtyBinding().setDisabled(true);
                getIssuedQtyBinding().setDisabled(true);
                getReturnQtyBinding().setDisabled(true);
                getConsumedQtyBinding().setDisabled(true);
                getIssuedQty1Binding().setDisabled(true);
                getConsQtyBinding().setDisabled(true);
                getBalanceQtyBinding().setDisabled(true);
                getCurrentStocksBinding().setDisabled(true);
//                getCurrentStockBinding().setDisabled(true);
            } else if (mode.equals("V")) {
                getAltPpJcTabBinding().setDisabled(false);
                getLotRmReqTabBinding().setDisabled(false);
                processDtlTabBinding.setDisabled(false);
            }
            
        }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void saveDataAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("generateJobCardNo").execute();
        AdfFacesContext.getCurrentInstance().addPartialTarget(panelCollectionBinding);
        ADFUtils.setLastUpdatedBy("ProcessJobCardHeaderVO1Iterator","LastUpdatedBy");
        if(!monthBinding.isDisabled())
        { 
            ADFUtils.findOperation("Commit").execute();
            FacesMessage Message = new FacesMessage("Record Saved Successfully.New Job Card No. is "+jobCardNoBinding.getValue()+"."); 
            Message.setSeverity(FacesMessage.SEVERITY_INFO); 
            FacesContext fc = FacesContext.getCurrentInstance(); 
            fc.addMessage(null, Message); 
        }
        else
        {
            ADFUtils.findOperation("Commit").execute();
            FacesMessage Message = new FacesMessage("Record Updated Successfully."); 
            Message.setSeverity(FacesMessage.SEVERITY_INFO); 
            FacesContext fc = FacesContext.getCurrentInstance(); 
            fc.addMessage(null, Message); 
        }
    }

    public void populateDetailDataAL(ActionEvent actionEvent) {
        AdfFacesContext.getCurrentInstance().addPartialTarget(saveButtonBinding);
        ADFUtils.findOperation("populateData").execute();
//        ADFUtils.findOperation("currentStockForProcessJobCard").execute();
        pop_Flag="Y";
        AdfFacesContext.getCurrentInstance().addPartialTarget(panelCollectionBinding);
        
    }

    public void setPartDescBinding(RichInputText partDescBinding) {
        this.partDescBinding = partDescBinding;
    }

    public RichInputText getPartDescBinding() {
        return partDescBinding;
    }

    public void setReqQty1Binding(RichInputText reqQty1Binding) {
        this.reqQty1Binding = reqQty1Binding;
    }

    public RichInputText getReqQty1Binding() {
        return reqQty1Binding;
    }

    public void setRmTypeBinding(RichInputText rmTypeBinding) {
        this.rmTypeBinding = rmTypeBinding;
    }

    public RichInputText getRmTypeBinding() {
        return rmTypeBinding;
    }

    public void setLotRmReqTabBinding(RichShowDetailItem lotRmReqTabBinding) {
        this.lotRmReqTabBinding = lotRmReqTabBinding;
    }

    public RichShowDetailItem getLotRmReqTabBinding() {
        return lotRmReqTabBinding;
    }

    public void setAltPpJcTabBinding(RichShowDetailItem altPpJcTabBinding) {
        this.altPpJcTabBinding = altPpJcTabBinding;
    }

    public RichShowDetailItem getAltPpJcTabBinding() {
        return altPpJcTabBinding;
    }

    public void yearValidator(FacesContext facesContext, UIComponent uIComponent, Object ob) {
       Integer year=(Integer)ob;
       Integer year1= (Integer)Calendar.getInstance().get(Calendar.YEAR);
       System.out.println("CURRENT YEAR"+year);
        System.out.println("CURRENT YEAR"+year1);
       if(year.compareTo(year1)==-1)
       {
           throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Year cannot be less than Current Year", null)); 
       }
    }

    public void monthValidator(FacesContext facesContext, UIComponent uIComponent, Object ob) {
        if(ob!=null && yearBinding.getValue()!=null)
        {
        Integer MonthCase=0;        
        if(ob.toString().equals("JAN"))
        {
            MonthCase=1;
        }
        else if(ob.toString().equals("FEB"))
        {
        MonthCase=2;    
        }
        else if(ob.toString().equals("MAR"))
        {
            MonthCase=3;
        }
        else if(ob.toString().equals("APR"))
        {
            MonthCase=4;
        }
        else if(ob.toString().equals("MAY"))
        {
        MonthCase=5;    
        }
        else if(ob.toString().equals("JUN"))
        {
            MonthCase=6;
        }
        else if(ob.toString().equals("JUL"))
        {
         MonthCase=7;   
         }
        else if(ob.toString().equals("AUG"))
        {
            MonthCase=8;
        }
        else if(ob.toString().equals("SEP"))
        {
            MonthCase=9;
        }
        else if(ob.toString().equals("OCT"))
        {
            MonthCase=10;
        }
        else if(ob.toString().equals("NOV"))
        {
        MonthCase=11;    
        }
        else if(ob.toString().equals("DEC"))
        {
        MonthCase=12;    
        }
        else
        {
        System.out.println("No Month Found");    
        }
        Integer j=1;
        j=j+(Integer)Calendar.getInstance().get(Calendar.MONTH);
        System.out.println("CURRENT MONTH"+j +"GIVEN MONTH"+MonthCase);
        if(MonthCase.compareTo(j)==-1)
        {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Month cannot be less than Current Month", null)); 
        }
        }
    }

    public void setUomBinding(RichInputText uomBinding) {
        this.uomBinding = uomBinding;
    }

    public RichInputText getUomBinding() {
        return uomBinding;
    }

    public void setProducedQtyBinding(RichInputText producedQtyBinding) {
        this.producedQtyBinding = producedQtyBinding;
    }

    public RichInputText getProducedQtyBinding() {
        return producedQtyBinding;
    }

    public void setIssuedQtyBinding(RichInputText issuedQtyBinding) {
        this.issuedQtyBinding = issuedQtyBinding;
    }

    public RichInputText getIssuedQtyBinding() {
        return issuedQtyBinding;
    }

    public void setReturnQtyBinding(RichInputText returnQtyBinding) {
        this.returnQtyBinding = returnQtyBinding;
    }

    public RichInputText getReturnQtyBinding() {
        return returnQtyBinding;
    }

    public void setConsumedQtyBinding(RichInputText consumedQtyBinding) {
        this.consumedQtyBinding = consumedQtyBinding;
    }

    public RichInputText getConsumedQtyBinding() {
        return consumedQtyBinding;
    }

    public void setIssuedQty1Binding(RichInputText issuedQty1Binding) {
        this.issuedQty1Binding = issuedQty1Binding;
    }

    public RichInputText getIssuedQty1Binding() {
        return issuedQty1Binding;
    }

    public void setConsQtyBinding(RichInputText consQtyBinding) {
        this.consQtyBinding = consQtyBinding;
    }

    public RichInputText getConsQtyBinding() {
        return consQtyBinding;
    }

    public void setBalanceQtyBinding(RichInputText balanceQtyBinding) {
        this.balanceQtyBinding = balanceQtyBinding;
    }

    public RichInputText getBalanceQtyBinding() {
        return balanceQtyBinding;
    }

    public void setPanelCollectionBinding(RichPanelCollection panelCollectionBinding) {
        this.panelCollectionBinding = panelCollectionBinding;
    }

    public RichPanelCollection getPanelCollectionBinding() {
        return panelCollectionBinding;
    }

    public void setSaveButtonBinding(RichButton saveButtonBinding) {
        this.saveButtonBinding = saveButtonBinding;
    }

    public RichButton getSaveButtonBinding() {
        return saveButtonBinding;
    }

    public void setPartCodeBinding(RichInputComboboxListOfValues partCodeBinding) {
        this.partCodeBinding = partCodeBinding;
    }

    public RichInputComboboxListOfValues getPartCodeBinding() {
        return partCodeBinding;
    }

    public void partCodeVCL(ValueChangeEvent vcl) {
        if(vcl.getNewValue()!=null)
        {
            getPartCodeBinding().setDisabled(true);
        }
    }

    public void quantityRefreshValueVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce != null) 
        {
            if(vce.getNewValue() != null && pop_Flag.equals("Y")) 
            {
                ADFUtils.findOperation("populateData").execute();
            }
        }
    }

    public void setCurrentStockBinding(RichInputText currentStockBinding) {
        this.currentStockBinding = currentStockBinding;
    }

    public RichInputText getCurrentStockBinding() {
        return currentStockBinding;
    }

    public void setClosStockBinding(RichInputText closStockBinding) {
        this.closStockBinding = closStockBinding;
    }

    public RichInputText getClosStockBinding() {
        return closStockBinding;
    }

    public void setCurrentStocksBinding(RichInputText currentStocksBinding) {
        this.currentStocksBinding = currentStocksBinding;
    }

    public RichInputText getCurrentStocksBinding() {
        return currentStocksBinding;
    }

    public void setProcessDtlTabBinding(RichShowDetailItem processDtlTabBinding) {
        this.processDtlTabBinding = processDtlTabBinding;
    }

    public RichShowDetailItem getProcessDtlTabBinding() {
        return processDtlTabBinding;
    }
}
