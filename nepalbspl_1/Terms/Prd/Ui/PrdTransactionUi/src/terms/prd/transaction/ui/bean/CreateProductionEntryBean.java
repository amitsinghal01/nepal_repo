package terms.prd.transaction.ui.bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;
import javax.faces.validator.ValidatorException;

import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.nav.RichCommandLink;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.adf.view.rich.util.ResetUtils;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

import org.apache.myfaces.trinidad.model.UploadedFile;

public class CreateProductionEntryBean {
    private RichTable createProductionEntryDetailTableBinding;
    private RichTable createProductionEntryDetailOperatorTableBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputDate entryDateBinding;
    private RichInputDate planDateBinding;
    private RichInputText entryNoBinding;
    private RichButton headerEditBinding;
    private RichButton headerSaveBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichOutputText outputTextBinding;
    private RichInputText jobCardNoBinding;
    private RichInputComboboxListOfValues productCodeBinding;
    private RichInputText fgStockQtyBinding;
    private RichInputText planQtyBinding;
    private RichInputText stdTimePerUnitBinding;
    private RichInputText timeTakenBinding;
    private RichInputText totalProductionQtyBinding;
    private RichInputText actualProductionQtyBinding;
    private RichInputText acceptQtyBinding;
    private RichInputText rejectQtyBinding;
    private RichInputText actualTimeTakenPerUnitBinding;
    private RichInputText remarksBinding;
    private RichInputText enteredByBinding;
    private RichInputText rejReasonBinding;
    private RichInputText correctiveActionBinding;
    private RichInputText preventiveActionBinding;
    private RichInputText otherRemarksBinding;
    private RichInputComboboxListOfValues shiftCodeBinding;
    private RichInputComboboxListOfValues employeeCodeBinding;
    private RichButton detailCreateBinding;
    private String msg="C";
    private RichButton populateButtonBinding;
    private RichButton detailCreateTableBinding;
    private RichInputText batchNoBinding;
    private RichInputText acceptBinding;
    private RichSelectOneChoice custTypeBinding;
    private RichInputComboboxListOfValues custCodeBinding;
    private RichInputComboboxListOfValues custTypeBinding1;
    private RichInputText pathBinding;
    private RichTable referenceDocumentTableBinding;
    private RichInputText docSlNoBinding;
    private RichInputText unitCdRefDocumentBinding;
    private RichInputText refDocNoBinding;
    private RichInputText refDocTypeBinding;
    private RichInputText docFileNameBinding;
    private RichInputDate refDocDateBinding;
    private RichInputText remarksRefDocumentBinding;
    private RichCommandLink downloadLinkBinding;
    private RichInputComboboxListOfValues approvedByBinding;
    private RichInputDate approvedDateBinding;
    private RichSelectOneChoice productionTypeBinding;
    private RichShowDetailItem requisitionForInspectionDetailsTabBinding;
    private RichShowDetailItem referenceDocumentTabBinding;

    public CreateProductionEntryBean() {
    }

    public void deletePopupDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                    {
                    ADFUtils.findOperation("Delete").execute();
                    //ADFUtils.findOperation("Commit").execute();    
                    System.out.println("Record Delete Successfully");
                   
                        FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                        FacesContext fc = FacesContext.getCurrentInstance();  
                        fc.addMessage(null, Message);  
                   
                    }

                AdfFacesContext.getCurrentInstance().addPartialTarget(createProductionEntryDetailTableBinding);
        }

    public void setCreateProductionEntryDetailTableBinding(RichTable createProductionEntryDetailTableBinding) {
        this.createProductionEntryDetailTableBinding = createProductionEntryDetailTableBinding;
    }

    public RichTable getCreateProductionEntryDetailTableBinding() {
        return createProductionEntryDetailTableBinding;
    }

    public void setCreateProductionEntryDetailOperatorTableBinding(RichTable createProductionEntryDetailOperatorTableBinding) {
        this.createProductionEntryDetailOperatorTableBinding = createProductionEntryDetailOperatorTableBinding;
    }

    public RichTable getCreateProductionEntryDetailOperatorTableBinding() {
        return createProductionEntryDetailOperatorTableBinding;
    }

    public void deletePopupDL1(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                    {
                    ADFUtils.findOperation("Delete1").execute();
                    //ADFUtils.findOperation("Commit").execute();    
                    System.out.println("Record Delete Successfully");
                   
                        FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                        FacesContext fc = FacesContext.getCurrentInstance();  
                        fc.addMessage(null, Message);  
                   
                    }

                AdfFacesContext.getCurrentInstance().addPartialTarget(createProductionEntryDetailOperatorTableBinding);
         }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setEntryDateBinding(RichInputDate entryDateBinding) {
        this.entryDateBinding = entryDateBinding;
    }

    public RichInputDate getEntryDateBinding() {
        return entryDateBinding;
    }

    public void setPlanDateBinding(RichInputDate planDateBinding) {
        this.planDateBinding = planDateBinding;
    }

    public RichInputDate getPlanDateBinding() {
        return planDateBinding;
    }

    public void setEntryNoBinding(RichInputText entryNoBinding) {
        this.entryNoBinding = entryNoBinding;
    }

    public RichInputText getEntryNoBinding() {
        return entryNoBinding;
    }

    public void saveProductionEntryAL(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedBy("ProductionEntryHeaderVO1Iterator","LastUpdatedBy");
//        System.out.println("ACCEPT QTY" + acceptQtyBinding.getValue());
        
//      if ((Long) ADFUtils.evaluateEL("#{bindings.ProductionEntryDetailVO1Iterator.estimatedRowCount}") > 0)
//      {
//          
//        OperationBinding op = ADFUtils.findOperation("populateDataProductionEntry");
//        Object obj = op.execute();
//        
//        System.out.println("RESULT IS************"+op.getResult());
//        
//        if(op.getResult().equals("Y"))
//        {
//             
//             if(!populateButtonBinding.isDisabled())
//             {
//                 OperationBinding op1 = ADFUtils.findOperation("generateEntryNoForProductionEntry");
//              op1.execute();
//            ADFUtils.findOperation("Commit").execute();
//            FacesMessage Message = new FacesMessage("Record Saved Successfully. New Entry Number is "+entryNoBinding.getValue()+"."); 
//            Message.setSeverity(FacesMessage.SEVERITY_INFO); 
//            FacesContext fc = FacesContext.getCurrentInstance(); 
//            fc.addMessage(null, Message);
//           }
//            else
//            {
//                    ADFUtils.findOperation("Commit").execute();
//                    FacesMessage Message = new FacesMessage("Record Updated Successfully."); 
//                    Message.setSeverity(FacesMessage.SEVERITY_INFO); 
//                    FacesContext fc = FacesContext.getCurrentInstance(); 
//                    fc.addMessage(null, Message); 
//            }
//        }
//        else
//        {
//            ADFUtils.showMessage("Plan Qty is Required", 0);
//        }
//      }
//      else{
//          ADFUtils.showMessage("Please Enter any record in the Detail table.", 0);
//      }
        if ((Long) ADFUtils.evaluateEL("#{bindings.ProductionEntryDetailVO1Iterator.estimatedRowCount}")>0)
        {              
            OperationBinding op = ADFUtils.findOperation("generateEntryNoForProductionEntry");
            op.execute();
            System.out.println("Result is===> "+op.getResult());
            if(op.getResult() != null && op.getResult().toString().equalsIgnoreCase("E"))
            {
                ADFUtils.findOperation("Commit").execute();
                ADFUtils.showMessage(" Record Updated Successfully", 2);
            }
            else if (op.getResult() != null && op.getResult().toString() != ""  && op.getResult().equals("S"))
            {
                System.out.println("+++++ in the save mode+++++++++++");
//                ADFUtils.findOperation("getQcdNoForProductionEntry").execute();
                ADFUtils.findOperation("Commit").execute();
                ADFUtils.showMessage("Record Saved Successfully.New Entry No.is "+ADFUtils.evaluateEL("#{bindings.EntryNo.inputValue}"), 2);
            }
            else if(op.getResult() != null && op.getResult().toString().equalsIgnoreCase("N"))
            {
                ADFUtils.showMessage("Entry No. could not be generated. Try Again !!", 0);
            }
            
//            ADFUtils.findOperation("getQcdNoForProductionEntry").execute();
        }
        else{
            ADFUtils.showMessage("Please Enter any record in the Detail table.", 0);
        }
  }


    public void editButtonAL(ActionEvent actionEvent) {
        if(approvedByBinding.getValue() != null) {
            System.out.println("INSIDE approvedByBinding #### ");
            ADFUtils.setEL("#{pageFlowScope.mode}","V");
            ADFUtils.showMessage("Approved Requisition For Inspection cannot be modified.", 0);
        }
        else {
            cevmodecheck();
        }
        
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setHeaderSaveBinding(RichButton headerSaveBinding) {
        this.headerSaveBinding = headerSaveBinding;
    }

    public RichButton getHeaderSaveBinding() {
        return headerSaveBinding;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setOutputTextBinding(RichOutputText outputTextBinding) {
        this.outputTextBinding = outputTextBinding;
    }

    public RichOutputText getOutputTextBinding() {
        cevmodecheck();
        return outputTextBinding;
    }
    
    private void cevmodecheck(){
            if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
                cevModeDisableComponent("V");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("C");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("E");
            }
        }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                try {
                    Method method1 =
                        component.getClass().getMethod("setDisabled", boolean.class);
                    if (method1 != null) {
                        method1.invoke(component, valueComponent);
                    }
                } catch (NoSuchMethodException e) {
                    try {
                        Method method =
                            component.getClass().getMethod("setReadOnly", boolean.class);
                        if (method != null) {
                            method.invoke(component, valueComponent);
                        }
                    } catch (Exception e1) {
                        // e.printStackTrace();//silently eat this exception.
                    }


                } catch (Exception e) {
                    // e.printStackTrace();//silently eat this exception.
                }
                List<UIComponent> childComponents = component.getChildren();
                for (UIComponent comp : childComponents) {
                    makeComponentHierarchyReadOnly(comp, valueComponent);
                }
            }
    
    public void cevModeDisableComponent(String mode) {

        if (mode.equals("E")) {
            msg="E";
            getUnitCodeBinding().setDisabled(true);
            getEntryDateBinding().setDisabled(true);
            getEntryNoBinding().setDisabled(true);
            getPlanDateBinding().setDisabled(true);
            getRejectQtyBinding().setDisabled(true);
            getProductCodeBinding().setDisabled(true);
            getFgStockQtyBinding().setDisabled(true);
            getAcceptQtyBinding().setDisabled(true);
            getRejectQtyBinding().setDisabled(true);
            getBatchNoBinding().setDisabled(true);
            getDocSlNoBinding().setDisabled(true);
            getUnitCdRefDocumentBinding().setDisabled(true);
            getRefDocNoBinding().setDisabled(true);
            getRefDocTypeBinding().setDisabled(true);
            getDocFileNameBinding().setDisabled(true);
            getRefDocDateBinding().setDisabled(true);
//            getPopulateButtonBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            
        } else if (mode.equals("C")) {
            msg="C";
            getUnitCodeBinding().setDisabled(true);
            getEntryDateBinding().setDisabled(true);
            getEntryNoBinding().setDisabled(true);
//            getRejectQtyBinding().setDisabled(true);
            getFgStockQtyBinding().setDisabled(true);
//            getAcceptBinding().setDisabled(true);
            getAcceptQtyBinding().setDisabled(true);
            getRejectQtyBinding().setDisabled(true);
            getDocSlNoBinding().setDisabled(true);
            getUnitCdRefDocumentBinding().setDisabled(true);
            getRefDocNoBinding().setDisabled(true);
            getRefDocTypeBinding().setDisabled(true);
            getDocFileNameBinding().setDisabled(true);
            getRefDocDateBinding().setDisabled(true);
            getApprovedByBinding().setDisabled(true);
            getApprovedDateBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            
            
        } else if (mode.equals("V")) {
//            getDetailCreateTableBinding().setDisabled(true);
            getRequisitionForInspectionDetailsTabBinding().setDisabled(false);
            getReferenceDocumentTabBinding().setDisabled(false);
            downloadLinkBinding.setDisabled(false);   
        }
}

    public void setJobCardNoBinding(RichInputText jobCardNoBinding) {
        this.jobCardNoBinding = jobCardNoBinding;
    }

    public RichInputText getJobCardNoBinding() {
        return jobCardNoBinding;
    }

    public void setProductCodeBinding(RichInputComboboxListOfValues productCodeBinding) {
        this.productCodeBinding = productCodeBinding;
    }

    public RichInputComboboxListOfValues getProductCodeBinding() {
        return productCodeBinding;
    }

    public void setFgStockQtyBinding(RichInputText fgStockQtyBinding) {
        this.fgStockQtyBinding = fgStockQtyBinding;
    }

    public RichInputText getFgStockQtyBinding() {
        return fgStockQtyBinding;
    }

    public void setPlanQtyBinding(RichInputText planQtyBinding) {
        this.planQtyBinding = planQtyBinding;
    }

    public RichInputText getPlanQtyBinding() {
        return planQtyBinding;
    }

    public void setStdTimePerUnitBinding(RichInputText stdTimePerUnitBinding) {
        this.stdTimePerUnitBinding = stdTimePerUnitBinding;
    }

    public RichInputText getStdTimePerUnitBinding() {
        return stdTimePerUnitBinding;
    }

    public void setTimeTakenBinding(RichInputText timeTakenBinding) {
        this.timeTakenBinding = timeTakenBinding;
    }

    public RichInputText getTimeTakenBinding() {
        return timeTakenBinding;
    }

    public void setTotalProductionQtyBinding(RichInputText totalProductionQtyBinding) {
        this.totalProductionQtyBinding = totalProductionQtyBinding;
    }

    public RichInputText getTotalProductionQtyBinding() {
        return totalProductionQtyBinding;
    }

    public void setActualProductionQtyBinding(RichInputText actualProductionQtyBinding) {
        this.actualProductionQtyBinding = actualProductionQtyBinding;
    }

    public RichInputText getActualProductionQtyBinding() {
        return actualProductionQtyBinding;
    }

    public void setAcceptQtyBinding(RichInputText acceptQtyBinding) {
        this.acceptQtyBinding = acceptQtyBinding;
    }

    public RichInputText getAcceptQtyBinding() {
        return acceptQtyBinding;
    }

    public void setRejectQtyBinding(RichInputText rejectQtyBinding) {
        this.rejectQtyBinding = rejectQtyBinding;
    }

    public RichInputText getRejectQtyBinding() {
        return rejectQtyBinding;
    }

    public void setActualTimeTakenPerUnitBinding(RichInputText actualTimeTakenPerUnitBinding) {
        this.actualTimeTakenPerUnitBinding = actualTimeTakenPerUnitBinding;
    }

    public RichInputText getActualTimeTakenPerUnitBinding() {
        return actualTimeTakenPerUnitBinding;
    }

    public void setRemarksBinding(RichInputText remarksBinding) {
        this.remarksBinding = remarksBinding;
    }

    public RichInputText getRemarksBinding() {
        return remarksBinding;
    }

    public void setEnteredByBinding(RichInputText enteredByBinding) {
        this.enteredByBinding = enteredByBinding;
    }

    public RichInputText getEnteredByBinding() {
        return enteredByBinding;
    }

    public void setRejReasonBinding(RichInputText rejReasonBinding) {
        this.rejReasonBinding = rejReasonBinding;
    }

    public RichInputText getRejReasonBinding() {
        return rejReasonBinding;
    }

    public void setCorrectiveActionBinding(RichInputText correctiveActionBinding) {
        this.correctiveActionBinding = correctiveActionBinding;
    }

    public RichInputText getCorrectiveActionBinding() {
        return correctiveActionBinding;
    }

    public void setPreventiveActionBinding(RichInputText preventiveActionBinding) {
        this.preventiveActionBinding = preventiveActionBinding;
    }

    public RichInputText getPreventiveActionBinding() {
        return preventiveActionBinding;
    }

    public void setOtherRemarksBinding(RichInputText otherRemarksBinding) {
        this.otherRemarksBinding = otherRemarksBinding;
    }

    public RichInputText getOtherRemarksBinding() {
        return otherRemarksBinding;
    }

    public void setShiftCodeBinding(RichInputComboboxListOfValues shiftCodeBinding) {
        this.shiftCodeBinding = shiftCodeBinding;
    }

    public RichInputComboboxListOfValues getShiftCodeBinding() {
        return shiftCodeBinding;
    }

    public void setEmployeeCodeBinding(RichInputComboboxListOfValues employeeCodeBinding) {
        this.employeeCodeBinding = employeeCodeBinding;
    }

    public RichInputComboboxListOfValues getEmployeeCodeBinding() {
        return employeeCodeBinding;
    }

//    public void acceptQtyValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
//        BigDecimal obj = (BigDecimal) object;
//        
//        BigDecimal acceptQty = new BigDecimal(0);
//        if(acceptQtyBinding.getValue() == null) 
//        {
//            setAcceptQtyBinding(acceptQty) = acceptQty;
//        }
//        if(acceptQtyBinding.getValue() != null && obj.compareTo(actualQty)==1)
//        {
//            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Accept Qty cannot be Greater than Actual Prodn Qty.", null));
//        }
//    }

//    public void totalProductionQtyValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
//        BigDecimal obj = (BigDecimal)object;
//        BigDecimal planQty = (BigDecimal)getPlanQtyBinding().getValue();
//        BigDecimal actQty = (BigDecimal)actualProductionQtyBinding.getValue();
//        
//        if(planQtyBinding.getValue() != null) 
//        {
//           planQty = (BigDecimal)getPlanQtyBinding().getValue();   
//        }
//        if(totalProductionQtyBinding.getValue() != null && obj.compareTo(planQty) == 1) {
//            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Total Production Qty + Actual Production Qty cannot be Greater than Required Plan Qty.", null));
//        }
//    }

    public void getPopulateDataAL(ActionEvent actionEvent) {
         ADFUtils.findOperation("getProductionEntryPopulateData").execute();
         AdfFacesContext.getCurrentInstance().addPartialTarget(createProductionEntryDetailTableBinding);
    }

    public void productCodeVCL(ValueChangeEvent valueChangeEvent) {
         ADFUtils.findOperation("getFgStockQtyForProductionEntry").execute();
    }

//    public void createDetailOperatorAL(ActionEvent actionEvent) {
//        ADFUtils.findOperation("CreateInsert").execute();
//        ADFUtils.findOperation("setShiftCdForProductionEntry").execute();
//    }

    public void shiftCodeTransVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        System.out.println("Inside VCE #######");
        if(vce!=null) 
        {
            ADFUtils.findOperation("getUniqueShiftCodeDtlForProductionEntry").execute();
        }
    }

    public void setPopulateButtonBinding(RichButton populateButtonBinding) {
        this.populateButtonBinding = populateButtonBinding;
    }

    public RichButton getPopulateButtonBinding() {
        return populateButtonBinding;
    }

    public void setDetailCreateTableBinding(RichButton detailCreateTableBinding) {
        this.detailCreateTableBinding = detailCreateTableBinding;
    }

    public RichButton getDetailCreateTableBinding() {
        return detailCreateTableBinding;
    }

//    public void actualProdQtyValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
//        BigDecimal obj = (BigDecimal)object;
//        
//        BigDecimal planQty = (BigDecimal)planQtyBinding.getValue();
//        
//        if(planQtyBinding.getValue() != null) {
//            planQty = (BigDecimal)planQtyBinding.getValue();
//        }
//        
//        if(actualProductionQtyBinding.getValue() != null && obj.compareTo(planQty) == 1) {
//            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Production Qty cannot be Greater than Actual Plan Qty.", null));
//        }
//      }

    public void createDetailAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert").execute();
        ADFUtils.findOperation("setProductionEntryNo").execute();
    }


    public String saveAndCloseProdnEntryAL() 
    {        
        if ((Long) ADFUtils.evaluateEL("#{bindings.ProductionEntryDetailVO1Iterator.estimatedRowCount}")>0)
        {              
            OperationBinding op = ADFUtils.findOperation("generateEntryNoForProductionEntry");
            op.execute();
            System.out.println("Result is===> "+op.getResult());
            if(op.getResult() != null && op.getResult().toString().equalsIgnoreCase("E"))
            {
                ADFUtils.findOperation("Commit").execute();
                ADFUtils.showMessage(" Record Updated Successfully", 2);
                return "saveandclose";
            }
            else if (op.getResult() != null && op.getResult().toString() != ""  && op.getResult().equals("S"))
            {
                System.out.println("+++++ in the save mode+++++++++++");
                ADFUtils.findOperation("getQcdNoForProductionEntry").execute();
                ADFUtils.findOperation("Commit").execute();
                ADFUtils.showMessage("Record Saved Successfully.New Entry No.is "+ADFUtils.evaluateEL("#{bindings.EntryNo.inputValue}"), 2);
                return "saveandclose";
            }
            else if(op.getResult() != null && op.getResult().toString().equalsIgnoreCase("N"))
            {
                ADFUtils.showMessage("Entry No. could not be generated. Try Again !!", 0);
                return null;
            }
        }
        else{
            ADFUtils.showMessage("Please Enter any record in the Detail table.", 0);
            return null;
        }
        return null;
    }

    public void setBatchNoBinding(RichInputText batchNoBinding) {
        this.batchNoBinding = batchNoBinding;
    }

    public RichInputText getBatchNoBinding() {
        return batchNoBinding;
    }

    public void setAcceptBinding(RichInputText acceptBinding) {
        this.acceptBinding = acceptBinding;
    }

    public RichInputText getAcceptBinding() {
        return acceptBinding;
    }

    public void acceptQtyValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        System.out.println("Inside VALIDATOR ####");
        if(object != null) {
            System.out.println("Accept Value ####" + object);
            acceptBinding.setDisabled(true);
        }

    }

    public void custTypeVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        
        System.out.println("INSIDE custTypeVCL #### ");
        if(vce != null) {
            System.out.println("Customer Type Binding Value ####" + custTypeBinding1.getValue());
            
//            String custType = "General";
            
            if(vce.getNewValue().equals("General")) {
                System.out.println("INSIDE IF BLOCK #### ");
                custCodeBinding.setDisabled(true);
//                custCodeBinding.setValue(custType);
            }
            else {
                System.out.println("INSIDE ELSE BLOCK #### ");
                custCodeBinding.setDisabled(false);
            }
        }
    }

    public void setCustTypeBinding(RichSelectOneChoice custTypeBinding) {
        this.custTypeBinding = custTypeBinding;
    }

    public RichSelectOneChoice getCustTypeBinding() {
        return custTypeBinding;
    }

    public void setCustCodeBinding(RichInputComboboxListOfValues custCodeBinding) {
        this.custCodeBinding = custCodeBinding;
    }

    public RichInputComboboxListOfValues getCustCodeBinding() {
        return custCodeBinding;
    }

    public void batchNoVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce != null)
        {
           ADFUtils.findOperation("getQcdNoForProductionEntry").execute();
        }
    }

    public void setCustTypeBinding1(RichInputComboboxListOfValues custTypeBinding1) {
        this.custTypeBinding1 = custTypeBinding1;
    }

    public RichInputComboboxListOfValues getCustTypeBinding1() {
        return custTypeBinding1;
    }

    public void uploadAction(ValueChangeEvent vce) {
        System.out.println("inside uploadAction #### ");
                    //Get File Object from VC Event
                    UploadedFile fileVal = (UploadedFile) vce.getNewValue();
                    //Upload File to path- Return actual server path
                    String path = ADFUtils.uploadFile(fileVal);
                      System.out.println(fileVal.getContentType());
//                      System.out.println("Content Type==>>"+fileVal.getContentType()+"Path==>>"+path+"File name==>>"+fileVal.getFilename());
                    //Method to insert data in table to keep track of uploaded files
                    OperationBinding ob = ADFUtils.findOperation("setFileDataForRequisitionForInspection");
                    ob.getParamsMap().put("name", fileVal.getFilename());
                    ob.getParamsMap().put("path", path);
                    ob.getParamsMap().put("contTyp", fileVal.getContentType());
                    ob.getParamsMap().put("SrNo", ADFUtils.evaluateEL("#{bindings.DocAttachRefDtlVO3Iterator.estimatedRowCount+1}"));
                    ob.execute();
                    // Reset inputFile component after upload
                    ResetUtils.reset(vce.getComponent());
    }
    
    public void downloadAction(FacesContext facesContext, OutputStream outputStream) throws Exception {
        
        File filed = new File(pathBinding.getValue().toString());
                               FileInputStream fis;
                               byte[] b;
                               try {
                                   fis = new FileInputStream(filed);

                                   int n;
                                   while ((n = fis.available()) > 0) {
                                       b = new byte[n];
                                       int result = fis.read(b);
                                       outputStream.write(b, 0, b.length);
                                       if (result == -1)
                                           break;
                                   }
                               } catch (Exception e) {
                                   e.printStackTrace();
                               }
                               outputStream.flush();
        
    }

    public void setPathBinding(RichInputText pathBinding) {
        this.pathBinding = pathBinding;
    }

    public RichInputText getPathBinding() {
        return pathBinding;
    }

    public void deletePopupDLReferenceDocument(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                    {
                    ADFUtils.findOperation("Delete1").execute();
                    //ADFUtils.findOperation("Commit").execute();    //Unable on Search page.
                    System.out.println("Record Delete Successfully");
                   
                        FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                        FacesContext fc = FacesContext.getCurrentInstance();  
                        fc.addMessage(null, Message);  
                   
                    }

              AdfFacesContext.getCurrentInstance().addPartialTarget(referenceDocumentTableBinding);
    }

    public void setReferenceDocumentTableBinding(RichTable referenceDocumentTableBinding) {
        this.referenceDocumentTableBinding = referenceDocumentTableBinding;
    }

    public RichTable getReferenceDocumentTableBinding() {
        return referenceDocumentTableBinding;
    }

    public void setDocSlNoBinding(RichInputText docSlNoBinding) {
        this.docSlNoBinding = docSlNoBinding;
    }

    public RichInputText getDocSlNoBinding() {
        return docSlNoBinding;
    }

    public void setUnitCdRefDocumentBinding(RichInputText unitCdRefDocumentBinding) {
        this.unitCdRefDocumentBinding = unitCdRefDocumentBinding;
    }

    public RichInputText getUnitCdRefDocumentBinding() {
        return unitCdRefDocumentBinding;
    }

    public void setRefDocNoBinding(RichInputText refDocNoBinding) {
        this.refDocNoBinding = refDocNoBinding;
    }

    public RichInputText getRefDocNoBinding() {
        return refDocNoBinding;
    }

    public void setRefDocTypeBinding(RichInputText refDocTypeBinding) {
        this.refDocTypeBinding = refDocTypeBinding;
    }

    public RichInputText getRefDocTypeBinding() {
        return refDocTypeBinding;
    }

    public void setDocFileNameBinding(RichInputText docFileNameBinding) {
        this.docFileNameBinding = docFileNameBinding;
    }

    public RichInputText getDocFileNameBinding() {
        return docFileNameBinding;
    }

    public void setRefDocDateBinding(RichInputDate refDocDateBinding) {
        this.refDocDateBinding = refDocDateBinding;
    }

    public RichInputDate getRefDocDateBinding() {
        return refDocDateBinding;
    }

    public void setRemarksRefDocumentBinding(RichInputText remarksRefDocumentBinding) {
        this.remarksRefDocumentBinding = remarksRefDocumentBinding;
    }

    public RichInputText getRemarksRefDocumentBinding() {
        return remarksRefDocumentBinding;
    }

    public void setDownloadLinkBinding(RichCommandLink downloadLinkBinding) {
        this.downloadLinkBinding = downloadLinkBinding;
    }

    public RichCommandLink getDownloadLinkBinding() {
        return downloadLinkBinding;
    }

    public void setApprovedByBinding(RichInputComboboxListOfValues approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputComboboxListOfValues getApprovedByBinding() {
        return approvedByBinding;
    }

    public void setApprovedDateBinding(RichInputDate approvedDateBinding) {
        this.approvedDateBinding = approvedDateBinding;
    }

    public RichInputDate getApprovedDateBinding() {
        return approvedDateBinding;
    }

    public void setProductionTypeBinding(RichSelectOneChoice productionTypeBinding) {
        this.productionTypeBinding = productionTypeBinding;
    }

    public RichSelectOneChoice getProductionTypeBinding() {
        return productionTypeBinding;
    }

    public void setRequisitionForInspectionDetailsTabBinding(RichShowDetailItem requisitionForInspectionDetailsTabBinding) {
        this.requisitionForInspectionDetailsTabBinding = requisitionForInspectionDetailsTabBinding;
    }

    public RichShowDetailItem getRequisitionForInspectionDetailsTabBinding() {
        return requisitionForInspectionDetailsTabBinding;
    }

    public void setReferenceDocumentTabBinding(RichShowDetailItem referenceDocumentTabBinding) {
        this.referenceDocumentTabBinding = referenceDocumentTabBinding;
    }

    public RichShowDetailItem getReferenceDocumentTabBinding() {
        return referenceDocumentTabBinding;
    }

    public void approvedByVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce.getNewValue()!= null) {
            OperationBinding ob1 = ADFUtils.findOperation("checkApprovalFunctionStatus");
            ob1.getParamsMap().put("formNm", "REQ_INSP");
            ob1.getParamsMap().put("authoLim", "AP");
            ob1.getParamsMap().put("UnitCd", unitCodeBinding.getValue());
            ob1.getParamsMap().put("empcd", vce.getNewValue().toString());
            ob1.execute();
            System.out.println(" EMP CD"+vce.getNewValue().toString());
            if((ob1.getResult() !=null && !ob1.getResult().equals("Y")))
            {
               Row row=(Row)ADFUtils.evaluateEL("#{bindings.ProductionEntryHeaderVO1Iterator.currentRow}");
               System.out.println("row value #### " + row);
            //           approvedByBinding.setValue(val);
            //           row.setAttribute("ApprovedBy", 0);
            //           row.setAttribute("ApproveDt", null);
               row.setAttribute("ApprovedBy", null);
//               AdfFacesContext.getCurrentInstance().addPartialTarget(approvedByBinding);
               ADFUtils.showMessage(ob1.getResult().toString(), 0);
            //           row.setAttribute("ApprovedBy", null);
//               approvedByBinding.setValue(null);
            }
        }
        
//        String val = "0";
        
    }
}
