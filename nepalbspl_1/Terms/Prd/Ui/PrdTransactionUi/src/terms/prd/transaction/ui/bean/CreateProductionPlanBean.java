package terms.prd.transaction.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.sql.Timestamp;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import javax.faces.validator.ValidatorException;

import oracle.adf.model.OperationBinding;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;
import oracle.jbo.server.ViewObjectImpl;

public class CreateProductionPlanBean {
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputDate entryDateBinding;
    private RichInputDate planDateBinding;
    private RichInputText jobCardNoBinding;
    private RichInputComboboxListOfValues saleOrderNoBinding;
    private RichInputComboboxListOfValues productCodeBinding;
    private RichInputComboboxListOfValues indentQtyBinding;
    private RichInputText stdTimePerUnitBinding;
    private RichInputText planQtyBinding;
    private RichInputText timeTakenBinding;
    private RichInputComboboxListOfValues shiftCodeBinding;
    private RichInputComboboxListOfValues employeeCodeBinding;
    private RichButton headerEditBinding;
    private RichButton headerSaveBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichOutputText outputTextBinding;
    private RichButton detailCreateBinding;
    private RichButton detailDeleteBinding;
    private RichButton detailOperatorDeleteBinding;
    private RichTable createProductionPlanDetailTableBinding;
    private RichTable productionPlanDtlOperatorTableBinding;
    private RichInputText indentQuantityBinding;
    private RichInputText entryIdBinding;
    private RichInputComboboxListOfValues shiftCdTransBinding;
    private RichInputComboboxListOfValues shiftCodeDtlOperatorBinding;
    private RichInputComboboxListOfValues shiftCodeDtlOperator1Binding;
    private RichInputText indentQtyBalanceBinding;
    private RichInputText fgStockQtyBinding;
    private RichInputText entryNoBinding;
    private RichInputText week1Binding;
    private RichInputText week2Binding;
    private RichInputText week3Binding;
    private RichInputText week4Binding;
    private RichInputText scheduleFirstMonthBinding;
    private RichInputText scheduleSecondMonthBinding;
    private RichInputText scheduleThirdMonthBinding;
    private RichInputText remarksBinding;
    private RichSelectOneChoice monthBinding;
    private RichInputText yearBinding;
    private RichOutputText firstBinding;
    private RichOutputText secondBinding;
    private RichInputText inputFieldValueBinding;
    private RichColumn weekCheckTransBinding;
    private RichInputText totalPlanQtyBinding;
    private RichInputText totalScheduleQtyBinding;
    private RichInputText hdAmdNoBinding;
    private RichColumn dtlAmdNoBinding;
    private RichInputText internalConspQty;
    private RichInputText internalConspFirstMonth;
    private RichInputText internalConspSecondMonth;


    public CreateProductionPlanBean() {
    }

    public void saveProductionPlanAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("generateProductionPlanEntryNo").execute();
        Integer TransValue=(Integer)outputTextBinding.getValue();
        ADFUtils.setLastUpdatedBy("ProductionPlanHeaderVO1Iterator","LastUpdatedBy");
        System.out.println("TRANS VALUE"+TransValue);
        //        String param=resolvEl("#{pageFlowScope.mode=='E'}");
        //        System.out.println("Save Mode is ====> "+param);
        if(TransValue==0)       
        {
           FacesMessage Message = new FacesMessage("Record Save Successfully. New Entry Number is "+entryNoBinding.getValue()+".");   
           Message.setSeverity(FacesMessage.SEVERITY_INFO);   
           FacesContext fc = FacesContext.getCurrentInstance();   
           fc.addMessage(null, Message);      
            
        }
        else 
        {
            FacesMessage Message = new FacesMessage("Record Update Successfully.");   
            Message.setSeverity(FacesMessage.SEVERITY_INFO);   
            FacesContext fc = FacesContext.getCurrentInstance();   
            fc.addMessage(null, Message);      
        }
//        if(!planDateBinding.isDisabled()) {
//            ADFUtils.findOperation("Commit").execute();
//            FacesMessage Message = new FacesMessage("Record Saved Successfully,for Plan Date: "+planDateBinding.getValue()+"."); 
//            Message.setSeverity(FacesMessage.SEVERITY_INFO); 
//            FacesContext fc = FacesContext.getCurrentInstance(); 
//            fc.addMessage(null, Message); 
//        }
//        else
//        {
//            ADFUtils.findOperation("Commit").execute();
//            FacesMessage Message = new FacesMessage("Record Updated Successfully."); 
//            Message.setSeverity(FacesMessage.SEVERITY_INFO); 
//            FacesContext fc = FacesContext.getCurrentInstance(); 
//            fc.addMessage(null, Message); 
//        }
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setEntryDateBinding(RichInputDate entryDateBinding) {
        this.entryDateBinding = entryDateBinding;
    }

    public RichInputDate getEntryDateBinding() {
        return entryDateBinding;
    }

    public void setPlanDateBinding(RichInputDate planDateBinding) {
        this.planDateBinding = planDateBinding;
    }

    public RichInputDate getPlanDateBinding() {
        return planDateBinding;
    }

    public void setJobCardNoBinding(RichInputText jobCardNoBinding) {
        this.jobCardNoBinding = jobCardNoBinding;
    }

    public RichInputText getJobCardNoBinding() {
        return jobCardNoBinding;
    }

    public void setSaleOrderNoBinding(RichInputComboboxListOfValues saleOrderNoBinding) {
        this.saleOrderNoBinding = saleOrderNoBinding;
    }

    public RichInputComboboxListOfValues getSaleOrderNoBinding() {
        return saleOrderNoBinding;
    }

    public void setProductCodeBinding(RichInputComboboxListOfValues productCodeBinding) {
        this.productCodeBinding = productCodeBinding;
    }

    public RichInputComboboxListOfValues getProductCodeBinding() {
        return productCodeBinding;
    }

    public void setIndentQtyBinding(RichInputComboboxListOfValues indentQtyBinding) {
        this.indentQtyBinding = indentQtyBinding;
    }

    public RichInputComboboxListOfValues getIndentQtyBinding() {
        return indentQtyBinding;
    }

    public void setStdTimePerUnitBinding(RichInputText stdTimePerUnitBinding) {
        this.stdTimePerUnitBinding = stdTimePerUnitBinding;
    }

    public RichInputText getStdTimePerUnitBinding() {
        return stdTimePerUnitBinding;
    }

    public void setPlanQtyBinding(RichInputText planQtyBinding) {
        this.planQtyBinding = planQtyBinding;
    }

    public RichInputText getPlanQtyBinding() {
        return planQtyBinding;
    }

    public void setTimeTakenBinding(RichInputText timeTakenBinding) {
        this.timeTakenBinding = timeTakenBinding;
    }

    public RichInputText getTimeTakenBinding() {
        return timeTakenBinding;
    }

    public void setShiftCodeBinding(RichInputComboboxListOfValues shiftCodeBinding) {
        this.shiftCodeBinding = shiftCodeBinding;
    }

    public RichInputComboboxListOfValues getShiftCodeBinding() {
        return shiftCodeBinding;
    }

    public void setEmployeeCodeBinding(RichInputComboboxListOfValues employeeCodeBinding) {
        this.employeeCodeBinding = employeeCodeBinding;
    }

    public RichInputComboboxListOfValues getEmployeeCodeBinding() {
        return employeeCodeBinding;
    }

    public void editButtonAL(ActionEvent actionEvent) {
        System.out.println("INSIDE EDIT BUTTON AL #####");
        BigDecimal amdNoValue = new BigDecimal(0);

        BigDecimal amdNo1 = new BigDecimal(0);
        BigDecimal amdNo2 = new BigDecimal(1);
 
        BigDecimal amdNo = (BigDecimal)hdAmdNoBinding.getValue();
//        BigDecimal dtlAmdNo = (BigDecimal)dtlAmdNoBinding.getValueBinding(dtlAmdNoBinding);
        
        System.out.println("Amd No #### " + (BigDecimal)hdAmdNoBinding.getValue());
        
        if(amdNo != null) {
            amdNoValue = amdNo.add(amdNo2);
            hdAmdNoBinding.setValue(amdNoValue);
            
            System.out.println("Amendment No+++++++ " + amdNoValue);
            
            DCIteratorBinding itr = ADFUtils.findIterator("ProductionPlanDetailVO1Iterator");
            if(itr != null) 
            {
                RowSetIterator rsi = itr.getRowSetIterator();
                if(rsi != null) {
                    Row[] allRowsInRange = rsi.getAllRowsInRange();
                    for (Row rw : allRowsInRange) {
                        if (rw != null)
                        System.out.println("INSIDE ROW SET ITERATOR ##### ");
                        
                        rw.setAttribute("HdAmdNo", amdNoValue);
//                        itr.getCurrentRow().setAttribute("HdAmdNo", amdNoValue);
//                        dtlAmdNoBinding.setValueBinding(amdNoValue, amdNoValue);
                        }
                }
                rsi.closeRowSetIterator();
            }
            cevmodecheck();
        }
        
        else {
            cevmodecheck();
        } 
        AdfFacesContext.getCurrentInstance().addPartialTarget(createProductionPlanDetailTableBinding);
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setHeaderSaveBinding(RichButton headerSaveBinding) {
        this.headerSaveBinding = headerSaveBinding;
    }

    public RichButton getHeaderSaveBinding() {
        return headerSaveBinding;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setOutputTextBinding(RichOutputText outputTextBinding) {
        this.outputTextBinding = outputTextBinding;
    }

    public RichOutputText getOutputTextBinding() {
        cevmodecheck();
        return outputTextBinding;
    }
    
    private void cevmodecheck(){
            if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
                cevModeDisableComponent("V");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("C");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("E");
            }
        }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                try {
                    Method method1 =
                        component.getClass().getMethod("setDisabled", boolean.class);
                    if (method1 != null) {
                        method1.invoke(component, valueComponent);
                    }
                } catch (NoSuchMethodException e) {
                    try {
                        Method method =
                            component.getClass().getMethod("setReadOnly", boolean.class);
                        if (method != null) {
                            method.invoke(component, valueComponent);
                        }
                    } catch (Exception e1) {
                        // e.printStackTrace();//silently eat this exception.
                    }


                } catch (Exception e) {
                    // e.printStackTrace();//silently eat this exception.
                }
                List<UIComponent> childComponents = component.getChildren();
                for (UIComponent comp : childComponents) {
                    makeComponentHierarchyReadOnly(comp, valueComponent);
                }
            }
    
    public void cevModeDisableComponent(String mode) {

        if (mode.equals("E")) {
            getUnitCodeBinding().setDisabled(true);
            getEntryNoBinding().setDisabled(true);
            getEntryDateBinding().setDisabled(true);
            getMonthBinding().setDisabled(true);
            getYearBinding().setDisabled(true);
//            getProductCodeBinding().setDisabled(true);
//            getWeek1Binding().setDisabled(true);
//            getWeek2Binding().setDisabled(true);
//            getWeek3Binding().setDisabled(true);
//            getWeek4Binding().setDisabled(true);
//            getScheduleFirstMonthBinding().setDisabled(true);
//            getScheduleSecondMonthBinding().setDisabled(true);
//            getFgStockQtyBinding().setDisabled(true);
//            getTimeTakenBinding().setDisabled(true);
            getHdAmdNoBinding().setDisabled(true);
            getTotalScheduleQtyBinding().setDisabled(true);
            getTotalPlanQtyBinding().setDisabled(true);
            getInternalConspQty().setDisabled(true);
            getInternalConspFirstMonth().setDisabled(true);
            getInternalConspSecondMonth().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            getDetailCreateBinding().setDisabled(false);
            try {
                enabledVlaues();
            } catch (Exception e) {
                // TODO: Add catch code
                e.printStackTrace();
            }
            try {
              //  changeLabelName();
            } catch (Exception e) {
                // TODO: Add catch code
                e.printStackTrace();
            }
            
        } else if (mode.equals("C")) {
            getUnitCodeBinding().setDisabled(true);
            getEntryNoBinding().setDisabled(true);
            getEntryDateBinding().setDisabled(true);
//            getFgStockQtyBinding().setDisabled(true);
//            getTimeTakenBinding().setDisabled(true);
            getHdAmdNoBinding().setDisabled(true);
            getTotalScheduleQtyBinding().setDisabled(true);
            getTotalPlanQtyBinding().setDisabled(true);
            getInternalConspQty().setDisabled(true);
            getInternalConspFirstMonth().setDisabled(true);
            getInternalConspSecondMonth().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            getDetailCreateBinding().setDisabled(false);
            
            
        } else if (mode.equals("V")) {
            getDetailCreateBinding().setDisabled(true);
//            getDetailDeleteBinding().setDisabled(true);
//            getDetailOperatorDeleteBinding().setDisabled(true);
                        try {
               // changeLabelName();
            } catch (Exception e) {
                // TODO: Add catch code
                e.printStackTrace();
            }
        }
        
    }

    public void setDetailCreateBinding(RichButton detailCreateBinding) {
        this.detailCreateBinding = detailCreateBinding;
    }

    public RichButton getDetailCreateBinding() {
        return detailCreateBinding;
    }

    public void setDetailDeleteBinding(RichButton detailDeleteBinding) {
        this.detailDeleteBinding = detailDeleteBinding;
    }

    public RichButton getDetailDeleteBinding() {
        return detailDeleteBinding;
    }


    public void setDetailOperatorDeleteBinding(RichButton detailOperatorDeleteBinding) {
        this.detailOperatorDeleteBinding = detailOperatorDeleteBinding;
    }

    public RichButton getDetailOperatorDeleteBinding() {
        return detailOperatorDeleteBinding;
    }

    public void deletePopupDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                    {
                    ADFUtils.findOperation("Delete").execute();
                    //ADFUtils.findOperation("Commit").execute();    
                    System.out.println("Record Delete Successfully");
                   
                        FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                        FacesContext fc = FacesContext.getCurrentInstance();  
                        fc.addMessage(null, Message);  
                   
                    }

                AdfFacesContext.getCurrentInstance().addPartialTarget(createProductionPlanDetailTableBinding);
    }

    public void setCreateProductionPlanDetailTableBinding(RichTable createProductionPlanDetailTableBinding) {
        this.createProductionPlanDetailTableBinding = createProductionPlanDetailTableBinding;
    }

    public RichTable getCreateProductionPlanDetailTableBinding() {
        return createProductionPlanDetailTableBinding;
    }

    public void setProductionPlanDtlOperatorTableBinding(RichTable productionPlanDtlOperatorTableBinding) {
        this.productionPlanDtlOperatorTableBinding = productionPlanDtlOperatorTableBinding;
    }

    public RichTable getProductionPlanDtlOperatorTableBinding() {
        return productionPlanDtlOperatorTableBinding;
    }

    public void deletePopupDL1(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                    {
                    ADFUtils.findOperation("Delete1").execute();
                    //ADFUtils.findOperation("Commit").execute();    
                    System.out.println("Record Delete Successfully");
                   
                        FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                        FacesContext fc = FacesContext.getCurrentInstance();  
                        fc.addMessage(null, Message);  
                   
                    }

                AdfFacesContext.getCurrentInstance().addPartialTarget(productionPlanDtlOperatorTableBinding);
    }

    public void planQtyVCL(ValueChangeEvent vce) {
        System.out.println("Inside Vce ####");
        if(vce.getNewValue() != null) {
            System.out.println("Main Block ####");
            OperationBinding op=(OperationBinding)ADFUtils.findOperation("planQtyValidationProductionPlan");
            op.getParamsMap().put("StdTime", getStdTimePerUnitBinding().getValue());
            op.getParamsMap().put("planQty", vce.getNewValue());
            op.execute();
//            ADFUtils.findOperation("planQtyValidationProductionPlan").execute();
        }
    }

    public void setIndentQuantityBinding(RichInputText indentQuantityBinding) {
        this.indentQuantityBinding = indentQuantityBinding;
    }

    public RichInputText getIndentQuantityBinding() {
        return indentQuantityBinding;
    }

    public void setEntryIdBinding(RichInputText entryIdBinding) {
        this.entryIdBinding = entryIdBinding;
    }

    public RichInputText getEntryIdBinding() {
        return entryIdBinding;
    }

    public void methodAction() {
        System.out.println("Before Create");
        ADFUtils.findOperation("CreateInsert").execute();
//        ADFUtils.findOperation("CreateInsert1").execute();
        System.out.println("After Create");
    }

    public void setShiftCdTransBinding(RichInputComboboxListOfValues shiftCdTransBinding) {
        this.shiftCdTransBinding = shiftCdTransBinding;
    }

    public RichInputComboboxListOfValues getShiftCdTransBinding() {
        return shiftCdTransBinding;
    }

    public void shiftCdTransVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        System.out.println("Inside VCE #######");
                if(vce!=null) 
                {
                    ADFUtils.findOperation("getShiftForDetail").execute();

                }
//        System.out.println("Inside VCE #######");
//        if(vce!=null) 
//        {
////            vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
////            ADFUtils.findOperation("setShiftCdForProductionPlan").execute();
////            System.out.println("Shift Code Trans Value " + vce.getNewValue());
////            if(!vce.getNewValue().toString().isEmpty()) {
////                AdfFacesContext.getCurrentInstance().addPartialTarget(productionPlanDtlOperatorTableBinding);
////                shiftCodeBinding.setValue(vce.getNewValue().toString());
//////                shiftCdTransBinding.setValueBinding(String, getShiftCodeBinding());
//////                shiftCodeBinding.setValueBinding(arg0, arg1);
//////              setShiftCodeBinding(shiftCdTransBinding);
////              System.out.println("After set Value " + shiftCodeBinding.getValue());
//////                AdfFacesContext.getCurrentInstance().addPartialTarget(productionPlanDtlOperatorTableBinding);
////            }
//        }
    }

    public void createDetailOperatorAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert1").execute();
        ADFUtils.findOperation("setShiftCdForProductionPlan").execute();
//        AdfFacesContext.getCurrentInstance().addPartialTarget(productionPlanDtlOperatorTableBinding);
    }

    public void setShiftCodeDtlOperatorBinding(RichInputComboboxListOfValues shiftCodeDtlOperatorBinding) {
        this.shiftCodeDtlOperatorBinding = shiftCodeDtlOperatorBinding;
    }

    public RichInputComboboxListOfValues getShiftCodeDtlOperatorBinding() {
        return shiftCodeDtlOperatorBinding;
    }

    public void setShiftCodeDtlOperator1Binding(RichInputComboboxListOfValues shiftCodeDtlOperator1Binding) {
        this.shiftCodeDtlOperator1Binding = shiftCodeDtlOperator1Binding;
    }

    public RichInputComboboxListOfValues getShiftCodeDtlOperator1Binding() {
        return shiftCodeDtlOperator1Binding;
    }

    public void productCodeVCL(ValueChangeEvent vce) {
        System.out.println("Inside VCE" + vce.getNewValue());
        OperationBinding op = (OperationBinding)ADFUtils.findOperation("getFgStockQtyForProductionPlan");
        op.getParamsMap().put("PrdCd", vce.getNewValue());
        op.execute();
        ADFUtils.findOperation("getProductionPlanIndentQtyBalance").execute();
    }

    public void setIndentQtyBalanceBinding(RichInputText indentQtyBalanceBinding) {
        this.indentQtyBalanceBinding = indentQtyBalanceBinding;
    }

    public RichInputText getIndentQtyBalanceBinding() {
        return indentQtyBalanceBinding;
    }

    public void setFgStockQtyBinding(RichInputText fgStockQtyBinding) {
        this.fgStockQtyBinding = fgStockQtyBinding;
    }

    public RichInputText getFgStockQtyBinding() {
        return fgStockQtyBinding;
    }

    public void planQtyValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if(object != null) {
            BigDecimal indentQtyBalance = (BigDecimal)getIndentQtyBalanceBinding().getValue();
            BigDecimal planQty = (BigDecimal)object;
            
            if(planQty.compareTo(indentQtyBalance)==1) 
            {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Plan Quantity should be less than or equal to Indent Qty Balance.", null));
            }
        }

    }

    public void getPopulateData(ActionEvent actionEvent) {
        OperationBinding op = (OperationBinding)ADFUtils.findOperation("uniqueMonthYearForProductionPlan");
        op.execute();
        System.out.println("op.getResult() " + op.getResult());
        String Mode =(String)ADFUtils.evaluateEL("#{pageFlowScope.mode}");
        System.out.println("MODE VALUE #### " + Mode);
        if(op.getResult()!=null && op.getResult().toString().equalsIgnoreCase("R") && Mode.equalsIgnoreCase("C")) {
            ADFUtils.showMessage("This Month and Year Production Plan has been generated.", 0);
//            ADFUtils.setEL("#{pageFlowScope.mode}", "C");
        }
        else {
        ADFUtils.findOperation("getProductionPlanDetailPopulateData").execute();
        try {
            
           // changeLabelName();
        } catch (Exception e) {
            // TODO: Add catch code
            e.printStackTrace();
        }
        }
        enabledVlaues();
    }
    

    public void setEntryNoBinding(RichInputText entryNoBinding) {
        this.entryNoBinding = entryNoBinding;
    }

    public RichInputText getEntryNoBinding() {
        return entryNoBinding;
    }

    public void setWeek1Binding(RichInputText week1Binding) {
        this.week1Binding = week1Binding;
    }

    public RichInputText getWeek1Binding() {
        
        return week1Binding;
    }

    public void setWeek2Binding(RichInputText week2Binding) {
        this.week2Binding = week2Binding;
    }

    public RichInputText getWeek2Binding() {
        return week2Binding;
    }

    public void setWeek3Binding(RichInputText week3Binding) {
        this.week3Binding = week3Binding;
    }

    public RichInputText getWeek3Binding() {
        return week3Binding;
    }

    public void setWeek4Binding(RichInputText week4Binding) {
        this.week4Binding = week4Binding;
    }

    public RichInputText getWeek4Binding() {
        return week4Binding;
    }

    public void setScheduleFirstMonthBinding(RichInputText scheduleFirstMonthBinding) {
        this.scheduleFirstMonthBinding = scheduleFirstMonthBinding;
    }

    public RichInputText getScheduleFirstMonthBinding() {
        return scheduleFirstMonthBinding;
    }

    public void setScheduleSecondMonthBinding(RichInputText scheduleSecondMonthBinding) {
        this.scheduleSecondMonthBinding = scheduleSecondMonthBinding;
    }

    public RichInputText getScheduleSecondMonthBinding() {
        return scheduleSecondMonthBinding;
    }

    public void setScheduleThirdMonthBinding(RichInputText scheduleThirdMonthBinding) {
        this.scheduleThirdMonthBinding = scheduleThirdMonthBinding;
    }

    public RichInputText getScheduleThirdMonthBinding() {
        return scheduleThirdMonthBinding;
    }

    public void setRemarksBinding(RichInputText remarksBinding) {
        this.remarksBinding = remarksBinding;
    }

    public RichInputText getRemarksBinding() {
        return remarksBinding;
    }
    
    
//    public void changeLabelName()
//    {
//        try {
//           // if(monthBinding.getValue()!=null && yearBinding.getValue()!=null)
//            
//            
//            if(monthBinding.getValue()!=null && yearBinding.getValue()!=null) {
//                String str_date = "01-" + monthBinding.getValue() + "-" + yearBinding.getValue();
//                System.out.println("STR DATE==>" + str_date);
//                DateFormat date_format = new SimpleDateFormat("dd-MMM-yyyy");
//                Date date = date_format.parse(str_date);
//                Calendar cal = GregorianCalendar.getInstance();
//                cal.setTime(date);
//                DateFormat df = new SimpleDateFormat("MMM");
//                //            String currentMonthAsSting = df.format(cal.getTime());
//
//                //For First Next Month
//                cal.set(Calendar.MONTH, cal.get(Calendar.MONTH) + 1);
//                String next_month = df.format(cal.getTime());
//                System.out.println("next_month" + next_month);
//
//                //For Second Next Month
//                cal.set(Calendar.MONTH, cal.get(Calendar.MONTH) + 1);
//                String next_to_month = df.format(cal.getTime());
//                System.out.println("next_to_month" + next_to_month);
//
//                String next = "Schedule For Next " + next_month + " Month";
//                String next_to = "Schedule For Next " + next_to_month + " Month";
//
//                System.out.println("NEXT============"+next);
//                System.out.println("NEXT TO NEXT======="+next_to);
//                firstBinding.setValue(next);
//                secondBinding.setValue(next_to);
////                AdfFacesContext.getCurrentInstance().addPartialTarget(firstBinding);
////                AdfFacesContext.getCurrentInstance().addPartialTarget(secondBinding);
//                //AdfFacesContext.getCurrentInstance().addPartialTarget(inputFieldValueBinding);
//  //              inputFieldValueBinding.setValue(next_to);
//
//            }
//
//
//        } catch (ParseException pe) {
//            // TODO: Add catch code
//            pe.printStackTrace();
//        }    
//        
//    
//    }

    public void setMonthBinding(RichSelectOneChoice monthBinding) {
        this.monthBinding = monthBinding;
    }

    public RichSelectOneChoice getMonthBinding() {
        return monthBinding;
    }

    public void setYearBinding(RichInputText yearBinding) {
        this.yearBinding = yearBinding;
    }

    public RichInputText getYearBinding() {
        return yearBinding;
    }

    public void setFirstBinding(RichOutputText firstBinding) {
        this.firstBinding = firstBinding;
    }

    public RichOutputText getFirstBinding() {
        return firstBinding;
    }

    public void setSecondBinding(RichOutputText secondBinding) {
        this.secondBinding = secondBinding;
    }

    public RichOutputText getSecondBinding() {
        return secondBinding;
    }

    public void setInputFieldValueBinding(RichInputText inputFieldValueBinding) {
        this.inputFieldValueBinding = inputFieldValueBinding;
    }

    public RichInputText getInputFieldValueBinding() {
        return inputFieldValueBinding;
    }

    public void monthValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if(object!=null && yearBinding.getValue()!=null)
        {
        Timestamp Date = (Timestamp) entryDateBinding.getValue();
        Calendar cal = Calendar.getInstance();
        cal.setTime(Date);
        String y=(String)yearBinding.getValue();
        
        int month = cal.get(Calendar.MONTH) + 1;  
        int year = cal.get(Calendar.YEAR);
        int year1=Integer.parseInt(y);
        int MonthCase=0;
        
        if(object.toString().equals("JAN"))
        {
            MonthCase=1;
        }
        else if(object.toString().equals("FEB"))
        {
        MonthCase=2;    
        }
        else if(object.toString().equals("MAR"))
        {
            MonthCase=3;
        }
        else if(object.toString().equals("APR"))
        {
            MonthCase=4;
        }
        else if(object.toString().equals("MAY"))
        {
        MonthCase=5;    
        }
        else if(object.toString().equals("JUN"))
        {
            MonthCase=6;
        }
        else if(object.toString().equals("JUL"))
        {
         MonthCase=7;   
         }
        else if(object.toString().equals("AUG"))
        {
            MonthCase=8;
        }
        else if(object.toString().equals("SEP"))
        {
            MonthCase=9;
        }
        else if(object.toString().equals("OCT"))
        {
            MonthCase=10;
        }
        else if(object.toString().equals("NOV"))
        {
        MonthCase=11;    
        }
        else if(object.toString().equals("DEC"))
        {
        MonthCase=12;    
        }
        else
        {
        System.out.println("No Month Found");    
        }
        System.out.println("Month Case is=>"+MonthCase);
        if(MonthCase < month && year1 > year)
        {
         //throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Month should be greater than or equals to Job Card Date Month", null));
        }
        else if(MonthCase < month && year1 <= year)
        {
         throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Month should be greater than or equals to Production Plan Entry Date Month", null));
        }
        }

    }

    public void yearValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if(object!=null)
        
        {
        String ob = (String)object;
        int yearObject = Integer.parseInt(ob);
        Timestamp Date = (Timestamp) entryDateBinding.getValue();
        Calendar cal = Calendar.getInstance();
        cal.setTime(Date);
        int year = cal.get(Calendar.YEAR);  
        String y = (String)yearBinding.getValue();
//        int year1 = Integer.parseInt(y);
        if(yearObject < year)
        {
         throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Year should be greater than or equals to Production Plan Entry Date Year", null));
        }
        }

    }

    public void yearVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        System.out.println("Inside VCE #######");
        if(vce!=null) 
        {
             ADFUtils.findOperation("uniqueMonthYearForProductionPlan").execute();
        }
    }

    public void setWeekCheckTransBinding(RichColumn weekCheckTransBinding) {
        this.weekCheckTransBinding = weekCheckTransBinding;
    }

    public RichColumn getWeekCheckTransBinding() throws ParseException {
        System.out.println("INSIDE getWeekCheckTransBinding() Bean Getter ######");
        
//        try {
//        String str_date = "01-" +monthBinding.getValue()+ "-" +yearBinding.getValue();
//        System.out.println("STR DATE==>" + str_date);
//        
//        DateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
//        java.util.Date date = df.parse(str_date);
//        
//        Calendar cal = Calendar.getInstance();
//        cal.setTime(date);
//        
//        cal.set(Calendar.MONTH, cal.get(Calendar.MONTH));
//        String current_month = df.format(cal.getTime());
//        System.out.println("CURRENT MONTH #####" + current_month);
//        
//        int week = cal.get(Calendar.WEEK_OF_MONTH);
//        System.out.println("CURRENT WEEK #### " + week);
//        if(week==1) {
//            System.out.println("INSIDE WEEK ##### " + week);
//            week1Binding.setDisabled(true);
//            week2Binding.setDisabled(false);
//            week3Binding.setDisabled(false);
//            week4Binding.setDisabled(false);
//        }
//        }
//        
//        catch (ParseException pe) {
//           // TODO: Add catch code
//           pe.printStackTrace();
//        }
//        if(getWeekCheckTransBinding() != null) {
//            System.out.println("INSIDE GETTER ####");
//            
//            Integer weekTrans = getWeekCheckTransBinding().getv
//            
//            if(weekCheckTransBinding <= week1Binding)
//        }
        
        return weekCheckTransBinding;
    }

    public void week1VCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce!=null) {
            System.out.println("INSIDE week1VCL #############");
            BigDecimal weekOne = (BigDecimal)week1Binding.getValue();
            BigDecimal weekTwo = (BigDecimal)week2Binding.getValue();
            BigDecimal weekThree = (BigDecimal)week3Binding.getValue();
            BigDecimal weekFour = (BigDecimal)week4Binding.getValue();
            
            BigDecimal total = weekOne.add(weekTwo).add(weekThree).add(weekFour);
            totalPlanQtyBinding.setValue(total);
        }
    }

    public void setTotalPlanQtyBinding(RichInputText totalPlanQtyBinding) {
        this.totalPlanQtyBinding = totalPlanQtyBinding;
    }

    public RichInputText getTotalPlanQtyBinding() {
        return totalPlanQtyBinding;
    }


    public void week2VCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce!=null) {
            System.out.println("INSIDE week2VCL #############");
            BigDecimal weekOne = (BigDecimal)week1Binding.getValue();
            BigDecimal weekTwo = (BigDecimal)week2Binding.getValue();
            BigDecimal weekThree = (BigDecimal)week3Binding.getValue();
            BigDecimal weekFour = (BigDecimal)week4Binding.getValue();
            
            BigDecimal total = weekOne.add(weekTwo).add(weekThree).add(weekFour);
            totalPlanQtyBinding.setValue(total);
        }
    }

    public void week3VCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce!=null) {
            System.out.println("INSIDE week3VCL #############");
            BigDecimal weekOne = (BigDecimal)week1Binding.getValue();
            BigDecimal weekTwo = (BigDecimal)week2Binding.getValue();
            BigDecimal weekThree = (BigDecimal)week3Binding.getValue();
            BigDecimal weekFour = (BigDecimal)week4Binding.getValue();
            
            BigDecimal total = weekOne.add(weekTwo).add(weekThree).add(weekFour);
            totalPlanQtyBinding.setValue(total);
        }
    }

    public void week4VCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce!=null) {
            System.out.println("INSIDE week4VCL #############");
            BigDecimal weekOne = (BigDecimal)week1Binding.getValue();
            BigDecimal weekTwo = (BigDecimal)week2Binding.getValue();
            BigDecimal weekThree = (BigDecimal)week3Binding.getValue();
            BigDecimal weekFour = (BigDecimal)week4Binding.getValue();
            
            BigDecimal total = weekOne.add(weekTwo).add(weekThree).add(weekFour);
            totalPlanQtyBinding.setValue(total);
        }
    }

    public void setTotalScheduleQtyBinding(RichInputText totalScheduleQtyBinding) {
        this.totalScheduleQtyBinding = totalScheduleQtyBinding;
    }

    public RichInputText getTotalScheduleQtyBinding() {
        return totalScheduleQtyBinding;
    }

    public void setHdAmdNoBinding(RichInputText hdAmdNoBinding) {
        this.hdAmdNoBinding = hdAmdNoBinding;
    }

    public RichInputText getHdAmdNoBinding() {
        return hdAmdNoBinding;
    }

    public void setDtlAmdNoBinding(RichColumn dtlAmdNoBinding) {
        this.dtlAmdNoBinding = dtlAmdNoBinding;
    }

    public RichColumn getDtlAmdNoBinding() {
        return dtlAmdNoBinding;
    }
    public void enabledVlaues()
    { 
        try
                {	
                if (yearBinding.getValue() != null && monthBinding.getValue() != null) {
                String str_date = "01-" +monthBinding.getValue()+ "-" +yearBinding.getValue();
                System.out.println("STR DATE==>" + str_date);
                DateFormat date_format = new SimpleDateFormat("dd-MMM-yyyy");
                java.util.Date date = date_format.parse(str_date);
                System.out.println("After convert date is====>"+date);
                System.out.println("GET ENTR Y DATE"+entryDateBinding.getValue());               
                
                java.sql.Timestamp entryDate =(java.sql.Timestamp)entryDateBinding.getValue();
                String str_entry = entryDate.toString();
                
                str_entry=str_entry.substring(0, 10);
                Calendar cal = GregorianCalendar.getInstance();
                cal.setTime(date);
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                

                //            String currentMonthAsSting = df.format(cal.getTime());
                
                //For First Next Month
                cal.set(Calendar.MONTH, cal.get(Calendar.MONTH) + 0);
                String next_month = df.format(cal.getTime());
                System.out.println("Current Month " + next_month);
                

                    next_month = next_month.substring(0,7);
                    str_entry =str_entry.substring(0,7);
                    System.out.println("NEXT MONTH DATE===>"+next_month);
                    System.out.println("ENTRY DATE DATE===>"+str_entry);
                    
                    if(!next_month.equals(str_entry))
                    {
                        getWeek1Binding().setDisabled(false);
                        getWeek2Binding().setDisabled(false);
                        getWeek3Binding().setDisabled(false);
                        getWeek4Binding().setDisabled(false);
                    }
                    else
                    {
                    try {
                        Timestamp entryDt1 = new Timestamp(System.currentTimeMillis());
                        java.util.Date date_curr = new java.util.Date();
                        date_curr.setTime(entryDt1.getTime());

                        String str_date1 = new SimpleDateFormat("dd-MMM-yyyy").format(date_curr);

                        //                       String str_date = getAttributeInternal(ENTRYDT);
                        System.out.println("STR DATE==>" + str_date1);
                        //               String format = "dd-mm-yyyy";

                        DateFormat new_df = new SimpleDateFormat("dd-MMM-yyyy");
                        java.util.Date date_new = new_df.parse(str_date1);

                        Calendar cal_new = Calendar.getInstance();
                        cal_new.setTime(date_new);

                        cal_new.set(Calendar.MONTH, cal_new.get(Calendar.MONTH));
                        String current_month1 = new_df.format(cal_new.getTime());
                        System.out.println("CURRENT MONTH #####" + current_month1);

                        int week_no = cal_new.get(Calendar.WEEK_OF_MONTH);
                        if(week_no==1)
                        {
                            getWeek1Binding().setDisabled(false);
                            getWeek2Binding().setDisabled(false);
                            getWeek3Binding().setDisabled(false);
                            getWeek4Binding().setDisabled(false);
                        }
                        else if(week_no ==2)
                        {
                            getWeek1Binding().setDisabled(true);
                            getWeek2Binding().setDisabled(false);
                            getWeek3Binding().setDisabled(false);
                            getWeek4Binding().setDisabled(false);
                        }
                        else if(week_no==3)
                        {
                            getWeek1Binding().setDisabled(true);
                            getWeek2Binding().setDisabled(true);
                            getWeek3Binding().setDisabled(false);
                            getWeek4Binding().setDisabled(false);
                        }
                        else if(week_no>=4)
                        {
                            getWeek1Binding().setDisabled(true);
                            getWeek2Binding().setDisabled(true);
                            getWeek3Binding().setDisabled(true);
                            getWeek4Binding().setDisabled(false);
                        
                        }
                        
                        
                        System.out.println("CURRENT WEEK #### " + cal_new.get(Calendar.WEEK_OF_MONTH));
                    } catch (ParseException pe) {
                        // TODO: Add catch code
                        pe.printStackTrace();
                    }
                    
                    
                    }

                
//                oracle.jbo.domain.Date popDate =new oracle.jbo.domain.Date(next_month);
//                oracle.jbo.domain.Date entry_date =new oracle.jbo.domain.Date(str_entry);
                }
                
                }
                catch(ParseException pe)
                {
                // TODO: Add catch code
                pe.printStackTrace();
                }
    
    
    }


    public void getIntConspFirstMonth(ValueChangeEvent vce) {
        System.out.println("INSIDE getIntConspFirstMonth VCL ##### ");
        if(vce.getNewValue() != null) 
        {
            System.out.println("Main Block First Month ####");
            OperationBinding op=(OperationBinding)ADFUtils.findOperation("getInternalConspFirstMonthForProdPlan");
            op.getParamsMap().put("schdNext1Month1", getScheduleFirstMonthBinding().getValue());
            op.execute();
        }
    }

    public void getIntConspSecondMonthVCL(ValueChangeEvent vce) {
        System.out.println("INSIDE getIntConspSecondMonthVCL VCL ##### ");
        if(vce.getNewValue() != null) 
        {
            System.out.println("Main Block Second Month ####");
            OperationBinding op=(OperationBinding)ADFUtils.findOperation("getInternalConspSecondMonthForProdPlan");
            op.getParamsMap().put("schdNext2Month1", getScheduleSecondMonthBinding().getValue());
            op.execute();
        }
    }

    public void setInternalConspQty(RichInputText internalConspQty) {
        this.internalConspQty = internalConspQty;
    }

    public RichInputText getInternalConspQty() {
        return internalConspQty;
    }

    public void setInternalConspFirstMonth(RichInputText internalConspFirstMonth) {
        this.internalConspFirstMonth = internalConspFirstMonth;
    }

    public RichInputText getInternalConspFirstMonth() {
        return internalConspFirstMonth;
    }

    public void setInternalConspSecondMonth(RichInputText internalConspSecondMonth) {
        this.internalConspSecondMonth = internalConspSecondMonth;
    }

    public RichInputText getInternalConspSecondMonth() {
        return internalConspSecondMonth;
    }

    public void getInternalConsumptionQtyVCL(ValueChangeEvent vce) {
        System.out.println("INSIDE getIntConspSecondMonthVCL VCL ##### ");
        if(vce.getNewValue() != null) 
        {
            System.out.println("Main Block getInternalConsumptionQtyVCL ####");
            OperationBinding op=(OperationBinding)ADFUtils.findOperation("getInternalConspQtyForProdPlan");
            op.getParamsMap().put("totalQty", getTotalPlanQtyBinding().getValue());
            op.execute();
        }
    }
}
