package terms.prd.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

public class CreateToolLoadingUnloadingBean {
    private RichButton headerEditBinding;
    private RichOutputText outputTextBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText entryNoBinding;
    private RichInputDate entryDateBinding;
    private RichInputComboboxListOfValues toolCodeBinding;
    private RichInputComboboxListOfValues forPartBinding;
    private RichInputComboboxListOfValues machineCodeBinding;
    private RichSelectOneChoice loadUnloadBinding;
    private RichInputDate dateBinding;
    private RichInputDate timeBinding;
    private RichInputText qtyProducedBinding;
    private RichInputText remarksBinding;

    public CreateToolLoadingUnloadingBean() {
    }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setOutputTextBinding(RichOutputText outputTextBinding) {
        this.outputTextBinding = outputTextBinding;
    }

    public RichOutputText getOutputTextBinding() {
        cevmodecheck();
        return outputTextBinding;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
               try {
                   Method method1 =
                       component.getClass().getMethod("setDisabled", boolean.class);
                   if (method1 != null) {
                       method1.invoke(component, valueComponent);
                   }
               } catch (NoSuchMethodException e) {
                   try {
                       Method method =
                           component.getClass().getMethod("setReadOnly", boolean.class);
                       if (method != null) {
                           method.invoke(component, valueComponent);
                       }
                   } catch (Exception e1) {
                       // e.printStackTrace();//silently eat this exception.
                   }


               } catch (Exception e) {
                   // e.printStackTrace();//silently eat this exception.
               }
               List<UIComponent> childComponents = component.getChildren();
               for (UIComponent comp : childComponents) {
                   makeComponentHierarchyReadOnly(comp, valueComponent);
               }
           }
    
    private void cevmodecheck(){
           if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
               cevModeDisableComponent("V");
           }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
               cevModeDisableComponent("C");
           }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
               cevModeDisableComponent("E");
           }
       }
    
    public void cevModeDisableComponent(String mode) 
        {
              if (mode.equals("E")) {
                getUnitCodeBinding().setDisabled(true);
                getEntryNoBinding().setDisabled(true);
                getEntryDateBinding().setDisabled(true);
                getHeaderEditBinding().setDisabled(true);
    //                getOperatorButtonBinding().setDisabled(false);
                
               
            } else if (mode.equals("C")) {
                getUnitCodeBinding().setDisabled(true);
                getEntryNoBinding().setDisabled(true);
                getEntryDateBinding().setDisabled(true);
                getHeaderEditBinding().setDisabled(true);
            
            } else if (mode.equals("V")) {
//                getHeaderEditBinding().setDisabled(true);

            }
        }


    public void saveGenerateEntryNo(ActionEvent actionEvent) {
        OperationBinding op=  ADFUtils.findOperation("generateToolLoadUnloadEntryNumber");
        Object rst=op.execute();
        
          if(rst.toString() != null && rst.toString() != "")
          { 
              ADFUtils.findOperation("Commit").execute();
              FacesMessage Message = new FacesMessage("Record Saved Successfully.New Tool Load Unload No. is "+rst+"."); 
              Message.setSeverity(FacesMessage.SEVERITY_INFO); 
              FacesContext fc = FacesContext.getCurrentInstance(); 
              fc.addMessage(null, Message); 
          }
          else
          {
              ADFUtils.findOperation("Commit").execute();
              FacesMessage Message = new FacesMessage("Record Updated Successfully."); 
              Message.setSeverity(FacesMessage.SEVERITY_INFO); 
              FacesContext fc = FacesContext.getCurrentInstance(); 
              fc.addMessage(null, Message); 
          }
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setEntryNoBinding(RichInputText entryNoBinding) {
        this.entryNoBinding = entryNoBinding;
    }

    public RichInputText getEntryNoBinding() {
        return entryNoBinding;
    }

    public void setEntryDateBinding(RichInputDate entryDateBinding) {
        this.entryDateBinding = entryDateBinding;
    }

    public RichInputDate getEntryDateBinding() {
        return entryDateBinding;
    }

    public void setToolCodeBinding(RichInputComboboxListOfValues toolCodeBinding) {
        this.toolCodeBinding = toolCodeBinding;
    }

    public RichInputComboboxListOfValues getToolCodeBinding() {
        return toolCodeBinding;
    }

    public void setForPartBinding(RichInputComboboxListOfValues forPartBinding) {
        this.forPartBinding = forPartBinding;
    }

    public RichInputComboboxListOfValues getForPartBinding() {
        return forPartBinding;
    }

    public void setMachineCodeBinding(RichInputComboboxListOfValues machineCodeBinding) {
        this.machineCodeBinding = machineCodeBinding;
    }

    public RichInputComboboxListOfValues getMachineCodeBinding() {
        return machineCodeBinding;
    }

    public void setLoadUnloadBinding(RichSelectOneChoice loadUnloadBinding) {
        this.loadUnloadBinding = loadUnloadBinding;
    }

    public RichSelectOneChoice getLoadUnloadBinding() {
        return loadUnloadBinding;
    }

    public void setDateBinding(RichInputDate dateBinding) {
        this.dateBinding = dateBinding;
    }

    public RichInputDate getDateBinding() {
        return dateBinding;
    }

    public void setTimeBinding(RichInputDate timeBinding) {
        this.timeBinding = timeBinding;
    }

    public RichInputDate getTimeBinding() {
        return timeBinding;
    }

    public void setQtyProducedBinding(RichInputText qtyProducedBinding) {
        this.qtyProducedBinding = qtyProducedBinding;
    }

    public RichInputText getQtyProducedBinding() {
        return qtyProducedBinding;
    }

    public void setRemarksBinding(RichInputText remarksBinding) {
        this.remarksBinding = remarksBinding;
    }

    public RichInputText getRemarksBinding() {
        return remarksBinding;
    }
}
