package terms.prd.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectBooleanCheckbox;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichButton;

public class CreateWipProcessJobCardBean {
    private RichInputComboboxListOfValues unitCdBinding;
    private RichInputText jobCardNoBinding;
    private RichInputDate jobCardDateBinding;
    private RichInputText monthBinding;
    private RichInputText yearBinding;
    private RichInputComboboxListOfValues partCdBinding;
    private RichInputComboboxListOfValues processCdBinding;
    private RichInputText transSeqNoBinding;
    private RichInputText procCdBinding;
    private RichInputText seqNoBinding;
    private RichInputText requiredBatchBinding;
    private RichInputText producedQtyBinding;
    private RichInputText balanceQtyBinding;
    private RichShowDetailItem requirementTabBinding;
    private RichShowDetailItem alternateTabBinding;
    private RichInputText altItemCdBinding;
    private RichInputText issUomBinding;
    private RichInputText reqQtyBinding;
    private RichInputText issuedQtyBinding;
    private RichInputText consQtyBinding;
    private RichInputText itemCdBinding;
    private RichInputText itemTypeBinding;
    private RichInputText rmTypeBinding;
    private RichInputText uomBinding;
    private RichInputText requiredQtyBinding;
    private RichInputText issuedQty1Binding;
    private RichInputText returnQtyBinding;
    private RichInputText consumedQtyBinding;
    private RichInputText outputTextBinding;
    private RichButton headerEditBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichInputComboboxListOfValues mainProcCdBinding;
    private RichInputText partDescriptionBinding;

    public CreateWipProcessJobCardBean() {
    }

    public void populateDateAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("wipPopulateData").execute();
    }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setUnitCdBinding(RichInputComboboxListOfValues unitCdBinding) {
        this.unitCdBinding = unitCdBinding;
    }

    public RichInputComboboxListOfValues getUnitCdBinding() {
        return unitCdBinding;
    }

    public void setJobCardNoBinding(RichInputText jobCardNoBinding) {
        this.jobCardNoBinding = jobCardNoBinding;
    }

    public RichInputText getJobCardNoBinding() {
        return jobCardNoBinding;
    }

    public void setJobCardDateBinding(RichInputDate jobCardDateBinding) {
        this.jobCardDateBinding = jobCardDateBinding;
    }

    public RichInputDate getJobCardDateBinding() {
        return jobCardDateBinding;
    }

    public void setMonthBinding(RichInputText monthBinding) {
        this.monthBinding = monthBinding;
    }

    public RichInputText getMonthBinding() {
        return monthBinding;
    }

    public void setYearBinding(RichInputText yearBinding) {
        this.yearBinding = yearBinding;
    }

    public RichInputText getYearBinding() {
        return yearBinding;
    }

    public void setPartCdBinding(RichInputComboboxListOfValues partCdBinding) {
        this.partCdBinding = partCdBinding;
    }

    public RichInputComboboxListOfValues getPartCdBinding() {
        return partCdBinding;
    }

    public void setProcessCdBinding(RichInputComboboxListOfValues processCdBinding) {
        this.processCdBinding = processCdBinding;
    }

    public RichInputComboboxListOfValues getProcessCdBinding() {
        return processCdBinding;
    }

    public void setTransSeqNoBinding(RichInputText transSeqNoBinding) {
        this.transSeqNoBinding = transSeqNoBinding;
    }

    public RichInputText getTransSeqNoBinding() {
        return transSeqNoBinding;
    }

    public void setProcCdBinding(RichInputText procCdBinding) {
        this.procCdBinding = procCdBinding;
    }

    public RichInputText getProcCdBinding() {
        return procCdBinding;
    }

    public void setSeqNoBinding(RichInputText seqNoBinding) {
        this.seqNoBinding = seqNoBinding;
    }

    public RichInputText getSeqNoBinding() {
        return seqNoBinding;
    }

    public void setRequiredBatchBinding(RichInputText requiredBatchBinding) {
        this.requiredBatchBinding = requiredBatchBinding;
    }

    public RichInputText getRequiredBatchBinding() {
        return requiredBatchBinding;
    }

    public void setProducedQtyBinding(RichInputText producedQtyBinding) {
        this.producedQtyBinding = producedQtyBinding;
    }

    public RichInputText getProducedQtyBinding() {
        return producedQtyBinding;
    }

    public void setBalanceQtyBinding(RichInputText balanceQtyBinding) {
        this.balanceQtyBinding = balanceQtyBinding;
    }

    public RichInputText getBalanceQtyBinding() {
        return balanceQtyBinding;
    }

    public void setRequirementTabBinding(RichShowDetailItem requirementTabBinding) {
        this.requirementTabBinding = requirementTabBinding;
    }

    public RichShowDetailItem getRequirementTabBinding() {
        return requirementTabBinding;
    }

    public void setAlternateTabBinding(RichShowDetailItem alternateTabBinding) {
        this.alternateTabBinding = alternateTabBinding;
    }

    public RichShowDetailItem getAlternateTabBinding() {
        return alternateTabBinding;
    }

    public void setAltItemCdBinding(RichInputText altItemCdBinding) {
        this.altItemCdBinding = altItemCdBinding;
    }

    public RichInputText getAltItemCdBinding() {
        return altItemCdBinding;
    }

    public void setIssUomBinding(RichInputText issUomBinding) {
        this.issUomBinding = issUomBinding;
    }

    public RichInputText getIssUomBinding() {
        return issUomBinding;
    }

    public void setReqQtyBinding(RichInputText reqQtyBinding) {
        this.reqQtyBinding = reqQtyBinding;
    }

    public RichInputText getReqQtyBinding() {
        return reqQtyBinding;
    }

    public void setIssuedQtyBinding(RichInputText issuedQtyBinding) {
        this.issuedQtyBinding = issuedQtyBinding;
    }

    public RichInputText getIssuedQtyBinding() {
        return issuedQtyBinding;
    }

    public void setConsQtyBinding(RichInputText consQtyBinding) {
        this.consQtyBinding = consQtyBinding;
    }

    public RichInputText getConsQtyBinding() {
        return consQtyBinding;
    }

    public void setItemCdBinding(RichInputText itemCdBinding) {
        this.itemCdBinding = itemCdBinding;
    }

    public RichInputText getItemCdBinding() {
        return itemCdBinding;
    }

    public void setItemTypeBinding(RichInputText itemTypeBinding) {
        this.itemTypeBinding = itemTypeBinding;
    }

    public RichInputText getItemTypeBinding() {
        return itemTypeBinding;
    }

    public void setRmTypeBinding(RichInputText rmTypeBinding) {
        this.rmTypeBinding = rmTypeBinding;
    }

    public RichInputText getRmTypeBinding() {
        return rmTypeBinding;
    }

    public void setUomBinding(RichInputText uomBinding) {
        this.uomBinding = uomBinding;
    }

    public RichInputText getUomBinding() {
        return uomBinding;
    }

    public void setRequiredQtyBinding(RichInputText requiredQtyBinding) {
        this.requiredQtyBinding = requiredQtyBinding;
    }

    public RichInputText getRequiredQtyBinding() {
        return requiredQtyBinding;
    }

    public void setIssuedQty1Binding(RichInputText issuedQty1Binding) {
        this.issuedQty1Binding = issuedQty1Binding;
    }

    public RichInputText getIssuedQty1Binding() {
        return issuedQty1Binding;
    }

    public void setReturnQtyBinding(RichInputText returnQtyBinding) {
        this.returnQtyBinding = returnQtyBinding;
    }

    public RichInputText getReturnQtyBinding() {
        return returnQtyBinding;
    }

    public void setConsumedQtyBinding(RichInputText consumedQtyBinding) {
        this.consumedQtyBinding = consumedQtyBinding;
    }

    public RichInputText getConsumedQtyBinding() {
        return consumedQtyBinding;
    }

    public void setOutputTextBinding(RichInputText outputTextBinding) {
        this.outputTextBinding = outputTextBinding;
    }

    public RichInputText getOutputTextBinding() {
        cevmodecheck();
        return outputTextBinding;
    }
    
    private void cevmodecheck(){
            if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
                cevModeDisableComponent("V");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("C");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("E");
            }
        }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                try {
                    Method method1 =
                        component.getClass().getMethod("setDisabled", boolean.class);
                    if (method1 != null) {
                        method1.invoke(component, valueComponent);
                    }
                } catch (NoSuchMethodException e) {
                    try {
                        Method method =
                            component.getClass().getMethod("setReadOnly", boolean.class);
                        if (method != null) {
                            method.invoke(component, valueComponent);
                        }
                    } catch (Exception e1) {
                        // e.printStackTrace();//silently eat this exception.
                    }


                } catch (Exception e) {
                    // e.printStackTrace();//silently eat this exception.
                }
                List<UIComponent> childComponents = component.getChildren();
                for (UIComponent comp : childComponents) {
                    makeComponentHierarchyReadOnly(comp, valueComponent);
                }
            }
    
    //Set Fields and Button disable.
        public void cevModeDisableComponent(String mode) {
        //            FacesContext fctx = FacesContext.getCurrentInstance();
        //            ELContext elctx = fctx.getELContext();
        //            Application jsfApp = fctx.getApplication();
        //            //create a ValueExpression that points to the ADF binding layer
        //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
        //            //
        //            ValueExpression valueExpr = exprFactory.createValueExpression(
        //                                         elctx,
        //                                         "#{pageFlowScope.mode=='E'}",
        //                                          Object.class
        //                                         );
        //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
            if (mode.equals("E")) {
                getHeaderEditBinding().setDisabled(true);
                getMonthBinding().setDisabled(true);
                getYearBinding().setDisabled(true);
                getTransSeqNoBinding().setDisabled(true);
                getUnitCdBinding().setDisabled(true);
                getJobCardNoBinding().setDisabled(true);
                getJobCardDateBinding().setDisabled(true);
                getProcCdBinding().setDisabled(true);
                getSeqNoBinding().setDisabled(true);
                getRequiredBatchBinding().setDisabled(true);
                getProducedQtyBinding().setDisabled(true);
                getBalanceQtyBinding().setDisabled(true);
                getItemCdBinding().setDisabled(true);
                getItemTypeBinding().setDisabled(true);
                getRmTypeBinding().setDisabled(true);
                getUomBinding().setDisabled(true);
                getRequiredQtyBinding().setDisabled(true);
                getIssuedQty1Binding().setDisabled(true);
                getReturnQtyBinding().setDisabled(true);
                getConsumedQtyBinding().setDisabled(true);
                getAltItemCdBinding().setDisabled(true);
                getIssUomBinding().setDisabled(true);
                getReqQtyBinding().setDisabled(true);
                getIssuedQtyBinding().setDisabled(true);
                getConsQtyBinding().setDisabled(true);
                getMainProcCdBinding().setDisabled(true);
                getPartDescriptionBinding().setDisabled(true);
                getPartCdBinding().setDisabled(true);
            } else if (mode.equals("C")) {
                getHeaderEditBinding().setDisabled(true);
                getUnitCdBinding().setDisabled(true);
                getJobCardNoBinding().setDisabled(true);
                getJobCardDateBinding().setDisabled(true);
                getProcCdBinding().setDisabled(true);
                getSeqNoBinding().setDisabled(true);
                getRequiredBatchBinding().setDisabled(true);
                getProducedQtyBinding().setDisabled(true);
                getBalanceQtyBinding().setDisabled(true);
                getItemCdBinding().setDisabled(true);
                getItemTypeBinding().setDisabled(true);
                getRmTypeBinding().setDisabled(true);
                getUomBinding().setDisabled(true);
                getRequiredQtyBinding().setDisabled(true);
                getIssuedQty1Binding().setDisabled(true);
                getReturnQtyBinding().setDisabled(true);
                getConsumedQtyBinding().setDisabled(true);
                getAltItemCdBinding().setDisabled(true);
                getIssUomBinding().setDisabled(true);
                getReqQtyBinding().setDisabled(true);
                getIssuedQtyBinding().setDisabled(true);
                getConsQtyBinding().setDisabled(true);
                getTransSeqNoBinding().setDisabled(true);
                getPartDescriptionBinding().setDisabled(true);

            } else if (mode.equals("V")) {
                getRequirementTabBinding().setDisabled(false);
                getAlternateTabBinding().setDisabled(false);

            }
            
        }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void saveDataAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("wipGenJobCardNo").execute();
        if(!monthBinding.isDisabled())
        { 
            ADFUtils.findOperation("Commit").execute();
            FacesMessage Message = new FacesMessage("Record Saved Successfully.New Job Card No. is "+jobCardNoBinding.getValue()+"."); 
            Message.setSeverity(FacesMessage.SEVERITY_INFO); 
            FacesContext fc = FacesContext.getCurrentInstance(); 
            fc.addMessage(null, Message); 
        }
        else
        {
            ADFUtils.findOperation("Commit").execute();
            FacesMessage Message = new FacesMessage("Record Updated Successfully."); 
            Message.setSeverity(FacesMessage.SEVERITY_INFO); 
            FacesContext fc = FacesContext.getCurrentInstance(); 
            fc.addMessage(null, Message); 
        }
    }

    public void partCodeVCL(ValueChangeEvent vcl) {
         if(vcl.getNewValue()!=null)
         {
             getPartCdBinding().setDisabled(true);
         }
    }

    public void setMainProcCdBinding(RichInputComboboxListOfValues mainProcCdBinding) {
        this.mainProcCdBinding = mainProcCdBinding;
    }

    public RichInputComboboxListOfValues getMainProcCdBinding() {
        return mainProcCdBinding;
    }

    public void setPartDescriptionBinding(RichInputText partDescriptionBinding) {
        this.partDescriptionBinding = partDescriptionBinding;
    }

    public RichInputText getPartDescriptionBinding() {
        return partDescriptionBinding;
    }
}
