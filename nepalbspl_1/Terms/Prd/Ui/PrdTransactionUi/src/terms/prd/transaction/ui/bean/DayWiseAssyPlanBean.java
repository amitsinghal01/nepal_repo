package terms.prd.transaction.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;
import javax.faces.validator.ValidatorException;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.adf.view.rich.remote.result.ErrorResult;

import oracle.binding.OperationBinding;

import terms.mtl.transaction.model.view.StoreReceiptVoucherDetailsVORowImpl;

import terms.prd.transaction.model.view.DayWiseAssyPlanDetailVORowImpl;

public class DayWiseAssyPlanBean {
    private RichOutputText bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText amdNoBinding;
    private RichInputText indentQtyBinding;
    private RichInputText mfgPlanBinding;
    private RichInputText planBinding;
    private RichSelectOneChoice monthBinding;
    private RichInputText yearBinding;
    private RichInputText custTypeBinding;
    private RichButton populateButtonBinding;
    private RichTable tableBinding;
    private RichInputComboboxListOfValues lineNoBinding;
    private RichInputText prodCodeBinding;
    private RichInputText bindDt1;
    private RichInputText bindDt2;
    private RichInputText bindDt3;
    private RichInputText bindDt4;
    private RichInputText bindDt5;
    private RichInputText bindDt6;
    private RichInputText bindDt7;
    private RichInputText bindDt8;
    private RichInputText bindDt9;
    private RichInputText bindDt10;
    private RichInputText bindDt11;
    private RichInputText bindDt12;
    private RichInputText bindDt13;
    private RichInputText bindDt14;
    private RichInputText bindDt15;
    private RichInputText bindDt16;
    private RichInputText bindDt17;
    private RichInputText bindDt18;
    private RichInputText bindDt19;
    private RichInputText bindDt20;
    private RichInputText bindDt21;
    private RichInputText bindDt22;
    private RichInputText bindDt23;
    private RichInputText bindDt24;
    private RichInputText bindDt25;
    private RichInputText bindDt26;
    private RichInputText bindDt27;
    private RichInputText bindDt28;
    private RichInputText bindDt29;
    private RichInputText bindDt30;
    private RichInputText bindDt31;
    private RichInputText dayTransBinding;
    private RichInputText fgStockBinding;

    public DayWiseAssyPlanBean() {
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    // Security...

    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }

    //Set Fields and Button disable.

    public void cevModeDisableComponent(String mode) {

        if (mode.equals("E")) {
            getUnitCodeBinding().setDisabled(true);
            getAmdNoBinding().setDisabled(true);
            getPlanBinding().setDisabled(true);
            getMfgPlanBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            getCustTypeBinding().setDisabled(true);
            getMonthBinding().setDisabled(true);
            getLineNoBinding().setDisabled(true);
            getProdCodeBinding().setDisabled(true);
            getIndentQtyBinding().setDisabled(true);
            getFgStockBinding().setDisabled(true);


        } else if (mode.equals("C")) {
            getUnitCodeBinding().setDisabled(true);
            getAmdNoBinding().setDisabled(true);
            getPlanBinding().setDisabled(true);
            getMfgPlanBinding().setDisabled(true);
            getCustTypeBinding().setDisabled(true);
            getLineNoBinding().setDisabled(true);
            getProdCodeBinding().setDisabled(true);
            getIndentQtyBinding().setDisabled(true);
            getFgStockBinding().setDisabled(true);


        } else if (mode.equals("V")) {
            getPopulateButtonBinding().setDisabled(true);

        }

    }


    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setAmdNoBinding(RichInputText amdNoBinding) {
        this.amdNoBinding = amdNoBinding;
    }

    public RichInputText getAmdNoBinding() {
        return amdNoBinding;
    }

    public void setIndentQtyBinding(RichInputText indentQtyBinding) {
        this.indentQtyBinding = indentQtyBinding;
    }

    public RichInputText getIndentQtyBinding() {
        return indentQtyBinding;
    }

    public void setMfgPlanBinding(RichInputText mfgPlanBinding) {
        this.mfgPlanBinding = mfgPlanBinding;
    }

    public RichInputText getMfgPlanBinding() {
        return mfgPlanBinding;
    }

    public void setPlanBinding(RichInputText planBinding) {
        this.planBinding = planBinding;
    }

    public RichInputText getPlanBinding() {
        return planBinding;
    }

    public void editButtonAL(ActionEvent actionEvent) {

        cevmodecheck();
        System.out.println("In the Edit Botton For disable Fields....");
        disabledFieldsAll();
        System.out.println("In the All disable Fields....");
        disabledFieldAccordingToDate();
        System.out.println("In the disable Fields according to date....");
        disableDateMonthwise();
        System.out.println("In the disable Fields according to month....");
    }


    // Action Listner For Save Button.........

    public void saveDataAL(ActionEvent actionEvent) {
            ADFUtils.setLastUpdatedBy("DayWiseAssyPlanHeaderVO1Iterator","LastUpdatedBy");
        if (!monthBinding.isDisabled()) {
            ADFUtils.findOperation("Commit").execute();
            FacesMessage Message = new FacesMessage("Record Saved Successfully.");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
            System.out.println("in theif block of true condition ");
        } else {
            ADFUtils.findOperation("Commit").execute();
            FacesMessage Message = new FacesMessage("Record Updated Successfully.");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
            System.out.println("in the else block of true condition ");
        }

    }


    public void setMonthBinding(RichSelectOneChoice monthBinding) {
        this.monthBinding = monthBinding;
    }

    public RichSelectOneChoice getMonthBinding() {
        return monthBinding;
    }

    // Method calling For Data Populate.......

    public void generatePlanPopulateAL(ActionEvent actionEvent) {

        ADFUtils.findOperation("goForPlantableTemp").execute();
        System.out.println("<<<<<<Plantable Temp Data>>>>>>>>>>");
        ADFUtils.findOperation("DayWiseAssyPlanPopulateData").execute();
        System.out.println("<<<<<<<<<<<<<<<<<<,,Data Populate operation end>>>>>>>>>>>");
        disabledFieldsAll();
        System.out.println("........Disable All fields .........");
        disabledFieldAccordingToDate();
        System.out.println("........Disable fields according to date.........");
        disableDateMonthwise();
        System.out.println("........Month wise disable.............");
        

        AdfFacesContext.getCurrentInstance().addPartialTarget(tableBinding);
    }


    // Month Validator................

    public void monthValidator(FacesContext facesContext, UIComponent uIComponent, Object ob) {
        if (ob != null && yearBinding.getValue() != null) {
            Integer MonthCase = 0;
            if (ob.toString().equals("JAN")) {
                MonthCase = 1;
            } else if (ob.toString().equals("FEB")) {
                MonthCase = 2;
            } else if (ob.toString().equals("MAR")) {
                MonthCase = 3;
            } else if (ob.toString().equals("APR")) {
                MonthCase = 4;
            } else if (ob.toString().equals("MAY")) {
                MonthCase = 5;
            } else if (ob.toString().equals("JUN")) {
                MonthCase = 6;
            } else if (ob.toString().equals("JUL")) {
                MonthCase = 7;
            } else if (ob.toString().equals("AUG")) {
                MonthCase = 8;
            } else if (ob.toString().equals("SEP")) {
                MonthCase = 9;
            } else if (ob.toString().equals("OCT")) {
                MonthCase = 10;
            } else if (ob.toString().equals("NOV")) {
                MonthCase = 11;
            } else if (ob.toString().equals("DEC")) {
                MonthCase = 12;
            } else {
                System.out.println("No Month Found");
            }
            Integer j = 1;
            j = j + (Integer) Calendar.getInstance().get(Calendar.MONTH);
            System.out.println("CURRENT MONTH"    + j +   "GIVEN MONTH" + MonthCase);
            Integer year1 = (Integer) Calendar.getInstance().get(Calendar.YEAR);
            Integer y=(Integer)getYearBinding().getValue();
            System.out.println("CURRENT YEAR IS BY SYSTEM*********** "+year1);
            System.out.println("CURRENT YEAR IS BY USER*********** "+y);
            if (y.compareTo(year1)==1 )
            {}else{
                if(MonthCase.compareTo(j) == -1)
                {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,  "'Back Month Plan Can Not Be Generated", null));
            }
            
            
            }
        }

    }

    // Year Validator..........

    public void yearValidator(FacesContext facesContext, UIComponent uIComponent, Object ob) {
        Integer year = (Integer) ob;
        Integer year1 = (Integer) Calendar.getInstance().get(Calendar.YEAR);
        System.out.println("CURRENT YEAR" + year);
        System.out.println("CURRENT YEAR" + year1);
        if (year.compareTo(year1) == -1) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                          "Back Year Plan Can Not Be Generated", null));
        }

    }

    public void setYearBinding(RichInputText yearBinding) {
        this.yearBinding = yearBinding;
    }

    public RichInputText getYearBinding() {
        return yearBinding;
    }

    public void setCustTypeBinding(RichInputText custTypeBinding) {
        this.custTypeBinding = custTypeBinding;
    }

    public RichInputText getCustTypeBinding() {
        return custTypeBinding;
    }

    public void setPopulateButtonBinding(RichButton populateButtonBinding) {
        this.populateButtonBinding = populateButtonBinding;
    }

    public RichButton getPopulateButtonBinding() {
        return populateButtonBinding;
    }

    public void setTableBinding(RichTable tableBinding) {
        this.tableBinding = tableBinding;
    }

    public RichTable getTableBinding() {
        return tableBinding;
    }

    public void setLineNoBinding(RichInputComboboxListOfValues lineNoBinding) {
        this.lineNoBinding = lineNoBinding;
    }

    public RichInputComboboxListOfValues getLineNoBinding() {
        return lineNoBinding;
    }

    public void setProdCodeBinding(RichInputText prodCodeBinding) {
        this.prodCodeBinding = prodCodeBinding;
    }

    public RichInputText getProdCodeBinding() {
        return prodCodeBinding;
    }

    public void setBindDt1(RichInputText bindDt1) {
        this.bindDt1 = bindDt1;
    }

    public RichInputText getBindDt1() {
        return bindDt1;
    }

    public void setBindDt2(RichInputText bindDt2) {
        this.bindDt2 = bindDt2;
    }

    public RichInputText getBindDt2() {
        return bindDt2;
    }

    public void setBindDt3(RichInputText bindDt3) {
        this.bindDt3 = bindDt3;
    }

    public RichInputText getBindDt3() {
        return bindDt3;
    }

    public void setBindDt4(RichInputText bindDt4) {
        this.bindDt4 = bindDt4;
    }

    public RichInputText getBindDt4() {
        return bindDt4;
    }

    public void setBindDt5(RichInputText bindDt5) {
        this.bindDt5 = bindDt5;
    }

    public RichInputText getBindDt5() {
        return bindDt5;
    }

    public void setBindDt6(RichInputText bindDt6) {
        this.bindDt6 = bindDt6;
    }

    public RichInputText getBindDt6() {
        return bindDt6;
    }

    public void setBindDt7(RichInputText bindDt7) {
        this.bindDt7 = bindDt7;
    }

    public RichInputText getBindDt7() {
        return bindDt7;
    }

    public void setBindDt8(RichInputText bindDt8) {
        this.bindDt8 = bindDt8;
    }

    public RichInputText getBindDt8() {
        return bindDt8;
    }

    public void setBindDt9(RichInputText bindDt9) {
        this.bindDt9 = bindDt9;
    }

    public RichInputText getBindDt9() {
        return bindDt9;
    }

    public void setBindDt10(RichInputText bindDt10) {
        this.bindDt10 = bindDt10;
    }

    public RichInputText getBindDt10() {
        return bindDt10;
    }

    public void setBindDt11(RichInputText bindDt11) {
        this.bindDt11 = bindDt11;
    }

    public RichInputText getBindDt11() {
        return bindDt11;
    }

    public void setBindDt12(RichInputText bindDt12) {
        this.bindDt12 = bindDt12;
    }

    public RichInputText getBindDt12() {
        return bindDt12;
    }

    public void setBindDt13(RichInputText bindDt13) {
        this.bindDt13 = bindDt13;
    }

    public RichInputText getBindDt13() {
        return bindDt13;
    }

    public void setBindDt14(RichInputText bindDt14) {
        this.bindDt14 = bindDt14;
    }

    public RichInputText getBindDt14() {
        return bindDt14;
    }

    public void setBindDt15(RichInputText bindDt15) {
        this.bindDt15 = bindDt15;
    }

    public RichInputText getBindDt15() {
        return bindDt15;
    }

    public void setBindDt16(RichInputText bindDt16) {
        this.bindDt16 = bindDt16;
    }

    public RichInputText getBindDt16() {
        return bindDt16;
    }

    public void setBindDt17(RichInputText bindDt17) {
        this.bindDt17 = bindDt17;
    }

    public RichInputText getBindDt17() {
        return bindDt17;
    }

    public void setBindDt18(RichInputText bindDt18) {
        this.bindDt18 = bindDt18;
    }

    public RichInputText getBindDt18() {
        return bindDt18;
    }

    public void setBindDt19(RichInputText bindDt19) {
        this.bindDt19 = bindDt19;
    }

    public RichInputText getBindDt19() {
        return bindDt19;
    }

    public void setBindDt20(RichInputText bindDt20) {
        this.bindDt20 = bindDt20;
    }

    public RichInputText getBindDt20() {
        return bindDt20;
    }

    public void setBindDt21(RichInputText bindDt21) {
        this.bindDt21 = bindDt21;
    }

    public RichInputText getBindDt21() {
        return bindDt21;
    }

    public void setBindDt22(RichInputText bindDt22) {
        this.bindDt22 = bindDt22;
    }

    public RichInputText getBindDt22() {
        return bindDt22;
    }

    public void setBindDt23(RichInputText bindDt23) {
        this.bindDt23 = bindDt23;
    }

    public RichInputText getBindDt23() {
        return bindDt23;
    }

    public void setBindDt24(RichInputText bindDt24) {
        this.bindDt24 = bindDt24;
    }

    public RichInputText getBindDt24() {
        return bindDt24;
    }

    public void setBindDt25(RichInputText bindDt25) {
        this.bindDt25 = bindDt25;
    }

    public RichInputText getBindDt25() {
        return bindDt25;
    }

    public void setBindDt26(RichInputText bindDt26) {
        this.bindDt26 = bindDt26;
    }

    public RichInputText getBindDt26() {
        return bindDt26;
    }

    public void setBindDt27(RichInputText bindDt27) {
        this.bindDt27 = bindDt27;
    }

    public RichInputText getBindDt27() {
        return bindDt27;
    }

    public void setBindDt28(RichInputText bindDt28) {
        this.bindDt28 = bindDt28;
    }

    public RichInputText getBindDt28() {
        return bindDt28;
    }

    public void setBindDt29(RichInputText bindDt29) {
        this.bindDt29 = bindDt29;
    }

    public RichInputText getBindDt29() {
        return bindDt29;
    }

    public void setBindDt30(RichInputText bindDt30) {
        this.bindDt30 = bindDt30;
    }

    public RichInputText getBindDt30() {
        return bindDt30;
    }

    public void setBindDt31(RichInputText bindDt31) {
        this.bindDt31 = bindDt31;
    }

    public RichInputText getBindDt31() {
        return bindDt31;
    }

    public void disabledFieldsAll() {
        getBindDt1().setDisabled(true);
        getBindDt2().setDisabled(true);
        getBindDt3().setDisabled(true);
        getBindDt4().setDisabled(true);
        getBindDt5().setDisabled(true);
        getBindDt6().setDisabled(true);
        getBindDt7().setDisabled(true);
        getBindDt8().setDisabled(true);
        getBindDt9().setDisabled(true);
        getBindDt10().setDisabled(true);
        getBindDt11().setDisabled(true);
        getBindDt12().setDisabled(true);
        getBindDt13().setDisabled(true);
        getBindDt14().setDisabled(true);
        getBindDt15().setDisabled(true);
        getBindDt16().setDisabled(true);
        getBindDt17().setDisabled(true);
        getBindDt18().setDisabled(true);
        getBindDt19().setDisabled(true);
        getBindDt20().setDisabled(true);
        getBindDt21().setDisabled(true);
        getBindDt22().setDisabled(true);
        getBindDt23().setDisabled(true);
        getBindDt24().setDisabled(true);
        getBindDt25().setDisabled(true);
        getBindDt26().setDisabled(true);
        getBindDt27().setDisabled(true);
        getBindDt28().setDisabled(true);
        getBindDt29().setDisabled(true);
        getBindDt30().setDisabled(true);
        getBindDt31().setDisabled(true);

    }


    public void disabledFieldAccordingToDate() {

        Integer d = (Integer) dayTransBinding.getValue();
        System.out.println("******************Value is D in Edit Mode ===>  " + d);
        if (!d.equals(null)) {
            if (d <= 1) {
                getBindDt1().setDisabled(false);
            }
            if (d <= 2) {
                getBindDt2().setDisabled(false);
            }
            if (d <= 3) {
                getBindDt3().setDisabled(false);
            }
            if (d <= 4) {
                getBindDt4().setDisabled(false);
            }
            if (d <= 5) {
                getBindDt5().setDisabled(false);
            }
            if (d <= 6) {
                getBindDt6().setDisabled(false);
            }
            if (d <= 7) {
                getBindDt7().setDisabled(false);
            }
            if (d <= 8) {
                getBindDt8().setDisabled(false);
            }
            if (d <= 9) {
                getBindDt9().setDisabled(false);
            }
            if (d <= 10) {
                getBindDt10().setDisabled(false);
            }
            if (d <= 11) {
                getBindDt11().setDisabled(false);
            }
            if (d <= 12) {
                getBindDt12().setDisabled(false);
            }
            if (d <= 13) {
                getBindDt13().setDisabled(false);
            }
            if (d <= 14) {
                getBindDt14().setDisabled(false);
            }
            if (d <= 15) {
                getBindDt15().setDisabled(false);
            }
            if (d <= 16) {
                getBindDt16().setDisabled(false);
            }
            if (d <= 17) {
                getBindDt17().setDisabled(false);
            }
            if (d <= 18) {
                getBindDt18().setDisabled(false);
            }
            if (d <= 19) {
                getBindDt19().setDisabled(false);
            }
            if (d <= 20) {
                getBindDt20().setDisabled(false);
            }
            if (d <= 21) {
                getBindDt21().setDisabled(false);
            }
            if (d <= 22) {
                getBindDt22().setDisabled(false);
            }
            if (d <= 23) {
                getBindDt23().setDisabled(false);
            }
            if (d <= 24) {
                getBindDt24().setDisabled(false);
            }
            if (d <= 25) {
                getBindDt25().setDisabled(false);
            }
            if (d <= 26) {
                getBindDt26().setDisabled(false);
            }
            if (d <= 27) {
                getBindDt27().setDisabled(false);
            }
            if (d <= 28) {
                getBindDt28().setDisabled(false);
            }
            if (d <= 29) {
                getBindDt29().setDisabled(false);
            }
            if (d <= 30) {
                getBindDt30().setDisabled(false);
            }
            if (d <= 31) {
                getBindDt31().setDisabled(false);
            }
            System.out.println("End>>>>>>>>>>>>>>>>>");
        }
    }


    public void setDayTransBinding(RichInputText dayTransBinding) {
        this.dayTransBinding = dayTransBinding;
    }

    public RichInputText getDayTransBinding() {
        return dayTransBinding;
    }

    public void disableDateMonthwise() {


        int flag = 0;

        if (monthBinding.getValue() != null) {
            System.out.println("In the if Bolck>>>>>>>>>>>>>");


            if (monthBinding.getValue().equals("JAN") || monthBinding.getValue().equals("MAR") ||
                monthBinding.getValue().equals("MAY") || monthBinding.getValue().equals("JUL") ||
                monthBinding.getValue().equals("AUG") || monthBinding.getValue().equals("OCT") ||
                monthBinding.getValue().equals("DEC")) {

                bindDt31.setDisabled(false);


            } else if (monthBinding.getValue().equals("APR") || monthBinding.getValue().equals("JUN") ||
                       monthBinding.getValue().equals("SEP") || monthBinding.getValue().equals("NOV")) {

                bindDt31.setDisabled(true);

            } else if (monthBinding.getValue().equals("FEB")) {


                int yr = (Integer) yearBinding.getValue();
                if (yr % 4 == 0 && yr % 100 != 0) {

                    flag = 1;

                } else {

                    flag = 0;
                }

                if (flag == 1) {

                    bindDt29.setDisabled(false);
                    bindDt30.setDisabled(true);
                    bindDt31.setDisabled(true);
                } else {
                    bindDt29.setDisabled(true);
                    bindDt30.setDisabled(true);
                    bindDt31.setDisabled(true);


                }
            }

            System.out.println("<<<<<<<<<<<<<<<End>>>>>>>>>>>>>");

        }

    }


    public void systemDateVCL(ValueChangeEvent vcl) {

    }

    public void monthVCL(ValueChangeEvent vcl) {

        AdfFacesContext.getCurrentInstance().addPartialTarget(dayTransBinding);
    }

    public void d1VCL(ValueChangeEvent vcl) {
        vcl.getComponent().processUpdates(FacesContext.getCurrentInstance());
        //if (vcl.getNewValue() != null)
        //{
            BigDecimal sum = new BigDecimal(0);
            BigDecimal s1 = new BigDecimal(0);
            BigDecimal v1 = (BigDecimal) bindDt1.getValue();
            BigDecimal v2 = (BigDecimal) bindDt2.getValue();
            BigDecimal v3 = (BigDecimal) bindDt3.getValue();
            BigDecimal v4 = (BigDecimal) bindDt4.getValue();
            BigDecimal v5 = (BigDecimal) bindDt5.getValue();
            BigDecimal v6 = (BigDecimal) bindDt6.getValue();
            BigDecimal v7 = (BigDecimal) bindDt7.getValue();
            BigDecimal v8 = (BigDecimal) bindDt8.getValue();
            BigDecimal v9 = (BigDecimal) bindDt9.getValue();
            BigDecimal v10 = (BigDecimal) bindDt10.getValue();
            BigDecimal v11 = (BigDecimal) bindDt11.getValue();
            BigDecimal v12 = (BigDecimal) bindDt12.getValue();
            BigDecimal v13 = (BigDecimal) bindDt13.getValue();
            BigDecimal v14 = (BigDecimal) bindDt14.getValue();
            BigDecimal v15 = (BigDecimal) bindDt15.getValue();
            BigDecimal v16 = (BigDecimal) bindDt16.getValue();
            BigDecimal v17 = (BigDecimal) bindDt17.getValue();
            BigDecimal v18 = (BigDecimal) bindDt18.getValue();
            BigDecimal v19 = (BigDecimal) bindDt19.getValue();
            BigDecimal v20 = (BigDecimal) bindDt20.getValue();
            BigDecimal v21 = (BigDecimal) bindDt21.getValue();
            BigDecimal v22 = (BigDecimal) bindDt22.getValue();
            BigDecimal v23 = (BigDecimal) bindDt23.getValue();
            BigDecimal v24 = (BigDecimal) bindDt24.getValue();
            BigDecimal v25 = (BigDecimal) bindDt25.getValue();
            BigDecimal v26 = (BigDecimal) bindDt26.getValue();
            BigDecimal v27 = (BigDecimal) bindDt27.getValue();
            BigDecimal v28 = (BigDecimal) bindDt28.getValue();
            BigDecimal v29 = (BigDecimal) bindDt29.getValue();
            BigDecimal v30 = (BigDecimal) bindDt30.getValue();
            BigDecimal v31 = (BigDecimal) bindDt31.getValue();
            System.out.println("Date is" + v30);
            sum = sum.add(v1 != null ? v1 : s1);
            sum = sum.add(v2 != null ? v2 : s1);
            sum = sum.add(v3 != null ? v3 : s1);
            sum = sum.add(v4 != null ? v4 : s1);
            sum = sum.add(v5 != null ? v5 : s1);
            sum = sum.add(v6 != null ? v6 : s1);
            sum = sum.add(v7 != null ? v7 : s1);
            sum = sum.add(v8 != null ? v8 : s1);
            sum = sum.add(v9 != null ? v9 : s1);
            sum = sum.add(v10 != null ? v10 : s1);
            sum = sum.add(v11 != null ? v11 : s1);
            sum = sum.add(v12 != null ? v12 : s1);
            sum = sum.add(v13 != null ? v13 : s1);
            sum = sum.add(v14 != null ? v14 : s1);
            sum = sum.add(v15 != null ? v15 : s1);
            sum = sum.add(v16 != null ? v16 : s1);
            sum = sum.add(v17 != null ? v17 : s1);
            sum = sum.add(v18 != null ? v18 : s1);
            sum = sum.add(v19 != null ? v19 : s1);
            sum = sum.add(v20 != null ? v20 : s1);
            sum = sum.add(v21 != null ? v21 : s1);
            sum = sum.add(v22 != null ? v22 : s1);
            sum = sum.add(v23 != null ? v23 : s1);
            sum = sum.add(v24 != null ? v24 : s1);
            sum = sum.add(v25 != null ? v25 : s1);
            sum = sum.add(v26 != null ? v26 : s1);
            sum = sum.add(v27 != null ? v27 : s1);
            sum = sum.add(v28 != null ? v28 : s1);
            sum = sum.add(v29 != null ? v29 : s1);
            sum = sum.add(v30 != null ? v30 : s1);
            sum = sum.add(v31 != null ? v31 : s1);
            planBinding.setValue(sum);
            System.out.println("<<<<<<<<<<<<<sum is>>>>>>>" + sum);
            System.out.println("Total Final Plan is " + planBinding.getValue());
            BigDecimal mfg = (BigDecimal) mfgPlanBinding.getValue();
            System.out.println("mfg plan is>>>>>>" + mfg);
            BigDecimal netp = (BigDecimal) planBinding.getValue();
            System.out.println("Total Plan is>>>>>>>>>>> " + netp);
            if (netp.compareTo(mfg) == 1 && mfg != null && netp != null) {

                FacesMessage Message = new FacesMessage("Total Final Plan can not be greater than MFG. Plan.");
                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);


            }


      //  }

    }


    public void dateValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        // Add event code here...




    }

    public void setFgStockBinding(RichInputText fgStockBinding) {
        this.fgStockBinding = fgStockBinding;
    }

    public RichInputText getFgStockBinding() {
        return fgStockBinding;
    }
    
   



    
}
