package terms.prd.transaction.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.Calendar;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import javax.faces.validator.ValidatorException;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;

public class ManufacturingPlanBean {
    private RichInputComboboxListOfValues unitCdBinding;
    private RichInputText minStockBinding;
    private RichInputText addPlanBinding;
    private RichInputText netPlanBinding;
    private RichInputText mktqtyBinding;
    private RichInputText fgStockBinding;
    private RichInputText rejPerBinding;
    private RichInputText rejPlanBinding;
    private RichSelectOneChoice monthBinding;
    private RichInputText custTypeBinding;
    private RichOutputText bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichButton populateButtonBinding;
    private RichInputText yearBinding;
    private RichTable tableBinding;
    private RichInputComboboxListOfValues prodCodeBinding;
    private RichInputText w1Binding;
    private RichInputText w2Binding;
    private RichInputText w3Binding;
    private RichInputText w4Binding;
    private RichInputText sumBinding;
    String status="F";

    public ManufacturingPlanBean() {
    }

    // Populate Data................

    public void populateDataAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("ManufacturingPlanPopulateData").execute();
        AdfFacesContext.getCurrentInstance().addPartialTarget(tableBinding);


    }


    public void setUnitCdBinding(RichInputComboboxListOfValues unitCdBinding) {
        this.unitCdBinding = unitCdBinding;
    }

    public RichInputComboboxListOfValues getUnitCdBinding() {
        return unitCdBinding;
    }

    public void setMinStockBinding(RichInputText minStockBinding) {
        this.minStockBinding = minStockBinding;
    }

    public RichInputText getMinStockBinding() {
        return minStockBinding;
    }

    public void setAddPlanBinding(RichInputText addPlanBinding) {
        this.addPlanBinding = addPlanBinding;
    }

    public RichInputText getAddPlanBinding() {
        return addPlanBinding;
    }

    // Min Stock Vcl For calculation of Net Plan...........

    public void minStockVCL(ValueChangeEvent vcl) {
        if(vcl!=null){

        BigDecimal minstock = new BigDecimal(0);
        minstock = (BigDecimal) vcl.getNewValue();
        BigDecimal mktqty = new BigDecimal(0);
        mktqty = (BigDecimal) mktqtyBinding.getValue();
        BigDecimal fgstock = new BigDecimal(0);
        System.out.println("Min Stock" + minstock + " Mkt Qty" + mktqty + " FG Stock" + fgstock);
        fgstock = (BigDecimal) fgStockBinding.getValue();
        BigDecimal net = new BigDecimal(0);
        net = mktqty.add(minstock).subtract(fgstock);
        System.out.println("NET PLAN CALCULATED" + net);
        BigDecimal val = new BigDecimal(0);
        if (net.compareTo(val) == 1) {
            netPlanBinding.setValue(net);
        } else {

            netPlanBinding.setValue(val);
        }
        BigDecimal rejper = (BigDecimal) rejPerBinding.getValue();
        System.out.println("Rejection Value Binding" + rejPerBinding.getValue());
        BigDecimal netplan = new BigDecimal(0);
        netplan = (BigDecimal) netPlanBinding.getValue();
        System.out.println("Net Plan is>>>>>>>>>>>" + netplan);
        BigDecimal rejp = new BigDecimal(0);
        rejp = (BigDecimal) rejPlanBinding.getValue();
        System.out.println("<<<<<<<<<<Rejection Plan is>>>>>>>>>>" + rejp);
        netplan = (BigDecimal) net.add(rejp);
        System.out.println("<<<<<<<<<<Net Plan is>>>>>>>>>" + netplan);
        if (netplan.compareTo(val) == 1) {
            netPlanBinding.setValue(netplan);
            System.out.println("In the if Loop.............");
        } else {

            netPlanBinding.setValue(val);
            System.out.println("In the else part of if Loop.............");
        }

    }
    }

    public void setNetPlanBinding(RichInputText netPlanBinding) {
        this.netPlanBinding = netPlanBinding;
    }

    public RichInputText getNetPlanBinding() {
        return netPlanBinding;
    }

    public void setMktqtyBinding(RichInputText mktqtyBinding) {
        this.mktqtyBinding = mktqtyBinding;
    }

    public RichInputText getMktqtyBinding() {
        return mktqtyBinding;
    }

    public void setFgStockBinding(RichInputText fgStockBinding) {
        this.fgStockBinding = fgStockBinding;
    }

    public RichInputText getFgStockBinding() {
        return fgStockBinding;
    }


    // Add. Plan Vcl For calculation of Net Plan and Rej Plan..........

    public void additionalPlanVCL(ValueChangeEvent vcl) {
          if(vcl!=null){

        BigDecimal addplan = new BigDecimal(0);
        addplan = (BigDecimal) vcl.getNewValue();
        BigDecimal minstock = (BigDecimal) (minStockBinding.getValue()!=null?minStockBinding.getValue():new BigDecimal(0));
        BigDecimal mktqty = (BigDecimal) (mktqtyBinding.getValue()!=null?mktqtyBinding.getValue():new BigDecimal(0));
        mktqty = (BigDecimal) mktqtyBinding.getValue();
        BigDecimal fgstock = (BigDecimal) (fgStockBinding.getValue()!=null?fgStockBinding.getValue():new BigDecimal(0));
        fgstock = (BigDecimal) fgStockBinding.getValue();
        System.out.println("Min Stock" + minstock + " Mkt Qty" + mktqty + " FG Stock" + fgstock + " Add. Plan" +
                           addplan);
        BigDecimal net = new BigDecimal(0);
        net = mktqty.add(minstock.add(addplan)).subtract(fgstock);
        System.out.println("NET PLAN CALCULATED" + net);
        BigDecimal val = new BigDecimal(0);
        if (net.compareTo(val) == 1) {
            netPlanBinding.setValue(net);
            System.out.println("In the if bolck for negative value.......");
        } else {

            netPlanBinding.setValue(val);
            System.out.println("In the else part of if block for negative value.....");
        }

        BigDecimal rejper = (BigDecimal) rejPerBinding.getValue();
        System.out.println("Rejection per Binding" + rejper);
            BigDecimal rejp = new BigDecimal(0);
            rejp = (BigDecimal) rejPlanBinding.getValue();
            System.out.println("Rej Plan is>>>>>>>" + rejp);
            BigDecimal netp = new BigDecimal(0);
            netp = (BigDecimal) netPlanBinding.getValue();
           netp = (BigDecimal) net.add(rejp);
            System.out.println("Net Plan after calculation with rej per is " + netp);
            if (netp.compareTo(val) == 1) {
               netPlanBinding.setValue(netp);
               System.out.println("In the if bolck for negative value.......");
           } else {

             netPlanBinding.setValue(val);
             System.out.println("In the else part of if block for negative value.....");
           }
      }
    }

    public void setRejPerBinding(RichInputText rejPerBinding) {
        this.rejPerBinding = rejPerBinding;
    }

    public RichInputText getRejPerBinding() {
        return rejPerBinding;
    }

    public void setRejPlanBinding(RichInputText rejPlanBinding) {
        this.rejPlanBinding = rejPlanBinding;
    }

    public RichInputText getRejPlanBinding() {
        return rejPlanBinding;
    }

    public void setMonthBinding(RichSelectOneChoice monthBinding) {
        this.monthBinding = monthBinding;
    }

    public RichSelectOneChoice getMonthBinding() {
        return monthBinding;
    }

    public void setCustTypeBinding(RichInputText custTypeBinding) {
        this.custTypeBinding = custTypeBinding;
    }

    public RichInputText getCustTypeBinding() {
        return custTypeBinding;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }

    //Set Fields and Button disable.
    public void cevModeDisableComponent(String mode) {

        if (mode.equals("E")) {
            getCustTypeBinding().setDisabled(true);
            getRejPerBinding().setDisabled(true);
            getMktqtyBinding().setDisabled(true);
            getFgStockBinding().setDisabled(true);
            getRejPlanBinding().setDisabled(true);
            getNetPlanBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            getMonthBinding().setDisabled(true);
            getUnitCdBinding().setDisabled(true);
            getProdCodeBinding().setDisabled(true);
            getMinStockBinding().setDisabled(true);
        }

        else if (mode.equals("C")) {
            getCustTypeBinding().setDisabled(true);
            getRejPerBinding().setDisabled(true);
            getMktqtyBinding().setDisabled(true);
            getFgStockBinding().setDisabled(true);
            getRejPlanBinding().setDisabled(true);
            getNetPlanBinding().setDisabled(true);
            getUnitCdBinding().setDisabled(true);
            getProdCodeBinding().setDisabled(true);
            getMinStockBinding().setDisabled(true);


        } else if (mode.equals("V")) {
            getPopulateButtonBinding().setDisabled(true);


        }

    }


    public void setPopulateButtonBinding(RichButton populateButtonBinding) {
        this.populateButtonBinding = populateButtonBinding;
    }

    public RichButton getPopulateButtonBinding() {
        return populateButtonBinding;
    }

    public void saveDataAL(ActionEvent actionEvent) {
        if(status.equalsIgnoreCase("F"))
        {
            ADFUtils.setLastUpdatedBy("ManufacturingPlanHeaderVO1Iterator","LastUpdatedBy");
        if (!monthBinding.isDisabled()) {
            ADFUtils.findOperation("Commit").execute();
            FacesMessage Message = new FacesMessage("Record Saved Successfully.");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
        } else {
            ADFUtils.findOperation("Commit").execute();
            FacesMessage Message = new FacesMessage("Record Updated Successfully.");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
        }
        }else
        {
            ADFUtils.showMessage("Net Qty should be equals to sum of Week Qty.", 0);
        }
    }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void yearValidator(FacesContext facesContext, UIComponent uIComponent, Object ob) {
        Integer year = (Integer) ob;
        Integer year1 = (Integer) Calendar.getInstance().get(Calendar.YEAR);
        System.out.println("CURRENT YEAR ENTER" + year);
        System.out.println("CURRENT YEAR SYSTEMDATE" + year1);
        if (year.compareTo(year1) == -1) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                          "Back Year Plan Can Not Be Generated", null));
        }

    }

    public void monthValidator(FacesContext facesContext, UIComponent uIComponent, Object ob) {
        if (ob != null && yearBinding.getValue() != null) {
           
            Integer MonthCase = 0;
            if   (ob.toString().equals("JAN")) {
                MonthCase = 1;
            } else if (ob.toString().equals("FEB")) {
                MonthCase = 2;
            } else if (ob.toString().equals("MAR")) {
                MonthCase = 3;
            } else if (ob.toString().equals("APR")) {
                MonthCase = 4;
            } else if (ob.toString().equals("MAY")) {
                MonthCase = 5;
            } else if (ob.toString().equals("JUN")) {
                MonthCase = 6;
            } else if (ob.toString().equals("JUL")) {
                MonthCase = 7;
            } else if (ob.toString().equals("AUG")) {
                MonthCase = 8;
            } else if (ob.toString().equals("SEP")) {
                MonthCase = 9;
            } else if (ob.toString().equals("OCT")) {
                MonthCase = 10;
            } else if (ob.toString().equals("NOV")) {
                MonthCase = 11;
            } else if (ob.toString().equals("DEC")) {
                MonthCase = 12;
            } else {
                System.out.println("No Month Found");
            }
            Integer j = 1;
            j = j + (Integer) Calendar.getInstance().get(Calendar.MONTH);
            System.out.println("CURRENT MONTH"    + j +   "GIVEN MONTH" + MonthCase);
            Integer year1 = (Integer) Calendar.getInstance().get(Calendar.YEAR);
            Integer y=(Integer)getYearBinding().getValue();
            System.out.println("CURRENT YEAR IS BY SYSTEM*********** "+year1);
            System.out.println("CURRENT YEAR IS BY USER*********** "+y);
            if (y.compareTo(year1)==1 )
            {}else{
                if(MonthCase.compareTo(j) == -1)
                {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,  "'Back Month Plan Can Not Be Generated", null));
            }
         
           
        }
    }
    }

    public void setYearBinding(RichInputText yearBinding) {
        this.yearBinding = yearBinding;
    }

    public RichInputText getYearBinding() {
        return yearBinding;
    }

    public void setTableBinding(RichTable tableBinding) {
        this.tableBinding = tableBinding;
    }

    public RichTable getTableBinding() {
        return tableBinding;
    }

    public void setProdCodeBinding(RichInputComboboxListOfValues prodCodeBinding) {
        this.prodCodeBinding = prodCodeBinding;
    }

    public RichInputComboboxListOfValues getProdCodeBinding() {
        return prodCodeBinding;
    }

    public void valAll(ValueChangeEvent vca) {
        vca.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vca.getNewValue()!=null){
            BigDecimal W1 =(BigDecimal)w1Binding.getValue()==null ? new BigDecimal(0):(BigDecimal)w1Binding.getValue();
            BigDecimal W2 =(BigDecimal)w2Binding.getValue()==null ? new BigDecimal(0):(BigDecimal)w2Binding.getValue();
            BigDecimal W3 =(BigDecimal)w3Binding.getValue()==null ? new BigDecimal(0):(BigDecimal)w3Binding.getValue();
            BigDecimal W4 =(BigDecimal)w4Binding.getValue()==null ? new BigDecimal(0):(BigDecimal)w4Binding.getValue();
            BigDecimal Tot=W1.add(W2).add(W3).add(W4);
            BigDecimal net =(BigDecimal)netPlanBinding.getValue()==null ? new BigDecimal(0):(BigDecimal)netPlanBinding.getValue();
            System.out.println("Total sum==>"+Tot+" Net Plan===>"+net);
            if(Tot.compareTo(new BigDecimal(0))==1 && net.compareTo(new BigDecimal(0))==1 && Tot.compareTo(net)==1){
                ADFUtils.showMessage("Net Qty should be equals to sum of Week Qty.", 0);
                status="T";
            }else
            {
                status="F";
                sumBinding.setValue(Tot);
                AdfFacesContext.getCurrentInstance().addPartialTarget(sumBinding);
            }
        }
    }

    public void setW1Binding(RichInputText w1Binding) {
        this.w1Binding = w1Binding;
    }

    public RichInputText getW1Binding() {
        return w1Binding;
    }

    public void setW2Binding(RichInputText w2Binding) {
        this.w2Binding = w2Binding;
    }

    public RichInputText getW2Binding() {
        return w2Binding;
    }

    public void setW3Binding(RichInputText w3Binding) {
        this.w3Binding = w3Binding;
    }

    public RichInputText getW3Binding() {
        return w3Binding;
    }

    public void setW4Binding(RichInputText w4Binding) {
        this.w4Binding = w4Binding;
    }

    public RichInputText getW4Binding() {
        return w4Binding;
    }

    public void setSumBinding(RichInputText sumBinding) {
        this.sumBinding = sumBinding;
    }

    public RichInputText getSumBinding() {
        return sumBinding;
    }

    public void sumVal(FacesContext facesContext, UIComponent uIComponent, Object ob) {
        BigDecimal sum =(BigDecimal)ob==null? new BigDecimal(0):(BigDecimal)ob;
        BigDecimal net =(BigDecimal)netPlanBinding.getValue()==null? new BigDecimal(0):(BigDecimal)netPlanBinding.getValue();
        if(net.compareTo(new BigDecimal(0))==1 && sum.compareTo(new BigDecimal(0))==1 && sum.compareTo(net)==1){
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "net qty should be equal to sum of week qty", null));

        }
    }

    public String saveAndCloseAL() {
        if(status.equalsIgnoreCase("F"))
        {
        ADFUtils.setLastUpdatedBy("ManufacturingPlanHeaderVO1Iterator","LastUpdatedBy");
        if (!monthBinding.isDisabled()) {
        ADFUtils.findOperation("Commit").execute();
        FacesMessage Message = new FacesMessage("Record Saved Successfully.");
        Message.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
            return "Save And Close";
        } else {
        ADFUtils.findOperation("Commit").execute();
        FacesMessage Message = new FacesMessage("Record Updated Successfully.");
        Message.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
            return "Save And Close";
        }
        }else
        {
            ADFUtils.showMessage("Net Qty should be equals to sum of Week Qty.", 0);
        }
        return null;
    }
}

