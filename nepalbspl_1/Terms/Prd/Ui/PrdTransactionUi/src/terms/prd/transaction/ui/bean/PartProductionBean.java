package terms.prd.transaction.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;
import javax.faces.validator.ValidatorException;

import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;

import oracle.adf.view.rich.component.rich.layout.RichPanelLabelAndMessage;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichButton;

import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

public class PartProductionBean {
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichOutputText outputTextBinding;
    private RichTable prodnShopRmTableBinding;
    private RichTable breakdownTableBinding;
    private RichTable prodnRejectionTableBinding;
    private RichTable prodnOperatorTableBinding;
    private RichTable prodnLotTableBinding;
    private RichInputText scheduleDescriptionBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputComboboxListOfValues approvedByBinding;
    private RichInputText prodnSlipNoBinding;
    private RichInputDate entryDateBinding;
    private RichInputText processSequenceBinding;
    private RichInputText processCodeDBinding;
    private RichInputText processSequenceDBinding;
    private RichShowDetailItem breakdownTabBinding;
    private RichShowDetailItem rejectionTabBinding;
    private RichShowDetailItem operatorTabBinding;
    private RichButton lotIssueButtonBinding;
    private RichShowDetailItem partRMTabBinding;
    private RichInputText inspStatusBinding;
    private RichInputText rejectedQtyBinding;
    private RichPopup lotRmReqTableBinding;
    private RichSelectOneChoice partBinding;
    private RichInputComboboxListOfValues itemCdBinding;
    private RichInputText itemTypeBinding;
    private RichInputText rmTypeBinding;
    private RichInputText availableQtyBinding;
    private RichInputText scrapBinding;
    private RichInputText requiredQtyBinding;
    private RichInputText defectQtyBinding;
    private RichInputText prodnQtyBinding;
    private RichInputText jobCardQtyBinding;
    private RichSelectOneChoice monthBinding;
    private RichInputText yearBinding;
    private RichInputDate startTimeBinding;
    private RichInputDate endTimeBinding;
    private RichInputText balanceQtyBinding;
    private RichInputText partLotNoBinding;
    private RichInputText toDestBinding;
    private RichInputText prevBlncProdnBinding;
    private RichInputText prevProcCdBinding;
    private RichInputText prevProcNameBinding;
    private RichInputText prevProcSeqBinding;
    private RichInputText prevProcProdnBinding;
    private RichSelectOneChoice lotGenerationBinding;
    private RichInputComboboxListOfValues fromLocationBinding;
    private RichButton populateButtonBinding;
    private RichInputComboboxListOfValues machineCdBinding;
    private RichInputComboboxListOfValues toolUsedBinding;
    private RichInputDate prodnSlipDtBinding;
    private RichInputComboboxListOfValues shiftCdBinding;
    private RichInputComboboxListOfValues processCdBinding;
    private RichInputComboboxListOfValues partCdBinding;
    private RichInputComboboxListOfValues procJobCardBinding;
    private RichInputText prodnHrsConBinding;
    private RichInputText acceptedQtyBinding;
    private RichInputComboboxListOfValues scheduleBinding;
    private RichButton saveButtonBinding;
    private RichButton saveCloseButtonBinding;
    private RichShowDetailItem partBreakUpBinding;

    public PartProductionBean() {
    }
    
    public void createShopRmAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("copyDataLotRmInPartProdn").execute();
        ADFUtils.findOperation("setProductionSlipNo").execute();
        saveButtonBinding.setDisabled(false);
        saveCloseButtonBinding.setDisabled(false);
        AdfFacesContext.getCurrentInstance().addPartialTarget(saveButtonBinding);
        AdfFacesContext.getCurrentInstance().addPartialTarget(saveCloseButtonBinding);
    }

    public void createProdnBreakdownAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert1").execute();
        ADFUtils.findOperation("setProductionSlipNo").execute();
    }

    public void createRejectionAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert2").execute();
        ADFUtils.findOperation("setProductionSlipNo").execute();
    }

    public void createOperatorAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert3").execute();
        ADFUtils.findOperation("setProductionSlipNo").execute();
    }
    public void createProdnLotAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert4").execute();
        ADFUtils.findOperation("setProductionSlipNo").execute();
    }



    public void savePartProductionSlipAL(ActionEvent actionEvent) {
     Row row=(Row)ADFUtils.evaluateEL("#{bindings.PartProductionSlipOperatorVO1.currentRow}");
     if(row!=null)
     {
        Integer i=(Integer)outputTextBinding.getValue();
            ADFUtils.setLastUpdatedBy("PartProductionSlipHeadVO1Iterator","LastUpdatedBy");
        if(i>0)
        {   
            ADFUtils.findOperation("Commit").execute();
            FacesMessage Message = new FacesMessage("Record Updated Successfully."); 
            Message.setSeverity(FacesMessage.SEVERITY_INFO); 
            FacesContext fc = FacesContext.getCurrentInstance(); 
            fc.addMessage(null, Message); 
        }
        else{
        BigDecimal RejQty=(BigDecimal)rejectedQtyBinding.getValue();
        BigDecimal rej=new BigDecimal(0);
     Integer procSeq=0,prevProcSeq1=0;
     BigDecimal prevProcSeq=new BigDecimal(0);
     String lotGen="";
     procSeq=(Integer)processSequenceBinding.getValue();
     prevProcSeq=(BigDecimal)prevProcSeqBinding.getValue();
     if(prevProcSeq!=null)
      {
     prevProcSeq1=Integer.valueOf(prevProcSeq.intValue());
       }
     lotGen=(String)lotGenerationBinding.getValue();
     System.out.println("PROC SEQ"+procSeq);
     System.out.println("PREV PROC SEQ"+prevProcSeq);
     System.out.println("LOT GENRATION VALUE"+lotGen);
        System.out.println("REJECT QTY"+RejQty);
     if(procSeq.compareTo(prevProcSeq1)==0 && lotGen.equals("FO"))
     {
         ADFUtils.showMessage("You cannot select Further Operation.This is last Further Operation.Please select either Main Store/FG Store/Production Store from Lot Generation.", 1);
     }else{
        if(RejQty.compareTo(rej)==1)
        {
           OperationBinding op= ADFUtils.findOperation("rejectionRow");
            Object rst=op.execute();
            if(op.getResult()!=null && op.getResult().toString().equalsIgnoreCase("Y"))
              {
              ADFUtils.showMessage("Rejection should be filled.", 1);              }
            if(op.getResult()!=null && op.getResult().toString().equalsIgnoreCase("R"))
              {
                  ADFUtils.showMessage("Defect Qty should be equals to Rejected Qty.", 1);
              }
            if(op.getResult()!=null && op.getResult().toString().equalsIgnoreCase("N"))
            {
                OperationBinding op1=  ADFUtils.findOperation("genarateProductionSlipNo");
                Object rst1=op1.execute();
                if(op1.getResult()!=null && op1.getResult().toString().equalsIgnoreCase("N"))
                  {
                      if (op.getErrors().isEmpty()) {
                      ADFUtils.findOperation("Commit").execute();
                      FacesMessage Message = new FacesMessage("Record Updated Successfully."); 
                      Message.setSeverity(FacesMessage.SEVERITY_INFO); 
                      FacesContext fc = FacesContext.getCurrentInstance(); 
                      fc.addMessage(null, Message); 
                  }
                }
                else if(op.getResult() != null && op.getResult().toString() != "")
                { 
                    if (op.getErrors().isEmpty()) {
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message = new FacesMessage("Record Saved Successfully.New Production Slip No. is "+rst1+"."); 
                    Message.setSeverity(FacesMessage.SEVERITY_INFO); 
                    FacesContext fc = FacesContext.getCurrentInstance(); 
                    fc.addMessage(null, Message); 
                    }
                } 
            }

        }
        else{
        OperationBinding op=  ADFUtils.findOperation("genarateProductionSlipNo");
        Object rst=op.execute();
        if(op.getResult()!=null && op.getResult().toString().equalsIgnoreCase("N"))
          {
              if (op.getErrors().isEmpty()) {
              ADFUtils.findOperation("Commit").execute();
              FacesMessage Message = new FacesMessage("Record Updated Successfully."); 
              Message.setSeverity(FacesMessage.SEVERITY_INFO); 
              FacesContext fc = FacesContext.getCurrentInstance(); 
              fc.addMessage(null, Message); 
          }
        }
        else if(op.getResult() != null && op.getResult().toString() != "")
        { 
            if (op.getErrors().isEmpty()) {
            ADFUtils.findOperation("Commit").execute();
            FacesMessage Message = new FacesMessage("Record Saved Successfully.New Production Slip No. is "+rst+"."); 
            Message.setSeverity(FacesMessage.SEVERITY_INFO); 
            FacesContext fc = FacesContext.getCurrentInstance(); 
            fc.addMessage(null, Message); 
            }
        }
        }
     } 
        }
     }else
     {
         ADFUtils.showMessage("Please Enter the Operator.", 0);
      }
 }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void editButtonAL(ActionEvent actionEvent) {
        System.out.println("PRODUCTIONSLIP NO====>"+getProdnSlipNoBinding().getValue());
        OperationBinding op=ADFUtils.findOperation("checkPartProductionQuality");
        op.getParamsMap().put("ProdSlipNo", getProdnSlipNoBinding().getValue());
        op.execute();
        if(op.getResult().equals("E"))
        {
            ADFUtils.showMessage("Part Production cannot be edited further, as its Inspection has been made.", 2);
            ADFUtils.setEL("#{pageFlowScope.mode}", "V");
        }
        else{
        cevmodecheck();
        }
//        if(inspStatusBinding.getValue()!=null)
//        {
//            if(inspStatusBinding.getValue().equals("Y"))
//            {
//                ADFUtils.setEL("#{pageFlowScope.mode}", "V");
//                ADFUtils.showMessage("Inspection has been done, This can't be Edit.", 2);
//               
//            }else
//            {
//            cevmodecheck(); 
//            }
//        }
//        else
//        {
//            cevmodecheck(); 
//        }
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }
    private void cevmodecheck(){
            if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
                cevModeDisableComponent("V");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("C");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("E");
            }
        }
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                try {
                    Method method1 =
                        component.getClass().getMethod("setDisabled", boolean.class);
                    if (method1 != null) {
                        method1.invoke(component, valueComponent);
                    }
                } catch (NoSuchMethodException e) {
                    try {
                        Method method =
                            component.getClass().getMethod("setReadOnly", boolean.class);
                        if (method != null) {
                            method.invoke(component, valueComponent);
                        }
                    } catch (Exception e1) {
                        // e.printStackTrace();//silently eat this exception.
                    }


                } catch (Exception e) {
                    // e.printStackTrace();//silently eat this exception.
                }
                List<UIComponent> childComponents = component.getChildren();
                for (UIComponent comp : childComponents) {
                    makeComponentHierarchyReadOnly(comp, valueComponent);
                }
            } 
    //Set Fields and Button disable.
        public void cevModeDisableComponent(String mode) {
            if (mode.equals("E")) {
                getScheduleDescriptionBinding().setDisabled(true);
                getHeaderEditBinding().setDisabled(true);
                getUnitCodeBinding().setDisabled(true);
                getProdnSlipNoBinding().setDisabled(true);
                getEntryDateBinding().setDisabled(true);
                getProcessSequenceBinding().setDisabled(true);
                getProcessCodeDBinding().setDisabled(true);
                getProcessSequenceDBinding().setDisabled(true);
                getInspStatusBinding().setDisabled(true);
                getRejectedQtyBinding().setDisabled(true);
                getPartBinding().setDisabled(true);
                getItemCdBinding().setDisabled(true);
                getItemTypeBinding().setDisabled(true);
                getRmTypeBinding().setDisabled(true);
                getRequiredQtyBinding().setDisabled(true);
                getAvailableQtyBinding().setDisabled(true);
                getScrapBinding().setDisabled(true);
                getMonthBinding().setDisabled(true);
                getYearBinding().setDisabled(true);
                getStartTimeBinding().setDisabled(true);
                getEndTimeBinding().setDisabled(true);
                getJobCardQtyBinding().setDisabled(true);
                getBalanceQtyBinding().setDisabled(true); 
                getPartLotNoBinding().setDisabled(true);
                getToDestBinding().setDisabled(true);
                getPrevProcCdBinding().setDisabled(true);
                getPrevProcNameBinding().setDisabled(true);
                getPrevProcSeqBinding().setDisabled(true);
                getPrevProcProdnBinding().setDisabled(true);
                getPrevBlncProdnBinding().setDisabled(true);
                getPopulateButtonBinding().setDisabled(false);
                getProdnSlipDtBinding().setDisabled(true);
                getShiftCdBinding().setDisabled(true);
                getFromLocationBinding().setDisabled(true);
                getProcessCdBinding().setDisabled(true);
                getPartCdBinding().setDisabled(true);
                getProcJobCardBinding().setDisabled(true);
                getProdnHrsConBinding().setDisabled(true);
                getProdnQtyBinding().setDisabled(true);
                getAcceptedQtyBinding().setDisabled(true);
                getScheduleBinding().setDisabled(true);
                getApprovedByBinding().setDisabled(true);
                getLotGenerationBinding().setDisabled(true);

            } else if (mode.equals("C")) {
               getProdnSlipNoBinding().setDisabled(true);
               getScheduleDescriptionBinding().setDisabled(true);
               getHeaderEditBinding().setDisabled(true);
               getUnitCodeBinding().setDisabled(true);
               getEntryDateBinding().setDisabled(true);
               getProcessSequenceBinding().setDisabled(true);
               getProcessCodeDBinding().setDisabled(true);
               getProcessSequenceDBinding().setDisabled(true);
               getInspStatusBinding().setDisabled(true);
               getRejectedQtyBinding().setDisabled(true);
               getPartBinding().setDisabled(true);
               getItemCdBinding().setDisabled(true);
               getItemTypeBinding().setDisabled(true);
               getRmTypeBinding().setDisabled(true);
               getRequiredQtyBinding().setDisabled(true);
               getAvailableQtyBinding().setDisabled(true);
               getScrapBinding().setDisabled(true);
               getMonthBinding().setDisabled(true);
               getYearBinding().setDisabled(true);
               getStartTimeBinding().setDisabled(true);
               getEndTimeBinding().setDisabled(true);
               getJobCardQtyBinding().setDisabled(true);
               getBalanceQtyBinding().setDisabled(true); 
               getPartLotNoBinding().setDisabled(true);
               getToDestBinding().setDisabled(true);
               getPrevProcCdBinding().setDisabled(true);
               getPrevProcNameBinding().setDisabled(true);
               getPrevProcSeqBinding().setDisabled(true);
               getPrevProcProdnBinding().setDisabled(true);
               getPrevBlncProdnBinding().setDisabled(true);
                saveButtonBinding.setDisabled(true);
                saveCloseButtonBinding.setDisabled(true);
            } else if (mode.equals("V")) {
                getPartRMTabBinding().setDisabled(false);
                getBreakdownTabBinding().setDisabled(false);
                getRejectionTabBinding().setDisabled(false);
                getOperatorTabBinding().setDisabled(false);
                getLotIssueButtonBinding().setDisabled(false);
                getPopulateButtonBinding().setDisabled(true);
                saveButtonBinding.setDisabled(true);
                saveCloseButtonBinding.setDisabled(true);
                partBreakUpBinding.setDisabled(false);
            }
            
        }

    public void setOutputTextBinding(RichOutputText outputTextBinding) {
        this.outputTextBinding = outputTextBinding;
    }

    public RichOutputText getOutputTextBinding() {
        cevmodecheck();
        return outputTextBinding;
    }

    public void deleteProdnShopRmDialogListener(DialogEvent dialogEvent) {

        if(dialogEvent.getOutcome().name().equals("ok"))
            {
            ADFUtils.findOperation("Delete").execute();
            System.out.println("Record Delete Successfully");
            
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");   
                Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                FacesContext fc = FacesContext.getCurrentInstance();   
                fc.addMessage(null, Message);   
            
            }

        AdfFacesContext.getCurrentInstance().addPartialTarget(prodnShopRmTableBinding);

    }

    public void setProdnShopRmTableBinding(RichTable prodnShopRmTableBinding) {
        this.prodnShopRmTableBinding = prodnShopRmTableBinding;
    }

    public RichTable getProdnShopRmTableBinding() {
        return prodnShopRmTableBinding;
    }

    public void deletePopupBreakdownDialogListener(DialogEvent dialogEvent) {

        if(dialogEvent.getOutcome().name().equals("ok"))
            {
            ADFUtils.findOperation("Delete1").execute();
            System.out.println("Record Delete Successfully");
            
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");   
                Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                FacesContext fc = FacesContext.getCurrentInstance();   
                fc.addMessage(null, Message);   
            
            }

        AdfFacesContext.getCurrentInstance().addPartialTarget(breakdownTableBinding);
    }

    public void setBreakdownTableBinding(RichTable breakdownTableBinding) {
        this.breakdownTableBinding = breakdownTableBinding;
    }

    public RichTable getBreakdownTableBinding() {
        return breakdownTableBinding;
    }

    public void deletePopupRejectionDialogListener(DialogEvent dialogEvent) {

        if(dialogEvent.getOutcome().name().equals("ok"))
            {
            ADFUtils.findOperation("Delete2").execute();
            System.out.println("Record Delete Successfully");
            
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");   
                Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                FacesContext fc = FacesContext.getCurrentInstance();   
                fc.addMessage(null, Message);   
            
            }

        AdfFacesContext.getCurrentInstance().addPartialTarget(prodnRejectionTableBinding);
    }

    public void setProdnRejectionTableBinding(RichTable prodnRejectionTableBinding) {
        this.prodnRejectionTableBinding = prodnRejectionTableBinding;
    }

    public RichTable getProdnRejectionTableBinding() {
        return prodnRejectionTableBinding;
    }

    public void deleteOperatorDialogListener(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
            {
            ADFUtils.findOperation("Delete3").execute();
            System.out.println("Record Delete Successfully");
            
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");   
                Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                FacesContext fc = FacesContext.getCurrentInstance();   
                fc.addMessage(null, Message);   
            
            }

        AdfFacesContext.getCurrentInstance().addPartialTarget(prodnOperatorTableBinding);
    }
    public void setProdnOperatorTableBinding(RichTable prodnOperatorTableBinding) {
        this.prodnOperatorTableBinding = prodnOperatorTableBinding;
    }

    public RichTable getProdnOperatorTableBinding() {
        return prodnOperatorTableBinding;
    }

    public void deletePopupLotDialogListener(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
            {
            ADFUtils.findOperation("Delete4").execute();
            System.out.println("Record Delete Successfully");
            
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");   
                Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                FacesContext fc = FacesContext.getCurrentInstance();   
                fc.addMessage(null, Message);   
            
            }

        AdfFacesContext.getCurrentInstance().addPartialTarget(prodnLotTableBinding);
    }

    public void setProdnLotTableBinding(RichTable prodnLotTableBinding) {
        this.prodnLotTableBinding = prodnLotTableBinding;
    }

    public RichTable getProdnLotTableBinding() {
        return prodnLotTableBinding;
    }

    public void setScheduleDescriptionBinding(RichInputText scheduleDescriptionBinding) {
        this.scheduleDescriptionBinding = scheduleDescriptionBinding;
    }

    public RichInputText getScheduleDescriptionBinding() {
        return scheduleDescriptionBinding;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setApprovedByBinding(RichInputComboboxListOfValues approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputComboboxListOfValues getApprovedByBinding() {
        return approvedByBinding;
    }

    public void setProdnSlipNoBinding(RichInputText prodnSlipNoBinding) {
        this.prodnSlipNoBinding = prodnSlipNoBinding;
    }

    public RichInputText getProdnSlipNoBinding() {
        return prodnSlipNoBinding;
    }

    public void setEntryDateBinding(RichInputDate entryDateBinding) {
        this.entryDateBinding = entryDateBinding;
    }

    public RichInputDate getEntryDateBinding() {
        return entryDateBinding;
    }

    public void setProcessSequenceBinding(RichInputText processSequenceBinding) {
        this.processSequenceBinding = processSequenceBinding;
    }

    public RichInputText getProcessSequenceBinding() {
        return processSequenceBinding;
    }

    public void setProcessCodeDBinding(RichInputText processCodeDBinding) {
        this.processCodeDBinding = processCodeDBinding;
    }

    public RichInputText getProcessCodeDBinding() {
        return processCodeDBinding;
    }

    public void setProcessSequenceDBinding(RichInputText processSequenceDBinding) {
        this.processSequenceDBinding = processSequenceDBinding;
    }

    public RichInputText getProcessSequenceDBinding() {
        return processSequenceDBinding;
    }

    public void setBreakdownTabBinding(RichShowDetailItem breakdownTabBinding) {
        this.breakdownTabBinding = breakdownTabBinding;
    }

    public RichShowDetailItem getBreakdownTabBinding() {
        return breakdownTabBinding;
    }

    public void setRejectionTabBinding(RichShowDetailItem rejectionTabBinding) {
        this.rejectionTabBinding = rejectionTabBinding;
    }

    public RichShowDetailItem getRejectionTabBinding() {
        return rejectionTabBinding;
    }

    public void setOperatorTabBinding(RichShowDetailItem operatorTabBinding) {
        this.operatorTabBinding = operatorTabBinding;
    }

    public RichShowDetailItem getOperatorTabBinding() {
        return operatorTabBinding;
    }

    public void setLotIssueButtonBinding(RichButton lotIssueButtonBinding) {
        this.lotIssueButtonBinding = lotIssueButtonBinding;
    }

    public RichButton getLotIssueButtonBinding() {
        return lotIssueButtonBinding;
    }

    public void setPartRMTabBinding(RichShowDetailItem partRMTabBinding) {
        this.partRMTabBinding = partRMTabBinding;
    }

    public RichShowDetailItem getPartRMTabBinding() {
        return partRMTabBinding;
    }

    public void setInspStatusBinding(RichInputText inspStatusBinding) {
        this.inspStatusBinding = inspStatusBinding;
    }

    public RichInputText getInspStatusBinding() {
        return inspStatusBinding;
    }

    public void setRejectedQtyBinding(RichInputText rejectedQtyBinding) {
        this.rejectedQtyBinding = rejectedQtyBinding;
    }

    public RichInputText getRejectedQtyBinding() {
        return rejectedQtyBinding;
    }
    
    public void setLotRmReqTableBinding(RichPopup lotRmReqTableBinding) {
        this.lotRmReqTableBinding = lotRmReqTableBinding;
    }

    public RichPopup getLotRmReqTableBinding() {
        return lotRmReqTableBinding;
    }

    public void setPartBinding(RichSelectOneChoice partBinding) {
        this.partBinding = partBinding;
    }

    public RichSelectOneChoice getPartBinding() {
        return partBinding;
    }

    public void setItemCdBinding(RichInputComboboxListOfValues itemCdBinding) {
        this.itemCdBinding = itemCdBinding;
    }

    public RichInputComboboxListOfValues getItemCdBinding() {
        return itemCdBinding;
    }

    public void setItemTypeBinding(RichInputText itemTypeBinding) {
        this.itemTypeBinding = itemTypeBinding;
    }

    public RichInputText getItemTypeBinding() {
        return itemTypeBinding;
    }

    public void setRmTypeBinding(RichInputText rmTypeBinding) {
        this.rmTypeBinding = rmTypeBinding;
    }

    public RichInputText getRmTypeBinding() {
        return rmTypeBinding;
    }

    public void setAvailableQtyBinding(RichInputText availableQtyBinding) {
        this.availableQtyBinding = availableQtyBinding;
    }

    public RichInputText getAvailableQtyBinding() {
        return availableQtyBinding;
    }

    public void setScrapBinding(RichInputText scrapBinding) {
        this.scrapBinding = scrapBinding;
    }

    public RichInputText getScrapBinding() {
        return scrapBinding;
    }

    public void setRequiredQtyBinding(RichInputText requiredQtyBinding) {
        this.requiredQtyBinding = requiredQtyBinding;
    }

    public RichInputText getRequiredQtyBinding() {
        return requiredQtyBinding;
    }

    public void setDefectQtyBinding(RichInputText defectQtyBinding) {
        this.defectQtyBinding = defectQtyBinding;
    }

    public RichInputText getDefectQtyBinding() {
        return defectQtyBinding;
    }

    public void acceptedQuantityValidator(FacesContext facesContext, UIComponent uIComponent, Object ob) {
       BigDecimal ProdnQty=(BigDecimal)prodnQtyBinding.getValue();
       BigDecimal acceptedQty=(BigDecimal)ob;
        BigDecimal val=new BigDecimal(0);
        if(acceptedQty != null)
        {
        if(acceptedQty.compareTo(val)==-1)
        {
            FacesMessage msg=new FacesMessage(FacesMessage.SEVERITY_ERROR, "Accepted Qty must be equals to greater than 0.", null);
            FacesContext fc = FacesContext.getCurrentInstance(); 
            fc.addMessage(null, msg); 
            throw new ValidatorException(msg);
          //  throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Accepted Qty must be equals to greater than 0.", null));

        }
       if(acceptedQty.compareTo(ProdnQty)==1)
       {
            FacesMessage msg=new FacesMessage(FacesMessage.SEVERITY_ERROR, "Accepted Qty cannot be greater than Production Qty.", null);
            FacesContext fc = FacesContext.getCurrentInstance(); 
            fc.addMessage(null, msg); 
            throw new ValidatorException(msg);
           // throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Accepted Qty cannot be greater than Production Qty.", null));
        }
        }

    }

    public void setProdnQtyBinding(RichInputText prodnQtyBinding) {
        this.prodnQtyBinding = prodnQtyBinding;
    }

    public RichInputText getProdnQtyBinding() {
        return prodnQtyBinding;
    }

    public void setJobCardQtyBinding(RichInputText jobCardQtyBinding) {
        this.jobCardQtyBinding = jobCardQtyBinding;
    }

    public RichInputText getJobCardQtyBinding() {
        return jobCardQtyBinding;
    }

    public void productionQtyValidator(FacesContext facesContext, UIComponent uIComponent, Object ob) {
        BigDecimal BlncQty=(BigDecimal)balanceQtyBinding.getValue();
        BigDecimal PrevBlncQty=(BigDecimal)prevBlncProdnBinding.getValue();
        BigDecimal ProductionQty=(BigDecimal)ob;
        BigDecimal val=new BigDecimal(0);
        if(ProductionQty != null)
        {
        if(ProductionQty.compareTo(val)==-1)
        {
            FacesMessage msg=new FacesMessage(FacesMessage.SEVERITY_ERROR, "Production Qty must be equals to greater than 0.", null);
            FacesContext fc = FacesContext.getCurrentInstance(); 
            fc.addMessage(null, msg); 
            throw new ValidatorException(msg);
           // throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Production Qty must be equals to greater than 0.", null));

        }
        if(ProductionQty.compareTo(BlncQty)==1)
        {
             FacesMessage msg=new FacesMessage(FacesMessage.SEVERITY_ERROR, "Production Qty cannot be greater than Balance Quantity.", null);
             FacesContext fc = FacesContext.getCurrentInstance(); 
             fc.addMessage(null, msg); 
             throw new ValidatorException(msg);
             //throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Production Qty cannot be greater than Balance Quantity.", null));
         }
        if(PrevBlncQty!=null)
        {
        if(ProductionQty.compareTo(PrevBlncQty)==1)
        {
             FacesMessage msg=new FacesMessage(FacesMessage.SEVERITY_ERROR, "Production Qty cannot be greater than Previous Balance Prodn.", null);
             FacesContext fc = FacesContext.getCurrentInstance(); 
             fc.addMessage(null, msg); 
             throw new ValidatorException(msg);
            // throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Production Qty cannot be greater than Previous Balance Prodn.", null));
         }
        }
        }

    }

    public void setMonthBinding(RichSelectOneChoice monthBinding) {
        this.monthBinding = monthBinding;
    }

    public RichSelectOneChoice getMonthBinding() {
        return monthBinding;
    }

    public void setYearBinding(RichInputText yearBinding) {
        this.yearBinding = yearBinding;
    }

    public RichInputText getYearBinding() {
        return yearBinding;
    }

    public void setStartTimeBinding(RichInputDate startTimeBinding) {
        this.startTimeBinding = startTimeBinding;
    }

    public RichInputDate getStartTimeBinding() {
        return startTimeBinding;
    }

    public void setEndTimeBinding(RichInputDate endTimeBinding) {
        this.endTimeBinding = endTimeBinding;
    }

    public RichInputDate getEndTimeBinding() {
        return endTimeBinding;
    }

    public void setBalanceQtyBinding(RichInputText balanceQtyBinding) {
        this.balanceQtyBinding = balanceQtyBinding;
    }

    public RichInputText getBalanceQtyBinding() {
        return balanceQtyBinding;
    }

    public void defectQtyValidator(FacesContext facesContext, UIComponent uIComponent, Object ob) {
        BigDecimal defectQty=new BigDecimal(0);
        defectQty=(BigDecimal)ob;
        BigDecimal reqQty=new BigDecimal(0);
        reqQty=(BigDecimal)rejectedQtyBinding.getValue();
        BigDecimal val=new BigDecimal(0);
        if(defectQty.compareTo(val)==-1)
        {
            FacesMessage msg=new FacesMessage(FacesMessage.SEVERITY_ERROR, "Defect Quantity must be equals to or greater than 0.", null);
            FacesContext fc = FacesContext.getCurrentInstance(); 
            fc.addMessage(null, msg); 
            throw new ValidatorException(msg);           
            //throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Defect Quantity must be equals to or greater than 0.", null));
 
        }
        if(defectQty.compareTo(reqQty)==1)
        {
            FacesMessage msg=new FacesMessage(FacesMessage.SEVERITY_ERROR, "Defect Quantity cannot be greater than Rejected Quantity.", null);
            FacesContext fc = FacesContext.getCurrentInstance(); 
            fc.addMessage(null, msg); 
            throw new ValidatorException(msg);  
            //throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Defect Quantity cannot be greater than Rejected Quantity.", null));

        }
    }

    public void processCodeVCL(ValueChangeEvent vcl) {
        ADFUtils.findOperation("inspectionStatus").execute();
        if(vcl.getNewValue()!=null)
        {
            getProcessCdBinding().setDisabled(true);
        }
    }

    public void setPartLotNoBinding(RichInputText partLotNoBinding) {
        this.partLotNoBinding = partLotNoBinding;
    }

    public RichInputText getPartLotNoBinding() {
        return partLotNoBinding;
    }

    public void setToDestBinding(RichInputText toDestBinding) {
        this.toDestBinding = toDestBinding;
    }

    public RichInputText getToDestBinding() {
        return toDestBinding;
    }

    public void partLotNoToDestVCL(ValueChangeEvent vcl) {
       ADFUtils.findOperation("partLotNoGenerationPartProd").execute();
        ADFUtils.findOperation("toDestGenerationPartProd").execute();
        ADFUtils.findOperation("lotGenerationPartProd").execute(); 
        BigDecimal ProdnQty=new BigDecimal(0);
        BigDecimal Val=new BigDecimal(0);
        ProdnQty=(BigDecimal)vcl.getNewValue();
        if(ProdnQty!=null && ProdnQty.compareTo(Val)!=-1)
        {
            acceptedQtyBinding.setValue(ProdnQty);
            rejectedQtyBinding.setValue(Val);
            AdfFacesContext.getCurrentInstance().addPartialTarget(acceptedQtyBinding);
        }
       
    }

    public void prevProductionValueVCL(ValueChangeEvent vcl) {
        vcl.getComponent().processUpdates(FacesContext.getCurrentInstance());
        ADFUtils.findOperation("prevProcCdPartProd").execute();
        ADFUtils.findOperation("prevProductionPartProd").execute();
        ADFUtils.findOperation("prevBalancePartProd").execute();
        if(vcl.getNewValue()!=null)
        {
            getProcJobCardBinding().setDisabled(true);
        }

    }

    public void setPrevBlncProdnBinding(RichInputText prevBlncProdnBinding) {
        this.prevBlncProdnBinding = prevBlncProdnBinding;
    }

    public RichInputText getPrevBlncProdnBinding() {
        return prevBlncProdnBinding;
    }

    public void setPrevProcCdBinding(RichInputText prevProcCdBinding) {
        this.prevProcCdBinding = prevProcCdBinding;
    }

    public RichInputText getPrevProcCdBinding() {
        return prevProcCdBinding;
    }

    public void setPrevProcNameBinding(RichInputText prevProcNameBinding) {
        this.prevProcNameBinding = prevProcNameBinding;
    }

    public RichInputText getPrevProcNameBinding() {
        return prevProcNameBinding;
    }

    public void setPrevProcSeqBinding(RichInputText prevProcSeqBinding) {
        this.prevProcSeqBinding = prevProcSeqBinding;
    }

    public RichInputText getPrevProcSeqBinding() {
        return prevProcSeqBinding;
    }

    public void setPrevProcProdnBinding(RichInputText prevProcProdnBinding) {
        this.prevProcProdnBinding = prevProcProdnBinding;
    }

    public RichInputText getPrevProcProdnBinding() {
        return prevProcProdnBinding;
    }

    public void setLotGenerationBinding(RichSelectOneChoice lotGenerationBinding) {
        this.lotGenerationBinding = lotGenerationBinding;
    }

    public RichSelectOneChoice getLotGenerationBinding() {
        return lotGenerationBinding;
    }

    public void lotGenerationVCL(ValueChangeEvent vce) {
       String lotGen=(String)vce.getNewValue();
       System.out.println("LOT GENERATION VALUE"+lotGen);
       if(lotGen.equals("FG"))
       {
           toDestBinding.setValue("FG");          
       }
        if(lotGen.equals("MS"))
        {
            toDestBinding.setValue("MS");          
        }
        if(lotGen.equals("PS"))
        {
            System.out.println("FROM LOC"+fromLocationBinding.getValue());;
            toDestBinding.setValue(fromLocationBinding.getValue());          
        }
    }

    public void setFromLocationBinding(RichInputComboboxListOfValues fromLocationBinding) {
        this.fromLocationBinding = fromLocationBinding;
    }

    public RichInputComboboxListOfValues getFromLocationBinding() {
        return fromLocationBinding;
    }

    public void setPopulateButtonBinding(RichButton populateButtonBinding) {
        this.populateButtonBinding = populateButtonBinding;
    }

    public RichButton getPopulateButtonBinding() {
        return populateButtonBinding;
    }

    public void setMachineCdBinding(RichInputComboboxListOfValues machineCdBinding) {
        this.machineCdBinding = machineCdBinding;
    }

    public RichInputComboboxListOfValues getMachineCdBinding() {
        return machineCdBinding;
    }

    public void setToolUsedBinding(RichInputComboboxListOfValues toolUsedBinding) {
        this.toolUsedBinding = toolUsedBinding;
    }

    public RichInputComboboxListOfValues getToolUsedBinding() {
        return toolUsedBinding;
    }

    public void setProdnSlipDtBinding(RichInputDate prodnSlipDtBinding) {
        this.prodnSlipDtBinding = prodnSlipDtBinding;
    }

    public RichInputDate getProdnSlipDtBinding() {
        return prodnSlipDtBinding;
    }

    public void setShiftCdBinding(RichInputComboboxListOfValues shiftCdBinding) {
        this.shiftCdBinding = shiftCdBinding;
    }

    public RichInputComboboxListOfValues getShiftCdBinding() {
        return shiftCdBinding;
    }

    public void setProcessCdBinding(RichInputComboboxListOfValues processCdBinding) {
        this.processCdBinding = processCdBinding;
    }

    public RichInputComboboxListOfValues getProcessCdBinding() {
        return processCdBinding;
    }

    public void setPartCdBinding(RichInputComboboxListOfValues partCdBinding) {
        this.partCdBinding = partCdBinding;
    }

    public RichInputComboboxListOfValues getPartCdBinding() {
        return partCdBinding;
    }

    public void setProcJobCardBinding(RichInputComboboxListOfValues procJobCardBinding) {
        this.procJobCardBinding = procJobCardBinding;
    }

    public RichInputComboboxListOfValues getProcJobCardBinding() {
        return procJobCardBinding;
    }

    public void setProdnHrsConBinding(RichInputText prodnHrsConBinding) {
        this.prodnHrsConBinding = prodnHrsConBinding;
    }

    public RichInputText getProdnHrsConBinding() {
        return prodnHrsConBinding;
    }

    public void setAcceptedQtyBinding(RichInputText acceptedQtyBinding) {
        this.acceptedQtyBinding = acceptedQtyBinding;
    }

    public RichInputText getAcceptedQtyBinding() {
        return acceptedQtyBinding;
    }

    public void setScheduleBinding(RichInputComboboxListOfValues scheduleBinding) {
        this.scheduleBinding = scheduleBinding;
    }

    public RichInputComboboxListOfValues getScheduleBinding() {
        return scheduleBinding;
    }

    public void fromLocationVCL(ValueChangeEvent vce) {
        if(vce.getNewValue()!=null)
        {
            fromLocationBinding.setDisabled(true);
        }
    }

    public void partCdVCL(ValueChangeEvent vcl) {
        vcl.getComponent().processUpdates(FacesContext.getCurrentInstance());
        System.out.println("PartCd==>"+vcl.getNewValue()+"ProcCd==>"+processCdBinding.getValue()+"ProcSeqNo==>"+processSequenceBinding.getValue());
        if(vcl.getNewValue()!=null && processCdBinding.getValue()!=null && processSequenceBinding.getValue()!=null)
        {
            System.out.println("PartCd==>"+vcl.getNewValue()+"ProcCd==>"+processCdBinding.getValue()+"ProcSeqNo==>"+processSequenceBinding.getValue());
           OperationBinding op=(OperationBinding)ADFUtils.findOperation("procInputAvailFunctionCheck");
           op.getParamsMap().put("PartCd", vcl.getNewValue());
           op.getParamsMap().put("ProcCd", processCdBinding.getValue());
           op.getParamsMap().put("ProcSeq", processSequenceBinding.getValue());
           op.execute();
           System.out.println("Avail Save==>"+op.getResult());
           if(op.getResult()!=null && op.getResult().equals("Y"))
           {
               saveButtonBinding.setDisabled(false);
               saveCloseButtonBinding.setDisabled(false);
               AdfFacesContext.getCurrentInstance().addPartialTarget(saveButtonBinding);
               AdfFacesContext.getCurrentInstance().addPartialTarget(saveCloseButtonBinding);
           }
           getPartCdBinding().setDisabled(true);
        }
    }

    public void setSaveButtonBinding(RichButton saveButtonBinding) {
        this.saveButtonBinding = saveButtonBinding;
    }

    public RichButton getSaveButtonBinding() {
        return saveButtonBinding;
    }

    public void setSaveCloseButtonBinding(RichButton saveCloseButtonBinding) {
        this.saveCloseButtonBinding = saveCloseButtonBinding;
    }

    public RichButton getSaveCloseButtonBinding() {
        return saveCloseButtonBinding;
    }

    public String saveCloseButtonAL() {
        Row row=(Row)ADFUtils.evaluateEL("#{bindings.PartProductionSlipOperatorVO1.currentRow}");
        if(row!=null)
        {
           Integer i=(Integer)outputTextBinding.getValue();
               ADFUtils.setLastUpdatedBy("PartProductionSlipHeadVO1Iterator","LastUpdatedBy");
           if(i>0)
           {   
               ADFUtils.findOperation("Commit").execute();
               FacesMessage Message = new FacesMessage("Record Updated Successfully."); 
               Message.setSeverity(FacesMessage.SEVERITY_INFO); 
               FacesContext fc = FacesContext.getCurrentInstance(); 
               fc.addMessage(null, Message);
               return "saveandclose";
           }
           else{
           BigDecimal RejQty=(BigDecimal)rejectedQtyBinding.getValue();
           BigDecimal rej=new BigDecimal(0);
        Integer procSeq=0,prevProcSeq1=0;
        BigDecimal prevProcSeq=new BigDecimal(0);
        String lotGen="";
        procSeq=(Integer)processSequenceBinding.getValue();
        prevProcSeq=(BigDecimal)prevProcSeqBinding.getValue();
        if(prevProcSeq!=null)
         {
        prevProcSeq1=Integer.valueOf(prevProcSeq.intValue());
          }
        lotGen=(String)lotGenerationBinding.getValue();
        System.out.println("PROC SEQ"+procSeq);
        System.out.println("PREV PROC SEQ"+prevProcSeq);
        System.out.println("LOT GENRATION VALUE"+lotGen);
           System.out.println("REJECT QTY"+RejQty);
        if(procSeq.compareTo(prevProcSeq1)==0 && lotGen.equals("FO"))
        {
            ADFUtils.showMessage("You cannot select Further Operation.This is last Further Operation.Please select either Main Store/FG Store/Production Store from Lot Generation.", 1);
        }else{
           if(RejQty.compareTo(rej)==1)
           {
              OperationBinding op= ADFUtils.findOperation("rejectionRow");
               Object rst=op.execute();
               if(op.getResult()!=null && op.getResult().toString().equalsIgnoreCase("Y"))
                 {
                 ADFUtils.showMessage("Rejection should be filled.", 1);              }
               if(op.getResult()!=null && op.getResult().toString().equalsIgnoreCase("R"))
                 {
                     ADFUtils.showMessage("Defect Qty should be equals to Rejected Qty.", 1);
                 }
               if(op.getResult()!=null && op.getResult().toString().equalsIgnoreCase("N"))
               {
                   OperationBinding op1=  ADFUtils.findOperation("genarateProductionSlipNo");
                   Object rst1=op1.execute();
                   if(op1.getResult()!=null && op1.getResult().toString().equalsIgnoreCase("N"))
                     {
                         if (op.getErrors().isEmpty()) {
                         ADFUtils.findOperation("Commit").execute();
                         FacesMessage Message = new FacesMessage("Record Updated Successfully."); 
                         Message.setSeverity(FacesMessage.SEVERITY_INFO); 
                         FacesContext fc = FacesContext.getCurrentInstance(); 
                         fc.addMessage(null, Message); 
                         return "saveandclose";
                     }
                   }
                   else if(op.getResult() != null && op.getResult().toString() != "")
                   { 
                       if (op.getErrors().isEmpty()) {
                       ADFUtils.findOperation("Commit").execute();
                       FacesMessage Message = new FacesMessage("Record Saved Successfully.New Production Slip No. is "+rst1+"."); 
                       Message.setSeverity(FacesMessage.SEVERITY_INFO); 
                       FacesContext fc = FacesContext.getCurrentInstance(); 
                       fc.addMessage(null, Message); 
                           return "saveandclose";
                       }
                   } 
               }

           }
           else{
           OperationBinding op=  ADFUtils.findOperation("genarateProductionSlipNo");
           Object rst=op.execute();
           if(op.getResult()!=null && op.getResult().toString().equalsIgnoreCase("N"))
             {
                 if (op.getErrors().isEmpty()) {
                 ADFUtils.findOperation("Commit").execute();
                 FacesMessage Message = new FacesMessage("Record Updated Successfully."); 
                 Message.setSeverity(FacesMessage.SEVERITY_INFO); 
                 FacesContext fc = FacesContext.getCurrentInstance(); 
                 fc.addMessage(null, Message); 
                 return "saveandclose";
             }
           }
           else if(op.getResult() != null && op.getResult().toString() != "")
           { 
               if (op.getErrors().isEmpty()) {
               ADFUtils.findOperation("Commit").execute();
               FacesMessage Message = new FacesMessage("Record Saved Successfully.New Production Slip No. is "+rst+"."); 
               Message.setSeverity(FacesMessage.SEVERITY_INFO); 
               FacesContext fc = FacesContext.getCurrentInstance(); 
               fc.addMessage(null, Message); 
                   return "saveandclose";
               }
           }
           }
        } 
           }
        }else
        {
            ADFUtils.showMessage("Please Enter the Operator.", 0);
         }
        return null;
    }

    public void setPartBreakUpBinding(RichShowDetailItem partBreakUpBinding) {
        this.partBreakUpBinding = partBreakUpBinding;
    }

    public RichShowDetailItem getPartBreakUpBinding() {
        return partBreakUpBinding;
    }
}
