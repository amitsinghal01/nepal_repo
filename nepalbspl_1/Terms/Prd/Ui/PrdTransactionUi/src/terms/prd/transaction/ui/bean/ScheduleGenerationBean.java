package terms.prd.transaction.ui.bean;

import java.util.Calendar;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.validator.ValidatorException;

import oracle.adf.model.OperationBinding;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.event.DialogEvent;

public class ScheduleGenerationBean {
    private RichPopup popupBinding;
    private String editAction="L";
    private RichOutputText outputTextBinding;
    private RichSelectOneChoice monthBinding;
    private RichInputText yearBinding;

    public ScheduleGenerationBean() {
    }

    public void ScheduleGenerateAL(ActionEvent actionEvent) {
    setEditAction("V");
    if (getEditAction().equals("V")){
        outputTextBinding.setValue("Generating Schedule");
    }    
       
        OperationBinding op = (OperationBinding) ADFUtils.findOperation("scheduleGeneration");
        Object Obj=op.execute();
        System.out.println("*************"+op.getResult());
              if(op.getResult()!= null &&op.getResult()!= "" && op.getResult().equals("C")){
                  System.out.println("intoif");
                      RichPopup.PopupHints hints = new RichPopup.PopupHints();
                     popupBinding.show(hints);
    }
              else
              {
                      OperationBinding opp = (OperationBinding) ADFUtils.findOperation("generationByFunction");
                      Object obj=opp.execute();
                      setEditAction("C");
                      if (getEditAction().equals("C"))
                      {
                          outputTextBinding.setValue("Schedule Generated");
                      }
                      
                  }
}

    public void setPopupBinding(RichPopup popupBinding) {
        this.popupBinding = popupBinding;
    }

    public RichPopup getPopupBinding() {
        return popupBinding;
    }

    public void Regeneration(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("yes"))
             {
                 OperationBinding opp = (OperationBinding) ADFUtils.findOperation("generationByFunction");
                 Object obj=opp.execute();
                 setEditAction("C");
                 if (getEditAction().equals("C"))
                 {
                     outputTextBinding.setValue("Schedule Generated");
                 }
                
             }
        else{
                    setEditAction("Z");
                            popupBinding.hide();
                          
                            if (getEditAction().equals("Z"))
                            {
                           
                            outputTextBinding.setValue("Schedule Generation Failed");
                        }
                    popupBinding.hide();
                    
                }
    }

    public void setOutputTextBinding(RichOutputText outputTextBinding) {
        this.outputTextBinding = outputTextBinding;
    }

    public RichOutputText getOutputTextBinding() {
        return outputTextBinding;
    }
    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }
    public void isEnableEdit(ActionEvent actionEvent) {
       this.editAction=getEditAction();
    }

    public void monthValidator(FacesContext facesContext, UIComponent uIComponent, Object ob) {
        if (ob != null && yearBinding.getValue() != null) {
           
            Integer MonthCase = 0;
            if   (ob.toString().equals("JAN")) {
                MonthCase = 1;
            } else if (ob.toString().equals("FEB")) {
                MonthCase = 2;
            } else if (ob.toString().equals("MAR")) {
                MonthCase = 3;
            } else if (ob.toString().equals("APR")) {
                MonthCase = 4;
            } else if (ob.toString().equals("MAY")) {
                MonthCase = 5;
            } else if (ob.toString().equals("JUN")) {
                MonthCase = 6;
            } else if (ob.toString().equals("JUL")) {
                MonthCase = 7;
            } else if (ob.toString().equals("AUG")) {
                MonthCase = 8;
            } else if (ob.toString().equals("SEP")) {
                MonthCase = 9;
            } else if (ob.toString().equals("OCT")) {
                MonthCase = 10;
            } else if (ob.toString().equals("NOV")) {
                MonthCase = 11;
            } else if (ob.toString().equals("DEC")) {
                MonthCase = 12;
            } else {
                System.out.println("No Month Found");
            }
            Integer j = 1;
            j = j + (Integer) Calendar.getInstance().get(Calendar.MONTH);
            System.out.println("CURRENT MONTH"    + j +   "GIVEN MONTH" + MonthCase);
            Integer year1 = (Integer) Calendar.getInstance().get(Calendar.YEAR);
            Integer y=(Integer)getYearBinding().getValue();
            System.out.println("CURRENT YEAR IS BY SYSTEM*********** "+year1);
            System.out.println("CURRENT YEAR IS BY USER*********** "+y);
            if (y.compareTo(year1)==1 )
            {}else{
                if(MonthCase.compareTo(j) == -1)
                {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,  "'Back Month Plan Can Not Be Generated", null));
            }
         
           
        }
        }

    }

    public void yearValidator(FacesContext facesContext, UIComponent uIComponent, Object ob) {
        Integer year = (Integer) ob;
        Integer year1 = (Integer) Calendar.getInstance().get(Calendar.YEAR);
        System.out.println("CURRENT YEAR ENTER" + year);
        System.out.println("CURRENT YEAR SYSTEMDATE" + year1);
        if (year.compareTo(year1) == -1) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                          "Back Year Plan Can Not Be Generated", null));
        }


    }

    public void setMonthBinding(RichSelectOneChoice monthBinding) {
        this.monthBinding = monthBinding;
    }

    public RichSelectOneChoice getMonthBinding() {
        return monthBinding;
    }

    public void setYearBinding(RichInputText yearBinding) {
        this.yearBinding = yearBinding;
    }

    public RichInputText getYearBinding() {
        return yearBinding;
    }
}
