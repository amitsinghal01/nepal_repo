package terms.prd.transaction.ui.bean;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;

import java.util.HashMap;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.io.FileInputStream;

import java.sql.Connection;

import java.sql.SQLException;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;

import javax.servlet.http.HttpServletResponse;

import javax.sql.DataSource;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

public class SearchProductionEntryBean {
    private String ReqMode ="";
    private RichTable searchProductionEntryTableBinding;

    public SearchProductionEntryBean() {
    }

    public void deletePopUpDialogListener(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            OperationBinding op = null;
            ADFUtils.findOperation("Delete").execute();
            op = (OperationBinding) ADFUtils.findOperation("Commit");
            Object rst = op.execute();
            System.out.println("Record Delete Successfully");
            if (op.getErrors().isEmpty()) {
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            } else if (!op.getErrors().isEmpty()) {
                OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
                Object rstr = opr.execute();
                FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(searchProductionEntryTableBinding);
    }

    public void setSearchProductionEntryTableBinding(RichTable searchProductionEntryTableBinding) {
        this.searchProductionEntryTableBinding = searchProductionEntryTableBinding;
    }

    public RichTable getSearchProductionEntryTableBinding() {
        return searchProductionEntryTableBinding;
    }
    
    public BindingContainer getBindings()
          {
            return BindingContext.getCurrent().getCurrentBindingsEntry();
          }
        
        public void downloadJrxml(FacesContext facesContext, OutputStream outputStream) throws FileNotFoundException {
            Connection conn=null;
            try {
            String file_name ="";
            System.out.println("ReqMode===================>"+ReqMode);
            
            if(ReqMode.equals("SP")) 
            {
                file_name="r_reqps.jasper";
            }
            else if(ReqMode.equals("MP"))
            {
                file_name="r_reqp.jasper";
            }
        
            System.out.println("After Set Value File Name is===>"+file_name);
            
            
            OperationBinding binding = ADFUtils.findOperation("fileNameForPrint");
            binding.getParamsMap().put("fileId","ERP0000001856");
            binding.execute();
            System.out.println("Binding Result :"+binding.getResult());
            if(binding.getResult() != null){
                String result = binding.getResult().toString();
                int last_index = result.lastIndexOf("/");
                String path = result.substring(0,last_index+1)+file_name;
                System.out.println("FILE PATH IS===>"+path);
                InputStream input = new FileInputStream(path);
                String LV_SID=oracle.jbo.server.uniqueid.UniqueIdHelper.getNextId().toString();
                System.out.println("SID======>"+LV_SID);

                DCIteratorBinding ppIter = (DCIteratorBinding) getBindings().get("SearchProductionEntryHeaderVO1Iterator");
                System.out.println("Unit Code #### " + ppIter.getCurrentRow().getAttribute("UnitCd"));
                String unitCode = ppIter.getCurrentRow().getAttribute("UnitCd").toString();
                String planNo = ppIter.getCurrentRow().getAttribute("EntryNo").toString();
                            
                System.out.println("Entry No. :- "+planNo+" "+ "Unit Code " + unitCode);
                
                Map n = new HashMap();
                
                if((ReqMode.equals("SP")) && (ADFUtils.resolveExpression("#{pageFlowScope.unitCode}").equals("10003")))
                {
                    System.out.println("INSIDE IF #### " + ReqMode);
                    n.put("p_entryno", planNo);
                    n.put("p_unit", unitCode);
                
                }
                else if((ReqMode.equals("MP")) && (ADFUtils.resolveExpression("#{pageFlowScope.unitCode}").equals("10004")))
                {
                    System.out.println("INSIDE ELSE #### "+ReqMode);
                    n.put("p_entryno", planNo);
                    n.put("p_unit", unitCode);
                    
                }
                
                    conn = getConnection( );
                    
                    JasperReport design = (JasperReport) JRLoader.loadObject(input);
                    System.out.println("Path : " + input + " -------" + design+" param:"+n);
                    
                    @SuppressWarnings("unchecked")
                    net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                    byte pdf[] = JasperExportManager.exportReportToPdf(print);
                    HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                    response.getOutputStream().write(pdf);
                    response.getOutputStream().flush();
                    response.getOutputStream().close();
                    facesContext.responseComplete();
                    }else
                    System.out.println("File Name/File Path not found.");
                    } catch (FileNotFoundException fnfe) {
                    // TODO: Add catch code
                    fnfe.printStackTrace();
                    try {
                        System.out.println("in finally connection closed");
                                conn.close( );
                                conn = null;
                        
                    } catch( SQLException e ) {
                        e.printStackTrace( );
                    }
                    } catch (Exception e) {
                    // TODO: Add catch code
                    e.printStackTrace();
                    try {
                        System.out.println("in finally connection closed");
                                conn.close( );
                                conn = null;
                        
                    } catch( SQLException e1 ) {
                        e1.printStackTrace( );
                    }
                    }finally {
                            try {
                                    System.out.println("in finally connection closed");
                                            conn.close( );
                                            conn = null;
                                    
                            } catch( SQLException e ) {
                                    e.printStackTrace( );
                            }
                    }
        }

    public void setReqMode(String ReqMode) {
        this.ReqMode = ReqMode;
    }

    public String getReqMode() {
        return ReqMode;
    }
    public Connection getConnection() throws Exception{
            Context ctx = null;
            Connection conn = null;
            ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/ApplicationDBDS");
            conn = ds.getConnection();
            return conn;
        }
}
