package terms.prd.transaction.ui.bean;

import java.util.Calendar;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.validator.ValidatorException;

import oracle.adf.model.OperationBinding;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.event.DialogEvent;

import org.apache.myfaces.trinidad.event.AttributeChangeEvent;

public class ShortageGenerationBean {
    private String editAction="L";
    private RichPopup popupBinding;
    private RichOutputText outputTextBinding;
    private RichSelectOneChoice monthBinding;
    private RichInputText yearBinding;
    private RichInputComboboxListOfValues unitCdBinding;

    public ShortageGenerationBean() {
    }

    public void ShortageGenerationAL(ActionEvent actionEvent) {
//        setEditAction("V");
        
        OperationBinding op1 = (OperationBinding) ADFUtils.findOperation("monthYearValidationForShortageGeneration");
        op1.execute();
        System.out.println("Inside op1 #### " + op1.getResult());
        
        if(op1.getResult()!= null && op1.getResult() != "" && op1.getResult().equals("Y")) {
            System.out.println("Inside op1.getResult() #### " + op1.getResult());
            
            setEditAction("V");
            
            if (getEditAction().equals("V")){
            outputTextBinding.setValue("Generating CO");
            }
            OperationBinding op = (OperationBinding) ADFUtils.findOperation("COGeneration");
            Object Obj=op.execute();
            System.out.println("*************"+op.getResult());
                 if(op.getResult()!= null &&op.getResult()!= "" && op.getResult().equals("C")){
                     System.out.println("intoif");
                         RichPopup.PopupHints hints = new RichPopup.PopupHints();
                        popupBinding.show(hints);
            }
                 else
                 {
                         OperationBinding opp = (OperationBinding) ADFUtils.findOperation("COgenerationByFunction");
                         Object obj=opp.execute();
                         setEditAction("C");
                         if (getEditAction().equals("C"))
                         {
                         outputTextBinding.setValue("Shortage Generated");
                     }
                 }
        }
//        ADFUtils.findOperation("monthYearValidationForShortageGeneration").execute();
        else {
            ADFUtils.showMessage("Back Month and Year Plan Can Not Be Generated", 0);
        }
        
//    yearBinding.setValue(null);
//    monthBinding.setValue(null);
}

    public void Regeneration(DialogEvent dialogEvent) {
            if(dialogEvent.getOutcome().name().equals("yes"))
                 {
                     OperationBinding opp = (OperationBinding) ADFUtils.findOperation("COgenerationByFunction");
                     Object obj=opp.execute();
                     setEditAction("C");
                     if (getEditAction().equals("C"))
                     {
                     outputTextBinding.setValue("Shortage Generated");
                     }
                 }
            else{
                setEditAction("Z");
                        popupBinding.hide();
                      
                        if (getEditAction().equals("Z"))
                        {
                       
                        outputTextBinding.setValue("Shortage Generation Failed");
                    }
            }
            }

    public void setPopupBinding(RichPopup popupBinding) {
        this.popupBinding = popupBinding;
    }

    public RichPopup getPopupBinding() {
        return popupBinding;
    }

    public void setOutputTextBinding(RichOutputText outputTextBinding) {
        this.outputTextBinding = outputTextBinding;
    }

    public RichOutputText getOutputTextBinding() {
        return outputTextBinding;
    }
    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }
    public void isEnableEdit(ActionEvent actionEvent) {
        // Add event code here...
        this.editAction=getEditAction();
    }

    public void monthValidator(FacesContext facesContext, UIComponent uIComponent, Object ob) {
        if (ob != null && yearBinding.getValue() != null) {
           
            Integer MonthCase = 0;
            if   (ob.toString().equals("JAN")) {
                MonthCase = 1;
            } else if (ob.toString().equals("FEB")) {
                MonthCase = 2;
            } else if (ob.toString().equals("MAR")) {
                MonthCase = 3;
            } else if (ob.toString().equals("APR")) {
                MonthCase = 4;
            } else if (ob.toString().equals("MAY")) {
                MonthCase = 5;
            } else if (ob.toString().equals("JUN")) {
                MonthCase = 6;
            } else if (ob.toString().equals("JUL")) {
                MonthCase = 7;
            } else if (ob.toString().equals("AUG")) {
                MonthCase = 8;
            } else if (ob.toString().equals("SEP")) {
                MonthCase = 9;
            } else if (ob.toString().equals("OCT")) {
                MonthCase = 10;
            } else if (ob.toString().equals("NOV")) {
                MonthCase = 11;
            } else if (ob.toString().equals("DEC")) {
                MonthCase = 12;
            } else {
                System.out.println("No Month Found");
            }
            Integer j = 1;
            j = j + (Integer) Calendar.getInstance().get(Calendar.MONTH);
            System.out.println("CURRENT MONTH"    + j +   "GIVEN MONTH" + MonthCase);
            Integer year1 = (Integer) Calendar.getInstance().get(Calendar.YEAR);
            Integer y=(Integer)getYearBinding().getValue();
            System.out.println("CURRENT YEAR IS BY SYSTEM*********** "+year1);
            System.out.println("CURRENT YEAR IS BY USER*********** "+y);
            if (y.compareTo(year1)==1 )
            {}else{
                if(MonthCase.compareTo(j) == -1)
                {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,  "'Back Month Plan Can Not Be Generated", null));
            }
         
           
        }
        }

    }

    public void yearValidator(FacesContext facesContext, UIComponent uIComponent, Object ob) {
        Integer year = (Integer) ob;
        Integer year1 = (Integer) Calendar.getInstance().get(Calendar.YEAR);
        System.out.println("CURRENT YEAR ENTER" + year);
        System.out.println("CURRENT YEAR SYSTEMDATE" + year1);
        if (ob!=null && year.compareTo(year1) == -1) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                          "Back Year Plan Can Not Be Generated", null));
        }

    }

    public void setMonthBinding(RichSelectOneChoice monthBinding) {
        this.monthBinding = monthBinding;
    }

    public RichSelectOneChoice getMonthBinding() {
        return monthBinding;
    }

    public void setYearBinding(RichInputText yearBinding) {
        this.yearBinding = yearBinding;
    }

    public RichInputText getYearBinding() {
        return yearBinding;
    }

    public void searchShortageAL(ActionEvent actionEvent) 
    {
        OperationBinding op1 = (OperationBinding) ADFUtils.findOperation("createFilterShortageGeneration");
        op1.getParamsMap().put("mode", "V");
        op1.execute();
        
        if(unitCdBinding.getValue()!=null)
        {
            if(monthBinding.getValue()!=null)
            {
                if(yearBinding.getValue()!=null){
                    OperationBinding op = (OperationBinding) ADFUtils.findOperation("searchShortageGeneration");
                    op.getParamsMap().put("year", yearBinding.getValue());
                    op.getParamsMap().put("month", monthBinding.getValue());
                    op.getParamsMap().put("unit", unitCdBinding.getValue());
                    Object Obj=op.execute();
                    System.out.println("*************"+op.getResult());
                    if(op.getResult()!= null &&op.getResult()!= "" && op.getResult().equals("C")){
                    System.out.println("intoif");
                    }
//                    else{
//                        ADFUtils.showMessage("Fields are required.", 0);
//                    }
                }
                else{
//                    FacesMessage message = new FacesMessage("Year is required.");
//                    message.setSeverity(FacesMessage.SEVERITY_ERROR);
//                    FacesContext context = FacesContext.getCurrentInstance();
//                    context.addMessage(yearBinding.getClientId(), message);
                }
            }
            else{
//                FacesMessage message = new FacesMessage("Month is required.");
//                message.setSeverity(FacesMessage.SEVERITY_ERROR);
//                FacesContext context = FacesContext.getCurrentInstance();
//                context.addMessage(monthBinding.getClientId(), message);
            }
        }
        else{
            ADFUtils.showMessage("Unit Code is required.", 0);
        }

    }

    public void setUnitCdBinding(RichInputComboboxListOfValues unitCdBinding) {
        this.unitCdBinding = unitCdBinding;
    }

    public RichInputComboboxListOfValues getUnitCdBinding() {
        return unitCdBinding;
    }

    public void pageLoad() {
        oracle.binding.OperationBinding op = ADFUtils.findOperation("CreateFilter");
        op.execute();
    }

    public void newProcedureCall(ActionEvent actionEvent) {        
            if(unitCdBinding.getValue()!=null) {
                if(monthBinding.getValue()!=null) {
                    if(yearBinding.getValue()!=null) {
                        OperationBinding op = (OperationBinding) ADFUtils.findOperation("shortageProcedureNew");
                        op.getParamsMap().put("year", yearBinding.getValue());
                        op.getParamsMap().put("month", monthBinding.getValue());
                        op.getParamsMap().put("unit", unitCdBinding.getValue());
                        op.getParamsMap().put("emp",ADFUtils.resolveExpression("#{pageFlowScope.empCode}") == null ? "E-001" : ADFUtils.resolveExpression("#{pageFlowScope.empCode}"));
                        Object Obj=op.execute();
                            System.out.println("************* "+op.getResult()+" *************");
                        if(op.getResult()!= null && op.getResult()!= "" && op.getResult().equals("C")){
                                System.out.println("intoif");
                            FacesMessage Message = new FacesMessage("Auto-Indent Generated Successfully.");  
                            Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                            FacesContext fc = FacesContext.getCurrentInstance();  
                            fc.addMessage(null, Message);
                        }
                    }
                    else{
                                FacesMessage message = new FacesMessage("Month is required.");
                                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                                FacesContext context = FacesContext.getCurrentInstance();
                                context.addMessage(monthBinding.getClientId(), message);
                    }
                }
                else{
                            FacesMessage message = new FacesMessage("Year is required.");
                            message.setSeverity(FacesMessage.SEVERITY_ERROR);
                            FacesContext context = FacesContext.getCurrentInstance();
                            context.addMessage(yearBinding.getClientId(), message);
                }
            }
            else{
                ADFUtils.showMessage("Unit Code is required.", 0);
            }
        }
}
