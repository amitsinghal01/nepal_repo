package terms.qcl.setup.model.applicationModule.common;

import oracle.jbo.ApplicationModule;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Thu Sep 20 16:08:35 IST 2018
// ---------------------------------------------------------------------
public interface QclSetupAM extends ApplicationModule {
    void getDefCode();

    String defectDescFind();
}

