package terms.qcl.transaction.model.view;

import java.sql.Timestamp;

import oracle.jbo.RowSet;
import oracle.jbo.server.EntityImpl;
import oracle.jbo.server.ViewRowImpl;

import terms.hrm.setup.model.entity.EmployeeMasterEOImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Mon Oct 22 12:17:46 IST 2018
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class CustComplaintAnalysisParticipantsVORowImpl extends ViewRowImpl {
    public static final int ENTITY_CUSTCOMPLAINTANALYSISPARTICI1 = 0;
    public static final int ENTITY_EMPLOYEEMASTEREO = 1;
    public static final int ENTITY_LOCATIONEO = 2;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        TasNo,
        EmployeeCode,
        UnitCd,
        DeptCode,
        TasId,
        TasLineId,
        CreatedBy,
        CreationDate,
        LastUpdatedBy,
        LastUpdateDate,
        ObjectVersionNumber,
        LongDesc,
        LocatCode,
        ObjectVersionNumber1,
        EmpFirstName,
        EmpNumber,
        ObjectVersionNumber2,
        EmpLastName,
        ShortDesc,
        LocationVO1,
        EmpViewVVO1;
        static AttributesEnum[] vals = null;
        ;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int TASNO = AttributesEnum.TasNo.index();
    public static final int EMPLOYEECODE = AttributesEnum.EmployeeCode.index();
    public static final int UNITCD = AttributesEnum.UnitCd.index();
    public static final int DEPTCODE = AttributesEnum.DeptCode.index();
    public static final int TASID = AttributesEnum.TasId.index();
    public static final int TASLINEID = AttributesEnum.TasLineId.index();
    public static final int CREATEDBY = AttributesEnum.CreatedBy.index();
    public static final int CREATIONDATE = AttributesEnum.CreationDate.index();
    public static final int LASTUPDATEDBY = AttributesEnum.LastUpdatedBy.index();
    public static final int LASTUPDATEDATE = AttributesEnum.LastUpdateDate.index();
    public static final int OBJECTVERSIONNUMBER = AttributesEnum.ObjectVersionNumber.index();
    public static final int LONGDESC = AttributesEnum.LongDesc.index();
    public static final int LOCATCODE = AttributesEnum.LocatCode.index();
    public static final int OBJECTVERSIONNUMBER1 = AttributesEnum.ObjectVersionNumber1.index();
    public static final int EMPFIRSTNAME = AttributesEnum.EmpFirstName.index();
    public static final int EMPNUMBER = AttributesEnum.EmpNumber.index();
    public static final int OBJECTVERSIONNUMBER2 = AttributesEnum.ObjectVersionNumber2.index();
    public static final int EMPLASTNAME = AttributesEnum.EmpLastName.index();
    public static final int SHORTDESC = AttributesEnum.ShortDesc.index();
    public static final int LOCATIONVO1 = AttributesEnum.LocationVO1.index();
    public static final int EMPVIEWVVO1 = AttributesEnum.EmpViewVVO1.index();

    /**
     * This is the default constructor (do not remove).
     */
    public CustComplaintAnalysisParticipantsVORowImpl() {
    }

    /**
     * Gets CustComplaintAnalysisPartici1 entity object.
     * @return the CustComplaintAnalysisPartici1
     */
    public EntityImpl getCustComplaintAnalysisPartici1() {
        return (EntityImpl) getEntity(ENTITY_CUSTCOMPLAINTANALYSISPARTICI1);
    }

    /**
     * Gets EmployeeMasterEO entity object.
     * @return the EmployeeMasterEO
     */
    public EmployeeMasterEOImpl getEmployeeMasterEO() {
        return (EmployeeMasterEOImpl) getEntity(ENTITY_EMPLOYEEMASTEREO);
    }

    /**
     * Gets LocationEO entity object.
     * @return the LocationEO
     */
    public EntityImpl getLocationEO() {
        return (EntityImpl) getEntity(ENTITY_LOCATIONEO);
    }

    /**
     * Gets the attribute value for TAS_NO using the alias name TasNo.
     * @return the TAS_NO
     */
    public String getTasNo() {
        return (String) getAttributeInternal(TASNO);
    }

    /**
     * Sets <code>value</code> as attribute value for TAS_NO using the alias name TasNo.
     * @param value value to set the TAS_NO
     */
    public void setTasNo(String value) {
        setAttributeInternal(TASNO, value);
    }

    /**
     * Gets the attribute value for EMPLOYEE_CODE using the alias name EmployeeCode.
     * @return the EMPLOYEE_CODE
     */
    public String getEmployeeCode() {
        return (String) getAttributeInternal(EMPLOYEECODE);
    }

    /**
     * Sets <code>value</code> as attribute value for EMPLOYEE_CODE using the alias name EmployeeCode.
     * @param value value to set the EMPLOYEE_CODE
     */
    public void setEmployeeCode(String value) {
        setAttributeInternal(EMPLOYEECODE, value);
    }

    /**
     * Gets the attribute value for UNIT_CD using the alias name UnitCd.
     * @return the UNIT_CD
     */
    public Integer getUnitCd() {
        return (Integer) getAttributeInternal(UNITCD);
    }

    /**
     * Sets <code>value</code> as attribute value for UNIT_CD using the alias name UnitCd.
     * @param value value to set the UNIT_CD
     */
    public void setUnitCd(Integer value) {
        setAttributeInternal(UNITCD, value);
    }

    /**
     * Gets the attribute value for DEPT_CODE using the alias name DeptCode.
     * @return the DEPT_CODE
     */
    public String getDeptCode() {
        return (String) getAttributeInternal(DEPTCODE);
    }

    /**
     * Sets <code>value</code> as attribute value for DEPT_CODE using the alias name DeptCode.
     * @param value value to set the DEPT_CODE
     */
    public void setDeptCode(String value) {
        setAttributeInternal(DEPTCODE, value);
    }

    /**
     * Gets the attribute value for TAS_ID using the alias name TasId.
     * @return the TAS_ID
     */
    public Long getTasId() {
        return (Long) getAttributeInternal(TASID);
    }

    /**
     * Sets <code>value</code> as attribute value for TAS_ID using the alias name TasId.
     * @param value value to set the TAS_ID
     */
    public void setTasId(Long value) {
        setAttributeInternal(TASID, value);
    }

    /**
     * Gets the attribute value for TAS_LINE_ID using the alias name TasLineId.
     * @return the TAS_LINE_ID
     */
    public Long getTasLineId() {
        return (Long) getAttributeInternal(TASLINEID);
    }

    /**
     * Sets <code>value</code> as attribute value for TAS_LINE_ID using the alias name TasLineId.
     * @param value value to set the TAS_LINE_ID
     */
    public void setTasLineId(Long value) {
        setAttributeInternal(TASLINEID, value);
    }

    /**
     * Gets the attribute value for CREATED_BY using the alias name CreatedBy.
     * @return the CREATED_BY
     */
    public String getCreatedBy() {
        return (String) getAttributeInternal(CREATEDBY);
    }

    /**
     * Gets the attribute value for CREATION_DATE using the alias name CreationDate.
     * @return the CREATION_DATE
     */
    public Timestamp getCreationDate() {
        return (Timestamp) getAttributeInternal(CREATIONDATE);
    }

    /**
     * Gets the attribute value for LAST_UPDATED_BY using the alias name LastUpdatedBy.
     * @return the LAST_UPDATED_BY
     */
    public String getLastUpdatedBy() {
        return (String) getAttributeInternal(LASTUPDATEDBY);
    }

    /**
     * Gets the attribute value for LAST_UPDATE_DATE using the alias name LastUpdateDate.
     * @return the LAST_UPDATE_DATE
     */
    public Timestamp getLastUpdateDate() {
        return (Timestamp) getAttributeInternal(LASTUPDATEDATE);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER);
    }

    /**
     * Gets the attribute value for LONG_DESC using the alias name LongDesc.
     * @return the LONG_DESC
     */
    public String getLongDesc() {
        return (String) getAttributeInternal(LONGDESC);
    }

    /**
     * Sets <code>value</code> as attribute value for LONG_DESC using the alias name LongDesc.
     * @param value value to set the LONG_DESC
     */
    public void setLongDesc(String value) {
        setAttributeInternal(LONGDESC, value);
    }

    /**
     * Gets the attribute value for LOCAT_CODE using the alias name LocatCode.
     * @return the LOCAT_CODE
     */
    public String getLocatCode() {
        return (String) getAttributeInternal(LOCATCODE);
    }

    /**
     * Sets <code>value</code> as attribute value for LOCAT_CODE using the alias name LocatCode.
     * @param value value to set the LOCAT_CODE
     */
    public void setLocatCode(String value) {
        setAttributeInternal(LOCATCODE, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber1.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber1() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER1);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber1.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber1(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER1, value);
    }

    /**
     * Gets the attribute value for EMP_FIRST_NAME using the alias name EmpFirstName.
     * @return the EMP_FIRST_NAME
     */
    public String getEmpFirstName() {
        if ((getAttributeInternal(EMPFIRSTNAME) != null) && (getAttributeInternal(EMPLASTNAME) != null)) {
            return (String) getAttributeInternal(EMPFIRSTNAME) + " " + (String) getAttributeInternal(EMPLASTNAME);
        } else {
            return (String) getAttributeInternal(EMPFIRSTNAME);
        }
        //return (String) getAttributeInternal(EMPFIRSTNAME);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_FIRST_NAME using the alias name EmpFirstName.
     * @param value value to set the EMP_FIRST_NAME
     */
    public void setEmpFirstName(String value) {
        setAttributeInternal(EMPFIRSTNAME, value);
    }

    /**
     * Gets the attribute value for EMP_NUMBER using the alias name EmpNumber.
     * @return the EMP_NUMBER
     */
    public String getEmpNumber() {
        return (String) getAttributeInternal(EMPNUMBER);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_NUMBER using the alias name EmpNumber.
     * @param value value to set the EMP_NUMBER
     */
    public void setEmpNumber(String value) {
        setAttributeInternal(EMPNUMBER, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber2.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber2() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER2);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber2.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber2(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER2, value);
    }

    /**
     * Gets the attribute value for EMP_LAST_NAME using the alias name EmpLastName.
     * @return the EMP_LAST_NAME
     */
    public String getEmpLastName() {
        return (String) getAttributeInternal(EMPLASTNAME);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_LAST_NAME using the alias name EmpLastName.
     * @param value value to set the EMP_LAST_NAME
     */
    public void setEmpLastName(String value) {
        setAttributeInternal(EMPLASTNAME, value);
    }

    /**
     * Gets the attribute value for SHORT_DESC using the alias name ShortDesc.
     * @return the SHORT_DESC
     */
    public String getShortDesc() {
        return (String) getAttributeInternal(SHORTDESC);
    }

    /**
     * Sets <code>value</code> as attribute value for SHORT_DESC using the alias name ShortDesc.
     * @param value value to set the SHORT_DESC
     */
    public void setShortDesc(String value) {
        setAttributeInternal(SHORTDESC, value);
    }

    /**
     * Gets the view accessor <code>RowSet</code> LocationVO1.
     */
    public RowSet getLocationVO1() {
        return (RowSet) getAttributeInternal(LOCATIONVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> EmpViewVVO1.
     */
    public RowSet getEmpViewVVO1() {
        return (RowSet) getAttributeInternal(EMPVIEWVVO1);
    }
}

