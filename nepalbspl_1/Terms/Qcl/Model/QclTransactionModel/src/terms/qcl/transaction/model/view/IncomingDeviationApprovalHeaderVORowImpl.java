package terms.qcl.transaction.model.view;

import java.sql.Timestamp;

import oracle.jbo.RowIterator;
import oracle.jbo.RowSet;
import oracle.jbo.server.EntityImpl;
import oracle.jbo.server.ViewRowImpl;

import terms.eng.setup.model.entity.VendorMasterEOImpl;

import terms.hrm.setup.model.entity.EmployeeMasterEOImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Tue Sep 12 15:54:05 IST 2017
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class IncomingDeviationApprovalHeaderVORowImpl extends ViewRowImpl {

    public static final int ENTITY_INCOMINGDEVIATIONAPPROVALHEA1 = 0;
    public static final int ENTITY_UNITEO = 1;
    public static final int ENTITY_VENDORMASTEREO = 2;
    public static final int ENTITY_EMPLOYEEMASTEREO = 3;
    public static final int ENTITY_EMPLOYEEMASTEREO1 = 4;
    public static final int ENTITY_EMPLOYEEMASTEREO2 = 5;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        ActiveStatus,
        ApprovedBy,
        ApprovedDt,
        CheckedBy,
        CreatedBy,
        CreatedDate,
        DevGivenType,
        DevReqDt,
        DevReqId,
        DevReqNo,
        ModifiedBy,
        ModifiedDate,
        ObjectVersionNumber,
        PreparedBy,
        QcApprovedBy,
        QcApprovedDt,
        QcRemarks,
        Remarks,
        SrvDt,
        SrvNo,
        SuppSrvNo,
        SystemName,
        UnitCd,
        VenCd,
        Name,
        Code,
        ObjectVersionNumber1,
        VendorCode,
        Name1,
        ObjectVersionNumber2,
        VendorId,
        EmpNumber,
        EmpFirstName,
        EmpLastName,
        ObjectVersionNumber3,
        EmpNumber1,
        EmpFirstName1,
        EmpLastName1,
        ObjectVersionNumber4,
        EmpNumber2,
        EmpFirstName2,
        EmpLastName2,
        ObjectVersionNumber5,
        EditTrans,
        IncomingDeviationApprovalDetailVO,
        UnitVO1,
        SecControlVVO1,
        VendorRegisteredVVO1,
        EmpViewVVO1,
        EmpViewVVO2,
        QcApprovedByEmpViewVVO3;
        static AttributesEnum[] vals = null;
        ;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int ACTIVESTATUS = AttributesEnum.ActiveStatus.index();
    public static final int APPROVEDBY = AttributesEnum.ApprovedBy.index();
    public static final int APPROVEDDT = AttributesEnum.ApprovedDt.index();
    public static final int CHECKEDBY = AttributesEnum.CheckedBy.index();
    public static final int CREATEDBY = AttributesEnum.CreatedBy.index();
    public static final int CREATEDDATE = AttributesEnum.CreatedDate.index();
    public static final int DEVGIVENTYPE = AttributesEnum.DevGivenType.index();
    public static final int DEVREQDT = AttributesEnum.DevReqDt.index();
    public static final int DEVREQID = AttributesEnum.DevReqId.index();
    public static final int DEVREQNO = AttributesEnum.DevReqNo.index();
    public static final int MODIFIEDBY = AttributesEnum.ModifiedBy.index();
    public static final int MODIFIEDDATE = AttributesEnum.ModifiedDate.index();
    public static final int OBJECTVERSIONNUMBER = AttributesEnum.ObjectVersionNumber.index();
    public static final int PREPAREDBY = AttributesEnum.PreparedBy.index();
    public static final int QCAPPROVEDBY = AttributesEnum.QcApprovedBy.index();
    public static final int QCAPPROVEDDT = AttributesEnum.QcApprovedDt.index();
    public static final int QCREMARKS = AttributesEnum.QcRemarks.index();
    public static final int REMARKS = AttributesEnum.Remarks.index();
    public static final int SRVDT = AttributesEnum.SrvDt.index();
    public static final int SRVNO = AttributesEnum.SrvNo.index();
    public static final int SUPPSRVNO = AttributesEnum.SuppSrvNo.index();
    public static final int SYSTEMNAME = AttributesEnum.SystemName.index();
    public static final int UNITCD = AttributesEnum.UnitCd.index();
    public static final int VENCD = AttributesEnum.VenCd.index();
    public static final int NAME = AttributesEnum.Name.index();
    public static final int CODE = AttributesEnum.Code.index();
    public static final int OBJECTVERSIONNUMBER1 = AttributesEnum.ObjectVersionNumber1.index();
    public static final int VENDORCODE = AttributesEnum.VendorCode.index();
    public static final int NAME1 = AttributesEnum.Name1.index();
    public static final int OBJECTVERSIONNUMBER2 = AttributesEnum.ObjectVersionNumber2.index();
    public static final int VENDORID = AttributesEnum.VendorId.index();
    public static final int EMPNUMBER = AttributesEnum.EmpNumber.index();
    public static final int EMPFIRSTNAME = AttributesEnum.EmpFirstName.index();
    public static final int EMPLASTNAME = AttributesEnum.EmpLastName.index();
    public static final int OBJECTVERSIONNUMBER3 = AttributesEnum.ObjectVersionNumber3.index();
    public static final int EMPNUMBER1 = AttributesEnum.EmpNumber1.index();
    public static final int EMPFIRSTNAME1 = AttributesEnum.EmpFirstName1.index();
    public static final int EMPLASTNAME1 = AttributesEnum.EmpLastName1.index();
    public static final int OBJECTVERSIONNUMBER4 = AttributesEnum.ObjectVersionNumber4.index();
    public static final int EMPNUMBER2 = AttributesEnum.EmpNumber2.index();
    public static final int EMPFIRSTNAME2 = AttributesEnum.EmpFirstName2.index();
    public static final int EMPLASTNAME2 = AttributesEnum.EmpLastName2.index();
    public static final int OBJECTVERSIONNUMBER5 = AttributesEnum.ObjectVersionNumber5.index();
    public static final int EDITTRANS = AttributesEnum.EditTrans.index();
    public static final int INCOMINGDEVIATIONAPPROVALDETAILVO =
        AttributesEnum.IncomingDeviationApprovalDetailVO.index();
    public static final int UNITVO1 = AttributesEnum.UnitVO1.index();
    public static final int SECCONTROLVVO1 = AttributesEnum.SecControlVVO1.index();
    public static final int VENDORREGISTEREDVVO1 = AttributesEnum.VendorRegisteredVVO1.index();
    public static final int EMPVIEWVVO1 = AttributesEnum.EmpViewVVO1.index();
    public static final int EMPVIEWVVO2 = AttributesEnum.EmpViewVVO2.index();
    public static final int QCAPPROVEDBYEMPVIEWVVO3 = AttributesEnum.QcApprovedByEmpViewVVO3.index();

    /**
     * This is the default constructor (do not remove).
     */
    public IncomingDeviationApprovalHeaderVORowImpl() {
    }

    /**
     * Gets IncomingDeviationApprovalHea1 entity object.
     * @return the IncomingDeviationApprovalHea1
     */
    public EntityImpl getIncomingDeviationApprovalHea1() {
        return (EntityImpl) getEntity(ENTITY_INCOMINGDEVIATIONAPPROVALHEA1);
    }

    /**
     * Gets UnitEO entity object.
     * @return the UnitEO
     */
    public EntityImpl getUnitEO() {
        return (EntityImpl) getEntity(ENTITY_UNITEO);
    }

    /**
     * Gets VendorMasterEO entity object.
     * @return the VendorMasterEO
     */
    public VendorMasterEOImpl getVendorMasterEO() {
        return (VendorMasterEOImpl) getEntity(ENTITY_VENDORMASTEREO);
    }

    /**
     * Gets EmployeeMasterEO entity object.
     * @return the EmployeeMasterEO
     */
    public EmployeeMasterEOImpl getEmployeeMasterEO() {
        return (EmployeeMasterEOImpl) getEntity(ENTITY_EMPLOYEEMASTEREO);
    }

    /**
     * Gets EmployeeMasterEO1 entity object.
     * @return the EmployeeMasterEO1
     */
    public EmployeeMasterEOImpl getEmployeeMasterEO1() {
        return (EmployeeMasterEOImpl) getEntity(ENTITY_EMPLOYEEMASTEREO1);
    }

    /**
     * Gets EmployeeMasterEO2 entity object.
     * @return the EmployeeMasterEO2
     */
    public EmployeeMasterEOImpl getEmployeeMasterEO2() {
        return (EmployeeMasterEOImpl) getEntity(ENTITY_EMPLOYEEMASTEREO2);
    }


    /**
     * Gets the attribute value for ACTIVE_STATUS using the alias name ActiveStatus.
     * @return the ACTIVE_STATUS
     */
    public String getActiveStatus() {
        return (String) getAttributeInternal(ACTIVESTATUS);
    }

    /**
     * Sets <code>value</code> as attribute value for ACTIVE_STATUS using the alias name ActiveStatus.
     * @param value value to set the ACTIVE_STATUS
     */
    public void setActiveStatus(String value) {
        setAttributeInternal(ACTIVESTATUS, value);
    }

    /**
     * Gets the attribute value for APPROVED_BY using the alias name ApprovedBy.
     * @return the APPROVED_BY
     */
    public String getApprovedBy() {
//        
//        if (getAttributeInternal(APPROVEDBY) != null && getApprovedDt() == null) {
//            setApprovedDt(new oracle.jbo.domain.Timestamp(System.currentTimeMillis()));
//        }
        return (String) getAttributeInternal(APPROVEDBY);
    }

    /**
     * Sets <code>value</code> as attribute value for APPROVED_BY using the alias name ApprovedBy.
     * @param value value to set the APPROVED_BY
     */
    public void setApprovedBy(String value) {
        setAttributeInternal(APPROVEDBY, value);
    }

    /**
     * Gets the attribute value for APPROVED_DT using the alias name ApprovedDt.
     * @return the APPROVED_DT
     */
    public oracle.jbo.domain.Timestamp getApprovedDt() {
        return (oracle.jbo.domain.Timestamp) getAttributeInternal(APPROVEDDT);
    }

    /**
     * Sets <code>value</code> as attribute value for APPROVED_DT using the alias name ApprovedDt.
     * @param value value to set the APPROVED_DT
     */
    public void setApprovedDt(oracle.jbo.domain.Timestamp value) {
        setAttributeInternal(APPROVEDDT, value);
    }

    /**
     * Gets the attribute value for CHECKED_BY using the alias name CheckedBy.
     * @return the CHECKED_BY
     */
    public String getCheckedBy() {
        return (String) getAttributeInternal(CHECKEDBY);
    }

    /**
     * Sets <code>value</code> as attribute value for CHECKED_BY using the alias name CheckedBy.
     * @param value value to set the CHECKED_BY
     */
    public void setCheckedBy(String value) {
        setAttributeInternal(CHECKEDBY, value);
    }

    /**
     * Gets the attribute value for CREATED_BY using the alias name CreatedBy.
     * @return the CREATED_BY
     */
    public String getCreatedBy() {
        return (String) getAttributeInternal(CREATEDBY);
    }

    /**
     * Sets <code>value</code> as attribute value for CREATED_BY using the alias name CreatedBy.
     * @param value value to set the CREATED_BY
     */
    public void setCreatedBy(String value) {
        setAttributeInternal(CREATEDBY, value);
    }

    /**
     * Gets the attribute value for CREATED_DATE using the alias name CreatedDate.
     * @return the CREATED_DATE
     */
    public Timestamp getCreatedDate() {
        return (Timestamp) getAttributeInternal(CREATEDDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for CREATED_DATE using the alias name CreatedDate.
     * @param value value to set the CREATED_DATE
     */
    public void setCreatedDate(Timestamp value) {
        setAttributeInternal(CREATEDDATE, value);
    }

    /**
     * Gets the attribute value for DEV_GIVEN_TYPE using the alias name DevGivenType.
     * @return the DEV_GIVEN_TYPE
     */
    public String getDevGivenType() {
        return (String) getAttributeInternal(DEVGIVENTYPE);
    }

    /**
     * Sets <code>value</code> as attribute value for DEV_GIVEN_TYPE using the alias name DevGivenType.
     * @param value value to set the DEV_GIVEN_TYPE
     */
    public void setDevGivenType(String value) {
        setAttributeInternal(DEVGIVENTYPE, value);
    }

    /**
     * Gets the attribute value for DEV_REQ_DT using the alias name DevReqDt.
     * @return the DEV_REQ_DT
     */
    public oracle.jbo.domain.Timestamp getDevReqDt() {
        return (oracle.jbo.domain.Timestamp) getAttributeInternal(DEVREQDT);
    }

    /**
     * Sets <code>value</code> as attribute value for DEV_REQ_DT using the alias name DevReqDt.
     * @param value value to set the DEV_REQ_DT
     */
    public void setDevReqDt(oracle.jbo.domain.Timestamp value) {
        setAttributeInternal(DEVREQDT, value);
    }

    /**
     * Gets the attribute value for DEV_REQ_ID using the alias name DevReqId.
     * @return the DEV_REQ_ID
     */
    public Long getDevReqId() {
        return (Long) getAttributeInternal(DEVREQID);
    }

    /**
     * Sets <code>value</code> as attribute value for DEV_REQ_ID using the alias name DevReqId.
     * @param value value to set the DEV_REQ_ID
     */
    public void setDevReqId(Long value) {
        setAttributeInternal(DEVREQID, value);
    }

    /**
     * Gets the attribute value for DEV_REQ_NO using the alias name DevReqNo.
     * @return the DEV_REQ_NO
     */
    public String getDevReqNo() {
        return (String) getAttributeInternal(DEVREQNO);
    }

    /**
     * Sets <code>value</code> as attribute value for DEV_REQ_NO using the alias name DevReqNo.
     * @param value value to set the DEV_REQ_NO
     */
    public void setDevReqNo(String value) {
        setAttributeInternal(DEVREQNO, value);
    }

    /**
     * Gets the attribute value for MODIFIED_BY using the alias name ModifiedBy.
     * @return the MODIFIED_BY
     */
    public String getModifiedBy() {
        return (String) getAttributeInternal(MODIFIEDBY);
    }

    /**
     * Sets <code>value</code> as attribute value for MODIFIED_BY using the alias name ModifiedBy.
     * @param value value to set the MODIFIED_BY
     */
    public void setModifiedBy(String value) {
        setAttributeInternal(MODIFIEDBY, value);
    }

    /**
     * Gets the attribute value for MODIFIED_DATE using the alias name ModifiedDate.
     * @return the MODIFIED_DATE
     */
    public Timestamp getModifiedDate() {
        return (Timestamp) getAttributeInternal(MODIFIEDDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for MODIFIED_DATE using the alias name ModifiedDate.
     * @param value value to set the MODIFIED_DATE
     */
    public void setModifiedDate(Timestamp value) {
        setAttributeInternal(MODIFIEDDATE, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER);
    }


    /**
     * Gets the attribute value for PREPARED_BY using the alias name PreparedBy.
     * @return the PREPARED_BY
     */
    public String getPreparedBy() {
        return (String) getAttributeInternal(PREPAREDBY);
    }

    /**
     * Sets <code>value</code> as attribute value for PREPARED_BY using the alias name PreparedBy.
     * @param value value to set the PREPARED_BY
     */
    public void setPreparedBy(String value) {
        setAttributeInternal(PREPAREDBY, value);
    }

    /**
     * Gets the attribute value for QC_APPROVED_BY using the alias name QcApprovedBy.
     * @return the QC_APPROVED_BY
     */
    public String getQcApprovedBy() {
        
        
        if (getAttributeInternal(QCAPPROVEDBY) != null && getQcApprovedDt()  == null) {
            setQcApprovedDt(new oracle.jbo.domain.Timestamp(System.currentTimeMillis()));
        }
        return (String) getAttributeInternal(QCAPPROVEDBY);
    }

    /**
     * Sets <code>value</code> as attribute value for QC_APPROVED_BY using the alias name QcApprovedBy.
     * @param value value to set the QC_APPROVED_BY
     */
    public void setQcApprovedBy(String value) {
        setAttributeInternal(QCAPPROVEDBY, value);
    }

    /**
     * Gets the attribute value for QC_APPROVED_DT using the alias name QcApprovedDt.
     * @return the QC_APPROVED_DT
     */
    public oracle.jbo.domain.Timestamp getQcApprovedDt() {
        return (oracle.jbo.domain.Timestamp) getAttributeInternal(QCAPPROVEDDT);
    }

    /**
     * Sets <code>value</code> as attribute value for QC_APPROVED_DT using the alias name QcApprovedDt.
     * @param value value to set the QC_APPROVED_DT
     */
    public void setQcApprovedDt(oracle.jbo.domain.Timestamp value) {
        setAttributeInternal(QCAPPROVEDDT, value);
    }

    /**
     * Gets the attribute value for QC_REMARKS using the alias name QcRemarks.
     * @return the QC_REMARKS
     */
    public String getQcRemarks() {
        return (String) getAttributeInternal(QCREMARKS);
    }

    /**
     * Sets <code>value</code> as attribute value for QC_REMARKS using the alias name QcRemarks.
     * @param value value to set the QC_REMARKS
     */
    public void setQcRemarks(String value) {
        setAttributeInternal(QCREMARKS, value);
    }

    /**
     * Gets the attribute value for REMARKS using the alias name Remarks.
     * @return the REMARKS
     */
    public String getRemarks() {
        return (String) getAttributeInternal(REMARKS);
    }

    /**
     * Sets <code>value</code> as attribute value for REMARKS using the alias name Remarks.
     * @param value value to set the REMARKS
     */
    public void setRemarks(String value) {
        setAttributeInternal(REMARKS, value);
    }

    /**
     * Gets the attribute value for SRV_DT using the alias name SrvDt.
     * @return the SRV_DT
     */
    public oracle.jbo.domain.Timestamp getSrvDt() {
        return (oracle.jbo.domain.Timestamp) getAttributeInternal(SRVDT);
    }

    /**
     * Sets <code>value</code> as attribute value for SRV_DT using the alias name SrvDt.
     * @param value value to set the SRV_DT
     */
    public void setSrvDt(oracle.jbo.domain.Timestamp value) {
        setAttributeInternal(SRVDT, value);
    }

    /**
     * Gets the attribute value for SRV_NO using the alias name SrvNo.
     * @return the SRV_NO
     */
    public String getSrvNo() {
        return (String) getAttributeInternal(SRVNO);
    }

    /**
     * Sets <code>value</code> as attribute value for SRV_NO using the alias name SrvNo.
     * @param value value to set the SRV_NO
     */
    public void setSrvNo(String value) {
        setAttributeInternal(SRVNO, value);
    }

    /**
     * Gets the attribute value for SUPP_SRV_NO using the alias name SuppSrvNo.
     * @return the SUPP_SRV_NO
     */
    public String getSuppSrvNo() {
        return (String) getAttributeInternal(SUPPSRVNO);
    }

    /**
     * Sets <code>value</code> as attribute value for SUPP_SRV_NO using the alias name SuppSrvNo.
     * @param value value to set the SUPP_SRV_NO
     */
    public void setSuppSrvNo(String value) {
        setAttributeInternal(SUPPSRVNO, value);
    }

    /**
     * Gets the attribute value for SYSTEM_NAME using the alias name SystemName.
     * @return the SYSTEM_NAME
     */
    public String getSystemName() {
        return (String) getAttributeInternal(SYSTEMNAME);
    }

    /**
     * Sets <code>value</code> as attribute value for SYSTEM_NAME using the alias name SystemName.
     * @param value value to set the SYSTEM_NAME
     */
    public void setSystemName(String value) {
        setAttributeInternal(SYSTEMNAME, value);
    }

    /**
     * Gets the attribute value for UNIT_CD using the alias name UnitCd.
     * @return the UNIT_CD
     */
    public String getUnitCd() {
        return (String) getAttributeInternal(UNITCD);
    }

    /**
     * Sets <code>value</code> as attribute value for UNIT_CD using the alias name UnitCd.
     * @param value value to set the UNIT_CD
     */
    public void setUnitCd(String value) {
        setAttributeInternal(UNITCD, value);
    }

    /**
     * Gets the attribute value for VEN_CD using the alias name VenCd.
     * @return the VEN_CD
     */
    public String getVenCd() {
        return (String) getAttributeInternal(VENCD);
    }

    /**
     * Sets <code>value</code> as attribute value for VEN_CD using the alias name VenCd.
     * @param value value to set the VEN_CD
     */
    public void setVenCd(String value) {
        setAttributeInternal(VENCD, value);
    }

    /**
     * Gets the attribute value for NAME using the alias name Name.
     * @return the NAME
     */
    public String getName() {
        return (String) getAttributeInternal(NAME);
    }

    /**
     * Sets <code>value</code> as attribute value for NAME using the alias name Name.
     * @param value value to set the NAME
     */
    public void setName(String value) {
        setAttributeInternal(NAME, value);
    }

    /**
     * Gets the attribute value for CODE using the alias name Code.
     * @return the CODE
     */
    public String getCode() {
        return (String) getAttributeInternal(CODE);
    }

    /**
     * Sets <code>value</code> as attribute value for CODE using the alias name Code.
     * @param value value to set the CODE
     */
    public void setCode(String value) {
        setAttributeInternal(CODE, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber1.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber1() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER1);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber1.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber1(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER1, value);
    }

    /**
     * Gets the attribute value for VENDOR_CODE using the alias name VendorCode.
     * @return the VENDOR_CODE
     */
    public String getVendorCode() {
        return (String) getAttributeInternal(VENDORCODE);
    }

    /**
     * Sets <code>value</code> as attribute value for VENDOR_CODE using the alias name VendorCode.
     * @param value value to set the VENDOR_CODE
     */
    public void setVendorCode(String value) {
        setAttributeInternal(VENDORCODE, value);
    }

    /**
     * Gets the attribute value for NAME using the alias name Name1.
     * @return the NAME
     */
    public String getName1() {
        return (String) getAttributeInternal(NAME1);
    }

    /**
     * Sets <code>value</code> as attribute value for NAME using the alias name Name1.
     * @param value value to set the NAME
     */
    public void setName1(String value) {
        setAttributeInternal(NAME1, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber2.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber2() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER2);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber2.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber2(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER2, value);
    }

    /**
     * Gets the attribute value for VENDOR_ID using the alias name VendorId.
     * @return the VENDOR_ID
     */
    public Long getVendorId() {
        return (Long) getAttributeInternal(VENDORID);
    }

    /**
     * Sets <code>value</code> as attribute value for VENDOR_ID using the alias name VendorId.
     * @param value value to set the VENDOR_ID
     */
    public void setVendorId(Long value) {
        setAttributeInternal(VENDORID, value);
    }

    /**
     * Gets the attribute value for EMP_NUMBER using the alias name EmpNumber.
     * @return the EMP_NUMBER
     */
    public String getEmpNumber() {
        return (String) getAttributeInternal(EMPNUMBER);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_NUMBER using the alias name EmpNumber.
     * @param value value to set the EMP_NUMBER
     */
    public void setEmpNumber(String value) {
        setAttributeInternal(EMPNUMBER, value);
    }

    /**
     * Gets the attribute value for EMP_FIRST_NAME using the alias name EmpFirstName.
     * @return the EMP_FIRST_NAME
     */
    public String getEmpFirstName() {
        if(getPreparedBy()==null)
        {
            return (String) getAttributeInternal(EMPFIRSTNAME);
        }
        return (String) getAttributeInternal(EMPFIRSTNAME)+ " "+ getAttributeInternal(EMPLASTNAME);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_FIRST_NAME using the alias name EmpFirstName.
     * @param value value to set the EMP_FIRST_NAME
     */
    public void setEmpFirstName(String value) {
        setAttributeInternal(EMPFIRSTNAME, value);
    }

    /**
     * Gets the attribute value for EMP_LAST_NAME using the alias name EmpLastName.
     * @return the EMP_LAST_NAME
     */
    public String getEmpLastName() {
        return (String) getAttributeInternal(EMPLASTNAME);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_LAST_NAME using the alias name EmpLastName.
     * @param value value to set the EMP_LAST_NAME
     */
    public void setEmpLastName(String value) {
        setAttributeInternal(EMPLASTNAME, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber3.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber3() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER3);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber3.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber3(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER3, value);
    }

    /**
     * Gets the attribute value for EMP_NUMBER using the alias name EmpNumber1.
     * @return the EMP_NUMBER
     */
    public String getEmpNumber1() {
        return (String) getAttributeInternal(EMPNUMBER1);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_NUMBER using the alias name EmpNumber1.
     * @param value value to set the EMP_NUMBER
     */
    public void setEmpNumber1(String value) {
        setAttributeInternal(EMPNUMBER1, value);
    }

    /**
     * Gets the attribute value for EMP_FIRST_NAME using the alias name EmpFirstName1.
     * @return the EMP_FIRST_NAME
     */
    public String getEmpFirstName1() {
        if(getApprovedBy()==null)
        {
            return (String) getAttributeInternal(EMPFIRSTNAME1);
        }
        return (String) getAttributeInternal(EMPFIRSTNAME1)+ " "+ getAttributeInternal(EMPLASTNAME1);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_FIRST_NAME using the alias name EmpFirstName1.
     * @param value value to set the EMP_FIRST_NAME
     */
    public void setEmpFirstName1(String value) {
        setAttributeInternal(EMPFIRSTNAME1, value);
    }

    /**
     * Gets the attribute value for EMP_LAST_NAME using the alias name EmpLastName1.
     * @return the EMP_LAST_NAME
     */
    public String getEmpLastName1() {
        return (String) getAttributeInternal(EMPLASTNAME1);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_LAST_NAME using the alias name EmpLastName1.
     * @param value value to set the EMP_LAST_NAME
     */
    public void setEmpLastName1(String value) {
        setAttributeInternal(EMPLASTNAME1, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber4.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber4() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER4);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber4.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber4(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER4, value);
    }

    /**
     * Gets the attribute value for EMP_NUMBER using the alias name EmpNumber2.
     * @return the EMP_NUMBER
     */
    public String getEmpNumber2() {
        return (String) getAttributeInternal(EMPNUMBER2);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_NUMBER using the alias name EmpNumber2.
     * @param value value to set the EMP_NUMBER
     */
    public void setEmpNumber2(String value) {
        setAttributeInternal(EMPNUMBER2, value);
    }

    /**
     * Gets the attribute value for EMP_FIRST_NAME using the alias name EmpFirstName2.
     * @return the EMP_FIRST_NAME
     */
    public String getEmpFirstName2() {
        if(getQcApprovedBy()==null)
        {
            return (String) getAttributeInternal(EMPFIRSTNAME2);
        }
        return (String) getAttributeInternal(EMPFIRSTNAME2)+ " "+ getAttributeInternal(EMPLASTNAME2);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_FIRST_NAME using the alias name EmpFirstName2.
     * @param value value to set the EMP_FIRST_NAME
     */
    public void setEmpFirstName2(String value) {
        setAttributeInternal(EMPFIRSTNAME2, value);
    }

    /**
     * Gets the attribute value for EMP_LAST_NAME using the alias name EmpLastName2.
     * @return the EMP_LAST_NAME
     */
    public String getEmpLastName2() {
        return (String) getAttributeInternal(EMPLASTNAME2);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_LAST_NAME using the alias name EmpLastName2.
     * @param value value to set the EMP_LAST_NAME
     */
    public void setEmpLastName2(String value) {
        setAttributeInternal(EMPLASTNAME2, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber5.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber5() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER5);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber5.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber5(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER5, value);
    }

    /**
     * Gets the attribute value for the calculated attribute EditTrans.
     * @return the EditTrans
     */
    public Integer getEditTrans() {
        byte entityState = this.getEntity(0).getEntityState();
                return new Integer(entityState);
        //return (Integer) getAttributeInternal(EDITTRANS);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute EditTrans.
     * @param value value to set the  EditTrans
     */
    public void setEditTrans(Integer value) {
        setAttributeInternal(EDITTRANS, value);
    }

    /**
     * Gets the associated <code>RowIterator</code> using master-detail link IncomingDeviationApprovalDetailVO.
     */
    public RowIterator getIncomingDeviationApprovalDetailVO() {
        return (RowIterator) getAttributeInternal(INCOMINGDEVIATIONAPPROVALDETAILVO);
    }

    /**
     * Gets the view accessor <code>RowSet</code> UnitVO1.
     */
    public RowSet getUnitVO1() {
        return (RowSet) getAttributeInternal(UNITVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> SecControlVVO1.
     */
    public RowSet getSecControlVVO1() {
        return (RowSet) getAttributeInternal(SECCONTROLVVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> VendorRegisteredVVO1.
     */
    public RowSet getVendorRegisteredVVO1() {
        return (RowSet) getAttributeInternal(VENDORREGISTEREDVVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> EmpViewVVO1.
     */
    public RowSet getEmpViewVVO1() {
        return (RowSet) getAttributeInternal(EMPVIEWVVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> EmpViewVVO2.
     */
    public RowSet getEmpViewVVO2() {
        return (RowSet) getAttributeInternal(EMPVIEWVVO2);
    }

    /**
     * Gets the view accessor <code>RowSet</code> QcApprovedByEmpViewVVO3.
     */
    public RowSet getQcApprovedByEmpViewVVO3() {
        return (RowSet) getAttributeInternal(QCAPPROVEDBYEMPVIEWVVO3);
    }
}

