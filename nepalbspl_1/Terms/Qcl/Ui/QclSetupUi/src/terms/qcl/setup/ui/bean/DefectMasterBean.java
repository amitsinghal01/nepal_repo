package terms.qcl.setup.ui.bean;

import javax.faces.application.FacesMessage;

import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import terms.qcl.setup.model.view.DefectMasterVORowImpl;

public class DefectMasterBean {
    private RichTable deletePopupDefectMasterbinding;
    private String editAction = "V";

    public DefectMasterBean() {
    }

    public void deletePopupDialogDl(DialogEvent dialogEvent) {

        if (dialogEvent.getOutcome().name().equals("ok")) {
            OperationBinding op = null;
            ADFUtils.findOperation("Delete").execute();
            op = (OperationBinding) ADFUtils.findOperation("Commit");
            if (getEditAction().equals("V")) {
                Object rst = op.execute();
            }
            System.out.println("Record Delete Successfully");
            if (op.getErrors().isEmpty()) {
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            } else if (!op.getErrors().isEmpty()) {
                OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
                Object rstr = opr.execute();
                FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(deletePopupDefectMasterbinding);


    }

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public void setDeletePopupDefectMasterbinding(RichTable deletePopupDefectMasterbinding) {
        this.deletePopupDefectMasterbinding = deletePopupDefectMasterbinding;
    }

    public RichTable getDeletePopupDefectMasterbinding() {
        return deletePopupDefectMasterbinding;
    }

    public void SaveAll(ActionEvent actionEvent) {
        OperationBinding opr = ADFUtils.findOperation("defectDescFind");
        Object obj = opr.execute();
        System.out.println("Returned After Amimpl: " + obj);
        if (obj != null && !obj.equals("Y")) {

            OperationBinding op = ADFUtils.findOperation("Commit");

            Object rst = op.execute();

            if (op.getErrors().isEmpty())

            {

            } else

            {

                FacesMessage Message = new FacesMessage("Record can't be modified");

                Message.setSeverity(FacesMessage.SEVERITY_ERROR);

                FacesContext fc = FacesContext.getCurrentInstance();

                fc.addMessage(null, Message);

            }
        } else {

            ADFUtils.setEL("#{pageFlowScope.DefectMasterBean.editAction}", "A");
            ADFUtils.showMessage("Please fill Defect Description.", 0);
            AdfFacesContext.getCurrentInstance().addPartialTarget(deletePopupDefectMasterbinding);
        }
    }

    public void createButtonAL(ActionEvent actionEvent) {

        DCIteratorBinding dci = ADFUtils.findIterator("DefectMasterVO1Iterator");
        System.out.println("Rows : " + dci.getAllRowsInRange());
        DefectMasterVORowImpl row = (DefectMasterVORowImpl) dci.getCurrentRow();
        if (row == null || row.getDefectDesc() != null) {
            ADFUtils.findOperation("CreateInsert").execute();
            ADFUtils.findOperation("getDefCode").execute();
        } else {
            ADFUtils.showMessage("Please fill Defect Description.", 0);
        }
    }
}


