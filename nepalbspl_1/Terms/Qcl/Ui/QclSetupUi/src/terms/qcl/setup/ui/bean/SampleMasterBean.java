package terms.qcl.setup.ui.bean;

import java.math.BigDecimal;

import javax.faces.application.FacesMessage;
import javax.faces.application.ViewHandler;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;
import javax.faces.validator.ValidatorException;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class SampleMasterBean {
    
    BigDecimal val=new BigDecimal(0);
    private RichTable sampleMasterbinding;
    private RichTable deletePopupSampleMasterbinding;
    private String editAction="V";

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public SampleMasterBean() {
    }


    public void lotStartingRangeValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        // Add event code here...
        BigDecimal obj = (BigDecimal) object;
        if(obj.compareTo(val) < 0){
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Lot Starting Range must be equal to or greater than zero.", null));
        }
    }

    public void endRangeValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        // Add event code here...
        BigDecimal obj = (BigDecimal) object;
        if(obj.compareTo(val) < 0){
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "End Range must be equal to or greater than zero.", null));
        }
    }

    public void sampleSizeValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        // Add event code here...
        BigDecimal obj = (BigDecimal) object;
        if(obj.compareTo(val) < 0){
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Sample Size must be equal to or greater than zero.", null));
        }
    }

    public void acceptanceNumberValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        // Add event code here...
        BigDecimal obj = (BigDecimal) object;
        if(obj.compareTo(val) < 0){
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Acceptance Number must be equal to or greater than zero.", null));
        }
    }

    public void rejectionNumberValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        // Add event code here...
        BigDecimal obj = (BigDecimal) object;
        if(obj.compareTo(val) < 0){
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Rejection Number must be equal to or greater than zero.", null));
        }
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
               if(dialogEvent.getOutcome().name().equals("ok"))
               {
               OperationBinding op = null;
               ADFUtils.findOperation("Delete").execute();
               op = (OperationBinding) ADFUtils.findOperation("Commit");
               if (getEditAction().equals("V")) {
               Object rst = op.execute();
               }
               System.out.println("Record Delete Successfully");
               if(op.getErrors().isEmpty()){
               FacesMessage Message = new FacesMessage("Record Deleted Successfully");
               Message.setSeverity(FacesMessage.SEVERITY_INFO);
               FacesContext fc = FacesContext.getCurrentInstance();
               fc.addMessage(null, Message);
               } else if (!op.getErrors().isEmpty())
               {
               OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
               Object rstr = opr.execute();
               FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
               Message.setSeverity(FacesMessage.SEVERITY_ERROR);
               FacesContext fc = FacesContext.getCurrentInstance();
               fc.addMessage(null, Message);
               }
               }
               AdfFacesContext.getCurrentInstance().addPartialTarget(deletePopupSampleMasterbinding);
               
             
    
           }

    public void setDeletePopupSampleMasterbinding(RichTable deletePopupSampleMasterbinding) {
        this.deletePopupSampleMasterbinding = deletePopupSampleMasterbinding;
    }

    public RichTable getDeletePopupSampleMasterbinding() {
        return deletePopupSampleMasterbinding;
    }

//    public void SaveAll(ActionEvent actionEvent) {
//
//        
//            OperationBinding op= ADFUtils.findOperation("Commit");
//
//            Object rst = op.execute();
//
//            if(op.getErrors().isEmpty())
//
//            {
//
//            }else
//
//            {
//
//             FacesMessage Message = new FacesMessage("Record can't be modified");
//
//            Message.setSeverity(FacesMessage.SEVERITY_ERROR);
//
//            FacesContext fc = FacesContext.getCurrentInstance();
//
//            fc.addMessage(null, Message);
//
//            }
//        }


    public void createMethodAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert").execute();
        refreshPage();
        
    }
    protected void refreshPage() 
    {
    FacesContext fctx = FacesContext.getCurrentInstance();
    String refreshpage = fctx.getViewRoot().getViewId();
    ViewHandler ViewH = fctx.getApplication().getViewHandler();
    UIViewRoot UIV = ViewH.createView(fctx, refreshpage);
    UIV.setViewId(refreshpage);
    fctx.setViewRoot(UIV);
    }

    public void saveAL(ActionEvent actionEvent) {
         OperationBinding op= ADFUtils.findOperation("Commit");
        Object rst = op.execute();
        refreshPage();
         if(op.getErrors().isEmpty())
         {
         }else
         {
        FacesMessage Message = new FacesMessage("Record can't be modified");
        Message.setSeverity(FacesMessage.SEVERITY_ERROR);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
    }
}
}

