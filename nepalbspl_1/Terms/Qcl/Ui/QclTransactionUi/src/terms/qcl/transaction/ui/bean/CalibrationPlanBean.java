package terms.qcl.transaction.ui.bean;

import java.util.Calendar;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectBooleanCheckbox;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class CalibrationPlanBean {
    private String editAction="V";
    private RichTable calTableBinding;
    private RichSelectOneChoice igTypeBinding;
    private RichSelectOneChoice monthBinding;
    private RichInputText yearBinding;
    private RichInputComboboxListOfValues unitCodeBinding;

    public CalibrationPlanBean() {
    }

    public void populateQueryAL(ActionEvent actionEvent) 
    {
        oracle.adf.model.OperationBinding op1 = (oracle.adf.model.OperationBinding) ADFUtils.findOperation("CreateFilterCP");
        op1.getParamsMap().put("mode", "C");
        op1.execute();
        String ob=(String)monthBinding.getValue();
        if(ob!=null)
        {
            if (ob != null && yearBinding.getValue() != null)
            {
                Integer MonthCase = 0;
                if   (ob.toString().equals("JAN")) {
                    MonthCase = 1;
                } else if (ob.toString().equals("FEB")) {
                    MonthCase = 2;
                } else if (ob.toString().equals("MAR")) {
                    MonthCase = 3;
                } else if (ob.toString().equals("APR")) {
                    MonthCase = 4;
                } else if (ob.toString().equals("MAY")) {
                    MonthCase = 5;
                } else if (ob.toString().equals("JUN")) {
                    MonthCase = 6;
                } else if (ob.toString().equals("JUL")) {
                    MonthCase = 7;
                } else if (ob.toString().equals("AUG")) {
                    MonthCase = 8;
                } else if (ob.toString().equals("SEP")) {
                    MonthCase = 9;
                } else if (ob.toString().equals("OCT")) {
                    MonthCase = 10;
                } else if (ob.toString().equals("NOV")) {
                    MonthCase = 11;
                } else if (ob.toString().equals("DEC")) {
                    MonthCase = 12;
                } else {
                    System.out.println("No Month Found");
                }
                Integer j = 1;
                j = j + (Integer) Calendar.getInstance().get(Calendar.MONTH);
                Integer year = (Integer) Calendar.getInstance().get(Calendar.YEAR);
                Integer y=(Integer)getYearBinding().getValue();
//                System.out.println("CURRENT YEAR IS BY SYSTEM*********** "+year);
//                System.out.println("CURRENT YEAR IS BY USER*********** "+y);
                if (ob!=null && y.compareTo(year) == -1) {
                   // System.out.println("=========When value is yearrrrrrr=============");
                    FacesMessage message = new FacesMessage("Back Year Plan Can Not Be Generated");
                    message.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext context = FacesContext.getCurrentInstance();
                    context.addMessage(yearBinding.getClientId(), message);
                }
                else{
                    
                    if(MonthCase.compareTo(j) == -1)
                    {
                        ADFUtils.findOperation("populateCalibrationPlan").execute();
                        igTypeBinding.setValue(null);
                        AdfFacesContext.getCurrentInstance().addPartialTarget(igTypeBinding);
//                       // System.out.println("=========When value is YYYYY=============");
//                        FacesMessage message = new FacesMessage("Back Month Plan Can Not Be Generated");
//                        message.setSeverity(FacesMessage.SEVERITY_ERROR);
//                        FacesContext context = FacesContext.getCurrentInstance();
//                        context.addMessage(monthBinding.getClientId(), message);
//                        //throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,  "'Back Month Plan Can Not Be Generated", null));
                    }
                    else{
                        if(igTypeBinding.getValue()!=null){
                            //System.out.println("-----------when populate=================");
                            ADFUtils.findOperation("populateCalibrationPlan").execute();
                            igTypeBinding.setValue(null);
                            AdfFacesContext.getCurrentInstance().addPartialTarget(igTypeBinding);
                        }
                        else{
                            FacesMessage message = new FacesMessage("IG Type is required.");
                            message.setSeverity(FacesMessage.SEVERITY_ERROR);
                            FacesContext context = FacesContext.getCurrentInstance();
                            context.addMessage(igTypeBinding.getClientId(), message);
                        }
                    }
              }
            }
            else{
                FacesMessage message = new FacesMessage("Year is required.");
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(yearBinding.getClientId(), message);
            }
            
        }else{
            FacesMessage message = new FacesMessage("Month is required.");
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(monthBinding.getClientId(), message); 
        }
    }

    public void deleteDialogListenerDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok")) {
            OperationBinding op = null;
            ADFUtils.findOperation("Delete").execute();
            op = (OperationBinding) ADFUtils.findOperation("Commit");
            if (getEditAction().equals("V")) {
                Object rst = op.execute();
            }
            if (op.getErrors().isEmpty()) {
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            } else if (!op.getErrors().isEmpty()) {
                OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
                Object rstr = opr.execute();
                FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            }    
        }
                AdfFacesContext.getCurrentInstance().addPartialTarget(calTableBinding);
    }

    public void setCalTableBinding(RichTable calTableBinding) {
        this.calTableBinding = calTableBinding;
    }

    public RichTable getCalTableBinding() {
        return calTableBinding;
    }

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public void setIgTypeBinding(RichSelectOneChoice igTypeBinding) {
        this.igTypeBinding = igTypeBinding;
    }

    public RichSelectOneChoice getIgTypeBinding() {
        return igTypeBinding;
    }

    public void searchCalibration(ActionEvent actionEvent) 
    {
        oracle.adf.model.OperationBinding op1 = (oracle.adf.model.OperationBinding) ADFUtils.findOperation("CreateFilterCP");
        op1.getParamsMap().put("mode", "V");
        op1.execute();
        
            if(monthBinding.getValue()!=null)
            {
                if(yearBinding.getValue()!=null){
                OperationBinding op = (OperationBinding) ADFUtils.findOperation("searchCalibrationPlan");
                op.getParamsMap().put("year", yearBinding.getValue());
                op.getParamsMap().put("month", monthBinding.getValue());
                Object Obj=op.execute();
                //                    System.out.println("*************"+op.getResult());
                //                    if(op.getResult()!= null &&op.getResult()!= "" && op.getResult().equals("C")){
                //                    System.out.println("intoif");
                //                    }
                //                    else{
                //                        ADFUtils.showMessage("Fields are required.", 0);
                //                    }
                igTypeBinding.setValue(null);
                AdfFacesContext.getCurrentInstance().addPartialTarget(igTypeBinding);
                }
                else{
                    FacesMessage message = new FacesMessage("Year is required.");
                    message.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext context = FacesContext.getCurrentInstance();
                    context.addMessage(yearBinding.getClientId(), message);
                }
            }
            else{
                FacesMessage message = new FacesMessage("Month is required.");
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(monthBinding.getClientId(), message);
            }
    }

    public void setMonthBinding(RichSelectOneChoice monthBinding) {
        this.monthBinding = monthBinding;
    }

    public RichSelectOneChoice getMonthBinding() {
        return monthBinding;
    }

    public void setYearBinding(RichInputText yearBinding) {
        this.yearBinding = yearBinding;
    }

    public RichInputText getYearBinding() {
        return yearBinding;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

}
