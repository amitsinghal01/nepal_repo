package terms.qcl.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

public class CalibrationRequestBean {
    private RichButton headerEditBinding;
    private RichOutputText bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichInputText entryNoBinding;
    private RichInputComboboxListOfValues getUnitCodeBinding;
    private RichInputDate entryDateBinding;
    private RichInputText instaNoDescBinding;
    private RichInputText igCodeDescBinding;
    private RichInputText venDescBinding;

    public CalibrationRequestBean() {
    }

    public void saveAL(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedBy("CustComplaintAnalysisHeaderVO1Iterator");
        
        if(entryDateBinding.getValue()!=null)
        {
        OperationBinding op = ADFUtils.findOperation("generateCalibrationReqNo");
        op.execute();
        System.out.println("Result is===> "+op.getResult());
        if(op.getResult() != null && op.getResult().toString().equalsIgnoreCase("E"))
        {
            ADFUtils.findOperation("Commit").execute();
            ADFUtils.showMessage(" Record Updated Successfully", 2);
            
        }
        else if (op.getResult() != null && op.getResult().toString() != ""  && op.getResult().equals("S"))
        {
            System.out.println("+++++ in the save mode+++++++++++");
            ADFUtils.findOperation("Commit").execute();
            ADFUtils.showMessage("Record Saved Successfully.Calibration Request No.is "+ADFUtils.evaluateEL("#{bindings.CalibNo.inputValue}"), 2);
            
        }
        else if(op.getResult() != null && op.getResult().toString().equalsIgnoreCase("N"))
        {
            ADFUtils.showMessage("Calibration Request No. could not be generated. Try Again !!", 0);
        }
        }else{
            ADFUtils.showMessage("Entry Date is required.", 0);
        }
    }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public String saveAndCloseAL() {
        ADFUtils.setLastUpdatedBy("CustComplaintAnalysisHeaderVO1Iterator");
        OperationBinding op = ADFUtils.findOperation("generateCalibrationReqNo");
        op.execute();
        System.out.println("Result is===> "+op.getResult());
        if(op.getResult() != null && op.getResult().toString().equalsIgnoreCase("E"))
        {
            ADFUtils.findOperation("Commit").execute();
            ADFUtils.showMessage(" Record Updated Successfully", 2);
            return "saveAndClose";
        }
        else if (op.getResult() != null && op.getResult().toString() != ""  && op.getResult().equals("S"))
        {
            System.out.println("+++++ in the save mode+++++++++++");
            ADFUtils.findOperation("Commit").execute();
            ADFUtils.showMessage("Record Saved Successfully.Calibration Request No.is "+ADFUtils.evaluateEL("#{bindings.CalibNo.inputValue}"), 2);
            return "saveAndClose";
        }
        else if(op.getResult() != null && op.getResult().toString().equalsIgnoreCase("N"))
        {
            ADFUtils.showMessage("Calibration Request No. could not be generated. Try Again !!", 0);
            return null;
        }
        return null;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }
    
    private void cevmodecheck(){
                if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
                    makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
                    cevModeDisableComponent("V");
                }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
                    makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                    cevModeDisableComponent("C");
                }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
                    makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                    cevModeDisableComponent("E");
                }
            }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                    try {
                        Method method1 =
                            component.getClass().getMethod("setDisabled", boolean.class);
                        if (method1 != null) {
                            method1.invoke(component, valueComponent);
                        }
                    } catch (NoSuchMethodException e) {
                        try {
                            Method method =
                                component.getClass().getMethod("setReadOnly", boolean.class);
                            if (method != null) {
                                method.invoke(component, valueComponent);
                            }
                        } catch (Exception e1) {
                            // e.printStackTrace();//silently eat this exception.
                        }


                    } catch (Exception e) {
                        // e.printStackTrace();//silently eat this exception.
                    }
                    List<UIComponent> childComponents = component.getChildren();
                    for (UIComponent comp : childComponents) {
                        makeComponentHierarchyReadOnly(comp, valueComponent);
                    }
                }
    
    public void cevModeDisableComponent(String mode) {
                if (mode.equals("E")) 
                {
                    getVenDescBinding().setDisabled(true);
                    getIgCodeDescBinding().setDisabled(true);
                    getInstaNoDescBinding().setDisabled(true);
                    getEntryDateBinding().setDisabled(true);
                    getEntryNoBinding().setDisabled(true);
                    getGetUnitCodeBinding().setDisabled(true);  
                   // getHeaderEditBinding().setDisabled(true);
                } else if (mode.equals("C"))
                {
                    getVenDescBinding().setDisabled(true);
                    getIgCodeDescBinding().setDisabled(true);
                    getInstaNoDescBinding().setDisabled(true);
                    getEntryNoBinding().setDisabled(true);
                    //getGetUnitCodeBinding().setDisabled(true);
                   // getHeaderEditBinding().setDisabled(true);
                } else if (mode.equals("V")) {
                    //getHeaderEditBinding().setDisabled(false);
//                    getDetailcreateBinding().setDisabled(true);
//                    getDetaildeleteBinding().setDisabled(true);
                }
                
            }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setEntryNoBinding(RichInputText entryNoBinding) {
        this.entryNoBinding = entryNoBinding;
    }

    public RichInputText getEntryNoBinding() {
        return entryNoBinding;
    }

    public void setGetUnitCodeBinding(RichInputComboboxListOfValues getUnitCodeBinding) {
        this.getUnitCodeBinding = getUnitCodeBinding;
    }

    public RichInputComboboxListOfValues getGetUnitCodeBinding() {
        return getUnitCodeBinding;
    }

    public void setEntryDateBinding(RichInputDate entryDateBinding) {
        this.entryDateBinding = entryDateBinding;
    }

    public RichInputDate getEntryDateBinding() {
        return entryDateBinding;
    }

    public void setInstaNoDescBinding(RichInputText instaNoDescBinding) {
        this.instaNoDescBinding = instaNoDescBinding;
    }

    public RichInputText getInstaNoDescBinding() {
        return instaNoDescBinding;
    }

    public void setIgCodeDescBinding(RichInputText igCodeDescBinding) {
        this.igCodeDescBinding = igCodeDescBinding;
    }

    public RichInputText getIgCodeDescBinding() {
        return igCodeDescBinding;
    }

    public void setVenDescBinding(RichInputText venDescBinding) {
        this.venDescBinding = venDescBinding;
    }

    public RichInputText getVenDescBinding() {
        return venDescBinding;
    }
}
