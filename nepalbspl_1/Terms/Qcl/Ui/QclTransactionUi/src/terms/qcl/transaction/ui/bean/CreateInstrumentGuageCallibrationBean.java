package terms.qcl.transaction.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichButton;

import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;

import terms.qcl.transaction.model.view.InstrumentGaugeCalibrationDetailVORowImpl;

public class CreateInstrumentGuageCallibrationBean {
    private RichButton headerEditBinding;
    private RichInputText calibEntNobinding;
    private RichOutputText bindingOutputText;
    private RichInputComboboxListOfValues unitCdBinding;
    private RichInputComboboxListOfValues instCdBinding;
    private RichInputText instDescBinding;
    private RichInputText temBinding;
    private RichInputText humidBinding;
    private RichInputDate currDateBinding;
    private RichInputText modelBinding;
    private RichInputText instSrNoBinding;
    private RichInputDate issuedDateBinding;
    private RichInputText empBinding;
    private RichInputText deptBinding;
    private RichInputText remarksBinding;
    private RichInputText instCdDetailBinding;
    private RichInputText instDescDeatilBinding;
    private RichInputText obsSizeBinding;
    private RichInputText acSizeBinding;
    private RichInputComboboxListOfValues instStatBinding;
    private RichInputText nxtCalDateBinding;
    private RichInputText frequencyBinding;
    private RichInputText calDescBinding;
    private RichInputText certificateNoBinding;
    private RichInputDate dateofOutCaliBinding;
    private RichInputText decriptionBinding;
    private RichTable observationTableBinding;
    private RichTable calibTableBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichShowDetailItem masterTabBinding;
    private RichShowDetailItem observTabBinding;
    private RichShowDetailItem statTabBinding;
    private RichShowDetailItem calibTabBinding;
    private RichShowDetailItem outSideTabBinding;
    private RichInputText calibNoBinidng;
    private RichButton create1ButtonBinding;
    private RichButton delete1ButtonBinding;
    private RichButton create2ButtonBinding;
    private RichButton delete2ButtonBinding;
    private RichInputComboboxListOfValues calCodeBinding;
    private RichInputText differenceBinding;
    private RichInputText resultBinding;

    public CreateInstrumentGuageCallibrationBean() {
    }

    public void saveAL(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedBy("InstrumentGaugeCalibrationHeaderVO1Iterator");
        ADFUtils.findOperation("geneCalbEntNo").execute();
        Integer TransValue = (Integer)bindingOutputText.getValue();
        System.out.println("TRANS VALUE"+TransValue);
       
        if(instStatBinding.getValue() != null)
        {

        if(TransValue==0)      
        {
           ADFUtils.findOperation("Commit").execute();
           FacesMessage Message = new FacesMessage("Record Save Successfully. Entry No Generated is " + calibEntNobinding.getValue()+".");  
           Message.setSeverity(FacesMessage.SEVERITY_INFO);  
           FacesContext fc = FacesContext.getCurrentInstance();  
           fc.addMessage(null, Message);     
           
        }
        else
        {
            ADFUtils.findOperation("Commit").execute();
            FacesMessage Message = new FacesMessage("Record Update Successfully.");  
            Message.setSeverity(FacesMessage.SEVERITY_INFO);  
            FacesContext fc = FacesContext.getCurrentInstance();  
            fc.addMessage(null, Message);     
        }
        }
        else{
             ADFUtils.showMessage("Please enter Instrument Status.", 0);
            }
//        if(instStatBinding.getValue() != null){
//        OperationBinding op=(OperationBinding)ADFUtils.findOperation("geneCalbEntNo");
//        Object obj= op.execute();
//                    System.out.println("result after function calling: "+op.getResult());
//            if(op.getResult()!=null ){
//                System.out.println("First:=>"+op.getResult());
//                   
//                if(op.getResult().equals("Y"))
//                {
//                System.out.println("Second:=>"+op.getResult());
////                ADFUtils.findOperation("Commit").execute();
//                ADFUtils.showMessage("Entry No Generated is "+calibEntNobinding.getValue(), 2);
//                }
//                else
//                        {
////                        ADFUtils.findOperation("Commit").execute();
//                        ADFUtils.showMessage("Record Updated successfully.", 2);
//                        }
//                }
//        }
// else{
//            ADFUtils.showMessage("Please enter Instrument Status.", 0);
//            }
    }
   
    public void deleteObserDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok")){
                ADFUtils.findOperation("Delete").execute();
                    System.out.println("Record Delete Successfully");
            FacesMessage Message = new FacesMessage("Record Deleted Successfully"); 
                   Message.setSeverity(FacesMessage.SEVERITY_INFO); 
            FacesContext fc = FacesContext.getCurrentInstance(); 
                         fc.addMessage(null, Message); 
                    }
                AdfFacesContext.getCurrentInstance().addPartialTarget(observationTableBinding);
    }

    public void deleteCalibDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok")){
                ADFUtils.findOperation("Delete1").execute();
                    System.out.println("Record Delete Successfully");
            FacesMessage Message = new FacesMessage("Record Deleted Successfully"); 
                   Message.setSeverity(FacesMessage.SEVERITY_INFO); 
            FacesContext fc = FacesContext.getCurrentInstance(); 
                         fc.addMessage(null, Message); 
                    }
                AdfFacesContext.getCurrentInstance().addPartialTarget(calibTableBinding);
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }
   
    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setCalibEntNobinding(RichInputText calibEntNobinding) {
        this.calibEntNobinding = calibEntNobinding;
    }

    public RichInputText getCalibEntNobinding() {
        return calibEntNobinding;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setUnitCdBinding(RichInputComboboxListOfValues unitCdBinding) {
        this.unitCdBinding = unitCdBinding;
    }

    public RichInputComboboxListOfValues getUnitCdBinding() {
        return unitCdBinding;
    }
    public void setInstCdBinding(RichInputComboboxListOfValues instCdBinding) {
        this.instCdBinding = instCdBinding;
    }

    public RichInputComboboxListOfValues getInstCdBinding() {
        return instCdBinding;
    }

    public void setInstDescBinding(RichInputText instDescBinding) {
        this.instDescBinding = instDescBinding;
    }

    public RichInputText getInstDescBinding() {
        return instDescBinding;
    }

    public void setTemBinding(RichInputText temBinding) {
        this.temBinding = temBinding;
    }

    public RichInputText getTemBinding() {
        return temBinding;
    }

    public void setHumidBinding(RichInputText humidBinding) {
        this.humidBinding = humidBinding;
    }

    public RichInputText getHumidBinding() {
        return humidBinding;
    }

    public void setCalibNoBinidng(RichInputText calibNoBinidng) {
        this.calibNoBinidng = calibNoBinidng;
    }

    public RichInputText getCalibNoBinidng() {
        return calibNoBinidng;
    }

    public void setCurrDateBinding(RichInputDate currDateBinding) {
        this.currDateBinding = currDateBinding;
    }

    public RichInputDate getCurrDateBinding() {
        return currDateBinding;
    }

    public void setModelBinding(RichInputText modelBinding) {
        this.modelBinding = modelBinding;
    }

    public RichInputText getModelBinding() {
        return modelBinding;
    }

    public void setInstSrNoBinding(RichInputText instSrNoBinding) {
        this.instSrNoBinding = instSrNoBinding;
    }

    public RichInputText getInstSrNoBinding() {
        return instSrNoBinding;
    }

    public void setIssuedDateBinding(RichInputDate issuedDateBinding) {
        this.issuedDateBinding = issuedDateBinding;
    }

    public RichInputDate getIssuedDateBinding() {
        return issuedDateBinding;
    }

    public void setEmpBinding(RichInputText empBinding) {
        this.empBinding = empBinding;
    }

    public RichInputText getEmpBinding() {
        return empBinding;
    }

    public void setDeptBinding(RichInputText deptBinding) {
        this.deptBinding = deptBinding;
    }

    public RichInputText getDeptBinding() {
        return deptBinding;
    }

    public void setRemarksBinding(RichInputText remarksBinding) {
        this.remarksBinding = remarksBinding;
    }

    public RichInputText getRemarksBinding() {
        return remarksBinding;
    }

    public void setInstCdDetailBinding(RichInputText instCdDetailBinding) {
        this.instCdDetailBinding = instCdDetailBinding;
    }

    public RichInputText getInstCdDetailBinding() {
        return instCdDetailBinding;
    }

    public void setInstDescDeatilBinding(RichInputText instDescDeatilBinding) {
        this.instDescDeatilBinding = instDescDeatilBinding;
    }

    public RichInputText getInstDescDeatilBinding() {
        return instDescDeatilBinding;
    }

    public void setObsSizeBinding(RichInputText obsSizeBinding) {
        this.obsSizeBinding = obsSizeBinding;
    }

    public RichInputText getObsSizeBinding() {
        return obsSizeBinding;
    }

    public void setAcSizeBinding(RichInputText acSizeBinding) {
        this.acSizeBinding = acSizeBinding;
    }

    public RichInputText getAcSizeBinding() {
        return acSizeBinding;
    }
   
    public void setInstStatBinding(RichInputComboboxListOfValues instStatBinding) {
        this.instStatBinding = instStatBinding;
    }

    public RichInputComboboxListOfValues getInstStatBinding() {
        return instStatBinding;
    }

    public void setNxtCalDateBinding(RichInputText nxtCalDateBinding) {
        this.nxtCalDateBinding = nxtCalDateBinding;
    }

    public RichInputText getNxtCalDateBinding() {
        return nxtCalDateBinding;
    }

    public void setFrequencyBinding(RichInputText frequencyBinding) {
        this.frequencyBinding = frequencyBinding;
    }

    public RichInputText getFrequencyBinding() {
        return frequencyBinding;
    }

    public void setCalCodeBinding(RichInputComboboxListOfValues calCodeBinding) {
        this.calCodeBinding = calCodeBinding;
    }

    public RichInputComboboxListOfValues getCalCodeBinding() {
        return calCodeBinding;
    }

    public void setCalDescBinding(RichInputText calDescBinding) {
        this.calDescBinding = calDescBinding;
    }

    public RichInputText getCalDescBinding() {
        return calDescBinding;
    }

    public void setCertificateNoBinding(RichInputText certificateNoBinding) {
        this.certificateNoBinding = certificateNoBinding;
    }

    public RichInputText getCertificateNoBinding() {
        return certificateNoBinding;
    }

    public void setDateofOutCaliBinding(RichInputDate dateofOutCaliBinding) {
        this.dateofOutCaliBinding = dateofOutCaliBinding;
    }

    public RichInputDate getDateofOutCaliBinding() {
        return dateofOutCaliBinding;
    }

    public void setDecriptionBinding(RichInputText decriptionBinding) {
        this.decriptionBinding = decriptionBinding;
    }

    public RichInputText getDecriptionBinding() {
        return decriptionBinding;
    }

    public void setObservationTableBinding(RichTable observationTableBinding) {
        this.observationTableBinding = observationTableBinding;
    }

    public RichTable getObservationTableBinding() {
        return observationTableBinding;
    }

    public void setCalibTableBinding(RichTable calibTableBinding) {
        this.calibTableBinding = calibTableBinding;
    }

    public RichTable getCalibTableBinding() {
        return calibTableBinding;
    }
   
    public void setMasterTabBinding(RichShowDetailItem masterTabBinding) {
        this.masterTabBinding = masterTabBinding;
    }

    public RichShowDetailItem getMasterTabBinding() {
        return masterTabBinding;
    }

    public void setObservTabBinding(RichShowDetailItem observTabBinding) {
        this.observTabBinding = observTabBinding;
    }

    public RichShowDetailItem getObservTabBinding() {
        return observTabBinding;
    }

    public void setStatTabBinding(RichShowDetailItem statTabBinding) {
        this.statTabBinding = statTabBinding;
    }

    public RichShowDetailItem getStatTabBinding() {
        return statTabBinding;
    }

    public void setCalibTabBinding(RichShowDetailItem calibTabBinding) {
        this.calibTabBinding = calibTabBinding;
    }

    public RichShowDetailItem getCalibTabBinding() {
        return calibTabBinding;
    }

    public void setOutSideTabBinding(RichShowDetailItem outSideTabBinding) {
        this.outSideTabBinding = outSideTabBinding;
    }

    public RichShowDetailItem getOutSideTabBinding() {
        return outSideTabBinding;
    }
   
    public void setCreate1ButtonBinding(RichButton create1ButtonBinding) {
        this.create1ButtonBinding = create1ButtonBinding;
    }

    public RichButton getCreate1ButtonBinding() {
        return create1ButtonBinding;
    }

    public void setDelete1ButtonBinding(RichButton delete1ButtonBinding) {
        this.delete1ButtonBinding = delete1ButtonBinding;
    }

    public RichButton getDelete1ButtonBinding() {
        return delete1ButtonBinding;
    }

    public void setCreate2ButtonBinding(RichButton create2ButtonBinding) {
        this.create2ButtonBinding = create2ButtonBinding;
    }

    public RichButton getCreate2ButtonBinding() {
        return create2ButtonBinding;
    }

    public void setDelete2ButtonBinding(RichButton delete2ButtonBinding) {
        this.delete2ButtonBinding = delete2ButtonBinding;
    }

    public RichButton getDelete2ButtonBinding() {
        return delete2ButtonBinding;
    }

    private void cevmodecheck(){
                if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
                    makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
                    cevModeDisableComponent("V");
                }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
                    makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                    cevModeDisableComponent("C");
                }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
                    makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                    cevModeDisableComponent("E");
                }
            }
   
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                    try {
                        Method method1 =
                            component.getClass().getMethod("setDisabled", boolean.class);
                        if (method1 != null) {
                            method1.invoke(component, valueComponent);
                        }
                    } catch (NoSuchMethodException e) {
                        try {
                            Method method =
                                component.getClass().getMethod("setReadOnly", boolean.class);
                            if (method != null) {
                                method.invoke(component, valueComponent);
                            }
                        } catch (Exception e1) {
                            // e.printStackTrace();//silently eat this exception.
                        }
                    } catch (Exception e) {
                        // e.printStackTrace();//silently eat this exception.
                    }
                    List<UIComponent> childComponents = component.getChildren();
                    for (UIComponent comp : childComponents) {
                        makeComponentHierarchyReadOnly(comp, valueComponent);
                    }
                }
   
    public void cevModeDisableComponent(String mode) {
                if (mode.equals("E")){
                    getHeaderEditBinding().setDisabled(true);
                    getMasterTabBinding().setDisabled(false);
                    getObservTabBinding().setDisabled(false);
                    getStatTabBinding().setDisabled(false);
                    getCalibTabBinding().setDisabled(false);
                    getOutSideTabBinding().setDisabled(false);
                   
                    getUnitCdBinding().setDisabled(true);
                    getInstCdBinding().setDisabled(false);
                    getCalibNoBinidng().setDisabled(true);
                    getCalibEntNobinding().setDisabled(true);
                    getTemBinding().setDisabled(true);
                    getHumidBinding().setDisabled(true);
                    getCurrDateBinding().setDisabled(true);
                    getModelBinding().setDisabled(true);
                    getInstSrNoBinding().setDisabled(true);
                    getIssuedDateBinding().setDisabled(true);
                    getEmpBinding().setDisabled(true);
                    getDeptBinding().setDisabled(true);
                    getRemarksBinding().setDisabled(false);
                   
                    getInstCdDetailBinding().setDisabled(true);
                    getInstDescDeatilBinding().setDisabled(true);
                    getObsSizeBinding().setDisabled(false);
                    //getAcSizeBinding().setDisabled(true);
                    getCreate1ButtonBinding().setDisabled(false);
                    getDelete1ButtonBinding().setDisabled(false);
                   
                    getInstStatBinding().setDisabled(true);
                    getNxtCalDateBinding().setDisabled(true);
                    getFrequencyBinding().setDisabled(true);
                   
                    getCalCodeBinding().setDisabled(false);
                    getCalDescBinding().setDisabled(true);
                    getCreate2ButtonBinding().setDisabled(false);
                    getDelete2ButtonBinding().setDisabled(false);
                   
                    getCertificateNoBinding().setDisabled(true);
                    getDateofOutCaliBinding().setDisabled(true);
                    getDecriptionBinding().setDisabled(true);
                    getDifferenceBinding().setDisabled(true);
                    getResultBinding().setDisabled(true);
//                    getFrequencyBinding().setDisabled(true);
                   
        }else if (mode.equals("C")){
                    getHeaderEditBinding().setDisabled(true);
                    getMasterTabBinding().setDisabled(false);
                    getObservTabBinding().setDisabled(false);
                    getStatTabBinding().setDisabled(false);
                    getCalibTabBinding().setDisabled(false);
                    getOutSideTabBinding().setDisabled(false);
                   
                    getUnitCdBinding().setDisabled(true);
                    getInstCdBinding().setDisabled(false);
                    getCalibNoBinidng().setDisabled(true);
                    getCalibEntNobinding().setDisabled(true);
                    getTemBinding().setDisabled(false);
                    getHumidBinding().setDisabled(false);
                    getCurrDateBinding().setDisabled(false);
                    getModelBinding().setDisabled(true);
                    getInstSrNoBinding().setDisabled(true);
                    getIssuedDateBinding().setDisabled(true);
                    getEmpBinding().setDisabled(true);
                    getDeptBinding().setDisabled(true);
                    getRemarksBinding().setDisabled(false);
                   
                    getInstCdDetailBinding().setDisabled(true);
                    getInstDescDeatilBinding().setDisabled(true);
                    getObsSizeBinding().setDisabled(false);
                    //getAcSizeBinding().setDisabled(true);
                    getCreate1ButtonBinding().setDisabled(false);
                    getDelete1ButtonBinding().setDisabled(false);
                   
                    getInstStatBinding().setDisabled(false);
                    getNxtCalDateBinding().setDisabled(true);
                    getFrequencyBinding().setDisabled(true);
                   
                    getCalCodeBinding().setDisabled(false);
                    getCalDescBinding().setDisabled(true);
                    getCreate2ButtonBinding().setDisabled(false);
                    getDelete2ButtonBinding().setDisabled(false);
                   
                    getCertificateNoBinding().setDisabled(false);
                    getDateofOutCaliBinding().setDisabled(false);
                    getDecriptionBinding().setDisabled(false);
                    getDifferenceBinding().setDisabled(true);
                    getResultBinding().setDisabled(true);
                   
                }else if (mode.equals("V")){
                    getHeaderEditBinding().setDisabled(false);
                    getMasterTabBinding().setDisabled(false);
                    getObservTabBinding().setDisabled(false);
                    getStatTabBinding().setDisabled(false);
                    getCalibTabBinding().setDisabled(false);
                    getOutSideTabBinding().setDisabled(false); 
                   
                    getUnitCdBinding().setDisabled(true);
                    getInstCdBinding().setDisabled(true);
                    getCalibNoBinidng().setDisabled(true);
                    getCalibEntNobinding().setDisabled(true);
                    getTemBinding().setDisabled(true);
                    getHumidBinding().setDisabled(true);
                    getCurrDateBinding().setDisabled(true);
                    getModelBinding().setDisabled(true);
                    getInstSrNoBinding().setDisabled(true);
                    getIssuedDateBinding().setDisabled(true);
                    getEmpBinding().setDisabled(true);
                    getDeptBinding().setDisabled(true);
                    getRemarksBinding().setDisabled(true);
                   
                    getInstCdDetailBinding().setDisabled(true);
                    getInstDescDeatilBinding().setDisabled(true);
                    getObsSizeBinding().setDisabled(true);
                    //getAcSizeBinding().setDisabled(true);
                    getCreate1ButtonBinding().setDisabled(true);
                    getDelete1ButtonBinding().setDisabled(true);
                   
                    getInstStatBinding().setDisabled(true);
                    getNxtCalDateBinding().setDisabled(true);
                    getFrequencyBinding().setDisabled(true);
                   
                    getCalCodeBinding().setDisabled(true);
                    getCalDescBinding().setDisabled(true);
                    getCreate2ButtonBinding().setDisabled(true);
                    getDelete2ButtonBinding().setDisabled(true);
                   
                    getCertificateNoBinding().setDisabled(true);
                    getDateofOutCaliBinding().setDisabled(true);
                    getDecriptionBinding().setDisabled(true);
                }
               
            }
   
    private Integer fetchMaxLineNumber(DCIteratorBinding itr) {
           System.out.println("####################In fetchmaxline number###########################");
           Integer max = new Integer(0);
           System.out.println("Starting Max value:" + max);
           InstrumentGaugeCalibrationDetailVORowImpl currRow = (InstrumentGaugeCalibrationDetailVORowImpl) itr.getCurrentRow();
           //        System.out.println("sequence number in bankcashvoucherdetail:" + currRow.getSeqNo());
           if (currRow != null && currRow.getSNo() != null)
               max = currRow.getSNo();
           System.out.println("Max Value after setting currentseqno:" + max);
           RowSetIterator rsi = itr.getRowSetIterator();
           if (rsi != null) {
               Row[] allRowsInRange = rsi.getAllRowsInRange();
               for (Row rw : allRowsInRange) {
                   if (rw != null && rw.getAttribute("SNo") != null &&
                       max.compareTo(Integer.parseInt(rw.getAttribute("SNo").toString())) < 0)
                       System.out.println("rw.getAttribute(\"SNo\")" + rw.getAttribute("SNo"));
                   max = Integer.parseInt(rw.getAttribute("SNo").toString());
               }
           }
           rsi.closeRowSetIterator();
           max = max + new Integer(1);
           System.out.println("max VAlue:" + max);
           System.out.println("####################out fetchmaxline number###########################");
           return max;

       }
   
    public void createDetailAL(ActionEvent actionEvent) {
        System.out.println("*************************************In Detail button action******************************");
                DCIteratorBinding dci = ADFUtils.findIterator("InstrumentGaugeCalibrationDetailVO1Iterator");
                if (dci != null) {
                    Integer seq_no = fetchMaxLineNumber(dci);
                    System.out.println("Sequence Number after fetchMaxLineNumber:" + seq_no);
                    RowSetIterator rsi = dci.getRowSetIterator();
                    if (rsi != null) {
                        Row last = rsi.last();
                        int i = rsi.getRangeIndexOf(last);
                        InstrumentGaugeCalibrationDetailVORowImpl newRow = (InstrumentGaugeCalibrationDetailVORowImpl) rsi.createRow();
                        newRow.setNewRowState(Row.STATUS_INITIALIZED);
                        rsi.insertRowAtRangeIndex(i + 1, newRow);
                        rsi.setCurrentRow(newRow);
                        newRow.setSNo(seq_no);
                        }
                    rsi.closeRowSetIterator();
                }
    }

    public BindingContainer getBindings()
      {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
      }
    public void instrumentCodeVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce!=null){
            if(vce.getNewValue()!=vce.getOldValue()){
            DCIteratorBinding poIter = (DCIteratorBinding) getBindings().get("InstrumentGaugeCalibrationHeaderVO1Iterator");
            poIter.getCurrentRow().setAttribute("CurDate", null);
            }
        }
    }

    public void differenceVCL(ValueChangeEvent vce) {
        System.out.println("INSIDE vce ");
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        
        ADFUtils.findOperation("checkLowerUpperForInstGaugeCalib").execute();
        ADFUtils.findOperation("changeStatusValueInInstrumentCalibration").execute();
        
    }

    public void setDifferenceBinding(RichInputText differenceBinding) {
        this.differenceBinding = differenceBinding;
    }

    public RichInputText getDifferenceBinding() {
        return differenceBinding;
    }

    public void setResultBinding(RichInputText resultBinding) {
        this.resultBinding = resultBinding;
    }

    public RichInputText getResultBinding() {
        return resultBinding;
    }

    public void resultVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        
        String val1 = "COK";
        String val2 = "CNR";
        if(vce.getNewValue().equals("Ok")) {
            instStatBinding.setValue(val1);
        }
        else {
            instStatBinding.setValue(val2);
        }
    }
}
