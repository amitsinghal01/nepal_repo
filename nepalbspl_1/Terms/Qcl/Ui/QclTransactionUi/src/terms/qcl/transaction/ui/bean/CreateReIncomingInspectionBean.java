package terms.qcl.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;

public class CreateReIncomingInspectionBean {
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichButton pendingInspectionButtonBinding;
    private RichPopup pendingInspectionPopupBinding;
    private RichTable reIncomingInspectionDetailTableBinding;
    private RichInputText srNoBinding;
    private RichInputText inspectedItemBinding;
    private RichInputText checkDescriptionBinding;
    private RichInputText inspectionTypeBinding;
    private RichInputText upperLimitBinding;
    private RichInputText lowerLimitBinding;
    private RichInputText uomBinding;
    private RichInputText calculativeFlagBinding;
    private RichInputText observationBinding;
    private RichInputText batchQtyDtlBinding;
    private RichInputText acceptQtyDtlBinding;
    private RichInputText rejectQtyDtlBinding;
    private RichInputText remarksBinding;
    private RichInputText inspectionNoBinding;
    private RichInputDate inspectionDateBinding;
    private RichSelectOneChoice qualityTypeBinding;
    private RichSelectOneChoice inspectionStatusBinding;
    private RichInputText batchNoBinding;
    private RichInputText qcdNoBinding;
    private RichInputComboboxListOfValues inspectionByBinding;
    private RichInputText itemCodeBinding;
    private RichInputText mriNoBinding;
    private RichInputDate mriDateBinding;
    private RichInputText poNoBinding;
    private RichInputText batchQuantityBinding;
    private RichInputText manufacturerNameBinding;
    private RichInputText acceptedQtyBinding;
    private RichInputText rejectQtyBinding;
    private RichOutputText outputTextBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichInputComboboxListOfValues supplierCodeBinding;
    private RichInputText okNotOkBinding;
    private RichInputComboboxListOfValues approvedByBinding;

    public CreateReIncomingInspectionBean() {
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void populatePendingInspectionAL(ActionEvent actionEvent) {
        RichPopup.PopupHints popup=new RichPopup.PopupHints();
        getPendingInspectionPopupBinding().show(popup);
//        ADFUtils.findOperation("populateReIncmgInspectionPendingInspectionQuery").execute();
//        pendingInspectionPopupBinding.cancel();
        
    }

    public void setPendingInspectionButtonBinding(RichButton pendingInspectionButtonBinding) {
        this.pendingInspectionButtonBinding = pendingInspectionButtonBinding;
    }

    public RichButton getPendingInspectionButtonBinding() {
        return pendingInspectionButtonBinding;
    }

    public void setPendingInspectionPopupBinding(RichPopup pendingInspectionPopupBinding) {
        this.pendingInspectionPopupBinding = pendingInspectionPopupBinding;
    }

    public RichPopup getPendingInspectionPopupBinding() {
        return pendingInspectionPopupBinding;
    }

    public void populatePendingInspectionButtonAL(ActionEvent actionEvent) {
        System.out.println("INSIDE populatePendingInspectionButtonAL ##### ");
        OperationBinding ob = (OperationBinding)ADFUtils.findOperation("populateReIncmgInspectionPendingInspectionQuery");
        Object op = ob.execute();
        AdfFacesContext.getCurrentInstance().addPartialTarget(reIncomingInspectionDetailTableBinding);
        
        if(op != null && op.equals("I")) {
            ADFUtils.showMessage("No Checkpoints Has Been Entered For This Part & Process.", 0);
        }
        getPendingInspectionPopupBinding().cancel();
    }

    public void setReIncomingInspectionDetailTableBinding(RichTable reIncomingInspectionDetailTableBinding) {
        this.reIncomingInspectionDetailTableBinding = reIncomingInspectionDetailTableBinding;
    }

    public RichTable getReIncomingInspectionDetailTableBinding() {
        return reIncomingInspectionDetailTableBinding;
    }

    public void setSrNoBinding(RichInputText srNoBinding) {
        this.srNoBinding = srNoBinding;
    }

    public RichInputText getSrNoBinding() {
        return srNoBinding;
    }

    public void setInspectedItemBinding(RichInputText inspectedItemBinding) {
        this.inspectedItemBinding = inspectedItemBinding;
    }

    public RichInputText getInspectedItemBinding() {
        return inspectedItemBinding;
    }

    public void setCheckDescriptionBinding(RichInputText checkDescriptionBinding) {
        this.checkDescriptionBinding = checkDescriptionBinding;
    }

    public RichInputText getCheckDescriptionBinding() {
        return checkDescriptionBinding;
    }

    public void setInspectionTypeBinding(RichInputText inspectionTypeBinding) {
        this.inspectionTypeBinding = inspectionTypeBinding;
    }

    public RichInputText getInspectionTypeBinding() {
        return inspectionTypeBinding;
    }

    public void setUpperLimitBinding(RichInputText upperLimitBinding) {
        this.upperLimitBinding = upperLimitBinding;
    }

    public RichInputText getUpperLimitBinding() {
        return upperLimitBinding;
    }

    public void setLowerLimitBinding(RichInputText lowerLimitBinding) {
        this.lowerLimitBinding = lowerLimitBinding;
    }

    public RichInputText getLowerLimitBinding() {
        return lowerLimitBinding;
    }

    public void setUomBinding(RichInputText uomBinding) {
        this.uomBinding = uomBinding;
    }

    public RichInputText getUomBinding() {
        return uomBinding;
    }

    public void setCalculativeFlagBinding(RichInputText calculativeFlagBinding) {
        this.calculativeFlagBinding = calculativeFlagBinding;
    }

    public RichInputText getCalculativeFlagBinding() {
        return calculativeFlagBinding;
    }

    public void setObservationBinding(RichInputText observationBinding) {
        this.observationBinding = observationBinding;
    }

    public RichInputText getObservationBinding() {
        return observationBinding;
    }

    public void setBatchQtyDtlBinding(RichInputText batchQtyDtlBinding) {
        this.batchQtyDtlBinding = batchQtyDtlBinding;
    }

    public RichInputText getBatchQtyDtlBinding() {
        return batchQtyDtlBinding;
    }

    public void setAcceptQtyDtlBinding(RichInputText acceptQtyDtlBinding) {
        this.acceptQtyDtlBinding = acceptQtyDtlBinding;
    }

    public RichInputText getAcceptQtyDtlBinding() {
        return acceptQtyDtlBinding;
    }

    public void setRejectQtyDtlBinding(RichInputText rejectQtyDtlBinding) {
        this.rejectQtyDtlBinding = rejectQtyDtlBinding;
    }

    public RichInputText getRejectQtyDtlBinding() {
        return rejectQtyDtlBinding;
    }

    public void setRemarksBinding(RichInputText remarksBinding) {
        this.remarksBinding = remarksBinding;
    }

    public RichInputText getRemarksBinding() {
        return remarksBinding;
    }

    public void setInspectionNoBinding(RichInputText inspectionNoBinding) {
        this.inspectionNoBinding = inspectionNoBinding;
    }

    public RichInputText getInspectionNoBinding() {
        return inspectionNoBinding;
    }

    public void setInspectionDateBinding(RichInputDate inspectionDateBinding) {
        this.inspectionDateBinding = inspectionDateBinding;
    }

    public RichInputDate getInspectionDateBinding() {
        return inspectionDateBinding;
    }

    public void setQualityTypeBinding(RichSelectOneChoice qualityTypeBinding) {
        this.qualityTypeBinding = qualityTypeBinding;
    }

    public RichSelectOneChoice getQualityTypeBinding() {
        return qualityTypeBinding;
    }

    public void setInspectionStatusBinding(RichSelectOneChoice inspectionStatusBinding) {
        this.inspectionStatusBinding = inspectionStatusBinding;
    }

    public RichSelectOneChoice getInspectionStatusBinding() {
        return inspectionStatusBinding;
    }

    public void setBatchNoBinding(RichInputText batchNoBinding) {
        this.batchNoBinding = batchNoBinding;
    }

    public RichInputText getBatchNoBinding() {
        return batchNoBinding;
    }

    public void setQcdNoBinding(RichInputText qcdNoBinding) {
        this.qcdNoBinding = qcdNoBinding;
    }

    public RichInputText getQcdNoBinding() {
        return qcdNoBinding;
    }

    public void setInspectionByBinding(RichInputComboboxListOfValues inspectionByBinding) {
        this.inspectionByBinding = inspectionByBinding;
    }

    public RichInputComboboxListOfValues getInspectionByBinding() {
        return inspectionByBinding;
    }

    public void setItemCodeBinding(RichInputText itemCodeBinding) {
        this.itemCodeBinding = itemCodeBinding;
    }

    public RichInputText getItemCodeBinding() {
        return itemCodeBinding;
    }

    public void setMriNoBinding(RichInputText mriNoBinding) {
        this.mriNoBinding = mriNoBinding;
    }

    public RichInputText getMriNoBinding() {
        return mriNoBinding;
    }

    public void setMriDateBinding(RichInputDate mriDateBinding) {
        this.mriDateBinding = mriDateBinding;
    }

    public RichInputDate getMriDateBinding() {
        return mriDateBinding;
    }

    public void setPoNoBinding(RichInputText poNoBinding) {
        this.poNoBinding = poNoBinding;
    }

    public RichInputText getPoNoBinding() {
        return poNoBinding;
    }

    public void setBatchQuantityBinding(RichInputText batchQuantityBinding) {
        this.batchQuantityBinding = batchQuantityBinding;
    }

    public RichInputText getBatchQuantityBinding() {
        return batchQuantityBinding;
    }

    public void setManufacturerNameBinding(RichInputText manufacturerNameBinding) {
        this.manufacturerNameBinding = manufacturerNameBinding;
    }

    public RichInputText getManufacturerNameBinding() {
        return manufacturerNameBinding;
    }

    public void setAcceptedQtyBinding(RichInputText acceptedQtyBinding) {
        this.acceptedQtyBinding = acceptedQtyBinding;
    }

    public RichInputText getAcceptedQtyBinding() {
        return acceptedQtyBinding;
    }

    public void setRejectQtyBinding(RichInputText rejectQtyBinding) {
        this.rejectQtyBinding = rejectQtyBinding;
    }

    public RichInputText getRejectQtyBinding() {
        return rejectQtyBinding;
    }

    public void setOutputTextBinding(RichOutputText outputTextBinding) {
        this.outputTextBinding = outputTextBinding;
    }

    public RichOutputText getOutputTextBinding() {
        cevmodecheck();
        return outputTextBinding;
    }
    
    private void cevmodecheck(){
            if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
               
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
                cevModeDisableComponent("V");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
                
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("C");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
                
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("E");
            }
        }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                try {
                    Method method1 =
                        component.getClass().getMethod("setDisabled", boolean.class);
                    if (method1 != null) {
                        method1.invoke(component, valueComponent);
                    }
                } catch (NoSuchMethodException e) {
                    try {
                        Method method =
                            component.getClass().getMethod("setReadOnly", boolean.class);
                        if (method != null) {
                            method.invoke(component, valueComponent);
                        }
                    } catch (Exception e1) {
                        // e.printStackTrace();//silently eat this exception.
                    }


                } catch (Exception e) {
                    // e.printStackTrace();//silently eat this exception.
                }
                List<UIComponent> childComponents = component.getChildren();
                for (UIComponent comp : childComponents) {
                    makeComponentHierarchyReadOnly(comp, valueComponent);
                }
            }
    public void cevModeDisableComponent(String mode) {
        
        if(mode.equals("E")){
            getUnitCodeBinding().setDisabled(true);
            getInspectionNoBinding().setDisabled(true);
            getInspectionDateBinding().setDisabled(true);
            getQualityTypeBinding().setDisabled(true);
            getInspectionStatusBinding().setDisabled(true);
            getQcdNoBinding().setDisabled(true);
            getBatchQuantityBinding().setDisabled(true);
            getBatchNoBinding().setDisabled(true);
            getQcdNoBinding().setDisabled(true);
            getInspectionByBinding().setDisabled(true);
            getSupplierCodeBinding().setDisabled(true);
            getItemCodeBinding().setDisabled(true);
            getMriNoBinding().setDisabled(true);
            getMriDateBinding().setDisabled(true);
            getPoNoBinding().setDisabled(true);
            getBatchQuantityBinding().setDisabled(true);
            getManufacturerNameBinding().setDisabled(true);
            getAcceptedQtyBinding().setDisabled(true);
            getRejectQtyBinding().setDisabled(true);
            getSrNoBinding().setDisabled(true);
            getInspectedItemBinding().setDisabled(true);
            getCheckDescriptionBinding().setDisabled(true);
            getInspectionTypeBinding().setDisabled(true);
            getUpperLimitBinding().setDisabled(true);
            getLowerLimitBinding().setDisabled(true);
            getUomBinding().setDisabled(true);
            getCalculativeFlagBinding().setDisabled(true);
            getObservationBinding().setDisabled(true);
            getBatchQtyDtlBinding().setDisabled(true);
            getAcceptQtyDtlBinding().setDisabled(true);
            getRejectQtyDtlBinding().setDisabled(true);
            getOkNotOkBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            }
        if(mode.equals("C")){
            getUnitCodeBinding().setDisabled(true);
            getInspectionNoBinding().setDisabled(true);
            getInspectionDateBinding().setDisabled(true);
            getInspectionStatusBinding().setDisabled(true);
            getBatchNoBinding().setDisabled(true);
            getQcdNoBinding().setDisabled(true);
            getBatchQuantityBinding().setDisabled(true);
            getItemCodeBinding().setDisabled(true);
            getMriNoBinding().setDisabled(true);
            getMriDateBinding().setDisabled(true);
            getAcceptedQtyBinding().setDisabled(true);
            getRejectQtyBinding().setDisabled(true);
            getApprovedByBinding().setDisabled(true);
            getSrNoBinding().setDisabled(true);
            getInspectedItemBinding().setDisabled(true);
            getCheckDescriptionBinding().setDisabled(true);
            getInspectionTypeBinding().setDisabled(true);
            getUpperLimitBinding().setDisabled(true);
            getLowerLimitBinding().setDisabled(true);
            getUomBinding().setDisabled(true);
            getCalculativeFlagBinding().setDisabled(true);
            getBatchQtyDtlBinding().setDisabled(true);
            getAcceptQtyDtlBinding().setDisabled(true);
            getRejectQtyDtlBinding().setDisabled(true);
            getOkNotOkBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
        }
        if (mode.equals("V")) {
    //            getRejectionDetailButtonBinding().setDisabled(false);
                            //getHeaderEditBinding().setDisabled(false);
        //                    getDetailcreateBinding().setDisabled(true);
        //                    getDetaildeleteBinding().setDisabled(true);
        }
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void observationVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
                
        if(vce != null) {
            System.out.println("INSIDE minValVCL #### ");
            ADFUtils.findOperation("setStatusValueInReIncomingInspection").execute();
        }
    }

    public void setSupplierCodeBinding(RichInputComboboxListOfValues supplierCodeBinding) {
        this.supplierCodeBinding = supplierCodeBinding;
    }

    public RichInputComboboxListOfValues getSupplierCodeBinding() {
        return supplierCodeBinding;
    }

    public void setOkNotOkBinding(RichInputText okNotOkBinding) {
        this.okNotOkBinding = okNotOkBinding;
    }

    public RichInputText getOkNotOkBinding() {
        return okNotOkBinding;
    }

    public void saveGenerateInspectionNoAL(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedBy("ReIncomingInspectionHeaderVO1Iterator");
        OperationBinding op = (OperationBinding)ADFUtils.findOperation("generateReIncomingInspectionNo");
        Object rst = op.execute();
        
        if(rst != null && rst != "") {
            if(op.getErrors().isEmpty()){
                ADFUtils.findOperation("Commit").execute();
                ADFUtils.showMessage("Record Saved Successfully " + "Inspection no is::" + rst, 2);
            }
        }
        
        if ((rst.toString() == null || rst.toString() == "" || rst.toString() == " ")) {
            if (op.getErrors().isEmpty()) {
                ADFUtils.findOperation("Commit").execute();
                ADFUtils.showMessage(" Record Updated Successfully.", 2);
            }
        }
    }

    public void setApprovedByBinding(RichInputComboboxListOfValues approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputComboboxListOfValues getApprovedByBinding() {
        return approvedByBinding;
    }
}
