package terms.qcl.transaction.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;

public class CreateReInspectionFinishGoodsBean {
    private RichPopup pendingInspectionPopupBinding;
    private RichInputText reqNoBinding;
    private RichTable reInspectionFGDtlTableBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText finalInspNoBinding;
    private RichInputDate finalInspDateBinding;
    private RichInputComboboxListOfValues productCodeBinding;
    private RichSelectOneChoice customerTypeBinding;
    private RichInputText qcdNoBinding;
    private RichInputComboboxListOfValues customerCodeBinding;
    private RichInputText productionSlipNoBinding;
    private RichInputText inspectionStatusBinding;
    private RichInputText batchQtyBinding;
    private RichInputText acceptQtyBinding;
    private RichInputText rejectQtyBinding;
    private RichInputText noOfPackingBinding;
    private RichInputText batchNoBinding;
    private RichInputText analysedByBinding;
    private RichInputText entryNoBinding;
    private RichSelectOneChoice checkPointStatusBinding;
    private RichInputText remarksBinding;
    private RichInputComboboxListOfValues approvedByBinding;
    private RichSelectOneChoice conclusionBinding;
    private RichInputText acceptQtyDtlBinding;
    private RichInputText rejectQtyDtlBinding;
    private RichInputText calculativeFlagBinding;
    private RichInputText upperLimitBinding;
    private RichInputText lowerLimitBinding;
    private RichInputText valueBinding;
    private RichOutputText outputTextBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichButton pendingInspectionButtonBinding;
    private RichInputText srNoDtlBinding;
    private RichInputText inspectionMethodNoBinding;
    private RichInputText batchQtyDtlBinding;
    private RichInputComboboxListOfValues analysedByHdBinding;
    private RichColumn chkStatBinding;
    private RichInputText okNotOkBinding;

    public CreateReInspectionFinishGoodsBean() {
    }

    public void populatePendingInspectionAL(ActionEvent actionEvent) {
        System.out.println("INSIDE populatePendingInspectionAL #### ");
        
        if(reqNoBinding.getValue() != null) {
            OperationBinding ob = (OperationBinding) ADFUtils.findOperation("populateReInspectionFinishGoods");
            Object op = ob.execute();
            AdfFacesContext.getCurrentInstance().addPartialTarget(reInspectionFGDtlTableBinding);
            
            if(op!=null && op.equals("I")) {
                ADFUtils.showMessage("No Checkpoints Has Been Entered For This Part & Process.", 0);
            }
            getPendingInspectionPopupBinding().cancel();
        }
    }

    public void setPendingInspectionPopupBinding(RichPopup pendingInspectionPopupBinding) {
        this.pendingInspectionPopupBinding = pendingInspectionPopupBinding;
    }

    public RichPopup getPendingInspectionPopupBinding() {
        return pendingInspectionPopupBinding;
    }

    public void setReqNoBinding(RichInputText reqNoBinding) {
        this.reqNoBinding = reqNoBinding;
    }

    public RichInputText getReqNoBinding() {
        return reqNoBinding;
    }

    public void pendingInspectionAL(ActionEvent actionEvent) {
        RichPopup.PopupHints popup=new RichPopup.PopupHints();
        getPendingInspectionPopupBinding().show(popup);
    }

    public void setReInspectionFGDtlTableBinding(RichTable reInspectionFGDtlTableBinding) {
        this.reInspectionFGDtlTableBinding = reInspectionFGDtlTableBinding;
    }

    public RichTable getReInspectionFGDtlTableBinding() {
        return reInspectionFGDtlTableBinding;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setFinalInspNoBinding(RichInputText finalInspNoBinding) {
        this.finalInspNoBinding = finalInspNoBinding;
    }

    public RichInputText getFinalInspNoBinding() {
        return finalInspNoBinding;
    }

    public void setFinalInspDateBinding(RichInputDate finalInspDateBinding) {
        this.finalInspDateBinding = finalInspDateBinding;
    }

    public RichInputDate getFinalInspDateBinding() {
        return finalInspDateBinding;
    }

    public void setProductCodeBinding(RichInputComboboxListOfValues productCodeBinding) {
        this.productCodeBinding = productCodeBinding;
    }

    public RichInputComboboxListOfValues getProductCodeBinding() {
        return productCodeBinding;
    }

    public void setCustomerTypeBinding(RichSelectOneChoice customerTypeBinding) {
        this.customerTypeBinding = customerTypeBinding;
    }

    public RichSelectOneChoice getCustomerTypeBinding() {
        return customerTypeBinding;
    }

    public void setQcdNoBinding(RichInputText qcdNoBinding) {
        this.qcdNoBinding = qcdNoBinding;
    }

    public RichInputText getQcdNoBinding() {
        return qcdNoBinding;
    }

    public void setCustomerCodeBinding(RichInputComboboxListOfValues customerCodeBinding) {
        this.customerCodeBinding = customerCodeBinding;
    }

    public RichInputComboboxListOfValues getCustomerCodeBinding() {
        return customerCodeBinding;
    }

    public void setProductionSlipNoBinding(RichInputText productionSlipNoBinding) {
        this.productionSlipNoBinding = productionSlipNoBinding;
    }

    public RichInputText getProductionSlipNoBinding() {
        return productionSlipNoBinding;
    }

    public void setInspectionStatusBinding(RichInputText inspectionStatusBinding) {
        this.inspectionStatusBinding = inspectionStatusBinding;
    }

    public RichInputText getInspectionStatusBinding() {
        return inspectionStatusBinding;
    }

    public void setBatchQtyBinding(RichInputText batchQtyBinding) {
        this.batchQtyBinding = batchQtyBinding;
    }

    public RichInputText getBatchQtyBinding() {
        return batchQtyBinding;
    }

    public void setAcceptQtyBinding(RichInputText acceptQtyBinding) {
        this.acceptQtyBinding = acceptQtyBinding;
    }

    public RichInputText getAcceptQtyBinding() {
        return acceptQtyBinding;
    }

    public void setRejectQtyBinding(RichInputText rejectQtyBinding) {
        this.rejectQtyBinding = rejectQtyBinding;
    }

    public RichInputText getRejectQtyBinding() {
        return rejectQtyBinding;
    }

    public void setNoOfPackingBinding(RichInputText noOfPackingBinding) {
        this.noOfPackingBinding = noOfPackingBinding;
    }

    public RichInputText getNoOfPackingBinding() {
        return noOfPackingBinding;
    }

    public void setBatchNoBinding(RichInputText batchNoBinding) {
        this.batchNoBinding = batchNoBinding;
    }

    public RichInputText getBatchNoBinding() {
        return batchNoBinding;
    }

    public void setAnalysedByBinding(RichInputText analysedByBinding) {
        this.analysedByBinding = analysedByBinding;
    }

    public RichInputText getAnalysedByBinding() {
        return analysedByBinding;
    }

    public void setEntryNoBinding(RichInputText entryNoBinding) {
        this.entryNoBinding = entryNoBinding;
    }

    public RichInputText getEntryNoBinding() {
        return entryNoBinding;
    }

    public void setCheckPointStatusBinding(RichSelectOneChoice checkPointStatusBinding) {
        this.checkPointStatusBinding = checkPointStatusBinding;
    }

    public RichSelectOneChoice getCheckPointStatusBinding() {
        return checkPointStatusBinding;
    }

    public void setRemarksBinding(RichInputText remarksBinding) {
        this.remarksBinding = remarksBinding;
    }

    public RichInputText getRemarksBinding() {
        return remarksBinding;
    }

    public void setApprovedByBinding(RichInputComboboxListOfValues approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputComboboxListOfValues getApprovedByBinding() {
        return approvedByBinding;
    }

    public void setConclusionBinding(RichSelectOneChoice conclusionBinding) {
        this.conclusionBinding = conclusionBinding;
    }

    public RichSelectOneChoice getConclusionBinding() {
        return conclusionBinding;
    }

    public void setAcceptQtyDtlBinding(RichInputText acceptQtyDtlBinding) {
        this.acceptQtyDtlBinding = acceptQtyDtlBinding;
    }

    public RichInputText getAcceptQtyDtlBinding() {
        return acceptQtyDtlBinding;
    }

    public void setRejectQtyDtlBinding(RichInputText rejectQtyDtlBinding) {
        this.rejectQtyDtlBinding = rejectQtyDtlBinding;
    }

    public RichInputText getRejectQtyDtlBinding() {
        return rejectQtyDtlBinding;
    }

    public void setCalculativeFlagBinding(RichInputText calculativeFlagBinding) {
        this.calculativeFlagBinding = calculativeFlagBinding;
    }

    public RichInputText getCalculativeFlagBinding() {
        return calculativeFlagBinding;
    }

    public void setUpperLimitBinding(RichInputText upperLimitBinding) {
        this.upperLimitBinding = upperLimitBinding;
    }

    public RichInputText getUpperLimitBinding() {
        return upperLimitBinding;
    }

    public void setLowerLimitBinding(RichInputText lowerLimitBinding) {
        this.lowerLimitBinding = lowerLimitBinding;
    }

    public RichInputText getLowerLimitBinding() {
        return lowerLimitBinding;
    }

    public void setValueBinding(RichInputText valueBinding) {
        this.valueBinding = valueBinding;
    }

    public RichInputText getValueBinding() {
        return valueBinding;
    }

    public void setOutputTextBinding(RichOutputText outputTextBinding) {
        this.outputTextBinding = outputTextBinding;
    }

    public RichOutputText getOutputTextBinding() {
        cevmodecheck();
        return outputTextBinding;
    }
    
    private void cevmodecheck(){
            if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
               
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
                cevModeDisableComponent("V");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
                
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("C");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
                
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("E");
            }
        }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                try {
                    Method method1 =
                        component.getClass().getMethod("setDisabled", boolean.class);
                    if (method1 != null) {
                        method1.invoke(component, valueComponent);
                    }
                } catch (NoSuchMethodException e) {
                    try {
                        Method method =
                            component.getClass().getMethod("setReadOnly", boolean.class);
                        if (method != null) {
                            method.invoke(component, valueComponent);
                        }
                    } catch (Exception e1) {
                        // e.printStackTrace();//silently eat this exception.
                    }


                } catch (Exception e) {
                    // e.printStackTrace();//silently eat this exception.
                }
                List<UIComponent> childComponents = component.getChildren();
                for (UIComponent comp : childComponents) {
                    makeComponentHierarchyReadOnly(comp, valueComponent);
                }
            }
    public void cevModeDisableComponent(String mode) {
        
        if(mode.equals("E")){
                getUnitCodeBinding().setDisabled(true);
                getFinalInspNoBinding().setDisabled(true);
                getFinalInspDateBinding().setDisabled(true);
                getCheckPointStatusBinding().setDisabled(true);
                getProductCodeBinding().setDisabled(true);
                getCustomerTypeBinding().setDisabled(true);
                getCustomerCodeBinding().setDisabled(true);
                getQcdNoBinding().setDisabled(true);
                getProductionSlipNoBinding().setDisabled(true);
                getInspectionStatusBinding().setDisabled(true);
                getBatchQtyBinding().setDisabled(true);
                getAcceptQtyBinding().setDisabled(true);
                getRejectQtyBinding().setDisabled(true);
                getNoOfPackingBinding().setDisabled(true);
                getBatchNoBinding().setDisabled(true);
                getAnalysedByHdBinding().setDisabled(true);
                getEntryNoBinding().setDisabled(true);
                getConclusionBinding().setDisabled(true);
                getSrNoDtlBinding().setDisabled(true);
                getInspectionMethodNoBinding().setDisabled(true);
                getBatchQtyDtlBinding().setDisabled(true);
                getAcceptQtyDtlBinding().setDisabled(true);
                getRejectQtyDtlBinding().setDisabled(true);
                getCalculativeFlagBinding().setDisabled(true);
                getUpperLimitBinding().setDisabled(true);
                getLowerLimitBinding().setDisabled(true);
                getValueBinding().setDisabled(true);
                getOkNotOkBinding().setDisabled(true);
                getHeaderEditBinding().setDisabled(true);
            }
        if(mode.equals("C")){
            getUnitCodeBinding().setDisabled(true);
            getFinalInspNoBinding().setDisabled(true);
            getFinalInspDateBinding().setDisabled(true);
            getProductCodeBinding().setDisabled(true);
            getProductionSlipNoBinding().setDisabled(true);
            getInspectionStatusBinding().setDisabled(true);
            getAcceptQtyBinding().setDisabled(true);
            getRejectQtyBinding().setDisabled(true);
            getBatchNoBinding().setDisabled(true);
            getAnalysedByHdBinding().setDisabled(true);
            getEntryNoBinding().setDisabled(true);
            getApprovedByBinding().setDisabled(true);
            getSrNoDtlBinding().setDisabled(true);
            getInspectionMethodNoBinding().setDisabled(true);
            getBatchQtyDtlBinding().setDisabled(true);
            getAcceptQtyDtlBinding().setDisabled(true);
            getRejectQtyDtlBinding().setDisabled(true);
            getCalculativeFlagBinding().setDisabled(true);
            getUpperLimitBinding().setDisabled(true);
            getLowerLimitBinding().setDisabled(true);
            getCheckPointStatusBinding().setDisabled(true);
            getOkNotOkBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
        }
        if (mode.equals("V")) {
    //            getRejectionDetailButtonBinding().setDisabled(false);
                            //getHeaderEditBinding().setDisabled(false);
        //                    getDetailcreateBinding().setDisabled(true);
        //                    getDetaildeleteBinding().setDisabled(true);
        }
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public String saveButtonAL(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedBy("ReInspectionFinishGoodsHeaderVO1Iterator");
        BigDecimal a = new BigDecimal(0);
        BigDecimal b = new BigDecimal(0);
        
        BigDecimal acceptQty = new BigDecimal(0);
        BigDecimal rejectQty = new BigDecimal(0);
        acceptQty = (BigDecimal)(acceptQtyBinding.getValue()!=null?acceptQtyBinding.getValue():new BigDecimal(0));
        rejectQty = (BigDecimal)(rejectQtyBinding.getValue()!=null?rejectQtyBinding.getValue():new BigDecimal(0));
        
        a=acceptQty.add(rejectQty);
        
        if(a.compareTo(new BigDecimal(0)) == 1) 
        {
            OperationBinding op = (OperationBinding)ADFUtils.findOperation("getFinalInspNoForReInspectionFG"); 
            Object rst = op.execute();
            
            if (rst != null && rst != "") 
            {
                if (op.getErrors().isEmpty()) {
                    ADFUtils.findOperation("Commit").execute();
                    ADFUtils.showMessage("Record Saved Successfully. " + "Final Inspection No. is:: " + rst, 2);
                    return "save and close";
                }
            }
            
            if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ") 
            {
                if (op.getErrors().isEmpty()) {
                    ADFUtils.findOperation("Commit").execute();
                    ADFUtils.showMessage(" Record Updated Successfully.", 2); 
                    return "save and close";
              }
            }
        }
        else {
            ADFUtils.showMessage("Please Enter the Result.", 0);
        }
        return null;
//        ADFUtils.findOperation("getFinalInspNoForReInspectionFG").execute();
    }

    public void setPendingInspectionButtonBinding(RichButton pendingInspectionButtonBinding) {
        this.pendingInspectionButtonBinding = pendingInspectionButtonBinding;
    }

    public RichButton getPendingInspectionButtonBinding() {
        return pendingInspectionButtonBinding;
    }

    public void valueVCL(ValueChangeEvent vce) {
        System.out.println("INSIDE vce #### ");
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        
        if(vce != null) {
            ADFUtils.findOperation("rejectionValuesForReInspectionFG").execute();
        }
    }

    public void setSrNoDtlBinding(RichInputText srNoDtlBinding) {
        this.srNoDtlBinding = srNoDtlBinding;
    }

    public RichInputText getSrNoDtlBinding() {
        return srNoDtlBinding;
    }

    public void setInspectionMethodNoBinding(RichInputText inspectionMethodNoBinding) {
        this.inspectionMethodNoBinding = inspectionMethodNoBinding;
    }

    public RichInputText getInspectionMethodNoBinding() {
        return inspectionMethodNoBinding;
    }

    public void setBatchQtyDtlBinding(RichInputText batchQtyDtlBinding) {
        this.batchQtyDtlBinding = batchQtyDtlBinding;
    }

    public RichInputText getBatchQtyDtlBinding() {
        return batchQtyDtlBinding;
    }

    public void setAnalysedByHdBinding(RichInputComboboxListOfValues analysedByHdBinding) {
        this.analysedByHdBinding = analysedByHdBinding;
    }

    public RichInputComboboxListOfValues getAnalysedByHdBinding() {
        return analysedByHdBinding;
    }

    public void customerTypeVCL(ValueChangeEvent vce) {
        System.out.println("INSIDE customerTypeVCL ##### ");
        
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        
        String custType = "General";
        if(vce != null) {
            System.out.println("INSIDE VCE ##### " + customerTypeBinding.getValue());
            
            if(customerTypeBinding.getValue().equals("G")) {
                System.out.println("INSIDE IF BLOCK ##### ");
                customerCodeBinding.setValue(custType);
                customerCodeBinding.setDisabled(true);
            }
            
            else {
                System.out.println("INSIDE ELSE BLOCK ##### ");
                customerCodeBinding.setDisabled(false);
            }
        } 
    }

    public String saveAndCloseReInspectionFGAL() {
        ADFUtils.setLastUpdatedBy("ReInspectionFinishGoodsHeaderVO1Iterator");
        BigDecimal a = new BigDecimal(0);
        BigDecimal b = new BigDecimal(0);
        
        BigDecimal acceptQty = new BigDecimal(0);
        BigDecimal rejectQty = new BigDecimal(0);
        acceptQty = (BigDecimal)(acceptQtyBinding.getValue()!=null?acceptQtyBinding.getValue():new BigDecimal(0));
        rejectQty = (BigDecimal)(rejectQtyBinding.getValue()!=null?rejectQtyBinding.getValue():new BigDecimal(0));
        
        a=acceptQty.add(rejectQty);
        
        if(a.compareTo(new BigDecimal(0)) == 1) 
        {
            OperationBinding op = (OperationBinding)ADFUtils.findOperation("getFinalInspNoForReInspectionFG"); 
            Object rst = op.execute();
            
            if (rst != null && rst != "") 
            {
                if (op.getErrors().isEmpty()) {
                    ADFUtils.findOperation("Commit").execute();
                    ADFUtils.showMessage("Record Saved Successfully " + "Final Inspection No. is:: " + rst, 2);
                    return "save and close";
                }
            }
            
            if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ") 
            {
                if (op.getErrors().isEmpty()) {
                    ADFUtils.findOperation("Commit").execute();
                    ADFUtils.showMessage(" Record Updated Successfully.", 2); 
                    return "save and close";
              }
            }
        }
        else {
            ADFUtils.showMessage("Please Enter the Result.", 0);
        }
        return null;
    }

    public void setChkStatBinding(RichColumn chkStatBinding) {
        this.chkStatBinding = chkStatBinding;
    }

    public RichColumn getChkStatBinding() {
        return chkStatBinding;
    }

    public void setOkNotOkBinding(RichInputText okNotOkBinding) {
        this.okNotOkBinding = okNotOkBinding;
    }

    public RichInputText getOkNotOkBinding() {
        return okNotOkBinding;
    }
}
