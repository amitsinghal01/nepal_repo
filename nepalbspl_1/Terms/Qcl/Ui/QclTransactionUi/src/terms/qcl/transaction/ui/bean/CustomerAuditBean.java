package terms.qcl.transaction.ui.bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichPanelLabelAndMessage;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.nav.RichCommandLink;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.adf.view.rich.util.ResetUtils;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

import org.apache.myfaces.trinidad.model.UploadedFile;

public class CustomerAuditBean {
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText entryNumberBinding;
    private RichInputText objectiveBinding;
    private RichInputText auditTeamDetailBinding;
    private RichInputDate entryDateBinding;
    private RichInputText auditReportNumber;
    private RichInputDate actualCompletionDateBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichInputComboboxListOfValues customerCodeBinding;
    private RichInputComboboxListOfValues auditPeriodBinding;
    private RichTable customerAuditDetailTableBinding;
    private RichInputDate targetDateDetailBinding;
    private RichSelectOneChoice statusDetailBinding;
    private RichInputText auditFindingsDetailBinding;
    private RichInputText auditRecommendationsDetailBinding;
    private RichInputText actionOwnerDetailBinding;
    private RichInputText managementResponseDetailBinding;
    private RichButton detailCreateBinding;
    private RichInputText siNoBinding;
    private RichOutputText outputTextBinding;
    private RichInputText unitCdRefDocBinding;
    private RichInputText pathBinding;
    private RichTable referenceDocumentTableBinding;
    private RichShowDetailItem refDocTabBinding;
    private RichShowDetailItem customerAuditDetailsTabBinding;
    private RichInputText docSlNoBinding;
    private RichInputText unitCodeReferenceDocumentBinding;
    private RichInputText refDocNoBinding;
    private RichInputText refDocTypeBinding;
    private RichInputText docFileNameBinding;
    private RichInputDate refDocDateBinding;
    private RichInputText remarksBinding;
    private RichCommandLink downloadLinkBinding;

    public CustomerAuditBean() {
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setEntryNumberBinding(RichInputText entryNumberBinding) {
        this.entryNumberBinding = entryNumberBinding;
    }

    public RichInputText getEntryNumberBinding() {
        return entryNumberBinding;
    }
    public void setObjectiveBinding(RichInputText objectiveBinding) {
        this.objectiveBinding = objectiveBinding;
    }

    public RichInputText getObjectiveBinding() {
        return objectiveBinding;
    }

    public void setAuditTeamDetailBinding(RichInputText auditTeamDetailBinding) {
        this.auditTeamDetailBinding = auditTeamDetailBinding;
    }

    public RichInputText getAuditTeamDetailBinding() {
        return auditTeamDetailBinding;
    }

    public void setEntryDateBinding(RichInputDate entryDateBinding) {
        this.entryDateBinding = entryDateBinding;
    }

    public RichInputDate getEntryDateBinding() {
        return entryDateBinding;
    }

    public void setAuditReportNumber(RichInputText auditReportNumber) {
        this.auditReportNumber = auditReportNumber;
    }

    public RichInputText getAuditReportNumber() {
        return auditReportNumber;
    }

    public void setActualCompletionDateBinding(RichInputDate actualCompletionDateBinding) {
        this.actualCompletionDateBinding = actualCompletionDateBinding;
    }

    public RichInputDate getActualCompletionDateBinding() {
        return actualCompletionDateBinding;
    }
    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }
    
    private void cevmodecheck(){
            if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
                cevModeDisableComponent("V");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("C");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("E");
            }
        }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                try {
                    Method method1 =
                        component.getClass().getMethod("setDisabled", boolean.class);
                    if (method1 != null) {
                        method1.invoke(component, valueComponent);
                    }
                } catch (NoSuchMethodException e) {
                    try {
                        Method method =
                            component.getClass().getMethod("setReadOnly", boolean.class);
                        if (method != null) {
                            method.invoke(component, valueComponent);
                        }
                    } catch (Exception e1) {
                        // e.printStackTrace();//silently eat this exception.
                    }


                } catch (Exception e) {
                    // e.printStackTrace();//silently eat this exception.
                }
                List<UIComponent> childComponents = component.getChildren();
                for (UIComponent comp : childComponents) {
                    makeComponentHierarchyReadOnly(comp, valueComponent);
                }
            }
    
    //Set Fields and Button disable.
        public void cevModeDisableComponent(String mode) {
            if (mode.equals("E")) {
                getUnitCodeBinding().setDisabled(true);
                getEntryNumberBinding().setDisabled(true);
//                getCustomerCodeBinding().setDisabled(true);
                getEntryDateBinding().setDisabled(true);
//                getActionOwnerDetailBinding().setDisabled(true);
//                getManagementResponseDetailBinding().setDisabled(true);
                getTargetDateDetailBinding().setDisabled(true);
                getDocSlNoBinding().setDisabled(true);
                getUnitCodeReferenceDocumentBinding().setDisabled(true);
                getRefDocNoBinding().setDisabled(true);
                getRefDocTypeBinding().setDisabled(true);
                getDocFileNameBinding().setDisabled(true);
                getRefDocDateBinding().setDisabled(true);
                
//                getStatusDetailBinding().setDisabled(true);
                getSiNoBinding().setDisabled(true);
            } else if (mode.equals("C")) {
               getUnitCodeBinding().setDisabled(true);
               getEntryNumberBinding().setDisabled(true);
               
//                getStatusDetailBinding().setDisabled(true);
                getSiNoBinding().setDisabled(true);
                getDocSlNoBinding().setDisabled(true);
                getUnitCodeReferenceDocumentBinding().setDisabled(true);
                getRefDocNoBinding().setDisabled(true);
                getRefDocTypeBinding().setDisabled(true);
                getDocFileNameBinding().setDisabled(true);
                getRefDocDateBinding().setDisabled(true);
            } else if (mode.equals("V")) {
                getCustomerAuditDetailsTabBinding().setDisabled(false);
                getRefDocTabBinding().setDisabled(false);
                getDownloadLinkBinding().setDisabled(false);
//               getDetailCreateBinding().setDisabled(true);
            }
        }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void saveAL(ActionEvent actionEvent) 
    {
        ADFUtils.setLastUpdatedBy("CustomerAuditVO1Iterator");
        if(unitCodeBinding.getValue()!=null)
        {
            
        if ((Long) ADFUtils.evaluateEL("#{bindings.CustomerAuditDetailVO1Iterator.estimatedRowCount}")>0) 
        {
                oracle.binding.OperationBinding op = ADFUtils.findOperation("generateEntryNumberForCustomerAudit");
                Object rst = op.execute();
                
                System.out.println("Value aftr getting result--->?" + rst);
                
                if (rst.toString() != null && rst.toString() != "") {
                    if (op.getErrors().isEmpty()) {
                        ADFUtils.findOperation("Commit").execute();
                        FacesMessage Message = new FacesMessage("Record Saved Successfully. New Entry Number is " + rst + ".");
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);
                        FacesContext fc = FacesContext.getCurrentInstance();
                        fc.addMessage(null, Message);
                    }
                }
                if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ")
                {
                    if (op.getErrors().isEmpty()) {
                        ADFUtils.findOperation("Commit").execute();
                        FacesMessage Message = new FacesMessage("Record Updated Successfully.");
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);
                        FacesContext fc = FacesContext.getCurrentInstance();
                        fc.addMessage(null, Message);
                    }
                }
            }
            else{
                ADFUtils.showMessage("Enter the data in detail table", 0);
            }
        }
        else{
            ADFUtils.showMessage("Unit Code is required.", 0);
        }
    }

    public void setCustomerCodeBinding(RichInputComboboxListOfValues customerCodeBinding) {
        this.customerCodeBinding = customerCodeBinding;
    }

    public RichInputComboboxListOfValues getCustomerCodeBinding() {
        return customerCodeBinding;
    }

    public void setAuditPeriodBinding(RichInputComboboxListOfValues auditPeriodBinding) {
        this.auditPeriodBinding = auditPeriodBinding;
    }

    public RichInputComboboxListOfValues getAuditPeriodBinding() {
        return auditPeriodBinding;
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
            {
            
            ADFUtils.findOperation("Delete").execute();
            System.out.println("Record Delete Successfully");
            
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");   
                Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                FacesContext fc = FacesContext.getCurrentInstance();   
                fc.addMessage(null, Message);   
            
            }

        AdfFacesContext.getCurrentInstance().addPartialTarget(customerAuditDetailTableBinding);
    }

    public void setCustomerAuditDetailTableBinding(RichTable customerAuditDetailTableBinding) {
        this.customerAuditDetailTableBinding = customerAuditDetailTableBinding;
    }

    public RichTable getCustomerAuditDetailTableBinding() {
        return customerAuditDetailTableBinding;
    }

    public void setTargetDateDetailBinding(RichInputDate targetDateDetailBinding) {
        this.targetDateDetailBinding = targetDateDetailBinding;
    }

    public RichInputDate getTargetDateDetailBinding() {
        return targetDateDetailBinding;
    }

    public void setStatusDetailBinding(RichSelectOneChoice statusDetailBinding) {
        this.statusDetailBinding = statusDetailBinding;
    }

    public RichSelectOneChoice getStatusDetailBinding() {
        return statusDetailBinding;
    }

    public void setAuditFindingsDetailBinding(RichInputText auditFindingsDetailBinding) {
        this.auditFindingsDetailBinding = auditFindingsDetailBinding;
    }

    public RichInputText getAuditFindingsDetailBinding() {
        return auditFindingsDetailBinding;
    }

    public void setAuditRecommendationsDetailBinding(RichInputText auditRecommendationsDetailBinding) {
        this.auditRecommendationsDetailBinding = auditRecommendationsDetailBinding;
    }

    public RichInputText getAuditRecommendationsDetailBinding() {
        return auditRecommendationsDetailBinding;
    }

    public void setActionOwnerDetailBinding(RichInputText actionOwnerDetailBinding) {
        this.actionOwnerDetailBinding = actionOwnerDetailBinding;
    }

    public RichInputText getActionOwnerDetailBinding() {
        return actionOwnerDetailBinding;
    }

    public void setManagementResponseDetailBinding(RichInputText managementResponseDetailBinding) {
        this.managementResponseDetailBinding = managementResponseDetailBinding;
    }

    public RichInputText getManagementResponseDetailBinding() {
        return managementResponseDetailBinding;
    }

    public void setDetailCreateBinding(RichButton detailCreateBinding) {
        this.detailCreateBinding = detailCreateBinding;
    }

    public RichButton getDetailCreateBinding() {
        return detailCreateBinding;
    }

    public void setSiNoBinding(RichInputText siNoBinding) {
        this.siNoBinding = siNoBinding;
    }

    public RichInputText getSiNoBinding() {
        return siNoBinding;
    }

    public void createDetailAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert").execute();
        siNoBinding.setValue(ADFUtils.evaluateEL("#{bindings.CustomerAuditDetailVO1Iterator.estimatedRowCount}"));
        
        AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent); 
        
    }

    public void setOutputTextBinding(RichOutputText outputTextBinding) {
        this.outputTextBinding = outputTextBinding;
    }

    public RichOutputText getOutputTextBinding() {
        cevmodecheck();
        return outputTextBinding;
    }

    public String saveandCloseAL() {
        ADFUtils.setLastUpdatedBy("CustomerAuditVO1Iterator");
        if(unitCodeBinding.getValue()!=null)
        {
            
        if ((Long) ADFUtils.evaluateEL("#{bindings.CustomerAuditDetailVO1Iterator.estimatedRowCount}")>0) 
        {
                oracle.binding.OperationBinding op = ADFUtils.findOperation("generateEntryNumberForCustomerAudit");
                Object rst = op.execute();
                
                System.out.println("Value aftr getting result--->?" + rst);
                
                if (rst.toString() != null && rst.toString() != "") {
                    if (op.getErrors().isEmpty()) {
                        ADFUtils.findOperation("Commit").execute();
                        FacesMessage Message = new FacesMessage("Record Saved Successfully. New Entry Number is " + rst + ".");
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);
                        FacesContext fc = FacesContext.getCurrentInstance();
                        fc.addMessage(null, Message);
                        return "SaveAndClose";
                    }
                }
                if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ")
                {
                    if (op.getErrors().isEmpty()) {
                        ADFUtils.findOperation("Commit").execute();
                        FacesMessage Message = new FacesMessage("Record Updated Successfully.");
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);
                        FacesContext fc = FacesContext.getCurrentInstance();
                        fc.addMessage(null, Message);
                        return "SaveAndClose";
                    }
                }
            }
            else{
                ADFUtils.showMessage("Enter the data in detail table", 0);
                return null;
            }
        }
        else{
            ADFUtils.showMessage("Unit Code is required.", 0);
            return null;
        }
        return null;
    }

    public void statusVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce!=null)
        {
            Row row=(Row)ADFUtils.evaluateEL("#{bindings.CustomerAuditDetailVO1Iterator.currentRow}");
            if(row.getAttribute("CompletionDate")==null && row.getAttribute("Status").equals("D"))
            {
                FacesMessage message = new FacesMessage("Status can not be Done without Completion Date.");
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(statusDetailBinding.getClientId(), message);
            }
            if(row.getAttribute("CompletionDate")!=null && !row.getAttribute("Status").equals("D"))
            {
                FacesMessage message = new FacesMessage("Status must be Done with Completion Date.");
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(statusDetailBinding.getClientId(), message);
            }
        }
    }

    public void uploadAction(ValueChangeEvent vce) {
        System.out.println("inside uploadAction #### ");
                    //Get File Object from VC Event
                    UploadedFile fileVal = (UploadedFile) vce.getNewValue();
                    //Upload File to path- Return actual server path
                    String path = ADFUtils.uploadFile(fileVal);
                      System.out.println(fileVal.getContentType());
        //                      System.out.println("Content Type==>>"+fileVal.getContentType()+"Path==>>"+path+"File name==>>"+fileVal.getFilename());
                    //Method to insert data in table to keep track of uploaded files
                    OperationBinding ob = ADFUtils.findOperation("setFileDataForCustomerAudit");
                    ob.getParamsMap().put("name", fileVal.getFilename());
                    ob.getParamsMap().put("path", path);
                    ob.getParamsMap().put("contTyp", fileVal.getContentType());
                    ob.getParamsMap().put("SrNo", ADFUtils.evaluateEL("#{bindings.DocAttachRefDtlVO3Iterator.estimatedRowCount+1}"));
                    ob.execute();
                    // Reset inputFile component after upload
                    ResetUtils.reset(vce.getComponent());
    }

    public void setUnitCdRefDocBinding(RichInputText unitCdRefDocBinding) {
        this.unitCdRefDocBinding = unitCdRefDocBinding;
    }

    public RichInputText getUnitCdRefDocBinding() {
        return unitCdRefDocBinding;
    }

    public void downloadAction(FacesContext facesContext, OutputStream outputStream) throws IOException {
        File filed = new File(pathBinding.getValue().toString());
                               FileInputStream fis;
                               byte[] b;
                               try {
                                   fis = new FileInputStream(filed);

                                   int n;
                                   while ((n = fis.available()) > 0) {
                                       b = new byte[n];
                                       int result = fis.read(b);
                                       outputStream.write(b, 0, b.length);
                                       if (result == -1)
                                           break;
                                   }
                               } catch (Exception e) {
                                   e.printStackTrace();
                               }
                               outputStream.flush();

    }

    public void setPathBinding(RichInputText pathBinding) {
        this.pathBinding = pathBinding;
    }

    public RichInputText getPathBinding() {
        return pathBinding;
    }

    public void setReferenceDocumentTableBinding(RichTable referenceDocumentTableBinding) {
        this.referenceDocumentTableBinding = referenceDocumentTableBinding;
    }

    public RichTable getReferenceDocumentTableBinding() {
        return referenceDocumentTableBinding;
    }

    public void setRefDocTabBinding(RichShowDetailItem refDocTabBinding) {
        this.refDocTabBinding = refDocTabBinding;
    }

    public RichShowDetailItem getRefDocTabBinding() {
        return refDocTabBinding;
    }

    public void setCustomerAuditDetailsTabBinding(RichShowDetailItem customerAuditDetailsTabBinding) {
        this.customerAuditDetailsTabBinding = customerAuditDetailsTabBinding;
    }

    public RichShowDetailItem getCustomerAuditDetailsTabBinding() {
        return customerAuditDetailsTabBinding;
    }

    public void setDocSlNoBinding(RichInputText docSlNoBinding) {
        this.docSlNoBinding = docSlNoBinding;
    }

    public RichInputText getDocSlNoBinding() {
        return docSlNoBinding;
    }

    public void setUnitCodeReferenceDocumentBinding(RichInputText unitCodeReferenceDocumentBinding) {
        this.unitCodeReferenceDocumentBinding = unitCodeReferenceDocumentBinding;
    }

    public RichInputText getUnitCodeReferenceDocumentBinding() {
        return unitCodeReferenceDocumentBinding;
    }

    public void setRefDocNoBinding(RichInputText refDocNoBinding) {
        this.refDocNoBinding = refDocNoBinding;
    }

    public RichInputText getRefDocNoBinding() {
        return refDocNoBinding;
    }

    public void setRefDocTypeBinding(RichInputText refDocTypeBinding) {
        this.refDocTypeBinding = refDocTypeBinding;
    }

    public RichInputText getRefDocTypeBinding() {
        return refDocTypeBinding;
    }

    public void setDocFileNameBinding(RichInputText docFileNameBinding) {
        this.docFileNameBinding = docFileNameBinding;
    }

    public RichInputText getDocFileNameBinding() {
        return docFileNameBinding;
    }

    public void setRefDocDateBinding(RichInputDate refDocDateBinding) {
        this.refDocDateBinding = refDocDateBinding;
    }

    public RichInputDate getRefDocDateBinding() {
        return refDocDateBinding;
    }

    public void setRemarksBinding(RichInputText remarksBinding) {
        this.remarksBinding = remarksBinding;
    }

    public RichInputText getRemarksBinding() {
        return remarksBinding;
    }

    public void deletePopupDLReferenceDocument(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                    {
                    ADFUtils.findOperation("Delete1").execute();
                    //ADFUtils.findOperation("Commit").execute();    //Unable on Search page.
                    System.out.println("Record Delete Successfully");
                   
                        FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                        FacesContext fc = FacesContext.getCurrentInstance();  
                        fc.addMessage(null, Message);  
                   
                    }

              AdfFacesContext.getCurrentInstance().addPartialTarget(referenceDocumentTableBinding);
    }

    public void setDownloadLinkBinding(RichCommandLink downloadLinkBinding) {
        this.downloadLinkBinding = downloadLinkBinding;
    }

    public RichCommandLink getDownloadLinkBinding() {
        return downloadLinkBinding;
    }
}
