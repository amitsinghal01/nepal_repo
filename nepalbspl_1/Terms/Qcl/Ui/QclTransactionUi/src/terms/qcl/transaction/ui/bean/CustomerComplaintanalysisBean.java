package terms.qcl.transaction.ui.bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichPanelLabelAndMessage;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.nav.RichCommandLink;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.adf.view.rich.util.ResetUtils;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

import org.apache.myfaces.trinidad.model.UploadedFile;

public class CustomerComplaintanalysisBean {
    private RichOutputText bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichTable correctiveTableBinding;
    private RichTable analysisTableBinding;
    private RichInputText tasNoBinding;
    private RichInputDate pcaDtBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputDate tasDateBinding;
    private Integer code=0;
    private RichInputText prodRevNoBinding;
    private RichButton detailCreateBinding;
    private RichButton detailDeleteBinding;
    private RichButton detailCreate1Binding;
    private RichButton detailDelete1Binding;
    private RichInputComboboxListOfValues preparedByBinding;
    private RichInputDate closeDateBinding;
    private RichSelectOneChoice statusBinding;
    private RichInputDate actualComDateBinding;
    private RichInputText pathBinding;
    private RichInputText documentSequenceNoBinding;
    private RichInputText unitCdReferenceDocumentBinding;
    private RichInputText referenceDocumentNoBinding;
    private RichInputText referenceDocumentTypeBinding;
    private RichInputText documentFileNameBinding;
    private RichInputDate referenceDocumentDateBinding;
    private RichInputText remarksBinding;
    private RichShowDetailItem referenceDocumentTabBinding;
    private RichCommandLink downloadLinkBinding;
    private RichTable referenceDocumentTableBinding;

    public CustomerComplaintanalysisBean() {
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete1").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(correctiveTableBinding);
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }


    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }


    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }


    //Set Fields and Button disable.
    public void cevModeDisableComponent(String mode) {

        if (mode.equals("E")) {
            System.out.println("Edit Mode");
            getUnitCodeBinding().setDisabled(true);
            getTasNoBinding().setDisabled(true);
//            getTasDateBinding().setDisabled(true);
            getPcaDtBinding().setDisabled(true);
            getProdRevNoBinding().setDisabled(true);
            getPreparedByBinding().setDisabled(true);
            getDocumentSequenceNoBinding().setDisabled(true);
            getUnitCdReferenceDocumentBinding().setDisabled(true);
            getReferenceDocumentNoBinding().setDisabled(true);
            getReferenceDocumentTypeBinding().setDisabled(true);
            getDocumentFileNameBinding().setDisabled(true);
            getReferenceDocumentDateBinding().setDisabled(true);
//            getDetailCreate1Binding().setDisabled(true);
//            getDetailDelete1Binding().setDisabled(true);
//            getDetailCreateBinding().setDisabled(true);
//            getDetailDeleteBinding().setDisabled(true);


        } else if (mode.equals("C")) {
            System.out.println("Create Mode");
            getTasNoBinding().setDisabled(true);
//            getTasDateBinding().setDisabled(true);
            getPcaDtBinding().setDisabled(true);
            getProdRevNoBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
            getPreparedByBinding().setDisabled(true);
            getDocumentSequenceNoBinding().setDisabled(true);
            getUnitCdReferenceDocumentBinding().setDisabled(true);
            getReferenceDocumentNoBinding().setDisabled(true);
            getReferenceDocumentTypeBinding().setDisabled(true);
            getDocumentFileNameBinding().setDisabled(true);
            getReferenceDocumentDateBinding().setDisabled(true);

        } else if (mode.equals("V")) {
            System.out.println("View Mode");
            getReferenceDocumentTabBinding().setDisabled(false);
            getDownloadLinkBinding().setDisabled(false);
//            getDetailCreate1Binding().setDisabled(true);
//            getDetailDelete1Binding().setDisabled(true);
//            getDetailCreateBinding().setDisabled(true);
//            getDetailDeleteBinding().setDisabled(true);


        }

    }


    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void deleteDialogDL1(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(analysisTableBinding);
    }

    public void setCorrectiveTableBinding(RichTable correctiveTableBinding) {
        this.correctiveTableBinding = correctiveTableBinding;
    }

    public RichTable getCorrectiveTableBinding() {
        return correctiveTableBinding;
    }

    public void setAnalysisTableBinding(RichTable analysisTableBinding) {
        this.analysisTableBinding = analysisTableBinding;
    }

    public RichTable getAnalysisTableBinding() {
        return analysisTableBinding;
    }

    public void saveButtonAL(ActionEvent actionEvent) 
    {
        ADFUtils.setLastUpdatedBy("CustComplaintAnalysisHeaderVO1Iterator");
        if ((Long) ADFUtils.evaluateEL("#{bindings.CustComplaintAnalysisDetailVO1Iterator.estimatedRowCount}")>0) 
        {
            System.out.println("Inside##saveButtonAL ");
            ADFUtils.findOperation("getPcaNumberCustCompAnalysis").execute();
            //        OperationBinding op = ADFUtils.findOperation("getPcaNumberCustCompAnalysis");
            //        Object rst = op.execute();
            
            
            System.out.println("--------Commit-------");
            //        System.out.println("value aftr getting result--->?" + rst);
            
            Integer TransValue = (Integer)bindingOutputText.getValue();
            System.out.println("TRANS VALUE"+TransValue);
            if(TransValue == 0){
                ADFUtils.findOperation("Commit").execute();
                FacesMessage Message = new FacesMessage("Record Saved Successfully. New Complaint Number is "+tasNoBinding.getValue()+".");   
                Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                FacesContext fc = FacesContext.getCurrentInstance();   
                fc.addMessage(null, Message);  
            }
            else{
                ADFUtils.findOperation("Commit").execute();
                FacesMessage Message = new FacesMessage("Record Update Successfully.");   
                Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                FacesContext fc = FacesContext.getCurrentInstance();   
                fc.addMessage(null, Message);   
            }
        }else{
            ADFUtils.showMessage("Enter the data in Corrective Action detail table", 0);
        }
    }

    public void setTasNoBinding(RichInputText tasNoBinding) {
        this.tasNoBinding = tasNoBinding;
    }

    public RichInputText getTasNoBinding() {
        return tasNoBinding;
    }

    public void setPcaDtBinding(RichInputDate pcaDtBinding) {
        this.pcaDtBinding = pcaDtBinding;
    }

    public RichInputDate getPcaDtBinding() {
        return pcaDtBinding;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setTasDateBinding(RichInputDate tasDateBinding) {
        this.tasDateBinding = tasDateBinding;
    }

    public RichInputDate getTasDateBinding() {
        return tasDateBinding;
    }

    public void pcaNoVCE(ValueChangeEvent valueChangeEvent) {
        System.out.println("inside bean of ");
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        System.out.println("inside entry");
        OperationBinding op2 = ADFUtils.findOperation("PcaNoValidation");
        Object rst2 = op2.execute();
        if (op2.getResult().toString().equalsIgnoreCase("Y")) {

            FacesMessage Message =new FacesMessage("Trouble analysis sheet has already been generated for this complaint");
            Message.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
        }
        System.out.println("inside exit");
    }

    public void createsrNoAL(ActionEvent actionEvent) {
        
        ADFUtils.findOperation("CreateInsert1").execute();
        {
            if(code==0)
            {
                OperationBinding opr= ADFUtils.findOperation("getSerialNoCustomeComplaint");
                            Integer sr_no=(Integer)opr.execute();
                            System.out.println("*************sr_no"+sr_no);
                            code=sr_no;
                         }           
                            else
                            {
                                code=code+1;
                                Row rr=(Row) ADFUtils.evaluateEL("#{bindings.CustComplaintAnalysisDetailVO1Iterator.currentRow}");
                                rr.setAttribute("SerialNo",code);
                            }

                    }
 
            
    }


    public void setProdRevNoBinding(RichInputText prodRevNoBinding) {
        this.prodRevNoBinding = prodRevNoBinding;
    }

    public RichInputText getProdRevNoBinding() {
        return prodRevNoBinding;
    }

    public void setDetailCreateBinding(RichButton detailCreateBinding) {
        this.detailCreateBinding = detailCreateBinding;
    }

    public RichButton getDetailCreateBinding() {
        return detailCreateBinding;
    }

    public void setDetailDeleteBinding(RichButton detailDeleteBinding) {
        this.detailDeleteBinding = detailDeleteBinding;
    }

    public RichButton getDetailDeleteBinding() {
        return detailDeleteBinding;
    }

    public void setDetailCreate1Binding(RichButton detailCreate1Binding) {
        this.detailCreate1Binding = detailCreate1Binding;
    }

    public RichButton getDetailCreate1Binding() {
        return detailCreate1Binding;
    }

    public void setDetailDelete1Binding(RichButton detailDelete1Binding) {
        this.detailDelete1Binding = detailDelete1Binding;
    }

    public RichButton getDetailDelete1Binding() {
        return detailDelete1Binding;
    }

    public void setPreparedByBinding(RichInputComboboxListOfValues preparedByBinding) {
        this.preparedByBinding = preparedByBinding;
    }

    public RichInputComboboxListOfValues getPreparedByBinding() {
        return preparedByBinding;
    }

    public void getActCompDateVCL(ValueChangeEvent valueChangeEvent) {
        ADFUtils.findOperation("getCustCompActCompDateSet").execute();
        AdfFacesContext.getCurrentInstance().addPartialTarget(closeDateBinding);
        
    }

    public void setCloseDateBinding(RichInputDate closeDateBinding) {
        this.closeDateBinding = closeDateBinding;
    }

    public RichInputDate getCloseDateBinding() {
        return closeDateBinding;
    }

    public String saveAndCloseAL() {
        ADFUtils.setLastUpdatedBy("CustComplaintAnalysisHeaderVO1Iterator");
        if ((Long) ADFUtils.evaluateEL("#{bindings.CustComplaintAnalysisDetailVO1Iterator.estimatedRowCount}")>0) 
        {
            System.out.println("Inside##saveButtonAL ");
            ADFUtils.findOperation("getPcaNumberCustCompAnalysis").execute();

            Integer TransValue = (Integer)bindingOutputText.getValue();
            System.out.println("TRANS VALUE"+TransValue);
            if(TransValue == 0){
                ADFUtils.findOperation("Commit").execute();
                FacesMessage Message = new FacesMessage("Record Saved Successfully. New Complaint Number is "+tasNoBinding.getValue()+".");   
                Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                FacesContext fc = FacesContext.getCurrentInstance();   
                fc.addMessage(null, Message);  
                return "Save And Close";
            }
            else{
                ADFUtils.findOperation("Commit").execute();
                FacesMessage Message = new FacesMessage("Record Update Successfully.");   
                Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                FacesContext fc = FacesContext.getCurrentInstance();   
                fc.addMessage(null, Message);
                return "Save And Close";
            }
        }else
        {
            ADFUtils.showMessage("Enter the data in Corrective Action detail table", 0);
            return null;
        }
       // return null;
    }

    public void setStatusBinding(RichSelectOneChoice statusBinding) {
        this.statusBinding = statusBinding;
    }

    public RichSelectOneChoice getStatusBinding() {
        return statusBinding;
    }

    public void setActualComDateBinding(RichInputDate actualComDateBinding) {
        this.actualComDateBinding = actualComDateBinding;
    }

    public RichInputDate getActualComDateBinding() {
        return actualComDateBinding;
    }

    public void statusVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce!=null)
        {
            Row row=(Row)ADFUtils.evaluateEL("#{bindings.CustComplaintAnalysisDetailVO1Iterator.currentRow}");
            if(row.getAttribute("ActCompDate")==null && row.getAttribute("Status").equals("D"))
            {
                FacesMessage message = new FacesMessage("Status can not be Done without Actual Completion Date.");
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(statusBinding.getClientId(), message);
            }
            if(row.getAttribute("ActCompDate")!=null && !row.getAttribute("Status").equals("D"))
            {
                FacesMessage message = new FacesMessage("Status must be Done with Actual Completion Date.");
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(statusBinding.getClientId(), message);
            }
        }
    }

    public void downloadAction(FacesContext facesContext, OutputStream outputStream) throws IOException {
        File filed = new File(pathBinding.getValue().toString());
                               FileInputStream fis;
                               byte[] b;
                               try {
                                   fis = new FileInputStream(filed);

                                   int n;
                                   while ((n = fis.available()) > 0) {
                                       b = new byte[n];
                                       int result = fis.read(b);
                                       outputStream.write(b, 0, b.length);
                                       if (result == -1)
                                           break;
                                   }
                               } catch (Exception e) {
                                   e.printStackTrace();
                               }
                               outputStream.flush();

    }

    public void uploadAction(ValueChangeEvent vce) {
        System.out.println("inside uploadAction #### ");
                    //Get File Object from VC Event
                    UploadedFile fileVal = (UploadedFile) vce.getNewValue();
                    //Upload File to path- Return actual server path
                    String path = ADFUtils.uploadFile(fileVal);
                      System.out.println(fileVal.getContentType());
        //                      System.out.println("Content Type==>>"+fileVal.getContentType()+"Path==>>"+path+"File name==>>"+fileVal.getFilename());
                    //Method to insert data in table to keep track of uploaded files
                    OperationBinding ob = ADFUtils.findOperation("setFileDataForCustomerCompAnalysis");
                    ob.getParamsMap().put("name", fileVal.getFilename());
                    ob.getParamsMap().put("path", path);
                    ob.getParamsMap().put("contTyp", fileVal.getContentType());
                    ob.getParamsMap().put("SrNo", ADFUtils.evaluateEL("#{bindings.DocAttachRefDtlVO2Iterator.estimatedRowCount+1}"));
                    ob.execute();
                    // Reset inputFile component after upload
                    ResetUtils.reset(vce.getComponent());
    }

    public void setPathBinding(RichInputText pathBinding) {
        this.pathBinding = pathBinding;
    }

    public RichInputText getPathBinding() {
        return pathBinding;
    }

    public void setDocumentSequenceNoBinding(RichInputText documentSequenceNoBinding) {
        this.documentSequenceNoBinding = documentSequenceNoBinding;
    }

    public RichInputText getDocumentSequenceNoBinding() {
        return documentSequenceNoBinding;
    }

    public void setUnitCdReferenceDocumentBinding(RichInputText unitCdReferenceDocumentBinding) {
        this.unitCdReferenceDocumentBinding = unitCdReferenceDocumentBinding;
    }

    public RichInputText getUnitCdReferenceDocumentBinding() {
        return unitCdReferenceDocumentBinding;
    }

    public void setReferenceDocumentNoBinding(RichInputText referenceDocumentNoBinding) {
        this.referenceDocumentNoBinding = referenceDocumentNoBinding;
    }

    public RichInputText getReferenceDocumentNoBinding() {
        return referenceDocumentNoBinding;
    }

    public void setReferenceDocumentTypeBinding(RichInputText referenceDocumentTypeBinding) {
        this.referenceDocumentTypeBinding = referenceDocumentTypeBinding;
    }

    public RichInputText getReferenceDocumentTypeBinding() {
        return referenceDocumentTypeBinding;
    }

    public void setDocumentFileNameBinding(RichInputText documentFileNameBinding) {
        this.documentFileNameBinding = documentFileNameBinding;
    }

    public RichInputText getDocumentFileNameBinding() {
        return documentFileNameBinding;
    }

    public void setReferenceDocumentDateBinding(RichInputDate referenceDocumentDateBinding) {
        this.referenceDocumentDateBinding = referenceDocumentDateBinding;
    }

    public RichInputDate getReferenceDocumentDateBinding() {
        return referenceDocumentDateBinding;
    }

    public void setRemarksBinding(RichInputText remarksBinding) {
        this.remarksBinding = remarksBinding;
    }

    public RichInputText getRemarksBinding() {
        return remarksBinding;
    }

    public void setReferenceDocumentTabBinding(RichShowDetailItem referenceDocumentTabBinding) {
        this.referenceDocumentTabBinding = referenceDocumentTabBinding;
    }

    public RichShowDetailItem getReferenceDocumentTabBinding() {
        return referenceDocumentTabBinding;
    }

    public void setDownloadLinkBinding(RichCommandLink downloadLinkBinding) {
        this.downloadLinkBinding = downloadLinkBinding;
    }

    public RichCommandLink getDownloadLinkBinding() {
        return downloadLinkBinding;
    }

    public void deletePopupDLReferenceDocument(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                    {
                    ADFUtils.findOperation("Delete2").execute();
                    //ADFUtils.findOperation("Commit").execute();    //Unable on Search page.
                    System.out.println("Record Delete Successfully");
                   
                        FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                        FacesContext fc = FacesContext.getCurrentInstance();  
                        fc.addMessage(null, Message);  
                   
                    }

              AdfFacesContext.getCurrentInstance().addPartialTarget(referenceDocumentTableBinding);
    }

    public void setReferenceDocumentTableBinding(RichTable referenceDocumentTableBinding) {
        this.referenceDocumentTableBinding = referenceDocumentTableBinding;
    }

    public RichTable getReferenceDocumentTableBinding() {
        return referenceDocumentTableBinding;
    }
}
