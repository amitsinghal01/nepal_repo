package terms.qcl.transaction.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import java.util.Set;

import javax.faces.application.FacesMessage;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import javax.faces.validator.ValidatorException;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

import oracle.jbo.ValidationException;

import terms.qcl.transaction.model.view.InProcessInspectionDetailVORowImpl;


public class InProcessInspectionBean {
    private RichTable inProcessInspDetailTablebinding;
    private RichTable rejectionBreakupTableBinding;
    private RichInputComboboxListOfValues unitBinding;
    private RichInputText inspectionNoBinding;
    private RichOutputText bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichButton detaildeleteBinding;
    private RichPopup pendingInspcPopupBinding;
    private RichInputDate inspectionDateBinding;
    private RichInputText acceptQtyBinding;
    private RichInputText rejectQtyBinding;
    private RichSelectOneChoice inspStatusBinding;
    private RichInputText prodSlipNoBinding;
    private RichInputText itemCodeBinding;
    private RichInputText prodnQtyBinding;
    private RichInputDate prodnDateBinding;
    private RichInputText processCdBinding;
    private RichInputText jobCardBinding;
    private RichInputText sequenceNoBinding;
    private RichSelectOneChoice inspTypeBinding;
    private RichInputText brkUpRejectQty;
    private RichInputText seqNoBinding;
    private RichInputText checkPointDescBinding;
    private RichInputComboboxListOfValues inspecToolBinding;
    private RichInputText productionQtyBinding;
    private RichInputText rejQtyBinding;


    public InProcessInspectionBean() {
        
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                    {
                    ADFUtils.findOperation("Delete").execute();
                    System.out.println("Record Delete Successfully");
                    
                        FacesMessage Message = new FacesMessage("Record Deleted Successfully");   
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                        FacesContext fc = FacesContext.getCurrentInstance();   
                        fc.addMessage(null, Message);   
                    }
                AdfFacesContext.getCurrentInstance().addPartialTarget(inProcessInspDetailTablebinding);

    }

    public void setInProcessInspDetailTablebinding(RichTable inProcessInspDetailTablebinding) {
        this.inProcessInspDetailTablebinding = inProcessInspDetailTablebinding;
    }

    public RichTable getInProcessInspDetailTablebinding() {
        return inProcessInspDetailTablebinding;
    }

    public void deletePopupDialogDL1(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
            {
            ADFUtils.findOperation("Delete1").execute();
            System.out.println("Record Delete Successfully");
            
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");   
                Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                FacesContext fc = FacesContext.getCurrentInstance();   
                fc.addMessage(null, Message);   
            
            }

        AdfFacesContext.getCurrentInstance().addPartialTarget(rejectionBreakupTableBinding);

    }

    public void setRejectionBreakupTableBinding(RichTable rejectionBreakupTableBinding) {
        this.rejectionBreakupTableBinding = rejectionBreakupTableBinding;
    }

    public RichTable getRejectionBreakupTableBinding() {
        return rejectionBreakupTableBinding;
    }

    public void setUnitBinding(RichInputComboboxListOfValues unitBinding) {
        this.unitBinding = unitBinding;
    }

    public RichInputComboboxListOfValues getUnitBinding() {
        return unitBinding;
    }

    public void setInspectionNoBinding(RichInputText inspectionNoBinding) {
        this.inspectionNoBinding = inspectionNoBinding;
    }

    public RichInputText getInspectionNoBinding() {
        return inspectionNoBinding;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setDetaildeleteBinding(RichButton detaildeleteBinding) {
        this.detaildeleteBinding = detaildeleteBinding;
    }

    public RichButton getDetaildeleteBinding() {
        return detaildeleteBinding;
    }
    private void cevmodecheck(){
          if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
              makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
              cevModeDisableComponent("V");
          }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
              makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
              cevModeDisableComponent("C");
          }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
              makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
              cevModeDisableComponent("E");
          }
      }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
            try {
                Method method1 =
                    component.getClass().getMethod("setDisabled", boolean.class);
                if (method1 != null) {
                    method1.invoke(component, valueComponent);
                }
            } catch (NoSuchMethodException e) {
                try {
                    Method method =
                        component.getClass().getMethod("setReadOnly", boolean.class);
                    if (method != null) {
                        method.invoke(component, valueComponent);
                    }
                } catch (Exception e1) {
                    // e.printStackTrace();//silently eat this exception.
                }


            } catch (Exception e) {
                // e.printStackTrace();//silently eat this exception.
            }
            List<UIComponent> childComponents = component.getChildren();
            for (UIComponent comp : childComponents) {
                makeComponentHierarchyReadOnly(comp, valueComponent);
            }
        }
    
        public void cevModeDisableComponent(String mode)
        {
       
        if (mode.equals("E"))
        {
                getInspStatusBinding().setDisabled(true);
                getProdSlipNoBinding().setDisabled(true);
                getUnitBinding().setDisabled(true);
                getItemCodeBinding().setDisabled(true);
                getProdnQtyBinding().setDisabled(true);
                getProdnDateBinding().setDisabled(true);
                getProcessCdBinding().setDisabled(true);
                getAcceptQtyBinding().setDisabled(true);
                getJobCardBinding().setDisabled(true);
                getSequenceNoBinding().setDisabled(true);
                getRejectQtyBinding().setDisabled(true);
                getInspectionNoBinding().setDisabled(true);
                getHeaderEditBinding().setDisabled(true);
                getInspectionDateBinding().setDisabled(true);
                getDetaildeleteBinding().setDisabled(false);
                getHeaderEditBinding().setDisabled(true);
            getProductionQtyBinding().setDisabled(true);
            getRejQtyBinding().setDisabled(true);
            getInspecToolBinding().setDisabled(true);
        } 
        else if (mode.equals("C"))
        {
            getInspStatusBinding().setDisabled(true);
            getProdSlipNoBinding().setDisabled(true);
         getUnitBinding().setDisabled(true);
            getItemCodeBinding().setDisabled(true);
            getProdnQtyBinding().setDisabled(true);
            getProdnDateBinding().setDisabled(true);
            getProcessCdBinding().setDisabled(true);
            getAcceptQtyBinding().setDisabled(true);
            getJobCardBinding().setDisabled(true);
            getSequenceNoBinding().setDisabled(true);
            getRejectQtyBinding().setDisabled(true);
            getInspectionNoBinding().setDisabled(true);
            getDetaildeleteBinding().setDisabled(false);
            getInspectionDateBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            getSeqNoBinding().setDisabled(true);
            getCheckPointDescBinding().setDisabled(true);
            getProductionQtyBinding().setDisabled(true);
            getRejQtyBinding().setDisabled(true);
            getInspecToolBinding().setDisabled(true);
            
        } 
        else if (mode.equals("V"))
        {
            getDetaildeleteBinding().setDisabled(true);
        }
        
    }

    public void editButtonAL(ActionEvent actionEvent) {
        FacesMessage Message = new FacesMessage("Modification Denied");   
                               Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                               FacesContext fc = FacesContext.getCurrentInstance();   
                               fc.addMessage(null, Message);
//        cevmodecheck();
    }


    public void buttonGetRecordAL(ActionEvent actionEvent) {
        System.out.println("Get Record AL");
         OperationBinding op =  ADFUtils.findOperation("inProcessInspcPopulate");
         Object rst = op.execute();
        if(rst!=null && rst.equals("I"))
        {
    
                       FacesMessage Message = new FacesMessage("No Checkpoints Has  Been Entered For This Part & Process.");   
                                              Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                                              FacesContext fc = FacesContext.getCurrentInstance();   
                                              fc.addMessage(null, Message);
                                 
                   }
        
         getPendingInspcPopupBinding().cancel();
        getInspTypeBinding().setDisabled(true);
       // ADFUtils.findOperation("inspSumQty").execute(); 
        AdfFacesContext.getCurrentInstance().addPartialTarget(inProcessInspDetailTablebinding);
    }

    public void setPendingInspcPopupBinding(RichPopup pendingInspcPopupBinding) {
        this.pendingInspcPopupBinding = pendingInspcPopupBinding;
    }

    public RichPopup getPendingInspcPopupBinding() {
        return pendingInspcPopupBinding;
    }

    public void pendingInspectionAL(ActionEvent actionEvent) {
    ADFUtils.findOperation("pendingInspection").execute();
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
                  getPendingInspcPopupBinding().show(hints);
                   
                   
//                   /
//#{bindings.InProcessPendingInspectionPopulateVVO1Iterator.currentRow}/*  */
                   
                   Row rr=(Row)ADFUtils.evaluateEL("#{bindings.InProcessPendingInspectionPopulateVVO1Iterator.currentRow}");
                   if(rr != null){
                   rr.getAttribute("ProdnQty");
                   System.out.println("rr.getAttribute(\"ProdnQty\")"+rr.getAttribute("ProdnQty"));
                   System.out.println("rr.getAttribute(\"ProductionQtyTrans\")"+rr.getAttribute("ProductionQtyTrans"));
                   
                   if(rr.getAttribute("ProductionQtyTrans")==null) 
                   {
                      rr.setAttribute("ProductionQtyTrans",rr.getAttribute("ProdnQty")); 
                    }
                   }
                   getInspTypeBinding().setDisabled(true);
//     AdfFacesContext.getCurrentInstance().addPartialTarget(inProcessInspDetailTablebinding);
}


    public void setInspectionDateBinding(RichInputDate inspectionDateBinding) {
        this.inspectionDateBinding = inspectionDateBinding;
    }

    public RichInputDate getInspectionDateBinding() {
        return inspectionDateBinding;
    }

  

    public void setAcceptQtyBinding(RichInputText acceptQtyBinding) {
        this.acceptQtyBinding = acceptQtyBinding;
    }

    public RichInputText getAcceptQtyBinding() {
        return acceptQtyBinding;
    }

    public void setRejectQtyBinding(RichInputText rejectQtyBinding) {
        this.rejectQtyBinding = rejectQtyBinding;
    }

    public RichInputText getRejectQtyBinding() {
        return rejectQtyBinding;
    }

    public void setInspStatusBinding(RichSelectOneChoice inspStatusBinding) {
        this.inspStatusBinding = inspStatusBinding;
    }

    public RichSelectOneChoice getInspStatusBinding() {
        return inspStatusBinding;
    }

//    public void acceptQtyVCE(ValueChangeEvent vce)
////    {
////        if(vce!=null)
////        {
////            System.out.println("sum of qty"+vce);
////               vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
////              ADFUtils.findOperation("inspSumQty").execute();  
////               
////        }   
////    }
    public void RejQtyCal(ValueChangeEvent vce) {
        if(vce!=null) {
            vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
       // ADFUtils.findOperation("inspSumQty").execute();
           // calculatedData();
            BigDecimal accpt=new BigDecimal(0);
        BigDecimal accpt1=new BigDecimal(0);
        BigDecimal Prodn=new BigDecimal(0);
        Prodn=(BigDecimal)prodnQtyBinding.getValue();
            accpt=(BigDecimal)vce.getNewValue();
            if(accpt.compareTo(accpt1)==-1)
                   {
                    FacesMessage msg=new FacesMessage(FacesMessage.SEVERITY_ERROR, "Accept Qty must be greater than or equals to 0.", null);
                    FacesContext fc = FacesContext.getCurrentInstance(); 
                    fc.addMessage(null, msg);
                    
            }
            if(accpt.compareTo(Prodn)==1)
                       {
                        FacesMessage msg=new FacesMessage(FacesMessage.SEVERITY_ERROR, "Accept Qty must be less than or equals to Production Qty.", null);
                        FacesContext fc = FacesContext.getCurrentInstance(); 
                        fc.addMessage(null, msg);
                        
           }else{
        ADFUtils.findOperation("inspSumQty").execute();
        
           AdfFacesContext.getCurrentInstance().addPartialTarget(inProcessInspDetailTablebinding);
            }
    }       
    }
    public void calculatedData(){
            DCIteratorBinding dciter =
                        (DCIteratorBinding) ADFUtils.findIterator("InProcessInspectionDetailVO1Iterator");
                        dciter.executeQuery(); 

             InProcessInspectionDetailVORowImpl inprInspDetal =
                        (InProcessInspectionDetailVORowImpl )dciter.getCurrentRow();
             if(inprInspDetal!=null && inprInspDetal.getProdnQty()!=null && inprInspDetal.getAcceptQty()!=null){
                 
                 inprInspDetal.setRejQty(inprInspDetal.getProdnQty().subtract(inprInspDetal.getAcceptQty()));
                 
                 //bindRejectQtyHD.setValue(incmInspDetal.getLotQty().subtract(incmInspDetal.getAcceptQty()));
                 
                 }

        
        }

    public void setProdSlipNoBinding(RichInputText prodSlipNoBinding) {
        this.prodSlipNoBinding = prodSlipNoBinding;
    }

    public RichInputText getProdSlipNoBinding() {
        return prodSlipNoBinding;
    }

    public void setItemCodeBinding(RichInputText itemCodeBinding) {
        this.itemCodeBinding = itemCodeBinding;
    }

    public RichInputText getItemCodeBinding() {
        return itemCodeBinding;
    }

    public void setProdnQtyBinding(RichInputText prodnQtyBinding) {
        this.prodnQtyBinding = prodnQtyBinding;
    }

    public RichInputText getProdnQtyBinding() {
        return prodnQtyBinding;
    }

    public void setProdnDateBinding(RichInputDate prodnDateBinding) {
        this.prodnDateBinding = prodnDateBinding;
    }

    public RichInputDate getProdnDateBinding() {
        return prodnDateBinding;
    }

    public void setProcessCdBinding(RichInputText processCdBinding) {
        this.processCdBinding = processCdBinding;
    }

    public RichInputText getProcessCdBinding() {
        return processCdBinding;
    }

    public void setJobCardBinding(RichInputText jobCardBinding) {
        this.jobCardBinding = jobCardBinding;
    }

    public RichInputText getJobCardBinding() {
        return jobCardBinding;
    }

    public void setSequenceNoBinding(RichInputText sequenceNoBinding) {
        this.sequenceNoBinding = sequenceNoBinding;
    }

    public RichInputText getSequenceNoBinding() {
        return sequenceNoBinding;
    }

    public void setInspTypeBinding(RichSelectOneChoice inspTypeBinding) {
        this.inspTypeBinding = inspTypeBinding;
    }

    public RichSelectOneChoice getInspTypeBinding() {
        return inspTypeBinding;
    }

    public void SumVce(ValueChangeEvent vce) {
        if(vce!=null){
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
            //bindAcccptdQty.setValue(vce.getNewValue().toString());
//            calculatedData();
              
            //calculatedlotSize();
            ADFUtils.findOperation("inspSumQty").execute();  
            AdfFacesContext.getCurrentInstance().addPartialTarget(inProcessInspDetailTablebinding);
        
        }   
    }
    public String errorOnSave(){
        
        OperationBinding ob=ADFUtils.findOperation("sumAllRejQtyBrkUpInprocessInspection");
                Object op=ob.execute();
        AdfFacesContext.getCurrentInstance().addPartialTarget(inProcessInspDetailTablebinding);
                BigDecimal rejbrk = (BigDecimal) ob.getResult();
                  BigDecimal rejhd = (BigDecimal) getRejectQtyBinding().getValue();
                    System.out.println("rej brk up qty is--===>>>"+rejbrk+"rej qty i head comppp"+rejhd);
                    if(rejbrk.equals(rejhd) || rejbrk.compareTo(rejhd)==-1 ){
                     
        
        return "Y";
        
    }
                    else {
                        
                        //ADFUtils.showMessage(" Reject Qty must be equal to the actual rejected qty.", 0);
                        return "N";
                    }
        
    }

    public void SumOfAlRejectQtyBrkupVCE(ValueChangeEvent vce) {
        if(vce!=null){
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        OperationBinding ob=ADFUtils.findOperation("sumAllRejQtyBrkUpInprocessInspection");
        Object op=ob.execute();
        BigDecimal rejbrk = (BigDecimal) ob.getResult();
            BigDecimal rejhd = (BigDecimal)rejectQtyBinding.getValue();
            System.out.println("rej brk up qty is--===>>>"+rejbrk+"rej qty i head comppp"+rejhd);
            if(!rejbrk.equals(rejhd) || rejbrk.compareTo(rejhd)==1 ){
            ADFUtils.showMessage(" Reject Qty must be equal to the actual rejected qty.", 0);  
        }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(inProcessInspDetailTablebinding);
    }
      

    public void setBrkUpRejectQty(RichInputText brkUpRejectQty) {
        this.brkUpRejectQty = brkUpRejectQty;
    }

    public RichInputText getBrkUpRejectQty() {
        return brkUpRejectQty;
    }

    public void OnlySaveAL(ActionEvent actionEvent) {
        //ADFUtils.setLastUpdatedBy("InProcessInspectionHeaderVO1Iterator");
        BigDecimal a=new BigDecimal(0);
            BigDecimal Rej=new BigDecimal(0);
            Rej=(BigDecimal)rejectQtyBinding.getValue();
         
        // String ErrorResult =errorOnSave();
       if(Rej.compareTo(a)==1) 
       {
           if(((Long)ADFUtils.evaluateEL("#{bindings.InProcessInspectionRejectBreakupVO1.estimatedRowCount}")>0))
              {
                   OperationBinding op = ADFUtils.findOperation("getInProcessInspNo");
                   Object rst = op.execute();
                       if (rst != null && rst != "") 
                       {
                           if (op.getErrors().isEmpty()) {
                               ADFUtils.findOperation("Commit").execute();
                               ADFUtils.showMessage("Record Saved Successfully" + "Inspection no is::"+ rst, 2);
                           }
                       }
                       
                       if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ") 
                       {
                           if (op.getErrors().isEmpty()) {
                               ADFUtils.findOperation("Commit").execute();
                               ADFUtils.showMessage(" Record Updated Successfully.", 2); 
                         }
                      }
             }
              else{
           ADFUtils.showMessage("Specify Rejection for the rejected quantity.", 0);
              }
       }else{
//        if(((Long)ADFUtils.evaluateEL("#{bindings.InProcessInspectionRejectBreakupVO1.estimatedRowCount}")>0))
        {
         OperationBinding op = ADFUtils.findOperation("getInProcessInspNo");
         Object rst = op.execute();
             if (rst != null && rst != "") 
             {
                 if (op.getErrors().isEmpty()) {
                     ADFUtils.findOperation("Commit").execute();
                     ADFUtils.showMessage("Record Saved Successfully" + "Inspection no is::"+ rst, 2);
                 }
             }
             
             if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ") 
             {
                 if (op.getErrors().isEmpty()) {
                     ADFUtils.findOperation("Commit").execute();
                     ADFUtils.showMessage(" Record Updated Successfully.", 2); 
               }
        }
//       }
//        else {
//         if((Long)ADFUtils.evaluateEL("#{bindings.InProcessInspectionRejectBreakupVO1.estimatedRowCount}")<1 )
//         {
//         ADFUtils.showMessage("Specify Rejection for the rejected quantity.", 0);
//         }
//         if(ErrorResult.equalsIgnoreCase("N"))
//         {
//              ADFUtils.showMessage(" Reject Qty must be equal to the actual rejected qty.", 0);
//          }
        }
        }
    }

    public String SaveAndCloseAC() {
        BigDecimal a=new BigDecimal(0);
        
        String ErrorResult =errorOnSave();
        if((((Long)ADFUtils.evaluateEL("#{bindings.InProcessInspectionRejectBreakupVO1.estimatedRowCount}")>0 && !rejectQtyBinding.getValue().equals(a)) && ErrorResult.equalsIgnoreCase("Y")  )|| rejectQtyBinding.getValue().equals(a))
        {
                ADFUtils.findOperation("inspStatus").execute();
                OperationBinding op = ADFUtils.findOperation("getInProcessInspNo");
                Object rst = op.execute();
            if (rst != null && rst != "") 
            {
                if (op.getErrors().isEmpty()) {
                    ADFUtils.findOperation("Commit").execute();
                    ADFUtils.showMessage("Record Saved Successfully" + "Inspection no is::"+ rst, 2);
                    return "SaveAndRollBack";    

                }
            }
            
            if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ") 
            {
                if (op.getErrors().isEmpty()) {
                    ADFUtils.findOperation("Commit").execute();
                    ADFUtils.showMessage(" Record Updated Successfully.", 2); 
                  return "SaveAndRollBack";    

              }
        }
        }
        else {
           
           if((Long)ADFUtils.evaluateEL("#{bindings.InProcessInspectionRejectBreakupVO1.estimatedRowCount}")<1)
           {
           ADFUtils.showMessage("Specify Rejection for the rejected quantity.", 0);
           }
           if(ErrorResult.equalsIgnoreCase("N"))
           {
                ADFUtils.showMessage(" Reject Qty must be equal to the actual rejected qty.", 0);
            }
        }
        return null;    
        }

    public void setSeqNoBinding(RichInputText seqNoBinding) {
        this.seqNoBinding = seqNoBinding;
    }

    public RichInputText getSeqNoBinding() {
        return seqNoBinding;
    }

    public void setCheckPointDescBinding(RichInputText checkPointDescBinding) {
        this.checkPointDescBinding = checkPointDescBinding;
    }

    public RichInputText getCheckPointDescBinding() {
        return checkPointDescBinding;
    }

    public void setInspecToolBinding(RichInputComboboxListOfValues inspecToolBinding) {
        this.inspecToolBinding = inspecToolBinding;
    }

    public RichInputComboboxListOfValues getInspecToolBinding() {
        return inspecToolBinding;
    }


    public void setheaderVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
            Row rr=(Row)ADFUtils.evaluateEL("#{bindings.InProcessPendingInspectionPopulateVVO1Iterator.currentRow}");
           rr.getAttribute("ProductionQtyTrans");
           BigDecimal prod = (BigDecimal) rr.getAttribute("ProductionQtyTrans");
           BigDecimal a= new BigDecimal(0);
//           *
        //                   System.out.println("rr.getAttribute(\"ProdnQty\")"+rr.getAttribute("ProdnQty"));
        //                   System.out.println("rr.getAttribute(\"ProductionQtyTrans\")"+rr.getAttribute("ProductionQtyTrans"));
        //
                          if(rr.getAttribute("ProductionQtyTrans")!=null && prod.compareTo(a)==1)
                           {
                            prodnQtyBinding.setValue(rr.getAttribute("ProductionQtyTrans"));
                            }  
    }

    public void setProductionQtyBinding(RichInputText productionQtyBinding) {
        this.productionQtyBinding = productionQtyBinding;
    }

    public RichInputText getProductionQtyBinding() {
        return productionQtyBinding;
    }

    public void setRejQtyBinding(RichInputText rejQtyBinding) {
        this.rejQtyBinding = rejQtyBinding;
    }

    public RichInputText getRejQtyBinding() {
        return rejQtyBinding;
    }
}
