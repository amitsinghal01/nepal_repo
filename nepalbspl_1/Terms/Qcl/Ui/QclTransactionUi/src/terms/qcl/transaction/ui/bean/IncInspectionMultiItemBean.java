package terms.qcl.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

public class IncInspectionMultiItemBean {
    private RichOutputText bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichInputComboboxListOfValues unitBinding;
    private RichInputText qclNoBinding;
    private RichInputComboboxListOfValues docNoBinding;
    private RichInputComboboxListOfValues checkByBinding;
    private RichInputDate dateBinding;
    private RichInputDate docDateBinding;
    private RichInputText vendorBinding;
    private RichSelectOneChoice qclTpBinding;
    private RichInputText itemBinding;
    private RichButton bindingEditbutton;
    private RichInputText procBinding;

    public IncInspectionMultiItemBean() {
    }

    public void populateDetailDataAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("PopulateMultiItemIncInspData").execute();
    }

    public void saveAL(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedBy("IncInspectionMultiItemHeaderVO1Iterator");
        OperationBinding op = ADFUtils.findOperation("generateQclNo");
        Object rst = op.execute();
        //ADFUtils.findOperation("goForUnitRate").execute(); 
        System.out.println("--------Commit-------");
        System.out.println("VALUE AFTER GETTING RESULT =================>   "+rst);
            
        
            if (rst.toString() != null && rst.toString() != "") {
                if (op.getErrors().isEmpty()) {
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message = new FacesMessage("Record Saved Successfully.Gate Entry No is "+rst+".");   
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                    FacesContext fc = FacesContext.getCurrentInstance();   
                    fc.addMessage(null, Message);  
                   
                    ADFUtils.setEL("#{pageFlowScope.mode}", "V");
//                    cevmodecheck();
//                    AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
//        
                
                }
            }
            
            if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ") {
                if (op.getErrors().isEmpty()) {
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message = new FacesMessage("Record Update Successfully.");   
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                    FacesContext fc = FacesContext.getCurrentInstance();   
                    fc.addMessage(null, Message);  
                    ADFUtils.setEL("#{pageFlowScope.mode}", "V");
//                    cevmodecheck();
//                    AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
                }
                
                

     
            
    }
    }


    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
            try {
                Method method1 =
                    component.getClass().getMethod("setDisabled", boolean.class);
                if (method1 != null) {
                    method1.invoke(component, valueComponent);
                }
            } catch (NoSuchMethodException e) {
                try {
                    Method method =
                        component.getClass().getMethod("setReadOnly", boolean.class);
                    if (method != null) {
                        method.invoke(component, valueComponent);
                    }
                } catch (Exception e1) {
                    // e.printStackTrace();//silently eat this exception.
                }


            } catch (Exception e) {
                // e.printStackTrace();//silently eat this exception.
            }
            List<UIComponent> childComponents = component.getChildren();
            for (UIComponent comp : childComponents) {
                makeComponentHierarchyReadOnly(comp, valueComponent);
            }
        }
    
    
    private void cevmodecheck(){
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }
    public void cevModeDisableComponent(String mode) {
        
        if(mode.equals("E")){
            unitBinding.setDisabled(true);
            qclNoBinding.setDisabled(true);
            dateBinding.setDisabled(true);
            docNoBinding.setDisabled(true);
            vendorBinding.setDisabled(true);
            docDateBinding.setDisabled(true);
            qclTpBinding.setDisabled(true);
            itemBinding.setDisabled(true);
            procBinding.setDisabled(true);
            }
        if(mode.equals("C")){
            itemBinding.setDisabled(true);
            qclNoBinding.setDisabled(true);
            procBinding.setDisabled(true);
        }
        if(mode.equals("V")){
            
        }
    }

    public void setUnitBinding(RichInputComboboxListOfValues unitBinding) {
        this.unitBinding = unitBinding;
    }

    public RichInputComboboxListOfValues getUnitBinding() {
        return unitBinding;
    }

    public void setQclNoBinding(RichInputText qclNoBinding) {
        this.qclNoBinding = qclNoBinding;
    }

    public RichInputText getQclNoBinding() {
        return qclNoBinding;
    }

    public void setDocNoBinding(RichInputComboboxListOfValues docNoBinding) {
        this.docNoBinding = docNoBinding;
    }

    public RichInputComboboxListOfValues getDocNoBinding() {
        return docNoBinding;
    }

    public void setCheckByBinding(RichInputComboboxListOfValues checkByBinding) {
        this.checkByBinding = checkByBinding;
    }

    public RichInputComboboxListOfValues getCheckByBinding() {
        return checkByBinding;
    }

    public void setDateBinding(RichInputDate dateBinding) {
        this.dateBinding = dateBinding;
    }

    public RichInputDate getDateBinding() {
        return dateBinding;
    }

    public void setDocDateBinding(RichInputDate docDateBinding) {
        this.docDateBinding = docDateBinding;
    }

    public RichInputDate getDocDateBinding() {
        return docDateBinding;
    }

    public void setVendorBinding(RichInputText vendorBinding) {
        this.vendorBinding = vendorBinding;
    }

    public RichInputText getVendorBinding() {
        return vendorBinding;
    }

    public void setQclTpBinding(RichSelectOneChoice qclTpBinding) {
        this.qclTpBinding = qclTpBinding;
    }

    public RichSelectOneChoice getQclTpBinding() {
        return qclTpBinding;
    }

    public void setItemBinding(RichInputText itemBinding) {
        this.itemBinding = itemBinding;
    }

    public RichInputText getItemBinding() {
        return itemBinding;
    }

    public void setBindingEditbutton(RichButton bindingEditbutton) {
        this.bindingEditbutton = bindingEditbutton;
    }

    public RichButton getBindingEditbutton() {
        return bindingEditbutton;
    }

    public void editButtonAL(ActionEvent actionEvent) {
     
     
     cevmodecheck();
        // Add event code here...
    }

    public void setProcBinding(RichInputText procBinding) {
        this.procBinding = procBinding;
    }

    public RichInputText getProcBinding() {
        return procBinding;
    }
}
