package terms.qcl.transaction.ui.bean;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;


import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;

import java.math.BigDecimal;

import java.sql.Connection;

import java.sql.SQLException;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.application.ViewHandler;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.naming.Context;
import javax.naming.InitialContext;

import javax.servlet.http.HttpServletResponse;

import javax.sql.DataSource;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.BindingContainer;

public class InstrumentGuageCallibrationBean {
    private String InstMode ="";
    private RichTable searchTableBinding;

    public InstrumentGuageCallibrationBean() {
    }

    public void setSearchTableBinding(RichTable searchTableBinding) {
        this.searchTableBinding = searchTableBinding;
    }

    public RichTable getSearchTableBinding() {
        return searchTableBinding;
    }

    public void deleteDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
            {
            OperationBinding op = null;
            ADFUtils.findOperation("Delete").execute();
            op = (OperationBinding) ADFUtils.findOperation("Commit");
            Object rst = op.execute();
            System.out.println("Record Delete Successfully");
                if(op.getErrors().isEmpty()){
                    FacesMessage Message = new FacesMessage("Record Deleted Successfully");   
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                    FacesContext fc = FacesContext.getCurrentInstance();   
                    fc.addMessage(null, Message);
                } else if (!op.getErrors().isEmpty())
                {
                   OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
                   Object rstr = opr.execute();
                   FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                   Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                   FacesContext fc = FacesContext.getCurrentInstance();
                   fc.addMessage(null, Message);
                 }
            }

            AdfFacesContext.getCurrentInstance().addPartialTarget(searchTableBinding);
    }
    
    //*********************Report Calling Code***********************//
    
    public BindingContainer getBindings()
      {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
      }
    
    public void downloadJrxml(FacesContext facesContext, OutputStream outputStream) {
        Connection conn=null;
        try {

                        String file_name ="";
                        System.out.println("InstMode===================>"+InstMode);
                        
                        if(InstMode.equals("IC")) 
                        {
                            file_name="r_calib.jasper";
                        }
                        else if(InstMode.equals("MF"))
                        {
                            file_name="r_calib_ms.jasper";
                        }
                        else if(InstMode.equals("PSR"))
                        {
                            file_name="r_calib_ps_pr.jasper";
                        }
                        else if(InstMode.equals("PW")){
                            file_name="r_calib_5p.jasper";
                        }
                        else if(InstMode.equals("TM")){
                            file_name="r_calib_tc.jasper";
                        }
                        else if(InstMode.equals("OTH")){
                            file_name="r_calib_hogs.jasper";
                        }
                        
                        System.out.println("After Set Value File Name is===>"+file_name);
                       
                       
                        OperationBinding binding = ADFUtils.findOperation("fileNameForPrint");
                        binding.getParamsMap().put("fileId","ERP0000000448");
                        binding.execute();
                        //*************End Find File name***********//
                        System.out.println("Binding Result :"+binding.getResult());
                        if(binding.getResult() != null){
                            String result = binding.getResult().toString();
                            int last_index = result.lastIndexOf("/");
                            String path = result.substring(0,last_index+1)+file_name;
                            System.out.println("FILE PATH IS===>"+path);
                            InputStream input = new FileInputStream(path);
                            String LV_SID=oracle.jbo.server.uniqueid.UniqueIdHelper.getNextId().toString();
                            System.out.println("SID======>"+LV_SID);

                            DCIteratorBinding instCdIter = (DCIteratorBinding) getBindings().get("SearchInstrumentGaugeCalibrationHeaderVO1Iterator");
                            String entryNo = instCdIter.getCurrentRow().getAttribute("CalibEntNo").toString();
                            String unitCode = instCdIter.getCurrentRow().getAttribute("UnitCd").toString();
                        
//                            BigDecimal amd = (BigDecimal)poIter.getCurrentRow().getAttribute("AmdNo");
                            System.out.println("Inst Cd :- "+entryNo+" "+ "Unit Code " + unitCode);
                            
//                            if(InstMode.equals("PSR")){
//                                System.out.println("************Inside calling procedure*******");
//                            oracle.adf.model.OperationBinding binding1 = (oracle.adf.model.OperationBinding) ADFUtils.findOperation("PoAmendReportCallingProcedure");
//                            binding1.getParamsMap().put("PO",poNo);
//                            binding1.getParamsMap().put("AMD",amd);
//                            binding1.getParamsMap().put("LV_SID",LV_SID);
//                            binding1.execute();
//                            }
                            
                            
                            Map n = new HashMap();
                            
                            if(InstMode.equals("IC") || InstMode.equals("MF") || InstMode.equals("PSR") || InstMode.equals("PW") || InstMode.equals("TM") || InstMode.equals("OTH"))
                            {
//                                System.out.println("sign binding ==>"+signBinding.getValue()+"upto binding =>"+uptoBinding.getValue()+"enclosure binding==>"+enclosureBinding.getValue());
                                n.put("p_ent_no", entryNo);
                                n.put("p_unit", unitCode);
                            
                            }
                            else
                            {
                                System.out.println("INSIDE ELSE FOR WHICH INST MODE==>>"+InstMode);
                                n.put("p_ent_no", entryNo);
                                n.put("p_unit", unitCode);
                                
                            }
                            
                            conn = getConnection( );
                            
                            JasperReport design = (JasperReport) JRLoader.loadObject(input);
                            System.out.println("Path : " + input + " -------" + design+" param:"+n);
                            
                            @SuppressWarnings("unchecked")
                            net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                            JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                            byte pdf[] = JasperExportManager.exportReportToPdf(print);
                            HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                            response.getOutputStream().write(pdf);
                            response.getOutputStream().flush();
                            response.getOutputStream().close();
                            facesContext.responseComplete();
                            }else
                            System.out.println("File Name/File Path not found.");
                            } catch (FileNotFoundException fnfe) {
                            // TODO: Add catch code
                            fnfe.printStackTrace();
                            try {
                                System.out.println("in finally connection closed");
                                        conn.close( );
                                        conn = null;
                                
                            } catch( SQLException e ) {
                                e.printStackTrace( );
                            }
                            } catch (Exception e) {
                            // TODO: Add catch code
                            e.printStackTrace();
                            try {
                                System.out.println("in finally connection closed");
                                        conn.close( );
                                        conn = null;
                                
                            } catch( SQLException e1 ) {
                                e1.printStackTrace( );
                            }
                            }finally {
                                    try {
                                            System.out.println("in finally connection closed");
                                                    conn.close( );
                                                    conn = null;
                                            
                                    } catch( SQLException e ) {
                                            e.printStackTrace( );
                                    }
                            }
    }
    
    public void setInstMode(String InstMode) {
        this.InstMode = InstMode;
    }

    public String getInstMode() {
        return InstMode;
    }
    public Connection getConnection() throws Exception{
            Context ctx = null;
            Connection conn = null;
            ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/ApplicationDBDS");
            conn = ds.getConnection();
            return conn;
        }
}
