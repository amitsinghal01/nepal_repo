package terms.qcl.transaction.ui.bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import javax.faces.validator.ValidatorException;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.nav.RichCommandLink;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.adf.view.rich.remote.result.ErrorResult;

import oracle.adf.view.rich.util.ResetUtils;

import oracle.binding.OperationBinding;

import org.apache.myfaces.trinidad.model.UploadedFile;

import terms.qcl.transaction.model.view.PredispatchAuditDetailVORowImpl;

public class PredispatchAuditBean {
    private RichPanelHeader getMyPageRootComponent;
    private RichPopup pendingPdqaPopupBinding;
    private RichOutputText bindingOutputText;
    private RichSelectOneChoice chkStBinding;
    private RichInputText prodSlipNoBinding;
    private RichInputText jobCardNoBinding;
    private RichInputDate jobCardDateBinding;
    private RichInputText prodCodeBinding;
    private RichInputText slipNoBinding;
    private RichInputText lotQtyBinding;
    private RichInputText batchCodeBinding;
    private RichInputText sampleSizeBinding;
    private RichInputText rejQtyBinding;
    private RichInputText acceptQtyHdBinding;
    private RichInputText jobCrdQtyBinding;
    private RichInputText custCdBinding;
    private RichInputText totChkBinding;
    private RichInputText rejQtyDtlBinding;
    private RichInputDate pdaqDtBinding;
    private RichInputText pdaqNoBinding;
    private RichInputText chkCdBinding;
    private RichInputText inspToolBinding;
    private RichTable dtlBinding;
    private RichSelectOneChoice inspStatBinding;
    private RichInputText checkByBinding;
    private RichSelectOneChoice statusBinding;
    private RichButton pendingButtonBinding;
    private RichInputComboboxListOfValues unitBinding;
    private RichInputText okBinding;
    private RichInputText unitCodePartProdLotBinding;
    private RichInputText itemCodePartProdLotBinding;
    private RichInputText lotNoPartProdLotBinding;
    private RichInputText prodSlipNoPartProdLotBinding;
    private RichInputText minOberservationTrans;
    private RichInputText maxObservationTrans;
    private RichInputText valueBinding;
    private RichButton rejectionDetailButtonBinding;
    private RichInputComboboxListOfValues defectCodeBinding;
    private RichInputText defectQtyBinding;
    private RichInputComboboxListOfValues statusBinding1;
    private RichSelectOneChoice customerTypeBinding;
    private RichInputComboboxListOfValues customerCodeBinding;
    private RichInputText calculativeFlagBinding;
    private RichInputComboboxListOfValues approvedByBinding;
    private RichInputComboboxListOfValues productCodeBinding;
    private RichInputText noOfPackingBinding;
    private RichButton headerEditBinding;
    private RichInputText qcdNoBinding;
    private RichInputText inspectionStatusBinding;
    private RichInputText entryNoBinding;
    private RichTable referenceDocumentTableBinding;
    private RichInputText pathBinding;
    private RichInputText docSlNoBinding;
    private RichInputText unitCodeRefDocBinding;
    private RichInputText refDocNoBinding;
    private RichInputText refDocTypeBinding;
    private RichInputText docFileNameBinding;
    private RichInputDate refDocDateBinding;
    private RichInputText remarksRefDocBinding;
    private RichShowDetailItem showDetailTabBinding;
    private RichSelectOneChoice conclusionBinding;
    private RichCommandLink downloadLinkBinding;
    private RichShowDetailItem referenceDocTabBinding;
    private RichSelectOneChoice chkSatus;

    public PredispatchAuditBean() {
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void pendingPdqaAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("exePendingPdqa").execute();
        RichPopup.PopupHints popup=new RichPopup.PopupHints();
        getPendingPdqaPopupBinding().show(popup);
        
    }

    public void setPendingPdqaPopupBinding(RichPopup pendingPdqaPopupBinding) {
        this.pendingPdqaPopupBinding = pendingPdqaPopupBinding;
    }

    public RichPopup getPendingPdqaPopupBinding() {
        return pendingPdqaPopupBinding;
    }

    public void deletePopupRejectionDL(DialogEvent dialogEvent) {
        // Add event code here...
    }

    public void pendingPopulatePdqaAL(ActionEvent actionEvent) {
        System.out.println("******************"+prodSlipNoBinding.getValue());
        if(prodSlipNoBinding.getValue()!=null)
        {
            
        System.out.println(getProdSlipNoBinding().getValue().toString().substring(0,1));
        
        System.out.println(getProdSlipNoBinding().getValue().toString().substring(6, 7));
        System.out.println(getProdSlipNoBinding().getValue().toString().substring(6, 8));

        String str="";
        str=getProdSlipNoBinding().getValue().toString().substring(5, 7);
        System.out.println(str);
        System.out.println("prod slip no binding in bean===>>>>"+str);
        OperationBinding ob=ADFUtils.findOperation("populatePredispatchAudit");
        ob.getParamsMap().put("prodSlip",str);
        Object op=ob.execute();
            AdfFacesContext.getCurrentInstance().addPartialTarget(dtlBinding);
            if(op!=null && op.equals("I")){
                    
                    ADFUtils.showMessage("No Checkpoints Has  Been Entered For This Part & Process.", 0);
            }
            if(chkStBinding.getValue()!=null && chkStBinding.getValue().equals("BC")){
                getValueBinding().setDisabled(false);
                getOkBinding().setDisabled(true);
                }
            else{
                getValueBinding().setDisabled(true);
                getOkBinding().setDisabled(false);
            }
    getPendingPdqaPopupBinding().cancel();
        }
    }

    public void saveAL(ActionEvent actionEvent) {
        //ADFUtils.setLastUpdatedBy("PredispatchAuditHeaderVO1Iterator");
        BigDecimal a=new BigDecimal(0);
        BigDecimal b=new BigDecimal(0);
        BigDecimal acceptQty = new BigDecimal(0);
        BigDecimal rejectQty = new BigDecimal(0);
        acceptQty = (BigDecimal)acceptQtyHdBinding.getValue();
        acceptQty= (BigDecimal) (acceptQtyHdBinding.getValue()!=null?acceptQtyHdBinding.getValue():new BigDecimal(0) ) ;
        rejectQty= (BigDecimal) (rejQtyBinding.getValue()!=null?rejQtyBinding.getValue():new BigDecimal(0) ) ;
        a=acceptQty.add(rejectQty);
        System.out.println(" The Sum Value is =====:"+a);   
        
        if(a.compareTo(new BigDecimal(0))==1)
        {
              String ErrorResult =errorOnSave();
             
              if((((Long)ADFUtils.evaluateEL("#{bindings.PreDispatchRejectionVO1Iterator.estimatedRowCount}")>0 && !rejQtyBinding.getValue().equals(b)) && ErrorResult.equalsIgnoreCase("Y")  )|| rejQtyBinding.getValue().equals(b))
              {
        OperationBinding op = ADFUtils.findOperation("getPdaqNo");
        Object rst = op.execute();
            if (rst != null && rst != "") 
            {
                if (op.getErrors().isEmpty()) {
                    ADFUtils.findOperation("Commit").execute();
                    ADFUtils.showMessage("Record Saved Successfully. " + "Final Inspection No. is:: "+ rst, 2);
                }
            }
            
            if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ") 
            {
                if (op.getErrors().isEmpty()) {
                    ADFUtils.findOperation("Commit").execute();
                    ADFUtils.showMessage(" Record Updated Successfully.", 2); 
              }
            }
          }
              else{
                  if((Long)ADFUtils.evaluateEL("#{bindings.PreDispatchRejectionVO1Iterator.estimatedRowCount}")==0){
                  ADFUtils.showMessage("Specify Rejection for the rejected quantity.", 0);
              }
                  if(ErrorResult.equalsIgnoreCase("N"))
                     {
                               ADFUtils.showMessage(" Reject Qty must be equal to the actual rejected qty.", 0);
                    }
              }
        }
        else {
            ADFUtils.showMessage("Please Enter the result.", 0);
        }
    }
    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }
    private void cevmodecheck(){
            if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
               
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
                cevModeDisableComponent("V");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
                
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("C");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
                
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("E");
            }
        }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                try {
                    Method method1 =
                        component.getClass().getMethod("setDisabled", boolean.class);
                    if (method1 != null) {
                        method1.invoke(component, valueComponent);
                    }
                } catch (NoSuchMethodException e) {
                    try {
                        Method method =
                            component.getClass().getMethod("setReadOnly", boolean.class);
                        if (method != null) {
                            method.invoke(component, valueComponent);
                        }
                    } catch (Exception e1) {
                        // e.printStackTrace();//silently eat this exception.
                    }


                } catch (Exception e) {
                    // e.printStackTrace();//silently eat this exception.
                }
                List<UIComponent> childComponents = component.getChildren();
                for (UIComponent comp : childComponents) {
                    makeComponentHierarchyReadOnly(comp, valueComponent);
                }
            }
    public void cevModeDisableComponent(String mode) {
        
        
        
        if(mode.equals("E")){
            
            getJobCardDateBinding().setDisabled(true);
            getJobCardNoBinding().setDisabled(true);
            getSlipNoBinding().setDisabled(true);
            getProdCodeBinding().setDisabled(true);
            getRejQtyBinding().setDisabled(true);
            getLotQtyBinding().setDisabled(true);
            getSampleSizeBinding().setDisabled(true);
             getAcceptQtyHdBinding().setDisabled(true);
             getJobCrdQtyBinding().setDisabled(true);
             getCustCdBinding().setDisabled(true);
             getTotChkBinding().setDisabled(true);
             getRejQtyDtlBinding().setDisabled(true);
             getPdaqDtBinding().setDisabled(true);
             getPdaqNoBinding().setDisabled(true);
            getChkCdBinding().setDisabled(true);
            getInspToolBinding().setDisabled(true);
            getInspectionStatusBinding().setDisabled(true);
            getCheckByBinding().setDisabled(true);
            getStatusBinding1().setDisabled(true);
            getUnitBinding().setDisabled(true);
            getOkBinding().setDisabled(true);
            getCalculativeFlagBinding().setDisabled(true);
            getProductCodeBinding().setDisabled(true);
            getCustomerTypeBinding().setDisabled(true);
            getCustomerCodeBinding().setDisabled(true);
            getBatchCodeBinding().setDisabled(true);
            getEntryNoBinding().setDisabled(true);
            getNoOfPackingBinding().setDisabled(true);
            getChkStBinding().setDisabled(true);
            getMinOberservationTrans().setDisabled(true);
            getMaxObservationTrans().setDisabled(true);

            getHeaderEditBinding().setDisabled(true);
            getQcdNoBinding().setDisabled(true);
            getDocSlNoBinding().setDisabled(true);
            getUnitCodeRefDocBinding().setDisabled(true);
            getRefDocNoBinding().setDisabled(true);
            getRefDocTypeBinding().setDisabled(true);
            getDocFileNameBinding().setDisabled(true);
            getRefDocDateBinding().setDisabled(true);
            getConclusionBinding().setDisabled(true);
//            getApprovedByBinding().setDisabled(true);
            
            if(approvedByBinding.getValue()!=null)
            {
                getValueBinding().setDisabled(true);
            }else{
                getValueBinding().setDisabled(false);
            }
            }
        if(mode.equals("C")){
                getJobCardDateBinding().setDisabled(true);
                getJobCardNoBinding().setDisabled(true);
                getSlipNoBinding().setDisabled(true);
                getProdCodeBinding().setDisabled(true);
                getProductCodeBinding().setDisabled(true);
                getCustomerTypeBinding().setDisabled(true);
                getCustomerCodeBinding().setDisabled(true);
                getRejQtyBinding().setDisabled(true);
                getNoOfPackingBinding().setDisabled(true);
                getLotQtyBinding().setDisabled(true);
                getSampleSizeBinding().setDisabled(true);
                getBatchCodeBinding().setDisabled(true);
                getEntryNoBinding().setDisabled(true);
                getAcceptQtyHdBinding().setDisabled(true);
                getJobCrdQtyBinding().setDisabled(true);
                getCustCdBinding().setDisabled(true);
                getRejQtyDtlBinding().setDisabled(true);
                getTotChkBinding().setDisabled(true);
                getPdaqDtBinding().setDisabled(true);
                getPdaqNoBinding().setDisabled(true);
                getChkCdBinding().setDisabled(true);
                getInspToolBinding().setDisabled(true);
                getInspectionStatusBinding().setDisabled(true);
                 getCheckByBinding().setDisabled(true);
                 getStatusBinding1().setDisabled(true);
                 if(chkStBinding.getValue().equals("BC"))
                 {
                     getOkBinding().setDisabled(false);
                 }
                valueBinding.setDisabled(true);
                getMinOberservationTrans().setDisabled(true);
                getMaxObservationTrans().setDisabled(true);
                 getCalculativeFlagBinding().setDisabled(true);
              getUnitBinding().setDisabled(true);
                getApprovedByBinding().setDisabled(true);
                getHeaderEditBinding().setDisabled(true);
                getQcdNoBinding().setDisabled(true);
                getDocSlNoBinding().setDisabled(true);
                getUnitCodeRefDocBinding().setDisabled(true);
                getRefDocNoBinding().setDisabled(true);
                getRefDocTypeBinding().setDisabled(true);
                getDocFileNameBinding().setDisabled(true);
                getRefDocDateBinding().setDisabled(true);
                getConclusionBinding().setDisabled(true);
//                 getUnitCodePartProdLotBinding().setDisabled(true);
//                 unitCodePartProdLotBinding.setDisabled(true);
            }
        if (mode.equals("V")) {
            downloadLinkBinding.setDisabled(false);
            showDetailTabBinding.setDisabled(false);
            referenceDocTabBinding.setDisabled(false);
//            getRejectionDetailButtonBinding().setDisabled(false);
                            //getHeaderEditBinding().setDisabled(false);
        //                    getDetailcreateBinding().setDisabled(true);
        //                    getDetaildeleteBinding().setDisabled(true);
                        }
        
    }    
    

    public void setChkStBinding(RichSelectOneChoice chkStBinding) {
        this.chkStBinding = chkStBinding;
    }

    public RichSelectOneChoice getChkStBinding() {
        return chkStBinding;
    }

    public void setProdSlipNoBinding(RichInputText prodSlipNoBinding) {
        this.prodSlipNoBinding = prodSlipNoBinding;
    }

    public RichInputText getProdSlipNoBinding() {
        return prodSlipNoBinding;
    }

    public void setJobCardNoBinding(RichInputText jobCardNoBinding) {
        this.jobCardNoBinding = jobCardNoBinding;
    }

    public RichInputText getJobCardNoBinding() {
        return jobCardNoBinding;
    }

    public void setJobCardDateBinding(RichInputDate jobCardDateBinding) {
        this.jobCardDateBinding = jobCardDateBinding;
    }

    public RichInputDate getJobCardDateBinding() {
        return jobCardDateBinding;
    }

    public void setProdCodeBinding(RichInputText prodCodeBinding) {
        this.prodCodeBinding = prodCodeBinding;
    }

    public RichInputText getProdCodeBinding() {
        return prodCodeBinding;
    }

    public void setSlipNoBinding(RichInputText slipNoBinding) {
        this.slipNoBinding = slipNoBinding;
    }

    public RichInputText getSlipNoBinding() {
        return slipNoBinding;
    }

    public void setLotQtyBinding(RichInputText lotQtyBinding) {
        this.lotQtyBinding = lotQtyBinding;
    }

    public RichInputText getLotQtyBinding() {
        return lotQtyBinding;
    }

    public void setBatchCodeBinding(RichInputText batchCodeBinding) {
        this.batchCodeBinding = batchCodeBinding;
    }

    public RichInputText getBatchCodeBinding() {
        return batchCodeBinding;
    }

    public void setSampleSizeBinding(RichInputText sampleSizeBinding) {
        this.sampleSizeBinding = sampleSizeBinding;
    }

    public RichInputText getSampleSizeBinding() {
        return sampleSizeBinding;
    }

    public void setRejQtyBinding(RichInputText rejQtyBinding) {
        this.rejQtyBinding = rejQtyBinding;
    }

    public RichInputText getRejQtyBinding() {
        return rejQtyBinding;
    }

    public void okPcsVCE(ValueChangeEvent vce) {
        if(vce!=null){
    vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        calculateRejQty();
          
            ADFUtils.findOperation("sumOfRejQtyDtl").execute();
//        AdfFacesContext.getCurrentInstance().addPartialTarget(dtlBinding);  
        ADFUtils.findOperation("changeStatusinDtl").execute();
        ADFUtils.findOperation("inspStatusForPDA").execute();
    
    }
        }
    
    public void calculateRejQty(){
        
        DCIteratorBinding dciter =
                    (DCIteratorBinding) ADFUtils.findIterator("PredispatchAuditDetailVO1Iterator");
                    dciter.executeQuery(); 
                   PredispatchAuditDetailVORowImpl vorow = (PredispatchAuditDetailVORowImpl) dciter.getCurrentRow();
                   if(vorow!=null && vorow.getTotChkd()!=null && vorow.getOkPcs()!=null){
                     vorow.setRejectedPcs(vorow.getTotChkd().subtract(vorow.getOkPcs()));
                       
                       
                   }
        
        
        
    }

    public void setAcceptQtyHdBinding(RichInputText acceptQtyHdBinding) {
        this.acceptQtyHdBinding = acceptQtyHdBinding;
    }

    public RichInputText getAcceptQtyHdBinding() {
        return acceptQtyHdBinding;
    }

    public void setJobCrdQtyBinding(RichInputText jobCrdQtyBinding) {
        this.jobCrdQtyBinding = jobCrdQtyBinding;
    }

    public RichInputText getJobCrdQtyBinding() {
        return jobCrdQtyBinding;
    }

    public void setCustCdBinding(RichInputText custCdBinding) {
        this.custCdBinding = custCdBinding;
    }

    public RichInputText getCustCdBinding() {
        return custCdBinding;
    }

    public void setTotChkBinding(RichInputText totChkBinding) {
        this.totChkBinding = totChkBinding;
    }

    public RichInputText getTotChkBinding() {
        return totChkBinding;
    }

    public void setRejQtyDtlBinding(RichInputText rejQtyDtlBinding) {
        this.rejQtyDtlBinding = rejQtyDtlBinding;
    }

    public RichInputText getRejQtyDtlBinding() {
        return rejQtyDtlBinding;
    }

    public void setPdaqDtBinding(RichInputDate pdaqDtBinding) {
        this.pdaqDtBinding = pdaqDtBinding;
    }

    public RichInputDate getPdaqDtBinding() {
        return pdaqDtBinding;
    }

    public void setPdaqNoBinding(RichInputText pdaqNoBinding) {
        this.pdaqNoBinding = pdaqNoBinding;
    }

    public RichInputText getPdaqNoBinding() {
        return pdaqNoBinding;
    }

    public void setChkCdBinding(RichInputText chkCdBinding) {
        this.chkCdBinding = chkCdBinding;
    }

    public RichInputText getChkCdBinding() {
        return chkCdBinding;
    }

    public void setInspToolBinding(RichInputText inspToolBinding) {
        this.inspToolBinding = inspToolBinding;
    }

    public RichInputText getInspToolBinding() {
        return inspToolBinding;
    }

    public void setDtlBinding(RichTable dtlBinding) {
        this.dtlBinding = dtlBinding;
    }

    public RichTable getDtlBinding() {
        return dtlBinding;
    }

    public void defQtyBrkUpVCE(ValueChangeEvent vce) {
        if(vce!=null){
           vce.getComponent().processUpdates(FacesContext.getCurrentInstance()); 
            OperationBinding ob=ADFUtils.findOperation("sumAllRejQtyBrkUpForPDA");
                    Object op=ob.execute();
                    BigDecimal rejbrk = (BigDecimal) ob.getResult();
                        BigDecimal rejhd = (BigDecimal) getRejQtyBinding().getValue();
                        System.out.println("rej brk up qty is--===>>>"+rejbrk+"rej qty i head comppp"+rejhd);
                        if(!rejbrk.equals(rejhd) || rejbrk.compareTo(rejhd)==1 ){
                        ADFUtils.showMessage(" Reject Qty must be equal to the actual rejected qty.", 0); 
                    }
                }
            
            
        }
    

    public void deletePopupDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                 {
                 ADFUtils.findOperation("Delete1").execute();
                 OperationBinding op = (OperationBinding) ADFUtils.findOperation("Commit");
                 Object rst = op.execute();
                 System.out.println("Record Delete Successfully");
                 

                     if(op.getErrors().isEmpty())
                     {
                     FacesMessage Message = new FacesMessage("Record Deleted Successfully");   
                     Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                     FacesContext fc = FacesContext.getCurrentInstance();   
                     fc.addMessage(null, Message);
                     }
                     else
                     {
                        FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                        Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                        FacesContext fc = FacesContext.getCurrentInstance();
                        fc.addMessage(null, Message);
                      }
                 
                 }

             AdfFacesContext.getCurrentInstance().addPartialTarget(dtlBinding);
    }
    
    public String errorOnSave(){
           
            OperationBinding ob=ADFUtils.findOperation("sumAllRejQtyBrkUpForPDA");
                    Object op=ob.execute();
                    BigDecimal rejbrk = (BigDecimal) ob.getResult();
                    BigDecimal rejhd = (BigDecimal) getRejQtyBinding().getValue()==null ? new BigDecimal(0) : (BigDecimal) getRejQtyBinding().getValue();
                        System.out.println("rej brk up qty is--===>>>"+rejbrk+"rej qty i head comppp"+rejhd);
//                        if(rejQtyBinding.getValue() != null) {
                            if(rejbrk.equals(rejhd) || rejbrk.compareTo(rejhd)==-1 ){
                            
                            
                            return "Y";
                            
                            }
//                            return "Y";
//                        }
//                        if(rejQtyBinding.getValue() != null && rejbrk.equals(rejhd) || rejbrk.compareTo(rejhd)==-1 ){
//                        
//           
//            return "Y";
//           
//        }
                        else {
                           
                            return "N";
                        }
    }
    
    public String SaveandClosePreDispatchAL(){
      ADFUtils.setLastUpdatedBy("PredispatchAuditHeaderVO1Iterator");
            BigDecimal a=new BigDecimal(0);
            BigDecimal b=new BigDecimal(0);
            BigDecimal acceptQty = new BigDecimal(0);
              BigDecimal rejectQty = new BigDecimal(0);
              acceptQty = (BigDecimal)acceptQtyHdBinding.getValue();
              acceptQty= (BigDecimal) (acceptQtyHdBinding.getValue()!=null?acceptQtyHdBinding.getValue():new BigDecimal(0) ) ;
              rejectQty= (BigDecimal) (rejQtyBinding.getValue()!=null?rejQtyBinding.getValue():new BigDecimal(0) ) ;
              a=acceptQty.add(rejectQty);
              System.out.println(" The Sum Value is =====:"+a);   
              
              if(a.compareTo(new BigDecimal(0))==1)
              {     
                  
                   String ErrorResult =errorOnSave();
                 
                  if((((Long)ADFUtils.evaluateEL("#{bindings.PreDispatchRejectionVO1Iterator.estimatedRowCount}")>0 && !rejQtyBinding.getValue().equals(b)) && ErrorResult.equalsIgnoreCase("Y")  )|| rejQtyBinding.getValue().equals(b))
                  {
            OperationBinding op = ADFUtils.findOperation("getPdaqNo");
            Object rst = op.execute();
                if (rst != null && rst != "") 
                {
                    if (op.getErrors().isEmpty()) {
                        ADFUtils.findOperation("Commit").execute();
                        ADFUtils.showMessage("Record Saved Successfully" + "PDAQ No is::"+ rst, 2);
                        return "SaveAndClose";
                    }
                }
                
                if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ") 
                {
                    if (op.getErrors().isEmpty()) {
                        ADFUtils.findOperation("Commit").execute();
                        ADFUtils.showMessage(" Record Updated Successfully.", 2); 
                  return "SaveAndClose";
                  
                  }
            }
            }
                  else{
                      if((Long)ADFUtils.evaluateEL("#{bindings.PreDispatchRejectionVO1Iterator.estimatedRowCount}")<1){
                      ADFUtils.showMessage("Specify Rejection for the rejected quantity.", 0);
                  }
                      if(ErrorResult.equalsIgnoreCase("N"))
                         {
                         ADFUtils.showMessage(" Reject Qty must be equal to the actual rejected qty.", 0);
                        }
                  }
              }
             else{
                  ADFUtils.showMessage("Please Enter the result.", 0);
                 }
        return null;
  }

    public void setInspStatBinding(RichSelectOneChoice inspStatBinding) {
        this.inspStatBinding = inspStatBinding;
    }

    public RichSelectOneChoice getInspStatBinding() {
        return inspStatBinding;
    }

    public void setCheckByBinding(RichInputText checkByBinding) {
        this.checkByBinding = checkByBinding;
    }

    public RichInputText getCheckByBinding() {
        return checkByBinding;
    }

    public void setStatusBinding(RichSelectOneChoice statusBinding) {
        this.statusBinding = statusBinding;
    }

    public RichSelectOneChoice getStatusBinding() {
        return statusBinding;
    }

    public void setPendingButtonBinding(RichButton pendingButtonBinding) {
        this.pendingButtonBinding = pendingButtonBinding;
    }

    public RichButton getPendingButtonBinding() {
        return pendingButtonBinding;
    }

    public void setUnitBinding(RichInputComboboxListOfValues unitBinding) {
        this.unitBinding = unitBinding;
    }

    public RichInputComboboxListOfValues getUnitBinding() {
        return unitBinding;
    }

    public void okPcsValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
            BigDecimal a=new BigDecimal(0);
            BigDecimal actqty = (BigDecimal) object;
            System.out.println("accept qty is===>"+actqty);
            if(object != null && actqty.compareTo(a)==-1){
            System.out.println("iside compare"+actqty);
                
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Accept Qty. must be greater than or equal to 0.", null));
            }
            else{
                
                
            }

    }

    public void setOkBinding(RichInputText okBinding) {
        this.okBinding = okBinding;
    }

    public RichInputText getOkBinding() {
        return okBinding;
    }

    public void setUnitCodePartProdLotBinding(RichInputText unitCodePartProdLotBinding) {
        this.unitCodePartProdLotBinding = unitCodePartProdLotBinding;
    }

    public RichInputText getUnitCodePartProdLotBinding() {
        return unitCodePartProdLotBinding;
    }

    public void setItemCodePartProdLotBinding(RichInputText itemCodePartProdLotBinding) {
        this.itemCodePartProdLotBinding = itemCodePartProdLotBinding;
    }

    public RichInputText getItemCodePartProdLotBinding() {
        return itemCodePartProdLotBinding;
    }

    public void setLotNoPartProdLotBinding(RichInputText lotNoPartProdLotBinding) {
        this.lotNoPartProdLotBinding = lotNoPartProdLotBinding;
    }

    public RichInputText getLotNoPartProdLotBinding() {
        return lotNoPartProdLotBinding;
    }

    public void setProdSlipNoPartProdLotBinding(RichInputText prodSlipNoPartProdLotBinding) {
        this.prodSlipNoPartProdLotBinding = prodSlipNoPartProdLotBinding;
    }

    public RichInputText getProdSlipNoPartProdLotBinding() {
        return prodSlipNoPartProdLotBinding;
    }

    public void generateLotNo(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert2").execute();
        ADFUtils.findOperation("generateLotNoPreDispatchAudit").execute();
    }

    public void setMinOberservationTrans(RichInputText minOberservationTrans) {
        this.minOberservationTrans = minOberservationTrans;
    }

    public RichInputText getMinOberservationTrans() {
        return minOberservationTrans;
    }

    public void setMaxObservationTrans(RichInputText maxObservationTrans) {
        this.maxObservationTrans = maxObservationTrans;
    }

    public RichInputText getMaxObservationTrans() {
        return maxObservationTrans;
    }

    public void valueValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if(object != null) {
            BigDecimal minValue = (BigDecimal)getMinOberservationTrans().getValue();
            BigDecimal maxValue = (BigDecimal)getMaxObservationTrans().getValue();
//            BigDecimal value = new BigDecimal(0);
            BigDecimal value = (BigDecimal)object;
            
            if(value != null && value.compareTo(minValue) == -1) {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Value must be greater than or equal to Observation Min.", null));
            }
            if(value != null && value.compareTo(maxValue) == 1) {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Value must be less than or equal to Observation Max.", null));
            }
         }

    }

    public void setValueBinding(RichInputText valueBinding) {
        this.valueBinding = valueBinding;
    }

    public RichInputText getValueBinding() {
        return valueBinding;
    }

    public void valueVCL(ValueChangeEvent vce) {
        System.out.println("INSIDE VCE ####");
        if(vce != null && chkStBinding.getValue().equals("BC")) {
            vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
//            ADFUtils.findOperation("getValueValidation").execute();
            BigDecimal minValue = (BigDecimal)minOberservationTrans.getValue();
            System.out.println("Min Value #### " + minOberservationTrans.getValue());
            BigDecimal maxValue = (BigDecimal)maxObservationTrans.getValue();
            System.out.println("Max Value #### " + maxObservationTrans.getValue());
            BigDecimal value = (BigDecimal)vce.getNewValue();
            
            if(value != null && value.compareTo(minValue) == -1) {
                ADFUtils.showMessage("Value must be greater than or equal to Observation Min.", 0);
            }
            if(value != null && value.compareTo(maxValue) == 1) {
                ADFUtils.showMessage("Value must be less than or equal to Observation Max.", 0);
            }
        }
    }

    public void setRejectionDetailButtonBinding(RichButton rejectionDetailButtonBinding) {
        this.rejectionDetailButtonBinding = rejectionDetailButtonBinding;
    }

    public RichButton getRejectionDetailButtonBinding() {
        return rejectionDetailButtonBinding;
    }

    public void setDefectCodeBinding(RichInputComboboxListOfValues defectCodeBinding) {
        this.defectCodeBinding = defectCodeBinding;
    }

    public RichInputComboboxListOfValues getDefectCodeBinding() {
        return defectCodeBinding;
    }

    public void setDefectQtyBinding(RichInputText defectQtyBinding) {
        this.defectQtyBinding = defectQtyBinding;
    }

    public RichInputText getDefectQtyBinding() {
        return defectQtyBinding;
    }

    public void valueVCL1(ValueChangeEvent vce) {
        System.out.println("INSIDE valueVCL1");
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce.getNewValue() != null  && chkStBinding.getValue().equals("BC")) {
//            BigDecimal minTrans = (BigDecimal) minOberservationTrans.getValue();
//            System.out.println("Min Trans " + minOberservationTrans.getValue());
//            
//            BigDecimal maxTrans = (BigDecimal) maxObservationTrans.getValue();
//            System.out.println("Max Trans " + maxObservationTrans.getValue());
//            BigDecimal zero = new BigDecimal(0);
//
//            BigDecimal value = (BigDecimal)vce.getNewValue();
            ADFUtils.findOperation("rejectionValuesInPredispatchAudit").execute();
//            ADFUtils.findOperation("changeConclusionInFGRequisitionForInspection").execute();
//            OperationBinding opr1 = ADFUtils.findOperation("rejectionValuesInPredispatchAudit");
//            opr1.getParamsMap().put("status","O");
//            opr1.execute();
            
//            if((value.compareTo(minTrans)==1 || value.compareTo(minTrans)==0) && (value.compareTo(maxTrans)==-1 
//               || value.compareTo(maxTrans)==0)) {
//                
//                OperationBinding opr1 = ADFUtils.findOperation("rejectionValuesInPredispatchAudit");
//                opr1.getParamsMap().put("status","O");
//                opr1.execute();
//                System.out.println("INSIDE IF ####");
////                acceptQtyHdBinding.setValue(totChkBinding.getValue());
////                rejQtyBinding.setValue(zero);
////                okBinding.setValue(totChkBinding.getValue());
////                statusBinding1.setValue("O");
//            }
//            else
//            {
//                System.out.println("INSIDE ELSE ####");
//                OperationBinding opr2 = ADFUtils.findOperation("rejectionValuesInPredispatchAudit");
//                opr2.getParamsMap().put("status","N");
//                opr2.execute();
////                acceptQtyHdBinding.setValue(zero);
////                rejQtyBinding.setValue(totChkBinding.getValue());
////                rejQtyDtlBinding.setValue(totChkBinding.getValue());
////                okBinding.setValue(zero);
////                statusBinding1.setValue("N");
////                ADFUtils.findOperation("getValueCheckCalculate").execute();
////                if(statusBinding1.getValue() == "N") {
////                    rejQtyBinding.setValue(valueBinding);
////                }
//            }
           // AdfFacesContext.getCurrentInstance().addPartialTarget(dtlBinding);
        }
    }

    public void setStatusBinding1(RichInputComboboxListOfValues statusBinding1) {
        this.statusBinding1 = statusBinding1;
    }

    public RichInputComboboxListOfValues getStatusBinding1() {
        return statusBinding1;
    }

    public void custTypeVCL(ValueChangeEvent vce) {
        System.out.println("INSIDE CUSTOMER TYPE VCL");
        
        if(vce != null) {
            vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
            System.out.println("INSIDE VCE ####");
            
            String custType1 = "General";
            
            System.out.println("Customer Type Value is " + customerTypeBinding.getValue());
            String custType = (String)customerTypeBinding.getValue();
            
            if(vce.getNewValue().equals("G")) {
                System.out.println("INSIDE IF BLOCK ####");
                customerCodeBinding.setDisabled(true);
                customerCodeBinding.setValue(custType1);
            }
            else {
                System.out.println("INSIDE ELSE BLOCK ####");
                customerCodeBinding.setDisabled(false);
            }
            
        }
    }

    public void setCustomerTypeBinding(RichSelectOneChoice customerTypeBinding) {
        this.customerTypeBinding = customerTypeBinding;
    }

    public RichSelectOneChoice getCustomerTypeBinding() {
        return customerTypeBinding;
    }

    public void setCustomerCodeBinding(RichInputComboboxListOfValues customerCodeBinding) {
        this.customerCodeBinding = customerCodeBinding;
    }

    public RichInputComboboxListOfValues getCustomerCodeBinding() {
        return customerCodeBinding;
    }

    public void setCalculativeFlagBinding(RichInputText calculativeFlagBinding) {
        this.calculativeFlagBinding = calculativeFlagBinding;
    }

    public RichInputText getCalculativeFlagBinding() {
        return calculativeFlagBinding;
    }

    public void setApprovedByBinding(RichInputComboboxListOfValues approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputComboboxListOfValues getApprovedByBinding() {
        return approvedByBinding;
    }

    public void editButtonAL(ActionEvent actionEvent) {
        if(approvedByBinding.getValue() != null){                      
            ADFUtils.showMessage("Approved record can not be edited!...", 0);
            ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V");
                  System.out.println("INSIDE editButtonAL #### ");
            if(conclusionBinding.getValue() == null) {
                String val = "COM";
                conclusionBinding.setValue(val);
                    System.out.println("inside if #### " + val);
                  }
        }else{
                cevmodecheck();
            } 
    }

    public void setProductCodeBinding(RichInputComboboxListOfValues productCodeBinding) {
        this.productCodeBinding = productCodeBinding;
    }

    public RichInputComboboxListOfValues getProductCodeBinding() {
        return productCodeBinding;
    }

    public void setNoOfPackingBinding(RichInputText noOfPackingBinding) {
        this.noOfPackingBinding = noOfPackingBinding;
    }

    public RichInputText getNoOfPackingBinding() {
        return noOfPackingBinding;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setQcdNoBinding(RichInputText qcdNoBinding) {
        this.qcdNoBinding = qcdNoBinding;
    }

    public RichInputText getQcdNoBinding() {
        return qcdNoBinding;
    }

    public void setInspectionStatusBinding(RichInputText inspectionStatusBinding) {
        this.inspectionStatusBinding = inspectionStatusBinding;
    }

    public RichInputText getInspectionStatusBinding() {
        return inspectionStatusBinding;
    }

    public void setEntryNoBinding(RichInputText entryNoBinding) {
        this.entryNoBinding = entryNoBinding;
    }

    public RichInputText getEntryNoBinding() {
        return entryNoBinding;
    }

    public void downloadAction(FacesContext facesContext, OutputStream outputStream) throws IOException {
        File filed = new File(pathBinding.getValue().toString());
                               FileInputStream fis;
                               byte[] b;
                               try {
                                   fis = new FileInputStream(filed);

                                   int n;
                                   while ((n = fis.available()) > 0) {
                                       b = new byte[n];
                                       int result = fis.read(b);
                                       outputStream.write(b, 0, b.length);
                                       if (result == -1)
                                           break;
                                   }
                               } catch (Exception e) {
                                   e.printStackTrace();
                               }
                               outputStream.flush();

    }

    public void uploadAction(ValueChangeEvent vce) {
        System.out.println("inside uploadAction #### ");
                    //Get File Object from VC Event
                    UploadedFile fileVal = (UploadedFile) vce.getNewValue();
                    //Upload File to path- Return actual server path
                    String path = ADFUtils.uploadFile(fileVal);
                      System.out.println(fileVal.getContentType());
        //                      System.out.println("Content Type==>>"+fileVal.getContentType()+"Path==>>"+path+"File name==>>"+fileVal.getFilename());
                    //Method to insert data in table to keep track of uploaded files
                    OperationBinding ob = ADFUtils.findOperation("setFileDataForFGRequisition");
                    ob.getParamsMap().put("name", fileVal.getFilename());
                    ob.getParamsMap().put("path", path);
                    ob.getParamsMap().put("contTyp", fileVal.getContentType());
                    ob.getParamsMap().put("SrNo", ADFUtils.evaluateEL("#{bindings.DocAttachRefDtlVO1Iterator.estimatedRowCount+1}"));
                    ob.execute();
                    // Reset inputFile component after upload
                    ResetUtils.reset(vce.getComponent());
    }

    public void deletePopupDLReferenceDocument(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                    {
                    ADFUtils.findOperation("Delete3").execute();
                    //ADFUtils.findOperation("Commit").execute();    //Unable on Search page.
                    System.out.println("Record Delete Successfully");
                   
                        FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                        FacesContext fc = FacesContext.getCurrentInstance();  
                        fc.addMessage(null, Message);  
                   
                    }

              AdfFacesContext.getCurrentInstance().addPartialTarget(referenceDocumentTableBinding);
    }

    public void setReferenceDocumentTableBinding(RichTable referenceDocumentTableBinding) {
        this.referenceDocumentTableBinding = referenceDocumentTableBinding;
    }

    public RichTable getReferenceDocumentTableBinding() {
        return referenceDocumentTableBinding;
    }

    public void setPathBinding(RichInputText pathBinding) {
        this.pathBinding = pathBinding;
    }

    public RichInputText getPathBinding() {
        return pathBinding;
    }

    public void setDocSlNoBinding(RichInputText docSlNoBinding) {
        this.docSlNoBinding = docSlNoBinding;
    }

    public RichInputText getDocSlNoBinding() {
        return docSlNoBinding;
    }

    public void setUnitCodeRefDocBinding(RichInputText unitCodeRefDocBinding) {
        this.unitCodeRefDocBinding = unitCodeRefDocBinding;
    }

    public RichInputText getUnitCodeRefDocBinding() {
        return unitCodeRefDocBinding;
    }

    public void setRefDocNoBinding(RichInputText refDocNoBinding) {
        this.refDocNoBinding = refDocNoBinding;
    }

    public RichInputText getRefDocNoBinding() {
        return refDocNoBinding;
    }

    public void setRefDocTypeBinding(RichInputText refDocTypeBinding) {
        this.refDocTypeBinding = refDocTypeBinding;
    }

    public RichInputText getRefDocTypeBinding() {
        return refDocTypeBinding;
    }

    public void setDocFileNameBinding(RichInputText docFileNameBinding) {
        this.docFileNameBinding = docFileNameBinding;
    }

    public RichInputText getDocFileNameBinding() {
        return docFileNameBinding;
    }

    public void setRefDocDateBinding(RichInputDate refDocDateBinding) {
        this.refDocDateBinding = refDocDateBinding;
    }

    public RichInputDate getRefDocDateBinding() {
        return refDocDateBinding;
    }

    public void setRemarksRefDocBinding(RichInputText remarksRefDocBinding) {
        this.remarksRefDocBinding = remarksRefDocBinding;
    }

    public RichInputText getRemarksRefDocBinding() {
        return remarksRefDocBinding;
    }

    public void approvedByVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce!=null)
        {
            
                if(approvedByBinding.getValue()!=null)
                {
                    getValueBinding().setDisabled(true);
                }else{
                    getValueBinding().setDisabled(false);
                }
        }
//        AdfFacesContext.getCurrentInstance().addPartialTarget(showDetailTabBinding);
//        AdfFacesContext.getCurrentInstance().addPartialTarget(dtlBinding);
        AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
    }

    public void setShowDetailTabBinding(RichShowDetailItem showDetailTabBinding) {
        this.showDetailTabBinding = showDetailTabBinding;
    }

    public RichShowDetailItem getShowDetailTabBinding() {
        return showDetailTabBinding;
    }

    public void setConclusionBinding(RichSelectOneChoice conclusionBinding) {
        this.conclusionBinding = conclusionBinding;
    }

    public RichSelectOneChoice getConclusionBinding() {
        return conclusionBinding;
    }

    public void setDownloadLinkBinding(RichCommandLink downloadLinkBinding) {
        this.downloadLinkBinding = downloadLinkBinding;
    }

    public RichCommandLink getDownloadLinkBinding() {
        return downloadLinkBinding;
    }

    public void setReferenceDocTabBinding(RichShowDetailItem referenceDocTabBinding) {
        this.referenceDocTabBinding = referenceDocTabBinding;
    }

    public RichShowDetailItem getReferenceDocTabBinding() {
        return referenceDocTabBinding;
    }

    public void acceptQtyVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(totChkBinding.getValue()!=null && vce.getNewValue()!=null)
        {
            BigDecimal BatchQty=(BigDecimal)totChkBinding.getValue();
            BigDecimal AcceptQty=(BigDecimal)vce.getNewValue();
            if(AcceptQty.compareTo(BatchQty)==-1)
            {
                rejQtyDtlBinding.setValue(BatchQty.subtract(AcceptQty));
                rejQtyBinding.setValue(BatchQty.subtract(AcceptQty));
                acceptQtyHdBinding.setValue(AcceptQty);
                statusBinding1.setValue("Not Ok");
//                chkSatus.setValue("N");
               ADFUtils.findOperation("changeStatusinDtl").execute();
              AdfFacesContext.getCurrentInstance().addPartialTarget(rejQtyBinding);
                AdfFacesContext.getCurrentInstance().addPartialTarget(acceptQtyHdBinding);
                AdfFacesContext.getCurrentInstance().addPartialTarget(statusBinding1);
            }
            else if(AcceptQty.compareTo(BatchQty)==0)
            {
                rejQtyDtlBinding.setValue(BatchQty.subtract(AcceptQty));
                rejQtyBinding.setValue(BatchQty.subtract(AcceptQty));
                acceptQtyHdBinding.setValue(AcceptQty);
                statusBinding1.setValue("Ok");
//                chkSatus.setValue("N");
               ADFUtils.findOperation("changeStatusinDtl").execute();
              AdfFacesContext.getCurrentInstance().addPartialTarget(rejQtyBinding);
                AdfFacesContext.getCurrentInstance().addPartialTarget(acceptQtyHdBinding);
                AdfFacesContext.getCurrentInstance().addPartialTarget(statusBinding1);
            }
        }
    }

    public void valValidator(FacesContext facesContext, UIComponent uIComponent, Object ob) {
        if(ob!=null && chkStBinding.getValue().equals("WC"))
        {
            BigDecimal val=(BigDecimal)ob;
            if(minOberservationTrans.getValue()!=null && maxObservationTrans.getValue()!=null)
            {
            BigDecimal upperVal=(BigDecimal)minOberservationTrans.getValue();
            BigDecimal lowerVal=(BigDecimal)maxObservationTrans.getValue();
            if(val.compareTo(lowerVal)==1 || val.compareTo(upperVal)==-1)
            {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Value must be greater than Upper and lower than Lower.", null));
            }
            }
        }

    }

    public void setChkSatus(RichSelectOneChoice chkSatus) {
        this.chkSatus = chkSatus;
    }

    public RichSelectOneChoice getChkSatus() {
        return chkSatus;
    }
}
