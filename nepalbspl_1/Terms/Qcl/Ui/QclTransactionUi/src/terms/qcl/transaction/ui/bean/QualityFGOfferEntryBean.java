package terms.qcl.transaction.ui.bean;

import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;

public class QualityFGOfferEntryBean {
    private String editAction="V";
    private RichTable fgOfferTableBinding;


    public QualityFGOfferEntryBean() {
    }

    public void populateButtonAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("populateDataInQualityFGOfferEntry").execute();
        
    }

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public void saveButtonAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("generateEntryNoInQualityFGOfferEntry").execute();
        ADFUtils.findOperation("Commit").execute();
        ADFUtils.findOperation("filterAfterNoGenerateInQualityFGOfferEntry").execute();
     
        //filterAfterNoGenerateInQualityFGOfferEntry
        AdfFacesContext.getCurrentInstance().addPartialTarget(fgOfferTableBinding);
    }

    public void setFgOfferTableBinding(RichTable fgOfferTableBinding) {
        this.fgOfferTableBinding = fgOfferTableBinding;
    }

    public RichTable getFgOfferTableBinding() {
        return fgOfferTableBinding;
    }
}
