package terms.qcl.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputText;

import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;

import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.binding.OperationBinding;

public class ReqForReInspectionBean {
    private RichInputText uomBinding;
    private RichInputComboboxListOfValues unitBinding;
    private RichInputText reqNoBinding;
    private RichInputComboboxListOfValues reqByBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichOutputText bindingOutputText;
    private RichSelectOneChoice statusBinding;
    private RichInputText manufNameBinding;

    public ReqForReInspectionBean() {
    }

    public void setUomBinding(RichInputText uomBinding) {
        this.uomBinding = uomBinding;
    }

    public RichInputText getUomBinding() {
        return uomBinding;
    }

    public void saveAL(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedBy("ReqForReInspectionVO1Iterator");
        OperationBinding op = ADFUtils.findOperation("getReqReInspectionNo");
        Object rst = op.execute();
            if (rst != null && rst != "") 
            {
                if (op.getErrors().isEmpty()) {
                    ADFUtils.findOperation("Commit").execute();
                    ADFUtils.showMessage("Record Saved Successfully. " + "Requisition No. is:: "+ rst, 2);
                }
            }
            
            if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ") 
            {
                if (op.getErrors().isEmpty()) {
                    ADFUtils.findOperation("Commit").execute();
                    ADFUtils.showMessage(" Record Updated Successfully.", 2); 
              }
            }
    }

    public void setUnitBinding(RichInputComboboxListOfValues unitBinding) {
        this.unitBinding = unitBinding;
    }

    public RichInputComboboxListOfValues getUnitBinding() {
        return unitBinding;
    }

    public void setReqNoBinding(RichInputText reqNoBinding) {
        this.reqNoBinding = reqNoBinding;
    }

    public RichInputText getReqNoBinding() {
        return reqNoBinding;
    }

    public void setReqByBinding(RichInputComboboxListOfValues reqByBinding) {
        this.reqByBinding = reqByBinding;
    }

    public RichInputComboboxListOfValues getReqByBinding() {
        return reqByBinding;
    }
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                try {
                    Method method1 =
                        component.getClass().getMethod("setDisabled", boolean.class);
                    if (method1 != null) {
                        method1.invoke(component, valueComponent);
                    }
                } catch (NoSuchMethodException e) {
                    try {
                        Method method =
                            component.getClass().getMethod("setReadOnly", boolean.class);
                        if (method != null) {
                            method.invoke(component, valueComponent);
                        }
                    } catch (Exception e1) {
                        // e.printStackTrace();//silently eat this exception.
                    }


                } catch (Exception e) {
                    // e.printStackTrace();//silently eat this exception.
                }
                List<UIComponent> childComponents = component.getChildren();
                for (UIComponent comp : childComponents) {
                    makeComponentHierarchyReadOnly(comp, valueComponent);
                }
            }
    public void cevModeDisableComponent(String mode) {
        if(mode.equals("C")){
          uomBinding.setDisabled(true);  
          reqNoBinding.setDisabled(true);
          unitBinding.setDisabled(true);
          reqByBinding.setDisabled(true);
          statusBinding.setDisabled(true);
          manufNameBinding.setDisabled(true);
        }
        if(mode.equals("E")){
            uomBinding.setDisabled(true);  
            reqNoBinding.setDisabled(true);
            unitBinding.setDisabled(true);
            reqByBinding.setDisabled(true);  
            statusBinding.setDisabled(true);
            manufNameBinding.setDisabled(true);
        }
        if(mode.equals("V")){
            
        }
        }
    
    private void cevmodecheck(){
            if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
               
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
                cevModeDisableComponent("V");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
                
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("C");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
                
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("E");
            }
        }

    public void EditButtonAL(ActionEvent actionEvent) {
        System.out.println("edit button al"+"mode at edit mode==>>"+ADFUtils.resolveExpression("#{pageFlowScope.mode}"));
        
        cevmodecheck();
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setStatusBinding(RichSelectOneChoice statusBinding) {
        this.statusBinding = statusBinding;
    }

    public RichSelectOneChoice getStatusBinding() {
        return statusBinding;
    }

    public void setManufNameBinding(RichInputText manufNameBinding) {
        this.manufNameBinding = manufNameBinding;
    }

    public RichInputText getManufNameBinding() {
        return manufNameBinding;
    }
}
