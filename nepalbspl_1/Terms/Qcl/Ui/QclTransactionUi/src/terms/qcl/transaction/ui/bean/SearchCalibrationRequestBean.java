package terms.qcl.transaction.ui.bean;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class SearchCalibrationRequestBean {
    private RichTable tableBinding;

    public SearchCalibrationRequestBean() {
    }

    public void deleteDialogListenerDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                 {
                 ADFUtils.findOperation("Delete").execute();
                 OperationBinding op = (OperationBinding) ADFUtils.findOperation("Commit");
                 Object rst = op.execute();
                 System.out.println("Record Delete Successfully");
                 

                     if(op.getErrors().isEmpty())
                     {
                     FacesMessage Message = new FacesMessage("Record Deleted Successfully");   
                     Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                     FacesContext fc = FacesContext.getCurrentInstance();   
                     fc.addMessage(null, Message);
                     }
                     else
                     {
                        FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                        Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                        FacesContext fc = FacesContext.getCurrentInstance();
                        fc.addMessage(null, Message);
                      }
                 
                 }

             AdfFacesContext.getCurrentInstance().addPartialTarget(tableBinding);
    }

    public void setTableBinding(RichTable tableBinding) {
        this.tableBinding = tableBinding;
    }

    public RichTable getTableBinding() {
        return tableBinding;
    }
}
