package terms.qcl.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.el.ELContext;
import javax.el.ExpressionFactory;

import javax.el.ValueExpression;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectBooleanCheckbox;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

public class createIncomingDeviationApprovalBean {
    private RichTable createIncomingDeviationApprovalTableBinding;
    private RichButton headerEditBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton detailcreateBinding;
    private RichButton detaildeleteBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText unitNameBinding;
    private RichOutputText bindingOutputText;
    private RichInputText itemCodeBinding;
    private RichInputText requestNoBinding;
    private RichInputText srvNoBinding;
    private RichInputComboboxListOfValues preparedByBinding;
    private RichInputText responsibilityDetailNameBinding;
    private RichInputDate srvDtBinding;
    private RichSelectOneChoice devTypBInding;
    private RichInputDate reqDtBinding;
    private RichSelectBooleanCheckbox stCancelBinding;
    private RichInputComboboxListOfValues approveByBinding;

    public createIncomingDeviationApprovalBean() {
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                    {
                    ADFUtils.findOperation("Delete").execute();
                    //ADFUtils.findOperation("Commit").execute();    //Unable on Search page.
                    System.out.println("Record Delete Successfully");
                   
                        FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                        FacesContext fc = FacesContext.getCurrentInstance();  
                        fc.addMessage(null, Message);  
                   
                    }

                AdfFacesContext.getCurrentInstance().addPartialTarget(createIncomingDeviationApprovalTableBinding);
    }

    public void setCreateIncomingDeviationApprovalTableBinding(RichTable createIncomingDeviationApprovalTableBinding) {
        this.createIncomingDeviationApprovalTableBinding = createIncomingDeviationApprovalTableBinding;
    }

    public RichTable getCreateIncomingDeviationApprovalTableBinding() {
        return createIncomingDeviationApprovalTableBinding;
    }

    public void recordSaveIncomingDeviationApproval(ActionEvent actionEvent) {
 //       ADFUtils.setLastUpdatedBy("IncomingDeviationApprovalDetailVO1Iterator");
        String param=resolvEl("#{pageFlowScope.mode=='E'}");
                System.out.println("Mode is ====> "+param);
                if(param.equalsIgnoreCase("true"))
                {
                   FacesMessage Message = new FacesMessage("Record Update Successfully");  
                   Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                   FacesContext fc = FacesContext.getCurrentInstance();  
                   fc.addMessage(null, Message);     
                   
                }
                else
                {
                    FacesMessage Message = new FacesMessage("Record Save Successfully");  
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                    FacesContext fc = FacesContext.getCurrentInstance();  
                    fc.addMessage(null, Message);     
                }
                
    }

    private String resolvEl(String data) {
        FacesContext fc = FacesContext.getCurrentInstance();
                             Application app = fc.getApplication();
                             ExpressionFactory elFactory = app.getExpressionFactory();
                             ELContext elContext = fc.getELContext();
                             ValueExpression valueExp = elFactory.createValueExpression(elContext, data, Object.class);
                             String Message=valueExp.getValue(elContext).toString();
                             return Message;
    }
    
    private void cevmodecheck(){
                if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
                    makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
                    cevModeDisableComponent("V");
                }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
                    makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                    cevModeDisableComponent("C");
                }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
                    makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                    cevModeDisableComponent("E");
                }
            }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                    try {
                        Method method1 =
                            component.getClass().getMethod("setDisabled", boolean.class);
                        if (method1 != null) {
                            method1.invoke(component, valueComponent);
                        }
                    } catch (NoSuchMethodException e) {
                        try {
                            Method method =
                                component.getClass().getMethod("setReadOnly", boolean.class);
                            if (method != null) {
                                method.invoke(component, valueComponent);
                            }
                        } catch (Exception e1) {
                            // e.printStackTrace();//silently eat this exception.
                        }


                    } catch (Exception e) {
                        // e.printStackTrace();//silently eat this exception.
                    }
                    List<UIComponent> childComponents = component.getChildren();
                    for (UIComponent comp : childComponents) {
                        makeComponentHierarchyReadOnly(comp, valueComponent);
                    }
                }
    
    public void cevModeDisableComponent(String mode) {
                if (mode.equals("E")) 
                {
                        if(stCancelBinding.getValue().equals(true))
                             {
                                 System.out.println("in the status chk loop****");
                                 System.out.println("cancel status is****");
                                     getStCancelBinding().setDisabled(true);
                                 }
                    getUnitCodeBinding().setDisabled(true);
                    getUnitNameBinding().setDisabled(true);
                    getItemCodeBinding().setDisabled(true);
                    getHeaderEditBinding().setDisabled(true);
                    getRequestNoBinding().setDisabled(true);
                    getSrvNoBinding().setDisabled(true);
                    getPreparedByBinding().setDisabled(true);
                    getResponsibilityDetailNameBinding().setDisabled(true);
                    getSrvDtBinding().setDisabled(true);
                    getReqDtBinding().setDisabled(true);
                    getDevTypBInding().setDisabled(true);
                    getApproveByBinding().setDisabled(true);
                       
                } else if (mode.equals("C"))
                {
                    getUnitCodeBinding().setDisabled(true);
                    getUnitNameBinding().setDisabled(true);
                    getItemCodeBinding().setDisabled(true);
                    getDetailcreateBinding().setDisabled(false);
                    getDetaildeleteBinding().setDisabled(false);
                    getHeaderEditBinding().setDisabled(true);
                    getRequestNoBinding().setDisabled(true);
                    getSrvNoBinding().setDisabled(true);
                    getPreparedByBinding().setDisabled(true);
                    getResponsibilityDetailNameBinding().setDisabled(true);
                   
                } else if (mode.equals("V")) {
                    getDetailcreateBinding().setDisabled(true);
                    getDetaildeleteBinding().setDisabled(true);
                }
                
            }


    public void editButtonAL(ActionEvent actionEvent) {
        OperationBinding opp = ADFUtils.findOperation("getCancelDevApproval");
        Object rst = opp.execute();
        System.out.println("Result is****of methdo execute" + rst);
        if (rst != null && rst.equals("N"))
        {
            System.out.println("in the if block whn row is presnt");
            ADFUtils.showMessage("You can not cancel this record,For this document supplementary SRV  is generated. ",
                                 2);
            ADFUtils.setEL("#{pageFlowScope.mode}", "V");
            cevmodecheck();

        }
        System.out.println("st is***"+stCancelBinding.getValue());

        cevmodecheck();
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setDetailcreateBinding(RichButton detailcreateBinding) {
        this.detailcreateBinding = detailcreateBinding;
    }

    public RichButton getDetailcreateBinding() {
        return detailcreateBinding;
    }

    public void setDetaildeleteBinding(RichButton detaildeleteBinding) {
        this.detaildeleteBinding = detaildeleteBinding;
    }

    public RichButton getDetaildeleteBinding() {
        return detaildeleteBinding;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setUnitNameBinding(RichInputText unitNameBinding) {
        this.unitNameBinding = unitNameBinding;
    }

    public RichInputText getUnitNameBinding() {
        return unitNameBinding;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setItemCodeBinding(RichInputText itemCodeBinding) {
        this.itemCodeBinding = itemCodeBinding;
    }

    public RichInputText getItemCodeBinding() {
        return itemCodeBinding;
    }

    public void setRequestNoBinding(RichInputText requestNoBinding) {
        this.requestNoBinding = requestNoBinding;
    }

    public RichInputText getRequestNoBinding() {
        return requestNoBinding;
    }

    public void setSrvNoBinding(RichInputText srvNoBinding) {
        this.srvNoBinding = srvNoBinding;
    }

    public RichInputText getSrvNoBinding() {
        return srvNoBinding;
    }

    public void setPreparedByBinding(RichInputComboboxListOfValues preparedByBinding) {
        this.preparedByBinding = preparedByBinding;
    }

    public RichInputComboboxListOfValues getPreparedByBinding() {
        return preparedByBinding;
    }

    public void setResponsibilityDetailNameBinding(RichInputText responsibilityDetailNameBinding) {
        this.responsibilityDetailNameBinding = responsibilityDetailNameBinding;
    }

    public RichInputText getResponsibilityDetailNameBinding() {
        return responsibilityDetailNameBinding;
    }

    public void setSrvDtBinding(RichInputDate srvDtBinding) {
        this.srvDtBinding = srvDtBinding;
    }

    public RichInputDate getSrvDtBinding() {
        return srvDtBinding;
    }

    public void setDevTypBInding(RichSelectOneChoice devTypBInding) {
        this.devTypBInding = devTypBInding;
    }

    public RichSelectOneChoice getDevTypBInding() {
        return devTypBInding;
    }

    public void setReqDtBinding(RichInputDate reqDtBinding) {
        this.reqDtBinding = reqDtBinding;
    }

    public RichInputDate getReqDtBinding() {
        return reqDtBinding;
    }

    public void qcApproveByVCL(ValueChangeEvent vce) {
//        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());            
//                            OperationBinding ob = ADFUtils.findOperation("checkApprovalStatus");
//                            ob.getParamsMap().put("formNm", "INCDEV");
//                            ob.getParamsMap().put("authoLim", "AP");
//                            ob.getParamsMap().put("empcd", vce.getNewValue().toString());
//                            ob.execute();
//                            System.out.println(" EMP CD"+vce.getNewValue().toString());
//                            if((ob.getResult() !=null && ob.getResult().equals("Y")))
//                            {
//                                Row row=(Row)ADFUtils.evaluateEL("#{bindings.IncomingDeviationApprovalHeaderVO1Iterator.currentRow}");
//                                row.setAttribute("QcApprovedBy", null);
//                                ADFUtils.showMessage("You have no authority to approve this Incoming Deviation.",0);
//                            }
    }

    public void setStCancelBinding(RichSelectBooleanCheckbox stCancelBinding) {
        this.stCancelBinding = stCancelBinding;
    }

    public RichSelectBooleanCheckbox getStCancelBinding() {
        return stCancelBinding;
    }

    public void setApproveByBinding(RichInputComboboxListOfValues approveByBinding) {
        this.approveByBinding = approveByBinding;
    }

    public RichInputComboboxListOfValues getApproveByBinding() {
        return approveByBinding;
    }
}
