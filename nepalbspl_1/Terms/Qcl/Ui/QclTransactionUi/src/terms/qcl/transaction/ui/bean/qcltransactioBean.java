package terms.qcl.transaction.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.application.ViewHandler;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import javax.faces.validator.ValidatorException;

import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;

import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;

import oracle.adf.view.rich.event.DialogEvent;

import terms.qcl.transaction.model.view.IncomingInspDetailVORowImpl;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichPopup;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;

import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.adf.view.rich.remote.result.ErrorResult;

import oracle.adf.view.rich.util.ResetUtils;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

import org.apache.myfaces.trinidad.model.UploadedFile;

import terms.qcl.transaction.model.view.IncomingInspHeaderVORowImpl;


public class qcltransactioBean {
    private RichInputComboboxListOfValues bindSrvNoHd;
    private RichInputText bindVendrCdHd;

    private RichInputText bindPoNo;
    private RichInputText bindlotQtyHd;
    private RichInputText bindSampleSize;
    private RichInputText bindQtyForQc;
    private RichInputText bindLotQty;
    private RichInputText bindAcccptdQty;
    private RichInputText bindRejectQtyHD;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputComboboxListOfValues srvNoBinding;
    private RichInputComboboxListOfValues inspByBinding;
    private RichSelectOneChoice inspStatusBinding;
    private RichOutputText bindingOutputText;


    private RichButton pendingInspButtonBinding;
    private RichButton lotDtlButtonBinding;
    private RichButton rejectionBrkupButtonBinding;
    private RichInputDate inspDateBinding;
    private RichInputText slNoBinding;
    private RichInputText checkDescBinding;
    private RichInputText inspToolBinding;
    private RichInputText toolDescBinding;
    private RichInputText rejQtyBinding;
    private RichColumn chkStatusBinding;
    private RichTable inspDetailTableBinding;
    private RichButton headerEditBinding;
    private RichInputText procCodeBinding;
    private RichSelectOneChoice inspLevelBinding;
    private RichInputText procSeqBinding;
    private RichInputText itemCdBinding;
    private RichInputDate srvDateBinding;
    private RichTable rejectionBrkupTableBinding;
    private RichInputText inspectionNoBinding;
    private RichInputText acceptQtyBinding;
    private RichInputText poNoBinding;
    private RichInputText qprNoBinding;
    private RichInputText convBinding;
    private RichInputText rejBrkupBinding;
    private RichButton getDataButtonBinding;
    private RichInputDate expDateBinding;
    private RichInputText itemCodeBinding;
    private RichInputText lotNoBinding;
    private RichInputText qtyBinding;
    private RichInputText poNumberBinding;
    private RichInputDate lotDtBinding;
    private RichButton getCreateButtonBinding;
    private RichButton getDeleteButtonBinding;
    private RichInputComboboxListOfValues defectCdBinding;
    private RichInputText rejectQtyBinding;
    private RichInputText okNotOkBinding;
    private RichInputText inspItemBinding;
    private RichInputText upperLimitBinding;
    private RichInputText lowerLimitBinding;
    private RichInputText uomBinding;
    private RichInputText calculativeFlagBinding;
    private RichInputText inspectionTypeBinding;
    private RichInputText minValueBinding;
    private RichInputComboboxListOfValues statusCheckBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichSelectOneChoice qualityTypeBinding;
    private RichInputText qcdNoBinding;
    private RichInputText manufactrNameBinding;
    private RichInputText batchNoBinding;
    private RichInputComboboxListOfValues approvedByBinding;
    private RichPopup pendingPopUpBinding;
    private RichInputText qcReceiptQtyBinding;
    private RichInputText qtyQCBinding;
    private RichInputText prmCodeBinding;
    private RichInputText vehicleNoBinding;
    private RichInputText vehicleTypeBinding;
    private RichInputText challanNoBinding;
    private RichInputText invoiceNoBinding;
    private RichSelectOneChoice conclusionBinding;

    public qcltransactioBean() {
    }
    public void setBindAcccptdQty(RichInputText bindAcccptdQty) {
        this.bindAcccptdQty = bindAcccptdQty;
    }

    public RichInputText getBindAcccptdQty() {
        return bindAcccptdQty;
    }

    public RichInputComboboxListOfValues getBindSrvNoHd() {
        return bindSrvNoHd;
    }

    public void setBindVendrCdHd(RichInputText bindVendrCdHd) {
        this.bindVendrCdHd = bindVendrCdHd;
    }

    public RichInputText getBindVendrCdHd() {
        return bindVendrCdHd;
    }

    public void setBindPoNo(RichInputText bindPoNo) {
        this.bindPoNo = bindPoNo;
    }

    public RichInputText getBindPoNo() {
        return bindPoNo;
    }

    public void setBindlotQtyHd(RichInputText bindlotQtyHd) {
        this.bindlotQtyHd = bindlotQtyHd;
    }

    public RichInputText getBindlotQtyHd() {
        return bindlotQtyHd;
    }

    public void setBindSampleSize(RichInputText bindSampleSize) {
        this.bindSampleSize = bindSampleSize;
    }

    public RichInputText getBindSampleSize() {
        return bindSampleSize;
    }

    public void setBindLotQty(RichInputText bindLotQty) {
        this.bindLotQty = bindLotQty;
    }

    public RichInputText getBindLotQty() {
        return bindLotQty;
    }
    public void setBindQtyForQc(RichInputText bindQtyForQc) {
        this.bindQtyForQc = bindQtyForQc;
    }

    public RichInputText getBindQtyForQc() {
        return bindQtyForQc;
    }
    
    public void SaveIncmngHdrAL(ActionEvent actionEvent) 
    {
        //ADFUtils.setLastUpdatedBy("IncomingInspHeaderVO1Iterator");
        String status="",status1="";
        Row rr =(Row) ADFUtils.evaluateEL("#{bindings.IncomingInspHeaderVO1Iterator.currentRow}");
        Row r =(Row) ADFUtils.evaluateEL("#{bindings.IncomingRejectBreakupVO1Iterator.currentRow}");
        BigDecimal rejectQty1= (BigDecimal) (bindRejectQtyHD.getValue()!=null?bindRejectQtyHD.getValue():new BigDecimal(0) ) ;
        System.out.println("reject"+rejectQty1);
        if(r!=null)
        {
                status1=(String)r.getAttribute("DefectCode");
        }
        if(rejectQty1.compareTo(new BigDecimal(0))==1 && status1.isEmpty())
        {
            ADFUtils.showMessage("Specify Rejection for the rejected quantity.", 0);
        }
        else{
         String stat = errorOnSave();  
            if(stat.equalsIgnoreCase("Y"))
        {
        if(rr.getAttribute("InspStat")!=null)
        {
            status= (String) rr.getAttribute("InspStat");
            OperationBinding op1 = ADFUtils.findOperation("setInspectionStatus");
            op1.execute();
            System.out.println("Result After Function Call: "+op1.getResult());
            if(op1.getResult()!=null)
            {
                if(status.equalsIgnoreCase(op1.getResult().toString()) || (status.equals("P") && op1.getResult().toString().equals("R")))
                {
                    BigDecimal a = new BigDecimal(0);
                    BigDecimal acceptQty = new BigDecimal(0);
                    BigDecimal rejectQty = new BigDecimal(0);
                    acceptQty = (BigDecimal)bindAcccptdQty.getValue();
                    acceptQty= (BigDecimal) (bindAcccptdQty.getValue()!=null?bindAcccptdQty.getValue():new BigDecimal(0) ) ;
                    rejectQty= (BigDecimal) (bindRejectQtyHD.getValue()!=null?bindRejectQtyHD.getValue():new BigDecimal(0) ) ;
                    a=acceptQty.add(rejectQty);
                    System.out.println(" The Sum Value is =====:"+a);
                    
                    if(a.compareTo(new BigDecimal(0))==1)
                    {
                        OperationBinding op = ADFUtils.findOperation("getGeneratedIncmgNo");
                        Object rst = op.execute();
                        
                        if (rst != null && rst != "") {
                        //&& op1.getResult().toString().equalsIgnoreCase("S"))
                        if (op.getErrors().isEmpty()){
                            ADFUtils.findOperation("Commit").execute();
                            ADFUtils.showMessage("Record Saved Successfully" + "Inspection no is::" + rst, 2);
                        }
                        }
                        if ((rst.toString() == null || rst.toString() == "" || rst.toString() == " ")) {
                        // && (op1.getResult() != null && op1.getResult().toString().equalsIgnoreCase("E"))
                        if (op.getErrors().isEmpty()) {
                            ADFUtils.findOperation("Commit").execute();
                            ADFUtils.showMessage(" Record Updated Successfully.", 2);
                        }
                        }
                    }else{
                    ADFUtils.showMessage("Please Enter the result.", 0);
                    }
                }
                else if((op1.getResult().toString()).equalsIgnoreCase("A")){
             //   ADFUtils.showMessage("Please Accept in Inspection Status.", 0);
                FacesMessage message = new FacesMessage("Please Accept in Inspection Status.");
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(inspStatusBinding.getClientId(), message);
                }
//                else if((op1.getResult().toString()).equalsIgnoreCase("P")){
//                ADFUtils.showMessage("Please Partial Accept in Inspection Status.", 0);
//                }
                else if((op1.getResult().toString()).equalsIgnoreCase("R")){
               // ADFUtils.showMessage("Either Select Reject or Partial Accept in Inspection Status.", 0);
                FacesMessage message = new FacesMessage("Either Select Reject or Partial Accept.");
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(inspStatusBinding.getClientId(), message);
                }
            }
        }
        }else{
            ADFUtils.showMessage("Reject Qty must be equal to the actual rejected qty.", 0);
        }
      
    }
    }

    public void pendngInspctnAL(ActionEvent actionEvent) {
        if(unitCodeBinding.getValue()!=null){
            ADFUtils.findOperation("exePendngInspectn").execute();
            RichPopup.PopupHints hints = new RichPopup.PopupHints();
            getPendingPopUpBinding().show(hints); 
        }else{
            ADFUtils.showMessage("Unit Code must be required.",0); 
        }
        
        
    }

    public void inspLevelVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce != null && !vce.getNewValue().equals("WOC")) {

            getBindSampleSize().setValue(new BigDecimal(0));

        } else {
            getBindSampleSize().setValue(new BigDecimal(5));

        }
    }


    /*     public void lotQtyVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce.getNewValue() != null) {
            //bindAcccptdQty.setValue(vce.getNewValue().toString());
            calculatedData();

            ADFUtils.findOperation("checkStatusValuesIQC").execute();
            //calculatedlotSize();
            ADFUtils.findOperation("sumOfAllQty").execute();
            BigDecimal lotQty = (BigDecimal) bindLotQty.getValue();
            BigDecimal acceptQty = (BigDecimal) vce.getNewValue();
            BigDecimal val = new BigDecimal(0);
            if (acceptQty == null || acceptQty.equals(val)) {
                inspStatusBinding.setValue("R");
                System.out.println("INSP STATUS R");
                System.out.println("INSP STATUS" + inspStatusBinding.getValue());
                AdfFacesContext.getCurrentInstance().addPartialTarget(inspStatusBinding);
            } else if (lotQty.compareTo(acceptQty) == 1) {
                inspStatusBinding.setValue("P");
                System.out.println("INSP STATUS P");
                System.out.println("INSP STATUS" + inspStatusBinding.getValue());
                AdfFacesContext.getCurrentInstance().addPartialTarget(inspStatusBinding);
            } else if (lotQty.compareTo(acceptQty) == 0) {
                inspStatusBinding.setValue("A");
                System.out.println("INSP STATUS A");
                System.out.println("INSP STATUS" + inspStatusBinding.getValue());
                AdfFacesContext.getCurrentInstance().addPartialTarget(inspStatusBinding);
            }

        }
    } */

    /*     public void calculatedData() {
        DCIteratorBinding dciter = (DCIteratorBinding) ADFUtils.findIterator("IncomingInspDetailVO1Iterator");
        dciter.executeQuery();

        IncomingInspDetailVORowImpl incmInspDetal = (IncomingInspDetailVORowImpl) dciter.getCurrentRow();
        if (incmInspDetal != null && incmInspDetal.getLotQty() != null && incmInspDetal.getAcceptQty() != null) {
            incmInspDetal.setRejQty(incmInspDetal.getLotQty().subtract(incmInspDetal.getAcceptQty()));
            //bindRejectQtyHD.setValue(incmInspDetal.getLotQty().subtract(incmInspDetal.getAcceptQty()));
        }
    } */

    public void sampleValueVCE(ValueChangeEvent vce) {
        if (vce != null) {
            vce.getComponent().processUpdates(FacesContext.getCurrentInstance());

            BigDecimal def1 = new BigDecimal(0);

            BigDecimal one = new BigDecimal(1);

            Object samp2 = vce.getNewValue();
            BigDecimal rej_val_chk = new BigDecimal(samp2.toString());
            BigDecimal samp_size = new BigDecimal(getBindSampleSize().getValue().toString());
            int rest;
            rest = rej_val_chk.compareTo(def1);
            System.out.println("comapre result for detail check" + rest);

            if (rest == 1) {
                int rest1;
                rest1 = samp_size.compareTo(one);
                if (rest1 == 0 || rest == 1) {

                }
                if (rest1 == -1) {
                    FacesMessage message = new FacesMessage("Check Sample Size");
                    message.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext context = FacesContext.getCurrentInstance();
                    context.addMessage(bindSampleSize.getClientId(), message);
                    AdfFacesContext.getCurrentInstance().addPartialTarget(getBindSampleSize());
                }
            }
        }
    }

    public void sampleValue2VCE(ValueChangeEvent vce) {
        if (vce != null) {
            vce.getComponent().processUpdates(FacesContext.getCurrentInstance());

            BigDecimal def1 = new BigDecimal(0);

            BigDecimal two = new BigDecimal(2);

            Object samp2 = vce.getNewValue();
            BigDecimal rej_val_chk = new BigDecimal(samp2.toString());
            BigDecimal samp_size = new BigDecimal(getBindSampleSize().getValue().toString());
            int rest;
            rest = rej_val_chk.compareTo(def1);
            System.out.println("comapre result for detail check" + rest);

            if (rest == 1) {
                int rest1;
                rest1 = samp_size.compareTo(two);
                if (rest1 == 0 || rest == 1) {


                }
                if (rest1 == -1) {
                    FacesMessage message = new FacesMessage("Check Sample Size");
                    message.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext context = FacesContext.getCurrentInstance();
                    context.addMessage(bindSampleSize.getClientId(), message);
                    AdfFacesContext.getCurrentInstance().addPartialTarget(getBindSampleSize());
                }
            }


        }
    }


    public void sampe3VCE(ValueChangeEvent vce) {
        if (vce != null) {
            vce.getComponent().processUpdates(FacesContext.getCurrentInstance());

            BigDecimal def1 = new BigDecimal(0);

            BigDecimal three = new BigDecimal(3);

            Object samp2 = vce.getNewValue();
            BigDecimal rej_val_chk = new BigDecimal(samp2.toString());
            BigDecimal samp_size = new BigDecimal(getBindSampleSize().getValue().toString());
            int rest;
            rest = rej_val_chk.compareTo(def1);
            System.out.println("comapre result for detail check" + rest);

            if (rest == 1) {
                int rest1;
                rest1 = samp_size.compareTo(three);
                if (rest1 == 0 || rest == 1) {


                }
                if (rest1 == -1) {
                    FacesMessage message = new FacesMessage("Check Sample Size");
                    message.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext context = FacesContext.getCurrentInstance();
                    context.addMessage(bindSampleSize.getClientId(), message);
                    AdfFacesContext.getCurrentInstance().addPartialTarget(getBindSampleSize());
                }
            }


        }
    }

    public void samp4VCE(ValueChangeEvent vce) {
        if (vce != null) {
            vce.getComponent().processUpdates(FacesContext.getCurrentInstance());

            BigDecimal def1 = new BigDecimal(0);

            BigDecimal four = new BigDecimal(4);

            Object samp2 = vce.getNewValue();
            BigDecimal rej_val_chk = new BigDecimal(samp2.toString());
            BigDecimal samp_size = new BigDecimal(getBindSampleSize().getValue().toString());
            int rest;
            rest = rej_val_chk.compareTo(def1);
            System.out.println("comapre result for detail check" + rest);

            if (rest == 1) {
                int rest1;
                rest1 = samp_size.compareTo(four);
                if (rest1 == 0 || rest == 1) {


                }
                if (rest1 == -1) {
                    FacesMessage message = new FacesMessage("Check Sample Size");
                    message.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext context = FacesContext.getCurrentInstance();
                    context.addMessage(bindSampleSize.getClientId(), message);
                    AdfFacesContext.getCurrentInstance().addPartialTarget(getBindSampleSize());
                }
            }


        }
    }

    public void samp5VCE(ValueChangeEvent vce) {
        if (vce != null) {
            vce.getComponent().processUpdates(FacesContext.getCurrentInstance());

            BigDecimal def1 = new BigDecimal(0);

            BigDecimal five = new BigDecimal(5);

            Object samp2 = vce.getNewValue();
            BigDecimal rej_val_chk = new BigDecimal(samp2.toString());
            BigDecimal samp_size = new BigDecimal(getBindSampleSize().getValue().toString());
            int rest;
            rest = rej_val_chk.compareTo(def1);
            System.out.println("comapre result for detail check" + rest);

            if (rest == 1) {
                int rest1;
                rest1 = samp_size.compareTo(five);
                if (rest1 == 0 || rest == 1) {


                }
                if (rest1 == -1) {
                    FacesMessage message = new FacesMessage("Check Sample Size");
                    message.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext context = FacesContext.getCurrentInstance();
                    context.addMessage(bindSampleSize.getClientId(), message);
                    AdfFacesContext.getCurrentInstance().addPartialTarget(getBindSampleSize());
                }
            }


        }
    }

    public void samp6VCE(ValueChangeEvent vce) {
        if (vce != null) {
            vce.getComponent().processUpdates(FacesContext.getCurrentInstance());

            BigDecimal def1 = new BigDecimal(0);

            BigDecimal six = new BigDecimal(6);

            Object samp2 = vce.getNewValue();
            BigDecimal rej_val_chk = new BigDecimal(samp2.toString());
            BigDecimal samp_size = new BigDecimal(getBindSampleSize().getValue().toString());
            int rest;
            rest = rej_val_chk.compareTo(def1);
            System.out.println("comapre result for detail check" + rest);

            if (rest == 1) {
                int rest1;
                rest1 = samp_size.compareTo(six);
                if (rest1 == 0 || rest == 1) {


                }
                if (rest1 == -1) {
                    FacesMessage message = new FacesMessage("Check Sample Size");
                    message.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext context = FacesContext.getCurrentInstance();
                    context.addMessage(bindSampleSize.getClientId(), message);
                    AdfFacesContext.getCurrentInstance().addPartialTarget(getBindSampleSize());
                }
            }


        }
    }

    public void samp7VCE(ValueChangeEvent vce) {
        if (vce != null) {
            vce.getComponent().processUpdates(FacesContext.getCurrentInstance());

            BigDecimal def1 = new BigDecimal(0);
            BigDecimal seven = new BigDecimal(7);

            Object samp2 = vce.getNewValue();
            BigDecimal rej_val_chk = new BigDecimal(samp2.toString());
            BigDecimal samp_size = new BigDecimal(getBindSampleSize().getValue().toString());
            int rest;
            rest = rej_val_chk.compareTo(def1);
            System.out.println("comapre result for detail check" + rest);

            if (rest == 1) {
                int rest1;
                rest1 = samp_size.compareTo(seven);
                if (rest1 == 0 || rest == 1) {
                }
                if (rest1 == -1) {
                    FacesMessage message = new FacesMessage("Check Sample Size");
                    message.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext context = FacesContext.getCurrentInstance();
                    context.addMessage(bindSampleSize.getClientId(), message);
                    AdfFacesContext.getCurrentInstance().addPartialTarget(getBindSampleSize());
                }
            }
        }
    }

    public void samp8VCE(ValueChangeEvent vce) {
        if (vce != null) {
            vce.getComponent().processUpdates(FacesContext.getCurrentInstance());

            BigDecimal def1 = new BigDecimal(0);
            BigDecimal eight = new BigDecimal(8);

            Object samp2 = vce.getNewValue();
            BigDecimal rej_val_chk = new BigDecimal(samp2.toString());
            BigDecimal samp_size = new BigDecimal(getBindSampleSize().getValue().toString());
            int rest;
            rest = rej_val_chk.compareTo(def1);
            System.out.println("comapre result for detail check" + rest);
            if (rest == 1) {
                int rest1;
                rest1 = samp_size.compareTo(eight);
                if (rest1 == 0 || rest == 1) {
                }
                if (rest1 == -1) {
                    FacesMessage message = new FacesMessage("Check Sample Size");
                    message.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext context = FacesContext.getCurrentInstance();
                    context.addMessage(bindSampleSize.getClientId(), message);
                    AdfFacesContext.getCurrentInstance().addPartialTarget(getBindSampleSize());
                }
            }
        }
    }

    public void samp9VCE(ValueChangeEvent vce) {
        if (vce != null) {
            vce.getComponent().processUpdates(FacesContext.getCurrentInstance());

            BigDecimal def1 = new BigDecimal(0);
            BigDecimal nine = new BigDecimal(9);

            Object samp2 = vce.getNewValue();
            BigDecimal rej_val_chk = new BigDecimal(samp2.toString());
            BigDecimal samp_size = new BigDecimal(getBindSampleSize().getValue().toString());
            int rest;
            rest = rej_val_chk.compareTo(def1);
            System.out.println("comapre result for detail check" + rest);
            if (rest == 1) {
                int rest1;
                rest1 = samp_size.compareTo(nine);
                if (rest1 == 0 || rest == 1) {
                } else {
                    FacesMessage message = new FacesMessage("Check Sample Size");
                    message.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext context = FacesContext.getCurrentInstance();
                    context.addMessage(bindSampleSize.getClientId(), message);
                    AdfFacesContext.getCurrentInstance().addPartialTarget(getBindSampleSize());
                }
            }
        }
    }

    public void samp10VCE(ValueChangeEvent vce) {
        if (vce != null) {
            vce.getComponent().processUpdates(FacesContext.getCurrentInstance());

            BigDecimal def1 = new BigDecimal(0);
            BigDecimal ten = new BigDecimal(10);

            Object samp2 = vce.getNewValue();
            BigDecimal rej_val_chk = new BigDecimal(samp2.toString());
            BigDecimal samp_size = new BigDecimal(getBindSampleSize().getValue().toString());
            int rest;
            rest = rej_val_chk.compareTo(def1);
            System.out.println("comapre result for detail check" + rest);

            if (rest == 1) {
                int rest1;
                rest1 = samp_size.compareTo(ten);
                if (rest1 == 0 || rest == 1) {
                }
                if (rest1 == -1) {
                    FacesMessage message = new FacesMessage("Check Sample Size");
                    message.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext context = FacesContext.getCurrentInstance();
                    context.addMessage(bindSampleSize.getClientId(), message);
                    AdfFacesContext.getCurrentInstance().addPartialTarget(getBindSampleSize());
                }
            }
        }
    }

    /*     public void calculatedlotSize() {
        //            DCIteratorBinding dciter =
        //                        (DCIteratorBinding) ADFUtils.findIterator("IncomingInspHeaderVO1Iterator");
        //                        dciter.executeQuery();
        //
        //             IncomingInspHeaderVORowImpl incmInsphdr =
        //                        (IncomingInspHeaderVORowImpl )dciter.getCurrentRow();
        //             if(incmInsphdr!=null && incmInsphdr.getLotSize()!=null && incmInsphdr.getAcceptedQty()!=null){
        //
        //                 incmInsphdr.setRejectQty(incmInsphdr.getLotSize().subtract(incmInsphdr.getAcceptedQty()));
        if (getBindlotQtyHd().getValue() != null && getBindAcccptdQty().getValue() != null) {
            BigDecimal lotQty = new BigDecimal(getBindlotQtyHd().getValue().toString());
            BigDecimal Accptd = new BigDecimal(getBindAcccptdQty().getValue().toString());

            BigDecimal sub_rej_val = lotQty.subtract(Accptd);
            bindRejectQtyHD.setValue(sub_rej_val);
        }
    } */

    public void ccpyQtyValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {

        BigDecimal a = new BigDecimal(0);
        if (object != null && getBindLotQty().getValue() != null) {
            BigDecimal Accptd = new BigDecimal(object.toString());
            BigDecimal lotQty = new BigDecimal(getBindLotQty().getValue().toString());

            int res;

            res = Accptd.compareTo(lotQty);
            {
                if (res == 0 || res == -1) {

                } else {
                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                  "Accept Qty. Can Not Be More Then Lot Qty.", null));

                }

            }

        }


        System.out.println("object:" + object + "   Bindinf Value=>" + getAcceptQtyBinding().getValue());
        BigDecimal actqty = (BigDecimal) object;
        System.out.println("accept qty is===>" + actqty);
        if (object != null && actqty.compareTo(a) == -1) {
            System.out.println("iside compare" + actqty);

            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                          "Accept Qty. must be greater than 0.", null));
        } else {


        }
    }


    public void LotDetailAL(ActionEvent actionEvent) {
        OperationBinding op = ADFUtils.findOperation("FetchingLotDetails");
   //     op.getParamsMap().put("SrvNo", getBindSrvNo().getValue());
    //    op.getParamsMap().put("itemCd", get.getValue())
      //  op.getParamsMap().put("Pono", getBindPoNo().getValue());

        Object rst = op.execute();
    }


    public void setBindRejectQtyHD(RichInputText bindRejectQtyHD) {
        this.bindRejectQtyHD = bindRejectQtyHD;
    }

    public RichInputText getBindRejectQtyHD() {
        return bindRejectQtyHD;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setSrvNoBinding(RichInputComboboxListOfValues srvNoBinding) {
        this.srvNoBinding = srvNoBinding;
    }

    public RichInputComboboxListOfValues getSrvNoBinding() {
        return srvNoBinding;
    }

    public void setInspByBinding(RichInputComboboxListOfValues inspByBinding) {
        this.inspByBinding = inspByBinding;
    }

    public RichInputComboboxListOfValues getInspByBinding() {
        return inspByBinding;
    }

    public void setInspStatusBinding(RichSelectOneChoice inspStatusBinding) {
        this.inspStatusBinding = inspStatusBinding;
    }

    public RichSelectOneChoice getInspStatusBinding() {
        return inspStatusBinding;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }


    public void EditButtonAL(ActionEvent actionEvent) {
        if(inspectionNoBinding.getValue() != null) {
            if(approvedByBinding.getValue()!=null){
                ADFUtils.showMessage("Approved Record can not be modified.", 0);
                ADFUtils.setEL("#{pageFlowScope.mode}", "V");
            }
//            else if(conclusionBinding.getValue() == null || approvedByBinding.getValue()==null) {
//                System.out.println("INSIDE ELSE IF ##### ");
//                String val = "COM";
//                
//                conclusionBinding.setValue(val);
//                cevmodecheck();
//            }
            else{
                cevmodecheck();
            }
        }
        
        
    }

    public void cevModeDisableComponent(String mode) {

        if (mode.equals("C")) {

            getConclusionBinding().setDisabled(true);
            getInspItemBinding().setDisabled(true);
            getUpperLimitBinding().setDisabled(true);
            getLowerLimitBinding().setDisabled(true);
            getUomBinding().setDisabled(true);
            getCalculativeFlagBinding().setDisabled(true);
            getInspectionTypeBinding().setDisabled(true);
            getSlNoBinding().setDisabled(true);
            getManufactrNameBinding().setDisabled(true);
            getApprovedByBinding().setDisabled(true);
            getQprNoBinding().setDisabled(true);
            getConvBinding().setDisabled(true);
            getBindlotQtyHd().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
            getInspectionNoBinding().setDisabled(true);
            getSlNoBinding().setDisabled(true);
            getCheckDescBinding().setDisabled(true);
            getQcdNoBinding().setDisabled(true);
            
            getRejQtyBinding().setDisabled(true);
            getBindLotQty().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            getPrmCodeBinding().setDisabled(true);
            getInspByBinding().setDisabled(true);
            getInspDateBinding().setDisabled(true);
            getSrvNoBinding().setDisabled(true);
            getSrvDateBinding().setDisabled(true);
            getBindVendrCdHd().setDisabled(true);
            getProcCodeBinding().setDisabled(true);
            getProcSeqBinding().setDisabled(true);
            getBindRejectQtyHD().setDisabled(true);
            getBindAcccptdQty().setDisabled(true);
            getBindLotQty().setDisabled(true);
            getPoNoBinding().setDisabled(true);
            getInspStatusBinding().setDisabled(true);
            getStatusCheckBinding().setDisabled(true);
            getOkNotOkBinding().setDisabled(true);
            
            getInvoiceNoBinding().setDisabled(true);
            getChallanNoBinding().setDisabled(true);
            getVehicleTypeBinding().setDisabled(true);
            getVehicleNoBinding().setDisabled(true);
//            if(inspLevelBinding.getValue()!=null && inspLevelBinding.getValue().equals("WOC"))
//            {
//                getAcceptQtyBinding().setDisabled(true);
//            }

        }
        if (mode.equals("E")) 
        {
            getPrmCodeBinding().setDisabled(true);
            getQualityTypeBinding().setDisabled(true);
            getQcdNoBinding().setDisabled(true);
            getInspItemBinding().setDisabled(true);
            getUpperLimitBinding().setDisabled(true);
            getLowerLimitBinding().setDisabled(true);
            getUomBinding().setDisabled(true);
            getCalculativeFlagBinding().setDisabled(true);
            getInspectionTypeBinding().setDisabled(true);
            getAcceptQtyBinding().setDisabled(true);
            getSlNoBinding().setDisabled(true);
            getRejQtyBinding().setDisabled(true);
            getManufactrNameBinding().setDisabled(true);
            getConclusionBinding().setDisabled(true);
            
         //   getMinValueBinding().setDisabled(true);
            getCheckDescBinding().setDisabled(true);
            getQprNoBinding().setDisabled(true);
            getConvBinding().setDisabled(true);
            getBindlotQtyHd().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);

            getInspByBinding().setDisabled(true);
            getSrvNoBinding().setDisabled(true);
            getProcCodeBinding().setDisabled(true);
            getBindSampleSize().setDisabled(true);
            getInspLevelBinding().setDisabled(true);
            getBindRejectQtyHD().setDisabled(true);
            getBindAcccptdQty().setDisabled(true);
            getBindLotQty().setDisabled(true);
            getProcSeqBinding().setDisabled(true);
            getBindVendrCdHd().setDisabled(true);
            getInspDateBinding().setDisabled(true);
            getSrvDateBinding().setDisabled(true);
            getInspectionNoBinding().setDisabled(true);
            getPoNoBinding().setDisabled(true);
            getInspStatusBinding().setDisabled(false);
            
            getStatusCheckBinding().setDisabled(true);
            getOkNotOkBinding().setDisabled(true);
            
            getInvoiceNoBinding().setDisabled(true);
            getChallanNoBinding().setDisabled(true);
            getVehicleTypeBinding().setDisabled(true);
            getVehicleNoBinding().setDisabled(true);
        }

        if (mode.equals("V")) {
            getLotDtlButtonBinding().setDisabled(false);
            getRejectionBrkupButtonBinding().setDisabled(false);
            getPendingInspButtonBinding().setDisabled(true);
            getGetDataButtonBinding().setDisabled(true);
            getExpDateBinding().setDisabled(true);
            getItemCodeBinding().setDisabled(true);
            getLotNoBinding().setDisabled(true);
            getLotDtBinding().setDisabled(true);
            getQtyBinding().setDisabled(true);
            getPoNumberBinding().setDisabled(true);
            getGetCreateButtonBinding().setDisabled(true);
            getGetDeleteButtonBinding().setDisabled(true);
            getDefectCdBinding().setDisabled(true);
            getRejectQtyBinding().setDisabled(true);
            // getDetailDeleteBinding().setDisabled(true);
        }


    }

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }

    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {

            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {

            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {

            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }

    public void setPendingInspButtonBinding(RichButton pendingInspButtonBinding) {
        this.pendingInspButtonBinding = pendingInspButtonBinding;
    }

    public RichButton getPendingInspButtonBinding() {
        return pendingInspButtonBinding;
    }

    public void setLotDtlButtonBinding(RichButton lotDtlButtonBinding) {
        this.lotDtlButtonBinding = lotDtlButtonBinding;
    }

    public RichButton getLotDtlButtonBinding() {
        return lotDtlButtonBinding;
    }

    public void setRejectionBrkupButtonBinding(RichButton rejectionBrkupButtonBinding) {
        this.rejectionBrkupButtonBinding = rejectionBrkupButtonBinding;
    }

    public RichButton getRejectionBrkupButtonBinding() {
        return rejectionBrkupButtonBinding;
    }


    public void setInspDateBinding(RichInputDate inspDateBinding) {
        this.inspDateBinding = inspDateBinding;
    }

    public RichInputDate getInspDateBinding() {
        return inspDateBinding;
    }

    public void setSlNoBinding(RichInputText slNoBinding) {
        this.slNoBinding = slNoBinding;
    }

    public RichInputText getSlNoBinding() {
        return slNoBinding;
    }

    public void setCheckDescBinding(RichInputText checkDescBinding) {
        this.checkDescBinding = checkDescBinding;
    }

    public RichInputText getCheckDescBinding() {
        return checkDescBinding;
    }
    public void setInspToolBinding(RichInputText inspToolBinding) {
        this.inspToolBinding = inspToolBinding;
    }

    public RichInputText getInspToolBinding() {
        return inspToolBinding;
    }

    public void setToolDescBinding(RichInputText toolDescBinding) {
        this.toolDescBinding = toolDescBinding;
    }

    public RichInputText getToolDescBinding() {
        return toolDescBinding;
    }

    public void setRejQtyBinding(RichInputText rejQtyBinding) {
        this.rejQtyBinding = rejQtyBinding;
    }

    public RichInputText getRejQtyBinding() {
        return rejQtyBinding;
    }

    public void setChkStatusBinding(RichColumn chkStatusBinding) {
        this.chkStatusBinding = chkStatusBinding;
    }

    public RichColumn getChkStatusBinding() {
        return chkStatusBinding;
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
        }
        ADFUtils.findOperation("setStatusInIncomingInspection").execute();
        AdfFacesContext.getCurrentInstance().addPartialTarget(inspDetailTableBinding);
    }

    public void setInspDetailTableBinding(RichTable inspDetailTableBinding) {
        this.inspDetailTableBinding = inspDetailTableBinding;
    }

    public RichTable getInspDetailTableBinding() {
        return inspDetailTableBinding;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setProcCodeBinding(RichInputText procCodeBinding) {
        this.procCodeBinding = procCodeBinding;
    }

    public RichInputText getProcCodeBinding() {
        return procCodeBinding;
    }

    public void setInspLevelBinding(RichSelectOneChoice inspLevelBinding) {
        this.inspLevelBinding = inspLevelBinding;
    }

    public RichSelectOneChoice getInspLevelBinding() {
        return inspLevelBinding;
    }

    public void setProcSeqBinding(RichInputText procSeqBinding) {
        this.procSeqBinding = procSeqBinding;
    }

    public RichInputText getProcSeqBinding() {
        return procSeqBinding;
    }

    public void setItemCdBinding(RichInputText itemCdBinding) {
        this.itemCdBinding = itemCdBinding;
    }

    public RichInputText getItemCdBinding() {
        return itemCdBinding;
    }
    public void setSrvDateBinding(RichInputDate srvDateBinding) {
        this.srvDateBinding = srvDateBinding;
    }

    public RichInputDate getSrvDateBinding() {
        return srvDateBinding;
    }

    public void deletePopupDL1(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete1").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(rejectionBrkupTableBinding);

    }


    public void setRejectionBrkupTableBinding(RichTable rejectionBrkupTableBinding) {
        this.rejectionBrkupTableBinding = rejectionBrkupTableBinding;
    }

    public RichTable getRejectionBrkupTableBinding() {
        return rejectionBrkupTableBinding;
    }

    public void setInspectionNoBinding(RichInputText inspectionNoBinding) {
        this.inspectionNoBinding = inspectionNoBinding;
    }

    public RichInputText getInspectionNoBinding() {
        return inspectionNoBinding;
    }

    public void setAcceptQtyBinding(RichInputText acceptQtyBinding) {
        this.acceptQtyBinding = acceptQtyBinding;
    }

    public RichInputText getAcceptQtyBinding() {
        return acceptQtyBinding;
    }

    public void setPoNoBinding(RichInputText poNoBinding) {
        this.poNoBinding = poNoBinding;
    }

    public RichInputText getPoNoBinding() {
        return poNoBinding;
    }

    public void setQprNoBinding(RichInputText qprNoBinding) {
        this.qprNoBinding = qprNoBinding;
    }

    public RichInputText getQprNoBinding() {
        return qprNoBinding;
    }

    public void setConvBinding(RichInputText convBinding) {
        this.convBinding = convBinding;
    }

    public RichInputText getConvBinding() {
        return convBinding;
    }

    public String SaveandCloseIncmngHdrAL()
    {
        ADFUtils.setLastUpdatedBy("IncomingInspHeaderVO1Iterator");
        String status=null;
        Row rr =(Row) ADFUtils.evaluateEL("#{bindings.IncomingInspHeaderVO1Iterator.currentRow}");
        Row r =(Row) ADFUtils.evaluateEL("#{bindings.IncomingRejectBreakupVO1Iterator.currentRow}");
        BigDecimal rejectQty1= (BigDecimal) (bindRejectQtyHD.getValue()!=null?bindRejectQtyHD.getValue():new BigDecimal(0) ) ;
        //System.out.println("reject"+rejectQty1+"breakup==>"+r.getAttribute("DefectCode"));
        //String stat1 = errorOnSave();  
        //if(rejectQty1!= null && rejectQty1.compareTo(new BigDecimal(0))==1 && r.getAttribute("DefectCode")==null)
        System.out.println("reject"+rejectQty1);
                if(rejectQty1.compareTo(new BigDecimal(0))==1 && r==null)
        {
            ADFUtils.showMessage("Specify Rejection for the rejected quantity.", 0);
        }else    
        {
         String stat = errorOnSave();  
            if(stat.equalsIgnoreCase("Y"))
            {
        if(rr.getAttribute("InspStat")!=null)
        {
            status= (String) rr.getAttribute("InspStat");
            OperationBinding op1 = ADFUtils.findOperation("setInspectionStatus");
            op1.execute();
            System.out.println("Result After Function Call: "+op1.getResult());
            if(op1.getResult()!=null)
            {
                if(status.equalsIgnoreCase(op1.getResult().toString()) || (status.equals("P") && op1.getResult().toString().equals("R")))
                {
                    BigDecimal a = new BigDecimal(0);
                    BigDecimal acceptQty = new BigDecimal(0);
                    BigDecimal rejectQty = new BigDecimal(0);
                    acceptQty = (BigDecimal)bindAcccptdQty.getValue();
                    acceptQty= (BigDecimal) (bindAcccptdQty.getValue()!=null?bindAcccptdQty.getValue():new BigDecimal(0) ) ;
                    rejectQty= (BigDecimal) (bindRejectQtyHD.getValue()!=null?bindRejectQtyHD.getValue():new BigDecimal(0) ) ;
                    a=acceptQty.add(rejectQty);
                    System.out.println(" The Sum Value is =====:"+a);
                    
                    if(a.compareTo(new BigDecimal(0))==1)
                    {
                        OperationBinding op = ADFUtils.findOperation("getGeneratedIncmgNo");
                        Object rst = op.execute();
                        
                        if (rst != null && rst != "") {
                        //&& op1.getResult().toString().equalsIgnoreCase("S"))
                        if (op.getErrors().isEmpty()){
                            ADFUtils.findOperation("Commit").execute();
                            ADFUtils.showMessage("Record Saved Successfully" + "Inspection no is::" + rst, 2);
                            return "SaveAndRollBack";
                        }
                        }
                        if ((rst.toString() == null || rst.toString() == "" || rst.toString() == " ")) {
                        // && (op1.getResult() != null && op1.getResult().toString().equalsIgnoreCase("E"))
                        if (op.getErrors().isEmpty()) {
                            ADFUtils.findOperation("Commit").execute();
                            ADFUtils.showMessage(" Record Updated Successfully.", 2);
                            return "SaveAndRollBack";
                        }
                        }
                    }else{
                    ADFUtils.showMessage("Please Enter the result.", 0);
                        return null;
                    }
                }
                else if((op1.getResult().toString()).equalsIgnoreCase("A")){
             //   ADFUtils.showMessage("Please Accept in Inspection Status.", 0);
                FacesMessage message = new FacesMessage("Please Accept in Inspection Status.");
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(inspStatusBinding.getClientId(), message);
                   // return null;
                }
        //                else if((op1.getResult().toString()).equalsIgnoreCase("P")){
        //                ADFUtils.showMessage("Please Partial Accept in Inspection Status.", 0);
        //                }
                else if((op1.getResult().toString()).equalsIgnoreCase("R")){
               // ADFUtils.showMessage("Either Select Reject or Partial Accept in Inspection Status.", 0);
                FacesMessage message = new FacesMessage("Either Select Reject or Partial Accept.");
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(inspStatusBinding.getClientId(), message);
                    //return null;
                }
            }
        }
        }        else{
            ADFUtils.showMessage("Reject Qty must be equal to the actual rejected qty.", 0);
            return null;
        }
        }
       
        /*         BigDecimal a = new BigDecimal(0);
        BigDecimal acceptQty = new BigDecimal(0);
        BigDecimal rejectQty = new BigDecimal(0);
        acceptQty = (BigDecimal)bindAcccptdQty.getValue();
        acceptQty= (BigDecimal) (bindAcccptdQty.getValue()!=null?bindAcccptdQty.getValue():new BigDecimal(0) ) ;
        rejectQty= (BigDecimal) (bindRejectQtyHD.getValue()!=null?bindRejectQtyHD.getValue():new BigDecimal(0) ) ;
        a=acceptQty.add(rejectQty);
        System.out.println(" The Sum Value is =====:"+a);

        if(a.compareTo(new BigDecimal(0))==1)
        {
//            OperationBinding op1 = ADFUtils.findOperation("getGeneratedQCDNoII");
//            Object rst1 = op1.execute();
//            System.out.println("The Result is------"+op1.getResult());
//            if(op1.getResult() != null && op1.getResult().toString().equalsIgnoreCase("Y"))
//            {
                
                OperationBinding op = ADFUtils.findOperation("getGeneratedIncmgNo");
                Object rst = op.execute();

                if (rst != null && rst != "") {
                    if (op.getErrors().isEmpty()){
                        ADFUtils.findOperation("Commit").execute();
                        ADFUtils.showMessage("Record Saved Successfully" + "Inspection no is::" + rst, 2);
                        return "SaveAndRollBack";
                    }
                }
                if ((rst.toString() == null || rst.toString() == "" || rst.toString() == " ")) {
                    if (op.getErrors().isEmpty()) {
                        ADFUtils.findOperation("Commit").execute();
                        ADFUtils.showMessage(" Record Updated Successfully.", 2);
                        return "SaveAndRollBack";
                    }
                }
//            }
//            else{
//                ADFUtils.showMessage("QCD Number could not be generated.Try Again !!", 0);
//                return null;
//            }
         }else{
            ADFUtils.showMessage("Please Enter the result.", 0);
            return null;
        } */
        return null;
    }

    public void rejQtyBrkUpVCE(ValueChangeEvent vce) {
        if (vce != null) {
            vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
            OperationBinding ob = ADFUtils.findOperation("sumAllRejQtyBrkUp");
            Object op = ob.execute();
            BigDecimal rejbrk = (BigDecimal) ob.getResult();
            BigDecimal rejhd = (BigDecimal) getBindRejectQtyHD().getValue();
            System.out.println("rej brk up qty is--===>>>" + rejbrk + "rej qty i head comppp" + rejhd);
            if (!rejbrk.equals(rejhd) || rejbrk.compareTo(rejhd) == 1) {
                ADFUtils.showMessage(" Reject Qty must be equal to the actual rejected qty.", 0);
            }
        }
    }


    public void setRejBrkupBinding(RichInputText rejBrkupBinding) {
        this.rejBrkupBinding = rejBrkupBinding;
    }

    public RichInputText getRejBrkupBinding() {
        return rejBrkupBinding;
    }

// Null values handled for rejbrk and rejhd. -- Atul Gupta -On : 10-09-2020
    
    public String errorOnSave() {
        OperationBinding ob = ADFUtils.findOperation("sumAllRejQtyBrkUp");
        Object op = ob.execute();
        BigDecimal rejbrk = (BigDecimal) ob.getResult();
        BigDecimal rejhd = (BigDecimal) getBindRejectQtyHD().getValue();
        //System.out.println("rej brk up qty is--===>>>" + rejbrk + "rej qty i head comppp" + rejhd);
        if (rejbrk != null && rejhd != null && rejbrk.compareTo(rejhd) == 0) {
            return "Y";

        } else {
            //ADFUtils.showMessage(" Reject Qty must be equal to the actual rejected qty.", 0);
            return "N";
        }
    }

    public void setGetDataButtonBinding(RichButton getDataButtonBinding) {
        this.getDataButtonBinding = getDataButtonBinding;
    }

    public RichButton getGetDataButtonBinding() {
        return getDataButtonBinding;
    }

    public void setExpDateBinding(RichInputDate expDateBinding) {
        this.expDateBinding = expDateBinding;
    }

    public RichInputDate getExpDateBinding() {
        return expDateBinding;
    }

    public void setItemCodeBinding(RichInputText itemCodeBinding) {
        this.itemCodeBinding = itemCodeBinding;
    }

    public RichInputText getItemCodeBinding() {
        return itemCodeBinding;
    }

    public void setLotNoBinding(RichInputText lotNoBinding) {
        this.lotNoBinding = lotNoBinding;
    }

    public RichInputText getLotNoBinding() {
        return lotNoBinding;
    }

    public void setQtyBinding(RichInputText qtyBinding) {
        this.qtyBinding = qtyBinding;
    }

    public RichInputText getQtyBinding() {
        return qtyBinding;
    }

    public void setPoNumberBinding(RichInputText poNumberBinding) {
        this.poNumberBinding = poNumberBinding;
    }

    public RichInputText getPoNumberBinding() {
        return poNumberBinding;
    }

    public void setLotDtBinding(RichInputDate lotDtBinding) {
        this.lotDtBinding = lotDtBinding;
    }

    public RichInputDate getLotDtBinding() {
        return lotDtBinding;
    }

    public void setGetCreateButtonBinding(RichButton getCreateButtonBinding) {
        this.getCreateButtonBinding = getCreateButtonBinding;
    }

    public RichButton getGetCreateButtonBinding() {
        return getCreateButtonBinding;
    }

    public void setGetDeleteButtonBinding(RichButton getDeleteButtonBinding) {
        this.getDeleteButtonBinding = getDeleteButtonBinding;
    }

    public RichButton getGetDeleteButtonBinding() {
        return getDeleteButtonBinding;
    }

    public void setDefectCdBinding(RichInputComboboxListOfValues defectCdBinding) {
        this.defectCdBinding = defectCdBinding;
    }

    public RichInputComboboxListOfValues getDefectCdBinding() {
        return defectCdBinding;
    }

    public void setRejectQtyBinding(RichInputText rejectQtyBinding) {
        this.rejectQtyBinding = rejectQtyBinding;
    }

    public RichInputText getRejectQtyBinding() {
        return rejectQtyBinding;
    }

    public void setOkNotOkBinding(RichInputText okNotOkBinding) {
        this.okNotOkBinding = okNotOkBinding;
    }

    public RichInputText getOkNotOkBinding() {
        return okNotOkBinding;
    }

    public void setInspItemBinding(RichInputText inspItemBinding) {
        this.inspItemBinding = inspItemBinding;
    }

    public RichInputText getInspItemBinding() {
        return inspItemBinding;
    }

    public void setUpperLimitBinding(RichInputText upperLimitBinding) {
        this.upperLimitBinding = upperLimitBinding;
    }

    public RichInputText getUpperLimitBinding() {
        return upperLimitBinding;
    }

    public void setLowerLimitBinding(RichInputText lowerLimitBinding) {
        this.lowerLimitBinding = lowerLimitBinding;
    }

    public RichInputText getLowerLimitBinding() {
        return lowerLimitBinding;
    }

    public void setUomBinding(RichInputText uomBinding) {
        this.uomBinding = uomBinding;
    }

    public RichInputText getUomBinding() {
        return uomBinding;
    }

    public void setCalculativeFlagBinding(RichInputText calculativeFlagBinding) {
        this.calculativeFlagBinding = calculativeFlagBinding;
    }

    public RichInputText getCalculativeFlagBinding() {
        return calculativeFlagBinding;
    }

    public void setInspectionTypeBinding(RichInputText inspectionTypeBinding) {
        this.inspectionTypeBinding = inspectionTypeBinding;
    }

    public RichInputText getInspectionTypeBinding() {
        return inspectionTypeBinding;
    }

    public void minValueVCE(ValueChangeEvent vce) 
    {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce!= null)
        {
            ADFUtils.findOperation("setStatusInIncomingInspection").execute();
        } 
       // AdfFacesContext.getCurrentInstance().addPartialTarget(okNotOkBinding);
        //AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
        
    }

    public void setMinValueBinding(RichInputText minValueBinding) {
        this.minValueBinding = minValueBinding;
    }

    public RichInputText getMinValueBinding() {
        return minValueBinding;
    }

    public void setStatusCheckBinding(RichInputComboboxListOfValues statusCheckBinding) {
        this.statusCheckBinding = statusCheckBinding;
    }

    public RichInputComboboxListOfValues getStatusCheckBinding() {
        return statusCheckBinding;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setQualityTypeBinding(RichSelectOneChoice qualityTypeBinding) {
        this.qualityTypeBinding = qualityTypeBinding;
    }

    public RichSelectOneChoice getQualityTypeBinding() {
        return qualityTypeBinding;
    }

    public void setQcdNoBinding(RichInputText qcdNoBinding) {
        this.qcdNoBinding = qcdNoBinding;
    }

    public RichInputText getQcdNoBinding() {
        return qcdNoBinding;
    }

    public void setManufactrNameBinding(RichInputText manufactrNameBinding) {
        this.manufactrNameBinding = manufactrNameBinding;
    }

    public RichInputText getManufactrNameBinding() {
        return manufactrNameBinding;
    }

    public void setBatchNoBinding(RichInputText batchNoBinding) {
        this.batchNoBinding = batchNoBinding;
    }

    public RichInputText getBatchNoBinding() {
        return batchNoBinding;
    }

    public void setApprovedByBinding(RichInputComboboxListOfValues approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputComboboxListOfValues getApprovedByBinding() {
        return approvedByBinding;
    }

    public void setPendingPopUpBinding(RichPopup pendingPopUpBinding) {
        this.pendingPopUpBinding = pendingPopUpBinding;
    }

    public RichPopup getPendingPopUpBinding() {
        return pendingPopUpBinding;
    }

    public void inspectionQualityAL(ActionEvent actionEvent) {
        OperationBinding op = ADFUtils.findOperation("sampSizeFetching");
        Object rst = op.execute();
        if (rst != null && rst.equals("I")) {
            ADFUtils.showMessage("No Checkpoints Has  Been Entered For This Part & Process.", 0);
        }
        if (rst != null && rst.equals("B")) {
            ADFUtils.showMessage("Specified sample size has multiple entries in Sample Size Master!!!!", 0);
        }
                  if(rst!=null && rst.equals("A")){
                           ADFUtils.showMessage("Specified sample size has no entries in Sample Size Master!!!!", 0);
                   }
        getPendingPopUpBinding().cancel();
        getQualityTypeBinding().setDisabled(true);
        AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
    }

    public void quantityQCValidator(FacesContext facesContext, UIComponent uIComponent, Object object) 
    {
        if (object != null && qcReceiptQtyBinding.getValue() != null)
        {
            BigDecimal qty_val = new BigDecimal(object.toString());
            BigDecimal rej_qty = new BigDecimal(getQcReceiptQtyBinding().getValue().toString());
            Object rest = qty_val.compareTo(rej_qty) == 1;
//            System.out.println("comapre result ---->>" + rest);
            if (rest.equals(false)) {
            
            }
            if (rest.equals(true)) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                "QC Qty Can Not Be More Then SRV Balance Qty.", null));
            }
         }
    }

    public void setQcReceiptQtyBinding(RichInputText qcReceiptQtyBinding) {
        this.qcReceiptQtyBinding = qcReceiptQtyBinding;
    }

    public RichInputText getQcReceiptQtyBinding() {
        return qcReceiptQtyBinding;
    }

    /*     public void quantityQCVCE(ValueChangeEvent vce) 
    {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce != null) 
        {
            Object qtyval = vce.getNewValue();
            BigDecimal qty_val = new BigDecimal(qtyval.toString());
            BigDecimal rej_qty = new BigDecimal(getQcReceiptQtyBinding().getValue().toString());
            Object rest = qty_val.compareTo(rej_qty) == 1;
            System.out.println("comapre result ---->>" + rest);
            
            if (rest.equals(false)) {
            }
            if (rest.equals(true))
            {
                FacesMessage message = new FacesMessage("QC Qty Can Not Be More Then SRV Balance Qty");
                ADFUtils.showMessage("QC Qty Can Not Be More Then SRV Balance Qty.", 0);
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(qtyQCBinding.getClientId(), message);
            }

         }
    } */

    public void setQtyQCBinding(RichInputText qtyQCBinding) {
        this.qtyQCBinding = qtyQCBinding;
    }

    public RichInputText getQtyQCBinding() {
        return qtyQCBinding;
    }

    public void setPrmCodeBinding(RichInputText prmCodeBinding) {
        this.prmCodeBinding = prmCodeBinding;
    }

    public RichInputText getPrmCodeBinding() {
        return prmCodeBinding;
    }

    public void inspectionStatusVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce != null) 
        {
            Row rr =(Row) ADFUtils.evaluateEL("#{bindings.IncomingInspHeaderVO1Iterator.currentRow}");
            OperationBinding op = ADFUtils.findOperation("setInspectionStatus");
            Object rst = op.execute();
         //   System.out.println("OPRR===status bean" + rst+"====vvvvvall"+op.getResult());
            if (rst != null && rst.equals("R"))
            {
                if(rr.getAttribute("InspStat").equals("P") || rr.getAttribute("InspStat").equals("R")){
                    //rr.setAttribute("InspStat", "P");
                }
                else{
                    rr.setAttribute("InspStat", null);
                    FacesMessage message = new FacesMessage("Either Select Reject or Partial Accept.");
                    message.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext context = FacesContext.getCurrentInstance();
                    context.addMessage(inspStatusBinding.getClientId(), message);
                }
            }
            else if(rst != null && rst.equals("A")){
                if(rr.getAttribute("InspStat").equals("A")){
                   // rr.setAttribute("InspStat", "A");
                }
                else{
                    rr.setAttribute("InspStat", null);
                    FacesMessage message = new FacesMessage("Only Accept must be set.");
                    message.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext context = FacesContext.getCurrentInstance();
                    context.addMessage(inspStatusBinding.getClientId(), message);
                }
            }
        }
    }

    public void approveByVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
//        if(vce!=null){
//            if(approvedByBinding!=null){
//                getInspStatusBinding().setDisabled(true);
//                AdfFacesContext.getCurrentInstance().addPartialTarget(inspStatusBinding);
//            }
//        }
    }

    public void setVehicleNoBinding(RichInputText vehicleNoBinding) {
        this.vehicleNoBinding = vehicleNoBinding;
    }

    public RichInputText getVehicleNoBinding() {
        return vehicleNoBinding;
    }

    public void setVehicleTypeBinding(RichInputText vehicleTypeBinding) {
        this.vehicleTypeBinding = vehicleTypeBinding;
    }

    public RichInputText getVehicleTypeBinding() {
        return vehicleTypeBinding;
    }

    public void setChallanNoBinding(RichInputText challanNoBinding) {
        this.challanNoBinding = challanNoBinding;
    }

    public RichInputText getChallanNoBinding() {
        return challanNoBinding;
    }

    public void setInvoiceNoBinding(RichInputText invoiceNoBinding) {
        this.invoiceNoBinding = invoiceNoBinding;
    }

    public RichInputText getInvoiceNoBinding() {
        return invoiceNoBinding;
    }

    public void setConclusionBinding(RichSelectOneChoice conclusionBinding) {
        this.conclusionBinding = conclusionBinding;
    }

    public RichSelectOneChoice getConclusionBinding() {
        return conclusionBinding;
    }

    public void acceptVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce.getNewValue()!=null && bindLotQty.getValue()!=null)
        {
            BigDecimal BQty=(BigDecimal) bindLotQty.getValue();
            BigDecimal AQty=(BigDecimal) vce.getNewValue();
            rejQtyBinding.setValue(BQty.subtract(AQty));
            AdfFacesContext.getCurrentInstance().addPartialTarget(rejQtyBinding);
            bindRejectQtyHD.setValue(BQty.subtract(AQty));
            AdfFacesContext.getCurrentInstance().addPartialTarget(bindRejectQtyHD);
            bindAcccptdQty.setValue(vce.getNewValue());
            AdfFacesContext.getCurrentInstance().addPartialTarget(bindAcccptdQty);
            if(BQty.subtract(AQty)!=null && BQty.subtract(AQty).compareTo(new BigDecimal(0))==1 && AQty.compareTo(new BigDecimal(0))==1)
            {
                inspStatusBinding.setValue("R");
                AdfFacesContext.getCurrentInstance().addPartialTarget(inspStatusBinding);
            }else if(BQty.subtract(AQty)!=null && BQty.subtract(AQty).compareTo(new BigDecimal(0))==1 && AQty.compareTo(new BigDecimal(0))==0)
            {
                inspStatusBinding.setValue("P");
                AdfFacesContext.getCurrentInstance().addPartialTarget(inspStatusBinding);
            }else if(AQty!=null && AQty.compareTo(new BigDecimal(0))==1 && BQty.subtract(AQty).compareTo(new BigDecimal(0))==0)
            {
                inspStatusBinding.setValue("A");
                AdfFacesContext.getCurrentInstance().addPartialTarget(inspStatusBinding);
            }
        }
    }

    public void rejectionBreakupDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
        String stat = errorOnSave();
        if(stat.equalsIgnoreCase("N")) {
            ADFUtils.showMessage("Reject Qty must be equal to the actual rejected qty.", 0);
        }
        }
    }

    public void uploadAction(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
        System.out.println("in VCl");
                    if (valueChangeEvent != null) {
                        System.out.println("in VCl");
                            //Get File Object from VC Event
                            UploadedFile fileVal = (UploadedFile) valueChangeEvent.getNewValue();
                            System.out.println("File Value==============<>" + fileVal);
                            //Upload File to path- Return actual server path
                            String path = ADFUtils.uploadFile(fileVal);
                            System.out.println(fileVal.getContentType());
                            System.out.println("Content Type==>>" + fileVal.getContentType() + "Path==>>" + path + "File name==>>" +
                                               fileVal.getFilename());
                            //Method to insert data in table to keep track of uploaded files
                            OperationBinding ob = ADFUtils.findOperation("setFileDataPI");
                            ob.getParamsMap().put("name", fileVal.getFilename());
                            ob.getParamsMap().put("path", path);
                            ob.getParamsMap().put("contTyp", fileVal.getContentType());
                            ob.getParamsMap().put("SrNo", ADFUtils.evaluateEL("#{bindings.DocAttachRefDtlVO4Iterator.estimatedRowCount+1}"));
                            ob.execute();
                            // Reset inputFile component after upload
                            ResetUtils.reset(valueChangeEvent.getComponent());
                        }
    }
}
