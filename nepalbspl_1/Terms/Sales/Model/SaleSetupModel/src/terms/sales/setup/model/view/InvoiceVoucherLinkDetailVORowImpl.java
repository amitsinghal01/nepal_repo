package terms.sales.setup.model.view;

import java.sql.Timestamp;

import oracle.jbo.Row;
import oracle.jbo.RowSet;
import oracle.jbo.domain.Number;
import oracle.jbo.server.EntityImpl;
import oracle.jbo.server.ViewObjectImpl;
import oracle.jbo.server.ViewRowImpl;

import terms.sales.setup.model.applicationModule.SaleSetupAMImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Sat Feb 16 15:22:14 IST 2019
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class InvoiceVoucherLinkDetailVORowImpl extends ViewRowImpl {
    public static final int ENTITY_INVOICEVOUCHERLINKDETAILEO = 0;
    public static final int ENTITY_GENERALLEDGERHEADEREO = 1;
    public static final int ENTITY_GENERALLEDGERHEADEREO1 = 2;
    public static final int ENTITY_GENERALLEDGERHEADEREO2 = 3;
    public static final int ENTITY_ITEMMASTEREO = 4;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        CreatedBy,
        CreationDate,
        GlCgst,
        GlIgst,
        GlSgst,
        GstCode,
        InvType,
        InvTypeId,
        InvTypeLineId,
        ItemType,
        LastUpdateDate,
        LastUpdatedBy,
        ObjectVersionNumber,
        StockType,
        UnitCode,
        GlDesc,
        GlId,
        GlDesc1,
        GlId1,
        GlDesc2,
        GlId2,
        ObjectVersionNumber1,
        ObjectVersionNumber2,
        ObjectVersionNumber3,
        ProdCode,
        ItemDescription,
        ItemId,
        ObjectVersionNumber4,
        GlCgstGenLedVVO1,
        GlSgstGenLedVVO1,
        TypeSecControlVO1,
        ItemStockVO1,
        GenledVVO1,
        GenledVVO2,
        GenledVVO3;
        static AttributesEnum[] vals = null;
        ;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int CREATEDBY = AttributesEnum.CreatedBy.index();
    public static final int CREATIONDATE = AttributesEnum.CreationDate.index();
    public static final int GLCGST = AttributesEnum.GlCgst.index();
    public static final int GLIGST = AttributesEnum.GlIgst.index();
    public static final int GLSGST = AttributesEnum.GlSgst.index();
    public static final int GSTCODE = AttributesEnum.GstCode.index();
    public static final int INVTYPE = AttributesEnum.InvType.index();
    public static final int INVTYPEID = AttributesEnum.InvTypeId.index();
    public static final int INVTYPELINEID = AttributesEnum.InvTypeLineId.index();
    public static final int ITEMTYPE = AttributesEnum.ItemType.index();
    public static final int LASTUPDATEDATE = AttributesEnum.LastUpdateDate.index();
    public static final int LASTUPDATEDBY = AttributesEnum.LastUpdatedBy.index();
    public static final int OBJECTVERSIONNUMBER = AttributesEnum.ObjectVersionNumber.index();
    public static final int STOCKTYPE = AttributesEnum.StockType.index();
    public static final int UNITCODE = AttributesEnum.UnitCode.index();
    public static final int GLDESC = AttributesEnum.GlDesc.index();
    public static final int GLID = AttributesEnum.GlId.index();
    public static final int GLDESC1 = AttributesEnum.GlDesc1.index();
    public static final int GLID1 = AttributesEnum.GlId1.index();
    public static final int GLDESC2 = AttributesEnum.GlDesc2.index();
    public static final int GLID2 = AttributesEnum.GlId2.index();
    public static final int OBJECTVERSIONNUMBER1 = AttributesEnum.ObjectVersionNumber1.index();
    public static final int OBJECTVERSIONNUMBER2 = AttributesEnum.ObjectVersionNumber2.index();
    public static final int OBJECTVERSIONNUMBER3 = AttributesEnum.ObjectVersionNumber3.index();
    public static final int PRODCODE = AttributesEnum.ProdCode.index();
    public static final int ITEMDESCRIPTION = AttributesEnum.ItemDescription.index();
    public static final int ITEMID = AttributesEnum.ItemId.index();
    public static final int OBJECTVERSIONNUMBER4 = AttributesEnum.ObjectVersionNumber4.index();
    public static final int GLCGSTGENLEDVVO1 = AttributesEnum.GlCgstGenLedVVO1.index();
    public static final int GLSGSTGENLEDVVO1 = AttributesEnum.GlSgstGenLedVVO1.index();
    public static final int TYPESECCONTROLVO1 = AttributesEnum.TypeSecControlVO1.index();
    public static final int ITEMSTOCKVO1 = AttributesEnum.ItemStockVO1.index();
    public static final int GENLEDVVO1 = AttributesEnum.GenledVVO1.index();
    public static final int GENLEDVVO2 = AttributesEnum.GenledVVO2.index();
    public static final int GENLEDVVO3 = AttributesEnum.GenledVVO3.index();

    /**
     * This is the default constructor (do not remove).
     */
    public InvoiceVoucherLinkDetailVORowImpl() {
    }

    /**
     * Gets InvoiceVoucherLinkDetailEO entity object.
     * @return the InvoiceVoucherLinkDetailEO
     */
    public EntityImpl getInvoiceVoucherLinkDetailEO() {
        return (EntityImpl) getEntity(ENTITY_INVOICEVOUCHERLINKDETAILEO);
    }

    /**
     * Gets GeneralLedgerHeaderEO entity object.
     * @return the GeneralLedgerHeaderEO
     */
    public EntityImpl getGeneralLedgerHeaderEO() {
        return (EntityImpl) getEntity(ENTITY_GENERALLEDGERHEADEREO);
    }

    /**
     * Gets GeneralLedgerHeaderEO1 entity object.
     * @return the GeneralLedgerHeaderEO1
     */
    public EntityImpl getGeneralLedgerHeaderEO1() {
        return (EntityImpl) getEntity(ENTITY_GENERALLEDGERHEADEREO1);
    }

    /**
     * Gets GeneralLedgerHeaderEO2 entity object.
     * @return the GeneralLedgerHeaderEO2
     */
    public EntityImpl getGeneralLedgerHeaderEO2() {
        return (EntityImpl) getEntity(ENTITY_GENERALLEDGERHEADEREO2);
    }

    /**
     * Gets ItemMasterEO entity object.
     * @return the ItemMasterEO
     */
    public EntityImpl getItemMasterEO() {
        return (EntityImpl) getEntity(ENTITY_ITEMMASTEREO);
    }

    /**
     * Gets the attribute value for CREATED_BY using the alias name CreatedBy.
     * @return the CREATED_BY
     */
    public String getCreatedBy() {
        return (String) getAttributeInternal(CREATEDBY);
    }

    /**
     * Gets the attribute value for CREATION_DATE using the alias name CreationDate.
     * @return the CREATION_DATE
     */
    public Timestamp getCreationDate() {
        return (Timestamp) getAttributeInternal(CREATIONDATE);
    }

    /**
     * Gets the attribute value for GL_CGST using the alias name GlCgst.
     * @return the GL_CGST
     */
    public String getGlCgst() {
        return (String) getAttributeInternal(GLCGST);
    }

    /**
     * Sets <code>value</code> as attribute value for GL_CGST using the alias name GlCgst.
     * @param value value to set the GL_CGST
     */
    public void setGlCgst(String value) {
        setAttributeInternal(GLCGST, value);
    }

    /**
     * Gets the attribute value for GL_IGST using the alias name GlIgst.
     * @return the GL_IGST
     */
    public String getGlIgst() {
        return (String) getAttributeInternal(GLIGST);
    }

    /**
     * Sets <code>value</code> as attribute value for GL_IGST using the alias name GlIgst.
     * @param value value to set the GL_IGST
     */
    public void setGlIgst(String value) {
        setAttributeInternal(GLIGST, value);
    }

    /**
     * Gets the attribute value for GL_SGST using the alias name GlSgst.
     * @return the GL_SGST
     */
    public String getGlSgst() {
        return (String) getAttributeInternal(GLSGST);
    }

    /**
     * Sets <code>value</code> as attribute value for GL_SGST using the alias name GlSgst.
     * @param value value to set the GL_SGST
     */
    public void setGlSgst(String value) {
        setAttributeInternal(GLSGST, value);
    }

    /**
     * Gets the attribute value for GST_CODE using the alias name GstCode.
     * @return the GST_CODE
     */
    public String getGstCode() {
        return (String) getAttributeInternal(GSTCODE);
    }

    /**
     * Sets <code>value</code> as attribute value for GST_CODE using the alias name GstCode.
     * @param value value to set the GST_CODE
     */
    public void setGstCode(String value) {
        setAttributeInternal(GSTCODE, value);
    }

    /**
     * Gets the attribute value for INV_TYPE using the alias name InvType.
     * @return the INV_TYPE
     */
    public String getInvType() {
        return (String) getAttributeInternal(INVTYPE);
    }

    /**
     * Sets <code>value</code> as attribute value for INV_TYPE using the alias name InvType.
     * @param value value to set the INV_TYPE
     */
    public void setInvType(String value) {
        setAttributeInternal(INVTYPE, value);
    }

    /**
     * Gets the attribute value for INV_TYPE_ID using the alias name InvTypeId.
     * @return the INV_TYPE_ID
     */
    public Long getInvTypeId() {
        return (Long) getAttributeInternal(INVTYPEID);
    }

    /**
     * Sets <code>value</code> as attribute value for INV_TYPE_ID using the alias name InvTypeId.
     * @param value value to set the INV_TYPE_ID
     */
    public void setInvTypeId(Long value) {
        setAttributeInternal(INVTYPEID, value);
    }

    /**
     * Gets the attribute value for INV_TYPE_LINE_ID using the alias name InvTypeLineId.
     * @return the INV_TYPE_LINE_ID
     */
    public Long getInvTypeLineId() {
        return (Long) getAttributeInternal(INVTYPELINEID);
    }

    /**
     * Sets <code>value</code> as attribute value for INV_TYPE_LINE_ID using the alias name InvTypeLineId.
     * @param value value to set the INV_TYPE_LINE_ID
     */
    public void setInvTypeLineId(Long value) {
        setAttributeInternal(INVTYPELINEID, value);
    }

    /**
     * Gets the attribute value for ITEM_TYPE using the alias name ItemType.
     * @return the ITEM_TYPE
     */
    public String getItemType() {
        return (String) getAttributeInternal(ITEMTYPE);
    }

    /**
     * Sets <code>value</code> as attribute value for ITEM_TYPE using the alias name ItemType.
     * @param value value to set the ITEM_TYPE
     */
    public void setItemType(String value) {
        setAttributeInternal(ITEMTYPE, value);
    }

    /**
     * Gets the attribute value for LAST_UPDATE_DATE using the alias name LastUpdateDate.
     * @return the LAST_UPDATE_DATE
     */
    public Timestamp getLastUpdateDate() {
        return (Timestamp) getAttributeInternal(LASTUPDATEDATE);
    }

    /**
     * Gets the attribute value for LAST_UPDATED_BY using the alias name LastUpdatedBy.
     * @return the LAST_UPDATED_BY
     */
    public String getLastUpdatedBy() {
        return (String) getAttributeInternal(LASTUPDATEDBY);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER, value);
    }

    /**
     * Gets the attribute value for STOCK_TYPE using the alias name StockType.
     * @return the STOCK_TYPE
     */
    public String getStockType() {
        return (String) getAttributeInternal(STOCKTYPE);
    }

    /**
     * Sets <code>value</code> as attribute value for STOCK_TYPE using the alias name StockType.
     * @param value value to set the STOCK_TYPE
     */
    public void setStockType(String value) {
        setAttributeInternal(STOCKTYPE, value);
    }

    /**
     * Gets the attribute value for UNIT_CODE using the alias name UnitCode.
     * @return the UNIT_CODE
     */
    public String getUnitCode() {
        return (String) getAttributeInternal(UNITCODE);
    }

    /**
     * Sets <code>value</code> as attribute value for UNIT_CODE using the alias name UnitCode.
     * @param value value to set the UNIT_CODE
     */
    public void setUnitCode(String value) {
        setAttributeInternal(UNITCODE, value);
    }

    /**
     * Gets the attribute value for GL_DESC using the alias name GlDesc.
     * @return the GL_DESC
     */
    public String getGlDesc() {
        return (String) getAttributeInternal(GLDESC);
    }

    /**
     * Sets <code>value</code> as attribute value for GL_DESC using the alias name GlDesc.
     * @param value value to set the GL_DESC
     */
    public void setGlDesc(String value) {
        setAttributeInternal(GLDESC, value);
    }

    /**
     * Gets the attribute value for GL_ID using the alias name GlId.
     * @return the GL_ID
     */
    public Number getGlId() {
        return (Number) getAttributeInternal(GLID);
    }

    /**
     * Sets <code>value</code> as attribute value for GL_ID using the alias name GlId.
     * @param value value to set the GL_ID
     */
    public void setGlId(Number value) {
        setAttributeInternal(GLID, value);
    }

    /**
     * Gets the attribute value for GL_DESC using the alias name GlDesc1.
     * @return the GL_DESC
     */
    public String getGlDesc1() {
        return (String) getAttributeInternal(GLDESC1);
    }

    /**
     * Sets <code>value</code> as attribute value for GL_DESC using the alias name GlDesc1.
     * @param value value to set the GL_DESC
     */
    public void setGlDesc1(String value) {
        setAttributeInternal(GLDESC1, value);
    }

    /**
     * Gets the attribute value for GL_ID using the alias name GlId1.
     * @return the GL_ID
     */
    public Number getGlId1() {
        return (Number) getAttributeInternal(GLID1);
    }

    /**
     * Sets <code>value</code> as attribute value for GL_ID using the alias name GlId1.
     * @param value value to set the GL_ID
     */
    public void setGlId1(Number value) {
        setAttributeInternal(GLID1, value);
    }

    /**
     * Gets the attribute value for GL_DESC using the alias name GlDesc2.
     * @return the GL_DESC
     */
    public String getGlDesc2() {
        return (String) getAttributeInternal(GLDESC2);
    }

    /**
     * Sets <code>value</code> as attribute value for GL_DESC using the alias name GlDesc2.
     * @param value value to set the GL_DESC
     */
    public void setGlDesc2(String value) {
        setAttributeInternal(GLDESC2, value);
    }

    /**
     * Gets the attribute value for GL_ID using the alias name GlId2.
     * @return the GL_ID
     */
    public Number getGlId2() {
        return (Number) getAttributeInternal(GLID2);
    }

    /**
     * Sets <code>value</code> as attribute value for GL_ID using the alias name GlId2.
     * @param value value to set the GL_ID
     */
    public void setGlId2(Number value) {
        setAttributeInternal(GLID2, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber1.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Number getObjectVersionNumber1() {
        return (Number) getAttributeInternal(OBJECTVERSIONNUMBER1);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber1.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber1(Number value) {
        setAttributeInternal(OBJECTVERSIONNUMBER1, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber2.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Number getObjectVersionNumber2() {
        return (Number) getAttributeInternal(OBJECTVERSIONNUMBER2);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber2.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber2(Number value) {
        setAttributeInternal(OBJECTVERSIONNUMBER2, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber3.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Number getObjectVersionNumber3() {
        return (Number) getAttributeInternal(OBJECTVERSIONNUMBER3);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber3.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber3(Number value) {
        setAttributeInternal(OBJECTVERSIONNUMBER3, value);
    }

    /**
     * Gets the attribute value for PROD_CODE using the alias name ProdCode.
     * @return the PROD_CODE
     */
    public String getProdCode() {
        return (String) getAttributeInternal(PRODCODE);
    }

    /**
     * Sets <code>value</code> as attribute value for PROD_CODE using the alias name ProdCode.
     * @param value value to set the PROD_CODE
     */
    public void setProdCode(String value) {
        setAttributeInternal(PRODCODE, value);
    }

    /**
     * Gets the attribute value for ITEM_DESCRIPTION using the alias name ItemDescription.
     * @return the ITEM_DESCRIPTION
     */
    public String getItemDescription()
    {
        if(getAttributeInternal(ITEMDESCRIPTION)==null)
        {
            try {
                SaleSetupAMImpl am = (SaleSetupAMImpl) this.getApplicationModule();
                ViewObjectImpl vo=am.getItemStockFilterVVO1();
                vo.setNamedWhereClauseParam("bindItemCd", getProdCode());
                vo.executeQuery();
                Row row[] = vo.getAllRowsInRange();
                System.out.println("InvoicelinkDetail"+row.length);
//                Row row[] = getItemStockVO1().getFilteredRows("ItemCd", getProdCode());
                if (row.length > 0) {
                    if (row[0].getAttribute("ItemDesc") != null) {
                        return (String) row[0].getAttribute("ItemDesc");
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            } 
            
        }
        return (String) getAttributeInternal(ITEMDESCRIPTION);
    }

    /**
     * Sets <code>value</code> as attribute value for ITEM_DESCRIPTION using the alias name ItemDescription.
     * @param value value to set the ITEM_DESCRIPTION
     */
    public void setItemDescription(String value) {
        setAttributeInternal(ITEMDESCRIPTION, value);
    }

    /**
     * Gets the attribute value for ITEM_ID using the alias name ItemId.
     * @return the ITEM_ID
     */
    public Long getItemId() {
        return (Long) getAttributeInternal(ITEMID);
    }

    /**
     * Sets <code>value</code> as attribute value for ITEM_ID using the alias name ItemId.
     * @param value value to set the ITEM_ID
     */
    public void setItemId(Long value) {
        setAttributeInternal(ITEMID, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber4.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber4() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER4);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber4.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber4(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER4, value);
    }

    /**
     * Gets the view accessor <code>RowSet</code> GlCgstGenLedVVO1.
     */
    public RowSet getGlCgstGenLedVVO1() {
        return (RowSet) getAttributeInternal(GLCGSTGENLEDVVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> GlSgstGenLedVVO1.
     */
    public RowSet getGlSgstGenLedVVO1() {
        return (RowSet) getAttributeInternal(GLSGSTGENLEDVVO1);
    }


    /**
     * Gets the view accessor <code>RowSet</code> TypeSecControlVO1.
     */
    public RowSet getTypeSecControlVO1() {
        return (RowSet) getAttributeInternal(TYPESECCONTROLVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> ItemStockVO1.
     */
    public RowSet getItemStockVO1() {
        return (RowSet) getAttributeInternal(ITEMSTOCKVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> GenledVVO1.
     */
    public RowSet getGenledVVO1() {
        return (RowSet) getAttributeInternal(GENLEDVVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> GenledVVO2.
     */
    public RowSet getGenledVVO2() {
        return (RowSet) getAttributeInternal(GENLEDVVO2);
    }

    /**
     * Gets the view accessor <code>RowSet</code> GenledVVO3.
     */
    public RowSet getGenledVVO3() {
        return (RowSet) getAttributeInternal(GENLEDVVO3);
    }
}

