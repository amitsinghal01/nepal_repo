package terms.sales.transaction.model.view;

import java.math.BigDecimal;

import java.sql.Timestamp;

import oracle.jbo.server.ViewRowImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Mon Jan 27 15:58:06 IST 2020
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class DolPendingDispatchAdvcHeaderVVORowImpl extends ViewRowImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        UnitCd,
        CustCode,
        CustomerName,
        DaNo,
        AmdNo,
        DaDate,
        InvHeadType,
        StockType,
        TransChkbox;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }

    public static final int UNITCD = AttributesEnum.UnitCd.index();
    public static final int CUSTCODE = AttributesEnum.CustCode.index();
    public static final int CUSTOMERNAME = AttributesEnum.CustomerName.index();
    public static final int DANO = AttributesEnum.DaNo.index();
    public static final int AMDNO = AttributesEnum.AmdNo.index();
    public static final int DADATE = AttributesEnum.DaDate.index();
    public static final int INVHEADTYPE = AttributesEnum.InvHeadType.index();
    public static final int STOCKTYPE = AttributesEnum.StockType.index();
    public static final int TRANSCHKBOX = AttributesEnum.TransChkbox.index();

    /**
     * This is the default constructor (do not remove).
     */
    public DolPendingDispatchAdvcHeaderVVORowImpl() {
    }

    /**
     * Gets the attribute value for the calculated attribute UnitCd.
     * @return the UnitCd
     */
    public String getUnitCd() {
        return (String) getAttributeInternal(UNITCD);
    }

    /**
     * Gets the attribute value for the calculated attribute CustCode.
     * @return the CustCode
     */
    public String getCustCode() {
        return (String) getAttributeInternal(CUSTCODE);
    }

    /**
     * Gets the attribute value for the calculated attribute CustomerName.
     * @return the CustomerName
     */
    public String getCustomerName() {
        return (String) getAttributeInternal(CUSTOMERNAME);
    }

    /**
     * Gets the attribute value for the calculated attribute DaNo.
     * @return the DaNo
     */
    public String getDaNo() {
        return (String) getAttributeInternal(DANO);
    }

    /**
     * Gets the attribute value for the calculated attribute AmdNo.
     * @return the AmdNo
     */
    public BigDecimal getAmdNo() {
        return (BigDecimal) getAttributeInternal(AMDNO);
    }

    /**
     * Gets the attribute value for the calculated attribute DaDate.
     * @return the DaDate
     */
    public Timestamp getDaDate() {
        return (Timestamp) getAttributeInternal(DADATE);
    }

    /**
     * Gets the attribute value for the calculated attribute InvHeadType.
     * @return the InvHeadType
     */
    public String getInvHeadType() {
        return (String) getAttributeInternal(INVHEADTYPE);
    }

    /**
     * Gets the attribute value for the calculated attribute StockType.
     * @return the StockType
     */
    public String getStockType() {
        return (String) getAttributeInternal(STOCKTYPE);
    }

    /**
     * Gets the attribute value for the calculated attribute TransChkbox.
     * @return the TransChkbox
     */
    public String getTransChkbox() {
        if(getAttributeInternal(TRANSCHKBOX)==null)
         return "N";
        return (String) getAttributeInternal(TRANSCHKBOX);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute TransChkbox.
     * @param value value to set the  TransChkbox
     */
    public void setTransChkbox(String value) {
        setAttributeInternal(TRANSCHKBOX, value);
    }
}

