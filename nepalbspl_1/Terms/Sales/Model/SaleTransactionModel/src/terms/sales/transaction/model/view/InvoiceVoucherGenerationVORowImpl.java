package terms.sales.transaction.model.view;

import java.math.BigDecimal;

import java.sql.Timestamp;

import oracle.jbo.server.EntityImpl;
import oracle.jbo.server.ViewRowImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Mon May 07 10:36:06 IST 2018
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class InvoiceVoucherGenerationVORowImpl extends ViewRowImpl {


    public static final int ENTITY_INVOICEVOUCHERGENERATIONEO = 0;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        CreatedBy,
        CreationDate,
        CurrRate,
        Dates,
        FinYear,
        Identifier,
        InvHeadType,
        LastUpdateDate,
        LastUpdatedBy,
        NetAmount,
        NetAmountFc,
        ObjectVersionNumber,
        PartyCode,
        PaymntDueDt,
        PoHeadAckNumber,
        SalesTaxRate,
        SalesTaxType,
        StockType,
        UnitCode,
        VouGen,
        VouNo,
        EditSelect,
        Name,
        VouDate,
        VouFlag;
        static AttributesEnum[] vals = null;
        ;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int CREATEDBY = AttributesEnum.CreatedBy.index();
    public static final int CREATIONDATE = AttributesEnum.CreationDate.index();
    public static final int CURRRATE = AttributesEnum.CurrRate.index();
    public static final int DATES = AttributesEnum.Dates.index();
    public static final int FINYEAR = AttributesEnum.FinYear.index();
    public static final int IDENTIFIER = AttributesEnum.Identifier.index();
    public static final int INVHEADTYPE = AttributesEnum.InvHeadType.index();
    public static final int LASTUPDATEDATE = AttributesEnum.LastUpdateDate.index();
    public static final int LASTUPDATEDBY = AttributesEnum.LastUpdatedBy.index();
    public static final int NETAMOUNT = AttributesEnum.NetAmount.index();
    public static final int NETAMOUNTFC = AttributesEnum.NetAmountFc.index();
    public static final int OBJECTVERSIONNUMBER = AttributesEnum.ObjectVersionNumber.index();
    public static final int PARTYCODE = AttributesEnum.PartyCode.index();
    public static final int PAYMNTDUEDT = AttributesEnum.PaymntDueDt.index();
    public static final int POHEADACKNUMBER = AttributesEnum.PoHeadAckNumber.index();
    public static final int SALESTAXRATE = AttributesEnum.SalesTaxRate.index();
    public static final int SALESTAXTYPE = AttributesEnum.SalesTaxType.index();
    public static final int STOCKTYPE = AttributesEnum.StockType.index();
    public static final int UNITCODE = AttributesEnum.UnitCode.index();
    public static final int VOUGEN = AttributesEnum.VouGen.index();
    public static final int VOUNO = AttributesEnum.VouNo.index();
    public static final int EDITSELECT = AttributesEnum.EditSelect.index();
    public static final int NAME = AttributesEnum.Name.index();
    public static final int VOUDATE = AttributesEnum.VouDate.index();
    public static final int VOUFLAG = AttributesEnum.VouFlag.index();

    /**
     * This is the default constructor (do not remove).
     */
    public InvoiceVoucherGenerationVORowImpl() {
    }

    /**
     * Gets InvoiceVoucherGenerationEO entity object.
     * @return the InvoiceVoucherGenerationEO
     */
    public EntityImpl getInvoiceVoucherGenerationEO() {
        return (EntityImpl) getEntity(ENTITY_INVOICEVOUCHERGENERATIONEO);
    }

    /**
     * Gets the attribute value for CREATED_BY using the alias name CreatedBy.
     * @return the CREATED_BY
     */
    public String getCreatedBy() {
        return (String) getAttributeInternal(CREATEDBY);
    }

    /**
     * Gets the attribute value for CREATION_DATE using the alias name CreationDate.
     * @return the CREATION_DATE
     */
    public Timestamp getCreationDate() {
        return (Timestamp) getAttributeInternal(CREATIONDATE);
    }

    /**
     * Gets the attribute value for CURR_RATE using the alias name CurrRate.
     * @return the CURR_RATE
     */
    public BigDecimal getCurrRate() {
        return (BigDecimal) getAttributeInternal(CURRRATE);
    }

    /**
     * Sets <code>value</code> as attribute value for CURR_RATE using the alias name CurrRate.
     * @param value value to set the CURR_RATE
     */
    public void setCurrRate(BigDecimal value) {
        setAttributeInternal(CURRRATE, value);
    }

    /**
     * Gets the attribute value for DATES using the alias name Dates.
     * @return the DATES
     */
    public Timestamp getDates() {
        return (Timestamp) getAttributeInternal(DATES);
    }

    /**
     * Sets <code>value</code> as attribute value for DATES using the alias name Dates.
     * @param value value to set the DATES
     */
    public void setDates(Timestamp value) {
        setAttributeInternal(DATES, value);
    }

    /**
     * Gets the attribute value for FIN_YEAR using the alias name FinYear.
     * @return the FIN_YEAR
     */
    public String getFinYear() {
        return (String) getAttributeInternal(FINYEAR);
    }

    /**
     * Sets <code>value</code> as attribute value for FIN_YEAR using the alias name FinYear.
     * @param value value to set the FIN_YEAR
     */
    public void setFinYear(String value) {
        setAttributeInternal(FINYEAR, value);
    }

    /**
     * Gets the attribute value for IDENTIFIER using the alias name Identifier.
     * @return the IDENTIFIER
     */
    public String getIdentifier() {
        return (String) getAttributeInternal(IDENTIFIER);
    }

    /**
     * Sets <code>value</code> as attribute value for IDENTIFIER using the alias name Identifier.
     * @param value value to set the IDENTIFIER
     */
    public void setIdentifier(String value) {
        setAttributeInternal(IDENTIFIER, value);
    }

    /**
     * Gets the attribute value for INV_HEAD_TYPE using the alias name InvHeadType.
     * @return the INV_HEAD_TYPE
     */
    public String getInvHeadType() {
        return (String) getAttributeInternal(INVHEADTYPE);
    }

    /**
     * Sets <code>value</code> as attribute value for INV_HEAD_TYPE using the alias name InvHeadType.
     * @param value value to set the INV_HEAD_TYPE
     */
    public void setInvHeadType(String value) {
        setAttributeInternal(INVHEADTYPE, value);
    }

    /**
     * Gets the attribute value for LAST_UPDATE_DATE using the alias name LastUpdateDate.
     * @return the LAST_UPDATE_DATE
     */
    public Timestamp getLastUpdateDate() {
        return (Timestamp) getAttributeInternal(LASTUPDATEDATE);
    }

    /**
     * Gets the attribute value for LAST_UPDATED_BY using the alias name LastUpdatedBy.
     * @return the LAST_UPDATED_BY
     */
    public String getLastUpdatedBy() {
        return (String) getAttributeInternal(LASTUPDATEDBY);
    }

    /**
     * Gets the attribute value for NET_AMOUNT using the alias name NetAmount.
     * @return the NET_AMOUNT
     */
    public BigDecimal getNetAmount() {
        return (BigDecimal) getAttributeInternal(NETAMOUNT);
    }

    /**
     * Sets <code>value</code> as attribute value for NET_AMOUNT using the alias name NetAmount.
     * @param value value to set the NET_AMOUNT
     */
    public void setNetAmount(BigDecimal value) {
        setAttributeInternal(NETAMOUNT, value);
    }

    /**
     * Gets the attribute value for NET_AMOUNT_FC using the alias name NetAmountFc.
     * @return the NET_AMOUNT_FC
     */
    public BigDecimal getNetAmountFc() {
        return (BigDecimal) getAttributeInternal(NETAMOUNTFC);
    }

    /**
     * Sets <code>value</code> as attribute value for NET_AMOUNT_FC using the alias name NetAmountFc.
     * @param value value to set the NET_AMOUNT_FC
     */
    public void setNetAmountFc(BigDecimal value) {
        setAttributeInternal(NETAMOUNTFC, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER, value);
    }

    /**
     * Gets the attribute value for PARTY_CODE using the alias name PartyCode.
     * @return the PARTY_CODE
     */
    public String getPartyCode() {
        return (String) getAttributeInternal(PARTYCODE);
    }

    /**
     * Sets <code>value</code> as attribute value for PARTY_CODE using the alias name PartyCode.
     * @param value value to set the PARTY_CODE
     */
    public void setPartyCode(String value) {
        setAttributeInternal(PARTYCODE, value);
    }

    /**
     * Gets the attribute value for PAYMNT_DUE_DT using the alias name PaymntDueDt.
     * @return the PAYMNT_DUE_DT
     */
    public Timestamp getPaymntDueDt() {
        return (Timestamp) getAttributeInternal(PAYMNTDUEDT);
    }

    /**
     * Sets <code>value</code> as attribute value for PAYMNT_DUE_DT using the alias name PaymntDueDt.
     * @param value value to set the PAYMNT_DUE_DT
     */
    public void setPaymntDueDt(Timestamp value) {
        setAttributeInternal(PAYMNTDUEDT, value);
    }

    /**
     * Gets the attribute value for PO_HEAD_ACK_NUMBER using the alias name PoHeadAckNumber.
     * @return the PO_HEAD_ACK_NUMBER
     */
    public String getPoHeadAckNumber() {
        return (String) getAttributeInternal(POHEADACKNUMBER);
    }

    /**
     * Sets <code>value</code> as attribute value for PO_HEAD_ACK_NUMBER using the alias name PoHeadAckNumber.
     * @param value value to set the PO_HEAD_ACK_NUMBER
     */
    public void setPoHeadAckNumber(String value) {
        setAttributeInternal(POHEADACKNUMBER, value);
    }

    /**
     * Gets the attribute value for SALES_TAX_RATE using the alias name SalesTaxRate.
     * @return the SALES_TAX_RATE
     */
    public BigDecimal getSalesTaxRate() {
        return (BigDecimal) getAttributeInternal(SALESTAXRATE);
    }

    /**
     * Sets <code>value</code> as attribute value for SALES_TAX_RATE using the alias name SalesTaxRate.
     * @param value value to set the SALES_TAX_RATE
     */
    public void setSalesTaxRate(BigDecimal value) {
        setAttributeInternal(SALESTAXRATE, value);
    }

    /**
     * Gets the attribute value for SALES_TAX_TYPE using the alias name SalesTaxType.
     * @return the SALES_TAX_TYPE
     */
    public String getSalesTaxType() {
        return (String) getAttributeInternal(SALESTAXTYPE);
    }

    /**
     * Sets <code>value</code> as attribute value for SALES_TAX_TYPE using the alias name SalesTaxType.
     * @param value value to set the SALES_TAX_TYPE
     */
    public void setSalesTaxType(String value) {
        setAttributeInternal(SALESTAXTYPE, value);
    }

    /**
     * Gets the attribute value for STOCK_TYPE using the alias name StockType.
     * @return the STOCK_TYPE
     */
    public String getStockType() {
        return (String) getAttributeInternal(STOCKTYPE);
    }

    /**
     * Sets <code>value</code> as attribute value for STOCK_TYPE using the alias name StockType.
     * @param value value to set the STOCK_TYPE
     */
    public void setStockType(String value) {
        setAttributeInternal(STOCKTYPE, value);
    }

    /**
     * Gets the attribute value for UNIT_CODE using the alias name UnitCode.
     * @return the UNIT_CODE
     */
    public String getUnitCode() {
        return (String) getAttributeInternal(UNITCODE);
    }

    /**
     * Sets <code>value</code> as attribute value for UNIT_CODE using the alias name UnitCode.
     * @param value value to set the UNIT_CODE
     */
    public void setUnitCode(String value) {
        setAttributeInternal(UNITCODE, value);
    }

    /**
     * Gets the attribute value for VOU_GEN using the alias name VouGen.
     * @return the VOU_GEN
     */
    public String getVouGen() {
        return (String) getAttributeInternal(VOUGEN);
    }

    /**
     * Sets <code>value</code> as attribute value for VOU_GEN using the alias name VouGen.
     * @param value value to set the VOU_GEN
     */
    public void setVouGen(String value) {
        setAttributeInternal(VOUGEN, value);
    }

    /**
     * Gets the attribute value for VOU_NO using the alias name VouNo.
     * @return the VOU_NO
     */
    public String getVouNo() {
        return (String) getAttributeInternal(VOUNO);
    }

    /**
     * Sets <code>value</code> as attribute value for VOU_NO using the alias name VouNo.
     * @param value value to set the VOU_NO
     */
    public void setVouNo(String value) {
        setAttributeInternal(VOUNO, value);
    }

    /**
     * Gets the attribute value for the calculated attribute EditSelect.
     * @return the EditSelect
     */
    public String getEditSelect() {
        return (String) getAttributeInternal(EDITSELECT);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute EditSelect.
     * @param value value to set the  EditSelect
     */
    public void setEditSelect(String value) {
        setAttributeInternal(EDITSELECT, value);
    }

    /**
     * Gets the attribute value for the calculated attribute Name.
     * @return the Name
     */
    public String getName() {
        return (String) getAttributeInternal(NAME);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute Name.
     * @param value value to set the  Name
     */
    public void setName(String value) {
        setAttributeInternal(NAME, value);
    }

    /**
     * Gets the attribute value for VOU_DATE using the alias name VouDate.
     * @return the VOU_DATE
     */
    public Timestamp getVouDate() {
        return (Timestamp) getAttributeInternal(VOUDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for VOU_DATE using the alias name VouDate.
     * @param value value to set the VOU_DATE
     */
    public void setVouDate(Timestamp value) {
        setAttributeInternal(VOUDATE, value);
    }

    /**
     * Gets the attribute value for VOU_FLAG using the alias name VouFlag.
     * @return the VOU_FLAG
     */
    public String getVouFlag() {
        return (String) getAttributeInternal(VOUFLAG);
    }

    /**
     * Sets <code>value</code> as attribute value for VOU_FLAG using the alias name VouFlag.
     * @param value value to set the VOU_FLAG
     */
    public void setVouFlag(String value) {
        setAttributeInternal(VOUFLAG, value);
    }


}

