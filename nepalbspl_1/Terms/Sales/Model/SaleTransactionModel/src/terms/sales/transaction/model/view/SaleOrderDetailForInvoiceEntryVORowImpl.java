package terms.sales.transaction.model.view;

import java.math.BigDecimal;

import java.sql.Timestamp;

import oracle.jbo.server.EntityImpl;
import oracle.jbo.server.ViewRowImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Tue Jan 23 12:42:49 IST 2018
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class SaleOrderDetailForInvoiceEntryVORowImpl extends ViewRowImpl {


    public static final int ENTITY_SALEORDERDETAILFORINVOICEENT1 = 0;
    public static final int ENTITY_ITEMSTOCKEO = 1;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        PendingSaleOrderForSaleTrans,
        EditSelectTrans,
        PoHeadAckNumber,
        PoHeadAmendAckNumber,
        ProdCode,
        Discount,
        ToolingAdvance,
        SubsidizedQuantity,
        SubsidizedPrice,
        SubsidizedDuration,
        QuantityDespatched,
        Warranty,
        Price,
        Quantity,
        DeliveryDate,
        CustBinNumber,
        OpenFix,
        CashDiscount,
        DiffAssVal,
        ProcCd,
        ProcSeq,
        CustMrpPrice,
        CustMrpPer,
        Uom,
        PoCan,
        NbtItemDesc,
        DiscountPrice,
        HsnNo,
        GstCode,
        Sgst,
        Cgst,
        Igst,
        SgstRate,
        CgstRate,
        IgstRate,
        AckId,
        AckLineId,
        CreatedBy,
        CreationDate,
        LastUpdatedBy,
        LastUpdateDate,
        ObjectVersionNumber,
        QuotIdentifier,
        ItemDesc,
        ItemCd;
        static AttributesEnum[] vals = null;
        ;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int PENDINGSALEORDERFORSALETRANS = AttributesEnum.PendingSaleOrderForSaleTrans.index();
    public static final int EDITSELECTTRANS = AttributesEnum.EditSelectTrans.index();
    public static final int POHEADACKNUMBER = AttributesEnum.PoHeadAckNumber.index();
    public static final int POHEADAMENDACKNUMBER = AttributesEnum.PoHeadAmendAckNumber.index();
    public static final int PRODCODE = AttributesEnum.ProdCode.index();
    public static final int DISCOUNT = AttributesEnum.Discount.index();
    public static final int TOOLINGADVANCE = AttributesEnum.ToolingAdvance.index();
    public static final int SUBSIDIZEDQUANTITY = AttributesEnum.SubsidizedQuantity.index();
    public static final int SUBSIDIZEDPRICE = AttributesEnum.SubsidizedPrice.index();
    public static final int SUBSIDIZEDDURATION = AttributesEnum.SubsidizedDuration.index();
    public static final int QUANTITYDESPATCHED = AttributesEnum.QuantityDespatched.index();
    public static final int WARRANTY = AttributesEnum.Warranty.index();
    public static final int PRICE = AttributesEnum.Price.index();
    public static final int QUANTITY = AttributesEnum.Quantity.index();
    public static final int DELIVERYDATE = AttributesEnum.DeliveryDate.index();
    public static final int CUSTBINNUMBER = AttributesEnum.CustBinNumber.index();
    public static final int OPENFIX = AttributesEnum.OpenFix.index();
    public static final int CASHDISCOUNT = AttributesEnum.CashDiscount.index();
    public static final int DIFFASSVAL = AttributesEnum.DiffAssVal.index();
    public static final int PROCCD = AttributesEnum.ProcCd.index();
    public static final int PROCSEQ = AttributesEnum.ProcSeq.index();
    public static final int CUSTMRPPRICE = AttributesEnum.CustMrpPrice.index();
    public static final int CUSTMRPPER = AttributesEnum.CustMrpPer.index();
    public static final int UOM = AttributesEnum.Uom.index();
    public static final int POCAN = AttributesEnum.PoCan.index();
    public static final int NBTITEMDESC = AttributesEnum.NbtItemDesc.index();
    public static final int DISCOUNTPRICE = AttributesEnum.DiscountPrice.index();
    public static final int HSNNO = AttributesEnum.HsnNo.index();
    public static final int GSTCODE = AttributesEnum.GstCode.index();
    public static final int SGST = AttributesEnum.Sgst.index();
    public static final int CGST = AttributesEnum.Cgst.index();
    public static final int IGST = AttributesEnum.Igst.index();
    public static final int SGSTRATE = AttributesEnum.SgstRate.index();
    public static final int CGSTRATE = AttributesEnum.CgstRate.index();
    public static final int IGSTRATE = AttributesEnum.IgstRate.index();
    public static final int ACKID = AttributesEnum.AckId.index();
    public static final int ACKLINEID = AttributesEnum.AckLineId.index();
    public static final int CREATEDBY = AttributesEnum.CreatedBy.index();
    public static final int CREATIONDATE = AttributesEnum.CreationDate.index();
    public static final int LASTUPDATEDBY = AttributesEnum.LastUpdatedBy.index();
    public static final int LASTUPDATEDATE = AttributesEnum.LastUpdateDate.index();
    public static final int OBJECTVERSIONNUMBER = AttributesEnum.ObjectVersionNumber.index();
    public static final int QUOTIDENTIFIER = AttributesEnum.QuotIdentifier.index();
    public static final int ITEMDESC = AttributesEnum.ItemDesc.index();
    public static final int ITEMCD = AttributesEnum.ItemCd.index();

    /**
     * This is the default constructor (do not remove).
     */
    public SaleOrderDetailForInvoiceEntryVORowImpl() {
    }


    /**
     * Gets SaleOrderDetailForInvoiceEnt1 entity object.
     * @return the SaleOrderDetailForInvoiceEnt1
     */
    public EntityImpl getSaleOrderDetailForInvoiceEnt1() {
        return (EntityImpl) getEntity(ENTITY_SALEORDERDETAILFORINVOICEENT1);
    }

    /**
     * Gets ItemStockEO entity object.
     * @return the ItemStockEO
     */
    public EntityImpl getItemStockEO() {
        return (EntityImpl) getEntity(ENTITY_ITEMSTOCKEO);
    }

    /**
     * Gets the attribute value for the calculated attribute PendingSaleOrderForSaleTrans.
     * @return the PendingSaleOrderForSaleTrans
     */
    public BigDecimal getPendingSaleOrderForSaleTrans() {
        return (BigDecimal) getAttributeInternal(PENDINGSALEORDERFORSALETRANS);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute PendingSaleOrderForSaleTrans.
     * @param value value to set the  PendingSaleOrderForSaleTrans
     */
    public void setPendingSaleOrderForSaleTrans(BigDecimal value) {
        setAttributeInternal(PENDINGSALEORDERFORSALETRANS, value);
    }

    /**
     * Gets the attribute value for the calculated attribute EditSelectTrans.
     * @return the EditSelectTrans
     */
    public String getEditSelectTrans() {
        return (String) getAttributeInternal(EDITSELECTTRANS);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute EditSelectTrans.
     * @param value value to set the  EditSelectTrans
     */
    public void setEditSelectTrans(String value) {
        setAttributeInternal(EDITSELECTTRANS, value);
    }

    /**
     * Gets the attribute value for PO_HEAD_ACK_NUMBER using the alias name PoHeadAckNumber.
     * @return the PO_HEAD_ACK_NUMBER
     */
    public String getPoHeadAckNumber() {
        return (String) getAttributeInternal(POHEADACKNUMBER);
    }

    /**
     * Sets <code>value</code> as attribute value for PO_HEAD_ACK_NUMBER using the alias name PoHeadAckNumber.
     * @param value value to set the PO_HEAD_ACK_NUMBER
     */
    public void setPoHeadAckNumber(String value) {
        setAttributeInternal(POHEADACKNUMBER, value);
    }

    /**
     * Gets the attribute value for PO_HEAD_AMEND_ACK_NUMBER using the alias name PoHeadAmendAckNumber.
     * @return the PO_HEAD_AMEND_ACK_NUMBER
     */
    public BigDecimal getPoHeadAmendAckNumber() {
        return (BigDecimal) getAttributeInternal(POHEADAMENDACKNUMBER);
    }

    /**
     * Sets <code>value</code> as attribute value for PO_HEAD_AMEND_ACK_NUMBER using the alias name PoHeadAmendAckNumber.
     * @param value value to set the PO_HEAD_AMEND_ACK_NUMBER
     */
    public void setPoHeadAmendAckNumber(BigDecimal value) {
        setAttributeInternal(POHEADAMENDACKNUMBER, value);
    }

    /**
     * Gets the attribute value for PROD_CODE using the alias name ProdCode.
     * @return the PROD_CODE
     */
    public String getProdCode() {
        return (String) getAttributeInternal(PRODCODE);
    }

    /**
     * Sets <code>value</code> as attribute value for PROD_CODE using the alias name ProdCode.
     * @param value value to set the PROD_CODE
     */
    public void setProdCode(String value) {
        setAttributeInternal(PRODCODE, value);
    }

    /**
     * Gets the attribute value for DISCOUNT using the alias name Discount.
     * @return the DISCOUNT
     */
    public BigDecimal getDiscount() {
        return (BigDecimal) getAttributeInternal(DISCOUNT);
    }

    /**
     * Sets <code>value</code> as attribute value for DISCOUNT using the alias name Discount.
     * @param value value to set the DISCOUNT
     */
    public void setDiscount(BigDecimal value) {
        setAttributeInternal(DISCOUNT, value);
    }

    /**
     * Gets the attribute value for TOOLING_ADVANCE using the alias name ToolingAdvance.
     * @return the TOOLING_ADVANCE
     */
    public BigDecimal getToolingAdvance() {
        return (BigDecimal) getAttributeInternal(TOOLINGADVANCE);
    }

    /**
     * Sets <code>value</code> as attribute value for TOOLING_ADVANCE using the alias name ToolingAdvance.
     * @param value value to set the TOOLING_ADVANCE
     */
    public void setToolingAdvance(BigDecimal value) {
        setAttributeInternal(TOOLINGADVANCE, value);
    }

    /**
     * Gets the attribute value for SUBSIDIZED_QUANTITY using the alias name SubsidizedQuantity.
     * @return the SUBSIDIZED_QUANTITY
     */
    public BigDecimal getSubsidizedQuantity() {
        return (BigDecimal) getAttributeInternal(SUBSIDIZEDQUANTITY);
    }

    /**
     * Sets <code>value</code> as attribute value for SUBSIDIZED_QUANTITY using the alias name SubsidizedQuantity.
     * @param value value to set the SUBSIDIZED_QUANTITY
     */
    public void setSubsidizedQuantity(BigDecimal value) {
        setAttributeInternal(SUBSIDIZEDQUANTITY, value);
    }

    /**
     * Gets the attribute value for SUBSIDIZED_PRICE using the alias name SubsidizedPrice.
     * @return the SUBSIDIZED_PRICE
     */
    public BigDecimal getSubsidizedPrice() {
        return (BigDecimal) getAttributeInternal(SUBSIDIZEDPRICE);
    }

    /**
     * Sets <code>value</code> as attribute value for SUBSIDIZED_PRICE using the alias name SubsidizedPrice.
     * @param value value to set the SUBSIDIZED_PRICE
     */
    public void setSubsidizedPrice(BigDecimal value) {
        setAttributeInternal(SUBSIDIZEDPRICE, value);
    }

    /**
     * Gets the attribute value for SUBSIDIZED_DURATION using the alias name SubsidizedDuration.
     * @return the SUBSIDIZED_DURATION
     */
    public BigDecimal getSubsidizedDuration() {
        return (BigDecimal) getAttributeInternal(SUBSIDIZEDDURATION);
    }

    /**
     * Sets <code>value</code> as attribute value for SUBSIDIZED_DURATION using the alias name SubsidizedDuration.
     * @param value value to set the SUBSIDIZED_DURATION
     */
    public void setSubsidizedDuration(BigDecimal value) {
        setAttributeInternal(SUBSIDIZEDDURATION, value);
    }

    /**
     * Gets the attribute value for QUANTITY_DESPATCHED using the alias name QuantityDespatched.
     * @return the QUANTITY_DESPATCHED
     */
    public BigDecimal getQuantityDespatched() {
        return (BigDecimal) getAttributeInternal(QUANTITYDESPATCHED);
    }

    /**
     * Sets <code>value</code> as attribute value for QUANTITY_DESPATCHED using the alias name QuantityDespatched.
     * @param value value to set the QUANTITY_DESPATCHED
     */
    public void setQuantityDespatched(BigDecimal value) {
        setAttributeInternal(QUANTITYDESPATCHED, value);
    }

    /**
     * Gets the attribute value for WARRANTY using the alias name Warranty.
     * @return the WARRANTY
     */
    public Integer getWarranty() {
        return (Integer) getAttributeInternal(WARRANTY);
    }

    /**
     * Sets <code>value</code> as attribute value for WARRANTY using the alias name Warranty.
     * @param value value to set the WARRANTY
     */
    public void setWarranty(Integer value) {
        setAttributeInternal(WARRANTY, value);
    }

    /**
     * Gets the attribute value for PRICE using the alias name Price.
     * @return the PRICE
     */
    public BigDecimal getPrice() {
        return (BigDecimal) getAttributeInternal(PRICE);
    }

    /**
     * Sets <code>value</code> as attribute value for PRICE using the alias name Price.
     * @param value value to set the PRICE
     */
    public void setPrice(BigDecimal value) {
        setAttributeInternal(PRICE, value);
    }

    /**
     * Gets the attribute value for QUANTITY using the alias name Quantity.
     * @return the QUANTITY
     */
    public BigDecimal getQuantity() {
        return (BigDecimal) getAttributeInternal(QUANTITY);
    }

    /**
     * Sets <code>value</code> as attribute value for QUANTITY using the alias name Quantity.
     * @param value value to set the QUANTITY
     */
    public void setQuantity(BigDecimal value) {
        setAttributeInternal(QUANTITY, value);
    }

    /**
     * Gets the attribute value for DELIVERY_DATE using the alias name DeliveryDate.
     * @return the DELIVERY_DATE
     */
    public Timestamp getDeliveryDate() {
        return (Timestamp) getAttributeInternal(DELIVERYDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for DELIVERY_DATE using the alias name DeliveryDate.
     * @param value value to set the DELIVERY_DATE
     */
    public void setDeliveryDate(Timestamp value) {
        setAttributeInternal(DELIVERYDATE, value);
    }

    /**
     * Gets the attribute value for CUST_BIN_NUMBER using the alias name CustBinNumber.
     * @return the CUST_BIN_NUMBER
     */
    public String getCustBinNumber() {
        return (String) getAttributeInternal(CUSTBINNUMBER);
    }

    /**
     * Sets <code>value</code> as attribute value for CUST_BIN_NUMBER using the alias name CustBinNumber.
     * @param value value to set the CUST_BIN_NUMBER
     */
    public void setCustBinNumber(String value) {
        setAttributeInternal(CUSTBINNUMBER, value);
    }

    /**
     * Gets the attribute value for OPEN_FIX using the alias name OpenFix.
     * @return the OPEN_FIX
     */
    public String getOpenFix() {
        return (String) getAttributeInternal(OPENFIX);
    }

    /**
     * Sets <code>value</code> as attribute value for OPEN_FIX using the alias name OpenFix.
     * @param value value to set the OPEN_FIX
     */
    public void setOpenFix(String value) {
        setAttributeInternal(OPENFIX, value);
    }

    /**
     * Gets the attribute value for CASH_DISCOUNT using the alias name CashDiscount.
     * @return the CASH_DISCOUNT
     */
    public BigDecimal getCashDiscount() {
        return (BigDecimal) getAttributeInternal(CASHDISCOUNT);
    }

    /**
     * Sets <code>value</code> as attribute value for CASH_DISCOUNT using the alias name CashDiscount.
     * @param value value to set the CASH_DISCOUNT
     */
    public void setCashDiscount(BigDecimal value) {
        setAttributeInternal(CASHDISCOUNT, value);
    }

    /**
     * Gets the attribute value for DIFF_ASS_VAL using the alias name DiffAssVal.
     * @return the DIFF_ASS_VAL
     */
    public BigDecimal getDiffAssVal() {
        return (BigDecimal) getAttributeInternal(DIFFASSVAL);
    }

    /**
     * Sets <code>value</code> as attribute value for DIFF_ASS_VAL using the alias name DiffAssVal.
     * @param value value to set the DIFF_ASS_VAL
     */
    public void setDiffAssVal(BigDecimal value) {
        setAttributeInternal(DIFFASSVAL, value);
    }

    /**
     * Gets the attribute value for PROC_CD using the alias name ProcCd.
     * @return the PROC_CD
     */
    public String getProcCd() {
        return (String) getAttributeInternal(PROCCD);
    }

    /**
     * Sets <code>value</code> as attribute value for PROC_CD using the alias name ProcCd.
     * @param value value to set the PROC_CD
     */
    public void setProcCd(String value) {
        setAttributeInternal(PROCCD, value);
    }

    /**
     * Gets the attribute value for PROC_SEQ using the alias name ProcSeq.
     * @return the PROC_SEQ
     */
    public Integer getProcSeq() {
        return (Integer) getAttributeInternal(PROCSEQ);
    }

    /**
     * Sets <code>value</code> as attribute value for PROC_SEQ using the alias name ProcSeq.
     * @param value value to set the PROC_SEQ
     */
    public void setProcSeq(Integer value) {
        setAttributeInternal(PROCSEQ, value);
    }

    /**
     * Gets the attribute value for CUST_MRP_PRICE using the alias name CustMrpPrice.
     * @return the CUST_MRP_PRICE
     */
    public BigDecimal getCustMrpPrice() {
        return (BigDecimal) getAttributeInternal(CUSTMRPPRICE);
    }

    /**
     * Sets <code>value</code> as attribute value for CUST_MRP_PRICE using the alias name CustMrpPrice.
     * @param value value to set the CUST_MRP_PRICE
     */
    public void setCustMrpPrice(BigDecimal value) {
        setAttributeInternal(CUSTMRPPRICE, value);
    }

    /**
     * Gets the attribute value for CUST_MRP_PER using the alias name CustMrpPer.
     * @return the CUST_MRP_PER
     */
    public BigDecimal getCustMrpPer() {
        return (BigDecimal) getAttributeInternal(CUSTMRPPER);
    }

    /**
     * Sets <code>value</code> as attribute value for CUST_MRP_PER using the alias name CustMrpPer.
     * @param value value to set the CUST_MRP_PER
     */
    public void setCustMrpPer(BigDecimal value) {
        setAttributeInternal(CUSTMRPPER, value);
    }

    /**
     * Gets the attribute value for UOM using the alias name Uom.
     * @return the UOM
     */
    public String getUom() {
        return (String) getAttributeInternal(UOM);
    }

    /**
     * Sets <code>value</code> as attribute value for UOM using the alias name Uom.
     * @param value value to set the UOM
     */
    public void setUom(String value) {
        setAttributeInternal(UOM, value);
    }

    /**
     * Gets the attribute value for PO_CAN using the alias name PoCan.
     * @return the PO_CAN
     */
    public String getPoCan() {
        return (String) getAttributeInternal(POCAN);
    }

    /**
     * Sets <code>value</code> as attribute value for PO_CAN using the alias name PoCan.
     * @param value value to set the PO_CAN
     */
    public void setPoCan(String value) {
        setAttributeInternal(POCAN, value);
    }

    /**
     * Gets the attribute value for NBT_ITEM_DESC using the alias name NbtItemDesc.
     * @return the NBT_ITEM_DESC
     */
    public String getNbtItemDesc() {
        return (String) getAttributeInternal(NBTITEMDESC);
    }

    /**
     * Sets <code>value</code> as attribute value for NBT_ITEM_DESC using the alias name NbtItemDesc.
     * @param value value to set the NBT_ITEM_DESC
     */
    public void setNbtItemDesc(String value) {
        setAttributeInternal(NBTITEMDESC, value);
    }

    /**
     * Gets the attribute value for DISCOUNT_PRICE using the alias name DiscountPrice.
     * @return the DISCOUNT_PRICE
     */
    public BigDecimal getDiscountPrice() {
        return (BigDecimal) getAttributeInternal(DISCOUNTPRICE);
    }

    /**
     * Sets <code>value</code> as attribute value for DISCOUNT_PRICE using the alias name DiscountPrice.
     * @param value value to set the DISCOUNT_PRICE
     */
    public void setDiscountPrice(BigDecimal value) {
        setAttributeInternal(DISCOUNTPRICE, value);
    }

    /**
     * Gets the attribute value for HSN_NO using the alias name HsnNo.
     * @return the HSN_NO
     */
    public String getHsnNo() {
        return (String) getAttributeInternal(HSNNO);
    }

    /**
     * Sets <code>value</code> as attribute value for HSN_NO using the alias name HsnNo.
     * @param value value to set the HSN_NO
     */
    public void setHsnNo(String value) {
        setAttributeInternal(HSNNO, value);
    }

    /**
     * Gets the attribute value for GST_CODE using the alias name GstCode.
     * @return the GST_CODE
     */
    public String getGstCode() {
        return (String) getAttributeInternal(GSTCODE);
    }

    /**
     * Sets <code>value</code> as attribute value for GST_CODE using the alias name GstCode.
     * @param value value to set the GST_CODE
     */
    public void setGstCode(String value) {
        setAttributeInternal(GSTCODE, value);
    }

    /**
     * Gets the attribute value for SGST using the alias name Sgst.
     * @return the SGST
     */
    public BigDecimal getSgst() {
        return (BigDecimal) getAttributeInternal(SGST);
    }

    /**
     * Sets <code>value</code> as attribute value for SGST using the alias name Sgst.
     * @param value value to set the SGST
     */
    public void setSgst(BigDecimal value) {
        setAttributeInternal(SGST, value);
    }

    /**
     * Gets the attribute value for CGST using the alias name Cgst.
     * @return the CGST
     */
    public BigDecimal getCgst() {
        return (BigDecimal) getAttributeInternal(CGST);
    }

    /**
     * Sets <code>value</code> as attribute value for CGST using the alias name Cgst.
     * @param value value to set the CGST
     */
    public void setCgst(BigDecimal value) {
        setAttributeInternal(CGST, value);
    }

    /**
     * Gets the attribute value for IGST using the alias name Igst.
     * @return the IGST
     */
    public BigDecimal getIgst() {
        return (BigDecimal) getAttributeInternal(IGST);
    }

    /**
     * Sets <code>value</code> as attribute value for IGST using the alias name Igst.
     * @param value value to set the IGST
     */
    public void setIgst(BigDecimal value) {
        setAttributeInternal(IGST, value);
    }

    /**
     * Gets the attribute value for SGST_RATE using the alias name SgstRate.
     * @return the SGST_RATE
     */
    public BigDecimal getSgstRate() {
        return (BigDecimal) getAttributeInternal(SGSTRATE);
    }

    /**
     * Sets <code>value</code> as attribute value for SGST_RATE using the alias name SgstRate.
     * @param value value to set the SGST_RATE
     */
    public void setSgstRate(BigDecimal value) {
        setAttributeInternal(SGSTRATE, value);
    }

    /**
     * Gets the attribute value for CGST_RATE using the alias name CgstRate.
     * @return the CGST_RATE
     */
    public BigDecimal getCgstRate() {
        return (BigDecimal) getAttributeInternal(CGSTRATE);
    }

    /**
     * Sets <code>value</code> as attribute value for CGST_RATE using the alias name CgstRate.
     * @param value value to set the CGST_RATE
     */
    public void setCgstRate(BigDecimal value) {
        setAttributeInternal(CGSTRATE, value);
    }

    /**
     * Gets the attribute value for IGST_RATE using the alias name IgstRate.
     * @return the IGST_RATE
     */
    public BigDecimal getIgstRate() {
        return (BigDecimal) getAttributeInternal(IGSTRATE);
    }

    /**
     * Sets <code>value</code> as attribute value for IGST_RATE using the alias name IgstRate.
     * @param value value to set the IGST_RATE
     */
    public void setIgstRate(BigDecimal value) {
        setAttributeInternal(IGSTRATE, value);
    }

    /**
     * Gets the attribute value for ACK_ID using the alias name AckId.
     * @return the ACK_ID
     */
    public Long getAckId() {
        return (Long) getAttributeInternal(ACKID);
    }

    /**
     * Sets <code>value</code> as attribute value for ACK_ID using the alias name AckId.
     * @param value value to set the ACK_ID
     */
    public void setAckId(Long value) {
        setAttributeInternal(ACKID, value);
    }

    /**
     * Gets the attribute value for ACK_LINE_ID using the alias name AckLineId.
     * @return the ACK_LINE_ID
     */
    public Long getAckLineId() {
        return (Long) getAttributeInternal(ACKLINEID);
    }

    /**
     * Sets <code>value</code> as attribute value for ACK_LINE_ID using the alias name AckLineId.
     * @param value value to set the ACK_LINE_ID
     */
    public void setAckLineId(Long value) {
        setAttributeInternal(ACKLINEID, value);
    }

    /**
     * Gets the attribute value for CREATED_BY using the alias name CreatedBy.
     * @return the CREATED_BY
     */
    public String getCreatedBy() {
        return (String) getAttributeInternal(CREATEDBY);
    }

    /**
     * Sets <code>value</code> as attribute value for CREATED_BY using the alias name CreatedBy.
     * @param value value to set the CREATED_BY
     */
    public void setCreatedBy(String value) {
        setAttributeInternal(CREATEDBY, value);
    }

    /**
     * Gets the attribute value for CREATION_DATE using the alias name CreationDate.
     * @return the CREATION_DATE
     */
    public Timestamp getCreationDate() {
        return (Timestamp) getAttributeInternal(CREATIONDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for CREATION_DATE using the alias name CreationDate.
     * @param value value to set the CREATION_DATE
     */
    public void setCreationDate(Timestamp value) {
        setAttributeInternal(CREATIONDATE, value);
    }

    /**
     * Gets the attribute value for LAST_UPDATED_BY using the alias name LastUpdatedBy.
     * @return the LAST_UPDATED_BY
     */
    public String getLastUpdatedBy() {
        return (String) getAttributeInternal(LASTUPDATEDBY);
    }

    /**
     * Sets <code>value</code> as attribute value for LAST_UPDATED_BY using the alias name LastUpdatedBy.
     * @param value value to set the LAST_UPDATED_BY
     */
    public void setLastUpdatedBy(String value) {
        setAttributeInternal(LASTUPDATEDBY, value);
    }

    /**
     * Gets the attribute value for LAST_UPDATE_DATE using the alias name LastUpdateDate.
     * @return the LAST_UPDATE_DATE
     */
    public Timestamp getLastUpdateDate() {
        return (Timestamp) getAttributeInternal(LASTUPDATEDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for LAST_UPDATE_DATE using the alias name LastUpdateDate.
     * @param value value to set the LAST_UPDATE_DATE
     */
    public void setLastUpdateDate(Timestamp value) {
        setAttributeInternal(LASTUPDATEDATE, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER, value);
    }

    /**
     * Gets the attribute value for QUOT_IDENTIFIER using the alias name QuotIdentifier.
     * @return the QUOT_IDENTIFIER
     */
    public String getQuotIdentifier() {
        return (String) getAttributeInternal(QUOTIDENTIFIER);
    }

    /**
     * Sets <code>value</code> as attribute value for QUOT_IDENTIFIER using the alias name QuotIdentifier.
     * @param value value to set the QUOT_IDENTIFIER
     */
    public void setQuotIdentifier(String value) {
        setAttributeInternal(QUOTIDENTIFIER, value);
    }

    /**
     * Gets the attribute value for ITEM_DESC using the alias name ItemDesc.
     * @return the ITEM_DESC
     */
    public String getItemDesc() {
        return (String) getAttributeInternal(ITEMDESC);
    }

    /**
     * Sets <code>value</code> as attribute value for ITEM_DESC using the alias name ItemDesc.
     * @param value value to set the ITEM_DESC
     */
    public void setItemDesc(String value) {
        setAttributeInternal(ITEMDESC, value);
    }

    /**
     * Gets the attribute value for ITEM_CD using the alias name ItemCd.
     * @return the ITEM_CD
     */
    public String getItemCd() {
        return (String) getAttributeInternal(ITEMCD);
    }

    /**
     * Sets <code>value</code> as attribute value for ITEM_CD using the alias name ItemCd.
     * @param value value to set the ITEM_CD
     */
    public void setItemCd(String value) {
        setAttributeInternal(ITEMCD, value);
    }
}

