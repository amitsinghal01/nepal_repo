package terms.sales.setup.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.el.ELContext;
import javax.el.ExpressionFactory;

import javax.el.ValueExpression;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

public class CreateInvoiceVoucherLinkBean {
    private RichTable bindingTable;
    private RichButton headerEditBinding;
    private RichPanelHeader getMyPageRootComponent;
   private RichButton detailCreateBinding;
     private RichOutputText bindingOutputText;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputComboboxListOfValues authCodeBinding;
    private RichInputText transfertypebinding;
    private RichSelectOneChoice itemTypeBinding;
    private RichInputComboboxListOfValues invTypeBinding;


    public CreateInvoiceVoucherLinkBean() {
    }

    public void DeletePopupbean(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
            {
            ADFUtils.findOperation("Delete").execute();
                OperationBinding op = (OperationBinding) ADFUtils.findOperation("Commit");
                Object rst = op.execute();
            System.out.println("Record Delete Successfully");
           
                if(op.getErrors().isEmpty())
                {
                FacesMessage Message = new FacesMessage("Record  Deleted Successfully");  
                Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                FacesContext fc = FacesContext.getCurrentInstance();  
                fc.addMessage(null, Message);
                 } 
             else
                {
                FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
                 } 
        } 
        AdfFacesContext.getCurrentInstance().addPartialTarget(bindingTable);
        
 
}

    public void setBindingTable(RichTable bindingTable) {
        this.bindingTable = bindingTable;
    }

    public RichTable getBindingTable() {
        return bindingTable;
    }

    public void recordSave(ActionEvent actionEvent) {
        ADFUtils.setLastUpdatedBy("InvoiceVoucherLinkHeaderVO1Iterator","LastUpdatedBy");

    Integer i=0;
                i=(Integer)bindingOutputText.getValue();
                System.out.println("EDIT TRANS VALUE"+i);
                   if(i.equals(0))
                       {
                          FacesMessage Message = new FacesMessage("Record Saved Successfully."); 
                          Message.setSeverity(FacesMessage.SEVERITY_INFO); 
                          FacesContext fc = FacesContext.getCurrentInstance(); 
                          fc.addMessage(null, Message);    
                          ADFUtils.findOperation("Commit").execute();
                       }
                       else
                       {
                           FacesMessage Message = new FacesMessage("Record Updated Successfully."); 
                           Message.setSeverity(FacesMessage.SEVERITY_INFO); 
                           FacesContext fc = FacesContext.getCurrentInstance(); 
                           fc.addMessage(null, Message);
                           ADFUtils.findOperation("Commit").execute();
                       }
     
    }

    private String resolvEl(String data) {

        FacesContext fc = FacesContext.getCurrentInstance();
        Application app = fc.getApplication();
        ExpressionFactory elFactory = app.getExpressionFactory();
        ELContext elContext = fc.getELContext();
        ValueExpression valueExp = elFactory.createValueExpression(elContext, data, Object.class);
        String Message=valueExp.getValue(elContext).toString();
        return Message;
    }
    
    private void cevmodecheck(){
           if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
               cevModeDisableComponent("V");
           }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
               cevModeDisableComponent("C");
           }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
               makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
               cevModeDisableComponent("E");
           }
       }
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
               try {
                   Method method1 =
                       component.getClass().getMethod("setDisabled", boolean.class);
                   if (method1 != null) {
                       method1.invoke(component, valueComponent);
                   }
               } catch (NoSuchMethodException e) {
                   try {
                       Method method =
                           component.getClass().getMethod("setReadOnly", boolean.class);
                       if (method != null) {
                           method.invoke(component, valueComponent);
                       }
                   } catch (Exception e1) {
                       // e.printStackTrace();//silently eat this exception.
                   }


               } catch (Exception e) {
                   // e.printStackTrace();//silently eat this exception.
               }
               List<UIComponent> childComponents = component.getChildren();
               for (UIComponent comp : childComponents) {
                   makeComponentHierarchyReadOnly(comp, valueComponent);
               }
           }
    //Set Fields and Button disable.
        public void cevModeDisableComponent(String mode) {
        //            FacesContext fctx = FacesContext.getCurrentInstance();
        //            ELContext elctx = fctx.getELContext();
        //            Application jsfApp = fctx.getApplication();
        //            //create a ValueExpression that points to the ADF binding layer
        //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
        //            //
        //            ValueExpression valueExpr = exprFactory.createValueExpression(
        //                                         elctx,
        //                                         "#{pageFlowScope.mode=='E'}",
        //                                          Object.class
        //                                         );
        //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
            if (mode.equals("E")) {
                //getItemTypeBinding().setDisabled(true);
                  getUnitCodeBinding().setDisabled(true);
                    getHeaderEditBinding().setDisabled(true);
                    getDetailCreateBinding().setDisabled(false);
                    if(getAuthCodeBinding().getValue()!=null)
                    {
                        getAuthCodeBinding().setDisabled(true);
                    }
                getInvTypeBinding().setDisabled(true);   
                getTransfertypebinding().setDisabled(true);
           
            } else if (mode.equals("C")) {
                
                getDetailCreateBinding().setDisabled(false);
                //getItemTypeBinding().setDisabled(true);
             getUnitCodeBinding().setDisabled(true);
           if(getAuthCodeBinding().getValue()!=null)
           {
               getAuthCodeBinding().setDisabled(true);
           }
             getTransfertypebinding().setDisabled(true);

                getHeaderEditBinding().setDisabled(true);
            } else if (mode.equals("V")) {
                getDetailCreateBinding().setDisabled(true);
            }
            
        }


    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        
        return headerEditBinding;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

   
    public void EditButtonAL(ActionEvent actionEvent) {
              cevmodecheck();
          }

    public void setDetailCreateBinding(RichButton detailCreateBinding) {
        this.detailCreateBinding = detailCreateBinding;
    }

    public RichButton getDetailCreateBinding() {
        return detailCreateBinding;
    }
   
    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
               return bindingOutputText;
    }


    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setAuthCodeBinding(RichInputComboboxListOfValues authCodeBinding) {
        this.authCodeBinding = authCodeBinding;
    }

    public RichInputComboboxListOfValues getAuthCodeBinding() {
        return authCodeBinding;
    }

    public void setTransfertypebinding(RichInputText transfertypebinding) {
        this.transfertypebinding = transfertypebinding;
    }

    public RichInputText getTransfertypebinding() {
        return transfertypebinding;
    }

    public void setItemTypeBinding(RichSelectOneChoice itemTypeBinding) {
        this.itemTypeBinding = itemTypeBinding;
    }

    public RichSelectOneChoice getItemTypeBinding() {
        return itemTypeBinding;
    }

    public void setInvTypeBinding(RichInputComboboxListOfValues invTypeBinding) {
        this.invTypeBinding = invTypeBinding;
    }

    public RichInputComboboxListOfValues getInvTypeBinding() {
        return invTypeBinding;
    }
}
