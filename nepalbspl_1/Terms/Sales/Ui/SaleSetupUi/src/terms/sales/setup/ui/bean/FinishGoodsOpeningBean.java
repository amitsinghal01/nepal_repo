package terms.sales.setup.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

public class FinishGoodsOpeningBean {
    private RichTable finishGoodsTablebinding;
    private RichInputComboboxListOfValues bindUnitDummy;
    private RichInputComboboxListOfValues bindUnitFG;
    private RichButton detailCreateBinding;
    private RichButton detailDeleteBinding;
    private RichPanelHeader getMyPageRootBinding;
    private RichOutputText bindingOutputText;
    private RichButton headerEditBinding;
    private RichButton populateButtonBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText unitDescriptionBinding;
    private RichInputText productDescriptionBinding;
    private RichInputText dummyUnitBinding;
    private RichInputText dummyProdDescriptionBinding;
    private RichInputText unitCodeDescriptionBinding;

    public FinishGoodsOpeningBean() {
    }

    public void populateFinishGoodsAC(ActionEvent actionEvent) {
      ADFUtils.findOperation("populateFGOpening").execute();
      
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        
        if(dialogEvent.getOutcome().name().equals("ok"))
                   {
               
                   ADFUtils.findOperation("Delete").execute();
                   FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                   Message.setSeverity(FacesMessage.SEVERITY_INFO);
                   FacesContext fc = FacesContext.getCurrentInstance();
                   fc.addMessage(null, Message);
   
               }
            
                      AdfFacesContext.getCurrentInstance().addPartialTarget(finishGoodsTablebinding);
    }
    
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
            try {
                Method method1 =
                    component.getClass().getMethod("setDisabled", boolean.class);
                if (method1 != null) {
                    method1.invoke(component, valueComponent);
                }
            } catch (NoSuchMethodException e) {
                try {
                    Method method =
                        component.getClass().getMethod("setReadOnly", boolean.class);
                    if (method != null) {
                        method.invoke(component, valueComponent);
                    }
                } catch (Exception e1) {
                    // e.printStackTrace();//silently eat this exception.
                }


            } catch (Exception e) {
                // e.printStackTrace();//silently eat this exception.
            }
            List<UIComponent> childComponents = component.getChildren();
            for (UIComponent comp : childComponents) {
                makeComponentHierarchyReadOnly(comp, valueComponent);
            }
        }
        
        
                        private void cevmodecheck(){
                            if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
                                makeComponentHierarchyReadOnly(this.getGetMyPageRootBinding(), true);
                                cevModeDisableComponent("V");
                            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
                                makeComponentHierarchyReadOnly(this.getGetMyPageRootBinding(), false);
                                cevModeDisableComponent("C");
                            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
                                makeComponentHierarchyReadOnly(this.getGetMyPageRootBinding(), false);
                                cevModeDisableComponent("E");
                            }
                        }
        
    public void cevModeDisableComponent(String mode) {
        if (mode.equals("E")) {            
              //  getDetailCreateBinding().setDisabled(true);
            // getDetailDeleteBinding().setDisabled(true);
            getDummyProdDescriptionBinding().setDisabled(true);
            getUnitCodeDescriptionBinding().setDisabled(true);
            getBindUnitDummy().setDisabled(true);
        } else if (mode.equals("C")) {
            
           getBindUnitDummy().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
//            getDetailCreateBinding().setDisabled(false);
//            getDetailDeleteBinding().setDisabled(false);
            getUnitCodeBinding().setDisabled(true);
            getDummyProdDescriptionBinding().setDisabled(true);
            getUnitCodeDescriptionBinding().setDisabled(true);
        } else if (mode.equals("V")) {
            getHeaderEditBinding().setDisabled(true);
            getPopulateButtonBinding().setDisabled(true);
//            getDetailCreateBinding().setDisabled(true);
//            getDetailDeleteBinding().setDisabled(true);
            getDummyProdDescriptionBinding().setDisabled(true);
            getUnitCodeDescriptionBinding().setDisabled(true);
            getBindUnitDummy().setDisabled(true);
        }
    }
    

    public void setFinishGoodsTablebinding(RichTable finishGoodsTablebinding) {
        this.finishGoodsTablebinding = finishGoodsTablebinding;
    }

    public RichTable getFinishGoodsTablebinding() {
        return finishGoodsTablebinding;
    }

    public void getUnitCode(ValueChangeEvent vce){
        
    }

    public void setBindUnitDummy(RichInputComboboxListOfValues bindUnitDummy) {
        this.bindUnitDummy = bindUnitDummy;
    }

    public RichInputComboboxListOfValues getBindUnitDummy() {
        return bindUnitDummy;
    }

    public void setBindUnitFG(RichInputComboboxListOfValues bindUnitFG) {
        this.bindUnitFG = bindUnitFG;
    }

    public RichInputComboboxListOfValues getBindUnitFG() {
        return bindUnitFG;
    }

    public void createFlowMode(ActionEvent actionEvent) {
        ADFUtils.findOperation("Create").execute();
    }

    public void setDetailCreateBinding(RichButton detailCreateBinding) {
        this.detailCreateBinding = detailCreateBinding;
    }

    public RichButton getDetailCreateBinding() {
        return detailCreateBinding;
    }

    public void setDetailDeleteBinding(RichButton detailDeleteBinding) {
        this.detailDeleteBinding = detailDeleteBinding;
    }

    public RichButton getDetailDeleteBinding() {
        return detailDeleteBinding;
    }

    public void setGetMyPageRootBinding(RichPanelHeader getMyPageRootBinding) {
        this.getMyPageRootBinding = getMyPageRootBinding;
    }

    public RichPanelHeader getGetMyPageRootBinding() {
        return getMyPageRootBinding;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        cevmodecheck();
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        return bindingOutputText;
    }

    public void editbuttonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setPopulateButtonBinding(RichButton populateButtonBinding) {
        this.populateButtonBinding = populateButtonBinding;
    }

    public RichButton getPopulateButtonBinding() {
        return populateButtonBinding;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setUnitDescriptionBinding(RichInputText unitDescriptionBinding) {
        this.unitDescriptionBinding = unitDescriptionBinding;
    }

    public RichInputText getUnitDescriptionBinding() {
        return unitDescriptionBinding;
    }

    public void setProductDescriptionBinding(RichInputText productDescriptionBinding) {
        this.productDescriptionBinding = productDescriptionBinding;
    }

    public RichInputText getProductDescriptionBinding() {
        return productDescriptionBinding;
    }

    public void setDummyUnitBinding(RichInputText dummyUnitBinding) {
        this.dummyUnitBinding = dummyUnitBinding;
    }

    public RichInputText getDummyUnitBinding() {
        return dummyUnitBinding;
    }

    public void setDummyProdDescriptionBinding(RichInputText dummyProdDescriptionBinding) {
        this.dummyProdDescriptionBinding = dummyProdDescriptionBinding;
    }

    public RichInputText getDummyProdDescriptionBinding() {
        return dummyProdDescriptionBinding;
    }

    public void setUnitCodeDescriptionBinding(RichInputText unitCodeDescriptionBinding) {
        this.unitCodeDescriptionBinding = unitCodeDescriptionBinding;
    }

    public RichInputText getUnitCodeDescriptionBinding() {
        return unitCodeDescriptionBinding;
    }

    public void saveAL(ActionEvent actionEvent) {
        Integer i=0;
                    i=(Integer)bindingOutputText.getValue();
                    System.out.println("EDIT TRANS VALUE"+i);
                       if(i.equals(0))
                           {
                              FacesMessage Message = new FacesMessage("Record Saved Successfully."); 
                              Message.setSeverity(FacesMessage.SEVERITY_INFO); 
                              FacesContext fc = FacesContext.getCurrentInstance(); 
                              fc.addMessage(null, Message);    
                             
                           }
    }
}
     
        
        
//        System.out.println("Before");
//          ADFUtils.findOperation("CreateInsert2").execute();
//          System.out.println("After");
          
       /* ADFUtils.findOperation("ForCreate").execute(); */
     
  


//    public void forCreate(ActionEvent actionEvent) {
//        ADFUtils.findOperation("resetViewCriteria").execute();
//        ADFUtils.findOperation("CreateInsert").execute();
//        ADFUtils.findOperation("CompareWithFinYear").execute();
//    }

