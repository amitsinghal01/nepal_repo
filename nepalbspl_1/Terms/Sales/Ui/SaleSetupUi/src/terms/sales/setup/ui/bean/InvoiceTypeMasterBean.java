package terms.sales.setup.ui.bean;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

public class InvoiceTypeMasterBean {
    private RichTable invoiceTypeMasterbinding;
     private String editAction="V";
    private RichButton editbinding;

    public InvoiceTypeMasterBean() {
    }

    public void deletepopupdialogDL(DialogEvent dialogEvent) {
           
            if(dialogEvent.getOutcome().name().equals("ok"))
            {
            OperationBinding op = null;
            ADFUtils.findOperation("Delete").execute();
            op = (OperationBinding) ADFUtils.findOperation("Commit");
            if (getEditAction().equals("V")) {
            Object rst = op.execute();
            }
            System.out.println("Record Delete Successfully");
            if(op.getErrors().isEmpty()){
            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
            } else if (!op.getErrors().isEmpty())
            {
            OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
            Object rstr = opr.execute();
            FacesMessage Message = new FacesMessage("Record cannot be deleted. Child recordfound.");
            Message.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
            }
            }
            AdfFacesContext.getCurrentInstance().addPartialTarget(invoiceTypeMasterbinding);
            }
          


    public void setInvoiceTypeMasterbinding(RichTable invoiceTypeMasterbinding) {
        this.invoiceTypeMasterbinding = invoiceTypeMasterbinding;
    }

    public RichTable getInvoiceTypeMasterbinding() {
        return invoiceTypeMasterbinding;
    }



    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public void DisableButton(ActionEvent actionEvent) {
        ADFUtils.showMessage("Data can't be Modify", 2);
        //        if(editbinding.isDisabled()){
//            editbinding.setDisabled(true);
//            
//        }
    }

    public void setEditbinding(RichButton editbinding) {
        this.editbinding = editbinding;
    }

    public RichButton getEditbinding() {
        return editbinding;
    }
}

