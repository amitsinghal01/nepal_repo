package terms.sales.setup.ui.bean;

import javax.faces.component.UIComponent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;

import oracle.jbo.domain.Date;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.model.OperationBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

public class ScrapStockReceivingBean {
    private RichInputDate entryDateBinding;
    private RichTable scrapStoreReceivingTableBinding;
    private String editAction = "V";
    private Integer count=0;
    private RichOutputText bindingOutputText;
    private RichInputComboboxListOfValues prodCodeBinding;

    public ScrapStockReceivingBean() {
        System.out.println("Constructor called");
        count=0;
    }

    public void isEnableEdit(ActionEvent actionEvent) {
        // Add event code here...
        this.editAction = getEditAction();
    }

    public void saveAL(ActionEvent actionEvent) {
        Date EntryDate = (Date) entryDateBinding.getValue();
        System.out.println("Entry Date:"+EntryDate);
        OperationBinding op = (OperationBinding) ADFUtils.findOperation("generateEntryNoScarpStockReceiving");
        Object obj = op.execute();
        OperationBinding op1 = (OperationBinding) ADFUtils.findOperation("Commit");
        Object obj1 = op1.execute();
        count=0;
    }

    public void setEntryDateBinding(RichInputDate entryDateBinding) {
        this.entryDateBinding = entryDateBinding;
    }

    public RichInputDate getEntryDateBinding() {
        return entryDateBinding;
    }

    public void popUpDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            OperationBinding op = null;
            ADFUtils.findOperation("Delete").execute();
            op = (OperationBinding) ADFUtils.findOperation("Commit");
            if (getEditAction().equals("V")) {
                Object rst = op.execute();
            }
            System.out.println("Record Delete Successfully");
            if (op.getErrors().isEmpty()) {
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            } else if (!op.getErrors().isEmpty()) {
                OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
                Object rstr = opr.execute();
                FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(scrapStoreReceivingTableBinding);
    }

    public void setScrapStoreReceivingTableBinding(RichTable scrapStoreReceivingTableBinding) {
        this.scrapStoreReceivingTableBinding = scrapStoreReceivingTableBinding;
    }

    public RichTable getScrapStoreReceivingTableBinding() {
        return scrapStoreReceivingTableBinding;
    }

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        return bindingOutputText;
    }


    public void createInsertAL(ActionEvent actionEvent) {
        if(count==0){
        ADFUtils.findOperation("CreateInsert").execute();
        count=count+1;
        }else{
            FacesMessage Message = new FacesMessage("Only one record will save at a time.");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
        }
    }

    public void cancelAL(ActionEvent actionEvent) {
         ADFUtils.findOperation("Rollback").execute();
         count=0;
    }

   

    public void setProdCodeBinding(RichInputComboboxListOfValues prodCodeBinding) {
        this.prodCodeBinding = prodCodeBinding;
    }

    public RichInputComboboxListOfValues getProdCodeBinding() {
        return prodCodeBinding;
    }
}
