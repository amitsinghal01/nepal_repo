package terms.sales.transaction.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.validator.ValidatorException;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class CaptiveConsumptionBean {
    private RichButton headerEditBinding;
    private RichTable tableBinding;
    private RichOutputText bindingOutputText;
    private RichPanelHeader getMyPageRootBinding;
    private RichInputDate entryDateBinding;
    private RichInputText entryNoBinding;
    private RichInputText stockQtyBinding;
    private RichInputComboboxListOfValues bindUnitBinding;
    private RichInputText bindUnitNameBinding;
    private RichInputText quantityBinding;

    public CaptiveConsumptionBean() {
    }

    public void createRecordAL(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert").execute();
    }

    public void saveAL(ActionEvent actionEvent)
    {             
            OperationBinding op = ADFUtils.findOperation("generateConsmpEntryNo");
            op.execute();
            System.out.println("Result is===> "+op.getResult());
            if(op.getResult() != null && op.getResult().toString().equalsIgnoreCase("E"))
            {
                ADFUtils.findOperation("Commit").execute();
                ADFUtils.showMessage(" Record Updated Successfully", 2);
                
            }
            else if (op.getResult() != null && op.getResult().toString() != ""  && op.getResult().equals("S"))
            {
                System.out.println("+++++ in the save mode+++++++++++");
                ADFUtils.findOperation("Commit").execute();
                ADFUtils.showMessage("Record Saved Successfully.Consumption Entry No.is "+ADFUtils.evaluateEL("#{bindings.EntryNo.inputValue}"), 2);
                
            }
            else if(op.getResult() != null && op.getResult().toString().equalsIgnoreCase("N"))
            {
                ADFUtils.showMessage("Consumption Entry No. could not be generated. Try Again !!", 0);
            }
    }

    public void onPageLoad() {
        ADFUtils.findOperation("filterCreate").execute();
    }

    public void editButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void deleteDialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                   {
               
                   ADFUtils.findOperation("Delete").execute();
                   FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                   Message.setSeverity(FacesMessage.SEVERITY_INFO);
                   FacesContext fc = FacesContext.getCurrentInstance();
                   fc.addMessage(null, Message);
               }
            AdfFacesContext.getCurrentInstance().addPartialTarget(tableBinding);
    }

    public void setTableBinding(RichTable tableBinding) {
        this.tableBinding = tableBinding;
    }

    public RichTable getTableBinding() {
        return tableBinding;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        cevmodecheck();
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        return bindingOutputText;
    }
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 =
                component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method =
                    component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }
        

    private void cevmodecheck(){
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
            makeComponentHierarchyReadOnly(this.getGetMyPageRootBinding(), true);
            cevModeDisableComponent("V");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootBinding(), false);
            cevModeDisableComponent("C");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootBinding(), false);
            cevModeDisableComponent("E");
        }
    }
        
    public void cevModeDisableComponent(String mode) {
        if (mode.equals("E")) {            
            
            getEntryDateBinding().setDisabled(true);
            getEntryNoBinding().setDisabled(true);
            getStockQtyBinding().setDisabled(true);
            getBindUnitNameBinding().setDisabled(true);
            getBindUnitBinding().setDisabled(true);
            
        } else if (mode.equals("C")) {
            
//            getEntryDateBinding().setDisabled(true);
            getEntryNoBinding().setDisabled(true);
            getStockQtyBinding().setDisabled(true);
            getBindUnitNameBinding().setDisabled(true);
            getBindUnitBinding().setDisabled(true);

        } else if (mode.equals("V")) {
 
        }
    }

    public void setGetMyPageRootBinding(RichPanelHeader getMyPageRootBinding) {
        this.getMyPageRootBinding = getMyPageRootBinding;
    }

    public RichPanelHeader getGetMyPageRootBinding() {
        return getMyPageRootBinding;
    }

    public void setEntryDateBinding(RichInputDate entryDateBinding) {
        this.entryDateBinding = entryDateBinding;
    }

    public RichInputDate getEntryDateBinding() {
        return entryDateBinding;
    }

    public void setEntryNoBinding(RichInputText entryNoBinding) {
        this.entryNoBinding = entryNoBinding;
    }

    public RichInputText getEntryNoBinding() {
        return entryNoBinding;
    }

    public void setStockQtyBinding(RichInputText stockQtyBinding) {
        this.stockQtyBinding = stockQtyBinding;
    }

    public RichInputText getStockQtyBinding() {
        return stockQtyBinding;
    }

    public void setBindUnitBinding(RichInputComboboxListOfValues bindUnitBinding) {
        this.bindUnitBinding = bindUnitBinding;
    }

    public RichInputComboboxListOfValues getBindUnitBinding() {
        return bindUnitBinding;
    }

    public void setBindUnitNameBinding(RichInputText bindUnitNameBinding) {
        this.bindUnitNameBinding = bindUnitNameBinding;
    }

    public RichInputText getBindUnitNameBinding() {
        return bindUnitNameBinding;
    }

    public void quantityValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        BigDecimal obj = (BigDecimal) object;
        
        BigDecimal stockQty = new BigDecimal(0);
        if(stockQtyBinding.getValue() != null) 
        {
           stockQty = (BigDecimal)stockQtyBinding.getValue();
        }
        if(obj!=null)
        {
            if(obj.compareTo(stockQty)==1)
            {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Quantity cannot be Greater than Stock Qty.", null));
            }
        }

    }

    public void setQuantityBinding(RichInputText quantityBinding) {
        this.quantityBinding = quantityBinding;
    }

    public RichInputText getQuantityBinding() {
        return quantityBinding;
    }
}
