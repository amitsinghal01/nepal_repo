package terms.sales.transaction.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;
import javax.faces.validator.ValidatorException;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectBooleanCheckbox;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.ViewObject;

public class CreateCaptiveStockStatusBean {
    private RichOutputText outputTextBinding;
    private RichPanelHeader getMyPageRootBinding;
    private RichInputComboboxListOfValues productCodeBinding;
    private RichInputText entryNoBinding;
    private RichInputDate entryDateBinding;
    private RichInputText stockQtyBinding;
    private RichInputText forCaptiveBinding;
    private RichInputText forDespatchBinding;
    private RichInputComboboxListOfValues approvedByBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText unitNameBinding;
    private RichTable captiveStockStatusTableBinding;
    private RichButton headerEditBinding;
    private RichSelectBooleanCheckbox transCheckBinding;

    public CreateCaptiveStockStatusBean() {
    }

    public void generateEntryNoSaveAL(ActionEvent actionEvent) {
        DCIteratorBinding dciter =
                    (DCIteratorBinding) ADFUtils.findIterator("CaptiveStockStatusVO1Iterator");
        ViewObject voh = dciter.getViewObject();
        
//        OperationBinding op1=ADFUtils.findOperation("checkValueForCaptiveStock");
//        op1.execute();
        
//        ADFUtils.findOperation("checkValueForCaptiveStock").execute();
        OperationBinding op = ADFUtils.findOperation("generateEntryNoForCaptiveStockStatus");
        op.execute();
        System.out.println("Result is===> "+op.getResult());
        if(op.getResult() != null && op.getResult().toString().equalsIgnoreCase("E"))
        {
//            ADFUtils.findOperation("checkValueForCaptiveStock").execute();
            ADFUtils.findOperation("Commit").execute();
            ADFUtils.showMessage("Record Updated Successfully.", 2);
            
        }
        else if (op.getResult() != null && op.getResult().toString() != ""  && op.getResult().equals("S"))
        {
            System.out.println("+++++ in the save mode+++++++++++");
            ADFUtils.findOperation("Commit").execute();
            Row row=(Row)ADFUtils.evaluateEL("#{bindings.CaptiveStockStatusVO1Iterator.currentRow}");
            String entryNo =(String)row.getAttribute("EntryNo"); 
            System.out.println("ENTRY NO ###### " + entryNo);
            ADFUtils.showMessage("Record Saved Successfully.Captive Stock Status Entry No. is " + entryNo, 2);
            
        }
        else if(op.getResult() != null && op.getResult().toString().equalsIgnoreCase("N"))
        {
            ADFUtils.showMessage("Captive Stock Status Entry No. could not be generated. Try Again !!", 0);
        }
    }

    public void setOutputTextBinding(RichOutputText outputTextBinding) {
        cevmodecheck();
        this.outputTextBinding = outputTextBinding;
    }

    public RichOutputText getOutputTextBinding() {
        return outputTextBinding;
    }

    public void setGetMyPageRootBinding(RichPanelHeader getMyPageRootBinding) {
        this.getMyPageRootBinding = getMyPageRootBinding;
    }

    public RichPanelHeader getGetMyPageRootBinding() {
        return getMyPageRootBinding;
    }

    public void setProductCodeBinding(RichInputComboboxListOfValues productCodeBinding) {
        this.productCodeBinding = productCodeBinding;
    }

    public RichInputComboboxListOfValues getProductCodeBinding() {
        return productCodeBinding;
    }

    public void setEntryNoBinding(RichInputText entryNoBinding) {
        this.entryNoBinding = entryNoBinding;
    }

    public RichInputText getEntryNoBinding() {
        return entryNoBinding;
    }

    public void setEntryDateBinding(RichInputDate entryDateBinding) {
        this.entryDateBinding = entryDateBinding;
    }

    public RichInputDate getEntryDateBinding() {
        return entryDateBinding;
    }

    public void setStockQtyBinding(RichInputText stockQtyBinding) {
        this.stockQtyBinding = stockQtyBinding;
    }

    public RichInputText getStockQtyBinding() {
        return stockQtyBinding;
    }

    public void setForCaptiveBinding(RichInputText forCaptiveBinding) {
        this.forCaptiveBinding = forCaptiveBinding;
    }

    public RichInputText getForCaptiveBinding() {
        return forCaptiveBinding;
    }

    public void setForDespatchBinding(RichInputText forDespatchBinding) {
        this.forDespatchBinding = forDespatchBinding;
    }

    public RichInputText getForDespatchBinding() {
        return forDespatchBinding;
    }

    public void editButtonAL(ActionEvent actionEvent) {
        System.out.println("INSIDE ####V ");
        
        Row row=(Row)ADFUtils.evaluateEL("#{bindings.CaptiveStockStatusVO1Iterator.currentRow}");
        String approvedBy =(String)row.getAttribute("ApprovedBy"); 
        
        if(approvedBy != null) {
            System.out.println("Approved By ###### " + approvedByBinding.getValue());
            ADFUtils.setEL("#{pageFlowScope.mode}","V");
            ADFUtils.showMessage("Approved Captive Stock cannot be modified.", 0);
        }
        else {
            cevmodecheck();
        }
//        cevmodecheck();
    }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 =
                component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method =
                    component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }
        

    private void cevmodecheck(){
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
            makeComponentHierarchyReadOnly(this.getGetMyPageRootBinding(), true);
            cevModeDisableComponent("V");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootBinding(), false);
            cevModeDisableComponent("C");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootBinding(), false);
            cevModeDisableComponent("E");
        }
    }
    
     public void cevModeDisableComponent(String mode) {
        if (mode.equals("E")) {            
            
//            getEntryDateBinding().setDisabled(true);
            getEntryNoBinding().setDisabled(true);
            getStockQtyBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
            getUnitNameBinding().setDisabled(true);
            getApprovedByBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);
            
        } else if (mode.equals("C")) {
            
    //            getEntryDateBinding().setDisabled(true);
            getEntryNoBinding().setDisabled(true);
            getStockQtyBinding().setDisabled(true);
            getApprovedByBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
            getUnitNameBinding().setDisabled(true);
            getApprovedByBinding().setDisabled(true);
            getTransCheckBinding().setDisabled(true);
            getHeaderEditBinding().setDisabled(true);

        } else if (mode.equals("V")) {
    
        }
    }
     
    public void onPageLoad() {
        ADFUtils.findOperation("filterCreateForCaptiveStockStatus").execute();
    }

    public void forDespatchValidator(FacesContext facesContext, UIComponent uIComponent, Object ob) {
        BigDecimal stockQty = (BigDecimal)stockQtyBinding.getValue();
        BigDecimal forCaptive = (BigDecimal)forCaptiveBinding.getValue();
        
        BigDecimal total = forCaptive.add((BigDecimal) ob);
        
        BigDecimal forDespatch = (BigDecimal) ob;
        
        if(ob != null) {
            if(total.compareTo(stockQty) == 1 || total.compareTo(stockQty) == -1) {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "For Captive and For Despatch cannot be Greater or less than Stock Qty.", null));
            }
        }
    }

    public void setApprovedByBinding(RichInputComboboxListOfValues approvedByBinding) {
        this.approvedByBinding = approvedByBinding;
    }

    public RichInputComboboxListOfValues getApprovedByBinding() {
        return approvedByBinding;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setUnitNameBinding(RichInputText unitNameBinding) {
        this.unitNameBinding = unitNameBinding;
    }

    public RichInputText getUnitNameBinding() {
        return unitNameBinding;
    }

    public void deleteDialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
        {
           ADFUtils.findOperation("Delete").execute();
           FacesMessage Message = new FacesMessage("Record Deleted Successfully");
           Message.setSeverity(FacesMessage.SEVERITY_INFO);
           FacesContext fc = FacesContext.getCurrentInstance();
           fc.addMessage(null, Message);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(captiveStockStatusTableBinding);
    }

    public void setCaptiveStockStatusTableBinding(RichTable captiveStockStatusTableBinding) {
        this.captiveStockStatusTableBinding = captiveStockStatusTableBinding;
    }

    public RichTable getCaptiveStockStatusTableBinding() {
        return captiveStockStatusTableBinding;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setTransCheckBinding(RichSelectBooleanCheckbox transCheckBinding) {
        this.transCheckBinding = transCheckBinding;
    }

    public RichSelectBooleanCheckbox getTransCheckBinding() {
        return transCheckBinding;
    }

    public void checkVCL(ValueChangeEvent vce) {
        System.out.println("INSIDE checkVCL" + vce.getNewValue());
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        
        if(vce.getNewValue()!=null && vce.getNewValue().equals(true)) {
            System.out.println("CHECK VCE ### ");
            approvedByBinding.setValue(ADFUtils.resolveExpression("#{pageFlowScope.empCode}")!=null?terms.sales.transaction.model.bean.ADFUtils.resolveExpression("#{pageFlowScope.empCode}"):"SWE161");
        }
    }

    public void forCaptiveValidator(FacesContext facesContext, UIComponent uIComponent, Object ob) {
        BigDecimal stockQty = (BigDecimal)stockQtyBinding.getValue();
        BigDecimal forDespatch = (BigDecimal)forDespatchBinding.getValue();
        BigDecimal object = (BigDecimal) ob;
        
        BigDecimal total = object.subtract(stockQty);
        
//        BigDecimal forDespatch = (BigDecimal) ob;
        
        if(object != null) {
//            setForDespatchBinding(total);
            if(total.compareTo(stockQty) == 1 || total.compareTo(stockQty) == -1) {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "For Captive and For Despatch cannot be Greater or less than Stock Qty.", null));
            }
        }

    }

    public void forCaptiveVCL(ValueChangeEvent vce) {
        System.out.println("INSIDE forCaptiveVCL" + vce.getNewValue());
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        
        BigDecimal stockQty = (BigDecimal)stockQtyBinding.getValue();
        BigDecimal forDespatch = (BigDecimal)forDespatchBinding.getValue();
        BigDecimal object = (BigDecimal) vce.getNewValue();
        
        if(vce.getNewValue() != null) {
            BigDecimal calculate = stockQty.subtract(object);
            System.out.println("Value " + calculate);
            
            forDespatchBinding.setValue(calculate);
        }
    }
}
