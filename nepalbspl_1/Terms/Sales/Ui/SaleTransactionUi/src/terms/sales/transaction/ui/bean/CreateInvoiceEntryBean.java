package terms.sales.transaction.ui.bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import javax.faces.validator.ValidatorException;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichSelectBooleanCheckbox;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichPanelLabelAndMessage;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.nav.RichCommandLink;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.jbo.domain.Number;

import java.sql.Timestamp;

import java.text.ParseException;

import java.util.Calendar;

import java.util.Date;

import java.util.List;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import javax.faces.validator.ValidatorException;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;

import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.adf.view.rich.util.ResetUtils;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

import oracle.jbo.RowSetIterator;
import oracle.jbo.ViewObject;

import oracle.security.xml.ws.secconv.wssx.bindings.Identifier;

import org.apache.myfaces.trinidad.component.UIXSwitcher;

import org.apache.myfaces.trinidad.event.DisclosureEvent;

import org.apache.myfaces.trinidad.model.UploadedFile;

import terms.sales.transaction.model.view.PendingSalesOrderDetailsVVORowImpl;

public class CreateInvoiceEntryBean {


    private RichPopup invoiceDeatilPopup;
    private RichPopup invoiceDetaiDetailPopup;
    private RichColumn prodCodeBinding;
    private RichInputComboboxListOfValues invoiceTypeBinding;
    private RichInputText poBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputComboboxListOfValues custCodeBinding;
    private RichInputDate invoiceIssueDateTimeDialogDL;
    private RichInputDate invoiceIssueDateTimeBinding;
    private RichInputDate removalDateTimeBinding;
    private RichInputDate dateBinding;
    private RichInputDate dueDateBinding;
    private RichInputText grossAmountBinding;
    private UIXSwitcher switcherBinding;
    private RichPopup soDetailPopUpBinding;
    private RichPopup daNoPopUpBinding;
    private RichInputText bindPendingQty;
    private RichOutputText stockQtyBinding;
    private RichInputText dispatchStkQtyBinding;
    private RichInputText dispatchPendQtyBinding;
    private RichButton selectOrdersBtBinding;
    private RichButton selectDispatchBtBinding;
    private RichInputText quantityBinding;
    private RichInputText grossWtBinding;
    private RichInputText priceBinding;
    private RichInputText custMrpBinding;
    private RichInputText toolCostBinding;
    private RichInputText packQtyBinding;
    private RichButton addButDetailBinding;
    private RichInputText bsrStockBinding;
    private RichOutputText totalInvoiceQtyBinding;
    private RichInputText identifierBinding;
    private RichInputText voucherBinding;
    private RichInputText unitNameBinding;
    private RichInputText invDescBinding;
    private RichSelectOneChoice docTypeBinding;
    private RichInputText consigneeNameBinding;
    private RichInputText customerNameBinding;
    private RichInputText cityNameBinding;
    private RichInputText transporterNameBinding;
    private RichInputText currencyCodeBinding;
    private RichInputText approvedByNameBind;
    private RichInputText currencyNameBind;
    private RichInputText ackNoBinding;
    private RichInputText amdNoBinding;
    private RichInputText netAmountBinding;
    private RichInputText discountBinding;
    private RichInputText grossAmtBinding;
    private RichInputText amtFCBinding;
    private RichInputText packingAmtBind;
    private RichInputText sgstRateBind;
    private RichInputText sgstAmtBind;
    private RichInputText cgstRateBind;
    private RichInputText cgstAmtBind;
    private RichInputText igstRateBind;
    private RichInputText igstAmtBind;
    private RichInputText lessToolDisAmtSum;
    private RichInputText sgstAmtSum;
    private RichInputText cgstAmtSum;
    private RichInputText igstAmtSum;
    private RichInputText lessDiscountSum;
    private RichInputText tcSPerSum;
    private RichInputText tcsAmtSum;
    private RichInputText otherSum;
    private RichInputText netAmtSum;
    private RichInputText grossAmtFCSum;
    private RichInputText netAmtFCSum;
    private RichInputDate vouDateSum;
    private RichInputText grNoSum;
    private RichInputDate grDateSum;
    private RichSelectOneChoice statusBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichButton deleteButtonBinding;
    private RichPanelLabelAndMessage bindingOutputText;
    private RichShowDetailItem invoiceDetailTabBinding;
    private RichShowDetailItem itemDetailtabBinding;
    private RichShowDetailItem invoiceSummaryBinding;
    private RichInputComboboxListOfValues approveCodeBinding;
    private RichInputComboboxListOfValues transporterCodeBinding;
    private RichInputText openFixBinding;
    private RichInputText openFixDispatchBinding;
    private RichInputText daNumberBinding;
    private RichInputText disPerBinding;
    private RichSelectOneChoice itemTypeBinding;
    private RichInputComboboxListOfValues productCodeBinding;
    private RichPopup deletePopupBinding;
    private RichTable detailTableBinding;
    private String mode_value = "C";
    private String value_check = "N";
    private RichInputComboboxListOfValues issueSlipNoBinding;
    private RichInputText dispAdviceNoBinding;
    private RichInputComboboxListOfValues transporterCdBinding;
    private RichInputText destinationBinding;
    private String da_and_amd = "";
    private RichShowDetailItem otherDetailBinding;
    private RichInputText invSubTypeBinding;
    private RichInputText addIdentifierBinding;
    private RichPopup dolDispatchBinding;
    private RichButton dolDispatchNoButtonBinding;
    private RichInputComboboxListOfValues invHeadTypeBinding;
    private RichInputText noOfCasesBinding;
    private RichTable dolDispatchTableBinding;
    private RichSelectBooleanCheckbox doltransCheckBoxBinding;
    private RichInputComboboxListOfValues preparedByBinding;
    private RichShowDetailItem additionalInvoiceOneTabBinding;
    private RichShowDetailItem additionalInvoiceTwoTabBinding;
    private RichInputComboboxListOfValues invTransBinding;
    private RichButton populateButtonBinding;
    private RichInputText insCompNameBinding;
    private RichInputDate validFromDateBinding;
    private RichInputDate fcrDateBinding;
    private RichInputText authNoBinding;
    private RichInputDate authDateBinding;
    private RichInputText balQuantityBinding;
    private RichSelectOneChoice invSubType1Binding;
    private RichPopup exportDetailPopupBinding;
    private RichInputComboboxListOfValues fcrNoBinding;
    private RichInputText custPoNoBinding;
    private RichInputDate custPoDateBinding;
    private RichInputText custProdDesBinding;
    private RichInputDate ewayDateBinding;
    private RichInputComboboxListOfValues lutSrNoBinding;
    private RichInputText pathBinding;
    private RichInputText docSeqNoBinding;
    private RichInputText unitCodeDocRefBinding;
    private RichInputText refDocNoBinding;
    private RichInputText refDocTypeBinding;
    private RichInputText docFileNameBinding;
    private RichInputDate refDocDateBinding;
    private RichTable docRefTableBinding;
    private RichShowDetailItem refDocTabBinding;
    private RichInputText cessAmountBinding;
    private RichCommandLink downloadLinkBinding;
    BigDecimal tcsPer = new BigDecimal(0);
    BigDecimal tcsAmt = new BigDecimal(0);
    private RichSelectBooleanCheckbox doltransCheckBoxBinding1;
    private RichInputText batchNumberBinding;
    private RichInputText expDtBinding;
    private RichInputText mfgDtBinding;
    private RichInputText invDateNepalBinding;
    private RichTable itemsummTableBinding;
    private RichInputText frExcHeaderBinding;
    private RichInputText frVatHeaderBinding;
    private RichInputText totAmtInvoiceHeaderBinding;
    private RichInputText roundOffHeaderBinding;
    private RichInputText packUomBinding;
    private RichInputText uomBinding;
    private RichInputText netWeightBinding;

    public CreateInvoiceEntryBean() {


    }

    public void saveButtonAL(ActionEvent actionEvent) {
        if ((Long) ADFUtils.evaluateEL("#{bindings.InvoiceEntryDetailVO1Iterator.estimatedRowCount}") == 0) {
            ADFUtils.showMessage(" Fill Item Details", 0);
        } else {
            ADFUtils.setLastUpdatedBy("InvoiceEntryHeaderVO1Iterator", "LastUpdatedBy");
            tcsPer = (BigDecimal) tcSPerSum.getValue();
            tcsAmt = (BigDecimal) tcsAmtSum.getValue();
            OperationBinding binding1 = ADFUtils.findOperation("checkAllValidations");
            binding1.execute();


            if (binding1.getResult() != null && binding1.getResult().toString().equalsIgnoreCase("Y")) {
                OperationBinding binding = ADFUtils.findOperation("generateSalesInvcNo");
                binding.execute();

                if (binding.getResult() != null && binding.getResult().toString().equalsIgnoreCase("Y")) {
                    binding = ADFUtils.findOperation("calculateAmountDetails");
                    binding.execute();
                    ADFUtils.findOperation("insertIntoSalesMapping").execute();
                }

                String param = mode_value;


                if (binding.getResult() != null && binding.getResult().toString().equalsIgnoreCase("Y") &&
                    param.equalsIgnoreCase("false")) {
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message = new FacesMessage("Record Updated Successfully.");
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);
                } else if (binding.getResult() != null && binding.getResult().toString().equalsIgnoreCase("Y") &&
                           param.equalsIgnoreCase("true")) {
                    mode_value = "false";
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message = new FacesMessage();
                    Message =
                        new FacesMessage("Record Saved Successfully. New invoice no. is " +
                                         getIdentifierBinding().getValue() + ".");
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);
                } else if (binding.getResult() != null && binding.getResult().toString().equalsIgnoreCase("N")) {
                    ADFUtils.findOperation("Rollback").execute();
                    FacesMessage Message = new FacesMessage("Error found to save data. Try Again!!");
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);
                }
            }
            tcSPerSum.setValue(tcsPer);
            tcsAmtSum.setValue(tcsAmt);
        }

        OperationBinding bindingN = ADFUtils.findOperation("callInvoiceApi");
        bindingN.execute();

    }


    public String resolvEl(String data) {
        FacesContext fc = FacesContext.getCurrentInstance();
        Application app = fc.getApplication();
        ExpressionFactory elFactory = app.getExpressionFactory();
        ELContext elContext = fc.getELContext();
        ValueExpression valueExp = elFactory.createValueExpression(elContext, data, Object.class);
        String Message = valueExp.getValue(elContext).toString();
        return Message;
    }


    public void itemDetailAL(ActionEvent actionEvent) {
        AdfFacesContext.getCurrentInstance().addPartialTarget(switcherBinding);
        OperationBinding op = ADFUtils.findOperation("finYearInvoiceEntry");
        Object rst = op.execute();
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        getInvoiceDeatilPopup().show(hints);

    }

    public void setInvoiceDeatilPopup(RichPopup invoiceDeatilPopup) {
        this.invoiceDeatilPopup = invoiceDeatilPopup;
    }

    public RichPopup getInvoiceDeatilPopup() {
        return invoiceDeatilPopup;
    }


    public void setInvoiceDetaiDetailPopup(RichPopup invoiceDetaiDetailPopup) {
        this.invoiceDetaiDetailPopup = invoiceDetaiDetailPopup;
    }

    public RichPopup getInvoiceDetaiDetailPopup() {
        return invoiceDetaiDetailPopup;
    }


    public void setPoBinding(RichInputText poBinding) {
        this.poBinding = poBinding;
    }

    public RichInputText getPoBinding() {
        return poBinding;
    }


    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setCustCodeBinding(RichInputComboboxListOfValues custCodeBinding) {
        this.custCodeBinding = custCodeBinding;
    }

    public RichInputComboboxListOfValues getCustCodeBinding() {
        return custCodeBinding;
    }

    public void prodCodeVCL(ValueChangeEvent vce) {
        // AdfFacesContext.getCurrentInstance().addPartialTarget(switcherBinding);

        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce != null) {
            if (poBinding.getValue() != null && (poBinding.getValue().equals("R"))) {
                {
                    AdfFacesContext.getCurrentInstance().addPartialTarget(switcherBinding);
                }
                RichPopup.PopupHints hints = new RichPopup.PopupHints();
                getInvoiceDetaiDetailPopup().show(hints);
            }
        }

        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        OperationBinding ob = (OperationBinding) ADFUtils.findOperation("RefreshRate");
        ob.getParamsMap().put("UnitCd", unitCodeBinding.getValue().toString());
        ob.getParamsMap().put("CustCd", custCodeBinding.getValue().toString());
        ob.execute();
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        ADFUtils.findOperation("pendingPoValues").execute();
    }


    public void quantityVCL(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        OperationBinding ob = (OperationBinding) ADFUtils.findOperation("RefreshRate");
        ob.getParamsMap().put("UnitCd", unitCodeBinding.getValue().toString());
        ob.getParamsMap().put("CustCd", custCodeBinding.getValue().toString());
        ob.execute();
    }

    public void priceVCL(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        OperationBinding ob = (OperationBinding) ADFUtils.findOperation("RefreshRate");
        ob.getParamsMap().put("UnitCd", unitCodeBinding.getValue().toString());
        ob.getParamsMap().put("CustCd", custCodeBinding.getValue().toString());
        ob.execute();
    }

    public void gstCodeVCL(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        OperationBinding ob = (OperationBinding) ADFUtils.findOperation("RefreshRate");
        ob.getParamsMap().put("UnitCd", unitCodeBinding.getValue().toString());
        ob.getParamsMap().put("CustCd", custCodeBinding.getValue().toString());
        ob.execute();
    }


    public void setInvoiceIssueDateTimeBinding(RichInputDate invoiceIssueDateTimeBinding) {
        this.invoiceIssueDateTimeBinding = invoiceIssueDateTimeBinding;
    }

    public RichInputDate getInvoiceIssueDateTimeBinding() {
        return invoiceIssueDateTimeBinding;
    }

    public void setRemovalDateTimeBinding(RichInputDate removalDateTimeBinding) {
        this.removalDateTimeBinding = removalDateTimeBinding;
    }

    public RichInputDate getRemovalDateTimeBinding() {
        return removalDateTimeBinding;
    }

    public void setDateBinding(RichInputDate dateBinding) {
        this.dateBinding = dateBinding;
    }

    public RichInputDate getDateBinding() throws ParseException {
        Object date = ADFUtils.convertAdToBs(ADFUtils.getTodayDate().toString());
        DCIteratorBinding dci = ADFUtils.findIterator("InvoiceEntryHeaderVO1Iterator");
        dci.getCurrentRow().setAttribute("DatesNepal", date);
        return dateBinding;
    }

    public void setDueDateBinding(RichInputDate dueDateBinding) {
        this.dueDateBinding = dueDateBinding;
    }

    public RichInputDate getDueDateBinding() {
        return dueDateBinding;
    }

    public void custCodeVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        ADFUtils.findOperation("dueDate").execute();


    }


    public void setGrossAmountBinding(RichInputText grossAmountBinding) {
        this.grossAmountBinding = grossAmountBinding;
    }

    public RichInputText getGrossAmountBinding() {
        return grossAmountBinding;
    }


    public void setSwitcherBinding(UIXSwitcher switcherBinding) {
        this.switcherBinding = switcherBinding;
    }

    public UIXSwitcher getSwitcherBinding() {
        return switcherBinding;
    }

    public void pagerefresh(ActionEvent actionEvent) {
        ADFUtils.findOperation("pendingPoValues").execute();
        AdfFacesContext.getCurrentInstance().addPartialTarget(switcherBinding);
    }

    public void populateDataToDetail(ActionEvent actionEvent) {
        OperationBinding op = ADFUtils.findOperation("editSelectTransMethod");
        Object rst = op.execute();

        actionEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        OperationBinding ob = (OperationBinding) ADFUtils.findOperation("RefreshRate");
        ob.getParamsMap().put("UnitCd", unitCodeBinding.getValue().toString());
        ob.getParamsMap().put("CustCd", custCodeBinding.getValue().toString());
        ob.execute();
        AdfFacesContext.getCurrentInstance().addPartialTarget(switcherBinding);
    }


    public void populateDataFromDetailAL(ActionEvent actionEvent) {

        System.out.println("After OK Button");
        {
            if (invoiceIssueDateTimeBinding.getValue() == null)
                System.out.println("After OK Button and in If invoice issue");
            {
                Timestamp t = new Timestamp(System.currentTimeMillis());
                invoiceIssueDateTimeBinding.setValue(t);
                System.out.println("Issue Date time" + t);
            }
            if (removalDateTimeBinding.getValue() == null)
                System.out.println("After OK Button and in If removal");
            {
                Timestamp t = new Timestamp(System.currentTimeMillis());
                removalDateTimeBinding.setValue(t);
                System.out.println("Issue Date time" + t);
            }

        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(switcherBinding);
        ADFUtils.findOperation("copyDatafromDtl").execute();

    }

    public void populateDataToDetailByD2(ActionEvent actionEvent) {
        OperationBinding op = ADFUtils.findOperation("editSelectTransMethodD2");
        Object rst = op.execute();

        actionEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        OperationBinding ob = (OperationBinding) ADFUtils.findOperation("RefreshRate");
        AdfFacesContext.getCurrentInstance().addPartialTarget(switcherBinding);
        ob.getParamsMap().put("UnitCd", unitCodeBinding.getValue().toString());
        ob.getParamsMap().put("CustCd", custCodeBinding.getValue().toString());
        ob.execute();
    }

    public String selectSalesOrderAction() {
        //OperationBinding binding1 = ADFUtils.findOperation("checkAllValidations");
        //binding1.execute();
        //if(binding1.getResult() != null && binding1.getResult().toString().equalsIgnoreCase("Y")){
        OperationBinding binding = ADFUtils.findOperation("filterVVOForProductDetail");
        binding.getParamsMap().put("filter_mode", "S");
        binding.execute();
        ADFUtils.showPopup(soDetailPopUpBinding);
        //}
        return null;
    }

    public void setSoDetailPopUpBinding(RichPopup soDetailPopUpBinding) {
        this.soDetailPopUpBinding = soDetailPopUpBinding;
    }

    public RichPopup getSoDetailPopUpBinding() {
        return soDetailPopUpBinding;
    }

    //    public void quantitychkValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
    //        System.out.println("bsrStockBindingbsrStockBinding:=>" + bsrStockBinding.getValue());
    //        if (object != null) {
    //
    //            if (!openFixBinding.getValue().equals("Open")) {
    //                if (object != null && ((BigDecimal) object).compareTo(new BigDecimal(0)) > 0) {
    //                    System.out.println("valuee of objct iss=" + object + " ,peding quanity iss= " +
    //                                       bindPendingQty.getValue());
    //                    if (((BigDecimal) object).compareTo((BigDecimal) bindPendingQty.getValue()) > 0)
    //                        throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
    //                                                                      "Quanity must be less than or equal to pending quantity.",
    //                                                                      ""));
    //                    if (((BigDecimal) object).compareTo((BigDecimal) stockQtyBinding.getValue()) > 0)
    //                        throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
    //                                                                      "Quanity must be less than or equal to total stock quanity.",
    //                                                                      ""));
    //                }
    //            } else {
    //                if (((BigDecimal) object).compareTo(((BigDecimal) bsrStockBinding.getValue())) == 1) {
    //                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
    //                                                                  "Open PO Quanity must be less than or equal to BSR Stock.",
    //                                                                  ""));
    //                }
    //            }
    //        }
    //    }


    public void quantitychkValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        System.out.println("bsrStockBindingbsrStockBinding:=>" + bsrStockBinding.getValue() + " openFixBinding: " +
                           openFixBinding.getValue());
        if (object != null) {

            if (!openFixBinding.getValue().equals("Open")) {
                if (object != null && ((BigDecimal) object).compareTo(new BigDecimal(0)) > 0) {
                    System.out.println("valuee of objct iss=" + object + " ,peding quanity iss= " +
                                       bindPendingQty.getValue());
                    if (((BigDecimal) object).compareTo((BigDecimal) bindPendingQty.getValue()) > 0)
                        throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                      "Quanity must be less than or equal to pending quantity.",
                                                                      ""));
                    if (((BigDecimal) object).compareTo((BigDecimal) stockQtyBinding.getValue()) > 0) {
                        OperationBinding opr = ADFUtils.findOperation("checkBSRStockTypeInvoiceEntry");
                        opr.getParamsMap().put("stock_type",
                                               ADFUtils.resolveExpression("#{bindings.StockType.inputValue}"));
                        opr.getParamsMap().put("unit_code", unitCodeBinding.getValue());
                        opr.getParamsMap().put("invType",
                                               ADFUtils.resolveExpression("#{bindings.InvHeadType.inputValue}"));
                        opr.execute();
                        if (opr.getResult() != null && opr.getResult().equals("Y")) {
                            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                          "Quanity must be less than or equal to total stock quanity.",
                                                                          ""));
                        }
                    }
                }
            } else {
                //               if(((BigDecimal)object).compareTo(((BigDecimal)bsrStockBinding.getValue()))==1)
                //               {
                //                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                //                                                                  "Open PO Quanity must be less than or equal to BSR Stock.",
                //                                                                  ""));
                //                }
            }
        }
    }


    public String SelectDispatchNumberAction() {
        //OperationBinding binding1 = ADFUtils.findOperation("checkAllValidations");
        //binding1.execute();
        //if(binding1.getResult() != null && binding1.getResult().toString().equalsIgnoreCase("Y")){
        OperationBinding binding = ADFUtils.findOperation("filterVVOForProductDetail");
        binding.getParamsMap().put("filter_mode", "D");
        binding.execute();
        da_and_amd = "";
        ADFUtils.showPopup(daNoPopUpBinding);
        //}
        return null;
    }

    public void setDaNoPopUpBinding(RichPopup daNoPopUpBinding) {
        this.daNoPopUpBinding = daNoPopUpBinding;
    }

    public RichPopup getDaNoPopUpBinding() {
        return daNoPopUpBinding;
    }

    public void soDetailDialogOkListener(DialogEvent dialogEvent) {
        int i = 0;
        //BigDecimal totalInvQty = (BigDecimal) getTotalInvoiceQtyBinding().getValue();

        BigDecimal totalInvQty =
            (BigDecimal) (getTotalInvoiceQtyBinding().getValue() != null ? getTotalInvoiceQtyBinding().getValue() :
                          new BigDecimal(0));
        System.out.println("totalInvQty====>" + totalInvQty);


        System.out.println("Total Invoice Value :- " + totalInvQty.compareTo(new BigDecimal(0)));
        if (totalInvQty.compareTo(new BigDecimal(0)) == 1) {
            OperationBinding binding = ADFUtils.findOperation("insertDataInToInvoiceDetails");
            binding.getParamsMap().put("source_mode", "SO");
            binding.execute();
            DCIteratorBinding dci = ADFUtils.findIterator("PendingSalesOrderDetailsVVOIterator");
            ViewObject vo = dci.getViewObject();
            RowSetIterator row = vo.createRowSetIterator(null);
            while (row.hasNext()) {
                Row r = row.next();
                String flag = (String) r.getAttribute("TransChkBox");
                if (flag != null && (flag.equals("Y") || flag.equals("true")) && i == 0) {
                    System.out.println("Inside flag condition Sale Order No: " + r.getAttribute("PoNo"));
                    OperationBinding op = ADFUtils.findOperation("populateMultiPaymentTermInvoice");
                    op.getParamsMap().put("soNo", (String) r.getAttribute("PoNo"));
                    op.getParamsMap().put("amdNo", (BigDecimal) r.getAttribute("AmndNo"));
                    op.execute();
                    if (op.getResult() != null && op.getResult().equals("N")) {
                        ADFUtils.showMessage("No Payment Term Specified for this Sale Order!", 1);
                    }

                    i++;
                }
            }
            row.closeRowSetIterator();

        } else {
            ADFUtils.findOperation("refreshDataInToInvoiceDetails").execute();
            ADFUtils.showMessage("Total invoice quantity must be greater then 0.", 1);
        }
    }

    public void setBindPendingQty(RichInputText bindPendingQty) {
        this.bindPendingQty = bindPendingQty;
    }

    public RichInputText getBindPendingQty() {
        return bindPendingQty;
    }

    public void setStockQtyBinding(RichOutputText stockQtyBinding) {
        this.stockQtyBinding = stockQtyBinding;
    }

    public RichOutputText getStockQtyBinding() {
        return stockQtyBinding;
    }

    public void dispatchAdvcNoDialogListener(DialogEvent dialogEvent) {
        OperationBinding binding = ADFUtils.findOperation("insertDataInToInvoiceDetails");
        binding.getParamsMap().put("source_mode", "DA");
        binding.execute();
        da_and_amd = "";
    }


    public void dispatchQtyValidatior(FacesContext facesContext, UIComponent uIComponent, Object object) { //31-JAN-19
        if (object != null) {

            if (!openFixDispatchBinding.equals("Open")) {
                if (object != null && ((BigDecimal) object).compareTo(new BigDecimal(0)) > 0) {
                    if (((BigDecimal) object).compareTo((BigDecimal) dispatchPendQtyBinding.getValue()) > 0) {
                        throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                      "Quanity must be less than or equal to pending quantity.",
                                                                      ""));
                    }
                    if (((BigDecimal) object).compareTo((BigDecimal) dispatchStkQtyBinding.getValue()) > 0) {
                        OperationBinding opr = ADFUtils.findOperation("checkBSRStockTypeInvoiceEntry");
                        opr.getParamsMap().put("stock_type",
                                               ADFUtils.resolveExpression("#{bindings.StockType.inputValue}"));
                        opr.getParamsMap().put("unit_code", unitCodeBinding.getValue());
                        opr.getParamsMap().put("invType",
                                               ADFUtils.resolveExpression("#{bindings.InvHeadType.inputValue}"));
                        opr.execute();
                        if (opr.getResult() != null && opr.getResult().equals("Y")) {
                            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                          "Quanity must be less than or equal to total stock quanity.",
                                                                          ""));
                        }
                    }
                }
            } else {
                if (((BigDecimal) object).compareTo(((BigDecimal) dispatchStkQtyBinding.getValue())) == 1) {
                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                  "Open PO Quanity must be less than or equal to Product Stock Qty.",
                                                                  ""));
                }
            }
        }

    }


    public void setDispatchStkQtyBinding(RichInputText dispatchStkQtyBinding) {
        this.dispatchStkQtyBinding = dispatchStkQtyBinding;
    }

    public RichInputText getDispatchStkQtyBinding() {
        return dispatchStkQtyBinding;
    }

    public void setDispatchPendQtyBinding(RichInputText dispatchPendQtyBinding) {
        this.dispatchPendQtyBinding = dispatchPendQtyBinding;
    }

    public RichInputText getDispatchPendQtyBinding() {
        return dispatchPendQtyBinding;
    }

    public String addItemDetailAction() {
        OperationBinding binding1 = ADFUtils.findOperation("checkAllValidations");
        binding1.execute();
        if (binding1.getResult() != null && binding1.getResult().toString().equalsIgnoreCase("Y")) {
            OperationBinding binding = ADFUtils.findOperation("CreateInsert");
            binding.execute();
        }
        return null;
    }

    public String deleteItemDetailAction() {
        OperationBinding binding = ADFUtils.findOperation("Delete");
        binding.execute();
        return null;
    }

    public void otherDetailsDisclousreListener(DisclosureEvent disclosureEvent) {
        OperationBinding binding = ADFUtils.findOperation("calculateAmountDetails");
        binding.execute();
    }

    public void invoiceHeadTypeVC(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        invoiceHeadTypeCheckingMethod();
    }

    public void setSelectOrdersBtBinding(RichButton selectOrdersBtBinding) {
        this.selectOrdersBtBinding = selectOrdersBtBinding;
    }

    public RichButton getSelectOrdersBtBinding() {
        return selectOrdersBtBinding;
    }

    public void setSelectDispatchBtBinding(RichButton selectDispatchBtBinding) {
        this.selectDispatchBtBinding = selectDispatchBtBinding;
    }

    public RichButton getSelectDispatchBtBinding() {
        return selectDispatchBtBinding;
    }

    public void setQuantityBinding(RichInputText quantityBinding) {
        this.quantityBinding = quantityBinding;
    }

    public RichInputText getQuantityBinding() {
        return quantityBinding;
    }

    public void setGrossWtBinding(RichInputText grossWtBinding) {
        this.grossWtBinding = grossWtBinding;
    }

    public RichInputText getGrossWtBinding() {
        return grossWtBinding;
    }

    public void setPriceBinding(RichInputText priceBinding) {
        this.priceBinding = priceBinding;
    }

    public RichInputText getPriceBinding() {
        return priceBinding;
    }

    public void setCustMrpBinding(RichInputText custMrpBinding) {
        this.custMrpBinding = custMrpBinding;
    }

    public RichInputText getCustMrpBinding() {
        return custMrpBinding;
    }

    public void setToolCostBinding(RichInputText toolCostBinding) {
        this.toolCostBinding = toolCostBinding;
    }

    public RichInputText getToolCostBinding() {
        return toolCostBinding;
    }

    public void setPackQtyBinding(RichInputText packQtyBinding) {
        this.packQtyBinding = packQtyBinding;
    }

    public RichInputText getPackQtyBinding() {
        return packQtyBinding;
    }

    public void setAddButDetailBinding(RichButton addButDetailBinding) {
        this.addButDetailBinding = addButDetailBinding;
    }

    public RichButton getAddButDetailBinding() {
        return addButDetailBinding;
    }

    public void setBsrStockBinding(RichInputText bsrStockBinding) {
        this.bsrStockBinding = bsrStockBinding;
    }

    public RichInputText getBsrStockBinding() {
        return bsrStockBinding;
    }

    public void setTotalInvoiceQtyBinding(RichOutputText totalInvoiceQtyBinding) {
        this.totalInvoiceQtyBinding = totalInvoiceQtyBinding;
    }

    public RichOutputText getTotalInvoiceQtyBinding() {
        return totalInvoiceQtyBinding;
    }

    public void setIdentifierBinding(RichInputText identifierBinding) {
        this.identifierBinding = identifierBinding;
    }

    public RichInputText getIdentifierBinding() {
        return identifierBinding;
    }

    public void setVoucherBinding(RichInputText voucherBinding) {
        this.voucherBinding = voucherBinding;
    }

    public RichInputText getVoucherBinding() {
        return voucherBinding;
    }

    public void gstCodeVCE(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        //        OperationBinding binding = (OperationBinding) ADFUtils.findOperation("refreshAmount1");
        //        binding.getParamsMap().put("pkeyid", 800);
        //        binding.getParamsMap().put("pkeyno", 1);
        //        binding.getParamsMap().put("punitcd", getUnitCodeBinding().getValue().toString());
        //        binding.execute();
        OperationBinding ob = (OperationBinding) ADFUtils.findOperation("refreshAmount2");
        ob.getParamsMap().put("pkeyid", 800);
        ob.getParamsMap().put("pkeyno", 1);
        ob.getParamsMap().put("punitcd", getUnitCodeBinding().getValue().toString());
        ob.execute();
        AdfFacesContext.getCurrentInstance().addPartialTarget(itemsummTableBinding);
    }

    public void itemDetailDisclosure(DisclosureEvent disclosureEvent) {
        // Add event code here...
        ADFUtils.findOperation("checkAllValidations").execute();

        try {
            if (invHeadTypeBinding.getValue() != null) {
                String type = invHeadTypeBinding.getValue().toString();
                System.out.println("Type is==>" + type);
                if (type.equals("H") || type.equals("A")) {
                    value_check = "N";
                    System.out.println("inside check value" + value_check);
                }
            }
        } catch (Exception e) {
            // TODO: Add catch code
            e.printStackTrace();
        }


    }

    public void setUnitNameBinding(RichInputText unitNameBinding) {
        this.unitNameBinding = unitNameBinding;
    }

    public RichInputText getUnitNameBinding() {
        return unitNameBinding;
    }

    public void setInvDescBinding(RichInputText invDescBinding) {
        this.invDescBinding = invDescBinding;
    }

    public RichInputText getInvDescBinding() {
        return invDescBinding;
    }

    public void setDocTypeBinding(RichSelectOneChoice docTypeBinding) {
        this.docTypeBinding = docTypeBinding;
    }

    public RichSelectOneChoice getDocTypeBinding() {
        return docTypeBinding;
    }

    public void setConsigneeNameBinding(RichInputText consigneeNameBinding) {
        this.consigneeNameBinding = consigneeNameBinding;
    }

    public RichInputText getConsigneeNameBinding() {
        return consigneeNameBinding;
    }

    public void setCustomerNameBinding(RichInputText customerNameBinding) {
        this.customerNameBinding = customerNameBinding;
    }

    public RichInputText getCustomerNameBinding() {
        return customerNameBinding;
    }

    public void setCityNameBinding(RichInputText cityNameBinding) {
        this.cityNameBinding = cityNameBinding;
    }

    public RichInputText getCityNameBinding() {
        return cityNameBinding;
    }

    public void setTransporterNameBinding(RichInputText transporterNameBinding) {
        this.transporterNameBinding = transporterNameBinding;
    }

    public RichInputText getTransporterNameBinding() {
        return transporterNameBinding;
    }

    public void setCurrencyCodeBinding(RichInputText currencyCodeBinding) {
        this.currencyCodeBinding = currencyCodeBinding;
    }

    public RichInputText getCurrencyCodeBinding() {
        return currencyCodeBinding;
    }

    public void setApprovedByNameBind(RichInputText approvedByNameBind) {
        this.approvedByNameBind = approvedByNameBind;
    }

    public RichInputText getApprovedByNameBind() {
        return approvedByNameBind;
    }

    public void setCurrencyNameBind(RichInputText currencyNameBind) {
        this.currencyNameBind = currencyNameBind;
    }

    public RichInputText getCurrencyNameBind() {
        return currencyNameBind;
    }

    public void setAckNoBinding(RichInputText ackNoBinding) {
        this.ackNoBinding = ackNoBinding;
    }

    public RichInputText getAckNoBinding() {
        return ackNoBinding;
    }

    public void setAmdNoBinding(RichInputText amdNoBinding) {
        this.amdNoBinding = amdNoBinding;
    }

    public RichInputText getAmdNoBinding() {
        return amdNoBinding;
    }

    public void setNetAmountBinding(RichInputText netAmountBinding) {
        this.netAmountBinding = netAmountBinding;
    }

    public RichInputText getNetAmountBinding() {
        return netAmountBinding;
    }

    public void setDiscountBinding(RichInputText discountBinding) {
        this.discountBinding = discountBinding;
    }

    public RichInputText getDiscountBinding() {
        return discountBinding;
    }

    public void setGrossAmtBinding(RichInputText grossAmtBinding) {
        this.grossAmtBinding = grossAmtBinding;
    }

    public RichInputText getGrossAmtBinding() {
        return grossAmtBinding;
    }

    public void setAmtFCBinding(RichInputText amtFCBinding) {
        this.amtFCBinding = amtFCBinding;
    }

    public RichInputText getAmtFCBinding() {
        return amtFCBinding;
    }

    public void setPackingAmtBind(RichInputText packingAmtBind) {
        this.packingAmtBind = packingAmtBind;
    }

    public RichInputText getPackingAmtBind() {
        return packingAmtBind;
    }

    public void setSgstRateBind(RichInputText sgstRateBind) {
        this.sgstRateBind = sgstRateBind;
    }

    public RichInputText getSgstRateBind() {
        return sgstRateBind;
    }

    public void setSgstAmtBind(RichInputText sgstAmtBind) {
        this.sgstAmtBind = sgstAmtBind;
    }

    public RichInputText getSgstAmtBind() {
        return sgstAmtBind;
    }

    public void setCgstRateBind(RichInputText cgstRateBind) {
        this.cgstRateBind = cgstRateBind;
    }

    public RichInputText getCgstRateBind() {
        return cgstRateBind;
    }

    public void setCgstAmtBind(RichInputText cgstAmtBind) {
        this.cgstAmtBind = cgstAmtBind;
    }

    public RichInputText getCgstAmtBind() {
        return cgstAmtBind;
    }

    public void setIgstRateBind(RichInputText igstRateBind) {
        this.igstRateBind = igstRateBind;
    }

    public RichInputText getIgstRateBind() {
        return igstRateBind;
    }

    public void setIgstAmtBind(RichInputText igstAmtBind) {
        this.igstAmtBind = igstAmtBind;
    }

    public RichInputText getIgstAmtBind() {
        return igstAmtBind;
    }

    public void setLessToolDisAmtSum(RichInputText lessToolDisAmtSum) {
        this.lessToolDisAmtSum = lessToolDisAmtSum;
    }

    public RichInputText getLessToolDisAmtSum() {
        return lessToolDisAmtSum;
    }

    public void setSgstAmtSum(RichInputText sgstAmtSum) {
        this.sgstAmtSum = sgstAmtSum;
    }

    public RichInputText getSgstAmtSum() {
        return sgstAmtSum;
    }

    public void setCgstAmtSum(RichInputText cgstAmtSum) {
        this.cgstAmtSum = cgstAmtSum;
    }

    public RichInputText getCgstAmtSum() {
        return cgstAmtSum;
    }

    public void setIgstAmtSum(RichInputText igstAmtSum) {
        this.igstAmtSum = igstAmtSum;
    }

    public RichInputText getIgstAmtSum() {
        return igstAmtSum;
    }

    public void setLessDiscountSum(RichInputText lessDiscountSum) {
        this.lessDiscountSum = lessDiscountSum;
    }

    public RichInputText getLessDiscountSum() {
        return lessDiscountSum;
    }

    public void setTcSPerSum(RichInputText tcSPerSum) {
        this.tcSPerSum = tcSPerSum;
    }

    public RichInputText getTcSPerSum() {
        return tcSPerSum;
    }

    public void setTcsAmtSum(RichInputText tcsAmtSum) {
        this.tcsAmtSum = tcsAmtSum;
    }

    public RichInputText getTcsAmtSum() {
        return tcsAmtSum;
    }

    public void setOtherSum(RichInputText otherSum) {
        this.otherSum = otherSum;
    }

    public RichInputText getOtherSum() {
        return otherSum;
    }

    public void setNetAmtSum(RichInputText netAmtSum) {
        this.netAmtSum = netAmtSum;
    }

    public RichInputText getNetAmtSum() {
        return netAmtSum;
    }

    public void setGrossAmtFCSum(RichInputText grossAmtFCSum) {
        this.grossAmtFCSum = grossAmtFCSum;
    }

    public RichInputText getGrossAmtFCSum() {
        return grossAmtFCSum;
    }

    public void setNetAmtFCSum(RichInputText netAmtFCSum) {
        this.netAmtFCSum = netAmtFCSum;
    }

    public RichInputText getNetAmtFCSum() {
        return netAmtFCSum;
    }

    public void setVouDateSum(RichInputDate vouDateSum) {
        this.vouDateSum = vouDateSum;
    }

    public RichInputDate getVouDateSum() {
        return vouDateSum;
    }

    public void setGrNoSum(RichInputText grNoSum) {
        this.grNoSum = grNoSum;
    }

    public RichInputText getGrNoSum() {
        return grNoSum;
    }

    public void setGrDateSum(RichInputDate grDateSum) {
        this.grDateSum = grDateSum;
    }

    public RichInputDate getGrDateSum() {
        return grDateSum;
    }

    public void setStatusBinding(RichSelectOneChoice statusBinding) {
        this.statusBinding = statusBinding;
    }

    public RichSelectOneChoice getStatusBinding() {
        return statusBinding;
    }


    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void EditButtonAL(ActionEvent actionEvent) {
        if (approveCodeBinding.getValue() != null &&
            !(invHeadTypeBinding.getValue().equals("E") || invHeadTypeBinding.getValue().equals("Z"))) {
            ADFUtils.showMessage("Record can't be modified.", 2);
            ADFUtils.setEL("#{pageFlowScope.mode}", "V");
        } else {
            cevmodecheck();
        }
        if (approveCodeBinding.getValue() != null &&
            (invHeadTypeBinding.getValue().equals("E") || invHeadTypeBinding.getValue().equals("Z"))) {
            getQuantityBinding().setDisabled(true);
        }
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }

    public void cevModeDisableComponent(String mode) {
        if (mode.equals("E")) {
            mode_value = "false";
            //getGrDateSum().setDisabled(true);
            //getEwayDateBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
            getIdentifierBinding().setDisabled(true);
            getUnitNameBinding().setDisabled(true);
            getInvDescBinding().setDisabled(true);
            getDueDateBinding().setDisabled(true);
            getDocTypeBinding().setDisabled(true);
            //getConsigneeNameBinding().setDisabled(true);
            getCustomerNameBinding().setDisabled(true);
            getCityNameBinding().setDisabled(true);
            getTransporterNameBinding().setDisabled(true);
            getCurrencyCodeBinding().setDisabled(true);
            getApprovedByNameBind().setDisabled(true);
            getCurrencyNameBind().setDisabled(true);
            getAckNoBinding().setDisabled(true);
            getAmdNoBinding().setDisabled(true);
            getNetAmountBinding().setDisabled(true);
            getDiscountBinding().setDisabled(true);
            getGrossAmtBinding().setDisabled(true);
            getAmtFCBinding().setDisabled(true);
            getPackingAmtBind().setDisabled(true);
            getSgstRateBind().setDisabled(true);
            getSgstAmtBind().setDisabled(false);
            getCgstRateBind().setDisabled(true);
            getCgstAmtBind().setDisabled(true);
            getIgstRateBind().setDisabled(true);
            getIgstAmtBind().setDisabled(false);
            getNetAmtFCSum().setDisabled(true);
            getNetAmtSum().setDisabled(true);
            // getGrDateSum().setDisabled(true);
            // getGrNoSum().setDisabled(true);
            getGrossAmtFCSum().setDisabled(true);
            getGrossAmountBinding().setDisabled(true);
            getLessDiscountSum().setDisabled(true);
            getLessToolDisAmtSum().setDisabled(true);
            getSgstAmtSum().setDisabled(true);
            getIgstAmtSum().setDisabled(true);
            getSgstAmtSum().setDisabled(true);

            //            getTcSPerSum().setDisabled(true);
            //            getTcsAmtSum().setDisabled(true);
            getVouDateSum().setDisabled(true);
            getVoucherBinding().setDisabled(true);
            getCgstAmtSum().setDisabled(true);
            getOtherSum().setDisabled(true);
            getDaNumberBinding().setDisabled(true);
            getDispAdviceNoBinding().setDisabled(true);
            getBsrStockBinding().setDisabled(true);
            //getDisPerBinding().setDisabled(true);

            getHeaderEditBinding().setDisabled(true);
            getAddButDetailBinding().setDisabled(false);
            getDeleteButtonBinding().setDisabled(false);
            getInvSubTypeBinding().setDisabled(true);
            getAddIdentifierBinding().setDisabled(true);
            getInvHeadTypeBinding().setDisabled(true);
            getPreparedByBinding().setDisabled(true);
            getInvTransBinding().setDisabled(true);
            getPopulateButtonBinding().setDisabled(true);
            invoiceHeadTypeCheckingMethod();
            getFcrDateBinding().setDisabled(true);
            getAuthDateBinding().setDisabled(true);
            getAuthNoBinding().setDisabled(true);
            getBalQuantityBinding().setDisabled(true);
            getCustPoDateBinding().setDisabled(true);
            getCustPoNoBinding().setDisabled(true);
            getCustProdDesBinding().setDisabled(true);

            getDolDispatchNoButtonBinding().setDisabled(true);

            getDocSeqNoBinding().setDisabled(true);
            getUnitCodeDocRefBinding().setDisabled(true);
            getRefDocNoBinding().setDisabled(true);
            getRefDocTypeBinding().setDisabled(true);
            getDocFileNameBinding().setDisabled(true);
            getRefDocDateBinding().setDisabled(true);
            getCessAmountBinding().setDisabled(true);
            getBatchNumberBinding().setDisabled(true);
            getMfgDtBinding().setDisabled(true);
            getExpDtBinding().setDisabled(true);
            getInvDateNepalBinding().setDisabled(true);
            getFrExcHeaderBinding().setDisabled(true);
            getFrVatHeaderBinding().setDisabled(true);
            getQuantityBinding().setDisabled(true);
            getTotAmtInvoiceHeaderBinding().setDisabled(true);
            getRoundOffHeaderBinding().setDisabled(true);
        } else if (mode.equals("C")) {
            mode_value = "true";
            //getGrDateSum().setDisabled(true);
            // getEwayDateBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
            getIdentifierBinding().setDisabled(true);
            getUnitNameBinding().setDisabled(true);
            getInvDescBinding().setDisabled(true);
            getDueDateBinding().setDisabled(true);
            getDocTypeBinding().setDisabled(true);
            //getConsigneeNameBinding().setDisabled(true);
            getCustomerNameBinding().setDisabled(true);
            getCityNameBinding().setDisabled(true);
            getTransporterNameBinding().setDisabled(true);
            getCurrencyCodeBinding().setDisabled(true);
            getApprovedByNameBind().setDisabled(true);
            getCurrencyNameBind().setDisabled(true);
            getAckNoBinding().setDisabled(true);
            getAmdNoBinding().setDisabled(true);
            getNetAmountBinding().setDisabled(true);
            getDiscountBinding().setDisabled(true);
            getGrossAmtBinding().setDisabled(true);
            getAmtFCBinding().setDisabled(true);
            getPackingAmtBind().setDisabled(true);
            getSgstRateBind().setDisabled(true);
            getSgstAmtBind().setDisabled(false);
            getCgstRateBind().setDisabled(true);
            getCgstAmtBind().setDisabled(true);
            getIgstRateBind().setDisabled(true);
            getIgstAmtBind().setDisabled(false);
            getNetAmtFCSum().setDisabled(true);
            getNetAmtSum().setDisabled(true);
            // getGrDateSum().setDisabled(true);
            //getGrNoSum().setDisabled(true);
            getGrossAmtFCSum().setDisabled(true);
            getGrossAmountBinding().setDisabled(true);
            getLessDiscountSum().setDisabled(true);
            getLessToolDisAmtSum().setDisabled(true);
            getSgstAmtSum().setDisabled(true);
            getIgstAmtSum().setDisabled(true);
            getSgstAmtSum().setDisabled(true);
            // getTcSPerSum().setDisabled(true);
            // getTcsAmtSum().setDisabled(true);
            getVouDateSum().setDisabled(true);
            getVoucherBinding().setDisabled(true);
            getStatusBinding().setDisabled(true);
            getCgstAmtSum().setDisabled(true);
            getOtherSum().setDisabled(true);
            getDaNumberBinding().setDisabled(true);
            getDispAdviceNoBinding().setDisabled(true);
            getBsrStockBinding().setDisabled(true);
            // getDisPerBinding().setDisabled(true);

            getHeaderEditBinding().setDisabled(true);
            getAddButDetailBinding().setDisabled(false);
            getDeleteButtonBinding().setDisabled(false);
            getInvSubTypeBinding().setDisabled(true);
            getAddIdentifierBinding().setDisabled(true);
            getDolDispatchNoButtonBinding().setDisabled(true);
            getIssueSlipNoBinding().setDisabled(true);
            getSelectOrdersBtBinding().setDisabled(true);
            getPreparedByBinding().setDisabled(true);
            getInvTransBinding().setDisabled(true);
            getPopulateButtonBinding().setDisabled(true);

            disbaledFieldsAccordingToConditions(true);


            getFcrDateBinding().setDisabled(true);
            getAuthDateBinding().setDisabled(true);
            getAuthNoBinding().setDisabled(true);
            getBalQuantityBinding().setDisabled(true);
            getInvSubType1Binding().setRendered(false);
            getCustPoDateBinding().setDisabled(true);
            getCustPoNoBinding().setDisabled(true);
            getCustProdDesBinding().setDisabled(true);


            getDocSeqNoBinding().setDisabled(true);
            getUnitCodeDocRefBinding().setDisabled(true);
            getRefDocNoBinding().setDisabled(true);
            getRefDocTypeBinding().setDisabled(true);
            getDocFileNameBinding().setDisabled(true);
            getRefDocDateBinding().setDisabled(true);
            getCessAmountBinding().setDisabled(true);
            getBatchNumberBinding().setDisabled(true);
            getMfgDtBinding().setDisabled(true);
            getExpDtBinding().setDisabled(true);
            getInvDateNepalBinding().setDisabled(true);
            getQuantityBinding().setDisabled(true);
            getFrExcHeaderBinding().setDisabled(true);
            getFrVatHeaderBinding().setDisabled(true);
            getTotAmtInvoiceHeaderBinding().setDisabled(true);
            getRoundOffHeaderBinding().setDisabled(true);

        } else if (mode.equals("V")) {
            getInvoiceSummaryBinding().setDisabled(false);
            getItemDetailtabBinding().setDisabled(false);
            getInvoiceDetailTabBinding().setDisabled(false);
            getAddButDetailBinding().setDisabled(true);
            getDeleteButtonBinding().setDisabled(false);
            getOtherDetailBinding().setDisabled(false);
            getAdditionalInvoiceOneTabBinding().setDisabled(false);
            getAdditionalInvoiceTwoTabBinding().setDisabled(false);
            getRefDocTabBinding().setDisabled(false);
            downloadLinkBinding.setDisabled(false);
            try {
                invoiceHeadTypeCheckingMethod();
                getFcrNoBinding().setDisabled(true);
                getLutSrNoBinding().setDisabled(true);

            } catch (Exception e) {
                // TODO: Add catch code
                e.printStackTrace();
            }
            getTcSPerSum().setDisabled(true);
            getTcsAmtSum().setDisabled(true);
            getDolDispatchNoButtonBinding().setDisabled(true);
            getBatchNumberBinding().setDisabled(true);
            getMfgDtBinding().setDisabled(true);
            getExpDtBinding().setDisabled(true);
            getQuantityBinding().setDisabled(true);
            getFrExcHeaderBinding().setDisabled(true);
            getFrVatHeaderBinding().setDisabled(true);
            getTotAmtInvoiceHeaderBinding().setDisabled(true);
            getRoundOffHeaderBinding().setDisabled(true);
        }

    }

    public void setDeleteButtonBinding(RichButton deleteButtonBinding) {
        this.deleteButtonBinding = deleteButtonBinding;
    }

    public RichButton getDeleteButtonBinding() {
        return deleteButtonBinding;
    }

    public void setBindingOutputText(RichPanelLabelAndMessage bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichPanelLabelAndMessage getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setInvoiceDetailTabBinding(RichShowDetailItem invoiceDetailTabBinding) {
        this.invoiceDetailTabBinding = invoiceDetailTabBinding;
    }

    public RichShowDetailItem getInvoiceDetailTabBinding() {
        return invoiceDetailTabBinding;
    }

    public void setItemDetailtabBinding(RichShowDetailItem itemDetailtabBinding) {
        this.itemDetailtabBinding = itemDetailtabBinding;
    }

    public RichShowDetailItem getItemDetailtabBinding() {
        return itemDetailtabBinding;
    }

    public void setInvoiceSummaryBinding(RichShowDetailItem invoiceSummaryBinding) {
        this.invoiceSummaryBinding = invoiceSummaryBinding;
    }

    public RichShowDetailItem getInvoiceSummaryBinding() {
        return invoiceSummaryBinding;
    }

    public void approveVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce.getNewValue() != null) {
            OperationBinding opr = ADFUtils.findOperation("checkApprovalStatus");
            opr.getParamsMap().put("emp_code", vce.getNewValue());
            opr.getParamsMap().put("form_name", "FINSI");
            opr.getParamsMap().put("unit_code", getUnitCodeBinding().getValue());
            opr.getParamsMap().put("autho_level", "AP");
            opr.getParamsMap().put("amount", netAmtSum.getValue());
            opr.execute();
            System.out.println("RESULT IS:=>" + opr.getResult());
            Row row = (Row) ADFUtils.evaluateEL("#{bindings.InvoiceEntryHeaderVO1Iterator.currentRow}");
            if (opr.getResult() != null && !opr.getResult().equals("Y")) {
                row.setAttribute("ApproveCode", null);
                ADFUtils.showMessage("You Don't have permission to approve this record.", 0);
                AdfFacesContext.getCurrentInstance().addPartialTarget(approveCodeBinding);
            }

            //String checkApprovalStatus(String emp_code,String form_name,String unit_code,String autho_level,BigDecimal amount)
        }
    }

    public void setApproveCodeBinding(RichInputComboboxListOfValues approveCodeBinding) {
        this.approveCodeBinding = approveCodeBinding;
    }

    public RichInputComboboxListOfValues getApproveCodeBinding() {
        return approveCodeBinding;
    }

    public void setTransporterCodeBinding(RichInputComboboxListOfValues transporterCodeBinding) {
        this.transporterCodeBinding = transporterCodeBinding;
    }

    public RichInputComboboxListOfValues getTransporterCodeBinding() {
        return transporterCodeBinding;
    }

    public void setOpenFixBinding(RichInputText openFixBinding) {
        this.openFixBinding = openFixBinding;
    }

    public RichInputText getOpenFixBinding() {
        return openFixBinding;
    }

    public void setOpenFixDispatchBinding(RichInputText openFixDispatchBinding) {
        this.openFixDispatchBinding = openFixDispatchBinding;
    }

    public RichInputText getOpenFixDispatchBinding() {
        return openFixDispatchBinding;
    }

    //    public void invoiceQtySaleOrderVCE(ValueChangeEvent valueChangeEvent) {
    //        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
    //
    //        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
    //        Row row = (Row) ADFUtils.evaluateEL("#{bindings.PendingSalesOrderDetailsVVOIterator.currentRow}");
    //        System.out.println("Check Box:=>" + (row.getAttribute("TransChkBox")));
    //        System.out.println("Invoice Quantity:=>" + row.getAttribute("TransInvcQuantity"));
    //
    //        if (valueChangeEvent.getNewValue() != null) {
    //            if ((row.getAttribute("TransChkBox").equals("true") || row.getAttribute("TransChkBox").equals("Y")) &&
    //                (((BigDecimal) row.getAttribute("TransInvcQuantity"))).compareTo(new BigDecimal(0)) == 1)
    //                ADFUtils.findOperation("calcualteItemWiseQtyInvoiceEntry").execute();
    //        }
    //
    //
    //    }

    public void invoiceQtySaleOrderVCE(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        //        Row row = (Row) ADFUtils.evaluateEL("#{bindings.PendingSalesOrderDetailsVVOIterator.currentRow}");
        //        System.out.println("Check Box:=>" + (row.getAttribute("TransChkBox")));
        //        System.out.println("Invoice Quantity:=>" + row.getAttribute("TransInvcQuantity"));
        //
        //        if (valueChangeEvent.getNewValue() != null) {
        //            if ((row.getAttribute("TransChkBox").equals("true") || row.getAttribute("TransChkBox").equals("Y")) &&
        //                (((BigDecimal) row.getAttribute("TransInvcQuantity"))).compareTo(new BigDecimal(0)) == 1)
        //                ADFUtils.findOperation("calcualteItemWiseQtyInvoiceEntry").execute();
        //        }

        OperationBinding opr = ADFUtils.findOperation("checkBSRStockTypeInvoiceEntry");
        opr.getParamsMap().put("stock_type", ADFUtils.resolveExpression("#{bindings.StockType.inputValue}"));
        opr.getParamsMap().put("unit_code", unitCodeBinding.getValue());
        opr.getParamsMap().put("invType", ADFUtils.resolveExpression("#{bindings.InvHeadType.inputValue}"));
        opr.execute();
        if (opr.getResult() != null && opr.getResult().equals("Y")) {
            Row row = (Row) ADFUtils.evaluateEL("#{bindings.PendingSalesOrderDetailsVVOIterator.currentRow}");

            if (valueChangeEvent.getNewValue() != null) {
                if ((row.getAttribute("TransChkBox").equals("true") || row.getAttribute("TransChkBox").equals("Y")) &&
                    (((BigDecimal) row.getAttribute("TransInvcQuantity"))).compareTo(new BigDecimal(0)) == 1)
                    ADFUtils.findOperation("calcualteItemWiseQtyInvoiceEntry").execute();
            }
        }


    }


    public void quantityDispatchVCE(ValueChangeEvent vce) {
        String values = null;

        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        Row row = (Row) ADFUtils.evaluateEL("#{bindings.PendingDispatchAdvcDetailVVO1Iterator.currentRow}");
        if (vce.getNewValue() != null) {
            if (row.getAttribute("TransCheckBox").equals("true") || row.getAttribute("TransCheckBox").equals("Y")) {
                if (da_and_amd == null || da_and_amd == "") {
                    da_and_amd = row.getAttribute("DaNo") + "" + row.getAttribute("PoAmdNumber");
                } else {
                    values = row.getAttribute("DaNo") + "" + row.getAttribute("PoAmdNumber");
                    if (da_and_amd != null && values != null) {
                        if (!da_and_amd.equals(values)) {
                            //                            ADFUtils.showMessage("Please select same Dispatch Advice No.", 0);
                            //                            row.setAttribute("TransCheckBox", "false");
                        }

                    }

                }
            } else {
                OperationBinding unselectOpr = ADFUtils.findOperation("checkAllUnCheckValuesInvoiceEntry");
                unselectOpr.execute();
                if (unselectOpr.getResult().equals("N"))
                    da_and_amd = "";
            }
            OperationBinding opr = ADFUtils.findOperation("checkBSRStockTypeInvoiceEntry");
            opr.getParamsMap().put("stock_type", ADFUtils.resolveExpression("#{bindings.StockType.inputValue}"));
            opr.getParamsMap().put("unit_code", unitCodeBinding.getValue());
            opr.getParamsMap().put("invType", ADFUtils.resolveExpression("#{bindings.InvHeadType.inputValue}"));
            opr.execute();
            if (opr.getResult() != null && opr.getResult().equals("Y")) {
                if ((row.getAttribute("TransCheckBox").equals("true") ||
                     row.getAttribute("TransCheckBox").equals("Y")) &&
                    (((BigDecimal) row.getAttribute("TransQuantity"))).compareTo(new BigDecimal(0)) == 1)
                    ADFUtils.findOperation("calcualteItemWiseQtyForDispatchOrderInvoiceEntry").execute();

            }
        }
    }


    public void setDaNumberBinding(RichInputText daNumberBinding) {
        this.daNumberBinding = daNumberBinding;
    }

    public RichInputText getDaNumberBinding() {
        return daNumberBinding;
    }

    public void setDisPerBinding(RichInputText disPerBinding) {
        this.disPerBinding = disPerBinding;
    }

    public RichInputText getDisPerBinding() {
        return disPerBinding;
    }

    public void setItemTypeBinding(RichSelectOneChoice itemTypeBinding) {
        this.itemTypeBinding = itemTypeBinding;
    }

    public RichSelectOneChoice getItemTypeBinding() {
        return itemTypeBinding;
    }

    public void setProductCodeBinding(RichInputComboboxListOfValues productCodeBinding) {
        this.productCodeBinding = productCodeBinding;
    }

    public RichInputComboboxListOfValues getProductCodeBinding() {
        return productCodeBinding;
    }

    public void deletePopupDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {

            Row row = (Row) ADFUtils.evaluateEL("#{bindings.InvoiceEntryDetailVO1Iterator.currentRow}");
            Integer edit_value = (Integer) row.getAttribute("Edittrans1");
            if (edit_value >= 0) {
                ADFUtils.findOperation("Delete").execute();
                ADFUtils.showMessage("Record Deleted Successfully.", 2);
            } else {
                ADFUtils.showMessage("You can not delete this record.", 0);
                deletePopupBinding.hide();
            }
            AdfFacesContext.getCurrentInstance().addPartialTarget(detailTableBinding);
        }
    }

    public void setDeletePopupBinding(RichPopup deletePopupBinding) {
        this.deletePopupBinding = deletePopupBinding;
    }

    public RichPopup getDeletePopupBinding() {
        return deletePopupBinding;
    }

    public void setDetailTableBinding(RichTable detailTableBinding) {
        this.detailTableBinding = detailTableBinding;
    }

    public RichTable getDetailTableBinding() {
        return detailTableBinding;
    }

    public void productCodeVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce.getNewValue() != null) {
            if (itemTypeBinding.getValue().equals("A") || itemTypeBinding.getValue().equals(0)) {
                try {
                    ADFUtils.findOperation("filterGstCodeAndHsnNoForInvoiceDetail").execute();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    OperationBinding opr = ADFUtils.findOperation("checkAndSetBsrStockValueDetailInvoiceEntry");
                    opr.getParamsMap().put("prod_code", vce.getNewValue());
                    opr.getParamsMap().put("date_value", dateBinding.getValue());
                    opr.getParamsMap().put("fin_year", ADFUtils.evaluateEL("#{pageFlowScope.finYear}"));
                    opr.getParamsMap().put("type", "OE");
                    opr.getParamsMap().put("unit_code", unitCodeBinding.getValue());
                    opr.execute();
                    bsrStockBinding.setValue(opr.getResult());
                    AdfFacesContext.getCurrentInstance().addPartialTarget(bsrStockBinding);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                ADFUtils.findOperation("filterGstCodeAndHsnNoOtherThanItemInvoiceEntry").execute();
            }
        }


    }

    public void issueSlipVCE(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        ADFUtils.findOperation("populateIssueSlipDataInvoiceEntry").execute();
    }

    public void setIssueSlipNoBinding(RichInputComboboxListOfValues issueSlipNoBinding) {
        this.issueSlipNoBinding = issueSlipNoBinding;
    }

    public RichInputComboboxListOfValues getIssueSlipNoBinding() {
        return issueSlipNoBinding;
    }

    public void bsrStockValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        String dispAdvValue = (String) ADFUtils.resolveExpression("#{bindings.DispAdv.inputValue}");
        if (dispAdvValue != null) {
            if (dispAdvValue.equals("Y")) {
                OperationBinding opr1 = ADFUtils.findOperation("checkSaleNoteQuantityInvoice");
                opr1.getParamsMap().put("qty", object);
                opr1.execute();

                if (opr1.getResult() != null) {
                    if (opr1.getResult().equals("N")) {
                        throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                      "Quantity must be less then or equals to Sale Note Quantity.",
                                                                      ""));
                    }
                }

                if (itemTypeBinding.getValue() != null &&
                    (itemTypeBinding.getValue().equals("A") || itemTypeBinding.getValue().equals(0))) {
                    if (object != null && bsrStockBinding.getValue() != null) {
                        BigDecimal bsr_stock =
                            (BigDecimal) (bsrStockBinding.getValue() != null ? bsrStockBinding.getValue() :
                                          new BigDecimal(0));
                        BigDecimal quantity = (BigDecimal) object;

                        if (quantity.compareTo(bsr_stock) == 1) {
                            OperationBinding opr = ADFUtils.findOperation("checkBSRStockTypeInvoiceEntry");
                            opr.getParamsMap().put("stock_type",
                                                   ADFUtils.resolveExpression("#{bindings.StockType.inputValue}"));
                            opr.getParamsMap().put("unit_code", unitCodeBinding.getValue());
                            opr.getParamsMap().put("invType",
                                                   ADFUtils.resolveExpression("#{bindings.InvHeadType.inputValue}"));
                            opr.execute();
                            if (opr.getResult() != null && opr.getResult().equals("Y")) {
                                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                              "Quantity must be less than or equals to BSR Stock.",
                                                                              ""));
                            }
                        }
                    }
                }
            } //Aakash
        } //Aakash
    }


    public void itemTypeValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (object != null) {
            if ((object.equals("A") || object.equals(0)) && value_check.equals("Y")) {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                              "Item Can't be select when in this case.", ""));
            }
        }

    }

    public void currencyRateValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (object != null && currencyCodeBinding.getValue() != null) {
            if (currencyCodeBinding.getValue().toString().equals("INR")) {
                if (!(((BigDecimal) object).compareTo(new BigDecimal(1)) == 0)) {
                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                  "In case of currency code INR, Rate must be 1.", ""));
                }

            }

        }
    }


    public String saveAndCloseButtonAction() {
        ADFUtils.setLastUpdatedBy("InvoiceEntryHeaderVO1Iterator", "LastUpdatedBy");
        OperationBinding binding1 = ADFUtils.findOperation("checkAllValidations");
        binding1.execute();


        if (binding1.getResult() != null && binding1.getResult().toString().equalsIgnoreCase("Y")) {
            OperationBinding binding = ADFUtils.findOperation("generateSalesInvcNo");
            binding.execute();


            if (binding.getResult() != null && binding.getResult().toString().equalsIgnoreCase("Y")) {
                binding = ADFUtils.findOperation("calculateAmountDetails");
                binding.execute();
                ADFUtils.findOperation("insertIntoSalesMapping").execute();
            }

            String param = mode_value;


            if (binding.getResult() != null && binding.getResult().toString().equalsIgnoreCase("Y") &&
                param.equalsIgnoreCase("false")) {
                ADFUtils.findOperation("Commit").execute();
                FacesMessage Message = new FacesMessage("Record Updated Successfully.");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
                return "SaveAndClose";
            } else if (binding.getResult() != null && binding.getResult().toString().equalsIgnoreCase("Y") &&
                       param.equalsIgnoreCase("true")) {
                mode_value = "false";
                ADFUtils.findOperation("Commit").execute();
                //                OperationBinding findOperation = ADFUtils.findOperation("getParameter10Value");
                //                findOperation.getParamsMap().put("keyno", 7);
                //                findOperation.getParamsMap().put("unitcd", getUnitCodeBinding().getValue());
                //                findOperation.execute();
                FacesMessage Message = new FacesMessage();
                //              if(findOperation.getResult() != null && findOperation.getResult().toString().equalsIgnoreCase("Y")){
                //                    if (getVoucherBinding().getValue() != null) {
                //                        Message =
                //                            new FacesMessage("Record Saved Successfully. New invoice no. is " +
                //                                             getIdentifierBinding().getValue() + ". Voucher no. is " +
                //                                             getVoucherBinding().getValue() + ".");
                //                    } else {
                //                        Message =
                //                            new FacesMessage("Record Saved Successfully. New invoice no. is " +
                //                                             getIdentifierBinding().getValue() + ".");
                //                    }
                Message =
                    new FacesMessage("Record Saved Successfully. New invoice no. is " +
                                     getIdentifierBinding().getValue() + ".");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);

                OperationBinding bindingN = ADFUtils.findOperation("callInvoiceApi");
                bindingN.execute();

                return "SaveAndClose";
            } else if (binding.getResult() != null && binding.getResult().toString().equalsIgnoreCase("N")) {
                ADFUtils.findOperation("Rollback").execute();
                FacesMessage Message = new FacesMessage("Error found to save data. Try Again!!");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
                return null;
            }
        }
        //        }
        //        else
        //        {
        //        ADFUtils.showMessage("Approved by is required.",0);
        //            return null;
        //        }
        return null;
    }


    public void invoiceHeadTypeCheckingMethod() {
        // Add event code here...
        String dispAdvValue = (String) ADFUtils.resolveExpression("#{bindings.DispAdv.inputValue}");
        String stkChkReq = (String) ADFUtils.resolveExpression("#{bindings.StockType.inputValue}");
        System.out.println("Stock Type Required :- " + stkChkReq);
        String poValue = (String) ADFUtils.resolveExpression("#{bindings.Po.inputValue}");
        System.out.println("Disp * Po Value :- " + dispAdvValue + "  " + poValue);


        if (dispAdvValue != null && poValue != null) {
            if (dispAdvValue.equals("Y")) {
                getSelectDispatchBtBinding().setDisabled(false);
                getSelectOrdersBtBinding().setDisabled(true);
                getDolDispatchNoButtonBinding().setDisabled(false);
                value_check = "Y";
                //getDestinationBinding().setRequired(false);
                getTransporterCdBinding().setRequired(false);

                disbaledFieldsAccordingToConditions(true);
                //getAddButDetailBinding().setDisabled(true);
                //--------------------Disable Detail Item-----------
                getQuantityBinding().setDisabled(true);
                getPackQtyBinding().setDisabled(true);
                getPackUomBinding().setDisabled(true);
                getUomBinding().setDisabled(true);
                getGrossWtBinding().setDisabled(true);
                getNetWeightBinding().setDisabled(true);
                //                        getPriceBinding().setDisabled(true);
                //                        getCustMrpBinding().setDisabled(true);
                //                        getToolCostBinding().setDisabled(true);
                //                        getBsrStockBinding().setDisabled(true);
                //                        getPackQtyBinding().setDisabled(false);
                //                        getItemTypeBinding().setDisabled(true);
                //                        getProductCodeBinding().setDisabled(true);
                //                        getDisPerBinding().setDisabled(true);
                if (stkChkReq.equals('Y')) {
                    ADFUtils.findOperation("setValuesPendingSaleOrder").execute();
                }
                //---------------------------------------------------
            } else if (dispAdvValue.equals("N")) {
                if (poValue.equals("R")) {
                    getSelectDispatchBtBinding().setDisabled(true);
                    getSelectOrdersBtBinding().setDisabled(false);
                    getDolDispatchNoButtonBinding().setDisabled(true);
                    disbaledFieldsAccordingToConditions(false);
                    value_check = "Y";
                    //getDestinationBinding().setRequired(false);
                    getTransporterCdBinding().setRequired(false);

                    //getAddButDetailBinding().setDisabled(true);
                    //                            //--------------------Disable Detail Item-----------
                    //                            getQuantityBinding().setDisabled(true);
                    //                            getGrossWtBinding().setDisabled(true);
                    //                            getPriceBinding().setDisabled(true);
                    //                            getCustMrpBinding().setDisabled(true);
                    //                            getToolCostBinding().setDisabled(true);
                    //                            getBsrStockBinding().setDisabled(true);
                    //                            getPackQtyBinding().setDisabled(false);
                    //                            getItemTypeBinding().setDisabled(true);
                    //                            getProductCodeBinding().setDisabled(true);
                    //                            getDisPerBinding().setDisabled(true);
                    if (stkChkReq.equals('Y')) {
                        ADFUtils.findOperation("setValuesPendingSaleOrder").execute();
                    }
                    //---------------------------------------------------
                } else if (poValue.equals("V")) {
                    getSelectDispatchBtBinding().setDisabled(true);
                    getSelectOrdersBtBinding().setDisabled(true);
                    getDolDispatchNoButtonBinding().setDisabled(true);
                    value_check = "N";
                    // getDestinationBinding().setRequired(true);
                    // getTransporterCdBinding().setRequired(true);
                    disbaledFieldsAccordingToConditions(false);
                    //getAddButDetailBinding().setDisabled(false);
                    //--------------------Disable Detail Item-----------
                    //                            getQuantityBinding().setDisabled(false);
                    //                            getGrossWtBinding().setDisabled(false);
                    //                            getPriceBinding().setDisabled(false);
                    //                            getCustMrpBinding().setDisabled(false);
                    //                            getToolCostBinding().setDisabled(false);
                    //                            getBsrStockBinding().setDisabled(false);
                    //                            getPackQtyBinding().setDisabled(false);
                    //                            getDisPerBinding().setDisabled(false);
                    //
                    //                            getItemTypeBinding().setDisabled(false);
                    //                            getProductCodeBinding().setDisabled(false);
                    //---------------------------------------------------
                } else {
                    getSelectDispatchBtBinding().setDisabled(true);
                    getSelectOrdersBtBinding().setDisabled(true);
                    getDolDispatchNoButtonBinding().setDisabled(true);
                    value_check = "N";
                    //getDestinationBinding().setRequired(true);
                    // getTransporterCdBinding().setRequired(true);
                    disbaledFieldsAccordingToConditions(false);
                    //getAddButDetailBinding().setDisabled(false);
                    //--------------------Disable Detail Item-----------
                    //                            getQuantityBinding().setDisabled(false);
                    //                            getGrossWtBinding().setDisabled(false);
                    //                            getPriceBinding().setDisabled(false);
                    //                            getCustMrpBinding().setDisabled(false);
                    //                            getToolCostBinding().setDisabled(false);
                    //                            getBsrStockBinding().setDisabled(false);
                    //                            getPackQtyBinding().setDisabled(false);
                    //
                    //                            getItemTypeBinding().setDisabled(false);
                    //                            getProductCodeBinding().setDisabled(false);
                    //                            getDisPerBinding().setDisabled(false);
                    //---------------------------------------------------
                }
            } else {
                getSelectDispatchBtBinding().setDisabled(true);
                getSelectOrdersBtBinding().setDisabled(true);
                getDolDispatchNoButtonBinding().setDisabled(true);

                value_check = "N";
                //getDestinationBinding().setRequired(true);
                // getTransporterCdBinding().setRequired(true);
                disbaledFieldsAccordingToConditions(false);
                //getAddButDetailBinding().setDisabled(false);
                //--------------------Disable Detail Item-----------
                //                        getQuantityBinding().setDisabled(false);
                //                        getGrossWtBinding().setDisabled(false);
                //                        getPriceBinding().setDisabled(false);
                //                        getCustMrpBinding().setDisabled(false);
                //                        getToolCostBinding().setDisabled(false);
                //                        getBsrStockBinding().setDisabled(false);
                //                        getPackQtyBinding().setDisabled(false);
                //
                //                        getItemTypeBinding().setDisabled(false);
                //                        getProductCodeBinding().setDisabled(false);
                //                        getDisPerBinding().setDisabled(false);
                //---------------------------------------------------
            }
        } else {
            getSelectDispatchBtBinding().setDisabled(true);
            getSelectOrdersBtBinding().setDisabled(true);
            getDolDispatchNoButtonBinding().setDisabled(true);

            value_check = "N";
            //getDestinationBinding().setRequired(true);
            // getTransporterCdBinding().setRequired(true);
            //getAddButDetailBinding().setDisabled(false);
            //--------------------Disable Detail Item-----------
            //                    getQuantityBinding().setDisabled(false);
            //                    getGrossWtBinding().setDisabled(false);
            //                    getPriceBinding().setDisabled(false);
            //                    getCustMrpBinding().setDisabled(false);
            //                    getToolCostBinding().setDisabled(false);
            //                    getBsrStockBinding().setDisabled(false);
            //                    getPackQtyBinding().setDisabled(false);
            //
            //                    getItemTypeBinding().setDisabled(false);
            //                    getProductCodeBinding().setDisabled(false);
            //                    getDisPerBinding().setDisabled(false);
            //---------------------------------------------------
        }


        System.out.println("stkChkReqstkChkReqstkChkReqstkChkReq:=" + stkChkReq);
        String inv_head_type = null;
        if (stkChkReq != null) {
            if (stkChkReq.equals("RM")) {
                System.out.println("WHEN VALUE IS RM");
                getIssueSlipNoBinding().setDisabled(false);
            } else {
                getIssueSlipNoBinding().setDisabled(true);
            }
            AdfFacesContext.getCurrentInstance().addPartialTarget(issueSlipNoBinding);

            inv_head_type = (String) ADFUtils.evaluateEL("#{bindings.InvHeadType.inputValue}");
            if (stkChkReq.equals("RM") || inv_head_type.equalsIgnoreCase("E")) {
                getAddButDetailBinding().setDisabled(true);
            } else {
                getAddButDetailBinding().setDisabled(false);

            }


        }


        System.out.println("INV HEAD TYPE====>" + inv_head_type + "||Stock Check====>" + stkChkReq);
        //String inv_type =(String) ADFUtils.evaluateEL("#{bindings.InvHeadType.inputValue}");
        if ((inv_head_type.equals("H") && stkChkReq.equals("SI")) ||
            (inv_head_type.equals("A") && stkChkReq.equals("SI"))) {
            //getInvSubType1Binding().setRendered(false);

            //getInvSubTypeBinding().setRendered(true);
            invSubTypeBinding.setValue("Other");
            getInvTransBinding().setDisabled(false);
            getPopulateButtonBinding().setDisabled(false);

        } else if ((inv_head_type.equals("Y") && stkChkReq.equals("PD")) ||
                   (inv_head_type.equals("Z") && stkChkReq.equals("PD"))) {
            //getInvSubType1Binding().setRendered(false);

            //getInvSubTypeBinding().setRendered(true);

            invSubTypeBinding.setValue("Bill Of Supply");
            getInvTransBinding().setDisabled(true);
            getPopulateButtonBinding().setDisabled(true);


        } else if (((inv_head_type.equals("G")) || (inv_head_type.equals("U")) || (inv_head_type.equals("M")) ||
                    (inv_head_type.equals("W"))) && stkChkReq.equals("PD")) {
            System.out.println("Inside INV HEAD TYPE====>" + inv_head_type + "||Stock Check====>" + stkChkReq);
            //getInvSubTypeBinding().setRendered(false);
            //getInvSubType1Binding().setRendered(true);

            if (mode_value.equalsIgnoreCase("true")) {
                invSubType1Binding.setValue("Job Work");
            }

        } else {
            // getInvSubType1Binding().setRendered(false);
            //getInvSubTypeBinding().setRendered(true);

            invSubTypeBinding.setValue("Tax invoice");
            getInvTransBinding().setDisabled(true);
            getPopulateButtonBinding().setDisabled(true);

        }

        if (inv_head_type.equalsIgnoreCase("E")) {
            getFcrNoBinding().setDisabled(false);
            getLutSrNoBinding().setDisabled(false);
        } else {
            getFcrNoBinding().setDisabled(true);
            getLutSrNoBinding().setDisabled(true);
        }
        if (inv_head_type.equalsIgnoreCase("S")) {
            getTcsAmtSum().setDisabled(false);
            getTcSPerSum().setDisabled(false);
        } else {
            getTcsAmtSum().setDisabled(true);
            getTcSPerSum().setDisabled(true);
        }

        AdfFacesContext.getCurrentInstance().addPartialTarget(invSubTypeBinding);
        AdfFacesContext.getCurrentInstance().addPartialTarget(invTransBinding);
        AdfFacesContext.getCurrentInstance().addPartialTarget(populateButtonBinding);
        AdfFacesContext.getCurrentInstance().addPartialTarget(invSubType1Binding);


        //        if (dispAdvValue != null && poValue != null){
        //            if(dispAdvValue.equals("Y")){
        //                getSelectDispatchBtBinding().setDisabled(false);
        //                getSelectOrdersBtBinding().setDisabled(true);
        //                //getAddButDetailBinding().setDisabled(true);
        //                //--------------------Disable Detail Item-----------
        //                getQuantityBinding().setDisabled(true);
        //                getGrossWtBinding().setDisabled(true);
        //                getPriceBinding().setDisabled(true);
        //                getCustMrpBinding().setDisabled(true);
        //                getToolCostBinding().setDisabled(true);
        //                getBsrStockBinding().setDisabled(true);
        //                getPackQtyBinding().setDisabled(false);
        //                getItemTypeBinding().setDisabled(true);
        //                getProductCodeBinding().setDisabled(true);
        //                getDisPerBinding().setDisabled(true);
        //                if (stkChkReq.equals('Y')){
        //                    ADFUtils.findOperation("setValuesPendingSaleOrder").execute();
        //                }
        //                //---------------------------------------------------
        //            } else if(dispAdvValue.equals("N")){
        //                if(poValue.equals("R")){
        //                    getSelectDispatchBtBinding().setDisabled(true);
        //                    getSelectOrdersBtBinding().setDisabled(false);
        //                    //getAddButDetailBinding().setDisabled(true);
        //                    //--------------------Disable Detail Item-----------
        //                    getQuantityBinding().setDisabled(true);
        //                    getGrossWtBinding().setDisabled(true);
        //                    getPriceBinding().setDisabled(true);
        //                    getCustMrpBinding().setDisabled(true);
        //                    getToolCostBinding().setDisabled(true);
        //                    getBsrStockBinding().setDisabled(true);
        //                    getPackQtyBinding().setDisabled(false);
        //                    getItemTypeBinding().setDisabled(true);
        //                    getProductCodeBinding().setDisabled(true);
        //                    getDisPerBinding().setDisabled(true);
        //                    if (stkChkReq.equals('Y')){
        //                        ADFUtils.findOperation("setValuesPendingSaleOrder").execute();
        //                    }
        //                    //---------------------------------------------------
        //                } else if(poValue.equals("V")){
        //                    getSelectDispatchBtBinding().setDisabled(true);
        //                    getSelectOrdersBtBinding().setDisabled(true);
        //                    //getAddButDetailBinding().setDisabled(false);
        //                    //--------------------Disable Detail Item-----------
        //                    getQuantityBinding().setDisabled(false);
        //                    getGrossWtBinding().setDisabled(false);
        //                    getPriceBinding().setDisabled(false);
        //                    getCustMrpBinding().setDisabled(false);
        //                    getToolCostBinding().setDisabled(false);
        //                    getBsrStockBinding().setDisabled(false);
        //                    getPackQtyBinding().setDisabled(false);
        //                    getDisPerBinding().setDisabled(false);
        //
        //                    getItemTypeBinding().setDisabled(false);
        //                    getProductCodeBinding().setDisabled(false);
        //                    //---------------------------------------------------
        //                } else {
        //                    getSelectDispatchBtBinding().setDisabled(true);
        //                    getSelectOrdersBtBinding().setDisabled(true);
        //                    //getAddButDetailBinding().setDisabled(false);
        //                    //--------------------Disable Detail Item-----------
        //                    getQuantityBinding().setDisabled(false);
        //                    getGrossWtBinding().setDisabled(false);
        //                    getPriceBinding().setDisabled(false);
        //                    getCustMrpBinding().setDisabled(false);
        //                    getToolCostBinding().setDisabled(false);
        //                    getBsrStockBinding().setDisabled(false);
        //                    getPackQtyBinding().setDisabled(false);
        //
        //                    getItemTypeBinding().setDisabled(false);
        //                    getProductCodeBinding().setDisabled(false);
        //                    getDisPerBinding().setDisabled(false);
        //                    //---------------------------------------------------
        //                }
        //            }
        //            else {
        //                getSelectDispatchBtBinding().setDisabled(true);
        //                getSelectOrdersBtBinding().setDisabled(true);
        //                //getAddButDetailBinding().setDisabled(false);
        //                //--------------------Disable Detail Item-----------
        //                getQuantityBinding().setDisabled(false);
        //                getGrossWtBinding().setDisabled(false);
        //                getPriceBinding().setDisabled(false);
        //                getCustMrpBinding().setDisabled(false);
        //                getToolCostBinding().setDisabled(false);
        //                getBsrStockBinding().setDisabled(false);
        //                getPackQtyBinding().setDisabled(false);
        //
        //                getItemTypeBinding().setDisabled(false);
        //                getProductCodeBinding().setDisabled(false);
        //                getDisPerBinding().setDisabled(false);
        //                //---------------------------------------------------
        //            }
        //        }
        //        else{
        //            getSelectDispatchBtBinding().setDisabled(true);
        //            getSelectOrdersBtBinding().setDisabled(true);
        //            //getAddButDetailBinding().setDisabled(false);
        //            //--------------------Disable Detail Item-----------
        //            getQuantityBinding().setDisabled(false);
        //            getGrossWtBinding().setDisabled(false);
        //            getPriceBinding().setDisabled(false);
        //            getCustMrpBinding().setDisabled(false);
        //            getToolCostBinding().setDisabled(false);
        //            getBsrStockBinding().setDisabled(false);
        //            getPackQtyBinding().setDisabled(false);
        //
        //            getItemTypeBinding().setDisabled(false);
        //            getProductCodeBinding().setDisabled(false);
        //            getDisPerBinding().setDisabled(false);
        //            //---------------------------------------------------
        //        }
        System.out.println("value_check is=====>" + value_check);
    }


    public void setDispAdviceNoBinding(RichInputText dispAdviceNoBinding) {
        this.dispAdviceNoBinding = dispAdviceNoBinding;
    }

    public RichInputText getDispAdviceNoBinding() {
        return dispAdviceNoBinding;
    }

    public void setTransporterCdBinding(RichInputComboboxListOfValues transporterCdBinding) {
        this.transporterCdBinding = transporterCdBinding;
    }

    public RichInputComboboxListOfValues getTransporterCdBinding() {
        return transporterCdBinding;
    }

    public void setDestinationBinding(RichInputText destinationBinding) {
        this.destinationBinding = destinationBinding;
    }

    public RichInputText getDestinationBinding() {
        return destinationBinding;
    }

    public void setOtherDetailBinding(RichShowDetailItem otherDetailBinding) {
        this.otherDetailBinding = otherDetailBinding;
    }

    public RichShowDetailItem getOtherDetailBinding() {
        return otherDetailBinding;
    }

    public void setInvSubTypeBinding(RichInputText invSubTypeBinding) {
        this.invSubTypeBinding = invSubTypeBinding;
    }

    public RichInputText getInvSubTypeBinding() {
        return invSubTypeBinding;
    }

    public void setAddIdentifierBinding(RichInputText addIdentifierBinding) {
        this.addIdentifierBinding = addIdentifierBinding;
    }

    public RichInputText getAddIdentifierBinding() {
        return addIdentifierBinding;
    }

    public void dolDispatchPopupDL(DialogEvent dialogEvent) {
        OperationBinding binding = ADFUtils.findOperation("insertDataInToInvoiceDetails");
        binding.getParamsMap().put("source_mode", "DA");
        binding.execute();
        da_and_amd = "";

        System.out.println("ADFUtils.evaluateEL(\"#{bindings.InsurType.inputValue}\")" +
                           ADFUtils.evaluateEL("#{bindings.InsurType.inputValue}"));

        try {
            if (ADFUtils.evaluateEL("#{bindings.InsurType.inputValue}").equals("U"))
                insuranceTypeValueEnabledDisabled(true);
            else
                insuranceTypeValueEnabledDisabled(false);
        } catch (Exception e) {
            // TODO: Add catch code
            e.printStackTrace();
        }


    }

    public void setDolDispatchBinding(RichPopup dolDispatchBinding) {
        this.dolDispatchBinding = dolDispatchBinding;
    }

    public RichPopup getDolDispatchBinding() {
        return dolDispatchBinding;
    }

    public String dolDispatchNoAction() {
        // Add event code here...
        da_and_amd = "";
        OperationBinding binding = ADFUtils.findOperation("filterDolDispatchVOInvoiceEntry");
        binding.getParamsMap().put("filter_mode", "D");
        binding.execute();
        ADFUtils.showPopup(dolDispatchBinding);
        // AdfFacesContext.getCurrentInstance().addPartialTarget(getDolDispatchTableBinding());


        return null;
    }

    public void setDolDispatchNoButtonBinding(RichButton dolDispatchNoButtonBinding) {
        this.dolDispatchNoButtonBinding = dolDispatchNoButtonBinding;
    }

    public RichButton getDolDispatchNoButtonBinding() {
        return dolDispatchNoButtonBinding;
    }

    public void setInvHeadTypeBinding(RichInputComboboxListOfValues invHeadTypeBinding) {
        this.invHeadTypeBinding = invHeadTypeBinding;
    }

    public RichInputComboboxListOfValues getInvHeadTypeBinding() {
        return invHeadTypeBinding;
    }

    public void setNoOfCasesBinding(RichInputText noOfCasesBinding) {
        this.noOfCasesBinding = noOfCasesBinding;
    }

    public RichInputText getNoOfCasesBinding() {
        return noOfCasesBinding;
    }

    public void disbaledFieldsAccordingToConditions(Boolean val) {
        getCustCodeBinding().setDisabled(val);
        //getDestinationBinding().setDisabled(val);
        //getTransporterCdBinding().setDisabled(val);
        getNoOfCasesBinding().setDisabled(val);

        AdfFacesContext.getCurrentInstance().addPartialTarget(custCodeBinding);
        AdfFacesContext.getCurrentInstance().addPartialTarget(destinationBinding);
        AdfFacesContext.getCurrentInstance().addPartialTarget(noOfCasesBinding);

    }

    public void dolDispatchCheckBox(ValueChangeEvent vce) {
        //        System.out.println("Starting Data");
        //        String values = null; //DolPendingDispatchAdvcDetail
        //        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        //        Row row = (Row) ADFUtils.evaluateEL("#{bindings.DolPendingDispatchAdvcDetailVVO1Iterator.currentRow}");
        //
        //        System.out.println("row.getAttribute(\"TransCheckBox\")"+row.getAttribute("TransCheckBox"));
        //
        //        if (vce.getNewValue() != null) {
        //            if (row.getAttribute("TransCheckBox").equals("true") || row.getAttribute("TransCheckBox").equals("Y")) {
        //                if (da_and_amd == null || da_and_amd == "") {
        //                    da_and_amd = row.getAttribute("DaNo") + "" + row.getAttribute("AmdNo");
        //                } else {
        //                    values = row.getAttribute("DaNo") + "" + row.getAttribute("AmdNo");
        //                    if (da_and_amd != null && values != null) {
        //                        if (!da_and_amd.equals(values)) {
        ////                            ADFUtils.showMessage("Please select same Dispatch Advice No.", 0);
        ////                            //row.setAttribute("TransCheckBox", "false");
        ////                            doltransCheckBoxBinding.setValue("false");
        ////                            AdfFacesContext.getCurrentInstance().addPartialTarget(doltransCheckBoxBinding);
        //                        }
        //
        //                    }
        //
        //                }
        //            } else {
        //                OperationBinding unselectOpr = ADFUtils.findOperation("dolCheckAllUnCheckValuesInvoiceEntry");
        //                unselectOpr.execute();
        //                System.out.println("UN-SELECT VALUE IN BEAN:=>" + unselectOpr.getResult());
        //                if (unselectOpr.getResult().equals("N"))
        //                    da_and_amd = "";
        //            }
        //    }

        System.out.println("Starting Data");
        String values = null; //DolPendingDispatchAdvcDetail
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        Row row = (Row) ADFUtils.evaluateEL("#{bindings.DolPendingDispatchAdvcHeaderVVO1Iterator.currentRow}");

        System.out.println("row.getAttribute(\"TransCheckBox\")" + row.getAttribute("TransChkbox"));

        if (vce.getNewValue() != null) {
            if (row.getAttribute("TransChkbox").equals("true") || row.getAttribute("TransChkbox").equals("Y")) {
                if (da_and_amd == null || da_and_amd == "") {
                    da_and_amd = row.getAttribute("DaNo") + "" + row.getAttribute("AmdNo");
                } else {
                    values = row.getAttribute("DaNo") + "" + row.getAttribute("AmdNo");
                    if (da_and_amd != null && values != null) {
                        if (!da_and_amd.equals(values)) {
                            //                            ADFUtils.showMessage("Please select same Dispatch Advice No.", 0);
                            //                            //row.setAttribute("TransCheckBox", "false");
                            //                            doltransCheckBoxBinding.setValue("false");
                            //                            AdfFacesContext.getCurrentInstance().addPartialTarget(doltransCheckBoxBinding);
                        }

                    }

                }
            } else {
                OperationBinding unselectOpr = ADFUtils.findOperation("dolCheckAllUnCheckValuesInvoiceEntry");
                unselectOpr.execute();
                System.out.println("UN-SELECT VALUE IN BEAN:=>" + unselectOpr.getResult());
                if (unselectOpr.getResult().equals("N"))
                    da_and_amd = "";
            }
        }
    }

    public void setDolDispatchTableBinding(RichTable dolDispatchTableBinding) {
        this.dolDispatchTableBinding = dolDispatchTableBinding;
    }

    public RichTable getDolDispatchTableBinding() {
        return dolDispatchTableBinding;
    }

    public void setDoltransCheckBoxBinding(RichSelectBooleanCheckbox doltransCheckBoxBinding) {
        this.doltransCheckBoxBinding = doltransCheckBoxBinding;
    }

    public RichSelectBooleanCheckbox getDoltransCheckBoxBinding() {
        return doltransCheckBoxBinding;
    }

    public void setPreparedByBinding(RichInputComboboxListOfValues preparedByBinding) {
        this.preparedByBinding = preparedByBinding;
    }

    public RichInputComboboxListOfValues getPreparedByBinding() {
        return preparedByBinding;
    }

    public void setAdditionalInvoiceOneTabBinding(RichShowDetailItem additionalInvoiceOneTabBinding) {
        this.additionalInvoiceOneTabBinding = additionalInvoiceOneTabBinding;
    }

    public RichShowDetailItem getAdditionalInvoiceOneTabBinding() {
        return additionalInvoiceOneTabBinding;
    }

    public void setAdditionalInvoiceTwoTabBinding(RichShowDetailItem additionalInvoiceTwoTabBinding) {
        this.additionalInvoiceTwoTabBinding = additionalInvoiceTwoTabBinding;
    }

    public RichShowDetailItem getAdditionalInvoiceTwoTabBinding() {
        return additionalInvoiceTwoTabBinding;
    }

    public void populateInvoiceDataAL(ActionEvent actionEvent) {
        if (invTransBinding.getValue() != null)
            ADFUtils.findOperation("populateDataInInvoiceDetailTableInvoceEntry").execute();
        else
            ADFUtils.showMessage("Please select Invoice No.", 1);

    }

    public void setInvTransBinding(RichInputComboboxListOfValues invTransBinding) {
        this.invTransBinding = invTransBinding;
    }

    public RichInputComboboxListOfValues getInvTransBinding() {
        return invTransBinding;
    }

    public void setPopulateButtonBinding(RichButton populateButtonBinding) {
        this.populateButtonBinding = populateButtonBinding;
    }

    public RichButton getPopulateButtonBinding() {
        return populateButtonBinding;
    }

    public void invPopulateVCE(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (valueChangeEvent.getNewValue() != null)
            ADFUtils.findOperation("populateDataInInvoiceDetailTableInvoceEntry").execute();
    }

    public void setInsCompNameBinding(RichInputText insCompNameBinding) {
        this.insCompNameBinding = insCompNameBinding;
    }

    public RichInputText getInsCompNameBinding() {
        return insCompNameBinding;
    }

    public void setValidFromDateBinding(RichInputDate validFromDateBinding) {
        this.validFromDateBinding = validFromDateBinding;
    }

    public RichInputDate getValidFromDateBinding() {
        return validFromDateBinding;
    }

    public void InsTypVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce.getNewValue() != null) {
            if (vce.getNewValue().equals("U"))
                insuranceTypeValueEnabledDisabled(true);
            else
                insuranceTypeValueEnabledDisabled(false);
        }

        //insuranceTypeValueEnabledDisabled

    }

    public void insuranceTypeValueEnabledDisabled(Boolean val) {
        getInsCompNameBinding().setDisabled(val);
        getValidFromDateBinding().setDisabled(val);


    }

    public void setFcrDateBinding(RichInputDate fcrDateBinding) {
        this.fcrDateBinding = fcrDateBinding;
    }

    public RichInputDate getFcrDateBinding() {
        return fcrDateBinding;
    }

    public void setAuthNoBinding(RichInputText authNoBinding) {
        this.authNoBinding = authNoBinding;
    }

    public RichInputText getAuthNoBinding() {
        return authNoBinding;
    }

    public void setAuthDateBinding(RichInputDate authDateBinding) {
        this.authDateBinding = authDateBinding;
    }

    public RichInputDate getAuthDateBinding() {
        return authDateBinding;
    }

    public void fcrNoValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {

        System.out.println("ROW COUNT===>" +
                           ADFUtils.evaluateEL("#{bindings.InvoiceEntryDetailVO1Iterator.estimatedRowCount}"));
        if ((Long) ADFUtils.evaluateEL("#{bindings.InvoiceEntryDetailVO1Iterator.estimatedRowCount}") > 0) {
            Row row = (Row) ADFUtils.evaluateEL("#{bindings.InvoiceEntryHeaderVO1Iterator.currentRow}");
            Row row1 = (Row) ADFUtils.evaluateEL("#{bindings.InvoiceEntryDetailVO1Iterator.currentRow}");

            if (row.getAttribute("LicQty") != null) {
                BigDecimal dtl_qty =
                    (BigDecimal) (row1.getAttribute("TotalQtyTrans") != null ? row1.getAttribute("TotalQtyTrans") :
                                  new BigDecimal(0));
                BigDecimal lic_qty = (BigDecimal) row.getAttribute("LicQty");
                if (!(dtl_qty.compareTo(lic_qty) <= 0)) {
                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                  "Quantity must be less then or equals to Balance Qunatity.",
                                                                  ""));
                }

            }
        }

    }

    public void setBalQuantityBinding(RichInputText balQuantityBinding) {
        this.balQuantityBinding = balQuantityBinding;
    }

    public RichInputText getBalQuantityBinding() {
        return balQuantityBinding;
    }

    public void setInvSubType1Binding(RichSelectOneChoice invSubType1Binding) {
        this.invSubType1Binding = invSubType1Binding;
    }

    public RichSelectOneChoice getInvSubType1Binding() {
        return invSubType1Binding;
    }


    public void setExportDetailPopupBinding(RichPopup exportDetailPopupBinding) {
        this.exportDetailPopupBinding = exportDetailPopupBinding;
    }

    public RichPopup getExportDetailPopupBinding() {
        return exportDetailPopupBinding;
    }

    public void exportDetailButton(ActionEvent actionEvent) {

        ADFUtils.showPopup(exportDetailPopupBinding);
        ADFUtils.findOperation("allRemarksFieldValuesSetInvoiceEntryDetail").execute();


    }

    public void setFcrNoBinding(RichInputComboboxListOfValues fcrNoBinding) {
        this.fcrNoBinding = fcrNoBinding;
    }

    public RichInputComboboxListOfValues getFcrNoBinding() {
        return fcrNoBinding;
    }

    public void setCustPoNoBinding(RichInputText custPoNoBinding) {
        this.custPoNoBinding = custPoNoBinding;
    }

    public RichInputText getCustPoNoBinding() {
        return custPoNoBinding;
    }

    public void setCustPoDateBinding(RichInputDate custPoDateBinding) {
        this.custPoDateBinding = custPoDateBinding;
    }

    public RichInputDate getCustPoDateBinding() {
        return custPoDateBinding;
    }

    public void setCustProdDesBinding(RichInputText custProdDesBinding) {
        this.custProdDesBinding = custProdDesBinding;
    }

    public RichInputText getCustProdDesBinding() {
        return custProdDesBinding;
    }

    public void tcsPerVCE(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        OperationBinding binding = (OperationBinding) ADFUtils.findOperation("refreshAmount2");
        binding.getParamsMap().put("pkeyid", 800);
        binding.getParamsMap().put("pkeyno", 1);
        binding.getParamsMap().put("punitcd", getUnitCodeBinding().getValue().toString());
        binding.execute();
        AdfFacesContext.getCurrentInstance().addPartialTarget(tcsAmtSum);
    }

    public void setEwayDateBinding(RichInputDate ewayDateBinding) {
        this.ewayDateBinding = ewayDateBinding;
    }

    public RichInputDate getEwayDateBinding() {
        return ewayDateBinding;
    }

    public void tcsAmountVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        System.out.println("vce.getNewValue()" + vce.getNewValue());
        if (vce.getNewValue() != null) {

            Row row = (Row) ADFUtils.evaluateEL("#{bindings.InvoiceEntryHeaderVO1Iterator.currentRow}");


            BigDecimal tcs_amount = (BigDecimal) (vce.getNewValue() != null ? vce.getNewValue() : new BigDecimal(0));
            BigDecimal gross_amount = (BigDecimal) row.getAttribute("GrossAmount");
            BigDecimal cgst = (BigDecimal) row.getAttribute("Cgst");
            BigDecimal igst = (BigDecimal) row.getAttribute("Igst");
            BigDecimal sgst = (BigDecimal) row.getAttribute("Sgst");
            BigDecimal result = gross_amount.add(tcs_amount).add(cgst).add(igst).add(sgst);
            result = result.setScale(2, BigDecimal.ROUND_HALF_UP);
            ;
            row.setAttribute("NetAmount", result);
            AdfFacesContext.getCurrentInstance().addPartialTarget(netAmtSum);

        }


    }

    public void setLutSrNoBinding(RichInputComboboxListOfValues lutSrNoBinding) {
        this.lutSrNoBinding = lutSrNoBinding;
    }

    public RichInputComboboxListOfValues getLutSrNoBinding() {
        return lutSrNoBinding;
    }


    public void downloadAction(FacesContext facesContext, OutputStream outputStream) throws Exception {

        File filed = new File(pathBinding.getValue().toString());
        FileInputStream fis;
        byte[] b;
        try {
            fis = new FileInputStream(filed);

            int n;
            while ((n = fis.available()) > 0) {
                b = new byte[n];
                int result = fis.read(b);
                outputStream.write(b, 0, b.length);
                if (result == -1)
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        outputStream.flush();

    }


    public void uploadAction(ValueChangeEvent vce) {
        // Add event code here...
        System.out.println("New Value In VCE Of Upload File" + vce.getNewValue());
        if (vce.getNewValue() != null) {
            //Get File Object from VC Event
            UploadedFile fileVal = (UploadedFile) vce.getNewValue();
            //Upload File to path- Return actual server path
            String path = ADFUtils.uploadFile(fileVal);
            System.out.println(fileVal.getContentType());
            System.out.println("Content Type==>>" + fileVal.getContentType() + "Path==>>" + path + "File name==>>" +
                               fileVal.getFilename());
            //Method to insert data in table to keep track of uploaded files
            OperationBinding ob = ADFUtils.findOperation("setFileDataInvoice");
            ob.getParamsMap().put("name", fileVal.getFilename());
            ob.getParamsMap().put("path", path);
            ob.getParamsMap().put("contTyp", fileVal.getContentType());
            ob.getParamsMap().put("SrNo",
                                  ADFUtils.evaluateEL("#{bindings.DocAttachRefDtlVO1Iterator.estimatedRowCount+1}"));
            ob.execute();
            // Reset inputFile component after upload
            ResetUtils.reset(vce.getComponent());
        }
    }

    public void setPathBinding(RichInputText pathBinding) {
        this.pathBinding = pathBinding;
    }

    public RichInputText getPathBinding() {
        return pathBinding;
    }

    public void setDocSeqNoBinding(RichInputText docSeqNoBinding) {
        this.docSeqNoBinding = docSeqNoBinding;
    }

    public RichInputText getDocSeqNoBinding() {
        return docSeqNoBinding;
    }

    public void setUnitCodeDocRefBinding(RichInputText unitCodeDocRefBinding) {
        this.unitCodeDocRefBinding = unitCodeDocRefBinding;
    }

    public RichInputText getUnitCodeDocRefBinding() {
        return unitCodeDocRefBinding;
    }

    public void setRefDocNoBinding(RichInputText refDocNoBinding) {
        this.refDocNoBinding = refDocNoBinding;
    }

    public RichInputText getRefDocNoBinding() {
        return refDocNoBinding;
    }

    public void setRefDocTypeBinding(RichInputText refDocTypeBinding) {
        this.refDocTypeBinding = refDocTypeBinding;
    }

    public RichInputText getRefDocTypeBinding() {
        return refDocTypeBinding;
    }

    public void setDocFileNameBinding(RichInputText docFileNameBinding) {
        this.docFileNameBinding = docFileNameBinding;
    }

    public RichInputText getDocFileNameBinding() {
        return docFileNameBinding;
    }

    public void setRefDocDateBinding(RichInputDate refDocDateBinding) {
        this.refDocDateBinding = refDocDateBinding;
    }

    public RichInputDate getRefDocDateBinding() {
        return refDocDateBinding;
    }

    public void docRefDeletePopupDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete1").execute();
            ADFUtils.showMessage("Record Deleted Successfully", 2);
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(docRefTableBinding);

    }

    public void setDocRefTableBinding(RichTable docRefTableBinding) {
        this.docRefTableBinding = docRefTableBinding;
    }

    public RichTable getDocRefTableBinding() {
        return docRefTableBinding;
    }

    public void setRefDocTabBinding(RichShowDetailItem refDocTabBinding) {
        this.refDocTabBinding = refDocTabBinding;
    }

    public RichShowDetailItem getRefDocTabBinding() {
        return refDocTabBinding;
    }

    public void cessVCE(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        OperationBinding binding = (OperationBinding) ADFUtils.findOperation("refreshAmount2");
        binding.getParamsMap().put("pkeyid", 800);
        binding.getParamsMap().put("pkeyno", 1);
        binding.getParamsMap().put("punitcd", getUnitCodeBinding().getValue().toString());
        binding.execute();
    }

    public void setCessAmountBinding(RichInputText cessAmountBinding) {
        this.cessAmountBinding = cessAmountBinding;
    }

    public RichInputText getCessAmountBinding() {
        return cessAmountBinding;
    }

    public void setDownloadLinkBinding(RichCommandLink downloadLinkBinding) {
        this.downloadLinkBinding = downloadLinkBinding;
    }

    public RichCommandLink getDownloadLinkBinding() {
        return downloadLinkBinding;
    }

    public void setDoltransCheckBoxBinding1(RichSelectBooleanCheckbox doltransCheckBoxBinding1) {
        this.doltransCheckBoxBinding1 = doltransCheckBoxBinding1;
    }

    public RichSelectBooleanCheckbox getDoltransCheckBoxBinding1() {
        return doltransCheckBoxBinding1;
    }

    public void setBatchNumberBinding(RichInputText batchNumberBinding) {
        this.batchNumberBinding = batchNumberBinding;
    }

    public RichInputText getBatchNumberBinding() {
        return batchNumberBinding;
    }

    public void setExpDtBinding(RichInputText expDtBinding) {
        this.expDtBinding = expDtBinding;
    }

    public RichInputText getExpDtBinding() {
        return expDtBinding;
    }

    public void setMfgDtBinding(RichInputText mfgDtBinding) {
        this.mfgDtBinding = mfgDtBinding;
    }

    public RichInputText getMfgDtBinding() {
        return mfgDtBinding;
    }

    public void invDateVCL(ValueChangeEvent valueChangeEvent) throws ParseException {
        System.out.println("invDateVCL" + valueChangeEvent.getNewValue().toString());
        Object date = ADFUtils.convertAdToBs(valueChangeEvent.getNewValue().toString());
        System.out.println("invDateVCL" + date);
        //            invDateNepalBinding.setValue(date.toString());
        DCIteratorBinding dci = ADFUtils.findIterator("InvoiceEntryHeaderVO1Iterator");
        dci.getCurrentRow().setAttribute("DatesNepal", date);
    }

    public void invDateNepalVCL(ValueChangeEvent valueChangeEvent) throws ParseException {
        System.out.println("invDateNepalVCL" + valueChangeEvent.getNewValue().toString());
        //        Object date=ADFUtils.convertAdToBs(valueChangeEvent.getNewValue().toString());
        //        System.out.println("invDateNepalVCL"+date);
        //            invDateNepalBinding.setValue(date.toString());
        //                DCIteratorBinding dci = ADFUtils.findIterator("InvoiceEntryHeaderVO1Iterator");
        //                dci.getCurrentRow().setAttribute("Dates",date);
    }

    public void setInvDateNepalBinding(RichInputText invDateNepalBinding) {
        this.invDateNepalBinding = invDateNepalBinding;
    }

    public RichInputText getInvDateNepalBinding() {
        return invDateNepalBinding;
    }

    public void frQtyVCL(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        OperationBinding ob = (OperationBinding) ADFUtils.findOperation("refreshAmount2");
        ob.getParamsMap().put("pkeyid", 800);
        ob.getParamsMap().put("pkeyno", 1);
        ob.getParamsMap().put("punitcd", getUnitCodeBinding().getValue().toString());
        ob.execute();
        AdfFacesContext.getCurrentInstance().addPartialTarget(itemsummTableBinding);
    }

    public void setItemsummTableBinding(RichTable itemsummTableBinding) {
        this.itemsummTableBinding = itemsummTableBinding;
    }

    public RichTable getItemsummTableBinding() {
        return itemsummTableBinding;
    }

    public void setFrExcHeaderBinding(RichInputText frExcHeaderBinding) {
        this.frExcHeaderBinding = frExcHeaderBinding;
    }

    public RichInputText getFrExcHeaderBinding() {
        return frExcHeaderBinding;
    }

    public void setFrVatHeaderBinding(RichInputText frVatHeaderBinding) {
        this.frVatHeaderBinding = frVatHeaderBinding;
    }

    public RichInputText getFrVatHeaderBinding() {
        return frVatHeaderBinding;
    }

    public void igstAmtVCL(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        OperationBinding ob = (OperationBinding) ADFUtils.findOperation("refreshAmount3");
        ob.getParamsMap().put("pkeyid", 800);
        ob.getParamsMap().put("pkeyno", 1);
        ob.getParamsMap().put("punitcd", getUnitCodeBinding().getValue().toString());
        ob.execute();
        AdfFacesContext.getCurrentInstance().addPartialTarget(itemsummTableBinding);
    }

    public void FrExcVCL(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        OperationBinding ob = (OperationBinding) ADFUtils.findOperation("refreshAmount3");
        ob.getParamsMap().put("pkeyid", 800);
        ob.getParamsMap().put("pkeyno", 1);
        ob.getParamsMap().put("punitcd", getUnitCodeBinding().getValue().toString());
        ob.execute();
        AdfFacesContext.getCurrentInstance().addPartialTarget(itemsummTableBinding);
    }

    public void sgstAmtVCL(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        OperationBinding ob = (OperationBinding) ADFUtils.findOperation("refreshAmount3");
        ob.getParamsMap().put("pkeyid", 800);
        ob.getParamsMap().put("pkeyno", 1);
        ob.getParamsMap().put("punitcd", getUnitCodeBinding().getValue().toString());
        ob.execute();
        AdfFacesContext.getCurrentInstance().addPartialTarget(itemsummTableBinding);
    }

    public void setTotAmtInvoiceHeaderBinding(RichInputText totAmtInvoiceHeaderBinding) {
        this.totAmtInvoiceHeaderBinding = totAmtInvoiceHeaderBinding;
    }

    public RichInputText getTotAmtInvoiceHeaderBinding() {
        return totAmtInvoiceHeaderBinding;
    }

    public void setRoundOffHeaderBinding(RichInputText roundOffHeaderBinding) {
        this.roundOffHeaderBinding = roundOffHeaderBinding;
    }

    public RichInputText getRoundOffHeaderBinding() {
        return roundOffHeaderBinding;
    }

    public void setPackUomBinding(RichInputText packUomBinding) {
        this.packUomBinding = packUomBinding;
    }

    public RichInputText getPackUomBinding() {
        return packUomBinding;
    }

    public void setUomBinding(RichInputText uomBinding) {
        this.uomBinding = uomBinding;
    }

    public RichInputText getUomBinding() {
        return uomBinding;
    }

    public void setNetWeightBinding(RichInputText netWeightBinding) {
        this.netWeightBinding = netWeightBinding;
    }

    public RichInputText getNetWeightBinding() {
        return netWeightBinding;
    }
}
