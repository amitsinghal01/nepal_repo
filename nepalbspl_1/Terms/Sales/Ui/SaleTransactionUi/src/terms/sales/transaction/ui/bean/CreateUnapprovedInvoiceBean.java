package terms.sales.transaction.ui.bean;

import java.lang.reflect.Method;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;
import javax.faces.validator.ValidatorException;

import oracle.adf.model.OperationBinding;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichPanelLabelAndMessage;
import oracle.adf.view.rich.component.rich.nav.RichButton;

public class CreateUnapprovedInvoiceBean {
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText invoiceTypeBinding;
    private RichInputText yearBinding;
    private RichPanelLabelAndMessage bindingOutputText;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichInputDate unapprovedDateBinding;
    private RichInputText timebinding;
    private RichInputComboboxListOfValues approveVCE;

    public CreateUnapprovedInvoiceBean() {
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setInvoiceTypeBinding(RichInputText invoiceTypeBinding) {
        this.invoiceTypeBinding = invoiceTypeBinding;
    }

    public RichInputText getInvoiceTypeBinding() {
        return invoiceTypeBinding;
    }

    public void setYearBinding(RichInputText yearBinding) {
        this.yearBinding = yearBinding;
    }

    public RichInputText getYearBinding() {
        return yearBinding;
    }

    public void setBindingOutputText(RichPanelLabelAndMessage bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichPanelLabelAndMessage getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }
    private void cevmodecheck(){
            if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
                cevModeDisableComponent("V");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("C");
            }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
                makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
                cevModeDisableComponent("E");
            }
        }
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
              try {
                  Method method1 =
                      component.getClass().getMethod("setDisabled", boolean.class);
                  if (method1 != null) {
                      method1.invoke(component, valueComponent);
                  }
              } catch (NoSuchMethodException e) {
                  try {
                      Method method =
                          component.getClass().getMethod("setReadOnly", boolean.class);
                      if (method != null) {
                          method.invoke(component, valueComponent);
                      }
                  } catch (Exception e1) {
                      // e.printStackTrace();//silently eat this exception.
                  }


              } catch (Exception e) {
                  // e.printStackTrace();//silently eat this exception.
              }
              List<UIComponent> childComponents = component.getChildren();
              for (UIComponent comp : childComponents) {
                  makeComponentHierarchyReadOnly(comp, valueComponent);
              }
          }
   //Set Fields and Button disable.
      public void cevModeDisableComponent(String mode) {
      //            FacesContext fctx = FacesContext.getCurrentInstance();
      //            ELContext elctx = fctx.getELContext();
      //            Application jsfApp = fctx.getApplication();
      //            //create a ValueExpression that points to the ADF binding layer
      //            ExpressionFactory exprFactory = jsfApp.getExpressionFactory();
      //            //
      //            ValueExpression valueExpr = exprFactory.createValueExpression(
      //                                         elctx,
      //                                         "#{pageFlowScope.mode=='E'}",
      //                                          Object.class
      //                                         );
      //            getEtdsBinding().setDisabled(Boolean.parseBoolean(valueExpr.getValue(elctx).toString()));
          if (mode.equals("E")) {
              getUnitCodeBinding().setDisabled(true);
              getInvoiceTypeBinding().setDisabled(true);
              getYearBinding().setDisabled(true);
              getUnapprovedDateBinding().setDisabled(true);
              getTimebinding().setDisabled(true);
          } else if (mode.equals("C")) {
              getUnitCodeBinding().setDisabled(true);
              getInvoiceTypeBinding().setDisabled(true);
              getYearBinding().setDisabled(true);
             // getHeaderEditBinding().setDisabled(true);
              getUnapprovedDateBinding().setDisabled(true);
               getTimebinding().setDisabled(true);
          } else if (mode.equals("V")) {
           
          }
          
      }

    public void editButtonAL(ActionEvent actionEvent) {
        ADFUtils.showMessage("Can not edit this record.",2);
        ADFUtils.setEL("#{pageFlowScope.mode}","V");
        //cevmodecheck();
    }

    public void PasswordValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if(object!=null)
        {
            System.out.println("object========="+object);
        OperationBinding ob = (OperationBinding) ADFUtils.findOperation("PasswordCheck");
        ob.getParamsMap().put("Password",object);
        ob.execute();
        System.out.println("ob.getResu"+ob.getResult());
        if(ob.getResult()!=null &&ob.getResult().toString().equalsIgnoreCase("Y"))
        {
         throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Password does not match",""));
        }


         }

    }

    public void setUnapprovedDateBinding(RichInputDate unapprovedDateBinding) {
        this.unapprovedDateBinding = unapprovedDateBinding;
    }

    public RichInputDate getUnapprovedDateBinding() {
        return unapprovedDateBinding;
    }

    public void setTimebinding(RichInputText timebinding) {
        this.timebinding = timebinding;
    }

    public RichInputText getTimebinding() {
        return timebinding;
    }

    public void saveButtonAL(ActionEvent actionEvent) 
    {
        ADFUtils.setLastUpdatedBy("UnapprovedInvoiceVO1Iterator","LastUpdatedBy");
        OperationBinding opr =(OperationBinding) ADFUtils.findOperation("saveDataUnApproveInvoice");
        opr.execute();
        System.out.println("Result======="+opr.getResult());
        if(opr.getResult()!=null)
        {
        if(opr.getResult().equals("Y"))
        {
         System.out.println("==========When result is Y==========");
        ADFUtils.findOperation("Commit").execute();
        ADFUtils.showMessage("Record saved successfully.", 2);
        ADFUtils.findOperation("callReturnInvApi").execute();
        ADFUtils.setEL("#{pageFlowScope.mode}","V");
        }
        }
    }

    public void setApproveVCE(RichInputComboboxListOfValues approveVCE) {
        this.approveVCE = approveVCE;
    }

    public RichInputComboboxListOfValues getApproveVCE() {
        return approveVCE;
    }

    public void approveVCE(ValueChangeEvent valueChangeEvent) 
    {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        ADFUtils.findOperation("checkDeleteAndCancelAuthorityForUnApproveInvoice").execute();
    }

    public String saveAndCloseAction() {
        ADFUtils.setLastUpdatedBy("UnapprovedInvoiceVO1Iterator","LastUpdatedBy");
        OperationBinding opr =(OperationBinding) ADFUtils.findOperation("saveDataUnApproveInvoice");
        opr.execute();
        System.out.println("Result======="+opr.getResult());
        if(opr.getResult()!=null)
        {
        if(opr.getResult().equals("Y"))
        {
         System.out.println("==========When result is Y==========");
        ADFUtils.findOperation("Commit").execute();
        ADFUtils.showMessage("Record saved successfully.", 2);
        ADFUtils.findOperation("callReturnInvApi").execute();
        return "SaveAndClose";
        }
        }
        else
        {
            ADFUtils.showMessage("problem in record save.",0);

        }
        return null;
    }
}
