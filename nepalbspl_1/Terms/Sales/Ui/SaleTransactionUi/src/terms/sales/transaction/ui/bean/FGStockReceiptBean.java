package terms.sales.transaction.ui.bean;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;

import java.math.BigDecimal;

import java.sql.Connection;

import java.sql.SQLException;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.validator.ValidatorException;

import javax.naming.Context;
import javax.naming.InitialContext;

import javax.servlet.http.HttpServletResponse;

import javax.sql.DataSource;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectBooleanCheckbox;

import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.ValidationException;
import oracle.jbo.ViewObject;

import oracle.jbo.domain.Date;

import oracle.jbo.domain.Timestamp;

import terms.sales.transaction.model.view.FgStockReceiptVORowImpl;

public class FGStockReceiptBean {
    private String editAction="V";
    private String goDownMode ="";
    private RichTable tableBinding;
    private RichInputText rcvdQtyBinding;
    private RichInputText prdctQtyBinding;
    private RichInputDate asOnDateBinding;
    private RichInputDate entryDateBinding;
    private RichInputDate docDateBinding;


    public FGStockReceiptBean() {
    }

    public void populateDetailAL(ActionEvent actionEvent) {
        OperationBinding ob=ADFUtils.findOperation("PopulateFgStockReceipt");
       Object rst= ob.execute();
       System.out.println("resulte==="+ob.getResult());
       if(ob.getResult()!=null && ob.getResult().toString().equalsIgnoreCase("C"))
       {
           ADFUtils.showMessage("Data can not be populated.", 2);
           }
    }

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }
    public void isEnableEdit(ActionEvent actionEvent) {
    this.editAction=getEditAction();
    }

    public void saveAL(ActionEvent actionEvent) {
       
    
        DCIteratorBinding dciter =
                    (DCIteratorBinding) ADFUtils.findIterator("FgStockReceiptVO1Iterator");

         ViewObject voh = dciter.getViewObject();
//         Row[] r=vo.getFilteredRows("TransCheck", "Y");
//         System.out.println("LFRFF"+r.length);
//         for(Row rr:r)
//        {
           OperationBinding op1=ADFUtils.findOperation("ErrorMsg");
           op1.execute();
           OperationBinding op=ADFUtils.findOperation("SetEntryNo");
           op.execute();
           
          System.out.println("op.getReFlag"+op.getResult());
          if(op.getResult().toString().equalsIgnoreCase("V"))
          {
       ADFUtils.findOperation("Commit").execute();
       ADFUtils.showMessage("Record Saved Successfully.", 2);
        voh.setNamedWhereClauseParam("BindModeType", "C");
        voh.executeQuery();
    }
          else
          {
              ADFUtils.showMessage("TransCheck must be check to save the particular row", 2);
              }
//     }
  
    }

    public void EditAL(ActionEvent actionEvent) {
      AdfFacesContext.getCurrentInstance().addPartialTarget(tableBinding);
    }

    public void setTableBinding(RichTable tableBinding) {
        this.tableBinding = tableBinding;
    }

    public RichTable getTableBinding() {
        return tableBinding;
    }

    public void ErrorMsgValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
//      
//        DCIteratorBinding dciter =
//                    (DCIteratorBinding) ADFUtils.findIterator("FgStockReceiptVO1Iterator");
//        //
//        //         FgStockReceiptVORowImpl vo=(FgStockReceiptVORowImpl )dciter.getCurrentRow();
//        //        if(vo!=null && vo.getTransCheck() !=null && vo.getTransCheck().equalsIgnoreCase("Y") )
//         ViewObject vo = dciter.getViewObject();
//         Row[] r=vo.getFilteredRows("TransCheck", "Y");
//         System.out.println("LFRFF"+r.length);
//         for(Row rr:r)
//        { 
//             OperationBinding op1=ADFUtils.findOperation("ErrorMsg");
//           op1.execute();
//
//    }
         }

    public void setRcvdQtyBinding(RichInputText rcvdQtyBinding) {
        this.rcvdQtyBinding = rcvdQtyBinding;
    }

    public RichInputText getRcvdQtyBinding() {
        return rcvdQtyBinding;
    }

    public void setPrdctQtyBinding(RichInputText prdctQtyBinding) {
        this.prdctQtyBinding = prdctQtyBinding;
    }

    public RichInputText getPrdctQtyBinding() {
        return prdctQtyBinding;
    }

    public void rcvdcompprodn(FacesContext facesContext, UIComponent uIComponent, Object object) {
      BigDecimal RcvdQty = (BigDecimal) rcvdQtyBinding.getValue();
      System.out.println( rcvdQtyBinding.getValue()+"fghfghhhhhhhh");
      BigDecimal ProdQty = (BigDecimal) prdctQtyBinding.getValue();
          System.out.println( prdctQtyBinding.getValue()+"kkkkkkkkkkkkkkkkkkkkkkkkkkkkk");
      if(RcvdQty.compareTo(ProdQty)==1)
      {
             System.out.println("fhgggggggggggggggggggggggggggggggggggggggg");
              throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Received Quantity must be less than or equal to Production Qantity.", null));
              }
      }

    public void downloadJrxml(FacesContext facesContext, OutputStream outputStream) {
        Connection conn=null;
        try {
            String file_name ="";
            System.out.println("godownMode===================>"+goDownMode);
           
            if(goDownMode.equals("GD"))
            {
                file_name="r_dpr.jasper";
            }
            System.out.println("After Set Value File Name is===>"+file_name);
           
           
            OperationBinding binding = ADFUtils.findOperation("fileNameForPrint");
            binding.getParamsMap().put("fileId","ERP0000000147");
            binding.execute();
            //*************End Find File name***********//
            System.out.println("Binding Result :"+binding.getResult());
            if(binding.getResult() != null){

                InputStream input = new FileInputStream(binding.getResult().toString());
                DCIteratorBinding gdIter = (DCIteratorBinding) getBindings().get("FgStockReceiptVO1Iterator");
                System.out.println(ADFUtils.resolveExpression("#{pageFlowScope.unitCode}") == null ? "10001" : ADFUtils.resolveExpression("#{pageFlowScope.unitCode}"));
                System.out.println("ADFUtils.resolveExpression(\"#{pageFlowScope.unitCode}\") == null ? \"10001\" : ADFUtils.resolveExpression(\"#{pageFlowScope.unitCode}\")");
                String unitCode = (String)(ADFUtils.resolveExpression("#{pageFlowScope.unitCode}") == null ? "10001" : ADFUtils.resolveExpression("#{pageFlowScope.unitCode}"));
//                String unitCode = "10001";
//                String unitCode = gdIter.getCurrentRow().getAttribute("UnitCd").toString();
                System.out.println("Unit Code :- " + unitCode);
                Map n = new HashMap();
                
                if(goDownMode.equals("GD"))
                {
                    System.out.println("As On Date binding ==>"+asOnDateBinding.getValue());
                    n.put("p_unit", unitCode);
                    n.put("p_dt", asOnDateBinding.getValue());
                }
                else
                {
                    System.out.println("hcvfdrfvyhrdhrvgrgvrg in else");
                    n.put("p_unit", unitCode);
                }

                conn = getConnection( );
                
                JasperReport design = (JasperReport) JRLoader.loadObject(input);
                System.out.println("Path : " + input + " -------" + design+" param:"+n);
                
                @SuppressWarnings("unchecked")
                net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                byte pdf[] = JasperExportManager.exportReportToPdf(print);
                HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                response.getOutputStream().write(pdf);
                response.getOutputStream().flush();
                response.getOutputStream().close();
                facesContext.responseComplete();
                }else
                System.out.println("File Name/File Path not found.");
                } catch (FileNotFoundException fnfe) {
                // TODO: Add catch code
                fnfe.printStackTrace();
                try {
                    System.out.println("in finally connection closed");
                            conn.close( );
                            conn = null;
                    
                } catch( SQLException e ) {
                    e.printStackTrace( );
                }
                } catch (Exception e) {
                // TODO: Add catch code
                e.printStackTrace();
                try {
                    System.out.println("in finally connection closed");
                            conn.close( );
                            conn = null;
                    
                } catch( SQLException e1 ) {
                    e1.printStackTrace( );
                }
                }finally {
                        try {
                                System.out.println("in finally connection closed");
                                        conn.close( );
                                        conn = null;
                                
                        } catch( SQLException e ) {
                                e.printStackTrace( );
                        }
                }
    }
    
    
    public BindingContainer getBindings()
    {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
    }

    public void setAsOnDateBinding(RichInputDate asOnDateBinding) {
        this.asOnDateBinding = asOnDateBinding;
    }

    public RichInputDate getAsOnDateBinding() {
        return asOnDateBinding;
    }

    public void setGoDownMode(String goDownMode) {
        this.goDownMode = goDownMode;
    }

    public String getGoDownMode() {
        return goDownMode;
    }

    public void entryDateValidator(FacesContext facesContext, UIComponent uIComponent, Object ob) {
        System.out.println("INSIDE entryDateValidator #### ");
        if(ob != null) {
            
            
            System.out.println("INSIDE ob ##### ");
            java.sql.Timestamp docDate = (java.sql.Timestamp)docDateBinding.getValue();
            java.sql.Timestamp entryDate = (java.sql.Timestamp)ob;
            System.out.println("Entry Date #### " + entryDate + "Doc Date ##### " + docDate);
            
            if(entryDate.compareTo(docDate) == -1) {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Entry Date should be less than Doc Date.", null));
            }
        }
    }

    public void setEntryDateBinding(RichInputDate entryDateBinding) {
        this.entryDateBinding = entryDateBinding;
    }

    public RichInputDate getEntryDateBinding() {
        return entryDateBinding;
    }

    public void setDocDateBinding(RichInputDate docDateBinding) {
        this.docDateBinding = docDateBinding;
    }

    public RichInputDate getDocDateBinding() {
        return docDateBinding;
    }
    
    public Connection getConnection() throws Exception{
            Context ctx = null;
            Connection conn = null;
            ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/ApplicationDBDS");
            conn = ds.getConnection();
            return conn;
        }
}


