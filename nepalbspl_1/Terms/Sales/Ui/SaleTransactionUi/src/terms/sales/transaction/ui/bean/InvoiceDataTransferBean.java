package terms.sales.transaction.ui.bean;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.input.RichInputText;

import oracle.binding.OperationBinding;

public class InvoiceDataTransferBean {
    private RichInputText urlBind;
    private RichInputText usernameBind;
    private RichInputText passwordBind;

    public InvoiceDataTransferBean() {
    }

    public void setUrlBind(RichInputText urlBind) {
        this.urlBind = urlBind;
    }

    public RichInputText getUrlBind() {
        return urlBind;
    }

    public void setUsernameBind(RichInputText usernameBind) {
        this.usernameBind = usernameBind;
    }

    public RichInputText getUsernameBind() {
        return usernameBind;
    }

    public void submitUrlAL(ActionEvent actionEvent) {
//        OperationBinding op = (OperationBinding) ADFUtils.findOperation("insertUrl");
//                op.getParamsMap().put("url", getUrlBind().getValue().toString());
//                op.getParamsMap().put("username", getUsernameBind().getValue().toString());
//                op.getParamsMap().put("password", getPasswordBind().getValue().toString());
//                op.execute();
//                if(op.getResult().equals("Done")){
//                    FacesMessage Message = new FacesMessage("Record Saved Successfully.");
//                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
//                    FacesContext fc = FacesContext.getCurrentInstance();
//                    fc.addMessage(null, Message);
        //            RichInputText input = (RichInputText)JsfUtils.findComponentInRoot("it1"); 
        //            input.setSubmittedValue(null); 
        //            input.resetValue(); 
        //            AdfFacesContext.getCurrentInstance().addPartialTarget(input); 
//                }else{
//                    FacesMessage Message = new FacesMessage("Cannot input again.");
//                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
//                    FacesContext fc = FacesContext.getCurrentInstance();
//                    fc.addMessage(null, Message);
//                }
        ADFUtils.findOperation("Commit").execute(); 
        FacesMessage Message = new FacesMessage("Record Saved Successfully.");
        Message.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, Message);
    }

    public void setPasswordBind(RichInputText passwordBind) {
        this.passwordBind = passwordBind;
    }

    public RichInputText getPasswordBind() {
        return passwordBind;
    }
}
