package terms.sales.transaction.ui.bean;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichSelectBooleanCheckbox;

import oracle.adf.view.rich.component.rich.nav.RichButton;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;

import terms.fin.transaction.model.view.BankRecoDetailVORowImpl;

import terms.sales.transaction.model.view.InvoiceVoucherGenerationVORowImpl;

public class InvoiceVoucherGenerationBean {
    private RichSelectBooleanCheckbox bindCheckAll;
    private RichSelectBooleanCheckbox vouGenBinding;
    private RichButton selectButtonBinding;
    private String Type="Y";
    private RichTable invVouGenTableBinding;

    public InvoiceVoucherGenerationBean() {
    }

    public void populateInvoiceAL(ActionEvent actionEvent) {
       OperationBinding ob=ADFUtils.findOperation("PopulateInvoiceVouGen");
       ob.execute();
        AdfFacesContext.getCurrentInstance().addPartialTarget(invVouGenTableBinding);
       
    }

  

    public void setBindCheckAll(RichSelectBooleanCheckbox bindCheckAll) {
        this.bindCheckAll = bindCheckAll;
    }

    public RichSelectBooleanCheckbox getBindCheckAll() {
        return bindCheckAll;
    }

    public void setVouGenBinding(RichSelectBooleanCheckbox vouGenBinding) {
        this.vouGenBinding = vouGenBinding;
    }

    public RichSelectBooleanCheckbox getVouGenBinding() {
        return vouGenBinding;
    }

    public void checkAllvce(ValueChangeEvent vce) {
       vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
       if(vce!=null)
       {
               OperationBinding Opr = ADFUtils.findOperation("checkEditSelect");
                Object Obj=Opr.execute();
//            if(bindCheckAll.getValue()!=null && bindCheckAll.getValue().equals("Y")){
//            vouGenBinding.setValue("Y");
           }
          
    }

   

    public void setSelectButtonBinding(RichButton selectButtonBinding) {
        this.selectButtonBinding = selectButtonBinding;
    }

    public RichButton getSelectButtonBinding() {
        return selectButtonBinding;
    }

    public void setInvVouGenTableBinding(RichTable invVouGenTableBinding) {
        this.invVouGenTableBinding = invVouGenTableBinding;
    }

    public RichTable getInvVouGenTableBinding() {
        return invVouGenTableBinding;
    }

    public void saveButtonAL(ActionEvent actionEvent) {
        DCIteratorBinding Dcite=ADFUtils.findIterator("InvoiceVoucherGenerationVO1Iterator");
        InvoiceVoucherGenerationVORowImpl row=(InvoiceVoucherGenerationVORowImpl) Dcite.getCurrentRow();
        if((Long)ADFUtils.evaluateEL("#{bindings.InvoiceVoucherGenerationVO1Iterator.estimatedRowCount}")>=1 )
        {
            System.out.println("Vou Gen Flag: "+row.getVouGen());
            if(row.getVouGen() !=null)
              {
            OperationBinding op=ADFUtils.findOperation("invoiceVouGenSaleVou");
            op.execute();
            System.out.println("After Function Call From Amimpl: "+op.getResult());
                if (op.getResult() != null && op.getResult().equals("Y"))
                {
                    if (op.getErrors().isEmpty()) 
                    {
                      ADFUtils.findOperation("Commit").execute();
                      ADFUtils.showMessage("Record Saved Successfully.", 2);
                    }
                }
            if (op.getResult() != null && op.getResult().equals("ERROR"))
            {
                    ADFUtils.showMessage("Error occurs while function call.", 0);
                }

//            RowSetIterator rsi = Dcite.getRowSetIterator();
//            if(rsi!=null)
//            {
//                System.out.println("============yessssss==========");
//                    Row[] allRowsInRange = rsi.getAllRowsInRange();
//                    for (Row rw : allRowsInRange) 
//                    {
//                        System.out.println("allRowsInRange====="+allRowsInRange.length);
//                        if (rw != null && (rw.getAttribute("VouGen") == null|| rw.getAttribute("VouGen").equals("N")))
//                        {
//                            System.out.println("------- Row remove method");
//                            rw.remove();
//                            ADFUtils.findOperation("Commit").execute();
//                        }
//                    }
//             }
        }
        else{
              ADFUtils.showMessage("No record is selected. Please Check!", 0);    
            }
        }
    }
}
