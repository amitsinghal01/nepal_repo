package terms.sales.transaction.ui.bean;


import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import javax.faces.validator.ValidatorException;

import javax.persistence.criteria.Order;

import oracle.adf.model.OperationBinding;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.jbo.Row;

import terms.fin.transaction.model.view.DrCrDetailItemVORowImpl;

import terms.sales.transaction.model.view.PerformaInvoiceDetailVORowImpl;


public class PerformaInvoiceBean {
    private RichTable performaInvoiceDetailTableBinding;
   
    private RichButton editButtonBinding;
    private RichOutputText outputtextBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichButton headerEditBinding;
    private RichButton detailcreateBinding;
    private RichButton detaildeleteBinding;
    private RichInputComboboxListOfValues approvedBy;
    private RichInputText orderQtyBinding;
    private RichInputText grossWtBinding;
    private RichInputText stockQty;
    private RichInputText rejQtyBinding;
    private RichColumn gstDescBinding;
    private RichColumn gstRateBinding;
    private RichInputText gstDescBind;
    private RichInputText gstRateBind;
    private RichInputText grossAmtBindng;
    private RichInputComboboxListOfValues unitCdBinding;
    private RichInputComboboxListOfValues custCdBinding;
    private RichInputText typeBinding;
    private RichInputText qtyBinding;
    private String pageMode="C";
    private RichShowDetailItem portDetailTabBinding;
    private RichShowDetailItem itemDetailTabBinding;
    private RichShowDetailItem perfrmaInvTabBinding;
    private RichInputComboboxListOfValues prodCodeBinding;
    private RichInputText hsnNoBinding;
    private RichInputText cgstAmtBinding;
    private RichInputText sgstAmtBinding;
    private RichInputText igstAmtBinding;
    private RichInputText netAmountBinding;
    private RichInputText sgstRateBinding;
    private RichInputText cgstRateBinding;
    private RichInputText amtBinding;
    private RichInputText igstAmtBind;
    private RichInputText sgstAmtBind;
    private RichInputText cgstAmtBind;
    private RichInputText adviceNoBinding;
    private RichInputDate entryDateBind;
    private RichInputText stockTypeBinding;
    private RichInputText amdNoBinding;
    private RichInputText cityBinding;
    private RichInputText plantBinding;
    private RichInputText stockTypeBin;
    private RichShowDetailItem prtDetailTab;
    private RichShowDetailItem tabPortBinding;

    public PerformaInvoiceBean() {
    }

    public void DeletePopupDialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                   {
               
                   ADFUtils.findOperation("Delete").execute();
                   FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                   Message.setSeverity(FacesMessage.SEVERITY_INFO);
                   FacesContext fc = FacesContext.getCurrentInstance();
                   fc.addMessage(null, Message);
      
     
               }
//               AdfFacesContext.getCurrentInstance().addPartialTarget(performaInvoiceDetailTableBinding);   
    }

    public void setPerformaInvoiceDetailTableBinding(RichTable performaInvoiceDetailTableBinding) {
        this.performaInvoiceDetailTableBinding = performaInvoiceDetailTableBinding;
    }

    public RichTable getPerformaInvoiceDetailTableBinding() {
        return performaInvoiceDetailTableBinding;
    }

    public void SaveAL(ActionEvent actionEvent) {
        DCIteratorBinding Dcite=ADFUtils.findIterator("PerformaInvoiceDetailVO1Iterator");
        PerformaInvoiceDetailVORowImpl row=(PerformaInvoiceDetailVORowImpl) Dcite.getCurrentRow();
        BigDecimal Qty= row.getQty()==null ? new BigDecimal(0):row.getQty();
        String Type=row.getType()==null ? "":row.getType();
        BigDecimal Order=row.getOrderQty()==null ? new BigDecimal(0) : row.getOrderQty();
        BigDecimal Rej=row.getRejQty()==null ?new BigDecimal(0) : row.getRejQty();
        if(((Qty.compareTo(Order)==-1||Qty.compareTo(Order)==0) && Type!=null && Type.equalsIgnoreCase("F")) ||Type.equalsIgnoreCase("O"))
        {
            if(Qty.compareTo(Rej)==1 || Qty.compareTo(Rej)==0 ) {
                
           
        oracle.binding.OperationBinding op = ADFUtils.findOperation("generatePerformaInvoicNo");
        Object rst = op.execute();

        System.out.println("--------Commit-------");
        System.out.println("value aftr getting result--->?" + rst);

        if (rst.toString() != null && rst.toString() != "") {
            if (op.getErrors().isEmpty()) {
                ADFUtils.findOperation("Commit").execute();
                FacesMessage Message = new FacesMessage("Record Saved Successfully.Advice No is " + rst + ".");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
                
                if(approvedBy.getValue()!=null || approvedBy.getValue() == null)
                {
                      ADFUtils.setEL("#{pageFlowScope.mode}","V");
                      cevmodecheck();
                      AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
                }
            }
        }

        if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ") {
            if (op.getErrors().isEmpty()) {
                ADFUtils.findOperation("Commit").execute();
                FacesMessage Message = new FacesMessage("Record Updated Successfully.");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
                
                if(approvedBy.getValue()!=null || approvedBy.getValue() == null)
                {
                      ADFUtils.setEL("#{pageFlowScope.mode}","V");
                      cevmodecheck();
                      AdfFacesContext.getCurrentInstance().addPartialTarget(getMyPageRootComponent);
                }
            }
           }
        }
        else
            {
                ADFUtils.showMessage("Check Qty value , As it should be greater the Reject Qty", 0);
            }
        }
        else
        {
            ADFUtils.showMessage("Check Qty value , As it should be less than Order Qty.", 0);
        }
    }


    public void EditButtonAL(ActionEvent actionEvent) {
        if(approvedBy.getValue()!=null){
            ADFUtils.showMessage("This Is Approved.You can not update this record. ", 2);
            ADFUtils.setEL("#{pageFlowScope.mode}", "V");
        }
        else{
            cevmodecheck();
        }
    }

    public void setEditButtonBinding(RichButton editButtonBinding) {
        this.editButtonBinding = editButtonBinding;
    }

    public RichButton getEditButtonBinding() {
        return editButtonBinding;
    }

    public void setOutputtextBinding(RichOutputText outputtextBinding) {
        this.outputtextBinding = outputtextBinding;
    }

    public RichOutputText getOutputtextBinding() {
        cevmodecheck();
        return outputtextBinding;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setDetailcreateBinding(RichButton detailcreateBinding) {
        this.detailcreateBinding = detailcreateBinding;
    }

    public RichButton getDetailcreateBinding() {
        return detailcreateBinding;
    }

    public void setDetaildeleteBinding(RichButton detaildeleteBinding) {
        this.detaildeleteBinding = detaildeleteBinding;
    }

    public RichButton getDetaildeleteBinding() {
        return detaildeleteBinding;
    }
    private void cevmodecheck(){
         if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
             makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
             cevModeDisableComponent("V");
         }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
             makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
             cevModeDisableComponent("C");
         }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
             makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
             cevModeDisableComponent("E");
         }
     }
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
            try {
                Method method1 =
                    component.getClass().getMethod("setDisabled", boolean.class);
                if (method1 != null) {
                    method1.invoke(component, valueComponent);
                }
            } catch (NoSuchMethodException e) {
                try {
                    Method method =
                        component.getClass().getMethod("setReadOnly", boolean.class);
                    if (method != null) {
                        method.invoke(component, valueComponent);
                    }
                } catch (Exception e1) {
                    // e.printStackTrace();//silently eat this exception.
                }


            } catch (Exception e) {
                // e.printStackTrace();//silently eat this exception.
            }
            List<UIComponent> childComponents = component.getChildren();
            for (UIComponent comp : childComponents) {
                makeComponentHierarchyReadOnly(comp, valueComponent);
            }
        }
   
        public void cevModeDisableComponent(String mode) {
      
            if (mode.equals("E")) {
                pageMode="E";
                getCgstAmtBind().setDisabled(true);
                getSgstAmtBind().setDisabled(true);
                getIgstAmtBind().setDisabled(true);
                getAmtBinding().setDisabled(true);
                getAdviceNoBinding().setDisabled(true);
                getEntryDateBind().setDisabled(true);
                getAmdNoBinding().setDisabled(true);
                getCityBinding().setDisabled(true);
                getPlantBinding().setDisabled(true);
                 getStockTypeBin().setDisabled(true);
                getHsnNoBinding().setDisabled(true);
                getCgstRateBinding().setDisabled(true);
                getSgstRateBinding().setDisabled(true);
                getCgstAmtBinding().setDisabled(true);
                getSgstAmtBinding().setDisabled(true);
                getIgstAmtBinding().setDisabled(true);
                getNetAmountBinding().setDisabled(true);
                getTypeBinding().setDisabled(true);
                getUnitCdBinding().setDisabled(true);
                  getOrderQtyBinding().setDisabled(true);
                  getGrossWtBinding().setDisabled(true);
                  getStockQty().setDisabled(true);
                 getGstDescBind().setDisabled(true);
                getGstRateBind().setDisabled(true);
                getGrossAmtBindng().setDisabled(true);
                    getHeaderEditBinding().setDisabled(true);
                    getDetailcreateBinding().setDisabled(false);
                    getDetaildeleteBinding().setDisabled(false);
            } else if (mode.equals("C")) {
                getUnitCdBinding().setDisabled(true);
                getCgstAmtBind().setDisabled(true);
                getSgstAmtBind().setDisabled(true);
                getIgstAmtBind().setDisabled(true);
                getAmtBinding().setDisabled(true);
                getAdviceNoBinding().setDisabled(true);
                getEntryDateBind().setDisabled(true);
                getAmdNoBinding().setDisabled(true);
                getCityBinding().setDisabled(true);
                getPlantBinding().setDisabled(true);
                getStockTypeBin().setDisabled(true);
                getHsnNoBinding().setDisabled(true);
                getCgstRateBinding().setDisabled(true);
                getSgstRateBinding().setDisabled(true);
                getCgstAmtBinding().setDisabled(true);
                getSgstAmtBinding().setDisabled(true);
                getIgstAmtBinding().setDisabled(true);
                getNetAmountBinding().setDisabled(true);
                getTypeBinding().setDisabled(true);
                getOrderQtyBinding().setDisabled(true);
                getGrossWtBinding().setDisabled(true);
                getStockQty().setDisabled(true);
                getRejQtyBinding().setDisabled(true);
                getGstDescBind().setDisabled(true);
                getGstRateBind().setDisabled(true);
                getGrossAmtBindng().setDisabled(true);
                getDetailcreateBinding().setDisabled(false);
                getDetaildeleteBinding().setDisabled(false);
            } else if (mode.equals("V")) {
                getDetailcreateBinding().setDisabled(true);
                getDetaildeleteBinding().setDisabled(true);
                
                getItemDetailTabBinding().setDisabled(false);
                getPerfrmaInvTabBinding().setDisabled(false);
                getTabPortBinding().setDisabled(false);
             
            }
            
        }

    public void setApprovedBy(RichInputComboboxListOfValues approvedBy) {
        this.approvedBy = approvedBy;
    }

    public RichInputComboboxListOfValues getApprovedBy() {
        return approvedBy;
    }

    public void setOrderQtyBinding(RichInputText orderQtyBinding) {
        this.orderQtyBinding = orderQtyBinding;
    }

    public RichInputText getOrderQtyBinding() {
        return orderQtyBinding;
    }

    public void setGrossWtBinding(RichInputText grossWtBinding) {
        this.grossWtBinding = grossWtBinding;
    }

    public RichInputText getGrossWtBinding() {
        return grossWtBinding;
    }

    public void setStockQty(RichInputText stockQty) {
        this.stockQty = stockQty;
    }

    public RichInputText getStockQty() {
        return stockQty;
    }

    public void setRejQtyBinding(RichInputText rejQtyBinding) {
        this.rejQtyBinding = rejQtyBinding;
    }

    public RichInputText getRejQtyBinding() {
        return rejQtyBinding;
    }

    public void setGstDescBinding(RichColumn gstDescBinding) {
        this.gstDescBinding = gstDescBinding;
    }

    public RichColumn getGstDescBinding() {
        return gstDescBinding;
    }

    public void setGstRateBinding(RichColumn gstRateBinding) {
        this.gstRateBinding = gstRateBinding;
    }

    public RichColumn getGstRateBinding() {
        return gstRateBinding;
    }

    public void setGstDescBind(RichInputText gstDescBind) {
        this.gstDescBind = gstDescBind;
    }

    public RichInputText getGstDescBind() {
        return gstDescBind;
    }

    public void setGstRateBind(RichInputText gstRateBind) {
        this.gstRateBind = gstRateBind;
    }

    public RichInputText getGstRateBind() {
        return gstRateBind;
    }

    public void setGrossAmtBindng(RichInputText grossAmtBindng) {
        this.grossAmtBindng = grossAmtBindng;
    }

    public RichInputText getGrossAmtBindng() {
        return grossAmtBindng;
    }

    public void GstCalculation(ValueChangeEvent vce) {
        if(vce!=null)
        {
            vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
            OperationBinding op = (OperationBinding) ADFUtils.findOperation("RefreshAmt");
            op.getParamsMap().put("Unit", unitCdBinding.getValue().toString());
            op.getParamsMap().put("vencd", custCdBinding.getValue().toString());
            op.execute();
//            AdfFacesContext.getCurrentInstance().addPartialTarget(performaInvoiceDetailTableBinding);
        }
    }

    public void setUnitCdBinding(RichInputComboboxListOfValues unitCdBinding) {
        this.unitCdBinding = unitCdBinding;
    }

    public RichInputComboboxListOfValues getUnitCdBinding() {
        return unitCdBinding;
    }

    public void setCustCdBinding(RichInputComboboxListOfValues custCdBinding) {
        this.custCdBinding = custCdBinding;
    }

    public RichInputComboboxListOfValues getCustCdBinding() {
        return custCdBinding;
    }

    public void setTypeBinding(RichInputText typeBinding) {
        this.typeBinding = typeBinding;
    }

    public RichInputText getTypeBinding() {
        return typeBinding;
    }

//    public void OrderQtyValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
//     
//       if (object != null )
//       {
//           BigDecimal Qty = (BigDecimal) qtyBinding.getValue();
//           System.out.println("Qtyyyy"+Qty);
//     BigDecimal OrderQty = (BigDecimal) orderQtyBinding.getValue();
//     System.out.println("Order Qtyyy"+OrderQty);
//     String Type = (String) typeBinding.getValue();
//     
//     System.out.println("Type"+Type);
//     if(Qty.compareTo(OrderQty)==-1 && Type.equalsIgnoreCase("F"))
//     {
//             throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,"Quanity can not be more than Order Qty.", "")); 
//         }
//       }
//    }

    public void setQtyBinding(RichInputText qtyBinding) {
        this.qtyBinding = qtyBinding;
    }

    public RichInputText getQtyBinding() {
        return qtyBinding;
    }

    public void validateQty(ValueChangeEvent vce) {
      if(vce.getNewValue()!=null)
      {
         vce.getComponent().processUpdates(FacesContext.getCurrentInstance()); 
              BigDecimal Qty = (BigDecimal) vce.getNewValue();
              System.out.println("Qtyyyy"+Qty);
              BigDecimal OrderQty =
                (BigDecimal) (orderQtyBinding.getValue() == null ? new BigDecimal(0) : orderQtyBinding.getValue());
              System.out.println("Order Qtyyy"+OrderQty);
              String Type = (String) typeBinding.getValue();
              BigDecimal Rej =
                (BigDecimal) (rejQtyBinding.getValue() == null ? new BigDecimal(0) : rejQtyBinding.getValue());
            ;
            System.out.println("Type"+Type);
              if(Qty.compareTo(OrderQty)==1 && Type!=null && Type.equalsIgnoreCase("F"))
              {
                  FacesMessage message = new FacesMessage("Quanity can not be more than Order Qty.");
                  message.setSeverity(FacesMessage.SEVERITY_ERROR);
                  FacesContext context = FacesContext.getCurrentInstance();
                  context.addMessage(qtyBinding.getClientId(), message);
              }
              if(Qty.compareTo(Rej)==-1 && pageMode.equalsIgnoreCase("E"))
              {
                      FacesMessage message = new FacesMessage("Reject Qty can not be more than Qty.");
                      message.setSeverity(FacesMessage.SEVERITY_ERROR);
                      FacesContext context = FacesContext.getCurrentInstance();
                      context.addMessage(rejQtyBinding.getClientId(), message);
                  }
                  vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
                  OperationBinding op = (OperationBinding) ADFUtils.findOperation("RefreshAmt");
                  op.getParamsMap().put("Unit", unitCdBinding.getValue().toString());
                  op.getParamsMap().put("vencd", custCdBinding.getValue().toString());
                  op.execute();
//                  AdfFacesContext.getCurrentInstance().addPartialTarget(performaInvoiceDetailTableBinding);
              }
    }

    public void RejectQtyVCE(ValueChangeEvent vce) {
        if(vce.getNewValue()!=null)
        {
           vce.getComponent().processUpdates(FacesContext.getCurrentInstance()); 
                BigDecimal Rej = (BigDecimal) vce.getNewValue();
                
                BigDecimal Qty = (BigDecimal) qtyBinding.getValue();
                    System.out.println("Qtyyyy"+Qty);
                    if(Qty.compareTo(Rej)==-1)
                    {
                            FacesMessage message = new FacesMessage("Reject Qty can not be more than Qty.");
                            message.setSeverity(FacesMessage.SEVERITY_ERROR);
                            FacesContext context = FacesContext.getCurrentInstance();
                            context.addMessage(rejQtyBinding.getClientId(), message);
                        }
//                    AdfFacesContext.getCurrentInstance().addPartialTarget(performaInvoiceDetailTableBinding);
                }
    }

    public String SaveAndCloseAC() 
    {
        DCIteratorBinding Dcite=ADFUtils.findIterator("PerformaInvoiceDetailVO1Iterator");
        PerformaInvoiceDetailVORowImpl row=(PerformaInvoiceDetailVORowImpl) Dcite.getCurrentRow();
        BigDecimal Qty= row.getQty()==null ? new BigDecimal(0):row.getQty();
        String Type=row.getType()==null ? "":row.getType();
        BigDecimal Order=row.getOrderQty()==null ? new BigDecimal(0) : row.getOrderQty();
        BigDecimal Rej=row.getRejQty()==null ?new BigDecimal(0) : row.getRejQty();
        if(((Qty.compareTo(Order)==-1||Qty.compareTo(Order)==0) && Type!=null && Type.equalsIgnoreCase("F")) ||Type.equalsIgnoreCase("O"))
        {
            if(Qty.compareTo(Rej)==1 || Qty.compareTo(Rej)==0 )
            {
            oracle.binding.OperationBinding op = ADFUtils.findOperation("generatePerformaInvoicNo");
            Object rst = op.execute();
            
            System.out.println("--------Commit-------");
            System.out.println("value aftr getting result--->?" + rst);
            
            
            if (rst.toString() != null && rst.toString() != "")
            {
                if (op.getErrors().isEmpty()) {
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message = new FacesMessage("Record Saved Successfully.Advice No is " + rst + ".");
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);
                    return "saveAndClose";
                }
            }
            if (rst.toString() == null || rst.toString() == "" || rst.toString() == " ")
            {
                if (op.getErrors().isEmpty()) 
                {
                    ADFUtils.findOperation("Commit").execute();
                    FacesMessage Message = new FacesMessage("Record Updated Successfully.");
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);
                    return "saveAndClose";
                }
            }
         }
         else{
                ADFUtils.showMessage("Check Qty value , As it should be greater the Reject Qty", 0);
                return null;
             }
        }
        else
        {
            ADFUtils.showMessage("Check Qty value , As it should be less than Order Qty.", 0);
            return null;
        }
        return null;
    }

    
    public void setItemDetailTabBinding(RichShowDetailItem itemDetailTabBinding) {
        this.itemDetailTabBinding = itemDetailTabBinding;
    }

    public RichShowDetailItem getItemDetailTabBinding() {
        return itemDetailTabBinding;
    }

    public void setPerfrmaInvTabBinding(RichShowDetailItem perfrmaInvTabBinding) {
        this.perfrmaInvTabBinding = perfrmaInvTabBinding;
    }

    public RichShowDetailItem getPerfrmaInvTabBinding() {
        return perfrmaInvTabBinding;
    }

    public void ProductCodeVCE(ValueChangeEvent vce) {
       if(vce!=null)
       {       
           vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
   
        if(prodCodeBinding.getValue()!=null)
        {
            String ProdCode=prodCodeBinding.getValue().toString();
            oracle.binding.OperationBinding opp=ADFUtils.findOperation("gethsnnoPerforma");
            opp.getParamsMap().put("ProdCode", ProdCode);
            opp.execute();
            
            }
            if(hsnNoBinding.getValue()!=null)
            {
                String HsnNo =(String) hsnNoBinding.getValue();
              oracle.binding.OperationBinding op = ADFUtils.findOperation("GstCodefromProdcodePerforma");
                
                System.out.println("--------Hsnno------"+HsnNo);
                op.getParamsMap().put("HsnNo", HsnNo);
                op.execute();
        }
        String Unit = "";
        String vencd = "";
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        OperationBinding ob = (OperationBinding) ADFUtils.findOperation("RefreshAmt");
        ob.getParamsMap().put("Unit", unitCdBinding.getValue().toString());
        ob.getParamsMap().put("vencd ", custCdBinding.getValue().toString());
        ob.execute();
//               AdfFacesContext.getCurrentInstance().addPartialTarget(performaInvoiceDetailTableBinding);
           }
    }

    public void setProdCodeBinding(RichInputComboboxListOfValues prodCodeBinding) {
        this.prodCodeBinding = prodCodeBinding;
    }

    public RichInputComboboxListOfValues getProdCodeBinding() {
        return prodCodeBinding;
    }

    public void setHsnNoBinding(RichInputText hsnNoBinding) {
        this.hsnNoBinding = hsnNoBinding;
    }

    public RichInputText getHsnNoBinding() {
        return hsnNoBinding;
    }

    public void setCgstAmtBinding(RichInputText cgstAmtBinding) {
        this.cgstAmtBinding = cgstAmtBinding;
    }

    public RichInputText getCgstAmtBinding() {
        return cgstAmtBinding;
    }

    public void setSgstAmtBinding(RichInputText sgstAmtBinding) {
        this.sgstAmtBinding = sgstAmtBinding;
    }

    public RichInputText getSgstAmtBinding() {
        return sgstAmtBinding;
    }

    public void setIgstAmtBinding(RichInputText igstAmtBinding) {
        this.igstAmtBinding = igstAmtBinding;
    }

    public RichInputText getIgstAmtBinding() {
        return igstAmtBinding;
    }

    public void setNetAmountBinding(RichInputText netAmountBinding) {
        this.netAmountBinding = netAmountBinding;
    }

    public RichInputText getNetAmountBinding() {
        return netAmountBinding;
    }

    public void setSgstRateBinding(RichInputText sgstRateBinding) {
        this.sgstRateBinding = sgstRateBinding;
    }

    public RichInputText getSgstRateBinding() {
        return sgstRateBinding;
    }

    public void setCgstRateBinding(RichInputText cgstRateBinding) {
        this.cgstRateBinding = cgstRateBinding;
    }

    public RichInputText getCgstRateBinding() {
        return cgstRateBinding;
    }

    public void setAmtBinding(RichInputText amtBinding) {
        this.amtBinding = amtBinding;
    }

    public RichInputText getAmtBinding() {
        return amtBinding;
    }

    public void setIgstAmtBind(RichInputText igstAmtBind) {
        this.igstAmtBind = igstAmtBind;
    }

    public RichInputText getIgstAmtBind() {
        return igstAmtBind;
    }

    public void setSgstAmtBind(RichInputText sgstAmtBind) {
        this.sgstAmtBind = sgstAmtBind;
    }

    public RichInputText getSgstAmtBind() {
        return sgstAmtBind;
    }

    public void setCgstAmtBind(RichInputText cgstAmtBind) {
        this.cgstAmtBind = cgstAmtBind;
    }

    public RichInputText getCgstAmtBind() {
        return cgstAmtBind;
    }

    public void setAdviceNoBinding(RichInputText adviceNoBinding) {
        this.adviceNoBinding = adviceNoBinding;
    }

    public RichInputText getAdviceNoBinding() {
        return adviceNoBinding;
    }

    public void setEntryDateBind(RichInputDate entryDateBind) {
        this.entryDateBind = entryDateBind;
    }

    public RichInputDate getEntryDateBind() {
        return entryDateBind;
    }

    public void setStockTypeBinding(RichInputText stockTypeBinding) {
        this.stockTypeBinding = stockTypeBinding;
    }

    public RichInputText getStockTypeBinding() {
        return stockTypeBinding;
    }

    public void setAmdNoBinding(RichInputText amdNoBinding) {
        this.amdNoBinding = amdNoBinding;
    }

    public RichInputText getAmdNoBinding() {
        return amdNoBinding;
    }

    public void setCityBinding(RichInputText cityBinding) {
        this.cityBinding = cityBinding;
    }

    public RichInputText getCityBinding() {
        return cityBinding;
    }

    public void setPlantBinding(RichInputText plantBinding) {
        this.plantBinding = plantBinding;
    }

    public RichInputText getPlantBinding() {
        return plantBinding;
    }

    public void setStockTypeBin(RichInputText stockTypeBin) {
        this.stockTypeBin = stockTypeBin;
    }

    public RichInputText getStockTypeBin() {
        return stockTypeBin;
    }

    public void chckAuthoVCE(ValueChangeEvent vce) 
    {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        
        try
        {
        
            OperationBinding OprAutho= (OperationBinding)ADFUtils.findOperation("checkApprovalStatus");
            OprAutho.getParamsMap().put("emp_code",vce.getNewValue());
            OprAutho.getParamsMap().put("form_name","PERINV");
            OprAutho.getParamsMap().put("unit_code",unitCdBinding.getValue());
            OprAutho.getParamsMap().put("amount",null);
            OprAutho.getParamsMap().put("autho_level","AP");
            OprAutho.execute();
            Row row =(Row)ADFUtils.evaluateEL("#{bindings.PerformaInvoiceHeaderVO1Iterator.currentRow}"); 
            if(OprAutho.getResult().equals("Y"))
            {
                //row.setAttribute("ApprovalDate",billEntryDateBinding.getValue());
            }
            else
            {
                row.setAttribute("AuthorisedBy",null);   
                //row.setAttribute("ApprovalDate", null);
                ADFUtils.showMessage("You Don't Have Permission To Approved This Record.",0);        
            }
            
        }
        catch(Exception ee)
        {
        ee.printStackTrace();
        }   
            
            
        //       if(valueChangeEvent!=null)
        //       {
        //           valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        //           OperationBinding op = (OperationBinding) ADFUtils.findOperation("CheckAuthorityForPerforma");
        //           op.getParamsMap().put("unitCd", unitCdBinding.getValue().toString());
        //           op.getParamsMap().put("EmpCd", approvedBy.getValue());
        //            op.execute();
        //               
        //               }
    }


    public void setTabPortBinding(RichShowDetailItem tabPortBinding) {
        this.tabPortBinding = tabPortBinding;
    }

    public RichShowDetailItem getTabPortBinding() {
        return tabPortBinding;
    }
}
