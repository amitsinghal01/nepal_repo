package terms.sales.transaction.ui.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.component.rich.output.RichPanelCollection;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.adf.view.rich.event.ReturnPopupEvent;

import oracle.binding.OperationBinding;

import org.apache.myfaces.trinidad.event.DisclosureEvent;

public class PurchaseReturnInvoiceBean {
    private RichPanelHeader getMyPageRootComponent;
    private RichTable dtlTableBinding;
    private RichOutputText bindingOutputText;
    private RichOutputText bindingTotQty;
    private RichShowDetailItem invDetailsTabBinding;
    private RichShowDetailItem itemDetailsTabBinding;
    private RichShowDetailItem othDetailsTabBinding;
    private RichPanelCollection pcBinding;
    private RichInputText igstAmtBinding;
    private RichInputText igstRateBinding;
    private RichInputText cgstAmtBinding;
    private RichInputText cgstRateBinding;
    private RichInputText sgstAmtBinding;
    private RichInputText sgstRateBinding;
    private RichInputText itemDescBinding;
    private RichInputText srvNoBinding;
    private RichInputDate srvDateBinding;
    private RichInputText qtyBinding;
    private RichInputText rateBinding;
    private RichInputText amtBinding;
    private RichInputText disPerBinding;
    private RichInputText disAmtBinding;
    private RichInputText grossAmtBinding;
    private RichInputText dismtHdrBinding;
    private RichInputText sgstAmtHdrBinding;
    private RichInputText cgstAmtHdrBinding;
    private RichInputText igstAmtHdrBinding;
    private RichInputText pckngAmtBinding;
    private RichInputText netAmtBinding;
    private RichInputText grNoBinding;
    private RichInputDate grdateBinding;
    private RichInputText vouNoBinding;
    private RichInputDate vouDtBinding;
    private RichInputDate remDtBinding;
    private RichInputDate issDtBinding;
    private RichInputText partyTpBinding;
    private RichInputComboboxListOfValues venCdBinding;
    private RichSelectOneChoice rejTypeBinding;
    private RichButton editButtonBinding;
    private RichInputComboboxListOfValues approveBy;
    private RichInputText invoiceNoBinding;
    private RichInputComboboxListOfValues unitBinding;

    public PurchaseReturnInvoiceBean() {
    }

    public void saveButtonAL(ActionEvent actionEvent) {
        
        
 if((Long)ADFUtils.evaluateEL("#{bindings.PurchaseReturnInvoiceDetailVO1Iterator.estimatedRowCount}")==0)
            {
     
                    ADFUtils.showMessage(" Fill Item Details", 0);
     
                }
 else{
     
     OperationBinding op = ADFUtils.findOperation("getInvoiceNoPRI");
     Object rst = op.execute();
         if (rst != null && rst != "") 
         {
             if (op.getErrors().isEmpty()) {
                 if(op.getResult()!=null && op.getResult().equals("Y"))
                 {
                 ADFUtils.findOperation("Commit").execute();
                 ADFUtils.showMessage("Record Saved Successfully" + "Invoice No is::"+ invoiceNoBinding.getValue(), 2);
             }
                 
             else{
                 
                 System.out.println("error in funvv");
                 ADFUtils.findOperation("Rollback").execute();
                 ADFUtils.findOperation("CreateInsert1").execute();
                 FacesMessage Message = new FacesMessage("Record can't saved successfully.");   
                 Message.setSeverity(FacesMessage.SEVERITY_ERROR);   
                 FacesContext fc = FacesContext.getCurrentInstance();   
                 fc.addMessage(null, Message);  
                AdfFacesContext.getCurrentInstance().addPartialTarget(dtlTableBinding);
                 
             }
         }
         }
         
         if (rst == null || rst == "" || rst == " ") 
         {
             if (op.getErrors().isEmpty()) {
                 ADFUtils.findOperation("Commit").execute();
                 ADFUtils.showMessage(" Record Updated Successfully.", 2); 
           }
     }
     }
     
//     ADFUtils.findOperation("getInvoiceNoPRI").execute();
//        ADFUtils.findOperation("Commit").execute();
       
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void deletePopupDL(DialogEvent dialogEvent) {
        
        BigDecimal a=new BigDecimal(0);
        if(dialogEvent.getOutcome().name().equals("ok"))
                    {
                    ADFUtils.findOperation("Delete").execute();
                    System.out.println("Record Delete Successfully");
                   
                        FacesMessage Message = new FacesMessage("Record Deleted Successfully");  
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);  
                        FacesContext fc = FacesContext.getCurrentInstance();  
                        fc.addMessage(null, Message);  
                        
                        if((Long)ADFUtils.evaluateEL("#{bindings.PurchaseReturnInvoiceDetailVO1Iterator.estimatedRowCount}")>0)
                        {
                        ADFUtils.findOperation("copyDatafromDtlinHdr").execute();
                        }
                        else
                        {
                            getGrossAmtBinding().setValue(a);
                            getCgstAmtHdrBinding().setValue(a);
                            getIgstAmtHdrBinding().setValue(a);
                            getSgstAmtHdrBinding().setValue(a);
                            getNetAmtBinding().setValue(a);
                            getDismtHdrBinding().setValue(a);
                            }
                    }

                AdfFacesContext.getCurrentInstance().addPartialTarget(dtlTableBinding);
           // AdfFacesContext.getCurrentInstance().addPartialTarget(pcBinding);
        }
    

    public void setDtlTableBinding(RichTable dtlTableBinding) {
        this.dtlTableBinding = dtlTableBinding;
    }

    public RichTable getDtlTableBinding() {
        return dtlTableBinding;
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }


    public void EditButtonAL(ActionEvent actionEvent) {
        
        if(approveBy.getValue()!=null){
            ADFUtils.showMessage("This Is Approved.You can not update this record. ", 2);
            ADFUtils.setEL("#{pageFlowScope.mode}", "V");
        }
        else{
            cevmodecheck();
        }
        
    }
    
    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
                try {
                    Method method1 =
                        component.getClass().getMethod("setDisabled", boolean.class);
                    if (method1 != null) {
                        method1.invoke(component, valueComponent);
                    }
                } catch (NoSuchMethodException e) {
                    try {
                        Method method =
                            component.getClass().getMethod("setReadOnly", boolean.class);
                        if (method != null) {
                            method.invoke(component, valueComponent);
                        }
                    } catch (Exception e1) {
                        // e.printStackTrace();//silently eat this exception.
                    }


                } catch (Exception e) {
                    // e.printStackTrace();//silently eat this exception.
                }
                List<UIComponent> childComponents = component.getChildren();
                for (UIComponent comp : childComponents) {
                    makeComponentHierarchyReadOnly(comp, valueComponent);
                }
            }
    
    private void cevmodecheck(){
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")){
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        }else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }
    
    public void cevModeDisableComponent(String mode) {
        
        if(mode.equals("V")){
        
        getItemDetailsTabBinding().setDisabled(false);
        getOthDetailsTabBinding().setDisabled(false);
        getInvDetailsTabBinding().setDisabled(false);

        }
        if(mode.equals("E")){
            getUnitBinding().setDisabled(true);
            getQtyBinding().setDisabled(true);
            getRateBinding().setDisabled(true);
            getItemDescBinding().setDisabled(true);
            getAmtBinding().setDisabled(true);
            getDisAmtBinding().setDisabled(true);
            getDisPerBinding().setDisabled(true);
            getSgstAmtBinding().setDisabled(true);
            getSgstRateBinding().setDisabled(true);
            getCgstAmtBinding().setDisabled(true);
            getCgstRateBinding().setDisabled(true);
            getIgstAmtBinding().setDisabled(true);
            getIgstRateBinding().setDisabled(true);
            getSrvDateBinding().setDisabled(true);
            getSrvNoBinding().setDisabled(true);
            getDismtHdrBinding().setDisabled(true);
         
            getSgstAmtHdrBinding().setDisabled(true);
            getCgstAmtHdrBinding().setDisabled(true);
            getIgstAmtHdrBinding().setDisabled(true);
            getVouDtBinding().setDisabled(true);
            getVouNoBinding().setDisabled(true);
            getGrNoBinding().setDisabled(true);
            getGrdateBinding().setDisabled(true);
            getPartyTpBinding().setDisabled(true);
            getGrossAmtBinding().setDisabled(true);
            getNetAmtBinding().setDisabled(true);
            getRejTypeBinding().setDisabled(true);
            getVenCdBinding().setDisabled(true);
            getEditButtonBinding().setDisabled(true);
           getInvoiceNoBinding().setDisabled(true);
        }
        if(mode.equals("C")){
            getEditButtonBinding().setDisabled(true);
            getGrossAmtBinding().setDisabled(true);
           getUnitBinding().setDisabled(true);
            getNetAmtBinding().setDisabled(true);
            getQtyBinding().setDisabled(true);
            getRateBinding().setDisabled(true);
            getItemDescBinding().setDisabled(true);
            getAmtBinding().setDisabled(true);
            getDisAmtBinding().setDisabled(true);
            getDisPerBinding().setDisabled(true);
            getSgstAmtBinding().setDisabled(true);
            getSgstRateBinding().setDisabled(true);
            getCgstAmtBinding().setDisabled(true);
            getCgstRateBinding().setDisabled(true);
            getIgstAmtBinding().setDisabled(true);
            getIgstRateBinding().setDisabled(true);
            getSrvDateBinding().setDisabled(true);
            getSrvNoBinding().setDisabled(true);
            getDismtHdrBinding().setDisabled(true);
            
            getSgstAmtHdrBinding().setDisabled(true);
            getCgstAmtHdrBinding().setDisabled(true);
            getIgstAmtHdrBinding().setDisabled(true);
            getVouDtBinding().setDisabled(true);
            getVouNoBinding().setDisabled(true);
            getGrNoBinding().setDisabled(true);
            getGrdateBinding().setDisabled(true);
            getPartyTpBinding().setDisabled(true);
            getInvoiceNoBinding().setDisabled(true);
                                                     
            
        }
    }
   


    public void itemVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance()); 
        System.out.println("INSIDE BEAN************");
        if(vce!=null){
          
            System.out.println("INSODE BEAN METHOD");
        ADFUtils.findOperation("getHsnNoGstCode").execute();
        ADFUtils.findOperation("calculateGSTAmount").execute();
    }
    }

    public void setBindingTotQty(RichOutputText bindingTotQty) {
        this.bindingTotQty = bindingTotQty;
    }

    public RichOutputText getBindingTotQty() {
        return bindingTotQty;
    }

    public void setInvDetailsTabBinding(RichShowDetailItem invDetailsTabBinding) {
        this.invDetailsTabBinding = invDetailsTabBinding;
    }

    public RichShowDetailItem getInvDetailsTabBinding() {
        return invDetailsTabBinding;
    }

    public void setItemDetailsTabBinding(RichShowDetailItem itemDetailsTabBinding) {
        this.itemDetailsTabBinding = itemDetailsTabBinding;
    }

    public RichShowDetailItem getItemDetailsTabBinding() {
        return itemDetailsTabBinding;
    }

    public void setOthDetailsTabBinding(RichShowDetailItem othDetailsTabBinding) {
        this.othDetailsTabBinding = othDetailsTabBinding;
    }

    public RichShowDetailItem getOthDetailsTabBinding() {
        return othDetailsTabBinding;
    }

    public void gstCodeVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance()); 
       System.out.println("INSIDE BEAN for calculation************");
        if(vce!=null){
          
          System.out.println("INSODE BEAN METHOD for calculation ");
        ADFUtils.findOperation("calculateGSTAmount").execute();
        }
    }

    public void setPcBinding(RichPanelCollection pcBinding) {
        this.pcBinding = pcBinding;
    }

    public RichPanelCollection getPcBinding() {
        return pcBinding;
    }

    public void setIgstAmtBinding(RichInputText igstAmtBinding) {
        this.igstAmtBinding = igstAmtBinding;
    }

    public RichInputText getIgstAmtBinding() {
        return igstAmtBinding;
    }

    public void setIgstRateBinding(RichInputText igstRateBinding) {
        this.igstRateBinding = igstRateBinding;
    }

    public RichInputText getIgstRateBinding() {
        return igstRateBinding;
    }

    public void setCgstAmtBinding(RichInputText cgstAmtBinding) {
        this.cgstAmtBinding = cgstAmtBinding;
    }

    public RichInputText getCgstAmtBinding() {
        return cgstAmtBinding;
    }

    public void setCgstRateBinding(RichInputText cgstRateBinding) {
        this.cgstRateBinding = cgstRateBinding;
    }

    public RichInputText getCgstRateBinding() {
        return cgstRateBinding;
    }

    public void setSgstAmtBinding(RichInputText sgstAmtBinding) {
        this.sgstAmtBinding = sgstAmtBinding;
    }

    public RichInputText getSgstAmtBinding() {
        return sgstAmtBinding;
    }

    public void setSgstRateBinding(RichInputText sgstRateBinding) {
        this.sgstRateBinding = sgstRateBinding;
    }

    public RichInputText getSgstRateBinding() {
        return sgstRateBinding;
    }

    public void setItemDescBinding(RichInputText itemDescBinding) {
        this.itemDescBinding = itemDescBinding;
    }

    public RichInputText getItemDescBinding() {
        return itemDescBinding;
    }

    public void setSrvNoBinding(RichInputText srvNoBinding) {
        this.srvNoBinding = srvNoBinding;
    }

    public RichInputText getSrvNoBinding() {
        return srvNoBinding;
    }

    public void setSrvDateBinding(RichInputDate srvDateBinding) {
        this.srvDateBinding = srvDateBinding;
    }

    public RichInputDate getSrvDateBinding() {
        return srvDateBinding;
    }

    public void setQtyBinding(RichInputText qtyBinding) {
        this.qtyBinding = qtyBinding;
    }

    public RichInputText getQtyBinding() {
        return qtyBinding;
    }

    public void setRateBinding(RichInputText rateBinding) {
        this.rateBinding = rateBinding;
    }

    public RichInputText getRateBinding() {
        return rateBinding;
    }

    public void setAmtBinding(RichInputText amtBinding) {
        this.amtBinding = amtBinding;
    }

    public RichInputText getAmtBinding() {
        return amtBinding;
    }

    public void setDisPerBinding(RichInputText disPerBinding) {
        this.disPerBinding = disPerBinding;
    }

    public RichInputText getDisPerBinding() {
        return disPerBinding;
    }

    public void setDisAmtBinding(RichInputText disAmtBinding) {
        this.disAmtBinding = disAmtBinding;
    }

    public RichInputText getDisAmtBinding() {
        return disAmtBinding;
    }

    public void setGrossAmtBinding(RichInputText grossAmtBinding) {
        this.grossAmtBinding = grossAmtBinding;
    }

    public RichInputText getGrossAmtBinding() {
        return grossAmtBinding;
    }

    public void setDismtHdrBinding(RichInputText dismtHdrBinding) {
        this.dismtHdrBinding = dismtHdrBinding;
    }

    public RichInputText getDismtHdrBinding() {
        return dismtHdrBinding;
    }

    public void setSgstAmtHdrBinding(RichInputText sgstAmtHdrBinding) {
        this.sgstAmtHdrBinding = sgstAmtHdrBinding;
    }

    public RichInputText getSgstAmtHdrBinding() {
        return sgstAmtHdrBinding;
    }

    public void setCgstAmtHdrBinding(RichInputText cgstAmtHdrBinding) {
        this.cgstAmtHdrBinding = cgstAmtHdrBinding;
    }

    public RichInputText getCgstAmtHdrBinding() {
        return cgstAmtHdrBinding;
    }

    public void setIgstAmtHdrBinding(RichInputText igstAmtHdrBinding) {
        this.igstAmtHdrBinding = igstAmtHdrBinding;
    }

    public RichInputText getIgstAmtHdrBinding() {
        return igstAmtHdrBinding;
    }

    public void setPckngAmtBinding(RichInputText pckngAmtBinding) {
        this.pckngAmtBinding = pckngAmtBinding;
    }

    public RichInputText getPckngAmtBinding() {
        return pckngAmtBinding;
    }

    public void setNetAmtBinding(RichInputText netAmtBinding) {
        this.netAmtBinding = netAmtBinding;
    }

    public RichInputText getNetAmtBinding() {
        return netAmtBinding;
    }

    public void setGrNoBinding(RichInputText grNoBinding) {
        this.grNoBinding = grNoBinding;
    }

    public RichInputText getGrNoBinding() {
        return grNoBinding;
    }

    public void setGrdateBinding(RichInputDate grdateBinding) {
        this.grdateBinding = grdateBinding;
    }

    public RichInputDate getGrdateBinding() {
        return grdateBinding;
    }

    public void setVouNoBinding(RichInputText vouNoBinding) {
        this.vouNoBinding = vouNoBinding;
    }

    public RichInputText getVouNoBinding() {
        return vouNoBinding;
    }

    public void setVouDtBinding(RichInputDate vouDtBinding) {
        this.vouDtBinding = vouDtBinding;
    }

    public RichInputDate getVouDtBinding() {
        return vouDtBinding;
    }

    public void setRemDtBinding(RichInputDate remDtBinding) {
        this.remDtBinding = remDtBinding;
    }

    public RichInputDate getRemDtBinding() {
        return remDtBinding;
    }

    public void setIssDtBinding(RichInputDate issDtBinding) {
        this.issDtBinding = issDtBinding;
    }

    public RichInputDate getIssDtBinding() {
        return issDtBinding;
    }

    public void setPartyTpBinding(RichInputText partyTpBinding) {
        this.partyTpBinding = partyTpBinding;
    }

    public RichInputText getPartyTpBinding() {
        return partyTpBinding;
    }

    public void venCdVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance()); 
        if(vce!=null){
            ADFUtils.findOperation("setpartyTp").execute();
        }
    }

    public void createAL(ActionEvent actionEvent) {
        // Add event code here...
    }

    public void deleteAL(ActionEvent actionEvent) {
       ADFUtils.findOperation("Delete").execute();
        
        
    }

    public void tabseletcted(DisclosureEvent disclosureEvent) {
        BigDecimal a=new BigDecimal(0);
        System.out.println("inside tab press event");
        if(disclosureEvent.isExpanded()){
            if((Long)ADFUtils.evaluateEL("#{bindings.PurchaseReturnInvoiceDetailVO1Iterator.estimatedRowCount}")>0)
            {
            ADFUtils.findOperation("copyDatafromDtlinHdr").execute();
            }
            else
            {
                getGrossAmtBinding().setValue(a);
                getCgstAmtHdrBinding().setValue(a);
                getIgstAmtHdrBinding().setValue(a);
                getSgstAmtHdrBinding().setValue(a);
                getNetAmtBinding().setValue(a);
                getDismtHdrBinding().setValue(a);
                } 
          //System.out.println("inside methode of tab press event");  
            //ADFUtils.findOperation("copyDatafromDtlinHdr").execute();
            
        }
    }

    public void itemTabListener(DisclosureEvent disclosureEvent) {
        if(disclosureEvent.isExpanded()){
        
        getRejTypeBinding().setDisabled(true);
        getVenCdBinding().setDisabled(true);
        
        }
    }

    public void setVenCdBinding(RichInputComboboxListOfValues venCdBinding) {
        this.venCdBinding = venCdBinding;
    }

    public RichInputComboboxListOfValues getVenCdBinding() {
        return venCdBinding;
    }

    public void setRejTypeBinding(RichSelectOneChoice rejTypeBinding) {
        this.rejTypeBinding = rejTypeBinding;
    }

    public RichSelectOneChoice getRejTypeBinding() {
        return rejTypeBinding;
    }

    public void setEditButtonBinding(RichButton editButtonBinding) {
        this.editButtonBinding = editButtonBinding;
    }

    public RichButton getEditButtonBinding() {
        return editButtonBinding;
    }

    public void glCdVCL(ValueChangeEvent valueChangeEvent) {
        BigDecimal a=new BigDecimal(0);
        if(valueChangeEvent!=null){
            if((Long)ADFUtils.evaluateEL("#{bindings.PurchaseReturnInvoiceDetailVO1Iterator.estimatedRowCount}")>0)
            {
            ADFUtils.findOperation("copyDatafromDtlinHdr").execute();
            }
            else
            {
                getGrossAmtBinding().setValue(a);
                getCgstAmtHdrBinding().setValue(a);
                getIgstAmtHdrBinding().setValue(a);
                getSgstAmtHdrBinding().setValue(a);
                getNetAmtBinding().setValue(a);
                getDismtHdrBinding().setValue(a);
                } 
        
            //ADFUtils.findOperation("copyDatafromDtlinHdr").execute();
        }
    }
    public String saveAndClose(){
        
        if((Long)ADFUtils.evaluateEL("#{bindings.PurchaseReturnInvoiceDetailVO1Iterator.estimatedRowCount}")==0)
                   {
            
                           ADFUtils.showMessage(" Fill Item Details", 0);
            
                       }
        else{
            OperationBinding op = ADFUtils.findOperation("getInvoiceNoPRI");
            Object rst = op.execute();
                if (rst != null && rst != "") 
                {
                    if (op.getErrors().isEmpty()) {
                        ADFUtils.findOperation("Commit").execute();
                    
                        ADFUtils.showMessage("Record Saved Successfully" + "Invoice No is::"+ rst, 2);
                        return "SaveAndClose";
                    
                    }
                    
                }
                
                if (rst == null || rst == "" || rst == " ") 
                {
                    if (op.getErrors().isEmpty()) {
                        ADFUtils.findOperation("Commit").execute();
                        ADFUtils.showMessage(" Record Updated Successfully.", 2); 
                      return "SaveAndClose";
                  }
            }
            
               
        }
        
       return null; 
    }

    public void setApproveBy(RichInputComboboxListOfValues approveBy) {
        this.approveBy = approveBy;
    }

    public RichInputComboboxListOfValues getApproveBy() {
        return approveBy;
    }

    public void setInvoiceNoBinding(RichInputText invoiceNoBinding) {
        this.invoiceNoBinding = invoiceNoBinding;
    }

    public RichInputText getInvoiceNoBinding() {
        return invoiceNoBinding;
    }

    public void freightVCL(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance()); 
        System.out.println("INSIDE BEAN************");
        if(valueChangeEvent!=null){
            if((Long)ADFUtils.evaluateEL("#{bindings.PurchaseReturnInvoiceDetailVO1Iterator.estimatedRowCount}")>0)
            {
            ADFUtils.findOperation("copyDatafromDtlinHdr").execute();
            }
          
        }
    }

    public void setUnitBinding(RichInputComboboxListOfValues unitBinding) {
        this.unitBinding = unitBinding;
    }

    public RichInputComboboxListOfValues getUnitBinding() {
        return unitBinding;
    }

    public void otherAmtValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
       if(object!=null) {
                        
                        BigDecimal oth = (BigDecimal) object;
                        System.out.println("other amount isss==>>"+oth);
                    
                    }

    }

    public void CheckAuthority(ReturnPopupEvent returnPopupEvent){
          OperationBinding op=ADFUtils.findOperation("CheckAuthority");
            op.getParamsMap().put("unitCd",unitBinding.getValue());
            op.getParamsMap().put("EmpCd", approveBy.getValue());
            op.execute();
        }
  
}
