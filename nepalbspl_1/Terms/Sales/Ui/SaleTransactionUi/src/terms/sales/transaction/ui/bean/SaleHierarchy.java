package terms.sales.transaction.ui.bean;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.servlet.http.HttpServletRequest;

public class SaleHierarchy {
    public SaleHierarchy() {
    }

    public String getURL() {
      String unitCode=  (ADFUtils.resolveExpression("#{pageFlowScope.unitCode}") == null ? "10001" : (String)ADFUtils.resolveExpression("#{pageFlowScope.unitCode}"));
        String empCode=  (ADFUtils.resolveExpression("#{pageFlowScope.empCode}") == null ? "E-001" : (String)ADFUtils.resolveExpression("#{pageFlowScope.empCode}"));
        String finYear=  (ADFUtils.resolveExpression("#{pageFlowScope.finYear}") == null ? "77-78" : (String)ADFUtils.resolveExpression("#{pageFlowScope.finYear}"));
        
        String addRight=  (ADFUtils.resolveExpression("#{pageFlowScope.addRight}") == null ? "N" : (String)ADFUtils.resolveExpression("#{pageFlowScope.addRight}"));
        String editRight=  (ADFUtils.resolveExpression("#{pageFlowScope.editRight}") == null ? "N" : (String)ADFUtils.resolveExpression("#{pageFlowScope.editRight}"));
        String dltRight=  (ADFUtils.resolveExpression("#{pageFlowScope.deleteRight}") == null ? "N" : (String)ADFUtils.resolveExpression("#{pageFlowScope.deleteRight}"));
      
        return "/bspl/sale/hierarchy/openSalesHierarchy?empCode="+empCode+"&unitCode="+unitCode+"&financialYear="+finYear+"&addRight="+addRight+"&editRight="+editRight+"&deleteRight="+dltRight;
    }
}
