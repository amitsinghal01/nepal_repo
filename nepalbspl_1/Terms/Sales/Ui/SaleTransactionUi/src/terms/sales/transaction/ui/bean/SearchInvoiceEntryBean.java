package terms.sales.transaction.ui.bean;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import java.io.OutputStream;

import java.sql.Connection;

import java.sql.SQLException;

import java.util.HashMap;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.naming.Context;
import javax.naming.InitialContext;

//import oracle.adf.model.OperationBinding;
import javax.servlet.http.HttpServletResponse;

import javax.sql.DataSource;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.BindingContainer;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import oracle.binding.OperationBinding;

public class SearchInvoiceEntryBean {
    private RichTable invoiceEntryTableBinding;
    private String invoceMode ="";

    public SearchInvoiceEntryBean() {
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                   {
                   ADFUtils.findOperation("Delete").execute();
                       OperationBinding op = ADFUtils.findOperation("Commit");
                       Object rst = op.execute();
                       System.out.println("Record Delete Successfully");
                       
                       if(op.getErrors().isEmpty())
                        {
                       FacesMessage Message = new FacesMessage("Record Deleted Successfully");   
                       Message.setSeverity(FacesMessage.SEVERITY_INFO);   
                       FacesContext fc = FacesContext.getCurrentInstance();   
                       fc.addMessage(null, Message);
                        }
                       else
                       {
                          FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                          Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                          FacesContext fc = FacesContext.getCurrentInstance();
                          fc.addMessage(null, Message);
                         }
                  
                   }

               AdfFacesContext.getCurrentInstance().addPartialTarget(invoiceEntryTableBinding);
    }

    public void setInvoiceEntryTableBinding(RichTable invoiceEntryTableBinding) {
        this.invoiceEntryTableBinding = invoiceEntryTableBinding;
    }

    public RichTable getInvoiceEntryTableBinding() {
        return invoiceEntryTableBinding;
    }
    public void deletePopupDialogDL(ActionEvent actionEvent) {
        // Add event code here...
    }
    
    public Connection getDataSourceConnection(String dataSourceName) throws Exception {
            Context ctx = new InitialContext();
            DataSource ds = (DataSource)ctx.lookup(dataSourceName);
            return ds.getConnection();
        }
        private Connection getConnection() throws Exception {
            return getDataSourceConnection("java:comp/env/jdbc//ApplicationDBDS");
        }

    public void downloadJrxml(FacesContext facesContext, OutputStream outputStream) {
        Connection conn=null;
            try {
                String file_name ="";
                System.out.println("invoceMode===================>"+invoceMode);
                
                if(invoceMode.equals("PRP"))
                {
                file_name="r_pre_pack.jasper";
                }
                else if(invoceMode.equals("POP"))
                {
                    file_name="r_post_pack.jasper";
                }
                else if(invoceMode.equals("PRI"))
                {
                    file_name="r_pre_inv.jasper";
                }
                else if(invoceMode.equals("POI"))
                {
                    file_name="r_post_inv.jasper";
                }
                else if(invoceMode.equals("TAI"))
                {
                    file_name="tax_invoice.jasper";
                }
                else if(invoceMode.equals("CO"))
                {
                    file_name="r_certi_origin.jasper";
                }
                else if(invoceMode.equals("DC"))
                {
                    file_name="tax_invoice_crdr.jasper";
                }
                System.out.println("After Set Value File Name is===>"+file_name);
                
                OperationBinding binding = ADFUtils.findOperation("fileNameForPrint");
                binding.getParamsMap().put("fileId","ERP0000000142");
                binding.execute();
                //*************End Find File name***********//
                System.out.println("Binding Result :"+binding.getResult());
                if(binding.getResult() != null){
                    String result = binding.getResult().toString();
//                    String result ="//home//lenovo//oracle//jasper";
                    int last_index = result.lastIndexOf("/");
                    String path = result.substring(0,last_index+1)+file_name;
                    System.out.println("FILE PATH IS===>"+path);
                    InputStream input = new FileInputStream(path); 
                    DCIteratorBinding poIter = (DCIteratorBinding) getBindings().get("SearchInvoiceEntryHeaderVO1Iterator");
                    String invoiceNum = poIter.getCurrentRow().getAttribute("Identifier").toString();
                    String unitCode = poIter.getCurrentRow().getAttribute("UnitCode").toString();
                    System.out.println("Pi Number :- "+invoiceNum+" "+unitCode);
                    Map n = new HashMap();

            
            if(invoceMode.equals("DC"))
            {
                    System.out.println("When Invoice Type Debit Or Credit");
                    n.put("p_inv_no", invoiceNum);
                n.put("p_unit", unitCode);
                n.put("p_tp", poIter.getCurrentRow().getAttribute("InvParamTrans"));
                conn = getConnection( );
                
                JasperReport design = (JasperReport) JRLoader.loadObject(input);
                System.out.println("Path : " + input + " -------" + design+" param:"+n);
                
                @SuppressWarnings("unchecked")
                net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, conn);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                byte pdf[] = JasperExportManager.exportReportToPdf(print);
                HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                response.getOutputStream().write(pdf);
                response.getOutputStream().flush();
                response.getOutputStream().close();
                facesContext.responseComplete();
            
            }
            else
                {
                    System.out.println("When Invoice Type NOT Debit Or Credit");
                    System.out.println("FILE PATH IS===>"+path);
                    n.put("p_inv_no", invoiceNum);
                    n.put("p_unit", unitCode);
                    Connection con = getConnection( );
                    if (invoceMode.equals("TAI")) {
                        n.put("p_tp", poIter.getCurrentRow().getAttribute("InvParamTrans"));
                    }
                    System.out.println("invoiceNum"+invoiceNum);
                    System.out.println("unitCode"+unitCode);
                    System.out.println("INV HEAD TYPE"+poIter.getCurrentRow().getAttribute("InvParamTrans"));
//                    JasperDesign jasperDesign = JRXmlLoader.load(input); 
//                    JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign); 
//                    System.out.println("Path : " + input + " -------" +" param:"+n);          
//                    JasperPrint print = JasperFillManager.fillReport(jasperReport, n, con);
//                    JasperPrintManager.printReport(print, true);
                JasperReport design = (JasperReport) JRLoader.loadObject(input);
                System.out.println("Path : " + input + " -------" + design+" param:"+n);
                @SuppressWarnings("unchecked")
                net.sf.jasperreports.engine.JasperPrint print = JasperFillManager.fillReport(design, n, con);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                JasperExportManager.exportReportToPdfStream(print, byteArrayOutputStream);
                byte pdf[] = JasperExportManager.exportReportToPdf(print);
                HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
                response.getOutputStream().write(pdf);
                response.getOutputStream().flush();
                response.getOutputStream().close();
                facesContext.responseComplete();
                    
                }
                    if(conn != null){
                                       System.out.println("in finally connection closed");
                                       conn.close( );
                                       conn = null;
                                   }
            }else
            System.out.println("File Name/File Path not found.");
            } catch (Exception fnfe) {
            // TODO: Add catch code
            fnfe.printStackTrace();
            }
        }

    public BindingContainer getBindings()
          {
            return BindingContext.getCurrent().getCurrentBindingsEntry();
          }

    public void setInvoceMode(String invoceMode) {
        this.invoceMode = invoceMode;
    }

    public String getInvoceMode() {
        return invoceMode;
    }
}
