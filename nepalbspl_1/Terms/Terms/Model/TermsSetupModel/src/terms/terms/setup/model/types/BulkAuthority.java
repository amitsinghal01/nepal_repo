package terms.terms.setup.model.types;

import java.io.Serializable;

import java.math.BigDecimal;

import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;

public class BulkAuthority implements Serializable,SQLData {
    @SuppressWarnings("compatibility:-8909851679023590633")
    private static final long serialVersionUID = 1L;
    private String EMP_CODE;
    private String DEPT_CODE;
    private String MODULE_CODE;
    private String UNIT_CODE;
    private BigDecimal FROM_LIMIT;
    private BigDecimal TO_LIMIT;
    private String EMP_CODE_COPY_FROM;
    private String CREATED_BY;

    public BulkAuthority() {
        super();
    }

    public BulkAuthority(String empCode, String deptCode, String moduleCode, String unitCode, BigDecimal fromLimit,
                         BigDecimal toLimit, String empCodeCopyFrom, String createdBy) {
        this.setEMP_CODE(empCode);
        this.setDEPT_CODE(deptCode);
        this.setMODULE_CODE(moduleCode);
        this.setUNIT_CODE(unitCode);
        this.setFROM_LIMIT(fromLimit);
        this.setTO_LIMIT(toLimit);
        this.setEMP_CODE_COPY_FROM(empCodeCopyFrom);
        this.setCREATED_BY(createdBy);
    }

    public BulkAuthority(String empCode, String deptCode, String moduleCode, String unitCode, BigDecimal fromLimit,
                         BigDecimal toLimit, String createdBy) {
        this.setEMP_CODE(empCode);
        this.setDEPT_CODE(deptCode);
        this.setMODULE_CODE(moduleCode);
        this.setUNIT_CODE(unitCode);
        this.setFROM_LIMIT(fromLimit);
        this.setTO_LIMIT(toLimit);
        this.setCREATED_BY(createdBy);
    }

    public BulkAuthority(String empCode, String empCodeCopyFrom, String createdBy) {
        this.setEMP_CODE(empCode);
        this.setEMP_CODE_COPY_FROM(empCodeCopyFrom);
        this.setCREATED_BY(createdBy);
    }

    public void setEMP_CODE(String EMP_CODE) {
        this.EMP_CODE = EMP_CODE;
    }

    public String getEMP_CODE() {
        return EMP_CODE;
    }

    public void setDEPT_CODE(String DEPT_CODE) {
        this.DEPT_CODE = DEPT_CODE;
    }

    public String getDEPT_CODE() {
        return DEPT_CODE;
    }

    public void setMODULE_CODE(String MODULE_CODE) {
        this.MODULE_CODE = MODULE_CODE;
    }

    public String getMODULE_CODE() {
        return MODULE_CODE;
    }

    public void setUNIT_CODE(String UNIT_CODE) {
        this.UNIT_CODE = UNIT_CODE;
    }

    public String getUNIT_CODE() {
        return UNIT_CODE;
    }

    public void setFROM_LIMIT(BigDecimal FROM_LIMIT) {
        this.FROM_LIMIT = FROM_LIMIT;
    }

    public BigDecimal getFROM_LIMIT() {
        return FROM_LIMIT;
    }

    public void setTO_LIMIT(BigDecimal TO_LIMIT) {
        this.TO_LIMIT = TO_LIMIT;
    }

    public BigDecimal getTO_LIMIT() {
        return TO_LIMIT;
    }

    public void setEMP_CODE_COPY_FROM(String EMP_CODE_COPY_FROM) {
        this.EMP_CODE_COPY_FROM = EMP_CODE_COPY_FROM;
    }

    public String getEMP_CODE_COPY_FROM() {
        return EMP_CODE_COPY_FROM;
    }

    public void setCREATED_BY(String CREATED_BY) {
        this.CREATED_BY = CREATED_BY;
    }

    public String getCREATED_BY() {
        return CREATED_BY;
    }

    @Override
    public String getSQLTypeName() throws SQLException {
        return "BULK_AUTHORITY_INPUT";
    }

    @Override
    public void readSQL(SQLInput sQLInput, String sqlTypeName) throws SQLException {
        this.EMP_CODE = sQLInput.readString();
        this.DEPT_CODE = sQLInput.readString();
        this.MODULE_CODE = sQLInput.readString();
        this.UNIT_CODE = sQLInput.readString();
        this.FROM_LIMIT = sQLInput.readBigDecimal();
        this.TO_LIMIT = sQLInput.readBigDecimal();
        this.EMP_CODE_COPY_FROM = sQLInput.readString();
        this.CREATED_BY = sQLInput.readString();
    }

    @Override
    public void writeSQL(SQLOutput sQLOutput) throws SQLException {
        sQLOutput.writeString(this.EMP_CODE);
        sQLOutput.writeString(this.DEPT_CODE);
        sQLOutput.writeString(this.MODULE_CODE);
        sQLOutput.writeString(this.UNIT_CODE);
        sQLOutput.writeBigDecimal(this.FROM_LIMIT);
        sQLOutput.writeBigDecimal(this.TO_LIMIT);
        sQLOutput.writeString(this.EMP_CODE_COPY_FROM);
        sQLOutput.writeString(this.CREATED_BY);
    }
}
