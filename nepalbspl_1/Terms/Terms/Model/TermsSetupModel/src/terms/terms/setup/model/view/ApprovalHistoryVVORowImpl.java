package terms.terms.setup.model.view;

import java.math.BigDecimal;

import java.sql.Timestamp;

import oracle.jbo.Row;
import oracle.jbo.RowSet;
import oracle.jbo.server.ViewRowImpl;

import terms.terms.setup.model.applicationModule.TermsSetupAMImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Fri Jun 07 17:14:07 IST 2019
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class ApprovalHistoryVVORowImpl extends ViewRowImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        DocNo,
        DocDt,
        DocDept,
        DocSt,
        DocBy,
        DocUnit,
        DocAmount,
        DocName,
        DocAlias,
        DocId,
        DocActName,
        DocNo1,
        ApprovedBy,
        ApprovedDate,
        ApprovedUname,
        Docunqno,
        Terminal,
        UnitNameTrans,
        DepartmentNameTrans,
        DocByNameTrans,
        UnitVO1,
        LocationAndDepartmenetVVO1,
        ApprovalHistoryVVO1,
        DistinctPendingApprovalActivity1,
        DistinctPendingApprovalFlag1;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int DOCNO = AttributesEnum.DocNo.index();
    public static final int DOCDT = AttributesEnum.DocDt.index();
    public static final int DOCDEPT = AttributesEnum.DocDept.index();
    public static final int DOCST = AttributesEnum.DocSt.index();
    public static final int DOCBY = AttributesEnum.DocBy.index();
    public static final int DOCUNIT = AttributesEnum.DocUnit.index();
    public static final int DOCAMOUNT = AttributesEnum.DocAmount.index();
    public static final int DOCNAME = AttributesEnum.DocName.index();
    public static final int DOCALIAS = AttributesEnum.DocAlias.index();
    public static final int DOCID = AttributesEnum.DocId.index();
    public static final int DOCACTNAME = AttributesEnum.DocActName.index();
    public static final int DOCNO1 = AttributesEnum.DocNo1.index();
    public static final int APPROVEDBY = AttributesEnum.ApprovedBy.index();
    public static final int APPROVEDDATE = AttributesEnum.ApprovedDate.index();
    public static final int APPROVEDUNAME = AttributesEnum.ApprovedUname.index();
    public static final int DOCUNQNO = AttributesEnum.Docunqno.index();
    public static final int TERMINAL = AttributesEnum.Terminal.index();
    public static final int UNITNAMETRANS = AttributesEnum.UnitNameTrans.index();
    public static final int DEPARTMENTNAMETRANS = AttributesEnum.DepartmentNameTrans.index();
    public static final int DOCBYNAMETRANS = AttributesEnum.DocByNameTrans.index();
    public static final int UNITVO1 = AttributesEnum.UnitVO1.index();
    public static final int LOCATIONANDDEPARTMENETVVO1 = AttributesEnum.LocationAndDepartmenetVVO1.index();
    public static final int APPROVALHISTORYVVO1 = AttributesEnum.ApprovalHistoryVVO1.index();
    public static final int DISTINCTPENDINGAPPROVALACTIVITY1 = AttributesEnum.DistinctPendingApprovalActivity1.index();
    public static final int DISTINCTPENDINGAPPROVALFLAG1 = AttributesEnum.DistinctPendingApprovalFlag1.index();

    /**
     * This is the default constructor (do not remove).
     */
    public ApprovalHistoryVVORowImpl() {
    }

    /**
     * Gets the attribute value for the calculated attribute DocNo.
     * @return the DocNo
     */
    public String getDocNo() {
        return (String) getAttributeInternal(DOCNO);
    }

    /**
     * Gets the attribute value for the calculated attribute DocDt.
     * @return the DocDt
     */
    public Timestamp getDocDt() {
        return (Timestamp) getAttributeInternal(DOCDT);
    }

    /**
     * Gets the attribute value for the calculated attribute DocDept.
     * @return the DocDept
     */
    public String getDocDept() {
        return (String) getAttributeInternal(DOCDEPT);
    }

    /**
     * Gets the attribute value for the calculated attribute DocSt.
     * @return the DocSt
     */
    public String getDocSt() {
        return (String) getAttributeInternal(DOCST);
    }

    /**
     * Gets the attribute value for the calculated attribute DocBy.
     * @return the DocBy
     */
    public String getDocBy() {
        return (String) getAttributeInternal(DOCBY);
    }

    /**
     * Gets the attribute value for the calculated attribute DocUnit.
     * @return the DocUnit
     */
    public String getDocUnit() {
        return (String) getAttributeInternal(DOCUNIT);
    }

    /**
     * Gets the attribute value for the calculated attribute DocAmount.
     * @return the DocAmount
     */
    public BigDecimal getDocAmount() {
        return (BigDecimal) getAttributeInternal(DOCAMOUNT);
    }

    /**
     * Gets the attribute value for the calculated attribute DocName.
     * @return the DocName
     */
    public String getDocName() {
        return (String) getAttributeInternal(DOCNAME);
    }

    /**
     * Gets the attribute value for the calculated attribute DocAlias.
     * @return the DocAlias
     */
    public String getDocAlias() {
        return (String) getAttributeInternal(DOCALIAS);
    }

    /**
     * Gets the attribute value for the calculated attribute DocId.
     * @return the DocId
     */
    public BigDecimal getDocId() {
        return (BigDecimal) getAttributeInternal(DOCID);
    }

    /**
     * Gets the attribute value for the calculated attribute DocActName.
     * @return the DocActName
     */
    public String getDocActName() {
        return (String) getAttributeInternal(DOCACTNAME);
    }

    /**
     * Gets the attribute value for the calculated attribute DocNo1.
     * @return the DocNo1
     */
    public String getDocNo1() {
        return (String) getAttributeInternal(DOCNO1);
    }

    /**
     * Gets the attribute value for the calculated attribute ApprovedBy.
     * @return the ApprovedBy
     */
    public String getApprovedBy() {
        return (String) getAttributeInternal(APPROVEDBY);
    }

    /**
     * Gets the attribute value for the calculated attribute ApprovedDate.
     * @return the ApprovedDate
     */
    public Timestamp getApprovedDate() {
        return (Timestamp) getAttributeInternal(APPROVEDDATE);
    }

    /**
     * Gets the attribute value for the calculated attribute ApprovedUname.
     * @return the ApprovedUname
     */
    public String getApprovedUname() {
        return (String) getAttributeInternal(APPROVEDUNAME);
    }

    /**
     * Gets the attribute value for the calculated attribute Docunqno.
     * @return the Docunqno
     */
    public Long getDocunqno() {
        return (Long) getAttributeInternal(DOCUNQNO);
    }

    /**
     * Gets the attribute value for the calculated attribute Terminal.
     * @return the Terminal
     */
    public String getTerminal() {
        return (String) getAttributeInternal(TERMINAL);
    }

    /**
     * Gets the attribute value for the calculated attribute UnitNameTrans.
     * @return the UnitNameTrans
     */
    public String getUnitNameTrans() {
        Row r[]= getUnitVO1().getFilteredRows("Code", getAttributeInternal(DOCUNIT));
        if(r.length>0)
        {
        if(r[0].getAttribute("ShortName")!=null)
        {
        return r[0].getAttribute("ShortName").toString();
        }
        }
        return (String) getAttributeInternal(UNITNAMETRANS);
    }


    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute UnitNameTrans.
     * @param value value to set the  UnitNameTrans
     */
    public void setUnitNameTrans(String value) {
        setAttributeInternal(UNITNAMETRANS, value);
    }

    /**
     * Gets the attribute value for the calculated attribute DepartmentNameTrans.
     * @return the DepartmentNameTrans
     */
    public String getDepartmentNameTrans() {
        if(getAttributeInternal(DEPARTMENTNAMETRANS)==null && getAttributeInternal(DOCDEPT)!=null)
        {
        Row r1[] = getLocationAndDepartmenetVVO1().getFilteredRows("Code",getAttributeInternal(DOCDEPT));
        if(r1.length>0)
        {
            if(r1[0].getAttribute("Name")!=null)
            {
            return r1[0].getAttribute("Name").toString();
            }
        }
        
        }
        return (String) getAttributeInternal(DEPARTMENTNAMETRANS);
    }


    /**
     * Gets the attribute value for the calculated attribute DocByNameTrans.
     * @return the DocByNameTrans
     */
    public String getDocByNameTrans() {
          if(getAttributeInternal(DOCBY)!=null && getAttributeInternal(DOCBYNAMETRANS)==null)
          {
              TermsSetupAMImpl am = (TermsSetupAMImpl) getApplicationModule();
              Row r1[] = am.getEmployeeMasterVO1().getFilteredRows("EmpNumber", getAttributeInternal(DOCBY));
              if (r1.length > 0) {
                  if (r1[0].getAttribute("EmpFirstName") != null) {
                      return r1[0].getAttribute("EmpFirstName").toString() + " " +
                             (r1[0].getAttribute("EmpLastName") != null ? r1[0].getAttribute("EmpLastName") : "");
                      //EmpLastName
                  }
              }
          }
          return (String) getAttributeInternal(DOCBYNAMETRANS);
      }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute DocByNameTrans.
     * @param value value to set the  DocByNameTrans
     */
    public void setDocByNameTrans(String value) {
        setAttributeInternal(DOCBYNAMETRANS, value);
    }

    /**
     * Gets the view accessor <code>RowSet</code> UnitVO1.
     */
    public RowSet getUnitVO1() {
        return (RowSet) getAttributeInternal(UNITVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> LocationAndDepartmenetVVO1.
     */
    public RowSet getLocationAndDepartmenetVVO1() {
        return (RowSet) getAttributeInternal(LOCATIONANDDEPARTMENETVVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> ApprovalHistoryVVO1.
     */
    public RowSet getApprovalHistoryVVO1() {
        return (RowSet) getAttributeInternal(APPROVALHISTORYVVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> DistinctPendingApprovalActivity1.
     */
    public RowSet getDistinctPendingApprovalActivity1() {
        return (RowSet) getAttributeInternal(DISTINCTPENDINGAPPROVALACTIVITY1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> DistinctPendingApprovalFlag1.
     */
    public RowSet getDistinctPendingApprovalFlag1() {
        return (RowSet) getAttributeInternal(DISTINCTPENDINGAPPROVALFLAG1);
    }


}

