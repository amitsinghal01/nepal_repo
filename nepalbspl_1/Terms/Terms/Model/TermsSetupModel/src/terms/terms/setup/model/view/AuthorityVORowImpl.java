package terms.terms.setup.model.view;

import java.math.BigDecimal;

import java.sql.Timestamp;

import oracle.jbo.RowSet;
import oracle.jbo.server.EntityImpl;
import oracle.jbo.server.ViewRowImpl;

import terms.hrm.setup.model.entity.EmployeeMasterEOImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Mon Feb 26 12:27:18 IST 2018
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class AuthorityVORowImpl extends ViewRowImpl {
    public static final int ENTITY_AUTHORITYEO = 0;
    public static final int ENTITY_UNITEO = 1;
    public static final int ENTITY_EMPLOYEEMASTEREO = 2;
    public static final int ENTITY_LOCATIONEO = 3;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        EmpCd,
        FormNm,
        AuthoLevel,
        Pwd,
        FinAuthoLimit,
        AuthoSeq,
        DeptCd,
        AuthoLimitFr,
        UnitCd,
        AuthoId,
        Name,
        Code,
        ObjectVersionNumber,
        EmpFirstName,
        EmpNumber,
        ObjectVersionNumber1,
        EmpLastName,
        LongDesc,
        LocatCode,
        ObjectVersionNumber2,
        EditTrans,
        LastUpdateDate,
        LastUpdatedBy,
        CreatedBy,
        CreationDate,
        ObjectVersionNumber3,
        UnitVO1,
        EmpViewVVO1,
        LocationVO1,
        SecControlVO1,
        SecControlVO2;
        static AttributesEnum[] vals = null; ;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int EMPCD = AttributesEnum.EmpCd.index();
    public static final int FORMNM = AttributesEnum.FormNm.index();
    public static final int AUTHOLEVEL = AttributesEnum.AuthoLevel.index();
    public static final int PWD = AttributesEnum.Pwd.index();
    public static final int FINAUTHOLIMIT = AttributesEnum.FinAuthoLimit.index();
    public static final int AUTHOSEQ = AttributesEnum.AuthoSeq.index();
    public static final int DEPTCD = AttributesEnum.DeptCd.index();
    public static final int AUTHOLIMITFR = AttributesEnum.AuthoLimitFr.index();
    public static final int UNITCD = AttributesEnum.UnitCd.index();
    public static final int AUTHOID = AttributesEnum.AuthoId.index();
    public static final int NAME = AttributesEnum.Name.index();
    public static final int CODE = AttributesEnum.Code.index();
    public static final int OBJECTVERSIONNUMBER = AttributesEnum.ObjectVersionNumber.index();
    public static final int EMPFIRSTNAME = AttributesEnum.EmpFirstName.index();
    public static final int EMPNUMBER = AttributesEnum.EmpNumber.index();
    public static final int OBJECTVERSIONNUMBER1 = AttributesEnum.ObjectVersionNumber1.index();
    public static final int EMPLASTNAME = AttributesEnum.EmpLastName.index();
    public static final int LONGDESC = AttributesEnum.LongDesc.index();
    public static final int LOCATCODE = AttributesEnum.LocatCode.index();
    public static final int OBJECTVERSIONNUMBER2 = AttributesEnum.ObjectVersionNumber2.index();
    public static final int EDITTRANS = AttributesEnum.EditTrans.index();
    public static final int LASTUPDATEDATE = AttributesEnum.LastUpdateDate.index();
    public static final int LASTUPDATEDBY = AttributesEnum.LastUpdatedBy.index();
    public static final int CREATEDBY = AttributesEnum.CreatedBy.index();
    public static final int CREATIONDATE = AttributesEnum.CreationDate.index();
    public static final int OBJECTVERSIONNUMBER3 = AttributesEnum.ObjectVersionNumber3.index();
    public static final int UNITVO1 = AttributesEnum.UnitVO1.index();
    public static final int EMPVIEWVVO1 = AttributesEnum.EmpViewVVO1.index();
    public static final int LOCATIONVO1 = AttributesEnum.LocationVO1.index();
    public static final int SECCONTROLVO1 = AttributesEnum.SecControlVO1.index();
    public static final int SECCONTROLVO2 = AttributesEnum.SecControlVO2.index();

    /**
     * This is the default constructor (do not remove).
     */
    public AuthorityVORowImpl() {
    }

    /**
     * Gets AuthorityEO entity object.
     * @return the AuthorityEO
     */
    public EntityImpl getAuthorityEO() {
        return (EntityImpl) getEntity(ENTITY_AUTHORITYEO);
    }

    /**
     * Gets UnitEO entity object.
     * @return the UnitEO
     */
    public EntityImpl getUnitEO() {
        return (EntityImpl) getEntity(ENTITY_UNITEO);
    }

    /**
     * Gets EmployeeMasterEO entity object.
     * @return the EmployeeMasterEO
     */
    public EmployeeMasterEOImpl getEmployeeMasterEO() {
        return (EmployeeMasterEOImpl) getEntity(ENTITY_EMPLOYEEMASTEREO);
    }

    /**
     * Gets LocationEO entity object.
     * @return the LocationEO
     */
    public EntityImpl getLocationEO() {
        return (EntityImpl) getEntity(ENTITY_LOCATIONEO);
    }

    /**
     * Gets the attribute value for EMP_CD using the alias name EmpCd.
     * @return the EMP_CD
     */
    public String getEmpCd() {
        return (String) getAttributeInternal(EMPCD);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_CD using the alias name EmpCd.
     * @param value value to set the EMP_CD
     */
    public void setEmpCd(String value) {
        setAttributeInternal(EMPCD, value);
    }

    /**
     * Gets the attribute value for FORM_NM using the alias name FormNm.
     * @return the FORM_NM
     */
    public String getFormNm() {
        return (String) getAttributeInternal(FORMNM);
    }

    /**
     * Sets <code>value</code> as attribute value for FORM_NM using the alias name FormNm.
     * @param value value to set the FORM_NM
     */
    public void setFormNm(String value) {
        setAttributeInternal(FORMNM, value);
    }

    /**
     * Gets the attribute value for AUTHO_LEVEL using the alias name AuthoLevel.
     * @return the AUTHO_LEVEL
     */
    public String getAuthoLevel() {
        return (String) getAttributeInternal(AUTHOLEVEL);
    }

    /**
     * Sets <code>value</code> as attribute value for AUTHO_LEVEL using the alias name AuthoLevel.
     * @param value value to set the AUTHO_LEVEL
     */
    public void setAuthoLevel(String value) {
        setAttributeInternal(AUTHOLEVEL, value);
    }

    /**
     * Gets the attribute value for PWD using the alias name Pwd.
     * @return the PWD
     */
    public String getPwd() {
        return (String) getAttributeInternal(PWD);
    }

    /**
     * Sets <code>value</code> as attribute value for PWD using the alias name Pwd.
     * @param value value to set the PWD
     */
    public void setPwd(String value) {
        setAttributeInternal(PWD, value);
    }

    /**
     * Gets the attribute value for FIN_AUTHO_LIMIT using the alias name FinAuthoLimit.
     * @return the FIN_AUTHO_LIMIT
     */
    public BigDecimal getFinAuthoLimit() {
        return (BigDecimal) getAttributeInternal(FINAUTHOLIMIT);
    }

    /**
     * Sets <code>value</code> as attribute value for FIN_AUTHO_LIMIT using the alias name FinAuthoLimit.
     * @param value value to set the FIN_AUTHO_LIMIT
     */
    public void setFinAuthoLimit(BigDecimal value) {
        setAttributeInternal(FINAUTHOLIMIT, value);
    }

    /**
     * Gets the attribute value for AUTHO_SEQ using the alias name AuthoSeq.
     * @return the AUTHO_SEQ
     */
    public BigDecimal getAuthoSeq() {
        return (BigDecimal) getAttributeInternal(AUTHOSEQ);
    }

    /**
     * Sets <code>value</code> as attribute value for AUTHO_SEQ using the alias name AuthoSeq.
     * @param value value to set the AUTHO_SEQ
     */
    public void setAuthoSeq(BigDecimal value) {
        setAttributeInternal(AUTHOSEQ, value);
    }

    /**
     * Gets the attribute value for DEPT_CD using the alias name DeptCd.
     * @return the DEPT_CD
     */
    public String getDeptCd() {
        return (String) getAttributeInternal(DEPTCD);
    }

    /**
     * Sets <code>value</code> as attribute value for DEPT_CD using the alias name DeptCd.
     * @param value value to set the DEPT_CD
     */
    public void setDeptCd(String value) {
        setAttributeInternal(DEPTCD, value);
    }

    /**
     * Gets the attribute value for AUTHO_LIMIT_FR using the alias name AuthoLimitFr.
     * @return the AUTHO_LIMIT_FR
     */
    public BigDecimal getAuthoLimitFr() {
        return (BigDecimal) getAttributeInternal(AUTHOLIMITFR);
    }

    /**
     * Sets <code>value</code> as attribute value for AUTHO_LIMIT_FR using the alias name AuthoLimitFr.
     * @param value value to set the AUTHO_LIMIT_FR
     */
    public void setAuthoLimitFr(BigDecimal value) {
        setAttributeInternal(AUTHOLIMITFR, value);
    }

    /**
     * Gets the attribute value for UNIT_CD using the alias name UnitCd.
     * @return the UNIT_CD
     */
    public String getUnitCd() {
        return (String) getAttributeInternal(UNITCD);
    }

    /**
     * Sets <code>value</code> as attribute value for UNIT_CD using the alias name UnitCd.
     * @param value value to set the UNIT_CD
     */
    public void setUnitCd(String value) {
        setAttributeInternal(UNITCD, value);
    }

    /**
     * Gets the attribute value for AUTHO_ID using the alias name AuthoId.
     * @return the AUTHO_ID
     */
    public Long getAuthoId() {
        return (Long) getAttributeInternal(AUTHOID);
    }

    /**
     * Sets <code>value</code> as attribute value for AUTHO_ID using the alias name AuthoId.
     * @param value value to set the AUTHO_ID
     */
    public void setAuthoId(Long value) {
        setAttributeInternal(AUTHOID, value);
    }

    /**
     * Gets the attribute value for NAME using the alias name Name.
     * @return the NAME
     */
    public String getName() {
        return (String) getAttributeInternal(NAME);
    }

    /**
     * Sets <code>value</code> as attribute value for NAME using the alias name Name.
     * @param value value to set the NAME
     */
    public void setName(String value) {
        setAttributeInternal(NAME, value);
    }

    /**
     * Gets the attribute value for CODE using the alias name Code.
     * @return the CODE
     */
    public String getCode() {
        return (String) getAttributeInternal(CODE);
    }

    /**
     * Sets <code>value</code> as attribute value for CODE using the alias name Code.
     * @param value value to set the CODE
     */
    public void setCode(String value) {
        setAttributeInternal(CODE, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER, value);
    }

    /**
     * Gets the attribute value for EMP_FIRST_NAME using the alias name EmpFirstName.
     * @return the EMP_FIRST_NAME
     */
    public String getEmpFirstName() {

        if ((getAttributeInternal(EMPFIRSTNAME) != null) && (getAttributeInternal(EMPLASTNAME) != null)) {
            return (String) getAttributeInternal(EMPFIRSTNAME) + " " + (String) getAttributeInternal(EMPLASTNAME);
        } else {
            return (String) getAttributeInternal(EMPFIRSTNAME);
        }

    }


    /**
     * Sets <code>value</code> as attribute value for EMP_FIRST_NAME using the alias name EmpFirstName.
     * @param value value to set the EMP_FIRST_NAME
     */
    public void setEmpFirstName(String value) {
        setAttributeInternal(EMPFIRSTNAME, value);
    }

    /**
     * Gets the attribute value for EMP_NUMBER using the alias name EmpNumber.
     * @return the EMP_NUMBER
     */
    public String getEmpNumber() {
        return (String) getAttributeInternal(EMPNUMBER);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_NUMBER using the alias name EmpNumber.
     * @param value value to set the EMP_NUMBER
     */
    public void setEmpNumber(String value) {
        setAttributeInternal(EMPNUMBER, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber1.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber1() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER1);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber1.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber1(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER1, value);
    }

    /**
     * Gets the attribute value for EMP_LAST_NAME using the alias name EmpLastName.
     * @return the EMP_LAST_NAME
     */
    public String getEmpLastName() {
        return (String) getAttributeInternal(EMPLASTNAME);
    }

    /**
     * Sets <code>value</code> as attribute value for EMP_LAST_NAME using the alias name EmpLastName.
     * @param value value to set the EMP_LAST_NAME
     */
    public void setEmpLastName(String value) {
        setAttributeInternal(EMPLASTNAME, value);
    }

    /**
     * Gets the attribute value for LONG_DESC using the alias name LongDesc.
     * @return the LONG_DESC
     */
    public String getLongDesc() {
        return (String) getAttributeInternal(LONGDESC);
    }

    /**
     * Sets <code>value</code> as attribute value for LONG_DESC using the alias name LongDesc.
     * @param value value to set the LONG_DESC
     */
    public void setLongDesc(String value) {
        setAttributeInternal(LONGDESC, value);
    }

    /**
     * Gets the attribute value for LOCAT_CODE using the alias name LocatCode.
     * @return the LOCAT_CODE
     */
    public String getLocatCode() {
        return (String) getAttributeInternal(LOCATCODE);
    }

    /**
     * Sets <code>value</code> as attribute value for LOCAT_CODE using the alias name LocatCode.
     * @param value value to set the LOCAT_CODE
     */
    public void setLocatCode(String value) {
        setAttributeInternal(LOCATCODE, value);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber2.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber2() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER2);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber2.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber2(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER2, value);
    }

    /**
     * Gets the attribute value for the calculated attribute EditTrans.
     * @return the EditTrans
     */
    public Integer getEditTrans() {
        byte entityState = this.getEntity(0).getEntityState();
        return new Integer(entityState);
        //return (Integer) getAttributeInternal(EDITTRANS);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute EditTrans.
     * @param value value to set the  EditTrans
     */
    public void setEditTrans(Integer value) {
        setAttributeInternal(EDITTRANS, value);
    }

    /**
     * Gets the attribute value for LAST_UPDATE_DATE using the alias name LastUpdateDate.
     * @return the LAST_UPDATE_DATE
     */
    public Timestamp getLastUpdateDate() {
        return (Timestamp) getAttributeInternal(LASTUPDATEDATE);
    }

    /**
     * Gets the attribute value for LAST_UPDATED_BY using the alias name LastUpdatedBy.
     * @return the LAST_UPDATED_BY
     */
    public String getLastUpdatedBy() {
        return (String) getAttributeInternal(LASTUPDATEDBY);
    }

    /**
     * Gets the attribute value for CREATED_BY using the alias name CreatedBy.
     * @return the CREATED_BY
     */
    public String getCreatedBy() {
        return (String) getAttributeInternal(CREATEDBY);
    }

    /**
     * Gets the attribute value for CREATION_DATE using the alias name CreationDate.
     * @return the CREATION_DATE
     */
    public Timestamp getCreationDate() {
        return (Timestamp) getAttributeInternal(CREATIONDATE);
    }

    /**
     * Gets the attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber3.
     * @return the OBJECT_VERSION_NUMBER
     */
    public Integer getObjectVersionNumber3() {
        return (Integer) getAttributeInternal(OBJECTVERSIONNUMBER3);
    }

    /**
     * Sets <code>value</code> as attribute value for OBJECT_VERSION_NUMBER using the alias name ObjectVersionNumber3.
     * @param value value to set the OBJECT_VERSION_NUMBER
     */
    public void setObjectVersionNumber3(Integer value) {
        setAttributeInternal(OBJECTVERSIONNUMBER3, value);
    }

    /**
     * Gets the view accessor <code>RowSet</code> UnitVO1.
     */
    public RowSet getUnitVO1() {
        return (RowSet) getAttributeInternal(UNITVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> EmpViewVVO1.
     */
    public RowSet getEmpViewVVO1() {
        return (RowSet) getAttributeInternal(EMPVIEWVVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> LocationVO1.
     */
    public RowSet getLocationVO1() {
        return (RowSet) getAttributeInternal(LOCATIONVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> SecControlVO1.
     */
    public RowSet getSecControlVO1() {
        return (RowSet) getAttributeInternal(SECCONTROLVO1);
    }

    /**
     * Gets the view accessor <code>RowSet</code> SecControlVO2.
     */
    public RowSet getSecControlVO2() {
        return (RowSet) getAttributeInternal(SECCONTROLVO2);
    }
}

