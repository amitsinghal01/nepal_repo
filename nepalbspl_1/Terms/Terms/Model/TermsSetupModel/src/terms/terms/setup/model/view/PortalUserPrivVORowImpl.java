package terms.terms.setup.model.view;

import java.sql.Timestamp;

import oracle.jbo.RowSet;
import oracle.jbo.server.EntityImpl;
import oracle.jbo.server.ViewRowImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Wed Apr 03 17:26:20 IST 2019
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class PortalUserPrivVORowImpl extends ViewRowImpl {


    public static final int ENTITY_PORTALUSERPRIVEO = 0;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        FromDate,
        NavId,
        NavName,
        NavStatus,
        ToDate,
        UserId,
        UserName,
        SecControlVO2;
        static AttributesEnum[] vals = null;
        ;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int FROMDATE = AttributesEnum.FromDate.index();
    public static final int NAVID = AttributesEnum.NavId.index();
    public static final int NAVNAME = AttributesEnum.NavName.index();
    public static final int NAVSTATUS = AttributesEnum.NavStatus.index();
    public static final int TODATE = AttributesEnum.ToDate.index();
    public static final int USERID = AttributesEnum.UserId.index();
    public static final int USERNAME = AttributesEnum.UserName.index();
    public static final int SECCONTROLVO2 = AttributesEnum.SecControlVO2.index();

    /**
     * This is the default constructor (do not remove).
     */
    public PortalUserPrivVORowImpl() {
    }

    /**
     * Gets PortalUserPrivEO entity object.
     * @return the PortalUserPrivEO
     */
    public EntityImpl getPortalUserPrivEO() {
        return (EntityImpl) getEntity(ENTITY_PORTALUSERPRIVEO);
    }

    /**
     * Gets the attribute value for FROM_DATE using the alias name FromDate.
     * @return the FROM_DATE
     */
    public Timestamp getFromDate() {
        return (Timestamp) getAttributeInternal(FROMDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for FROM_DATE using the alias name FromDate.
     * @param value value to set the FROM_DATE
     */
    public void setFromDate(Timestamp value) {
        setAttributeInternal(FROMDATE, value);
    }

    /**
     * Gets the attribute value for NAV_ID using the alias name NavId.
     * @return the NAV_ID
     */
    public String getNavId() {
        return (String) getAttributeInternal(NAVID);
    }

    /**
     * Sets <code>value</code> as attribute value for NAV_ID using the alias name NavId.
     * @param value value to set the NAV_ID
     */
    public void setNavId(String value) {
        setAttributeInternal(NAVID, value);
    }

    /**
     * Gets the attribute value for NAV_NAME using the alias name NavName.
     * @return the NAV_NAME
     */
    public String getNavName() {
        return (String) getAttributeInternal(NAVNAME);
    }

    /**
     * Sets <code>value</code> as attribute value for NAV_NAME using the alias name NavName.
     * @param value value to set the NAV_NAME
     */
    public void setNavName(String value) {
        setAttributeInternal(NAVNAME, value);
    }

    /**
     * Gets the attribute value for NAV_STATUS using the alias name NavStatus.
     * @return the NAV_STATUS
     */
    public String getNavStatus() {
        return (String) getAttributeInternal(NAVSTATUS);
    }

    /**
     * Sets <code>value</code> as attribute value for NAV_STATUS using the alias name NavStatus.
     * @param value value to set the NAV_STATUS
     */
    public void setNavStatus(String value) {
        setAttributeInternal(NAVSTATUS, value);
    }

    /**
     * Gets the attribute value for TO_DATE using the alias name ToDate.
     * @return the TO_DATE
     */
    public Timestamp getToDate() {
        return (Timestamp) getAttributeInternal(TODATE);
    }

    /**
     * Sets <code>value</code> as attribute value for TO_DATE using the alias name ToDate.
     * @param value value to set the TO_DATE
     */
    public void setToDate(Timestamp value) {
        setAttributeInternal(TODATE, value);
    }

    /**
     * Gets the attribute value for USER_ID using the alias name UserId.
     * @return the USER_ID
     */
    public String getUserId() {
        return (String) getAttributeInternal(USERID);
    }

    /**
     * Sets <code>value</code> as attribute value for USER_ID using the alias name UserId.
     * @param value value to set the USER_ID
     */
    public void setUserId(String value) {
        setAttributeInternal(USERID, value);
    }

    /**
     * Gets the attribute value for USER_NAME using the alias name UserName.
     * @return the USER_NAME
     */
    public String getUserName() {
        return (String) getAttributeInternal(USERNAME);
    }

    /**
     * Sets <code>value</code> as attribute value for USER_NAME using the alias name UserName.
     * @param value value to set the USER_NAME
     */
    public void setUserName(String value) {
        setAttributeInternal(USERNAME, value);
    }


    /**
     * Gets the view accessor <code>RowSet</code> SecControlVO2.
     */
    public RowSet getSecControlVO2() {
        return (RowSet) getAttributeInternal(SECCONTROLVO2);
    }
}

