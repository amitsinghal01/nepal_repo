package terms.terms.setup.ui.workarea.bean;

import java.util.HashMap;
import java.util.Map;

import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.nav.RichLink;

import oracle.adf.view.rich.component.rich.output.RichPanelCollection;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.adf.view.rich.event.QueryEvent;
import oracle.adf.view.rich.model.FilterableQueryDescriptor;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;
import oracle.jbo.domain.Char;

import oracle.ui.pattern.dynamicShell.TabContext;
import terms.terms.setup.ui.workarea.bean.TermsSetupUiWorkareaBean;

public class ApprovalPendencyBean {
    private RichTable aprtableBinding;
    private RichPanelCollection aprPcBinding;
    private RichSelectOneChoice searchFilterBinding;

    public ApprovalPendencyBean() {
    }

    public void openTabForAuth(ActionEvent actionEvent) {
        // Add event code here...
        RichLink link = (RichLink) actionEvent.getComponent();
        String TaskFlowId = link.getAttributes().get("TaskFlowId")!=null?link.getAttributes().get("TaskFlowId").toString():"ERP0000000031";
        String TaskFlowName =
            link.getAttributes().get("TaskFlowName") != null ? link.getAttributes().get("TaskFlowName").toString() :
            null;
        String TaskFlowSource =
            link.getAttributes().get("TaskFlowSource") != null ? link.getAttributes().get("TaskFlowSource").toString() :
            null;
        String DocumentNo =
            link.getAttributes().get("DocumentNo") != null ? link.getAttributes().get("DocumentNo").toString() : null;
        String DocAlias =
            link.getAttributes().get("DocAlias") != null ? link.getAttributes().get("DocAlias").toString() : null;
        if (TaskFlowSource != null && TaskFlowName != null && DocumentNo != null) {
            try {
                System.out.println("Task Flow Id :"+TaskFlowId+" "+TaskFlowName+" "+TaskFlowSource+" "+DocumentNo);
                OperationBinding binding = ADFUtils.findOperation("fetchSecurityValues");
                binding.getParamsMap().put("user_id", ADFUtils.resolveExpression("#{sessionScope.userId}"));
                binding.getParamsMap().put("file_name", TaskFlowName);
                binding.getParamsMap().put("file_id", TaskFlowId);
                binding.execute();
                if (binding.getResult() != null) {
                    String sec_string = binding.getResult().toString();
                    Map parameterMap = new HashMap();
                    //                parameterMap.put("viewString", sec_string.charAt(0));
                    parameterMap.put("editString", sec_string.charAt(1));
                    //                parameterMap.put("deleteString", sec_string.charAt(2));
                    //                parameterMap.put("addString", sec_string.charAt(3));
                    parameterMap.put("mode", "V");
                    //DCIteratorBinding cIteratorBinding = ADFUtils.findIterator("PendingApprovalVVO1Iterator");
                    //String formname =
                    //    cIteratorBinding.getCurrentRow().getAttribute("DocAlias") != null ?
                    //    (String) cIteratorBinding.getCurrentRow().getAttribute("DocAlias") : null;
                    String formname = DocAlias;
                    System.out.println("Form Name Value: "+formname+" "+sec_string.charAt(1));
                    if (formname != null) {
                        System.out.println("Form Name Value 1: "+formname);
                        if (formname.equals("IND") || formname.equals("IND_CAP")) {
                            String indNo = DocumentNo.substring(0, DocumentNo.indexOf("~"));
                            String amdNo = DocumentNo.substring(DocumentNo.indexOf("~")+1);
                            System.out.println("Indent Number :- "+indNo+" "+amdNo);
                            parameterMap.put("indNo", indNo);
                            parameterMap.put("AmdNo", amdNo);
                        } else if (formname.equals("PO") || formname.equals("PO_HO") || formname.equals("PO_IMP") || formname.equals("PO_STOCK") || formname.equals("PO_CAP")) {
                            String poNo = DocumentNo.substring(0, DocumentNo.indexOf("~"));
                            String amdNo = DocumentNo.substring(DocumentNo.indexOf("~")+1);
                            System.out.println("Po Number :- "+poNo+" "+amdNo);
                            parameterMap.put("PoNo", poNo);
                            parameterMap.put("AmdNo", amdNo);
                        } else if (formname.equals("REQ")) {
                            parameterMap.put("ReqSlipId", DocumentNo);
                        } else if (formname.equals("ISSUE")) {
                            parameterMap.put("issueId", DocumentNo);
                        } else if (formname.equals("CR")) {
                            parameterMap.put("ReqNumber", DocumentNo);
                        } else if (formname.equals("CT")) {
                            String joNo = DocumentNo.substring(0, DocumentNo.indexOf("~"));
                            String amdNo = DocumentNo.substring(DocumentNo.indexOf("~")+1);
                            parameterMap.put("JoNo", joNo);
                            parameterMap.put("AmdNo", amdNo);
                        } else if (formname.equals("MPO")) {
                            String ackNo = DocumentNo.substring(0, DocumentNo.indexOf("~"));
                            String amdNo = DocumentNo.substring(DocumentNo.indexOf("~")+1);
                            parameterMap.put("AckNumber", ackNo);
                            parameterMap.put("AmendAckNumber", amdNo);
                        } else if (formname.equals("RTN")) {
                            parameterMap.put("RetSlipId", DocumentNo);
                        } else if (formname.equals("ADVS")) {
                            parameterMap.put("AdvId", DocumentNo);
                        } else if (formname.equals("DELV")) {
                            String schNo = DocumentNo.substring(0, DocumentNo.indexOf("~"));
                            String amdNo = DocumentNo.substring(DocumentNo.indexOf("~")+1);
                            parameterMap.put("SchNo", schNo);
                            parameterMap.put("schdAmdNo", amdNo);
                        } 
                        System.out.println("mapvaluee iss=>" + parameterMap);
                        //                TabContext.getCurrentInstance().addOrSelectTab(TaskFlowName, TaskFlowSource, parameterMap);
                        TabContext.getCurrentInstance().addTab(TaskFlowName, TaskFlowSource, parameterMap);
                        //  TabContext.getCurrentInstance().addOrSelectTab(TaskFlowName, TaskFlowSource);
                        //            RequestContext.getCurrentInstance().getPageFlowScope().put("secString", TaskFlowSource) ;
                        //            System.out.println("Parameter Value is " + RequestContext.getCurrentInstance().getPageFlowScope().get("secString")+"    "+RequestContext.getCurrentInstance().getPageFlowScope());
                    }

                }
            } catch (TabContext.TabOverflowException e) {
                e.printStackTrace();
            }
        }
    }

    public void updateApprovalAuth(DialogEvent dialogEvent) {
        // Add event code here...
        DCBindingContainer dcb = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();    
        DCIteratorBinding dcItr = dcb.findIteratorBinding("PendingApprovalVVO1Iterator");
        RowSetIterator rsIter = dcItr.getRowSetIterator();
        Row rowObj = rsIter.getCurrentRow();
        
        if (dialogEvent.getOutcome() == DialogEvent.Outcome.yes) {
            System.out.println("Value For Approval status "+rowObj.getAttribute("AppSt"));
            if(rowObj.getAttribute("AppSt") != null && rowObj.getAttribute("AppSt").equals("Y")){
                OperationBinding binding = ADFUtils.findOperation("updateDocForDelegation");
                binding.getParamsMap().put("empCode", ADFUtils.resolveExpression("#{sessionScope.empCode}"));
                binding.getParamsMap().put("terminal", ADFUtils.clientIpAddress()+"/"+ADFUtils.clientHostName());
                binding.execute();
                String bindStatus = binding.getResult() != null ? binding.getResult().toString().substring(0,1) : "N";
                System.out.println("Binding Value For Authority "+bindStatus+"  "+binding.getResult().toString().substring(0));
                if (bindStatus != null && bindStatus.equals("Y")){
                    //ADFUtils.findOperation("Commit").execute();
                    ADFUtils.showMessage("Doucument update successfully.", 2);
                }else if(binding.getResult() != null && !binding.getResult().equals("Y")){
                    ADFUtils.showMessage("Trouble to get update document. Try again. "+binding.getResult().toString().substring(1, binding.getResult().toString().length()), 0);
                }
                ADFUtils.findIterator("PendingApprovalVVO1Iterator").executeQuery();
                ADFUtils.findIterator("CountPendingApprovalVVO1Iterator").executeQuery();
                AdfFacesContext.getCurrentInstance().addPartialTarget(aprPcBinding);
                AdfFacesContext.getCurrentInstance().addPartialTarget(aprtableBinding);
            }else{
                ADFUtils.showMessage("Approval for this document is blocked from outside. You may click on Get Info button and approve the document.", 1);
            }
        }
    }

    public void setAprtableBinding(RichTable aprtableBinding) {
        this.aprtableBinding = aprtableBinding;
    }

    public RichTable getAprtableBinding() {
        return aprtableBinding;
    }

    public void setAprPcBinding(RichPanelCollection aprPcBinding) {
        this.aprPcBinding = aprPcBinding;
    }

    public RichPanelCollection getAprPcBinding() {
        return aprPcBinding;
    }

    public void filterTableByDocName(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
//        RichTable tbl = this.getAprtableBinding();
//        FilterableQueryDescriptor filterQD = (FilterableQueryDescriptor)tbl.getFilterModel();
//        Map filterCriteria = filterQD.getFilterCriteria();
//        if (searchFilterBinding.getValue().equals("All")){
//            filterCriteria.put("DocName", "");
//            getAprtableBinding().queueEvent(new QueryEvent(getAprtableBinding(), filterQD));
//            AdfFacesContext.getCurrentInstance().addPartialTarget(this.getAprtableBinding());
//        }else{
//            filterCriteria.put("DocName", searchFilterBinding.getValue());
//            getAprtableBinding().queueEvent(new QueryEvent(getAprtableBinding(), filterQD));
//            AdfFacesContext.getCurrentInstance().addPartialTarget(this.getAprtableBinding());
//        }
        if (searchFilterBinding.getValue() != null ){
            OperationBinding binding = ADFUtils.findOperation("filterDataAppPendency");
            System.out.println("Value For DocName Binding : "+searchFilterBinding.getValue());
            if (searchFilterBinding.getValue().equals("All")){
                binding.getParamsMap().put("DocName", "");
            }
            else   { 
                binding.getParamsMap().put("DocName", searchFilterBinding.getValue());
            }
            binding.execute();
        }
    }

    public void setSearchFilterBinding(RichSelectOneChoice searchFilterBinding) {
        this.searchFilterBinding = searchFilterBinding;
    }

    public RichSelectOneChoice getSearchFilterBinding() {
        return searchFilterBinding;
    }

    public String getInfoAction() {
        // Add event code here...
        DCBindingContainer dcb = (DCBindingContainer) BindingContext.getCurrent().getCurrentBindingsEntry();    
        DCIteratorBinding dcItr = dcb.findIteratorBinding("PendingApprovalVVO1Iterator");
        RowSetIterator rsIter = dcItr.getRowSetIterator();
        Row rowObj = rsIter.getCurrentRow();
        System.out.println("Value For Get Info status "+rowObj.getAttribute("GetInfoSt"));
        if(rowObj.getAttribute("GetInfoSt") != null && rowObj.getAttribute("GetInfoSt").equals("Y")){
            return "gotoDecision";
        }else{
            ADFUtils.showMessage("Get Info for this document is blocked from outside. You may try approve outside or contact ERP Administrator.", 1);
        }
        return null;
    }
}
