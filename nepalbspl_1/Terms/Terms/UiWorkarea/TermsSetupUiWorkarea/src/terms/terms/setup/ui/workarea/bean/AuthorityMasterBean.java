package terms.terms.setup.ui.workarea.bean;

import java.math.BigDecimal;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;
import javax.faces.validator.ValidatorException;

import oracle.adf.model.OperationBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;


public class AuthorityMasterBean {
    private RichTable authorityMasterTableBinding;
    private String editAction = "V";
    private RichInputText limitFromBinding;
    private RichInputText limitToBinding;

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public AuthorityMasterBean() {
    }

    public void DeletepopupDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            OperationBinding op = null;
            ADFUtils.findOperation("Delete").execute();
            op = (OperationBinding) ADFUtils.findOperation("Commit");
            if (getEditAction().equals("V")) {
                Object rst = op.execute();
            }
            System.out.println("Record Delete Successfully");
            if (op.getErrors().isEmpty()) {
                FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                Message.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            } else if (!op.getErrors().isEmpty()) {
                OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
                Object rstr = opr.execute();
                FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, Message);
            }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(authorityMasterTableBinding);
    }

    public void setAuthorityMasterTableBinding(RichTable authorityMasterTableBinding) {
        this.authorityMasterTableBinding = authorityMasterTableBinding;
    }

    public RichTable getAuthorityMasterTableBinding() {
        return authorityMasterTableBinding;
    }

    public void LimitAuthority(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if (object != null) {
            if (getLimitFromBinding().getValue() != null && getLimitToBinding().getValue() != null) {
                BigDecimal LimitFrom = (BigDecimal) getLimitFromBinding().getValue();
                BigDecimal limitTo = (BigDecimal) getLimitToBinding().getValue();
                if (LimitFrom.compareTo(limitTo) == 1) {
                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                  "Limit From must be less than Limit To.", null));
                }
            }

        }
    }

    public void setLimitFromBinding(RichInputText limitFromBinding) {
        this.limitFromBinding = limitFromBinding;
    }

    public RichInputText getLimitFromBinding() {
        return limitFromBinding;
    }

    public void setLimitToBinding(RichInputText limitToBinding) {
        this.limitToBinding = limitToBinding;
    }

    public RichInputText getLimitToBinding() {
        return limitToBinding;
    }

    public void bulkAuthorityPopDialogListner(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().toString().equalsIgnoreCase("ok")) {
            ADFUtils.findOperation("inserteBulkAuthorityData").execute();
        }
    }

    public void populateBulkAuthority(ActionEvent actionEvent) {
        ADFUtils.findOperation("populateBulkAuthorityData").execute();
    }

    public void resetBulkAuthorityInputs(ActionEvent actionEvent) {
        ADFUtils.findOperation("resetTempChckAuthoVO").execute();
    }

    @SuppressWarnings("unchecked")
    public void selectNewAuthority(ActionEvent actionEvent) {
        OperationBinding op= (OperationBinding) ADFUtils.findOperation("selectDeselectNewAuthority");
        op.getParamsMap().put("value", "Y");
        op.execute();
    }

    @SuppressWarnings("unchecked")
    public void deselectNewAuthority(ActionEvent actionEvent) {
        OperationBinding op= (OperationBinding) ADFUtils.findOperation("selectDeselectNewAuthority");
        op.getParamsMap().put("value", "N");
        op.execute();
    }
}
