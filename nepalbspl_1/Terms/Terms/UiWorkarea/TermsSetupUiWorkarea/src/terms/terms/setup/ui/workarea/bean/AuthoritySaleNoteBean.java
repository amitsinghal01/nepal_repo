package terms.terms.setup.ui.workarea.bean;

import java.math.BigDecimal;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import javax.faces.validator.ValidatorException;

import oracle.adf.model.OperationBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

public class AuthoritySaleNoteBean {
    private RichTable authSaleNoteTableBinding;
    private String editAction="V";
    private RichInputText limitFromBinding;
    private RichInputText limitToBinding;


    public AuthoritySaleNoteBean() {
    }

    public void deletePopUpDL(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().name().equals("ok"))
                    {
                    OperationBinding op = null;
                    ADFUtils.findOperation("Delete").execute();
                    op = (OperationBinding) ADFUtils.findOperation("Commit");
                    if (getEditAction().equals("V")) {
                    Object rst = op.execute();
                    }
                    System.out.println("Record Delete Successfully");
                    if(op.getErrors().isEmpty()){
                    FacesMessage Message = new FacesMessage("Record Deleted Successfully");
                    Message.setSeverity(FacesMessage.SEVERITY_INFO);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);
                    } else if (!op.getErrors().isEmpty())
                    {
                    OperationBinding opr = (OperationBinding) ADFUtils.findOperation("Rollback");
                    Object rstr = opr.execute();
                    FacesMessage Message = new FacesMessage("Record cannot be deleted. Child record found.");
                    Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext fc = FacesContext.getCurrentInstance();
                    fc.addMessage(null, Message);
                    }
                    }
                AdfFacesContext.getCurrentInstance().addPartialTarget(authSaleNoteTableBinding);    }

    public void setAuthSaleNoteTableBinding(RichTable authSaleNoteTableBinding) {
        this.authSaleNoteTableBinding = authSaleNoteTableBinding;
    }

    public RichTable getAuthSaleNoteTableBinding() {
        return authSaleNoteTableBinding;
    }

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public String getEditAction() {
        return editAction;
    }

    public void setLimitFromBinding(RichInputText limitFromBinding) {
        this.limitFromBinding = limitFromBinding;
    }

    public RichInputText getLimitFromBinding() {
        return limitFromBinding;
    }

    public void setLimitToBinding(RichInputText limitToBinding) {
        this.limitToBinding = limitToBinding;
    }

    public RichInputText getLimitToBinding() {
        return limitToBinding;
    }

    public void limitToValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if(object!=null && limitFromBinding.getValue()!=null)
        {
        BigDecimal limit_to =(BigDecimal)object;
        BigDecimal limit_from =(BigDecimal) limitFromBinding.getValue();
        System.out.println("Limit To limit_to.compareTo(limit_from)|||"+limit_to.compareTo(limit_from));
        if(limit_to.compareTo(limit_from) <0 )
        {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,"Limit To must be greater than or equals to Limit From","Please Select Another Date."));  
        }
        }

    }

    public void limitFromBinding(FacesContext facesContext, UIComponent uIComponent, Object object) {
        if(object!=null && limitToBinding.getValue()!=null)
        {
            BigDecimal limit_from =(BigDecimal)object;
            BigDecimal limit_to =(BigDecimal) limitToBinding.getValue();
            System.out.println("Limit from limit_from.compareTo(limit_to)||||"+limit_from.compareTo(limit_to));
            if(limit_from.compareTo(limit_to) >0)
            {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,"Limit From must be less than or equals to Limit To","Please Select Another Date."));  
            }
        
        }

    }
}
