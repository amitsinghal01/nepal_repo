package terms.terms.setup.ui.workarea.bean;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.model.FilterableQueryDescriptor;

import oracle.binding.OperationBinding;

import oracle.jbo.domain.Date;

public class CreateReportFactoryPrivilegeBean {
    private RichTable portalPrivTableBinding;
    private Date currentDate;
    private String mode =
        ADFUtils.resolveExpression("#{pageFlowScope.PARAM_MODE}") != null ?
        ADFUtils.resolveExpression("#{pageFlowScope.PARAM_MODE}").toString() : null;
    private RichInputComboboxListOfValues moduleNameBinding;
    private RichInputComboboxListOfValues navIdBinding;


    public CreateReportFactoryPrivilegeBean() {
    }

    public void moduleVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce.getNewValue() != null && vce.getNewValue().toString().trim().length() > 0) {
            resetTbaleFilters(portalPrivTableBinding);
            OperationBinding binding = ADFUtils.findOperation("fetchPortalPrivilegeDetails");
            binding.getParamsMap().put("module_Id", vce.getNewValue().toString());
            binding.execute();
        }
    }

    public void setPortalPrivTableBinding(RichTable portalPrivTableBinding) {
        this.portalPrivTableBinding = portalPrivTableBinding;
    }

    public RichTable getPortalPrivTableBinding() {
        return portalPrivTableBinding;
    }
    
    public String resetTbaleFilters(RichTable tableBinding)
    {
        FilterableQueryDescriptor filterModel = (FilterableQueryDescriptor) tableBinding.getFilterModel();
        if (filterModel != null && filterModel.getFilterCriteria() != null)
            filterModel.getFilterCriteria().clear();
        return null;
    }
    
    
    public String saveButtonAction() {
        OperationBinding binding = ADFUtils.findOperation("portalPrivilegedeleteBeforeCommit");
        binding.execute();
        mode = "V";
        return "save_go";
    }

   
    public String userPriviledgeEditAction() {
        System.out.println("mode value in edit iss =" + mode);
        mode = "E";
        ADFUtils.setEL("#{pageFlowScope.mode}","E");
        return null;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getMode() {
        return mode;
    }

    public void populateDataAL(ActionEvent actionEvent) {
//        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
//        if (vce.getNewValue() != null && vce.getNewValue().toString().trim().length() > 0) {
//            resetTbaleFilters(portalPrivTableBinding);
//            OperationBinding binding = ADFUtils.findOperation("fetchPortalPrivilegeDetails");
//            binding.getParamsMap().put("module_Id", vce.getNewValue().toString());
//            binding.execute();
//        }
        
        if(moduleNameBinding.getValue()!=null)
        {
        resetTbaleFilters(portalPrivTableBinding);
        OperationBinding binding = ADFUtils.findOperation("fetchPortalPrivilegeDetails");
        binding.getParamsMap().put("module_Id", moduleNameBinding.getValue());
        binding.execute();

        
        }
        
        
    }

    public void setModuleNameBinding(RichInputComboboxListOfValues moduleNameBinding) {
        this.moduleNameBinding = moduleNameBinding;
    }

    public RichInputComboboxListOfValues getModuleNameBinding() {
        return moduleNameBinding;
    }

    public void setNavIdBinding(RichInputComboboxListOfValues navIdBinding) {
        this.navIdBinding = navIdBinding;
    }

    public RichInputComboboxListOfValues getNavIdBinding() {
        return navIdBinding;
    }
}
