package terms.terms.setup.ui.workarea.bean;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

import oracle.ui.pattern.dynamicShell.TabContext;

import org.apache.myfaces.trinidad.event.SelectionEvent;

public class ListSelectionBean {
    public ListSelectionBean() {
    }

    public void listSelectionPendingApproval(SelectionEvent selectionEvent) {
        // Add event code here...
        try{
            ADFUtils.invokeEL("#{bindings.CountPendingApprovalVVO1.treeModel.makeCurrent}", new Class[] { SelectionEvent.class }, new Object[] {
            selectionEvent });            
            System.out.println("Value for plath init : ");
            Row r = (Row) ADFUtils.evaluateEL("#{bindings.CountPendingApprovalVVO1.currentRow}");
            String plath = (String) r.getAttribute("DocSt");
            System.out.println("Value for plath : "+plath);
            OperationBinding op1 = ADFUtils.findOperation("setDocStApprPend");
            op1.getParamsMap().put("doc_st", ADFUtils.resolveExpression("#{item.bindings.DocSt.inputValue}"));
            op1.execute();
            TabContext.getCurrentInstance().addTab("Approval Pendency ", "/WEB-INF/terms/terms/setup/ui/workarea/flow/ApprovalPendencyFlow.xml#ApprovalPendencyFlow");
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
