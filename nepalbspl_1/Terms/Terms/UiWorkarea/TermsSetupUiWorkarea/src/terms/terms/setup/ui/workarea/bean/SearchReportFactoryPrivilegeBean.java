package terms.terms.setup.ui.workarea.bean;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

import terms.terms.setup.model.view.SearchPortalUserPrivVORowImpl;

public class SearchReportFactoryPrivilegeBean {
    public SearchReportFactoryPrivilegeBean() {
    }

    public String viewReportFactoryPriviledge() {
        DCIteratorBinding itr = ADFUtils.findIterator("SearchPortalUserPrivVO1Iterator");
        OperationBinding binding = ADFUtils.findOperation("filterPortalPriviledge");
        binding.getParamsMap().put("usr_id", ((SearchPortalUserPrivVORowImpl)itr.getCurrentRow()).getUserId());
        binding.execute();
        AdfFacesContext.getCurrentInstance().getPageFlowScope().put("PARAM_MODE", "V");
        ADFUtils.setEL("#{pageFlowScope.mode}","V");
        return "GoToNext";
    }

    public String addReportFactoryPrivAction() {

        ADFUtils.setEL("#{pageFlowScope.mode}","C");
        OperationBinding binding = ADFUtils.findOperation("filterPortalPriviledge");
        binding.getParamsMap().put("usr_id", "-1");
        binding.execute();
        AdfFacesContext.getCurrentInstance().getPageFlowScope().put("PARAM_MODE", "A");
        return "GoToNext";
    }
}
