package terms.terms.setup.ui.workarea.bean;

import oracle.adf.model.binding.DCIteratorBinding;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

import terms.terms.setup.model.view.SearchUserPriviledgeVORowImpl;

public class SearchUserPriviledgeBean {
    public SearchUserPriviledgeBean() {
    }

    public String viewUsrPriviledge() {
        DCIteratorBinding itr = ADFUtils.findIterator("SearchUserPrivVOIterator");
        OperationBinding binding = ADFUtils.findOperation("filterUserPriviledge");
        binding.getParamsMap().put("usr_id", ((SearchUserPriviledgeVORowImpl)itr.getCurrentRow()).getUserId());
        binding.execute();
        AdfFacesContext.getCurrentInstance().getPageFlowScope().put("PARAM_MODE", "V");
        return "go";
    }

    public String addUsrPriviledge() {
        OperationBinding binding = ADFUtils.findOperation("filterUserPriviledge");
        binding.getParamsMap().put("usr_id", "-1");
        binding.execute();
        AdfFacesContext.getCurrentInstance().getPageFlowScope().put("PARAM_MODE", "A");
        return "go";
    }
}
