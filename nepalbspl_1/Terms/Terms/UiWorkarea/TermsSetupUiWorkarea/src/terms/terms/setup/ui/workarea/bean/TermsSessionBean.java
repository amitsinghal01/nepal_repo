package terms.terms.setup.ui.workarea.bean;

import java.io.IOException;

import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import javax.faces.validator.ValidatorException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputText;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.OperationBinding;

import terms.terms.setup.model.applicationModule.TermsSetupAMImpl;

public class TermsSessionBean extends HttpServlet{
    private RichInputText usernameBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText unitNameBinding;
    private RichInputText passwordBinding;
    private RichPopup changePasswordPopup;
    private RichInputText currentPasswordBinding;
    private RichInputText newPasswordBinding;
    private RichInputText confirmPasswordBinding;
    private RichInputComboboxListOfValues finyearBinding;

    public TermsSessionBean() {
        super();
    }
    private static final ADFLogger logger = ADFLogger.createADFLogger(TermsSessionBean.class);
    
    public HttpSession fetchSession() 
    {
        HttpServletRequest request =
            (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        HttpSession httpSession = request.getSession(true);
        logger.info("Session value with true : "+httpSession);
        
        HttpSession httpSessionfalse = request.getSession(false);
        logger.info("Session value with false : "+httpSessionfalse);
        return httpSession;
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    {
//         HttpSession newsession = request.getSession(false);
//            if (newsession != null) 
//            {
//                 newsession.invalidate();
//            }
           try
            {	
                    response.sendRedirect("/TermsApplication-TermsSetupUiWorkarea-context-root/faces/Logout");
            }
            catch(IOException ioe)
            {
            // TODO: Add catch code
            ioe.printStackTrace();
            }
    }
    
    /**Modified By:Aakash Kumar
     * Modified On:24-FEB-2020
     * Remarks:
     */
    
    public String authenticateLogin() 
    {
        logger.info("authenticateLogin");
        String username = (String) getUsernameBinding().getValue();
        String currentPassword =getCurrentPasswordBinding().getValue() != null ? getCurrentPasswordBinding().getValue().toString() : "";
        //logger.info("Aakash----Username :- " + username+"currentPassword"+currentPassword);
        String password = "";
        String passtype = "PP";
        if (!currentPassword.isEmpty()) 
        {
            password = currentPassword;
            passtype = "CP";
           //System.out.println("password & passtype value 1 " + password + "  " + passtype);
        } else {
            password = (String) getPasswordBinding().getValue();
            passtype = "PP";
           //System.out.println("password & passtype value 2 " + password + "  " + passtype);
    }
        OperationBinding op = ADFUtils.findOperation("authenticateUserPassword");
        op.getParamsMap().put("username", username);
        op.getParamsMap().put("password", password);
        op.getParamsMap().put("pwtype", passtype);
        Object rst = op.execute();
        HttpSession fetchSession = fetchSession();
        logger.info("Return Value " + rst.toString());
    try {
        //***************Get Employee ID and User ID*******************//
        OperationBinding op1 = ADFUtils.findOperation("findUserCode");
        op1.getParamsMap().put("username", username);
        Object rst1 = op1.execute();
        OperationBinding op2 = ADFUtils.findOperation("findUserId");
        op2.getParamsMap().put("username", username);
        Object rst2 = op2.execute();
        if (rst1.toString() != "Not Found") {
            //AdfFacesContext.getCurrentInstance().getPageFlowScope().put("empCode", rst1.toString());
            fetchSession.setAttribute("empCode", rst1.toString());
        } else if (rst1.toString() == "Not Found") {
            ADFUtils.showMessage("System not able to find out current user employee code.", 0);
            return null;
        }

        if (rst2.toString() != "Not Found") {
            fetchSession.setAttribute("userId", rst2.toString());
        } else if (rst2.toString() == "Not Found") {
            ADFUtils.showMessage("System not able to find out current user id.", 0);
            return null;
        }

        if (getFinyearBinding().getValue().toString() == null) {
            ADFUtils.showMessage("System not able to find out current financial year.", 0);
            return null;
        }
        
        OperationBinding portApexReport = ADFUtils.findOperation("pathReportFactory");
        portApexReport.execute();
        
//        OperationBinding portApexReport = ADFUtils.findOperation("portApexReport");
//        portApexReport.execute();
       
       // ============================= Rachit Mishra =====================
        FacesContext facesCtx = FacesContext.getCurrentInstance();
        ExternalContext extCtx = facesCtx.getExternalContext();
        HttpSession session = (HttpSession)extCtx.getSession(false);
        String appURL="";
        String portNo="";
        if(portApexReport.getResult() !=null){
             portNo= portApexReport.getResult().toString();
            }
        if(session !=null){
                HttpServletRequest req = (HttpServletRequest)extCtx.getRequest();
                 appURL =
                    "http://" + req.getServerName() + ":"+portNo +"/apex/f?p=101";
                //http://10.0.6.201:8086/apex/f?p=101
            }
       
        if (appURL != null && appURL.length()>0) {
            fetchSession.setAttribute("repFacPath",appURL);
        } 
        //==================================08-10-2020=====================
        
//        if (bindpath.getResult().toString() != null) {
//            fetchSession.setAttribute("repFacPath",bindpath.getResult().toString());
//        } 
        
      

        if (rst.toString() != "Success") 
        {
            if (op.getErrors().isEmpty()) 
            {
                ADFUtils.showMessage(rst.toString(), 0);
                //Failure Attempt Audit
                if(getUsernameBinding().getValue() != null && getUnitCodeBinding().getValue() != null &&
                                                                        fetchSession.getId().toString() != null)
                {         
                    OperationBinding binding = ADFUtils.findOperation("createUserSession");
                    binding.getParamsMap().put("userId", ADFUtils.resolveExpression("#{sessionScope.userId}"));
                    binding.getParamsMap().put("sessionId", fetchSession.getId().toString());
                    binding.getParamsMap().put("unitCode", getUnitCodeBinding().getValue().toString());
                    binding.getParamsMap().put("clientIp", getClientIpAddress()+"/"+getClientCompName());
                    binding.getParamsMap().put("successValue", "No");
                    Object ob = binding.execute();
                    logger.info("Binding Result : "+binding.getResult().toString()+" "+System.getProperty("user.name"));
                    if (binding.getResult() != null && binding.getResult().toString() == "Y")
                    {
                        OperationBinding opCommit = ADFUtils.findOperation("Commit");
                        opCommit.execute();
                    }
                }
                //***************************************************//
            } else {
                ADFUtils.showMessage(op.getErrors().toString(), 0);
            }
            
        } 
        else if (rst.toString() == "Success") 
        {
            fetchSession.setAttribute("userName", getUsernameBinding().getValue().toString());
            fetchSession.setAttribute("unitCode", getUnitCodeBinding().getValue().toString());
            fetchSession.setAttribute("unitName", getUnitNameBinding().getValue().toString());
            fetchSession.setAttribute("finYear",  getFinyearBinding().getValue().toString());
            
            
            
            OperationBinding op0 = ADFUtils.findOperation("firstLogon");
            op0.getParamsMap().put("username", username);
            Object rst0 = op0.execute();
            logger.info("Return Value First Logon" + rst0.toString());
            fetchSession.setAttribute("fLogon", rst0.toString());
            
            logger.info("Session : "+ADFUtils.resolveExpression("#{sessionScope.userId}")+" "
                                           +ADFUtils.resolveExpression("#{sessionScope.userName}")+" "
                                           +ADFUtils.resolveExpression("#{sessionScope.unitCode}")+" "
                                           +ADFUtils.resolveExpression("#{sessionScope.unitName}")+" "
                                           +ADFUtils.resolveExpression("#{sessionScope.finYear}")+" "
                                           +ADFUtils.resolveExpression("#{sessionScope.empCode}"));
            if(ADFUtils.resolveExpression("#{sessionScope.userId}")   != null &&
               ADFUtils.resolveExpression("#{sessionScope.userName}") != null &&
               ADFUtils.resolveExpression("#{sessionScope.unitCode}") != null &&
               ADFUtils.resolveExpression("#{sessionScope.unitName}") != null &&
               ADFUtils.resolveExpression("#{sessionScope.finYear}")  != null &&
               ADFUtils.resolveExpression("#{sessionScope.empCode}")  != null   )
            {         
                OperationBinding binding = ADFUtils.findOperation("createUserSession");
                binding.getParamsMap().put("userId", ADFUtils.resolveExpression("#{sessionScope.userId}"));
                binding.getParamsMap().put("sessionId", fetchSession.getId().toString());
                binding.getParamsMap().put("unitCode", ADFUtils.resolveExpression("#{sessionScope.unitCode}"));
                binding.getParamsMap().put("clientIp", getClientIpAddress()+"/"+getClientCompName());
                binding.getParamsMap().put("successValue", "Yes");
                Object ob = binding.execute();
                TermsSetupUiWorkareaBean.setFinyearSessionParameters(getFinyearBinding().getValue().toString());
                TermsSetupUiWorkareaBean.setUnitSessionParameters(getUnitCodeBinding().getValue().toString(), 
                                                                                              getUnitNameBinding().getValue().toString());
                TermsSetupUiWorkareaBean.setUserSessionParameters(getUsernameBinding().getValue().toString(),
                                                                                      rst1!=null?rst1.toString():null,
                                                                                      rst2!=null?rst2.toString():null);
                
                usernameBinding.setValue("");
                unitCodeBinding.setValue("");
                passwordBinding.setValue("");
                finyearBinding.setValue("");
                //unitNameBinding.setValue("");
                logger.info("Binding Result : "+binding.getResult().toString());
                if (binding.getResult() != null && binding.getResult().toString() == "Y"){
                    OperationBinding opCommit = ADFUtils.findOperation("Commit");
                    opCommit.execute();
                    return "globalWelcome";
                }else if(binding.getResult() != null && binding.getResult().toString() == "N")
                {
                    OperationBinding opCommit = ADFUtils.findOperation("Commit");
                    opCommit.execute();
                    return "globalWelcome";
                }
                return null;  
            }
            return null;
        }
        
    } catch (Exception e) {
        e.printStackTrace();
    }
    return null;
    }
    
    public void setUsernameBinding(RichInputText usernameBinding) {
        this.usernameBinding = usernameBinding;
    }

    public RichInputText getUsernameBinding() {
        return usernameBinding;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setUnitNameBinding(RichInputText unitNameBinding) {
        this.unitNameBinding = unitNameBinding;
    }

    public RichInputText getUnitNameBinding() {
        return unitNameBinding;
    }

    public void setPasswordBinding(RichInputText passwordBinding) {
        this.passwordBinding = passwordBinding;
    }

    public RichInputText getPasswordBinding() {
        return passwordBinding;
    }


    public String onLogout() {
        // Add event code here...
        if(ADFUtils.resolveExpression("#{pageFlowScope.userId}")   != null &&
           ADFUtils.resolveExpression("#{pageFlowScope.userName}") != null &&
           ADFUtils.resolveExpression("#{pageFlowScope.unitCode}") != null &&
           ADFUtils.resolveExpression("#{pageFlowScope.unitName}") != null &&
           ADFUtils.resolveExpression("#{pageFlowScope.finYear}")  != null &&
           ADFUtils.resolveExpression("#{pageFlowScope.empCode}")  != null   ){  
            HttpSession fetchSession = fetchSession();
            OperationBinding binding = ADFUtils.findOperation("logoutUserSession");
            binding.getParamsMap().put("userId", ADFUtils.resolveExpression("#{pageFlowScope.userId}"));
            binding.getParamsMap().put("sessionId", fetchSession.getId().toString());
            binding.execute();
            OperationBinding opCommit = ADFUtils.findOperation("Commit");
            opCommit.execute();
        }
        HttpServletRequest request =
            (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        HttpSession httpSession = request.getSession(true);
        doPost(request, null);
        return "globalLogout";
    }
    
    public void changePasswordButtonAL(ActionEvent actionEvent) 
    {
        try {

            String username =  getUsernameBinding().getValue() != null ?
                getUsernameBinding().getValue().toString() : "";
            
            String unitCode =  getUnitCodeBinding().getValue() != null ?
                getUnitCodeBinding().getValue().toString() : "";

           // System.out.println("Username :- " + username + " " + unitCode);
            if (username.isEmpty()) {
                ADFUtils.showMessage("Username must be enter to change password.", 0);
                } else if(unitCode.isEmpty()){
                    ADFUtils.showMessage("Unit Code must be enter to change password.", 0);         
                }else {
                OperationBinding op1 = ADFUtils.findOperation("findUserCode");
                op1.getParamsMap().put("username", username);
                Object rst1 = op1.execute();
                //System.out.println("Return Value Emp Code AL" + rst1.toString());
                if (rst1.toString() == "Not Found") {
                    ADFUtils.showMessage("Username not found. Try Again.", 0);
                } else {
                    ADFUtils.showPopup(getChangePasswordPopup());
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setChangePasswordPopup(RichPopup changePasswordPopup) {
        this.changePasswordPopup = changePasswordPopup;
    }

    public RichPopup getChangePasswordPopup() {
        return changePasswordPopup;
    }

    public void popupConfirmButtonAL(ActionEvent actionEvent) 
    {
        String currentPassword = getCurrentPasswordBinding().getValue().toString();
        String newPassword = getNewPasswordBinding().getValue().toString();
        String confirmPassword = getConfirmPasswordBinding().getValue().toString();
        
       // System.out.println("The Confirm Button-currentPassword->>"+currentPassword+"newPassword:"+newPassword+"confirmPassword:"+confirmPassword);
        if(currentPassword!=null && newPassword!=null && confirmPassword!=null)
        {
//            OperationBinding op = ADFUtils.findOperation("checkConfirmPass");
//            op.getParamsMap().put("username", getUsernameBinding().getValue().toString());
//            op.getParamsMap().put("currPass", confirmPassword);
//            Object rs = op.execute();
//            System.out.println("The New Result is----->>"+op.getResult());
            try 
            {
//                if(op.getResult()!=null && op.getResult().equals("F"))
//                {
                    System.out.println("Start change password process.");
                    if(!newPassword.equals(confirmPassword)){
                        ADFUtils.showMessage("New Password and Confirm Password didn't match. Try again.", 0);
                    }
                    else
                    {
                        OperationBinding binding = ADFUtils.findOperation("setNewPassword");
                        binding.getParamsMap().put("username", getUsernameBinding().getValue().toString());
                        binding.getParamsMap().put("newpassword", newPassword);
                        Object rst = binding.execute();

                        if(rst.toString().equals("Success") && binding.getErrors().isEmpty() && binding.getResult()!=null)
                        {
                            OperationBinding op2 = ADFUtils.findOperation("findUserId");
                            op2.getParamsMap().put("username", getUsernameBinding().getValue().toString());
                            Object rst2 = op2.execute();
                            String userId=null;
                            if (rst2.toString() != "Not Found") 
                            {
                                userId=rst2.toString();
                                logger.info("New User Id is------"+userId);
                            }
                            
                            OperationBinding binding1 = ADFUtils.findOperation("setChPassHistory");
                            binding1.getParamsMap().put("username",getUsernameBinding().getValue().toString());
                            binding1.getParamsMap().put("newpassword", newPassword);
                            binding1.getParamsMap().put("oldpassword", currentPassword);
                            binding1.getParamsMap().put("terminal", ADFUtils.clientIpAddress()+"/"+ADFUtils.clientHostName());
                            binding1.getParamsMap().put("userid", userId);
                            binding1.execute();
                            ADFUtils.findOperation("Commit").execute();
                            ADFUtils.showMessage("Password Changed Successfully.", 2);
                            getChangePasswordPopup().cancel();
                            
                        }else{
                            ADFUtils.showMessage("System having trouble to change password. Try Again.", 2);
                        }
                    }  
//                }else{
//                    currentPasswordBinding.setValue(null);
//                    newPasswordBinding.setValue(null);
//                    confirmPasswordBinding.setValue(null);
//                    ADFUtils.showMessage("Current Password was not match.!!",0);
//                    AdfFacesContext.getCurrentInstance().addPartialTarget(changePasswordPopup); 
//                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        
    }

    public void popupCancelButtonAL(ActionEvent actionEvent) {
        getChangePasswordPopup().hide();
    }

    public void setCurrentPasswordBinding(RichInputText currentPasswordBinding) {
        this.currentPasswordBinding = currentPasswordBinding;
    }

    public RichInputText getCurrentPasswordBinding() {
        return currentPasswordBinding;
    }

    public void setNewPasswordBinding(RichInputText newPasswordBinding) {
        this.newPasswordBinding = newPasswordBinding;
    }

    public RichInputText getNewPasswordBinding() {
        return newPasswordBinding;
    }

    public void setConfirmPasswordBinding(RichInputText confirmPasswordBinding) {
        this.confirmPasswordBinding = confirmPasswordBinding;
    }

    public RichInputText getConfirmPasswordBinding() {
        return confirmPasswordBinding;
    }

    public void userNameVCE(ValueChangeEvent valueChangeEvent) 
    {
        logger.info("Enter in VCE Before Process Update");
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        System.out.println("Enter in VCE After Process Update");
        String newValue = (String) valueChangeEvent.getNewValue();
        //System.out.println("new value in vcll iss=" + newValue);
        try {
            String username = newValue;
            OperationBinding op1 = ADFUtils.findOperation("findUserCode");
            op1.getParamsMap().put("username", username);
            Object rst1 = op1.execute();
            //System.out.println("Return Value Emp Code VCE" + rst1.toString());
            if (rst1.toString() == "Not Found") {
                FacesMessage message = new FacesMessage("Username not found. Try Again.");
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(usernameBinding.getClientId(), message);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public String getClientIpAddress() {
        try {
            String clientIpAddress =
                ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest()).getRemoteAddr();
            return clientIpAddress;
        } catch (Exception e) {
            // TODO: Add catch code
            e.printStackTrace();
        }
        return null;
    }

    public String getClientCompName() {
        try {
            HttpServletRequest request =
                (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
            InetAddress i = InetAddress.getByName(request.getRemoteAddr());
            //InetAddress i = InetAddress.getLocalHost();
            String computerName = i.getHostName();
            logger.info("Hostname : " + computerName + "  " + i.getCanonicalHostName());
            return computerName;
        } catch (UnknownHostException uhe) {
            // TODO: Add catch code
            uhe.printStackTrace();
        }
        return null;
    }

    public void newPasswordVCE(ValueChangeEvent vce) 
    {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce!=null && newPasswordBinding.getValue()!=null)
        {
            OperationBinding ob = ADFUtils.findOperation("checkUserPassword");
            ob.getParamsMap().put("password", newPasswordBinding.getValue());
            ob.execute();
            
            if(ob.getResult()!=null && !ob.getResult().equals("T"))
            {
                String result=ob.getResult().toString();
                logger.info("newPasswordVCE Result---->>"+result);
                FacesMessage Message = new FacesMessage(result);
                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(newPasswordBinding.getClientId(), Message);   
            }
        }
    }

    public void confirmPasswordVCE(ValueChangeEvent vce) 
    {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce!=null){
        OperationBinding op = ADFUtils.findOperation("checkConfirmPass");
        op.getParamsMap().put("username", usernameBinding.getValue());
        op.getParamsMap().put("currPass", currentPasswordBinding.getValue());
        Object rs = op.execute();
        //System.out.println("The New Result is----->>"+op.getResult());

            if(op.getResult()!=null && op.getResult().equals("T"))
            {                  
                FacesMessage Message = new FacesMessage("Current Password was not match.!!");
                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(currentPasswordBinding.getClientId(), Message); 
                
            }
        }
    }

    public void setFinyearBinding(RichInputComboboxListOfValues finyearBinding) {
        this.finyearBinding = finyearBinding;
    }

    public RichInputComboboxListOfValues getFinyearBinding() {
        return finyearBinding;
    }
}
