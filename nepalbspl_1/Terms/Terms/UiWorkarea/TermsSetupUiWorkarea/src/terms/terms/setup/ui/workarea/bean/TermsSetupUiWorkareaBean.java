package terms.terms.setup.ui.workarea.bean;

import java.io.IOException;

import java.net.InetAddress;

import java.net.UnknownHostException;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import java.util.function.Consumer;

import javax.faces.application.FacesMessage;
import javax.faces.application.ViewHandler;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import oracle.adf.controller.ControllerContext;
import oracle.adf.controller.TaskFlowId;
import oracle.adf.model.BindingContext;
import oracle.adf.model.DataControlFrame;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCDataControl;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichForm;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichListView;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.nav.RichLink;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;


import oracle.ui.pattern.dynamicShell.TabContext;

import org.apache.myfaces.trinidad.context.RequestContext;
import org.apache.myfaces.trinidad.context.RequestContextFactory;

import terms.terms.setup.model.view.SearchTCodeDualVORowImpl;

import org.apache.myfaces.trinidad.event.SelectionEvent;
import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;


public class TermsSetupUiWorkareaBean {
    private RichInputText usernameBinding;
    private RichInputText passwordBinding;
    private RichInputText uniCodeBinding;
    private RichInputText finyearBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText unitNameBinding;
    private RichInputText newPasswordBinding;
    private RichInputText confirmPasswordBinding;
    private RichPopup changeFirstPassPopup;
    private RichInputText unitNBinding;
    private RichPopup bindDocCheckPopup;
    private String newTaskFlowId;
    private Map globalParam = new HashMap();
    private String taskFlowId = "/WEB-INF/MainWorkareaFlow.xml#MainWorkareaFlow";
    private String mainWorkAreaTaskFlowId = "/WEB-INF/MainWorkareaFlow.xml#MainWorkareaFlow";
    private static String TskFlowId = null;
    private static String TskFlowName = null;
    private static String TskFlowSource = null;
    private static String TskFile = null;


    public void setGlobalParam(Map globalParam) {
        this.globalParam = globalParam;
    }

    public Map getGlobalParam() {
        return globalParam;
    }


    public TermsSetupUiWorkareaBean() {
        try {

            String OnlodeFinYear =
                ADFUtils.getValueFrmExpression("#{bindings.TransFinYear.inputValue}"); //TO GET LOV VALUE 'APOORVA'
            System.out.println("Binding Expression Value++++***" + OnlodeFinYear);
            sessionScope.put("FinYear", OnlodeFinYear); // TO SET SESSION VALUE 'APOORVA'
        }

        catch (Exception ex) {
            System.out.println("++ERROR++" + ex.toString());
        }

    }

    ADFContext context1 = ADFContext.getCurrent(); //INIT SESSION SCOPE 'APOORVA'
    Map sessionScope = context1.getCurrent().getSessionScope();

    public void selectOrCreateTab(ActionEvent actionEvent) {
        // Add event code here...
    }
    private static final ADFLogger logger = ADFLogger.createADFLogger(TermsSetupUiWorkareaBean.class);

    public void selectOrCreateNewTab(ActionEvent actionEvent) {

        RichLink link = (RichLink) actionEvent.getComponent();
        int num = TabContext.getCurrentInstance().getNumberOfTabs();

        TskFlowId =
            link.getAttributes().get("TaskFlowId") != null ? link.getAttributes().get("TaskFlowId").toString() : null;
        TskFlowName =
            link.getAttributes().get("TaskFlowName") != null ? link.getAttributes().get("TaskFlowName").toString() :
            null;
        TskFlowSource =
            link.getAttributes().get("TaskFlowSource") != null ? link.getAttributes().get("TaskFlowSource").toString() :
            null;
        TskFile = (String) ADFUtils.resolveExpression("#{node.FileSname}");
        System.out.println("Task Flow Detail :-" + TskFlowId + "  " + TskFlowName + "  " + TskFlowSource);
        if (num == 0) {

            if (TskFlowSource != null && TskFlowName != null) {
                try {
                    /////////////////////////////User Wise Form Entry//////////////////////////
                    String FileName = (String) ADFUtils.resolveExpression("#{node.FileSname}");
                    System.out.println("File Name===>" + FileName + "User Id===>" +
                                       ADFUtils.resolveExpression("#{sessionScope.userId}") + "clientIp==>" +
                                       getClientIpAddress() + "/" + getClientCompName());
                    OperationBinding b = ADFUtils.findOperation("createUserSessionDtl");
                    b.getParamsMap().put("userId", ADFUtils.resolveExpression("#{sessionScope.userId}"));
                    b.getParamsMap().put("clientIp", getClientIpAddress() + "/" + getClientCompName());
                    b.getParamsMap().put("DocName", FileName);
                    b.execute();
                    ADFUtils.findOperation("Commit").execute();
                    ////////////////////////////User Wise Form Entry//////////////////////////


                    System.out.println("Task Flow Id :" + TskFlowId);
                    OperationBinding binding = ADFUtils.findOperation("fetchSecurityValues");
                    binding.getParamsMap().put("user_id", ADFUtils.resolveExpression("#{sessionScope.userId}"));
                    binding.getParamsMap().put("file_name", TskFlowName);
                    binding.getParamsMap().put("file_id", TskFlowId);
                    binding.execute();
                    if (binding.getResult() != null) {
                        String sec_string = binding.getResult().toString();
                        Map parameterMap = new HashMap();
                        parameterMap.put("viewString", sec_string.charAt(0));
                        parameterMap.put("editString", sec_string.charAt(1));
                        parameterMap.put("deleteString", sec_string.charAt(2));
                        parameterMap.put("addString", sec_string.charAt(3));
                        parameterMap.put("finYear", ADFUtils.resolveExpression("#{pageFlowScope.finYear}"));
                        parameterMap.put("unitCode", ADFUtils.resolveExpression("#{pageFlowScope.unitCode}"));
                        parameterMap.put("userName", ADFUtils.resolveExpression("#{pageFlowScope.userName}"));
                        parameterMap.put("empCode", ADFUtils.resolveExpression("#{pageFlowScope.empCode}"));
                        System.out.println("mapvaluee iss=>" + parameterMap);
                        TabContext.getCurrentInstance().addOrSelectTab(TskFlowName, TskFlowSource, parameterMap);
                        TskFlowSource = null;
                        TskFlowName = null;
                        TskFlowId = null;
                        TskFile = null;
                        bindDocCheckPopup.cancel();
                        //                            reloadThePage();
                        //  TabContext.getCurrentInstance().addOrSelectTab(TaskFlowName, TaskFlowSource);
                        //            RequestContext.getCurrentInstance().getPageFlowScope().put("secString", TaskFlowSource) ;
                        //            System.out.println("Parameter Value is " + RequestContext.getCurrentInstance().getPageFlowScope().get("secString")+"    "+RequestContext.getCurrentInstance().getPageFlowScope());

                    }
                } catch (TabContext.TabOverflowException e) {
                    e.printStackTrace();
                }
            }

        } else {
            //            reloadThePage();
            ADFUtils.showPopup(bindDocCheckPopup);
        }

    }

    public void openTabForAuth(ActionEvent actionEvent) {
        RichLink link = (RichLink) actionEvent.getComponent();
        String TaskFlowId =
            link.getAttributes().get("TaskFlowId") != null ? link.getAttributes().get("TaskFlowId").toString() : null;
        String TaskFlowName =
            link.getAttributes().get("TaskFlowName") != null ? link.getAttributes().get("TaskFlowName").toString() :
            null;
        String TaskFlowSource =
            link.getAttributes().get("TaskFlowSource") != null ? link.getAttributes().get("TaskFlowSource").toString() :
            null;
        String DocumentNo =
            link.getAttributes().get("DocumentNo") != null ? link.getAttributes().get("DocumentNo").toString() : null;
        if (TaskFlowSource != null && TaskFlowName != null && DocumentNo != null) {
            try {
                System.out.println("Task Flow Id :" + TaskFlowId + " " + TaskFlowName + " " + TaskFlowSource + " " +
                                   DocumentNo);
                OperationBinding binding = ADFUtils.findOperation("fetchSecurityValues");
                binding.getParamsMap().put("user_id", ADFUtils.resolveExpression("#{sessionScope.userId}"));
                binding.getParamsMap().put("file_name", TaskFlowName);
                binding.getParamsMap().put("file_id", TaskFlowId);
                binding.execute();
                if (binding.getResult() != null) {
                    String sec_string = binding.getResult().toString();
                    Map parameterMap = new HashMap();
                    //                parameterMap.put("viewString", sec_string.charAt(0));
                    parameterMap.put("editString", sec_string.charAt(1));
                    //                parameterMap.put("deleteString", sec_string.charAt(2));
                    //                parameterMap.put("addString", sec_string.charAt(3));
                    parameterMap.put("finYear", ADFUtils.resolveExpression("#{pageFlowScope.finYear}"));
                    parameterMap.put("unitCode", ADFUtils.resolveExpression("#{pageFlowScope.unitCode}"));
                    parameterMap.put("userName", ADFUtils.resolveExpression("#{pageFlowScope.userName}"));
                    parameterMap.put("empCode", ADFUtils.resolveExpression("#{pageFlowScope.empCode}"));
                    parameterMap.put("mode", "V");
                    DCIteratorBinding cIteratorBinding = ADFUtils.findIterator("PendingApprovalVVO1Iterator");
                    String formname =
                        cIteratorBinding.getCurrentRow().getAttribute("DocAlias") != null ?
                        (String) cIteratorBinding.getCurrentRow().getAttribute("DocAlias") : null;
                    System.out.println("Form Name Value: " + formname + " " + sec_string.charAt(1));
                    if (formname != null) {
                        System.out.println("Form Name Value 1: " + formname);
                        if (formname.equals("IND") || formname.equals("IND_CAP")) {
                            String indNo = DocumentNo.substring(0, DocumentNo.indexOf("~"));
                            String amdNo = DocumentNo.substring(DocumentNo.indexOf("~") + 1);
                            System.out.println("Indent Number :- " + indNo + " " + amdNo);
                            parameterMap.put("indNo", indNo);
                            parameterMap.put("AmdNo", amdNo);
                        } else if (formname.equals("PO") || formname.equals("PO_HO") || formname.equals("PO_IMP") ||
                                   formname.equals("PO_STOCK") || formname.equals("PO_CAP")) {
                            String poNo = DocumentNo.substring(0, DocumentNo.indexOf("~"));
                            String amdNo = DocumentNo.substring(DocumentNo.indexOf("~") + 1);
                            System.out.println("Po Number :- " + poNo + " " + amdNo);
                            parameterMap.put("PoNo", poNo);
                            parameterMap.put("AmdNo", amdNo);
                        } else if (formname.equals("REQ")) {
                            parameterMap.put("ReqSlipId", DocumentNo);
                        } else if (formname.equals("ISSUE")) {
                            parameterMap.put("issueId", DocumentNo);
                        } else if (formname.equals("CR")) {
                            parameterMap.put("ReqNumber", DocumentNo);
                        } else if (formname.equals("CT")) {
                            String joNo = DocumentNo.substring(0, DocumentNo.indexOf("~"));
                            String amdNo = DocumentNo.substring(DocumentNo.indexOf("~") + 1);
                            parameterMap.put("JoNo", joNo);
                            parameterMap.put("AmdNo", amdNo);
                        } else if (formname.equals("MPO")) {
                            String ackNo = DocumentNo.substring(0, DocumentNo.indexOf("~"));
                            String amdNo = DocumentNo.substring(DocumentNo.indexOf("~") + 1);
                            parameterMap.put("AckNumber", ackNo);
                            parameterMap.put("AmendAckNumber", amdNo);
                        } else if (formname.equals("RTN")) {
                            parameterMap.put("RetSlipId", DocumentNo);
                        } else if (formname.equals("ADVS")) {
                            parameterMap.put("AdvId", DocumentNo);
                        } else if (formname.equals("DELV")) {
                            String schNo = DocumentNo.substring(0, DocumentNo.indexOf("~"));
                            String amdNo = DocumentNo.substring(DocumentNo.indexOf("~") + 1);
                            parameterMap.put("SchNo", schNo);
                            parameterMap.put("schdAmdNo", amdNo);
                        }
                        System.out.println("mapvaluee iss=>" + parameterMap);
                        //                TabContext.getCurrentInstance().addOrSelectTab(TaskFlowName, TaskFlowSource, parameterMap);
                        TabContext.getCurrentInstance().addTab(TaskFlowName, TaskFlowSource, parameterMap);
                        //  TabContext.getCurrentInstance().addOrSelectTab(TaskFlowName, TaskFlowSource);
                        //            RequestContext.getCurrentInstance().getPageFlowScope().put("secString", TaskFlowSource) ;
                        //            System.out.println("Parameter Value is " + RequestContext.getCurrentInstance().getPageFlowScope().get("secString")+"    "+RequestContext.getCurrentInstance().getPageFlowScope());
                    }

                }
            } catch (TabContext.TabOverflowException e) {
                e.printStackTrace();
            }
        }
    }

    public void setUsernameBinding(RichInputText usernameBinding) {
        this.usernameBinding = usernameBinding;
    }

    public RichInputText getUsernameBinding() {
        return usernameBinding;
    }

    public void setPasswordBinding(RichInputText passwordBinding) {
        this.passwordBinding = passwordBinding;
    }

    public RichInputText getPasswordBinding() {
        return passwordBinding;
    }

    public void loginButtonAL(ActionEvent actionEvent) {
        // Add event code here...
        //TabContext.getCurrentInstance().closeAllTabs(actionEvent);
    }

    public void setFinyearBinding(RichInputText finyearBinding) {
        this.finyearBinding = finyearBinding;
    }

    public RichInputText getFinyearBinding() {
        return finyearBinding;
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void userNameVCE(ValueChangeEvent valueChangeEvent) {
        // Add event code here...

        System.out.println("Enter in VCE Before Process Update");
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        System.out.println("Enter in VCE After Process Update");
        String newValue = (String) valueChangeEvent.getNewValue();
        System.out.println("new value in vcll iss=" + newValue);
        try {
            String username = newValue;
            OperationBinding op1 = ADFUtils.findOperation("findUserCode");
            op1.getParamsMap().put("username", username);
            Object rst1 = op1.execute();
            System.out.println("Return Value Emp Code VCE" + rst1.toString());
            if (rst1.toString() == "Not Found") {
                //ADFUtils.showMessage("Username not found. Try Again.", 0);
                FacesMessage message = new FacesMessage("Username not found. Try Again.");
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(usernameBinding.getClientId(), message);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public String deselectAddForAllAction() {
        // Add event code here...
        return null;
    }

    public HttpSession fetchSession() {
        HttpServletRequest request =
            (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        HttpSession httpSession = request.getSession();
        return httpSession;
    }

    public void welcomeTreeAL(ActionEvent actionEvent) {
        // Add event code here...
        Object obj = actionEvent.getComponent().getAttributes().get("moduleName");
        HttpSession fetchSession = fetchSession();
        fetchSession.setAttribute("currModule", obj.toString());
    }

    public void searchTCodeVCL(ValueChangeEvent vce) {
        int num = TabContext.getCurrentInstance().getNumberOfTabs();
        if (num == 0) {

            try {
                vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
                DCIteratorBinding binding1 = ADFUtils.findIterator("SearchTCodeDualVO1Iterator");
                SearchTCodeDualVORowImpl row = (SearchTCodeDualVORowImpl) binding1.getCurrentRow();
                String fileName = (String) row.getTransFileName();
                String tfFileId = (String) row.getTransFileId();
                String flowPath = (String) row.getTransFlowPath();
                if (checkValidation(fileName, tfFileId, flowPath)) {
                    globalNavigation(fileName, tfFileId, flowPath);
                }
                if (row.getTransFileName() != null && row.getTransFlowPath() != null) {
                    try {
                        OperationBinding binding = ADFUtils.findOperation("fetchSecurityValues");
                        binding.getParamsMap().put("user_id", ADFUtils.resolveExpression("#{sessionScope.userId}"));
                        binding.getParamsMap().put("file_name", row.getTransFileName());
                        binding.getParamsMap().put("file_id", row.getTransFileId1());
                        binding.execute();

                        if (binding.getResult() != null) {
                            String sec_string = binding.getResult().toString();
                            Map parameterMap = new HashMap();
                            parameterMap.put("viewString", sec_string.charAt(0));
                            parameterMap.put("editString", sec_string.charAt(1));
                            parameterMap.put("deleteString", sec_string.charAt(2));
                            parameterMap.put("addString", sec_string.charAt(3));
                            parameterMap.put("finYear", ADFUtils.resolveExpression("#{pageFlowScope.finYear}"));
                            parameterMap.put("unitCode", ADFUtils.resolveExpression("#{pageFlowScope.unitCode}"));
                            parameterMap.put("userName", ADFUtils.resolveExpression("#{pageFlowScope.userName}"));
                            parameterMap.put("empCode", ADFUtils.resolveExpression("#{pageFlowScope.empCode}"));
                            //                TabContext.getCurrentInstance().removeCurrentTab();
                            TabContext.getCurrentInstance().addOrSelectTab(row.getTransFileName(),
                                                                           row.getTransFlowPath(), parameterMap);
                            //                System.out.println("year="+parameterMap.get("finYear"));

                        }
                    } catch (TabContext.TabOverflowException e) {
                        e.printStackTrace();
                    }
                }
                OperationBinding binding = ADFUtils.findOperation("Execute");
                binding.execute();
                reloadThePage();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            //            OperationBinding binding = ADFUtils.findOperation("Execute");
            //            binding.execute();
            //            reloadThePage();
            ADFUtils.showPopup(bindDocCheckPopup);
        }
    }

    public boolean checkValidation(String tfFileName, String tfFileId, String tfPath) {
        if (tfFileName == null) {
            ADFUtils.showMessage("File Name is missing in FileMasterErp.", 0);
            return false;
        } else if (tfFileId == null) {
            ADFUtils.showMessage("File Id is missing in FileMasterErp.", 0);
            return false;
        } else if (tfPath == null) {
            ADFUtils.showMessage("File Path is missing in FileMasterErp.", 0);
            return false;
        } else {
            return true;
        }
    }


    public void globalNavigation(String tfFileName, String tfFileId, String tfPath) {
        String userId = (String) ADFUtils.resolveExpression("#{pageFlowScope.userId}");
        OperationBinding binding = ADFUtils.findOperation("fetchSecurityValues");
        binding.getParamsMap().put("user_id", userId);
        binding.getParamsMap().put("file_name", tfFileName);
        binding.getParamsMap().put("file_id", tfFileId);
        binding.execute();
        System.out.println("Sec Value 1 ==>" + binding.getResult().toString());
        if (binding.getResult() != null) {
            String sec_string = binding.getResult().toString();
            Map parameterMap = new HashMap();
            parameterMap.put("viewString", sec_string.charAt(0));
            parameterMap.put("editString", sec_string.charAt(1));
            parameterMap.put("deleteString", sec_string.charAt(2));
            parameterMap.put("addString", sec_string.charAt(3));
            parameterMap.put("finYear", ADFUtils.resolveExpression("#{pageFlowScope.finYear}"));
            parameterMap.put("unitCode", ADFUtils.resolveExpression("#{pageFlowScope.unitCode}"));
            parameterMap.put("userName", ADFUtils.resolveExpression("#{pageFlowScope.userName}"));
            parameterMap.put("empCode", ADFUtils.resolveExpression("#{pageFlowScope.empCode}"));
            parameterMap.put("userId", ADFUtils.resolveExpression("#{pageFlowScope.userId}"));
            System.out.println("Sec Value 2 ==>" + binding.getResult().toString() + " globalParam:" + parameterMap);

            //            if (tfPath != null)
            //            {
            System.out.println("hd check--->>" + pendingChangeExist() + " tfPath:" + tfPath);
            if (this.pendingChangeExist()) {
                newTaskFlowId = tfPath;
                ADFUtils.showPopup(bindDocCheckPopup);
            } else {
                reloadThePage();
                //                    regionBind.refresh(FacesContext.getCurrentInstance());
                setDynamicTaskFlowId(tfPath, parameterMap);
            }
            //            }
            //            else {
            //
            //                reloadThePage();
            //                regionBind.refresh(FacesContext.getCurrentInstance());
            //                setDynamicTaskFlowId(taskFlowId, parameterMap);
            //            }
        } else {
            ADFUtils.showMessage("Problem in fetching security parameter.", 0);
        }
    }

    public void setDynamicTaskFlowId(String taskFlowId, Map map) {
        this.taskFlowId = taskFlowId;
        this.globalParam = map;
    }

    private boolean pendingChangeExist() {

        ControllerContext cctx = ControllerContext.getInstance();
        return cctx.getCurrentRootViewPort().getTaskFlowContext().isDataDirty();

    }

    public static String reloadThePage() {
        rollbackPartialTransaction();
        FacesContext fcontext = FacesContext.getCurrentInstance();
        String viewId = fcontext.getViewRoot().getViewId();
        String actionUrl = fcontext.getApplication().getViewHandler().getActionURL(fcontext, viewId);

        try {
            ExternalContext eContext = fcontext.getExternalContext();
            String resourceUrl = actionUrl;

            eContext.redirect(resourceUrl);

        } catch (IOException ioe) {
            // TODO: Add catch code
            ioe.printStackTrace();
        }
        return null;
    }

    public static String rollbackPartialTransaction() {
        BindingContext bc = BindingContext.getCurrent();
        DataControlFrame dcf = bc.findDataControlFrame(bc.getCurrentDataControlFrame());
        Collection<DCDataControl> dcCol = dcf.datacontrols();
        for (DCDataControl dc : dcCol) {
            if (dc.isTransactionDirty()) {
                try {
                    dc.rollbackTransaction();
                } catch (Exception e) {
                    // TODO: Add catch code
                    e.printStackTrace();
                }
            }
        }
        return null;
    }


    public void closeAllTabAL(ActionEvent actionEvent) {
        // Add event code here...
        //HttpSession fetchSession = fetchSession();
        //fetchSession.setAttribute("currModule", null);
        //TabContext.getCurrentInstance().closeAllTabs(actionEvent);
        if (ADFUtils.resolveExpression("#{sessionScope.userId}") != null &&
            ADFUtils.resolveExpression("#{sessionScope.userName}") != null &&
            ADFUtils.resolveExpression("#{sessionScope.unitCode}") != null &&
            ADFUtils.resolveExpression("#{sessionScope.unitName}") != null &&
            ADFUtils.resolveExpression("#{sessionScope.finYear}") != null &&
            ADFUtils.resolveExpression("#{sessionScope.empCode}") != null) {
            HttpSession fetchSession = fetchSession();
            OperationBinding binding = ADFUtils.findOperation("logoutUserSession");
            binding.getParamsMap().put("userId", ADFUtils.resolveExpression("#{sessionScope.userId}"));
            binding.getParamsMap().put("sessionId", fetchSession.getId().toString());
            binding.execute();
            OperationBinding opCommit = ADFUtils.findOperation("Commit");
            opCommit.execute();
        }
        FacesContext fctx = FacesContext.getCurrentInstance();
        ExternalContext ectx = fctx.getExternalContext();
        String url = ectx.getRequestContextPath() + "/faces/Login";
        //String page="http://127.0.0.1:7101/TermsApplication-TermsSetupUiWorkarea-context-root/faces/Login";
        //System.out.println("Url : "+url);
        this.launchNewPage(url);
        //HttpSession fetchSession = fetchSession();
        //fetchSession.invalidate();

        HttpSession session = (HttpSession) ectx.getSession(false);
        session.invalidate();
    }

    public void setUnitNameBinding(RichInputText unitNameBinding) {
        this.unitNameBinding = unitNameBinding;
    }

    public RichInputText getUnitNameBinding() {
        return unitNameBinding;
    }


    public void openApprPendencyAL(ActionEvent actionEvent) {

        try {
            //  #{bindings.CountPendingAuth1Iterator.currentRow}

            //            Row selectedRow = (Row)ADFUtils.evaluateEL("#{bindings.CountPendingAuth1Iterator.currentRow}");
            //            System.out.println("selected PATH  is "+selectedRow.getAttribute("LAth"));

            //            ADFUtils.invokeEL("#{bindings.Departments1.collectionModel.makeCurrent}", new Class[] { SelectionEvent.class }, new Object[] {
            //            actionEvent });
            // get the selected row , by this you can get any attribute of that row
            //            Row selectedRow = (Row) ADFUtils.evaluateEL("#{bindings.CountPendingAuth1Iterator.currentRow}");
            //            FacesMessage msg = new FacesMessage(selectedRow.getAttribute("LAth").toString());
            //            msg.setSeverity(FacesMessage.SEVERITY_INFO);
            //            FacesContext.getCurrentInstance().addMessage(null, msg);

            Row r = (Row) ADFUtils.evaluateEL("#{bindings.CountPendingAuth1Iterator.currentRow}");
            //System.out.println("L PATH VALUE "+r.getAttribute("LAth"));
            //DCIteratorBinding binding = ADFUtils.findIterator("CountPendingAuth1Iterator");
            //String plath = (String) binding.getCurrentRow().getAttribute("LAth");
            String plath = (String) r.getAttribute("LAth");

            //String plath = selectedRow.getAttribute("LAth").toString();

            System.out.println("Parameters From Ui :- " +
                               ADFUtils.resolveExpression("#{item.bindings.FormName.inputValue}") + "  " + plath);
            OperationBinding op1 = ADFUtils.findOperation("setFormNameApprPend");
            op1.getParamsMap().put("form_name", ADFUtils.resolveExpression("#{item.bindings.FormName.inputValue}"));
            op1.getParamsMap().put("lath", plath);
            op1.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void listSelectionListner(SelectionEvent selectionEvent) {

        try {
            ADFUtils.invokeEL("#{bindings.CountPendingApprovalVVO1.treeModel.makeCurrent}",
                              new Class[] { SelectionEvent.class }, new Object[] { selectionEvent });
            System.out.println("Value for plath init : ");
            Row r = (Row) ADFUtils.evaluateEL("#{bindings.CountPendingApprovalVVO1.currentRow}");
            String plath = (String) r.getAttribute("DocSt");
            //  System.out.println("Value for plath : "+plath);
            OperationBinding op1 = ADFUtils.findOperation("setDocStApprPend");
            op1.getParamsMap().put("doc_st", plath);
            op1.execute();

            Map parameterMap = new HashMap();

            parameterMap.put("finYear", ADFUtils.resolveExpression("#{pageFlowScope.finYear}"));
            parameterMap.put("unitCode", ADFUtils.resolveExpression("#{pageFlowScope.unitCode}"));
            parameterMap.put("userName", ADFUtils.resolveExpression("#{pageFlowScope.userName}"));
            parameterMap.put("empCode", ADFUtils.resolveExpression("#{pageFlowScope.empCode}"));

            //  TabContext.getCurrentInstance().addTab("Approval Pendency ", "/WEB-INF/terms/terms/setup/ui/workarea/flow/ApprovalPendencyFlow.xml#ApprovalPendencyFlow");

            TabContext.getCurrentInstance().addOrSelectTab("Approval Pendency ",
                                                           "/WEB-INF/terms/terms/setup/ui/workarea/flow/ApprovalPendencyFlow.xml#ApprovalPendencyFlow",
                                                           parameterMap);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setNewPasswordBinding(RichInputText newPasswordBinding) {
        this.newPasswordBinding = newPasswordBinding;
    }

    public RichInputText getNewPasswordBinding() {
        return newPasswordBinding;
    }

    public void setConfirmPasswordBinding(RichInputText confirmPasswordBinding) {
        this.confirmPasswordBinding = confirmPasswordBinding;
    }

    public RichInputText getConfirmPasswordBinding() {
        return confirmPasswordBinding;
    }

    public void confirmButtonAL(ActionEvent actionEvent) {
        if (confirmPasswordBinding.getValue() != null && newPasswordBinding.getValue() != null) {
            String currentPassword = "None";
            String newPassword = getNewPasswordBinding().getValue().toString();
            String confirmPassword = getConfirmPasswordBinding().getValue().toString();
            try {
                logger.info("Start change password process.1--");
                if (!newPassword.equals(confirmPassword)) {
                    ADFUtils.showMessage("New Password and Confirm Password didn't match. Try again.", 0);
                } else {
                    OperationBinding binding = ADFUtils.findOperation("setNewPassword");
                    binding.getParamsMap().put("username", ADFUtils.resolveExpression("#{sessionScope.userName}"));
                    binding.getParamsMap().put("newpassword", newPassword);
                    Object rst = binding.execute();

                    if (rst.toString().equals("Success") && binding.getErrors().isEmpty() &&
                        binding.getResult() != null) {
                        OperationBinding binding1 = ADFUtils.findOperation("setChPassHistory");
                        binding1.getParamsMap().put("username", ADFUtils.resolveExpression("#{sessionScope.userName}"));
                        binding1.getParamsMap().put("newpassword", newPassword);
                        binding1.getParamsMap().put("oldpassword", currentPassword);
                        binding1.getParamsMap().put("terminal",
                                                    ADFUtils.clientIpAddress() + "/" + ADFUtils.clientHostName());
                        binding1.getParamsMap().put("userid", ADFUtils.resolveExpression("#{sessionScope.userId}"));
                        binding1.execute();
                        ADFUtils.findOperation("Commit").execute();
                        ADFUtils.showMessage("Password Changed Successfully.", 2);

                        getChangeFirstPassPopup().cancel();
                    } else {
                        ADFUtils.showMessage("System having trouble to change password. Try Again.", 2);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            FacesMessage Message = new FacesMessage("Confirm Password is required.");
            Message.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(confirmPasswordBinding.getClientId(), Message);
        }
        confirmPasswordBinding.setValue("");
        newPasswordBinding.setValue("");
    }

    public void cancelButtonAL(ActionEvent actionEvent) {
        getChangeFirstPassPopup().hide();
    }

    public void setChangeFirstPassPopup(RichPopup changeFirstPassPopup) {
        this.changeFirstPassPopup = changeFirstPassPopup;
    }

    public RichPopup getChangeFirstPassPopup() {
        return changeFirstPassPopup;
    }

    public void redirectRFLink(ActionEvent actionEvent) {
        FacesContext fc = FacesContext.getCurrentInstance();
        OperationBinding binding = ADFUtils.findOperation("pathReportFactory");
        binding.execute();
        //System.out.println("RF Path value :-"+binding.getResult().toString());
        try {
            fc.getExternalContext().redirect(binding.getResult().toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public String getClientIpAddress() {
        try {
            String clientIpAddress =
                ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest()).getRemoteAddr();
            return clientIpAddress;
        } catch (Exception e) {
            // TODO: Add catch code
            e.printStackTrace();
        }
        return null;
    }

    public String getClientCompName() {
        try {
            HttpServletRequest request =
                (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
            InetAddress i = InetAddress.getByName(request.getRemoteAddr());
            //InetAddress i = InetAddress.getLocalHost();
            String computerName = i.getHostName();
            //System.out.println("Hostname : "+computerName+"  "+i.getCanonicalHostName());
            return computerName;
        } catch (UnknownHostException uhe) {
            // TODO: Add catch code
            uhe.printStackTrace();
        }
        return null;
    }

    public void launchNewPage(String p) {
        String newPage = p;
        FacesContext ctx = FacesContext.getCurrentInstance();
        ExtendedRenderKitService erks = Service.getService(ctx.getRenderKit(), ExtendedRenderKitService.class);
        String url = "window.open('" + newPage + "','_self');";
        erks.addScript(FacesContext.getCurrentInstance(), url);
    }

    public void newPasswordCheckVCE(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce != null && newPasswordBinding.getValue() != null) {
            OperationBinding ob = ADFUtils.findOperation("checkUserPassword");
            ob.getParamsMap().put("password", newPasswordBinding.getValue());
            ob.execute();

            if (ob.getResult() != null && !ob.getResult().equals("T")) {
                String result = ob.getResult().toString();
                logger.info("newPasswordCheckVCE Result---->>" + result);
                FacesMessage Message = new FacesMessage(result);
                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(newPasswordBinding.getClientId(), Message);
            }
        }
    }

    public static void setUnitSessionParameters(String unitCode, String unitName) {
        ADFUtils.setPageFlowScopeAttribute("unitCode", unitCode);
        ADFUtils.setPageFlowScopeAttribute("unitName", unitName);
    }

    public static void setFinyearSessionParameters(String finYear) {
        ADFUtils.setPageFlowScopeAttribute("finYear", finYear);
    }

    public static void setUserSessionParameters(String userName, String empCode, String userId) {
        ADFUtils.setPageFlowScopeAttribute("userName", userName);
        ADFUtils.setPageFlowScopeAttribute("empCode", empCode);
        ADFUtils.setPageFlowScopeAttribute("userId", userId);
    }

    public void unitCodeVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce != null && getUnitNBinding().getValue() != null) {
            TermsSetupUiWorkareaBean.setUnitSessionParameters(vce.getNewValue().toString(),
                                                              getUnitNBinding().getValue().toString());
        }
    }

    public void finacialYearVCL(ValueChangeEvent vce) {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce != null) {
            TermsSetupUiWorkareaBean.setFinyearSessionParameters(vce.getNewValue().toString());
            String s = vce.getNewValue().toString();
            System.out.println("FinYear---" + vce.getNewValue().toString());
            sessionScope.put("FinYear", vce.getNewValue().toString()); // TO SET SESSION VALUE 'APOORVA'

        }
    }

    public void setUnitNBinding(RichInputText unitNBinding) {
        this.unitNBinding = unitNBinding;
    }

    public RichInputText getUnitNBinding() {
        return unitNBinding;
    }

    public void setBindDocCheckPopup(RichPopup bindDocCheckPopup) {
        this.bindDocCheckPopup = bindDocCheckPopup;
    }

    public RichPopup getBindDocCheckPopup() {
        return bindDocCheckPopup;
    }


    public void openForm(ActionEvent actionEvent) {
        try {
            //                vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
            DCIteratorBinding binding1 = ADFUtils.findIterator("SearchTCodeDualVO1Iterator");
            SearchTCodeDualVORowImpl row = (SearchTCodeDualVORowImpl) binding1.getCurrentRow();

            if (row.getTransFileName() != null && row.getTransFlowPath() != null) {
                try {
                    OperationBinding binding = ADFUtils.findOperation("fetchSecurityValues");
                    binding.getParamsMap().put("user_id", ADFUtils.resolveExpression("#{sessionScope.userId}"));
                    binding.getParamsMap().put("file_name", row.getTransFileName());
                    binding.getParamsMap().put("file_id", row.getTransFileId1());
                    binding.execute();

                    if (binding.getResult() != null) {
                        String sec_string = binding.getResult().toString();
                        Map parameterMap = new HashMap();
                        parameterMap.put("viewString", sec_string.charAt(0));
                        parameterMap.put("editString", sec_string.charAt(1));
                        parameterMap.put("deleteString", sec_string.charAt(2));
                        parameterMap.put("addString", sec_string.charAt(3));
                        parameterMap.put("finYear", ADFUtils.resolveExpression("#{pageFlowScope.finYear}"));
                        parameterMap.put("unitCode", ADFUtils.resolveExpression("#{pageFlowScope.unitCode}"));
                        parameterMap.put("userName", ADFUtils.resolveExpression("#{pageFlowScope.userName}"));
                        parameterMap.put("empCode", ADFUtils.resolveExpression("#{pageFlowScope.empCode}"));
                        TabContext.getCurrentInstance().removeCurrentTab();
                        TabContext.getCurrentInstance().addOrSelectTab(row.getTransFileName(), row.getTransFlowPath(),
                                                                       parameterMap);
                        //                System.out.println("year="+parameterMap.get("finYear"));

                    }
                } catch (TabContext.TabOverflowException e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    System.out.println("Task Flow Detail :-" + TskFlowId + "  " + TskFlowName + "  " + TskFlowSource);
                    if (TskFlowSource != null && TskFlowName != null) {
                        try {
                            /////////////////////////////User Wise Form Entry//////////////////////////
                            //                                String FileName = (String) ADFUtils.resolveExpression("#{node.FileSname}");
                            String FileName = TskFile;
                            System.out.println("File Name===>" + FileName + "User Id===>" +
                                               ADFUtils.resolveExpression("#{sessionScope.userId}") + "clientIp==>" +
                                               getClientIpAddress() + "/" + getClientCompName());
                            OperationBinding b = ADFUtils.findOperation("createUserSessionDtl");
                            b.getParamsMap().put("userId", ADFUtils.resolveExpression("#{sessionScope.userId}"));
                            b.getParamsMap().put("clientIp", getClientIpAddress() + "/" + getClientCompName());
                            b.getParamsMap().put("DocName", FileName);
                            b.execute();
                            ADFUtils.findOperation("Commit").execute();
                            ////////////////////////////User Wise Form Entry//////////////////////////


                            System.out.println("Task Flow Id :" + TskFlowId);
                            OperationBinding binding = ADFUtils.findOperation("fetchSecurityValues");
                            binding.getParamsMap().put("user_id", ADFUtils.resolveExpression("#{sessionScope.userId}"));
                            binding.getParamsMap().put("file_name", TskFlowName);
                            binding.getParamsMap().put("file_id", TskFlowId);
                            binding.execute();
                            if (binding.getResult() != null) {
                                String sec_string = binding.getResult().toString();
                                Map parameterMap = new HashMap();
                                parameterMap.put("viewString", sec_string.charAt(0));
                                parameterMap.put("editString", sec_string.charAt(1));
                                parameterMap.put("deleteString", sec_string.charAt(2));
                                parameterMap.put("addString", sec_string.charAt(3));
                                parameterMap.put("finYear", ADFUtils.resolveExpression("#{pageFlowScope.finYear}"));
                                parameterMap.put("unitCode", ADFUtils.resolveExpression("#{pageFlowScope.unitCode}"));
                                parameterMap.put("userName", ADFUtils.resolveExpression("#{pageFlowScope.userName}"));
                                parameterMap.put("empCode", ADFUtils.resolveExpression("#{pageFlowScope.empCode}"));
                                System.out.println("mapvaluee iss=>" + parameterMap);
                                TabContext.getCurrentInstance().removeCurrentTab();
                                TabContext.getCurrentInstance().addOrSelectTab(TskFlowName, TskFlowSource,
                                                                               parameterMap);
                                TskFlowSource = null;
                                TskFlowName = null;
                                TskFlowId = null;
                                TskFile = null;

                                //  TabContext.getCurrentInstance().addOrSelectTab(TaskFlowName, TaskFlowSource);
                                //            RequestContext.getCurrentInstance().getPageFlowScope().put("secString", TaskFlowSource) ;
                                //            System.out.println("Parameter Value is " + RequestContext.getCurrentInstance().getPageFlowScope().get("secString")+"    "+RequestContext.getCurrentInstance().getPageFlowScope());

                            }
                        } catch (TabContext.TabOverflowException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
            bindDocCheckPopup.cancel();
            OperationBinding binding = ADFUtils.findOperation("Execute");
            binding.execute();
            reloadThePage();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public void closeForm(ActionEvent actionEvent) {
        bindDocCheckPopup.cancel();
    }


}


