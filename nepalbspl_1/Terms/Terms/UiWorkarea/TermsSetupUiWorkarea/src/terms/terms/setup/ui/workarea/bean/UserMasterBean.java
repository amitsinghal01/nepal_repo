package terms.terms.setup.ui.workarea.bean;

import java.lang.reflect.Method;

import java.math.BigDecimal;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import javax.faces.validator.ValidatorException;

import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichDialog;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.nav.RichButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.OperationBinding;

public class UserMasterBean 
{
    private RichButton headerEditBinding;
    private RichPanelHeader getMyPageRootComponent;
    private RichOutputText bindingOutputText;
    private RichInputText userIdDetailBinding;
    private RichInputText useridHeaderBinding;
    private RichInputText userfNameBinding;
    private RichButton detailcreateBinding;
    private RichButton detaildeleteBinding;
    private RichTable createUserMasterTableBinding;
    private RichInputComboboxListOfValues unitCodeBinding;
    private RichInputText userNameBinding;
    private RichInputDate validFromBinding;
    private RichInputText userPwdBinding;
    private RichInputText confirmPwdBinding;

    public UserMasterBean() {
    }

    private static final ADFLogger logger = ADFLogger.createADFLogger(UserMasterBean.class);
    
    public void EditButtonAL(ActionEvent actionEvent) {
        cevmodecheck();
    }

    public void setHeaderEditBinding(RichButton headerEditBinding) {
        this.headerEditBinding = headerEditBinding;
    }

    public RichButton getHeaderEditBinding() {
        return headerEditBinding;
    }

    public void setGetMyPageRootComponent(RichPanelHeader getMyPageRootComponent) {
        this.getMyPageRootComponent = getMyPageRootComponent;
    }

    public RichPanelHeader getGetMyPageRootComponent() {
        return getMyPageRootComponent;
    }

    public void makeComponentHierarchyReadOnly(UIComponent component, boolean valueComponent) {
        try {
            Method method1 = component.getClass().getMethod("setDisabled", boolean.class);
            if (method1 != null) {
                method1.invoke(component, valueComponent);
            }
        } catch (NoSuchMethodException e) {
            try {
                Method method = component.getClass().getMethod("setReadOnly", boolean.class);
                if (method != null) {
                    method.invoke(component, valueComponent);
                }
            } catch (Exception e1) {
                // e.printStackTrace();//silently eat this exception.
            }


        } catch (Exception e) {
            // e.printStackTrace();//silently eat this exception.
        }
        List<UIComponent> childComponents = component.getChildren();
        for (UIComponent comp : childComponents) {
            makeComponentHierarchyReadOnly(comp, valueComponent);
        }
    }

    private void cevmodecheck() {
        if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("V")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), true);
            cevModeDisableComponent("V");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("C")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("C");
        } else if (ADFUtils.resolveExpression("#{pageFlowScope.mode}").equals("E")) {
            makeComponentHierarchyReadOnly(this.getGetMyPageRootComponent(), false);
            cevModeDisableComponent("E");
        }
    }

    public void cevModeDisableComponent(String mode) {

        if (mode.equals("E")) {
            getHeaderEditBinding().setDisabled(true);
            getDetailcreateBinding().setDisabled(false);
            getDetaildeleteBinding().setDisabled(false);
            getUserIdDetailBinding().setDisabled(true);
            getUseridHeaderBinding().setDisabled(true);
            getUserfNameBinding().setDisabled(true);
            getUnitCodeBinding().setDisabled(true);
            getUserNameBinding().setDisabled(true);
            getValidFromBinding().setDisabled(true);
        } else if (mode.equals("C")) {
            getHeaderEditBinding().setDisabled(true);
            getDetailcreateBinding().setDisabled(false);
            getDetaildeleteBinding().setDisabled(false);
            getUserIdDetailBinding().setDisabled(true);
            getUseridHeaderBinding().setDisabled(true);
            getUserfNameBinding().setDisabled(true);
        } else if (mode.equals("V")) {
            getDetailcreateBinding().setDisabled(true);
            getDetaildeleteBinding().setDisabled(true);
        }
    }

    public void setBindingOutputText(RichOutputText bindingOutputText) {
        this.bindingOutputText = bindingOutputText;
    }

    public RichOutputText getBindingOutputText() {
        cevmodecheck();
        return bindingOutputText;
    }

    public void setUserIdDetailBinding(RichInputText userIdDetailBinding) {
        this.userIdDetailBinding = userIdDetailBinding;
    }

    public RichInputText getUserIdDetailBinding() {
        return userIdDetailBinding;
    }

    public void setUseridHeaderBinding(RichInputText useridHeaderBinding) {
        this.useridHeaderBinding = useridHeaderBinding;
    }

    public RichInputText getUseridHeaderBinding() {
        return useridHeaderBinding;
    }

    public void EmployeeIdVCL(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (useridHeaderBinding.getValue() == null) {
            ADFUtils.findOperation("getUserId").execute();
        }
    }

    public void userNameVCL(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (valueChangeEvent.getNewValue() != null) {
            System.out.println("in username vcl");
            OperationBinding ob = ADFUtils.findOperation("empNameSet");
            ob.getParamsMap().put("userName", valueChangeEvent.getNewValue());
            ob.execute();
        }
    }

    public void setUserfNameBinding(RichInputText userfNameBinding) {
        this.userfNameBinding = userfNameBinding;
    }

    public RichInputText getUserfNameBinding() {
        return userfNameBinding;
    }

    public void setDetailcreateBinding(RichButton detailcreateBinding) {
        this.detailcreateBinding = detailcreateBinding;
    }

    public RichButton getDetailcreateBinding() {
        return detailcreateBinding;
    }

    public void setDetaildeleteBinding(RichButton detaildeleteBinding) {
        this.detaildeleteBinding = detaildeleteBinding;
    }

    public RichButton getDetaildeleteBinding() {
        return detaildeleteBinding;
    }

    public void deletePopupDialogDL(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            ADFUtils.findOperation("Delete").execute();
            System.out.println("Record Delete Successfully");

            FacesMessage Message = new FacesMessage("Record Deleted Successfully");
            Message.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);

        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(createUserMasterTableBinding);
    }

    public void setCreateUserMasterTableBinding(RichTable createUserMasterTableBinding) {
        this.createUserMasterTableBinding = createUserMasterTableBinding;
    }

    public RichTable getCreateUserMasterTableBinding() {
        return createUserMasterTableBinding;
    }

    public void saveAL(ActionEvent actionEvent) {
        BigDecimal editText=new BigDecimal(bindingOutputText.getValue().toString());
        System.out.println("EditText Value:"+editText);
                if(editText.compareTo(new BigDecimal(0))==0){
                    OperationBinding op = ADFUtils.findOperation("Commit");
                    Object rst = op.execute();
                    System.out.println("--------Commit-------");
                    if (op.getErrors().isEmpty()) {
                        FacesMessage Message =
                            new FacesMessage("New User Created Sucessfully.");
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);
                        FacesContext fc = FacesContext.getCurrentInstance();
                        fc.addMessage(null, Message);  
                    }
                    OperationBinding ob = ADFUtils.findOperation("encryptPassword");
                    ob.getParamsMap().put("password", userPwdBinding.getValue().toString());
                    ob.execute();
                }else{
                    OperationBinding op = ADFUtils.findOperation("Commit");
                    Object rst = op.execute();
                    System.out.println("--------Commit-------");
                    if (op.getErrors().isEmpty()) {
                        FacesMessage Message = new FacesMessage("Record Update Successfully.");
                        Message.setSeverity(FacesMessage.SEVERITY_INFO);
                        FacesContext fc = FacesContext.getCurrentInstance();
                        fc.addMessage(null, Message);    
                    }    
                }
    }

    public void setUnitCodeBinding(RichInputComboboxListOfValues unitCodeBinding) {
        this.unitCodeBinding = unitCodeBinding;
    }

    public RichInputComboboxListOfValues getUnitCodeBinding() {
        return unitCodeBinding;
    }

    public void setUserNameBinding(RichInputText userNameBinding) {
        this.userNameBinding = userNameBinding;
    }

    public RichInputText getUserNameBinding() {
        return userNameBinding;
    }

    public void setValidFromBinding(RichInputDate validFromBinding) {
        this.validFromBinding = validFromBinding;
    }

    public RichInputDate getValidFromBinding() {
        return validFromBinding;
    }


    public void setUserPwdBinding(RichInputText userPwdBinding) {
        this.userPwdBinding = userPwdBinding;
    }

    public RichInputText getUserPwdBinding() {
        return userPwdBinding;
    }

    public void setConfirmPwdBinding(RichInputText confirmPwdBinding) {
        this.confirmPwdBinding = confirmPwdBinding;
    }

    public RichInputText getConfirmPwdBinding() {
        return confirmPwdBinding;
    }

    public void confirmPwdValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        
        System.out.println("----IN CheckPWD VCL----");
        System.out.println("userpwd:"+userPwdBinding.getValue()+" ------ confirm pwd:"+object);
        if(object!=null && userPwdBinding.getValue()!=null){
            String usrpwd=userPwdBinding.getValue().toString();
            String cnfrmpwd=object.toString();
            System.out.println(" value after converting to string userpwd:"+usrpwd+" confirm pwd:"+cnfrmpwd);
            System.out.println("String equals or not:"+usrpwd.equalsIgnoreCase(cnfrmpwd));
            if(!usrpwd.equalsIgnoreCase(cnfrmpwd)){
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "User Paswword and Confirm Password does not match.", null));
            }
        }

    }

    public void usrPasswordVCE(ValueChangeEvent vce) 
    {
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if(vce!=null && userPwdBinding.getValue()!=null)
        {
            OperationBinding ob = ADFUtils.findOperation("checkUserPassword");
            ob.getParamsMap().put("password", userPwdBinding.getValue());
            ob.execute();
            
            if(ob.getResult()!=null && ob.getResult().equals("T"))
            {
                logger.info("Success");
            }
            else
            {
                String result=ob.getResult().toString();
                logger.info("usrPasswordVCE Result---->>"+result);
                FacesMessage Message = new FacesMessage(result);
                Message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(userPwdBinding.getClientId(), Message);   
            }
        }
    }
    
}
