package terms.terms.setup.ui.workarea.bean;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.validator.ValidatorException;

import oracle.adf.view.rich.component.rich.data.RichTable;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.input.RichSelectOneRadio;
import oracle.adf.view.rich.event.QueryEvent;
import oracle.adf.view.rich.model.FilterableQueryDescriptor;

import oracle.jbo.domain.Date;

import oracle.binding.OperationBinding;

public class UserPriviledgebean {
    private RichTable userPrivTableBinding;
    private RichSelectOneRadio optionBinding;
    private RichInputComboboxListOfValues transUserCodeBinding;
    private RichInputComboboxListOfValues transCopyUserBinding;
    private RichSelectOneChoice transModuleBinding;

    public UserPriviledgebean() {
    }
    private Date currentDate;
    private String mode =
        ADFUtils.resolveExpression("#{pageFlowScope.PARAM_MODE}") != null ?
        ADFUtils.resolveExpression("#{pageFlowScope.PARAM_MODE}").toString() : null;

    public void moduleNameVCL(ValueChangeEvent vce) {
        if (vce.getNewValue() != null && vce.getNewValue().toString().trim().length() > 0) {
            resetTbaleFilters(userPrivTableBinding);
            OperationBinding binding = ADFUtils.findOperation("fetchUserPriviledgeDetails");
            binding.getParamsMap().put("module_Id", vce.getNewValue().toString());
            binding.execute();
        }
    }

    public String saveButtonAction() {
        OperationBinding binding = ADFUtils.findOperation("deleteBeforeCommit");
        binding.execute();
        mode = "V";
        return "save_go";
    }

    public void setCurrentDate(Date currentDate) {
        this.currentDate = currentDate;
    }

    public Date getCurrentDate() {
        Date date = new Date();
        Date sysdate = new Date(date.getCurrentDate().dateValue());
        return sysdate;
    }

    public String userPriviledgeEditAction() {
        System.out.println("mode value in edit iss =" + mode);
        mode = "E";
        return null;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getMode() {
        return mode;
    }

    public String selectAddForAllAction() {
        OperationBinding binding = ADFUtils.findOperation("selectAndDeselectMethod");
        binding.getParamsMap().put("mode_type", "S");
        binding.getParamsMap().put("grant_type", "A");
        binding.execute();
        return null;
    }

    public String selectEditForAllAction() {
        OperationBinding binding = ADFUtils.findOperation("selectAndDeselectMethod");
        binding.getParamsMap().put("mode_type", "S");
        binding.getParamsMap().put("grant_type", "E");
        binding.execute();
        return null;
    }

    public String deselectAddForAllAction() {
        OperationBinding binding = ADFUtils.findOperation("selectAndDeselectMethod");
        binding.getParamsMap().put("mode_type", "D");
        binding.getParamsMap().put("grant_type", "A");
        binding.execute();
        return null;
    }

    public String deselectEditForAllAction() {
        OperationBinding binding = ADFUtils.findOperation("selectAndDeselectMethod");
        binding.getParamsMap().put("mode_type", "D");
        binding.getParamsMap().put("grant_type", "E");
        binding.execute();
        return null;
    }

    public String selectDelForAllAction() {
        OperationBinding binding = ADFUtils.findOperation("selectAndDeselectMethod");
        binding.getParamsMap().put("mode_type", "S");
        binding.getParamsMap().put("grant_type", "D");
        binding.execute();
        return null;
    }

    public String deselectDelForAllAction() {
        OperationBinding binding = ADFUtils.findOperation("selectAndDeselectMethod");
        binding.getParamsMap().put("mode_type", "D");
        binding.getParamsMap().put("grant_type", "D");
        binding.execute();
        return null;
    }

    public String selectViewForAllAction() {
        OperationBinding binding = ADFUtils.findOperation("selectAndDeselectMethod");
        binding.getParamsMap().put("mode_type", "S");
        binding.getParamsMap().put("grant_type", "V");
        binding.execute();
        return null;
    }

    public String deselectViewForAllAction() {
        OperationBinding binding = ADFUtils.findOperation("selectAndDeselectMethod");
        binding.getParamsMap().put("mode_type", "D");
        binding.getParamsMap().put("grant_type", "V");
        binding.execute();
        return null;
    }

    public String resetTbaleFilters(RichTable tableBinding) {
        FilterableQueryDescriptor filterModel = (FilterableQueryDescriptor) tableBinding.getFilterModel();
        if (filterModel != null && filterModel.getFilterCriteria() != null)
            filterModel.getFilterCriteria().clear();
        return null;
    }

    public void setUserPrivTableBinding(RichTable userPrivTableBinding) {
        this.userPrivTableBinding = userPrivTableBinding;
    }

    public RichTable getUserPrivTableBinding() {
        return userPrivTableBinding;
    }

    public void setOptionBinding(RichSelectOneRadio optionBinding) {
        this.optionBinding = optionBinding;
    }

    public RichSelectOneRadio getOptionBinding() {
        return optionBinding;
    }

    public void changeSelectEntity(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
        String val1 = getOptionBinding().getValue().toString();
        System.out.println("Value For val1 " + val1);
        if (val1.equalsIgnoreCase("N")) {
            System.out.println("New User");
            getTransCopyUserBinding().setDisabled(true);
            getTransUserCodeBinding().setDisabled(false);
            getTransModuleBinding().setDisabled(false);
            getTransCopyUserBinding().setValue("");
        } else if (val1.equalsIgnoreCase("U")) {
            System.out.println("Copy User");
            getTransCopyUserBinding().setDisabled(false);
            getTransUserCodeBinding().setDisabled(false);
            getTransModuleBinding().setDisabled(true);
            getTransModuleBinding().setValue("");
        }
    }

    public void setTransUserCodeBinding(RichInputComboboxListOfValues transUserCodeBinding) {
        this.transUserCodeBinding = transUserCodeBinding;
    }

    public RichInputComboboxListOfValues getTransUserCodeBinding() {
        return transUserCodeBinding;
    }

    public void setTransCopyUserBinding(RichInputComboboxListOfValues transCopyUserBinding) {
        this.transCopyUserBinding = transCopyUserBinding;
    }

    public RichInputComboboxListOfValues getTransCopyUserBinding() {
        return transCopyUserBinding;
    }

    public void setTransModuleBinding(RichSelectOneChoice transModuleBinding) {
        this.transModuleBinding = transModuleBinding;
    }

    public RichSelectOneChoice getTransModuleBinding() {
        return transModuleBinding;
    }

    public void copyUserVCL(ValueChangeEvent vce) {
        // Add event code here...
        vce.getComponent().processUpdates(FacesContext.getCurrentInstance());
        if (vce.getNewValue() != null && vce.getNewValue().toString().trim().length() > 0) {
            System.out.println("Value For User Code  & Copy User "+getTransUserCodeBinding().getValue()+" "+vce.getNewValue().toString());
            if (!getTransUserCodeBinding().getValue().toString().equals(vce.getNewValue().toString())){
                OperationBinding binding1 = ADFUtils.findOperation("checkUserPrivledgesInDB");
                binding1.execute();
                System.out.println("Count For Records Saved in DB "+binding1.getResult());
                if(binding1.getResult() != null && (Integer) binding1.getResult() == 0){
                    resetTbaleFilters(userPrivTableBinding);
                    OperationBinding binding = ADFUtils.findOperation("fetchCopyUserDetails");
                    binding.getParamsMap().put("cp_user_id", vce.getNewValue().toString());
                    binding.execute();   
                } else
                ADFUtils.showMessage("User already have some privledges. System not allowed to copy user functionality.", 2);
            } else {
                ADFUtils.showMessage("Copy User Code could not same as User Code. Select another one and try again.", 1);
                getTransCopyUserBinding().setValue("");
                resetTbaleFilters(userPrivTableBinding);
                OperationBinding binding2 = ADFUtils.findOperation("deleteCopyUserDetails");
                binding2.execute();
            }    
        }
    }
}
