package terms.terms.setup.ui.workarea.bean;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;

import oracle.adf.view.rich.model.FilterableQueryDescriptor;

import oracle.binding.OperationBinding;
 
public class UserRoleAccessBean {

    private RichTable userRoleTableBinding;

    public UserRoleAccessBean() {
    }
    
    private String mode =
           ADFUtils.resolveExpression("#{pageFlowScope.mode}") != null ?
           ADFUtils.resolveExpression("#{pageFlowScope.mode}").toString() : null;
    
    
    public String userPriviledgeEditAction() {
           System.out.println("mode value in edit iss =" + mode);
           mode = "E";
           return null;
       }
    
    public void moduleViseDataPopVCE(ValueChangeEvent vce) {
        if (vce.getNewValue() != null && vce.getNewValue().toString().trim().length() > 0) {
            resetTbaleFilters(userRoleTableBinding);
            OperationBinding binding = ADFUtils.findOperation("fetchUserRoleDetails");
            binding.getParamsMap().put("module_Id", vce.getNewValue().toString());
            binding.execute();
        }
    }
    public String selectAddForAllAction() {
           OperationBinding binding = ADFUtils.findOperation("selectAndDeselectMethodForUSerRole");
           binding.getParamsMap().put("mode_type", "S");
           binding.getParamsMap().put("grant_type", "A");
           binding.execute();
           return null;
       }

       public String selectEditForAllAction() {
           OperationBinding binding = ADFUtils.findOperation("selectAndDeselectMethodForUSerRole");
           binding.getParamsMap().put("mode_type", "S");
           binding.getParamsMap().put("grant_type", "E");
           binding.execute();
           return null;
       }

       public String deselectAddForAllAction() {
           OperationBinding binding = ADFUtils.findOperation("selectAndDeselectMethodForUSerRole");
           binding.getParamsMap().put("mode_type", "D");
           binding.getParamsMap().put("grant_type", "A");
           binding.execute();
           return null;
       }

       public String deselectEditForAllAction() {
           OperationBinding binding = ADFUtils.findOperation("selectAndDeselectMethodForUSerRole");
           binding.getParamsMap().put("mode_type", "D");
           binding.getParamsMap().put("grant_type", "E");
           binding.execute();
           return null;
       }

       public String selectDelForAllAction() {
           OperationBinding binding = ADFUtils.findOperation("selectAndDeselectMethodForUSerRole");
           binding.getParamsMap().put("mode_type", "S");
           binding.getParamsMap().put("grant_type", "D");
           binding.execute();
           return null;
       }

       public String deselectDelForAllAction() {
           OperationBinding binding = ADFUtils.findOperation("selectAndDeselectMethodForUSerRole");
           binding.getParamsMap().put("mode_type", "D");
           binding.getParamsMap().put("grant_type", "D");
           binding.execute();
           return null;
       }

       public String selectViewForAllAction() {
           OperationBinding binding = ADFUtils.findOperation("selectAndDeselectMethodForUSerRole");
           binding.getParamsMap().put("mode_type", "S");
           binding.getParamsMap().put("grant_type", "V");
           binding.execute();
           return null;
       }

       public String deselectViewForAllAction() {
           OperationBinding binding = ADFUtils.findOperation("selectAndDeselectMethodForUSerRole");
           binding.getParamsMap().put("mode_type", "D");
           binding.getParamsMap().put("grant_type", "V");
           binding.execute();
           return null;
       }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getMode() {
        return mode;
    }
    public String resetTbaleFilters(RichTable tableBinding)
    {
        FilterableQueryDescriptor filterModel = (FilterableQueryDescriptor) tableBinding.getFilterModel();
        if (filterModel != null && filterModel.getFilterCriteria() != null)
            filterModel.getFilterCriteria().clear();
        return null;
    }

    public void setUserRoleTableBinding(RichTable userRoleTableBinding) {
        this.userRoleTableBinding = userRoleTableBinding;
    }

    public RichTable getUserRoleTableBinding() {
        return userRoleTableBinding;
    }
}
