package terms.terms.setup.ui.workarea.bean;

import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;

public class UserSessionBean {
    private RichTable tableBinding;

    public UserSessionBean() {
    }

    public void saveButtonAL(ActionEvent actionEvent) {
        
        ADFUtils.findOperation("Commit").execute();
        ADFUtils.showMessage("Record Updated Successfully.",2);
       // AdfFacesContext.getCurrentInstance().addPartialTarget(tableBinding); 
    }

    public void setTableBinding(RichTable tableBinding) {
        this.tableBinding = tableBinding;
    }

    public RichTable getTableBinding() {
        return tableBinding;
    }
}
